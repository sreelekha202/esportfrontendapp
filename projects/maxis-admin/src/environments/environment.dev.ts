export const environment = {
  production: false,
  buildConfig: 'dev',
  apiEndPoint:
    'https://8k0ce7eqed.execute-api.ap-southeast-1.amazonaws.com/dev/',
  currentToken: 'DUID',
  socketEndPoint: 'https://chat-maxis.dynasty-dev.com',
  socketEndPointWSS: 'wss://chat-maxis.dynasty-dev.com',
  cookieDomain: '.dynasty-dev.com',
  articleS3BucketName: 'article',
  tournamentS3BucketName: 'tournament',
  enableFirebase: false,
  maxisLogin:
    'https://id2-uat.maxis.com.my/oauth2/authorize?client_id=MAXISGAME&scope=openid%20maxis_profile%20email%20entitlement%20name%20uuid%20msisdn%20brand&response_type=code&redirect_uri=https://maxis-stg-admin.dynasty-dev.com/home&nonce=maxisgame&brand=MAXIS',
  maxisLogout:
    'https://id2-uat.maxis.com.my/logout?returnUrl=https://maxis-stg-admin.dynasty-dev.com/home',

  defaultLangCode: 'en',
  rtl: ['en'],
  language: [
    { code: 'en', key: 'english', value: 'English' },
  ],
  pageSizeOptions: [5, 10, 15, 20],
  gTagId: '',
  gTagUrl: 'https://www.googletagmanager.com/gtag/js?id=',
  gtmId: '',
};
