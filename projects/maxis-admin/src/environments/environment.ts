export const environment = {
  production: false,
  buildConfig: 'dev',
  // ====> Dev Api Endpoint :
  // apiEndPoint: "https://8k0ce7eqed.execute-api.ap-southeast-1.amazonaws.com/dev/",
  // ====> Staging Api EndPoint :
  apiEndPoint:
    'https://v99xl7m118.execute-api.ap-southeast-1.amazonaws.com/stage/',
  currentToken: 'DUID',
  facebookAPPID: '1417432915264232',
  googleAPPID:
    '346336302247-93msahs9cjrh82dfefgb824sske1g1uo.apps.googleusercontent.com',
  socketEndPoint: 'https://chat-maxis.dynasty-staging.com',
  socketEndPointWSS: 'wss://chat-maxis.dynasty-staging.com',
  cookieDomain: 'localhost',
  eCommerceAPIEndPoint:
    'https://tuub60cp6h.execute-api.ap-southeast-1.amazonaws.com/dev/',
  firebase: {
    apiKey: 'AIzaSyATZFDppCblUstkDrf7HZjMktacH2zkKPU',
    authDomain: 'paida-test.firebaseapp.com',
    projectId: 'paida-test',
    storageBucket: 'paida-test.appspot.com',
    messagingSenderId: '78844726296',
    appId: '1:78844726296:web:607036be8f64ce7f79476d',
    measurementId: 'G-YNC2LN04G9',
  },
  articleS3BucketName: 'article',
  tournamentS3BucketName: 'tournament',
  iOSBundelName: '',
  iOSAppstoreURL: '',
  paypal_client_id:
    'ASf-mIwkHu2j6hchcTquNOcfFtBbfzxC5qhbjOiXrQ1v-DTAlsUFnge0XHEJrUC_UtDiSBR7UvTK48WE&currency=EUR',
  enableFirebase: true,
  // staging
  maxisLogin:
    'https://id2-uat.maxis.com.my/oauth2/authorize?client_id=MAXISGAME&scope=openid%20maxis_profile%20email%20entitlement%20name%20uuid%20msisdn%20brand&response_type=code&redirect_uri=https://maxis-stg-admin.dynasty-staging.com/home&nonce=maxisgame&brand=MAXIS',
  maxisLogout:
    'https://id2-uat.maxis.com.my/logout?returnUrl=https://maxis-stg-admin.dynasty-staging.com/home',
  // dev
  // maxisLogin: "https://id2-uat.maxis.com.my/oauth2/authorize?client_id=MAXISGAME&scope=openid%20maxis_profile%20email%20entitlement%20name%20uuid%20msisdn%20brand&response_type=code&redirect_uri=https://maxis-dev.dynasty-dev.com/home&nonce=maxisgame&brand=MAXIS",
  // maxisLogout: "https://id2-uat.maxis.com.my/logout?returnUrl=https://maxis-dev.dynasty-dev.com/home",
  productDetailsApiEndPoint:
    'https://vydprijeg9.execute-api.ap-southeast-1.amazonaws.com/core/user/eshop/productDetails/',
  defaultLangCode: 'en',
  rtl: ['en'],
  language: [
    { code: 'en', key: 'english', value: 'English' },
    { code: 'ar', key: 'arabic', value: 'عربى' },
  ],
  pageSizeOptions: [5, 10, 15, 20],
  gTagId: '',
  gTagUrl: 'https://www.googletagmanager.com/gtag/js?id=',
  gtmId: '',
};
