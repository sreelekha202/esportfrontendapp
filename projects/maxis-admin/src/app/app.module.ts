import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DatePipe } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { JwtInterceptor } from './core/helpers/interceptors/token-interceptors.service';
import { AppRoutingModule } from './app-routing.module';
import { CoreModule } from './core/core.module';
import { SharedModule } from './shared/modules/shared.module';
import { AppComponent } from './app.component';
import { FooterComponent } from './core/footer/footer.component';
import { HeaderComponent } from './core/header/header.component';
import { UserPopupComponent } from './core/header/components/user-popup/user-popup.component';
import { TermsOfUsComponent } from './core/header/components/terms-of-us/terms-of-us.component';
import { UnderConstructionComponent } from './modules/under-construction/under-construction.component';
import { ToastsContainer } from './shared/toast/toast-container.component';
import { appInitializer } from './core/helpers/app.initializer';
import { environment } from '../environments/environment';
import { InlineSVGModule } from 'ng-inline-svg';
import { LadderPopupComponent } from './shared/popups/ladder-popup/ladder-popup.component';
import { CountdownModule } from 'ngx-countdown';
import { ToastrModule } from 'ngx-toastr';
import {
  BrowserModule,
  BrowserTransferStateModule,
} from '@angular/platform-browser';
import { FormComponentModule } from './shared/components/form-component/form-component.module';
import { ScrollingModule } from '@angular/cdk/scrolling';
import {
  EsportsLoaderModule,
  EsportsPaginationService,
  EsportsModule,
  EsportsToastService,
  EsportsSnackBarModule,
  EsportsChatService,
} from 'esports';
import { NotfoundComponent } from './modules/not-found/not-found.component';
import { UserService } from './core/service';
@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    HeaderComponent,
    UserPopupComponent,
    ToastsContainer,
    LadderPopupComponent,
    UnderConstructionComponent,
    TermsOfUsComponent,
    NotfoundComponent,
  ],
  imports: [
    AppRoutingModule,
    BrowserAnimationsModule,
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    BrowserTransferStateModule,
    CoreModule,
    InlineSVGModule.forRoot(),
    SharedModule,
    FormComponentModule,
    CountdownModule,
    ScrollingModule,
    EsportsModule.forRoot(environment),
    EsportsSnackBarModule.setPlatform('ESPORTSCORE'),
    EsportsLoaderModule.setColor('#1d252d'),
    ToastrModule.forRoot(),
  ],
  exports: [HeaderComponent, FooterComponent, ToastsContainer],
  providers: [
    EsportsChatService,
    DatePipe,
    EsportsPaginationService,
    EsportsToastService,
    {
      provide: APP_INITIALIZER,
      useFactory: appInitializer,
      multi: true,
      deps: [UserService],
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptor,
      multi: true,
    },
    ...(environment.gtmId ? [{ provide: 'googleTagManagerId', useValue: environment.gtmId }] : [])
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
