import { AfterViewInit, Component, ElementRef, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { Direction } from '@angular/cdk/bidi';
import { Meta, Title } from '@angular/platform-browser';
import {
  EsportsChatService,
  EsportsPaginationService,
  EsportsConstantsService,
  IUser,
  GlobalUtils,
} from 'esports';
import { filter, map, take } from 'rxjs/operators';
import { AppHtmlRoutes } from './app-routing.model';
import { AppRoutesData } from './app-routing.model';
import { environment } from '../environments/environment';
import { UserService } from './core/service';
// declare var gtag;
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})

export class AppComponent implements OnInit, AfterViewInit {
  AppHtmlRoutes = AppHtmlRoutes;
  isAdminPage: boolean = false;
  isRootPage: boolean = false;
  isUserLoggedIn: boolean = false;
  direction: Direction = 'ltr';
  currentUserId: String = '';
  currentUser: IUser;
  public scroll: ElementRef;
  isBrowser = GlobalUtils.isBrowser();
  constructor(
    private activatedRoute: ActivatedRoute,
    private meta: Meta,
    private paginationService: EsportsPaginationService,
    private titleService: Title,
    private userService: UserService,
    public router: Router,
    private chatService: EsportsChatService
  ) {
    if (GlobalUtils.isBrowser()) {
      this.checkAdmin();
      this.globalRouterEvents();
      window.addEventListener('storage', (event) => {
        if (event.newValue && event.key === environment.currentToken) {
          window.location.reload();
        }
        if (event.oldValue && event.key === environment.currentToken) {
          window.location.reload();
        }
      });
      // const naveEndEvents = router.events.pipe(
      //   filter((event) => event instanceof NavigationEnd)
      // );
      // naveEndEvents.subscribe((event: NavigationEnd) => {
      //   gtag('config', 'G-FB4CHLFXC0', {
      //     page_path: event.urlAfterRedirects,
      //   });
      // });
    }
  }

  ngOnInit(): void {
    if (GlobalUtils.isBrowser()) {
      this.paginationService.pageChanged.subscribe(
        (res) => {
          if (res) {
            this.scroll.nativeElement.scrollTop = 0;
          }
        },
        (err) => {}
      );

      this.userService.currentUser.subscribe((data) => {
        if (data) {
          this.currentUser = data;
          this.isUserLoggedIn = !!this.currentUser;
          this.currentUserId = this.currentUser._id;
          this.chatService.initialiseSocket();
        } else {
          //this.userService.getAuthenticatedUser();
        }
      });
    }
  }

  ngAfterViewInit(): void {
    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
        if (this.scroll?.nativeElement?.scrollTop) {
          this.scroll.nativeElement.scrollTop = 0;
        }
        return;
      }
      if (this.scroll?.nativeElement?.scrollTop) {
        this.scroll.nativeElement.scrollTop = 0;
      }
    });
  }

  onActivate(): void {}

  checkAdmin(): void {
    this.userService.currentUser
      .pipe(
        filter((data) => !!data),
        take(1)
      )
      .subscribe((data) => {
        if (data) {
          if (data.accountType === EsportsConstantsService.AccountType.Admin) {
            this.isAdminPage = true;
            this.router.navigate(["/admin"]);
          }
        } else {
          this.isAdminPage = false;
        }
      });
  }

  private globalRouterEvents(): void {
    this.router.events
      .pipe(
        filter((event) => event instanceof NavigationEnd),
        map(() => this.activatedRoute.root.firstChild.snapshot.data)
      )
      .subscribe((data: AppRoutesData) => {
        this.isRootPage = data && data.isRootPage;
        const title = data && data.title;
        const tags = data && data.tags;
        if (title) {
          this.titleService.setTitle(title);
        }
        if (tags) {
          tags.forEach((tag) => {
            this.meta.updateTag(tag);
          });
        }
      });
  }
}
