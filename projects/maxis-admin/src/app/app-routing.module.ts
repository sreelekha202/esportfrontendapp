import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NotfoundComponent } from './modules/not-found/not-found.component';
import { UnderConstructionComponent } from './modules/under-construction/under-construction.component';
import { AuthGuard } from './shared/guard/auth.guard';

const routes: Routes = [
  {
    path: 'admin',
    canActivate: [AuthGuard],
    loadChildren: () =>
      import('./modules/admin/admin.module').then((m) => m.AdminModule),
    data: { isRootPage: true, title: 'Maxis Admin' },
  },
  {
    path: 'home',
    loadChildren: () =>
      import('./modules/shop/shop.module').then((m) => m.ShopModule),
  },
  // {
  //   path: 'news',
  //   loadChildren: () =>
  //     import('./modules/news/news.module').then((m) => m.NewsModule),
  // },
  // {
  //   path: 'news/create',
  //   data: { isRootPage: true },
  //   canActivate: [AuthGuard],
  //   loadChildren: () =>
  //     import('./modules/news/add-content/add-content.module').then(
  //       (m) => m.AddContentModule
  //     ),
  // },
  // {
  //   path: 'news/edit/:id',
  //   data: { isRootPage: true },
  //   canActivate: [AuthGuard],
  //   loadChildren: () =>
  //     import('./modules/news/edit-content/edit-content.module').then(
  //       (m) => m.EditContentModule
  //     ),
  // },
  // {
  //   path: 'search',
  //   loadChildren: () =>
  //     import('./modules/search/search.module').then((m) => m.SearchModule),
  //   data: { title: 'Maxis Search' },
  // },
  // {
  //   path: 'privacy-policy',
  //   loadChildren: () =>
  //     import('./modules/privacy-policy/privacy-policy.module').then(
  //       (m) => m.PrivacyPolicyModule
  //     ),
  //   data: { Title: 'Maxis Privacy-Policy' },
  // },
  // {
  //   path: 'terms-of-use',
  //   loadChildren: () =>
  //     import('./modules/terms-of-use/terms-of-use.module').then(
  //       (m) => m.TermsOfUseModule
  //     ),
  //   data: { Title: 'Maxis Terms-Of-Use' },
  // },
  // {
  //   path: 'terms-conditions',
  //   loadChildren: () =>
  //     import('./modules/terms-conditions/terms-conditions.module').then(
  //       (m) => m.TermsConditionsModule
  //     ),
  //   data: { Title: 'Maxis Terms & Conditions' },
  // },
  // {
  //   path: 'faq',
  //   loadChildren: () =>
  //     import('./modules/faq/faq.module').then((m) => m.FaqModule),
  //   data: { Title: 'Maxis FAQ' },
  // },
  // {
  //   path: 'contact-us',
  //   loadChildren: () =>
  //     import('./modules/contact-us/contact-us.module').then(
  //       (m) => m.ContactUsModule
  //     ),
  //   data: { Title: 'Maxis Contact Us' },
  // },
  // {
  //   path: 'profile',
  //   canActivate: [AuthGuard],
  //   loadChildren: () =>
  //     import('./modules/profile/profile.module').then((m) => m.ProfileModule),
  //   data: { title: 'Maxis Profile' },
  // },
  // {
  //   path: 'wallet',
  //   canActivate: [AuthGuard],
  //   loadChildren: () =>
  //     import('./modules/wallet/wallet.module').then((m) => m.WalletModule),
  //   data: { title: 'Maxis Wallet' },
  // },
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: '404', component: NotfoundComponent },
  { path: 'underconstruction', component: UnderConstructionComponent },
  { path: '**', redirectTo: '/home', pathMatch: 'full' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      scrollPositionRestoration: 'enabled',
      initialNavigation: 'enabled',
    }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule { }
