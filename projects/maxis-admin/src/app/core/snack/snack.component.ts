import { Component, Inject, OnInit, TemplateRef } from "@angular/core";
import {
  MAT_SNACK_BAR_DATA,
  MatSnackBarRef,
} from "@angular/material/snack-bar";

export interface SnackComponentData {
  text: string;
  type: SnackComponentType;
}

export enum SnackComponentType {
  success = 'success-message',
  info = 'info-message',
  error = 'error-message',
  promo = 'promo-message',
}

@Component({
  selector: "app-snack",
  templateUrl: "./snack.component.html",
  styleUrls: ["./snack.component.scss"],
})
export class SnackComponent implements OnInit {
  SnackComponentType = SnackComponentType;

  constructor(
    @Inject(MAT_SNACK_BAR_DATA) public data: SnackComponentData,
    private matSnackBarRef: MatSnackBarRef<SnackComponent>
  ) {}

  ngOnInit(): void {
    setTimeout(() => {
      this.matSnackBarRef.dismiss();
    }, 3000);
  }

  getSnackType() {
    switch (this.data.type) {
      case SnackComponentType.success: {
        return "success-message";
      }
      case SnackComponentType.info: {
        return "info-message";
      }
      case SnackComponentType.error: {
        return "error-message";
      }
    }
  }

  onClose(): void {
    this.matSnackBarRef.dismiss();
  }

  isTemplate(template) {
    return template instanceof TemplateRef;
  }
}
