import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";

import { SubscribableOrPromise } from "rxjs";
import { environment } from "../../../environments/environment";

@Injectable({
  providedIn: "root",
})
export class PlatformFeeService {
  constructor(private http: HttpClient) {}

  getPlatformFee(): SubscribableOrPromise<any> {
    return this.http
      .get(`${environment.apiEndPoint}tournament/platform_fee`)
      .toPromise();
  }

  updatePlatformFee(payload): SubscribableOrPromise<any> {
    return this.http
      .patch(`${environment.apiEndPoint}tournament/platform_fee`, payload)
      .toPromise();
  }
}
