import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Subscription } from 'rxjs/Subscription';
import { Observable, Subject } from 'rxjs';
import { BehaviorSubject } from 'rxjs';
import { GlobalUtils } from 'esports';

@Injectable({
  providedIn: 'root',
})
export class WalletService {
  constructor(private httpClient: HttpClient) {}

  BaseURL = environment.apiEndPoint;
  private recordsLoaded$ = new Subject<any>();
  public recordsLoadedSubject = new BehaviorSubject('');
  public recordsLoaded = this.recordsLoadedSubject.asObservable();

  public getPurchaseTransactions(pagenum, pagesize) {
    return this.httpClient.get(
      environment.apiEndPoint +
        'user/eshop/get-purchase-transactions?page=' +
        pagenum +
        '&pageSize=' +
        pagesize
    );
  }

  public getWalletdetails() {
    return this.httpClient.get(
      environment.apiEndPoint + 'user/epm-user-wallet'
    );
  }
  public loadWalletDetails() {
    this.getWalletdetails().subscribe((container: any) => {
      this.recordsLoaded$.next(container);
      this.recordsLoadedSubject.next(container);
    });
  }

  public submitnewuserwallet(data) {
    return this.httpClient.post(
      environment.apiEndPoint + 'user/epm-user-wallet',
      data
    );
  }

  public getWallettopupamount() {
    return this.httpClient.get(
      environment.apiEndPoint + 'user/wallet-topup-amount'
    );
  }

  public getWallettopupmethods() {
    return this.httpClient.get(
      environment.apiEndPoint + 'user/epm-payment-method'
    );
  }

  public gettransactions() {
    return this.httpClient.get(
      environment.apiEndPoint + 'user/all-transactions'
    );
  }

  public gettransactionswithpagination(pagenum, pagesize) {
    return this.httpClient.get(
      environment.apiEndPoint +
        'user/eshop/get-reload-transactions?page=' +
        pagenum +
        '&pageSize=' +
        pagesize
    );
  }

  public redirectToPayment(data, sst: boolean = false): Observable<any> {
    let redirectURL = '';
    if (GlobalUtils.isBrowser())
      redirectURL = !sst ? `&shopUrl=${window.location.href}` : '';
    return this.httpClient.post(
      environment.apiEndPoint +
        `auth/visit-gen-mstoken?sst=${sst}${redirectURL}`,
      data
    );
  }

  public getlatesttransaction() {
    return this.httpClient.get(
      environment.apiEndPoint + 'user/latest-transactions'
    );
  }

  public redirecttopaymentforDBC(data, sst: boolean = false) {
    return this.httpClient.post(
      environment.apiEndPoint + `user/dcb-charge?sst=${sst}`,
      data
    );
  }

  public subscribeToLoadedRecords(callback: (b: any) => void): Subscription {
    return this.recordsLoaded$.subscribe(callback);
  }
  public updateSubscriberProfile(): Observable<any> {
    return this.httpClient.get(
      environment.apiEndPoint + 'auth/update-subscriber-profile'
    );
  }

  // wallet report in admin

  getPaymentsMethodList(): Observable<any> {
    return this.httpClient.get(`${environment.apiEndPoint}admin/payments`);
  }
  getUserManagementWalletReport(param): Observable<any> {
    return this.httpClient.get(
      `${environment.apiEndPoint}admin/user-management-wallet-report`,
      {
        params: param,
      }
    );
  }
  // account-management-report
  getAccountManagementReport(param): Observable<any> {
    return this.httpClient.get(
      `${environment.apiEndPoint}admin/account-management-report`,
      {
        params: param,
      }
    );
  }
}
