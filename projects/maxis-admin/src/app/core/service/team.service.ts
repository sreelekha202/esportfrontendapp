import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class TeamService {
  constructor(private http: HttpClient) {}

  deleteTeam(data) {
    return this.http
      .patch(`${environment.apiEndPoint}user/update_team_admin`, data)
      .toPromise();
  }
}
