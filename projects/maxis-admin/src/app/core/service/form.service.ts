import { Injectable } from "@angular/core";
import {
  AbstractControl,
  FormArray,
  FormControl,
  FormGroup,
  ValidationErrors,
  ValidatorFn,
} from "@angular/forms";

@Injectable({
  providedIn: "root",
})
export class FormService {
  constructor() {}

  groupFieldValidator(): ValidatorFn {
    return (group: FormGroup): ValidationErrors => {
      const value = group.value;
      const isValid = Object.values(value).some((el: any) => el.trim());
      return isValid ? null : { required: true };
    };
  }

  // max length
  arrayMaxLength(max: number): ValidatorFn | any {
    return (control: AbstractControl[]) => {
      if (!(control instanceof FormArray)) return;
      return control.length > max ? { maxLength: true } : null;
    };
  }

  // min length
  minLength(min: number): ValidatorFn | any {
    return (control: AbstractControl[]) => {
      if (!(control instanceof FormArray)) return;
      return control.length < min ? { minLength: true } : null;
    };
  }

  emptyCheck = (control: FormControl) => {
    return control.value && control.value.trim() ? null : { required: true };
  };
}
