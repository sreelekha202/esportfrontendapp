import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { IUser, GlobalUtils } from 'esports';
import { catchError, map } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { Router } from '@angular/router';

const API = environment.apiEndPoint + 'user/';
@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor(private http: HttpClient, private router: Router) {
    this.currentUserSubject = new BehaviorSubject(null);
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public congratulationPopupSubject = new BehaviorSubject(false);
  public congratulationPopup = this.congratulationPopupSubject.asObservable();

  // maxis user details
  public currentUserSubject: BehaviorSubject<IUser>;
  public currentUser: Observable<IUser>;

  refreshCurrentUser(api, tokenName) {
    this.getProfile(api, tokenName).subscribe((res) => {
      this.currentUserSubject.next(res.data);
      return this.currentUserSubject.value;
    });
  }

  getProfile(api, tokenName): Observable<any> {
    let currToken;
    if (GlobalUtils.isBrowser()) {
      currToken = localStorage.getItem(tokenName);
    }
    return this.http.get(api + 'user/maxisprofile').pipe(
      map((value) => {
        return value;
      }),
      catchError(this.handleError)
    );
  }
  handleError(error: HttpErrorResponse, tokenName) {
    if (GlobalUtils.isBrowser() && error?.error?.message == 'Unauthorized') {
      localStorage.removeItem(tokenName);
      window.location.reload();
    }
    let msg = '';
    if (error?.error instanceof ErrorEvent) {
      // client-side error
      msg = error?.error?.message;
    } else {
      // server-side error
      msg = error?.error?.message;
    }
    return throwError(msg);
  }

  getRecentlyUpdatedUsers(params): Observable<any> {
    const encodedUrl = encodeURIComponent(params['query']);
    const encodedPagination = encodeURIComponent(params['pagination']);
    return this.http.get(
      API + `usersPaginated?query=${encodedUrl}&pagination=${encodedPagination}`
    );
  }
  getAllCountries(): Observable<any> {
    return this.http.get('./assets/json/cities.json');
  }
  getStates(): Observable<any> {
    return this.http.get<any>('./assets/json/states.json');
  }
  getCity(): Observable<any> {
    return this.http.get<any>('./assets/json/cities.json');
  }
  getAvatars(): Observable<any> {
    return this.http.get<any>(`${environment.apiEndPoint}admin/profile/avatar`);
  }

  updateUserPopup(data): Observable<any> {
    return this.http.post(`${API}maxis_update_user`, data);
  }
  private refreshTokenTimeout;
  public startRefreshTokenTimer(api, tokeName) {
    let jwtToken;
    if (GlobalUtils.isBrowser()) {
      // parse json object from base64 encoded jwt token
      jwtToken = JSON.parse(atob(localStorage.getItem(tokeName).split('.')[1]));

      // set a timeout to refresh the token a minute before it expires
      const expires = new Date(jwtToken.exp * 1000);
      const timeout = expires.getTime() - Date.now() - 60 * 1000;
      this.refreshTokenTimeout = setTimeout(
        () =>
          this.refreshToken(api, tokeName).subscribe(
            (data) => {},
            (error) => {
              this.logout(api, tokeName);
            }
          ),
        timeout
      );
    }
  }
  refreshToken(api, tokenName) {
    if (GlobalUtils.isBrowser()) {
      const data = {
        refreshToken: localStorage.getItem('refreshToken'),
      };
      return this.http.post<any>(api + 'auth/web_refresh_token', data, {}).pipe(
        map((user) => {
          if (GlobalUtils.isBrowser()) {
            localStorage.setItem(tokenName, user.data);
          }

          this.startRefreshTokenTimer(api, tokenName);
          return user;
        })
      );
    }
  }
  // Return the current User details
  public getAuthenticatedUser(api, tokenName): IUser {
    let currToken;
    if (GlobalUtils.isBrowser()) {
      currToken = localStorage.getItem(tokenName);
    }

    if (this.currentUserSubject.value && currToken) {
      // Check if current user details and token are available
      this.getProfile(api, tokenName).subscribe(
        (res) => {
          this.currentUserSubject.next(res.data);
          return this.currentUserSubject.value;
        },
        (error) => {
          this.logout(api, tokenName);
          this.router.navigate(['/home']);
          //this.toastService.showError(error);
        }
      );
      return this.currentUserSubject.value;
    } else if (currToken && !this.currentUserSubject.value) {
      // if token is available but user details is not available in observables
      this.getProfile(api, tokenName).subscribe(
        (res) => {
          this.currentUserSubject.next(res.data);
          return this.currentUserSubject.value;
        },
        (error) => {
          this.logout(api, tokenName);
          this.router.navigate(['/home']);
          //this.toastService.showError(error);
        }
      );
      return this.currentUserSubject.value;
    } else {
      // return null if token and user details not available
      return null;
    }
  }
  removeUser(api, id): Observable<any> {
    return this.http.delete(api + `user/remove/${id}`);
  }
  getTopSpendingUserList(api): Observable<any> {
    return this.http.get(api + `user/users-account`);
  }
  updateUser(api, formValues, id): Observable<any> {
    const data = formValues;
    return this.http.patch(api + `user/admin_user_update/${id}`, data);
  }
  getAdminUsers(api, query): Observable<any> {
    return this.http.get(api + `user/users${query}`);
  }
  getRewardTransactionByUserId(api, params): Observable<any> {
    let encodedUrl = encodeURIComponent(params['query']);
    return this.http.get(api + `user/admin_reward_tran?query=${encodedUrl}`);
  }
  getUserDetails(api, id): Observable<any> {
    return this.http.get(api + `user/users/${id}`);
  }
  getUserPrefernces(api, id): Observable<any> {
    return this.http.get(api + `user/admin_user_preference/${id}`);
  }
  searchUsers(searchKeyword, specificProjection = undefined) {
    const projection =
      specificProjection && specificProjection.state == true
        ? specificProjection.read
        : 'fullName,username,phoneNumber,email,profilePicture,country,state';
    return this.http
      .get(
        `${environment?.apiEndPoint}user/search_users?text=${searchKeyword}&fields=${projection}`
      )
      .pipe(
        map((value: any) => {
          return value.data;
        }),
        catchError((err) => err)
      );
  }
  teamsDetail_Admin(api, params): Observable<any> {
    const encodedPagination = encodeURIComponent(params['pagination']);
    return this.http.get(
      api + `user/teamsDetail_Admin?query=${encodedPagination}`
    );
  }
  update_team_admin(api, team): Observable<any> {
    return this.http.patch(api + 'user/update_team_admin', team);
  }
  getTeamLogs(api, id): Observable<any> {
    return this.http.get(api + `user/get_team_activity_log/${id}`);
  }
  public getUserAccountDetails(api, userId): Observable<any> {
    return this.http.get(api + `user/accountDetails?userId=${userId}`);
  }
  searchUsersAdminPanel(searchKeyword, specificProjection = undefined) {
    const query = JSON.stringify({
      text: searchKeyword,
    });
    const projection =
      specificProjection && specificProjection.state == true
        ? specificProjection.read
        : 'fullName,username,phoneNumber,email,profilePicture,country,state';
    const encodedUrl = encodeURIComponent(query);
    return this.http
      .get(
        `${environment?.apiEndPoint}user/search_user_admin_panel?query=${encodedUrl}&select=${projection}`
      )
      .pipe(
        map((value: any) => {
          return value.data;
        }),
        catchError((err) => err)
      );
  }
  logout(api, tokenName) {
    if (GlobalUtils.isBrowser()) {
      localStorage.removeItem(tokenName);
      localStorage.removeItem('refreshToken');
      window.location.replace(`${environment.maxisLogout}`);
    }
  }
}
