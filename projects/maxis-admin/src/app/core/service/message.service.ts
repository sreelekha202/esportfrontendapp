import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";

import { BehaviorSubject, Observable } from "rxjs";
import { environment } from "../../../environments/environment";

@Injectable({
  providedIn: "root",
})
export class MessageService {
  constructor(private http: HttpClient) { }

  getInboxMessages(): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set("Content-Type", "application/json");
    return this.http.get(environment.apiEndPoint + `messages`, {
      headers: httpHeaders,
      params: new HttpParams().set("requesttype", "message-list"),
    });
  }

  sendMessage(messageData): Observable<any> {
    const data = messageData; //  sample data {"toUser":"5ef2bb7d3c77c800077f1fbf","subject":"Subject 1","message": "Message 1",(Optional)"parentId":"5f0d4f94c07998afe6d6ccb3"}
    data.requesttype = "save-message";
    const httpHeaders = new HttpHeaders();
    httpHeaders.set("Content-Type", "application/json");
    return this.http.post(environment.apiEndPoint + `messages`, data, {
      headers: httpHeaders,
    });
  }

  // Profile Left Side massage Count
  public messageCountSubject = new BehaviorSubject(null);
  public messageCount = this.messageCountSubject.asObservable()

  getMessage(messageId: string): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set("Content-Type", "application/json");
    return this.http.get(environment.apiEndPoint + `messages`, {
      headers: httpHeaders,
      params: new HttpParams()
        .set("requesttype", "get-message")
        .set("id", messageId),
    });
  }

  seenMessage(messageId: string, messageData): Observable<any> {
    const data = messageData;
    data.requesttype = "message-seen";
    data.id = messageId;
    const httpHeaders = new HttpHeaders();
    httpHeaders.set("Content-Type", "application/json");
    return this.http.post(environment.apiEndPoint + `messages`, data, {
      headers: httpHeaders,
    });
  }

  deleteMultipleMessage(messageIds: Array<string>): Observable<any> {
    const data = { requesttype: "delete-multiple", messageIds: messageIds };
    const httpHeaders = new HttpHeaders();
    httpHeaders.set("Content-Type", "application/json");
    return this.http.post(environment.apiEndPoint + `messages`, data, {
      headers: httpHeaders,
    });
  }
  // deleteMessage(messageId: Array<string>):Observable<void> {
  //   const i = this.getInboxMessages.findIndex(messages => messages.messageId == messageId);
  //   if (i !== -1) {
  //     this.getInboxMessages.splice(i, 1);
  //   }
  // }
}
