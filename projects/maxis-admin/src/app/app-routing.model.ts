import { MetaDefinition } from '@angular/platform-browser';

export interface AppRoutesData {
  isRootPage?: boolean;
  tags?: MetaDefinition[];
  title?: string;
}

export enum AppHtmlRoutes {
  home = '/home',
  play = '/play',
  shop = '/shop',
  videos = '/videos',
  userPageType = '/user',
  search = '/search',
  searchArticle = '/search/article',
  searchShop = '/search/shop',
  searchTournament = '/search/tournament',
  searchVideo = '/search/video',
  tournaments='/tournaments',
  tournament = '/tournament',
  tournamentManage = '/tournament/manage',
  tournamentedit = '/tournament/edit/',
  tournamentView = '/tournament',
  manageTeam = '/manage-team/',
  article = '/news',
  videosView = '/videos/view',
  login = '/login',
  faq = '/faq',
  post = '/content/more/all-post',
  news = '/news/details/',
  newsdetail = '/details',
  viewteam = '/view-team',
  manageteam = '/manage-team/team-members',
  manageEdit = '/manage-team/edit-team',
  manageTournament = '/manage-tournament',
  underconstruction='/underconstruction',
  contactUs='/contact-us',
  termsConditions='/terms-conditions',
  privacyPolicy='/privacy-policy',
  termsOfUse='/terms-of-use',
  admin = '/admin',
  wallet = '/wallet'
}

export enum AppHtmlProfileRoutes {
  accountSetting = '/profile/account-settings',
  bookmarks = '/profile/bookmarks',
  connections = '/404',
  content = '/profile/content',
  dashboard = '/profile/dashboard',
  inbox = '/profile/inbox',
  matches = '/profile/matches',
  myStats = '/profile/my-stats',
  myTeams = '/profile/my-teams',
  myTeamsView = '/profile/my-teams/',
  tournamentsCreated = '/profile/tournaments-created',
  tournamentsJoined = '/profile/tournaments-joined',
  transactions = '/profile/transactions',
  setting = '',
  newsCreate = '/news/create',
  newsEdit = '/news/edit/',
  createTeam = '/create-team'
}

export enum AppHtmlAdminRoutes {
  ShopReports = '/admin/shop-reports',
  accessManagement = '/admin/access-management',
  contentManagement = '/admin/content-management',
  dashboard = '/admin/dashboard',
  editTeam = '/edit-team',
  esportsManagement = '/admin/esports-management',
  siteConfiguration = '/admin/site-configuration',
  teamManagement = '/admin/team-management',
  teamManagementEdit = '/admin/team-management/edit',
  userManagement = '/admin/user-management',
  userManagementView = '/admin/user-management/view',
  userNotifications = '/admin/user-notifications',
  seasonManagement = '/admin/season-management',
  seasonManagementCreate = '/admin/season-management/create',
  WalletReports = '/admin/wallet-reports',
  UserManagement = '/admin/users-management',
  AccountManagement = '/admin/account-management'
}
