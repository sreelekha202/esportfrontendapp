import { IAdvertisementManagement } from "./advertisement";
import { IBracket } from "./bracket";
import { IMatch, IMatchSet } from "./match";
import { IMessage } from "./message";
import { IParticipant } from "./participant";
import { IReward } from "./reward";
import { ITournamentDetails } from "./tournaments";

export {
  IBracket,
  IMatch,
  IMatchSet,
  IMessage,
  IParticipant,
  IReward,
  ITournamentDetails,
};
