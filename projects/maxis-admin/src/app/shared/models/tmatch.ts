interface TMatch {
  LastMessage?: String;
  LastMessageDateAndTime?: String;
  matchId?: String;
  participants?: Array<String>;
  tournamentID?: String;
  tournamentLogo?: String;
  tournamentName?: String;
}

export { TMatch };
