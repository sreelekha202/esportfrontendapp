export interface IParticipant {
  _id?: string;
  checkedIn: boolean;
  createdOn?: Date;
  email: string;
  inGamerUserId?: string;
  logo?: string;
  matchLoss?: number;
  matchTie?: number;
  matchWin?: number;
  name?: string;
  participantStatus?: string;
  participantType?: string;
  phonenumber?: string;
  seed?: number;
  status?: number;
  teamMembers?: [];
  teamName?: string;
  tournamentId?: string;
  tournamentUsername?: string;
  updatedOn?: Date;
  userId?: string;
}
