import { Pipe } from '@angular/core';

@Pipe({
  name: 'phone',
})
export class PhonePipe {
  transform(n) {
    let num: any = n.toString();
    if (num && num.length > 8) {
      return `+${num.slice(0, 2)} ${num.slice(2, 4)}-${num.slice(4,8)} ${num.slice(8, num.length)}`;
    } else {
      return (' ');
    }
  }
}
