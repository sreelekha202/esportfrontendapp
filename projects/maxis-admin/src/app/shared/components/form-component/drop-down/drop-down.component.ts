import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { EsportsLanguageService } from 'esports';

@Component({
  selector: 'app-drop-down',
  templateUrl: './drop-down.component.html',
  styleUrls: ['./drop-down.component.scss'],
})
export class DropDownComponent implements OnInit {
  @Input() customFormControlName: string;
  @Input() customFormGroup: FormGroup;
  @Input() dropdownList;
  @Input() filterType;
  @Input() readOnly: boolean = false;
  @Input() title: string;
  @Input() type: string;
  @Output() valueEmit = new EventEmitter<number>();
  @Input() enableHiddenLabel: boolean;
  @Input() showTopLabel = false;

  isClicked: boolean = false;
  currentLang: string = 'en';
  countryList;

  constructor(private languageService: EsportsLanguageService) {}

  ngOnInit(): void {
    this.languageService.language.subscribe(
      (lang) => (this.currentLang = lang)
    );
    this.countryList = this.dropdownList;
  }

  setValue(val) {
    this.customFormGroup.controls[this.customFormControlName].setValue(val);
    this.valueEmit.emit(val);
  }

  onKeypressEvent(event: any) {
    if (!this.isClicked) {
      this.isClicked = true;
      this.countryList = this.dropdownList;
    }

    if (event.target.value.trim().length == 0) {
      this.dropdownList = this.countryList;
    }

    this.dropdownList = this.countryList.filter(function (elem) {
      return elem.name.includes(event.target.value);
    });
  }

  setValueInArray(val) {
    const array = this.customFormGroup.value[this.customFormControlName];
    const index = array.indexOf(val);

    if (index < 0) {
      array.push(val);
      this.customFormGroup.controls[this.customFormControlName].setValue(array);
    }

    this.valueEmit.emit(val);
  }
}
