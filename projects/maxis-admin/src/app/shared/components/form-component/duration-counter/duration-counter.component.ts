import { Component, Input, OnInit } from "@angular/core";
import { FormGroup } from "@angular/forms";

@Component({
  selector: "app-duration-counter",
  templateUrl: "./duration-counter.component.html",
  styleUrls: ["./duration-counter.component.scss"],
})
export class DurationCounterComponent implements OnInit {
  @Input() title;
  @Input() customFormControlName: string;
  @Input() customFormGroup: FormGroup;

  constructor() {}

  ngOnInit(): void {}

  increment(field) {
    const childFormGroup: any = this.customFormGroup.get(
      this.customFormControlName
    );
    childFormGroup.markAsTouched({ onlySelf: true });
    const control = childFormGroup.get(field);
    let val = control.value;
    val = this.fieldLimit(val + 1, field);
    control.setValue(Number(val));
  }

  decrement(field) {
    const childFormGroup: any = this.customFormGroup.get(
      this.customFormControlName
    );
    childFormGroup.markAsTouched({ onlySelf: true });
    const control = childFormGroup.get(field);
    let val = control.value;
    if (val > 0) {
      val = this.fieldLimit(val - 1, field);
    }
    control.setValue(Number(val));
  }

  onChange(value, field) {
    const childFormGroup: any = this.customFormGroup.get(
      this.customFormControlName
    );
    childFormGroup.markAsTouched({ onlySelf: true });
    const control = childFormGroup.get(field);
    if (value > 0) {
      value = this.fieldLimit(value, field);
    }
    control.setValue(Number(value));
  }

  fieldLimit = (value, field) => {
    switch (field) {
      case "hours":
        return value > 23 ? 23 : value;
      case "mins":
        return value > 59 ? 59 : value;
      default:
        return value;
    }
  };
}
