import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';



@Component({
  selector: 'app-phone-no',
  templateUrl: './phone-no.component.html',
  styleUrls: ['./phone-no.component.scss'],
})
export class PhoneNoComponent implements OnInit {
  @Input() customFormControlName: string;
  @Input() customFormGroup: FormGroup;
  @Input() readOnly: boolean;
  @Input() separateDialCode = false;
  @Input() title: string;
  @Output() valueEmit = new EventEmitter<any>();


  constructor() {}

  ngOnInit(): void {}
}
