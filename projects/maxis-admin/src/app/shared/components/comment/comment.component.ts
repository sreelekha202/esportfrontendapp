import { Component, OnInit, Input } from '@angular/core';
import { IUser } from 'esports';
import { UserService } from '../../../core/service';

@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.scss'],
})
export class CommentComponent implements OnInit {
  @Input() placeholder: string = '';
  @Input() isReplying: boolean = false;

  currentUser: IUser;

  constructor(private userService: UserService) {}

  ngOnInit(): void {
    this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.currentUser = data;
      }
    });
  }
}
