import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { ProfileService } from './../../../core/service/profile.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { environment } from '../../../../environments/environment';
import { EsportsArticleService, IUser } from 'esports';
const API = environment.apiEndPoint;

@Component({
  selector: 'app-search-filter',
  templateUrl: './search-filter.component.html',
  styleUrls: ['./search-filter.component.scss'],
})
export class SearchFilterComponent implements OnInit {
  user: IUser;
  paginationData = {
    page: 1,
    limit: 20,
    sort: '-views',
  };
  addForm: FormGroup;
  stateList: any = [];
  categoryList: any = [];
  gameList: any = [];
  platformList: any = [];
  @Input() formData = [];
  @Input() placeholder: string = '';
  text = '';
  mobFiltersMenuOpened: boolean = false;
  showLoader: false;
  articles: any = [];
  dataLoaded = false;
  platforms = [
    { value: 'all', viewValue: 'Xbox, PC, Mobile' },
    { value: 'xbox', viewValue: 'Xbox' },
    { value: 'pc', viewValue: 'PC' },
    { value: 'mobile', viewValue: 'Mobile' },
  ];

  sortby = [
    { value: '0', viewValue: 'Upcoming tournaments' },
    { value: '1', viewValue: 'Ongoing tournaments' },
    { value: '2', viewValue: 'Completed tournaments' },
    { value: '3', viewValue: 'All tournaments' },
  ];

  regions = [
    { value: 'all', viewValue: 'All' },
    { value: 'test1', viewValue: 'Test1' },
    { value: 'test2', viewValue: 'Test2' },
  ];

  selected_platform = this.platforms[0].value;
  selected_sortby = this.sortby[3].value;
  selected_region = this.regions[0].value;

  individualTournaments = [];
  prizeMoneyTournaments = [];
  teamTournaments = [];
  tournamentData: any;
  tournamentDetails: any;

  constructor(
    private fb: FormBuilder,
    private profileService: ProfileService,
    private articleApiService: EsportsArticleService
  ) {}

  ngOnInit(): void {
    this.getAllGame();
    this.getAllTournaments();
    this.getAllCategories();
  }
  getAllGame() {
    this.profileService.getAllGame().subscribe((res) => {
      if (res) this.gameList = res.data;
    });
  }

  // onChangeGame() {
  //   this.gameList.map((res) => {
  //     if (this.addForm.value.gameDetails == res._id) {
  //       this.platformList = res.platform;
  //     }
  //   })
  // }

  onSearch = (event: any) => {
    this.text = String(event.target.value);
    this.getAllTournaments();
  };
  getAllTournaments() {
    const pagination = JSON.stringify({
      page: this.paginationData.page,
      limit: 20,
      sort: { createdDate: -1 },
    });
    const query = JSON.stringify({
      articleStatus: 'publish',
    });

    const perfernce = JSON.stringify({
      prefernce: this.user?.preference?.game
        ? this.user?.preference?.game.map((item) => {
            return { gameDetails: item };
          })
        : '',
    });
    this.individualTournaments = [];
    this.prizeMoneyTournaments = [];
    this.teamTournaments = [];
    // this.showLoader = true;
    const params: any = {};
    this.selected_sortby ? (params.status = this.selected_sortby) : '';
    this.text ? (params.text = this.text) : '';
    this.articleApiService
      .getLatestArticle(API, {
        pagination: pagination,
        query: query,
        preference: perfernce,
      })
      .subscribe(
        (res: any) => {
          this.showLoader = false;
          this.articles = res['data']['docs'];
          this.dataLoaded = true;
          for (let d of res.data.docs) {
            if (d.participantType == 'individual') {
              this.individualTournaments.push(d);
            }
            if (d.isPrize == true) {
              this.prizeMoneyTournaments.push(d);
            }

            if (d.participantType != 'individual') {
              this.teamTournaments.push(d);
            }
          }
        },
        (err: any) => {
          this.showLoader = false;
        }
      );
  }
  getAllCategories() {
    this.profileService.getAllCategories().subscribe((res) => {
      if (res) this.categoryList = res.data;
    });
  }
  onToggleFilterMenu() {
    this.mobFiltersMenuOpened = !this.mobFiltersMenuOpened;
  }
}
