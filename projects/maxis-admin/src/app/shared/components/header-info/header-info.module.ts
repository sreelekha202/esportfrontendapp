import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { HeaderInfoComponent } from "./header-info.component";
import { RouterModule } from "@angular/router";

@NgModule({
  declarations: [HeaderInfoComponent],
  imports: [CommonModule, RouterModule],
  exports: [HeaderInfoComponent],
})
export class HeaderInfoModule {}
