import {
  AfterViewInit,
  Component,
  ElementRef,
  Inject,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import {
  EsportsChatService,
  GlobalUtils,
  EventProperties,
  SuperProperties,
  EsportsChatSidenavService,
  // EsportsGtmService,
  IUser,
} from 'esports';
import { MatSidenav } from '@angular/material/sidenav';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { UserService } from '../../../../../core/service';

@Component({
  selector: 'app-chatsidebar',
  templateUrl: './chatsidebar.component.html',
  styleUrls: ['./chatsidebar.component.scss'],
})
export class ChatsidebarComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild('menuSidenav') public menuSidenav: MatSidenav;
  @ViewChild('chatSidenav') public chatSidenav: MatSidenav;
  @ViewChild('searchinput') searchinput: ElementRef;
  showChat: boolean = false;
  matchdetails: any;
  matchlist: any;
  public spamcomment: any;
  typeofchat: any;
  useridtoblock: any;
  chatWindows = [];
  initialUserList = [];
  userlist: any;
  windowposition: String = 'chat_window chat_window_right_drawer';
  currentUserName: String = '';
  isSpin: boolean = false;
  isSpin2: boolean = false;
  isUserLoggedIn: boolean = false;
  currentUserId: String = '';
  currentUser: IUser;
  showSearchListTitle: boolean = false;
  isShowBlockUser: boolean = false;
  userSubscription: Subscription;
  matchId: any;
  currentsearch: '';
  currenUser: IUser;
  isBrowser = GlobalUtils.isBrowser();
  constructor(
    private chatSidenavService: EsportsChatSidenavService,
    public chatService: EsportsChatService,
    private userService: UserService,
    private modalService: NgbModal // private gtmService: EsportsGtmService
  ) {}

  ngOnInit(): void {
    if (GlobalUtils.isBrowser()) {
      this.userSubscription = this.userService.currentUser.subscribe((data) => {
        if (data) {
          this.currenUser = data;
          this.isUserLoggedIn = !!this.currenUser;
          this.currentUserId = this.currenUser._id;

          this.chatService.mtches.subscribe((matcList) => {
            this.isSpin2 = true;
            this.chatWindows = matcList;
          });

          if (this.chatWindows?.length == 0) {
            this.chatService?.getAllMatches(this.currentUserId, 1, 30);
          }

          this.chatService.chatStatus.subscribe((cstatus) => {
            this.showChat = cstatus;
          });
        }
      });
    }
  }

  ngAfterViewInit(): void {
    this.chatSidenavService.setChatSidenav(this.chatSidenav);
  }

  ngOnDestroy(): void {
    this.chatService.setChatStatus(false);
    this.chatService?.disconnectMatch(
      this.matchdetails?.matchid
        ? this.matchdetails?.matchid
        : this.matchdetails,
      this.typeofchat
    );
    this.userSubscription?.unsubscribe();
  }

  public onMenuClick(): void {
    this.menuSidenav.toggle();
  }

  public searchUsers(event) {
    this.userlist = [];
    this.showSearchListTitle = false;
    if (event.keyCode == 13) {
      if (this.currentsearch != '') {
        this.isSpin = true;
        this.showSearchListTitle = false;
        this.chatService.searchUsers(this.currentsearch).subscribe(
          (res: any) => {
            this.isSpin = false;
            this.showSearchListTitle = true;
            this.userlist = res.data;
          },
          (err) => {}
        );
      }
    }
  }

  public onChatSidenavToggle(): void {
    this.chatSidenavService.toggle();

    if (this.currentUserId) {
      this.chatService?.getAllMatches(this.currentUserId, 1, 30);
    }

    if (this.chatSidenavService.checkOpen()) {
      if (this.chatService.getChatStatus()) {
        this.chatService.setChatStatus(false);
      }
    } else {
      if (this.chatService.getChatStatus()) {
        this.chatService.setChatStatus(false);
      }
    }
  }

  public toggleChat(chatwindow) {
    this.pushGTMTags('Chat_Opened');
    this.typeofchat = chatwindow.typeofchat;

    if (
      chatwindow.typeofchat == 'tournament' ||
      chatwindow.typeofchat == 'game'
    ) {
      this.matchdetails = chatwindow;
    } else {
      if (chatwindow.matchid) {
        this.matchdetails = chatwindow.matchid;
      } else {
        this.matchdetails = chatwindow._id + '-' + this.currentUserId;
      }
      this.showChat = true;
    }

    this.chatService.setWindowPos(this.windowposition);
    this.chatService.setCurrentMatch(this.matchdetails);
    this.chatService.setTypeOfChat(this.typeofchat);
    this.chatService.setChatStatus(true);
    this.chatService.setTextButtonStatus(true);

    if (this.chatService.getChatStatus() == false) {
      this.chatService?.disconnectMatch(
        this.matchdetails?.matchid
          ? this.matchdetails?.matchid
          : this.matchdetails,
        this.typeofchat
      );
    }
    this.chatSidenav.toggle();
  }

  public blockUser(content, userid, matchid) {
    this.pushGTMTags('Chat_Block', true);
    this.useridtoblock = userid;
    this.matchId = matchid;

    this.modalService.open(content, {
      centered: true,
      windowClass: 'custom-modal-content',
    });
  }

  public onConfirmSpam(blocktype = 'spm') {
    let matchid = this.matchId;
    this.pushGTMTags('Chat_Report_Spam', true);

    this.chatService
      ?.getAllUserChatMessages(matchid, 1, 50)
      .subscribe((chats) => {
        const body = {
          userid: this.currentUserId,
          blocktype: blocktype,
          spamuserid: this.useridtoblock,
          comment: this.spamcomment,
          chathistorymessages: JSON.stringify(chats).replace(/"/g, '\\"'),
          matchid: matchid,
        };

        this.chatService?.blockUser(body).subscribe((resp: any) => {
          if (resp.status == 'success') {
            this.chatService?.getAllMatches(this.currentUserId, 1, 30);
            this.useridtoblock = '';
            this.modalService.dismissAll();
            this.spamcomment = '';
          }
        });
      });
  }

  onConfirmBlock() {
    this.onConfirmSpam('blck');
  }

  pushGTMTags(eventName: string, eventProps = false) {
    let superProperties: SuperProperties = {};
    // if (this.currenUser) {
    //   superProperties = this.gtmService.assignLoggedInUsedData(this.currenUser);
    // }

    let eventProperties: EventProperties = {};
    if (eventProps) {
      eventProperties.fromPage = 'Chats';
    }

    // this.gtmService.gtmEventWithSuperProp({
    //   eventName: eventName,
    //   superProps: superProperties,
    //   eventProps: eventProperties,
    // });
  }
}
