import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";

@Component({
  selector: "app-table-counter-cell",
  templateUrl: "./table-counter-cell.component.html",
  styleUrls: ["./table-counter-cell.component.scss"],
})
export class TableCounterCellComponent implements OnInit {
  @Input() max: number = 0;
  @Input() min: number = 0;
  @Input() value: number = 0;
  @Input() notPlaying: boolean = false;
  @Output() counterChange = new EventEmitter<number>();

  constructor() {}

  ngOnInit(): void {}

  increment() {
    this.updateValue("inc");
  }

  decrement() {
    this.updateValue("dec");
  }

  private updateValue(action: "inc" | "dec") {
    const delta = action === "inc" ? 1 : -1;

    this.value += delta;
    this.counterChangeHandler();
  }

  counterChangeHandler() {
    this.value =
      this.value > this.max
        ? this.max || this.value
        : this.value < this.min
        ? this.min
        : this.value || 0;
    this.counterChange.emit(this.value);
  }
}
