import { Component, OnInit } from '@angular/core';
import { IUser } from 'esports';
import { Subscription } from 'rxjs';
import { CommentService, UserService } from '../../../core/service';

@Component({
  selector: 'app-comments-influence',
  templateUrl: './comments-influence.component.html',
  styleUrls: ['./comments-influence.component.scss'],
})
export class CommentsInfluenceComponent implements OnInit {
  currentUser: IUser;
  userSubscription: Subscription;

  comments = [];
  tournamentDetails: any;

  constructor(private userService: UserService, private commentService: CommentService) { }

  ngOnInit(): void {

    this.tournamentDetails = localStorage.getItem("tournamentDetails")
    this.tournamentDetails = JSON.parse(this.tournamentDetails)
    this.comments = [];
    // this.userSubscription = this.userService.currentUser.subscribe((data) => {
    //   if (data) {
    //     this.currentUser = data;
    //   }
    // });

    // this.commentService.getAllComment1("tournament",this.tournamentDetails._id,1).subscribe((data) => {
    //   this.comments = data.data
    // });



    // MOCK COMMENTS DATA
    // this.comments = [
    //     {
    //         "_id":"60ed62733d76400008b412a2",
    //         "isPinned":false,
    //         "comment":"hlo\n",
    //         "modifiedOn":"2021-07-13T09:52:51.376Z",
    //         "totalReplies":1,
    //         "totalLikes":0,
    //         "user":{
    //             "fullName":"Mortal1",
    //             "profilePicture":"https://dynastyesports-img.s3.ap-southeast-1.amazonaws.com/dev/tournament/1619670145324.jpeg"
    //         },
    //         "type":-1,
    //         "isCommentedUser":false,
    //         "isAdminUser":true,
    //         "isDeleteAllow":false,
    //         "isTournamentOrganizer":false
    //     },
    //     {
    //         "_id":"60ec2b71f3a1ee00082ce14c",
    //         "isPinned":false,
    //         "comment":"Hello all",
    //         "modifiedOn":"2021-07-12T11:45:53.167Z",
    //         "totalReplies":1,
    //         "totalLikes":0,
    //         "user":{
    //             "fullName":"Ashit12131313313",
    //             "profilePicture":"https://dynastyesports-img.s3.ap-southeast-1.amazonaws.com/dev/avatar/1617256940735.png"
    //         },
    //         "type":-1,
    //         "isCommentedUser":false,
    //         "isAdminUser":true,
    //         "isDeleteAllow":false,
    //         "isTournamentOrganizer":true
    //     }
    // ]
  }
}
