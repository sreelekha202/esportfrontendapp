import { Component, OnInit, Input } from "@angular/core";
import { FormGroup } from "@angular/forms";
import { EsportsToastService } from "esports";
import { S3UploadService } from "../../../core/service";
@Component({
  selector: "app-upload-image",
  templateUrl: "./upload-image.component.html",
  styleUrls: ["./upload-image.component.scss"],
})
export class UploadImageComponent implements OnInit {
  @Input() customFormControlName: string;
  @Input() customFormGroup: FormGroup;
  @Input() title: string;
  @Input() subtitle: string;
  @Input() dimension;
  @Input() size;
  @Input() type;
  @Input() id;
  @Input() required: boolean = false;

  isProcessing: boolean = false;

  constructor(
    private s3UploadService: S3UploadService,
    private toastService: EsportsToastService
  ) {}

  ngOnInit(): void {}

  eventChange = async (event) => {
    try {
      this.isProcessing = true;

      var files = event.target.files;
      var file = files[0];

      if (file) {
        const upload: any = await this.s3UploadService.toBase64(file);
        // const upload: any = await this.s3UploadService.validateUploadImage(
        //   event,
        //   this.dimension,
        //   this.size,
        //   this.type
        // );

        // this.customFormGroup
        //   .get(this.customFormControlName)
        //   .setValue(upload?.data?.Location);
        this.customFormGroup.get(this.customFormControlName).setValue(upload);
        //this.toastService.showSuccess(upload?.message);
        this.isProcessing = false;
      } else {
        this.customFormGroup.get(this.customFormControlName).setValue("");
        //this.toastService.showSuccess(upload?.message);
        this.isProcessing = false;
      }
    } catch (error) {
      this.isProcessing = false;
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };
}
