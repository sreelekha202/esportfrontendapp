import { Component, OnInit } from '@angular/core';
import { toggleOpacity, VisibilityState } from '../../../animations';

@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.scss'],
  animations: [toggleOpacity],
})
export class CommentsComponent implements OnInit {
  VisibilityState = VisibilityState;
  collapseOrExpand: VisibilityState = VisibilityState.Visible;

  comments = [];

  constructor() {}

  ngOnInit(): void {
    // MOCK COMMENTS DATA
    // this.comments = [
    //   {
    //     _id: '60a16de1b5fd77a0007500652',
    //     isPinned: false,
    //     comment:
    //       'I agree completely with your sentiments. Always such a joy reading your articles Emily! This here is my completely valid opinion about what you have written and is in no way filler text.',
    //     modifiedOn: '2021-05-16T19:09:21.839Z',
    //     totalReplies: 0,
    //     totalLikes: 2,
    //     user: {
    //       fullName: 'Misha Omar',
    //       profilePicture:
    //         'https://i115.fastpic.ru/big/2021/0627/63/0b03433a53012f3e94ba7c809e032863.png',
    //     },
    //     type: 1,
    //     isCommentedUser: false,
    //     isAdminUser: true,
    //     isDeleteAllow: true,
    //     isTournamentOrganizer: false,
    //   },
    //   {
    //     _id: '6063282408d85b9000834617c',
    //     comment:
    //       'Cool article! Not too sure about the second point OP made but I can see where they’re coming from.',
    //     modifiedOn: '2021-03-30T13:31:12.229Z',
    //     totalReplies: 1,
    //     totalLikes: 0,
    //     user: {
    //       fullName: 'Annabelle Thompson ',
    //       profilePicture:
    //         'https://i115.fastpic.ru/big/2021/0627/ce/b7ba21cd05597813ce8010f2e2a648ce.png',
    //     },
    //     type: 0,
    //     isCommentedUser: false,
    //     isAdminUser: false,
    //     isDeleteAllow: true,
    //     isTournamentOrganizer: false,
    //     page: 2,
    //     isLoadingNextReplies: false,
    //     replies: [
    //       {
    //         _id: '6080ff6e52eedc9900085fd8f0',
    //         comment:
    //           'Cool article! Not too sure about the second point OP made but I can see where they’re coming from.',
    //         modifiedOn: '2021-04-22T04:45:34.242Z',
    //         totalReplies: 1,
    //         totalLikes: 3,
    //         user: {
    //           fullName: 'Emily Richard ',
    //           profilePicture:
    //             'https://i115.fastpic.ru/big/2021/0627/79/c29266652b9c5148ac824dfa86ed4379.png',
    //         },
    //         type: 1,
    //         isCommentedUser: false,
    //         isAdminUser: false,
    //         isDeleteAllow: true,
    //         isTournamentOrganizer: false,
    //         replies: [
    //           {
    //             _id: '60a16d6e1bfd77a0007500652',
    //             isPinned: false,
    //             comment: 'We believe our esport should reflect the diverse',
    //             modifiedOn: '2021-05-16T19:09:21.839Z',
    //             totalReplies: 0,
    //             totalLikes: 0,
    //             user: {
    //               fullName: 'Misha Omar',
    //               profilePicture:
    //                 'https://i115.fastpic.ru/big/2021/0627/63/0b03433a53012f3e94ba7c809e032863.png',
    //             },
    //             type: 0,
    //             isCommentedUser: false,
    //             isAdminUser: true,
    //             isDeleteAllow: true,
    //             isTournamentOrganizer: false,
    //           },
    //         ],
    //       },
    //     ],
    //     hasNextReply: false,
    //     showReplies: true,
    //   },
    // ];
  }
}
