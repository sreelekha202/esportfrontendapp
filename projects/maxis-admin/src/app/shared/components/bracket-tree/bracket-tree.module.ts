import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { I18nModule } from 'esports';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatIconModule } from '@angular/material/icon';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';

import { BracketTreeComponent } from './bracket-tree.component';
import { SingleEliminationTreeComponent } from './single-elimination-tree/single-elimination-tree.component';
import { MatchComponent } from './match/match.component';
import { environment } from '../../../../environments/environment';

const components = [
  BracketTreeComponent,
  SingleEliminationTreeComponent,
  MatchComponent,
];

const modules = [
  CommonModule,
  I18nModule.forRoot(environment),
  MatDatepickerModule,
  MatIconModule,
  NgxMaterialTimepickerModule,
];

@NgModule({
  declarations: components,
  imports: modules,
  exports: [BracketTreeComponent],
})
export class BracketTreeModule {}
