import { Component, OnInit, SimpleChanges } from '@angular/core';
import { BracketService } from '../../../core/service';
import { Input, Output, EventEmitter } from '@angular/core';

export interface NgttTournament {
  rounds: NgttRound[];
}
export interface NgttRound {
  type: 'Winnerbracket' | 'Loserbracket' | 'Final';
  matches: any[];
}
@Component({
  selector: 'app-bracket-tree',
  templateUrl: './bracket-tree.component.html',
  styleUrls: ['./bracket-tree.component.scss'],
})
export class BracketTreeComponent implements OnInit {
  @Input() from: any;   // form ='create' means generate demo or Tournament Details get on LocalStorage
  @Input() structure: any  //pass structure to set bracket
  @Output() valueEmit = new EventEmitter<any>();
  singleEliminationTournament: NgttTournament = null;
  dataforbracket1: any = [];
  tournamentDetails: any;

  constructor(private bracketService: BracketService) { }

  ngOnInit(): void {
    if (this.from != 'create') {
      this.tournamentDetails = localStorage.getItem("tournamentDetails")
      this.tournamentDetails = JSON.parse(this.tournamentDetails)
      let datatosend = {
        bracketType: this.tournamentDetails.bracketType,
        maximumParticipants: this.tournamentDetails.maxParticipants,
        noOfSet: this.tournamentDetails.noOfSet
      }
      this.bracketService.generateBracket1(datatosend).subscribe((data: any) => {
       this.setStructure(data);
      })
    }
  }
  setStructure(data: any) {
    this.dataforbracket1 = [];
    let total = data.data.noOfRound;
    let round = data.data.round
    let i = 0;
    let dataforbracket = []
    //this.dataforbracket[0].type = 'Winnerbracket'
    let j = 1;
    let k = 0;
    for (let data of round["1"]) {
      let number = j;
      let players: any = [];
      let dt = {
        name: data.teamA.teamName, score: data.sets[0].teamAScore, number: j, previousMatch: ''
      }
      let dt1 = {
        name: data.teamB.teamName, score: data.sets[0].teamBScore, number: j, previousMatch: ''
      }
      players.push(dt)
      players.push(dt1)
      let data3 = {
        number: j,
        players: players
      }
      dataforbracket.push(data3)
      j = j + 1
    }
    let data4 = {
      type: 'Winnerbracket',
      matches: dataforbracket
    }
    this.dataforbracket1.push(data4)
    let s = 1
    let a = 2
    for (let m = 2; m < total; m++) {
      dataforbracket = []
      for (let data of round[m.toString()]) {
        let number = j;
        let players: any = [];
        let dt = {
          name: data.placeHolderA, score: 0, number: j, previousMatch: s
        }
        s = s + 1
        let dt1 = {
          name: data.placeHolderB, score: 0, number: j, previousMatch: s
        }
        s = s + 1
        players.push(dt)
        players.push(dt1)
        let data3 = {
          number: j,
          players: players
        }
        dataforbracket.push(data3)
        j = j + 1
      }
      let data5 = {
        type: 'Winnerbracket',
        matches: dataforbracket
      }

      this.dataforbracket1.push(data5)
    }
    dataforbracket = []
    for (let data of round[total.toString()]) {
      let number = j;
      let players: any = [];
      let dt = {
        name: data.placeHolderA, score: 0, number: j, previousMatch: s
      }
      s = s + 1
      let dt1 = {
        name: data.placeHolderB, score: 0, number: j, previousMatch: s
      }
      s = s + 1
      players.push(dt)
      players.push(dt1)
      let data3 = {
        number: j,
        players: players
      }
      dataforbracket.push(data3)
      j = j + 1
    }
    let data6 = {
      type: 'Final',
      matches: dataforbracket
    }
    this.dataforbracket1.push(data6)
    this.singleEliminationTournament = {
      rounds: this.dataforbracket1
    };
    this.valueEmit.emit(this.singleEliminationTournament)
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.hasOwnProperty('structure') && changes.structure.currentValue) {
      // this.structure = changes.structure.currentValue
      if (this.structure) {
        this.setStructure(this.structure);
      }
    }
  }
}
