import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { I18nModule } from 'esports';
import { LazyLoadImageModule } from 'ng-lazyload-image';
import { environment } from '../../../../../environments/environment';
import { CardsStatisticComponent } from './cards-statistic.component';

const modules = [
  CommonModule,
  I18nModule.forRoot(environment),
  LazyLoadImageModule,
];

@NgModule({
  declarations: [CardsStatisticComponent],
  imports: modules,
  exports: [CardsStatisticComponent],
})
export class CardsStatisticModule {}
