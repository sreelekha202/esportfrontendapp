import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { I18nModule } from 'esports';
import { LazyLoadImageModule } from 'ng-lazyload-image';
import { environment } from '../../../../../environments/environment';
import { PrizesStatisticComponent } from './prizes-statistic.component';

const modules = [
  CommonModule,
  I18nModule.forRoot(environment),
  LazyLoadImageModule,
];

@NgModule({
  declarations: [PrizesStatisticComponent],
  imports: modules,
  exports: [PrizesStatisticComponent],
})
export class PrizesStatisticModule {}
