import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-game-statistic',
  templateUrl: './game-statistic.component.html',
  styleUrls: ['./game-statistic.component.scss'],
})
export class GameStatisticComponent implements OnInit {
  @Input() data;
  matches = [];
  isStatistic: boolean = true;
  chart = {
    options: {
      responsive: true,
      legend: {
        display: false,
      },
    },
    labels: ['0', '1', '5', '10', '15', '25', '30'],
    type: 'bar',
    data: [{ data: [0, 100, 200, 300, 400] }],
    colors: [{ backgroundColor: '#736cff' }],
  };

  constructor() {}

  ngOnInit(): void {
    // MOCK DATA
    this.matches = [
      {
        image:
          'https://i115.fastpic.ru/big/2021/0719/28/d72b5749d496c8bf92f5ded0d21b0228.jpg',
        isWin: true,
        title: 'Naga siren',
        level: 25,
        kda: '8/4/11',
        net: '22,107',
        lhdh: '352 / 4',
        xpm: 546,
        dmg: '10,107',
      },
      {
        image:
          'https://i115.fastpic.ru/big/2021/0719/53/afffd898698f3804ee4f576ba77c7e53.jpg',
        isWin: false,
        title: 'Dawnbreaker',
        level: 25,
        kda: '8/4/11',
        net: '22,107',
        lhdh: '352 / 4',
        xpm: 546,
        dmg: '10,107',
      },
      {
        image:
          'https://i115.fastpic.ru/big/2021/0719/1f/af4d16a8f741ef813233b6ea9a07781f.jpg',
        isWin: true,
        title: 'Enchantress',
        level: 25,
        kda: '8/4/11',
        net: '22,107',
        lhdh: '352 / 4',
        xpm: 546,
        dmg: '10,107',
      },
      {
        image:
          'https://i115.fastpic.ru/big/2021/0719/0e/d94c3fb9dc80811d173c05e98c523a0e.jpg',
        isWin: true,
        title: 'Hoodwink',
        level: 25,
        kda: '8/4/11',
        net: '22,107',
        lhdh: '352 / 4',
        xpm: 546,
        dmg: '10,107',
      },
    ];
  }

  getGameIcon(name: string): string {
    const iconPath = 'assets/icons/games';
    const nameWithoutSpace = name.replace(/\s+/g, '').toLowerCase();

    switch (nameWithoutSpace) {
      case 'thesims4':
        return `assets/icons/games/sims4.svg`;
      case 'dota2':
        return `assets/icons/games/dota2.svg`;
      case 'valorant':
        return `${iconPath}/valorant.svg`;
      case 'fortnine':
        return `${iconPath}/fortnine.svg`;
      case 'minecraft':
        return `${iconPath}/minecraft.svg`;
      case 'rust':
        return `${iconPath}/rust.svg`;
      case 'cod':
        return `${iconPath}/cod.svg`;
      case 'overwatch':
        return `${iconPath}/overwatch.svg`;
    }
  }
}
