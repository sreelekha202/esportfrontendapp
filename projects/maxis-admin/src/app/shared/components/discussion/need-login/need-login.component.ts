import { Component, OnInit } from '@angular/core';
import { AppHtmlRoutes } from '../../../../app-routing.model';

@Component({
  selector: 'app-need-login',
  templateUrl: './need-login.component.html',
  styleUrls: ['./need-login.component.scss'],
})
export class NeedLoginComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;

  constructor() {}

  ngOnInit(): void {}
}
