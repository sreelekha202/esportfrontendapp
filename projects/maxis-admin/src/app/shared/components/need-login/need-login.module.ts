import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { I18nModule } from 'esports';
import { environment } from '../../../../environments/environment';
import { NeedLoginComponent } from './need-login.component';

@NgModule({
  declarations: [NeedLoginComponent],
  imports: [CommonModule, I18nModule.forRoot(environment), RouterModule],
  exports: [NeedLoginComponent],
})
export class NeedLogingModule {}
