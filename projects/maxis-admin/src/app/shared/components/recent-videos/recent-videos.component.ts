import { Component, OnInit } from '@angular/core';
import { AppHtmlRoutes } from '../../../app-routing.model';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {
  EsportsToastService,
  EsportsUtilsService,
  EsportsVideoLibraryService,
} from 'esports';
import { environment } from './../../../../environments/environment';
const API = environment.apiEndPoint;

@Component({
  selector: 'app-recent-videos',
  templateUrl: './recent-videos.component.html',
  styleUrls: ['./recent-videos.component.scss'],
})
export class RecentVideosComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;
  currLanguage = 'english';
  videoLibrary = [];
  activeVideo = null;

  constructor(
    private modalService: NgbModal,
    private toastService: EsportsToastService,
    private videoLibraryService: EsportsVideoLibraryService,
    public utilsService: EsportsUtilsService
  ) {}

  ngOnInit(): void {
    this.getVideoLibrary();
  }

  onShowVideo(content) {
    this.modalService.open(content, {
      centered: true,
      windowClass: 'custom-modal-content',
    });
  }

  getVideoLibrary = async () => {
    try {
      const pagination = {
        page: 1,
        limit: 5,
        sort: '-updatedOn',
        projection: [
          '_id',
          'slug',
          'title',
          'description',
          'youtubeUrl',
          'thumbnailUrl',
          'createdBy',
        ],
      };

      const query = `?pagination=${encodeURIComponent(
        JSON.stringify(pagination)
      )}`;

      const response = await this.videoLibraryService.fetchVideoLibrary(
        API,
        query
      );
      this.videoLibrary = await Promise.all(
        response?.data?.docs.slice(0, 4).map(async (item) => {
          return {
            ...item,
            youtubeUrl: await this.utilsService.getEmbbedUrl(item.youtubeUrl),
          };
        })
      );
      this.activeVideo = this.videoLibrary.length ? this.videoLibrary[0] : null;
    } catch (error) {
      this.toastService.showError(error.error?.message || error?.message);
    }
  };

  getVideoIconByType(type: string): string {
    const path = 'assets/icons/news';

    switch (type) {
      case 'feature':
        return `${path}/feature.svg`;
      case 'opinion':
        return `${path}/opinion.svg`;
      case 'guide':
        return `${path}/guide.svg`;
      case 'listicle':
        return `${path}/listicle.svg`;
      case 'hot':
        return `${path}/hot.svg`;
      case 'trending':
        return `${path}/trending.svg`;
    }
  }
}
