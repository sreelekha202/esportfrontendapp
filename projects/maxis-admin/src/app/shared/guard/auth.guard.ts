import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';

import { AuthServices } from '../../core/service';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';
import { GlobalUtils } from 'esports';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanActivate {
  constructor(private authService: AuthServices, private router: Router) {}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    let url: string;

    if (next?.params?.id && next.routeConfig.path === 'join') {
      url = '/tournament/' + next?.params?.id + '/' + next.routeConfig.path;
    } else {
      url = state.url;
    }

    return this.checkLogin(url);
  }

  canActivateChild(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    return this.canActivate(route, state);
  }

  checkLogin(url: string): boolean {
    if (
      GlobalUtils.isBrowser() &&
      localStorage.getItem(environment.currentToken)
    ) {
      return true;
    } else {
      // Store the attempted URL for redirecting
      this.authService.redirectUrl = url;

      //this.toastService.showInfo("Please login to access that page.");
      // Navigate to the login page with extras
      this.router.navigate(['/user/email-login']);
      return false;
    }
  }
}
