import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { NgbdSortableHeader } from "./sort-table.directive";
import { OnlyNumberDirective } from "./only-number.directive";

@NgModule({
  declarations: [OnlyNumberDirective, NgbdSortableHeader],
  imports: [CommonModule],
  exports: [OnlyNumberDirective, NgbdSortableHeader],
})
export class DirectivesModule {}
