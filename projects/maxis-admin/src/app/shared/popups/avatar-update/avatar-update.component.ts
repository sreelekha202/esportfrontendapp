import { Component, OnInit, Inject } from "@angular/core";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";
@Component({
  selector: "app-avatar-update",
  templateUrl: "./avatar-update.component.html",
  styleUrls: ["./avatar-update.component.scss"],
})
export class AvatarUpdateComponent implements OnInit {
  selectedAvatar: any;
  selectedAvatarIndex;
  base64textString;
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<any>
  ) {}

  ngOnInit(): void {}

  onClose(): void {
    let image = this.selectedAvatar || this.base64textString;
    this.dialogRef.close(image);
  }
  selectImage(imgUrl, index) {
    this.base64textString = null;
    this.selectedAvatarIndex = index;
    this.selectedAvatar = imgUrl;
  }
  //convert image to binary
  handleFileSelect(evt) {
    this.selectedAvatarIndex = null;
    this.selectedAvatar = null;
    const files = evt.target.files;
    const file = files[0];
    if (files && file) {
      var reader = new FileReader();
      reader.onload = this._handleReaderLoaded.bind(this);
      reader.readAsBinaryString(file);
    }
  }
  _handleReaderLoaded(readerEvt) {
    var binaryString = readerEvt.target.result;
    this.base64textString = "data:image/png;base64," + btoa(binaryString);
  }
}
