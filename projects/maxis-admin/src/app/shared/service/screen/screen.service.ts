import { Injectable } from "@angular/core";
import { GlobalUtils } from "esports";
import { BehaviorSubject } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class ScreenService {
  isLgScreen = new BehaviorSubject(ScreenService.isLgScreen());

  static isLgScreen(): boolean {
    const md = 1199;
    return Boolean(window.innerWidth < md);
  }

  constructor() {}

  onAppResizeListener(): void {
    if (GlobalUtils.isBrowser()) {
      window.addEventListener("resize", () => {
        this.isLgScreen.next(ScreenService.isLgScreen());
      });
    }
  }
}
