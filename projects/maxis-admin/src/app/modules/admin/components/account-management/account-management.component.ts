import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { WalletService } from 'projects/maxis-admin/src/app/core/service/wallet.service';
import { EditColumnsComponent } from '../../popups/edit-columns/edit-columns.component';
import { ngxCsv } from 'ngx-csv/ngx-csv';
import { EsportsToastService } from 'esports';
@Component({
  selector: 'app-account-management',
  templateUrl: './account-management.component.html',
  styleUrls: ['./account-management.component.scss'],
})
export class AccountManagementComponent implements OnInit {
  selectedTabIndex = 0;
  // data
  tableColumns: any = [];
  displayedColumns = this.tableColumns;
  editColumns = [];
  dataSource = [];
  profileDetails: any;
  // filter
  startDate: any;
  endDate: any;
  username: any;
  msisdn: any;
  email: any;
  walletStatusList = [
    { label: 'All', value: 'All' },
    { label: 'Reload Success', value: '1' },
    { label: 'Reload Failed', value: '2' },
    { label: 'Pending', value: '3' },
  ];
  selectedWalletStatus: any = this.walletStatusList[0]['value'];
  purchaseStatusList = [
    { label: 'All', value: 'All' },
    { label: 'Purchase Success', value: '1' },
    { label: 'Purchase Failed', value: '2' },
  ];
  selectedPurchaseStatus: any = this.purchaseStatusList[0]['value'];
  typeList = ['profile', 'wallet', 'purchase'];
  selectedType = this.typeList[0];
  isLoading = false;
  constructor(
    public dialog: MatDialog,
    private esportsToastService: EsportsToastService,
    private walletService: WalletService
  ) {}

  ngOnInit(): void {}

  getUserManagementShopReport() {
    this.isLoading = true;
    const param: any = { type: this.selectedType };
    this.selectedTabIndex == 1
      ? this.selectedWalletStatus
        ? (param.status =
            this.selectedWalletStatus == 3 ? '0' : this.selectedWalletStatus)
        : ''
      : '';
    this.selectedTabIndex == 2
      ? this.selectedPurchaseStatus
        ? (param.status = this.selectedPurchaseStatus)
        : ''
      : '';
    this.startDate ? (param.startDate = this.startDate) : '';
    this.endDate ? (param.endDate = this.endDate) : '';
    this.username ? (param.username = this.username) : '';
    this.email ? (param.email = this.email) : '';
    this.msisdn
      ? this.msisdn.length > 10
        ? (param.msisdn = this.msisdn)
        : (param.modemId = this.msisdn)
      : '';
    this.walletService.getAccountManagementReport(param).subscribe(
      (res) => {
        this.isLoading = false;
        this.dataSource = res.data.array;
        this.profileDetails = res.data.profile;
      },
      (err) => {
        this.dataSource = [];
        this.tableColumns = [];
        this.profileDetails = false;
        this.isLoading = false;
        this.esportsToastService.showError(err.error.message);
      }
    );
  }
  exportCSV() {
    let data = [];
    this.dataSource.map((obj, i) => {
      let object = new Object();
      for (let key in obj) {
        if (this.displayedColumns.includes(key)) {
          object[key] = obj[key];
        }
      }
      data.push(object);
    });
    new ngxCsv(data, 'user-report', {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalseparator: '.',
      showLabels: true,
      headers: this.displayedColumns,
    });
  }
  openDialog(): void {
    const dialogRef = this.dialog.open(EditColumnsComponent, {
      width: '600px',
      data: this.editColumns,
      disableClose: true,
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.editColumns = result;
        this.displayedColumns = [];
        this.editColumns.map((obj) => {
          obj.isSelected ? this.displayedColumns.push(obj.label) : '';
        });
      }
    });
  }

  onTabChanged(event) {
    switch (event.index) {
      case 0:
        {
          this.selectedTabIndex = 0;
          this.selectedType = this.typeList[0];
          this.dataSource = [];
          this.tableColumns = [
            'dateTime',
            'activity',
            'updatedAt',
            'oldmsisdn',
            'newmsisdn',
          ];
          this.editColumn();
          this.displayedColumns = this.tableColumns;
          this.getUserManagementShopReport();
        }
        break;
      case 1:
        {
          this.selectedTabIndex = 1;
          this.selectedType = this.typeList[1];
          this.dataSource = [];
          this.tableColumns = [
            'dateTime',
            'orderId',
            'msisdn',
            'email',
            'transactionType',
            'transactionId',
            'paymentMethod',
            'reloadAmount',
            'gengCoins',
            'reloadStatus',
            'expiryDateTime',
            'GCExpired',
            'GCExpiredInRM',
          ];
          this.editColumn();
          this.displayedColumns = this.tableColumns;
          this.getUserManagementShopReport();
        }
        break;
      case 2:
        {
          this.selectedTabIndex = 2;
          this.selectedType = this.typeList[2];
          this.dataSource = [];
          this.tableColumns = [
            'dateTime',
            'orderId',
            'msisdn',
            'email',
            'voucherCode',
            'transactionId',
            'paymentMethod',
            'purchaseStatus',
          ];
          this.editColumn();
          this.displayedColumns = this.tableColumns;
          this.getUserManagementShopReport();
        }
        break;
    }
  }
  editColumn() {
    this.editColumns = [];
    this.tableColumns.map((obj) => {
      this.editColumns.push({ label: obj, isSelected: true });
    });
  }
  onSearch() {
    this.selectedTabIndex = 0;
    this.selectedType = this.typeList[0];
    this.tableColumns = [
      'dateTime',
      'activity',
      'updatedAt',
      'oldmsisdn',
      'newmsisdn',
    ];
    this.editColumn();
    this.displayedColumns = this.tableColumns;
    this.getUserManagementShopReport();
  }
}
