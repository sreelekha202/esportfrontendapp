import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { EsportsGameService } from 'esports';
import { environment } from './../../../../../environments/environment';
const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;
import {
  InfoPopupComponentData,
  InfoPopupComponentType,
  InfoPopupComponent,
} from "../../../../shared/popups/info-popup/info-popup.component";
import { MatDialog } from "@angular/material/dialog";
import { GlobalUtils } from "esports";
import { TranslateService } from "@ngx-translate/core";
import { DatatableComponent } from "@swimlane/ngx-datatable";

@Component({
  selector: "app-game",
  templateUrl: "./game.component.html",
  styleUrls: ["./game.component.scss"],
})
export class GameComponent implements OnInit {
  newObj: any = {
    _id: "0123456789ab",
  };
  tempTable = [];
  @ViewChild(DatatableComponent) table: DatatableComponent;
  columnsFirstTab = [
    { name: "name" },
    { name: "bracketTypes" },
    { name: "platforms" },
  ];
  platformsError = false;
  selectedItems = [];
  dropdownSettings = {};
  logoURI: any = "";
  rowsFirstTab = [];
  imageURI: any = "";
  name: any;
  isEditable = {};
  showGameTable = true;
  isTournamentAllowed = false;
  type = {
    single: false,
    double: false,
    round_robin: false,
    battle_royale: false,
    swiss_safeis: false,
  };
  platformList = [];
  isLoading: boolean = true;

  @ViewChild("logoUpload", { static: false }) set logoUpload(element) {
    if (GlobalUtils.isBrowser() && element && this.logoURI) {
      document.getElementById("logoUpload").style.background =
        "url(" + this.logoURI + ")";
    }
  }
  @ViewChild("imageUpload", { static: false }) set imageUpload(element) {
    if (GlobalUtils.isBrowser() && element && this.imageURI) {
      document.getElementById("imageUpload").style.background =
        "url(" + this.imageURI + ")";
    }
  }

  /**
   * Contructor of the page
   * @param gameService
   * @param globalUtils
   * @param dialog
   */
  constructor(
    private gameService: EsportsGameService,
    public globalUtils: GlobalUtils,
    public dialog: MatDialog,
    private translateService: TranslateService
  ) {}

  /**
   * Page Initialization
   */
  ngOnInit(): void {
    this.platformList = [];
    this.selectedItems = [];
    this.dropdownSettings = {
      singleSelection: false,
      text: "",
      selectAllText: this.translateService.instant("GAME.SELECT_ALL"),
      unSelectAllText: this.translateService.instant("GAME.UNSELECT_ALL"),
      searchPlaceholderText: this.translateService.instant("GAME.SEARCH"),
      enableSearchFilter: true,
      classes: "myclass multiselect-class",
    };
    this.fetchPlatforms();
  }

  searchByValueFilter(event) {
    const value = event.target.value.toLowerCase().trim();
    const count = Object.keys(this.tempTable[0]).length;
    const keys = Object.keys(this.tempTable[0]);
    const filterData = this.tempTable.filter((item) => {
      return item && item["name"].toLowerCase().includes(value);
    });
    this.rowsFirstTab = [...filterData];
    this.table.offset = 0;
  }

  /**
   * method to be called to get games list
   */
  getGames() {
    this.isLoading = true;
    this.gameService.getAdminAllGames(API).subscribe(
      (res) => {
        if (res && res.data) {
          this.rowsFirstTab = res.data;
          this.tempTable = res.data;
          this.isLoading = false;
        }
      },
      (err) => {
        this.isLoading = false;
        const afterBlockData: InfoPopupComponentData = {
          title: this.translateService.instant("API.GAME.GET.ERROR_HEADER"),
          text:
            this.translateService.instant(err.error.messageCode) ||
            err.error.message,
          type: InfoPopupComponentType.info,
        };
        this.dialog.open(InfoPopupComponent, { data: afterBlockData });
      }
    );
  }

  emitChangedValue(e, type) {
    if (type !== "isTournamentAllowed") {
      this.type[type] = e.target.checked;
    } else {
      this.isTournamentAllowed = e.target.checked;
    }
  }

  showPlatformName(item) {
    const platformName: any = this.platformList.find((ele) => {
      return ele.id == item;
    });
    return platformName.itemName;
  }

  /**
   * method to change the status and order of games
   * @param e
   * @param u_id
   */
  updateRow(e, type, row) {
    this.isEditable = {};
    if (type === "status") {
      const blockUserData: InfoPopupComponentData = {
        title: this.translateService.instant(
          "API.GAME.MODAL.STATUS_CONFIRMATION_MODAL.TITLE"
        ),
        text: this.translateService.instant(
          "API.GAME.MODAL.STATUS_CONFIRMATION_MODAL.TEXT"
        ),
        type: InfoPopupComponentType.confirm,
        btnText: this.translateService.instant(
          "API.GAME.MODAL.STATUS_CONFIRMATION_MODAL.BUTTONTEXT"
        ),
      };
      const dialogRef = this.dialog.open(InfoPopupComponent, {
        data: blockUserData,
      });

      dialogRef.afterClosed().subscribe((confirmed) => {
        if (confirmed) {
          this.isLoading = true;
          this.gameService
            .saveGame(API,row["_id"], { status: e.checked ? 1 : 0 })
            .subscribe(
              (res: any) => {
                this.getGames();
                const afterBlockData: InfoPopupComponentData = {
                  title: this.translateService.instant(
                    "API.GAME.PUT.SUCCESS_HEADER"
                  ),
                  text:
                    this.translateService.instant(res.messageCode) ||
                    res.message,
                  type: InfoPopupComponentType.info,
                };
                this.dialog.open(InfoPopupComponent, { data: afterBlockData });
              },
              (err: any) => {
                this.isLoading = false;
                const afterBlockData: InfoPopupComponentData = {
                  title: this.translateService.instant(
                    "API.GAME.PUT.ERROR_HEADER"
                  ),
                  text:
                    this.translateService.instant(err.error.messageCode) ||
                    err.error.message,
                  type: InfoPopupComponentType.info,
                };
                this.dialog.open(InfoPopupComponent, { data: afterBlockData });
              }
            );
        }
      });
    } else if (
      type === "order" &&
      e.target.value &&
      row["order"] != e.target.value
    ) {
      this.isLoading = true;
      this.gameService
        .saveGame(API,row["_id"], { order: +e.target.value })
        .subscribe(
          (res: any) => {
            this.getGames();
            const afterBlockData: InfoPopupComponentData = {
              title: this.translateService.instant(
                "API.GAME.PUT.SUCCESS_HEADER"
              ),
              text:
                this.translateService.instant(res.messageCode) || res.message,
              type: InfoPopupComponentType.info,
            };
            this.dialog.open(InfoPopupComponent, { data: afterBlockData });
          },
          (err) => {
            this.isLoading = false;
            const afterBlockData: InfoPopupComponentData = {
              title: this.translateService.instant("API.GAME.PUT.ERROR_HEADER"),
              text:
                this.translateService.instant(err.error.messageCode) ||
                err.error.message,
              type: InfoPopupComponentType.info,
            };
            this.dialog.open(InfoPopupComponent, { data: afterBlockData });
          }
        );
    }
  }

  /**
   * method to be called to delete the game
   * @param _gameId
   */
  onRemoveGame(_gameId) {
    this.isLoading = true;
    this.gameService.disableGame(API,_gameId).subscribe(
      (res: any) => {
        this.getGames();
        const afterBlockData: InfoPopupComponentData = {
          title: this.translateService.instant("API.GAME.DELETE.ERROR_HEADER"),
          text: this.translateService.instant(res.messageCode) || res.message,
          type: InfoPopupComponentType.info,
        };
        this.dialog.open(InfoPopupComponent, { data: afterBlockData });
        this.isLoading = false;
      },
      (err: any) => {
        this.isLoading = false;
        const afterBlockData: InfoPopupComponentData = {
          title: this.translateService.instant("API.GAME.DELETE.ERROR_HEADER"),
          text:
            this.translateService.instant(err.error.messageCode) ||
            err.error.message,
          type: InfoPopupComponentType.info,
        };
        this.dialog.open(InfoPopupComponent, { data: afterBlockData });
      }
    );
  }

  async fetchPlatforms() {
    const platformsData: any = await this.gameService
      .fetchPlatforms(API)
      .toPromise();
    if (platformsData.data) {
      this.platformList = platformsData.data.map((item) => {
        return {
          id: item._id,
          itemName: item.name,
        };
      });
    } else {
      this.platformList = [];
    }
    this.getGames();
  }

  /**
   * method to load the log image on the page
   */
  logoUploader() {
    if (GlobalUtils.isBrowser()) {
      const logo: any = document.getElementById("logofileUpload");
      logo.click();
      logo.onchange = function () {
        const reader = new FileReader();
        reader.onload = function (event: any) {
          const img = new Image();
          img.onload = function () {
            this.logoURI = event.target.result;
            document.getElementById("logoUpload").style["background"] =
              "url(" + event.target.result + ")";
          }.bind(this);
          img.onerror = function () {};
          img.src = window.URL.createObjectURL(logo.files[0]);
        }.bind(this);
        reader.readAsDataURL(logo.files[0]);
      }.bind(this);
    }
  }

  /**
   * method to load image on the page
   */
  imageUploader() {
    if (GlobalUtils.isBrowser()) {
      const image: any = document.getElementById("imagefileUpload");
      image.click();
      image.onchange = function () {
        const reader = new FileReader();
        reader.onload = function (event: any) {
          const img = new Image();
          img.onload = function () {
            this.imageURI = event.target.result;
            document.getElementById("imageUpload").style["background"] =
              "url(" + event.target.result + ")";
          }.bind(this);
          img.onerror = function () {};
          img.src = window.URL.createObjectURL(image.files[0]);
        }.bind(this);
        reader.readAsDataURL(image.files[0]);
      }.bind(this);
    }
  }

  onItemSelect(item: any) {
    if (this.selectedItems.length > 0) {
      this.platformsError = false;
    }
  }

  OnItemDeSelect(item: any) {
    if (this.selectedItems.length == 0) {
      this.platformsError = true;
    }
  }

  onSelectAll(items: any) {
    if (this.selectedItems.length > 0) {
      this.platformsError = false;
    }
  }

  onDeSelectAll(items: any) {
    if (this.selectedItems.length == 0) {
      this.platformsError = true;
    }
  }

  /**
   * method to call before editing the game
   * @param gameId
   */
  editGame(gameId) {
    let gameObject = this.rowsFirstTab.find((ele) => {
      return ele._id === gameId;
    });
    this.newObj = { ...gameObject };

    if (GlobalUtils.isBrowser()) {
      this.selectedItems = this.newObj.platform.map((item) => {
        return {
          id: item?._id,
          itemName: item?.name,
        };
      });
    }
    this.name = this.newObj.name;
    if (this.newObj.bracketTypes) {
      this.type = this.newObj.bracketTypes;
    }
    this.logoURI = this.newObj.logo;
    this.imageURI = this.newObj.image;
    this.isTournamentAllowed = this.newObj.isTournamentAllowed
      ? this.newObj.isTournamentAllowed
      : false;
    this.showGameTable = false;
  }

  /**
   * method to reset the controls of the game
   */
  resetGamesControl() {
    this.type = {
      single: false,
      double: false,
      round_robin: false,
      battle_royale: false,
      swiss_safeis: false,
    };
    this.newObj._id = "0123456789ab";
    this.logoURI = "";
    this.imageURI = "";
    this.name = "";
    this.selectedItems = [];
  }

  /**
   * method to toggle the edit panel
   */
  toggleGamePanel() {
    this.resetGamesControl();
    this.rowsFirstTab = [];
    this.getGames();
    this.showGameTable = !this.showGameTable;
  }

  /**
   * method to call to save the game
   */
  saveGame() {
    let selectedPlatform = this.selectedItems.map((item) => {
      return item.id;
    });
    if (
      this.logoURI &&
      this.imageURI &&
      this.name &&
      this.selectedItems.length > 0
    ) {
      const gameObj = {
        name: this.name,
        logo: this.logoURI,
        image: this.imageURI,
        slug: this.name.replace(/\s/g, ""),
        isTournamentAllowed: this.isTournamentAllowed,
        bracketTypes: this.type,
        platform: selectedPlatform,
      };
      this.isLoading = true;
      this.gameService.saveGame(API,this.newObj._id, gameObj).subscribe(
        (res: any) => {
          this.getGames();
          const afterBlockData: InfoPopupComponentData = {
            title: this.translateService.instant(
              "API.GAME.POST.SUCCESS_HEADER"
            ),
            text: this.translateService.instant(res.messageCode) || res.message,
            type: InfoPopupComponentType.info,
          };
          this.dialog.open(InfoPopupComponent, { data: afterBlockData });
          this.resetGamesControl();
          this.showGameTable = true;
        },
        (err: any) => {
          this.isLoading = false;
          const afterBlockData: InfoPopupComponentData = {
            title: this.translateService.instant("API.GAME.POST.ERROR_HEADER"),
            text:
              this.translateService.instant(err.error.messageCode) ||
              err.error.message,
            type: InfoPopupComponentType.info,
          };
          this.dialog.open(InfoPopupComponent, { data: afterBlockData });
        }
      );
    } else {
      if (this.selectedItems.length == 0) {
        this.platformsError = true;
      }
    }
  }
}
