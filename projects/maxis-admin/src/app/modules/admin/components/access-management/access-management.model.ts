export interface AccessManagementUser {
  userId: number;
  name: string;
  email: string;
  profilePicture: string;
  accessLevel: UserAccessType[];
  phone: string;
  buttons: string;
}

export enum UserAccessType {
  em = "em",
  un = "un",
  cm = "cm",
  um = "um",
  sc = "sc",
  acm = "acm",
  tm = "tm",
  // sm = "sm",
  sem = 'sem',
  store = 'store',
  wm = 'wm'
}

export const AccessType = {
  em: "ADMIN.ACCESS_MANAGEMENT.LABEL.ESPORTS_MANAGEMENT",
  un: "ADMIN.ACCESS_MANAGEMENT.LABEL.USER_NOTIFICATION",
  cm: "ADMIN.ACCESS_MANAGEMENT.LABEL.CONTENT_MANAGEMENT",
  um: "ADMIN.ACCESS_MANAGEMENT.LABEL.USER_MANAGEMENT",
  sc: "ADMIN.ACCESS_MANAGEMENT.LABEL.SITE_CONFIGURATION",
  acm: "ADMIN.ACCESS_MANAGEMENT.LABEL.ACCESS_MANAGEMENT",
  tm: "ADMIN.ACCESS_MANAGEMENT.LABEL.TEAM_MANAGEMENT",
  // sm: "ADMIN.ACCESS_MANAGEMENT.LABEL.SPAM_MANAGEMENT",
  sem: 'ADMIN.ACCESS_MANAGEMENT.LABEL.SEASON_MANAGEMENT',
  store: 'ADMIN.ACCESS_MANAGEMENT.LABEL.SHOP_MANAGEMENT',
  wm: 'ADMIN.ACCESS_MANAGEMENT.LABEL.WALLET_MANAGEMENT',
};
