import { Component, OnInit, Inject } from "@angular/core";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";

@Component({
  selector: "app-payment-disbursement-dialog",
  templateUrl: "./payment-disbursement-dialog.component.html",
  styleUrls: [
    "./payment-disbursement-dialog.component.scss",
    "../esports-payment-details.component.scss",
  ],
})
export class PaymentDisbursementDialogComponent implements OnInit {
  constructor(
    @Inject(MAT_DIALOG_DATA) public data,
    public dialogRef: MatDialogRef<PaymentDisbursementDialogComponent>
  ) {}

  ngOnInit(): void {}

  onClose(text): void {
    this.dialogRef.close(text);
  }
}
