import { Component, OnInit, ViewChild } from "@angular/core";
import { EsportsAdminService, GlobalUtils } from 'esports';
import { MatDialog } from "@angular/material/dialog";
import { FormGroup, Validators, FormBuilder } from "@angular/forms";
import {
  InfoPopupComponentData,
  InfoPopupComponentType,
  InfoPopupComponent,
} from "../../../../shared/popups/info-popup/info-popup.component";
import { TranslateService } from "@ngx-translate/core";
import { environment } from './../../../../../environments/environment';

const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;
@Component({
  selector: "app-shop-configuration",
  templateUrl: "./shop-configuration.component.html",
  styleUrls: ["./shop-configuration.component.scss"],
})
export class ShopConfigurationComponent implements OnInit {
  isLoading = false;
  public ownerForm: FormGroup;
  productsList: [];
  id;
  isEditViewOpen = false;
  logoUri = "";
  logoError = false;
  bannerError = false;
  bannerUri = "";
  statusType = {
    Active: "Active",
    InProgress: "In-Progress",
    Done: "Done",
  };
  typeValues = {
    TOPUP: this.translateService.instant(
      "ADMIN.SITE_CONFIGURATION.SUB_PAGE.TOP_UP"
    ),
    VOUCHER: this.translateService.instant(
      "ADMIN.SITE_CONFIGURATION.SUB_PAGE.VOUCHER"
    ),
  };
  selectedStatus: any;
  selectedType: any;

  /**
   * ViewChild check of logo image object
   */
  @ViewChild("logoUpload", { static: false }) set logoUpload(element) {
    if (GlobalUtils.isBrowser() && element && this.logoUri) {
      document.getElementById("logoUpload").style.background =
        "url(" + this.logoUri + ")";
    }
  }

  /**
   * ViewChild check of banner image object
   */
  @ViewChild("bannerUpload", { static: false }) set bannerUpload(element) {
    if (GlobalUtils.isBrowser() && element && this.bannerUri) {
      document.getElementById("bannerUpload").style.background =
        "url(" + this.bannerUri + ")";
    }
  }

  constructor(
    private adminService: EsportsAdminService,
    private dialog: MatDialog,
    private fb: FormBuilder,
    private translateService: TranslateService
  ) {}

  ngOnInit(): void {
    this.ownerForm = this.fb.group({
      name: ["", Validators.compose([Validators.required])],
      p_name: ["", Validators.compose([Validators.required])],
      type: ["", Validators.compose([Validators.required])],
      s_descp: ["", Validators.compose([Validators.required])],
      l_descp: ["", Validators.compose([Validators.required])],
      status: [""],
    });
    this.loadProducts();
  }

  selectValue(e) {
    this.selectedStatus = e;
  }

  selectType(e) {
    this.selectedType = e;
  }

  public hasError = (controlName: string, errorName: string) => {
    return this.ownerForm.controls[controlName].hasError(errorName);
  };

  loadProducts() {
    this.isLoading = true;
    this.adminService.getAllProducts(API).subscribe(
      (res: any) => {
        this.productsList = res.data;
        this.isLoading = false;
      },
      (err: any) => {
        const afterBlockData: InfoPopupComponentData = {
          title: this.translateService.instant("API.PRODUCT.GET.ERROR_HEADER"),
          text:
            this.translateService.instant(err.error.messageCode) ||
            err.error.message,
          type: InfoPopupComponentType.info,
        };
        this.dialog.open(InfoPopupComponent, { data: afterBlockData });
      }
    );
  }

  markFormGroupTouched(formGroup: FormGroup) {
    (<any>Object).values(formGroup.controls).forEach((control) => {
      control.markAsTouched();

      if (control.controls) {
        this.markFormGroupTouched(control);
      }
    });
  }

  sync(_this) {
    const element: any = document.getElementById(_this);
    (<HTMLInputElement>(
      document.getElementById("p_name")
    )).value = element.value.replace(/\s/g, "");
    this.ownerForm.controls.p_name.setValue(
      (<HTMLInputElement>document.getElementById("p_name")).value
    );
  }

  /**
   * method to upload an image object in page
   */
  logoUploader() {
    const logo: any = document.getElementById("logoUploader");
    logo.click();
    logo.onchange = function () {
      const reader = new FileReader();
      reader.onload = function (event: any) {
        const img = new Image();
        img.onload = function () {
          this.logoUri = event.target.result;
          this.logoError = false;
          document.getElementById("logoUpload").style["background"] =
            "url(" + event.target.result + ")";
        }.bind(this);
        img.onerror = function () {};
        img.src = window.URL.createObjectURL(logo.files[0]);
      }.bind(this);
      reader.readAsDataURL(logo.files[0]);
    }.bind(this);
  }

  bannerUploader() {
    const banner: any = document.getElementById("bannerUploader");
    banner.click();
    banner.onchange = function () {
      const reader = new FileReader();
      reader.onload = function (event: any) {
        const img = new Image();
        img.onload = function () {
          this.bannerUri = event.target.result;
          this.bannerError = false;
          document.getElementById("bannerUpload").style["background"] =
            "url(" + event.target.result + ")";
        }.bind(this);
        img.onerror = function () {};
        img.src = window.URL.createObjectURL(banner.files[0]);
      }.bind(this);
      reader.readAsDataURL(banner.files[0]);
    }.bind(this);
  }

  updateProduct() {
    if (!this.ownerForm.valid) {
      this.markFormGroupTouched(this.ownerForm);
      return;
    } else if (!this.logoUri) {
      this.logoError = true;
      return;
    } else if (!this.bannerUri) {
      this.bannerError = true;
      return;
    }
    this.isLoading = true;
    const formData = {
      name: this.ownerForm.value.name,
      productName: this.ownerForm.value.p_name,
      type: this.selectedType,
      logo: this.logoUri,
      banner: this.bannerUri,
      shortDesc: this.ownerForm.value.s_descp,
      longDesc: this.ownerForm.value.l_descp,
    };
    const subsciber = this.id
      ? this.adminService.updateProduct(API,this.id, formData)
      : this.adminService.addProduct(API,formData);
    subsciber.subscribe(
      (res: any) => {
        this.isEditViewOpen = false;
        this.isLoading = false;
        const afterBlockData: InfoPopupComponentData = {
          title: this.translateService.instant(
            "API.PRODUCT.POST.SUCCESS_HEADER"
          ),
          text: this.translateService.instant(res.messageCode) || res.message,
          type: InfoPopupComponentType.info,
        };
        this.dialog.open(InfoPopupComponent, { data: afterBlockData });
        this.loadProducts();
      },
      (err: any) => {
        this.isLoading = false;
        const afterBlockData: InfoPopupComponentData = {
          title: this.translateService.instant("API.PRODUCT.PUT.ERROR_HEADER"),
          text:
            this.translateService.instant(err.error.messageCode) ||
            err.error.message,
          type: InfoPopupComponentType.info,
        };
        this.dialog.open(InfoPopupComponent, { data: afterBlockData });
      }
    );
  }

  toggleEditPane() {
    this.isEditViewOpen = !this.isEditViewOpen;
  }

  resetControls() {
    this.ownerForm.reset();
    this.logoUri = "";
    this.bannerUri = "";
    this.bannerError = false;
    this.logoError = false;
    this.id = null;
  }

  editPane(row) {
    this.isEditViewOpen = !this.isEditViewOpen;
    this.id = row._id;
    this.logoUri = row.logo;
    this.bannerUri = row.banner;
    this.ownerForm.setValue({
      name: row.name,
      p_name: row.productName,
      type: row.type,
      s_descp: row.shortDesc,
      l_descp: row.longDesc,
      status: 1,
    });
    this.ownerForm.setErrors(null);
  }
}
