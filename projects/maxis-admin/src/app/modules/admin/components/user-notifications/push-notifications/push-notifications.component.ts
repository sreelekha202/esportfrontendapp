import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EsportsNotificationsService, GlobalUtils } from 'esports';
import {
  InfoPopupComponentData,
  InfoPopupComponentType,
  InfoPopupComponent,
} from '../../../../../shared/popups/info-popup/info-popup.component';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
import { MatDialog } from '@angular/material/dialog';
import { environment } from './../../../../../../environments/environment';
const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;
@Component({
  selector: 'app-push-notifications',
  templateUrl: './push-notifications.component.html',
  styleUrls: [
    './push-notifications.component.scss',
    '../user-notifications.component.scss',
  ],
})
export class PushNotificationsComponent implements OnInit, OnDestroy {
  pushNotificationForm = this.fb.group({
    userSegment: ['', Validators.required],
    title: ['', Validators.required],
    message: ['', Validators.required],
  });
  @ViewChild('bannerUpload', { static: false }) set bannerUpload(element) {
    if (GlobalUtils.isBrowser() && element && this.headerImageUri) {
      document.getElementById('bannerUpload').style.background =
        'url(' + this.headerImageUri + ')';
    }
  }
  isLoading = false;
  headerImageUri = '';
  userSegementValues = {
    ALL_USERS: this.translateService.instant(
      'ADMIN.USER_NOTIFICATION.LABEL.ALL_USERS'
    ),
    TOURNAMENT_PLAYERS: this.translateService.instant(
      'ADMIN.USER_NOTIFICATION.LABEL.TOURNAMENT_PLAYERS'
    ),
    TOURNAMENT_ORGANIZERS: this.translateService.instant(
      'ADMIN.USER_NOTIFICATION.LABEL.TOURNAMENT_ORGANIZER'
    ),
  };
  messageSent = false;

  constructor(
    private fb: FormBuilder,
    private userNotificationsService: EsportsNotificationsService,
    private translateService: TranslateService,
    private dialog: MatDialog
  ) {
    translateService.onLangChange.subscribe((event: LangChangeEvent) => {
      this.userSegementValues = {
        ALL_USERS: this.translateService.instant(
          'ADMIN.USER_NOTIFICATION.LABEL.ALL_USERS'
        ),
        TOURNAMENT_PLAYERS: this.translateService.instant(
          'ADMIN.USER_NOTIFICATION.LABEL.TOURNAMENT_PLAYERS'
        ),
        TOURNAMENT_ORGANIZERS: this.translateService.instant(
          'ADMIN.USER_NOTIFICATION.LABEL.TOURNAMENT_ORGANIZER'
        ),
      };
    });
  }

  ngOnInit(): void {}

  ngOnDestroy(): void {
    this.resetControls();
  }
  sendPushNotifications() {
    this.isLoading = true;
    this.messageSent = true;
    const inputData = {
      message: this.pushNotificationForm.value.message,
      title: this.pushNotificationForm.value.title,
      userSegment: this.pushNotificationForm.value.userSegment,
      image: this.headerImageUri,
    };
    this.userNotificationsService
      .sendPushNotifications(API, inputData)
      .subscribe(
        (response: any) => {
          this.resetControls();
          this.isLoading = false;
          this.messageSent = false;
          const afterBlockData: InfoPopupComponentData = {
            title: this.translateService.instant(
              'ADMIN.USER_NOTIFICATION.RESPONSE.SUCCESS'
            ),
            text: response.message,
            type: InfoPopupComponentType.info,
          };
          this.dialog.open(InfoPopupComponent, { data: afterBlockData });
        },
        (err: any) => {
          this.isLoading = false;
          this.messageSent = false;
          const afterBlockData: InfoPopupComponentData = {
            title: this.translateService.instant(
              'ADMIN.USER_NOTIFICATION.RESPONSE.ERROR'
            ),
            text: err.error.message,
            type: InfoPopupComponentType.info,
          };
          this.dialog.open(InfoPopupComponent, { data: afterBlockData });
        }
      );
  }

  markFormGroupTouched(formGroup: FormGroup) {
    (<any>Object).values(formGroup.controls).forEach((control) => {
      control.markAsTouched();

      if (control.controls) {
        this.markFormGroupTouched(control);
      }
    });
  }

  selectType(e) {
    this.pushNotificationForm.controls['userSegment'].setValue(e);
  }
  updatePush() {
    if (!this.pushNotificationForm.valid) {
      this.markFormGroupTouched(this.pushNotificationForm);
      return;
    }
    if (!this.messageSent) {
      this.sendPushNotifications();
    }
  }
  toggleEditPane() {}
  resetControls() {
    this.pushNotificationForm.reset();
    this.headerImageUri = '';
    this.isLoading = false;
    document.getElementById('bannerUpload').style['background'] = '';
  }
  uploadImage() {
    const banner: any = document.getElementById('bannerUploader');
    banner.click();
    banner.onchange = function () {
      const reader = new FileReader();
      reader.onload = function (event: any) {
        const img = new Image();
        img.onload = function () {
          this.headerImageUri = event.target.result;
          document.getElementById('bannerUpload').style['background'] =
            'url(' + event.target.result + ')';
        }.bind(this);
        img.onerror = function () {};
        img.src = window.URL.createObjectURL(banner.files[0]);
      }.bind(this);
      reader.readAsDataURL(banner.files[0]);
    }.bind(this);
  }
  public hasError = (controlName: string, errorName: string) => {
    return this.pushNotificationForm.controls[controlName].hasError(errorName);
  };

  get userSegment() {
    return this.pushNotificationForm.get('userSegment');
  }

  get message() {
    return this.pushNotificationForm.get('message');
  }

  get title() {
    return this.pushNotificationForm.get('title');
  }
}
