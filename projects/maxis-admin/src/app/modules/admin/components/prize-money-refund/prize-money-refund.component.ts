import { Component, OnInit } from "@angular/core";
import { FormControl } from "@angular/forms";
import { debounceTime } from "rxjs/operators";
import { EsportsToastService, EsportsTournamentService } from "esports";
import { environment } from "projects/maxis-admin/src/environments/environment";
const API = environment.apiEndPoint;
@Component({
  selector: "app-prize-money-refund",
  templateUrl: "./prize-money-refund.component.html",
  styleUrls: ["./prize-money-refund.component.scss"],
})
export class PrizeMoneyRefundComponent implements OnInit {
  columns = [
    { name: "Tournament Name" },
    { name: "Game" },
    { name: "End Date" },
    { name: "Participant(s)" },
    { name: "Disbursal Status" },
    { name: "Details" },
  ];
  rows: any = [];
  showDetails = false;
  isLoading = false;

  pagination = {
    page: 1,
    limit: 20,
  };

  tournamentDetails;
  tournamentId;
  pageDetail;
  search = new FormControl("");

  constructor(
    private tournamentService: EsportsTournamentService,
    public toastService: EsportsToastService
  ) { }

  ngOnInit(): void {
    this.getTournamentList();
    this.attachDebounce();
  }

  attachDebounce() {
    this.search.valueChanges.pipe(debounceTime(1000)).subscribe(() => {
      this.pagination.page = 1;
      this.getTournamentList();
    });
  }

  pageChanged(page): void {
    this.pagination.page = page;
    this.getTournamentList();
  }

  getPaginationQuery() {
    return {
      pagination: this.pagination,
    };
  }

  async getTournamentList() {
    try {
      const payload = JSON.stringify({
        searchKey: this.search.value,
        ...this.getPaginationQuery(),
      });
      const res = await this.tournamentService.getPrizeMoneyRefundDisbursals(API,
        payload
      );
      this.pageDetail = res.data;
      this.rows = res.data["docs"];
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
    }
  }

  setPage(event) {
    this.pagination.page = event.offset + 1;
    this.isLoading = true;
    this.rows.length = 0;
    this.getTournamentList();
    this.isLoading = false;
  }

  navigate(tournamentDetails) {
    this.tournamentDetails = tournamentDetails;
    this.showDetails = true;
  }

  isToggle(e) {
    this.showDetails = e.toggle;
    if (!e.toggle) {
      this.getTournamentList();
    }
  }
}
