import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ShopService } from 'projects/maxis-admin/src/app/core/service/shop.service';
import { EditColumnsComponent } from '../../popups/edit-columns/edit-columns.component';
import { ngxCsv } from 'ngx-csv/ngx-csv';

@Component({
  selector: 'app-users-management',
  templateUrl: './users-management.component.html',
  styleUrls: ['./users-management.component.scss'],
})
export class UsersManagementComponent implements OnInit {
  // data
  tableColumns: string[] = [
    'date',
    'status',
    'customerID',
    'UUID',
    'fullName',
    'userName',
    'dob',
    'subscriberType',
    'subscriberSegment',
    'planType',
    'serviceType',
    'paymentMethod',
    'notificationMethod',
    'MSISDN',
    'modemID',
    'email',
    'walletBalance',
    'walletMinBalance',
    'walletMaxBalance',
  ];
  displayedColumns = this.tableColumns;
  editColumns = [];
  dataSource = [];
  // filter
  startDate: any;
  endDate: any;
  subscriberTypesList = ['All'];
  selectedSubscriberType: any = this.subscriberTypesList[0];
  isLoading = false;
  constructor(public dialog: MatDialog, private shopService: ShopService) {}

  ngOnInit(): void {
    this.tableColumns.map((obj) => {
      this.editColumns.push({ label: obj, isSelected: true });
    });
    this.getVendorList();
    this.getUserManagementUserReport();
  }

  getVendorList() {
    this.isLoading = true;
    this.shopService.getSubscriberTypesList().subscribe(
      (res) => {
        this.isLoading = false;
        this.subscriberTypesList = [...this.subscriberTypesList, ...res.data];
      },
      (err) => {
        this.isLoading = false;
      }
    );
  }

  getUserManagementUserReport() {
    this.isLoading = true;
    const param: any = {};
    this.selectedSubscriberType
      ? (param.type = this.selectedSubscriberType)
      : '';
    this.startDate ? (param.startDate = this.startDate) : '';
    this.endDate ? (param.endDate = this.endDate) : '';
    this.shopService.getUserManagementUserReport(param).subscribe(
      (res) => {
        this.isLoading = false;
        let dataSource = res.data;
        let temp = [];
        dataSource.map((obj, i) => {
          let object = new Object();
          for (let key in obj) {
            if (this.displayedColumns.includes(key)) {
              object[key] = obj[key] ? obj[key] : ' - ';
            }
          }
          temp.push(object);
        });
        this.dataSource = temp;
      },
      (err) => {
        this.isLoading = false;
      }
    );
  }
  exportCSV() {
    let data = [];
    this.dataSource.map((obj, i) => {
      let object = new Object();
      for (let key in obj) {
        if (this.displayedColumns.includes(key)) {
          object[key] = obj[key];
        }
      }
      data.push(object);
    });
    new ngxCsv(data, 'user-management-report', {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalseparator: '.',
      showLabels: true,
      headers: this.displayedColumns,
    });
  }
  openDialog(): void {
    const dialogRef = this.dialog.open(EditColumnsComponent, {
      width: '600px',
      data: this.editColumns,
      disableClose: true,
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.editColumns = result;
        this.displayedColumns = [];
        this.editColumns.map((obj) => {
          obj.isSelected ? this.displayedColumns.push(obj.label) : '';
        });
      }
    });
  }
}
