import { Component, OnInit, OnDestroy } from '@angular/core';
import { IPagination } from '../../../../shared/models';
import { DatePipe } from '@angular/common';
import { MatDialog } from '@angular/material/dialog';
import {
  AppHtmlProfileRoutes,
  AppHtmlRoutes,
  AppHtmlAdminRoutes,
} from '../../../../app-routing.model';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import {
  InfoPopupComponent,
  InfoPopupComponentData,
  InfoPopupComponentType,
} from '../../../../shared/popups/info-popup/info-popup.component';
import { EsportsToastService } from 'esports';
import { environment } from './../../../../../environments/environment';
import { UserService } from '../../../../core/service/user.service';
const API = environment.apiEndPoint;
@Component({
  selector: 'app-team-management',
  templateUrl: './team-management.component.html',
  styleUrls: ['./team-management.component.scss'],
})
export class TeamManagementComponent implements OnInit, OnDestroy {
  AppHtmlAdminRoutes = AppHtmlAdminRoutes;
  constructor(
    private userService: UserService,
    public datePipe: DatePipe,
    public toastService: EsportsToastService,
    private modalService: NgbModal,
    private router: Router,
    private matDialog: MatDialog,
    private translateService: TranslateService
  ) {}
  AppHtmlProfileRoutes = AppHtmlProfileRoutes;
  AppHtmlRoutes = AppHtmlRoutes;

  mock_teams = [
    {
      name: 'Dark Ninja',
      count: 8,
      ownerName: 'Aimée',
      createdOn: '10 Jan 20',
    },
    {
      name: 'Turbo',
      count: 3,
      ownerName: 'KEoraer',
      createdOn: '11 Jan 20',
    },
    {
      name: 'Gen G Gold',
      count: 81,
      ownerName: 'ADasdasd',
      createdOn: '12 Jan 20',
    },
  ];

  paginationData = {
    page: 1,
    limit: 50,
    sort: { createdOn: -1 },
    text: '',
  };
  page: IPagination;
  Team = {
    rows: [],
    columns: [
      { name: 'name' },
      { name: 'createdOn' },
      { name: 'count' },
      { ownerName: 'ownerName' },
    ],
  };
  ngOnInit(): void {
    this.getTeamList();
  }
  getTeamList = () => {
    const pagination = JSON.stringify(this.paginationData);
    this.userService.teamsDetail_Admin(API, { pagination }).subscribe(
      (res) => {
        this.Team.rows = res.data;
        this.page = {
          totalItems: res.count,
          itemsPerPage: 50,
          maxSize: 50,
        };
        this.Team.rows.forEach((obj, index) => {
          obj.createdOn = this.datePipe.transform(obj.createdOn, 'd MMMM yyyy');
        });
      },
      (err) => {}
    );
  };
  setPage1(event) {
    this.paginationData.page = event;
    this.getTeamList();
  }
  updateTeam(id, flag) {
    const teamObj = {
      id: id,
      admin: true,
      query: flag
        ? {
            condition: { _id: id, status: 'active' },
            update: { status: 'inactive' },
          }
        : {
            condition: { _id: id, status: 'inactive' },
            update: { status: 'active' },
          },
    };
    this.userService.update_team_admin(API, teamObj).subscribe(
      (res) => {
        if (res.data) {
          const index = this.Team.rows.findIndex((x) => x._id === id);
          if (flag) {
            this.Team.rows[index].status = 'ïnactive';
          } else {
            this.Team.rows[index].status = 'active';
          }
          this.router
            .navigateByUrl('/admin', { skipLocationChange: true })
            .then(() => {
              this.router.navigate(['/admin/team-management']);
            });
          this.toastService.showSuccess(res.message);
        } else {
          this.toastService.showError(res.message);
        }
      },
      (err) => {
        this.toastService.showError(err.error.message);
      }
    );
  }
  async open(id, flag) {
    const data: InfoPopupComponentData = {
      ...this.popUpTitleAndText(flag),
      type: InfoPopupComponentType.confirm,
      btnText: this.translateService.instant('ADMIN.TEAM_MANAGEMENT.YES'),
    };
    const confirmed = await this.matDialog
      .open(InfoPopupComponent, { data })
      .afterClosed()
      .toPromise();

    if (confirmed) {
      this.updateTeam(id, flag);
    } else {
    }
  }
  popUpTitleAndText = (flag) => {
    if (flag) {
      return {
        title: this.translateService.instant(
          'ADMIN.TEAM_MANAGEMENT.TEAM_OPTION.DELETE'
        ),
        text: this.translateService.instant(
          'ADMIN.TEAM_MANAGEMENT.DELETE_WORNNING'
        ),
      };
    } else {
      return {
        title: this.translateService.instant(
          'ADMIN.TEAM_MANAGEMENT.TEAM_OPTION.ÄCTIVE'
        ),
        text: this.translateService.instant(
          'ADMIN.TEAM_MANAGEMENT.ACTIVE_WORNNING'
        ),
      };
    }
  };
  searchByValueFilter(event) {
    this.paginationData.text = event.target.value.trim();
    this.getTeamList();
  }
  ngOnDestroy() {
    this.modalService.dismissAll();
  }
}
