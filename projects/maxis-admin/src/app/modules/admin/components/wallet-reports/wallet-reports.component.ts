import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { WalletService } from 'projects/maxis-admin/src/app/core/service/wallet.service';
import { EditColumnsComponent } from '../../popups/edit-columns/edit-columns.component';
import { ngxCsv } from 'ngx-csv/ngx-csv';

@Component({
  selector: 'app-wallet-reports',
  templateUrl: './wallet-reports.component.html',
  styleUrls: ['./wallet-reports.component.scss'],
})
export class WalletReportsComponent implements OnInit {

  // filter
  startDate: any;
  endDate: any;
  PaymentsMethodList: any = [{ paymentMethod: 'All', _id: 'All' }];
  selectedPaymentsMethod: any = this.PaymentsMethodList[0]['paymentMethod'];
  denominationList: any = [{ label: 'All', value: 'All' }];
  selectedDenomination: any = this.denominationList[0]['value'];
  statusList = [
    { label: 'Successful', value: '1' },
    { label: 'Failed', value: '2' }
  ];
  selectedStatus: any = this.statusList[0]['value'];
  reportTypesList: any = [
    { label: 'Reload', value: 'reload' },
    { label: 'Refund', value: 'refund' },
    { label: 'Expiry', value: 'expiry' },
    { label: 'Breakdown', value: 'breakdown' },
  ];
  selectedReportType: any = this.reportTypesList[0]['value'];
  // totalWalletBalance: any = 0;
  selectedReportTypeResponse: any = this.selectedReportType;
  // data
  tableColumns: any = this.setReportField();
  displayedColumns = this.tableColumns;
  editColumns = [];
  dataSource = [];
  isLoading = false;
  isFilter = false;
  constructor(public dialog: MatDialog, private walletService: WalletService) { }

  ngOnInit(): void {
    this.tableColumns.map((obj) => {
      this.editColumns.push({ label: obj, isSelected: true });
    });
    this.getPaymentsMethodList();
    this.getUserManagementWalletReport();
  }

  getPaymentsMethodList() {
    this.isLoading = true;
    this.walletService.getPaymentsMethodList().subscribe(
      (res) => {
        this.isLoading = false;
        this.PaymentsMethodList = [...this.PaymentsMethodList, ...res.data];
      },
      (err) => {
        this.isLoading = false;
      }
    );
  }

  getUserManagementWalletReport() {
    this.isLoading = true;
    const param: any = {};
    this.selectedStatus ? (param.status = this.selectedStatus) : '';
    this.selectedPaymentsMethod
      ? (param.paymentType = this.selectedPaymentsMethod)
      : '';
    this.selectedReportType
      ? (param.type = this.selectedReportType)
      : '';
    this.startDate ? (param.startDate = this.startDate) : '';
    this.endDate ? (param.endDate = this.endDate) : '';
    this.walletService.getUserManagementWalletReport(param).subscribe(
      (res) => {
        this.selectedReportType
          ? (this.selectedReportTypeResponse = this.selectedReportType)
          : '';
        this.isLoading = false;
        let dataSource = res.data;
        this.tableColumns = this.setReportField();
        this.displayedColumns = this.tableColumns;
        let temp = [];
        dataSource.map((obj, i) => {
          let object = new Object();
          for (let key in obj) {
            if (this.displayedColumns.includes(key)) {
              object[key] = obj[key];
              // ? obj[key] : ' - ';
            }
          }
          temp.push(object);
        });
        this.dataSource = temp;
        // this.totalWalletBalance = res.data.totalWalletBalance;
      },
      (err) => {
        this.dataSource = []
        this.isLoading = false;
      }
    );
  }
  exportCSV() {
    if (this.dataSource.length > 0) {
      let data = [];
      this.dataSource.map((obj, i) => {
        let object = new Object();
        for (let key in obj) {
          if (this.displayedColumns.includes(key)) {
            object[key] = obj[key];
          }
        }
        data.push(object);
      });
      this.downloadCSVFromJson(`wallet-report.csv`, data)

      // ngxCsv plugin not working properly if data size is large

      // new ngxCsv(data, 'wallet-report', {
      //   fieldSeparator: ',',
      //   quoteStrings: '"',
      //   decimalseparator: '.',
      //   showLabels: true,
      //   headers: this.displayedColumns,
      // });
    }
  }

  downloadCSVFromJson = (filename, arrayOfJson) => {
    // convert JSON to CSV
    const replacer = (key, value) => value === null ? '' : value // specify how you want to handle null values here
    const header = Object.keys(arrayOfJson[0])
    let csv = arrayOfJson.map(row => header.map(fieldName =>
      JSON.stringify(row[fieldName], replacer)).join(','))
    csv.unshift(header.join(','))
    csv = csv.join('\r\n')

    // Create link and download
    var link = document.createElement('a');
    link.setAttribute('href', 'data:text/csv;charset=utf-8,%EF%BB%BF' + encodeURIComponent(csv));
    link.setAttribute('download', filename);
    link.style.visibility = 'hidden';
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  };

  openDialog(): void {
    const dialogRef = this.dialog.open(EditColumnsComponent, {
      width: '600px',
      data: this.editColumns,
      disableClose: true,
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.editColumns = result;
        this.displayedColumns = [];
        this.editColumns.map((obj) => {
          obj.isSelected ? this.displayedColumns.push(obj.label) : '';
        });
      }
    });
  }
  setReportField() {
    switch (this.selectedReportTypeResponse) {
      case this.reportTypesList[0]['value']:
        return [
          'ReloadDateAndTime',
          'ActivityType',
          'CustomerID',
          'SubscriberType',
          'PlanType',
          'UUID',
          'FullName',
          'UserName',
          'MSISDN',
          'ModemID',
          'Email',
          'TransactionID',
          'ReloadMethod',
          'DCBShortCode',
          'OrderID',
          'ItemDescription',
          'GengCoin',
          'GengCoinInRM',
          'ReloadAmount',
          'ReloadStatus',
          'FailureReason',
          'FailureReasonCode',
        ]
      case this.reportTypesList[1]['value']:
        return [
          'RefundDateAndTime',
          'ActivityType',
          'CustomerID',
          'SubscriberType',
          'PlanType',
          'UUID',
          'FullName',
          'UserName',
          'MSISDN',
          'ModemID',
          'Email',
          'OrderID',
          'TransactionID',
          'RefundTransactionID',
          'RefundMethod',
          'ItemDescription',
          'RefundAmountGC',
          'RefundAmountRM',
          'RefundAmountSST',
          'RefundStatus',
          'FailureReason',
          'FailureReasonCode',
        ]
      case this.reportTypesList[2]['value']:
        return [
          'ReportDateAndTime',
          'CustomerID',
          'UUID',
          'FullName',
          'UserName',
          'MSISDN',
          'ModemID',
          'Email',
          'GengCoinExpired',
          'GengCoinExpiredRM',
        ]
      case this.reportTypesList[3]['value']:
        return [
          'DateAndTime',
          'CustomerID',
          'UUID',
          'FullName',
          'UserName',
          'MSISDN',
          'ModemID',
          'Email',
          'OpeningBalance_WalletBalanceGengCoin',
          'OpeningBalance_WalletBalanceRM',
          'TotalReloadGengCoin',
          'TotalReloadRM',
          'TotalRefundGengCoin',
          'TotalRefundRM',
          'TotalFreebiesRewardsPromo',
          'TotalFrebiesRewardsPromoRM',
          'TotalPurchaseUsageGengCoin',
          'TotalPurchaseUsageRM',
          'TotalExpired',
          'TotalExpiredInRM',
          'ClosingBalance_WalletBalanceGengCoin',
          'ClosingBalance_WalletBalanceRM',
          'DaysToExpiry',
          'ExpiryDate'
        ]
    }
  }
  onChangeReportType() {
    this.selectedStatus = '';
    switch (this.selectedReportType) {
      case this.reportTypesList[0]['value']:
        this.statusList = [
          { label: 'Successful', value: '1' },
          { label: 'Failed', value: '2' }
        ];
        this.selectedStatus = this.statusList[0]['value'];
        break;
      case this.reportTypesList[1]['value']:
        this.statusList = [
          { label: 'Refund Successful', value: '3' },
          { label: 'Refund Failed', value: '4' },
        ]
        this.selectedStatus = this.statusList[0]['value'];
        break;
      default:
        this.statusList = []
        this.selectedStatus = '';
        break;

    }
  }
}
