import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ShopService } from 'projects/maxis-admin/src/app/core/service/shop.service';
import { EditColumnsComponent } from '../../popups/edit-columns/edit-columns.component';
import { ngxCsv } from 'ngx-csv/ngx-csv';

@Component({
  selector: 'app-shop-reports',
  templateUrl: './shop-reports.component.html',
  styleUrls: ['./shop-reports.component.scss'],
})
export class ShopReportsComponent implements OnInit {
  // data
  tableColumns: string[] = [
    'dateTime',
    'activityType',
    'customerID',
    'UUID',
    'fullName',
    'userName',
    'MSISDN',
    'modemID',
    'email',
    'purchaseTransactionId',
    'purchaseMethod',
    'vendorID',
    'vendorName',
    'publisherName',
    'gameTitle',
    'contentType',
    'productID',
    'productSKU',
    'SKUDescription',
    'gamerUsername',
    'zoneID',
    'serverID',
    'CPInGC',
    'CPInRM',
    'SPInGC',
    'SPInRM',
    'promoType',
    'discountAmountInGC',
    'discountedAmountInRM',
    'amountAfterDiscountInGC',
    'amountAfterDiscountInRM',
    'totalFreebiesGrantedRM',
    'totalFreebiesGrantedGC',
    'failureReason',
    'failureReasonCode',
    'purchaseStatus',
  ];
  displayedColumns = this.tableColumns;
  editColumns = [];
  dataSource = [];
  // filter
  startDate: any;
  endDate: any;
  vendorList: any = [{ _id: 'All', name: 'All' }];
  selectedVendor: any = this.vendorList[0]['name'];
  productCategoryList: any = [{ label: 'All', value: 'All' }];
  selectedProductCategory: any = this.productCategoryList[0]['value'];
  gameList: any = [{ label: 'All', value: 'All' }];
  selectedGame: any = this.gameList[0]['value'];
  statusList = [
    { label: 'Successful ', value: '1' },
    { label: 'Failed', value: '2' },
    { label: 'All', value: 'All' },
  ];
  selectedStatus: any = this.statusList[0]['value'];
  isLoading = false;
  constructor(public dialog: MatDialog, private shopService: ShopService) {}

  ngOnInit(): void {
    this.tableColumns.map((obj) => {
      this.editColumns.push({ label: obj, isSelected: true });
    });
    this.getVendorList();
    this.getUserManagementShopReport();
  }

  getVendorList() {
    this.isLoading = true;
    this.shopService.getVendorsList().subscribe(
      (res) => {
        this.isLoading = false;
        this.vendorList = [...this.vendorList, ...res.data];
      },
      (err) => {
        this.isLoading = false;
      }
    );
  }

  getUserManagementShopReport() {
    this.isLoading = true;
    const param: any = {};
    this.selectedVendor ? (param.type = this.selectedVendor) : '';
    this.selectedStatus ? (param.status = this.selectedStatus) : '';
    this.startDate ? (param.startDate = this.startDate) : '';
    this.endDate ? (param.endDate = this.endDate) : '';
    this.shopService.getUserManagementShopReport(param).subscribe(
      (res) => {
        this.isLoading = false;
        let dataSource = res.data;
        let temp = [];
        dataSource.map((obj, i) => {
          let object = new Object();
          for (let key in obj) {
            if (this.displayedColumns.includes(key)) {
              object[key] = obj[key] ? obj[key] : '-';
            }
          }
          temp.push(object);
        });
        this.dataSource = temp;
      },
      (err) => {
        this.isLoading = false;
      }
    );
  }
  exportCSV() {
    let data = [];
    this.dataSource.map((obj, i) => {
      let object = new Object();
      for (let key in obj) {
        if (this.displayedColumns.includes(key)) {
          object[key] = obj[key];
        }
      }
      data.push(object);
    });

    new ngxCsv(data, 'shop-report', {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalseparator: '.',
      showLabels: true,
      headers: this.displayedColumns,
    });
  }
  openDialog(): void {
    const dialogRef = this.dialog.open(EditColumnsComponent, {
      width: '600px',
      data: this.editColumns,
      disableClose: true,
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.editColumns = result;
        this.displayedColumns = [];
        this.editColumns.map((obj) => {
          obj.isSelected ? this.displayedColumns.push(obj.label) : '';
        });
      }
    });
  }
}
