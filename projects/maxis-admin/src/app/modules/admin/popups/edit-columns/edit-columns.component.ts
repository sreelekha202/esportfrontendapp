import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-edit-columns',
  templateUrl: './edit-columns.component.html',
  styleUrls: ['./edit-columns.component.scss'],
})
export class EditColumnsComponent implements OnInit {
  columnList: any = [];
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: EditColumnsComponent,
    public dialogRef: MatDialogRef<EditColumnsComponent>
  ) {}

  ngOnInit(): void {
    this.columnList = this.data;
  }
  onClose(flag = false): void {
    this.dialogRef.close(flag ? this.columnList : false);
  }
}
