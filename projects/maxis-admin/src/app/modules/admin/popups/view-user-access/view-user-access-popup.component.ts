import { Component, Inject, OnInit } from "@angular/core";
import { MAT_DIALOG_DATA } from "@angular/material/dialog";

import {
  AccessManagementUser,
  AccessType,
} from "../../components/access-management/access-management.model";

export interface ViewUserAccessPopupComponentData {
  user: AccessManagementUser;
}

@Component({
  selector: "app-view-user-access",
  templateUrl: "./view-user-access-popup.component.html",
  styleUrls: ["./view-user-access-popup.component.scss"],
})
export class ViewUserAccessPopupComponent implements OnInit {
  AccessType = AccessType;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: ViewUserAccessPopupComponentData
  ) {}

  ngOnInit(): void {}
}
