import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { TermsOfUseComponent } from './terms-of-use.component';
import { I18nModule } from 'esports';
import { environment } from './../../../environments/environment';
const routes: Routes = [
  {
    path: '',
    component: TermsOfUseComponent,
  },
];

@NgModule({
  declarations: [TermsOfUseComponent],
  imports: [
    CommonModule,
    I18nModule.forRoot(environment),
    RouterModule.forChild(routes),
  ],
})
export class TermsOfUseModule {}
