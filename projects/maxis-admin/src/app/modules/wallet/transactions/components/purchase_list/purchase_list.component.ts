import { Component, Input, OnInit } from '@angular/core';
import { Output, EventEmitter } from '@angular/core';
@Component({
  selector: 'app-purchase-list',
  templateUrl: './purchase_list.component.html',
  styleUrls: ['./purchase_list.component.scss'],
})
export class ListPurchaseComponent implements OnInit {
  sortflag = false;
  @Input() showalldataPurchase;
  showdata: any;
  tableflag: boolean;
  @Output() newItemEvent = new EventEmitter<any>();

  constructor() {}

  ngOnInit(): void {}

  sort() {
    if (this.sortflag == false) {
      this.showalldataPurchase.sort(function (a, b) {
        var dateA = new Date(a.createdAt).getTime(),
          dateB = new Date(b.createdAt).getTime();
        return dateA - dateB; //sort by date ascending
      });
      this.sortflag = true;
    } else {
      this.showalldataPurchase.sort(function (a, b) {
        var dateA = new Date(a.createdAt).getTime(),
          dateB = new Date(b.createdAt).getTime();
        return dateB - dateA; //sort by date ascending
      });
      this.sortflag = false;
    }
  }

  showDetails(data) {
    this.showdata = data;
    this.newItemEvent.emit(data);
  }
}
