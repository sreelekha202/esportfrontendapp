import { Component, Input, OnInit } from '@angular/core';
import { Output, EventEmitter } from '@angular/core';
@Component({
  selector: 'app-reload-list',
  templateUrl: './reload_list.component.html',
  styleUrls: ['./reload_list.component.scss'],
})
export class ListReloadComponent implements OnInit {
  sortflag = false;
  @Input() showalldata;
  showdata: any;
  tableflag: boolean;
  @Output() newItemEvent = new EventEmitter<any>();

  constructor() {}

  ngOnInit(): void {}

  sort() {
    if (this.sortflag == false) {
      this.showalldata.sort(function (a, b) {
        var dateA = new Date(a.createdAt).getTime(),
          dateB = new Date(b.createdAt).getTime();
        return dateA - dateB; //sort by date ascending
      });
      this.sortflag = true;
    } else {
      this.showalldata.sort(function (a, b) {
        var dateA = new Date(a.createdAt).getTime(),
          dateB = new Date(b.createdAt).getTime();
        return dateB - dateA; //sort by date ascending
      });
      this.sortflag = false;
    }
  }

  showDetails(data) {
    this.showdata = data;
    this.newItemEvent.emit(data);
  }
}
