import { Component, OnDestroy, OnInit } from '@angular/core';
import { WalletService } from '../../../core/service/wallet.service';
import { Router } from '@angular/router';
import { environment } from '../../../../environments/environment';
import { ReloadComponent } from '../../reload/reload.component';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';
@AutoUnsubscribe()
@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.scss'],
})
export class TransactionsComponent implements OnInit, OnDestroy {

  isReloadTab: boolean = true;
  isPurchasesTab: boolean = false;
  isLoaded: boolean = false;

  // pageNavigation = [
  //   {
  //     name: 'Balance',
  //     url: '/wallet/home',
  //     //icon: 'assets/wallet/B6EEC02B-00AE-4AD2-A15B-97E69D228B42.svg',
  //     test_id: 'tournament-button-home',
  //   },
  //   {
  //     name: 'Transactions',
  //     url: '/wallet/transactions',
  //     //icon: 'assets/wallet/Icon awesome-list-ul.svg',
  //     test_id: 'tournament-button-discover',
  //   },
  //   {
  //     name: 'Reload Wallet',
  //     url: '/paymentmethods',
  //     // icon: 'assets/icons/navigations/games.svg',
  //     test_id: 'tournament-button-games',
  //   },
  // ];
  // public walletSelectedValue = this.pageNavigation[1].name;

  tableFlag = true;
  showAllData: any = [];
  showAllDataPurchase: any = [];
  totalRecorders = 100;
  showData: any;
  viewReload: boolean = false;
  viewPurchase: boolean = false;
  pageSizeOptions = environment.pageSizeOptions;
  // datafortopupmoney: any = [];
  // datafortopupmethod: any = [];
  walletSubscription: Subscription;

  constructor(
    private walletService: WalletService,
    public matDialog: MatDialog,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.isLoaded = false;
    this.walletSubscription = this.walletService.recordsLoaded.subscribe(
      (data: any) => {
        if (data) {
          let flag = sessionStorage.getItem('flagforpruchase');
          if (flag == '1') {
            sessionStorage.setItem('flagforpruchase', '0');
            this.getPurchaseTransactions(1, 10);
            this.isPurchasesTab = true;
            this.isReloadTab = false;
          } else if(flag == '0') {
            this.getReloadTransactions(1, 10);
          }else{
            this.getPurchaseTransactions(1, 10);
            this.isPurchasesTab = true;
            this.isReloadTab = false;
          }
        }
      }
    );

    // this.walletService.getWallettopupamount().subscribe(
    //   (data: any) => {
    //     this.datafortopupmoney = data.data;
    //     for (let data of this.datafortopupmoney) {
    //       data.selectflag = false;
    //     }
    //   },
    //   (error) => {}
    // );
    // this.walletService.getWallettopupmethods().subscribe(
    //   (data: any) => {
    //     this.datafortopupmethod = data.data;
    //     for (let data of this.datafortopupmethod) {
    //       data.selectflag = false;
    //     }
    //   },
    //   (error) => {}
    // );
  }
  navigateTo(value) {
    if (value == 'Transactions') {
      this.router.navigate(['../', 'transactions']);
    }
    if (value == 'Balance') {
      this.router.navigate(['../', 'wallet']);
    }
  }

  checkScreen() {
    if (this.viewReload == true) {
      this.tableFlag = true;
      this.viewReload = false;
    }
    if (this.viewPurchase == true) {
      this.tableFlag = true;
      this.viewPurchase = false;
    }
  }

  addItem(newItem: any) {
    this.showdetailfun(newItem);
  }

  showdetailfun(data) {
    window.scroll(0, 0);
    this.showData = data;
    this.tableFlag = false;
    this.viewReload = true;
    this.viewPurchase = false;
    this.setTransactionType();
  }

  setTransactionType() {
    let tt = '';
    if (this.showData.paymentMethod == 'Online banking') {
      tt = 'Reload';
    } else if (this.showData.paymentMethod == 'Credit/Debit card') {
      tt = 'Reload';
    } else if (this.showData.paymentMethod == 'DCB') {
      tt = 'Reload';
    } else if (this.showData.paymentMethod == 'Cashback') {
      tt = 'Cashback';
    } else {
      tt = 'Purchase';
    }
    this.showData['transactionType'] = tt;
  }

  addItem1(newItem: any) {
    this.showdetailfun1(newItem);
  }

  showdetailfun1(data) {
    window.scroll(0, 0);
    this.showData = data;
    this.tableFlag = false;
    this.viewPurchase = true;
    this.viewReload = false;
    this.setTransactionType();
  }

  back() {
    this.tableFlag = true;
    this.viewPurchase = !this.viewPurchase;
  }

  onPageChange(event): void {
    this.getReloadTransactions(event.pageIndex + 1, event.pageSize);
  }

  getReloadTransactions(pagenum, pagesize) {
    this.isReloadTab = true;
    this.isPurchasesTab = false;
    sessionStorage.setItem('flagforpruchase', '0');
    this.walletService
      .gettransactionswithpagination(pagenum, pagesize)
      .subscribe((data: any) => {
        this.showAllData = data.data.docs;
        for (let d of this.showAllData) {
          if (d.paymentMethod == '10' || d.paymentMethod == 'Online banking') {
            d.type = 'Online Banking';
          }
          if (
            d.paymentMethod == '6' ||
            d.paymentMethod == 'Credit/Debit card'
          ) {
            d.type = 'Credit/Debit Card';
          }
          if (d.paymentMethod == 'DCB') {
            d.type = 'Pay with Maxis';
          }
          if (d.paymentMethod == 'Cashback') {
            d.type = 'Cashback';
          }
        }
        this.isLoaded = true;
        this.totalRecorders = data.data.totalDocs;
      });
  }

  onPageChange1(event): void {
    this.getPurchaseTransactions(event.pageIndex + 1, event.pageSize);
  }

  getPurchaseTransactions(pagenum, pagesize) {
    this.isLoaded = false;
    this.isReloadTab = false;
    this.isPurchasesTab = true;
    sessionStorage.setItem('flagforpruchase', '1');
    this.walletService
      .getPurchaseTransactions(pagenum, pagesize)
      .subscribe((data: any) => {
        this.showAllDataPurchase = data.data.docs;
        this.totalRecorders = data.data.totalDocs;
        this.isLoaded = true;
      });
  }
  openReloadPopup() {
    this.matDialog.open(ReloadComponent, {
      disableClose: true,
      panelClass: 'reload_popup',
    });
  }
  ngOnDestroy() {
    if (this.walletSubscription) this.walletSubscription?.unsubscribe();
    sessionStorage.removeItem('flagforpruchase');
  }
}
