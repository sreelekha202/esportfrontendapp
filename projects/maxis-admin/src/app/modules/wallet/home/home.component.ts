import { Component, OnInit } from '@angular/core';
import { WalletService } from '../../../core/service/wallet.service';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { ReloadComponent } from '../../reload/reload.component';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';
@AutoUnsubscribe()
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class WalletHomeComponent implements OnInit {
  pageNavigation = [
    {
      name: 'Balance',
      url: '/wallet/home',
      //icon: 'assets/wallet/B6EEC02B-00AE-4AD2-A15B-97E69D228B42.svg',
      test_id: 'tournament-button-home',
    },
    {
      name: 'Transactions',
      url: '/wallet/transactions',
      //icon: 'assets/wallet/Icon awesome-list-ul.svg',
      test_id: 'tournament-button-discover',
    },
    {
      name: 'Reload Wallet',
      url: '/paymentmethods',
      // icon: 'assets/icons/navigations/games.svg',
      test_id: 'tournament-button-games',
    },
    // {
    //   name: 'header.MATCHES',
    //   url: AppHtmlRoutes.myMatches,
    //   icon: 'assets/icons/navigations/my-matches.svg',
    //   test_id: 'tournament-button-matches',
    // },
    // {
    //   name: 'header.PROFILE',
    //   url: AppHtmlProfileRoutes.basicInfo,
    //   icon: 'assets/icons/navigations/profile.svg',
    //   test_id: "tournament-button-profile"
    // },
  ];
  public walletSelectedValue = this.pageNavigation[0].name;
  isSelected: boolean = false;
  isPaymentmethod: boolean = false;
  firtstimeflag: boolean = false;
  closeResult = '';
  firstflag1 = false;
  secondflag1 = false;
  thridflag1 = false;
  otp1 = '';
  otp2 = '';
  otp3 = '';
  otp4 = '';
  otp5 = '';
  otp6 = '';
  otp7 = '';
  otp8 = '';
  checkbox = false;
  amount = '';
  expdate = '';
  coins = '';
  datafortopupmoney: any = [];
  datafortopupmethod: any = [];
  uidtoken = '';
  amountfordisplay: any;
  coinsfordisplay: any;
  emailfordiaplay: any;
  latesttransactions: any = [];
  profiledata: any;
  type: any = '';
  sortflag = false;
  error_text = '';
  showwalletthings = true;
  error_text1: any = '';
  isLoaded = true;
  walletSubscription: Subscription;
  maxLimit: any=0;

  constructor(
    private walletService: WalletService,
    private router: Router,
    public matDialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.walletService.loadWalletDetails();
    this.walletSubscription = this.walletService.recordsLoaded.subscribe(
      (data: any) => {
        if (data) {
          this.firtstimeflag = true;
          this.amount = data.data.currentBalanceCoins;
          this.expdate = data.data.expiryDate;
          this.coins = data.data.expiryCoins;
          this.maxLimit=data.data.maxLimit;
          if (this.amount == '0') {
            this.firtstimeflag = false;
          }
          this.getLatestTransactions();
        }
      }
    );
    this.walletService.getWallettopupamount().subscribe(
      (data: any) => {
        this.datafortopupmoney = data.data;
        for (let data of this.datafortopupmoney) {
          data.selectflag = false;
        }
      },
      (error) => {}
    );
    this.walletService.getWallettopupmethods().subscribe(
      (data: any) => {
        this.datafortopupmethod = data.data;
        for (let data of this.datafortopupmethod) {
          data.selectflag = false;
        }
      },
      (error) => {}
    );
    // this.getLatestTransactions();
  }
  navigateTo(value) {
    if (value == 'Transactions') {
      this.router.navigate(['../wallet', 'transactions']);
    }
    if (value == 'Balance') {
      this.router.navigate(['../wallet', 'home']);
    }
  }

  getLatestTransactions() {
    this.sortflag = false;
    this.walletService.getlatesttransaction().subscribe((data: any) => {
      this.latesttransactions = data.data;
      for (let d of this.latesttransactions) {
        if (d.paymentMethod == '10' || d.paymentMethod == 'Online banking') {
          d.type = 'Reload';
        } else if (
          d.paymentMethod == '6' ||
          d.paymentMethod == 'Credit/Debit card'
        ) {
          d.type = 'Reload';
        } else if (d.paymentMethod == 'DCB') {
          d.type = 'Reload';
        } else if (d.paymentMethod == 'Cashback') {
          d.type = 'Cashback';
        } else {
          d.type = 'Purchase';
        }
      }
    });
  }

  openReloadPopup() {
    this.matDialog.open(ReloadComponent, {
      disableClose: true,
      panelClass: 'reload_popup',
    });
  }
  ngOnDestroy() {
    if (this.walletSubscription) this.walletSubscription?.unsubscribe();
  }
}
