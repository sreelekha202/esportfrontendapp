import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/modules/shared.module';
import { WalletHomeComponent } from './home/home.component';
import { latestTransactionsComponent } from './home/components/latest-transactions/latest-transactions.component';
import { WalletComponent } from './wallet.component';
import { TransactionsComponent } from './transactions/transactions.component';
import { ListPurchaseComponent } from './transactions/components/purchase_list/purchase_list.component';
import { ViewPurchaseComponent } from './transactions/components/purchase_view/purchase_view.component';
import { ListReloadComponent } from './transactions/components/reload_list/reload_list.component';
import { ViewReloadComponent } from './transactions/components/reload_view/reload_view.component';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: WalletComponent,
    children: [
      {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full',
      },
      {
        path: 'home',
        component: TransactionsComponent,
      },
      {
        path: 'transactions',
        redirectTo: 'home',
        pathMatch: 'full',
      },
      // {
      //   path: 'transactions',
      //   component: TransactionsComponent,
      // },
    ],
  },
];

@NgModule({
  declarations: [
    WalletComponent,
    WalletHomeComponent,
    TransactionsComponent,
    ListPurchaseComponent,
    ViewPurchaseComponent,
    ListReloadComponent,
    ViewReloadComponent,
    latestTransactionsComponent,
  ],
  imports: [CommonModule, SharedModule, RouterModule.forChild(routes)],
})
export class WalletModule {}
