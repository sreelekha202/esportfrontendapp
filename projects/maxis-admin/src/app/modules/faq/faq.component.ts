import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.scss'],
})
export class FaqComponent implements OnInit {
  panelOpenState = false;
  selectedItem = 'Tournaments';
  list_item = [
    // {
    //   title: 'Tournament',
    // },
    // {
    //   title: 'Leaderboards',
    // },
    {
      title: 'Tournaments',
    },
    // {
    //   title: 'Rewards',
    // },
    // {
    //   title: 'Prize Money',
    // },
    // {
    //   title: 'Content',
    // },
    // {
    //   title: 'Community',
    // },
    {
      title: 'Leaderboards',
    },
    {
      title: 'Registration',
    },
    {
      title: 'Rewards',
    },
    {
      title: 'Prize Money',
    },
    {
      title: 'Content',
    },
    {
      title: 'Community',
    },
    {
      title: 'Profile',
    },
    {
      title: 'Shop',
    },
    {
      title: 'Geng Coin Wallet',
    },
    {
      title: 'How to redeems voucher steps',
    },
  ];
  public faqSelectedValue = this.list_item[0].title;
  constructor() {}

  ngOnInit(): void {}
  listClick(event, newValue) {
    this.selectedItem = newValue.title;
  }
  navigateTo(value) {
    this.selectedItem = value;
  }
}
