import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IPagination, EsportsToastService } from 'esports';
import { Subscription } from 'rxjs';
import { HomeService, UserService } from '../../../core/service';
import { TranslateService } from '@ngx-translate/core';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';
@AutoUnsubscribe()
@Component({
  selector: 'app-search-shop',
  templateUrl: './search-shop.component.html',
  styleUrls: ['./search-shop.component.scss'],
})
export class SearchShopComponent implements OnInit {
  isShopFlag = false;
  userSubscription: Subscription;
  currentUser: any;
  constructor(
    private toastService: EsportsToastService,
    private translateService: TranslateService,
    private userService: UserService,
    public activatedRoute: ActivatedRoute,
    public homeService: HomeService,
    private router: Router
  ) {}
  page: IPagination;
  paginationData = {
    page: 1,
    limit: 8,
    sort: '-startDate',
  };
  text;

  ngOnInit(): void {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.currentUser = data;
      }
    });
    this.activatedRoute.queryParamMap.subscribe((e) => {
      if (e.get('text')) {
        this.text = e.get('text');
      }
    });
    this.getSearchArticle();
  }

  getSearchArticle() {
    this.homeService.searchedShop.subscribe((res: any) => {
      if (res?.docs?.length > 0) this.isShopFlag = true;
      this.page = {
        totalItems: res?.totalDocs,
        itemsPerPage: res?.limit,
        maxSize: 5,
      };
    });
  }
  redirectToupOrVoucher(category: any, id) {
    // if (this.currentUser) {
      if (category == 'VOUCHER')
        this.router.navigateByUrl('/home/voucher/' + id);
      if (category == 'TOPUP') this.router.navigateByUrl('/home/topup/' + id);
    // } else {
    //   this.toastService.showInfo(
    //     this.translateService.instant('BASIC_INFO_POPUP.PLEASE_LOGIN')
    //   );
    // }
  }

  pageChanged(page): void {
    this.paginationData.page = page;
    this.homeService.updateSearchParams(this.text, this.paginationData);
    this.homeService.searchArticle();
  }
  ngOnDestroy() {
    if (this.userSubscription) this.userSubscription?.unsubscribe();
  }
}
