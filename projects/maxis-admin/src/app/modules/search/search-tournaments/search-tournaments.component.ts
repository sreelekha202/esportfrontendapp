import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IPagination } from 'esports';
import { HomeService } from '../../../core/service';

@Component({
  selector: 'app-search-tournaments',
  templateUrl: './search-tournaments.component.html',
  styleUrls: ['./search-tournaments.component.scss'],
})
export class SearchTournamentsComponent implements OnInit {
  isTournamentFlag = false;

  constructor(
    public homeService: HomeService,
    public activatedRoute: ActivatedRoute
  ) {}

  page: IPagination;
  paginationData = {
    page: 1,
    limit: 8,
    sort: '-startDate',
  };
  text;

  ngOnInit(): void {
    this.activatedRoute.queryParamMap.subscribe((e) => {
      if (e.get('text')) {
        this.text = e.get('text');
      }
    });
    this.getSearchTournament();
  }

  getSearchTournament() {
    this.homeService.searchedTournament.subscribe((res: any) => {
      if (res?.docs?.length > 0) this.isTournamentFlag = true;
      this.page = {
        totalItems: res?.totalDocs,
        itemsPerPage: res?.limit,
        maxSize: 5,
      };
    });
  }

  pageChanged(page): void {
    this.paginationData.page = page;
    this.homeService.updateSearchParams(this.text, this.paginationData);
    this.homeService.searchTournament();
  }
}
