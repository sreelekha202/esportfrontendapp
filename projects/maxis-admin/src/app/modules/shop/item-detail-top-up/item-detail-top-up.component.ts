import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { ActivatedRoute } from '@angular/router';
import { WalletService } from '../../../core/service/wallet.service';
import { MatDialog } from '@angular/material/dialog';
import { ReloadComponent } from '../../reload/reload.component';
import { Subscription } from 'rxjs/Subscription';
import { EsportsToastService } from 'esports';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { ShopService } from '../../../core/service/shop.service';
import { AppHtmlRoutes } from '../../../app-routing.model';
import { UserService } from '../../../core/service';
import { LoginPopupComponent } from '../components/login-popup/login-popup.component';
declare var $: any;
@Component({
  selector: 'app-item-detail-top-up',
  templateUrl: './item-detail-top-up.component.html',
  styleUrls: ['./item-detail-top-up.component.scss'],
})
export class ItemDetailTopUpComponent implements OnInit {
  @ViewChild('purchaseConfirmationModal')
  AppHtmlRoutes = AppHtmlRoutes;
  public purchaseConfirmationModal: ModalDirective;
  userSubscription: Subscription;
  walletSubscription: Subscription;
  isShowTopUpForm: boolean = true;
  datafortopupmethod: any = [];
  selected: any;
  allDenominationsData: any;
  denomination_coins: any;
  denomination_name: any;
  productId: any;
  denomination_code: any;
  res: any;
  toggleStoreContainer: boolean = false;
  isUserLoggedID: boolean = false;
  isTopUpSelected: boolean = false;
  isInsuficientBalance: boolean = false;
  isPurchaseSuccessful: boolean = false;
  isPurchaseUnSuccessful: boolean = false;
  currentBalance: number = 0;
  newBalance: number = 0;
  selectedTopUp: any = {};
  denominations: any;
  iconUrl;
  purchaseResponse: any;
  isLoaded: boolean = false;
  reloadOptions: any = [];
  @Input() shop: any = {};
  currentUser: any = {};
  reactiveForm: FormGroup;
  isDiscounted = false;
  formInvalid = false;
  isServer = false;
  isZoneid = false;
  isUsername = false;
  isUserid = false;
  servers: any = [];
  sessionTime: number = 180;
  leftTime: number = this.sessionTime;
  isActive = (item) => {
    return this.selected === item;
  };
  formValidMsg: any = '';
  serverName: any;
  purchaseResponseType: string = 'normal';
  isStartTime: boolean = false;
  productVerifyDetails: any;
  isLogin: boolean = false;

  constructor(
    private route: ActivatedRoute,
    public toastService: EsportsToastService,
    private walletService: WalletService,
    private shopService: ShopService,
    private matDialog: MatDialog,
    private userService: UserService,
    private fb: FormBuilder
  ) {
    this.isUserLoggedID = true;
    this.reactiveForm = this.fb.group({
      city: [null],
      userid: [null],
      zoneid: [null],
      serverid: [null],
      username: [null],
      UserEmail: [
        null,
        Validators.compose([Validators.required, Validators.email]),
      ],
      isInsuficientBalance: [null, Validators.compose([Validators.required])],
    });
  }

  ngOnInit(): void {
    this.disableForm();
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.currentUser = data;
        this.isLogin = true;
        this.disableForm();
        this.walletSubscription = this.walletService.recordsLoaded.subscribe(
          (data: any) => {
            if (data) {
              this.currentBalance = data.data.currentBalanceCoins;
              this.newBalance = this.currentBalance;
              this.isInsuficientBalance = this.checkInsuficientBalance();
              if (this.isTopUpSelected && this.selected) {
                this.topupSelect(this.selected);
              }
            }
          }
        );
        this.reactiveForm.patchValue({ UserEmail: this.currentUser.email });
      }
    });
  }
  ngOnDestroy() {
    if (this.userSubscription) this.userSubscription?.unsubscribe();
    if (this.walletSubscription) this.walletSubscription?.unsubscribe();
  }
  disableForm() {
    for (var control in this.reactiveForm.controls) {
      !this.isLogin
        ? this.reactiveForm.controls[control].disable()
        : this.reactiveForm.controls[control].enable();
    }
  }
  async productDetails(productId) {
    this.isLoaded = true;
    this.isLogin = true;
    this.disableForm();
    await this.shopService.productDetails(productId).subscribe(
      (res: any) => {
        this.setProductDetails(res);
      },
      (err) => {
        this.isLoaded = false;
      }
    );
  }

  getProductValidateUser() {
    this.setServerName();
    if (
      this.reactiveForm?.get('userid')?.valid &&
      this.reactiveForm?.get('zoneid')?.valid &&
      this.reactiveForm?.get('serverid')?.valid &&
      this.reactiveForm?.get('username')?.valid
    ) {
      let validateJson: any = { sku: this.productId };
      this.isUserid
        ? (validateJson['userid'] = this.reactiveForm.value.userid)
        : '';
      this.isZoneid
        ? (validateJson['zoneid'] = this.reactiveForm.value.zoneid)
        : '';
      this.isServer
        ? (validateJson['server'] = this.reactiveForm.value.serverid)
        : '';
      this.isUsername
        ? (validateJson['username'] = this.reactiveForm.value.username)
        : '';
      this.shopService.getProductValidateUser(validateJson).subscribe(
        (res: any) => {
          if (res.success) {
            // this.reactiveForm.setErrors(null);
            this.formInvalid = false;
          } else {
            this.reactiveForm.setErrors({ userid: true });
            this.formValidMsg = res.message;
            this.formInvalid = true;
          }
        },
        (err) => {
          this.formInvalid = true;
          this.reactiveForm.setErrors({ userid: true });
          this.formValidMsg = err.error.message;
        }
      );
    }
  }
  setServerName() {
    if (this.isServer) {
      this.servers.map((server) => {
        server.value == this.reactiveForm.value.serverid
          ? (this.serverName = server?.name)
          : '';
      });
    }
  }

  confirmPurchase(): void {
    this.isStartTime = false;
    this.isLoaded = true;
    let data: any = {
      sku: this.productId,
      denominationCode: this.denomination_code,
      ruleCategory: this.denominations?.ruleCategory,
      ruleDetailId: this.denominations?.ruleDetailId,
      ruleId: this.denominations?.ruleId,
      topupEmail: this.reactiveForm.value.UserEmail,
    };
    this.isUserid ? (data['userid'] = this.reactiveForm.value.userid) : '';
    this.isZoneid ? (data['zoneid'] = this.reactiveForm.value.zoneid) : '';
    this.isServer ? (data['server'] = this.reactiveForm.value.serverid) : '';
    this.isUsername
      ? (data['username'] = this.reactiveForm.value.username)
      : '';

    this.shopService.confirmPurchase(data, {}).subscribe(
      (res: any) => {
        this.purchaseResponse = res.data;
        this.purchaseResponse.hasOwnProperty('discountedGC') &&
        this.purchaseResponse.discountedGC > 0
          ? (this.purchaseResponseType = 'promo')
          : 'normal';
        this.purchaseResponse.hasOwnProperty('cashbackInGC')
          ? (this.purchaseResponseType = 'cashback')
          : 'normal';
        this.isPurchaseSuccessful = true;
        this.isPurchaseUnSuccessful = false;
        this.isShowTopUpForm = false;
        this.walletService.loadWalletDetails();
        this.isLoaded = false;
      },
      (error) => {
        if (error.error && error.error.error && error.error.error.message)
          this.toastService.showError(error.error.error.message);
        else this.toastService.showError(error.error.message);
        this.isPurchaseUnSuccessful = true;
        this.isPurchaseSuccessful = false;
        this.isShowTopUpForm = false;
        this.isLoaded = false;
      }
    );
  }

  async ngAfterViewInit(): Promise<void> {
    let productId = this.route.snapshot.paramMap.get('id');
    if (this.currentUser && this.currentUser?._id) {
      this.isLogin = true;
      this.disableForm();
      if (productId) {
        await this.productDetails(productId);
      }
      this.walletService.getWallettopupmethods().subscribe(
        (data: any) => {
          this.datafortopupmethod = data.data;
          for (let data of this.datafortopupmethod) {
            data.selectflag = false;
          }
        },
        (error) => {}
      );
    } else {
      this.isLogin = false;
      if (productId) {
        this.anonymousDetails(productId);
      }
    }
  }
  anonymousDetails(productId) {
    this.isLogin = false;
    this.shopService.anonymousDetails(productId).subscribe(
      (data) => {
        this.setProductDetails(data);
      },
      (err) => {
        this.isLoaded = false;
      }
    );
  }
  setProductDetails(res) {
    this.isLoaded = false;
    this.productId = res.data.sku;
    this.res = res.data.name;
    this.allDenominationsData = res.data;
    if (this.allDenominationsData.isServer == true) {
      this.isServer = true;
      this.reactiveForm
        .get('serverid')
        .setValidators(Validators.compose([Validators.required]));
      this.reactiveForm.get('serverid').updateValueAndValidity();
    }
    if (this.allDenominationsData.isUserid == true) {
      this.isUserid = true;
      this.reactiveForm
        .get('userid')
        .setValidators(Validators.compose([Validators.required]));
      this.reactiveForm.get('userid').updateValueAndValidity();
    }
    if (this.allDenominationsData.isZoneid == true) {
      this.isZoneid = true;
      this.reactiveForm
        .get('zoneid')
        .setValidators(Validators.compose([Validators.required]));
      this.reactiveForm.get('zoneid').updateValueAndValidity();
    }
    if (this.allDenominationsData.isUsername == true) {
      this.isUsername = true;
      this.reactiveForm
        .get('username')
        .setValidators(Validators.compose([Validators.required]));
      this.reactiveForm.get('username').updateValueAndValidity();
    }
    this.servers = this.allDenominationsData.serverData;
    this.iconUrl = res.data.iconUrl;
    this.denominations = this.allDenominationsData.denominations;
  }
  topupSelect(topup): void {
    this.isTopUpSelected = true;
    if (topup.isDiscounted == true) {
      this.isDiscounted = true;
    } else {
      this.isDiscounted = false;
    }
    this.selectedTopUp = topup;
    this.denomination_coins = topup.denomination_coins;
    this.denomination_code = topup.denomination_code;
    this.denomination_name = topup.denomination_name;
    this.productId = topup.sku;
    this.newBalance =
      this.currentBalance -
      (topup?.isDiscounted
        ? topup.discountedPriceCoins
        : topup.denomination_coins);
    this.isInsuficientBalance = this.checkInsuficientBalance();
    this.reactiveForm.patchValue({
      isInsuficientBalance: this.isInsuficientBalance ? null : true,
    });
    this.selected = topup;
    this.getProductValidateUser();
    this.userCheck();
  }
  checkInsuficientBalance = (): boolean => {
    return this.newBalance < -1;
  };

  toggleDisplayStore(): void {
    this.toggleStoreContainer = !this.toggleStoreContainer;
  }

  showReloadPopup(): void {
    this.matDialog.open(ReloadComponent, {
      disableClose: true,
      panelClass: 'reload_popup',
    });
  }
  onClose(): void {
    this.matDialog.openDialogs[this.matDialog.openDialogs.length - 1].close();
  }
  openLoginDialog(): void {
    if (!this.isLogin) {
      this.matDialog.open(LoginPopupComponent, {
        disableClose: true,
        panelClass: 'login_popup',
      });
    }
  }
  userCheck() {
    if (this.formInvalid) {
      this.reactiveForm.setErrors({ userid: true });
    }
    this.validateAllFormFields(this.reactiveForm);
  }
  validateAllFormFields(formGroup: FormGroup) {
    //{1}
    Object.keys(formGroup.controls).forEach((field) => {
      //{2}
      const control = formGroup.get(field); //{3}
      if (control instanceof FormControl) {
        //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        //{5}
        this.validateAllFormFields(control); //{6}
      }
    });
  }
  onNext() {
    this.isLoaded = true;

    this.leftTime = this.sessionTime;
    let data: any = {
      sku: this.productId,
      denominationCode: this.denomination_code,
      ruleCategory: this.selectedTopUp?.ruleCategory,
      ruleDetailId: this.selectedTopUp?.ruleDetailId,
      ruleId: this.selectedTopUp?.ruleId,
      topupEmail: this.reactiveForm.value.UserEmail,
    };
    this.isUserid ? (data['userid'] = this.reactiveForm.value.userid) : '';
    this.isZoneid ? (data['zoneid'] = this.reactiveForm.value.zoneid) : '';
    this.isServer ? (data['server'] = this.reactiveForm.value.serverid) : '';
    this.isUsername
      ? (data['username'] = this.reactiveForm.value.username)
      : '';

    this.shopService.confirmPurchase(data, { verify: true }).subscribe(
      (res: any) => {
        this.isLoaded = false;
        this.productVerifyDetails = res;
        $('#purchaseConfirmationModal').modal('show');
        this.isStartTime = true;
      },
      (err) => {
        this.isLoaded = false;
      }
    );
  }
  closePopupOnTimeout(event) {
    if (event) {
      if (event.action == 'done') {
        this.leftTime = this.sessionTime;
        this.isStartTime = false;
        $('#purchaseConfirmationModal').modal('hide');
        $('#purchaseExpireModal').modal('show');
      }
    }
  }
  close() {
    this.isStartTime = false;
    $('#purchaseConfirmationModal').modal('hide');
  }
  closePurchaseExpireModal() {
    this.isStartTime = false;
    this.leftTime = this.sessionTime;
    $('#purchaseExpireModal').modal('hide');
  }
}
