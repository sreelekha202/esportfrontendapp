import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppHtmlRoutes } from '../../../app-routing.model';

@Component({
  selector: 'app-purchase-successful',
  templateUrl: './purchase-successful.component.html',
  styleUrls: ['./purchase-successful.component.scss'],
})
export class PurchaseSuccessfulComponent implements OnInit {
  // @Input()shopItem:ShopItem;
  AppHtmlRoutes = AppHtmlRoutes 
  @Input() shopItem: any;
  @Input() type: string = 'normal';
  constructor(private router: Router) {}

  ngOnInit(): void {
    window.scroll(0, 0);
  }

  navigatefunc() {
    let flag = '1';
    sessionStorage.setItem('flagforpruchase', flag);
    this.router.navigateByUrl('wallet/transactions');
  }
}
