import { Component, Inject, OnInit, PLATFORM_ID } from '@angular/core';
import { EsportsToastService } from 'esports';
import { ActivatedRoute, Router } from '@angular/router';
import { HomeService } from '../../core/service/home.service';
import { MatDialog } from '@angular/material/dialog';
import { AuthServices, UserService } from '../../core/service';
import { UserPopupComponent } from '../../core/header/components/user-popup/user-popup.component';
import { environment } from '../../../environments/environment';
import { ShopService } from '../../core/service/shop.service';
import { TranslateService } from '@ngx-translate/core';
import { isPlatformBrowser } from '@angular/common';
const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;
@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.scss'],
})
export class ShopComponent implements OnInit {
  shop_slider = {
    slidesToShow: 1,
    slidesToScroll: 1,
    centerMode: true,
    dots: true,
    autoplay: true,
    autoplaySpeed: 3000,
    variableWidth: true,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true,
          dots: true,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  };

  shopsList: any = [];
  categoryList: any = [];

  isShowVideo: boolean = false;
  isShowVouchers = false;
  isAllGames = true;
  isDirectPopUp = false;
  isVouchers = false;
  showLoader: boolean = false;
  sortOrderArray: any = [
    { label: 'Featured', value: 0 },
    { label: 'A-Z', value: 1 },
    { label: 'Z-A', value: -1 },
  ];
  sortOrderType = this.sortOrderArray[0]['value'];
  searchText: string = '';
  paginationDetails: any;
  param: any = {
    type: '',
    page: 1,
    pageSize: 20,
    sortBy: 'categoryName',
    sortOrder: this.sortOrderType,
    search: this.searchText,
  };
  slides: any;
  isBrowser: boolean;
  isSpinner: boolean;
  constructor(
    @Inject(PLATFORM_ID) private platformId,
    private homeService: HomeService,
    private shopService: ShopService,
    private userService: UserService,
    public toastService: EsportsToastService,
    private translateService: TranslateService,
    private authService: AuthServices,
    public matDialog: MatDialog,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {
    this.isBrowser = isPlatformBrowser(platformId);
  }
  ngOnInit(): void {
    this.showLoader = true;
    if (this.isBrowser) {
      try {
        this.activatedRoute.queryParams.subscribe((params) => {
          this.showLoader = false;
          if (params.code != null) {
            let auth_code = params['code'];
            this.showLoader = true;
            this.authService.maxis_auth(auth_code).subscribe(
              (authData) => {
                this.showLoader = false;
                let flag = false;
                if (localStorage.hasOwnProperty(environment.currentToken)) {
                  localStorage.removeItem(TOKEN);
                  localStorage.removeItem('refreshToken');
                  flag=true;
                }
                localStorage.setItem(
                  environment.currentToken,
                  authData.data.token
                );
                localStorage.setItem(
                  'refreshToken',
                  authData.data.refreshToken
                );
                this.router.navigate([], {
                  queryParams: {
                    code: null,
                    brand: null,
                    isPostPre: null,
                  },
                  queryParamsHandling: 'merge',
                });
                // flag?window.location.reload():'';
                this.userService.getAuthenticatedUser(API, TOKEN);
                if (authData.data.firstLogin == 1) {
                  this.matDialog.open(UserPopupComponent, {
                    panelClass: 'user_popup_main',
                    disableClose: true,
                  });
                }
              },
              (err) => {
                console.log(JSON.stringify(err));
                this.toastService.showError(err?.error?.message);
                localStorage.removeItem(TOKEN);
                localStorage.removeItem('refreshToken');
                window.location.replace(`${environment.maxisLogout}`);
                this.showLoader = false;
              }
            );
          } else if (params.token != null) {
            let mstoken = params['token'];
            this.showLoader = true;
            this.authService.maxis_authMsToken(mstoken).subscribe(
              (authData) => {
                let flag = false;
                if (localStorage.hasOwnProperty(environment.currentToken)) {
                  localStorage.removeItem(TOKEN);
                  localStorage.removeItem('refreshToken');
                  flag=true;
                }
                this.showLoader = false;
                localStorage.setItem(
                  environment.currentToken,
                  authData.data.token
                );
                localStorage.setItem(
                  'refreshToken',
                  authData.data.refreshToken
                );
                this.router.navigate([], {
                  queryParams: {
                    token: null,
                    channel: null,
                    langcode: null,
                  },
                  queryParamsHandling: 'merge',
                });
                // flag?window.location.reload():'';
                this.userService.getAuthenticatedUser(API, TOKEN);
                if (authData.data.firstLogin == 1) {
                  this.matDialog.open(UserPopupComponent, {
                    panelClass: 'user_popup_main',
                    disableClose: true,
                  });
                }
              },
              (err) => {
                console.log(JSON.stringify(err));
                this.toastService.showError(err?.error?.message);
                localStorage.removeItem(TOKEN);
                localStorage.removeItem('refreshToken');
                window.location.replace(`${environment.maxisLogout}`);
                this.showLoader = false;
              }
            );
          }
        });
        this.showLoader = true;
        setTimeout(() => {
          this.showAllGames();
          this.getProductCategoryGroups();
          this.homeService._getBanner().subscribe(
            (res) => {
              this.showLoader = false;
              this.slides = res.data;
            },
            (err) => {
              this.showLoader = false;
            }
          );
        }, 2000);
      } catch (error) {}
    }
  }

  getProductCategoryGroups() {
    this.showLoader = true;
    this.shopService.getProductCategoryGroups().subscribe(
      (res) => {
        this.showLoader = false;
        this.categoryList = res.data;
      },
      (err) => {
        this.showLoader = false;
      }
    );
  }

  onFilter = () => {
    this.shopsList = [];
    this.param.page = 1;
    !this.isSpinner ? this.getProductsByCategory() : '';
  };

  getProductsByCategory() {
    this.isSpinner = true;
    setTimeout(() => {
      this.param.search = this.searchText;
      this.param.sortOrder = this.sortOrderType;
      this.sortOrderType == 0
        ? delete this.param.sortOrder
        : this.sortOrderType;
      this.shopService.getProductsByCategory(this.param).subscribe(
        (res) => {
          this.isSpinner = false;
          this.showLoader = false;
          this.shopsList = this.shopsList.concat(res.data.docs);
          this.paginationDetails = res.data;
        },
        (err) => {
          this.isSpinner = false;
          this.showLoader = false;
        }
      );
    }, 500);
  }

  showArticles() {
    this.isShowVideo = false;
    this.isShowVouchers = false;
  }

  showVideos() {
    this.isShowVideo = true;
  }

  showAllGames() {
    this.isAllGames = true;
    this.isDirectPopUp = false;
    this.isVouchers = false;
    this.showLoader = true;
    this.param.type = '';
    this.shopsList = [];
    this.param.page = 1;
    this.getProductsByCategory();
  }
  //
  showDirectPopUp() {
    this.isAllGames = false;
    this.isDirectPopUp = true;
    this.isVouchers = false;
    this.showLoader = true;
    this.param.type = 'topup';
    this.shopsList = [];
    this.param.page = 1;
    this.getProductsByCategory();
  }

  showVouchers() {
    this.isAllGames = false;
    this.isDirectPopUp = false;
    this.isVouchers = true;
    this.showLoader = true;
    this.param.type = 'voucher';
    this.shopsList = [];
    this.param.page = 1;
    this.getProductsByCategory();
  }

  pageChanged(page): void {
    if (this.paginationDetails?.totalDocs > 0) {
      this.param.page = page.pageIndex + 1;
      this.param.pageSize = page.pageSize;
      this.getProductsByCategory();
    }
  }

  onScroll() {
    if (this.paginationDetails?.hasNextPage) {
      this.isSpinner = true;
      this.param.page++;
      this.getProductsByCategory();
    }
  }

  redirectToupOrVoucher(category: any, id) {
    if (category == 'VOUCHER') this.router.navigateByUrl('/home/voucher/' + id);
    if (category == 'TOPUP') this.router.navigateByUrl('/home/topup/' + id);
  }
  ngOnDestroy() {}
}
