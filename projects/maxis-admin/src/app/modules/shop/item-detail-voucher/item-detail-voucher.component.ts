import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { FormGroup, Validators } from '@angular/forms';
import { WalletService } from '../../../core/service/wallet.service';
import { MatDialog } from '@angular/material/dialog';
import { ReloadComponent } from '../../reload/reload.component';
import { Subscription } from 'rxjs/Subscription';
import { EsportsToastService } from 'esports';
import { ShopService } from '../../../core/service/shop.service';
import { AppHtmlRoutes } from '../../../app-routing.model';
import { UserService } from '../../../core/service';
import { LoginPopupComponent } from '../components/login-popup/login-popup.component';
declare var $: any;

@Component({
  selector: 'app-item-detail-voucher',
  templateUrl: './item-detail-voucher.component.html',
  styleUrls: ['./item-detail-voucher.component.scss'],
})
export class ItemDetailVoucherComponent implements OnInit {
  isShowVoucherForm: boolean = true;
  userSubscription: Subscription;
  walletSubscription: Subscription;
  AppHtmlRoutes = AppHtmlRoutes;
  @ViewChild('purchaseConfirmationModal')
  public purchaseConfirmationModal: ModalDirective;
  shopItem: any = {
    UserID: '',
    ZoneID: '',
    TopUp: {
      Amount: 230,
      DisplayText: '',
    },
    UserEmail: '',
    CurrentBalance: 1000000,
  };
  list: any;
  selected: any;
  allDenominationsData: any;
  denomination_coins: any;
  denomination_name: any;
  productId: any;
  discription: any;
  denomination_code: any;
  isPopup: boolean = false;
  isError: boolean = false;
  islow: boolean = false;
  isEmailEmpty: string;
  toggleStoreContainer: boolean = false;
  isUserLoggedID: boolean = false;
  isVoucherSelected: boolean = false;
  isInsuficientBalance: boolean = true;
  isPurchaseSuccessful: boolean = false;
  isPurchaseUnSuccessful: boolean = false;
  currentBalance: number = 0;
  newBalance: number = 0;
  selectedVoucher: any = {};
  denominations: any;
  iconUrl;
  purchaseResponse: any;
  isLoaded: boolean = false;
  reactiveForm: FormGroup;

  vouchers: any = [
    { uc: 60, gc: 140 },
    { uc: 325, gc: 700 },
    { uc: 660, gc: 1400 },
    { uc: 1800, gc: 3500 },
  ];
  reloadOptions: any = [
    { rm: 5, gc: 500 },
    { rm: 10, gc: 1000 },
    { rm: 25, gc: 2500 },
    { rm: 50, gc: 5000 },
    { rm: 75, gc: 7500 },
    { rm: 100, gc: 10000 },
    { rm: 150, gc: 15000 },
    { rm: 200, gc: 20000 },
    { rm: 250, gc: 25000 },
  ];

  @Input() shop: any = {};
  currentUser: any = {};
  productVerifyDetails: any;
  isLogin: boolean = false;

  constructor(
    private route: ActivatedRoute,
    public toastService: EsportsToastService,
    private walletService: WalletService,
    private shopService: ShopService,
    private formBuilder: FormBuilder,
    private matDialog: MatDialog,
    private userService: UserService
  ) {
    this.reactiveForm = this.formBuilder.group({
      UserEmail: [null, Validators.required],
      isInsuficientBalance: [null, Validators.required],
    });
    this.isUserLoggedID = true;
    this.newBalance = this.currentBalance;
    this.isInsuficientBalance = this.checkInsuficientBalance();
  }

  ngOnInit(): void {
    this.disableForm();
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.isLogin = true;
        this.disableForm();
        this.currentUser = data;
        this.reactiveForm.patchValue({ UserEmail: this.currentUser.email });
      }
    });

    this.walletSubscription = this.walletService.recordsLoaded.subscribe(
      (data: any) => {
        if (data) {
          this.currentBalance = data.data.currentBalanceCoins;
          this.newBalance = this.currentBalance;
          this.isInsuficientBalance = this.checkInsuficientBalance();
        }
      }
    );
  }

  disableForm() {
    for (var control in this.reactiveForm.controls) {
      !this.isLogin
        ? this.reactiveForm.controls[control].disable()
        : this.reactiveForm.controls[control].enable();
    }
  }

  ngOnDestroy() {
    if (this.userSubscription) this.userSubscription?.unsubscribe();
    if (this.walletSubscription) this.walletSubscription?.unsubscribe();
  }
  async ngAfterViewInit(): Promise<void> {
    let productId = this.route.snapshot.paramMap.get('id');
    if (this.currentUser && this.currentUser?._id) {
      this.productDetails(productId);
    } else {
      this.isLogin = false;
      if (productId) {
        this.anonymousDetails(productId);
      }
    }
  }
  anonymousDetails(productId) {
    this.isLogin = false;
    this.shopService.anonymousDetails(productId).subscribe(
      (data) => {
        this.isLoaded = true;
        this.setProductDetails(data);
      },
      (err) => {
        this.isLoaded = true;
      }
    );
  }
  setProductDetails(res: any) {
    this.isLoaded = true;
    this.allDenominationsData = res.data;
    this.iconUrl = res.data.iconUrl;
    this.denominations = this.allDenominationsData.denominations;
  }
  checkInsuficientBalance(): boolean {
    return this.newBalance < -1;
  }

  voucherSelect(voucher): void {
    this.isVoucherSelected = true;
    this.selectedVoucher = voucher;
    this.denomination_coins = voucher.denomination_coins;
    this.denomination_code = voucher.denomination_code;
    this.denomination_name = voucher.denomination_name;
    this.newBalance = this.currentBalance - voucher.denomination_coins;
    this.isInsuficientBalance = this.checkInsuficientBalance();
    this.reactiveForm.patchValue({
      isInsuficientBalance: this.isInsuficientBalance ? null : true,
    });
    this.selected = voucher;
  }
  isActive(item) {
    return this.selected === item;
  }
  confirmPurchase(voucherEmail): void {
    this.isPopup = false;
    this.islow = true;
    let data = {
      sku: this.productId,
      denominationCode: this.denomination_code,
      voucherEmail: voucherEmail,
      ruleCategory: this.selectedVoucher?.ruleCategory,
      ruleDetailId: this.selectedVoucher?.ruleDetailId,
      ruleId: this.selectedVoucher?.ruleId,
    };
    this.shopService.confirmPurchase(data, {}).subscribe(
      (res: any) => {
        this.purchaseResponse = res.data;
        this.isPurchaseSuccessful = true;
        this.isShowVoucherForm = false;
        this.walletService.loadWalletDetails();
      },
      (error) => {
        if (error.error && error.error.error && error.error.error.message)
          this.toastService.showError(error.error.error.message);
        else this.toastService.showError(error.error.message);
        this.isPurchaseUnSuccessful = true;
        this.isShowVoucherForm = false;
      }
    );
  }
  openLoginDialog(): void {
    if (!this.isLogin) {
      this.matDialog.open(LoginPopupComponent, {
        disableClose: true,
        panelClass: 'login_popup',
      });
    }
  }
  toggleDisplayStore(): void {
    this.toggleStoreContainer = !this.toggleStoreContainer;
  }

  productDetails(productId) {
    this.productId = productId;
    this.shopService.productDetails(productId).subscribe(
      (res: any) => {
        this.setProductDetails(res);
      },
      (err) => {
        this.isLoaded = true;
      }
    );
  }

  byNow() {
    this.isLoaded = false;
    this.isPopup = true;

    let data = {
      sku: this.productId,
      denominationCode: this.denomination_code,
      voucherEmail: this.reactiveForm.value.UserEmail,
      ruleCategory: this.selectedVoucher?.ruleCategory,
      ruleDetailId: this.selectedVoucher?.ruleDetailId,
      ruleId: this.selectedVoucher?.ruleId,
    };
    this.shopService.confirmPurchase(data, { verify: true }).subscribe(
      (res: any) => {
        this.productVerifyDetails = res;
        this.isLoaded = true;
        $('#purchaseConfirmationModal').modal('show');
      },
      (err) => {
        this.isLoaded = true;
      }
    );
    this.shopItem.UserID = this.allDenominationsData.vendorId._id;
    this.shopItem.TopUp.DisplayText = this.denomination_name;
    this.shopItem.TopUp.Amount = this.denomination_coins;
    this.shopItem.UserEmail = this.reactiveForm.value.UserEmail;
  }

  showReloadPopup(): void {
    this.matDialog.open(ReloadComponent, {
      disableClose: true,
      panelClass: 'reload_popup',
    });
  }
  onClose(): void {
    this.matDialog.openDialogs[this.matDialog.openDialogs.length - 1].close();
  }
}
