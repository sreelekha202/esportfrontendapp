import { Component, OnInit } from '@angular/core';
import { AppHtmlRoutes } from '../../../../app-routing.model';

@Component({
  selector: 'app-login-popup',
  templateUrl: './login-popup.component.html',
  styleUrls: ['./login-popup.component.scss'],
})
export class LoginPopupComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;
  constructor() {}

  ngOnInit(): void {}
}
