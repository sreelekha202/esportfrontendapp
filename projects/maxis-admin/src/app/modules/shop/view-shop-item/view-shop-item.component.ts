import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { ActivatedRoute } from '@angular/router';
import { AppHtmlRoutes } from '../../../app-routing.model';
import { ShopService } from '../../../core/service/shop.service';

@Component({
  selector: 'app-view-shop-item',
  templateUrl: './view-shop-item.component.html',
  styleUrls: ['./view-shop-item.component.scss'],
})
export class ViewShopItemComponent implements OnInit {
  @ViewChild('purchaseConfirmationModal')
  AppHtmlRoutes = AppHtmlRoutes;
  public purchaseConfirmationModal: ModalDirective;
  shopItem: any = {
    UserID: '351247326',
    ZoneID: '9496',
    TopUp: {
      Amount: 230,
      DisplayText: '107 Diamonds + 11 Bonus',
    },
    UserEmail: 'sitikus96@gmail.com',
    CurrentBalance: 1000000,
  };
  toggleStoreContainer: boolean = false;
  isUserLoggedID: boolean = false;
  isTopUpSelected: boolean = false;
  isInsuficientBalance: boolean;
  isPurchaseSuccessful: boolean = false;
  isPurchaseUnSuccessful: boolean = false;
  currentBalance: number = 0;
  newBalance: number = 0;
  selectedTopUp: any = { amount: 0, diamonds: 0, bonus: 0 };
  topUPS: any = [
    { amount: 100, diamonds: 36, bonus: 4 },
    { prevamount: 288, amount: 230, diamonds: 107, bonus: 11 },
    { amount: 479, diamonds: 179, bonus: 18 },
    { amount: 959, diamonds: 334, bonus: 43 },
    { amount: 1918, diamonds: 690, bonus: 107 },
    { prevamount: 2877, amount: 2302, diamonds: 1000, bonus: 177 },
    { prevamount: 4794, amount: 3835, diamonds: 1812, bonus: 344 },
    { prevamount: 7671, amount: 6137, diamonds: 2685, bonus: 537 },
    { prevamount: 9589, amount: 7671, diamonds: 3356, bonus: 671 },
  ];
  reloadOptions: any = [
    { rm: 5, gc: 500 },
    { rm: 10, gc: 1000 },
    { rm: 25, gc: 2500 },
    { rm: 50, gc: 5000 },
    { rm: 75, gc: 7500 },
    { rm: 100, gc: 10000 },
    { rm: 150, gc: 15000 },
    { rm: 200, gc: 20000 },
    { rm: 250, gc: 25000 },
  ];
  constructor() {
    this.isUserLoggedID = true;
    this.currentBalance = 101;
    this.newBalance = this.currentBalance;
    this.isInsuficientBalance = this.checkInsuficientBalance();
  }

  ngOnInit(): void {
    // let productId=this.route.snapshot.paramMap.get('id')
    // this.productDetails(productId);
  }

  checkInsuficientBalance(): boolean {
    return this.newBalance <= 0;
  }

  topupSelect(topup: any): void {
    this.isTopUpSelected = true;
    this.selectedTopUp = topup;
    this.newBalance = this.currentBalance - topup.amount;
    this.isInsuficientBalance = this.checkInsuficientBalance();
  }
  confirmPurchase(): void {
    this.isPurchaseSuccessful = true;
    this.purchaseConfirmationModal.hide();
  }

  toggleDisplayStore(): void {
    this.toggleStoreContainer = !this.toggleStoreContainer;
  }
}
