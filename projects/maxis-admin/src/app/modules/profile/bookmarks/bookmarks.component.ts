import { ProfileService } from './../../../core/service/profile.service';
import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { HttpParams } from '@angular/common/http';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';
import { UserService } from '../../../core/service';
import { IUser } from 'esports';
@AutoUnsubscribe()
@Component({
  selector: 'app-bookmarks',
  templateUrl: './bookmarks.component.html',
  styleUrls: ['./bookmarks.component.scss'],
})
export class BookmarksComponent implements OnInit {
  articles = [];
  videos = [];
  active = 1;
  userSubscription: Subscription;
  currentUser: IUser;
  showLoader: boolean = true;
  articleItems = [];

  constructor(
    private userService: UserService,
    private profileService: ProfileService
  ) {}

  ngOnInit(): void {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.currentUser = data;
        this.getBookmarks();
      }
    });
  }
  getBookmarks() {
    this.showLoader = true;

    let params = new HttpParams();
    params = params.append(
      'query',
      JSON.stringify({ userId: this.currentUser._id })
    );
    params = params.append('select', 'articleBookmarks,videoLibrary');

    this.profileService.getBookmarks(params).subscribe(
      (res) => {
        this.videos = res.data[0].videoLibrary;
        this.articles = res.data[0].articleBookmarks;
        this.showLoader = false;
      },
      (err) => {
        this.showLoader = false;
      }
    );
  }
  ngOnDestroy(): void {
    if (this.userSubscription) this.userSubscription.unsubscribe();
  }
}
