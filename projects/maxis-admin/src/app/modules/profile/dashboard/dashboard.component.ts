import { ProfileService } from './../../../core/service/profile.service';
import { Component, OnInit } from '@angular/core';
import { IUser } from 'esports';
import { Subscription } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { SettingsLightboxComponent } from '../../settings-lightbox/settings-lightbox.component';
import { AppHtmlRoutes } from '../../../app-routing.model';
import { UserService } from '../../../core/service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {
  hasDataBg = 'assets/images/Profile/dashboard/has-data-bg.jpg';
  noDataBg = 'assets/images/Profile/dashboard/no-data-bg.jpg';

  matches = [];
  AppHtmlRoutes = AppHtmlRoutes;
  currentUser: IUser;
  userSubscription: Subscription;
  country: any;
  state: any;
  showLoader: boolean;

  constructor(
    private userService: UserService,
    private profileService: ProfileService,
    public matDialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.currentUser = data;
        this.getCountry();
        this.getState();
      }
    });
  }
  getCountry() {
    this.userService.getAllCountries().subscribe((res) => {
      if (res && res.countries) {
        res.countries.filter((obj) => {
          if (obj.id == this.currentUser.country) {
            this.country = obj;
          }
        });
      }
    });
  }
  getState() {
    this.userService.getStates().subscribe((res) => {
      if (res && res.states) {
        res.states.filter((obj) => {
          if (obj.id == this.currentUser.state) {
            this.state = obj;
          }
        });
      }
    });
  }

  isUserContainData(): boolean {
    const {
      profilePicture: picture,
      preference: { followersCount: folowers, followingCount: following },
    } = this.currentUser;

    return picture && folowers && following;
  }

  openSettingsLightbox(selectedTab = 0) {
    const data = { selectedTab };
    this.matDialog.open(SettingsLightboxComponent, {
      data,
      disableClose: true,
      position: {
        top: '34px',
      },
    });
  }

  getMatches() {
    this.showLoader = true;
    const params: any = { page: 1 };
    params.status = 'completed';
    this.profileService.getMatches(params).subscribe(
      (res) => {
        this.matches = res.data;
        this.showLoader = false;
      },
      (err) => {
        this.showLoader = false;
      }
    );
  }
}
