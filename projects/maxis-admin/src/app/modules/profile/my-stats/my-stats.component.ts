import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { IUser } from 'esports';
import { ProfileService } from '../../../core/service/profile.service';
import { UserService } from '../../../core/service';

@Component({
  selector: 'app-my-stats',
  templateUrl: './my-stats.component.html',
  styleUrls: ['./my-stats.component.scss'],
})
export class MyStatsComponent implements OnInit {
  games = [];
  showLoader: boolean = true;
  months = [
    'JANUARY',
    'FEBRUARY',
    'MARCH',
    'APRIL',
    'MAY',
    'JUNE',
    'JULY',
    'AUGUST',
    'SEPTEMBER',
    'OCTOBER',
    'NOVEMBER',
    'DECEMBER',
  ];
  selectedMonth = this.months[new Date().getMonth()];
  selectedYear = new Date().getFullYear();
  statisticsDetails: any;
  userSubscription: Subscription;
  currentUser: IUser;

  constructor(
    private userService: UserService,
    private profileService: ProfileService
  ) {
    // MOCK DATA
    this.games = [{ name: 'valorant' }, { name: 'fortnine' }];
  }

  ngOnInit(): void {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.currentUser = data;
        this.getMyStatistics();
      }
    });
  }

  getMyStatistics() {
    const params: any = {};
    params.month = this.months.indexOf(this.selectedMonth) + 1;
    params.year = this.selectedYear;
    this.profileService.getMyStatistics(this.currentUser._id, params).subscribe(
      (res) => {
        this.statisticsDetails = res.data;
        this.showLoader = false;
      },
      (err) => {
        this.showLoader = false;
      }
    );
  }
  onMonthChange(data) {
    this.selectedMonth = data;
    this.getMyStatistics();
  }

  ngOnDestroy(): void {
    this.userSubscription.unsubscribe();
  }
}
