import { ProfileService } from './../../../core/service/profile.service';
import { Component, OnInit } from '@angular/core';
import { LangChangeEvent, TranslateService } from '@ngx-translate/core';
import { HttpParams } from '@angular/common/http';
import { AppHtmlProfileRoutes } from '../../../app-routing.model';
import { UserService } from '../../../core/service';
@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss'],
})
export class ContentComponent implements OnInit {
  AppHtmlProfileRoutes = AppHtmlProfileRoutes;
  articles = [];
  currentLang: string = 'english';
  articlesPerPage = [5, 10, 15, 20];
  showLoader: boolean = true;
  slicesArticles = [];
  currentUser: any;
  isAdmin: boolean = false;
  sortFlag: boolean;

  constructor(
    private profileService: ProfileService,
    private userService: UserService,
    private translateService: TranslateService
  ) {
    this.currentLang =
      translateService.currentLang == 'ms' ? 'malay' : 'english';
    translateService.onLangChange.subscribe((event: LangChangeEvent) => {
      this.currentLang = event.lang == 'ms' ? 'malay' : 'english';
    });
  }

  ngOnInit(): void {
    this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.currentUser = data;
        this.getContent();
        if (this.currentUser.hasOwnProperty('isAuthor')) {
          this.currentUser.isAuthor == 1 ? (this.isAdmin = true) : '';
        }
      }
    });
  }

  getContent() {
    let params = new HttpParams();
    params = params.append(
      'query',
      JSON.stringify({ author: this.currentUser._id })
    );
    params = params.append('sort', JSON.stringify({ createdDate: -1 }));
    this.profileService.getContent(params).subscribe(
      (res) => {
        this.articles = res.data;
        this.showLoader = false;
        this.slicesArticles = this.articles.slice(0, this.articlesPerPage[0]);
      },
      (err) => {
        this.showLoader = false;
      }
    );
  }

  onPageChange(event): void {
    const startIndex = event.pageIndex * event.pageSize;
    let endIndex = startIndex + event.pageSize;
    if (endIndex > this.articles.length) {
      endIndex = this.articles.length;
    }
    this.slicesArticles = this.articles.slice(startIndex, endIndex);
  }

  sort() {
    if (this.sortFlag == false) {
      this.slicesArticles.sort((a, b) => {
        return (
          new Date(a.createdDate).getTime() - new Date(b.createdDate).getTime()
        ); //sort by date ascending
      });
      this.sortFlag = true;
    } else {
      this.slicesArticles.sort((a, b) => {
        return (
          new Date(b.createdDate).getTime() - new Date(a.createdDate).getTime()
        ); //sort by date ascending
      });
      this.sortFlag = false;
    }
  }
}
