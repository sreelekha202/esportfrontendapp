import { Message } from '@angular/compiler/src/i18n/i18n_ast';
import { Component, Input, OnInit } from '@angular/core';
import { MessageService } from '../../../core/service';
import { IMessage } from '../../../shared/models/message';
import { SelectionModel } from '@angular/cdk/collections';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-inbox',
  templateUrl: './inbox.component.html',
  styleUrls: ['./inbox.component.scss'],
})
export class InboxComponent implements OnInit {
  showLoader: boolean = true;
  messages: Array<any> = [];
  selectedItems = [];

  // messages: Array<any> =  [
  //   {
  //     createdAt: "2021-07-14T08:14:39.311Z",
  //     createdBy: "60ee9cef1d1bfc0008ebe380",
  //     message: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Assumenda quis dolor suscipit perspiciatis inventore? Perferendis, cupiditate et!",
  //     seen: false,
  //     senderDetails: null,
  //     sentBy: "60ee9cef1d1bfc0008ebe380",
  //     status: 1,
  //     subject: "Rewards Alert",
  //     toUser: "60e551952c6c2f00086ad208",
  //     updatedAt: "2021-07-14T08:14:39.311Z",
  //     _id: "60ee9cef1d1bfc0008ebe381"
  //   },
  //   {
  //     createdAt: "2021-07-14T08:14:39.311Z",
  //     createdBy: "60ee9cef1d1bfc0008ebe380",
  //     message: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Assumenda quis dolor suscipit perspiciatis inventore? Perferendis, cupiditate et!",
  //     seen: false,
  //     senderDetails: null,
  //     sentBy: "60ee9cef1d1bfc0008ebe380",
  //     status: 1,
  //     subject: "Rewards Alert",
  //     toUser: "60e551952c6c2f00086ad208",
  //     updatedAt: "2021-07-14T08:14:39.311Z",
  //     _id: "60ee9cef1d1bfc0008ebe381"
  //   },
  //   {
  //     createdAt: "2021-07-14T08:14:39.311Z",
  //     createdBy: "60ee9cef1d1bfc0008ebe380",
  //     message: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Assumenda quis dolor suscipit perspiciatis inventore? Perferendis, cupiditate et!",
  //     seen: false,
  //     senderDetails: null,
  //     sentBy: "60ee9cef1d1bfc0008ebe380",
  //     status: 1,
  //     subject: "Rewards Alert",
  //     toUser: "60e551952c6c2f00086ad208",
  //     updatedAt: "2021-07-14T08:14:39.311Z",
  //     _id: "60ee9cef1d1bfc0008ebe381"
  //   },
  //   {
  //     createdAt: "2021-07-14T08:14:39.311Z",
  //     createdBy: "60ee9cef1d1bfc0008ebe380",
  //     message: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Assumenda quis dolor suscipit perspiciatis inventore? Perferendis, cupiditate et!",
  //     seen: false,
  //     senderDetails: null,
  //     sentBy: "60ee9cef1d1bfc0008ebe380",
  //     status: 1,
  //     subject: "Rewards Alert",
  //     toUser: "60e551952c6c2f00086ad208",
  //     updatedAt: "2021-07-14T08:14:39.311Z",
  //     _id: "60ee9cef1d1bfc0008ebe381"
  //   },
  //   {
  //     createdAt: "2021-07-14T08:14:39.311Z",
  //     createdBy: "60ee9cef1d1bfc0008ebe380",
  //     message: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Assumenda quis dolor suscipit perspiciatis inventore? Perferendis, cupiditate et!",
  //     seen: false,
  //     senderDetails: null,
  //     sentBy: "60ee9cef1d1bfc0008ebe380",
  //     status: 1,
  //     subject: "Rewards Alert",
  //     toUser: "60e551952c6c2f00086ad208",
  //     updatedAt: "2021-07-14T08:14:39.311Z",
  //     _id: "60ee9cef1d1bfc0008ebe381"
  //   },
  //   {
  //     createdAt: "2021-07-14T08:14:39.311Z",
  //     createdBy: "60ee9cef1d1bfc0008ebe380",
  //     message: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Assumenda quis dolor suscipit perspiciatis inventore? Perferendis, cupiditate et!",
  //     seen: false,
  //     senderDetails: null,
  //     sentBy: "60ee9cef1d1bfc0008ebe380",
  //     status: 1,
  //     subject: "Rewards Alert",
  //     toUser: "60e551952c6c2f00086ad208",
  //     updatedAt: "2021-07-14T08:14:39.311Z",
  //     _id: "60ee9cef1d1bfc0008ebe381"
  //   },
  //   {
  //     createdAt: "2021-07-14T08:14:39.311Z",
  //     createdBy: "60ee9cef1d1bfc0008ebe380",
  //     message: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Assumenda quis dolor suscipit perspiciatis inventore? Perferendis, cupiditate et!",
  //     seen: false,
  //     senderDetails: null,
  //     sentBy: "60ee9cef1d1bfc0008ebe380",
  //     status: 1,
  //     subject: "Rewards Alert",
  //     toUser: "60e551952c6c2f00086ad208",
  //     updatedAt: "2021-07-14T08:14:39.311Z",
  //     _id: "60ee9cef1d1bfc0008ebe381"
  //   },
  //   {
  //     createdAt: "2021-07-14T08:14:39.311Z",
  //     createdBy: "60ee9cef1d1bfc0008ebe380",
  //     message: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Assumenda quis dolor suscipit perspiciatis inventore? Perferendis, cupiditate et!",
  //     seen: false,
  //     senderDetails: null,
  //     sentBy: "60ee9cef1d1bfc0008ebe380",
  //     status: 1,
  //     subject: "Rewards Alert",
  //     toUser: "60e551952c6c2f00086ad208",
  //     updatedAt: "2021-07-14T08:14:39.311Z",
  //     _id: "60ee9cef1d1bfc0008ebe381"
  //   },
  //   {
  //     createdAt: "2021-07-14T08:14:39.311Z",
  //     createdBy: "60ee9cef1d1bfc0008ebe380",
  //     message: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Assumenda quis dolor suscipit perspiciatis inventore? Perferendis, cupiditate et!",
  //     seen: false,
  //     senderDetails: null,
  //     sentBy: "60ee9cef1d1bfc0008ebe380",
  //     status: 1,
  //     subject: "Rewards Alert",
  //     toUser: "60e551952c6c2f00086ad208",
  //     updatedAt: "2021-07-14T08:14:39.311Z",
  //     _id: "60ee9cef1d1bfc0008ebe381"
  //   },
  //   {
  //     createdAt: "2021-07-14T08:14:39.311Z",
  //     createdBy: "60ee9cef1d1bfc0008ebe380",
  //     message: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Assumenda quis dolor suscipit perspiciatis inventore? Perferendis, cupiditate et!",
  //     seen: false,
  //     senderDetails: null,
  //     sentBy: "60ee9cef1d1bfc0008ebe380",
  //     status: 1,
  //     subject: "Rewards Alert",
  //     toUser: "60e551952c6c2f00086ad208",
  //     updatedAt: "2021-07-14T08:14:39.311Z",
  //     _id: "60ee9cef1d1bfc0008ebe381"
  //   }
  // ];

  messagesPerPage = [10, 15, 25];

  slicesMessages = [];

  constructor(private messageService: MessageService,

  ) { }

  ngOnInit(): void {
    this.getMessages();

  }

  getMessages(): void {
    this.messageService.getInboxMessages().subscribe(
      (response) => {
        this.messages = response.data;
        this.slicesMessages = this.messages.slice(0, this.messagesPerPage[0]);
        this.showLoader = false;
        this.messageService.messageCountSubject.next(this.messages.length)
        this.dataSource = new MatTableDataSource(this.messages);
      },
      (error) => {
        this.showLoader = false;
      }
    );
  }
  displayedColumns: string[] = ['select', 'position', 'name'];
  dataSource: any;
  selection = new SelectionModel(true, []);

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.messages.forEach(row => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1}`;
    // const removeIndex = this.slicesMessages.findIndex(item => item.isChecked == true);
    // this.slicesMessages.splice(removeIndex,1)
  }

  onSelectAll(): void { }
  onPageChange(event): void {
    const startIndex = event.pageIndex * event.pageSize;
    let endIndex = startIndex + event.pageSize;
    if (endIndex > this.messages.length) {
      endIndex = this.messages.length;
    }
    this.slicesMessages = this.messages.slice(startIndex, endIndex);
  }
  onSelect(row) {
    this.selectedItems = row;
  }
  //   deletebotton() {
  // const removeIndex = this.slicesMessages.findIndex(row => row.isChecked == true);
  // this.slicesMessages.splice(removeIndex,1)
  //   }
  // removeSelectedRows() {
  //   this.selection.selected.forEach(item => {
  //    let index: number = this.messages.findIndex(d => d === item);
  //    this.dataSource.data.splice(index,1);

  //    this.dataSource = new MatTableDataSource(this.dataSource.data);
  //  });
  //  this.selection = new SelectionModel(true, []);
  // }

  removeSelectedRows(): void {
    this.messageService.deleteMultipleMessage(this.selectedItems).subscribe(
      (response) => {
        this.messages = response.data;
        this.slicesMessages = this.messages.splice(0, this.messagesPerPage[0]);
        this.showLoader = false;
        this.messageService.messageCountSubject.next(this.messages.length)
        this.dataSource = new MatTableDataSource(this.messages);
      },
      (error) => {
        this.showLoader = false;
      }
    );
  }

  // onDeleteChecked(): void {
  //   const selectedMessages = this.messages.filter(
  //     (item) => item.isChecked
  //   );
  //   const selectedMessagesId = selectedMessages.map((item) => item._id);
  //   this.messageService
  //   this.dataSource.data.delete(selectedMessagesId)
  //     .subscribe((response) => {
  //       this.getMessages();
  //     });
  //   this.messages = this.messages.filter((item) => !item.isChecked);
  // }

  // deleteSelectedMessage(delete_confirmation_template: TemplateRef<any>): void {
  //   const selectedCheckbox = this.selectedItems.filter(
  //     (checkbox) => checkbox.checked
  //   );
  //   this.selectedItems = selectedCheckbox.map(
  //     (checkbox) => checkbox.value
  //   );
  //   this.deleteConfirmationMessage =
  //     'Are you sure you want to delete the selected message?';
  //   this.modalRef = this.modalService.show(delete_confirmation_template, {
  //     class: 'modal-sm',
  //   });
  // }

  // deletebotton() {
  //   const i =this.messages.findIndex(messages => messages.id == id);
  //   if(i !== -1){
  //     this.messages.splice(i, 1);

  //   }

  // }

  // checkboxes: boolean[];

  // delete(){
  //   var atleastOneSelected=this.checkboxes.some(checkbox => checkbox == true);
  //   var allselected = this.checkboxes.every(checkbox => checkbox == true);
  //   if(!atleastOneSelected){
  //     return;
  //   }
  //   for(let i=this.messages.length-1; i>=0; i--){
  //     if(this.checkboxes[i]){
  //       this.messages.splice(i, 1);
  //     }
  //   }
  // }

}
