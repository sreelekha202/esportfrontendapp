import { ProfileService } from './../../../core/service/profile.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.scss'],
})
export class TransactionsComponent implements OnInit {
  purchaseHistory: any;
  rewardseHistory: any;
  active = 1;
  nextId: number = 1;
  pagination: any;
  paginationParams: any;
  showLoader: boolean = true;

  // MOCK DATA
  // transactions_mock_data = [
  //   {
  //     date: '9/10/2021',
  //     item: 'Item name here',
  //     orderId: 'RrBp3CgCMV',
  //     tpa: '100.00',
  //     paymentMethod: '100.00',
  //     status: 'successful',
  //   },
  //   {
  //     date: '1/10/2021',
  //     item: 'Item name here',
  //     orderId: 'vXNZfTHdfl',
  //     tpa: '75.00',
  //     paymentMethod: '75.00',
  //     status: 'successful',
  //   },
  //   {
  //     date: '21/9/2021',
  //     item: 'Item name here',
  //     orderId: 'Vo5LifQVlV',
  //     tpa: '100.00',
  //     paymentMethod: '100.00',
  //     status: 'successful',
  //   },
  //   {
  //     date: '20/9/2021',
  //     item: 'Item name here',
  //     orderId: 'owfQRp0mvr',
  //     tpa: '15.00',
  //     paymentMethod: '15.00',
  //     status: 'successful',
  //   },
  //   {
  //     date: '27/8/2021',
  //     item: 'Item name here',
  //     orderId: 'pQdVEU37Nh',
  //     tpa: '25.00',
  //     paymentMethod: '25.00',
  //     status: 'failed',
  //   },
  //   {
  //     date: '3/8/2021',
  //     item: 'Item name here',
  //     orderId: 'H4z4aASmRO',
  //     tpa: '100.00',
  //     paymentMethod: '100.00',
  //     status: 'successful',
  //   },
  //   {
  //     date: '30/7/2021',
  //     item: 'Item name here',
  //     orderId: 'voAV4kwm4q',
  //     tpa: '10.00',
  //     paymentMethod: '10.00',
  //     status: 'failed',
  //   },
  // ];

  constructor(private profileService: ProfileService) { }

  ngOnInit(): void {
    this.getPurchaseHistory();
  }
  getPurchaseHistory() {
    this.showLoader = true;
    const params: any = {};
    this.paginationParams ? params.pagination = this.paginationParams : ''
    this.profileService.getPurchaseHistory(params).subscribe((res) => {
      this.pagination = res.data;
      this.purchaseHistory = (res.data && res.data.docs.length > 0) ? res.data.docs :'';
      this.showLoader = false;
    }, (err) => { this.showLoader = false; })
  }

  getRewardseHistory() {
    this.showLoader = true;
    const params: any = {};
    this.paginationParams ? params.pagination = this.paginationParams : ''
    this.profileService.getRewardseHistory(params).subscribe((res) => {
      this.pagination = res.data;
      this.rewardseHistory = (res.data && res.data.docs.length > 0) ? res.data.docs : '';
      this.showLoader = false;
    }, (err) => { this.showLoader = false; })
  }

  navChanges = (e) => {
    this.nextId = e.nextId;
    switch (this.nextId) {
      case 1: this.getPurchaseHistory(); break;
      case 2: this.getRewardseHistory(); break;
    }
  }

  onPageChanges(event) { this.getPurchaseHistory(); }
}
