import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-bookmarks-table',
  templateUrl: './bookmarks-table.component.html',
  styleUrls: ['./bookmarks-table.component.scss'],
})
export class BookmarksTableComponent implements OnInit {
  @Input() data = [];
  @Input() isVideo: boolean = false;
  itemsPerPage = [3, 6, 10];
  slicesItems = [];

  constructor() {}

  ngOnInit(): void {
    this.slicesItems = this.data.slice(0, this.itemsPerPage[0]);
  }

  onPageChange(event): void {
    const startIndex = event.pageIndex * event.pageSize;
    let endIndex = startIndex + event.pageSize;
    if (endIndex > this.data.length) {
      endIndex = this.data.length;
    }
    this.slicesItems = this.data.slice(startIndex, endIndex);
  }
}
