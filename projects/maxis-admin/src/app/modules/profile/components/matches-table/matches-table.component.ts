import { Component, OnInit, Input, SimpleChanges, ChangeDetectorRef, IterableDiffers } from '@angular/core';

@Component({
  selector: 'app-matches-table',
  templateUrl: './matches-table.component.html',
  styleUrls: ['./matches-table.component.scss'],
})
export class MatchesTableComponent implements OnInit {
  @Input() matches: Array<any> = [];

  matchesPerPage = [2, 5, 10];

  slicesMatches = [];

  constructor(private cdr: ChangeDetectorRef,private _differs: IterableDiffers) { }

  ngOnInit(): void {
    this.slicesMatches = this.matches.slice(0, this.matchesPerPage[0]);
  }

  onPageChange(event): void {
    const startIndex = event.pageIndex * event.pageSize;
    let endIndex = startIndex + event.pageSize;
    if (endIndex > this.matches.length) {
      endIndex = this.matches.length;
    }
    this.slicesMatches = this.matches.slice(startIndex, endIndex);
  }
}
