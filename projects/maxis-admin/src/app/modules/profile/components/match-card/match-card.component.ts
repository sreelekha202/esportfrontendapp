import { Component, OnInit, Input, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';
import { AppHtmlRoutes } from '../../../../app-routing.model';

@Component({
  selector: 'app-match-card',
  templateUrl: './match-card.component.html',
  styleUrls: ['./match-card.component.scss'],
})
export class MatchCardComponent implements OnInit {
  @Input() data: any;
  @Input() isEditable: boolean = false;
  @Input() index: number;
  AppHtmlRoutes = AppHtmlRoutes;
  constructor(private router: Router) { }

  ngOnInit(): void {
    this.data = this.data;
  }

  genrateReport() {
    if (this.data?.tournamentId && this.data?.matchId) {
      this.router.navigate([
        `${AppHtmlRoutes.tournament}/report/${this.data?.matchId}/${this.data?.tournamentId}`,
      ]);
    }
  }
  reportScore(data) {
    localStorage.setItem('t_report', JSON.stringify(data))
    this.router.navigate([`/report-score`]);

  }

  exitFromScoreCard(data) {

  }

  viewMatch() {
    if (this.data?.tournamentSlug) {
      this.router.navigate([
        `${AppHtmlRoutes.tournament}/${this.data?.tournamentSlug}`,
      ]);
    }
  }
}
