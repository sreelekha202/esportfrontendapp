import { Component, OnInit, Input } from '@angular/core';
import { AppHtmlRoutes } from '../../../../app-routing.model';
@Component({
  selector: 'app-tournament-card',
  templateUrl: './tournament-card.component.html',
  styleUrls: ['./tournament-card.component.scss'],
})
export class TournamentCardComponent implements OnInit {
  @Input() data: any;
  @Input() isManage: boolean = false;
  AppHtmlRoutes = AppHtmlRoutes;
  constructor() {}
  ngOnInit(): void {}
  function(data) {
    localStorage.setItem('tournamentDetails', JSON.stringify(data));
  }
}
