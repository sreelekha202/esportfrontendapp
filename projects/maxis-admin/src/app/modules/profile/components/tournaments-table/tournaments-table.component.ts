import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-tournaments-table',
  templateUrl: './tournaments-table.component.html',
  styleUrls: ['./tournaments-table.component.scss'],
})
export class TournamentsTableComponent implements OnInit {
  @Input() tournaments = [];
  @Input() isCardManage: boolean = false;

  tournamentsPerPage = [5, 10, 25];

  slicesTournaments = [];

  constructor() {}

  ngOnInit(): void {
    this.slicesTournaments = this.tournaments.slice(
      0,
      this.tournamentsPerPage[0]
    );
  }

  onPageChange(event): void {
    const startIndex = event.pageIndex * event.pageSize;
    let endIndex = startIndex + event.pageSize;

    if (endIndex > this.tournaments.length) {
      endIndex = this.tournaments.length;
    }

    this.slicesTournaments = this.tournaments.slice(startIndex, endIndex);
  }
}
