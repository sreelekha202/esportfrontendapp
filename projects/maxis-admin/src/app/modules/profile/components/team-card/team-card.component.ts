import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { TeamService } from '../../../../core/service/team.service';
import { AppHtmlRoutes } from '../../../../app-routing.model';
@Component({
  selector: 'app-team-card',
  templateUrl: './team-card.component.html',
  styleUrls: ['./team-card.component.scss'],
})
export class TeamCardComponent implements OnInit {
  @Input() isManage: boolean = false;
  @Input() data;
  myTeams: '/my-teams'[];
  AppHtmlRoutes = AppHtmlRoutes;

  constructor(private router: Router, private teamService: TeamService) {}

  ngOnInit(): void {}

  deleteTeam(team_id) {
    const data = {
      id: team_id,
      admin: true,
      query: {
        condition: { _id: team_id, status: 'active' },
        update: { status: 'inactive' },
      },
    };
    this.teamService
      .deleteTeam(data)
      .then((data: any) => {
        let res = JSON.stringify(data);
        this.router.navigate(['profile/my-teams']);
      })
      .catch((error) => {});
  }
}
