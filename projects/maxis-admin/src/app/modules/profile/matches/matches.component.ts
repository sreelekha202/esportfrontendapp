import { Component, OnInit, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { Subscription } from 'rxjs';
import { UserReportsService } from 'esports';
import { environment } from 'projects/maxis-admin/src/environments/environment';
import { UserService } from '../../../core/service';
const API = environment.apiEndPoint;

@Component({
  selector: 'app-matches',
  templateUrl: './matches.component.html',
  styleUrls: ['./matches.component.scss'],
})
export class MatchesComponent implements OnInit, OnDestroy {
  mock_cards = [];
  isLoaded = false;
  isAdmin = false;
  userSubscription: Subscription;
  active = 1;
  nextId = 1;
  tab = ['current', 'completed', 'past'];

  constructor(
    private cdr: ChangeDetectorRef,
    private userReportsService: UserReportsService,
    private userService: UserService
  ) {}

  ngOnInit(): void {
    this.getCurrentUserDetails();
    this.getMyMatches();
    // this.active = 1;
  }

  ngOnDestroy() {
    this.userSubscription?.unsubscribe();
  }
  //pageChanged = (e) => { this.nextId = e.nextId; this.getMyMatches(); }
  pageChanged = (e) => {
    this.getMyMatches();
  };

  getCurrentUserDetails() {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        if (data?.accountType === 'admin') {
          this.isAdmin = true;
        }
      }
    });
  }

  getMyMatches() {
    this.isLoaded = true;
    this.userReportsService.getMyMatches(API).subscribe(
      (res) => {
        this.mock_cards = [];
        this.isLoaded = false;
        const data = res.data;
        let a = [];
        for (let index = 0, len = data.length; index < len; index++) {
          const element = data[index];
          // element?.match?.currentMatch?.round &&
          // element?.opponentPlayer?.teamName
          a.push({
            title: element?.match?.currentMatch?.round
              ? `Round ${element?.match?.currentMatch?.round}`
              : 'N/A',
            opponentName: element?.opponentPlayer?.teamName || 'N/A',
            gameAccount: element?.opponentPlayer?.inGamerUserId || 'N/A',
            image: element?.match?.tournamentId?.gameDetail?.logo || 'N/A',
            tournamentId: element?.match?.tournamentId?._id || '',
            tournamentSlug: element?.match?.tournamentId?.slug || '',
            matchId: element?.match?._id || '',
            participantId: element?.userParticepentId || '',
            match: element?.match,
            isAdmin: this.isAdmin,
            tournamentName: element?.match?.tournamentId?.name
              ? element?.match?.tournamentId?.name.length > 28
                ? `${element?.match?.tournamentId?.name
                    .toLowerCase()
                    .substring(0, 28)
                    .slice(0, -3)}...`
                : element?.match?.tournamentId?.name.toLowerCase()
              : '',
          });
        }
        this.mock_cards = a;
      },
      (err) => {
        this.isLoaded = false;
      }
    );
  }
}
