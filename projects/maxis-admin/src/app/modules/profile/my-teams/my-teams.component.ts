import { ProfileService } from './../../../core/service/profile.service';
import { Component, OnInit } from '@angular/core';
import { AppHtmlProfileRoutes } from '../../../app-routing.model';

@Component({
  selector: 'app-my-teams',
  templateUrl: './my-teams.component.html',
  styleUrls: ['./my-teams.component.scss'],
})
export class MyTeamsComponent implements OnInit {
  AppHtmlProfileRoutes = AppHtmlProfileRoutes
  active = 1;
  nextId: number = 1;
  myTeams = [];
  teamRequests = [];
  showLoader: boolean = true;
  paginationData = {
    page: 1,
    limit: 10,
    sort: { _id: -1 },
    text: '',
  };
  constructor(private profileService: ProfileService) { }

  ngOnInit(): void {

    this.getMyTeams();
    this.getMyInvite();
    // // MOCK DATA
    // this.myTeams = [
    //   {
    //     image:
    //       'https://i115.fastpic.ru/big/2021/0711/d5/e284e76b3d0de8760861e2d97642e5d5.png',
    //     name: 'Nexplay Empress',
    //     teamsLength: 5,
    //     createtdAt: 1,
    //   },
    //   {
    //     image:
    //       'https://i115.fastpic.ru/big/2021/0711/bc/3c1e95f844d101e292be66deca3b09bc.png',
    //     name: 'Dark Ninja',
    //     teamsLength: 4,
    //     createtdAt: 1,
    //   },
    // ];

    // // MOCK DATA
    // this.teamRequests = [
    //   {
    //     image:
    //       'https://i115.fastpic.ru/big/2021/0711/f1/8bd6a86ccd571aa928cc13958c4173f1.png',
    //     name: 'Paradox',
    //     teamsLength: 5,
    //     createtdAt: 1,
    //   },
    //   {
    //     image:
    //       'https://i115.fastpic.ru/big/2021/0711/bc/3c1e95f844d101e292be66deca3b09bc.png',
    //     name: 'Dark Ninja',
    //     teamsLength: 4,
    //     createtdAt: 2,
    //   },
    // ];
  }
  navChanges = (e) => {
    this.nextId = e.nextId;
    switch (this.nextId) {
      case 1:
        this.getMyTeams();
        break;
      case 2:
        this.getMyInvite();
        break;
    }
  }
  getMyInvite() {
    this.showLoader = true;
    const pagination = JSON.stringify(this.paginationData);
    this.profileService.getMyInvite({
      pagination: pagination,
    }).subscribe((res) => {
      this.teamRequests = []
      for (let d of res.data.docs) {
        let d1 = {
          image: d.logo,
          name: d.teamName,
          teamsLength: 5,
          createtdAt: 1,
          _id: d._id
        }
        this.teamRequests.push(d1)
      }
      this.showLoader = false;
      // this.teamRequests = res.data;
    }, (err) => { this.showLoader = false; })
  }
  getMyTeams() {
    const pagination = JSON.stringify(this.paginationData);
    this.profileService.getMyTeam({
      pagination: pagination,
    }).subscribe((res) => {
      this.myTeams = []
      for (let d of res.data.docs) {
        let d1 = {
          image:
            d.logo,
          name: d.teamName,
          id: d._id,
          teamsLength: 5,
          createtdAt: 1,
          // id: d._id,
        }
        this.myTeams.push(d1)

      }
      this.showLoader = false;
    }, (err) => { this.showLoader = false; })
    // this.showLoader = true;
    // const params: any = {
    //   "page":1,
    //   "limit":100,
    //   "hasNextPage":true
    // };
    // this.profileService.getMyTeams(params).subscribe((res) => {
    //   // this.myTeams = res.data;
    //   this.showLoader = false;
    // }, (err) => { this.showLoader = false; })
  }

  acceptfunction(data) {
    const datatosend = {
      "teamId": data._id,
      "status": "active"
    }
    this.profileService.accept_reject(datatosend).subscribe((data: any) => {
    })
  }
}
