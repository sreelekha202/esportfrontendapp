import { Component, OnInit } from '@angular/core';
import { ProfileService } from './../../../core/service/profile.service';
import { TranslateService } from '@ngx-translate/core';
import {
  EsportsArticleService,
  EsportsLanguageService,
  IUser,
  EsportsUtilsService,
} from 'esports';
import {
  InfoPopupComponent,
  InfoPopupComponentData,
  InfoPopupComponentType,
} from '../../../shared/popups/info-popup/info-popup.component';
import { MatDialog } from '@angular/material/dialog';
import { environment } from '../../../../environments/environment';
import { AppHtmlRoutes } from '../../../app-routing.model';
const API = environment.apiEndPoint;

@Component({
  selector: 'app-news-main',
  templateUrl: './news-main.component.html',
  styleUrls: ['./news-main.component.scss'],
})
export class NewsMainComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;
  allTournaments: any = [];
  newsData: any = [];
  slideConfig = {
    slidesToShow: 1,
    slidesToScroll: 1,
    centerMode: true,
    dots: true,
    autoplay: true,
    autoplaySpeed: 3000,
    variableWidth: true,
    responsive: [
      {
        breakpoint: 1400,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true,
          dots: true,
        },
      },
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true,
          dots: true,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  };

  showLoader: boolean = true;
  user: IUser;
  hottestArticle = [];
  latestArticle = [];

  searchFilterForm = [
    {
      title: 'Select game',
      fields: [{ label: 'Select games', value: 'all' }],
    },
    {
      title: 'Article type',
      fields: [{ label: 'All', value: 'all' }],
    },
  ];
  categoryList: any = [];
  trendingNews = [];
  categoryId: any;

  constructor(
    private articleService: EsportsArticleService,
    public matDialog: MatDialog,
    public language: EsportsLanguageService,
    public translateService: TranslateService,
    private profileService: ProfileService,
    public utilsService: EsportsUtilsService
  ) {}

  ngOnInit(): void {
    this.getArticleNews('');
    this.getHottestArticle();
    // this.getCurrentUserDetails();
    this.getAllCategory();
  }

  getArticleNews(categoryId) {
    const query = JSON.stringify({
      articleStatus: 'publish',
      category: categoryId,
    });
    const option = JSON.stringify({ limit: 7, sort: { createdDate: -1 } });
    this.articleService
      ?.getArticles_PublicAPI(API, { query, option })
      .subscribe(
        (res: any) => {
          this.trendingNews = res?.data;
        },
        (err) => {}
      );
  }
  getAllCategory() {
    this.profileService.getAllCategories().subscribe((res) => {
      if (res?.data && res?.data) {
        let categoryList = res?.data;
        categoryList?.map((cat: any) => {
          if (cat?.name == 'News') {
            this.categoryId = cat?._id;
          }
          this.categoryList?.push({ [cat._id]: cat });
        });

        this.getArticleNews(this.categoryId);
      }
    });
  }

  openSocialShareComponent() {
    const data: InfoPopupComponentData = {
      title: this.translateService.instant('ARTICLE_POST.SHARE'),
      text: ``,
      type: InfoPopupComponentType.socialSharing,
      cancelBtnText: 'Close',
    };

    this.matDialog.open(InfoPopupComponent, {
      data,
    });
  }

  // getRecentPost() {
  //   const pagination = JSON.stringify({ limit: 4, sort: { createdDate: -1 } });
  //   const query = JSON.stringify({
  //     articleStatus: 'publish',
  //   });

  //   const perfernce = JSON.stringify({
  //     prefernce: this.user?.preference?.game
  //       ? this.user?.preference?.game.map((item) => {
  //         return { gameDetails: item };
  //       })
  //       : '',
  //   });

  //   this.articleService
  //     .getLatestArticle(API, {
  //       pagination: pagination,
  //       query: query,
  //       preference: perfernce,
  //     })
  //     .subscribe(
  //       (res: any) => {
  //         if (res && res.data && res.data.docs) {
  //           let latestArticle = res.data.docs;
  //           latestArticle.map((art: any) => {
  //             if (true) {
  //               // art.isFeature
  //               this.latestArticle.push({ ...art, category_name: this.categoryList[art.category] })
  //             }
  //           })
  //         }
  //         this.showLoader = false;
  //       },
  //       (err) => {
  //         this.showLoader = false;
  //       }
  //     );
  // }

  getHottestArticle() {
    this.articleService.getArticleList(API).subscribe(
      (res) => {
        // MOCK HOTTEST ARTICLE TYPE
        this.hottestArticle = res['data'];
        this.showLoader = false;
      },
      (err) => {
        this.showLoader = false;
      }
    );
  }

  // getCurrentUserDetails() {
  //   this.userService.currentUser.subscribe((data) => {
  //     if (data) {
  //       this.user = data;
  //     }
  //   });
  // }

  // Mock Data

  // tournamentsSlides = [
  //   {
  //     bannerFileUrl:
  //       'https://i115.fastpic.ru/big/2021/0706/e5/c625bd1ac866cfba9340ebc800b241e5.jpg',
  //     title: 'Is Valorant finally the ‘IT’ FPS title in Central Europe and Asia-Pacific?',
  //     date: 'Aug 4, 2021',
  //   },
  //   {
  //     bannerFileUrl:
  //       'https://i115.fastpic.ru/big/2021/0706/15/f534b3311fd642a13dc57103b92b5a15.jpg',
  //     title: 'Is Valorant finally the ‘IT’ FPS title in Central Europe and Asia-Pacific?',
  //     date: 'Aug 4, 2021',
  //   },
  // ];

  // VideosData = [
  //   {
  //     videosUrl: '../../../../assets/images/News/Video-1.jpg',
  //     title: 'Geguri – the First Female Pro Signed to an Overwatch League ...',
  //     views: '145K',
  //     hours: '2 hours'
  //   },
  //   {
  //     videosUrl: '../../../../assets/images/News/Video-2.jpg',
  //     title: 'Geguri – the First Female Pro Signed to an Overwatch League ...',
  //     views: '145K',
  //     hours: '2 hours'
  //   },
  //   {
  //     videosUrl: '../../../../assets/images/News/Video-3.jpg',
  //     title: 'Geguri – the First Female Pro Signed to an Overwatch League ...',
  //     views: '145K',
  //     hours: '2 hours'
  //   },
  //   {
  //     videosUrl: '../../../../assets/images/News/Video-4.jpg',
  //     title: 'Geguri – the First Female Pro Signed to an Overwatch League ...',
  //     views: '35K',
  //     hours: '2 mins'
  //   },
  //   {
  //     videosUrl: '../../../../assets/images/News/Video-5.jpg',
  //     title: 'Geguri – the First Female Pro Signed to an Overwatch League ...',
  //     views: '145K',
  //     hours: '2 hours'
  //   },
  //   {
  //     videosUrl: '../../../../assets/images/News/Video-6.jpg',
  //     title: 'Geguri – the First Female Pro Signed to an Overwatch League ...',
  //     views: '15K',
  //     hours: '1 hours'
  //   }
  // ]

  // newsData = [
  //   {
  //     newsImg : "../../../../assets/images/News/latest-news-1.jpg",
  //     newsTitle : "Nintendo of America President Doug Bowser Responds to Reports of New Switch Hardware",
  //     newsDes : "While he didn't give a straight answer, he did shed light on Nintendo's philosophy in regard to advancing technology.",
  //     newsDate : "Aug 1, 2021",
  //     newsType : "Games"
  //   },
  //   {
  //     newsImg : "../../../../assets/images/News/latest-news-2.jpg",
  //     newsTitle : "Bayonetta 3 Is 'Progressing Well' Despite Its No Show at E3 2021, Nintendo Says",
  //     newsDes : "Bayonetta 3 was first announced at The Game Awards 2017.",
  //     newsDate : "Sept 10, 2021",
  //     newsType : "Lifestyle"
  //   },
  //   {
  //     newsImg : "../../../../assets/images/News/latest-news-3.jpg",
  //     newsTitle : "18 New Details We Learned From the Sea of Thieves: A Pirate's Life Showcase",
  //     newsDes : "A Pirate's Life is a love letter to the Pirates of the Caribbean films and the Disneyland attraction.",
  //     newsDate : "Aug 1, 2021",
  //     newsType : "Games"
  //   },
  //   {
  //     newsImg : "../../../../assets/images/News/latest-news-4.jpg",
  //     newsTitle : "Sea of Thieves: A Pirate's Life Preview – E3 2021",
  //     newsDes : "Welcome to the Caribbean, mate.",
  //     newsDate : "Aug 1, 2021",
  //     newsType : "Games"
  //   },
  //   {
  //     newsImg : "../../../../assets/images/News/latest-news-5.jpg",
  //     newsTitle : "Why Adora and the Distance Is a True Father's Day Gift",
  //     newsDes : "Writer Marc Bernardin reveals why Adora and the Distance is both an epic fantasy and a deeply personal tribute to his daughter.",
  //     newsDate : "Aug 1, 2021",
  //     newsType : "Games"
  //   }
  // ]
}
