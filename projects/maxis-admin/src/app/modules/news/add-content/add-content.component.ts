import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EsportsToastService } from 'esports';
import { ProfileService } from '../../../core/service/profile.service';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import {
  AppHtmlProfileRoutes,
  AppHtmlRoutes,
} from '../../../app-routing.model';
import { MatDialog } from '@angular/material/dialog';
import { SaveAsDraftComponentt } from './components/save-as-draft/save-as-draft.component';
import { SubmitForApprovalComponentt } from './components/submit-for-approval/submit-for-approval.component';
import { UserService } from '../../../core/service';
@Component({
  selector: 'app-add-content',
  templateUrl: './add-content.component.html',
  styleUrls: ['./add-content.component.scss'],
})
export class AddContentComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;
  AppHtmlProfileRoutes = AppHtmlProfileRoutes;
  imgurl = '';
  form: FormGroup;
  addForm: FormGroup;
  stateList: any = [];
  categoryList: any = [];
  gameList: any = [];
  tagList: any = [];
  platformList: any = [];
  genreList: any = [];
  articleStatus = ['draft', 'submitted_for_approval'];
  minReadList: any = [
    { _id: 2, name: '2 Minutes' },
    { _id: 3, name: '3 Minutes' },
    { _id: 4, name: '4 Minutes' },
    { _id: 5, name: '5 Minutes' },
    { _id: 6, name: '6 Minutes' },
    { _id: 7, name: '7 Minutes' },
  ];
  showLoader: boolean;
  currentUser: any;
  loader: any;
  editorConfig = {};
  constructor(
    private fb: FormBuilder,
    private profileService: ProfileService,
    private userService: UserService,
    private toastService: EsportsToastService,
    private router: Router,
    public dialog: MatDialog
  ) {
    // this.Editor.execute( 'fontColor', { value: 'rgb(30, 188, 97)' } );
    this.addForm = this.fb.group({
      title: [null, Validators.compose([Validators.required])],
      category: [null, Validators.compose([Validators.required])],
      gameDetails: [null, Validators.compose([Validators.required])],
      minRead: [null],
      tags: [null],
      platform: [null, Validators.compose([Validators.required])],
      genre: [null],
      shortDescription: [
        null,
        Validators.compose([Validators.required, Validators.maxLength(150)]),
      ],
      content: [''],
      image: [null, Validators.compose([Validators.required])],
      author: [null],
      game: [null],
      articleStatus: [''],
      comment: [''],
    });
  }

  ngOnInit(): void {
    this.userService.currentUser.subscribe((userdata) => {
      if (userdata) {
        this.currentUser = userdata;
        this.addForm.patchValue({ author: this.currentUser._id });
      }
    });
    this.getAllCategories();
    this.getAllGenres();
    this.getAllTags();
    this.getAllGame();
    this.editorConfig = {
      editable: true,
      spellcheck: true,
      height: '180px',
      width: 'auto',
      minWidth: '0',
      translate: 'yes',
      enableToolbar: true,
      showToolbar: true,
      defaultParagraphSeparator: '',
      defaultFontName: '',
      defaultFontSize: '',
      fonts: [
        { class: 'arial', name: 'Arial' },
        { class: 'times-new-roman', name: 'Times New Roman' },
        { class: 'calibri', name: 'Calibri' },
        { class: 'comic-sans-ms', name: 'Comic Sans MS' },
      ],

      customClasses: [
        {
          name: 'quote',
          class: 'quote',
        },
        {
          name: 'redText',
          class: 'redText',
        },
        {
          name: 'titleText',
          class: 'titleText',
          tag: 'h1',
        },
      ],
      sanitize: true,
      toolbarPosition: 'top',
      toolbarHiddenButtons: [['bold', 'italic'], ['fontSize']],
    };
  }

  getAllCategories() {
    this.profileService.getAllCategories().subscribe((res) => {
      if (res) this.categoryList = res.data;
    });
  }
  getAllTags() {
    this.profileService.getAllTags().subscribe((res) => {
      if (res) this.tagList = res.data;
    });
  }
  getAllGenres() {
    this.profileService.getAllGenres().subscribe((res) => {
      if (res) this.genreList = res.data;
    });
  }
  getAllGame() {
    this.profileService.getAllGame().subscribe((res) => {
      if (res) this.gameList = res.data;
    });
  }
  onChangeGame() {
    this.gameList.map((res) => {
      if (this.addForm.value.gameDetails == res._id) {
        this.platformList = res.platform;
      }
    });
  }
  onSubmit(type = null) {
    this.addForm.markAllAsTouched();
    if (this.addForm.value.gameDetails) {
      this.gameList.map((g) => {
        if (g._id == this.addForm.value.gameDetails) {
          this.addForm.patchValue({ game: g.name });
        }
      });
    }
    if (type == this.articleStatus[0]) {
      this.addForm.patchValue({ articleStatus: this.articleStatus[0] });
    }
    if (type == this.articleStatus[1]) {
      this.addForm.patchValue({ articleStatus: this.articleStatus[1] });
    }
    if (this.addForm.valid) {
      this.showLoader = true;
      this.addForm.patchValue({
        // image: this.addForm.value.image.data[0].Location,
        shortDescription: { english: this.addForm.value.shortDescription },
        title: { english: String(this.addForm.value.title) },
        comment: { english: String(this.addForm.value.comment) },
        content: { english: String(this.addForm.value.content) },
      });
      this.profileService.saveContent(this.addForm.value).subscribe(
        (res) => {
          this.showLoader = false;
          this.toastService.showSuccess(res.message);
          if (type == this.articleStatus[0]) {
            this.dialog.open(SaveAsDraftComponentt, {
              panelClass: 'custom_dialog',
              disableClose: true,
            });
          }
          if (type == this.articleStatus[1]) {
            this.dialog.open(SubmitForApprovalComponentt, {
              panelClass: 'custom_dialog',
              disableClose: true,
            });
          }
          this.router.navigateByUrl('/profile/content');
        },
        (err) => {
          this.showLoader = false;
        }
      );
    }
  }
}
