import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddContentComponent } from './add-content.component';
import { SaveAsDraftComponentt } from './components/save-as-draft/save-as-draft.component';
import { SubmitForApprovalComponentt } from './components/submit-for-approval/submit-for-approval.component';
import { RouterBackModule } from '../../../shared/directives/router-back.module';
import { SharedModule } from '../../../shared/modules/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { HeaderPaidiaModule } from '../../../shared/components/header-paidia/header-paidia.module';
import { MaterialModule } from '../../../shared/modules/material.module';
import { MatExpansionModule } from '@angular/material/expansion';
import { CoreModule } from '../../../core/core.module';
import { TeamIconComponent } from './components/team-icon/team-icon.component';
import { WYSIWYGEditorModule } from 'esports';
import { environment } from '../../../../environments/environment';
export const routes: Routes = [{
  path: '',
  component: AddContentComponent,
}]
@NgModule({
  declarations: [
    AddContentComponent,
    SaveAsDraftComponentt,
    SubmitForApprovalComponentt,
    TeamIconComponent
  ],
  imports: [
    CommonModule,
    CoreModule,
    RouterBackModule,
    MaterialModule,
    MatExpansionModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    WYSIWYGEditorModule.forRoot(environment),
    RouterModule.forChild(routes),
    HeaderPaidiaModule
  ],
  providers: [ ]
})
export class AddContentModule { }
