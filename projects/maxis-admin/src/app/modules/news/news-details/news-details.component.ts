import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit, Inject } from '@angular/core';
import { DOCUMENT, Location } from '@angular/common';
import { MatDialog } from '@angular/material/dialog';
import { Title } from '@angular/platform-browser';
import { TranslateService } from '@ngx-translate/core';
import { AppHtmlRoutes } from '../../../app-routing.model';
import {
  CommentService,
  HomeService,
  UserPreferenceService,
  UserService,
} from '../../../core/service';
import {
  InfoPopupComponent,
  InfoPopupComponentData,
  InfoPopupComponentType,
} from '../../../shared/popups/info-popup/info-popup.component';
import { IArticle } from '../../../shared/models';
import { toggleHeight } from '../../../animations';
import {
  GlobalUtils,
  EsportsToastService,
  EsportsLanguageService,
  EsportsArticleService,
  IUser,
  EsportsOptionService,
  CustomTranslatePipe,
} from 'esports';
import { environment } from './../../../../environments/environment';
const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;
@Component({
  selector: 'app-news-details',
  templateUrl: './news-details.component.html',
  styleUrls: ['./news-details.component.scss'],
  animations: [toggleHeight],
  providers: [CustomTranslatePipe],
})
export class NewsDetailsComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;
  showLoader = false;
  article: IArticle;
  user: IUser;
  currentUser: IUser;
  relatedArticles = [];
  latestArticle = [];
  highlighted = 'any';
  categoryname: 'any';
  followStatus: any = 'follow';
  hottestArticle = null;
  apiEndPoint = environment.apiEndPoint;
  categoryList: any;
  currentUrl: any;
  comments: any;
  constructor(
    @Inject(DOCUMENT) private document: Document,
    private activatedRoute: ActivatedRoute,
    private articleService: EsportsArticleService,
    private commentService: CommentService,
    private customTranslatePipe: CustomTranslatePipe,
    private globalUtils: GlobalUtils,
    private homeService: HomeService,
    private location: Location,
    private router: Router,
    private titleService: Title,
    private toastService: EsportsToastService,
    private translateService: TranslateService,
    private userService: UserService,
    public languageService: EsportsLanguageService,
    public matDialog: MatDialog,
    public userPreferenceService: UserPreferenceService,
    private optionService: EsportsOptionService
  ) {
    this.commentService?.comments.subscribe((data) => {
      if (data) this.comments = data;
    });
  }

  ngOnInit(): void {
    this.currentUrl = window.location.href;
    this.activatedRoute?.params?.subscribe((params) => {
      if (params?.id) {
        this.getArticleBySlug(params?.id);
      }
    });
    this.getCurrentUserDetails();
    this.getRecentPost();
  }

  onselect() {
    this.highlighted;
  }

  goBack() {
    this.location.back();
  }

  getRecentPost() {
    const pagination = JSON.stringify({ limit: 4, sort: { createdDate: -1 } });
    const query = JSON.stringify({
      articleStatus: 'publish',
    });

    const perfernce = JSON?.stringify({
      prefernce: this.user?.preference?.game
        ? this.user?.preference?.game.map((item) => {
            return { gameDetails: item };
          })
        : '',
    });

    this.articleService
      .getLatestArticle(API, {
        pagination: pagination,
        query: query,
        preference: perfernce,
      })
      .subscribe(
        (res) => {
          this.latestArticle = res['data']['docs'];
          this.showLoader = false;
        },
        (err) => {
          this.showLoader = false;
        }
      );
  }

  getArticleBySlug = async (slug) => {
    try {
      this.showLoader = true;
      const article: any = await this.articleService.getArticleBySlug(
        API,
        slug
      );
      this.article = article?.data;
      this.setcategory();
      if (!this.article) this.router?.navigate(['/404']);
      this.showLoader = false;
      this.setMetaTags();
      this.updateView();
      // this.trendingPost();
      this.showLoader = false;
    } catch (error) {
      this.showLoader = false;
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  getCurrentUserDetails() {
    this.userService?.currentUser.subscribe((data) => {
      if (data) {
        this.user = data;
      }
    });
  }

  setMetaTags() {
    this.titleService.setTitle(
      this.customTranslatePipe.transform(this.article?.title)
    );
    if (this.article?.image) {
      this.globalUtils.setMetaTags([
        {
          property: 'twitter:image',
          content: this.article?.image,
        },
        {
          property: 'og:image',
          content: this.article?.image,
        },
        {
          property: 'og:image:secure_url',
          content: this.article?.image,
        },
        {
          property: 'og:image:url',
          content: this.article?.image,
        },
        {
          property: 'og:image:width',
          content: '1200',
        },
        {
          property: 'og:image:height',
          content: '630',
        },
        {
          name: 'description',
          content: this.customTranslatePipe?.transform(
            this.article?.shortDescription
          ),
        },
        {
          name: 'title',
          content: this?.customTranslatePipe?.transform(this.article?.title),
        },
        {
          property: 'og:description',
          content: this.customTranslatePipe?.transform(
            this.article?.shortDescription
          ),
        },
        {
          property: 'twitter:description',
          content: this.customTranslatePipe?.transform(
            this.article?.shortDescription
          ),
        },
        {
          property: 'og:title',
          content: this.customTranslatePipe?.transform(this.article?.title),
        },
        {
          property: 'twitter:title',
          content: this.customTranslatePipe?.transform(this.article?.title),
        },
        {
          property: 'og:url',
          content:
            this.document?.location?.protocol +
            '//' +
            this.document?.location?.hostname +
            this.router?.url,
        },
      ]);
    }
  }

  updateView = async () => {
    const payload = {
      _id: this.article?._id,
      modalName: 'article',
    };
    await this.homeService?.updateView(payload);
  };

  trendingPost = async () => {
    this.showLoader = true;
    try {
      const date = new Date();
      date.setDate(date?.getDate() - 30);
      const pagination = JSON.stringify({ limit: 8, sort: { views: -1 } });
      const query = JSON.stringify({
        $and: [
          { articleStatus: 'publish' },
          { createdDate: { $gte: date, $lt: new Date() } },
        ],
      });
      const prefernce = JSON.stringify({
        prefernce: this.user?.preference?.game
          ? this.user?.preference?.game.map((item) => {
              return { gameDetails: item };
            })
          : '',
      });
      this.articleService
        .getPaginatedArticles(API, { pagination, query, preference: prefernce })
        .subscribe(
          (res: any) => {
            this.showLoader = false;
            this.relatedArticles = res['data']['docs'];
          },
          (err) => {}
        );
    } catch (error) {
      this.showLoader = false;
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  upsertBookmark = async () => {
    try {
      if (!this.user?._id) {
        this.toastService.showInfo(
          this.translateService.instant('ARTICLE.BOOKMARK_ALERT')
        );
        return;
      }

      if (this.article?.bookmarkProccesing) return;

      // Bookmark field
      const removeBookmark = `articleId=${this.article?._id}`;
      const addBookmark = { articleId: this.article?._id };
      this.article.bookmarkProccesing = true;

      const bookmark: any = this.article?.isBookmarked
        ? await this.userPreferenceService
            .removeBookmark(removeBookmark)
            .toPromise()
        : await this.userPreferenceService.addBookmark(addBookmark).toPromise();
      this.article = {
        ...this.article,
        ...(bookmark?.data?.nModified == 1 && {
          isBookmarked: !this.article?.isBookmarked,
        }),
      };
      this.toastService.showSuccess(bookmark?.message);
      this.article.bookmarkProccesing = false;
    } catch (error) {
      this.article.bookmarkProccesing = false;
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  openSocialShareComponent() {
    const data: InfoPopupComponentData = {
      title: this.translateService.instant('ARTICLE_POST.SHARE'),
      text: ``,
      type: InfoPopupComponentType.socialSharing,
      cancelBtnText: 'Close',
    };

    this.matDialog.open(InfoPopupComponent, {
      data,
    });
  }

  setcategory = async () => {
    const option = await Promise.all([
      this.optionService.fetchAllCategories(API),
    ]);
    this.categoryList = option[0]?.data;
    for (const iterator of this.categoryList) {
      if (iterator._id == this.article?.category) {
        this.categoryname = iterator?.name;
      }
    }
  };
  scroll() {
    if (GlobalUtils.isBrowser()) {
      document.getElementById('comments').scrollIntoView({
        behavior: 'smooth',
      });
    }
  }
}
