import { Component, OnInit } from '@angular/core';
import { IPagination } from '../../../shared/models';
import { EsportsArticleService, EsportsGameService, IUser } from 'esports';
import { environment } from './../../../../environments/environment';
import {
  InfoPopupComponent,
  InfoPopupComponentData,
  InfoPopupComponentType,
} from '../../../shared/popups/info-popup/info-popup.component';
import { ProfileService } from './../../../core/service/profile.service';
import { TranslateService } from '@ngx-translate/core';
import { MatDialog } from '@angular/material/dialog';
import { AppHtmlRoutes } from '../../../app-routing.model';
const API = environment.apiEndPoint;
@Component({
  selector: 'app-news-all',
  templateUrl: './news-all.component.html',
  styleUrls: ['./news-all.component.scss'],
})
export class NewsAllComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;
  hasNextPage;
  gameArray = [];
  gameList = [];
  articles: any = [];
  user: IUser;
  page: IPagination;
  showLoader = false;
  dataLoaded = false;
  filterShow: boolean = false;
  selectedCategory: any = 'all';
  paginationData = {
    page: 1,
    limit: 8,
    sort: '-views',
  };
  sortOrderArray: any = [
    { label: 'Default', value: 0 },
    { label: 'A-Z', value: 1 },
    { label: 'Z-A', value: -1 },
  ];
  sortOrderType = this.sortOrderArray[0]['value'];
  categoryList: any = [];
  constructor(
    private articleApiService: EsportsArticleService,
    private esportsGameService: EsportsGameService,
    private profileService: ProfileService,
    public translateService: TranslateService,
    public matDialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.getGames();
    this.getAllCategory();
    this.getRecentPost('');
  }

  getGames() {
    this.esportsGameService.getGames(API).subscribe((data: any) => {
      this.gameList = data.data;
    });
  }

  getAllCategory() {
    this.profileService.getAllCategories().subscribe((res) => {
      if (res && res?.data) {
        this.categoryList = [{ name: 'All Updates', _id: 'all' }, ...res?.data];
      }
    });
  }
  getRecentPost(id) {
    if (id) {
      let isChecked = id?.target?.checked ? true : false;
      const gameid = { gameDetails: id?.target?.value };

      if (isChecked && id !== '') {
        this.gameArray.push(gameid);
      } else {
        for (let i = 0; i < this.gameArray.length; i++) {
          if (JSON.stringify(this.gameArray[i]) == JSON.stringify(gameid)) {
            this.gameArray.splice(i, 1);
            if (this.gameArray == []) {
              id = '';
            }
          }
        }
      }
    }

    const pagination = JSON.stringify({
      page: this.paginationData.page,
      limit: this.paginationData.limit,
      sort: { createdDate: -1 },
    });

    const query = JSON.stringify({
      articleStatus: 'publish',
      // category: this.selectedCategory != 'all' ? this.selectedCategory : '',
    });
    const perfernce = JSON.stringify({
      preference: this.gameArray,
    });
    this.showLoader = true;
    this.articleApiService
      .getLatestArticle(API, {
        pagination: pagination,
        query: query,
        preference: perfernce,
      })
      .subscribe(
        (res: any) => {
          this.hasNextPage = res.data.hasNextPage ? true : false;
          if (res && res.data && res.data.docs) {
            if (this.articles.length)
              this.articles = [...this.articles, ...res['data']['docs']];
            else this.articles = res['data']['docs'];
          }
          this.showLoader = false;
          this.page = {
            totalItems: res?.data?.totalDocs,
            itemsPerPage: res?.data?.limit,
            maxSize: 5,
          };
        },
        (err) => {
          this.showLoader = false;
        }
      );
  }

  openSocialShareComponent() {
    const data: InfoPopupComponentData = {
      title: this.translateService.instant('ARTICLE_POST.SHARE'),
      text: ``,
      type: InfoPopupComponentType.socialSharing,
      cancelBtnText: 'Close',
    };

    this.matDialog.open(InfoPopupComponent, {
      data,
    });
  }

  LoadMoreData() {
    this.paginationData.page++;
    // this.paginationData.limit = this.paginationData.limit + 2;
    this.getRecentPost('');
  }
  filterShowM() {
    this.filterShow = true;
  }
  filterCloseM() {
    this.filterShow = false;
  }
}
