import { RouterBackModule } from './../../shared/directives/router-back.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ClipboardModule } from '@angular/cdk/clipboard';
import { InlineSVGModule } from 'ng-inline-svg';
import { LazyLoadImageModule } from 'ng-lazyload-image';
import { MaterialModule } from '../../shared/modules/material.module';
import { SettingsLightboxComponent } from './settings-lightbox.component';
import { ProfileComponent } from './profile/profile.component';
import { I18nModule, PipeModule } from 'esports';
import { environment } from '../../../environments/environment';
import { LoadingModule } from '../../core/loading/loading.module';
import { PhonePipe } from '../../shared/pipe/phone.pipe';
@NgModule({
  declarations: [ProfileComponent, SettingsLightboxComponent, PhonePipe],
  imports: [
    ClipboardModule,
    CommonModule,
    FormsModule,
    I18nModule.forRoot(environment),
    InlineSVGModule,
    LazyLoadImageModule,
    MaterialModule,
    ReactiveFormsModule,
    RouterBackModule,
    PipeModule,
    LoadingModule,
  ],
  exports: [SettingsLightboxComponent],
})
export class SettingsLightboxModule {}
