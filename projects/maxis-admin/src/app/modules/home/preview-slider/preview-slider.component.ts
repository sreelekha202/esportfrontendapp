import { Component, OnInit, ViewChild } from '@angular/core';
import { AppHtmlRoutes } from '../../../app-routing.model';
@Component({
  selector: 'app-preview-slider',
  templateUrl: './preview-slider.component.html',
  styleUrls: ['./preview-slider.component.scss'],
})
export class PreviewSliderComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;
  showLoader: boolean = true;
  slideConfig = {
    slidesToShow: 1,
    slidesToScroll: 1,
    centerMode: true,
    dots: true,
    autoplay: true,
    autoplaySpeed: 3000,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true,
          dots: true,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  };
  constructor() {}

  ngOnInit(): void {
    setTimeout(() => {
      this.showLoader = false;
    }, 1000);
  }
}
