import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { EsportsComponent } from './esports.component';
import { EsportschatComponent } from './components/esportschat/esportschat.component';
import { chatComponent } from './components/chat/chat.component';
import { EsportschatwindowComponent } from './components/esportschatwindow/esportschatwindow.component';
import { EsportschatsidebarComponent } from './components/esportschatsidebar/esportschatsidebar.component';
import { EsportsChatService } from './services/esport.chat.service';
import { I18nModule } from './i18n/i18n.module';
import { MatSidenavModule } from '@angular/material/sidenav';
import { FormsModule } from '@angular/forms';
import { EsportsConstantsService } from './services/esport.constants.service';
import { EsportschatlogComponent } from './components/esportschatlog/esportschatlog.component';
import { PipeModule } from './pipes/pipe.module';
import { EsportsSharedModule } from './shared/module/shared.module';

@NgModule({
  declarations: [
    EsportsComponent,
    EsportschatComponent,
    chatComponent,
    EsportschatwindowComponent,
    EsportschatsidebarComponent,
    EsportschatlogComponent,
  ],
  imports: [
    I18nModule,
    CommonModule,
    MatSidenavModule,
    FormsModule,
    PipeModule,
    EsportsSharedModule,
  ],
  exports: [
    EsportsComponent,
    EsportschatComponent,
    chatComponent,
    EsportschatwindowComponent,
    EsportschatsidebarComponent,
    EsportschatlogComponent,
  ],
})
export class EsportsModule {
  public static forRoot(environment: any): ModuleWithProviders<EsportsModule> {
    return {
      ngModule: EsportsModule,
      providers: [
        I18nModule,
        {
          provide: 'env', // you can also use InjectionToken
          useValue: environment,
        },
      ],
    };
  }
}
