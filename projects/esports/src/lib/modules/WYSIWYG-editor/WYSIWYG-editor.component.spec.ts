import {async, ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {WYSIWYGEditorComponent} from './WYSIWYG-editor.component';
import {WYSIWYGEditorToolbarComponent} from './WYSIWYG-editor-toolbar.component';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {AeSelectComponent} from './ae-select/ae-select.component';
import {WYSIWYGEditorModule} from './WYSIWYG-editor.module';

describe('WYSIWYGEditorComponent', () => {
  let component: WYSIWYGEditorComponent;
  let fixture: ComponentFixture<WYSIWYGEditorComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [ FormsModule, HttpClientModule],
      declarations: [WYSIWYGEditorComponent, WYSIWYGEditorToolbarComponent, AeSelectComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WYSIWYGEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should paste raw text', () => {
    const htmlText = '<h1>Hello!</h1>';
    const rawText = 'Hello!';
    component.config.rawPaste = true;

    const dataTransfer = new DataTransfer();

    const clipboardEvent = new ClipboardEvent("paste", {
      clipboardData: dataTransfer,
    });
    clipboardEvent.clipboardData.setData("text/plain", rawText);
    clipboardEvent.clipboardData.setData("text/html", htmlText);

    const outputRawText = component.onPaste(clipboardEvent);

    expect(outputRawText).toEqual(rawText);
  });
});
