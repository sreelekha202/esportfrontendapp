import {ModuleWithProviders, NgModule} from '@angular/core';
import {WYSIWYGEditorComponent} from './WYSIWYG-editor.component';
import {WYSIWYGEditorToolbarComponent} from './WYSIWYG-editor-toolbar.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import { AeSelectComponent } from './ae-select/ae-select.component';

@NgModule({
  imports: [
    CommonModule, FormsModule, ReactiveFormsModule
  ],
  declarations: [WYSIWYGEditorComponent, WYSIWYGEditorToolbarComponent, AeSelectComponent],
  exports: [WYSIWYGEditorComponent, WYSIWYGEditorToolbarComponent]
})
export class WYSIWYGEditorModule {
  public static forRoot(environment: any): ModuleWithProviders<WYSIWYGEditorModule> {
    return {
      ngModule: WYSIWYGEditorModule,
      providers: [
        {
          provide: 'env', // you can also use InjectionToken
          useValue: environment,
        },
      ],
    };
  }
}
