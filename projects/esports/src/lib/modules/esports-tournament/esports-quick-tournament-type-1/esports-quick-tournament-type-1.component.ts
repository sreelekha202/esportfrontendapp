import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { GlobalUtils } from '../../../services/global-utils';
import { da } from 'date-fns/locale';

export interface QuickTournamnetComponentData {
  step: number;
}

@Component({
  selector: 'esports-quick-tournament-type-1',
  templateUrl: './esports-quick-tournament-type-1.component.html',
  styleUrls: ['./esports-quick-tournament-type-1.component.scss'],
})
export class EsportsQuickTournamentType1Component implements OnInit {
  @Input() data: QuickTournamnetComponentData;
  @Output() submit = new EventEmitter();
  tournament = {
    selectedGame: '',
    tournamentName: '',
    tournamentNameTextarea: '',
    tournament_name_skip: '',
    bannerSRC: '',
    visibility: '',
    selected_platform: '',
    selectedPlatformIndex: '',
    tournamentType: '',
    start_date: '',
    start_time: '',
    participant_limit: '',
  };

  constructor() {}

  ngOnInit(): void {}

  stepOne(gameData) {
    if (gameData.selectedGame) {
      this.data.step = 2;
      this.tournament.selectedGame = gameData.selectedGame;
      this.submit.emit({
        data: this.data,
      });
    }
  }

  stepTwo(nameData) {
    if (nameData.tournamentName) {
      this.data.step = 3;
      this.tournament.tournamentName = nameData.tournamentName;
      this.tournament.tournamentNameTextarea = nameData.tournamentNameTextarea;
      this.tournament.tournament_name_skip = nameData.tournament_name_skip;
      this.tournament.bannerSRC = nameData.bannerSRC;
      this.tournament.visibility = nameData.visibility;
      this.submit.emit({
        data: this.data,
      });
    }
  }

  stepThree(platformData) {
    if (platformData.selected_platform) {
      this.data.step = 4;
      this.tournament.selected_platform = platformData.selected_platform;
      this.tournament.selectedPlatformIndex =
        platformData.selectedPlatformIndex;
      this.tournament.tournamentType = platformData.tournamentType;
      this.submit.emit({
        data: this.data,
      });
    }
  }

  stepFour(dateData) {
    if (dateData.start_date) {
      this.data.step = 5;
      this.tournament.start_date = dateData.start_date;
      this.tournament.start_time = dateData.start_time;
      this.tournament.participant_limit = dateData.participant_limit;
      this.submit.emit({
        data: this.data,
      });
    }
  }

  stepFive(tournamnetData) {
    if (tournamnetData.step) {
      this.data.step = 1;
    }
    if (tournamnetData.tournamentdetails) {
      this.submit.emit({
        tournamentdetails: tournamnetData.tournamentdetails.data,
      });
    }
  }
}
