import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
// import { MatExpansionModule } from '@angular/material/expansion';
// import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { WYSIWYGEditorModule } from '../../WYSIWYG-editor/WYSIWYG-editor.module';
import { InlineSVGModule } from 'ng-inline-svg';
import { I18nModule } from '../../../i18n/i18n.module';
import { EsportsSharedModule } from '../../../shared/module/shared.module';
import { EsportsLoaderModule } from '../../esports-loader/esports-loader.module';
import { EsportsInputSearchComponent } from '../components/input-search/esports-input-search.component';
import { EsportsSelectDateComponent } from '../components/select-date/esports-select-date.component';
import { EsportsSelectGameComponent } from '../components/select-game/esports-select-game.component';
import { EsportsSelectPlatformComponent } from '../components/select-platform/esports-select-platform.component';
import { EsportsTournamentDateComponent } from '../components/tournament-date/esports-tournament-date.component';
import { EsportsTournamentDetailsComponent } from '../components/tournament-details/esports-tournament-details.component';
import { EsportsTournamentImageComponent } from '../components/tournament-image/esports-tournament-image.component';
import { EsportsTournamentNameInputComponent } from '../components/tournament-name-input/esports-tournament-name-input.component';
import { EsportsTournamentNameComponent } from '../components/tournament-name/esports-tournament-name.component';
import { EsportsTournamentNumberplayerComponent } from '../components/tournament-numberplayer/esports-tournament-numberplayer.component';
import { EsportsTournamentTimeComponent } from '../components/tournament-time/esports-tournament-time.component';
import { EsportsQuickTournamentType1Component } from './esports-quick-tournament-type-1.component';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';

@NgModule({
  declarations: [
    EsportsQuickTournamentType1Component,
    EsportsSelectGameComponent,
    EsportsTournamentNameComponent,
    EsportsInputSearchComponent,
    EsportsTournamentNameInputComponent,
    EsportsSelectPlatformComponent,
    EsportsSelectDateComponent,
    EsportsTournamentDateComponent,
    EsportsTournamentTimeComponent,
    EsportsTournamentNumberplayerComponent,
    EsportsTournamentDetailsComponent,
    EsportsTournamentImageComponent
  ],
  imports: [
    I18nModule,
    EsportsLoaderModule,
    EsportsSharedModule,
    //CKEditorModule,
    WYSIWYGEditorModule,
    CommonModule,
    ReactiveFormsModule,
    InlineSVGModule,
    NgxMaterialTimepickerModule
  ],
  exports: [
    EsportsQuickTournamentType1Component,
    EsportsSelectGameComponent,
    EsportsTournamentNameComponent,
    EsportsInputSearchComponent,
    EsportsTournamentNameInputComponent,
    EsportsSelectPlatformComponent,
    EsportsSelectDateComponent,
    EsportsTournamentDateComponent,
    EsportsTournamentTimeComponent,
    EsportsTournamentNumberplayerComponent,
    EsportsTournamentDetailsComponent,
    EsportsTournamentImageComponent
  ],
})
export class EsportsQuickTournamentType1Module {
  public static forRoot(
    environment: any
  ): ModuleWithProviders<EsportsQuickTournamentType1Module> {
    return {
      ngModule: EsportsQuickTournamentType1Module,
      providers: [
        I18nModule,
        {
          provide: 'env', // you can also use InjectionToken
          useValue: environment,
        },
      ],
    };
  }
}
