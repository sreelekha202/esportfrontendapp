import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
@Component({
  selector: 'esports-tournament-date',
  templateUrl: './esports-tournament-date.component.html',
  styleUrls: ['./esports-tournament-date.component.scss'],
})
export class EsportsTournamentDateComponent implements OnInit {
  @Input() date: any = new Date();
  @Input() title: any;
  @Output() dateChange = new EventEmitter();
  constructor() { }
  ngOnInit(): void { }
  onDateChange = () => { this.dateChange.emit(this.date) }
  minDate = new Date();
}
