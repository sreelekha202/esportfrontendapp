import { EsportsToastService } from '../../../../services/esports.toast.service';
import { GlobalUtils } from '../../../../services/global-utils';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'esports-select-platform',
  templateUrl: './esports-select-platform.component.html',
  styleUrls: ['./esports-select-platform.component.scss'],
})
export class EsportsSelectPlatformComponent implements OnInit {
  @Input() number;
  @Input() data;
  @Output() numberChange = new EventEmitter();
  @Output() submit = new EventEmitter();
  selectedGame: any;
  selectedPlatformIndex = null;
  tournamentType;
  platforms = [];
  gametype = [];
  bracketType: any;
  dataFilter = '';
  dataFilterTeamDropdownItem = '';
  valuePlatform2;
  activeButton = '';
  dataDlectPlatform = [
    {
      img: '../../../../../assets/icons/create-tournament/Device_PC.svg',
      text: 'PC',
    },
    {
      img: '../../../../../assets/icons/create-tournament/Game_controller.svg',
      text: 'Console',
    },
    {
      img: '../../../../../assets/icons/create-tournament/Device_MB.svg',
      text: 'Mobile',
    },
  ];
  dataTeamDropdownItem = [
    {
      id: 0,
      text: 'Duo',
      subText: 'Each participating team will comprise of 2 players',
    },
    {
      id: 1,
      text: 'Squad',
      subText: 'Each participating team will comprise of 4 players',
    },
    {
      id: 2,
      text: 'X vs X',
      subText: 'Set the number of players allowed per team',
    },
  ];
  constructor(
    private toastService: EsportsToastService,
    private translateService: TranslateService
  ) {}

  ngOnInit(): void {
    if (GlobalUtils.isBrowser()) {
      this.selectedGame = this.data?.selectedGame;
      this.gametype = [
        {
          name: 'Single',
          type: 'individual',
          icon: 'assets/icons/matchmaking/platforms/single-player-2.svg',
          title: 'Single',
          createdBy: 'admin',
          createdOn: '2020-08-19T12:56:15.185Z',
          status: '1',
          updatedBy: 'admin',
          updatedOn: '2020-08-19T12:56:15.185Z',
          _id: 'aaa',
          is_selected: false,
        },
        {
          name: 'Team',
          type: 'team',
          icon: 'assets/icons/matchmaking/platforms/team-2.svg',
          title: 'team',
          createdBy: 'admin1',
          createdOn: '2020-08-19T12:56:15.185Z',
          status: '2',
          updatedBy: 'admin',
          updatedOn: '2020-08-19T12:56:15.185Z',
          _id: 'bbb',
          is_selected: false,
        },
      ];

      if (
        this.selectedGame &&
        this.selectedGame.platform &&
        this.selectedGame.platform.length > 0
      )
        this.selectedGame.platform.map((obj) => {
          if (obj.name == 'pc') {
            this.platforms.push({
              ...obj,
              type: 'pc',
              icon: 'assets/icons/matchmaking/platforms/pc.svg',
              title: 'MATCHMAKING.SELECT_PLATFORM.PLATFORMS.PC',
            });
          } else if (obj.name == 'Mobile') {
            this.platforms.push({
              ...obj,
              type: 'mobile',
              icon: 'assets/icons/matchmaking/platforms/mobile.svg',
              title: 'MATCHMAKING.SELECT_PLATFORM.PLATFORMS.MOBILE',
            });
          } else if (obj.name == 'Other') {
            this.platforms.push({
              ...obj,
              type: 'console',
              icon: 'assets/icons/matchmaking/platforms/console.svg',
              title: 'MATCHMAKING.SELECT_PLATFORM.PLATFORMS.CONSOLE',
            });
          } else if (obj.name == 'Xbox') {
            this.platforms.push({
              ...obj,
              type: 'xbox',
              icon: 'assets/icons/matchmaking/platforms/console.svg',
              title: 'MATCHMAKING.SELECT_PLATFORM.PLATFORMS.XBOX',
            });
          } else if (obj.name == 'PS4') {
            this.platforms.push({
              ...obj,
              type: 'ps4',
              icon: 'assets/icons/matchmaking/platforms/console.svg',
              title: 'MATCHMAKING.SELECT_PLATFORM.PLATFORMS.PS4',
            });
          } else if (obj.name == 'PS5') {
            this.platforms.push({
              ...obj,
              type: 'ps5',
              icon: 'assets/icons/matchmaking/platforms/console.svg',
              title: 'MATCHMAKING.SELECT_PLATFORM.PLATFORMS.PS5',
            });
          } else {
            this.platforms.push({
              ...obj,
              type: 'pc',
              icon: 'assets/icons/matchmaking/platforms/pc.svg',
              title: 'MATCHMAKING.SELECT_PLATFORM.PLATFORMS.PC',
            });
          }
        });
    }
  }

  //number counter input box
  plusNumber(): void {
    this.number = (this.number === undefined ? 0 : this.number) + 1;
    this.numberChange.emit(this.number);
  }
  minusNumber(): void {
    if (this.number > 1) {
      this.number = this.number - 1;
      this.numberChange.emit(this.number);
    }
  }
  onChange() {
    this.numberChange.emit(this.number);
  }

  setPlatform(i) {
    this.selectedPlatformIndex = i;
  }
  setPlatform2(type) {
    this.tournamentType = type;
    this.valuePlatform2 = type;
    // this.gametype.map((obj)=>{obj.is_selected = false})
    // this.gametype[i].is_selected = true;
  }
  next = () => {
    if (GlobalUtils.isBrowser()) {
      if (
        this.selectedPlatformIndex != null &&
        this.selectedPlatformIndex != undefined &&
        this.selectedGame.platform[this.selectedPlatformIndex]
      ) {
        if (this.tournamentType != null) {
          if (this.tournamentType == 'team') {
            if (this.dataFilterTeamDropdownItem !== '') {
              if (this.dataFilterTeamDropdownItem == 'X vs X') {
                if (this.number === undefined) {
                  this.toastService.showError(
                    this.translateService.instant(
                      'CREATE_TOURNAMENT.ERRORS.ERROR_4'
                    )
                  );
                } else {
                  this.submit.emit({
                    selected_platform:
                      this.selectedGame.platform[this.selectedPlatformIndex],
                    selectedPlatformIndex: this.selectedPlatformIndex,
                    tournamentType: {
                      type: this.tournamentType,
                      teamSize: this.number,
                    },
                  });
                }
                  
              } else {
                this.submit.emit({
                  selected_platform:
                    this.selectedGame.platform[this.selectedPlatformIndex],
                  selectedPlatformIndex: this.selectedPlatformIndex,
                  tournamentType: {
                    type: this.tournamentType,
                    teamSize: this.dataFilterTeamDropdownItem === 'Duo' ? 2 : 4,
                  },
                });
              }
            } else {
              this.toastService.showError(
                this.translateService.instant(
                  'CREATE_TOURNAMENT.ERRORS.ERROR_3'
                )
              );
            }
          } else {
            this.submit.emit({
              selected_platform:
                this.selectedGame.platform[this.selectedPlatformIndex],
              selectedPlatformIndex: this.selectedPlatformIndex,
              tournamentType: {
                type: this.tournamentType,
              },
            });
          }
        } else {
          this.toastService.showError(
            this.translateService.instant('CREATE_TOURNAMENT.ERRORS.ERROR_1')
          );
        }
      } else {
        this.toastService.showError(
          this.translateService.instant('CREATE_TOURNAMENT.ERRORS.ERROR_2')
        );
      }
    }
  };

  status: boolean = false;
  clickEvent(value) {
    this.dataFilter = value.text;
  }
  clickChange(value) {
    this.dataFilterTeamDropdownItem = value;
    this.activeButton = value;
  }
}
