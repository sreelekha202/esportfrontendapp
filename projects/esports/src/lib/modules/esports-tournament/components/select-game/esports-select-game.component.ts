import { EsportsTournamentService } from '../../../../services/esport.tournament.service';
import { Component, EventEmitter, OnInit, Output,Inject } from '@angular/core';

import { EsportsToastService } from '../../../../services/esports.toast.service';
import { GlobalUtils } from '../../../../services/global-utils';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'esports-select-game',
  templateUrl: './esports-select-game.component.html',
  styleUrls: ['./esports-select-game.component.scss'],
})
export class EsportsSelectGameComponent implements OnInit {
  @Output() submit = new EventEmitter();
  games = [];
  text: string = '';
  selectedGame = null;
  showLoader: boolean = true;

  constructor(
    @Inject('env') private environment,
    private tournamentService: EsportsTournamentService,
    private toastService: EsportsToastService,
    private translateService: TranslateService
  ) {}

  ngOnInit(): void {
    this.getGames();
  }

  onTextChange = (data) => {
    this.text = data;
  };

  // search on Array
  filterGame = () => {
    return this.games.filter((game) =>
      game.name.includes(this.text.toLowerCase().trim())
    );
  };

  getGames() {
    if (GlobalUtils.isBrowser()) {
      this.showLoader = true;
      const param: any = {};
      this.text ? (param.search = this.text) : '';
      this.tournamentService.getAllGames(param).subscribe(
        (res) => {
          this.showLoader = false;
          if (res && res.data && res.data.length) {
            res.data.map((obj) => {
              /* Quick tournament games will not list those bracket type with battle_royale only  */
              if(((obj.bracketTypes.double || obj.bracketTypes.ladder || obj.bracketTypes.round_robin || obj.bracketTypes.single || obj.bracketTypes.swiss_safeis ) && !obj.bracketTypes.battle_royale) && (obj.quickFormatConfig && obj.isQuickFormatAllow))
                this.games.push({ ...obj, name: String(obj.name).toLowerCase() });

            });
          }
        },
        (err) => {
          this.showLoader = false;
        }
      );
    }
  }

  next() {
    if (GlobalUtils.isBrowser()) {
      if (this.selectedGame) {
        this.submit.emit({
          selectedGame: this.selectedGame,
        });
      } else {
        this.toastService.showError(
          this.translateService.instant('CREATE_TOURNAMENT.SELECT_GAME.TITLE')
        );
      }
    }
  }

  setGame(game) {
    this.selectedGame = game;
  }
}
