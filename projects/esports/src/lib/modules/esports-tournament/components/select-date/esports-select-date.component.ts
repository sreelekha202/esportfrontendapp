import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { EsportsToastService } from '../../../../services/esports.toast.service';
import { GlobalUtils } from '../../../../services/global-utils';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'esports-select-date',
  templateUrl: './esports-select-date.component.html',
  styleUrls: ['./esports-select-date.component.scss'],
})
export class EsportsSelectDateComponent implements OnInit {
  // date: Date = new Date();
  // time: any = timeFormatAMPM(new Date())
  @Input() data;
  @Output() submit = new EventEmitter();
  selectedGame: any;
  date: Date;
  time: any;
  participant: any = 0;
  platforms = [];

  constructor(
    private toastService: EsportsToastService,
    public translate: TranslateService
  ) {}

  ngOnInit(): void {
    if (GlobalUtils.isBrowser()) {
      this.selectedGame = this.data?.selectedGame;
    }
  }

  onDateChange(data) {
    this.date = data;
  }
  onTimeChange(data) {
    this.time = data;
  }

  onnumberChange(data) {
    if (this.selectedGame.quickFormatConfig && data > this.selectedGame.quickFormatConfig.maxParticipants) {
      this.toastService.showError(
        this.translate.instant(
          'CREATE_TOURNAMENT.TOURNAMENT_NAME.TOURNAMENT_REVIEWS.MAX_PARTICIPANT_ERROR'
        )
      );
    }
    this.participant = data;
  }

  next = () => {
    if (GlobalUtils.isBrowser()) {
      if (this.date && this.time) {
        if (this.participant) {
          if (
            this.participant >
            this.selectedGame?.quickFormatConfig?.maxParticipants
          ) {
            this.toastService.showError(
              this.translate.instant(
                'CREATE_TOURNAMENT.TOURNAMENT_NAME.TOURNAMENT_REVIEWS.MAX_PARTICIPANT_ERROR'
              )
            );
          } else if (this.participant < 2) {
            this.toastService.showError(
              this.translate.instant(
                'CREATE_TOURNAMENT.TOURNAMENT_NAME.TOURNAMENT_REVIEWS.MIN_PARTICIPANT_ERROR'
              )
            );
          } else {
            const myArr = this.time.split(' ');
            let AMPM = myArr[1];
            let time = myArr[0].split(':');
            let hour: number = Number(time[0]);
            let minute = Number(time[1]);
            AMPM == 'PM' || AMPM == 'pm' || AMPM == 'Pm' || AMPM == 'pM'
              ? (hour = hour + 12)
              : '';
            const date = new Date(this.date);
            date.setHours(hour);
            date.setMinutes(minute);
            const diff = Date.now() - date.getTime();
            this.date = date;
            if (diff < 300000) {
              this.submit.emit({
                start_date: this.date,
                start_time: this.time,
                participant_limit: this.participant,
              });
            } else {
              this.toastService.showError(
                this.translate.instant('ERROR.PAST_START_TIME')
              );
            }
          }
        } else {
          this.toastService.showError(
            this.translate.instant('BRACKET.REQUIRED.MAX_PARTICIPANT')
          );
        }
      } else {
        this.toastService.showError(
          this.translate.instant('BRACKET.REQUIRED.DATE_TIME')
        );
      }
    }
  };

  checkDateTime(): Boolean {
    return true;
  }
}
