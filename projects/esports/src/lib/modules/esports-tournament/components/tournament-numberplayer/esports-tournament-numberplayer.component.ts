import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'esports-tournament-numberplayer',
  templateUrl: './esports-tournament-numberplayer.component.html',
  styleUrls: ['./esports-tournament-numberplayer.component.scss'],
})
export class EsportsTournamentNumberplayerComponent implements OnInit {
  @Input() number;
  @Output() numberChange = new EventEmitter();
  constructor() {}

  ngOnInit(): void {}

  plusNumber(): void {
    this.number = this.number + 1;
    this.numberChange.emit(this.number);
  }
  minusNumber(): void {
    if (this.number > 0) {
      this.number = this.number - 1;
      this.numberChange.emit(this.number);
    }
  }
  onChange() {
    this.numberChange.emit(this.number)
  }
}
