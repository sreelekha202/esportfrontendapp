import { Component, OnInit, Input ,EventEmitter, Output} from '@angular/core';
import { FormGroup } from '@angular/forms';
import { S3UploadService } from '../../../../services/esport.s3upload.service';
import { EsportsToastService } from '../../../../services/esports.toast.service';

@Component({
  selector: 'esports-tournament-image',
  templateUrl: './esports-tournament-image.component.html',
  styleUrls: ['./esports-tournament-image.component.scss'],
})
export class EsportsTournamentImageComponent implements OnInit {
  @Input() customFormControlName: string;
  @Input() customFormGroup: FormGroup;

  @Input() title: string;
  @Input() subtitle: string;

  @Input() img: string;

  @Input() dimension;
  @Input() size;
  @Input() type;

  @Input() id;
  @Input() required: boolean = false;
  @Output() imgurltosave = new EventEmitter();

  isProcessing: boolean = false;
  imgurl: any = '';

  constructor(
    private s3UploadService: S3UploadService,
    private toastService: EsportsToastService
  ) {}

  ngOnInit(): void {
    this.imgurl = this.img;
  }

  eventChange = async (event) => {
    try {

      this.isProcessing = true;

      const upload: any = await this.s3UploadService.validateAndUploadImage(
        event,
        this.dimension,
        this.size,
        this.type
      );
       this.imgurl = upload.data[0].Location;
        this.imgurltosave.emit(this.imgurl)
      //  this.customFormGroup.get(this.customFormControlName).setValue(upload);
      // this.customFormGroup
      //   .get(this.customFormControlName)
      //   .setValue(upload?.data?.Location);
      // this.toastService.showSuccess(upload?.message);
      // this.isProcessing = false;
    } catch (error) {
      this.isProcessing = false;
      const errorMessage = error?.error?.message || error?.message;
      this.toastService.showError(errorMessage);
    }
  };
}
