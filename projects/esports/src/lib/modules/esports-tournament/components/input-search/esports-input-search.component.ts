import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'esports-input-search',
  templateUrl: './esports-input-search.component.html',
  styleUrls: ['./esports-input-search.component.scss'],
})
export class EsportsInputSearchComponent implements OnInit {
  @Input() placeholder: string = '';
  @Input() text: string = '';
  @Output() onTextChange = new EventEmitter<string>();

  constructor() {}
  ngOnInit(): void {}

  onSearchTextChange = () => this.onTextChange.emit(this.text);
}
