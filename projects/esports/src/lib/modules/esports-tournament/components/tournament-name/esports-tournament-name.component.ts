import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { EsportsToastService } from '../../../../services/esports.toast.service';
import { S3UploadService } from '../../../../services/esport.s3upload.service';
import { GlobalUtils } from '../../../../services/global-utils';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'esports-tournament-name',
  templateUrl: './esports-tournament-name.component.html',
  styleUrls: ['./esports-tournament-name.component.scss'],
})
export class EsportsTournamentNameComponent implements OnInit {
  @Output() submit = new EventEmitter();
  loading: boolean = false;
  text: string = '';
  toggle: boolean = false;
  tournamentNameTextarea: string = '';

  fileToUpload: File | null = null;
  bannerSRC: string = '';
  checkFileName: boolean = false;
  toggleInputFile: boolean = false;
  visibility: number = 1;
  dataVisibility = 1;
  visiblitytype = [
    {
      id : 2,
      name: 'Invite only',
      type: 'private',
      icon: 'assets/icons/private.svg',
      title: 'Single',
      createdBy: 'admin',
      createdOn: '2020-08-19T12:56:15.185Z',
      status: '1',
      updatedBy: 'admin',
      updatedOn: '2020-08-19T12:56:15.185Z',
      _id: 'aaa',
      is_selected: false,
    },
    {
      id : 1,
      name: 'Everyone',
      type: 'public',
      icon: 'assets/icons/public.svg',
      title: 'team',
      createdBy: 'admin1',
      createdOn: '2020-08-19T12:56:15.185Z',
      status: '2',
      updatedBy: 'admin',
      updatedOn: '2020-08-19T12:56:15.185Z',
      _id: 'bbb',
      is_selected: false,
    },
  ];

  constructor(
    @Inject('env') private environment,
    private toastService: EsportsToastService,
    public s3Service: S3UploadService,
    private translateService: TranslateService
  ) {}

  ngOnInit(): void {
  }
  onTextChange = (text) => {
    this.text = text;
  };

  onOverviewChang = (text) => {
    this.tournamentNameTextarea = text;
  };

  next() {
    if (GlobalUtils.isBrowser()) {
      this.text = this.text ? this.text.trim() : '';
      if (this.text) {
        this.submit.emit({
          tournamentName: this.text,
          tournamentNameTextarea: this.tournamentNameTextarea,
          tournament_name_skip: false,
          bannerSRC: this.bannerSRC,
          visibility: this.visibility
        });
      } else {
        this.toastService.showError(
          this.translateService.instant(
            'CREATE_TOURNAMENT.TOURNAMENT_NAME.PLACEHOLDER'
          )
        );
      }
    }
  }

  clickEvent() {
    this.toggle = !this.toggle;
  }

  handleFileInput(files: FileList) {
    this.isValidImage(files, { width: 1920, height: 300 });
  }

  isValidImage(files: any, values): Boolean {
    if (files && files[0]) {
      if (files[0]['size'] / (1000 * 1000) > 2) {
        this.toastService.showError(
          this.translateService.instant('TOURNAMENT.ERROR_IMG_2')
        );
        return;
      }

      if (
        files[0].type != 'image/jpeg' &&
        files[0].type != 'image/jpg' &&
        files[0].type != 'image/png'
      ) {
        this.toastService.showError(
          this.translateService.instant('TOURNAMENT.ERROR_IMG_1')
        );
        return;
      }

      try {
        const file = files[0];

        const reader = new FileReader();
        reader.readAsDataURL(file);
        let th = this;
        reader.onload = (e) => {
          let image: any = new Image();
          image.src = e.target.result;
          image.onload = function () {
            let height = this.height;
            let width = this.width;
            if (height != values['height'] && width != values['width']) {
              th.toastService.showInfo(
                th.translateService.instant('TOURNAMENT.ERROR_IMG')
              );
            }
            th.upload(files);
          };
        };
      } catch (err) {
        return false;
      }
    } else {
      return false;
    }
  }

  async upload(files) {
    let promises = [];
    for (let obj of files) {
      promises.push(this.toBase64(obj));
    }
    let Base64String = await Promise.all(promises);
    let imageData = {
      path: this.environment.tournamentS3BucketName,
      files: Base64String,
    };
    this.loading = true;
    this.s3Service
      .fileUpload(this.environment.apiEndPoint, imageData)
      .subscribe(
        (res) => {
          this.bannerSRC = res['data'][0]['Location'];
          this.toggleInputFile = !this.toggleInputFile;
          this.checkFileName = true;
          this.fileToUpload = files.item(0);
          this.loading = false;
          this.toastService.showSuccess(
            this.translateService.instant('ARTICLE_POST.BANNER_UPLOAD')
          );
        },
        (err) => {
          this.loading = false;
          this.toastService.showError(
            this.translateService.instant('TOURNAMENT.ERROR_LOGO')
          );
        }
      );
  }

  toBase64(file) {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = (error) => reject(error);
    });
  }

  clickCheckedVisibility(checked) {
    this.dataVisibility = checked.id;
    this.visibility = checked.id;
    checked.ichecked = true;
  }
}
