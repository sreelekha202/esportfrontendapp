import { HttpClient } from '@angular/common/http';
import {
  Component,
  EventEmitter,
  Inject,
  Input,
  OnInit,
  Output,
} from '@angular/core';

//import { ChangeEvent } from '@ckeditor/ckeditor5-angular/ckeditor.component';
//import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { WYSIWYGEditorConfig } from '../../../WYSIWYG-editor/config';
import { EsportsToastService } from '../../../../services/esports.toast.service';
import { EsportsTournamentService } from '../../../../services/esport.tournament.service';
import { EsportsUserService } from '../../../../services/esport.user.service';
import { EsportsGameService } from '../../../../services/esport.game.service';
import { GlobalUtils } from '../../../../services/global-utils';

import { FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { EsportsEditPopupComponent } from '../../../esports-popup/esports-edit-popup/esports-edit-popup.component';
@Component({
  selector: 'esports-tournament-details',
  templateUrl: './esports-tournament-details.component.html',
  styleUrls: ['./esports-tournament-details.component.scss'],
})
export class EsportsTournamentDetailsComponent implements OnInit {
  @Input() data;
  @Output() submit = new EventEmitter();
  //Editor = ClassicEditor;
  editorConfig: WYSIWYGEditorConfig = {};
  ediDirection = '';
  text: string;
  ckConfig: any;
  games = [];
  placeHolder = 'Input text'
  date: Date;
  name = ' ';
  selectedPlatformIndex = '';
  game: any;
  time = '';
  gameid = '';
  platformId = '';
  TournamentID = '';
  currentUser: any;
  Tournmenttype = [];
  platforms = [];
  rules: any;
  isScreenshotRequired: boolean = false;
  isParticipantsLimit: boolean = true;
  isShowCountryFlag: boolean = false;
  scoreReporting: number = 1;
  visibility;
  bra_date: any;
  participants: any = [];
  form: FormGroup;
  imgurl = '';
  platformId1: string;
  TournamentID1: string;
  selectedPlatformIndex1: any;
  emailsforinvite: any = [];
  date1: any;
  time1: any;
  par_limit;
  isSpecifyAllowedRegions: boolean = false;
  games1: any = [];
  selectedcontries: any = [];
  start_date: any;
  showLoader: boolean = false;
  minDate = new Date();
  selectedGame: any;
  selected_platform: any;
  tournamentNameTextarea: string;

  todayDate: Date = new Date(
    new Date().setFullYear(new Date().getFullYear() - 10)
  );
  parentalDate: Date = new Date(
    new Date().setFullYear(new Date().getFullYear() - 18)
  );
  parental = false;
  bracketType: any;
  structure: any;
  toggle: boolean = false;
  toggle2: boolean = false;

  dataFilterTeamDropdownItem = '';
  regionList = [];
  dataAdditional = 3;
  dataAdditionalSetting = [
    {
      id: 0,
      textTitle: 'Screenshot required',
    },
    {
      id: 1,
      textTitle: 'Show country flag',
    },
  ];
  dataScore = 1;
  dataScoreReporting = [
    {
      id: 2,
      textTitle: 'Participants & myself',
    },
    {
      id: 1,
      textTitle: 'Myself only',
    },
  ];
  dataParticipant = 1;
  dataParticipantLimit = [
    {
      id: 0,
      textTitle: 'No limit',
    },
    {
      id: 1,
      textTitle: 'Add limit (please specify)',
    },
  ];
  dataVisibility = 1;
  dataVisibilitySetting = [
    {
      id: 2,
      textTitle: 'Private',
    },
    {
      id: 1,
      textTitle: 'Public',
    },
  ];

  locationFlag:boolean = false;
  checkedArray: any = [];
  showDropdown: boolean = false;
  constructor(
    @Inject('env') private environment,
    private gameService: EsportsGameService,
    private userService: EsportsUserService,
    public tournamentService: EsportsTournamentService,
    private toastService: EsportsToastService,
    public dialog: MatDialog
  ) {}

  ngOnInit(): void {
    let regions = [];
    if (GlobalUtils.isBrowser()) {
      this.showLoader = true;
      let countries = [];
      this.games1 = [];
      this.getGames();

      this.TournamentID = this.data?.tournamentType.type;
      this.name = this.data?.tournamentName;
      this.visibility = this.data?.visibility;
      this.game = this.data?.selectedGame;
      this.selectedGame = this.data?.selectedGame;

      this.gameid = this.game._id;

      this.date = new Date(this.data?.start_date);
      this.time = this.data?.start_time;

      this.date1 = new Date(this.data?.start_date);
      this.date1.setDate(this.date1.getDate() + 7);
      this.time1 = this.data?.start_time;

      this.imgurl = this.data?.bannerSRC;
      this.selected_platform = this.data?.selected_platform;
      this.tournamentNameTextarea = this.data?.tournamentNameTextarea;

      if (this.selectedGame && this.selectedGame.quickFormatConfig) {
        this.isParticipantsLimit =
          this.selectedGame.quickFormatConfig.isParticipantsLimit;
        this.start_date = new Date(this.data?.start_date);
        this.par_limit = JSON.parse(this.data?.participant_limit);
        this.bracketType = this.selectedGame.quickFormatConfig.bracketType;
        this.platformId = this.selected_platform._id;
        let b = {
          bracketType: this.bracketType,
          maximumParticipants: this.par_limit,
          noOfSet: 1,
        };
        this.tournamentService.generateBracket1(b).subscribe((response) => {
          this.structure = response;
        });
      }

      this.editorConfig = {
        editable: true,
        spellcheck: true,
        height: '200px',
        width: 'auto',
        minWidth: '0',
        translate: 'false',
        enableToolbar: true,
        showToolbar: true,
        defaultParagraphSeparator: '',
        defaultFontName: '',
        defaultFontSize: '',
        fonts: [
          { class: 'arial', name: 'Arial' },
          { class: 'times-new-roman', name: 'Times New Roman' },
          { class: 'calibri', name: 'Calibri' },
          { class: 'comic-sans-ms', name: 'Comic Sans MS' },
        ],
        customClasses: [
          {
            name: 'quote',
            class: 'quote',
          },
          {
            name: 'redText',
            class: 'redText',
          },
          {
            name: 'titleText',
            class: 'titleText',
            tag: 'h1',
          },
        ],
        sanitize: true,
        toolbarPosition: 'top',
        toolbarHiddenButtons: [['bold', 'italic'], ['fontSize']],
      };
      this.locationFlag = ((this.environment.platform && this.environment.platform=='spark') ? true : false );

      this.userService.getStates().subscribe(
        (data: any) => {
          regions = data.states;
          regions.filter((obj) => {
          if (obj['country_id'] == 157) {
            this.regionList.push(obj);
          }
        });
          // for (let d of countries) {
          //   d.checked = false;
          //   //  this.games1.push(d)
          // }
          // this.games1 = countries;
          this.showLoader = false;
          //this.products = data;
        },
        (err) => {
          this.showLoader = false;
        }
      );

    }
  }
  eventshowDropdown() {
    this.showDropdown = !this.showDropdown;
  }
  parentalCheckBox(date: any) {
    const selectedDate = new Date(date.value);
    if (selectedDate > this.parentalDate && selectedDate < this.todayDate) {
      this.parental = true;
      this.form.get('dobPolicy').setValidators(Validators.pattern('true'));
      this.form.get('dobPolicy').setValue(true);
    } else {
      this.parental = false;
      this.form.get('dobPolicy').setValidators(Validators.pattern('false'));
      this.form.get('dobPolicy').setValue(false);
    }

    this.form.get('dobPolicy').updateValueAndValidity();
  }

  getNotification(event) {
    this.isScreenshotRequired = event;
  }

  getNotification5(event) {
    this.isSpecifyAllowedRegions = event.checked;
  }

  getNotification6(event) {
    this.selectedcontries = event;
  }

  getNotification1(event) {
    this.isParticipantsLimit = event.checked;
  }

  getNotification2(event) {
    this.isShowCountryFlag = event;
  }
  getNotification3(event) {
    this.scoreReporting = event;
  }
  getNotification4(event) {}

  bracket(data) {
    this.bra_date = data;
  }

  bracket1(data) {
    this.participants = data;
  }

  datatoimg(data) {
    this.imgurl = data;
  }

  emitValuefor(data) {
    this.emailsforinvite.push(data);
  }

  onDateChange() {}

  datefun1() {}

  getGames() {
    if (GlobalUtils.isBrowser()) {
      this.showLoader = true;
      const param: any = {};
      this.text ? (param.search = this.text) : '';
      this.gameService.getGames(param).subscribe(
        (res) => {
          this.games = res.data;
          this.showLoader = false;
          for (let data of this.games) {
            if (data._id == this.gameid) {
              this.platforms = data.platform;
              this.Tournmenttype = data.Tournmenttype;
            }
          }
          let i = 0;

          for (let data1 of this.platforms) {
            this.selectedPlatformIndex = this.data?.selectedPlatformIndex;
            if (i == Number(this.selectedPlatformIndex)) {
              this.platformId = data1._id;
            }
          }

          for (let data1 of this.Tournmenttype) {
            this.selectedPlatformIndex = this.data?.selectedPlatformIndex;
            if (i == Number(this.selectedPlatformIndex)) {
              this.TournamentID = data1._id;
            }
          }
        },
        (err) => {
          this.showLoader = false;
        }
      );

      this.platformId1 = this.data?.tournamentType.type;
    }
  }

  onDateChange2() {}

  showCreatedPopup(): void {
    this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.currentUser = data;
      }
    });
    if (
      this.time1 != undefined &&
      this.date1 != undefined &&
      this.time1 != '' &&
      this.date1 != ''
    ) {
      let datatosend = {
        bracketType: this.bracketType,
        description: this.tournamentNameTextarea,
        emailInvite: [],
        endDate: this.date1,
        gameDetail: this.gameid,
        isParticipantsLimit: this.isParticipantsLimit,
        isRegEndDate: false,
        isRegStartDate: false,
        isScreenshotRequired: this.isScreenshotRequired,
        isShowCountryFlags: this.isShowCountryFlag,
        isSpecifyAllowedRegions: this.isSpecifyAllowedRegions,
        maxParticipants: this.par_limit,
        name: this.name,
        noOfSet: 1,
        //noOfRoundPerGroup: '',
        //noOfTeamInGroup: '',
        //noOfWinningTeamInGroup: '',
        participants: this.participants,
        platform: this.platformId,
        scoreReporting: this.scoreReporting,
        stageBracketType: '',
        startDate: this.date,
        step: 6,
        type: 'quick',
        visibility: this.visibility,
        startTime: this.time,
        endTime: this.time1,
        banner: this.imgurl,
        SpecifyAllowedRegions: this.selectedcontries,
        rules: this.rules,
        criticalRules: '',
        tournamentStatus: 'publish',
        participantType: this.platformId1,
        teamSize:
          this.data?.tournamentType.type == 'team'
            ? this.data?.tournamentType.teamSize
            : 0,
        createdBy: this.currentUser._id,
        updatedBy: this.currentUser._id,
        // url: 'https://dev.stcplay.gg/tournament/t22_7_21One',

        // isPaid: false,
        //
        //
        // isPrize: false,
        // faqs: '',
        // schedule: '',
        // isIncludeSponsor: false,
        // tournamentType: 'online',
        // isShowCountryFlag: this.isShowCountryFlag,
        // isManualApproverParticipant: false,
        // invitationLink: '',
        // youtubeVideoLink: '',
        // facebookVideoLink: '',
        // contactDetails: '',
        // twitchVideoLink: '',
        // checkInEndDate: '',
        // regionsAllowed: [],
        // sponsors: [],
        // bracket_data: this.bra_date,
        // stageMatch: '',
        // stageMatchNoOfSet: '',
        // isAllowMultipleRounds: false,
        // venueAddress: [],
        // contactOn: '',
        // whatsApp: '',
        // prizeList: [],
        // substituteMemberSize: 0,

        // //"Tournmenttype":this.Tournmenttype,
        // allowSubstituteMember: false,
        // allowAdvanceStage: false,
        // timezone: 'IST',
        // isCheckInRequired: false,
        // checkInStartDate: '',
        // isAllowThirdPlaceMatch: false,
        // tournamentStatus: 'submitted_for_approval',
        // organizerDetail: this.currentUser._id
      };


      const myArr = this.time1.split(' ');
      let AMPM = myArr[1];
      let time = myArr[0].split(':');
      let hour: number = Number(time[0]);
      let minute = Number(time[1]);
      AMPM == 'PM' || AMPM == 'pm' || AMPM == 'Pm' || AMPM == 'pM'
        ? (hour = hour + 12)
        : '';
      const date = new Date(this.date1);
      date.setHours(hour);
      date.setMinutes(minute);
      //this.date1 = date;
      let currentDate = new Date();

      if (this.isParticipantsLimit) {
        if (this.par_limit >= 2) {
          this.showLoader = true;
          this.tournamentService.saveTournament1(datatosend).subscribe(
            (data: any) => {
              this.showLoader = false;
              this.clearStorage();
              this.submit.emit({
                tournamentdetails: data,
              });
            },
            (err) => {
              this.showLoader = false;
              this.toastService.showError(err.error.message);
            }
          );
        } else {
          this.toastService.showError('Minimum allowed limit is 2');
        }
      } else {
        this.showLoader = true;
        this.tournamentService.saveTournament1(datatosend).subscribe(
          (data: any) => {
            this.showLoader = false;
            this.clearStorage();
            this.submit.emit({
              tournamentdetails: data,
            });
          },
          (err) => {
            this.showLoader = false;
            this.toastService.showError(err.message);
          }
        );
      }
    } else {
      this.toastService.showError('Please select End Date and Time.');
    }
  }

  clearStorage() {

  }

  onReady(eventData) {
    let environment = this.environment;
    eventData.plugins.get('FileRepository').createUploadAdapter = function (
      loader
    ) {
      return new UploadAdapter(loader, environment);
    };
  }

  clickEvent() {
    this.toggle = !this.toggle;
  }

  clickEvent2() {
    this.toggle2 = !this.toggle2;
  }

  clickChange(value) {
    this.dataFilterTeamDropdownItem = value;
  }

  clickCheckedAdditional(checked) {
    if (this.checkedArray.includes(checked)) {
      const filterIndexCheck = this.checkedArray.indexOf(checked);
      const newArray = [...this.checkedArray];
      newArray.splice(filterIndexCheck, 1);
      this.checkedArray = newArray;
    } else {
      this.checkedArray = [...this.checkedArray, checked];
    }
    // this.dataAdditional = checked.id;
    // checked.ichecked = true;
  }
  clickCheckedScoreReporting(checked) {
    this.dataScore = checked.id;
    this.scoreReporting = checked.id;
    checked.ichecked = true;
  }
  clickCheckedParticipantLimit(checked) {
    this.dataParticipant = checked.id;
    checked.ichecked = true;
  }
  clickCheckedVisibility(checked) {
    this.dataVisibility = checked.id;
    checked.ichecked = true;
  }

  edit() {
    const dialogRef = this.dialog.open(EsportsEditPopupComponent, {
      data: this.data,
    });
    dialogRef.afterClosed().subscribe((newData) => {
      if (newData) {
        this.data.selectedGame = newData.selectedGame;
        this.data.selectedPlatformIndex = newData.selectedPlatformIndex;
        this.data.selected_platform = newData.selected_platform;
        this.data.tournamentNameTextarea = newData.tournamentNameTextarea;
        this.data.tournamentType = newData.tournamentType;
        this.data.visibility = newData.visibility;
        this.data.tournamentName = newData.tournamentName;

        if (GlobalUtils.isBrowser()) {
          let countries = [];
          this.games1 = [];
          this.getGames();

          this.TournamentID = this.data?.tournamentType.type;
          this.name = this.data?.tournamentName;
          this.visibility = this.data?.visibility;
          this.game = this.data?.selectedGame;
          this.selectedGame = this.data?.selectedGame;

          this.gameid = this.game._id;

          this.date = new Date(this.data?.start_date);
          this.time = this.data?.start_time;

          this.date1 = new Date(this.data?.start_date);
          this.date1.setDate(this.date1.getDate() + 7);
          this.time1 = this.data?.start_time;

          this.imgurl = this.data?.bannerSRC;
          this.selected_platform = this.data?.selected_platform;
          this.tournamentNameTextarea = this.data?.tournamentNameTextarea;

          if (this.selectedGame && this.selectedGame.quickFormatConfig) {
            this.isParticipantsLimit =
              this.selectedGame.quickFormatConfig.isParticipantsLimit;
            this.start_date = new Date(this.data?.start_date);
            this.par_limit = JSON.parse(this.data?.participant_limit);
            this.bracketType = this.selectedGame.quickFormatConfig.bracketType;
            this.platformId = this.selected_platform._id;
            let b = {
              bracketType: this.bracketType,
              maximumParticipants: this.par_limit,
              noOfSet: 1,
            };
            this.tournamentService.generateBracket1(b).subscribe((response) => {
              this.structure = response;
            });
          }

        }
      }
    });
  }
}
class UploadAdapter {
  loader: any;
  environment: any;

  constructor(loader, environment) {
    this.loader = loader;
    this.environment = environment;
  }

  upload() {
    if (GlobalUtils.isBrowser()) {
      return this.loader.file.then(
        (file) =>
          new Promise((resolve, reject) => {
            var myReader = new FileReader();
            myReader.onloadend = (e) => {
              const imageData: any = {
                path: this.environment.articleS3BucketName,
                files: [myReader.result],
              };

              // resolve({ default: myReader.result });
              const accessToken = localStorage.getItem(
                this.environment.currentToken
              );
              fetch(`${this.environment.apiEndPoint}file-upload`, {
                method: 'POST',
                headers: {
                  authorization: `Bearer ${accessToken}`,
                  'Access-Control-Allow-Origin': '*',
                },
                body: JSON.stringify(imageData), // This is your file object
              })
                .then(
                  (response) => response.json() // if the response is a JSON object
                )
                .then(
                  (success) => {
                    resolve({ default: success.data[0]['Location'] });
                  } // Handle the success response object
                )
                .catch(
                  (error) => {} // Handle the error response object
                );
            };

            myReader.readAsDataURL(file);
          })
      );
    }
  }
}
