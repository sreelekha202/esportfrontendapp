import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { timeFormatAMPM } from '../../../../shared/comon';

@Component({
  selector: 'esports-tournament-time',
  templateUrl: './esports-tournament-time.component.html',
  styleUrls: ['./esports-tournament-time.component.scss'],
})
export class EsportsTournamentTimeComponent implements OnInit {
  @Input() time:any=timeFormatAMPM(new Date());
  @Input() title:any;
  @Output() timeChange = new EventEmitter();
  constructor() { }
  ngOnInit(): void { }
  onTimeChange = () => { this.timeChange.emit(this.time)
  }
}
