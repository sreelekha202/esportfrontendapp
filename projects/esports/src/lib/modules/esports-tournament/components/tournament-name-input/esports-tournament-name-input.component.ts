import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'esports-tournament-name-input',
  templateUrl: './esports-tournament-name-input.component.html',
  styleUrls: ['./esports-tournament-name-input.component.scss'],
})
export class EsportsTournamentNameInputComponent implements OnInit {
  @Input() placeholder: string = '';
  @Input() text: string = '';
  @Output() onTextChange = new EventEmitter<string>();
  constructor() { }
  ngOnInit(): void { }
  onTextChang = () => this.onTextChange.emit(this.text)
}
