import { Component, OnInit, Input, Output, EventEmitter, Inject } from '@angular/core';

@Component({
  selector: 'esports-header-type-1',
  templateUrl: './header-type-1.component.html',
  styleUrls: ['./header-type-1.component.scss'],
})
export class HeaderType1Component implements OnInit {
  @Input() title: string;
  @Output() back = new EventEmitter();
  @Output() close = new EventEmitter();
  matchmakingDetails: any;
  selectedGame: any;
  checkQuick8 = false;
  check_lobby = false;
  popup = false;
  constructor(
    @Inject('data') public data
  ) {}

  checkTitle: string = 'Create Tournament';

  ngOnInit(): void {
  }

  backFn() {
    this.back.emit({
      back: true
    });
  }

  closeFn(){
    this.close.emit({
      close: true
    });
  }
 
 
 
}
