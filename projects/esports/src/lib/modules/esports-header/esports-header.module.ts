import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { InlineSVGModule } from 'ng-inline-svg';
import { I18nModule } from '../../i18n/i18n.module';
import { EsportsSharedModule } from '../../shared/module/shared.module';
import { HeaderType1Component } from './header-type-1/header-type-1.component';

@NgModule({
  declarations: [
    HeaderType1Component
  ],
  imports: [
    I18nModule,
    EsportsSharedModule,
    CommonModule,
    ReactiveFormsModule,
    InlineSVGModule
  ],
  exports: [
    HeaderType1Component
  ],
})
export class EsportsHeaderModule {
  public static forRoot(
    data: any
  ): ModuleWithProviders<EsportsHeaderModule> {
    return {
      ngModule: EsportsHeaderModule,
      providers: [
        I18nModule,
        {
          provide: 'data', // you can also use InjectionToken
          useValue: data,
        },
      ],
    };
  }
}
