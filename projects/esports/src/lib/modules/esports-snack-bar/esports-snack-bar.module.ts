import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EsportsSnackBarComponent } from './esports-snack-bar.component';

@NgModule({
  declarations: [EsportsSnackBarComponent],
  imports: [CommonModule],
  exports: [EsportsSnackBarComponent],
})
export class EsportsSnackBarModule {
  // TODO: we need to handle css based on platform
  public static setPlatform(
    platform: any
  ): ModuleWithProviders<EsportsSnackBarModule> {
    return {
      ngModule: EsportsSnackBarModule,
      providers: [
        EsportsSnackBarComponent,
        {
          provide: 'platform',
          useValue: platform
        },
      ],
    };
  }
}
