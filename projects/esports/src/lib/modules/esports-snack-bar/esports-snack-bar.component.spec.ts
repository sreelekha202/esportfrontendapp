import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EsportsSnackBarComponent } from './esports-snack-bar.component';

describe('ArticleItemComponent', () => {
  let component: EsportsSnackBarComponent;
  let fixture: ComponentFixture<EsportsSnackBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EsportsSnackBarComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EsportsSnackBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
