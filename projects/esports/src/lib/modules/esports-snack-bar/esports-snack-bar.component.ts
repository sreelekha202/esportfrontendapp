
import { Component, Inject, OnInit, TemplateRef } from '@angular/core';

import {
  MAT_SNACK_BAR_DATA,
  MatSnackBarRef,
} from '@angular/material/snack-bar';

export interface EsportsSnackBarDataType {
  text: string;
  type: MessageType;
  enableDefaultTimeout?: boolean;
  defaultTimeout?: number
}

export enum MessageType {
  success = 'success-message',
  info = 'info-message',
  error = 'error-message',
  promo = 'promo-message',
}

@Component({
  selector: 'esports-snack-bar',
  templateUrl: './esports-snack-bar.component.html',
  styleUrls: ['./esports-snack-bar.component.scss'],
})
export class EsportsSnackBarComponent implements OnInit {
  SnackComponentType = MessageType;

  constructor(
    @Inject(MAT_SNACK_BAR_DATA) public data: EsportsSnackBarDataType,
    @Inject('platform') public platform,
    private matSnackBarRef: MatSnackBarRef<EsportsSnackBarComponent>
  ) {
  }

  ngOnInit(): void {
    if (this.data?.enableDefaultTimeout) {
      setTimeout(() => {
        this.matSnackBarRef.dismiss();
      }, this.data?.defaultTimeout);
    }
  }

  getSnackType() {
    switch (this.data.type) {
      case MessageType.success: {
        return 'success';
      }
      case MessageType.info: {
        return 'info';
      }
      case MessageType.error: {
        return 'error';
      }
      case MessageType.promo: {
        return 'promo';
      }
    }
  }

  onClose(): void {
    this.matSnackBarRef.dismiss();
  }

  isTemplate(template) {
    return template instanceof TemplateRef;
  }
}
