import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'esports-feature-player',
  templateUrl: './feature-player.component.html',
})
export class FeaturePlayerComponent implements OnInit {
  @Input() API;
  playersOfMonth = [];
  constructor() {}

  ngOnInit(): void {
    this.featurePlayerList();
  }

  featurePlayerList() {
    this.playersOfMonth = [
      {
        image:
          'https://i114.fastpic.ru/big/2021/0331/9b/ada1e8d938bd705eea267a8f833f5d9b.png',
        name: 'Tony Matt',
        score: '2,345',
      },
      {
        image:
          'https://i114.fastpic.ru/big/2021/0331/ec/3c0faeb267e60153565ef1804e8931ec.png',
        name: 'Cormac Sabastian',
        score: '2,345',
      },
      {
        image:
          'https://i114.fastpic.ru/big/2021/0331/be/1a283f3803ff5888941f89e9aa9094be.png',
        name: 'Tony Matt',
        score: '2,345',
      },
      {
        image:
          'https://i114.fastpic.ru/big/2021/0331/99/4ef8afe35c9b6dd1a1646df558bca899.png',
        name: 'Weston Dawn',
        score: '2,345',
      },
      {
        image:
          'https://i114.fastpic.ru/big/2021/0331/4b/c86e08b34318507541619b88204b944b.png',
        name: 'Zack Malory',
        score: '2,345',
      },
      {
        image:
          'https://i114.fastpic.ru/big/2021/0331/03/75b24ea6209aa1292fba999967774103.png',
        name: 'Eric John',
        score: '2,345',
      },
      {
        image:
          'https://i114.fastpic.ru/big/2021/0331/9b/ada1e8d938bd705eea267a8f833f5d9b.png',
        name: 'Tony Matt',
        score: '2,345',
      },
      {
        image:
          'https://i114.fastpic.ru/big/2021/0331/ec/3c0faeb267e60153565ef1804e8931ec.png',
        name: 'Cormac Sabastian',
        score: '2,345',
      },
      {
        image:
          'https://i114.fastpic.ru/big/2021/0331/be/1a283f3803ff5888941f89e9aa9094be.png',
        name: 'Tony Matt',
        score: '2,345',
      },
      {
        image:
          'https://i114.fastpic.ru/big/2021/0331/99/4ef8afe35c9b6dd1a1646df558bca899.png',
        name: 'Weston Dawn',
        score: '2,345',
      },
      {
        image:
          'https://i114.fastpic.ru/big/2021/0331/4b/c86e08b34318507541619b88204b944b.png',
        name: 'Zack Malory',
        score: '2,345',
      },
      {
        image:
          'https://i114.fastpic.ru/big/2021/0331/03/75b24ea6209aa1292fba999967774103.png',
        name: 'Eric John',
        score: '2,345',
      },
    ];
  }

}

