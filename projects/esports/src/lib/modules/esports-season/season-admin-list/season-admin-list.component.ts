import {
  Component,
  Input,
  OnInit,
} from '@angular/core';
import { EsportsSeasonService } from '../../../services/esports.season.service';

@Component({
  selector: 'esports-season-admin-list',
  templateUrl: './season-admin-list.component.html',
})
export class SeasonAdminListComponent implements OnInit {
  @Input() isFeatureEditable: boolean;
  @Input() editIconShow: boolean;
  @Input() viewIconShow: boolean;
  @Input() type: string;
  @Input() api;

  columnsFirstTab = [
    { name: 'Season Name' },
    { name: 'Start Date' },
    { name: 'End Date' },
    { name: '' },
  ];

  rows: any = [];

  constructor(private seasonService: EsportsSeasonService) {}

  ngOnInit(): void {
    this.getSeasonList();
  }

  getSeasonList() {
    let isActive;
    if (this.type === "active") {
      isActive = true;
    } else {
      isActive = false;
    }
      let query = `isActive=${isActive}&fields=isFeatured,name,slug,startDate,endDate,duration,createdOn&sort=-createdOn&page=1&limit=20`;
    this.seasonService.getSeasons(this.api, query).subscribe(
      (data) => {
        this.rows = data?.data?.data?.docs;
      }
    );
  }

  searchByValueFilter(event) {
  }
}
