import { Component, Input, OnInit } from '@angular/core';
import { EsportsSeasonService } from '../../../services/esports.season.service';
import { formatDistanceToNowStrict } from 'date-fns';

@Component({
  selector: 'esports-active-seasons',
  templateUrl: './active-seasons.component.html',
})
export class ActiveSeasonsComponent implements OnInit {
  @Input() API;
  @Input() type;
  seasonsCards = [];
  constructor(private seasonService: EsportsSeasonService) {}

  ngOnInit(): void {
    switch (this.type) {
      case 'feature':
        this.featureSeasonList();
        break;
      case 'allSeasons':
        this.allSeasonList();
        break;
      default:
        this.allSeasonList();
        break;
    }
  }

  featureSeasonList() {
    let query = `isActive=true&isFeatured=true&fields=isFeatured,name,slug,endDate,startDate,image&sort=-createdOn&page=1&limit=20`;
    this.seasonService.getSeasons(this.API, query).subscribe((data) => {

      data?.data?.data?.docs.forEach(element => {
         let text;
         const endDate = new Date(element.endDate);
         const startDate = new Date(element.startDate);
         const currentDate = new Date();
         if (startDate > currentDate) {
           text = `Starts ${formatDistanceToNowStrict(startDate, {
             addSuffix: true,
             unit: 'day',
           })}`;
         } else if (endDate > currentDate) {
           text = `Ends ${formatDistanceToNowStrict(endDate, {
             addSuffix: true,
             unit: 'day',
           })}`;
         } else {
           text = 'Season ended';
        }
        element.text = text;
      });
      this.seasonsCards = data?.data?.data?.docs;
    });
  }

  allSeasonList() {
    let query = `isActive=true&fields=isFeatured,name,slug,endDate,startDate,image&sort=-createdOn&page=1`;
    this.seasonService.getSeasons(this.API, query).subscribe((data) => {
      data?.data?.data?.docs.forEach((element) => {
        let text;
        const endDate = new Date(element.endDate);
        const startDate = new Date(element.startDate);
        const currentDate = new Date();
        if (startDate > currentDate) {
          text = `Starts ${formatDistanceToNowStrict(startDate, {
            addSuffix: true,
            unit: 'day',
          })}`;
        } else if (endDate > currentDate) {
          text = `Ends ${formatDistanceToNowStrict(endDate, {
            addSuffix: true,
            unit: 'day',
          })}`;
        } else {
          text = 'Season ended';
        }
        element.text = text;
      });
      this.seasonsCards = data?.data?.data?.docs;
    });
  }
}

