import { ModuleWithProviders, NgModule } from '@angular/core';
import { WYSIWYGEditorModule } from '../WYSIWYG-editor/WYSIWYG-editor.module';
import { I18nModule } from '../../i18n/i18n.module';
import { EsportsSharedModule } from '../../shared/module/shared.module';
import { EsportsLoaderModule } from '../esports-loader/esports-loader.module';
import { CreateSeasonComponent } from './create-season/create-season.component';
import { SeasonAdminListComponent } from './season-admin-list/season-admin-list.component';
import { SeasonNotificationComponent } from './season-notification/season-notification.component';
import { ActiveSeasonsComponent } from './active-seasons/active-seasons.component';
import { FeaturePlayerComponent } from './feature-player/feature-player.component';
import { CounterComponent } from '../../components/counter/counter.component';
import { ToggleSwitchComponent } from '../../components/toggle-switch/toggle-switch.component';
import { DropDownSelectComponent } from '../../components/drop-down-select/drop-down-select.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {
  SWIPER_CONFIG,
  SwiperConfigInterface,
  SwiperModule,
} from 'ngx-swiper-wrapper';
const DEFAULT_SWIPER_CONFIG: SwiperConfigInterface = {
  direction: 'horizontal',
  slidesPerView: 'auto',
};
@NgModule({
  declarations: [
    CounterComponent,
    DropDownSelectComponent,
    FeaturePlayerComponent,
    ToggleSwitchComponent,
    ActiveSeasonsComponent,
    SeasonAdminListComponent,
    CreateSeasonComponent,
    SeasonNotificationComponent,
  ],
  imports: [
    SwiperModule,
    I18nModule,
    EsportsSharedModule,
    EsportsLoaderModule.setColor('#1d252d'),
    WYSIWYGEditorModule,
    NgbModule,
  ],
  exports: [
    SwiperModule,
    SeasonAdminListComponent,
    CreateSeasonComponent,
    ActiveSeasonsComponent,
    FeaturePlayerComponent,
    SeasonNotificationComponent,
  ],
  providers: [
    {
      provide: SWIPER_CONFIG,
      useValue: DEFAULT_SWIPER_CONFIG,
    },
  ],
})
export class EsportsSeasonModule {
  public static forRoot(
    environment: any
  ): ModuleWithProviders<EsportsSeasonModule> {
    return {
      ngModule: EsportsSeasonModule,
      providers: [
        I18nModule,
        {
          provide: 'env', // you can also use InjectionToken
          useValue: environment,
        },
      ],
    };
  }
}
