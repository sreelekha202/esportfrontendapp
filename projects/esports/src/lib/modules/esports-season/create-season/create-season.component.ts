import { ActivatedRoute, Router } from '@angular/router';
import { Component, Input, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';

import { WYSIWYGEditorConfig } from '../../WYSIWYG-editor/config';
import { EsportsGameService } from '../../../../lib/services/esport.game.service';
import { EsportsSeasonService } from '../../../services/esports.season.service';
import { EsportsToastService } from '../../../services/esports.toast.service';
import { EsportsUserService } from '../../../services/esport.user.service';
import { Location } from '@angular/common';
import { MatDialog } from '@angular/material/dialog';
import { S3UploadService } from '../../../services/esport.s3upload.service';
import { SwiperConfigInterface } from 'ngx-swiper-wrapper';
import { TranslateService } from '@ngx-translate/core';
import { faTrash } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'esports-create-season',
  templateUrl: './create-season.component.html',
  styleUrls: ['./create-season.component.scss'],
})
export class CreateSeasonComponent implements OnInit {
  @Input() API;
  @Input() seasonId;
  minStartDateValue = this.setMinDate();
  maxCheckInOpenDateValue: any;
  minCheckInClosingDateValue = this.setMinDate();
  maxCheckInClosingDateValue: any;
  enableteamCounter = false;
  faTrash = faTrash;
  editorConfig: WYSIWYGEditorConfig = {};
  ediDirection = '';

  createForm: FormGroup;
  bannerSRC;
  showLoader = false;
  isControlDisabled = false;
  isLoading = false;
  updateSeasonId: string;
  isArabic: boolean;
  gameArray = [];
  gameArrayDropDown = [];
  platform = [];
  userObject: any = {
    _id: '',
    accessLevel: [],
    email: '',
    fullName: '',
    phoneNumber: '',
  };
  teamFormat = ['', 'Duo', 'Squad', 'X vs X'];
  seasonTypes = [];
  isSeasonEdit = false;
  constructor(
    private router: Router,
    private eSportsToastService: EsportsToastService,
    private fb: FormBuilder,
    private seasonService: EsportsSeasonService,
    public s3Service: S3UploadService,
    private translateService: TranslateService,
    public dialog: MatDialog,
    private gameService: EsportsGameService,
    private activatedRoute: ActivatedRoute
  ) {}

  async ngOnInit() {
    // this.getIdFromRoute();
    this.createFormFn();
    await this.getGames();
    if (this.activatedRoute.snapshot.params['id']) {
      this.getSeasonData(this.activatedRoute.snapshot.params['id']);
      this.isSeasonEdit = true;
      this.updateSeasonId = this.activatedRoute.snapshot.params['id'];
    }
  }

  createFormFn() {
    this.editorConfig = {
      editable: true,
      spellcheck: true,
      height: '200px',
      width: 'auto',
      minWidth: '0',
      translate: 'false',
      enableToolbar: true,
      showToolbar: true,
      defaultParagraphSeparator: '',
      defaultFontName: '',
      defaultFontSize: '',
      fonts: [
        { class: 'arial', name: 'Arial' },
        { class: 'times-new-roman', name: 'Times New Roman' },
        { class: 'calibri', name: 'Calibri' },
        { class: 'redhat-regular', name: 'redhat-regular' },
        { class: 'archivo-black', name: 'archivo-black' },
        { class: 'rock-salt-regular', name: 'rock-salt-regular' },
        { class: 'roboto-bold', name: 'roboto-bold' },
      ],
      customClasses: [
        {
          name: 'quote',
          class: 'quote',
        },
        {
          name: 'redText',
          class: 'redText',
        },
        {
          name: 'titleText',
          class: 'titleText',
          tag: 'h1',
        },
      ],
      sanitize: true,
      toolbarPosition: 'top',
      toolbarHiddenButtons: [['bold', 'italic'], ['fontSize']],
    };

    this.createForm = this.fb.group({
      name: ['', Validators.compose([Validators.required])],
      // email: ['', Validators.compose([Validators.required])],
      image: ['', Validators.compose([Validators.required])],
      description: ['', Validators.compose([Validators.required])],
      gameDetail: ['', Validators.compose([Validators.required])],
      platform: ['', Validators.compose([Validators.required])],
      participantType: ['', Validators.compose([Validators.required])],
      teamFormat: ['', Validators.compose([Validators.required])],
      duration: ['', Validators.compose([Validators.required])],
      isRecurring: [false, Validators.compose([Validators.required])],
      isFeatured: [false, Validators.compose([Validators.required])],
      // allowSubstituteMember: [false, Validators.compose([Validators.required])],
      startDate: ['', Validators.compose([Validators.required])],
      endDate: [
        { value: '', disabled: true },
        Validators.compose([Validators.required]),
      ],
      cooldownPeriod: [0],
      // substituteMemberSize: [0],
      teamSize: [0],
    });
  }
  setMinDate() {
    let date = new Date();
    let year: any = date.getFullYear();
    let month: any = date.getMonth() + 1;
    month = month < 10 ? '0' + month : month;
    let day: any = date.getDate();
    day = day < 10 ? '0' + day : day;
    let d = `${year}-${month}-${day}`;
    return d;
  }
  selectFile(event) {
    this.isValidImage(event, { height: 560, width: 1088 });
  }
  teamFormatHandler = (event, field) => {
    if (event == '1') {
      this.createForm.get(field).setValue(2);
      this.enableteamCounter = false;
    } else if (event == '2') {
      this.createForm.get(field).setValue(4);
      this.enableteamCounter = false;
    } else if (event == '3') {
      this.enableteamCounter = true;
    }
  };
  setPlatform(index) {
    this.platform = this.gameArray[index]?.platform;
  }

  isValidImage(event: any, values): Boolean {
    if (event.target.files && event.target.files[0]) {
      if (event.target.files[0]['size'] / (1000 * 1000) > 2) {
        this.eSportsToastService.showError(
          this.translateService.instant(
            'ADMIN.SEASON_MANAGEMENT.ERRORS.IMG_LESS'
          )
        );
        return;
      }

      if (
        event.target.files[0].type != 'image/jpeg' &&
        event.target.files[0].type != 'image/jpg' &&
        event.target.files[0].type != 'image/png'
      ) {
        this.eSportsToastService.showError(
          this.translateService.instant(
            'ADMIN.SEASON_MANAGEMENT.ERRORS.UPLOAD_CORRECT'
          )
        );
        return;
      }

      try {
        const file = event.target.files[0];

        const reader = new FileReader();
        reader.readAsDataURL(file);
        let th = this;
        reader.onload = (e) => {
          let image: any = new Image();
          image.src = e.target.result;
          image.onload = function () {
            let height = this.height;
            let width = this.width;
            if (height == values['height'] && width == values['width']) {
              th.eSportsToastService.showInfo(
                this.translateService.instant(
                  'ADMIN.SEASON_MANAGEMENT.ERRORS.PROPER'
                )
              );
            }
            th.upload(event);
            th.readURL(event);
          };
        };
      } catch (err) {
        return false;
      }
    } else {
      return false;
    }
  }
  getGames = async () => {
    try {
      const encodeUrl = `?query=${encodeURIComponent(
        JSON.stringify({
          isTournamentAllowed: true,
          searchBy: '',
        })
      )}`;
      const { data } = await this.gameService
        .getAllGames(this.API, encodeUrl)
        .toPromise();
      if (Array.isArray(data)) {
        if (this.isArabic) {
          this.gameArray = data.reverse();
        } else {
          this.gameArray = data;
        }
        this.gameArray.unshift({
          name: '',
        });
        this.gameArray.map((x) => this.gameArrayDropDown.push(x.name));
        this.seasonTypes = ['', 'team', 'individual'];
      }
      this.showLoader = false;
    } catch (error) {
      this.showLoader = false;
      this.eSportsToastService.showError(
        error?.error?.message || error?.message
      );
    }
  };
  async upload(event) {
    this.showLoader = true;
    const files = event.target.files;
    let promises = [];
    for (let obj of files) {
      promises.push(this.seasonService.toBase64(obj));
    }
    let Base64String = await Promise.all(promises);
    let imageData = {
      path: 'season',
      files: Base64String,
    };

    this.s3Service.fileUpload(this.API, imageData).subscribe(
      (res) => {
        this.showLoader = false;
        if (
          res &&
          res['data'] &&
          res['data'][0] &&
          res['data'][0]['Location']
        ) {
          this.createForm.controls.image.setValue(res['data'][0]['Location']);
          this.eSportsToastService.showSuccess(
            this.translateService.instant('ARTICLE_POST.BANNER_UPLOAD')
          );
        }
      },
      (err) => {
        this.showLoader = false;
        if (
          err &&
          err.error &&
          err.error.statusCode &&
          err.error.statusCode == 401
        ) {
          this.eSportsToastService.showError(
            this.translateService.instant('ARTICLE_POST.SESSION_EXPIRED')
          );
        } else {
          this.eSportsToastService.showError(
            this.translateService.instant('ARTICLE_POST.ERROR_BANNER')
          );
        }
      }
    );
  }
  readURL(event: any): void {
    if (event.target.files && event.target.files[0]) {
      const file = event.target.files[0];

      const reader = new FileReader();
      reader.onload = (e) => (this.bannerSRC = reader.result);

      reader.readAsDataURL(file);
    }
  }
  setEndDateByDuration() {
    if (this.createForm.controls.startDate.value) {
      this.createForm.controls.endDate.setValue(
        this.setEndDate(
          this.createForm.controls.startDate.value,
          this.createForm.value.duration
        )
      );
    } else {
      this.createForm.controls.endDate.setValue(
        this.setEndDate(new Date(), this.createForm.value.duration)
      );
    }
  }
  setEndDateByStartTime(env) {
    this.createForm.controls.endDate.setValue(
      this.setEndDate(env.value, this.createForm.value.duration)
    );
  }
  setEndDate(date, duration) {
    return new Date(date.getTime() + duration * 24 * 60 * 60 * 1000);
  }
  removeBanner() {
    this.bannerSRC = '';
    this.createForm.controls.image.setValue('');
  }

  public hasError = (controlName: string, errorName: string) => {
    return this.createForm.controls[controlName].hasError(errorName);
  };
  goBack() {
    this.router.navigate(['/admin/season-management']);
  }
  async getSeasonData(id) {
    let query = `isActive=${true}&_id=${id}&fields=isFeatured,name,slug,platform,teamFormat,teamSize,isRecurring,rule,description,participantType,startDate,image,game,endDate,duration,cooldownPeriod,createdOn&sort=-createdOn&page=1&limit=20`;
    this.seasonService.getSeasons(this.API, query).subscribe((data) => {
      const seasonData = {
        name: data.data.data.docs[0].name,
        image: data.data.data.docs[0].image,
        gameDetail: this.gameArray.findIndex(
          (game) => game._id == data.data.data.docs[0].game['_id']
        ),
        participantType: this.seasonTypes.findIndex(
          (type) => type == data.data.data.docs[0].participantType
        ),
        teamFormat: this.teamFormat.findIndex(
          (format) => format == data.data.data.docs[0].teamFormat
        ),
        duration: data.data.data.docs[0].duration,
        platform: data.data.data.docs[0].platform[0],
        isRecurring: data.data.data.docs[0].isRecurring,
        startDate: data.data.data.docs[0].startDate,
        endDate: data.data.data.docs[0].endDate,
        cooldownPeriod: data.data.data.docs[0].cooldownPeriod,
        description: data.data.data.docs[0].description,
        // allowSubstituteMember:data.data.data.docs[0].allowSubstituteMember,
        // substituteMemberSize:data.data.data.docs[0].substituteMemberSize,
        teamSize: data.data.data.docs[0].teamSize,
        isFeatured: data.data.data.docs[0].isFeatured,
      };
      this.setPlatform(seasonData.gameDetail);
      if (seasonData.teamFormat == 3) {
        this.enableteamCounter = true;
      }
      this.createForm.patchValue(seasonData);
      this.createForm.controls.participantType.setValue(
        seasonData.participantType
      );
      this.bannerSRC = data.data.data.docs[0].image;
    });
  }
  async saveSeason() {
    try {
      if (this.userObject.accessLevel.length == 0) {
      }
      const platform = [];
      platform.push(this.createForm.controls['platform'].value);
      const _seasonObject = {
        name: this.createForm.controls['name'].value,
        image: this.createForm.controls['image'].value,
        game: this.gameArray[this.createForm.controls['gameDetail'].value][
          '_id'
        ],
        duration: this.createForm.controls['duration'].value,
        platform: platform,
        isRecurring: this.createForm.controls['isRecurring'].value,
        startDate: this.createForm.controls['startDate'].value,
        endDate: this.createForm.controls['endDate'].value,
        cooldownPeriod: this.createForm.controls['cooldownPeriod'].value,
        participantType:
          this.seasonTypes[this.createForm.controls['participantType'].value],
        description: this.createForm.controls['description'].value,
        teamFormat:
          this.teamFormat[this.createForm.controls['teamFormat'].value],
        // allowSubstituteMember:this.createForm.controls['allowSubstituteMember'].value,
        // substituteMemberSize:this.createForm.controls['substituteMemberSize'].value,
        teamSize: this.createForm.controls['teamSize'].value,
        isFeatured: this.createForm.controls['isFeatured'].value,
      };
      const res = await this.seasonService
        .saveSeason(this.API, _seasonObject)
        .toPromise();
      if (res?.success) {
        this.eSportsToastService.showSuccess(
          this.translateService.instant('GAMES.SEASONS.SEASON_CREATED')
        );
        this.goBack();
      } else {
        this.eSportsToastService.showError(
          this.translateService.instant('GAMES.SEASONS.SEASON_ERROR')
        );
      }
      this.isLoading = true;
    } catch (error) {
      this.eSportsToastService.showError(error?.error?.message);
    }
  }
  async updateSeason() {
    const platform = [];
    platform.push(this.createForm.controls['platform'].value);
    const _seasonObject = {
      name: this.createForm.controls['name'].value,
      image: this.createForm.controls['image'].value,
      game: this.gameArray[this.createForm.controls['gameDetail'].value]['_id'],
      duration: this.createForm.controls['duration'].value,
      platform: platform,
      isRecurring: this.createForm.controls['isRecurring'].value,
      startDate: this.createForm.controls['startDate'].value,
      endDate: this.createForm.controls['endDate'].value,
      cooldownPeriod: this.createForm.controls['cooldownPeriod'].value,
      participantType:
        this.seasonTypes[this.createForm.controls['participantType'].value],
      description: this.createForm.controls['description'].value,
      teamFormat: this.createForm.controls['teamFormat'].value,
      // allowSubstituteMember:this.createForm.controls['allowSubstituteMember'].value,
      // substituteMemberSize:this.createForm.controls['substituteMemberSize'].value,
      teamSize: this.createForm.controls['teamSize'].value,
      id: this.updateSeasonId,
      isFeatured: this.createForm.controls['isFeatured'].value,
    };
    const res = await this.seasonService
      .updateSeason(this.API, _seasonObject)
      .toPromise();
    if (res.success) {
      this.eSportsToastService.showSuccess(
        this.translateService.instant('GAMES.SEASONS.SEASON_UPDATED')
      );
      this.goBack();
    } else {
      this.eSportsToastService.showError(
        this.translateService.instant('GAMES.SEASONS.SEASON_UPDATE_ERROR')
      );
    }
    this.isLoading = true;
  }
  teamFormatSize(size) {
    if (size == '2') this.createForm.controls.teamSize.setValue(1);
    this.createForm.controls.teamFormat.setValue('individual');
  }
}
