import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormGroupDirective, Validators } from '@angular/forms';
import { EsportsSeasonService } from '../../../services/esports.season.service';
import { EsportsNotificationsService } from '../../../services/esport.notification.service';
import { EsportsToastService } from '../../../services/esports.toast.service';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'esports-season-notification',
    templateUrl: './season-notification.component.html',
})
export class SeasonNotificationComponent implements OnInit {

    isLoading: boolean = false;
    seasonsArray = [];
    selectedSeason;
    pushNotificationForm = this.fb.group({
        message: ['', Validators.required],
        title: ['', Validators.required],
        season: ['', Validators.required],
    });
    @Input() api;
    constructor(
        private fb: FormBuilder,
        private seasonService: EsportsSeasonService,
        private userNotificationsService: EsportsNotificationsService,
        private eSportsToastService: EsportsToastService
    ) { }
    ngOnInit(): void {
        this.allSeasonList();
    }

    selectSeason(season) {
        this.selectedSeason = season;
        this.pushNotificationForm.controls['season'].setValue(season._id);
    }

    allSeasonList() {
        let query = `isActive=true&fields=isFeatured,name,slug,endDate,image&sort=-createdOn&page=1`;
        this.seasonService.getSeasons(this.api, query).subscribe((data) => {
            this.seasonsArray = data?.data?.data?.docs;
        });
    }

    updatePush(form: FormGroup,
        formDirective: FormGroupDirective) {
        if (!this.pushNotificationForm.valid) {
            return;
        }
        const inputData = {
            message: this.pushNotificationForm.value.message,
            title: this.pushNotificationForm.value.title,
            seasonId: this.pushNotificationForm.value.season
        };
        this.isLoading = true;
        this.userNotificationsService
            .sendPushNotifications(this.api, inputData)
            .pipe(
                finalize(() => {
                    this.isLoading = false;
                })
            )
            .subscribe(
                (res: any) => {
                    if (res) {
                        formDirective.resetForm();
                        this.selectedSeason = null;
                        this.eSportsToastService.showSuccess(res?.message);
                    }
                },
                (err: any) => {
                    formDirective.resetForm();
                    this.selectedSeason = null;
                    this.eSportsToastService.showError(
                        err?.error?.message || err?.message
                    );
                }
            );
    }

}
