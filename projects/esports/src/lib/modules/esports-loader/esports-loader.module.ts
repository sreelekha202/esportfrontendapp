import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// components
import { LoadingComponent } from './loading/loading.component';
import { FullscreenLoadingComponent } from './fullscreen-loading/fullscreen-loading.component';

@NgModule({
  declarations: [
    LoadingComponent,
    FullscreenLoadingComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    LoadingComponent,
    FullscreenLoadingComponent
  ],
})
export class EsportsLoaderModule {
  public static setColor(color: string): ModuleWithProviders<EsportsLoaderModule> {
    return {
        ngModule: EsportsLoaderModule,
        providers: [
          {
              provide: 'color', // you can also use InjectionToken
              useValue: color
          }
        ]
    }
  }
}
