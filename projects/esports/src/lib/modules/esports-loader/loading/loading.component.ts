import { Component, Inject, OnInit } from '@angular/core';

@Component({
  selector: 'esports-loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.scss'],
})
export class LoadingComponent implements OnInit {
  
  constructor(
    @Inject('color') public color
  ) {}

  ngOnInit(): void {}
}
