import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { I18nModule } from '../../i18n/i18n.module';
import { EsportsCustomPaginationModule } from '../esports-custom-pagination/esport-custom-pagination.module';
import { EsportsLoaderModule } from '../esports-loader/esports-loader.module';
import { LaderStandingComponent } from './lader-standing/lader-standing.component';
import { PipeModule } from '../../pipes/pipe.module';
@NgModule({
  declarations: [LaderStandingComponent],
  imports: [
    EsportsLoaderModule,
    I18nModule,
    CommonModule,
    EsportsCustomPaginationModule,
    PipeModule,
  ],
  exports: [LaderStandingComponent],
})
export class EsportsEliminationModule {
  public static forRoot(
    environment: any
  ): ModuleWithProviders<EsportsEliminationModule> {
    return {
      ngModule: EsportsEliminationModule,
      providers: [
        I18nModule,
        {
          provide: 'env', // you can also use InjectionToken
          useValue: environment,
        },
      ],
    };
  }
}
