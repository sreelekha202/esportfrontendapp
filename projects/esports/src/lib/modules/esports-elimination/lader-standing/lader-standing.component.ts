import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { IPagination } from '../../../models/paginations';
import { EsportsLanguageService } from '../../../services/esport.language.service';
import { EsportsTournamentService } from '../../../services/esport.tournament.service';

@Component({
  selector: 'esports-lader-standing',
  templateUrl: './lader-standing.component.html',
  styleUrls: [],
})
export class LaderStandingComponent implements OnInit {
  @Input() tournamentId;
  @Input() tournamentType;
  @Input() api;
  isProccessing = false;
  standing = [];

  your_rank: any = {};
  pageNo = 1;
  page: IPagination;
  constructor(
    public languageService: EsportsLanguageService,
    private translateService: TranslateService,
    private tournamnet: EsportsTournamentService
  ) {}

  ngOnInit(): void {
    if (this.tournamentType === 'ladder') {
      this.fetchdata();
    }
  }

  fetchdata = async () => {
    try {
      this.isProccessing = true;
      const parms = {
        id: this.tournamentId,
        page: this.pageNo,
      };
      const response = await this.tournamnet
        .getLaderStanding(this.api, parms)
        .toPromise();
      this.standing = response?.data?.standing?.docs;
      this.your_rank = response?.data?.your_rank;
      this.page = {
        totalItems: response?.data?.standing?.totalDocs,
        itemsPerPage: response?.data?.standing?.limit,
        maxSize: 5,
      };
      this.isProccessing = false;
    } catch (error) {
      this.isProccessing = false;
    }
  };

  pageChanged(page): void {
    this.pageNo = page;
    this.fetchdata();
  }
}
