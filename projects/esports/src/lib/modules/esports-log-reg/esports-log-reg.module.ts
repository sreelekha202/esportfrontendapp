import { ModuleWithProviders, NgModule } from '@angular/core';
import { I18nModule } from '../../i18n/i18n.module';
import { SelectProfileComponent } from './select-profile/select-profile.component';

@NgModule({
  declarations: [SelectProfileComponent],
  imports: [I18nModule],
  exports: [SelectProfileComponent],
})
export class EsportsLogRegModule {
  public static forRoot(
    environment: any
  ): ModuleWithProviders<EsportsLogRegModule> {
    return {
      ngModule: EsportsLogRegModule,
      providers: [
        I18nModule,
        {
          provide: 'env', // you can also use InjectionToken
          useValue: environment,
        },
      ],
    };
  }
}
