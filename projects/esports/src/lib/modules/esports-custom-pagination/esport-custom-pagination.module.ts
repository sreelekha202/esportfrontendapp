import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';

import { EsportsCustomPaginationComponent } from './esport-custom-pagination.component';
import { PaginationModule, PaginationConfig } from 'ngx-bootstrap/pagination';

@NgModule({
  declarations: [EsportsCustomPaginationComponent],
  imports: [CommonModule, PaginationModule.forRoot(), FormsModule],
  exports: [EsportsCustomPaginationComponent],
  providers: [{ provide: PaginationConfig }],
})
export class EsportsCustomPaginationModule {}
