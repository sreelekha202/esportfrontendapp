import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';

import { IPagination } from '../../models/paginations';
import { PageChangedEvent } from 'ngx-bootstrap/pagination';
import { EsportsPaginationService } from '../../services/esports.pagination.service';

@Component({
  selector: 'esports-pagination',
  templateUrl: './esport-custom-pagination.component.html',
  styleUrls: ['./esport-custom-pagination.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EsportsCustomPaginationComponent implements OnInit, OnChanges {
  @Input() activePage = 1;
  @Input() page: IPagination;
  @Output() currentPage = new EventEmitter<Number>();

  constructor(private paginationService: EsportsPaginationService) {}

  ngOnInit(): void {}

  pageChanged(event: PageChangedEvent) {
    this.currentPage.emit(event.page);
    this.paginationService.setPageChanged(true);
  }

  ngOnChanges(simpleChanges: SimpleChanges) {}
}
