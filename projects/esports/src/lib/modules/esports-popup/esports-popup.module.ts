import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { I18nModule } from '../../i18n/i18n.module';
import { EsportsSharedModule } from '../../shared/module/shared.module';
import { EsportsEditPopupComponent } from './esports-edit-popup/esports-edit-popup.component';
import { EsportsInfoPopupComponent } from './esports-info-popup/esports-info-popup.component';

@NgModule({
  declarations: [EsportsInfoPopupComponent, EsportsEditPopupComponent],
  imports: [CommonModule, I18nModule, EsportsSharedModule, ],
  exports: [EsportsInfoPopupComponent, EsportsEditPopupComponent],
  providers: [],
})
export class EsportsPopupModule {
  public static forRoot(
    environment: any
  ): ModuleWithProviders<EsportsPopupModule> {
    return {
      ngModule: EsportsPopupModule,
      providers: [
        I18nModule,
        {
          provide: 'env', // you can also use InjectionToken
          useValue: environment,
        },
      ],
    };
  }
}
