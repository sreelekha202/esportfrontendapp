import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Component, Inject, OnInit } from '@angular/core';

export enum EsportsInfoPopupComponentType {
  info,
  editInfo
}

export interface EsportsInfoPopupComponentData {
  btnText?: string;
  cancelBtnText?: string;
  text: string;
  title: string;
  type: EsportsInfoPopupComponentType;
  label?:string;
  inputType?:string;
  isCancelledRequired?:boolean;
  
}

@Component({
  selector: 'esports-info-popup',
  templateUrl: './esports-info-popup.component.html',
  styleUrls: ['./esports-info-popup.component.scss'],
})
export class EsportsInfoPopupComponent implements OnInit {
  InfoPopupComponentType = EsportsInfoPopupComponentType;
  inputValue;
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: EsportsInfoPopupComponentData,
    public dialogRef: MatDialogRef<EsportsInfoPopupComponent>
  ) {}

  ngOnInit(): void {}

  onClose(): void {
    this.dialogRef.close();
  }

  onKey(event) {this.inputValue = event.target.value;}

  onConfirm(): void {
    this.dialogRef.close(true);
  }
  onConfirm2(): void {
    this.dialogRef.close(this.inputValue);
  }
}
