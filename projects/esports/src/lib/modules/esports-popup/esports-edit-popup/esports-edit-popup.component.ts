import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { EsportsTournamentService } from '../../../services/esport.tournament.service';

@Component({
  selector: 'esports-edit-popup',
  templateUrl: './esports-edit-popup.component.html',
  styleUrls: ['./esports-edit-popup.component.scss'],
})
export class EsportsEditPopupComponent implements OnInit {
  games = [];
  platforms = [];
  showDropdown: boolean = false;

  tournamentForm: FormGroup;
  constructor(
    @Inject(MAT_DIALOG_DATA) public data,
    public dialogRef: MatDialogRef<EsportsEditPopupComponent>,
    private tournamentService: EsportsTournamentService,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.getGames();
    this._initiateTournament();
  }

  _initiateTournament() {
    this.tournamentForm = this.fb.group({
      tournamentName: [''],
      game: [''],
      platform: [''],
      tournamentType: [''],
      visibility: [''],
      tournamentNameTextarea: [''],
    });

    this.tournamentForm
      .get('tournamentName')
      .patchValue(this.data?.tournamentName);
    if (this.data?.selectedGame) {
      this.tournamentForm.get('game').patchValue(this.data?.selectedGame._id);
    }
    this.tournamentForm
      .get('platform')
      .patchValue(this.data?.selectedPlatformIndex);
    this.tournamentForm
      .get('tournamentType')
      .patchValue(this.data?.tournamentType.type);
    // this.tournamentForm.get('visibility').patchValue(this.data.visibility);
    this.tournamentForm
      .get('tournamentNameTextarea')
      .patchValue(this.data?.tournamentNameTextarea);
    this.tournamentForm.controls['visibility'].setValue(
      this.data['visibility'].toString()
    );
  }

  getGames() {
    const param: any = {};
    this.tournamentService.getAllGames(param).subscribe(
      (res) => {
        if (res && res.data && res.data.length) {
          res.data.map((obj) => {
            this.games.push({ ...obj, name: String(obj.name).toLowerCase() });
          });
        }
      },
      (err) => {}
    );

    if (
      this.data?.selectedGame &&
      this.data?.selectedGame.platform &&
      this.data?.selectedGame.platform.length > 0
    )
      this.data?.selectedGame.platform.map((obj) => {
        if (obj.name == 'pc') {
          this.platforms.push({
            ...obj,
            type: 'pc',
            icon: 'assets/icons/matchmaking/platforms/pc.svg',
            title: 'MATCHMAKING.SELECT_PLATFORM.PLATFORMS.PC',
          });
        } else if (obj.name == 'Mobile') {
          this.platforms.push({
            ...obj,
            type: 'mobile',
            icon: 'assets/icons/matchmaking/platforms/mobile.svg',
            title: 'MATCHMAKING.SELECT_PLATFORM.PLATFORMS.MOBILE',
          });
        } else if (obj.name == 'Other') {
          this.platforms.push({
            ...obj,
            type: 'console',
            icon: 'assets/icons/matchmaking/platforms/console.svg',
            title: 'MATCHMAKING.SELECT_PLATFORM.PLATFORMS.CONSOLE',
          });
        } else if (obj.name == 'Xbox') {
          this.platforms.push({
            ...obj,
            type: 'xbox',
            icon: 'assets/icons/matchmaking/platforms/console.svg',
            title: 'MATCHMAKING.SELECT_PLATFORM.PLATFORMS.XBOX',
          });
        } else if (obj.name == 'PS4') {
          this.platforms.push({
            ...obj,
            type: 'ps4',
            icon: 'assets/icons/matchmaking/platforms/console.svg',
            title: 'MATCHMAKING.SELECT_PLATFORM.PLATFORMS.PS4',
          });
        } else if (obj.name == 'PS5') {
          this.platforms.push({
            ...obj,
            type: 'ps5',
            icon: 'assets/icons/matchmaking/platforms/console.svg',
            title: 'MATCHMAKING.SELECT_PLATFORM.PLATFORMS.PS5',
          });
        } else {
          this.platforms.push({
            ...obj,
            type: 'pc',
            icon: 'assets/icons/matchmaking/platforms/pc.svg',
            title: 'MATCHMAKING.SELECT_PLATFORM.PLATFORMS.PC',
          });
        }
      });
  }

  eventshowDropdown() {
    this.showDropdown = !this.showDropdown;
  }

  onClose(): void {
    this.dialogRef.close();
  }

  onConfirm(): void {
    let newData = {
      selectedGame: this.games.filter(
        (x) => x._id === this.tournamentForm.value.game
      )[0],
      selectedPlatformIndex: this.tournamentForm.value.platform,
      selected_platform: this.tournamentForm.value.game ? this.games.filter(
        (x) => x._id === this.tournamentForm.value.game
      )[0].platform[this.tournamentForm.value?.platform] : '',
      tournamentType: {
        type: this.tournamentForm.value.tournamentType,
      },
      visibility: this.tournamentForm.value.visibility,
      tournamentNameTextarea: this.tournamentForm.value.tournamentNameTextarea,
      tournamentName: this.tournamentForm.value.tournamentName,
    };
    this.dialogRef.close(newData);
  }

  gamechange() {
    let selectGame = this.games.filter(
      (x) => x._id === this.tournamentForm.get('game').value
    )[0];
    this.platforms.length = 0;
    selectGame.platform.map((obj) => {
      if (obj.name == 'pc') {
        this.platforms.push({
          ...obj,
          type: 'pc',
          icon: 'assets/icons/matchmaking/platforms/pc.svg',
          title: 'MATCHMAKING.SELECT_PLATFORM.PLATFORMS.PC',
        });
      } else if (obj.name == 'Mobile') {
        this.platforms.push({
          ...obj,
          type: 'mobile',
          icon: 'assets/icons/matchmaking/platforms/mobile.svg',
          title: 'MATCHMAKING.SELECT_PLATFORM.PLATFORMS.MOBILE',
        });
      } else if (obj.name == 'Other') {
        this.platforms.push({
          ...obj,
          type: 'console',
          icon: 'assets/icons/matchmaking/platforms/console.svg',
          title: 'MATCHMAKING.SELECT_PLATFORM.PLATFORMS.CONSOLE',
        });
      } else if (obj.name == 'Xbox') {
        this.platforms.push({
          ...obj,
          type: 'xbox',
          icon: 'assets/icons/matchmaking/platforms/console.svg',
          title: 'MATCHMAKING.SELECT_PLATFORM.PLATFORMS.XBOX',
        });
      } else if (obj.name == 'PS4') {
        this.platforms.push({
          ...obj,
          type: 'ps4',
          icon: 'assets/icons/matchmaking/platforms/console.svg',
          title: 'MATCHMAKING.SELECT_PLATFORM.PLATFORMS.PS4',
        });
      } else if (obj.name == 'PS5') {
        this.platforms.push({
          ...obj,
          type: 'ps5',
          icon: 'assets/icons/matchmaking/platforms/console.svg',
          title: 'MATCHMAKING.SELECT_PLATFORM.PLATFORMS.PS5',
        });
      } else {
        this.platforms.push({
          ...obj,
          type: 'pc',
          icon: 'assets/icons/matchmaking/platforms/pc.svg',
          title: 'MATCHMAKING.SELECT_PLATFORM.PLATFORMS.PC',
        });
      }
    });
    this.tournamentForm.get('platform').patchValue(0);
  }
}
