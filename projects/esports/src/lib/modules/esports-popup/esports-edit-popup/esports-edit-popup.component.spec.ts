import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EsportsEditPopupComponent } from './esports-edit-popup.component';

describe('PopupComponent', () => {
  let component: EsportsEditPopupComponent;
  let fixture: ComponentFixture<EsportsEditPopupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EsportsEditPopupComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EsportsEditPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
