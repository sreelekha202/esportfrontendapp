import { Pipe, PipeTransform } from '@angular/core';
import { EsportsLanguageService } from '../services/esport.language.service';
import { EsportsConstantsService } from '../services/esport.constants.service';

@Pipe({
  name: 'customTranslate',
})
export class CustomTranslatePipe implements PipeTransform {
  currLanguage;
  constructor(
    public language: EsportsLanguageService,
    public ConstantsService: EsportsConstantsService
  ) {
    this.language.language.subscribe((langCode) => {
      if (langCode) {
        this.currLanguage = this.ConstantsService?.language.find(
          (el) => el.code == langCode
        )?.key;
      }
    });
  }
  transform(value, language?) {
    if (value) {
      return !value
        ? ''
        : !(typeof value === 'object')
        ? value
        : value[this.currLanguage] || this.fetchDefaultValues(value);
    } else {
      return '';
    }
  }

  fetchDefaultValues(value) {
    for (const item of this.ConstantsService?.language) {
      if (value[item.key]) {
        return value[item.key];
      }
    }
    return '';
  }
}
