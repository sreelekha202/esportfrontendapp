import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'articleStatus',
})
export class ArticleStatusPipe implements PipeTransform {
  transform(value) {
    switch (value) {
      case 'draft':
        return 'PROFILE.CONTENTS.ARTICLE.STATUS.DRAFT';
      case 'submitted_for_approval':
        return 'PROFILE.CONTENTS.ARTICLE.STATUS.INAPPROVAL';
      case 'request_edit':
        return 'PROFILE.CONTENTS.ARTICLE.STATUS.EDIT';
      case 'publish':
        return 'PROFILE.CONTENTS.ARTICLE.STATUS.PUBLISH';
      case 'deleted':
        return 'PROFILE.CONTENTS.ARTICLE.STATUS.DELETED';
      default:
        return 'N/A';
    }
  }
}
