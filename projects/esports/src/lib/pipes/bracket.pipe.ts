import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'bracket',
})
export class BracketPipe implements PipeTransform {
  transform(value) {
    switch (value) {
      case 'single':
        return 'OPTIONS.BRACKET_TYPE.SINGLE';
      case 'double':
        return 'OPTIONS.BRACKET_TYPE.DOUBLE';
      case 'round_robin':
        return 'OPTIONS.BRACKET_TYPE.ROUND_ROBIN';
      case 'battle_royale':
        return 'OPTIONS.BRACKET_TYPE.BATTLE_ROYALE';
      case 'swiss_safeis':
        return 'OPTIONS.BRACKET_TYPE.SWISS_SYSTEM';
      case 'ladder':
        return 'OPTIONS.BRACKET_TYPE.LADDER';
      default:
        return 'N/A';
    }
  }
}
