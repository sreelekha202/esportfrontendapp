import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'customStatus',
})
export class CustomStatusPipe implements PipeTransform {
  transform(value) {
    switch (value) {
      case 1:
        return 'CUSTOMSTATUS.INITIATED';
        break;
      case 2:
        return 'CUSTOMSTATUS.PASSED';
        break;
      case 3:
        return 'CUSTOMSTATUS.CANCEL';
        break;
      case 4:
        return 'CUSTOMSTATUS.FAILED';
        break;
      default:
        return 'CUSTOMSTATUS.INITIATED';
        break;
    }
    return null;
  }
}
