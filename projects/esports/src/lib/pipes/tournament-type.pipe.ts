import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'tournamentType',
})
export class TournamentTypePipe implements PipeTransform {
  transform(value) {
    switch (value) {
      case 'online':
        return 'CREATE_TOURNAMENT.TOURNAMENT_TYPE.ONLINE';
      case 'offline':
        return 'CREATE_TOURNAMENT.TOURNAMENT_TYPE.OFFLINE';
      case 'hybrid':
        return 'CREATE_TOURNAMENT.TOURNAMENT_TYPE.HYBRID';
      default:
        return 'N/A';
    }
  }
}
