import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'limitString',
})
export class limitStringPipe implements PipeTransform {
  transform(content: string, length: number, tail: string, innerWidth?) {
    if(!content){
      return "";
    }
    if (isNaN(length)) {
      length = 40;
    }
    if (tail === undefined) {
      tail = '...';
    }
    if (content.length <= length || content.length - tail.length <= length) {
      return content;
    } else {
      if(innerWidth){
      if (!isNaN(innerWidth)) {
        if (innerWidth >= 1040) {
          length = length - length / 70;
        }
        if (innerWidth >= 1024 && innerWidth < 1440) {
          length = length - length / 60;
        }
        if (innerWidth >= 768 && innerWidth < 1024) {
          length = length - length / 50;
        }
        // if (innerWidth >= 425 && innerWidth < 768) {
        //   length = length - length / 4.5;
        // }
        // if (innerWidth >= 375 && innerWidth < 425) {
        //   length = length - length / 3;
        // }
        // if (innerWidth >= 320 && innerWidth < 375) {
        //   length = length - length / 2.3;
        // }
      }}
      return String(content).substring(0, length + tail.length) + tail;
    }
  }
}
//Syntax to use
//{{ randomText | limitChar }} default charater length is 40
//{{ randomText | limitChar:10 }}  charater length is 10
//{{ randomText | limitChar:10:"...!!!" }} we can set limit specific as well as we can specify last char as we want.
