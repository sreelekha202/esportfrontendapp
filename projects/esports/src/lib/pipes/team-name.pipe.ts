import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'teamName',
})
export class TeamNamePipe implements PipeTransform {
  transform(value, type) {
    return type == 'individual'
      ? value?.name || value?.teamName
      : value?.teamName;
  }
}
