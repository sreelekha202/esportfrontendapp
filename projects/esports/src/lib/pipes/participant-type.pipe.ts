import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'pType',
})
export class ParticipantTypePipe implements PipeTransform {
  transform(value, size, bracketType?) {
    if (bracketType == 'swiss_safeis') {
      if (value == 'individual') {
        return 'HOME.SECTIONB.SWISS_CARD';
      } else {
        return 'HOME.SECTIONB.SWISS_CARD';
      }
    }
    switch (value) {
      case 'individual':
        return size <= 1
          ? 'HOME.SECTIONB.PLAYER_TOURNAMENT_CARD'
          : 'HOME.SECTIONB.PLAYER_TOURNAMENT_CARD';
      case 'team':
        return size <= 1
          ? 'HOME.SECTIONB.TEAM_TOURNAMENT_CARD'
          : 'HOME.SECTIONB.TEAM_TOURNAMENT_CARD';
      default:
        return 'N/A';
    }
  }
}
