import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'nameById',
})
export class NameByIdPipe implements PipeTransform {
  transform(_id: string, field: string, list: any) {
    if (_id) {
      const obj = list.find((item) => item._id == _id);
      if (obj?.[field] && obj?.[field].length > 16) {
        return obj?.[field] && obj?.[field].slice(0, 16) + '...';
      } else {
        return (obj?.[field] && obj?.[field]) || _id;
      }
    } else {
      return null;
    }
  }
}
