import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'errorPipe',
})
export class ErrorPipe implements PipeTransform {
  errorString = {
    required: 'ERROR.REQUIRED',
    pastStartTime: 'ERROR.PAST_START_TIME',
    pastStartDate: 'ERROR.PAST_START_DATE',
    pastEndDate: 'ERROR.PAST_END_DATE',
    pastEndTime: 'ERROR.PAST_END_TIME',
    pastRegStartDate: 'ERROR.PAST_REG_START_DATE',
    pastRegStartTime: 'ERROR.PAST_REG_START_TIME',
    pastRegEndTime: 'ERROR.PAST_REG_END_TIME',
    pastRegEndDate: 'ERROR.PAST_REG_END_DATE',
    swissPastRegEndDate: 'ERROR.SWISS_PAST_REG_END_DATE',
    swissPastRegEndTime: 'ERROR.SWISS_PAST_REG_END_TIME',
    ladderPastRegEndDate: 'ERROR.LADDER_PAST_REG_END_DATE',
    ladderPastRegEndTime: 'ERROR.LADDER_PAST_REG_END_TIME',
    ladderRescheduleEndTime:'ERROR.LADDER_RESCHEDULE_END_TIME',
    max: 'ERROR.MAX_',
    min: 'ERROR.MIN_',
    maxlength: 'ERROR.MAX_LENGTH_',
    playStoreUrl: 'ERROR.PLAYSTORE_URL',
    appStoreUrl: 'ERROR.APPSTORE_URL',
    website: 'ERROR.WEBSITE_URL',
    youtubeUrl: 'ERROR.YOUTUBE_URL',
    facebookVideoUrl: 'ERROR.FACEBOOK_VIDEO_URL',
    twitchVideoUrl: 'ERROR.TWITCH_VIDEO_URL',
    pattern: 'ERROR.PATTERN', // UNWANTED PATTERN
    wait: 'ERROR.WAIT',
    prizeLimit: 'ERROR.PRIZELIMIT',
    isNameAvailable: 'ERROR.IS_ALREADY_EXIST',
    isValidPhoneNumber: 'ERROR.PHONE_NUMBER',
    invalidDuration: 'ERROR.INVALID_DURATION',
    wagerPrizeMoneyInvalid: 'ERROR.WAGER_PRIZE_MONEY',
  };

  transform(value, type?): string {
    if (value) {
      const error = Object.keys(value);
      const key = error.length ? error[0] : 'none';
      if (key === 'maxlength') {
        return `${this.errorString[key]}${value?.maxlength?.requiredLength}`;
      } else if (key === 'min') {
        return `${this.errorString[key]}${
          value?.min?.requiredLength || value?.min?.min
        }`;
      } else if (key === 'max') {
        return `${this.errorString[key]}${
          value?.max?.requiredLength || value?.max?.max
        }`;
      } else if (key === 'pattern') {
        return this.errorString[type] || this.errorString.pattern;
      } else if (key === 'validatePhoneNumber') {
        const isValidPhoneNumber = value?.validatePhoneNumber?.valid;
        return isValidPhoneNumber ? null : this.errorString?.isValidPhoneNumber;
      } else {
        return this.errorString[key] || this.errorString.required;
      }
    } else {
      return '';
    }
  }
}
