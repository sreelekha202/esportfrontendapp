import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'region',
})
export class RegionPipe implements PipeTransform {
  transform(value, currLanguage, dropDownList, type) {
    switch (type) {
      case 'state':
        const currState = dropDownList.find((item) => item.name == value);
        return currState?.[currLanguage] || value || '';
      case 'country':
        const country = dropDownList.find((item) => item.name == value);
        return country?.[currLanguage] || value || '';
      default:
        return null;
    }
  }
}
