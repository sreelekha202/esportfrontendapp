import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'remainingDays',
})
export class RemainingDaysPipe implements PipeTransform {
  transform(value) {
    let date1 = new Date();
    let date2 = new Date(value);

    let Difference_In_Time = date2.getTime() - date1.getTime();

    // To calculate the no. of days between two dates
    let Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);

    if (~~Difference_In_Days > 0) {
      return `Ends in ${~~Difference_In_Days} Days`;
    } else {
      return 'Season ended';
    }
  }
}
