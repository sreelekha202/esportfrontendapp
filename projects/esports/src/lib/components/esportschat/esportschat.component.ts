import {
  Component,
  OnInit,
  Input,
  ViewEncapsulation,
  OnDestroy,
  ViewChild,
  ElementRef,
} from '@angular/core';
import {
  trigger,
  state,
  style,
  animate,
  transition,
} from '@angular/animations';

import { EsportsChatService } from '../../services/esport.chat.service';
@Component({
  selector: 'lib-esportschat',
  templateUrl: './esportschat.component.html',
  encapsulation: ViewEncapsulation.None,
  animations: [
    trigger('openClose', [
      state(
        'open',
        style({
          height: '515px',
          opacity: 1,
        })
      ),
      state(
        'closed',
        style({
          height: '0px',
          opacity: 0.5,
        })
      ),
      transition('open => closed', [animate('1s')]),
      transition('closed => open', [animate('0.5s')]),
    ]),
  ],
})
export class EsportschatComponent implements OnInit, OnDestroy {
  @Input() matchdetails: any;
  @Input() typeofchat: any;
  @Input() windowposition: any;
  @Input() showChatComponent: boolean = false;
  @Input() isComponent: boolean = false;
  @ViewChild('lastelement') lastelement: ElementRef;
  constructor(public chatService: EsportsChatService) {}
  ngOnInit(): void {
    this.chatService.chatWindowPos.subscribe((winpos) => {
      this.windowposition = winpos;
    });
  }
  public onClickedOutside(e: Event) {
    this.chatService.setChatStatus(false);
  }
  ngOnDestroy() {

    if (this.matchdetails) {
      this.chatService?.disconnectMatch(
        this.matchdetails?.matchid
          ? this.matchdetails?.matchid
          : this.matchdetails,
        this.typeofchat
      );
    }
  }
}
