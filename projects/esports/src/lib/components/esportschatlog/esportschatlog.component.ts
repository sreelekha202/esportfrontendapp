import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { EsportsChatService } from '../../services/esport.chat.service';

@Component({
  selector: 'lib-esportschatlog',
  templateUrl: './esportschatlog.component.html',
})
export class EsportschatlogComponent implements OnInit {
  @Input() public matchDetails: any;
  @Input() public chats: any;
  windowPositionAbuser: String = 'chat_window chat_window_abuser_drawer';
  @Output() submit = new EventEmitter();
  constructor(public chatService: EsportsChatService) {}

  ngOnInit(): void {}

  showChatLog() {
    let matchid = this.matchDetails._id;

    this.chatService.getMatchInformation(matchid).subscribe((result: any) => {
      const mtchdetails = {
        matchid: result.matchdetails._id,

        teamAId: result.matchdetails.teamA.userId._id,

        teamBId: result.matchdetails.teamB.userId._id,

        tournamentid: result.matchdetails.tournamentId,

        typeofchat: 'tournament',

        users: [
          {
            _id: result.matchdetails.teamA.userId._id,
            fullName: result.matchdetails.teamA.userId.fullName,
            profilePicture: result.matchdetails.teamA.userId.profilePicture,
          },
          {
            _id: result.matchdetails.teamB.userId._id,
            fullName: result.matchdetails.teamB.userId.fullName,
            profilePicture: result.matchdetails.teamB.userId.profilePicture,
          },
        ],
      };

      this.chatService.setWindowPos(this.windowPositionAbuser);
      this.chatService.setCurrentMatch(mtchdetails);
      this.chatService.setChatHistory(this.chats);
      this.chatService.setTypeOfChat('tournamenthistory');
      this.chatService.setTextButtonStatus(false);
      this.chatService.setAdminMessageStatus(true);
      this.chatService.setChatStatus(true);
      if (this.chatService.getChatStatus() == false) {
        this.chatService?.disconnectMatch(matchid, 'chathistory');
      }
    });

    this.submit.emit({ click:true});
  }
}
