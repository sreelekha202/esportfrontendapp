import {
  Component,
  OnInit,
  Input,
  ViewEncapsulation,
  ViewChild,
  ElementRef,
  OnDestroy,
  AfterViewInit,
  Inject,
} from '@angular/core';

import {
  trigger,
  state,
  style,
  animate,
  transition,
} from '@angular/animations';

import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
//import { LanguageService } from '../../../../../core/service';
import { EsportsChatService } from '../../services/esport.chat.service';

import { IChat } from '../../models/chat';
import { forkJoin } from 'rxjs';
import { EsportsUserService } from '../../services/esport.user.service';
import { EsportsLanguageService } from '../../services/esport.language.service';
import { TranslateService } from '@ngx-translate/core';
import { ActivatedRoute } from '@angular/router';
import {
  EsportsGtmService,
  SuperProperties,
  EventProperties,
} from '../../services/esport.gtm.service';

@Component({
  selector: 'lib-chat',
  templateUrl: './chat.component.html',
  encapsulation: ViewEncapsulation.None,
  animations: [
    trigger('openClose', [
      state(
        'open',
        style({
          height: '515px',
          opacity: 1,
        })
      ),
      state(
        'closed',
        style({
          height: '0px',
          opacity: 0.5,
        })
      ),
      transition('open => closed', [animate('1s')]),
      transition('closed => open', [animate('0.5s')]),
    ]),
  ],
})
export class chatComponent implements OnInit, OnDestroy {
  @Input() matchdetails: any;
  @Input() typeofchat: any;
  @Input() windowposition: any;
  @Input() showChatComponent: boolean = false;
  @Input() isComponent: boolean = false;
  @ViewChild('messageinput') messageinput: ElementRef;
  @ViewChild('lastelement') lastelement: ElementRef;
  @ViewChild('messageinputwrapper') inputWrapper: ElementRef;
  @ViewChild('fileInput') fileInput: ElementRef;
  @ViewChild('messagesul') messagesul: ElementRef;

  public isImageInClipBoard: boolean = false;
  public imageElementToRemove: any = '';
  public userObject: any;
  public chats: Array<IChat>;
  public testnumber: any = 1;
  public currentTournamentOwner = {
    id: '',
    name: '',
  };
  public typingmessage: string = '';
  public showProgress: boolean = false;
  public progressValue: number = 0;
  public filesToUpload = [];
  public dummyFiles = [];
  public s3files = [];
  public tid: string = '';
  public subtitle: string = '';
  public profilePicture: string = '';
  public subtitle2: string = '';
  public chattitle: string = '';
  public matchtitle: string = '';
  public chatidtodelete = '';
  public deletereceiverid = '';
  public currenUser: any;
  public currentUserId: any;
  public showTextAndButton: boolean = true;
  public chatAdminFlag: boolean = false;
  public chatLoader: boolean = true;

  constructor(
    @Inject('env') public environment,
    public chatService: EsportsChatService,
    public userService: EsportsUserService,
    public modalService: NgbModal,
    public languageService: EsportsLanguageService,
    public translateService: TranslateService,
    private gtmService: EsportsGtmService,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.chatAdminFlag = this.chatService.getAdminMessageStatus();
    this.userService?.currentUser?.subscribe((data) => {
      if (data) {
        this.userObject = data;
        this.currentUserId = this.userObject._id;
        this.chatService.initialiseSocket();
      }
    });
    this.languageService.language.subscribe((lang) =>
      this.translateService.use(lang || 'en')
    );

    this.chatService.chatWindowPos.subscribe((winpos) => {
      this.windowposition = winpos;
    });

    this.chatService.currentMatch.subscribe((currentMatch) => {
      this.matchdetails = currentMatch;
    });

    this.chatService.typeOfChatSub.subscribe((typeOfChatSub) => {
      this.typeofchat = typeOfChatSub;
    });

    this.chatService.chatStatus.subscribe((status) => {
      this.showChatComponent = status;
      if (this.showChatComponent == true) {
        this.updateChats();
        this.moveToLastElement();
      }
    });

    this.chatService.showTextAndButton.subscribe((status) => {
      this.showTextAndButton = status;
    });

    this.chatService?.getDeleteReceived().subscribe((res: any) => {
      const chatobj = document.querySelector('.chatmessage-' + res.chatid);
      if (typeof chatobj != 'undefined' && chatobj != null) {
        if (res.receiverid == this.currentUserId) {
          const foundchat = this.chats.find((chat) => chat._id == res.chatid);
          foundchat.deleteflag = 'yes';
          this.chatAdminFlag = this.chatService.getAdminMessageStatus();
          if (!this.chatAdminFlag) {
            // chatobj.innerHTML = this.translateService.instant(
            //   'CHAT.MESSAGE_WAS_DELETED'
            // );
            chatobj.innerHTML = 'message was deleted';
          }
        }
      }
    });
  }

  ngOnChanges() {
    this.chatService?.getMessages((message) => {
      let index = this.chats.findIndex(
        (x) => x._id.toString() === message._id.toString()
      );

      if (index >= 0) {
      } else {
        this.chats.push(message);
      }
      this.chatLoader = false;
      this.moveToLastElement();
    });

    this.chatService?.getTypingInfo1((typingObject) => {
      this.typingmessage = `${
        typingObject.fullName
      } ${this.translateService.instant('ELIMINATION.CHAT.IS_TYPING')}`;
    });
    this.chatService?.getStopTypingInfo1((typingObject) => {
      this.typingmessage = '';
    });
    this.moveToLastElement();
    this.chatAdminFlag = this.chatService.getAdminMessageStatus();
  }

  ngAfterViewChecked() {
    this.moveToLastElement();
    this.chatAdminFlag = this.chatService.getAdminMessageStatus();
  }

  ngOnDestroy() {
    if (this.matchdetails) {
      this.chatService?.disconnectMatch(
        this.matchdetails?.matchid
          ? this.matchdetails?.matchid
          : this.matchdetails,
        this.typeofchat
      );
    }

    this.chattitle = '';
    this.subtitle = '';
    this.profilePicture = '';
    this.chats = [];
    this.chatLoader = true;
  }

  returnFormattedChatMessage(message) {
    return message.replace(/((([A-Za-z]{3,9}:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w-_]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[\w]*))?)/g, (url) => {
      return '<a href="' + url + '" target="_blank">' + url + '</a>';
    });
  }

  public closeChat() {
    this.chatService.setChatStatus(false);
  }
  public onClickedOutside(e: Event) {
    this.chatService.setChatStatus(false);
  }
  public showProgressBar(value, hideProgress = false) {
    if (hideProgress) {
      this.showProgress = false;
      this.progressValue = 0;
    } else {
      this.showProgress = true;
      this.progressValue = value;
    }
  }

  public deleteChat(content, chatid, receiverid) {
    this.chatidtodelete = chatid;
    this.deletereceiverid = receiverid;
    this.modalService.open(content, {
      centered: true,
      windowClass: 'custom-modal-content',
    });
  }

  public onConfirmDelete() {
    this.chatService?.deleteChat(this.chatidtodelete, this.deletereceiverid);
    const chatobj = document.querySelector(
      '.chatmessage-' + this.chatidtodelete
    );
    if (typeof chatobj != 'undefined' && chatobj != null) {
      const foundchat = this.chats.find(
        (chat) => chat._id == this.chatidtodelete
      );
      foundchat.deleteflag = 'yes';
      // chatobj.innerHTML = this.translateService.instant('CHAT.YOU_DELETED_THIS_MESSAGE');
      this.chatidtodelete = '';
      this.modalService.dismissAll();
    }
  }

  public showDelete($event, chatid) {
    const deleteobj = $event.target.querySelector('.delete-' + chatid);
    if (typeof deleteobj != 'undefined' && deleteobj != null) {
      deleteobj.style.display = 'block';
    }
  }

  public hideDelete($event, chatid) {
    const deleteobj = $event.target.querySelector('.delete-' + chatid);
    if (typeof deleteobj != 'undefined' && deleteobj != null) {
      deleteobj.style.display = 'none';
    }
  }

  public updateChats() {
    this.chatLoader = true;
    if (this.matchdetails) {
      if (
        this.typeofchat == 'tournament' ||
        this.typeofchat == 'tournamenthistory'
      ) {
        if (
          typeof this.matchdetails.tournamentid === 'string' ||
          this.matchdetails.tournamentid instanceof String
        ) {
          this.tid = this.matchdetails.tournamentid;
        } else {
          this.tid = this.matchdetails.tournamentid._id;
        }
        this.chats = [];
        this.chatService
          ?.getOwnerForTournament(this.tid)
          .subscribe((ownerName: any) => {
            let currentOwner;
            this.matchdetails?.users.forEach(function (value) {
              if (ownerName?.organiserId == value?._id) {
                currentOwner = {
                  id: value?._id,
                  name: value?.fullName,
                };
              }
            });
            if (currentOwner) {
              this.currentTournamentOwner = currentOwner;
            } else {
              this.currentTournamentOwner = {
                id: ownerName?.organiserId,
                name: ownerName?.organiserName,
              };
            }

          });
        this.chattitle = this.matchdetails?.tournamentid?.name;
        // this.matchtitle =
        //   this.translateService.instant('CHAT.MATCH_NUMBER') +
        //   this.matchdetails.matchno;
        //this.matchtitle = 'Match Number' + this.matchdetails.matchno;

        let participantsName = this.matchdetails.users.map(function (item) {
          return item['fullName'];
        });
        this.subtitle = participantsName;
        this.chatService?.connectMatch(this.matchdetails.matchid, 'tournament');
        this.chatService
          ?.getAllChatMessages(this.matchdetails.matchid, 1, 30)
          .subscribe(
            (data: any) => {
              this.chats = [];
              for (var k = 0; k < data?.docs?.length; k++) {
                if (
                  data?.docs[k].messagetype == 'tournamentmessage' &&
                  data?.docs[k].userid == this.currentUserId
                ) {
                  this.chats.push(data?.docs[k]);
                } else if (data?.docs[k].messagetype != 'tournamentmessage') {
                  this.chats.push(data?.docs[k]);
                }
              }
              this.chats.reverse();
              this.chatLoader = false;
              this.moveToLastElement();
            },
            (err) => {}
          );
      } else if (this.typeofchat == 'game') {
        this.tid = this.matchdetails.matchid;
        this.chats = [];
        //this.currentTournamentOwner = this.matchdetails;
        let betweenusers = this.tid.split('-');
        let searchuser =
          betweenusers[0] == this.currentUserId
            ? betweenusers[1]
            : betweenusers[0];

        this.chatService.searchUsers(searchuser).subscribe(
          (res: any) => {
            if (res.data.length > 0) {
              // this.chatService
              //   .getGameIds(this.matchdetails.gameid, searchuser)
              //   .subscribe((resp: any) => {
                  // this.chattitle = this.translateService.instant(
                  //   'CHAT.GAME_CHAT'
                  // );
                  this.chattitle = this.translateService.instant('CHAT.GAME_CHAT');
                  this.matchtitle = '';
                  let gameidcheck1 = '';
                  let gameidcheck2 = '';

                  // if (resp.inGameUserName1 == 'NA') {
                  //   gameidcheck1 = '<Add your gameID in your profile>';
                  // } else {
                  //   gameidcheck1 = resp.inGameUserName1;
                  // }
                  // if (resp.inGameUserName2 == 'NA') {
                  //   gameidcheck2 = '<Add your gameID in your profile>';
                  // } else {
                  //   gameidcheck2 = resp.inGameUserName1;
                  // }
                  // this.subtitle = `${this.translateService.instant(
                  //   'CHAT.RECEIVER'
                  // )} ${res.data[0].fullName}`;
                  this.subtitle = `receiver ${res.data[0].fullName}`;
                  this.profilePicture = res.data[0].profilePicture;
                  // this.subtitle2 =
                  //   this.translateService.instant('CHAT.SENDER') +
                  //   this.userObject.fullName +
                  //   ' (Game ID : ' +
                  //   gameidcheck1 +
                  //   ')';

                  let messageObject: IChat;
                  messageObject = {
                    chatusername: this.userObject.fullName,
                    fullName: this.userObject.fullName,
                    matchid: '',
                    messagetype: 'game',
                    msg:
                      'Hello ' +
                      res.data[0].fullName +
                      ' and ' +
                      this.userObject.fullName +
                      ', your Tournamentname match is starting soon. You can add each other to the match. Your In-gameIDs are ' +
                      gameidcheck2 +
                      ' and ' +
                      gameidcheck1 +
                      '. Play by the rules and report the results on the Matchresults page.',
                    tournamentid: '',
                    typeofchat: this.typeofchat,
                    updatedAt: Date.now(),
                    userid: this.userObject['_id'],
                    deleteflag: 'no',
                    receiverid: '',
                  };

                  //this.chats.push(messageObject);
                // });
            }
          },
          (err) => {}
        );

        this.chatService?.connectMatch(this.tid, 'user');
        this.chatService?.getAllUserChatMessages(this.tid, 1, 30).subscribe(
          (data: any) => {
            this.chats = data?.docs;
            this.chats.reverse();
            this.chatLoader = false;
            this.moveToLastElement();
          },
          (err) => {}
        );
      } else {
        this.tid = this.matchdetails;
        this.chats = [];
        this.currentTournamentOwner = this.matchdetails;

        let betweenusers = this.matchdetails.split('-');
        let searchuser =
          betweenusers[0] == this.currentUserId
            ? betweenusers[1]
            : betweenusers[0];
        this.chatService.searchUsers(searchuser).subscribe(
          (res: any) => {
            if (res.data.length > 0) {
              if (this.chatService.getTextButtonStatus() == false) {
                this.messagesul.nativeElement.style.height = '450px';
                this.chattitle = this.translateService.instant(
                  'CHAT.CHAT_HISTORY'
                );
                //this.chattitle = 'chat history';
                this.matchtitle = '';
                // this.subtitle =
                //   this.translateService.instant('CHAT.SENDER') +
                //   res.data[0].fullName;
                this.subtitle = 'sender ' + res.data[0].fullName;
                this.profilePicture = res.data[0].profilePicture;
              } else {
                this.messagesul.nativeElement.style.height = '350px';
                this.chattitle = this.translateService.instant(
                  'CHAT.SINGLE_CHAT'
                );
                //this.chattitle = 'single chat';
                this.matchtitle = '';
                // this.subtitle =
                //   this.translateService.instant('CHAT.RECEIVER') +
                //   res.data[0].fullName;
                this.subtitle = 'receiver' + ' ' + res.data[0].fullName;
                this.profilePicture = res.data[0].profilePicture;
              }
            }
          },
          (err) => {}
        );

        if (
          this.typeofchat == 'chathistory' ||
          this.typeofchat == 'tournamenthistory'
        ) {
          this.chatService.chathistory.subscribe((chathistory) => {
            this.chats = [];
            this.chats = chathistory.docs ? chathistory.docs : chathistory;
            this.chatLoader = false;
            this.moveToLastElement();
          });
        } else {
          this.chatService?.connectMatch(this.matchdetails, 'user');
          this.chatService
            ?.getAllUserChatMessages(this.matchdetails, 1, 30)
            .subscribe(
              (data: any) => {
                this.chats = data?.docs;
                this.chats.reverse();
                this.chatLoader = false;
                this.moveToLastElement();
              },
              (err) => {}
            );
        }
      }
    }
  }

  public moveToLastElement() {
    if (this.lastelement?.nativeElement != null) {
      this.lastelement?.nativeElement.scrollIntoView({
        behavior: 'smooth',
        block: 'start',
      });
    }
  }

  public onTyping(event) {
    this.typingmessage = '';
    let typingObject;
    if (this.typeofchat == 'tournament') {
      let gameusername = '';
      if (this.userObject['_id'] == this.matchdetails?.teamAId) {
        gameusername = this.matchdetails?.teamAGameId;
      } else if (this.userObject['_id'] == this.matchdetails?.teamBId) {
        gameusername = this.matchdetails?.teamBGameId;
      } else {
        gameusername = this.userObject['fullName'];
      }
      typingObject = {
        matchid: this.matchdetails?.matchid,
        fullName: this.userObject['fullName'],
        userid: this.userObject['_id'],
      };
    } else {
      typingObject = {
        matchid: this.matchdetails,
        fullName: this.userObject['fullName'],
        userid: this.userObject['_id'],
      };
    }

    this.chatService?.sendTyping(typingObject, this.typeofchat);
  }

  public hideTyping(event) {
    let typingObject;
    if (this.typeofchat == 'tournament') {
      let gameusername = '';
      if (this.userObject['_id'] == this.matchdetails?.teamAId) {
        gameusername = this.matchdetails?.teamAGameId;
      } else if (this.userObject['_id'] == this.matchdetails?.teamBId) {
        gameusername = this.matchdetails?.teamBGameId;
      } else {
        gameusername = this.userObject['fullName'];
      }
      typingObject = {
        matchid: this.matchdetails.matchid,
        fullName: this.userObject['fullName'],
        userid: this.userObject['_id'],
      };
    } else {
      typingObject = {
        matchid: this.matchdetails,
        fullName: this.userObject['fullName'],
        userid: this.userObject['_id'],
      };
    }
    this.chatService?.stopTyping(typingObject, this.typeofchat);
    // this.chatService?.getStopTypingInfo().subscribe((typingObject) => {
    //   this.typingmessage = '';
    // });
  }

  public sendMsg(messagetyped, type) {
    let messageObject: IChat;
    let typingObject;

    if (this.typeofchat == 'tournament') {
      let gameusername = '';
      if (this.userObject['_id'] == this.matchdetails?.teamAId) {
        gameusername = this.matchdetails?.teamAGameId;
      } else if (this.userObject['_id'] == this.matchdetails?.teamBId) {
        gameusername = this.matchdetails?.teamBGameId;
      } else {
        gameusername = this.userObject['fullName'];
      }
      let receiverid;

      if (this.userObject['_id'] == this.matchdetails?.teamAId) {
        receiverid = this.matchdetails?.teamBId;
      } else if (this.userObject['_id'] == this.matchdetails?.teamBId) {
        receiverid = this.matchdetails?.teamAId;
      }
      messageObject = {
        //chatusername: gameusername,
        chatusername: this.userObject['fullName'],
        fullName: this.userObject['fullName'],
        matchid: this.matchdetails?.matchid,
        messagetype: type,
        msg: messagetyped,
        tournamentid: this.matchdetails?.tournamentid?._id,
        typeofchat: this.typeofchat,
        updatedAt: Date.now(),
        userid: this.userObject['_id'],
        receiverid: receiverid,
      };
    } else if (this.typeofchat == 'game') {
      messageObject = {
        chatusername: this.userObject['fullName'],
        fullName: this.userObject['fullName'],
        matchid: this.matchdetails.matchid,
        matchImage: this.matchdetails.image,
        matchLocation: this.matchdetails.location,
        matchName: this.matchdetails.matchName,
        messagetype: type,
        msg: messagetyped,
        typeofchat: this.typeofchat,
        updatedAt: Date.now(),
        userid: this.userObject['_id'],
        receiverid: this.matchdetails._id,
      };
    } else {
      const splitmatchid = this.matchdetails.split('-');
      let receiverid;
      if (splitmatchid[0] != this.userObject['_id']) {
        receiverid = splitmatchid[0];
      } else if (splitmatchid[1] != this.userObject['_id']) {
        receiverid = splitmatchid[1];
      }
      messageObject = {
        chatusername: this.userObject['fullName'],
        fullName: this.userObject['fullName'],
        matchid: this.matchdetails,
        messagetype: type,
        msg: messagetyped,
        typeofchat: this.typeofchat,
        updatedAt: Date.now(),
        userid: this.userObject['_id'],
        receiverid: receiverid,
      };
    }
    // if (type == 'textwithfiles') {
    //   messageObject.filesattached = this.s3files;
    //   this.s3files = [];
    //   this.filesToUpload = [];
    //   this.dummyFiles = [];
    // }
    if (this.typeofchat == 'tournament') {
      this.chatService?.sendMessage(messageObject, this.typeofchat);
      typingObject = {
        matchid: this.matchdetails?.matchid,
        fullName: this.userObject['fullName'],
        userid: this.userObject['_id'],
      };
      this.chatService?.getMessages((message) => {

        let index = this.chats.findIndex(
          (x) => x._id.toString() === message._id.toString()
        );

        if (index >= 0) {
        } else {
          this.chats.push(message);
        }
        this.moveToLastElement();
      });
    } else if (this.typeofchat == 'game') {
      this.chatService?.sendMessage(messageObject, this.typeofchat);
      typingObject = {
        matchid: this.matchdetails.matchid,
        fullName: this.userObject['fullName'],
        userid: this.userObject['_id'],
      };
      this.chatService?.getMessages((message) => {

        let index = this.chats.findIndex(
          (x) => x._id.toString() === message._id.toString()
        );

        if (index >= 0) {
        } else {
          this.chats.push(message);
        }
        this.moveToLastElement();
      });
    } else {
      this.chatService?.sendMessage(messageObject, this.typeofchat);
      typingObject = {
        matchid: this.matchdetails,
        fullName: this.userObject['fullName'],
        userid: this.userObject['_id'],
      };
      this.chatService?.getMessages((message) => {

        let index = this.chats.findIndex(
          (x) => x._id.toString() === message._id.toString()
        );

        if (index >= 0) {
        } else {
          this.chats.push(message);
        }
        this.moveToLastElement();
      });
    }

    this.chatService?.stopTyping(typingObject, this.typeofchat);
    this.messageinput.nativeElement.value = '';
    this.typingmessage = '';
    this.moveToLastElement();
  }

  public uploadFiles(messagetyped) {
    this.fileInput.nativeElement.value = '';
    let observables = [];
    for (let i = 0; i < this.filesToUpload.length; i++) {
      const formData = new FormData();
      formData.append('filetoupload', this.filesToUpload[i].data);
      this.filesToUpload[i].inProgress = true;
      observables.push(this.chatService.upload(formData));
    }
    if (observables.length > 0) {
      forkJoin(observables).subscribe((results: any[]) => {
        if (results.length > 0) {
          for (let k = 0; k < results.length; k++) {
            if (results[k]?.status == 'success') {
              const s3url = results[k]?.filapath;
              const fname = s3url.split('/').pop();
              this.dummyFiles.splice(this.dummyFiles.indexOf(fname), 1);
              this.s3files.push(s3url);
            } else {
              this.s3files.push('error : ' + results[k]?.message);
            }
          }
        }
        this.sendMsg(messagetyped, 'textwithfiles');
      });
    }
  }

  public onFileAttach(event) {
    event.preventDefault();
    let inputElement: HTMLElement = this.fileInput.nativeElement as HTMLElement;
    inputElement.click();
  }

  public handleFileInput(files: FileList) {
    for (let index = 0; index < files.length; index++) {
      const file = files[index];
      this.filesToUpload.push({ data: file, inProgress: false, progress: 0 });
      this.dummyFiles.push(file.name);
    }
  }

  public async onEnterKey(event) {
    event.preventDefault();
    let messagetyped = this.messageinput?.nativeElement.value;
    this.pushGTMTags('Send_Chat_Message', messagetyped);
    if (messagetyped != '') {
      this.sendMsg(messagetyped, 'text');
    }
  }

  pushGTMTags(eventName: string, message: string) {
    let superProperties: SuperProperties = {};
    if (this.userObject) {
      superProperties = this.gtmService.assignLoggedInUsedData(this.userObject);
    }

    let eventProperties: EventProperties = {};

    eventProperties['messageLength'] = message.length;

    this.gtmService.gtmEventWithSuperProp({
      eventName: eventName,
      superProps: superProperties,
      eventProps: eventProperties,
    });

  }
}
