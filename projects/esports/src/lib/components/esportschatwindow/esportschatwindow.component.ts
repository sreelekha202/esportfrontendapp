import {
  Component,
  Input,
} from '@angular/core';
import { EsportsChatService } from '../../services/esport.chat.service';

@Component({
  selector: 'lib-esportschatwindow',
  templateUrl: './esportschatwindow.component.html'
})
export class EsportschatwindowComponent {
  @Input() public matchdetails: any;
  public showChat: boolean = false;
  public windowposition: String = 'chat_window chat_window_right';

  constructor(public chatService: EsportsChatService) { }

  public toggleChat() {
    if (this.matchdetails) {
      this.chatService
        .getMatchInformation(this.matchdetails._id)
        .subscribe((result: any) => {
          const mtchdetails = {
            _id: result.matchdetails._id,
            picture: result.matchdetails.tournamentId.gameDetail.logo,
            name: result.matchdetails.tournamentId.name,
            matchno: result.matchdetails.matchNo,
            participants:
              result.matchdetails.teamA.inGamerUserId +
              ',' +
              result.matchdetails.teamB.inGamerUserId,
            tournamentId: result.matchdetails.tournamentId._id,
            teamAId: result.matchdetails.teamA.userId,
            teamBId: result.matchdetails.teamB.userId,
            teamAGameId: result.matchdetails.teamA.inGamerUserId,
            teamBGameId: result.matchdetails.teamB.inGamerUserId,
            type: 'tournament',
          };
          this.chatService.setWindowPos(this.windowposition);
          let firstChat = this.chatService.getChatStatus();
          if (firstChat == true) {
            this.chatService.setChatStatus(false);
            this.chatService?.disconnectMatch(mtchdetails._id, 'tournament');
            this.chatService.setTextButtonStatus(false);
          } else {
            this.chatService.setCurrentMatch(mtchdetails);
            this.chatService.setTypeOfChat('tournament');
            this.chatService.setChatStatus(true);
            this.chatService.setTextButtonStatus(true);
          }
        });
    }
  }

}
