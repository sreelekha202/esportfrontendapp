import { isPlatformBrowser } from '@angular/common';
import {
  AfterViewInit,
  Component,
  Inject,
  OnInit,
  PLATFORM_ID,
  ViewChild,
  Input,
  OnDestroy,
} from '@angular/core';
import { EsportsChatService } from '../../services/esport.chat.service';
import { EsportsChatSidenavService } from '../../services/esport.chatsidenav.service';
import { MatSidenav } from '@angular/material/sidenav';
import { IChat } from '../../models/chat';
import { IUser } from '../../models/user';
import { EsportsUserService } from '../../services/esport.user.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { EsportsGtmService, EventProperties, SuperProperties } from "../../services/esport.gtm.service";
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';

@AutoUnsubscribe()
@Component({
  selector: 'lib-esportschatsidebar',
  templateUrl: './esportschatsidebar.component.html',
  styleUrls: ['./esportschatsidebar.component.scss'],
})
export class EsportschatsidebarComponent
  implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild('menuSidenav') public menuSidenav: MatSidenav;
  @ViewChild('chatSidenav') public chatSidenav: MatSidenav;
  @Input() isShowBlockUser: boolean = false;
  public isBrowser: boolean;
  public showChat: boolean = false;
  public matchdetails: any;
  public matchlist: any;
  public spamcomment: any;
  public typeofchat: any;
  public useridtoblock: any;
  public matchId: any;
  public chatWindows = [];
  public initialUserList = [];
  public userlist: any;
  public windowposition: String = 'chat_window chat_window_right_drawer';
  public currentUserName: String = '';
  public isSpin: boolean = false;
  public isSpin2: boolean = false;
  public isUserLoggedIn: boolean = false;
  public currentUserId: String = '';
  public currenUser: IUser;
  public showSearchListTitle: boolean = false;
  public currentsearch: '';
  public chats: Array<IChat>;
  userSubscription: Subscription;
  subscription: Subscription;

  constructor(
    @Inject('env') public environment,
    @Inject(PLATFORM_ID) public platformId,
    public chatSidenavService: EsportsChatSidenavService,
    public chatService: EsportsChatService,
    public userService: EsportsUserService,
    private gtmService: EsportsGtmService,
    public modalService: NgbModal
  ) {
    this.isBrowser = isPlatformBrowser(platformId);
  }

  ngOnInit(): void {
    if (this.isBrowser) {
      this.userSubscription = this.userService.currentUser.subscribe((data) => {
        if (data) {
          this.currenUser = data;
          this.isUserLoggedIn = !!this.currenUser;
          this.currentUserId = this.currenUser._id;

          this.chatService.mtches.subscribe((matcList) => {
            this.isSpin2 = true;
            this.chatWindows = matcList;
          });

          if (this.chatWindows?.length == 0) {
            this.chatService?.getAllMatches(this.currentUserId, 1, 30);
          }

          this.chatService.chatStatus.subscribe((cstatus) => {
            this.showChat = cstatus;
          });

          
        }
      });
    }
  }

  ngAfterViewInit(): void {
    this.chatSidenavService.setChatSidenav(this.chatSidenav);

    this.subscription = this.chatSidenav.closedStart.subscribe( () => {
      this.currentsearch = '';
    })
  }

  ngOnDestroy(): void {
    this.chatService.setChatStatus(false);
    this.chatService?.disconnectMatch(
      this.matchdetails?.matchid
        ? this.matchdetails?.matchid
        : this.matchdetails,
      this.typeofchat
    );
    this.userSubscription?.unsubscribe();
    this.subscription?.unsubscribe();
  }

  public onMenuClick(): void {
    this.menuSidenav.toggle();
  }

  public searchUsers(event) {
    this.userlist = [];
    this.showSearchListTitle = false;
    if (event.keyCode == 13) {
      if (this.currentsearch != '') {
        this.isSpin = true;
        this.showSearchListTitle = false;
        this.chatService.searchUsers(this.currentsearch).subscribe(
          (res: any) => {
            this.isSpin = false;
            this.showSearchListTitle = true;
            this.userlist = res.data;
          },
          (err) => {}
        );
      }
    }
  }

  public onChatSidenavToggle(): void {
    this.chatSidenavService.toggle();

    if (this.currentUserId) {
      this.chatService?.getAllMatches(this.currentUserId, 1, 30);
    }

    if (this.chatSidenavService.checkOpen()) {
      if (this.chatService.getChatStatus()) {
        this.chatService.setChatStatus(false);
      }
    } else {
      if (this.chatService.getChatStatus()) {
        this.chatService.setChatStatus(false);
      }
    }
  }

  public toggleChat(chatwindow) {
    this.pushGTMTags('Chat_Opened');
    this.typeofchat = chatwindow.typeofchat;

    if (
      chatwindow.typeofchat == 'tournament' ||
      chatwindow.typeofchat == 'game'
    ) {
      this.matchdetails = chatwindow;
    } else {
      if (chatwindow.matchid) {
        this.matchdetails = chatwindow.matchid;
      } else {
        this.matchdetails = chatwindow._id + '-' + this.currentUserId;
      }
      this.showChat = true;
    }

    this.chatService.setWindowPos(this.windowposition);
    this.chatService.setCurrentMatch(this.matchdetails);
    this.chatService.setTypeOfChat(this.typeofchat);
    this.chatService.setChatStatus(true);
    this.chatService.setTextButtonStatus(true);

    if (this.chatService.getChatStatus() == false) {
      this.chatService?.disconnectMatch(
        this.matchdetails?.matchid
          ? this.matchdetails?.matchid
          : this.matchdetails,
        this.typeofchat
      );
    }
    this.chatSidenav.toggle();
  }

  public blockUser(content, userid, matchid) {
    this.pushGTMTags('Chat_Block', true);
    this.useridtoblock = userid;
    this.matchId = matchid;

    this.modalService.open(content, {
      centered: true,
      windowClass: 'custom-modal-content',
    });
  }

  public onConfirmSpam(blocktype = 'spm') {
    let matchid = this.matchId;
    this.pushGTMTags('Chat_Report_Spam', true);

    this.chatService
      ?.getAllUserChatMessages(matchid, 1, 50)
      .subscribe((chats) => {
        const body = {
          userid: this.currentUserId,
          blocktype: blocktype,
          spamuserid: this.useridtoblock,
          comment: this.spamcomment,
          chathistorymessages: JSON.stringify(chats).replace(/"/g, '\\"'),
          matchid: matchid,
        };

        this.chatService?.blockUser(body).subscribe((resp: any) => {
          if (resp.status == 'success') {
            this.chatService?.getAllMatches(this.currentUserId, 1, 30);
            this.useridtoblock = '';
            this.modalService.dismissAll();
            this.spamcomment = '';
          }
        });
      });
  }

  onConfirmBlock() {
    this.onConfirmSpam('blck');
  }

  pushGTMTags(eventName: string, eventProps = false) {
    let superProperties: SuperProperties = {};
    if (this.currenUser) {
      superProperties = this.gtmService.assignLoggedInUsedData(this.currenUser);
    }

    let eventProperties: EventProperties = {};
    if (eventProps) {
      eventProperties.fromPage = 'Chats';
    }

    this.gtmService.gtmEventWithSuperProp({
      eventName: eventName,
      superProps: superProperties,
      eventProps: eventProperties,
    });
  }
}
