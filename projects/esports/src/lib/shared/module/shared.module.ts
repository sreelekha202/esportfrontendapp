import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MaterialModule } from './material.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { PipeModule } from '../../pipes/pipe.module';
import { LazyLoadImageModule } from 'ng-lazyload-image';
import { DirectivesModule } from '../../directives/directive.module';
import { CountdownModule } from 'ngx-countdown';

const modules = [
  CommonModule,
  RouterModule,
  FormsModule,
  ReactiveFormsModule,
  MaterialModule,
  NgxDatatableModule,
  FontAwesomeModule,
  PipeModule,
  LazyLoadImageModule,
  DirectivesModule,
  CountdownModule,
];

@NgModule({
  imports: [...modules],
  declarations: [],
  exports: [...modules],
  providers: [],
})
export class EsportsSharedModule {
  constructor() {}
}
