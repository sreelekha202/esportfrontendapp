import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DateAdapter, MAT_DATE_FORMATS } from '@angular/material/core';
import { APP_DATE_FORMATS, AppDateAdapter } from './date.adapter';

const provider = [
  { provide: DateAdapter, useClass: AppDateAdapter },
  { provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS }
];

@NgModule({
  declarations: [],
  imports: [CommonModule],
  exports: [],
  providers:[...provider]
})
export class DirectivesModule {}
