export interface IO_COMMENT {
  message?: string;
  postAuthor?: string;
  commentId?: string;
}

export interface IO_REPLY {
  message?: {
    toCommentAuthor?: string;
    toPostAuthor?: string;
  };
  commentId?: string;
  postAuthor?: string;
  commentAuthor?: string;
}

export interface IO_LIKE {
  message?: string;
  postAuthor?: string;
  likeId?: string;
}

export interface IO_REPORT {
  message?: string;
  postAuthor?: string;
  reportId?: string;
}
