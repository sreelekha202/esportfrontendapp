export interface ILIKE {
  _id?: string;
  postId?: string;
  likeReact?: LKIE_REACT;
  createdBy?: any;
  createdOn?: string;
  updatedBy?: any;
  updatedOn?: string;
}

export enum LKIE_REACT {
  FEELS = 0,
  SAFEGUARDED = 1,
  WINNING = 2,
  AGREED = 3,
  NONE = 4,
}
