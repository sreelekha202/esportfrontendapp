export class IUser {
  id?: number;
  _id?: string; // the unique key from backend is _id
  fullName?: string;
  username?: string;
  phoneNumber?: string;
  phoneisVerified?: boolean;
  email?: string;
  emailisVerified?: boolean;
  identifiers?: [
    {
      idenType?: string;
      uuid?: string;
    }
  ];
  accessLevel?: string[];
  isPassword?: boolean;
  password?: string;
  postalcode?: string;
  emailSecond?: string;
  coverPicture?: string;
  profilePicture?: string;
  aboutMe?: string;
  country?: string;
  state?: string;
  progress?: number;
  havePin?: boolean;
  token?: string;
  gender?: string;
  gamesPlayed?: string;
  martialStatus?: string;
  parentalStatus?: string;
  dob?: Date;
  shortBio?: string;
  profession?: string;
  postalCode?: number;
  status?: number;
  createdOn?: Date;
  updatedOn?: Date;
  preference?: any;
  accountDetail?: {
    reward?: number;
    paymentType?: string;
    paymentAccountId?: string;
    isPaymentAccountVerified?: boolean;
    _id?: string;
    referralId?: string;
  };
  accountType?: string;
  firstLogin?: number;
  isInfluencer?: number;
  isVerified?: number;
  organizerRating?: number;
  userTypeDetail?: {
    _id?: number;
    city?: string;
    country?: string;
    websiteUrl?: string;
    commercialRegistrationEndDate?: Date;
    commercialRegistrationNum?: number;
    contactEmail?: string;
    contactName?: string;
    contactNumber?: number;
    userAccType?: string;
    sectorName?: string;
    profilePicture?: string;
    userAccTypeNo?: number;
  };
  isEO?: any;
  socialAccounts?: [];
  customPronoun?: string;
}

export enum UserAccountType {
  'Gamer' = 0,
  'Game Center' = 1,
  'Team' = 2,
  'Organizer' = 3,
}

export enum GENDER {
  HE = 'he',
  SHE = 'she',
  THEY = 'they',
}
