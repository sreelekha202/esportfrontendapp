export interface IPagination {
  activePage?: number;
  totalItems: number;
  itemsPerPage: number;
  maxSize: number;
}
