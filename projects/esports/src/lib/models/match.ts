import { IParticipant } from "./participant";

interface IMatch {
  _id?: string;
  bye?: boolean;
  createdOn?: Date;
  currentMatch?: object;
  disQualifiedTeam?: object;
  isNextMatchStart?: boolean;
  isThirdPlaceMatch?: boolean;
  loserNextMatch?: object;
  loserTeam?: IParticipant;
  matchNo?: number;
  matchStatus?: string;
  placeHolderA?: string;
  placeHolderB?: string;
  refereeId?: string;
  sets?: IMatchSet[];
  status?: number;
  teamA?: IParticipant;
  teamAWinSet?: number;
  teamB?: IParticipant;
  teamBWinSet?: number;
  totalSet?: number;
  tournamentId?: ITournament;
  updatedOn?: Date;
  winnerNextMatch?: object;
  winnerTeam?: IParticipant;


  battleRoyalTeams?: BattleRoyaleTeam[];
  loserTeam2?: string;
  isScoreUpdatedByParticipant?: boolean;
  scoreUpdatedByParticipantDt?: Date;
  scoreUpdateByParticipant?: string;
}

interface IMatchSet {
  id?: number;
  status?: string;
  teamAScore?: number;
  teamAScreenShot?: string;
  teamBScore?: number;
  teamBScreenShot?: string;
  winner?: null;
}

interface BattleRoyaleTeam {
  _id?: string;
  id?: number;
  playerId?: string;
  gamerId?: string;
  noOfKill?: number;
  placement?: number;
  score?: number;
  notPlaying?: number;
}

interface ITournament {
  _id?: string;
}

export { IMatch, IMatchSet };
