import { LKIE_REACT } from './likePost';

export interface IPOST {
  _id?: string;
  message?: string;
  videoLink?: any[];
  imagesLink?: any[];
  isPrivate?: boolean;
  url?: string;
  shares?: any[];
  likes?: any[];
  comments?: any[];
  reports?: any[];
  createdBy?: any;
  updatedBy?: any;
  createdOn?: string;
  updatedOn?: string;
  originalPostId?: string;
  postType?: POST_TYPE;
  likeReact?: LKIE_REACT;
  likeId?: string;
}

export interface IPREVIEW_LINK {
  image?: string;
  title?: string;
  description?: string;
}

export enum FILE_TYPE {
  VIDEO = 'video',
  IMAGE = 'image',
}

export enum POST_TYPE {
  MEDIA = 'media',
  LINK = 'link',
  SHARE = 'share',
  BROAD_CAST = 'broadcast',
}
