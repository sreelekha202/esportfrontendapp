export interface ICOMMENT {
  _id?: string;
  postId?: string;
  message?: string;
  imagesLink?: string;
  commentParent?: any;
  commentChilds?: any[];
  createdBy?: any;
  createdOn?: string;
  updatedBy?: any;
  updatedOn?: string;
}
