import { IPOST } from './postfeed';

export interface IMODAL_POST {
  type: POST_EVENT_TYPE;
  username: string;
  showLinkContainer?: boolean;
  post?: IPOST;
}

export enum POST_EVENT_TYPE {
  ADD = 'ADD',
  EDIT = 'EDIT',
}
