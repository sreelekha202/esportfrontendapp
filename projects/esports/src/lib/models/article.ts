import { IGame } from './game';
import { IUser } from './user';

interface IArticle {
  _id?: string;
  game?: string;
  genre?: string;
  tags?: string;
  highlight?: number;
  minRead?: number;
  location?: string;
  isSponsor?: boolean;
  isInfluencer?: boolean;
  isInfluencerHighlight?: boolean;
  fullText?: string;
  status?: string;
  title?: string;
  slug?: string;
  content?: string;
  category?: string;
  platform?: string;
  author?: string;
  image?: string;
  shortDescription?: string;
  publishDate?: Date;
  likes?: number;
  id?: string;
  authorDetails?: IUser;
  createdDate?: Date;
  totalLikes?: string;
  gameDetails?: IGame;
  bookmarkProccesing?: boolean;
  isBookmarked?: boolean;
  likeProccesing?: boolean;
  type?: number;
  createdOn?: Date;
}

export { IArticle };
