import {
  TranslateLoader,
  TranslateModule,
  TranslateService,
} from '@ngx-translate/core';

import { HttpClient, HttpClientModule } from '@angular/common/http';
import {
  ModuleWithProviders,
  Inject,
  NgModule,
  PLATFORM_ID,
} from '@angular/core';
import {
  BrowserTransferStateModule,
  TransferState,
} from '@angular/platform-browser';

import { EsportsConstantsService } from '../services/esport.constants.service';
import { EsportsLanguageService } from '../services/esport.language.service';

import { isPlatformBrowser } from '@angular/common';
import { TranslateBrowserLoader } from './translate-browser.loader';
import { TranslateFsLoader } from './translate-fs-loader';

@NgModule({
  imports: [
    HttpClientModule,
    BrowserTransferStateModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: translateLoaderFactory,
        deps: [HttpClient, TransferState, PLATFORM_ID],
      },
    }),
  ],
  exports: [TranslateModule],
})
export class I18nModule {
  public static forRoot(environment: any): ModuleWithProviders<I18nModule> {
    // User config get logged here
    return {
      ngModule: I18nModule,
      providers: [
        EsportsConstantsService,
        { provide: 'env', useValue: environment },
      ],
    };
  }

  constructor(
    languageService: EsportsLanguageService,
    ConstantsService: EsportsConstantsService,
    translateService: TranslateService,
    @Inject(PLATFORM_ID) private platform: any
  ) {
    translateService.addLangs(ConstantsService?.language?.map((el) => el.code));

    const browserLang = isPlatformBrowser(this.platform)
      ? localStorage.getItem('currentLanguage') ||
        ConstantsService?.defaultLangCode ||
        translateService.getBrowserLang()
      : ConstantsService?.defaultLangCode;

    languageService.setLanguage(browserLang);
    translateService.use(browserLang || ConstantsService?.defaultLangCode);
  }
}

export function translateLoaderFactory(
  httpClient: HttpClient,
  transferState: TransferState,
  platform: any
) {
  return isPlatformBrowser(platform)
    ? new TranslateBrowserLoader(transferState, httpClient)
    : new TranslateFsLoader(transferState);
}