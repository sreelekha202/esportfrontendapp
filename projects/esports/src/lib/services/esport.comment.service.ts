import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { SubscribableOrPromise, Observable } from 'rxjs';
 
@Injectable({
  providedIn: 'root',
})
export class EsportsCommentService {
  constructor(private http: HttpClient, @Inject('env') private environment) {}

  upsertComment(data): SubscribableOrPromise<any> {
    return this.http
      .patch(`${this.environment.apiEndPoint}comment`, data)
      .toPromise();
  }

  deleteComment(id): SubscribableOrPromise<any> {
    return this.http
      .delete(`${this.environment.apiEndPoint}comment/${id}`)
      .toPromise();
  }

  upsertLike(data): SubscribableOrPromise<any> {
    return this.http
      .patch(this.environment.apiEndPoint + 'comment/like', data)
      .toPromise();
  }

  getAllComment(type, id, page): SubscribableOrPromise<any> {
    return this.http
      .get(`${this.environment.apiEndPoint}comment/${type}/${id}?page=${page}`)
      .toPromise();
  }

  getAllComment1(type, id, page): Observable<any> {
    return this.http.get(
      `${this.environment.apiEndPoint}comment/${type}/${id}?page=${page}`
    );
  }
  saveComment(data): Observable<any> {
    return this.http.patch(`${this.environment.apiEndPoint}comment`, data);
  }
}
