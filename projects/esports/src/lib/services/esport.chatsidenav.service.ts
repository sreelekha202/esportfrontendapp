import { Injectable } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import { MatDrawerToggleResult } from '@angular/material/sidenav/drawer';

@Injectable({ providedIn: 'root' })
export class EsportsChatSidenavService {
  chatSidenav: MatSidenav;

  public setChatSidenav(sidenav: MatSidenav) {
    this.chatSidenav = sidenav;
  }

  public open(): Promise<MatDrawerToggleResult> {
    return this.chatSidenav.open();
  }

  public close(): Promise<MatDrawerToggleResult> {
    return this.chatSidenav.close();
  }

  public toggle(): void {    
    this.chatSidenav.toggle();
  }

  public checkOpen() {
    return this.chatSidenav.opened;
  }

  public getChatSidenav(): Promise<MatSidenav | null> {
    return new Promise((resolve, reject) => {
      let count = 0;
      const interval = setInterval(() => {
        count++;

        if (this.chatSidenav) {
          clearInterval(interval);
          resolve(this.chatSidenav);
        }

        if (count === 100) {
          reject(null);
        }
      }, 100);
    });
  }
}
