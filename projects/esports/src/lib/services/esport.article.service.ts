import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { SubscribableOrPromise,BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class EsportsArticleService {
  constructor(private httpClient: HttpClient) {}
  public getallArticle(api) {
    return this.httpClient.get(`${api}article`);
  }
  public saveArticle(api, obj) {
    return this.httpClient.post(`${api}article`, obj);
  }
  public getArticleByID(api, id) {
    return this.httpClient.get(`${api}article/${id}`);
  }
  public getArticleBySlug(api, slug): SubscribableOrPromise<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json; charset=utf-8');
    return this.httpClient
      .get(`${api}article/slug/${slug}`, { headers: httpHeaders })
      .toPromise();
  }
  public delArticle(api, id) {
    return this.httpClient.delete(`${api}article/${id}`);
  }
  public updateArticle(api, id, value) {
    return this.httpClient.put(`${api}article/${id}`, value);
  }
  public saveComment(api, data) {
    return this.httpClient.post(`${api}comment`, data);
  }
  public addBookmark(api, data) {
    return this.httpClient.patch(`${api}userpreference/add_bookmark`, data);
  }
  public getTrendingAuthors(api) {
    return this.httpClient.get(`${api}home/trending-authors`);
  }
  public getArticles_PublicAPI(api, params) {
    const encodedUrl = encodeURIComponent(params['query']);
    let url = `${api}home/getArticles?query=${encodedUrl}`;
    if (params['projection']) {
      url += `&select=${params['projection']}`;
    }
    if (params['option']) {
      const option = encodeURIComponent(params['option']);
      url += `&option=${option}`;
    }
    if (params['pagination']) {
      let option = encodeURIComponent(params['pagination']);
      url += `&pagination=${option}`;
    }
    return this.httpClient.get(url);
  }
  public getHottestPost(api) {
    return this.httpClient.get(`${api}home/hottest_post`);
  }

  public getGameList(api, query) {
    const encodedUrl = encodeURIComponent(query);
    return this.httpClient.get(`${api}home/getGames?${encodedUrl}`);
  }
  public getTrendingPosts(api) {
    return this.httpClient.get(`${api}home/trending_posts`);
  }
  public getArticles(api, params) {
    const encodedUrl = encodeURIComponent(params['query']);
    let url = `${api}article?query=${encodedUrl}`;
    if (params['projection']) {
      url += `&select=${params['projection']}`;
    }
    if (params['option']) {
      let option = encodeURIComponent(params['option']);
      url += `&option=${option}`;
    }
    return this.httpClient.get(url);
  }

  public updateViews(api, id, value) {
    return this.httpClient.put(`${api}article/updateViews/${id}`, value);
  }

  public getArticleList(api) {
    return this.httpClient.get(`${api}home/article-list`);
  }

  /**
 *  Get paginated Articles (public API)
 * @param {*} params // params = {query: "JSON String", pagination: "JSON String"}
 *
 * query -  Filter
 * pagination - mongoose-paginate-v2 pagination type Object
 *Example: -
 params = {
   query:   "{\"articleStatus\":\"publish\"}",
   pagination: "{\"page\":1,\"limit\":4,\"sort\":\"-views\"}"
}
 */

  public getPaginatedArticles(api, params) {
    const encodedUrl = encodeURIComponent(params['query']);
    const encodedPagination = encodeURIComponent(params['pagination']);
    let url = `${api}home/getPaginatedArticles?query=${encodedUrl}&pagination=${encodedPagination}`;
    return this.httpClient.get(url);
  }

  public getLatestArticle(api, params) {
    const encodedUrl = encodeURIComponent(params['query']);
    const encodedPagination = encodeURIComponent(params['pagination']);
    const encodedPrefernce = params['preference']
      ? encodeURIComponent(params['preference'])
      : JSON.stringify({});
    let url = `${api}home/latestarticles?query=${encodedUrl}&pagination=${encodedPagination}&preference=${encodedPrefernce}`;
    return this.httpClient.get(url);
  }

  // for category

  public getLatestArticleWithCategory(api, params) {
    const encodedUrl = encodeURIComponent(params['query']);

    const encodedPagination = encodeURIComponent(params['pagination']);

    const encodedPreference = params['prefernce']
      ? encodeURIComponent(params['prefernce'])
      : JSON.stringify({});

    const encodedCategory = params['category']
      ? encodeURIComponent(params['category'])
      : JSON.stringify({});

    let url = `${api}home/latestarticles?query=${encodedUrl}&pagination=${encodedPagination}&preference=${encodedPreference}
    
        ${params['category'] ? '&category=' + encodedCategory : ''}`;

    return this.httpClient.get(url);
  }
  
  // for category and gamelist
  public getLatestArticleAll(api, params) {
    /**
     * @param
     * articleStatus: 'publish',
     * page: 1,
     * limit: 8,
     * sort: 'createdOn', or sort: '-createdOn'
     * gameDetails: first_game_id || second_game_id,
     */
    return this.httpClient.get(`${api}home/articles`, { params: params });
  }

  public createArticleSubject = new BehaviorSubject({});
  public createArticle = this.createArticleSubject.asObservable();
}
