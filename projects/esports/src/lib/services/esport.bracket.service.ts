import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { SubscribableOrPromise } from 'rxjs';
import { IBracket } from '../models/bracket';

@Injectable({
  providedIn: 'root',
})
export class EsportsBracketService {
  bracketData;
  mobileWebViewMetaData;
  macthRoundData: any;
  selectedTournament: any;

  constructor(private http: HttpClient, @Inject('env') private environment) {}

  fetchAllBracket(query): SubscribableOrPromise<any> {
    return this.http
      .get(`${this.environment.apiEndPoint}bracket${query}`)
      .toPromise();
  }

  getBracketByID(id: string): SubscribableOrPromise<any> {
    return this.http
      .get(`${this.environment.apiEndPoint}bracket/${id}`)
      .toPromise();
  }

  saveBracket(bracket: IBracket): SubscribableOrPromise<any> {
    return this.http
      .post(`${this.environment.apiEndPoint}bracket`, bracket)
      .toPromise();
  }

  updateBracket(id: string, bracket: IBracket): SubscribableOrPromise<any> {
    return this.http
      .patch(`${this.environment.apiEndPoint}bracket/${id}`, bracket)
      .toPromise();
  }

  deleteBracket(id: string): SubscribableOrPromise<any> {
    return this.http
      .delete(`${this.environment.apiEndPoint}bracket/${id}`)
      .toPromise();
  }

  generateBracket(payload): SubscribableOrPromise<any> {
    return this.http
      .post(`${this.environment.apiEndPoint}match`, payload)
      .toPromise();
  }

  generateBracket1(payload) {
    return this.http.post(`${this.environment.apiEndPoint}match`, payload);
  }

  fetchAllMatches(query): SubscribableOrPromise<any> {
    return this.http
      .get(`${this.environment.apiEndPoint}match${query}`)
      .toPromise();
  }

  saveGeneratedBracket(payload): SubscribableOrPromise<any> {
    return this.http
      .post(`${this.environment.apiEndPoint}match/save`, payload)
      .toPromise();
  }

  fetchDistinctValue(query, field): SubscribableOrPromise<any> {
    return this.http
      .get(
        `${this.environment.apiEndPoint}match/distict-round${query}&field=${field}`
      )
      .toPromise();
  }

  updateMatch(queryParam, payload): SubscribableOrPromise<any> {
    return this.http
      .patch(
        `${this.environment.apiEndPoint}match/update${queryParam}`,
        payload
      )
      .toPromise();
  }

  updateParticipantScore(queryParam, payload): SubscribableOrPromise<any> {
    return this.http
      .patch(
        `${this.environment.apiEndPoint}match/pariticipant-score${queryParam}`,
        payload
      )
      .toPromise();
  }

  battleRoyalePlayerStanding(queryParam): SubscribableOrPromise<any> {
    return this.http
      .get(
        `${this.environment.apiEndPoint}match/battle-royale-standing${queryParam}`
      )
      .toPromise();
  }

  fetchStanding(queryParam): SubscribableOrPromise<any> {
    return this.http
      .get(`${this.environment.apiEndPoint}match/player-standing${queryParam}`)
      .toPromise();
  }

  fetchWinnerList(tId): SubscribableOrPromise<any> {
    return this.http
      .get(`${this.environment.apiEndPoint}match/champions?tId=${tId}`)
      .toPromise();
  }

  // For Single, Double and Round Robin
  assembleStructure(list) {
    const obj = {};
    const insertAt = (array, index, ele) => {
      array[index] = ele;
      return array;
    };

    for (const l of list) {
      obj[l.currentMatch.round] = obj[l.currentMatch.round] || [];
      if (l.currentMatch.hasOwnProperty('subround')) {
        obj[l.currentMatch.round] = obj[l.currentMatch.round] || {};
        obj[l.currentMatch.round].subround =
          obj[l.currentMatch.round].subround || {};
        obj[l.currentMatch.round].subround[l.currentMatch.subround] =
          obj[l.currentMatch.round].subround[l.currentMatch.subround] || [];
        obj[l.currentMatch.round].subround[l.currentMatch.subround] = insertAt(
          obj[l.currentMatch.round].subround[l.currentMatch.subround],
          l.currentMatch.matchPosition,
          l
        );
      } else {
        obj[l.currentMatch.round] = insertAt(
          obj[l.currentMatch.round],
          l.currentMatch.matchPosition,
          l
        );
      }
    }
    return obj;
  }

  // For Multi Stage Battle Royale
  assembleMultiStageBattleStructure(list) {
    if (list) {
      const obj = [];

      for (const l of list) {
        obj[l.currentMatch.stage - 1] = obj[l.currentMatch.stage - 1] || {
          id: l.currentMatch.stage,
          isLoaded: true,
          group: [],
        };

        obj[l.currentMatch.stage - 1].group[l.currentMatch.group - 1] = obj[
          l.currentMatch.stage - 1
        ]?.group[l.currentMatch.group - 1] || {
          id: l.currentMatch.group,
          isLoaded: true,
          round: [],
        };

        obj[l.currentMatch.stage - 1].group[l.currentMatch.group - 1].round[
          l.currentMatch.matchPosition
        ] =
          obj[l.currentMatch.stage - 1]?.group[l.currentMatch.group - 1]?.round[
            l.currentMatch.matchPosition
          ] || l;
      }
      return obj;
    } else {
      return [];
    }
  }

  // For Multi Stage Round Robin
  assembleMultiRoundRobinStructure(list) {
    if (list) {
      const obj = [];
      for (const l of list) {
        obj[l.currentMatch.stage - 1] = obj[l.currentMatch.stage - 1] || {
          id: l.currentMatch.stage,
          isLoaded: true,
          group: [],
        };

        obj[l.currentMatch.stage - 1].group[l.currentMatch.group - 1] = obj[
          l.currentMatch.stage - 1
        ]?.group[l.currentMatch.group - 1] || {
          id: l.currentMatch.group,
          isLoaded: true,
          round: [],
        };

        obj[l.currentMatch.stage - 1].group[l.currentMatch.group - 1].round[
          l.currentMatch.round - 1
        ] = obj[l.currentMatch.stage - 1]?.group[l.currentMatch.group - 1]
          ?.round[l.currentMatch.round - 1] || {
          id: l.currentMatch.round,
          match: [],
        };

        obj[l.currentMatch.stage - 1].group[l.currentMatch.group - 1].round[
          l.currentMatch.round - 1
        ].match[l.currentMatch.matchPosition] = l;
      }
      return obj;
    } else {
      return [];
    }
  }

  // For Multi Stage Round Robin
  assembleMultiStageRoundRobinStructure(list) {
    const obj = {};
    const insertAt = (array, index, ele) => {
      array[index] = ele;
      return array;
    };
    for (const l of list) {
      obj[l.currentMatch.stage] = obj[l.currentMatch.stage] || { group: {} };

      obj[l.currentMatch.stage].group[l.currentMatch.group] = obj[
        l.currentMatch.stage
      ]?.group[l.currentMatch.group] || { round: {} };

      obj[l.currentMatch.stage].group[l.currentMatch.group].round[
        l.currentMatch.round
      ] =
        obj[l.currentMatch.stage].group[l.currentMatch.group].round[
          l.currentMatch.round
        ] || [];
      if (l.currentMatch.hasOwnProperty('subround')) {
        obj[l.currentMatch.stage].group[l.currentMatch.group].round[
          l.currentMatch.round
        ] =
          obj[l.currentMatch.stage].group[l.currentMatch.group].round[
            l.currentMatch.round
          ] || {};
        obj[l.currentMatch.stage].group[l.currentMatch.group].round[
          l.currentMatch.round
        ].subround =
          obj[l.currentMatch.stage].group[l.currentMatch.group].round[
            l.currentMatch.round
          ].subround || {};
        obj[l.currentMatch.stage].group[l.currentMatch.group].round[
          l.currentMatch.round
        ].subround[l.currentMatch.subround] =
          obj[l.currentMatch.stage].group[l.currentMatch.group].round[
            l.currentMatch.round
          ].subround[l.currentMatch.subround] || [];
        obj[l.currentMatch.stage].group[l.currentMatch.group].round[
          l.currentMatch.round
        ].subround[l.currentMatch.subround] = insertAt(
          obj[l.currentMatch.stage].group[l.currentMatch.group].round[
            l.currentMatch.round
          ].subround[l.currentMatch.subround],
          l.currentMatch.matchPosition,
          l
        );
      } else {
        obj[l.currentMatch.stage].group[l.currentMatch.group].round[
          l.currentMatch.round
        ] = insertAt(
          obj[l?.currentMatch?.stage].group[l?.currentMatch?.group].round[
            l?.currentMatch?.round
          ],
          l?.currentMatch?.matchPosition,
          l
        );
      }
    }
    return obj;
  }

  setBracketData(data: any) {
    this.bracketData = data;
  }

  getBracketData() {
    return this.bracketData;
  }

  setWebViewMeta(data) {
    this.mobileWebViewMetaData = data;
  }

  getWebViewMeta() {
    return this.mobileWebViewMetaData;
  }

  fetchMatchResults(id): SubscribableOrPromise<any> {
    return this.http
      .get(`${this.environment.apiEndPoint}match/result-log/${id}`)
      .toPromise();
  }

  fetchAllMatchesV3(query): Promise<any> {
    return this.http
      .get(`${this.environment.apiEndPoint}match/v3`, {
        params: query,
      })
      .toPromise();
  }

  updateParticipantScoreV2(queryParam, payload): Promise<any> {
    return this.http
      .patch(
        `${this.environment.apiEndPoint}match/pariticipant-score/v2`,
        payload,
        {
          params: queryParam,
        }
      )
      .toPromise();
  }

  updateMatchV2(queryParam, payload): Promise<any> {
    return this.http
      .patch(
        `${this.environment.apiEndPoint}match/update/v2`,
        payload,
        {
          params: queryParam,
        }
      )
      .toPromise();
  }
  // ladder
  fetchAllLaderMatches(params): SubscribableOrPromise<any> {
    let url = `${this.environment.apiEndPoint}match/lader-matches?tournamentId=${params.id}&page=${params.page}`;
    return this.http.get(url).toPromise();
  }

}
