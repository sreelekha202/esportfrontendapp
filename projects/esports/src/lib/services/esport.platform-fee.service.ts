import { HttpClient } from "@angular/common/http";
import { Inject, Injectable } from "@angular/core";
import { SubscribableOrPromise } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class EsportsPlatformFeeService {
  constructor(private http: HttpClient,
    @Inject('env') private environment) {}

  getPlatformFee(): SubscribableOrPromise<any> {
    return this.http
      .get(`${this.environment.apiEndPoint}tournament/platform_fee`)
      .toPromise();
  }

  updatePlatformFee(payload): SubscribableOrPromise<any> {
    return this.http
      .patch(`${this.environment.apiEndPoint}tournament/platform_fee`, payload)
      .toPromise();
  }
}
