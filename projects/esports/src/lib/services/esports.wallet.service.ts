import { Inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { Observable, Subject } from 'rxjs';
import { BehaviorSubject } from 'rxjs';
import { GlobalUtils } from './global-utils';

@Injectable({
  providedIn: 'root',
})
export class EsportsWalletService {
  constructor(
    @Inject('env') private environment,
    private httpClient: HttpClient
  ) {}

  BaseURL = this.environment.apiEndPoint;
  private recordsLoaded$ = new Subject<any>();
  public recordsLoadedSubject = new BehaviorSubject('');
  public recordsLoaded = this.recordsLoadedSubject.asObservable();

  public getPurchaseTransactions(pagenum, pagesize) {
    return this.httpClient.get(
      this.environment.apiEndPoint +
        'user/eshop/get-purchase-transactions?page=' +
        pagenum +
        '&pageSize=' +
        pagesize
    );
  }

  public getWalletdetails() {
    return this.httpClient.get(
      this.environment.apiEndPoint + 'user/epm-user-wallet'
    );
  }
  public loadWalletDetails() {
    this.getWalletdetails().subscribe((container: any) => {
      this.recordsLoaded$.next(container);
      this.recordsLoadedSubject.next(container);
    });
  }

  public submitnewuserwallet(data) {
    return this.httpClient.post(
      this.environment.apiEndPoint + 'user/epm-user-wallet',
      data
    );
  }

  public getWallettopupamount() {
    return this.httpClient.get(
      this.environment.apiEndPoint + 'user/wallet-topup-amount'
    );
  }

  public getWallettopupmethods() {
    return this.httpClient.get(
      this.environment.apiEndPoint + 'user/epm-payment-method'
    );
  }

  public gettransactions() {
    return this.httpClient.get(
      this.environment.apiEndPoint + 'user/all-transactions'
    );
  }

  public gettransactionswithpagination(pagenum, pagesize) {
    return this.httpClient.get(
      this.environment.apiEndPoint +
        'user/eshop/get-reload-transactions?page=' +
        pagenum +
        '&pageSize=' +
        pagesize
    );
  }

  public redirectToPayment(data, sst: boolean = false): Observable<any> {
    let redirectURL = '';
    if (GlobalUtils.isBrowser())
      redirectURL = !sst ? `&shopUrl=${window.location.href}` : '';
    return this.httpClient.post(
      this.environment.apiEndPoint +
        `auth/visit-gen-mstoken?sst=${sst}${redirectURL}`,
      data
    );
  }

  public getlatesttransaction() {
    return this.httpClient.get(
      this.environment.apiEndPoint + 'user/latest-transactions'
    );
  }

  public redirecttopaymentforDBC(data, sst: boolean = false) {
    return this.httpClient.post(
      this.environment.apiEndPoint + `user/dcb-charge?sst=${sst}`,
      data
    );
  }

  public subscribeToLoadedRecords(callback: (b: any) => void): Subscription {
    return this.recordsLoaded$.subscribe(callback);
  }
  public updateSubscriberProfile(): Observable<any> {
    return this.httpClient.get(
      this.environment.apiEndPoint + 'auth/update-subscriber-profile'
    );
  }

  // wallet

  public  getPayMark(data):Observable<any> {
    let url = this.environment.apiEndPoint +'v2/transaction/paymark-charge';
    return  this.httpClient.post(url, data);

  }

}
