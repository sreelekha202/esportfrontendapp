import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';

@Injectable({
  providedIn: 'root',
})
export class EsportsSeasonService {
  constructor(private http: HttpClient) {}

  getSeasons(api, query): Observable<any> {
    return this.http.get(api + `seasons?${query}`);
  }

  saveSeason(api, formValues): Observable<any> {
    const data = formValues;
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    return this.http.post(api + 'seasons', data, {
      headers: httpHeaders,
    });
  }
  updateSeason(api, formValues): Observable<any> {
    const data = formValues;
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    return this.http.patch(api + 'seasons', data, {
      headers: httpHeaders,
    });
  }
  toBase64(file) {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = (error) => reject(error);
    });
  }

  checkSeasons(api, data): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    return this.http.post(api + 'seasons/checkSeasonExist', data, {
      headers: httpHeaders,
    });
  }
}
