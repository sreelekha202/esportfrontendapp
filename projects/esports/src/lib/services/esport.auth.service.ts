import { MatDialog } from '@angular/material/dialog';
import { Inject, Injectable } from "@angular/core";
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
} from "@angular/common/http";

import { BehaviorSubject, Observable, throwError } from "rxjs";
import { map } from "rxjs/operators";

import { CookieService } from "ngx-cookie-service";
import { Router } from "@angular/router";
import { GlobalUtils } from './global-utils';

@Injectable({
  providedIn: "root",
})
export class EsportsAuthServices {
  // store the URL so we can redirect after logging in
  redirectUrl: string;

  constructor(
    @Inject('env') private environment,
    private cookieService: CookieService,
    private globalUtils: GlobalUtils,
    private http: HttpClient,
    private router: Router,
    private dialog: MatDialog
  ) { }

  register(data): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set("Content-Type", "application/json");
    // return this.http.post(API + "stc_register", data, { headers: httpHeaders });
    return this.http.post(this.environment.apiEndPoint + "auth/register", data, { headers: httpHeaders });
  }

  confirmUser(utoken: string, code: string, type: string): Observable<any> {
    const data = {
      token: utoken,
      otp: code,
      type: type,
    };

    const httpHeaders = new HttpHeaders();
    httpHeaders.set("Content-Type", "application/json");
    return this.http.post(this.environment.apiEndPoint + "auth/stc_verify", data, { headers: httpHeaders });
  }

  login(data) {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set("Content-Type", "application/json");

    return this.http.post<any>(this.environment.apiEndPoint + "auth/login", data, {
      headers: httpHeaders,
    });
  }

  update(data): Observable<any> {
    return this.http.put(this.environment.apiEndPoint + "auth/login_update", data);
  }

  confirm(uid: string, code: string, utype: string): Observable<any> {
    const data = {
      otp: code,
      type: utype,
      id: uid,
    };

    const httpHeaders = new HttpHeaders();
    httpHeaders.set("Content-Type", "application/json");
    return this.http.post(this.environment.apiEndPoint + "auth/login_verify", data, {
      headers: httpHeaders,
    });
  }

  social(authtoken, sprovider) {
    if (GlobalUtils.isBrowser()) {
      const httpHeaders = new HttpHeaders();
      httpHeaders.set("Content-Type", "application/json");

      const data = {
        type: "formation",
        provider: sprovider,
        token: authtoken,
      };

      return this.http
        .post<any>(this.environment.apiEndPoint + "auth/social_login", data, {
          headers: httpHeaders,
        })
        .pipe(
          map((user) => {
            if (user.code == "ER1001") {
              return user;
            } else {
              this.setCookie(user.data.token, "accessToken");
              // store user details and jwt token in local storage to keep user logged in between page refreshes
              localStorage.setItem(this.environment.currentToken, user.data.token);
              return user;
            }
          })
        );
    }
  }

  social_login(data): Observable<any> {
    return this.http.post(this.environment.apiEndPoint + "auth/social_login", data);
  }

  resendOTP(data): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set("Content-Type", "application/json");
    return this.http.post(this.environment.apiEndPoint + "auth/resend_otp", data);
  }

  forgotPassword(data): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set("Content-Type", "application/json");
    return this.http.post(this.environment.apiEndPoint + "auth/forgot_password", data);
  }

  isAuthenticated() { }

  initAuth() { }

  verify(username: string) { }

  devicelist() { }

  // Error
  handleError(error: HttpErrorResponse) {
    let msg = "";
    if (error.error instanceof ErrorEvent) {
      // client-side error
      msg = error.error.message;
    } else {
      // server-side error
      msg = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    return throwError(msg);
  }

  setCookie(value, name) {
    var date = new Date();
    const MILLISECONDS_IN_A_DAY = 864000;

    if (GlobalUtils.isBrowser()) {
      // Get Unix milliseconds at current time plus 365 days
      date.setTime(+date + 365 * MILLISECONDS_IN_A_DAY); //24 \* 60 \* 60 \* 100
      this.cookieService.set(name, value, date, "/", this.environment.cookieDomain);
    }
  }

  searchUsername(text, Id?): Observable<any> {
    const urlString = Id
      ? `auth/search_username?text=${encodeURIComponent(text)}&Id=${Id}`
      : `auth/search_username?text=${encodeURIComponent(text)}`;
    return this.http.get(this.environment.apiEndPoint + urlString);
  }

  getPaymentAccountVerfiedStatus = async () => {
    try {
      const accountPaymentStatus: any = await this.http
        .get(`${this.environment.apiEndPoint}auth/getPaymentAccountStatus`)
        .toPromise();

      if (
        accountPaymentStatus?.data &&
        accountPaymentStatus?.data.hasOwnProperty('stcAccountVerified') &&
        !accountPaymentStatus?.data?.stcAccountVerified
      ) {
        // const result = await this.dialog
        //   .open(StcPopupComponent, {
        //     panelClass: 'payment-dialog',
        //     autoFocus: false,
        //     restoreFocus: false,
        //   })
        //   .afterClosed()
        //   .toPromise();
        // return result;
      } else if (!accountPaymentStatus?.data) {
        this.router.navigateByUrl('/user/phone-login');
      } else {
        return true;
      }
    } catch (error) {
      throw error;
    }
  };

  // Registration FormData
  public formDataSubject=new BehaviorSubject(null);
  public formData=this.formDataSubject.asObservable()

}
