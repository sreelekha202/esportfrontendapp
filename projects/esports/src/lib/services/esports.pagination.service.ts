import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class EsportsPaginationService {
  pageChanged = new BehaviorSubject(false);
  constructor() {}

  setPageChanged(val) {
    this.pageChanged.next(val);
  }
}
