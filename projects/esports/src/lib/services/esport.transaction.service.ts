import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Inject, Injectable } from "@angular/core";

import { Observable, SubscribableOrPromise } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class EsportsTransactionService {
  constructor(private http: HttpClient, @Inject('env') private environment) { }

  getTransactionDetail(api, tDetail): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set("Content-Type", "application/json");

    return this.http.post(
      api + "transaction/getDetail",
      tDetail,
      {
        headers: httpHeaders,
      }
    );
  }

  createOrUpdateTransaction(api, payload): SubscribableOrPromise<any> {
    return this.http
      .patch(`${api}transaction/pay`, payload)
      .toPromise();
  }

  verifyStcTransaction(api, payload): SubscribableOrPromise<any> {
    return this.http
      .post(`${api}transaction/stcVerifyPay`, payload)
      .toPromise();
  }

  getProduct(api, type): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set("Content-Type", "application/json");
    const url = `${api}transaction/getProduct?type=${type}`;
    return this.http.get(url);
  }

  paymentDetail(api, params): SubscribableOrPromise<any> {
    const encodedPagination = encodeURIComponent(params["pagination"]);
    const url = `${api}transaction/paymenthitstory?pagination=${encodedPagination}`;
    return this.http.get(url).toPromise();
  }
  paymentHistory(api, params): SubscribableOrPromise<any> {
    const encodedPagination = encodeURIComponent(params["pagination"]);
    const url = `${api}transaction/admin/paymentHistory?userId=${params.userId}&pagination=${encodedPagination}`;
    return this.http.get(url).toPromise();
  }
  getRegFeeDisbursalStatus(api, tournamentId): SubscribableOrPromise<any> {
    const url = `${api}transaction/disburse_reg_fee_status?tournamentId=${tournamentId}`;
    return this.http.get(url).toPromise();
  }

  disburseRegFee(api, payload): SubscribableOrPromise<any> {
    const url = `${api}transaction/disburse_reg_fee`;
    return this.http.post(url, payload).toPromise();
  }

  getPrizeMoneyRefundStatus(api, tournamentId): SubscribableOrPromise<any> {
    const url = `${api}transaction/prize_money_refund_status?tournamentId=${tournamentId}`;
    return this.http.get(url).toPromise();
  }
  refundPrizeMoney(api, payload): SubscribableOrPromise<any> {
    const url = `${api}transaction/refund_prize_money`;
    return this.http.post(url, payload).toPromise();
  }


  getPaymentDetails(api, payload): Promise<any> {
    const url = `${api}v2/transaction/payment-details`;
    return this.http.get(url, {
      params: {
        ...payload
      }
    }).toPromise();
  }

  createTransaction(api, payload): Promise<any> {
    const url = `${api}v2/transaction/create`;
    return this.http.post(url, payload).toPromise();
  }


  getRegFeeRefundStatus(query): SubscribableOrPromise<any> {
    const url = `${this.environment.apiEndPoint}transaction/reg_fee_refund_status`;
    return this.http.get(url, { params: query }).toPromise();
  }
  refundRegFee(payload): SubscribableOrPromise<any> {
    const url = `${this.environment.apiEndPoint}transaction/refund_reg_fee`;
    return this.http.post(url, payload).toPromise();
  }
}
