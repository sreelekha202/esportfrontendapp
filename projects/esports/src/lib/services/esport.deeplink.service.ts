import { Inject, Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class EsportsDeeplinkService {
  constructor(@Inject('env') private environment) {}

  deeplink({ objectType, objectId }) {
    let deeplinkURL = `${this.environment.iOSBundelName}://${objectType}?id=${objectId}`;

    let ua = navigator.userAgent.toLowerCase();
    let isAndroid = ua.indexOf('android') > -1; // android check
    let isIphone = ua.indexOf('iphone') > -1; // ios check

    if (isIphone == true) {
      let app = {
        launchApp: function () {
          setTimeout(function () {
            window.location.href = `${this.environment.iOSAppstoreURL}`;
          }, 25);
          window.location.href = deeplinkURL; //which page to open(now from mobile, check its authorization)
        },
        openWebApp: function () {
          window.location.href = `${this.environment.iOSBundelName}`;
        },
      };
      app.launchApp();
    } else if (isAndroid == true) {
      let app = {
        launchApp: function () {
          window.location.replace('bundlename://linkname'); //which page to open(now from mobile, check its authorization)
          setTimeout(this.openWebApp, 500);
        },
        openWebApp: function () {
          window.location.href =
            'https://play.google.com/store/apps/details?id=packagename';
        },
      };
      app.launchApp();
    }
  }
}
