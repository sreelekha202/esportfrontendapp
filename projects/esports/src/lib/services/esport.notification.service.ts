import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class EsportsNotificationsService {
  constructor(private http: HttpClient) {}

  sendInboxMessages(api, inputData): Observable<any> {
    const data = inputData;
    data.requestType = 'inbox-message';
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    return this.http.post(
      api + `user-notifications`,
      data,
      {
        headers: httpHeaders,
      }
    );
  }

  sendEmails(api, inputData): Observable<any> {
    const data = inputData;
    data.requestType = 'email';
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    return this.http.post(
      api + `user-notifications`,
      data,
      {
        headers: httpHeaders,
      }
    );
  }

  registerPushToken(api, token): Observable<any> {
    const data = {
      requestType: 'register-push-notification',
      platform: 'web',
      token,
    };
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    return this.http.post(
      api + `user-notifications`,
      data,
      {
        headers: httpHeaders,
      }
    );
  }

  sendPushNotifications(api, inputData) {
    const data = inputData;
    data.requestType = 'push-notification';
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    return this.http.post(
      api + `user-notifications`,
      data,
      {
        headers: httpHeaders,
      }
    );
  }

  getSentItems(api) {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    return this.http.post(
      api + `user-notifications`,
      { requestType: 'sent' },
      {
        headers: httpHeaders,
      }
    );
  }
}
