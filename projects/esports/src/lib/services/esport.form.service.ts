import { Injectable } from "@angular/core";
import {
  AbstractControl,
  FormArray,
  FormControl,
  FormGroup,
  ValidationErrors,
  ValidatorFn,
} from "@angular/forms";

@Injectable({
  providedIn: "root",
})
export class EsportsFormService {
  constructor() {}

  groupFieldValidator(): ValidatorFn {
    return (group: FormGroup): ValidationErrors => {
      const value = group.value;
      const isValid = Object.values(value).some((el: any) => el.trim());
      return isValid ? null : { required: true };
    };
  }

  // max length
  arrayMaxLength(max: number): ValidatorFn | any {
    return (control: AbstractControl[]) => {
      if (!(control instanceof FormArray)) return;
      return control.length > max ? { maxLength: true } : null;
    };
  }

  // min length
  minLength(min: number): ValidatorFn | any {
    return (control: AbstractControl[]) => {
      if (!(control instanceof FormArray)) return;
      return control.length < min ? { minLength: true } : null;
    };
  }

  emptyCheck = (control: FormControl) => {
    return control.value && control.value.trim() ? null : { required: true };
  };

  minLengthArray(min: number) {
    return (c: AbstractControl): {[key: string]: any} => {
        if (c?.value?.length >= min)
            return null; 

        return { 'minLengthArray': {valid: false }};
    }
  }
}
