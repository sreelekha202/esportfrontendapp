import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ICOMMENT } from '../models/commentPost';

@Injectable({
  providedIn: 'root',
})
export class EsportsSocialService {
  constructor(private http: HttpClient, @Inject('env') private env) {}

  getPostPreLogin(skip: number, limit: number): Observable<any> {
    const url = `${this.env.apiEndPoint}social/post/home?option=%7B%22skip%22%3A${skip}%2C%22limit%22%3A${limit}%7D`;
    return this.http.get(url);
  }
  getPostAfterLogin(skip: number, limit: number): Observable<any> {
    const url = `${this.env.apiEndPoint}social/post?query=%7B%22isPrivate%22%3Afalse%7D&option=%7B%22skip%22%3A${skip}%2C%22limit%22%3A${limit}%7D`;
    return this.http.get(url);
  }
  getTrendingPosts(): Observable<any> {
    const url = `${this.env.apiEndPoint}social/trending`;
    return this.http.get(url);
  }

  getPostById(postId: string): Observable<any> {
    const url = `${this.env.apiEndPoint}social/post/${postId}`;
    return this.http.get(url);
  }

  getUserProfile(userId: string): Observable<any> {
    const url = `${this.env.apiEndPoint}user/profile/${userId}`;
    return this.http.get(url);
  }

  getPostPrivate(userId: string, skip: number, limit: number): Observable<any> {
    const url = `${this.env.apiEndPoint}social/post?query=%7B%22createdBy%22%3A%22${userId}%22%7D&option=%7B%22skip%22%3A${skip}%2C%22limit%22%3A${limit}%7D`;
    return this.http.get(url);
  }
  getPostPublic(userId: string, skip: number, limit: number): Observable<any> {
    const url = `${
      this.env.apiEndPoint
    }social/post?query=%7B%22createdBy%22%3A%22${userId}%22%2C%22isPrivate%22%3A${false}%7D&option=%7B%22skip%22%3A${skip}%2C%22limit%22%3A${limit}%7D`;
    return this.http.get(url);
  }

  addComment(postId: string, comment: ICOMMENT): Observable<any> {
    const url = `${this.env.apiEndPoint}social/comment?postId=${postId}`;
    const data = { message: comment.message, imagesLink: comment.imagesLink };
    return this.http.post(url, data);
  }

  updateComment(commentId: string, comment: ICOMMENT): Observable<any> {
    const url = `${this.env.apiEndPoint}social/comment/${commentId}`;
    const data = { message: comment.message, imagesLink: comment.imagesLink };
    return this.http.patch(url, data);
  }

  deleteComment(commentId: string): Observable<any> {
    const url = `${this.env.apiEndPoint}social/comment/${commentId}`;
    return this.http.delete(url);
  }

  addLikePost(data: Object, postId: string): Observable<any> {
    const url = `${this.env.apiEndPoint}social/like/?postId=${postId}`;
    return this.http.post(url, data);
  }

  removeLikePost(likeId: string): Observable<any> {
    const url = `${this.env.apiEndPoint}social/like/${likeId}`;
    return this.http.delete(url);
  }

  updateLikePost(data: Object, likeId: string): Observable<any> {
    const url = `${this.env.apiEndPoint}social/like/${likeId}`;
    return this.http.patch(url, data);
  }

  addReply(
    postId: string,
    commentId: string,
    payload: ICOMMENT
  ): Observable<any> {
    const url = `${this.env.apiEndPoint}social/comment?postId=${postId}&commentParent=${commentId}`;
    return this.http.post(url, payload);
  }

  getNofitication(): Observable<any> {
    const url = `${this.env.apiEndPoint}social/notification`;
    return this.http.get(url);
  }

  addNewPost(data: object): Observable<any> {
    const url = `${this.env.apiEndPoint}social/post`;
    return this.http.post(url, data);
  }

  updatePost(postId: string, data: object): Observable<any> {
    const url = `${this.env.apiEndPoint}social/post/${postId}`;
    return this.http.patch(url, data);
  }

  uploadImages(data: object): Observable<any> {
    const url = `${this.env.apiEndPoint}social/post/uploadImage`;
    return this.http.post(url, data);
  }

  signedFile(filename: string, type: string): Observable<any> {
    const url = `${this.env.apiEndPoint}file-upload?filename=${filename}&type=${type}`;
    return this.http.get(url);
  }

  putFile(signed: string, file: any): Observable<any> {
    let formdata = new FormData();
    formdata.append('file', file);
    return this.http.put(signed, formdata);
  }

  deletePostById(postId: string): Observable<any> {
    const url = `${this.env.apiEndPoint}social/post/${postId}`;
    return this.http.delete(url);
  }

  getCommentById(commentId: string): Observable<any> {
    const url = `${this.env.apiEndPoint}social/comment/${commentId}`;
    return this.http.get(url);
  }

  getLikeById(likeId: string): Observable<any> {
    const url = `${this.env.apiEndPoint}social/like/${likeId}`;
    return this.http.get(url);
  }

  getReportById(reportId: string): Observable<any> {
    const url = `${this.env.apiEndPoint}social/report/${reportId}`;
    return this.http.get(url);
  }

  reportPost(postId: string, type: string, data): Observable<any> {
    const url = `${this.env.apiEndPoint}social/report?postId=${postId}`;
    return this.http.post(url, data);
  }

  getCommentByPost(
    postId: string,
    skip: number,
    limit: number
  ): Observable<any> {
    const url = `${this.env.apiEndPoint}social/comment?query=%7B%22postId%22%3A%22${postId}%22%2C%22commentParent%22%3Anull%7D&option=%7B%22skip%22%3A${skip}%2C%22limit%22%3A${limit}%7D`;
    return this.http.get(url);
  }

  getReplyByComment(
    commentId: string,
    skip: number,
    limit: number
  ): Observable<any> {
    const url = `${this.env.apiEndPoint}social/comment?query=%7B%22commentParent%22%3A%22${commentId}%22%7D&option=%7B%22skip%22%3A${skip}%2C%22limit%22%3A${limit}%7D`;
    return this.http.get(url);
  }

  getPreviewURL(link: string): Observable<any> {
    const url = `${this.env.apiEndPoint}social/post/urlMetadata`;
    const data = { url: link };
    return this.http.post(url, data);
  }
  changeStatusNotifi(notificationId: string): Observable<any> {
    const url = `${this.env.apiEndPoint}social/notification/${notificationId}`;
    return this.http.patch(url, null);
  }
}
