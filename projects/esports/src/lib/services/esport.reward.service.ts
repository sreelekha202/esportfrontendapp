import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class EsportsRewardService {
  constructor(private httpClient: HttpClient) {}

  public getRewardsData(api) {
    return this.httpClient.get(`${api}general/reward`);
  }
  public updateRewards(api, formArrayData) {
    return this.httpClient.post(`${api}general/reward`, formArrayData);
  }
}
