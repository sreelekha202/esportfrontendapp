import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class EsportsProfileService {

  constructor(private httpClient: HttpClient, @Inject('env') private environment,) { }

  getMatches(params): Observable<any> {
    return this.httpClient.get(`${this.environment.apiEndPoint}/match/v2/my_matches`, { params: params });
  }
  getMyStatistics(id, params): Observable<any> {
    return this.httpClient.get(`${this.environment.apiEndPoint}home/fetchleaderboard/${id}`, { params: params });
  }
  getMyJoinedTournament(params): Observable<any> {
    return this.httpClient.get(`${this.environment.apiEndPoint}participant/tournament`, { params: params });
  }
  getMyCreatedTournament(params): Observable<any> {
    return this.httpClient.get(`${this.environment.apiEndPoint}tournament/my_tournaments`, { params: params });
  }
  getBookmarks(params): Observable<any> {
    return this.httpClient.get(`${this.environment.apiEndPoint}userpreference`, { params: params });
  }
  getPurchaseHistory(params): Observable<any> {
    return this.httpClient.get(`${this.environment.apiEndPoint}transaction/paymenthitstory`, { params: params });
  }
  getRewardseHistory(params): Observable<any> {
    return this.httpClient.get(`${this.environment.apiEndPoint}user/reward_transaction`, { params: params });
  }
  getMyTeams(params): Observable<any> {
    return this.httpClient.get(`${this.environment.apiEndPoint}user/my_teams`, { params: params });
  }
  getMyInvites(params): Observable<any> {
    return this.httpClient.get(`${this.environment.apiEndPoint}user/my_invites`, { params: params });
  }

  getMyTeam(params): Observable<any> {
    const encodedPagination = encodeURIComponent(params['pagination']);
    const url = `${this.environment.apiEndPoint}user/my_teams?pagination=${encodedPagination}`;
    return this.httpClient.get(url);
  }

  getTeamIvitationsSent(teamId, pagination): Promise<any> {
    const url = `${this.environment.apiEndPoint}user/team_invites_sent/${teamId}`;
    return this.httpClient
      .get(url, {
        params: pagination,
      })
      .toPromise();
  }

  deleteTeam(teamId: number): Observable<void> {
    return this.httpClient.delete<void>(`url/${teamId}`)

  }

  getMyInvite(params): Observable<any> {
    const encodedPagination = encodeURIComponent(params['pagination']);
    const url = `${this.environment.apiEndPoint}user/my_invites?pagination=${encodedPagination}`;
    return this.httpClient.get(url);
  }

  getplayers(id): Observable<any> {
    return this.httpClient.get(`${this.environment.apiEndPoint}home/team/${id}`);
  }

  accept_reject(data) {
    return this.httpClient.post(`${this.environment.apiEndPoint}user/update_invites`, data);
  }

  jointournament(data) {
    return this.httpClient.post(`${this.environment.apiEndPoint}participant`, data);
  }

  ckeckuserid(data, id) {
    const url = `${this.environment.apiEndPoint}participant/search?field=inGamerUserId&text=${data}&tournamentId=${id}`;
    return this.httpClient.get(url);
  }

  // Content
  getContent(params): Observable<any> { return this.httpClient.get(`${this.environment.apiEndPoint}article`, { params: params }); }
  getAllCategories(): Observable<any> { return this.httpClient.get(`${this.environment.apiEndPoint}option/all-categories`); }
  getAllTags(): Observable<any> { return this.httpClient.get(`${this.environment.apiEndPoint}option/all-tags`); }
  getAllGame(): Observable<any> { return this.httpClient.get(`${this.environment.apiEndPoint}game`); }
  getAllGenres(): Observable<any> { return this.httpClient.get(`${this.environment.apiEndPoint}option/all-genres`) }
  saveContent(data): Observable<any> { return this.httpClient.post(`${this.environment.apiEndPoint}article`, data); }
  getGameDetails(id): Observable<any> { return this.httpClient.get(`${this.environment.apiEndPoint}tournament/${id}`); }
  // manage Tournamnet
  getUserReport(t_id): Observable<any> { return this.httpClient.get(`${this.environment.apiEndPoint}tournament/reports_by_tournament/${t_id}`,); }
  getParticipant(params): Observable<any> { return this.httpClient.get(`${this.environment.apiEndPoint}participant/v2`, { params: params }); }
 
}
