import { Injectable, Inject, PLATFORM_ID } from '@angular/core';
// import { environment } from './../../../environments/environment';
import { Observable, BehaviorSubject, Subscription, Subscriber } from 'rxjs';
// https://github.com/angular/universal/issues/1236
// import { Socket } from 'ngx-socket-io';
import io from 'socket.io-client';
import { isPlatformBrowser } from '@angular/common';
import { HttpClient, HttpHeaders, HttpBackend } from '@angular/common/http';

import { EsportsTournamentService } from './esport.tournament.service';

import { IChat } from '../models/chat';

// import { TournamentService } from './tournament.service';
import { EsportsUserService } from './esport.user.service';
import { GlobalUtils } from './global-utils';
import {
  IO_COMMENT,
  IO_LIKE,
  IO_REPLY,
  IO_REPORT,
} from '../models/feed-socketIo';

@Injectable({
  providedIn: 'root',
})
export class EsportsChatService {
  public tournamentList = [];
  //   private url = environment.socketEndPoint;
  public url: string;
  public currentToken;
  public userId: any;
  public accessToken: string = 'a';
  public chatStatus: Observable<any>;
  public chatStatusSub: BehaviorSubject<boolean>;
  public chatWindowPos: Observable<any>;
  public chatWindowPosSub: BehaviorSubject<any>;
  public cid: string;
  public currentMatch: Observable<any>;
  public currentMatchSub: BehaviorSubject<any>;
  public currentUserId: any;
  public currenUser: any;
  public isUserLoggedIn: any;
  public login: Observable<any>;
  public loginSub: BehaviorSubject<any>;
  public mtches: Observable<any>;
  public mtchesSub: BehaviorSubject<any>;
  public refreshToken: string;
  public showStatus = false;
  public socket: any;
  public totalMatches: number;
  public typeOfChat: Observable<any>;
  public typeOfChatSub: BehaviorSubject<any>;

  public chathistory: Observable<any>;
  public chathistorySub: BehaviorSubject<any>;

  public showTextAndButton: Observable<any>;
  public showTextAndButtonSub: BehaviorSubject<any>;

  public isAdminMessages: Observable<any>;
  public isAdminMessagesSub: BehaviorSubject<any>;

  public unreadCount: Observable<any>;
  public unreadCountSub: BehaviorSubject<any>;
  userSubscription: Subscription;

  constructor(
    @Inject(PLATFORM_ID) private platformId: Object, // private socket: Socket
    private handler: HttpBackend,
    private httpClient: HttpClient,
    private tournamentService: EsportsTournamentService,
    private userService: EsportsUserService,
    @Inject('env') private env
  ) {
    // this.httpClient = new HttpClient(handler);
    this.currentMatchSub = new BehaviorSubject(null);
    this.currentMatch = this.currentMatchSub.asObservable();
    this.typeOfChatSub = new BehaviorSubject([]);
    this.typeOfChat = this.typeOfChatSub.asObservable();
    this.mtchesSub = new BehaviorSubject([]);
    this.mtches = this.mtchesSub.asObservable();
    this.loginSub = new BehaviorSubject([]);
    this.login = this.loginSub.asObservable();

    this.chathistorySub = new BehaviorSubject([]);
    this.chathistory = this.chathistorySub.asObservable();

    this.unreadCountSub = new BehaviorSubject([]);
    this.unreadCount = this.unreadCountSub.asObservable();

    this.chatStatusSub = new BehaviorSubject<boolean>(false);
    this.chatStatus = this.chatStatusSub.asObservable();

    this.showTextAndButtonSub = new BehaviorSubject<boolean>(false);
    this.showTextAndButton = this.showTextAndButtonSub.asObservable();

    this.isAdminMessagesSub = new BehaviorSubject<boolean>(false);
    this.isAdminMessages = this.isAdminMessagesSub.asObservable();

    this.chatWindowPosSub = new BehaviorSubject<string>(
      'chat_window chat_window_right'
    );
    this.chatWindowPos = this.chatWindowPosSub.asObservable();
    this.url = this.env.socketEndPoint;
    this.currentToken = this.env.currentToken;

    if (GlobalUtils.isBrowser()) {
      if (this.currentToken) {
        this.getCurrentUserDetails();
      }
    }
  }

  public initialiseSocket() {
    if (isPlatformBrowser(this.platformId)) {
      const ctoken = localStorage.getItem('refreshToken');
      if (ctoken) {
        //if (!this.socket) {
        this.socket = io(this.url, {
          query: {
            token: ctoken,
          },
          transports: ['websocket'],
          upgrade: false,
        });
        //}
      }
    }
  }

  public getCurrentUserDetails() {
    this.userSubscription = this.userService.currentUser.subscribe(
      (data: any) => {
        if (data) {
          this.userId = data._id;
          this.currenUser = data;
          this.isUserLoggedIn = !!this.currenUser;
          this.currentUserId = this.currenUser._id;
          this.loginSub.next(data);
          if (isPlatformBrowser(this.platformId)) {
            this.refreshToken = localStorage.getItem('refreshToken');
          }
        }
      }
    );
  }

  public doLogin(pnumber, password, type) {
    const gurl = `${this.url}/login`;
    const body = { phone_number: pnumber, password, type };
    this.httpClient.post<any>(gurl, body).subscribe((data) => {
      this.accessToken = data.accessToken;
      this.refreshToken = data.refreshToken;
      this.loginSub.next(data);
    });
  }

  public doRefresh() {
    if (this.refreshToken != '') {
      const gurl = `${this.url}/token`;
      const body = { token: this.refreshToken };
      this.httpClient.post<any>(gurl, body).subscribe((data) => {
        this.accessToken = data.accessToken;
      });
    }
  }

  public doLogout() {
    // const gurl = `${this.url}/logout`;
    // const body = { token: this.accessToken };
    // this.httpClient.post<any>(gurl, body).subscribe((data) => {
    //   this.accessToken = '';
    //   this.refreshToken = '';
    // });
  }

  public connectUser() {
    if (isPlatformBrowser(this.platformId) && this.currentUserId) {
      this.socket?.emit('connectuser', this.currentUserId);
    }
  }

  public disconnectUser() {
    if (isPlatformBrowser(this.platformId)) {
      this.socket?.emit('disconnectuser', this.currentUserId);
      this.userSubscription?.unsubscribe();
    }
  }

  public getCurrentUserId() {
    return this.currentUserId;
  }

  public connectMatch(matchid, type) {
    if (isPlatformBrowser(this.platformId)) {
      const body = { matchid, type };
      this.socket?.emit('matchConnected', body);
    }
  }

  public disconnectMatch(matchid, type) {
    if (matchid && type) {
      if (isPlatformBrowser(this.platformId)) {
        const body = { matchid, type };
        this.socket?.emit('matchDisconnected', body);
      }
    }
  }

  public sendMessage(message: IChat, type) {
    if (isPlatformBrowser(this.platformId)) {
      const body = { message, type };
      const receivemessage = {
        receiverid: message.receiverid,
        matchid: message.matchid,
      };
      this.socket?.emit('new-message', body);
      this.socket?.emit('new-message-for-user', receivemessage);
      // if (this.currentUserId) {
      //   this.getAllMatches(this.currentUserId);
      // }
    }
  }

  public deleteUnread(matchid) {
    if (isPlatformBrowser(this.platformId)) {
      const body = { userid: this.currentUserId, matchid };
      this.socket?.emit('delete-unread', body);
    }
  }

  public sendTyping(typingObject, type) {
    if (isPlatformBrowser(this.platformId)) {
      const body = { typingObject, type };
      this.socket?.emit('typing', body);
    }
  }

  public stopTyping(typingObject, type) {
    if (isPlatformBrowser(this.platformId)) {
      const body = { typingObject, type };
      this.socket?.emit('stopTyping', body);
    }
  }

  public upload(formData) {
    const gurl = `${this.url}/uploadfiles`;
    return this.httpClient.post<any>(gurl, formData);
  }

  public getMessages = (next) => {
    if (isPlatformBrowser(this.platformId)) {
      this.socket?.on('received', (message) => {
        if (this.getChatStatus() == true) {
          if (message.receiverid == this.currentUserId) {
            this.deleteUnread(message.matchid);
          }
        }
        next(message);
      });
    }
  };

  public getTypingInfo1 = (next) => {
    if (isPlatformBrowser(this.platformId)) {
      this.socket?.on('notifyTyping', (typingObject) => {
        next(typingObject);
      });
    }
  };

  public getStopTypingInfo1 = (next) => {
    if (isPlatformBrowser(this.platformId)) {
      this.socket?.on('notifyStopTyping', (typingObject) => {
        next(typingObject);
      });
    }
  };

  public getUnReadCount = () => {
    return Observable.create((observer) => {
      if (isPlatformBrowser(this.platformId)) {
        this.socket?.on('received-message-for-user', (receivemessage) => {
          this.getAllMatches(this.currentUserId, 1, 30);
          observer.next(receivemessage);
        });
      }
    });
  };

  public getDeleteReceived = () => {
    return Observable.create((observer) => {
      if (isPlatformBrowser(this.platformId)) {
        this.socket?.on('delete-message-received', (receivemessage) => {
          observer.next(receivemessage);
        });
      }
    });
  };

  public getAllMatches(userid, page, limit) {
    const gurl = `${this.url}/chat-history?page=${page}&limit=${limit}`;
    this.httpClient?.get(gurl).subscribe((res: any) => {
      const mlist = [];
      for (let i = 0; i < res?.docs?.length; i++) {
        if (res?.docs[i]._id != this.currentUserId) {
          mlist.push(res?.docs[i]);
        }
      }
      this.unreadCountSub.next(0);
      this.mtchesSub.next(mlist);
    });
  }

  public deleteChat(chatid, deletereceiverid) {
    if (typeof this.accessToken != 'undefined' && this.accessToken != null) {
      const body = {
        chatid,
        receiverid: deletereceiverid,
        senderid: this.currentUserId,
      };
      this.socket?.emit('delete-message', body);
    }
  }

  public getBlockedUsers(userId) {
    const gurl = `${this.url}/blockuser/get-blocked-users/${userId}`;
    return this.httpClient?.get(gurl);
  }

  public unblockUser(body) {
    const gurl = `${this.url}/blockuser/unblock-user`;
    return this.httpClient.post<any>(gurl, body);
  }

  public blockUser(body) {
    const gurl = `${this.url}/blockuser`;
    return this.httpClient.post<any>(gurl, body);
  }

  public setChatHistory(chathistory) {
    const chathis = JSON.parse(chathistory.replace(/\\"/g, '"'));
    this.chathistorySub.next(chathis);
  }

  public changeBlockStatus(cblockid, status) {
    let gurl = `${this.url}/changeblockstatus/${cblockid}/${status}`;
    return this.httpClient?.get(gurl);
  }

  public getGameIds(gameid: string, user2: string) {
    let gurl = `${this.url}/getGameIDs/${gameid}/${this.currentUserId}/${user2}`;
    return this.httpClient?.get(gurl);
  }

  public getAllChatMessages(matchid, page, limit) {
    const gurl = `${this.url}/chats-v2/v1/${matchid}?page=${page}&limit=${limit}`;
    return this.httpClient?.get(gurl);
  }

  public getAllUserChatMessages(matchid, page, limit) {
    const gurl = `${this.url}/user-v2/v1/get-one-to-one-all-chats?uniqueId=${matchid}&page=${page}&limit=${limit}`;
    return this.httpClient?.get(gurl);
  }

  public getAllSpamList() {
    const gurl = `${this.url}/spamlist/all`;
    return this.httpClient?.get(gurl);
  }

  public getOwnerForTournament(tournamentid) {
    const gurl = `${this.url}/ownerfortournament/${tournamentid}`;
    return this.httpClient?.get(gurl);
  }

  public getMatchInformation(matchid) {
    const gurl = `${this.url}/matchdetails/${matchid}`;
    return this.httpClient?.get(gurl);
  }

  public searchUsers(text) {
    const gurl = `${this.url}/user-v2/search_users?text=${text}&spam=true`;
    return this.httpClient?.get(gurl);
  }

  public startTournament(matchid) {
    const gurl = `${this.url}/addMatchDetails/${matchid}`;
    return this.httpClient?.get(gurl);
  }

  public setChatStatus(cstatus: boolean) {
    this.chatStatusSub.next(cstatus);
  }

  public setTextButtonStatus(cstatus: boolean) {
    this.showTextAndButtonSub.next(cstatus);
  }

  public getTextButtonStatus() {
    return this.showTextAndButtonSub.getValue();
  }

  public setAdminMessageStatus(cstatus: boolean) {
    this.isAdminMessagesSub.next(cstatus);
  }

  public getAdminMessageStatus() {
    return this.isAdminMessagesSub.getValue();
  }

  public setTypeOfChat(ty) {
    this.typeOfChatSub.next(ty);
  }

  public getTypeOfChat() {
    return this.typeOfChatSub.getValue();
  }

  public setCurrentMatch(md) {
    this.currentMatchSub.next(md);
  }

  public getCurrentMatch() {
    return this.currentMatchSub.getValue();
  }

  public getChatStatus() {
    return this.chatStatusSub.getValue();
  }

  public toggleChat() {
    if (this.chatStatusSub.getValue() == true) {
      this.chatStatusSub.next(false);
    } else {
      this.chatStatusSub.next(true);
    }
  }

  public setWindowPos(winpos) {
    this.chatWindowPosSub.next(winpos);
  }

  public getWindowPos() {
    return this.chatWindowPosSub.getValue();
  }

  public connectMatchEvents(uniqueId) {
    if (isPlatformBrowser(this.platformId)) {
      this.socket?.emit('connect-match-events', uniqueId);
    }
  }

  public disconnectMatchEvents(uniqueId) {
    if (isPlatformBrowser(this.platformId)) {
      this.socket?.emit('disconnect-match-events', uniqueId);
    }
  }

  public emitMatchEvents(uniqueId, userId, eventType = 'refresh') {
    if (isPlatformBrowser(this.platformId)) {
      const payload = {
        uniqueId,
        eventType,
        userId,
      };
      this.socket?.emit('send-match-events', payload);
    }
  }

  public getMatchEvents = (matchId) => {
    return Observable.create((observer) => {
      if (isPlatformBrowser(this.platformId)) {
        this.socket?.on('receive-match-events', (message) => {
          if (matchId == message?.uniqueId) {
            observer.next(message);
          }
        });
      }
    });
  };

  public findOpponent(payload) {
    if (isPlatformBrowser(this.platformId)) {
      this.socket?.emit('play-now', payload);
    }
  }

  public removeFromQueue(payload) {
    if (isPlatformBrowser(this.platformId)) {
      this.socket?.emit('remove-from-queue', payload);
    }
  }

  public listenChallengeFound = () => {
    return Observable.create((observer) => {
      if (isPlatformBrowser(this.platformId)) {
        this.socket?.on('challenge-found', (payload) => {
          observer.next(payload);
        });
      }
    });
  };

  public listenChallengeError = () => {
    return Observable.create((observer) => {
      if (isPlatformBrowser(this.platformId)) {
        this.socket?.on('play-now-error', (payload) => {
          observer.next(payload);
        });
      }
    });
  };

  public listenUpdateScore = (next) => {
    if (isPlatformBrowser(this.platformId)) {
      this.socket?.on('update-score', (payload) => {
        next(payload);
      });
    }
  };

  public connectSocket = () => {
    if (isPlatformBrowser(this.platformId)) {
      const ctoken = localStorage.getItem('refreshToken');
      if (!this.socket) {
        this.socket = io(this.url, {
          query: {
            token: ctoken,
          },
          transports: ['websocket'],
          upgrade: false,
        });
      }
    }
  };

  public connectTournamentEvents(uniqueId) {
    if (isPlatformBrowser(this.platformId)) {
      this.socket?.emit('connect-tournament-events', uniqueId);
    }
  }

  public disconnectTournamentEvents(uniqueId) {
    if (isPlatformBrowser(this.platformId)) {
      this.socket?.emit('disconnect-tournament-events', uniqueId);
    }
  }

  public emitTournamentEvents(uniqueId, userId, eventType) {
    if (isPlatformBrowser(this.platformId)) {
      const payload = {
        uniqueId,
        eventType,
        userId,
      };
      this.socket?.emit('send-tournament-events', payload);
    }
  }

  public getTournamentEvents = (slug) => {
    return Observable.create((observer) => {
      if (isPlatformBrowser(this.platformId)) {
        this.socket?.on('receive-tournament-events', (message) => {
          if (slug == message?.uniqueId) {
            observer.next(message);
          }
        });
      }
    });
  };

  public findSeasonMatch({ seasonId }) {
    if (isPlatformBrowser(this.platformId)) {
      this.socket?.emit('find-season-match', seasonId);
    }
  }

  public listenSeasonMatchFound = (listener: Subscriber<any>) => {
    if (isPlatformBrowser(this.platformId)) {
      this.socket?.on('season-match-found', (payload) => {
        listener.next(payload);
        listener.complete();
      });
    }
  };

  public listenFindSeasonMatchError = (listener: Subscriber<any>) => {
    if (isPlatformBrowser(this.platformId)) {
      this.socket?.on('find-season-match-error', (payload) => {
        listener.next(payload);
        listener.complete();
      });
    }
  };

  public listenShowUpdateScore = (next) => {
    if (isPlatformBrowser(this.platformId)) {
      this.socket?.on('allow-score-update', (payload) => {
        next(payload);
      });
    }
  };

  //Socket for social wall

  public emitCommentPost = (payload: IO_COMMENT) => {
    if (isPlatformBrowser(this.platformId)) {
      this.socket?.emit('comment-post', payload);
    }
  };

  public listenCommentPost = () => {
    return new Observable((observer) => {
      if (isPlatformBrowser(this.platformId)) {
        this.socket?.on('comment-post', (payload) => {
          observer.next(payload);
        });
      }
    });
  };

  public emitReplyComment = (payload: IO_REPLY) => {
    if (isPlatformBrowser(this.platformId)) {
      this.socket?.emit('reply-comment', payload);
    }
  };

  public listenReplyPost = () => {
    return new Observable((observer) => {
      if (isPlatformBrowser(this.platformId)) {
        this.socket?.on('reply-post-notification', (payload) => {
          observer.next(payload);
        });
      }
    });
  };

  public listenReplyComment = () => {
    return new Observable((observer) => {
      if (isPlatformBrowser(this.platformId)) {
        this.socket?.on('reply-comment-notification', (payload) => {
          observer.next(payload);
        });
      }
    });
  };

  public emitLikePost = (payload: IO_LIKE) => {
    if (isPlatformBrowser(this.platformId)) {
      this.socket?.emit('like-post', payload);
    }
  };

  public listenLikePost = () => {
    return new Observable((observer) => {
      if (isPlatformBrowser(this.platformId)) {
        this.socket?.on('like-post', (payload) => {
          observer.next(payload);
        });
      }
    });
  };

  public emitReportPost = (payload: IO_REPORT) => {
    if (isPlatformBrowser(this.platformId)) {
      this.socket?.emit('being-report-post', payload);
    }
  };

  public listenReportPost = () => {
    return new Observable((observer) => {
      if (isPlatformBrowser(this.platformId)) {
        this.socket?.on('being-report-post', (payload) => {
          observer.next(payload);
        });
      }
    });
  };
}
