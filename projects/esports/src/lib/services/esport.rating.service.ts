import { HttpClient } from "@angular/common/http";
import { Inject, Injectable } from "@angular/core";

import { SubscribableOrPromise } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class EsportsRatingService {
  constructor(private http: HttpClient, @Inject('env') private environment) {}

  addRating(payload): SubscribableOrPromise<any> {
    return this.http
      .post(`${this.environment.apiEndPoint}rating`, payload)
      .toPromise();
  }

  getRating(query): SubscribableOrPromise<any> {
    return this.http
      .get(`${this.environment.apiEndPoint}rating${query}`)
      .toPromise();
  }

  updateRating(id, payload): SubscribableOrPromise<any> {
    return this.http
      .patch(`${this.environment.apiEndPoint}rating/${id}`, payload)
      .toPromise();
  }
}
