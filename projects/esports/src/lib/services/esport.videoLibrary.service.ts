import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { SubscribableOrPromise } from 'rxjs';
import { IVideoLibrary } from '../models/video-library';

@Injectable({
  providedIn: 'root',
})
export class EsportsVideoLibraryService {
  constructor(private http: HttpClient) { }

  createVideoLibrary(API, payload: IVideoLibrary): SubscribableOrPromise<any> {
    return this.http
      .post(`${API}video-library`, payload)
      .toPromise();
  }

  fetchVideoLibrary(API, query): SubscribableOrPromise<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json; charset=utf-8');
    return this.http
      .get(`${API}video-library${query}`, { headers: httpHeaders })
      .toPromise();
  }

  // Video Filter API
  fetchVideoLibraryFilter(api, params): SubscribableOrPromise<any> {
    return this.http.get(`${api}video-library/all`, { params: params }).toPromise();
  }

  fetchVideoLibraryById(API, id): SubscribableOrPromise<any> {
    return this.http
      .get(`${API}video-library/${id}`)
      .toPromise();
  }

  fetchVideoLibraryBySlug(API, slug): SubscribableOrPromise<any> {
    return this.http
      .get(`${API}video-library/slug/${slug}`)
      .toPromise();
  }

  updateVideoLibrary(API, payload: IVideoLibrary): SubscribableOrPromise<any> {
    return this.http
      .put(`${API}video-library/${payload.id}`, payload)
      .toPromise();
  }

  deleteVideoLibrary(API, id): SubscribableOrPromise<any> {
    return this.http
      .delete(`${API}video-library/${id}`)
      .toPromise();
  }

  getVideoThumbnail(videoId) {
    this.http
      .get(`https://img.youtube.com/vi/${videoId}/hqdefault.jpg`)
      .toPromise();
  }
}
