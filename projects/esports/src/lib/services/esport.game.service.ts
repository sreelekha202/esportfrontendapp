import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class EsportsGameService {
  constructor(private http: HttpClient) {}

  getGames(api): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    return this.http.get(api + `game`, {
      headers: httpHeaders,
    });
  }

  public createMatchMakingSubject = new BehaviorSubject({});
  public createMatchMaking = this.createMatchMakingSubject.asObservable();

  getAllGenre(api): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    return this.http.get(api + `userpreference/get_all_genre`, {
      headers: httpHeaders,
    });
  }

  getAllGames(api, encodedUrl: string): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    return this.http.get(api + `game${encodedUrl}`, {
      headers: httpHeaders,
    });
  }

  fetchRecentSeasonMatches(api, params): Observable<any> {
    const encodedUrl = encodeURIComponent(params.query);
    let url = `${api}match/season-matches?query=${encodedUrl}`;
    return this.http.get(url);
  }

  getAdminAllGames(api): Observable<any> {
    const httpHeaders = new HttpHeaders();

    httpHeaders.set('Content-Type', 'application/json');
    return this.http.get(api + `game`, {
      headers: httpHeaders,
    });
  }

  saveGame(api, id, formData): Observable<any> {
    return this.http.post(api + `game/upload/` + id, formData);
  }

  disableGame(api, gameId): Observable<any> {
    return this.http.delete(api + `game/${gameId}`);
  }

  fetchPlatforms(api): Observable<any> {
    return this.http.get(api + `game/platform`);
  }

  fetchSeasonDetails(api, seasonId): Observable<any> {
    return this.http.get(api + `seasons/${seasonId}`);
  }

  readyParticipant(api, data): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    return this.http.patch(api + 'match/season/participantReady', data, {
      headers: httpHeaders,
    });
  }

  checkSeasonJoinStatus(api, seasonId: string): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    return this.http.get(
      api + `participant/season-join-status?seasonId=` + seasonId,
      {
        headers: httpHeaders,
      }
    );
  }

  getGamesWithParams(api, param: any = null): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    return this.http.get(api + `game`, {
      headers: httpHeaders,
      params: param,
    });
  }

  checkPlatform(api, id, data: any): Observable<any> {
    return this.http.get(api + `userpreference/matchmaking/` + id, {
      params: data,
    });
  }
}
