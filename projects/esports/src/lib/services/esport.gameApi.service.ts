import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class EsportsGameApiService {

  public gameApiurl: string;

  constructor(private http: HttpClient,
    @Inject('env') private env

  ) {
    this.gameApiurl = this.env.gameAPIEndPoint
  }

  getJoinDotaLink(steamId): Observable<any> {

    return this.http.get(`${this.gameApiurl}home/createsteamlobby?steamId=${steamId}`);
  }

  getUsergetMedal(params): Observable<any> {

    return this.http.get(`${this.gameApiurl}home/getPlayerStats?gamerId=${params['gamerId']}&gameType=${params['gameType']}`);
  }

  getCSGOPlayerStats(params): Observable<any> {
    return this.http.get(`${this.gameApiurl}home/get-csgo-player-stats-by-id/${params['gamerId']}`);
  }


  getUsersMatchHistory(params): Observable<any> {

    return this.http.get(`${this.gameApiurl}home/getUsersMatchHistory?gamerId=${params['gamerId']}&gameType=${params['gameType']}`);
  }


  getUserWinsLose(params): Observable<any> {

    return this.http.get(`${this.gameApiurl}home/playerWinRate?gamerId=${params['gamerId']}&gameType=${params['gameType']}`);
  }

  getPlayerHistogram(params): Observable<any> {

    return this.http.get(`${this.gameApiurl}home/playerHistogram?gamerId=${params['gamerId']}&gameType=${params['gameType']}`);
  }

  getMMR(params): Observable<any> {

    return this.http.get(`${this.gameApiurl}home/getUserProfile?gamerId=${params['gamerId']}&gameType=${params['gameType']}`);
  }



  getStcStats(params): Observable<any> {

    return this.http.get(`${this.gameApiurl}home/getMatchStatsPlatform?gamerId=${params['gamerId']}&gameType=${params['gameType']}`);
  }

}
