import { Inject, Injectable } from '@angular/core';
import { GlobalUtils } from './global-utils';
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
} from '@angular/common/http';
import { BehaviorSubject, Observable, SubscribableOrPromise } from 'rxjs';
import { IUser } from '../models/user';
import { Router } from '@angular/router';
import { catchError, map } from 'rxjs/operators';
import { throwError } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class EsportsUserService {
  constructor(
    private router: Router,
    private http: HttpClient,
    @Inject('env') private environment
  ) {
    this.currentUserSubject = new BehaviorSubject(null);
    this.currentUser = this.currentUserSubject.asObservable();
    this.searchedUserSubject = new BehaviorSubject([]);
    this.searchedUser = this.searchedUserSubject.asObservable();
  }
  public currentUserSubject: BehaviorSubject<IUser>;
  public currentUser: Observable<IUser>;
  public searchedUser: Observable<[]>;
  public searchedUserSubject: BehaviorSubject<[]>;

  private refreshTokenTimeout;

  refreshToken(api, tokenName) {
    if (GlobalUtils.isBrowser()) {
      const data = {
        refreshToken: localStorage.getItem('refreshToken'),
      };
      return this.http.post<any>(api + 'auth/web_refresh_token', data, {}).pipe(
        map((user) => {
          if (GlobalUtils.isBrowser()) {
            localStorage.setItem(tokenName, user.data);
          }

          this.startRefreshTokenTimer(api, tokenName);
          return user;
        })
      );
    }
  }

  /**
   * Get Profile details
   * @param token: profile data based on token Id
   */
  getProfile(api, tokenName): Observable<any> {
    let currToken;
    if (GlobalUtils.isBrowser()) {
      currToken = localStorage.getItem(tokenName);
    }
    return this.http.get(api + 'user/profile').pipe(
      map((value) => {
        return value;
      }),
      catchError(this.handleError)
    );
  }

  getUserProfileById(api, userId: string): Observable<any> {
    const url = `${api}user/profile/${userId}`;
    return this.http.get(url);
  }

  // checkAdmin(api) {
  //   return this.http.get(api + 'admin').pipe(
  //     map(
  //       (res: any) => {
  //         return res;
  //       },
  //       (error: any) => {
  //         return false;
  //       }
  //     ),
  //     catchError(this.handleError)
  //   );
  // }

  // Return the current User details
  public getAuthenticatedUser(api, tokenName): IUser {
    let currToken;
    if (GlobalUtils.isBrowser()) {
      currToken = localStorage.getItem(tokenName);
    }

    if (this.currentUserSubject.value && currToken) {
      // Check if current user details and token are available
      return this.currentUserSubject.value;
    } else if (currToken && !this.currentUserSubject.value) {
      // if token is available but user details is not available in observables
      this.getProfile(api, tokenName).subscribe(
        (res) => {
          this.currentUserSubject.next(res.data);
          return this.currentUserSubject.value;
        },
        (error) => {
          this.logout(api, tokenName);
          this.router.navigate(['/home']);
          //this.toastService.showError(error);
        }
      );
      return this.currentUserSubject.value;
    } else {
      // return null if token and user details not available
      return null;
    }
  }

  updateProfile(api, data): Observable<any> {
    return this.http.post(api + 'user/user_update', data).pipe(
      map((value) => {
        return value;
      }),
      catchError(this.handleError)
    );
  }

  updatePaymentInfo(api, data): Observable<any> {
    return this.http.post(api + 'user/add_payment_info', data);
  }

  refreshCurrentUser(api, tokenName) {
    this.getProfile(api, tokenName).subscribe((res) => {
      this.currentUserSubject.next(res.data);
      return this.currentUserSubject.value;
    });
  }

  // Error
  handleTokenError(error: HttpErrorResponse, api, tokenName) {
    const that = this;
    that.logout(api, tokenName);
    let msg = '';
    if (error?.error instanceof ErrorEvent) {
      // client-side error
      msg = error?.error?.message;
    } else {
      // server-side error
      msg = `Error Code: ${error.status || 0}\nMessage: ${error.message}`;
    }
    return throwError(msg);
  }
  handleError(error: HttpErrorResponse, tokenName) {
    if (GlobalUtils.isBrowser() && error?.error?.message == 'Unauthorized') {
      localStorage.removeItem(tokenName);
      window.location.reload();
    }
    let msg = '';
    if (error?.error instanceof ErrorEvent) {
      // client-side error
      msg = error?.error?.message;
    } else {
      // server-side error
      msg = error?.error?.message;
    }
    return throwError(msg);
  }

  /**
   * Use to set/update the current user profile
   * @param value : update user requests
   */
  public setCurrentUser(value: IUser) {
    this.currentUserSubject.next(value);
  }

  logout(api, tokenName): any {
    if (GlobalUtils.isBrowser()) {
      // remove user from local storage to log user out
      const data = {
        refreshToken: localStorage.getItem('refreshToken'),
      };
      this.http.post<any>(api + 'user/logout', data, {}).subscribe(
        () => {
          this.stopRefreshTokenTimer();
          //this.authService.redirectUrl = '/';
          this.currentUserSubject.next(null);

          localStorage.removeItem(tokenName);
          localStorage.removeItem('refreshToken');

          window.location.reload();
        },
        (error) => {
          this.stopRefreshTokenTimer();
          //this.authService.redirectUrl = '/';
          this.currentUserSubject.next(null);

          localStorage.removeItem(tokenName);
          localStorage.removeItem('refreshToken');

          window.location.reload();
        }
      );
    }
  }

  searchUsers(searchKeyword, specificProjection = undefined) {
    const projection =
      specificProjection && specificProjection.state == true
        ? specificProjection.read
        : 'fullName,username,phoneNumber,email,profilePicture,country,state';
    return this.http
      .get(
        `${this.environment?.apiEndPoint}user/search_users?text=${searchKeyword}&fields=${projection}`
      )
      .pipe(
        map((value: any) => {
          this.searchedUserSubject.next(value?.data || []);
          return value.data;
        }),
        catchError((err) => {
          this.searchedUserSubject.next([]);
          return err;
        })
      );
  }

  searchUsersAdminPanel(searchKeyword, specificProjection = undefined) {
    const query = JSON.stringify({
      text: searchKeyword,
    });
    const projection =
      specificProjection && specificProjection.state == true
        ? specificProjection.read
        : 'fullName,username,phoneNumber,email,profilePicture,country,state';
    const encodedUrl = encodeURIComponent(query);
    return this.http
      .get(
        `${this.environment?.apiEndPoint}user/search_user_admin_panel?query=${encodedUrl}&select=${projection}`
      )
      .pipe(
        map((value: any) => {
          return value.data;
        }),
        catchError((err) => err)
      );
  }

  searchUserWithPagination(api, searchKeyword, paginationQuery) {
    const page = paginationQuery.page ? paginationQuery.page : 1;
    const limit = paginationQuery.limit ? paginationQuery.limit : 100;
    const projection = 'fullName,username,phoneNumber,email,profilePicture';
    const uri = `${api}user/search_users?text=${searchKeyword}&fields=${projection}&page=${page}&limit=${limit}`;
    return this.http.get(uri);
  }

  add_social(api, authtoken, sprovider) {
    const data = {
      provider: sprovider,
      token: authtoken,
    };
    return this.http.post<any>(api + 'user/add_social', data);
  }

  addEmail(api, data): Observable<any> {
    return this.http.post(api + 'user/user_update', data);
  }

  createAdminUser(api, formData): Observable<any> {
    return this.http.post(api + 'user/create_admin_user', formData);
  }

  verifyEmail(api, data): Observable<any> {
    return this.http.post(api + 'user/user_verify', data);
  }

  promoNotify(api): Observable<any> {
    return this.http.get(api + `user/promo_notify`);
  }

  //#region - Get the user details for Admin Panel
  getRecentlyUpdatedUserList(api): Observable<any> {
    return this.http.get(api + `user/users`);
  }

  removeUser(api, id): Observable<any> {
    return this.http.delete(api + `user/remove/${id}`);
  }

  getUserDetails(api, id): Observable<any> {
    return this.http.get(api + `user/users/${id}`);
  }

  getAdminUsers(api, query): Observable<any> {
    return this.http.get(api + `user/users${query}`);
  }

  getTopSpendingUserList(api): Observable<any> {
    return this.http.get(api + `user/users-account`);
  }
  //#endregion

  getAllCountries(): Observable<any> {
    return this.http.get('./assets/json/countries.json');
  }

  getStates(): Observable<any> {
    return this.http.get<any>('./assets/json/states.json');
  }

  getRewardTransaction(api, params): SubscribableOrPromise<any> {
    const encodedPagination = encodeURIComponent(params['pagination']);
    const url = `${api}user/reward_transaction?pagination=${encodedPagination}`;
    return this.http.get(url).toPromise();
  }
  getUserinviteesList(api, params): Observable<any> {
    const encodedPagination = encodeURIComponent(params['pagination']);
    return this.http.get(
      api + `user/get_reffered_user?query=${encodedPagination}`
    );
  }
  getTeamList(api, query): Observable<any> {
    return this.http.get(api + `user/teamsDetail${query}`);
  }
  teamsDetail_Admin(api, params): Observable<any> {
    const encodedPagination = encodeURIComponent(params['pagination']);
    return this.http.get(
      api + `user/teamsDetail_Admin?query=${encodedPagination}`
    );
  }
  getTeamLogs(api, id): Observable<any> {
    return this.http.get(api + `user/get_team_activity_log/${id}`);
  }
  team_update(api, team): Observable<any> {
    return this.http.patch(api + 'user/team_update', team);
  }
  update_team_admin(api, team): Observable<any> {
    return this.http.patch(api + 'user/update_team_admin', team);
  }
  update_team_owner(api, team): Observable<any> {
    return this.http.patch(api + 'user/update_team_owner', team);
  }
  updateUser(api, formValues, id): Observable<any> {
    const data = formValues;
    return this.http.patch(api + `user/admin_user_update/${id}`, data);
  }
  getRewardTransactionByUserId(api, params): Observable<any> {
    let encodedUrl = encodeURIComponent(params['query']);
    return this.http.get(api + `user/admin_reward_tran?query=${encodedUrl}`);
  }

  public startRefreshTokenTimer(api, tokeName) {
    let jwtToken;
    if (GlobalUtils.isBrowser()) {
      // parse json object from base64 encoded jwt token
      jwtToken = JSON.parse(atob(localStorage.getItem(tokeName).split('.')[1]));

      // set a timeout to refresh the token a minute before it expires
      const expires = new Date(jwtToken.exp * 1000);
      const timeout = expires.getTime() - Date.now() - 60 * 1000;
      this.refreshTokenTimeout = setTimeout(
        () =>
          this.refreshToken(api, tokeName).subscribe(
            (data) => {},
            (error) => {
              this.logout(api, tokeName);
            }
          ),
        timeout
      );
    }
  }

  private stopRefreshTokenTimer() {
    clearTimeout(this.refreshTokenTimeout);
  }

  getUserPrefernces(api, id): Observable<any> {
    return this.http.get(api + `user/admin_user_preference/${id}`);
  }

  getUserFollower(params): Observable<any> {
    let api = params['API'];
    const encodedPagination = encodeURIComponent(params['pagination']);
    return this.http.get(
      api +
        `userpreference/getFollowUsers/userfollow/getFollowUsers?pagination=${encodedPagination}`
    );
  }
  getFollowerList(params): Observable<any> {
    let api = params['API'];
    const encodedPagination = encodeURIComponent(params['pagination']);
    return this.http.get(
      api +
        `userpreference/getFollowingUsers/userfollow/getFollowingUsers?pagination=${encodedPagination}`
    );
  }
  removeFollowingUser(api, queryParam): Observable<any> {
    const url = `${api}userpreference/removeFollowingUser/userfollow/removefollowingUser/${queryParam}`;
    return this.http.post(url, null);
  }
  removeFollowerUser(api, queryParam): Observable<any> {
    const url = `${api}userpreference/removeFollower/userfollow/removeFollower/${queryParam}`;
    return this.http.post(url, null);
  }
  changePassword(api, data): Observable<any> {
    return this.http.post(api + 'user/change_password', data);
  }

  create_team(api, data): Observable<any> {
    return this.http.post<any>(api + 'user/create_team', data);
  }

  getMyTeam(api, params): SubscribableOrPromise<any> {
    const encodedPagination = encodeURIComponent(params['pagination']);
    const url = `${api}user/my_teams?pagination=${encodedPagination}`;
    return this.http.get(url).toPromise();
  }

  getJoinMyTeam(api, id): SubscribableOrPromise<any> {
    const url = `${api}user/join_my_teams/${id}`;
    return this.http.get(url).toPromise();
  }

  getMyInvite(api, params): SubscribableOrPromise<any> {
    const encodedPagination = encodeURIComponent(params['pagination']);
    const url = `${api}user/my_invites?pagination=${encodedPagination}`;
    return this.http.get(url).toPromise();
  }

  public update_invitees(api, data): Observable<any> {
    return this.http.post<any>(api + 'user/update_invites', data);
  }

  public getTeamById(api, id): SubscribableOrPromise<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json; charset=utf-8');
    return this.http
      .get(`${api}home/team/${id}`, {
        headers: httpHeaders,
      })
      .toPromise();
  }

  public getTeamMember(api, id): SubscribableOrPromise<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json; charset=utf-8');
    return this.http
      .get(`${api}user/team_member/${id}`, {
        headers: httpHeaders,
      })
      .toPromise();
  }

  public update_member(api, data): Observable<any> {
    return this.http.post<any>(api + 'user/teammember_update', data);
  }
  public getUserAccountDetails(api, userId): Observable<any> {
    return this.http.get(api + `user/accountDetails?userId=${userId}`);
  }
  searchForFollowingUser(params): Observable<any> {
    let api = params['API'];
    const encodedPagination = encodeURIComponent(params['pagination']);
    const encodedText = encodeURIComponent(params['value']);
    return this.http.get(
      api +
        `userpreference/searchFollowingUsers/userfollow/searchFollowingUsers?text=${encodedText}&pagination=${encodedPagination}`
    );
  }
  searchForFollowerUser(params): Observable<any> {
    let api = params['API'];
    const encodedPagination = encodeURIComponent(params['pagination']);
    const encodedText = encodeURIComponent(params['value']);
    return this.http.get(
      api +
        `userpreference/searchFollowerUsers/userfollow/searchFollowerUsers?text=${encodedText}&pagination=${encodedPagination}`
    );
  }
  getTournamentByTeamId(api, teamId): Observable<any> {
    return this.http.get(api + `user/tournament/${teamId}`);
  }

  fetchAdminDefaultAccess() {
    let returnObject;
    if (
      this.currentUserSubject.value &&
      this.currentUserSubject.value.accessLevel.length > 0
    ) {
      switch (this.currentUserSubject.value.accessLevel[0]) {
        case 'sc': {
          returnObject = '/admin/site-configuration';
          break;
        }
        case 'acm': {
          returnObject = '/admin/access-management';
          break;
        }
        case 'em': {
          returnObject = '/admin/esports-management';
          break;
        }
        case 'cm': {
          returnObject = '/admin/content-management';
          break;
        }
        case 'un': {
          returnObject = '/admin/user-notifications';
          break;
        }
        case 'um': {
          returnObject = '/admin/user-management';
          break;
        }
        case 'sm': {
          returnObject = '/admin/spam-management';
          break;
        }
        case 'tm': {
          returnObject = '/admin/team-management';
          break;
        }
        default: {
          returnObject = undefined;
          break;
        }
      }
    }
    return returnObject;
  }

  getMyTeams(params): Observable<any> {
    return this.http.get(`${this.environment.apiEndPoint}user/my_teams`, {
      params: params,
    });
  }

  deleteTeam(teamId: number): Observable<void> {
    return this.http.delete<void>(`url/${teamId}`);
  }

  getRecentlyUpdatedUsers(params): Observable<any> {
    const encodedUrl = encodeURIComponent(params['query']);
    const encodedPagination = encodeURIComponent(params['pagination']);
    return this.http.get(
      this.environment.apiEndPoint +
        `user/usersPaginated?query=${encodedUrl}&pagination=${encodedPagination}`
    );
  }
  // cities static  api for
  getCity(): Observable<any> {
    return this.http.get<any>('./assets/json/cities.json');
  }
}
