import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class EsportsLanguageService {
  public languageSub: BehaviorSubject<string>;
  public language: Observable<string>;
  constructor() {
    this.languageSub = new BehaviorSubject('en');
    this.language = this.languageSub.asObservable();
  }

  setLanguage(lang) {
    this.languageSub.next(lang || 'en');
  }
}
