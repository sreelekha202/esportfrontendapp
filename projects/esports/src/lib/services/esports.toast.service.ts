import { Injectable, TemplateRef } from '@angular/core';
import { EsportsSnackBarComponent, EsportsSnackBarDataType, MessageType } from '../modules/esports-snack-bar/esports-snack-bar.component';
import { MatSnackBar } from '@angular/material/snack-bar';
import { GlobalUtils } from './global-utils';

@Injectable({ providedIn: 'root' })
export class EsportsToastService {
  toasts: any[] = [];
  constructor(public matSnackBar: MatSnackBar) {}

  showSuccess(message: string, enableDefaultTimeout: boolean = true, defaultTimeout: number = 3000) {
    const data: EsportsSnackBarDataType = {
      text: message,
      type: MessageType.success,
      enableDefaultTimeout,
      defaultTimeout
    };
    this.matSnackBar.openFromComponent(EsportsSnackBarComponent, {
      data,
      panelClass: 'global-Snack',
      horizontalPosition: GlobalUtils.isLTR() ? 'end' : 'start',
      verticalPosition: 'top',
    });
  }

  showError(message: string, enableDefaultTimeout: boolean = true, defaultTimeout: number = 3000) {
    const data: EsportsSnackBarDataType = {
      text: message,
      type: MessageType.error,
      enableDefaultTimeout,
      defaultTimeout
    };
    this.matSnackBar.openFromComponent(EsportsSnackBarComponent, {
      data,
      panelClass: 'global-Snack',
      horizontalPosition: GlobalUtils.isLTR() ? 'end' : 'start',
      verticalPosition: 'top',
    });
  }

  showInfo(message: string, enableDefaultTimeout: boolean = true, defaultTimeout: number = 3000) {
    const data: EsportsSnackBarDataType = {
      text: message,
      type: MessageType.info,
      enableDefaultTimeout,
      defaultTimeout
    };
    this.matSnackBar.openFromComponent(EsportsSnackBarComponent, {
      data,
      panelClass: 'global-Snack',
      horizontalPosition: GlobalUtils.isLTR() ? 'end' : 'start',
      verticalPosition: 'top',
    });
  }

  showPromo(message: string, enableDefaultTimeout: boolean = true, defaultTimeout: number = 3000) {
    const data: EsportsSnackBarDataType = {
      text: message,
      type: MessageType.promo,
      enableDefaultTimeout,
      defaultTimeout
    };
    this.matSnackBar.openFromComponent(EsportsSnackBarComponent, {
      data,
      panelClass: 'global-Snack',
      horizontalPosition: GlobalUtils.isLTR() ? 'end' : 'start',
      verticalPosition: 'top',
    });
  }

  show(textOrTpl: string | TemplateRef<any>, options: any = {}) {
    this.toasts.push({ textOrTpl, ...options });
  }

  remove(toast: any) {
    this.toasts = this.toasts.filter((t) => t !== toast);
  }
}
