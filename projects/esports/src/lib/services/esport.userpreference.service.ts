import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Inject, Injectable } from "@angular/core";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class EsportsUserPreferenceService {

  constructor(private httpClient: HttpClient, @Inject('env') private environment) {}

  public getPreferences(query, projection = ""): Observable<any> {
    let filter = encodeURIComponent(query);
    return this.httpClient.get(
      `${this.environment?.apiEndPoint}userpreference?query=${filter}&select=${projection}`
    );
  }

  removeBookmark(queryParam): Observable<any> {
    return this.httpClient.delete(
      `${this.environment?.apiEndPoint}userpreference/remove_bookmark?${queryParam}`
    );
  }

  addBookmark(data): Observable<any> {
    return this.httpClient.patch(
      `${this.environment?.apiEndPoint}userpreference/add_bookmark`,
      data
    );
  }

  getAllSettingPageData(): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set("Content-Type", "application/json");

    return this.httpClient.get(
        this.environment.apiEndPoint + `userpreference/get_all_setting_preference`,
      {
        headers: httpHeaders,
      }
    );
  }

  getLoginPrefrence(): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set("Content-Type", "application/json");

    return this.httpClient.get(
        this.environment.apiEndPoint + `userpreference/login_user`
    );
  }

  addPrefrence(data): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set("Content-Type", "application/json");

    return this.httpClient.post(
        this.environment.apiEndPoint + `userpreference/add_preference`,
      data
    );
  }

   matchMaking(gameId, platformId): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    let url = `${this.environment?.apiEndPoint}userpreference/matchmaking/${gameId}?`;
    if (platformId != null && platformId != undefined && platformId != '') {
      url += `platformId=${platformId}`;
    }
    return this.httpClient.get(url);
  }
}
