import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SubscribableOrPromise } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class EsportsOptionService {
  constructor(private http: HttpClient) { }

  fetchAllGenres(API): SubscribableOrPromise<any> {
    return this.http
      .get(`${API}option/all-genres`)
      .toPromise();
  }

  fetchAllCategories(API): SubscribableOrPromise<any> {
    return this.http
      .get(`${API}option/all-categories`)
      .toPromise();
  }

  fetchAllTags(API): SubscribableOrPromise<any> {
    return this.http
      .get(`${API}option/all-tags`)
      .toPromise();
  }
}
