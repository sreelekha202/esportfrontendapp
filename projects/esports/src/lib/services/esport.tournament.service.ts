import { Inject, Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable, SubscribableOrPromise } from 'rxjs';
import { ITournament } from '../models/tournament';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class EsportsTournamentService {
  public isTournamentPaymentProcessing: boolean = false;
  public tournamentActiveTab = 1;
  gameList = [];
  platformList = [];
  bracketTypes = [];

  isAdvance = new BehaviorSubject<boolean>(false);
  activePage = new BehaviorSubject<number>(0);
  totalPages = new BehaviorSubject<number>(0);



  public manageTournamentSubject: BehaviorSubject<ITournament>;
  public manageTournament: Observable<ITournament>;

  public createTournamentSubject = new BehaviorSubject(null);
  public createTournament = this.createTournamentSubject.asObservable()

  public joinTournamentSubject = new BehaviorSubject(null);
  public joinTournament = this.createTournamentSubject.asObservable()

  constructor(
    @Inject('env') private environment,
    @Inject('color') private color,
    private http: HttpClient,
    private router: Router
  ) {
    this.joinDetailSubject = new BehaviorSubject(null);
    this.joinDetail = this.joinDetailSubject.asObservable();
    this.manageTournamentSubject = new BehaviorSubject(null);
    this.manageTournament = this.manageTournamentSubject.asObservable();
  }

  public joinDetailSubject: BehaviorSubject<any>;
  public joinDetail: Observable<any>;

  public setSelectedTeam(value: any) {
    this.joinDetailSubject.next(value);
  }

  saveTournament(payload): SubscribableOrPromise<any> {
    return this.http
      .post(`${this.environment?.apiEndPoint}tournament`, payload)
      .toPromise();
  }

  saveTournament1(datas): Observable<any> {
    const data = datas;
    const httpHeaders = new HttpHeaders();
    httpHeaders.set("Content-Type", "application/json");

    return this.http.post(this.environment.apiEndPoint + "tournament", data, {
      headers: httpHeaders,
    });
  }

  updateTournament(payload, id): SubscribableOrPromise<any> {
    return this.http
      .put(`${this.environment?.apiEndPoint}tournament/${id}`, payload)
      .toPromise();
  }


  updateTournament2(formValues, id): Observable<any> {
    const data = formValues;
    const httpHeaders = new HttpHeaders();
    httpHeaders.set("Content-Type", "application/json");

    return this.http.put(this.environment.apiEndPoint + `tournament/${id}`, data, {
      headers: httpHeaders,
    });
  }

  getTournamentBySlug(slug): SubscribableOrPromise<any> {
    return this.http
      .get(`${this.environment?.apiEndPoint}tournament/slug/${slug}`)
      .toPromise();
  }

  updateGWBTournament(api, formValues, id): Observable<any> {
    const data = formValues;

    return this.http.put(api + `tournament/${id}`, data);
  }

  getAllGames(param): Observable<any> {
    return this.http.get(this.environment.apiEndPoint + `game`,{params:param});
  }

  abortTournament(api, formValues): Observable<any> {
    const data = formValues;
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    return this.http.patch(api + `tournament/abort`, data, {
      headers: httpHeaders,
    });
  }

  getTournaments(api, params, selectParams = ``, option = ''): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json; charset=utf-8');
    const encodedUrl = encodeURIComponent(params.query);
    let url = `${api}tournament?query=${encodedUrl}${selectParams}`;
    if (option) {
      const opt = encodeURIComponent(option);
      url += `&option=${opt}`;
    }
    return this.http.get(url, { headers: httpHeaders });
  }

  getLaderStanding(api, params): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json; charset=utf-8');
    let url = `${api}match/lader-standing?tournamentId=${params.id}&page=${params.page}`;
    return this.http.get(url, { headers: httpHeaders });
  }

  getTournament(api, id): Observable<any> {
    return this.http.get(api + `tournament/${id}`);
  }

  saveParticipant(api, data): Observable<any> {
    return this.http.post(api + 'participant', data);
  }

  getParticipants(api, encodedUrl): Observable<any> {
    return this.http.get(api + `participant/v2?type=1&tournamentId=`+encodedUrl+`&page=1&limit=50`);
  }

  getParticipants1(encodedUrl): Observable<any> {
     return this.http.get(this.environment.apiEndPoint + `participant${encodedUrl}`);
  }

  searchParticipant(api, field, text, tournamentId, pId?): Observable<any> {
    const urlString = pId
      ? `participant/search?field=${field}&text=${encodeURIComponent(
          text
        )}&tournamentId=${tournamentId}&pId=${pId}`
      : `participant/search?field=${field}&text=${encodeURIComponent(
          text
        )}&tournamentId=${tournamentId}`;
    return this.http.get(api + urlString);
  }

  updateParticipant(api, id, data): Observable<any> {
    return this.http.patch(api + `participant/${id}`, data);
  }

  updateParticipantBulk(api, data): Observable<any> {
    return this.http.put(api + `participant/updateAll`, data);
  }

  getParticipantTournament(api, params): Observable<any> {
    const encodedUrl = encodeURIComponent(params.query);
    const encodedPagination = encodeURIComponent(params.pagination);
    const url = `${api}participant/tournament?query=${encodedUrl}&pagination=${encodedPagination}`;
    return this.http.get(url);
  }

  getParticipantTournament1(params): Observable<any> {
    const encodedUrl = encodeURIComponent(params.query);
    const encodedPagination = encodeURIComponent(params.pagination);
    const url = `${this.environment.apiEndPoint}participant/tournament?status=${params.type}&pagination=${encodedPagination}`;
    return this.http.get(url);
  }

  getJoinedTournamentsByUser(api, params): Observable<any> {
    const encodedUrl = encodeURIComponent(params.query);
    const encodedPagination = encodeURIComponent(params.pagination);
    const url = `${api}admin/participant/tournament?query=${encodedUrl}&pagination=${encodedPagination}`;
    return this.http.get(url);
  }

  /**
   * Delete Tournament details By request Id
   * @param id: Request Id
   */
  deleteTournament(api, id): Observable<any> {
    return this.http.delete(api + `tournament/${id}`);
  }

  getStreamToken(api, streamId): SubscribableOrPromise<any> {
    return this.http
      .get(`${api}video-streaming?streamId=${streamId}`)
      .toPromise();
  }

  getStreamKey(api): SubscribableOrPromise<any> {
    return this.http
      .get(`${api}video-streaming/generate-stream-key`)
      .toPromise();
  }

  getPaginatedTournaments(api, params) {
    const { status, limit, sort, page, game, organizer, prefernce } = params;
    let url = `${api}home/tournament?`;
    for (const key in params) {
      if (Object.prototype.hasOwnProperty.call(params, key)) {
        let element = params[key];
        if(typeof element == 'object'){
          element = encodeURIComponent(JSON.stringify(element))
        }
        url+=`&${key}=${element}`;
      }
    }
    return this.http.get(url);
  }

  fetchParticipantRegistrationStatus(
    api,
    id,
    participantId = null
  ): SubscribableOrPromise<any> {
    let url = `${api}participant/join-status?tournamentId=${id}`;
    if (participantId) {
      url += `&guestId=${participantId}`;
    }
    return this.http.get(url).toPromise();
  }

  fetchParticipantRegistrationStatusMyTeams(
    api,
    id,
    participantId = null
  ): Observable<any> {
    let url = `${this.environment.apiEndPoint}participant/season-join-status?seasonId=${id}`;
    if (participantId) {
      url += `&guestId=${participantId}`;
    }
    return this.http.get(url);
  }

  fetchParticipantRegistrationStatusMyTeams2(
    id,
    participantId
  ): Observable<any> {
    return this.http.get(
      `${this.environment.apiEndPoint}participant/join-status-my-teams?tournamentId=${id}&participantId=${participantId}`
    );
  }

  fetchTournamentByStatus(api, payload): SubscribableOrPromise<any> {
    const { status, page, limit, text } = payload;
    const encodedText = encodeURIComponent(text);
    const url = `${api}tournament/get_all_tournament_by_status?status=${status}&page=${page}&limit=${limit}&text=${encodedText}`;
    return this.http.get(url).toPromise();
  }

  getPrizeMoneyRefundDisbursals(api, query): SubscribableOrPromise<any> {
    const encodedQuery = encodeURIComponent(query);
    const url = `${api}tournament/disbursals/prize_money_refund_disbursals?query=${encodedQuery}`;
    return this.http.get(url).toPromise();
  }



  getGames(): SubscribableOrPromise<any> {
    return this.http.get(`${this.environment.apiEndPoint}game`).toPromise();
  }

  set setGameList(list: any[]) {
    this.gameList = list;
  }

  get getGameList() {
    return this.gameList || [];
  }

  set setPlatformList(platforms: any[]) {
    this.platformList = platforms;
  }

  get getPlatformList() {
    return this.platformList || [];
  }

  set setBracketTypes(brackets: any) {
    this.bracketTypes = brackets;
  }

  get getBracketTypes() {
    return this.bracketTypes;
  }

  formatAMPM(d) {
    const date = new Date(d);
    let hh = date.getHours();
    let mm: any = date.getMinutes();
    const ampm = hh >= 12 ? "PM" : "AM";
    hh = hh % 12 ? hh : 12; // the hour '0' should be '12'
    mm = mm < 10 ? "0" + mm : mm;
    return `${hh}:${mm} ${ampm}`;
  }

  generateBracket(payload): Promise<any> {
    return this.http
      .post(`${this.environment?.apiEndPoint}match`, payload)
      .toPromise();
  }

  generateBracket1(payload): Observable<any> {
    return this.http
      .post(`${this.environment?.apiEndPoint}match`, payload)
  }

  createOrUpdateTransaction(payload): SubscribableOrPromise<any> {
    return this.http
      .patch(`${this.environment.apiEndPoint}transaction/pay`, payload)
      .toPromise();
  }

  getAllCountries(): SubscribableOrPromise<any> {
    return this.http.get("./assets/json/countries.json").toPromise();
  }

  getStates(): SubscribableOrPromise<any> {
    return this.http.get<any>('./assets/json/states.json').toPromise();
  }

  getRegFeeDisbursals(payload): SubscribableOrPromise<any> {
    const encodedPayload = encodeURIComponent(payload);
    const url = `${this.environment.apiEndPoint}tournament/disbursals/reg_fee_disbursals?query=${encodedPayload}`;
    return this.http.get(url).toPromise();
  }

  getRegFeeRefundDisbursals(query): SubscribableOrPromise<any> {
    const encodedQuery = encodeURIComponent(query);
    const url = `${this.environment.apiEndPoint}tournament/disbursals/reg_fee_refund_disbursals?query=${encodedQuery}`;
    return this.http.get(url).toPromise();
  }

  rejectTournament(formValues): Observable<any> {
    return this.http.patch(
      this.environment.apiEndPoint + `tournament/reject`,
      formValues
    );
  }

  saveSeasonParticipantDetails = async (payload, tournament) => {
    try {
      // const isFieldValid = Object.values(payload).every((el) => el);

      const isSubMemberFieldsValid = async (isPreviousFieldValid, value) => {
        if (isPreviousFieldValid && value?.length) {
          return value.every((item) => {
            return Object.values(item).every((el) => el);
          });
        } else {
          return isPreviousFieldValid;
        }
      };
      let isFieldValid = Object.values(payload).every((el) => el);
      isFieldValid = await isSubMemberFieldsValid(
        isFieldValid,
        payload?.teamMembers
      );
      isFieldValid = await isSubMemberFieldsValid(
        isFieldValid,
        payload?.substituteMembers
      );
      if (!isFieldValid) {
        this.router.navigate([`/matchmaking/${tournament?.slug}`]);
        return {
          isProcessing: true,
          message: null,
        };
      }

      if (tournament?.participantType == 'individual') {
        const response = await this.searchParticipant(
          this.environment.apiEndPoint,
          'inGamerUserId',
          payload.inGamerUserId,
          tournament?._id
        ).toPromise();
        const isExist = Object.values(response.data).some((el) => el);
        if (isExist) {
          this.router.navigate([`/matchmaking/${tournament?.slug}`]);
          return {
            isProcessing: true,
            message: null,
          };
        }
      }
      if (tournament?.isPaid) {
        this.router.navigate([`/matchmaking/${tournament?.slug}`]);
        return {
          isProcessing: true,
          message: null,
        };
      }

      const saveParticipant = await this.saveParticipant(this.environment.apiEndPoint, payload).toPromise();
      return {
        isProcessing: false,
        message: saveParticipant?.message,
      };
    } catch (error) {
      throw error;
    }
  };

  getSeasonTournaments(params, selectParams = ``, option = ''): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json; charset=utf-8');
    const encodedUrl = encodeURIComponent(params.query);
    let url = `${this.environment.apiEndPoint}tournament/season_tournament?query=${encodedUrl}${selectParams}`;
    if (option) {
      const opt = encodeURIComponent(option);
      url += `&option=${opt}`;
    }
    return this.http.get(url, { headers: httpHeaders });
  }

  fetchSeasonParticipantRegistrationStatus(
    id,
    participantId = null
  ): SubscribableOrPromise<any> {
    let url = `${this.environment.apiEndPoint}participant/season-join-status?seasonId=${id}`;
    if (participantId) {
      url += `&guestId=${participantId}`;
    }
    return this.http.get(url).toPromise();
  }

  getParticipantBracket(params): Observable<any> { return this.http.get(`${this.environment.apiEndPoint}participant`, { params: params }); }

  fetchParticipantRegistrationStatus1(
    id,
    participantId = null
  ) {
    let url = `${this.environment.apiEndPoint}participant/join-status?tournamentId=${id}`;
    if (participantId) {
      url += `&guestId=${participantId}`;
    }
    return this.http.get(url);
  }

  getTournamentDetails(params): Observable<any> {
    return this.http.get(this.environment.apiEndPoint + `tournament/`, { params: params });
  }

  fetchMyTournament(payload): SubscribableOrPromise<any> {
    const { status, page, limit } = payload;
    const url = `${this.environment.apiEndPoint}tournament/my_tournaments?status=${status}&page=${page}&limit=${limit}`;
    return this.http.get(url).toPromise();
  }

  tournamentInvites(payload): SubscribableOrPromise<any> {
    return this.http
      .post(`${this.environment?.apiEndPoint}tournament/invite_player`, payload)
      .toPromise();
  }
}
