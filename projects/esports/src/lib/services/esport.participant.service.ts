import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, SubscribableOrPromise } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class EsportsParticipantService {
  constructor(private http: HttpClient) {}

  public getParticipantsWithQuery(API, params) {
    const encodedUrl = encodeURIComponent(params.filter);
    let url = `${API}participant?query=${encodedUrl}`;
    if (params.projection) {
      const select = encodeURIComponent(params.select);
      url = `&select=${select}`;
    }
    if (params.option) {
      const opt = encodeURIComponent(params.option);
      url += `&option=${opt}`;
    }
    return this.http.get(`${url}`);
  }

  public getMatchOpponent(API, payload): Observable<any> {
    let url = `${API}participant/find-match-opponent`;
    return this.http.post(url, payload);
  }

  public acceptOrRejectChallenge(API, payload) {
    let url = `${API}participant/ladder/accept-reject-challenge`;
    return this.http.patch(url, payload);
  }

  public getMyChallenges(API) {
    let url = `${API}participant/ladder/my-challenges`;

    const params = new HttpParams({
      fromObject: {
        limit: '200',
        page: '1',
      },
    });

    return this.http.get(url, { params });
  }

  public getChallengeDetails(API: string, challengeId: string) {
    let url = `${API}participant/ladder/challenge`;

    const params = new HttpParams({
      fromObject: {
        challengeId: challengeId,
      },
    });

    return this.http.get(url, { params });
  }

  public getMatchDetails(API: string, matchId: string) {
    let url = `${API}match/${matchId}?`;

    return this.http.get(url);
  }

  public suggestNewTime(API, payload) {
    let url = `${API}participant/ladder/suggest-new-time`;
    return this.http.patch(url, payload);
  }

  public getSeasonJoinStatus(API, payload) {
    let url = `${API}participant/season-join-status`;
    return this.http.get(url, {
      params: {
        seasonId: payload.seasonId,
      },
    });
  }

  fetchParticipantByStatus(
    API,
    type: number,
    tournamentId: string,
    page: number
  ): SubscribableOrPromise<any> {
    const requestUrl = `${API}participant/v2?type=${type}&tournamentId=${tournamentId}&page=${page}`;
    return this.http.get(requestUrl).toPromise();
  }

  /**
   * fetch participant details by participant Id
   * @param api
   * @param participantId
   * @returns participant details
   */
  getParticipantDetails(api, query): SubscribableOrPromise<any> {
    const url = `${api}participant/details`;
    return this.http.get(url, {
      params: query
    }).toPromise();
  }
}
