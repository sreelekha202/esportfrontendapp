import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { BehaviorSubject, Observable, SubscribableOrPromise } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class EsportsHomeService {
  public searchedArticle: Observable<[]>;
  public searchedArticleSubject: BehaviorSubject<[]>;
  public searchedTournament: Observable<[]>;
  public searchedTournamentSubject: BehaviorSubject<[]>;
  public searchedSeason: Observable<[]>;
  public searchedSeasonSubject: BehaviorSubject<[]>;
  public searchedVideo: Observable<[]>;
  public searchedVideoSubject: BehaviorSubject<[]>;
  public searchedShop: Observable<[]>;
  public searchedShopSubject: BehaviorSubject<[]>;

  private tournamentStatusFilter = 3;

  private category = '';
  private headerSearchText = '';
  private sort = '';
  private page = '';
  private limit = '';

  constructor(private http: HttpClient, @Inject('env') private environment) {
    this.searchedTournamentSubject = new BehaviorSubject([]);
    this.searchedTournament = this.searchedTournamentSubject.asObservable();
    this.searchedSeasonSubject = new BehaviorSubject([]);
    this.searchedSeason = this.searchedSeasonSubject.asObservable();
    this.searchedArticleSubject = new BehaviorSubject([]);
    this.searchedArticle = this.searchedArticleSubject.asObservable();
    this.searchedVideoSubject = new BehaviorSubject([]);
    this.searchedVideo = this.searchedVideoSubject.asObservable();
    this.searchedShopSubject = new BehaviorSubject([]);
    this.searchedShop = this.searchedShopSubject.asObservable();
  }

  ongoing_tournaments(): Observable<any> {
    return this.http.get(
      this.environment.apiEndPoint + `home/ongoing_tournaments`
    );
  }
  getTournament(params): Observable<any> {
    return this.http.get(this.environment.apiEndPoint + `home/tournament`, {
      params: params,
    });
  }

  getongoing(): Observable<any> {
    return this.http.get(
      this.environment.apiEndPoint +
        `home/tournament?&status=1&limit=8&sort=participantJoined`
    );
  }
  getUpcoming(): Observable<any> {
    return this.http.get(
      this.environment.apiEndPoint +
        `home/tournament?&status=0&limit=8&sort=participantJoined`
    );
  }
  getPastList(): Observable<any> {
    return this.http.get(
      this.environment.apiEndPoint +
        `home/tournament?&status=2&limit=8&sort=participantJoined`
    );
  }

  hottest_post(): Observable<any> {
    return this.http.get(this.environment.apiEndPoint + `home/hottest_post`);
  }

  trending_posts(): Observable<any> {
    return this.http.get(this.environment.apiEndPoint + `home/trending_posts`);
  }

  /**
   * Tournament search private helper function
   */
  private _searchTournament(): Observable<any> {
    return this.http.get(
      this.environment.apiEndPoint +
        `home/tournament?text=${this.headerSearchText}&status=${this.tournamentStatusFilter}&category=${this.category}&sort=${this.sort}&page=${this.page}&limit=${this.limit}`
    );
  }

    /**
   * seasons search private helper function
   */
     private _searchSeason(): Observable<any> {
      return this.http.get(
        this.environment.apiEndPoint +
          `seasons`
      );
    }


    /**
   * shop search private helper function
   */
    private _searchShop(): Observable<any> {
      return this.http.get(
        this.environment.apiEndPoint +
          `user/eshop/get-products-by-category?search=${
            this.headerSearchText
          }&page=${this.page}&pageSize=${12}`
      );
    }

  /**
   * Article search private helper function
   */
  private _searchArticle(): Observable<any> {
    return this.http.get(
      this.environment.apiEndPoint +
        `home/article?text=${this.headerSearchText}&page=${this.page}&limit=${this.limit}`
    );
  }

  /**
   * method to get an announcement object
   */
  getAnnouncements() {
    return this.http.get(this.environment.apiEndPoint + `general/announcement`);
  }

  /**
   * banner list private helper function
   */
  _getBanner(): Observable<any> {
    return this.http.get(this.environment.apiEndPoint + `banner`);
  }

  /**
   * top players List
   */
  _topPlayers(): Observable<any> {
    return this.http.get(this.environment.apiEndPoint + `home/top-player`);
  }

  /**
   * leader board List
   */
  _leaderBoard(): Observable<any> {
    return this.http.get(
      this.environment.apiEndPoint + `home/fetchleaderboard`
    );
  }

  /**
   * Article search private helper function
   */
  private _searchVideo(): Observable<any> {
    return this.http.get(
      this.environment.apiEndPoint +
        `home/video?text=${this.headerSearchText}&page=${this.page}&limit=${this.limit}`
    );
  }

  /**
   * Tournament search service
   * can be called from anywhere
   */
  searchTournament() {
    //this.headerSearchText = query || '';
    this._searchTournament().subscribe(
      (res) => {
        this.searchedTournamentSubject.next(res?.data || []);
        return this.searchedTournamentSubject.value;
      },
      (err) => {
        this.searchedTournamentSubject.next([]);
      }
    );
  }

    /**
   * shop search service
   * can be called from anywhere
   */
  searchShop() {
    this._searchShop().subscribe((res) => {
      this.searchedShopSubject.next(res.data);
      return this.searchedShopSubject.value;
    });
  }
    /**
   * Season search service
   * can be called from anywhere
   */
     searchSeason() {
      //this.headerSearchText = query || '';
      this._searchSeason().subscribe(
        (res) => {
          this.searchedSeasonSubject.next(res?.data || []);
          return this.searchedSeasonSubject.value;
        },
        (err) => {
          this.searchedSeasonSubject.next([]);
        }
      );
    }
  /**
   * Article search service
   * can be called from anywhere
   */
  searchArticle() {
    this._searchArticle().subscribe(
      (res) => {
        this.searchedArticleSubject.next(res?.data || []);
        return this.searchedArticleSubject.value;
      },
      (err) => {
        this.searchedArticleSubject.next([]);
      }
    );
  }

  /**
   * Video search service
   * can be called from anywhere
   */
  searchVideo() {
    this._searchVideo().subscribe(
      (res) => {
        this.searchedVideoSubject.next(res?.data || []);
        return this.searchedVideoSubject.value;
      },
      (err) => {
        this.searchedVideoSubject.next([]);
      }
    );
  }

  /**
   * Update tournament status, this is global function.
   * Header search will take filter value from here only.
   * to add filter value call this function.
   * @param status TournamentStatus
   */
  updateTournamentStatusFilter(status) {
    this.tournamentStatusFilter = status || 0;
    //this.tournamentStatusFilterSubject.next(status || 0);
    this._searchTournament().subscribe((res) => {
      this.searchedTournamentSubject.next(res.data);
      return this.searchedTournamentSubject.value;
    });
  }

  updateSearchParams(param, pagination) {
    this.headerSearchText = param;
    this.sort = encodeURIComponent(JSON.stringify(pagination.sort));
    this.page = pagination.page;
    this.limit = pagination.limit;
  }

  updateView(body): SubscribableOrPromise<any> {
    return this.http
      .patch(`${this.environment.apiEndPoint}home/updateView`, body)
      .toPromise();
  }

  fetchScore(): SubscribableOrPromise<any> {
    return this.http
      .get(`${this.environment.apiEndPoint}home/fetch_score`)
      .toPromise();
  }

  // upcoming slider after Login
  getUpcomingSliderTournament(): Observable<any> {
    return this.http.get(
      `${this.environment.apiEndPoint}tournament/my_tournaments?status=6`
    );
  }

  /**
   * Play Now Ladder Keys
   */
   _playNowKeys(): Observable<any> {
    return this.http.get(this.environment.apiEndPoint + `home/play-now-keys`);
  }
}
