import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class EsportsLikeService {
  constructor(private http: HttpClient, @Inject('env') private environment) {}

  saveLike(formValues): Observable<any> {
    const data = formValues;
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');

    return this.http.post(this.environment.apiEndPoint + 'like', data, {
      headers: httpHeaders,
    });
  }

  updateLike(formValues, id): Observable<any> {
    const data = formValues;
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');

    return this.http.put(this.environment.apiEndPoint + `like/${id}`, data, {
      headers: httpHeaders,
    });
  }

  getLike(articleId, userId): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');

    return this.http.get(
        this.environment.apiEndPoint +
        `like/article?articleId=${articleId}&userId=${userId}`,
      {
        headers: httpHeaders,
      }
    );
  }

  getTournamentCommentsWithLikes(params) {
    return this.http.get(
        this.environment.apiEndPoint +
        `like/tournament?userId=${params['userId']}&tournamentId=${params['tournamentId']}&limit=${params['limit']}&skip=${params['skip']}`
    );
  }
}
