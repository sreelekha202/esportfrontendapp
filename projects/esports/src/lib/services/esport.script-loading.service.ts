import {
  Inject,
  Injectable,
  NgZone,
  Renderer2,
  RendererFactory2,
} from '@angular/core';
import { GlobalUtils } from './global-utils';
import { DOCUMENT } from '@angular/common';
@Injectable({
  providedIn: 'root',
})
export class EsportsScriptLoadingService {
  private renderer: Renderer2;

  constructor(
    private zone: NgZone,
    private rendererFactory: RendererFactory2,
    @Inject('env') private environment,
    @Inject(DOCUMENT) private document: Document
  ) {
    this.renderer = this.rendererFactory.createRenderer(null, null);
  }

  /**
   * Loads scripts that need additional configurations
   * @param url
   * @param variable
   * @param loaded
   * @returns
   */
  registerScript(
    url: string,
    variable: string,
    loaded: (variable: any) => void
  ): void {
    if (GlobalUtils.isBrowser()) {
      const existingVariable = (window as any)[variable];
      if (existingVariable) {
        this.zone.run(() => {
          loaded(existingVariable);
        });
        return;
      }

      const scriptElement = document.createElement('script');
      scriptElement.id = `script-${variable}`;
      scriptElement.innerHTML = '';
      scriptElement.onload = () => {
        this.zone.run(() => {
          loaded((window as any)[variable]);
        });
      };
      scriptElement.src = url;
      scriptElement.async = true;
      scriptElement.defer = true;

      document.getElementsByTagName('head')[0].appendChild(scriptElement);
    }
  }

  /**
   * Loads scripts from urls. do not need additional settings
   * @param {string} scriptSrc - url of the script to be loaded
   * @param {'head' | 'body'} location - location of the script to be loaded
   * @param {string} name - name of the script
   */
  loadScriptFromUrl(scriptSrc, location, name) {
    const existingVariable = (window as any)[`script-${name}`];
    if (existingVariable) {
      return;
    }

    const scriptTagElement: HTMLScriptElement =
      this.renderer.createElement('script');
    scriptTagElement.type = `text/javascript`;
    scriptTagElement.src = scriptSrc;
    scriptTagElement.id = `script-${name}`;
    scriptTagElement.async = true;
    this.renderer.appendChild(this.document[location], scriptTagElement);
  }

  /**
   * Creates scripts from plain text. do not need additional settings
   * @param {string} scriptText - text of the script
   * @param {'head' | 'body'} location - location of the script to be loaded (head / body)
   * @param {string} name - name of the script
   * @param {Boolean} priority - shall the script be put at the top?
   * @param {Boolean} putAtBottom - shall the script be put at the bottom?
   */

  loadStaticScript(
    scriptText,
    location,
    name,
    priority = false,
    putAtBottom = false
  ) {
    const existingVariable = (window as any)[`script-${name}`];
    if (existingVariable) {
      return;
    }

    const scriptTagElement: HTMLScriptElement =
      this.renderer.createElement('script');
    scriptTagElement.type = `text/javascript`;
    let scriptBody = scriptText;
    scriptTagElement.text = scriptBody;
    scriptTagElement.id = `script-${name}`;

    if (priority) {
      const firstChild = this.document[location].firstChild;
      this.renderer.insertBefore(
        this.document[location],
        scriptTagElement,
        firstChild
      );
    } else if (putAtBottom) {
      this.renderer.appendChild(this.document[location], scriptTagElement);
    } else {
      this.renderer.appendChild(this.document[location], scriptTagElement);
    }
  }

  loadGTMNoScript(gtmId) {
    const existingVariable = (window as any)[`noscript-gtm`];
    if (existingVariable) {
      return;
    }
    const noscriptTagElement = this.renderer.createElement('noscript');
    const iframe = this.renderer.createElement('iframe');
    iframe.src = `https://www.googletagmanager.com/ns.html?id=${gtmId}`;
    iframe.height = '0';
    iframe.width = '0';
    iframe.style = 'display:none;visibility:hidden';
    noscriptTagElement.appendChild(iframe);
    noscriptTagElement.id = `noscript-gtm`;
    const firstChild = this.document['body'].firstChild;
    this.renderer.insertBefore(
      this.document['body'],
      noscriptTagElement,
      firstChild
    );
  }

  /**
   * Loads scripts with default settings
   * @param data
   * @returns
   */
  scriptLoader(data) {
    if (!data?.name) return;

    switch (data.name) {
      case 'GTag': {
        if (!this.environment.gTagId) {
          return;
        }
        const scriptSrcUrl = `${this.environment.gTagUrl}${this.environment.gTagId}`;
        this.loadScriptFromUrl(scriptSrcUrl, 'head', 'gtagUrl');
        let scriptText = `window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());
          gtag('config', '${this.environment.gTagId}');`;
        if (this.environment.googleAdsId) {
          scriptText += `
           gtag('config', '${this.environment.googleAdsId}');`;
        }
        this.loadStaticScript(scriptText, 'head', 'gtagText', false);
        break;
      }
      case 'GTMTag': {
        if (!this.environment.gtmId) {
          return;
        }
        const scriptText = `(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','${this.environment.gtmId}');`;
        this.loadStaticScript(scriptText, 'head', 'gtmText', true);
        this.loadGTMNoScript(this.environment.gtmId);
        break;
      }
      case 'twitterUniversalPageView': {
        if (!this.environment.twitterUniversalTagId) {
          return;
        }
        const scriptText = `!(function (e, n, u, a) {e.twq ||
              (a=e.twq=function(){a.exe?a.exe.apply(a,arguments):
                a.queue.push(arguments);},a.version='1',a.queue=[],t=n.createElement(u),
              t.async=!0,t.src='//static.ads-twitter.com/uwt.js',s=n.getElementsByTagName(u)[0],
              s.parentNode.insertBefore(t,s))})(window, document, 'script');
          twq('init', '${this.environment.twitterUniversalTagId}');
          twq('track', 'PageView');`;
        this.loadStaticScript(
          scriptText,
          'body',
          'twitterUniversalText',
          false,
          true
        );
        break;
      }
      case 'facebookPixel': {
        if (!this.environment.facebookPixelId) {
          return;
        }
        const scriptText = `!function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window, document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '${this.environment.facebookPixelId}');
        fbq('track', 'PageView');`;
        this.loadStaticScript(scriptText, 'head', 'facebookPixel', true, false);
        break;
      }

      default:
        break;
    }
  }
}
