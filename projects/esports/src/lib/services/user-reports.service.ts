import { Inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, SubscribableOrPromise } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class UserReportsService {
  constructor(
    private http: HttpClient, 
    @Inject('env') private env
  ) {}

  getUserReports(apiEndPoint): Observable<any> {
    return this.http.get(
      apiEndPoint + `tournament/reports/all_reports`
    );
  }

  getTournamentsUserReport(apiEndPoint, id, pageSize=10, page=1): Observable<any> {
    return this.http.get(
      apiEndPoint + `tournament/reports_by_tournament/${id}?limit=${pageSize}&page=${page}`
    );
  }

  getMyMatches(apiEndPoint): Observable<any> {
    return this.http.get(apiEndPoint + `match/my_matches`);
  }

  getUserReportById(apiEndPoint, id): Observable<any> {
    return this.http.get(apiEndPoint + `tournament/reports/${id}`);
  }

  replyOnUserReport(apiEndPoint, data): Observable<any> {
    return this.http.post(
      apiEndPoint + 'tournament/report/reply',
      data
    );
  }

  closeReport(apiEndPoint, id): Observable<any> {
    return this.http.get(
      apiEndPoint + `tournament/close_report/${id}`
    );
  }

  createReport(apiEndPoint, data): Observable<any> {
    return this.http.post(
      apiEndPoint + 'tournament/create_report',
      data
    );
  }

  getUserMatchesByStatus(status: string, queryParam: string): SubscribableOrPromise<any> {
    const requestUrl = `${this.env.apiEndPoint}match/v2/my_matches?status=${status}${queryParam}`;
    return this.http?.get(requestUrl).toPromise();
  }
}
