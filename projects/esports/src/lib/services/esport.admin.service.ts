import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SubscribableOrPromise } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class EsportsAdminService {
  constructor(private httpClient: HttpClient) {}

  /**
   * service method to add new banner data
   * @param formData
   */
  public addbanner(api, formData) {
    return this.httpClient.post(`${api}banner`, formData);
  }

  getTournamentWinners(api, formData) {
    const encodedFilter = encodeURIComponent(JSON.stringify(formData.filter));
    const encodedOption = encodeURIComponent(JSON.stringify(formData.option));
    return this.httpClient.get(
      `${api}admin/winners?query=${encodedFilter}&option=${encodedOption}`
    );
  }

  /**
   * service method to get list of banners
   */
  public getBanner(api) {
    return this.httpClient.get(`${api}banner`);
  }

  getLogs(api, params) {
    const { limit, page, searchtext } = params;
    let url = 'admin/admin_logs?';
    if (limit != null && limit != undefined && limit != '') {
      url += `&limit=${limit}`;
    }
    if (searchtext) {
      url += `&searchtext=${encodeURIComponent(searchtext)}`;
    }
    if (page) {
      url += `&page=${page}`;
    }
    return this.httpClient.get(api + url);
  }

  getEvents(api) {
    return this.httpClient.get(api + `admin/getEventCodes`);
  }

  public analyticsData(api, filter) {
    if (filter) {
      return this.httpClient.get(
        `${api}admin/analytics?from=${filter.fromDate}&to=${filter.toDate}`
      );
    } else {
      return this.httpClient.get(`${api}admin/analytics`);
    }
  }

  public getTournamentsforPayments(api) {
    const encodedFilter = encodeURIComponent(
      JSON.stringify({
        isFinished: true,
        $or: [
          { isWinnersPrizeDisbursed: { $exists: false } },
          { isWinnersPrizeDisbursed: { $eq: false } },
        ],
      })
    );
    const encodedOption = encodeURIComponent(
      JSON.stringify({ sort: { startDate: -1 } })
    );
    return this.httpClient.get(
      `${api}admin/tournament?query=${encodedFilter}&option=${encodedOption}`
    );
  }

  /**
   * service method to update the banner
   * @param _id
   * @param formData
   */
  public updateBanner(api, _id, formData) {
    return this.httpClient.put(`${api}banner/${_id}`, formData);
  }

  /**
   * service method to get the dashboard counts
   * @param _id
   * @param formData
   */
  public getCounts(api) {
    return this.httpClient.get(`${api}admin/all-count-list`);
  }

  /**
   * service method to get the dashboard counts on filter
   * @param _id
   * @param formData
   */
  public getCountsOnfilter(api, { country, region, gameId }) {
    return this.httpClient.get(
      `${api}admin/all-count-list?gameId=${gameId}&country=${country}&region=${region}`
    );
  }

  /**
   * service method to delete the banner
   * @param _id
   */
  public deletebanner(api, _id) {
    return this.httpClient.delete(`${api}banner/${_id}`);
  }

  /**
   * service method to get list of products
   */
  public getAllProducts(api) {
    return this.httpClient.get(`${api}transaction/getProduct`);
  }

  /**
   * get regions list
   */
  getRegions(api) {
    return this.httpClient.get(`${api}admin/region`);
  }

  /**
   * add regions
   */
  addRegions(api, formData) {
    return this.httpClient.post(`${api}admin/region`, formData);
  }
  /**
   * update regions
   */
  updateRegion(api, _id, formData) {
    return this.httpClient.put(`${api}admin/region/${_id}`, formData);
  }
  /**
   * get language details list
   */
  getLanguageDetails(api) {
    return this.httpClient.get(`${api}admin/languageDetails`);
  }
  /**
   * add language details
   */
  addLanguageDetails(api, formData) {
    return this.httpClient.post(`${api}admin/languageDetails`, formData);
  }
  /**
   * update language details
   */
  updateLanguageDetails(api, _id, formData) {
    return this.httpClient.put(`${api}admin/languageDetails/${_id}`, formData);
  }
  /**
   * get announcement list
   */
  getAnnouncement(api) {
    return this.httpClient.get(`${api}admin/announcement`);
  }

  /**
   * add announcement data
   * @param formData
   */
  addannouncement(api, formData) {
    return this.httpClient.post(`${api}admin/announcement`, formData);
  }

  /**
   * update announcement data based on id
   * @param formData
   * @param _id
   */
  updateAnnouncement(api, formData, _id) {
    return this.httpClient.put(`${api}admin/announcement/${_id}`, formData);
  }

  stcCall(api, formData) {
    return this.httpClient.post(`${api}transaction/payout/stc`, formData);
  }

  paypalCall(api, formData) {
    return this.httpClient.post(`${api}transaction/payout/paypal`, formData);
  }

  checkDisbursal(api, formData) {
    return this.httpClient.post(
      `${api}transaction/getDisbursalStatus`,
      formData
    );
  }

  /**
   * delete announcement data based on id
   * @param formData
   * @param _id
   */
  deleteAnnouncement(api, _id) {
    return this.httpClient.delete(`${api}admin/announcement/${_id}`);
  }

  /**
   * service method to get a particular product
   * @param _id
   */
  public getProduct(api, _id) {
    return this.httpClient.get(`${api}transaction/getProduct/${_id}`);
  }

  /**
   * service method to add the product
   * @param formData
   */
  public addProduct(api, formData) {
    return this.httpClient.post(`${api}transaction/addProduct`, formData);
  }

  /**
   * service method to update a product
   * @param formData
   */
  public updateProduct(api, _id, formData) {
    return this.httpClient.put(
      `${api}transaction/updateProduct/${_id}`,
      formData
    );
  }

  /**
   * service method to delete a product
   * @param formData
   */
  public deleteProduct(api, _id) {
    return this.httpClient.delete(`${api}transaction/deleteProduct/${_id}`);
  }

  /**
   * service method to get all the leaderboard type based config data
   */
  public getLeaderboardConfig(api) {
    return this.httpClient.get(`${api}admin/leaderboard-config/STC`);
  }

  public updateleaderboard(api, type, id, formData) {
    return this.httpClient.put(
      `${api}admin/leaderboard-config/STC/${type}?typeId=${id}`,
      formData
    );
  }

  public deleteleaderboardItem(api, type, id) {
    return this.httpClient.delete(
      `${api}admin/leaderboard-config/STC/${type}?typeId=${id}`
    );
  }

  public addLeaderboard(api, type, formData) {
    return this.httpClient.put(
      `${api}admin/leaderboard-config/STC/${type}`,
      formData
    );
  }

  public addleaderboard(api, type, formData) {
    return this.httpClient.post(
      `${api}admin/leaderboard-config/${type}`,
      formData
    );
  }

  /**
   * service method to create new leaderboard level config data
   */
  public postLeaderboardLevelConfig(api, formData) {
    return this.httpClient.post(`${api}admin/leaderboard/level`, formData);
  }

  /**
   * service method to create new leaderboard badge config data
   */
  public postLeaderboardBadgeConfig(api, formData) {
    return this.httpClient.delete(`${api}admin/leaderboard/badge`, formData);
  }

  /**
   * service method to create new leaderboard level config data
   */
  public updateLeaderboardLevelConfig(api, formData) {
    return this.httpClient.put(`${api}admin/leaderboard/level`, formData);
  }

  /**
   * service method to create new leaderboard badge config data
   */
  public updateLeaderboardBadgeConfig(api, formData) {
    return this.httpClient.put(`${api}admin/leaderboard/badge`, formData);
  }

  /**
   * service method to upload avatars to S3
   */
  public getAvatars(api) {
    return this.httpClient.get(`${api}admin/profile/avatar`);
  }

  /**
   * service method to upload avatars to S3
   */
  public uploadAvatar(api, formData) {
    return this.httpClient.post(`${api}admin/profile/upload-avatar`, formData);
  }

  /**
   * service method to add avatars in admin panel
   */
  public saveAvatar(api, formData) {
    return this.httpClient.post(`${api}admin/profile/avatar`, formData);
  }

  public getOptions(api): SubscribableOrPromise<any> {
    return this.httpClient.get(`${api}admin/option`).toPromise();
  }

  public patchOptions(api, payload): SubscribableOrPromise<any> {
    return this.httpClient.patch(`${api}admin/option`, payload).toPromise();
  }

  //service to update participant visibility
  public updateShowParticipantsStatus(
    api,
    formData
  ): SubscribableOrPromise<any> {
    return this.httpClient
      .post(`${api}admin/updateShowParticipantsStatus`, formData)
      .toPromise();
  }
}
