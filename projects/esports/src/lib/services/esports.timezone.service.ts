import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class EsportsTimezone {

    getTimeZoneName() {
        const date = new Date();
        const timezone = date.toString().slice(date.toString().indexOf('(') + 1, date.toString().length - 1).replace(/[a-z]/g, '').split(" ");
        return timezone.toString().replace(/,/g, '');
       }
}
