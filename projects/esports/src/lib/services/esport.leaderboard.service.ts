import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class EsportsLeaderboardService {
  constructor(private http: HttpClient) {}

  getGameLeaderboard(apiEndPoint, params,type=null): Observable<any> {
    const { gameId, country, state, page, limit } = params;
    let url = null;
    if(!type)
      url = `${apiEndPoint}home/fetchleaderboard/game/${gameId}?`;
    else
      url = `${apiEndPoint}home/fetchteamleaderboard/game/${gameId}?`;

    if (country != null && country != undefined && country != '') {
      url += `&country=${country}`;
    }
    if (state != null && state != undefined && state != '') {
      url += `&state=${state}`;
    }
    if (page != null && page != undefined && page != '') {
      url += `&page=${page}`;
    }
    if (limit != null && limit != undefined && limit != '') {
      url += `&limit=${limit}`;
    }
    return this.http.get(url);
  }

  getUserLeaderboard(apiEndPoint, params): Observable<any> {
    const { userId, month, year } = params;
    let url = `${apiEndPoint}home/fetchleaderboard/${userId}?`;
    if (month != null && month != undefined && month != '') {
      url += `&month=${month}`;
    }
    if (year != null && year != undefined && year != '') {
      url += `&year=${year}`;
    }
    return this.http.get(url);
  }

  getTeamLeaderboard(apiEndPoint, params): Observable<any> {
    const { teamId, month, year } = params;
    let url = `${apiEndPoint}home/fetchteamleaderboard/${teamId}?`;
    if (month != null && month != undefined && month != '') {
      url += `&month=${month}`;
    }
    if (year != null && year != undefined && year != '') {
      url += `&year=${year}`;
    }
    return this.http.get(url);
  }


  checkFollowStatus(apiEndPoint, followedUserId): Observable<any> {
    const url = `${apiEndPoint}userpreference/checkfollowstatus/userfollow/checkfollowstatus/${followedUserId}`;
    return this.http.get(url);
  }

  followUser(apiEndPoint, followedUserId): Observable<any> {
    const url = `${apiEndPoint}userpreference/follow/userfollow/follow/${followedUserId}`;
    return this.http.post(url, null);
  }

  unfollowUser(apiEndPoint, followedUserId): Observable<any> {
    const url = `${apiEndPoint}userpreference/unfollow/userfollow/unfollow/${followedUserId}`;
    return this.http.post(url, null);
  }
}





