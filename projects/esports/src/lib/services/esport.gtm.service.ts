import { Inject, Injectable, Injector } from '@angular/core';
import { GoogleTagManagerService } from 'angular-google-tag-manager';
import { IUser, UserAccountType } from '../models/user';
import * as firebase from 'firebase/app';
import 'firebase/analytics';
import { EsportsUserService } from "./esport.user.service";

export interface SuperProperties {
  fullname?: string;
  phoneNumber?: string;
  gender?: string;
  birthdate?: string;
  country?: string;
  profileType?: string;
  city?: string;
  createdOn?: string;
  userId?: string;
  channel?: string;
  email?: string;
  username?: string;
  points?: string;
}

export interface EventProperties {
  articleTitle?: string;
  banner_title?: string;
  bannerDetails?: any;
  bannerFileUrl?: string;
  commentLength?: number;
  country?: string;
  state?: string;
  created_on?: Date;
  destination?: string;
  errorCode?: string;
  fromPage?: string;
  gameTitle?: string;
  imageName?: any;
  imageType?: any;
  item_brand?: string;
  item_name?: string;
  item_id?: any;
  item_category?: string;
  quantity?: string;
  inviteExternalUsers?: number;
  loginMethod?: string;
  messageLength?: number;
  noOfArticles?: number;
  noOfResults?: number;
  noOfSearches?: number;
  noOfTournaments?: number;
  noOfVideos?: number;
  orderId?: string;
  platform?: string;
  popUpId?: any;
  price?: any;
  productName?: string;
  profileName?: string;
  profileRanking?: string;
  purchase_type?: string;
  redirectionURL?: any;
  reloadMethod?: string;
  reloadAmount?: any;
  searchCharacterLength?: number;
  searchTerms?: string;
  search_terms?: string;
  sort_type?: string;
  sortType?: string;
  tabValue?: string;
  title?: string;
  topupItem?: any;
  topupValue?: any;
  tournamentName?: string;
  videoTitle?: string;
  voucherItem?: string;
  voucherValue?: string;
}

interface gtmFuncParams {
  eventName?: string;
  superProps?: SuperProperties;
  eventProps?: EventProperties;
}

@Injectable({
  providedIn: 'root',
})
export class EsportsGtmService {
  private analytics: any;
  private gtmService: GoogleTagManagerService;
  constructor(
    private injector: Injector,
    private userService: EsportsUserService,
    // private gtmService: GoogleTagManagerService,
    @Inject('env') private env
  ) {
    if (this.env?.enableFirebase) {
      this.initializeFirebaseAnalytics();
    }
    if(this.env.gtmId) {
      this.gtmService = <GoogleTagManagerService>this.injector.get(GoogleTagManagerService)
    }
  }

  initializeFirebaseAnalytics() {
    firebase.default.analytics.isSupported().then((isSupported) => {
      if (isSupported) {
        this.analytics = firebase.default.analytics();
      }
    });
  }

  gtmEvent(event, data) {
    switch (event) {
      case 'user_login':
        this.gtmService.pushTag({
          event: event,
          event_properties: {
            login_method: data.loginMethod,
            last_login_date: '',
            platform: 'Web',
          },
          user_properties: {
            user_id: data.userId,
            last_login_date: '',
          },
        });
        break;
      case 'user_registers':
        this.gtmService.pushTag({
          event: event,
          event_properties: {
            registration_method: data.regMethod,
            registration_date: data.regDate,
            registration_platform: data.regPlatform,
            platform: 'Web',
          },
          user_properties: {
            user_id: data.userId,
            registration_date: data.regDate,
            registration_platform: data.regPlatform,
          },
        });
        break;
      case 'viewed_tournament':
        this.gtmService.pushTag({
          event: event,
          event_properties: {
            tournament_name: data?.name,
            tourname_date: data?.startDate, //YYYY-MM-DD
            tournament_game: data.gameDetail?.name,
            tournament_category: '',
            tournament_type: data?.tournamentType,
            tournament_creator: data.organizerDetail?.fullName,
            tournament_registration: data.isPaid ? 'Paid' : 'Free',
            tournament_format:
              data?.participantType == 'individual' ? '1 vs 1' : 'Team',
            tournament_region: '',
            tournament_bracket_format: data?.bracketType,
            tournament_platform: '',
            tournament_participants: data?.maxParticipants,
            tournament_match_format: 'Best of ' + data?.nfMatchBetweenTwoTeam,
            tournament_rewards: true,
            tournament_total_rewards: '',
            tournament_sponsored: data?.isIncludeSponsor,
            tournament_Venue: '',
            platform: 'Web',
          },
          user_properties: {
            user_id: data.username ? data.username : '',
            login_status: data.loggedInStatus,
          },
        });
        break;
      case 'clicked_join_tournament':
        this.gtmService.pushTag({
          event: event,
          event_properties: {
            tournament_name: data?.name,
            tourname_date: data?.startDate, //YYYY-MM-DD
            tournament_game: data.gameDetail?.name,
            tournament_category: '',
            tournament_type: data?.tournamentType,
            tournament_creator: data.organizerDetail?.fullName,
            tournament_registration: data.isPaid ? 'Paid' : 'Free',
            tournament_format:
              data?.participantType == 'individual' ? '1 vs 1' : 'Team',
            tournament_region: '',
            tournament_bracket_format: data?.bracketType,
            tournament_platform: '',
            tournament_participants: data?.maxParticipants,
            tournament_match_format: 'Best of ' + data?.nfMatchBetweenTwoTeam,
            tournament_rewards: true,
            tournament_total_rewards: '',
            tournament_sponsored: data?.isIncludeSponsor,
            tournament_Venue: '',
            platform: 'Web',
          },
          user_properties: {
            user_id: data.username ? data.username : '',
            login_status: data.loggedInStatus,
          },
        });

        break;
      case 'joined_tournament':
        this.gtmService.pushTag({
          event: event,
          event_properties: {
            tournament_name: data?.name,
            tourname_date: data?.startDate, //YYYY-MM-DD
            tournament_game: data.gameDetail?.name,
            tournament_category: '',
            tournament_type: data?.tournamentType,
            tournament_creator: data.organizerDetail?.fullName,
            tournament_registration: data.isPaid ? 'Paid' : 'Free',
            tournament_format:
              data?.participantType == 'individual' ? '1 vs 1' : 'Team',
            tournament_region: '',
            tournament_bracket_format: data?.bracketType,
            tournament_platform: '',
            tournament_participants: data?.maxParticipants,
            tournament_match_format: 'Best of ' + data?.nfMatchBetweenTwoTeam,
            tournament_rewards: true,
            tournament_total_rewards: '',
            tournament_sponsored: data?.isIncludeSponsor,
            tournament_Venue: '',
            platform: 'Web',
          },
          user_properties: {
            user_id: data.username ? data.username : '',
            login_status: data.loggedInStatus,
            tournaments_joined: '',
            paid_tournaments_joined: '',
            total_spends: '',
          },
        });
        break;
      case 'completed_tournament':
        this.gtmService.pushTag({
          event: event,
          event_properties: {
            tournament_name: data.name,
            tourname_date: data.startDate, //YYYY-MM-DD
            tournament_game: data.gameDetail.name,
            tournament_category: '',
            tournament_type: data.tournamentType,
            tournament_creator: data.organizerDetail.fullName,
            tournament_registration: data.isPaid ? 'Paid' : 'Free',
            tournament_format:
              data.participantType == 'individual' ? '1 vs 1' : 'Team',
            tournament_region: data.venueAddress[0]
              ? data.venueAddress[0].region
              : '',
            tournament_bracket_format: data.bracketType,
            tournament_platform: 'PS5',
            tournament_participants: '10',
            tournament_match_format: 'Best of ' + data.nfMatchBetweenTwoTeam,
            tournament_rewards: true,
            tournament_total_rewards: '3',
            tournament_sponsored: data.isIncludeSponsor,
            tournament_username: 'Punisher',
            tournament_rank: '10th',
            tournament_earning: '0',
            total_points_earned: 100,
            tournament_Venue: 'N/A',
            platform: 'Web',
          },
          user_properties: {
            user_id: data.username ? data.username : '',
            login_status: data.loggedInStatus,
            tournaments_completed: '',
            paid_tournaments_completed: '',
            total_earnings: '',
            trophies_won: '',
            total_points: '',
          },
        });

        break;
      case 'created_tournament':
        this.gtmService.pushTag({
          event: event,
          event_properties: {
            tournament_name: data?.name,
            tourname_date: data?.startDate, //YYYY-MM-DD
            tournament_game: data.gameDetail?.name,
            tournament_category: '',
            tournament_type: data?.tournamentType,
            tournament_creator: data.organizerDetail?.fullName,
            tournament_registration: data.isPaid ? 'Paid' : 'Free',
            tournament_format:
              data?.participantType == 'individual' ? '1 vs 1' : 'Team',
            tournament_region: '',
            tournament_bracket_format: data?.bracketType,
            tournament_platform: '',
            tournament_participants: data?.maxParticipants,
            tournament_match_format: 'Best of ' + data?.nfMatchBetweenTwoTeam,
            tournament_rewards: true,
            tournament_total_rewards: '',
            tournament_sponsored: data?.isIncludeSponsor,
            tournament_username: '',
            tournament_Venue: '',
            tournament_payment_method: '',
            tournament_paid_amount: '',
            platform: 'Web',
          },
          user_properties: {
            user_id: data.username ? data.username : '',
            login_status: data.loggedInStatus,
            tournaments_created: '',
            paid_tournaments_created: '',
            total_spends: '',
          },
        });
        break;
      case 'searched_match':
        this.gtmService.pushTag({
          event: event,
          event_properties: {
            game: data.name,
            platform: 'Web',
          },
          user_properties: {
            user_id: data.username ? data.username : '',
            login_status: data.loggedInStatus,
          },
        });
        break;
      case 'searched_tournament':
        this.gtmService.pushTag({
          event: event,
          event_properties: {
            game: data.name,
            platform: 'Web',
          },
          user_properties: {
            user_id: data.username ? data.username : '',
            login_status: data.loggedInStatus,
          },
        });
        break;
      case 'started_conversation':
        this.gtmService.pushTag({
          event: event,
          event_properties: {
            game: data.name,
            platform: 'Web',
          },
          user_properties: {
            user_id: data.username ? data.username : '',
            login_status: data.loggedInStatus,
          },
        });
        break;
      case 'sent_message':
        this.gtmService.pushTag({
          event: event,
          event_properties: {
            game: data.name,
            platform: 'Web',
          },
          user_properties: {
            user_id: data.username ? data.username : '',
            login_status: data.loggedInStatus ? data.loggedInStatus : '',
          },
        });
        break;
      default:
        break;
    }
    return;
  }

  gtmEventWithSuperProp({ eventName, superProps, eventProps }: gtmFuncParams) {
    let user_properties: SuperProperties = {};
    for (var propName in superProps) {
      if (
        superProps[propName] === null ||
        superProps[propName] === undefined ||
        superProps[propName] === ''
      ) {
        delete superProps[propName];
      }
    }

    // adding extra precaution for spaces
    eventName = eventName.replace(/ /g, '_');
    eventName = eventName.slice(0, 40);

    superProps.channel = 'Web';
    if (superProps) {
      Object.assign(user_properties, superProps);
    }

    let event_properties: EventProperties = {
      created_on: new Date(),
      platform: 'Web',
    };

    if (eventProps) {
      Object.assign(event_properties, eventProps);
    }

    if(this.env.gtmId) {
      this.gtmService.pushTag({
        event: eventName,
        event_properties: event_properties,
        user_properties: user_properties,
      });
    }


    this.firebaseLogEvents({
      eventName: eventName,
      eventProps: event_properties,
      superProps: user_properties,
    });
  }

  firebaseLogEvents(eventData: gtmFuncParams): void {
    if (this.analytics) {
      this.analytics.logEvent(eventData.eventName, {
        ...eventData.eventProps,
        ...eventData.superProps,
        event_properties: JSON.stringify(eventData.eventProps),
        user_properties: eventData.superProps,
      });
    }
  }

  assignLoggedInUsedData(userData: IUser) {
    let dob = userData.dob ? new Date(userData.dob) : null;
    let createdOn = userData.createdOn ? new Date(userData.createdOn) : null;
    let userProp: SuperProperties = {};

    userProp.birthdate = dob
      ? `${dob.getDate()}-${dob.getMonth() + 1}-${dob.getFullYear()}`
      : null;
    userProp.city = userData.country || null;
    userProp.country = userData.country || null;
    userProp.createdOn = createdOn
      ? `${createdOn.getDate()}-${
          createdOn.getMonth() + 1
        }-${createdOn.getFullYear()}`
      : null;
    userProp.email = userData.email || null;
    userProp.fullname = userData.fullName || null;
    userProp.gender = userData.gender || null;
    userProp.phoneNumber = userData.phoneNumber || null;
    userProp.points = userData.accountDetail?.reward?.toString() || null;
    userProp['profileType'] = userData.userTypeDetail?.userAccTypeNo
      ? UserAccountType[userData.userTypeDetail?.userAccTypeNo]
      : UserAccountType[0];
    userProp.userId = userData._id;
    userProp.username = userData.username || userData.fullName;
    userProp.gender = userData.gender;
    return userProp;
  }

  pushGTMTags(eventName: string, eventData = null) {
    let superProperties: SuperProperties = {};
    let eventProperties: EventProperties = {};
    if(this.userService.currentUserSubject.value){
      superProperties = this.assignLoggedInUsedData(this.userService.currentUserSubject.value);
    }
    if (eventData) {
      Object.assign(eventProperties, eventData);
    }
    this.gtmEventWithSuperProp({
      eventName: eventName,
      superProps: superProperties,
      eventProps: eventProperties,
    });
  }

}
