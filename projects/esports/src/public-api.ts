import { from } from 'rxjs';
import { fromPromise } from 'rxjs/internal-compatibility';

/*
 * Public API Surface of esports
 */
export * from './lib/esports.service';
export * from './lib/esports.component';
export * from './lib/esports.module';
export * from './lib/i18n/i18n.module';
export * from './lib/modules/shop/shop.module';
export * from './lib/modules/esports-snack-bar/esports-snack-bar.module';
export * from './lib/modules/esports-loader/esports-loader.module';
export * from './lib/modules/esports-log-reg/esports-log-reg.module';
export * from './lib/modules/esports-elimination/esports-elimination.module';
export * from './lib/pipes/pipe.module';
export * from './lib/pipes';
export * from './lib/modules/esports-custom-pagination/esport-custom-pagination.module';
export * from './lib/modules/esports-season/esport-season.module';
export * from './lib/shared/module/material.module';
export * from './lib/shared/module/shared.module';
export * from './lib/modules/esports-tournament/esports-quick-tournament-type-1/esports-quick-tournament-type-1.module';
export * from './lib/modules/esports-header/esports-header.module';
export * from './lib/directives/directive.module';
export * from './lib/modules/esports-popup/esports-popup.module';

//services
export { EsportsUserService } from './lib/services/esport.user.service';
export { EsportsAdminService } from './lib/services/esport.admin.service';
export { UserReportsService } from './lib/services/user-reports.service';
export { EsportsLeaderboardService } from './lib/services/esport.leaderboard.service';
export { EsportsTournamentService } from './lib/services/esport.tournament.service';
export { EsportsArticleService } from './lib/services/esport.article.service';
export { EsportsGameService } from './lib/services/esport.game.service';
export { EsportsNotificationsService } from './lib/services/esport.notification.service';
export { EsportsRewardService } from './lib/services/esport.reward.service';
export { EsportsGameApiService } from './lib/services/esport.gameApi.service';
export { EsportsChatService } from './lib/services/esport.chat.service';
export { EsportsChatSidenavService } from './lib/services/esport.chatsidenav.service';
export { EsportsConstantsService } from './lib/services/esport.constants.service';
export { EsportsLanguageService } from './lib/services/esport.language.service';
export { EsportImageService } from './lib/services/esport.image.service';
export { EsportsTransactionService } from './lib/services/esport.transaction.service';
export { GlobalUtils } from './lib/services/global-utils';
export { EsportsToastService } from './lib/services/esports.toast.service';
export { EsportsUtilsService } from './lib/services/esport.utils.service';
export { EsportsVideoLibraryService } from './lib/services/esport.videoLibrary.service';
export { EsportsOptionService } from './lib/services/esport.option.service';
export { EsportsParticipantService } from './lib/services/esport.participant.service';
export {
  EsportsGtmService,
  EventProperties,
  SuperProperties,
} from './lib/services/esport.gtm.service';
export { EsportsPaginationService } from './lib/services/esports.pagination.service';
export { EsportsTimezone } from './lib/services/esports.timezone.service';
export { EsportsSeasonService } from './lib/services/esports.season.service';
export { S3UploadService } from './lib/services/esport.s3upload.service';
export { EsportsSocialService } from './lib/services/esport.social.service';
export { EsportsAdvertisementService } from './lib/services/esport.advertisement.service';
export { EsportsAuthServices } from './lib/services/esport.auth.service';
export { EsportsBracketService } from './lib/services/esport.bracket.service';
export { EsportsCommentService } from './lib/services/esport.comment.service';
export { EsportsDeeplinkService } from './lib/services/esport.deeplink.service';
export { EsportsFormService } from './lib/services/esport.form.service';
export { EsportsHomeService } from './lib/services/esport.home.service';
export { EsportsLikeService } from './lib/services/esport.like.service';
export { EsportsMessageService } from './lib/services/esport.message.service';
export { EsportsPlatformFeeService } from './lib/services/esport.platform-fee.service';
export { EsportsUserPreferenceService } from './lib/services/esport.userpreference.service';
export { EsportsScriptLoadingService } from './lib/services/esport.script-loading.service';
export { EsportsRatingService } from './lib/services/esport.rating.service';
export { EsportsProfileService } from './lib/services/esport.profile.service';
export { EsportsWalletService } from './lib/services/esports.wallet.service';
export { EsportsShopService } from './lib/services/esports.shop.service';

// model
export { IUser, UserAccountType, GENDER } from './lib/models/user';
export { IArticle } from './lib/models/article';
export { IGame } from './lib/models/game';
export { IChat } from './lib/models/chat';
export { IPagination } from './lib/models/paginations';
export { ITournament } from './lib/models/tournament';
export { IAdvertisementManagement } from './lib/models/advertisement';
export { IMatch, IMatchSet } from './lib/models/match';
export { IMessage } from './lib/models/message';
export { IParticipant } from './lib/models/participant';
export { PasteImage } from './lib/models/pasteimage';
export { IReward } from './lib/models/reward';
export { TMatch } from './lib/models/tmatch';
export { IBracket } from './lib/models/bracket';
export { IVideoLibrary } from './lib/models/video-library';
export {
  IPOST,
  IPREVIEW_LINK,
  FILE_TYPE,
  POST_TYPE,
} from './lib/models/postfeed';
export { ILIKE, LKIE_REACT } from './lib/models/likePost';
export { ICOMMENT } from './lib/models/commentPost';
export { IMODAL_POST, POST_EVENT_TYPE } from './lib/models/payloadEvent';
export {
  IO_COMMENT,
  IO_LIKE,
  IO_REPLY,
  IO_REPORT,
} from './lib/models/feed-socketIo';
// components
export * from './lib/components/esportschatsidebar/esportschatsidebar.component';
export * from './lib/components/esportschat/esportschat.component';
export * from './lib/components/chat/chat.component';
export * from './lib/components/esportschatwindow/esportschatwindow.component';
export * from './lib/modules/esports-snack-bar/esports-snack-bar.component';
export * from './lib/components/esportschatlog/esportschatlog.component';
export * from './lib/modules/esports-loader/loading/loading.component';
export * from './lib/modules/esports-loader/fullscreen-loading/fullscreen-loading.component';
export * from './lib/modules/esports-custom-pagination/esport-custom-pagination.component';
//esports-log-reg
export * from './lib/modules/esports-log-reg/select-profile/select-profile.component';
//esports-elimination
export * from './lib/modules/esports-elimination/lader-standing/lader-standing.component';
//esports-season
export * from './lib/modules/esports-season/season-admin-list/season-admin-list.component';
export * from './lib/modules/esports-season/create-season/create-season.component';
export * from './lib/modules/esports-season/active-seasons/active-seasons.component';
export * from './lib/modules/esports-season/feature-player/feature-player.component';
export * from './lib/modules/esports-season/season-notification/season-notification.component';
//esports-tournament
export * from './lib/modules/esports-tournament/esports-quick-tournament-type-1/esports-quick-tournament-type-1.component';
export * from './lib/modules/esports-tournament/components/select-game/esports-select-game.component';
export * from './lib/modules/esports-tournament/components/input-search/esports-input-search.component';
export * from './lib/modules/esports-tournament/components/tournament-name/esports-tournament-name.component';
export * from './lib/modules/esports-tournament/components/tournament-name-input/esports-tournament-name-input.component';
export * from './lib/modules/esports-tournament/components/select-platform/esports-select-platform.component';
export * from './lib/modules/esports-tournament/components/select-date/esports-select-date.component';
export * from './lib/modules/esports-tournament/components/tournament-date/esports-tournament-date.component';
export * from './lib/modules/esports-tournament/components/tournament-time/esports-tournament-time.component';
export * from './lib/modules/esports-tournament/components/tournament-numberplayer/esports-tournament-numberplayer.component';
export * from './lib/modules/esports-tournament/components/tournament-details/esports-tournament-details.component';
export * from './lib/modules/esports-tournament/components/tournament-image/esports-tournament-image.component';
//headers
export * from './lib/modules/esports-header/header-type-1/header-type-1.component';
//popup
export * from './lib/modules/esports-popup/esports-info-popup/esports-info-popup.component';
export * from './lib/modules/esports-popup/esports-edit-popup/esports-edit-popup.component';

//WYSIWYG Editor
export * from './lib/modules/WYSIWYG-editor/WYSIWYG-editor.service';
export * from './lib/modules/WYSIWYG-editor/WYSIWYG-editor.component';
export * from './lib/modules/WYSIWYG-editor/WYSIWYG-editor-toolbar.component';
export * from './lib/modules/WYSIWYG-editor/WYSIWYG-editor.module';
export {
  WYSIWYGEditorConfig,
  CustomClass,
} from './lib/modules/WYSIWYG-editor/config';
