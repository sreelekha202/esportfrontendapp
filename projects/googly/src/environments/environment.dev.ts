// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  buildConfig: "dev",
  apiEndPoint: "https://24n3z33o7e.execute-api.ap-southeast-2.amazonaws.com/dev/",
  currentToken: "DUID",
  facebookAPPID: "1417432915264232",
  googleAPPID:
    "346336302247-93msahs9cjrh82dfefgb824sske1g1uo.apps.googleusercontent.com",
  socketEndPoint: "https://chat-spark.dynasty-dev.com",
  cookieDomain: ".dynasty-dev.com",
  eCommerceAPIEndPoint:
    "https://tuub60cp6h.execute-api.ap-southeast-1.amazonaws.com/dev/",
  firebase: {
    apiKey: "AIzaSyATZFDppCblUstkDrf7HZjMktacH2zkKPU",
    authDomain: "paida-test.firebaseapp.com",
    projectId: "paida-test",
    storageBucket: "paida-test.appspot.com",
    messagingSenderId: "78844726296",
    appId: "1:78844726296:web:607036be8f64ce7f79476d",
    measurementId: "G-YNC2LN04G9"
  },
  articleS3BucketName: "article",
  tournamentS3BucketName: "tournament",
  iOSBundelName: "",
  iOSAppstoreURL: "",
  paypal_client_id:
    "ASf-mIwkHu2j6hchcTquNOcfFtBbfzxC5qhbjOiXrQ1v-DTAlsUFnge0XHEJrUC_UtDiSBR7UvTK48WE&currency=EUR",
  enableFirebase: false,
  defaultLangCode: 'en',
  rtl: ['en'],
  language: [
    { code: 'en', key: 'english', value: 'English' },
    { code: 'ar', key: 'arabic', value: 'عربى' },
  ],
  pageSizeOptions: [5, 10, 15, 20],
  maxparticipant: {
    single: 4096,
    double: 2048,
    roundRobin: 128,
    battleRoyale: 4096,
  },
  gTagId: '',
  gTagUrl: 'https://www.googletagmanager.com/gtag/js?id=',
  gtmId: '',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
