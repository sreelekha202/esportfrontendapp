export const environment = {
  production: true,
  buildConfig: "prod",
  apiEndPoint: "https://24n3z33o7e.execute-api.ap-southeast-2.amazonaws.com/dev/",
  currentToken: "DUID",
  facebookAPPID: "1417432915264232",
  googleAPPID:
    "346336302247-93msahs9cjrh82dfefgb824sske1g1uo.apps.googleusercontent.com",
  socketEndPoint: "https://chat-spark.dynasty-dev.com",
  cookieDomain: ".dynasty-stagging.com",
  eCommerceAPIEndPoint:
    "https://tuub60cp6h.execute-api.ap-southeast-1.amazonaws.com/dev/",
  firebase: {
    apiKey: "AIzaSyATZFDppCblUstkDrf7HZjMktacH2zkKPU",
    authDomain: "paida-test.firebaseapp.com",
    projectId: "paida-test",
    storageBucket: "paida-test.appspot.com",
    messagingSenderId: "78844726296",
    appId: "1:78844726296:web:607036be8f64ce7f79476d",
    measurementId: "G-YNC2LN04G9"
  },
  articleS3BucketName: "article",
  tournamentS3BucketName: "tournament",
  iOSBundelName: "",
  iOSAppstoreURL: "",
  paypal_client_id:
    "AdMFsBVl_2MVoO2KRT9feGxThC2h8nwUo2mANwlp1lqfY5S1RyG-82f99-filmDdpMXOQbnx8pli8WPd&currency=EUR",
    enableFirebase: false,
    defaultLangCode: 'en',
    rtl: ['en'],
    language: [
      { code: 'en', key: 'english', value: 'English' },
      { code: 'ar', key: 'arabic', value: 'عربى' },
    ],
    pageSizeOptions: [5, 10, 15, 20],
    maxparticipant: {
      single: 4096,
      double: 2048,
      roundRobin: 128,
      battleRoyale: 4096,
    },
    gTagId: '',
    gTagUrl: 'https://www.googletagmanager.com/gtag/js?id=',
    gtmId: 'G-FB4CHLFXC0',
  };
