import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { CollapseModule } from 'ngx-bootstrap/collapse';

import { I18nModule } from 'esports';
import { MaterialModule } from '../../modules/material.module';

import { SearchFilterComponent } from './search-filter.component';
import { environment } from '../../../../environments/environment';

const modules = [CollapseModule, CommonModule,  I18nModule.forRoot(environment), MaterialModule];

@NgModule({
  declarations: [SearchFilterComponent],
  imports: modules,
  exports: [SearchFilterComponent],
})
export class SearchFilterModule {}
