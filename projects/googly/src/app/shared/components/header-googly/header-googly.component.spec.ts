import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderGooglyComponent } from './header-googly.component';

describe('PaidiaHeaderComponent', () => {
  let component: HeaderGooglyComponent;
  let fixture: ComponentFixture<HeaderGooglyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HeaderGooglyComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderGooglyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
