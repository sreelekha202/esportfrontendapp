import { Component, OnInit, Input } from '@angular/core';
import { AppHtmlRoutes } from '../../../app-routing.model';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { EsportsGameService } from 'esports';

@Component({
  selector: 'app-header-googly',
  templateUrl: './header-googly.component.html',
  styleUrls: ['./header-googly.component.scss'],
  host: { '[join-tournament]': 'isJoinTournament()' }
})
export class HeaderGooglyComponent implements OnInit {
  @Input() currentPageLink: string;
  @Input() routerLink: string;
  @Input() title: string;
  matchmakingDetails: any;
  selectedGame: any;
  checkQuick8 = false;
  check_lobby = false;
  AppHtmlRoutes = AppHtmlRoutes;
  popup = false;
  popupjoin = false;
  isCheck_lobby=false;
  constructor(
    private _location: Location,
    private router: Router,
    private gameService: EsportsGameService,
  ) { }

  checkTitle: string = 'Create Tournament';

  ngOnInit(): void {
    if(this.title){

    }
    this.matchmakingDetails = this.gameService.createMatchMaking.subscribe(
      (data: any) => {
        if (data) {
          this.selectedGame = data.selectedGame;
        }
      }
    );
    this.checkRouter();
    this.router.events.subscribe((val) => {
      this.checkRouter();
    });
  }
  checkRouter() {
    if (this.router.url === '/get-match/game-lobby') {
      this.routerLink = '/matchmaking';
      this.check_lobby = true;
    }
    if (this.router.url === '/get-match/game-lobby') {
      this.routerLink = '/matchmaking';
      this.check_lobby = true;
    }
    if (this.router.url === '/get-match/match-lobby-non-api') {
      this.check_lobby = true;
      this.isCheck_lobby = true;
    }
    if (this.router.url === '/get-match/match-completed') {
      this.check_lobby = true;
    }
    if (this.router.url === '/create-tournament/CreateQuick') {
      this.checkQuick8 = true;
    } else {
      this.checkQuick8 = false;
    }
  }
  backClicked() {
    this._location.back();
  }
  isMatchmakingRoute() {
    let gameLobbyFlag = this.router.url.includes('/get-match/game-lobby');
    gameLobbyFlag ? (this.check_lobby = true) : '';
    return (
      this.router.url === '/get-match/platforms' ||
      this.router.url === '/get-match/platforms-single' ||
      this.router.url === '/get-match/team-game' ||
      this.router.url === '/get-match/game-loading' ||
      this.router.url === '/get-match/casual-matchmaking' ||
      this.router.url === '/get-match/match-lobby-api' ||
      this.router.url === '/get-match/match-lobby-non-api' ||
      this.router.url === '/get-match/match-lobby' ||
      this.router.url === '/get-match/match-detail' ||
      this.router.url === '/get-match/tournament-scoring' ||
      this.router.url === '/get-match/update-score' ||
      this.router.url === '/get-match/match-completed' ||
      this.router.url === '/get-match/season/select-team' ||
      gameLobbyFlag
    );
  }
  isMatchmakingLobby() {
    return this.router.url === '/get-match/game-lobby';
  }
  isMatchLobby() {
    return (
      this.router.url === '/get-match/game-lobby' ||
      this.router.url === '/get-match/match-lobby-api'
    );
  }
  haveBtnBack() {
    return (
      this.router.url === '/get-match/platforms' ||
      this.router.url === '/get-match/platforms-single' ||
      this.router.url === '/get-match/game-loading' ||
      this.router.url === '/get-match/casual-matchmaking' ||
      this.router.url === '/get-match/game-lobby' ||
      this.router.url === '/get-match/match-lobby-api' ||
      this.router.url === '/get-match/match-lobby-non-api' ||
      this.router.url === '/get-match/match-completed' ||
      this.router.url === '/get-match/season/select-team'
    );
  }
  noBtnClose() {
    return (
      this.router.url === '/get-match/scoring-success'
    );
  }
  havePopupConfirmExit() {
    return (
      this.router.url === '/get-match/game-loading' ||
      this.router.url === '/get-match/casual-matchmaking' ||
      this.router.url === '/get-match/tournament-scoring' ||
      this.router.url === '/get-match/select-platforms' ||
      this.router.url === '/get-match/platforms-single' ||
      this.router.url === '/get-match/game-lobby' ||
      this.router.url === '/get-match/match-lobby-api' ||
      this.router.url === '/get-match/match-lobby-non-api' ||
      this.router.url === '/get-match/match-completed' ||
      this.router.url === '/get-match/match-detail' ||
      this.router.url === '/get-match/update-score' ||
      this.router.url === '/get-match/season/select-team'
    );
  }
  isJoinTournament() {
    return (
      this.router.url === '/join-tournament/checkout' ||
      this.router.url === '/join-tournament/payment-successful' ||
      this.router.url === '/join-tournament/teammembers' ||
      this.router.url === '/join-tournament/teamdetails' ||
      this.router.url === '/join-tournament/registration' ||
      this.router.url === '/join-tournament/soloregistration' ||
      this.router.url === '/join-tournament'
    );
  }

  isMatchDetails(){
    return (this.router.url.includes('/get-match/match-detail'));
  }

  isTitleJoin() {
    return (this.router.url === '/join');
  }
  isEditVideo() {
    return (this.router.url.includes('/profile/edit-video'));
  }
  isCreateVideo() {
    return (this.router.url === '/profile/add-video');
  }
}
