import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderGooglyComponent } from './header-googly.component';
import { RouterModule } from '@angular/router';

import { MatIconModule } from '@angular/material/icon';
import { I18nModule } from 'esports';
import { environment } from '../../../../environments/environment';

@NgModule({
  declarations: [HeaderGooglyComponent],
  imports: [CommonModule, RouterModule, MatIconModule,  I18nModule.forRoot(environment),],
  exports: [HeaderGooglyComponent],
})
export class HeaderGooglyModule {}
