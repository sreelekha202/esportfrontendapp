import { PhoneNoComponent } from './phone-no/phone-no.component';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';

import { CollapseModule } from 'ngx-bootstrap/collapse';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatIconModule } from '@angular/material/icon';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { I18nModule,PipeModule } from 'esports';
import { LoadingModule } from '../../../core/loading/loading.module';
import { CounterComponent } from './counter/counter.component';
import { DropDownComponent } from './drop-down/drop-down.component';
import { DurationCounterComponent } from './duration-counter/duration-counter.component';
import { InputComponent } from './input/input.component';
import { SelectToggleComponent } from './select-toggle/select-toggle.component';
import { ToggleSwitchComponent } from './toggle-switch/toggle-switch.component';
import { UploadComponent } from './upload/upload.component';
import { MultipleSelectComponent } from './multiple-select/multiple-select.component';
import { NgxIntlTelInputModule } from 'ngx-intl-tel-input';
import { environment } from '../../../../environments/environment';

const components = [
  CounterComponent,
  DropDownComponent,
  DurationCounterComponent,
  InputComponent,
  SelectToggleComponent,
  ToggleSwitchComponent,
  MultipleSelectComponent,
  UploadComponent,
  PhoneNoComponent
];

const modules = [
  CollapseModule,
  CommonModule,
  FontAwesomeModule,
  FormsModule,
  I18nModule.forRoot(environment),
  LoadingModule,
  NgxIntlTelInputModule,
  MatCheckboxModule,
  MatIconModule,
  MatSlideToggleModule,
  NgbModule,
  PipeModule,
  ReactiveFormsModule,
];

@NgModule({
  declarations: components,
  imports: modules,
  exports: components,
})
export class FormComponentModule {}
