import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FooterGooglyComponent } from './footer-googly.component';
import { RouterModule } from '@angular/router';

import { MatIconModule } from '@angular/material/icon';
import { I18nModule } from 'esports';
import { environment } from '../../../../environments/environment';

@NgModule({
  declarations: [FooterGooglyComponent],
  imports: [CommonModule, RouterModule, MatIconModule,  I18nModule.forRoot(environment),],
  exports: [FooterGooglyComponent],
})
export class FooterGooglyModule {}
