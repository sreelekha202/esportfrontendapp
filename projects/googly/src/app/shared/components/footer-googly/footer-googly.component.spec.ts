import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FooterGooglyComponent } from './footer-googly.component';

describe('FooterGooglyComponent', () => {
  let component: FooterGooglyComponent;
  let fixture: ComponentFixture<FooterGooglyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FooterGooglyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FooterGooglyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
