import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderBottomComponent } from './header-bottom.component';
import { RouterModule } from '@angular/router';

import { MatIconModule } from '@angular/material/icon';
import { I18nModule } from 'esports';
import { environment } from '../../../../environments/environment';

@NgModule({
  declarations: [HeaderBottomComponent],
  imports: [CommonModule, RouterModule, MatIconModule,  I18nModule.forRoot(environment),],
  exports: [HeaderBottomComponent],
})
export class HeaderBottomModule {}
