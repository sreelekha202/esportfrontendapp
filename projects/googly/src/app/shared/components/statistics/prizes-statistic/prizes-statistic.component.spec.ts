import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PrizesStatisticsComponent } from './prizes-statistics.component';

describe('PrizesStatisticsComponent', () => {
  let component: PrizesStatisticsComponent;
  let fixture: ComponentFixture<PrizesStatisticsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PrizesStatisticsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PrizesStatisticsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
