import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';

import { I18nModule } from 'esports';
import { environment } from '../../../../../environments/environment';

import { MonthsStatisticComponent } from './months-statistic.component';

const modules = [CommonModule, MatIconModule, MatMenuModule,  I18nModule.forRoot(environment)];

@NgModule({
  declarations: [MonthsStatisticComponent],
  imports: modules,
  exports: [MonthsStatisticComponent],
})
export class MonthsStatisticModule {}
