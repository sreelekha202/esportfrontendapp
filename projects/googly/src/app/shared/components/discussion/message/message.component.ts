import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import {
  IUser,
  EsportsUserService,
  EsportsToastService,
  EsportsCommentService,
  EsportsFormService
} from 'esports';
@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.scss'],
})
export class MessageComponent implements OnInit {
  @Input() _id: string;
  @Input() DOM_ID: string;
  @Input() enableCancelEvent: boolean = true;
  @Input() enableInput: boolean;
  @Input() objectId: string;
  @Input() objectType: string;
  @Input() placeholder: string;
  @Input() text: string;

  @Output() valueEmitOnSubmit = new EventEmitter<any>();
  @Output() valueEmitOnCancel = new EventEmitter<boolean>();

  currentUser: IUser;

  messageForm: FormGroup;
  isProcessing: boolean = false;

  constructor(
    private commentService: EsportsCommentService,
    private fb: FormBuilder,
    private formService: EsportsFormService,
    private toastService: EsportsToastService,
    private userService: EsportsUserService
  ) {}

  ngOnInit(): void {
    this.messageForm = this.fb.group({
      text: [
        '',
        Validators.compose([Validators.required, this.formService.emptyCheck]),
      ],
    });

    this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.currentUser = data;
      }
    });
  }

  ngOnChanges() {
    if (this.text) {
      this.messageForm?.get('text')?.setValue(this.text);
    }
  }

  onSubmit = async () => {
    try {
      const payload = {
        comment: this.messageForm.value.text,
        objectId: this.objectId,
        objectType: this.objectType,
        ...(this._id && { _id: this._id }),
      };

      this.isProcessing = true;
      const message = await this.commentService.upsertComment(payload);
      this.isProcessing = false;
      this.messageForm.reset();
      this.valueEmitOnSubmit.emit({ ...message?.data, isCommentedUser: true });
    } catch (error) {
      this.isProcessing = false;
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  onCancel = async () => {
    this.valueEmitOnCancel.emit(true);
  };
}
