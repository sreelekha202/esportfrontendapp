import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FooterMenuMobileComponent } from './footer-menu-mobile.component';
import { RouterModule } from '@angular/router';

import { MatIconModule } from '@angular/material/icon';
import { I18nModule } from 'esports';
import { environment } from '../../../../environments/environment';

@NgModule({
  declarations: [FooterMenuMobileComponent],
  imports: [CommonModule, RouterModule, MatIconModule,  I18nModule.forRoot(environment),],
  exports: [FooterMenuMobileComponent],
})
export class FooterMenuMobileModule {}
