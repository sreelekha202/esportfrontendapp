import { Component, OnInit } from '@angular/core';
import { AppHtmlRoutes } from '../../../app-routing.model';

@Component({
  selector: 'app-footer-menu-mobile',
  templateUrl: './footer-menu-mobile.component.html',
  styleUrls: ['./footer-menu-mobile.component.scss']
})
export class FooterMenuMobileComponent implements OnInit {
  show= false;
  AppHtmlRoutes = AppHtmlRoutes;
  constructor() { }

  ngOnInit(): void {
  }

}
