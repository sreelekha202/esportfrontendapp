import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  AfterContentChecked,
  ChangeDetectorRef,
} from '@angular/core';

import {
  EsportsToastService,
  EsportsBracketService,
  EsportsUtilsService,
  ITournament,
} from 'esports';
import { Router } from "@angular/router";

@Component({
  selector: 'app-round-robin-multi',
  templateUrl: './round-robin-multi.component.html',
  styleUrls: ['./round-robin-multi.component.scss'],
})
export class RoundRobinMultiComponent
  implements OnInit, OnChanges, AfterContentChecked
{
  @Input() countryList: Array<any> = [];
  @Input() isAdmin: boolean;
  @Input() isScreenshotRequired: boolean;
  @Input() isSeeded: boolean;
  @Input() isShowCountryFlag: boolean = false;
  @Input() participantId: any;
  @Input() scoreReporting: number = 2;
  @Input() stageBracketType: string;
  @Input() structure: any;
  @Input() tournamentId: string;
  @Input() participantType: string;
  @Input() tournament: ITournament | any;
  @Output() openScoreCard = new EventEmitter<any>();

  enableSecondStage: boolean = false;
  isAllDataLoaded: boolean = false;
  isProcessing: boolean = false;
  isUpdateMatchScore: boolean = false;

  group = [];
  matchList = [];
  round = [];
  standingList = [];

  currGroup = 1;
  currRound: any = 0;
  currStage = 1;

  match;
  stageTwoStructure;

  stage: Array<any> = [];

  constructor(
    private bracketService: EsportsBracketService,
    private toastService: EsportsToastService,
    private utilsService: EsportsUtilsService,
    private router: Router,
    private ref: ChangeDetectorRef
  ) {}

  ngOnInit(): void {}

  ngOnChanges(simpleChanges: SimpleChanges) {
    if (this.tournamentId && this.isSeeded) {
      this.stage = [];
      this.fetchDistinctStage();
    }

    if (
      !this.isSeeded &&
      typeof this.isSeeded === 'boolean' &&
      this.structure
    ) {
      this.stage = this.bracketService.assembleMultiRoundRobinStructure(
        this.structure
      );
      this.isAllDataLoaded = true;
    }
  }

  ngAfterContentChecked() {
    this.ref.detectChanges();
  }

  selectedStage(currStage) {
    this.currStage = currStage + 1;
    this.currGroup = 1;
    this.isAllDataLoaded = false;
    this.fetchDistinctStageGroup(this.currStage);
  }

  selectedGroup(currGroup) {
    this.isAllDataLoaded = false;
    this.currGroup = currGroup + 1;
    if (this.currStage == 2) {
      this.fetchSecondStageMatches();
    } else {
      this.currRound = 0;
      this.fetchStanding();
      this.fetchDistinctGroupRound(this.currStage, this.currGroup);
    }
  }

  fetchDistinctStage = async () => {
    try {
      const queryParam = `?query=${encodeURIComponent(
        JSON.stringify({ tournamentId: this.tournamentId })
      )}`;

      const { data } = await this.bracketService.fetchDistinctValue(
        queryParam,
        'currentMatch.stage'
      );

      if (this.stage.length != data?.length) {
        this.stage = data.map((s) => {
          return {
            id: s,
            isLoaded: false,
            group: [],
          };
        });
      }
      this.isAllDataLoaded = true;
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  fetchDistinctStageGroup = async (stageId) => {
    try {
      const isLoaded = this.stage[stageId - 1]?.isLoaded;

      if (!isLoaded) {
        const queryParam = `?query=${encodeURIComponent(
          JSON.stringify({
            tournamentId: this.tournamentId,
            'currentMatch.stage': stageId,
          })
        )}`;

        const { data } = await this.bracketService.fetchDistinctValue(
          queryParam,
          'currentMatch.group'
        );

        this.stage[stageId - 1].group = data.map((g) => {
          return {
            id: g,
            isLoaded: false,
          };
        });
        this.stage[stageId - 1].isLoaded = true;
      } else {
        this.isAllDataLoaded = true;
      }
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  fetchDistinctGroupRound = async (stageId, groupId) => {
    try {
      const isLoaded = this.stage[stageId - 1].group[groupId - 1]?.isLoaded;

      if (!isLoaded) {
        const queryParam = `?query=${encodeURIComponent(
          JSON.stringify({
            tournamentId: this.tournamentId,
            'currentMatch.stage': stageId,
            'currentMatch.group': groupId,
          })
        )}`;

        const { data } = await this.bracketService.fetchDistinctValue(
          queryParam,
          'currentMatch.round'
        );

        this.stage[stageId - 1].group[groupId - 1].round = data.map((r) => {
          return {
            id: r,
            isLoaded: false,
          };
        });

        this.stage[stageId - 1].group[groupId - 1].round.unshift({
          id: 0,
          isLoaded: false,
        });

        this.stage[stageId - 1].group[groupId - 1].isLoaded = true;

        if (this.stage[stageId - 1].group[groupId - 1].round?.length) {
          this.fetchRoundDetails(
            stageId,
            this.stage[stageId - 1].group[groupId - 1].id,
            this.stage[stageId - 1].group[groupId - 1].round[0]?.id
          );
        }
      } else {
        this.isAllDataLoaded = true;
      }
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  fetchRoundDetails = async (stageId, groupId, roundId) => {
    try {
      if (roundId == 0) {
        this.fetchStanding();
      } else {
        const isLoaded =
          this.stage[stageId - 1]?.group[groupId - 1]?.round[roundId]?.isLoaded;

        if (!isLoaded) {
          const query = {
            tournamentId: this.tournamentId,
            'currentMatch.stage': stageId,
            'currentMatch.group': groupId,
            'currentMatch.round': roundId,
          };

          const queryParam = `?query=${this.utilsService.getEncodedQuery(
            query
          )}`;
          const { data } = await this.bracketService.fetchAllMatches(
            queryParam
          );
          this.stage[stageId - 1].group[groupId - 1].round[roundId].match =
            data;
          this.stage[stageId - 1].group[groupId - 1].round[roundId].isLoaded =
            true;
        }
      }
      this.fetchDistinctStage();
      this.isAllDataLoaded = true;
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  fetchSecondStageMatches = async () => {
    try {
      const isLoaded = this.stage[1]?.group[0]?.isLoaded;

      if (!isLoaded) {
        const queryParam = `?query=${encodeURIComponent(
          JSON.stringify({
            tournamentId: this.tournamentId,
            'currentMatch.stage': 2,
            'currentMatch.group': 1,
          })
        )}`;

        const { data } = await this.bracketService.fetchAllMatches(queryParam);
        const stage =
          this.bracketService.assembleMultiStageRoundRobinStructure(data);
        this.stage[1].group[0].match = stage[2].group[1].round;
        this.stage[1].group[0].isLoaded = true;
      }
      this.isAllDataLoaded = true;
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  tabChange(event) {
    this.currRound = event;
    if (event == 0) {
      this.fetchStanding();
    } else {
      this.fetchRoundDetails(this.currStage, this.currGroup, this.currRound);
    }
  }

  scoreCard(match) {
    this.router.navigate([`report-score/${match._id}`]);
    // this.isUpdateMatchScore = true;
    // this.match = match;
  }

  updatedScoreCard(event) {
    this.isUpdateMatchScore = true;
    this.match = event.match;
  }

  exitFromScoreCard(event) {
    this.isUpdateMatchScore = event?.isOpenScoreCard;
    if (this.currStage == 2) {
      this.stage[1].group[0].isLoaded = false;
      this.fetchSecondStageMatches();
    } else {
      this.stage[this.currStage - 1].group[this.currGroup - 1].round[
        this.currRound
      ].isLoaded = false;
      this.fetchRoundDetails(this.currStage, this.currGroup, this.currRound);
    }
  }

  fetchStanding = async () => {
    try {
      this.standingList = [];
      this.isProcessing = true;

      const queryParam = `?stage=${this.currStage}&group=${this.currGroup}&tournamentId=${this.tournamentId}`;
      const response = await this.bracketService.fetchStanding(queryParam);

      this.standingList = response.data;
      this.isProcessing = false;
    } catch (error) {
      this.isProcessing = false;
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  setValues(value, field) {
    this[field] = value + 1;
  }
}
