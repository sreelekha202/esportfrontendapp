import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.scss'],
})
export class CarouselComponent implements OnInit {
  @Input() list: Array<number> = [];
  @Input() title: string;
  @Input() loading: boolean = false;
  @Input() value: number = 0;
  @Input() userDropdown = false;

  @Output() emitValue = new EventEmitter<boolean>();
  constructor() {}

  ngOnInit(): void {}

  slideChange(event) {
    this.emitValue.emit(event || 0);
  }

  selectionChange(event) {
    if (this.value == event) return;
    this.emitValue.emit(event || 0);
  }

}
