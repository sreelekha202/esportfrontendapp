import {
  Component,
  ElementRef,
  Inject,
  OnDestroy,
  OnInit,
  PLATFORM_ID,
} from '@angular/core';
import { DOCUMENT, isPlatformBrowser } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { fromEvent, Subscription } from 'rxjs';
import { environment } from '../../../environments/environment';
import {
  AppHtmlProfileRoutes,
  AppHtmlRoutes,
  AppHtmlRoutesLoginType,
  AppHtmlAccountSettingRoutes,
  AppHtmlOverviewRoutes
} from '../../app-routing.model';
import { TranslateService } from '@ngx-translate/core';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';
import {
  faBars,
  faTimes,
  faSearch,
  faBell,
} from '@fortawesome/free-solid-svg-icons';
import { MatDialog } from '@angular/material/dialog';
import { SettingsLightboxComponent } from '../../modules/settings-lightbox/settings-lightbox.component';
import {
  EsportsUserService,
  EsportsLanguageService,
  EsportsConstantsService,
  EsportsToastService,
  EsportsGameService,
  GlobalUtils,
  EsportsChatService,
  IUser,
  EsportsChatSidenavService,
} from 'esports';
import { AccountSettingComponent } from '../../modules/account-setting/account-setting/account-setting.component';

const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;

enum AppLanguage {
  fr = 'fr',
  en = 'en',
}

@AutoUnsubscribe()
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit, OnDestroy {
  activeLang = this.constantsService?.defaultLangCode;
  AppHtmlProfileRoutes = AppHtmlProfileRoutes;
  AppHtmlRoutes = AppHtmlRoutes;
  AppHtmlRoutesLoginType = AppHtmlRoutesLoginType;
  AppHtmlOverviewRoutes = AppHtmlOverviewRoutes
  collapsed: boolean = true;
  isAdmin: boolean = false;
  isBrowser: boolean;
  isUserLoggedIn: boolean = false;
  mobMenuOpened: boolean = false;
  mobMenuOpeneds: boolean = false;
  showLoader: boolean = false;
  gameListShow: boolean = false;

  currentUser: IUser;
  gameList: any;
  tournaments: any;
  matchlist: any;
  matchcount: number;
  chatWins = [];
  currentUserId: any;

  AppLanguage = [];

  faBell = faBell;
  faSearch = faSearch;
  faBars = faBars;
  faTimes = faTimes;

  navigation = [
    {
      title: 'HEADER.DISCOVER',
      icon: 'icon_discover.svg',
      url: AppHtmlRoutes.content,
    },
    {
      title: 'HEADER.PLAY',
      icon: 'icon_play.svg',
      url: AppHtmlRoutes.play,
    },
    {
      title: 'HEADER.STORE',
      icon: 'icon_shop.svg',
      url: AppHtmlRoutes.store,
    },
    {
      title: 'HEADER.LEADERBOARD',
      icon: 'icon_leaderboard.svg',
      url: AppHtmlRoutes.leaderBoard,
    },
    {
      title: 'HEADER.MATCHMAKING',
      icon: 'icon_matchmaking.svg',
      url: AppHtmlRoutes.matchmaking,
    },
  ];

  socials = [
    {
      link: 'https://twitter.com/SparkNZ',
      icon: 'assets/icons/socials/twitter.svg',
    },
    {
      link: 'https://www.facebook.com/spark4nz/',
      icon: 'assets/icons/socials/facebook.svg',
    },
    {
      link: 'https://www.instagram.com/sparknz/',
      icon: 'assets/icons/socials/instagram.svg',
    },
    {
      link: 'https://www.linkedin.com/company/spark-new-zealand/',
      icon: 'assets/icons/socials/linkedin.svg',
    },
  ];
  userLinks = [
    {
      title: 'My profile',
      icon: 'person_outline',
      url: AppHtmlProfileRoutes.dashboard,
    },
    {
      title: 'My Teams',
      icon: 'people_outline',
      url: AppHtmlProfileRoutes.myTeams,
    },
    {
      title: 'My Tournament',
      icon: 'people_outline',
      url: AppHtmlProfileRoutes.myTeams,
    },
    {
      title: 'My Stats',
      icon: 'show_chart',
      url: AppHtmlProfileRoutes.myStats,
    },
    {
      title: 'My bookmarks',
      icon: 'bookmark_border',
      url: AppHtmlProfileRoutes.bookmarks,
    },
  ];

  private sub1: Subscription;
  userSubscription: Subscription;

  constructor(
    @Inject(PLATFORM_ID) private platformId,
    @Inject(DOCUMENT) private document: Document,
    private chatService: EsportsChatService,
    private chatSidenavService: EsportsChatSidenavService,
    private elementRef: ElementRef,
    private languageService: EsportsLanguageService,
    private router: Router,
    private userService: EsportsUserService,
    public toastService: EsportsToastService,
    public constantsService: EsportsConstantsService,
    public translate: TranslateService,
    public matDialog: MatDialog,
    public gameService: EsportsGameService
  ) {
    this.isBrowser = isPlatformBrowser(platformId);
  }

  ngOnInit(): void {
    this.matchlist = [];
    this.activeLang = this.translate.currentLang;
    this.AppLanguage = this.constantsService?.language;
    this.matchcount = 0;
    this.getGames();

    if (
      this.isBrowser &&
      GlobalUtils.isBrowser() &&
      localStorage.getItem(environment.currentToken)
    ) {
      this.showLoader = true;

      this.userSubscription = this.userService.currentUser.subscribe(
        (data) => {
          if (data) {
            this.currentUser = data;
            this.isUserLoggedIn = !!this.currentUser;
            this.showLoader = false;

            this.currentUserId = this.currentUser._id;
            this.isAdmin =
              data?.accountType === EsportsConstantsService.AccountType.Admin;
            const accessToken = localStorage.getItem(environment.currentToken);

            this.chatService?.login.subscribe((data) => {
              // if (data.accessToken != 'undefined' && data.accessToken != null) {
              this.chatService?.connectUser();

              this.chatService.unreadCount.subscribe((unreadCount) => {
                this.matchcount = unreadCount;
              });

              this.chatService?.getUnReadCount().subscribe((res: any) => { });
              // }
            });
          } else {
            this.userService.getAuthenticatedUser(API, TOKEN);
          }
        },
        (error) => {
          this.showLoader = false;
          this.toastService.showError(error.message);
        }
      );
    }
    this.onHeaderScroll();
  }

  ready() { }

  ngOnDestroy() {
    this.userSubscription?.unsubscribe();
    if (this.currentUserId) {
      this.chatService?.disconnectUser();
    }
  }

  onselectedGame(gameId) {
    localStorage.setItem('gameId', gameId)
  }

  onChatSidenavToggle(): void {
    this.chatSidenavService.toggle();

    if (this.currentUserId) {
      this.chatService?.getAllMatches(this.currentUserId, 1, 30);
    }

    if (this.chatSidenavService.checkOpen()) {
      if (this.chatService.getChatStatus()) {
        this.chatService.setChatStatus(false);
      }
    } else {
      if (this.chatService.getChatStatus()) {
        this.chatService.setChatStatus(false);
      }
    }
  }

  onLogOut(): void {
    if (GlobalUtils.isBrowser()) {
      this.chatService.setChatStatus(false);
      this.userService.logout(API, TOKEN);
      this.chatSidenavService.close();
      this.chatService.doLogout();
      this.chatService?.disconnectUser();
    }

    this.router.navigate([
      AppHtmlRoutes.userPageType,
      AppHtmlRoutesLoginType.emailLogin,
    ]);
  }

  onLanguageChange(lang): void {
    if (GlobalUtils.isBrowser()) {
      localStorage.setItem('currentLanguage', lang);
      this.activeLang = lang;
      this.translate.use(lang);
      this.languageService.setLanguage(lang);
    }
  }

  private onHeaderScroll(): void {
    if (GlobalUtils.isBrowser()) {
      const scrollingContent = document.querySelector('mat-sidenav-content');

      if (scrollingContent) {
        const header = this.elementRef.nativeElement;

        this.sub1 = fromEvent(scrollingContent, 'scroll').subscribe(
          (e: Event) => {
            if ((e.target as Element).scrollTop > header.clientHeight) {
              header.classList.add('is-scrolling');
            } else {
              header.classList.remove('is-scrolling');
            }
          }
        );
      }
    }
  }

  onScrollBody() {
    const hiddenOrVisible = this.mobMenuOpened ? 'hidden' : 'visible';
    this.document.body.style.overflow = hiddenOrVisible;
  }

  openSettingsLightbox(selectedTab = 0) {
    this.router.navigate([
      AppHtmlAccountSettingRoutes.accountSetting
    ]);
  }

  getGames() {
    this.gameService.getGames(API).subscribe(
      (res) => {
        this.gameList = res.data;
      },
      (err) => { }
    );
  }
}
