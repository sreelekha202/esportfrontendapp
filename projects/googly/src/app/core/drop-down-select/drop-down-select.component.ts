import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { FormGroup } from "@angular/forms";

@Component({
  selector: "app-drop-down-select",
  templateUrl: "./drop-down-select.component.html",
  styleUrls: ["./drop-down-select.component.scss"],
})
export class DropDownSelectComponent implements OnInit {
  @Input() customFormControlName: string;
  @Input() customFormGroup: FormGroup;
  @Input() dropdownList;
  @Input() title: string;
  @Input() type: string;

  @Output() valueEmit = new EventEmitter<number>();

  constructor() {}

  ngOnInit(): void {}

  setValue(val) {
    this.customFormGroup.controls[this.customFormControlName].setValue(val);
    this.valueEmit.emit(val);
  }
}
