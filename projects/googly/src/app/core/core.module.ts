import { NgModule } from '@angular/core';
import { environment } from '../../environments/environment';
import { AnnouncementComponent } from './../modules/home/announcement/announcement.component';
import { ArticleItemComponent } from './article-item/article-item.component';
import { DropDownSelectComponent } from './drop-down-select/drop-down-select.component';
import { JumbotronComponent } from './jumbotron/jumbotron.component';
import { ScrollTopComponent } from './scroll-top/scroll-top.component';
import { SearchInputComponent } from './search-input/search-input.component';
import { SnackComponent } from './snack/snack.component';
import { StepperComponent } from './stepper/stepper.component';
import { TournamentsCardComponent } from '../modules/home/tournaments-card/tournaments-card.component';
import { TournamentCardHomeComponent } from '../modules/home/tournament-card-home/tournament-card-home.component';
import { VideoItemComponent } from './video-item/video-item.component';
// import { StcPopupComponent } from './stc-popup/stc-popup.component';
import { SharedModule } from '../shared/modules/shared.module';
import { EsportsLoaderModule } from 'esports';

const components = [
  AnnouncementComponent,
  ArticleItemComponent,
  DropDownSelectComponent,
  JumbotronComponent,
  ScrollTopComponent,
  SearchInputComponent,
  SnackComponent,
  StepperComponent,
  TournamentsCardComponent,
  TournamentCardHomeComponent,
  VideoItemComponent,
  // StcPopupComponent
];

@NgModule({
  declarations: [...components],
  imports: [SharedModule],
  exports: [...components],
})
export class CoreModule {EsportsLoaderModule}
