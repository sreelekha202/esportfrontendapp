import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";

@Component({
  selector: "payment",
  templateUrl: "./payment.component.html",
  styleUrls: ["./payment.component.scss"],
})
export class PaymentComponent implements OnInit {
  @Input() type;
  @Input() transaction;
  @Input() isOtp;

  @Output() valueEmitAfterTransaction = new EventEmitter();
  @Output() valueEmitForOtp = new EventEmitter();
  @Output() valueEmitForAfterOtp = new EventEmitter();
  @Output() isTransactionInProgress = new EventEmitter();

  constructor() {}

  ngOnInit(): void {}

  paypalResponse(data) {
    this.valueEmitAfterTransaction.emit(data);
  }

  isProcessing(data) {
    this.isTransactionInProgress.emit(data);
  }

  stcResponse(data) {
    this.valueEmitForOtp.emit(data);
  }
  stcOtpVerify(data) {
    this.valueEmitForAfterOtp.emit(data);
  }
}
