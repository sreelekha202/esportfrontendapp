import { ValidationErrors, ValidatorFn, AbstractControl,FormGroup, FormControl } from "@angular/forms";

export class MYCustomValidators {
  static equalTo(password: FormControl): ValidatorFn {
    throw new Error("Method not implemented.");
  }
  static patternValidator(regex: RegExp, error: ValidationErrors): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
      if (!control.value) {
        // if control is empty return no error
        return null;
      }

      // test the value of the control against the regexp supplied
      const valid = regex.test(control.value);

      // if true, return no error (no error), else return error passed in the second parameter
      return valid ? null : error;
    };
  }

  static passwordMatchValidator(group: FormGroup) {
    const password = group.get('password').value;
    const certainPassword = group.get('certainPassword').value;
    if (password !== certainPassword){
      group.get("certainPassword").setErrors({ equalTo: true });
      return password !== certainPassword ? null : { equalTo: true }

    }

  }

}
