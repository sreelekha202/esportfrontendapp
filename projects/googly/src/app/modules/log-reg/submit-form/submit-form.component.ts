import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
} from '@angular/core';
import { faFacebookF, faTwitter } from '@fortawesome/free-brands-svg-icons';
import { faEnvelope } from '@fortawesome/free-solid-svg-icons';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import {
  AppHtmlRoutes,
  AppHtmlRoutesLoginType,
} from '../../../app-routing.model';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import {
  EsportsToastService,
  GlobalUtils,
  EsportsGtmService,
  EsportsAuthServices,
} from 'esports';

export interface SubmitFormComponentOutputData {
  [key: string]: string;
}

export interface SubmitFormComponentDataItem {
  title: string;
  subtitle: string;
  checkboxLabel: string;
  checkboxLabelpp: string;
  //checkboxLabelP :string;
  submitBtn: string;
  or: string;
  orLabel: string;
  footerText: string;
  footerLink: string;
  footerLinkUrl: string[];
  firstInputName: string;
  firstInputType: string;
  firstInputPlaceholder: string;
  firstInputIcon: string;
  fifthInputName: string;
  fifthInputType: string;
  fifthInputPlaceholder: string;
  fifthInputIcon: string;
  secondInputName: string;
  secondInputType: string;
  secondInputPlaceholder: string;
  secondInputIcon: string;
  fourthInputName: string;
  fourthInputType: string;
  fourthInputPlaceholder: string;
  fourthInputIcon: string;

  forgotPass?: string;
  error?: string;
  // event:any;
}

@Component({
  selector: 'app-submit-form',
  templateUrl: './submit-form.component.html',
  styleUrls: ['./submit-form.component.scss'],
})
export class SubmitFormComponent implements OnInit {
  @Output() dateChange: EventEmitter<MatDatepickerInputEvent<any>>;
  @Output() submit = new EventEmitter();
  @Input() data: SubmitFormComponentDataItem;
  timeoutId = null;
  hide = true;
  regType: string = 'email';
  isError1: boolean = false;
  isError5: boolean = false;
  isError2: boolean = false;
  isError4: boolean = false;
  isError6: boolean = false;
  @Input() set type(type: AppHtmlRoutesLoginType) {
    this.activeTypeValue = type;

    if (this.activeTypeValue === AppHtmlRoutesLoginType.registration) {
      this.form
        .get('firstInput')
        .setValidators([
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(100),
          Validators.pattern(
            /^[a-zA-Z0-9\u0600-\u06FF,!@#$&()\\-`.+,/\"][\sa-zA-Z0-9\u0600-\u06FF,!@#$*%&()\\-`.+,/\"]*$/
          ),
        ]);
      this.form
        .get('fifthInput')
        .setValidators([
          Validators.required,
          Validators.minLength(3),
          Validators.pattern(/^[a-zA-Z0-9_]+$/),
        ]);

      this.form
        .get('secondInput')
        .setValidators([Validators.required, Validators.email]);

      this.form.get('fourthInput').setValidators([Validators.required]);
      this.form.get('privacyPolicy').setValidators(Validators.pattern('true'));
      this.form.get('dobPolicy').setValidators(Validators.pattern('false'));
      this.form.get('dobPolicy').updateValueAndValidity();
    }

    if (this.activeTypeValue === AppHtmlRoutesLoginType.emailLogin) {
      this.form
        .get('firstInput')
        .setValidators([Validators.required, Validators.email]);
      this.form.get('dobPolicy').updateValueAndValidity();
    }

    if (this.activeTypeValue === AppHtmlRoutesLoginType.phoneLogin) {
      this.form
        .get('firstInput')
        .setValidators([
          Validators.required,
          Validators.pattern(/^[\+\d]?(?:[\d-.\s()]*)$/),
        ]);
      this.form.get('firstInput').updateValueAndValidity();
      this.form.get('dobPolicy').updateValueAndValidity();
    }
  }

  AppHtmlRoutes = AppHtmlRoutes;
  AppHtmlRoutesLoginType = AppHtmlRoutesLoginType;
  activeTypeValue: AppHtmlRoutesLoginType;

  form = this.formBuilder.group({
    firstInput: ['', Validators.required],
    secondInput: ['', Validators.required],
    fourthInput: [''],
    fifthInput: [''],

    keepMeLoggedIn: [false],
    privacyPolicy: [false],
    dobPolicy: [false],
  });

  todayDate: Date = new Date(
    new Date().setFullYear(new Date().getFullYear() - 10)
  );
  parentalDate: Date = new Date(
    new Date().setFullYear(new Date().getFullYear() - 18)
  );
  parental = false;
  isExist = false;

  faFacebookF = faFacebookF;
  faTwitter = faTwitter;
  faEnvelope = faEnvelope;

  constructor(
    private formBuilder: FormBuilder,
    public router: Router,
    public toastService: EsportsToastService,
    private gtmService: EsportsGtmService,
    private authServices: EsportsAuthServices
  ) {
    this.checkForKeepMeLoggedIn();
  }

  ngOnInit() {
    // if (this.activeTypeValue === this.AppHtmlRoutesLoginType.registration) {
    //   this.toastService.showError(
    //     'You need to be at least 10 years old to register on this platform.'
    //   );
    // }
    if (this.activeTypeValue == AppHtmlRoutesLoginType.registration) {
      this.authServices.formData.subscribe((data) => {
        if (data && data.regType) {
          this.updateRegType(data.regType);
          this.form.patchValue(data);
          if (data.fourthInput) this.parentalCheckBox(data.fourthInput);
        }
      });
    }
    this.isLogginPage();
  }

  valid(n) {
    if (n == 1) {
      this.isError1 = false;
    }

    if (n == 5) {
      this.isError5 = false;
      if (
        this.form.controls.firstInput.pristine &&
        !this.form.controls.firstInput.touched
      ) {
        this.isError1 = true;
      }
    }

    if (n == 2) {
      this.isError2 = false;
      if (
        this.form.controls.fifthInput.pristine &&
        !this.form.controls.fifthInput.touched
      ) {
        this.isError5 = true;
      }

      if (
        this.form.controls.firstInput.pristine &&
        !this.form.controls.firstInput.touched
      ) {
        this.isError1 = true;
      }
    }

    if (n == 4) {
      this.isError4 = false;
      if (
        this.form.controls.secondInput.pristine &&
        !this.form.controls.secondInput.touched
      ) {
        this.isError2 = true;
      }
      if (
        this.form.controls.fifthInput.pristine &&
        !this.form.controls.fifthInput.touched
      ) {
        this.isError5 = true;
      }
      if (
        this.form.controls.firstInput.pristine &&
        !this.form.controls.firstInput.touched
      ) {
        this.isError1 = true;
      }
    }

    if (n == 6) {
      this.isError6 = false;

      if (
        this.form.controls.fourthInput.pristine &&
        !this.form.controls.fourthInput.touched
      ) {
        this.isError4 = true;
      }

      if (
        this.form.controls.secondInput.pristine &&
        !this.form.controls.secondInput.touched
      ) {
        this.isError2 = true;
      }
      if (
        this.form.controls.fifthInput.pristine &&
        !this.form.controls.fifthInput.touched
      ) {
        this.isError5 = true;
      }
      if (
        this.form.controls.firstInput.pristine &&
        !this.form.controls.firstInput.touched
      ) {
        this.isError1 = true;
      }
    }
  }

  isUniqueName = async (name) => {
    try {
      const isAvailable = async () => {
        try {
          const response = await this.authServices
            .searchUsername(name)
            .toPromise();
          this.isExist = response.data?.isExist;
          if (this.isExist) {
            this.form.controls['fifthInput'].setErrors({
              incorrect: true,
            });
            this.form.controls['fifthInput'].setErrors({
              incorrect: true,
            });
          } else {
            this.form.controls['fifthInput'].setErrors(null);
            this.form.controls['fifthInput'].setValidators([
              Validators.required,
              Validators.minLength(3),
              Validators.pattern(/^[a-zA-Z0-9_]+$/),
            ]);
            this.form.controls['fifthInput'].updateValueAndValidity();
          }
        } catch (error) {
          throw error;
        } finally {
          clearTimeout(this.timeoutId);
        }
      };
      if (!this.timeoutId) {
        this.timeoutId = setTimeout(isAvailable, 500);
      } else {
        clearTimeout(this.timeoutId);
        this.timeoutId = setTimeout(isAvailable, 500);
      }
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  onCountryChange() {
    this.onKeepLoggedIn();
  }

  parentalCheckBox(date: any) {
    const selectedDate = new Date(date.value);
    if (selectedDate > this.parentalDate && selectedDate < this.todayDate) {
      this.parental = true;
      this.form.get('dobPolicy').setValidators(Validators.pattern('true'));
      this.form.get('dobPolicy').setValue(true);
    } else {
      this.parental = false;
      this.form.get('dobPolicy').setValidators(Validators.pattern('false'));
      this.form.get('dobPolicy').setValue(false);
    }

    this.form.get('dobPolicy').updateValueAndValidity();
  }

  onKeepLoggedIn() {
    if (GlobalUtils.isBrowser()) {
      if (this.form.get('privacyPolicy').value) {
        const keepMeLoggedIn = {
          keepMeLoggedIn: true,
        };
        localStorage.setItem('keepMeLoggedIn', JSON.stringify(keepMeLoggedIn));
      } else {
        localStorage.removeItem('keepMeLoggedIn');
      }
    }
  }

  onKeepdob(date: any) {}

  onSubmitEvent(): void {
    this.submit.emit({
      [this.data.firstInputName]: this.form.value.firstInput,
      [this.data.secondInputName]: this.form.value.secondInput,
      [this.data.fourthInputName]: this.form.value.fourthInput,
      [this.data.fifthInputName]: this.form.value.fifthInput,
      firstInputType: this.data.firstInputName,
      secondInputType: this.data.secondInputName,
      fourthInputType: this.data.fourthInputName,
      fifthInputType: this.data.fifthInputName,
      data: this.form.value,
      regType: this.regType,
    });
  }

  changeInput(input): void {
    if (input === 'phone') {
      this.data.secondInputName = 'email';
      this.data.secondInputType = 'email';
      this.data.secondInputPlaceholder = 'LOG_REG.LOGIN.firstInputPlaceholder1';
      this.data.secondInputIcon = 'email';
      this.form
        .get('secondInput')
        .setValidators([Validators.required, Validators.email]);
      this.form.get('secondInput').reset();
    } else {
      this.data.secondInputName = 'phone';
      this.data.secondInputType = 'phone';
      this.data.secondInputPlaceholder = 'LOG_REG.LOGIN.firstInputPlaceholder';
      this.data.secondInputIcon = 'phone';
      this.form.get('secondInput').reset();
      this.form.get('secondInput').setValidators([Validators.required]);
    }
  }

  changeLoginType(type) {
    if (type === 'email') {
      this.router
        .navigateByUrl('/home', { skipLocationChange: true })
        .then(() => {
          this.router.navigate(['/user', AppHtmlRoutesLoginType.emailLogin]);
        });
    }
    if (type === 'phone') {
      this.router
        .navigateByUrl('/home', { skipLocationChange: true })
        .then(() => {
          this.router.navigate(['/user', AppHtmlRoutesLoginType.phoneLogin]);
        });
    }
  }

  forgot() {
    this.pushGTMTags('View_Forgot_Password');
    this.router
      .navigateByUrl('/home', { skipLocationChange: true })
      .then(() => {
        this.router.navigate(['/user', AppHtmlRoutesLoginType.forgotPass]);
      });
  }

  private checkForKeepMeLoggedIn(): void {
    if (GlobalUtils.isBrowser()) {
      const keepMeLoggedIn = JSON.parse(localStorage.getItem('keepMeLoggedIn'));
      if (keepMeLoggedIn) {
        this.form.get('privacyPolicy').setValue(keepMeLoggedIn.keepMeLoggedIn);
      }
    }
  }

  isLogginPage(): boolean {
    return this.router.url === '/user/email-login';
  }

  updateRegType(type) {
    this.regType = type;
    if (type === 'email') {
      this.data.secondInputName = 'email';
      this.data.secondInputType = 'email';
      this.data.secondInputPlaceholder = 'LOG_REG.LOGIN.firstInputPlaceholder1';
      this.form
        .get('secondInput')
        .setValidators([Validators.required, Validators.email]);
      this.form.get('secondInput').reset();
      this.form.get('secondInput').updateValueAndValidity();
    } else {
      this.data.secondInputName = 'phone';
      this.data.secondInputType = 'phone';
      this.data.secondInputPlaceholder = 'LOG_REG.LOGIN.firstInputPlaceholder';
      this.form.get('secondInput').reset();
      this.form.get('secondInput').clearValidators();
      this.form.get('secondInput').updateValueAndValidity();
      this.form
        .get('secondInput')
        .setValidators([Validators.required, Validators.required]);
    }
  }

  goto(type: number) {
    this.authServices.formDataSubject.next({
      ...this.form.value,
      regType: this.regType,
    });
    if (type == 1) this.router.navigateByUrl(AppHtmlRoutes.privacyPolicy);
    else if (type == 2) this.router.navigateByUrl(AppHtmlRoutes.terms);
  }

  hideSow(display) {
    if (display) {
      this.data.secondInputType = 'password';
    } else {
      this.data.secondInputType = 'text';
    }
  }

  pushViewLoginTag() {
    switch (this.activeTypeValue) {
      case AppHtmlRoutesLoginType.registration:
        this.pushGTMTags('View_Login');
        break;
      case AppHtmlRoutesLoginType.emailLogin:
        this.pushGTMTags('View_Registration');
          break;
      default:
        break;
    }
  }

  pushGTMTags(eventName: string, eventData = null) {
    this.gtmService.pushGTMTags(eventName, eventData)
  }
}
