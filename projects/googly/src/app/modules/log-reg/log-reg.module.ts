import { RouterBackModule } from './../../shared/directives/router-back.module';
import { NgModule } from "@angular/core";
import { CoreModule } from "../../core/core.module";
import { LogRegRoutingModule } from "./log-reg-routing.module";
import { CongratulationComponent } from "./congratulation-form/congratulation.component";
import { PasswordFormComponent } from "./password-form/password-form.component";
import { PhoneNoFormComponent } from "./phone-no/phone-no.component";
import { LogRegComponent } from "./log-reg.component";
import { SocialMediaLoginComponent } from "../../core/social-media-login/social-media-login.component";
import { SubmitFormComponent } from "./submit-form/submit-form.component";
import { VerificationFormComponent } from "./verification-form/verification-form.component";
import { SharedModule } from '../../shared/modules/shared.module';
import {HeaderGooglyModule} from './../../shared/components/header-googly/header-googly.module'
import { ReactiveFormsModule } from '@angular/forms';
import { NgxCaptchaModule } from 'ngx-captcha';
import {NgxIntlTelInputModule} from "ngx-intl-tel-input";
import { environment } from '../../../environments/environment';
import { AngularFireModule } from '@angular/fire';
import { EsportsCustomPaginationModule, EsportsLoaderModule, EsportsLogRegModule } from 'esports'

@NgModule({
  declarations: [
    CongratulationComponent,
    LogRegComponent,
    PasswordFormComponent,
    PhoneNoFormComponent,
    SocialMediaLoginComponent,
    SubmitFormComponent,
    VerificationFormComponent,
  ],
  imports: [
    CoreModule,
     LogRegRoutingModule,
     SharedModule,
     RouterBackModule,
    ReactiveFormsModule,
    NgxCaptchaModule,
    EsportsLogRegModule.forRoot(environment),
    AngularFireModule.initializeApp(environment.firebase),
    NgxIntlTelInputModule,
    EsportsCustomPaginationModule,
     EsportsLoaderModule,
     HeaderGooglyModule
    ],
  exports: [PhoneNoFormComponent, VerificationFormComponent],
})
export class LogRegModule { }
