import { Component, Inject, Input, OnInit } from "@angular/core";
import { DOCUMENT } from "@angular/common";

import {
  AppHtmlRoutes,
  AppHtmlRoutesLoginType,
} from "../../../app-routing.model";
import { SubmitFormComponentOutputData } from "../submit-form/submit-form.component";

export interface CongratulationComponentData {
  title: string;
  subtitle: string;
}

@Component({
  selector: "app-congratulation",
  templateUrl: "./congratulation.component.html",
  styleUrls: ["./congratulation.component.scss"],
})
export class CongratulationComponent implements OnInit {
  @Input() data: CongratulationComponentData;
  @Input() formValue: SubmitFormComponentOutputData;

  AppHtmlRoutes = AppHtmlRoutes;
  AppHtmlRoutesLoginType = AppHtmlRoutesLoginType;

  constructor(@Inject(DOCUMENT) private document: Document) {}

  ngOnInit(): void {}

  goToUrl(): void {
    this.document.location.href = "https://googly.gg/";
  }
}
