import { Subscription } from 'rxjs';

import { Component, OnInit } from '@angular/core';
import { AppHtmlGetMatchRoutes } from '../../../app-routing.model';
import { Router } from '@angular/router';
import { EsportsToastService, EsportsSeasonService, EsportsGtmService, EventProperties, EsportsGameService } from 'esports';
import { MatDialog } from '@angular/material/dialog';
import { environment } from "../../../../environments/environment";
const API = environment.apiEndPoint;
import { AppHtmlRoutes } from '../../../app-routing.model';
import { TranslateService } from "@ngx-translate/core";
@Component({
  selector: 'app-select-platform',
  templateUrl: './select-platform.component.html',
  styleUrls: ['./select-platform.component.scss'],
})
export class SelectPlatformComponent implements OnInit {
  AppHtmlGetMatchRoutes = AppHtmlGetMatchRoutes;
  AppHtmlRoutes = AppHtmlRoutes;
  // isLoaded=true;
  selectedPlatform = null;
  selectedGameType = null;
  platforms = [];
  gameTypes = [];
  matchmakingDetails: Subscription;
  selectedGame: any = null;
  selectedPlatformDetails: any = null;
  createMatchMaking: any;
  showLoader: boolean;

  constructor(
    private gameService: EsportsGameService,
    private esportsSeasonService: EsportsSeasonService,
    private router: Router,
    private matDialog: MatDialog,
    private toastService: EsportsToastService,
    private gtmService: EsportsGtmService,
    private translateService: TranslateService
  ) {
    this.matchmakingDetails = this.gameService.createMatchMaking.subscribe((data: any) => {
      if (data) {
        this.createMatchMaking = data;
        this.selectedGame = data.selectedGame;
        this.setPlatForm(this.selectedGame['platform'])
        this.setGameType(this.selectedGame['bracketTypes'])
      }
    });
  }
  setGameType(arg: any) {
    // arg && arg.single
    if (true) {
      this.gameTypes.push(
        {
          type: 'individual',
          icon: 'assets/icons/matchmaking/game-type/single-player.svg',
          title: 'MATCHMAKING.SELECT_PLATFORM.GAME_TYPES.SINGLE',
        },
      )
    }
    // arg && arg.double
    if (true) {
      this.gameTypes.push(
        {
          type: 'team',
          icon: 'assets/icons/matchmaking/game-type/team.svg',
          title: 'MATCHMAKING.SELECT_PLATFORM.GAME_TYPES.TEAMS',
        },
      )
    }
  }

  ngOnInit(): void {
  }

  // getPlatforms() {
  //   let data: any = []
  //   this.gameService.fetchPlatforms().subscribe((res) => {
  //     // this.isLoaded=false;
  //     // data = res.data;
  //   });
  // }

  setPlatForm(data) {
    data.map((obj) => {
      if (obj.name == 'pc') {
        this.platforms.push({
          ...obj,
          type: 'pc',
          icon: 'assets/icons/matchmaking/platforms/pc.svg',
          title: 'MATCHMAKING.SELECT_PLATFORM.PLATFORMS.PC',
        })
      }
      else if (obj.name == 'Mobile') {
        this.platforms.push({
          ...obj,
          type: 'mobile',
          icon: 'assets/icons/matchmaking/platforms/mobile.svg',
          title: 'MATCHMAKING.SELECT_PLATFORM.PLATFORMS.MOBILE',
        })
      }
      else if (obj.name == 'Other') {
        this.platforms.push({
          ...obj,
          type: 'console',
          icon: 'assets/icons/matchmaking/platforms/console.svg',
          title: 'MATCHMAKING.SELECT_PLATFORM.PLATFORMS.CONSOLE',
        })
      }
      else if (obj.name == 'Xbox') {
        this.platforms.push({
          ...obj,
          type: 'xbox',
          icon: 'assets/icons/matchmaking/platforms/xbox.svg',
          title: 'MATCHMAKING.SELECT_PLATFORM.PLATFORMS.XBOX',
        })
      }
      else if (obj.name == 'PS4') {
        this.platforms.push({
          ...obj,
          type: 'ps4',
          icon: 'assets/icons/matchmaking/platforms/ps4.svg',
          title: 'MATCHMAKING.SELECT_PLATFORM.PLATFORMS.PS4',
        })
      }
      else if (obj.name == 'PS5') {
        this.platforms.push({
          ...obj,
          type: 'ps5',
          icon: 'assets/icons/matchmaking/platforms/ps5.svg',
          title: 'MATCHMAKING.SELECT_PLATFORM.PLATFORMS.PS5',
        })
      }
      else if (obj.name == 'Xbox Series X') {
        this.platforms.push({
          ...obj,
          type: 'Xbox Series X',
          icon: 'assets/icons/matchmaking/platforms/series.svg',
          title: 'MATCHMAKING.SELECT_PLATFORM.PLATFORMS.PS5',
        })
      }
      else if (obj.name == 'Nintendo Switch') {
        this.platforms.push({
          ...obj,
          type: 'Nintendo Switch',
          icon: 'assets/icons/matchmaking/platforms/vector.svg',
          title: 'MATCHMAKING.SELECT_PLATFORM.PLATFORMS.PS5',
        })
      }
      else if (obj.name == 'Nintendo Wii') {
        this.platforms.push({
          ...obj,
          type: 'Nintendo Wii',
          icon: 'assets/icons/matchmaking/platforms/nintendo.svg',
          title: 'MATCHMAKING.SELECT_PLATFORM.PLATFORMS.PS5',
        })
      }
      else {
        this.platforms.push({
          ...obj,
          type: 'pc',
          icon: 'assets/icons/matchmaking/platforms/pc.svg',
          title: 'MATCHMAKING.SELECT_PLATFORM.PLATFORMS.PC',
        })
      }
    })
  }

  selectPlatformType(platform) {
    this.pushGTMTags(`Matchmaking_Select_${platform?.name}_Gaming_Platform`);
    this.selectedPlatform = platform.type
    this.selectedPlatformDetails = platform;
  }
  next() {
    if (this.selectedGame && this.selectedPlatformDetails && this.selectedGameType) {
      const params = {
        game: this.selectedGame._id,
        platform: this.selectedPlatformDetails._id,
        type: this.selectedGameType
      };
      // version 2.0 (Matchmaking)
      this.showLoader = true;
      this.esportsSeasonService.checkSeasons(API, params)
        .subscribe((res) => {
          this.showLoader = false;
          if (res?.data?.data?.season.length > 0) {
            this.toastService.showSuccess(res?.message);
            if (res?.data?.data?.participant) {
              this.router.navigate(
                ['/games/match-season/' + res?.data?.data?.season[0]?._id],
                { queryParams: { playnow: 'true' } }
              );
            } else {
              if (res?.data?.data?.season[0]?.participantType === 'team') {
                this.router.navigate([
                  // `/get-match/team-registration/${res?.data?.data?.season[0]?.slug}-${res?.data?.data?.season[0]?._id}`,
                  `/team-registration/${res?.data?.data?.season[0]?.slug}-${res?.data?.data?.season[0]?._id}`,
                ]);
              } else {
                this.router.navigate([
                  `tournament/${res?.data?.data?.season[0]?.slug}-${res?.data?.data?.season[0]?._id}/join`
                ]);
              }
            }
          } else {
            this.toastService.showError(res?.message);
          }
        }, (err) => {
          this.showLoader = false;
          this.toastService.showError(err.error.message)
        });

      

    }
    // else {
    //   this.toastService.showError("Select game, platform, type.")
    // }
    else if(this.selectedPlatformDetails && !this.selectedGameType){
      this.toastService.showError(
        this.translateService.instant(
          'MATCHMAKING.ERROR.ERR'
        )
      );
    }
    else if(!this.selectedPlatformDetails && this.selectedGameType){
      this.toastService.showError(
        this.translateService.instant(
          'MATCHMAKING.ERROR.ERROR'
        )
      );
    }
    else if(!this.selectedPlatformDetails && !this.selectedGameType){
      this.toastService.showError(
        this.translateService.instant(
          'MATCHMAKING.ERROR.ERROR1'
        )
      );
    }
  }

 
  ngOnDestroy(): void {
    if (this.matchmakingDetails)
      this.matchmakingDetails.unsubscribe();
  }

  pushGTMTags(eventName: string, eventData: EventProperties = null) {
    this.gtmService.pushGTMTags(eventName, eventData);
  }

}

