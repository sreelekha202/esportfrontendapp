import { AuthGuard } from './../../shared/guard/auth.guard';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GetMatchComponent } from './get-match.component';
import { GameLobbyComponent } from './game-lobby/game-lobby.component';
import { SelectPlatformComponent } from './select-platform/select-platform.component';
import { ManageScoreComponent } from './manage-score/manage-score.component';
import { SelectGameComponent } from './select-game/select-game.component';
import { MatchDetailComponent } from './match-detail/match-detail.component';
const routes: Routes = [
  {
    path: '',
    component: GetMatchComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'game',
        component: SelectGameComponent
      },
      {
        path: 'platform',
        component: SelectPlatformComponent,
      },
      {
        path: 'game-lobby',
        component: GameLobbyComponent,
      },
      {
        path: 'manage-score',
        component: ManageScoreComponent,
      },
      {
        path: 'match-detail/:id',
        component: MatchDetailComponent,
      }
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GetMatchRoutingModule { }
