import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClipboardModule } from '@angular/cdk/clipboard';
import { CoreModule } from '../../core/core.module';
import { FormComponentModule } from '../../shared/components/form-component/form-component.module';
import { HeaderBottomModule } from '../../shared/components/header-bottom/header-bottom.module';
import { UploadImageModule } from '../../shared/components/upload-image/upload-image.module';
import { GetMatchComponent } from './get-match.component';
import { GetMatchRoutingModule } from './get-match-routing.module';
import { MatchDetailComponent } from './match-detail/match-detail.component';
import { GameLobbyComponent } from './game-lobby/game-lobby.component';
import { SelectPlatformComponent } from './select-platform/select-platform.component';
import { HeaderMatchmakingComponent } from './header-matchmaking/header-matchmaking.component';
// GAME LOBBY COMPONENTS
import { MatchroomChatComponent } from './game-lobby/matchroom-chat/matchroom-chat.component';
import { PlayersComponent } from './game-lobby/players/players.component';
import { PostgameStatsComponent } from './game-lobby/postgame-stats/postgame-stats.component';
import { RouterBackModule } from '../../shared/directives/router-back.module';
import { GameheaderComponent } from './game-lobby/gameheader/gameheader.component';
import { SharedModule } from '../../shared/modules/shared.module';
import {
  EsportsModule,
  EsportsSeasonModule,
  EsportsLoaderModule,
  EsportsCustomPaginationModule,
  EsportsChatService
} from 'esports';
import { FooterMenuMobileModule } from '../../shared/components/footer-menu-mobile/footer-menu-mobile.module';
import { environment } from '../../../environments/environment';
import { ManageScoreComponent } from './manage-score/manage-score.component';
import { TeamBannerComponent } from './manage-score/components/team-banner/team-banner.component';
import { CdkAccordionModule } from '@angular/cdk/accordion';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { JwtInterceptor } from '../../core/helpers/interceptors/token-interceptors.service';
import { HeaderGooglyModule } from '../../shared/components/header-googly/header-googly.module';
import { FooterGooglyModule } from '../../shared/components/footer-googly/footer-googly.module';
import { SelectGameComponent } from './select-game/select-game.component';
const components = [
  GameLobbyComponent,
  GetMatchComponent,
  MatchroomChatComponent,
  PlayersComponent,
  PostgameStatsComponent,
  SelectPlatformComponent,
  ManageScoreComponent,
  GameheaderComponent,
  MatchDetailComponent,
  HeaderMatchmakingComponent,
  TeamBannerComponent,
  SelectGameComponent
];

const modules = [
  ClipboardModule,
  CommonModule,
  CoreModule,
  FormComponentModule,
  GetMatchRoutingModule,
  HeaderGooglyModule,
  SharedModule,
  UploadImageModule,
  RouterBackModule,
  HeaderBottomModule,
  FooterGooglyModule,
  FooterMenuMobileModule,
  EsportsLoaderModule.setColor('#1d252d'),
  EsportsModule.forRoot(environment),
  EsportsSeasonModule.forRoot(environment),
  EsportsCustomPaginationModule,
  CdkAccordionModule
];
@NgModule({
  declarations: components,
  imports: modules,
  providers: [
    EsportsChatService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptor,
      multi: true,
    },
  ]
})
export class GetMatchModule { }
