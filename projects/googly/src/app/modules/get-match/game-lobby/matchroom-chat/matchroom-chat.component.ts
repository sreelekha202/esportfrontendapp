import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-matchroom-chat',
  templateUrl: './matchroom-chat.component.html',
  styleUrls: ['./matchroom-chat.component.scss'],
})
export class MatchroomChatComponent implements OnInit {
  currentGame = null;

  players = [];

  constructor() {}

  ngOnInit(): void {
    // MOCK GAME DATA
    this.currentGame = {
      name: 'Dota2',
      image:
        'https://i114.fastpic.ru/big/2021/0613/ea/1362412cbb852fd522e0ef0b781cafea.jpg',
    };

    // MOCK PLAYERS DATA
    this.players = [
      {
        image:
          'https://www.wallsauce.com/uploads/wallsauce-com/images/blogs/gamer-room.jpg',
        message: {
          text: 'Cool article! Not too sure about the second point OP but, I can see where theyre coming from.',
          time: '11:30pm',
        },
      },
      {
        image:
          'https://www.letsbuyit.se/wp-content/uploads/2020/09/gamingdator1.jpg',
        message: {
          text: 'That’s a good one! We like to read more like this.',
          time: '11:35pm',
        },
      },
      {
        isAuthor: true,
        image:
          'https://animacoesports.com/wp-content/uploads/2021/05/12312.jpg',
        message: {
          text: 'Hey guys, see y’all inside the game in 10 mins! Can’t wait for today’s match!',
          time: '1:11am',
        },
      },
    ];
  }
}
