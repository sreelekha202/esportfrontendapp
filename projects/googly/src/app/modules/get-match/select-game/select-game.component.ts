import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { EsportsGameService, EsportsTournamentService } from 'esports';
import { AppHtmlRoutes } from '../../../app-routing.model';

@Component({
  selector: 'app-select-game',
  templateUrl: './select-game.component.html',
  styleUrls: ['./select-game.component.scss'],
})
export class SelectGameComponent implements OnInit {

  text: string = '';
  tournamentDetails: Subscription;
  createTournament: any;
  AppHtmlRoutes = AppHtmlRoutes;
  games = [];
  selectedGame = []
  showLoader = false;
  isSelectedGameId: any;
  createMatchMaking: any;
  matchmakingDetails: any;
  constructor(
    public eSportsTournamentService: EsportsTournamentService,
    private router: Router,
    private gameService: EsportsGameService
  ) { }


  ngOnInit(): void {

    this.matchmakingDetails = this.gameService.createMatchMaking.subscribe(
      (data) => {
        if (data) {
          this.createMatchMaking = data;
        }
      }
    );
    this.getGames();


  }


  onTextChange = (data) => {
    this.text = data;
    // search on API
    //  this.getGames();
  };

  // search on Array
  filterGame = () => {
    return this.games.filter((game) =>
      game.name.includes(this.text.toLowerCase().trim())
    );
  };

  getGames() {
    this.showLoader = true;
    const param: any = {};
    this.text ? (param.search = this.text) : '';
    this.eSportsTournamentService.getAllGames(param).subscribe(
      (res) => {
        this.showLoader = false;
        if (res && res.data && res.data.length) {
          res.data.map((obj) => {
            this.games.push({ ...obj, name: String(obj.name).toLowerCase() });
          });
        }
      },
      (err) => {
        this.showLoader = false;
      }
    );
  }


  onSelectGame(selectedGame) {

    this.selectedGame = selectedGame
    this.isSelectedGameId = selectedGame?._id

  }
  next() {
    if (this.selectedGame) {
      this.gameService.createMatchMakingSubject.next({
        ...this.createMatchMaking,
        selectedGame: this.selectedGame,
      });
      this.router.navigateByUrl('get-match/platform');
    }
  }
}
