import { Component, OnInit, Input, EventEmitter, Output, } from '@angular/core';
import { EsportsChatService, EsportsConstantsService,
  EsportsLeaderboardService,EsportsToastService,GlobalUtils,
  EsportsChatSidenavService,IUser,EsportsUserService } from 'esports';
import { Subscription } from 'rxjs';
import { AppHtmlProfileRoutes, AppHtmlRoutes,AppHtmlRoutesLoginType } from '../../../../app-routing.model';
import { environment } from './../../../../../environments/environment';
const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;
import { TranslateService } from '@ngx-translate/core';
import {
  faBars,
  faTimes,
  faSearch,
  faBell,
} from '@fortawesome/free-solid-svg-icons';
import { Router } from '@angular/router';

@Component({
  selector: 'app-team-card',
  templateUrl: './team-card.component.html',
  styleUrls: ['./team-card.component.scss'],
})
export class TeamCardComponent implements OnInit {
  @Output() showPopup = new EventEmitter();
  @Input() isManage: boolean = false;
  @Input() isStatus: boolean = false;
  @Input() data: any;
  @Input() teamId;
  activeLang: string = this.constantsService?.defaultLangCode;
  AppHtmlProfileRoutes = AppHtmlProfileRoutes;
  AppHtmlRoutes = AppHtmlRoutes;
  AppHtmlRoutesLoginType = AppHtmlRoutesLoginType;
  collapsed: boolean = true;
  isAdmin: boolean = false;
  isBrowser: boolean;
  isUserLoggedIn: boolean = false;
  mobMenuOpened: boolean = false;
  mobMenuOpeneds: boolean = false;
  showLoader: boolean = false;
  followStatus:String = 'follow';
  teamManageData:any;

  currentUser: IUser;

  tournaments: any;
  matchlist: any;
  matchcount: number;
  chatWins = [];
  currentUserId: any;

  AppLanguage = [];

  faBell = faBell;
  faSearch = faSearch;
  faBars = faBars;
  faTimes = faTimes;

  private sub1: Subscription;
  userSubscription: Subscription;

  constructor(
    private chatService: EsportsChatService,
    private chatSidenavService: EsportsChatSidenavService,
    private router: Router,
    private userService: EsportsUserService,
    private toastService: EsportsToastService,
    private leaderboardService: EsportsLeaderboardService,
    private constantsService: EsportsConstantsService,
    public translate: TranslateService,
    ) {

    // this.isBrowser = isPlatformBrowser(platformId);
  }

  ngOnInit(): void {
    if (this.data) {
      this.teamManageData = this.data;
    }
    this.matchlist = [];
    this.activeLang = this.translate.currentLang;
    this.AppLanguage = this.constantsService?.language;
    this.matchcount = 0;

    if (
      this.isBrowser &&
      GlobalUtils.isBrowser() &&
      localStorage.getItem(environment.currentToken)
    ) {
      this.showLoader = true;

      this.userSubscription = this.userService.currentUser.subscribe(
        (data) => {
          if (data) {
            this.currentUser = data;
            this.isUserLoggedIn = !!this.currentUser;
            this.showLoader = false;

            this.currentUserId = this.currentUser._id;
            this.isAdmin =
              data?.accountType === EsportsConstantsService.AccountType.Admin;
            const accessToken = localStorage.getItem(environment.currentToken);

            this.chatService?.login.subscribe((data) => {
              // if (data.accessToken != 'undefined' && data.accessToken != null) {
              this.chatService?.connectUser();

              this.chatService.unreadCount.subscribe((unreadCount) => {
                this.matchcount = unreadCount;
              });

              this.chatService?.getUnReadCount().subscribe((res: any) => {});
              // }
            });
          } else {
            this.userService.getAuthenticatedUser(API, TOKEN);
          }
        },
        (error) => {
          this.showLoader = false;
          // this.toastService.showError(error.message);
        }
      );
    }
    // this.onHeaderScroll();
  }
  onChatSidenavToggle(): void {
    this.chatSidenavService.toggle();
    if (this.currentUserId) {
      this.chatService?.getAllMatches(this.currentUserId, 1, 30);
    }

    if (this.chatSidenavService.checkOpen()) {
      if (this.chatService.getChatStatus()) {
        this.chatService.setChatStatus(false);
      }
    } else {
      if (this.chatService.getChatStatus()) {
        this.chatService.setChatStatus(false);
      }
    }
  }
  ngOnDestroy() {
    this.userSubscription?.unsubscribe();
    if (this.currentUserId) {
      this.chatService?.disconnectUser();
    }
  }

  /* follow or unfollow user actions here */
  followUser(){
    if (this.data) {
      if (this.followStatus == 'follow') {
        this.leaderboardService.followUser(API, this.data.userId).subscribe(
          (res: any) => {
            this.followStatus = 'unfollow';
            this.checkFollowStatus();
            this.toastService.showSuccess(res.message);
            this.userService.refreshCurrentUser(API, TOKEN);
          },
          (err) => {
             this.showLoader = false;
          }
        );
      } else if (this.followStatus == 'unfollow') {
        this.leaderboardService.unfollowUser(API, this.data.userId).subscribe(
          (res: any) => {
            this.followStatus = 'follow';
            this.checkFollowStatus();
            this.toastService.showSuccess(res.message);
            this.userService.refreshCurrentUser(API, TOKEN);
          },
          (res: any) => {
             this.showLoader = false;
          }
        );
      }
    } else {
      
    }
  }

  /* check for follow status here */
  checkFollowStatus() {
    this.leaderboardService.checkFollowStatus(API, this.data.userId).subscribe(
      (res: any) => {
        if (res.data[0].status == 1) {
          this.followStatus = 'unfollow';
        } else if (res.data[0].status == 0) {
          this.followStatus = 'follow';
        }
      },
      (err) => {
         this.showLoader = false;
      }
    );
  }

  
}
