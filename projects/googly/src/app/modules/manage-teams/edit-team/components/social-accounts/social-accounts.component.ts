import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-social-accounts',
  templateUrl: './social-accounts.component.html',
  styleUrls: ['./social-accounts.component.scss'],
})
export class SocialAccountsComponent implements OnInit {
  accounts = [];
  @Input() listData;
  constructor() {}

  ngOnInit(): void {
    // MOCK DATA
    this.accounts = this.listData
  }
}
