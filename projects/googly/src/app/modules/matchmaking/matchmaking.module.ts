import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CountdownModule } from "ngx-countdown";
import { MatchmakingComponent } from './matchmaking.component';
import { EsportsCustomPaginationModule, EsportsLoaderModule } from 'esports';
import { SharedModule } from '../../shared/modules/shared.module';
import { FooterMenuMobileModule } from '../../shared/components/footer-menu-mobile/footer-menu-mobile.module';
import { FooterGooglyModule } from '../../shared/components/footer-googly/footer-googly.module';
import { HeaderBottomModule } from '../../shared/components/header-bottom/header-bottom.module';
import { PrizePoollModule } from '../tournament/pipe/prize-pooll.module';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { CoreModule } from '../../core/core.module';
import { RouterModule, Routes } from '@angular/router';
const routes: Routes = [{ path: '', component: MatchmakingComponent }];
@NgModule({
  declarations: [MatchmakingComponent],
  imports: [
    CommonModule,
    CountdownModule,
    MatProgressSpinnerModule,
    SharedModule,
    CoreModule,
    PrizePoollModule,
    HeaderBottomModule,
    FooterGooglyModule,
    FooterMenuMobileModule,
    EsportsCustomPaginationModule,
    RouterModule.forChild(routes),
    EsportsLoaderModule.setColor('#1d252d')],
})
export class MatchmakingModule { }
