import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { AppHtmlRoutes } from '../../app-routing.model';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { TranslateService } from '@ngx-translate/core';
import { ActivatedRoute, Router } from '@angular/router';
import {
  IPagination,
  EsportsLanguageService,
  EsportsTournamentService,
  EsportsHomeService,
  EsportsGameService,
  GlobalUtils,
  EsportsSeasonService,
} from 'esports';
import { environment } from '../../../environments/environment';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';

const API = environment.apiEndPoint;

@AutoUnsubscribe()
@Component({
  selector: 'app-matchmaking',
  templateUrl: './matchmaking.component.html',
  styleUrls: ['./matchmaking.component.scss'],
})
export class MatchmakingComponent implements OnInit {
  @ViewChild('mainContainer', { read: ElementRef })
  public scroll: ElementRef<any>;
  AppHtmlRoutes = AppHtmlRoutes;
  showLoader: boolean = false;
  currentTab = 'upcoming';
  page: IPagination;
  allTournamentList: any = [];
  seasonData: any;
  curentPage = 1;
  activeIndex = 0;
  showFilter = true;
  selectedSortBy: any = '';
  paginationData = {
    page: 1,
    limit: 8,
    sort: 'startDate',
  };
  participantPage: IPagination = {
    activePage: 1,
    totalItems: 1,
    maxSize: 10,
    itemsPerPage: 9,
  };
  checkboxFilter = [];
  selected_platform: any;
  faSearch = faSearch;
  isShown: boolean = false;
  params: any = {};
  gameDetail: any;
  paginationDetails: any;
  pageSizeOptions = environment.pageSizeOptions;
  individualTournaments = [];
  prizeMoneyTournaments = [];
  teamTournaments = [];
  hidePagination = false;
  sortby = [
    { value: '0', viewValue: 'Upcoming tournaments' },
    { value: '1', viewValue: 'Ongoing tournaments' },
    { value: '2', viewValue: 'Completed tournaments' },
    { value: '3', viewValue: 'All tournaments' },
  ];
  selected_sortby_tournament = this.sortby[3].value;
  listFilter = [
    {
      label: 'ADMIN.SEASON_MANAGEMENT.TABLE_HEADER.MOBILE',
      value: 'mobile',
      check: false,
    },
    {
      label: 'ADMIN.SEASON_MANAGEMENT.TABLE_HEADER.CONSOLE',
      value: 'console',
      check: false,
    },
    {
      label: 'ADMIN.SEASON_MANAGEMENT.TABLE_HEADER.PC',
      value: 'pc',
      check: false,
    },
  ];
  sortByList = [
    {
      label: 'ADMIN.SEASON_MANAGEMENT.TABLE_HEADER.PRIZE',
      value: 'Prize',
    },
    {
      label: 'ADMIN.SEASON_MANAGEMENT.TABLE_HEADER.TEAM',
      value: 'Team',
    },
  ];
  platformList: any = [];
  plist = [];
  plateformDetailsList:any;
  constructor(
    private languageService: EsportsLanguageService,
    private esportsSeasonService: EsportsSeasonService,
    private translateService: TranslateService,
    private gameService: EsportsGameService,
  ) { }

  ngOnInit(): void {
    this.gameService.fetchPlatforms(API).subscribe((res) => {
      this.platformList = res?.data;
    });
    this.languageService.language.subscribe((lang) =>
      this.translateService.use(lang || 'en')
    );
    this.getAllSeason();
  }

  clearFilter() {
    this.showFilter = !this.showFilter;
  }

  isSelected(isactive) {
    this.hidePagination = true;
    this.participantPage.activePage = 1;
    this.curentPage = 1;
  }

  platformFilters(option,index){
    this.hidePagination = true;
    this.participantPage.activePage = 1;
    this.plist = [];
    option.check = !option.check
    this.listFilter[index].check = option.check
    this.listFilter.forEach((listfilter,j) =>{
      this.platformList.forEach((platform,i) => {
        if(listfilter?.check){
          if((listfilter?.value.toLowerCase()!='console') &&
          (platform?.name.toLowerCase() == listfilter?.value)){
            this.plist.push(platform?._id)
          }
           if((listfilter?.value.toLowerCase()=='console')){
                if((platform?.name.toLowerCase() != 'pc' &&  platform?.name.toLowerCase() != 'mobile')){
                  this.plist.push(platform?._id)
                }
          }
         }
     });

     })

     this.plateformDetailsList = '';
     this.plist.forEach((plateFormDetails,i) => {
       if (i == 0) {
        this.plateformDetailsList = plateFormDetails;
       } else {
        this.plateformDetailsList = this.plateformDetailsList + '||' + plateFormDetails;
       }
     });
       this.getAllSeason();
}

  getAllSeason() {
    this.showLoader = true;
    let querydata: any = `isActive=true&fields=isFeatured,name,slug,endDate,startDate,image,duration&sort=-createdOn&limit=${this.participantPage.itemsPerPage}&page=${this.participantPage.activePage}`;
    let query;

    if(this.plateformDetailsList){
      query = `platform=${encodeURIComponent(
        this.plateformDetailsList 
     )}`+`&${querydata}`;

    }else{
      query = `${querydata}`;
    }
    this.esportsSeasonService
      .getSeasons(
        API,query
      )
      .subscribe((res) => {
        this.seasonData = res?.data?.data;
        this.showLoader = false;
        this.participantPage.itemsPerPage = this.seasonData?.limit;
        this.participantPage.totalItems = this.seasonData?.totalDocs;
        this.hidePagination = false;
      },
        (err: any) => {
          this.showLoader = false;
          this.hidePagination = false;
        }
      );
  }

  currentPage(page): void {
    this.participantPage.activePage = Number(page);
    this.curentPage = Number(page);
    this.getAllSeason();
  }

  scrollToTop() {
    this.scroll.nativeElement.scrollIntoView();
  }

  showPopup() {
    this.isShown = true;
  }
  hidePopup() {
    this.isShown = false;
  }
  onSortBy(item) {
    const tournament = [];
    this.selectedSortBy = item;
    this.seasonData.docs.forEach((t) => {
      if (item.value.toLowerCase() == t.participantType.toLowerCase()) {
        tournament.push(t);
      }
      if (item.value.toLowerCase() == 'prize' && t.isPrize) {
        tournament.push(t);
      }
    });
    this.seasonData.docs = tournament;
    // this.getAllSeason();
  }
  onSortByoption(event, option) {
    const index = this.listFilter.findIndex((item, i) => {
      return item.value == option.value;
    });
    this.listFilter[index] = {
      ...this.listFilter[index],
      check: event.checked,
    };
    this.selectedSortBy = option;

    // this.onSortByLogic(option);
    // this.getAllSeason();
    this.platformFilters(option,index);
  }

  onSortByLogic(option) {
    let t = [];
    this.seasonData.docs.forEach((tournament, i) => {
      this.listFilter.forEach((opValueFromList) => {
        if (opValueFromList.check) {
          if (
            opValueFromList.value.toLowerCase() ==
            tournament.platform.name.toLowerCase()
          ) {
            t.push(tournament);
          }
        }
      });
    });
    this.seasonData.docs = t;
  }
  removeSortBy(option) {
    const index = this.listFilter.findIndex((item) => {
      return item.value == option.value;
    });
    this.listFilter[index] = {
      ...this.listFilter[index],
      check: false,
    };
    // this.selectedSortBy = '';
    this.platformFilters(option,index);
  }
  clearFilterBtn() {
    this.listFilter = this.listFilter.map((item) => {
      return {
        ...item,
        check: false,
      };
    });
    location.reload();
    this.getAllSeason();
  }
  ngOnDestroy(): void { }
}
