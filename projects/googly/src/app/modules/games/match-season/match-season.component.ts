import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DatePipe } from '@angular/common';
import { Observable, Subscription } from 'rxjs';
import { formatDistanceToNowStrict } from 'date-fns';
import { Location, formatDate } from '@angular/common';
import {
  IPagination,
  EsportsChatService,
  EsportsToastService,
  EsportsHomeService,
  EsportsTournamentService,
  EsportsGameService,
  GlobalUtils,
} from 'esports';
import { TranslateService } from '@ngx-translate/core';
import {
  LadderPopupComponent,
  LadderPopupComponentData,
} from '../../../shared/popups/ladder-popup/ladder-popup.component';
import { MatDialog } from '@angular/material/dialog';
import { environment } from '../../../../environments/environment';
import { AppHtmlGetMatchRoutes } from '../../../app-routing.model';
const API = environment.apiEndPoint;

@Component({
  selector: 'app-match-season',
  templateUrl: './match-season.component.html',
  styleUrls: ['./match-season.component.scss'],
  providers: [DatePipe],
})
export class MatchSeasonComponent implements OnInit {
  AppHtmlGetMatchRoutes = AppHtmlGetMatchRoutes;
  // tableData: any[];
  //matchSeason: any;
  headerImage: string;
  endDays: Number;

  matchSeason = null;
  gameInfo = null;
  tableData = [];
  page = 1;
  seasonJoinStatus = null;
  matchFoundListener: Subscription;
  seasonId: string = null;
  showLoader: boolean = true;
  params: any = {};
  paginationData = {
    page: 1,
    limit: 50,
  };
  tournamentData;
  gameDetail: any;
  participantPage: IPagination = {
    activePage: 1,
    totalItems: 1,
    maxSize: 10,
    itemsPerPage: 9,
  };
  myData: any;
  constructor(
    private location: Location,
    private esportsTournamentService: EsportsTournamentService,
    private gameService: EsportsGameService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private datePipe: DatePipe,
    private toastService: EsportsToastService,
    private chatService: EsportsChatService,
    private matDialog: MatDialog,
    private translateService: TranslateService,
    private _location: Location,
    private homeService: EsportsHomeService
  ) { }

  ngOnInit(): void {
    this.getSeason();

    this.showLoader = true;
    if (this.activatedRoute.snapshot.params.id) {
      this.seasonId = this.activatedRoute.snapshot.params.id;
      this.fetchSeasonDetails();
      this.loadLadder();

    }

    this.matchSeason = {};
    this.gameInfo = {};
    this.seasonJoinStatus = {};

    if (this.activatedRoute.snapshot.queryParamMap.get('playnow')) {
      this.openPlayNowPopup();
      this.router
        .navigateByUrl('/games', { skipLocationChange: true })
        .then(() => {
          this.router.navigate(['/games/match-season/', this.seasonId]);
        });
    }

    this.getSeasonJoinStatus();
    this.getRecentMatches();
  }
  getSeason() {
    this.showLoader = true;
    this.params = {
      limit: this.participantPage.itemsPerPage,
      page: this.participantPage.activePage,
    };
    // this.params.limit = this.participantPage.itemsPerPage;
    // this.params.page = this.participantPage.activePage;
    this.homeService.getTournament(this.params).subscribe(
      (res: any) => {
        this.tournamentData = res?.data;
        this.showLoader = false;
        this.participantPage.itemsPerPage = this.tournamentData?.limit;
        this.participantPage.totalItems = this.tournamentData?.totalDocs;
        this.gameDetail = res?.data?.docs[0].gameDetail;
        this.gameService.createMatchMakingSubject.next({
          selectedGame: this.gameDetail,
          selectedPlatForm: res?.data?.docs[0].platform,
        });
      },
      (err: any) => {
        this.showLoader = false;
      }
    );
  }

  async loadLadder() {
    const parms = {
      id: this.seasonId,
      page: this.page,
    };
    // /6102497780f8e50008ebb587
    this.esportsTournamentService
      .getLaderStanding(API, parms)
      .subscribe((res) => {
        this.tableData = res.data.standing.docs;
        this.myData = res.data.your_rank;
        this.showLoader = false;
      });
  }

  pageChanged(page): void {
    this.paginationData.page = page;
    //this.getLogData();
  }

  // seasonDetails(seasonId) {
  //   this.gameService.getSeason(seasonId).subscribe((res) => {
  //     this.matchSeason = res.data.data;
  //     var currentDate = new Date();
  //     var convertedDate = this.datePipe.transform(this.matchSeason.endDate, 'yyyy-MM-dd');
  //     var Time = new Date(convertedDate).getTime() - currentDate.getTime();
  //     var Days = Time / (1000 * 3600 * 24); //Diference in Days
  //     this.endDays = Math.round(Days);
  //   })
  // }

  fetchSeasonDetails = async () => {
    this.gameService.fetchSeasonDetails(API, this.seasonId).subscribe((res) => {
      const data = res['data']['data'];
      let text;
      const endDate = new Date(data.endDate);
      const startDate = new Date(data.startDate);
      const currentDate = new Date();
      if (startDate > currentDate) {
        text = `Starts ${formatDistanceToNowStrict(startDate, {
          addSuffix: true,
          unit: 'day',
        })}`;
      } else if (endDate > currentDate) {
        text = `Ends ${formatDistanceToNowStrict(endDate, {
          addSuffix: true,
          unit: 'day',
        })}`;
      } else {
        text = 'Season ended';
      }
      //  this.matchSeason = data;
      Object.assign(this.matchSeason, {
        title: data?.name,
        text: text,
        logo: data?.game.logo,
        image: data?.image,
        id: data?._id,
        participantType: data?.participantType,
        slug: data?.slug,
        description: data?.description,
      });

      Object.assign(this.gameInfo, {
        image: data?.game?.image,
        icon: data?.game?.icon,
        details: {
          platform: data?.platform?.map((ele) => ele?.name).join(', '),
          duration: `${data?.duration} days`,
          endDate: formatDate(endDate, 'dd MMM yyyy', 'en'),
          totalPlayers: data?.participantCount,
        },
      });
      this.showLoader = false;
    });
    
  };


  getRecentMatches() {
    const params = {
      query: JSON.stringify({
        seasonId: this.seasonId,
        limit: 5,
        page: 1,
      }),
    };
    this.gameService.fetchRecentSeasonMatches(API, params).subscribe(
      (res) => {
        this.showLoader = false;
        this.gameInfo.matches = res.data.docs;
      },
      (err) => {
        this.showLoader = false;
      }
    );
  }

  goBack() {
    this.location.back();
  }

  async getSeasonJoinStatus() {
    this.gameService
      .checkSeasonJoinStatus(API, this.seasonId)
      .subscribe((res) => {
        this.seasonJoinStatus = res.data;
      });
  }

  playNow() {
    if (this.seasonJoinStatus?.isRegistered) {
      this.openPlayNowPopup();
    } else {
      if (this.matchSeason.participantType === 'team') {
        this.router.navigate(
          [
            `/team-registration/${this.matchSeason?.slug}-${this.matchSeason?.id}`,
          ],
          {
            queryParams: { isSeason: true },
          }
        );
      } else {
        this.router.navigate([
          `tournament/${this.matchSeason?.slug}-${this.matchSeason?.id}/join`,
        ]);
      }
    }
  }

  openPlayNowPopup() {
    this.chatService.connectSocket();
    let popupData: LadderPopupComponentData = {
      isShowLoading: true,
      payload: { leftTime: 180 },
    };

    const loadDialogRef = this.matDialog.open(LadderPopupComponent, {
      data: popupData,
      disableClose: true,
    });

    this.chatService.findSeasonMatch({
      seasonId: this.seasonId,
    });

    const successObservable = new Observable((subscriber) => {
      this.chatService.listenSeasonMatchFound(subscriber);
    });

    let matchFoundSubscription = successObservable.subscribe((data) => {
      if (data) {
        loadDialogRef.close(true);
        this.toastService.showSuccess(
          this.translateService.instant('Match Found')
        );
        this.router.navigate(['get-match/game-lobby'], {
          queryParams: { matchId: data['data']['newMatch']['_id'] },
        });
      }
    });

    const errorObservable = new Observable((subscriber) => {
      this.chatService.listenFindSeasonMatchError(subscriber);
    });

    let findSeasonMatchErrorSubscription = errorObservable.subscribe(
      (error) => {
        loadDialogRef.close(true);
        if (error['errorCode']) {
          this.toastService.showError(
            'You already have 1 active match in this tournament'
          );
        } else {
          this.toastService.showError(
            this.translateService.instant('LADDER.FIND_MATCH_ERROR')
          );
        }
      }
    );

    loadDialogRef.afterClosed().subscribe((loadCloseData) => {
      this.chatService.removeFromQueue('');
      matchFoundSubscription.unsubscribe();
      findSeasonMatchErrorSubscription.unsubscribe();
      if (loadCloseData == 'timeOut') {
        const notFoundPopupRef = this.matDialog.open(LadderPopupComponent, {
          data: { isShowNotFoundSeason: true },
        });
        notFoundPopupRef.afterClosed().subscribe((action) => {
          if (action == 'searchAgain') {
            this.openPlayNowPopup();
          }
        });
      }
    });
  }
  findAMatch() {
    this.router.navigateByUrl('/get-match/season/select-team');
  }
  backClicked() {
    this._location.back();
  }
  checkNonApi() {
    return this.router.url === '/get-match/season/detail-non-api';
  }
}
