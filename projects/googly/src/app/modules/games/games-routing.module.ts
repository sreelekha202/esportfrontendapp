import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { GamesComponent } from './games.component';
import { MatchSeasonComponent } from './match-season/match-season.component';

const routes: Routes = [
  {
    path: '',
    component: GamesComponent,
  },
  { path: 'match-season/:id', component: MatchSeasonComponent },
  { path: ':id', component: MatchSeasonComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GamesRoutingModule {}
