import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EsportsCustomPaginationModule, EsportsLoaderModule } from 'esports';
import { FooterMenuMobileModule } from '../../shared/components/footer-menu-mobile/footer-menu-mobile.module';
import { FooterGooglyModule } from '../../shared/components/footer-googly/footer-googly.module';
import { HeaderBottomModule } from '../../shared/components/header-bottom/header-bottom.module';
import { SharedModule } from '../../shared/modules/shared.module';
import { GamesRoutingModule } from './games-routing.module';
import { GamesComponent } from './games.component';
import { MatchSeasonComponent } from './match-season/match-season.component';
import { MatchmakerDialogComponent } from './matchmaker-dialog/matchmaker-dialog.component';
@NgModule({
  declarations: [
    GamesComponent,
    MatchmakerDialogComponent,
    MatchSeasonComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    GamesRoutingModule,
    HeaderBottomModule,
    FooterGooglyModule,
    FooterMenuMobileModule,
    EsportsLoaderModule,
    EsportsCustomPaginationModule
  ],
})
export class GamesModule { }
