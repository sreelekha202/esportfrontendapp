import { Optional, Component, OnInit, OnDestroy } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import {
  IUser,
  EsportsUserService,
  EsportsToastService,
  EsportsLanguageService,
  EsportsAuthServices,
  EsportsUserPreferenceService
} from 'esports';
@Component({
  selector: 'app-matchmaker-dialog',
  templateUrl: './matchmaker-dialog.component.html',
  styleUrls: ['./matchmaker-dialog.component.scss'],
})
export class MatchmakerDialogComponent implements OnInit, OnDestroy {
  profile = false;
  progressValue = 0;
  progressBar = false;
  continue = true;
  notfound = false;
  matchMakingBanner = 'assets/images/GamesCards/matchmaking.png';

  matchMakerProfile = {
    id: null,
    image: 'assets/images/GamesCards/matchmaking.png',
    name: 'Ikhwan Mohammed',
    location: 'Riyadh, Saudi Arabia',
    matchName: 'FIFA21',
  };

  gameId: any = null;
  gameName: any = null;
  gamePlatforms: any;
  user: IUser;
  showLoader = true;
  isPlatform = true;
  selectedPlatform: any = null;

  userSubscription: Subscription;

  constructor(
    public dialogRef: MatDialogRef<MatchmakerDialogComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) public data: any,
    private userService: EsportsUserService,
    private authService: EsportsAuthServices,
    private router: Router,
    public toastService: EsportsToastService,
    private userpreferenceService: EsportsUserPreferenceService,
    public language: EsportsLanguageService,
    public translateService: TranslateService
  ) {}

  ngOnInit(): void {
    this.language.language.subscribe((lang) => {
      this.translateService.use(lang || 'en');
    });
    this.gameId = this.data._id;
    this.gameName = this.data.name;
    this.gamePlatforms = [];
    if (this.data.platform.length == 0) this.isPlatform = false;
    this.getCurrentUserDetails();
  }

  ngOnDestroy() {
    this.userSubscription?.unsubscribe();
  }

  getCurrentUserDetails() {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.user = data;
      } else {
        this.authService.redirectUrl = '/games';
        this.toastService.showError(
          this.translateService.instant('TOURNAMENT.TOURNAMENT.PLEASE_LOGIN')
        );
        // Navigate to the login page with extras
        this.router.navigate(['/user/phone-login']);
        this.closeDialog();
      }
    });
  }

  matchMaking() {
    if (this.user) {
      let counter = 0;
      const timer = setInterval(() => {
        this.progressValue = counter;
        if (counter < 90) {
          counter += 10;
        }
      }, 100);
      // this.userpreferenceService
      //   .matchMaking(this.gameId, this.selectedPlatform)
      //   .subscribe(
      //     (res: any) => {
      //       if (res.data != null) {
      //         this.matchMakerProfile = {
      //           id: res.data._id,
      //           image:
      //             res.data.profilePicture == ''
      //               ? './assets/images/Leaderboard/leader-board-card-image.png'
      //               : res.data.profilePicture,
      //           name: res.data.fullName,
      //           location: '',
      //           matchName: this.gameName,
      //         };
      //         if (res.data.state != null)
      //           this.matchMakerProfile.location += res.data.state;
      //         if (res.data.country != null) {
      //           if (res.data.state != null) {
      //             this.matchMakerProfile.location += ', ' + res.data.country;
      //           } else {
      //             this.matchMakerProfile.location += res.data.country;
      //           }
      //         }
      //         if (timer) {
      //           clearInterval(timer);
      //         }
      //         this.progressValue = 100;
      //         this.progressBar = false;
      //         this.profile = true;
      //       } else {
      //         if (timer) {
      //           clearInterval(timer);
      //         }
      //         this.progressBar = false;
      //         this.notfound = true;
      //       }
      //     },
      //     (err) => {
      //       this.showLoader = false;
      //     }
      //   );
      // call follow API
    }
  }

  closeDialog(data = null) {
    this.dialogRef.close(data);
  }

  selectPlatform(id) {
    this.selectedPlatform = id;
  }

  onContinue(selectedPlatform = null) {
    this.selectedPlatform = selectedPlatform;
    this.progressBar = true;
    this.continue = false;
    this.matchMaking();
  }
}
