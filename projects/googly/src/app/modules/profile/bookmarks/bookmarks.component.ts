import { environment } from '../../../../environments/environment';
import { IUser, EsportsUserService, EsportsUserPreferenceService } from 'esports';
const API = environment.apiEndPoint;
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';

@AutoUnsubscribe()
@Component({
  selector: 'app-bookmarks',
  templateUrl: './bookmarks.component.html',
  styleUrls: ['./bookmarks.component.scss'],
})
export class BookmarksComponent implements OnInit,OnDestroy {
  articles = [];
  videos = [];
  active = 1;
  userSubscription: Subscription;
  currentUser: IUser;
  showLoader: boolean = true;
  articleItems = [];

  constructor(
    private userService: EsportsUserService,
    private userPreferenceService: EsportsUserPreferenceService,
  ) { }
  ngOnDestroy(): void {}

  ngOnInit(): void {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.currentUser = data;
        this.getBookmarks();
      }
    });
  }


 getBookmarks = async () => {
  try {
    const filter = JSON.stringify({ userId: this.currentUser?._id });
    const projection = 'articleBookmarks,videoLibrary';
    const userPreference = await this.userPreferenceService
      .getPreferences(filter, projection)
      .toPromise();
            this.videos = userPreference?.data[0]?.videoLibrary;
            this.articles = userPreference?.data[0]?.articleBookmarks;
            this.showLoader = false;
  } catch (error) {
    this.showLoader = false;
  }
};


}
