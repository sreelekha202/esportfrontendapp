import { Component, OnInit, Input } from '@angular/core';
import { TranslateService} from '@ngx-translate/core';
import { MatDialog } from '@angular/material/dialog';
import { InfoPopupComponent, InfoPopupComponentData, InfoPopupComponentType } from '../../../../shared/popups/info-popup/info-popup.component';
import {  EsportsToastService,
  EsportsUserPreferenceService,
  EsportsUserService,
  IUser } from 'esports';
import { Subscription } from 'rxjs';
import { AppHtmlRoutes } from '../../../../app-routing.model';

interface BookmarkCard {
  image: string;
  thumbnailUrl:any,
  title: any;
  shortDescription: any;
  description:any,
  createdDate: any;
  author?: string;
  views?: string;
  _id:string;
  game:string
  slug:any
}
@Component({
  selector: 'app-bookmarks-cardvideos',
  templateUrl: './bookmarks-cardvideos.component.html',
  styleUrls: ['./bookmarks-cardvideos.component.scss']
})
export class BookmarksCardvideosComponent implements OnInit {
  @Input() data: BookmarkCard;
  @Input() isVideo: boolean = false;
  showLoader: boolean = true;
  currentUser: IUser;
  articles = [];
  videos = [];
  userSubscription: Subscription;
  article: any;
  AppHtmlRoutes=AppHtmlRoutes;
  constructor(
    private translateService: TranslateService,
    public matDialog: MatDialog,
    private toastService: EsportsToastService,
    private userPreferenceService: EsportsUserPreferenceService,
    private userService: EsportsUserService,)
    {

    }

    ngOnInit(): void {
      this.userSubscription = this.userService.currentUser.subscribe((data) => {
        if (data) { this.currentUser = data; this.getBookmarks(); }
      });
    }

    openSocialShareComponent() {
      const data: InfoPopupComponentData = {
        title: this.translateService.instant('ARTICLE_POST.SHARE'),
        text: String(this.data?.title?.english),
        type: InfoPopupComponentType.socialSharing,
        cancelBtnText: 'Close',
      };
      this.matDialog.open(InfoPopupComponent, {
        data,
      });
    }

    removeBookmark = async (_id) => {
      try {
        const queryParam = `articleId=${_id}`;
        const bookmark = await this.userPreferenceService
          .removeBookmark(queryParam)
          .toPromise();
        this.toastService.showSuccess(bookmark?.message);
        this.getBookmarks();
      } catch (error) {
        this.toastService.showError(error?.error?.message || error?.message);
      }
    };

   getBookmarks = async () => {
  try {
    const filter = JSON.stringify({ userId: this.currentUser?._id });
    const projection = 'articleBookmarks,videoLibrary';
    const userPreference = await this.userPreferenceService
      .getPreferences(filter, projection)
      .toPromise();
      this.videos = userPreference?.data[0]?.videoLibrary;
            this.articles = userPreference?.data[0]?.articleBookmarks;
            this.showLoader = false;
  } catch (error) {
    this.showLoader = false;
  }
    };

}
