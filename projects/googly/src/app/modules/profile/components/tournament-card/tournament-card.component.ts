import { Component, OnInit, Input } from '@angular/core';
import { GlobalUtils } from 'esports';
import { AppHtmlManageTournamentRoutes } from '../../../../app-routing.model';
@Component({
  selector: 'app-tournament-card',
  templateUrl: './tournament-card.component.html',
  styleUrls: ['./tournament-card.component.scss'],
})
export class TournamentCardComponent implements OnInit {
  AppHtmlManageTournamentRoutes = AppHtmlManageTournamentRoutes;
  @Input() data: any;
  @Input() isManage: boolean = false;

  constructor() {}
  ngOnInit(): void {
  }
  function(data) {
    if (GlobalUtils.isBrowser()) {
      localStorage.setItem('tournamentDetails', JSON.stringify(data));
    }
  }
}
