import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-bookmarks-videos',
  templateUrl: './bookmarks-videos.component.html',
  styleUrls: ['./bookmarks-videos.component.scss']
})
export class BookmarksVideosComponent implements OnInit {
  @Input() data = [];
  @Input() isVideo: boolean = false;
  itemsPerPage = [3, 6, 10];
  slicesItems = [];
  constructor() { }

  ngOnInit(): void {
    this.slicesItems = this.data.slice(0, this.itemsPerPage[0]);
  }
  
  onPageChange(event): void {
    const startIndex = event.pageIndex * event.pageSize;
    let endIndex = startIndex + event.pageSize;
    if (endIndex > this.data.length) {
      endIndex = this.data.length;
    }
    this.slicesItems = this.data.slice(startIndex, endIndex);
  }


}
