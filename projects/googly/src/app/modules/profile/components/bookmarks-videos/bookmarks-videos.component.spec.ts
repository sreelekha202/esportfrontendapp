import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BookmarksVideosComponent } from './bookmarks-videos.component';

describe('BookmarksVideosComponent', () => {
  let component: BookmarksVideosComponent;
  let fixture: ComponentFixture<BookmarksVideosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BookmarksVideosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BookmarksVideosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
