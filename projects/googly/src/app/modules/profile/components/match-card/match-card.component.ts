import { Component, OnInit, Input, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';
import { AppHtmlRoutes } from '../../../../app-routing.model';
import { EsportsTournamentService, GlobalUtils } from 'esports';
import { environment } from '../../../../../environments/environment';
const API = environment.apiEndPoint;
@Component({
  selector: 'app-match-card',
  templateUrl: './match-card.component.html',
  styleUrls: ['./match-card.component.scss'],
})
export class MatchCardComponent implements OnInit {
  @Input() data: any;
  @Input() isEditable: boolean = false;
  @Input() index: number;
  AppHtmlRoutes = AppHtmlRoutes;
  show = false;
  showIssue = false;
  constructor(
    private router: Router,
    private tournamentService: EsportsTournamentService
  ) {}

  ngOnInit(): void {
    this.data = this.data;
  }
  showPopupIssue($event) {
    this.showIssue = true;
  }
  hidePopupIssue($event) {
    this.showIssue = false;
  }
  closePopup(event) {

  }
  genrateReport() {
    if (GlobalUtils.isBrowser() && this.data?.tournament && this.data?.matchId) {
      localStorage.setItem('t_report', JSON.stringify(this.data));
      this.router.navigate([`/report-issue`],
       {queryParams: { matchId:this.data?.matchId,tournamentId:this.data?.tournament }
      });
    }
  }

  // async reportScore(data) {
  //   if (!(data.match && data?.match?.seasonId)) {
  //     const participantData =
  //       await this.tournamentService.fetchParticipantRegistrationStatus(API,
  //         data.tournaments._id
  //       );
  //     data.participantId = participantData?.data?.participantId;
  //   }

  //   localStorage.setItem('t_report', JSON.stringify(data));
  //   this.router.navigate([`/report-score`]);
  // }

  async reportScore(data) {
    if (!(data.match && data?.match?.seasonId)) {
      const participantData =
        await this.tournamentService.fetchParticipantRegistrationStatus(API,
          data.tournaments._id
        );
      data.participantId = participantData?.data?.participantId;
    }

    /*redirection has been updated*/;
    this.router.navigate(['/report-score',data.matchId]);
  }

  exitFromScoreCard(data) {}
  // viewMatch() {
  //   if (this.data?.tournamentSlug) {
  //     this.router.navigate([
  //       `${AppHtmlRoutes.tournament}/${this.data?.tournamentSlug}`,
  //     ]);
  //   }
  // }

  viewMatch(_matchId) {
    this.router.navigate([`/get-match/game-lobby`], {
      queryParams: { matchId: _matchId },
    });
  }
}
