import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  OnDestroy,
} from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { MatDialog } from '@angular/material/dialog';
import {
  InfoPopupComponent,
  InfoPopupComponentData,
  InfoPopupComponentType,
} from '../../../../shared/popups/info-popup/info-popup.component';
import {
  IArticle,
  IUser,
  EsportsToastService,
  EsportsUserService,
  EsportsUserPreferenceService,
} from 'esports';
import { Subscription } from 'rxjs';
import { environment } from '../../../../../environments/environment';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';
import { AppHtmlRoutes } from '../../../../app-routing.model';

const API = environment.apiEndPoint;
const TOKEN = environment.currentToken;

interface BookmarkCard {
  image: string;
  thumbnailUrl: any;
  title: any;
  shortDescription: any;
  description: any;
  createdDate: any;
  author?: string;
  views?: string;
  _id: string;
  game: string;
  fullText: string;
  slug: any;
}

@AutoUnsubscribe()
@Component({
  selector: 'app-bookmark-card',
  templateUrl: './bookmark-card.component.html',
  styleUrls: ['./bookmark-card.component.scss'],
})
export class BookmarkCardComponent implements OnInit, OnDestroy {
  @Input() data: BookmarkCard;
  @Input() isVideo: boolean = false;
  @Input() creatorId;
  @Output() refresh = new EventEmitter();
  showLoader: boolean = true;
  currentUser: IUser;
  articles = [];
  videos = [];
  userSubscription: Subscription;
  article: IArticle;
  currentUserId: any;
  allowDelete: boolean = false;
  AppHtmlRoutes = AppHtmlRoutes;
  constructor(
    private translateService: TranslateService,
    public matDialog: MatDialog,
    private toastService: EsportsToastService,
    private userPreferenceService: EsportsUserPreferenceService,
    private userService: EsportsUserService
  ) {}
  ngOnInit(): void {
    this.getCurrentUserDetails();
  }

  getCurrentUserDetails() {
    this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.currentUser = data;
        this.currentUserId = data?._id;
        if (data?.accountType == 'admin') {
          this.allowDelete = true;
        }
        if (data?._id == this.creatorId) {
          this.allowDelete = true;
        }
      }
    });
  }

  openSocialShareComponent() {
    const data: InfoPopupComponentData = {
      title: this.translateService.instant('ARTICLE_POST.SHARE'),
      text: String(this.data?.title?.english),
      type: InfoPopupComponentType.socialSharing,
      cancelBtnText: 'Close',
    };
    this.matDialog.open(InfoPopupComponent, {
      data,
    });
  }

  removeBookmark = async (_id) => {
    try {
      const queryParam = `articleId=${_id}`;
      const bookmark = await this.userPreferenceService
        .removeBookmark(queryParam)
        .toPromise();
      this.refresh.emit(_id);
      this.toastService.showSuccess(bookmark?.message);
      this.getBookmarks();
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  getBookmarks = async () => {
    try {
      const filter = JSON.stringify({ userId: this.currentUser?._id });
      const projection = 'articleBookmarks,videoLibrary';
      const userPreference = await this.userPreferenceService
        .getPreferences(filter, projection)
        .toPromise();
      this.videos = userPreference?.data[0]?.videoLibrary;
      this.articles = userPreference?.data[0]?.articleBookmarks;
      this.showLoader = false;
    } catch (error) {
      this.showLoader = false;
    }
  };

  ngOnDestroy(): void {}
}
