import { Component, OnInit } from '@angular/core';
import { EsportsTournamentService, IPagination } from 'esports';
import { environment } from '../../../../environments/environment';
import { AppHtmlProfileRoutes } from '../../../app-routing.model';
import { Router } from '@angular/router';
@Component({
  selector: 'app-tournaments-created',
  templateUrl: './tournaments-created.component.html',
  styleUrls: ['./tournaments-created.component.scss'],
})
export class TournamentsCreatedComponent implements OnInit {
  AppHtmlProfileRoutes = AppHtmlProfileRoutes;
  viewStatsFlag: any = null;
  tournaments:any;
  showLoader: boolean = true;
  statisticsData: any;
  userId: any;
  active = 1;
  nextId = 1;
  paginationDetails: any;
  pageSizeOptions = environment.pageSizeOptions;
  paginationData = {
    page: 1,
    limit: 10,
    sort: 'startDate',
  };
  participantPage: IPagination = {
    activePage: 1,
    totalItems: 1,
    maxSize: 10,
    itemsPerPage: 10,
  };


  constructor(private esportsTournamentService: EsportsTournamentService, public router: Router,) {}

  ngOnInit(): void {
    this.getTournaments();
     /* check for view stats here based on URL */
     this.viewStatsFlag = false;
     if (this.router.url.includes('view-stats')) {
       this.viewStatsFlag = true;
       this.userId = this.router.url.split('/').reverse()[0];
       this.getStatData();
     }
  }

  navChanges = (e) => {
    this.nextId = e.nextId;
    this.getTournaments();
  };

  getTournaments = async () => {
    this.showLoader = true;
    const params: any = {
      limit: this.participantPage.itemsPerPage,
      page: this.participantPage.activePage,
      status: this.nextId,
    };
    try {
      const tournament = await this.esportsTournamentService.fetchMyTournament(
        params
      );
      if (tournament) {
        this.tournaments = tournament?.data;
        this.participantPage.itemsPerPage = this.tournaments?.limit;
        this.participantPage.totalItems = this.tournaments?.totalDocs;
        // this.paginationData.page = this.paginationDetails.page;
        this.showLoader = false;
      }
    } catch (error) {}
  };

  pageChanged(page): void {
    if (this.paginationDetails?.totalDocs > 0) {
      this.paginationData.page = page.pageIndex + 1;
      this.paginationData.limit = page.pageSize;
      this.navChanges({ nextId: this.nextId });
    }
  }
  currentPage(page): void {
    this.participantPage.activePage = page;
    this.navChanges({ nextId: this.nextId });
     this.getTournaments();
  }
  getStatData() {
    // this.statisticsData = this.gameService.createMatchMaking.subscribe(
    //   (data: any) => {
    //     if (data) {
    //       this.statisticsDetails = data.statisticsDetails;
    //       if (this.router.url.includes('my-stats')) {
    //         this.viewStatsFlag = false;
    //       }
    //     }
    //   }
    // );
  }
}
