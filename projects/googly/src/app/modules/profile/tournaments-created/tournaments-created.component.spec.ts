import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TournamentsCreatedComponent } from './tournaments-created.component';

describe('TournamentsCreatedComponent', () => {
  let component: TournamentsCreatedComponent;
  let fixture: ComponentFixture<TournamentsCreatedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TournamentsCreatedComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TournamentsCreatedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
