import { Component, OnInit } from '@angular/core';
import { EsportsProfileService } from 'esports';

@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.scss'],
})
export class TransactionsComponent implements OnInit {
  purchaseHistory: any;
  rewardseHistory: any;
  active = 1;
  nextId: number = 1;
  pagination: any;
  paginationParams: any;
  showLoader: boolean = true;
  constructor(private profileService: EsportsProfileService) { }

  ngOnInit(): void {
    this.getPurchaseHistory();
  }
  getPurchaseHistory() {
    this.showLoader = true;
    const params: any = {};
    this.paginationParams ? params.pagination = this.paginationParams : ''
    this.profileService.getPurchaseHistory(params).subscribe((res) => {
      this.pagination = res.data;
      this.purchaseHistory = (res.data && res.data.docs.length > 0) ? res.data.docs :'';
      this.showLoader = false;
    }, (err) => { this.showLoader = false; })
  }

  getRewardseHistory() {
    this.showLoader = true;
    const params: any = {};
    this.paginationParams ? params.pagination = this.paginationParams : ''
    this.profileService.getRewardseHistory(params).subscribe((res) => {
      this.pagination = res.data;
      this.rewardseHistory = (res.data && res.data.docs.length > 0) ? res.data.docs : '';
      this.showLoader = false;
    }, (err) => { this.showLoader = false; })
  }

  navChanges = (e) => {
    this.nextId = e.nextId;
    switch (this.nextId) {
      case 1: this.getPurchaseHistory(); break;
      case 2: this.getRewardseHistory(); break;
    }
  }

  onPageChanges(event) { this.getPurchaseHistory(); }
}
