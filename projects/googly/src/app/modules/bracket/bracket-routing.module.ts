import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { BracketComponent } from "./bracket.component";
import { BracketPreviewComponent } from "./bracket-preview/bracket-preview.component";
import { UpsertBracketComponent } from "./upsert-bracket/upsert-bracket.component";

const routes: Routes = [
  { path: "", component: BracketComponent },
  { path: "create", component: UpsertBracketComponent },
  { path: "edit/:id", component: UpsertBracketComponent },
  { path: "preview", component: BracketPreviewComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BracketRoutingModule {}
