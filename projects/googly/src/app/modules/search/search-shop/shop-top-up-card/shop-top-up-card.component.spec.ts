import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShopTopUpCardComponent } from './shop-top-up-card.component';

describe('ShopTopUpCardComponent', () => {
  let component: ShopTopUpCardComponent;
  let fixture: ComponentFixture<ShopTopUpCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShopTopUpCardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShopTopUpCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
