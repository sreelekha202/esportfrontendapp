import { Component,Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-shop-top-up-card',
  templateUrl: './shop-top-up-card.component.html',
  styleUrls: ['./shop-top-up-card.component.scss']
})
export class ShopTopUpCardComponent implements OnInit {
  router : string;
  constructor() { }

  @Input() shop: any = {};

  ngOnInit(): void { 
    this.router = `voucher/${this.shop?._id}`
  }

}
