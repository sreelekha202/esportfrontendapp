import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IPagination, EsportsHomeService } from 'esports';
import { Subscription } from 'rxjs';
/* import { TopUpItemComponentData } from "../../../core/top-up-item/top-up-item.component";
 */
@Component({
  selector: 'app-search-shop',
  templateUrl: './search-shop.component.html',
  styleUrls: ['./search-shop.component.scss'],
})
export class SearchShopComponent implements OnInit {
  page: IPagination;
  paginationData = {
    page: 1,
    limit: 8,
    sort: '-startDate',
  };
  text;
  isShopFlag = false;
  userSubscription: Subscription;
  currentUser: any;

  constructor(    public homeService: EsportsHomeService,
    public activatedRoute: ActivatedRoute,
    private router: Router) {}

  ngOnInit(): void {
    this.activatedRoute.queryParamMap.subscribe((e) => {
      if (e.get('text')) {
        this.text = e.get('text');
      }
    });
    this.getSearchShop();
  }

  getSearchShop() {
    this.homeService.searchedShop.subscribe((res: any) => {
      if (res?.docs?.length > 0) this.isShopFlag = true;
      this.page = {
        totalItems: res?.totalDocs,
        itemsPerPage: res?.limit,
        maxSize: 5,
      };
    });
  }
  redirectToupOrVoucher(category: any, id) {
      if (category == 'VOUCHER')
        this.router.navigateByUrl('/shop/voucher/' + id);
      if (category == 'TOPUP') this.router.navigateByUrl('/shop/topup/' + id);
  }
}
