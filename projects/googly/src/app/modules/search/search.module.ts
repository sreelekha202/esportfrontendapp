import { NgModule } from '@angular/core';

import { CoreModule } from '../../core/core.module';
import { SearchRoutingModule } from './search-routing.module';
import { SearchAllComponent } from './search-all/search-all.component';
import { SearchArticleComponent } from './search-article/search-article.component';
import { SearchComponent } from './search.component';
import { SearchTournamentsComponent } from './search-tournaments/search-tournaments.component';
import { SearchVideoComponent } from './search-video/search-video.component';
import { SearchShopComponent } from './search-shop/search-shop.component';
import { SharedModule } from '../../shared/modules/shared.module';
import { FooterGooglyModule } from '../../shared/components/footer-googly/footer-googly.module';
import { SearchSeasonsComponent } from './search-seasons/search-seasons.component';
import { ShopTopUpCardComponent } from './search-shop/shop-top-up-card/shop-top-up-card.component';
import { SeasonslistComponent } from './search-seasons/seasonslist/seasonslist.component';

@NgModule({
  declarations: [
    SearchAllComponent,
    SearchArticleComponent,
    SearchComponent,
    SearchTournamentsComponent,
    SearchVideoComponent,
    SearchShopComponent,
    SearchSeasonsComponent,
    ShopTopUpCardComponent,
    SeasonslistComponent
  ],
  imports: [CoreModule, SharedModule, SearchRoutingModule,FooterGooglyModule],
})
export class SearchModule {}
