import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchSeasonsComponent } from './search-seasons.component';

describe('SearchSeasonsComponent', () => {
  let component: SearchSeasonsComponent;
  let fixture: ComponentFixture<SearchSeasonsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SearchSeasonsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchSeasonsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
