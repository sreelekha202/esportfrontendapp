import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IPagination,EsportsHomeService, EsportsSeasonService } from 'esports';
import { environment } from '../../../../environments/environment';
import { AppHtmlRoutes } from '../../../app-routing.model';
const API = environment.apiEndPoint;

@Component({
  selector: 'app-search-seasons',
  templateUrl: './search-seasons.component.html',
  styleUrls: ['./search-seasons.component.scss']
})
export class SearchSeasonsComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;
  seasonData: any;
  showLoader: boolean = false;
  isSeasonFlag = false;
  page: IPagination;
  paginationData = {
    page: 1,
    limit: 50,
    sort: '-startDate',
  };
  text;
  constructor(private esportsSeasonService: EsportsSeasonService,
    public homeService: EsportsHomeService,
    public activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.activatedRoute.queryParamMap.subscribe((e) => {
      if (e.get('text')) {
        this.text = e.get('text');
      }
    });
    this.getSearchSeason();
  }

  getSearchSeason() {
    this.homeService.searchedSeason.subscribe((res: any) => {
      if (res?.data?.data?.docs?.length > 0) this.isSeasonFlag = true;
      this.page = {
        totalItems: res?.totalDocs,
        itemsPerPage: res?.limit,
        maxSize: 5,
      };
    });
  }



  getAllSeason() {
    this.showLoader = true;
    this.esportsSeasonService
      .getSeasons(
        API,
        'isActive=true&fields=isFeatured,name,slug,endDate,startDate,image,duration&sort=-createdOn&page=1&limit=8'
      )
      .subscribe((res) => {
        this.isSeasonFlag = true;
        this.seasonData = res?.data?.data;
        // console.log("data==>",this.seasonData)
        this.showLoader = false;
      },
        (err: any) => {
          this.showLoader = false;
        }
      );
  }

}
