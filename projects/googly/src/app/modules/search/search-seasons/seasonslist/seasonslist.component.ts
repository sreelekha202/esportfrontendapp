import { Component,Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-seasonslist',
  templateUrl: './seasonslist.component.html',
  styleUrls: ['./seasonslist.component.scss']
})
export class SeasonslistComponent implements OnInit {
  @Input() item: any = {};
  constructor() { }

  ngOnInit(): void {
  }

}
