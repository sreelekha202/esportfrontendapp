import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SeasonslistComponent } from './seasonslist.component';

describe('SeasonslistComponent', () => {
  let component: SeasonslistComponent;
  let fixture: ComponentFixture<SeasonslistComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SeasonslistComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SeasonslistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
