import { Component, OnInit, Input } from '@angular/core';
import { AppHtmlRoutes } from '../../../../../app-routing.model';

interface MatchCard {
  image: string;
  match: number;
  set: number;
  title: string;
  gameName: string;
  startDate: string;
  slug:string;
  date: {
    when: string;
    report: string;
  };
}

@Component({
  selector: 'app-match-card',
  templateUrl: './match-card.component.html',
  styleUrls: ['./match-card.component.scss'],
})
export class MatchCardComponent implements OnInit {
  @Input() data: MatchCard;
  @Input() isEditable: boolean = false;
  AppHtmlRoutes = AppHtmlRoutes;
  constructor() {}

  ngOnInit(): void {}
}
