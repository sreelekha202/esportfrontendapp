import { Component, OnInit, Input } from '@angular/core';

interface TournamentCarad {
  image: string;
  title: string;
  startDate: string;
  gameName: string;
  slug:string;
}

@Component({
  selector: 'app-tournament-card',
  templateUrl: './tournament-card.component.html',
  styleUrls: ['./tournament-card.component.scss'],
})
export class TournamentCardComponent implements OnInit {
  @Input() data: TournamentCarad;
  @Input() isManage: boolean = false;
  @Input()  index:number;
  constructor() {}

  ngOnInit(): void {}
}
