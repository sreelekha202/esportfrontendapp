import { RouterBackModule } from './../../../shared/directives/router-back.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ViewTeamComponent } from './view-team.component';
import { CoreModule } from '../../../core/core.module';
import { Routes, RouterModule } from '@angular/router';
import { MatchCardComponent } from './components/match-card/match-card.component';
import { TournamentCardComponent } from './components/tournament-card/tournament-card.component';
import { SharedModule } from '../../../shared/modules/shared.module';
import { FooterGooglyModule } from '../../../shared/components/footer-googly/footer-googly.module';
import { EsportsCustomPaginationModule } from 'esports';

export const routes: Routes = [
  {
    path: '',
    component: ViewTeamComponent
  }
]
@NgModule({
  declarations: [
    ViewTeamComponent, MatchCardComponent, TournamentCardComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    CoreModule,
    RouterBackModule,
    RouterModule.forChild(routes),
    FooterGooglyModule,
    EsportsCustomPaginationModule
    
  ]
})
export class ViewTeamModule { }
