import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { environment } from '../../../../environments/environment';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';
import {
  EsportsConstantsService,
  EsportsToastService,
  EsportsTournamentService,
  EsportsUserService,
  IPagination,
  S3UploadService,
  EsportsLeaderboardService,
  EsportsGtmService,
  SuperProperties,
  IUser
} from 'esports';
const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;

@Component({
  selector: 'app-view-team',
  templateUrl: './view-team.component.html',
  styleUrls: ['./view-team.component.scss'],
})
export class ViewTeamComponent implements OnInit {
  imageSrc: any = ''
  active = 1;
  nextId: number = 1;
  params: any = {};
  showLoader: boolean = true;
  tournamentDetails: any;
  teamId: any;
  teamList = [];
  show = false;
  isRemove = false;
  teamData:any = {};
  constructor(
    private _activateRoute: ActivatedRoute,
    private router: Router,
    public s3Service: S3UploadService,
    private eSportsToastService: EsportsToastService,
    private eSportsTournamentService: EsportsTournamentService,
    private userService: EsportsUserService,
    private translateService: TranslateService,
    private leaderboardService: EsportsLeaderboardService,
    private gtmService: EsportsGtmService
  ) { }

  followStatus:any = 'follow';
  data:any = {};
  currentUser: IUser;
  teamManageData:any = {};
  tournamentsUpcoming:any = [];
  tournamentsPast:any = [];
  teamLogoSRCFile:any = '';
  limitViewMember:any = 6;
  userSubscription: Subscription;
  isRemoveTeam:any = "remove_team";
  miniBox: boolean = false;
  paginationUpcomingDetails: any;
  paginationPastDetails: any;
  teamLeaderBoardDataVo:any = {};
  viewUpcomingMatch: IPagination = {
    activePage: 1,
    totalItems: 1,
    maxSize: 5,
    itemsPerPage: 5,
  };
  viewPastMatch: IPagination = {
    activePage: 1,
    totalItems: 1,
    maxSize: 5,
    itemsPerPage: 5,
  };


  async ngOnInit() {
    if (window.screen.width <= 576) {
      this.active = 3;
    }
    this._activateRoute.paramMap.subscribe((params) => {
      this.teamId = params.get('id');
    });

    if(!this.teamId){
      this.teamId = String(window.location.href).split('view-team/')[1];
    }
    this.getCurrentUserDetails(this.teamId);

    this.getTournamentList(0);
    this.getTournamentList(2);
    this.getTeamLeaderboardData();
    const teamData: any = await this.userService.getTeamById(API, this.teamId);
      // this.teamList = res.data.members;
      this.teamList = [];
      for (let d of teamData.data.members) {
        let d1 = {
          image: d.userId.profilePicture,
          name: d.userId.fullName,
        };
      }

    this.teamManageData = teamData.data.team;
    this.teamManageData.members = teamData.data.members;
    
    this.getAllMembersList();
    this.params.type = 0;
    this.params.tournamentId = (this.tournamentDetails ? this.tournamentDetails._id : null);
  }

  ngOnDestroy() {
    if(this.userSubscription)
    this.userSubscription.unsubscribe();
  }

  getCurrentUserDetails(id) {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.currentUser = data;
      }
    this.params.type = 0;
    this.params.tournamentId = (this.tournamentDetails ? this.tournamentDetails._id : null);
    });

    const query =  {_id: this.teamId };
    const encodedUrl = '?query='+encodeURIComponent(JSON.stringify(query));
    this.userService.getTeamList(API,encodedUrl).subscribe((res: any) => {
      this.teamData = {
        teamLogo: res.data[0].logo,
        socialMedia: res.data[0]?.social,
        shortDescription: res.data[0].shortDescription,
        teamName: res.data[0].name,
        members: res.data[0].member,
        teamBanner: res.data[0].teamBanner,
        status: res.data[0].status,
        socialMediaExist: Object.keys(res.data[0]?.social)
      };
    });
  }

  navChanges = (e) => {
    this.nextId = e.nextId;
    switch (this.nextId) {
      case 1:
        this.params.type = 0;
        break;
      case 2:
        this.params.type = 2;
        break;
    }
  };
  getLogoFile(event) {
    if (event.target.files && event.target.files[0]) {
      const file = event.target.files[0];
      const reader = new FileReader();
      reader.onload = e => this.imageSrc = reader.result;
      reader.readAsDataURL(file);
    }
  }

  /* delete team action here */
  deleteTeam(team_id) {

    const data = {
      "id": team_id,
      "admin": true,
      "query": {
        "condition": { "_id": team_id, "status": "active" },
        "update": { "status": "inactive" }
      }
    };
    this.showLoader = true;
    this.userService.update_team_admin(API, data).subscribe((data: any) => {
      let res = JSON.stringify(data);
      this.showLoader = false;
      this.eSportsToastService.showSuccess(
        this.translateService.instant('TEAMS.ACTION.SUCCESS_DELETE')
      );
      this.router.navigate(['profile/my-teams']);
    },(error) => {
      this.showLoader = false;
      this.eSportsToastService.showError(
        this.translateService.instant('TEAMS.ACTION.ERROR_DELETE')
      );
    });
  }

  /* get team mebers list here */
  async getAllMembersList(){
    this._activateRoute.paramMap.subscribe((params)=>{
      this.teamId = params.get('id');
  })
  const teamMembersData: any = await this.userService.getTeamMember(API, this.teamId);
    // this.myTeams = res;
    this.teamList = [];
     for (let d of teamMembersData.data) {
      let d1 = {
        image: d.userId.profilePicture,
        name: d.userId.fullName,
        role : d.role,
        userId : d.userId._id,
        userVo : d.userId,
        status : d.status
      }

      this.teamList.push(d1)
    }
  }

  /* follow or unfollow user actions here */
  followUser(){
    if (this.data) {
      if (this.followStatus == 'follow') {
        this.leaderboardService.followUser(API, this.data.userId).subscribe(
          (res: any) => {
            this.followStatus = 'unfollow';
            this.checkFollowStatus();
            this.eSportsToastService.showSuccess(res.message);
            this.userService.refreshCurrentUser(API, TOKEN);
          },
          (err) => {
             this.showLoader = false;
          }
        );
      } else if (this.followStatus == 'unfollow') {
        this.leaderboardService.unfollowUser(API, this.data.userId).subscribe(
          (res: any) => {
            this.followStatus = 'follow';
            this.checkFollowStatus();
            this.eSportsToastService.showSuccess(res.message);
            this.userService.refreshCurrentUser(API, TOKEN);
          },
          (res: any) => {
             this.showLoader = false;
          }
        );
      }
    } else {
      
    }
  }

  /* check for follow status here */
  checkFollowStatus() {
    this.leaderboardService.checkFollowStatus(API, this.data.userId).subscribe(
      (res: any) => {
        if (res.data[0].status == 1) {
          this.followStatus = 'unfollow';
        } else if (res.data[0].status == 0) {
          this.followStatus = 'follow';
        }
      },
      (err) => {
         this.showLoader = false;
      }
    );
  }

  async changeRole(player, role) {

    if (role == 'captain') {
      this.pushGTMTags('Make_Captain_Clicked');
    }

    await this.userService
      .update_member(API, {
        teamId: this.teamId,
        userId: player.userId,
        role: role,
        name: player.name,
      })
      .subscribe(
        (data) => {
          this.eSportsToastService.showSuccess(data?.message);
          this.router
            .navigateByUrl('/profile/teams', { skipLocationChange: true })
            .then(() => {
              this.router.navigate(['/manage-team/team-members', this.teamId]);
            });
        },
        (error) => {}
      );
  }

  pushGTMTags(eventName: string, articleOrVideo = null) {
    let superProperties: SuperProperties = {};
    if (this.currentUser) {
      superProperties = this.gtmService.assignLoggedInUsedData(this.currentUser);
    }

    this.gtmService.gtmEventWithSuperProp({
      eventName: eventName,
      superProps: superProperties,
    });
  }

  /* remove memebr from the team action here  */
  deleteTeamMember(player, status) {
    let data = {
      teamId: this.teamId,
      userId: player.userId,
      name: player.name,
      status: status,
    };

    this.userService.update_member(API, data).subscribe(
      (data) => {
        this.eSportsToastService.showSuccess(data?.message);
        this.router
          .navigateByUrl('/profile/teams', { skipLocationChange: true })
          .then(() => {
            this.router.navigate(['/manage-team/team-members', this.teamId]);
            //  this.router.navigate(['profile/my-teams']);
          });
      },
      (error) => {}
    );
  }

  getMemberData(dat){
    this.data = dat;
  }

  async getTournamentList(typeDefault,pageNum=1){
    const params: any = {};
    params.status = typeDefault;
    params.page = pageNum;
    params.limit = 5;

    const tournament = await this.eSportsTournamentService.fetchMyTournament(params);
    if(typeDefault==0){
      this.tournamentsUpcoming = tournament.data.docs || [];
      this.paginationUpcomingDetails = tournament?.data;
      for(var j=0;j<this.tournamentsUpcoming.length;j++){
        this.tournamentsUpcoming[j].image = this.tournamentsUpcoming[j].gameDetail.image;
        this.tournamentsUpcoming[j].gameName = this.tournamentsUpcoming[j].gameDetail.name;
        this.tournamentsUpcoming[j].matchId = this.tournamentsUpcoming[j].gameDetail._id;
      }
      this.viewUpcomingMatch.itemsPerPage = this.paginationUpcomingDetails?.limit;
      this.viewUpcomingMatch.totalItems = this.paginationUpcomingDetails?.totalDocs;
    }
    else{
      this.tournamentsPast = tournament.data.docs || [];
      this.paginationPastDetails = tournament?.data;
      for(var i=0;i<this.tournamentsPast.length;i++){
        this.tournamentsPast[i].image = this.tournamentsPast[i].gameDetail.image;
        this.tournamentsPast[i].gameName = this.tournamentsPast[i].gameDetail.name;
        this.tournamentsPast[i].matchId = this.tournamentsPast[i].gameDetail._id;
      }
      this.viewPastMatch.itemsPerPage = this.paginationPastDetails?.limit;
      this.viewPastMatch.totalItems = this.paginationPastDetails?.totalDocs;
    }
  }

  async handleFileInput(files) {
    /*this.teamLogoSRCFile = URL.createObjectURL(files.item(0));
    if (files.item(0)) {
      const file = files.item(0);
      const reader = new FileReader();
      reader.onload = (e: any) => {
        this.teamLogoSRCFile = e.target.result;
      };
      reader.readAsDataURL(file);
    }*/
    //this.isValidImage(files, { width: 800, height: 800 },'logo');
    let fileDiamention:any = { width: 1600, height: 900 };
    const upload: any = await this.s3Service.isValidImage(files, fileDiamention,'banner');
    if(upload && upload=='invalid_size'){
      this.eSportsToastService.showError(
        this.translateService.instant('TOURNAMENT.ERROR_IMG_2')
      );
      return;
    }
    else if(upload && upload=='valid_image'){
      this.eSportsToastService.showError(
        this.translateService.instant('TOURNAMENT.ERROR_IMG_1')
      );

      const file = files[0];
        const reader = new FileReader();
        reader.readAsDataURL(file);
        let th = this;
        reader.onload = (e) => {
          let image: any = new Image();
          image.src = e.target.result;
          image.onload = function () {
            let height = this.height;
            let width = this.width;
            if (height != fileDiamention['height'] && width != fileDiamention['width']) {
              th.eSportsToastService.showInfo(
                th.translateService.instant('TOURNAMENT.ERROR_IMG')
              );
            }
          };
        }

      this.upload(files,'logo');
    }
  }

  async upload(files,type:string = 'logo') {
    let promises = [];
    for (let obj of files) {
      promises.push(this.toBase64(obj));
    }
    let Base64String = await Promise.all(promises);
    let imageData = {
      path: environment.tournamentS3BucketName,
      files: Base64String,
    };
    this.teamLogoSRCFile = null;
    this.s3Service.fileUpload(environment.apiEndPoint,imageData).subscribe(
      (res) => {
        if(type=='logo'){
            this.teamLogoSRCFile = res['data'][0]['Location'];
        }
        this.updateTeamInfo();
        this.eSportsToastService.showSuccess(
          this.translateService.instant('API.BANNER.POST.SUCCESS')
        );
      },
      (err) => {
        this.eSportsToastService.showError(
          this.translateService.instant('API.BANNER.POST.ERROR')
        );
      }
    );
  }

  toBase64(file) {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = (error) => reject(error);
    });
  }

  /* update uploaded logo here */
  updateTeamInfo(){
    let teamObj: any = {
      admin: true,
      id: this.teamId,
      email: [],
      member: this.teamManageData.members,
      query: {
        condition: { _id: this.teamId },
        update: {
          logo: (this.teamLogoSRCFile ? this.teamLogoSRCFile : this.teamManageData?.logo ),
          shortDescription: this.teamManageData.shortDescription,
          social: (this.teamManageData.socialMedia ? this.teamManageData.socialMedia : {}),
          teamName: this.teamManageData.teamName,
          teamBanner: this.teamManageData.teamBanner,
        },
      },
      userName: (this.currentUser ? this.currentUser.username : ''),
    };
    this.userService.team_update(API, teamObj).subscribe(
      (res: any) => {
        if (res.data) {
          this.eSportsToastService.showSuccess(res.message);
        } else {
          this.eSportsToastService.showError(res.message);
        }
      },
      (err) => {
        this.eSportsToastService.showError(err.error.message);
      }
    );

  }

  clickMiniBox() {
    this.miniBox = !this.miniBox;
  }
  isShowPopup(parm){
    this.show = !this.show;

    if(parm === 'remove_team' || parm === 'switch_team'){
      this.isRemoveTeam = parm;
    }else if(parm && this.isRemoveTeam == 'switch_team'){
      /* redirect here for switch team */
    }
  }
  showPopup(event){
    this.show = !this.show
    this.isRemove = event;

    if(event){
      this.deleteTeam(this.teamId);
    }
    
  }

  upcomingPageChanged(page){
    this.getTournamentList(0,page);
  }
  pastPageChanged(page){
    this.getTournamentList(2,page);
  }

  getTeamLeaderboardData(){
    const params = {
      teamId: this.teamId
    };
    this.leaderboardService.getTeamLeaderboard(API, params).subscribe(
      (res: any) => {
        this.showLoader = false;
        this.teamLeaderBoardDataVo = res.data;
      },
      (err) => {
        this.showLoader = false;
      }
    );
  }
    
}
