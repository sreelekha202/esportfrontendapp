import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MatchmakingViewComponent } from './matchmaking.component';

describe('MatchmakingComponent', () => {
  let component: MatchmakingViewComponent;
  let fixture: ComponentFixture<MatchmakingViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MatchmakingViewComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MatchmakingViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
