import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { CreateTeamRoutingModule } from "./create-team-routing.module";
import { CreateTeamComponent } from "./create-team.component";
import { InputSearchComponent } from "./components/input-search/input-search.component";
import { TeamIconComponent } from "./components/team-icon/team-icon.component";
import { TeamBannerComponent } from "./components/team-banner/team-banner.component";
import { SocialAccountsComponent } from "./components/social-accounts/social-accounts.component";
import { AccountItemComponent } from "./components/account-item/account-item.component";
import { CoreModule } from "../../../core/core.module";
import { FormComponentModule } from "../../../shared/components/form-component/form-component.module";
import { HeaderGooglyModule } from '../../../shared/components/header-googly/header-googly.module';
import { UploadImageModule } from "../../../shared/components/upload-image/upload-image.module";
import { SharedModule } from "../../../shared/modules/shared.module";

@NgModule({
  declarations: [CreateTeamComponent, InputSearchComponent,
    TeamIconComponent, TeamBannerComponent,
    SocialAccountsComponent, AccountItemComponent],
  imports: [
    CommonModule,
    CoreModule,
    CreateTeamRoutingModule,
    FormComponentModule,
    SharedModule,
    HeaderGooglyModule,
    UploadImageModule,
  ],
  providers: []
})
export class CreateTeamModule { }
