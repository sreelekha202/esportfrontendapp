import { Component, OnDestroy, OnInit } from '@angular/core';
import {FormBuilder, FormGroup,FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';
import {
  catchError,
  debounceTime,
  distinctUntilChanged,
  switchMap,
} from 'rxjs/operators';
import {
  IUser,
  EsportsUserService,
  EsportsToastService,
  GlobalUtils,
  S3UploadService,
  EsportsConstantsService
} from 'esports';
import { Location } from '@angular/common';
import {
  AppHtmlProfileRoutes,
  AppHtmlRoutes,
} from '../../../app-routing.model';
import { environment } from '../../../../environments/environment';

const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;
@Component({
  selector: 'app-create-team',
  templateUrl: './create-team.component.html',
  styleUrls: ['./create-team.component.scss'],
})
export class CreateTeamComponent implements OnInit, OnDestroy {
  name = '';
  bio = '';
  AppHtmlRoutes = AppHtmlRoutes;
  AppHtmlProfileRoutes = AppHtmlProfileRoutes;
  form: FormGroup;
  currentUser: IUser;
  membersObj;
  emailInvitation = [];
  singleEmail;
  dropdown: any;
  txtValue: any;
  playername = [];
  createMemberForm:FormGroup;
  fileToUploadBanner:any = '';
  fileToUploadLogo:any = '';
  teamBannerSRCFile:any = '';
  teamLogoSRCFile:any = '';
  formatter = (result: any) => {
    return result;
  };
  get email(): any {
    // return this.createMemberForm.get("email");
    return this.singleEmail;
  }
  hideDropdown: any;

  biographyLength = 250;

  mock_select = [
    {
      name: 'facebook',
      placeholder: 'Facebook url',
      value: '',
    },
    {
      name: 'twitch',
      placeholder: 'Twitch url',
      value: '',
    },
    {
      name: 'instagram',
      placeholder: 'Instagram url',
      value: '',
    },
    {
      name: 'youtube',
      placeholder: 'Youtube url',
      value: '',
    },
    {
      name: 'discord',
      placeholder: 'Discord url',
      value: '',
    },
    {
      name: 'twitter',
      placeholder: 'Twitter url',
      value: '',
    },
  ];
  listData = []
  selectedSocialName: string = '';
  selectedSocialIdObj: any;
  selectedSocialIdArrayObj:any = [];
  isInEditMode: boolean = false;
  clicked: boolean = false;
  selectedSocial = [];
  teamData;
  teamId: string;
  userSubscription: Subscription;
  fallbackURL: string;
  socialList = [
    {
      icon: '../../../../assets/socialNetwork/twitter.png',
      label: 'Twitter',
      placeHolder: "Twitter streaming URL"
    },
    {
      icon: '../../../../assets/socialNetwork/facebook.png',
      label: 'Facebook',
      placeHolder: "Facebook URL"
    },
    {
      icon: '../../../../assets/socialNetwork/instargram.png',
      label: 'Instagram',
      placeHolder: "Instagram URL"
    },
    {
      icon: '../../../../assets/socialNetwork/twitch.png',
      label: 'Twitch',
      placeHolder: "Twitch URL"
    },
    {
      icon: '../../../../assets/socialNetwork/discord.png',
      label: 'Discord',
      placeHolder: "Discord URL"
    },
  ];
  constructor(
    private fb: FormBuilder,
    private userService: EsportsUserService,
    public toastService: EsportsToastService,
    private translateService: TranslateService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private location: Location,
    public s3Service: S3UploadService,
  ) {}

  ngOnInit(): void {
    if (
      GlobalUtils.isBrowser() &&
      localStorage.getItem(environment.currentToken)
    ) {
      this.getUserData();
    } else {
      this.router.navigate(['/']);
    }
    this.createTournamentForm();
    if (this.activatedRoute.snapshot.params['id']) {
      this.isInEditMode = true;
      this.teamId = this.activatedRoute.snapshot.params['id'];
      this.fallbackURL = '/admin/team-management';
      this.getTeamData(this.activatedRoute.snapshot.params['id']);
    }
  }
  onToggleChangeSpecial(special){
    if(special.label){
    this.selectedSocialName = special.label;
    this.selectedSocialIdObj = special;
    }else{
      for(var i=0;i<this.socialList.length;i++){
        if(this.selectedSocialIdArrayObj[0] && this.socialList[i].label==special.labelText){
          if(special.deleted){
            let socialAnotherIndexId = this.selectedSocialIdArrayObj.findIndex((x) => x[special.labelText]);
            this.selectedSocialIdArrayObj.splice(socialAnotherIndexId,1);
            let socialIndexId = this.listData.findIndex((x) => x.label === special.labelText);
            this.listData.splice(socialIndexId,1);
          }else{
            let socialAnotherIndexId = this.listData.findIndex((x) => x.label === special.labelText);
            this.selectedSocialIdArrayObj[socialAnotherIndexId][this.socialList[i].label] = special[this.socialList[i].label];
          }
          break;
        }
      }
    }
  }
  onAddPlatform() {
    //this.socialList = this.socialList.filter((item) => item.label != this.selectedSocialIdObj.label)
    if(this.selectedSocialIdObj.label){
      if(this.listData.findIndex(item => item.label === this.selectedSocialIdObj.label)==-1){
        this.listData.push(this.selectedSocialIdObj);
        let selectedAdd:any = {};
        selectedAdd[this.selectedSocialIdObj.label] = '';
        this.selectedSocialIdArrayObj.push(selectedAdd);
      } 
      else{
        this.toastService.showInfo("Already "+this.selectedSocialIdObj.label+" has been added");
      }
    }
  }
  getTeamData(id) {
    const query = `?query=${encodeURIComponent(JSON.stringify({ _id: id }))}`;
    this.userService.getTeamList(API, query).subscribe(
      (res) => {
        this.teamData = {
          teamLogo: res.data[0].logo,
          socialMedia: res.data[0]?.social,
          shortDescription: res.data[0].shortDescription,
          teamName: res.data[0].name,
          members: res.data[0].member,
        };
        var list = [];

        if (this.teamData?.socialMedia) {
          for (
            var i = 0;
            i < Object.keys(this.teamData?.socialMedia).length;
            i++
          ) {
            var item = {};
            item['name'] = Object.keys(this.teamData?.socialMedia)[i];
            item['placeholder'] =
              Object.keys(this.teamData?.socialMedia)[i] + ' url';
            item['value'] =
              this.teamData?.socialMedia[
              Object.keys(this.teamData?.socialMedia)[i]
              ];

            let validatorFn = this.fb.control('', []);
            this.createMemberForm.addControl(
              Object.keys(this.teamData?.socialMedia)[i],
              validatorFn
            );
            this.createMemberForm.controls[
              Object.keys(this.teamData?.socialMedia)[i]
            ].setValue(
              this.teamData?.socialMedia[
              Object.keys(this.teamData?.socialMedia)[i]
              ]
            );
            this.createMemberForm.updateValueAndValidity();

            list.push(item);
          }
        }

        this.selectedSocial = list;
        const indexm = this.teamData.members.findIndex(
          (x) => JSON.stringify(x._id) === JSON.stringify(this.currentUser?._id)
        );
        if (indexm > -1) {
          if (
            this.teamData.members[indexm].role != 'player' ||
            this.currentUser.accountType == 'admin'
          ) {
            this.createMemberForm.patchValue(this.teamData);
          } else {
            this.toastService.showError(
              this.translateService.instant('PROFILE.TEAMS.ERRORS.ERROR6')
            );
            this.router.navigate(['/profile/teams']);
          }
        } else if (this.currentUser.accountType == 'admin') {
          this.createMemberForm.patchValue(this.teamData);
        } else {
          this.toastService.showError(
            this.translateService.instant('PROFILE.TEAMS.ERRORS.ERROR6')
          );
          this.router.navigate(['/profile/teams']);
        }
      },
      (err) => { }
    );
  }
  cancel() {
    this.location.back();
  }
  ngOnDestroy() {
    this.userSubscription?.unsubscribe();
  }

  getUserData() {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.currentUser = data;
      }
    });
  }

  filterFunction() {
    var input, filter, a, i;
    input = document.getElementById('myInput');
    filter = input.value.toUpperCase();
    this.dropdown = document.getElementById('myDropdown');
    a = this.dropdown.getElementsByTagName('a');
    for (i = 0; i < a.length; i++) {
      this.txtValue = a[i].textContent || a[i].innerText;
      if (this.txtValue.toUpperCase().indexOf(filter) > -1) {
        a[i].style.display = '';
      } else {
        a[i].style.display = 'none';
      }
    }
  }

  getValue(value) {
    if (!this.isSocialAlreadyAdded(this.selectedSocial, value?.name)) {
      this.selectedSocial.push(value);
      let validatorFn = this.fb.control('', []);
      this.createMemberForm.addControl(value.name, validatorFn);
      this.createMemberForm.updateValueAndValidity();
    }
    this.hideDropdown = false;
  }
  removeSocial(i) {
    this.createMemberForm.controls[this.selectedSocial[i].name].reset();
    this.selectedSocial.splice(i, 1);
  }

  createTournamentForm() {
    this.createMemberForm = this.fb.group({
      teamLogo: [''],
      teamBanner: [''],
      teamName: ['', Validators.required],
      shortDescription: [''],
      members: [[], Validators.compose([])],
      socialMedia: [''],
      email: [''],
    });
  }

  createTeam() {
    /* validate URL here */
    if(this.selectedSocialIdArrayObj && this.selectedSocialIdArrayObj.length){
      for(let soc of this.selectedSocialIdArrayObj){
        if(soc['Facebook'] && !EsportsConstantsService.facebookRegex.test(soc['Facebook'])){
          this.toastService.showError(this.translateService.instant('MANAGE_TOURNAMENT.STREAM.ERROR.FACEBOOK_URL'));
          return;
        }else if(soc['Twitter'] && !EsportsConstantsService.twitterRegex.test(soc['Twitter'])){
          this.toastService.showError(this.translateService.instant('MANAGE_TOURNAMENT.STREAM.ERROR.TWITTER_URL'));
          return;
        }else if(soc['Instagram'] && !EsportsConstantsService.instagramRegex.test(soc['Instagram'])){
          this.toastService.showError(this.translateService.instant('MANAGE_TOURNAMENT.STREAM.ERROR.INSTAGRAM_URL'));
          return;
        }else if(soc['Twitch'] && !EsportsConstantsService.twitchRegex.test(soc['Twitch'])){
          this.toastService.showError(this.translateService.instant('MANAGE_TOURNAMENT.STREAM.ERROR.TWITCH_URL'));
          return;
        }else if(soc['Discord'] && !EsportsConstantsService.discordURLRegex.test(soc['Discord'])){
          this.toastService.showError(this.translateService.instant('MANAGE_TOURNAMENT.STREAM.ERROR.DISCORD_URL'));
          return;
        }
      }
    }

    const controls = this.createMemberForm.controls;
    if (this.createMemberForm.invalid) {
      Object.keys(controls).forEach((controlName) =>
        controls[controlName].markAsTouched()
      );
      return;
    }

    let social = {};
    let members = [];
    let that = this;
    
    this.mock_select.forEach(function (value) {
      for(var j=0;j<that.selectedSocialIdArrayObj.length;j++){
        if(that.selectedSocialIdArrayObj[j][value.name.charAt(0).toUpperCase() + value.name.slice(1).toLowerCase()]){
          social[value.name.toLowerCase()] = that.selectedSocialIdArrayObj[j][value.name.charAt(0).toUpperCase() + value.name.slice(1).toLowerCase()];
        }
      } 
    });

    /* social media required validation */
    /*if(that.selectedSocialIdArrayObj && (!that.selectedSocialIdArrayObj.length || !that.selectedSocialIdArrayObj[0])){
      this.toastService.showError(
        this.translateService.instant('CREATE_TEAM.SOCIAL_MEDIA_REQUIRED')
      );
      return;
    }*/

    this.createMemberForm.value.members.forEach(function (value) {
      members.push(value._id);
    });

    let payload = {
      logo: (this.teamLogoSRCFile),
      teamBanner : (this.teamBannerSRCFile),
      teamName: this.createMemberForm.value.teamName,
      shortDescription: this.createMemberForm.value.shortDescription,
      social: social,
      members: members,
      email:[]
    };
    this.clicked = true;
    if (this.isInEditMode) {
      //this.updateTeam(payload);
    } else {
      this.userService.create_team(API, payload).subscribe(
        (data) => {
          this.clicked = false;
          this.toastService.showSuccess(data?.message);
          this.cancel();
        },
        (error) => {
          this.clicked = false;
          this.toastService.showError(error?.error?.message);
        }
      );
    }
  }
  updateTeam(payload) {
    this.clicked = true;
    let teamData = {
      logo: payload.logo,
      teamName: payload.teamName,
      shortDescription: payload.shortDescription,
      social: payload.social,
    };
    const teamObj = {
      id: this.teamId,
      admin: true,
      member: this.createMemberForm.value.members,
      userName: this.currentUser.fullName,
      email: payload.email,
      query: {
        condition: { _id: this.teamId },
        update: teamData,
      },
    };
    this.userService.team_update(API, teamObj).subscribe(
      (res) => {
        this.clicked = false;
        if (res.data) {
          this.toastService.showSuccess(res.message);
          this.cancel();
        } else {
          this.toastService.showError(res.message);
        }
      },
      (err) => {
        this.clicked = false;
        this.toastService.showError(err.error.message);
      }
    );
  }

  onChangeMembers() {
    this.membersObj = '';
  }

  addMembers() {
    setTimeout(() => {
      if (this.membersObj && typeof this.membersObj == 'object') {
        if (
          !this.isMembersAlreadyAdded(
            this.createMemberForm.value.members,
            this.membersObj?._id
          )
        ) {
          if (this.membersObj._id != this.currentUser._id) {
            this.createMemberForm.value.members.push(this.membersObj);
          } else {
            this.toastService.showError(
              this.translateService.instant('PROFILE.TEAMS.ERRORS.ERROR4')
            );
          }
        } else {
          this.toastService.showError(
            this.translateService.instant('PROFILE.TEAMS.ERRORS.ERROR3')
          );
        }
        this.membersObj = '';
      }
    }, 100);
  }

  isMembersAlreadyAdded(arr, _id) {
    const { length } = arr;
    const id = length + 1;
    const found = arr.some((el) => el?._id === _id);
    return found;
  }

  isSocialAlreadyAdded(arr, name) {
    const { length } = arr;
    const id = length + 1;
    const found = arr.some((el) => el?.name === name);
    return found;
  }

  removeMember(index) {
    // if (this.isInEditMode) {
    //   if (this.createMemberForm.value.members[index].role == 'owner') {
    //     this.toastService.showError(
    //       this.translateService.instant('PROFILE.TEAMS.ERRORS.ERROR2')
    //     );
    //   } else {
    //     this.createMemberForm.value.members[index].status = 'deleted';
    //   }
    // } else {
    this.createMemberForm.value.members.splice(index, 1);
    // }
  }

  validateEmail(email) {
    const regularExpression =
      /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (!regularExpression.test(String(email).toLowerCase())) {
      return 0;
    }
    return 1;
  }
  addEmails() {
    if (this.validateEmail(this.email)) {
      if (this.emailInvitation.indexOf(this.email) == -1) {
        this.emailInvitation.push(this.email);
      } else {
        this.toastService.showError(
          this.translateService.instant('Duplicate invite by email address')
        );
      }
    } else {
      this.toastService.showError(
        this.translateService.instant(
          'Invite by email address is not a valid email address'
        )
      );
    }
    this.singleEmail = '';
  }
  removeEmail(index) {
    this.emailInvitation.splice(index, 1);
  }
  onTextChange(a) {
    this.playername = [];
    if (a.length >= 3) {
      this.userService.searchUsers(a).subscribe((data: any) => {
        this.playername = data;
        for (let s of this.playername) {
          s.selected = false;
        }
      });
    }
  }

  /* function need to be modifed */
  saveChange() {
    this.toastService.showSuccess(
      this.translateService.instant('CREATE_TEAM.DETAILS.SUCCESS')
    );
    let d = [];
    for (let a of this.playername) {
      if (a.selected == true) {
        d.push(a._id);
      }
    }
    let datatosend = {};
    /*let datatosend = {
      email: this.emailInvitation,
      logo: imgurl,
      teamBanner:bannerurl,
      members: d,
      shortDescription: this.bio,
      social: sociall,
      teamName: this.name,
    };*/
    this.userService.create_team(API, datatosend).subscribe(
      (data: any) => {
        this.cancel();
        this.clearsession();
      },
      (err) => {
        this.clearsession();
      }
    );
  }
  clearsession() {

  }
  clearSocialMedia() {

  }

  async handleFileInputLogo(files: FileList){
    this.fileToUploadLogo = files.item(0);
    let fileDiamention:any = { width: 900, height: 900 };
    const upload: any = await this.s3Service.isValidImage(files, fileDiamention,'banner');
    if(upload && upload=='invalid_size'){
      this.toastService.showError(
        this.translateService.instant('TOURNAMENT.ERROR_IMG_2')
      );
      return;
    }
    else if(upload && upload=='valid_image'){
      this.toastService.showError(
        this.translateService.instant('TOURNAMENT.ERROR_IMG_1')
      );

      const file = files[0];
        const reader = new FileReader();
        reader.readAsDataURL(file);
        let th = this;
        reader.onload = (e) => {
          let image: any = new Image();
          image.src = e.target.result;
          image.onload = function () {
            let height = this.height;
            let width = this.width;
            if (height != fileDiamention['height'] && width != fileDiamention['width']) {
              th.toastService.showInfo(
                th.translateService.instant('TOURNAMENT.ERROR_IMG')
              );
            }
          };
        }

      this.upload(files,'logo');
    }
  }

  async handleFileInputBanner(files: FileList){
    this.fileToUploadBanner = files.item(0);
    let fileDiamention:any = { width: 1600, height: 900 };
    const upload: any = await this.s3Service.isValidImage(files, fileDiamention,'banner');
    if(upload && upload=='invalid_size'){
      this.toastService.showError(
        this.translateService.instant('TOURNAMENT.ERROR_IMG_2')
      );
      return;
    }
    else if(upload && upload=='valid_image'){
      this.toastService.showError(
        this.translateService.instant('TOURNAMENT.ERROR_IMG_1')
      );

      const file = files[0];
        const reader = new FileReader();
        reader.readAsDataURL(file);
        let th = this;
        reader.onload = (e) => {
          let image: any = new Image();
          image.src = e.target.result;
          image.onload = function () {
            let height = this.height;
            let width = this.width;
            if (height != fileDiamention['height'] && width != fileDiamention['width']) {
              th.toastService.showInfo(
                th.translateService.instant('TOURNAMENT.ERROR_IMG')
              );
            }
          };
        }

      this.upload(files,'banner');
    }
  }

  async upload(files,type:string = 'banner') {
    let promises = [];
    for (let obj of files) {
      promises.push(this.toBase64(obj));
    }
    let Base64String = await Promise.all(promises);
    let imageData = {
      path: environment.tournamentS3BucketName,
      files: Base64String,
    };
    this.s3Service.fileUpload(environment.apiEndPoint,imageData).subscribe(
      (res) => {
        if(type=='banner'){
          this.teamBannerSRCFile = '';
          this.teamBannerSRCFile = res['data'][0]['Location'];
          this.fileToUploadBanner = files.item(0);
        }

        if(type=='logo'){
          this.teamLogoSRCFile = '';
          this.teamLogoSRCFile = res['data'][0]['Location'];
          this.fileToUploadLogo = files.item(0);
        }

        this.toastService.showSuccess(
          this.translateService.instant('FILE_UPLOAD.SUCCESS')
        );
      },
      (err) => {
        this.toastService.showError(
          this.translateService.instant('FILE_UPLOAD.ERROR')
        );
      }
    );
  }

  toBase64(file) {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = (error) => reject(error);
    });
  }
}
