import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

const routes: Routes = [
  {
    path: ':id',
    loadChildren: () =>
      import(
        '../../modules/tournament/view-tournament/view-tournament.module'
      ).then((m) => m.ViewTournamentModule),
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TournamentRoutingModule { }
