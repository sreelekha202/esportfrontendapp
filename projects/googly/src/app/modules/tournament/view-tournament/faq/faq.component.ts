import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.scss']
})
export class FaqComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  set_1 = ['FAQ Question 1'];
  set_2 = ['FAQ Question 2'];
  set_3 = ['FAQ Question 3'];
  set_4 = ['FAQ Question 4'];
  expandedIndex = 0;

  onTextChange(data) {
  }
  data: any = [
    {
      'description': {
        english: "How are my Fortnite replays stored? Replays are stored locally on your console or PC. On console your last 10...",
        french: ""
      },
      "slug": "testt--600ecd33089c510008c6e57b",
      "thumbnailUrl": "assets/icons/video-player/image 59.png",
      "title": { english: "fortnite battle royale - replay system", french: "" },
      "youtubeUrl": "https://www.youtube.com/watch?v=VPe6-LoYXyQ",
      "_id": "600ecd33089c510008c6e57b"
    },
    {
      'description': {
        english: "ESRB Rating: Teen with violence sign up for the fortnite defending the Fort- Fortnite gameplay...",
        french: ""
      },
      "slug": "testt--600ecd33089c510008c6e57b",
      "thumbnailUrl": "assets/icons/video-player/image 60.png",
      "title": { english: "defending the fort - fortnite gameplay", french: "" },
      "youtubeUrl": "https://www.youtube.com/watch?v=VPe6-LoYXyQ",
      "_id": "600ecd33089c510008c6e57b"
    },
    {
      'description': {
        english: "Checkout some of the new features in Fortnite chapter 2, and we even got a Victory Royale to celebrate...",
        french: ""
      },
      "slug": "testt--600ecd33089c510008c6e57b",
      "thumbnailUrl": "assets/icons/video-player/image 61.png",
      "title": { english: "fortnite chapter 2 - victory royale gamepaly", french: "" },
      "youtubeUrl": "https://www.youtube.com/watch?v=VPe6-LoYXyQ",
      "_id": "600ecd33089c510008c6e57b"
    }

  ]

}
