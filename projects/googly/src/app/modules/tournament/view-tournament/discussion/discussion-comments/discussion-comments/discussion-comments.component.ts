import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { IUser, EsportsUserService } from 'esports';

@Component({
  selector: 'app-discussion-comments',
  templateUrl: './discussion-comments.component.html',
  styleUrls: ['./discussion-comments.component.scss'],
})
export class DiscussionCommentsComponent implements OnInit {
  @Input() placeholder: string = '';
  @Input() isReplying: boolean = false;
  @Output() onSaveComment = new EventEmitter();
  text: any;
  currentUser: IUser;

  constructor(private userService: EsportsUserService) {}

  ngOnInit(): void {
    this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.currentUser = data;
      }
    });
  }

  comment() {
    this.onSaveComment.emit(this.text);
  }
}
