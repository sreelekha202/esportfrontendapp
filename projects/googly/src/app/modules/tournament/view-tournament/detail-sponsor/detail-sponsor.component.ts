import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'app-detail-sponsor',
  templateUrl: './detail-sponsor.component.html',
  styleUrls: ['./detail-sponsor.component.scss']
})
export class DetailSponsorComponent implements OnInit {
  @Output() hidePopup = new EventEmitter();
  @Input() list;
  constructor() { }

  ngOnInit(): void {
  }
}
