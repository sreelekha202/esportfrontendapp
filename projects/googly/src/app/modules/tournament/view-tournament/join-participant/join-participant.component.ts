import {
  EsportsUserService,
  EsportsToastService,
  EsportsAuthServices,
  EsportsTournamentService,
} from 'esports';
import { Component, OnDestroy, OnInit } from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormArray,
  FormControl,
} from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import 'rxjs/add/operator/filter';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';
import { environment } from '../../../../../environments/environment';
import { Location } from '@angular/common';
const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;

@Component({
  selector: 'app-join-season-participant',
  templateUrl: './join-participant.component.html',
  styleUrls: ['./join-participant.component.scss'],
})
export class JoinParticipantComponent implements OnInit, OnDestroy {
  participantForm: FormGroup;
  user: any;

  tournament: any = {};
  teamData: any = {};
  isProcessing = false;
  isLoaded = false;
  showParticipantForm = false;
  requestProcessing = false; // for those user whose all the field are valid in the form.

  participantRS: string | null;
  timeoutId = null;

  // logo config
  logoDimension = { width: 180, height: 180 };
  logoSize = 1000 * 1000 * 5;

  userSubscription: Subscription;
  participantJoinStatus;
  tournamentSubscription: Subscription;
  showPayment: Boolean = false;
  originalValues = {};
  registrationStatus = {
    isOpen: false,
    isClosed: false,
    notYetStarted: false,
  };
  constructor(
    private fb: FormBuilder,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private tournamentService: EsportsTournamentService,
    private eSportsToastService: EsportsToastService,
    private userService: EsportsUserService,
    private translateService: TranslateService,
    private location: Location,
    private authServices: EsportsAuthServices
  ) {}

  ngOnInit(): void {
    this.getCurrentUserDetails();
    this.tournamentService.joinDetail.subscribe((data) => {
      this.teamData = data;
    });
  }

  ngOnDestroy() {
    this.userSubscription?.unsubscribe();
  }

  getCurrentUserDetails() {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.user = data;
        const pId = this.activatedRoute.snapshot.params.pId;
        if (pId) {
          //this.tournament.slug = this.router.url.split('/').reverse()[2];
          this.tournament.slug = this.activatedRoute.snapshot.params.id;
          this.fetchTournamentDetails(this.tournament?.slug);
        } else {
          //this.tournament.slug = this.router.url.split('/').reverse()[1];
          this.tournament.slug = this.activatedRoute.snapshot.params.id;
          this.fetchTournamentDetails(this.tournament?.slug);
          // this.tournamentService.tournamentActiveTab = 3;
        }
        const tournamentId = this.tournament.slug.split('-').reverse()[0];
        this.fetchParticipantRegistrationStatus(tournamentId);
      }
    });
  }

  set_1 = ['Player details'];
  set_2 = ['Player 2 details'];
  set_3 = ['Player 3 details'];
  expandedIndex = 0;

  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);

  /**
   * Fetch Some tournament Details
   * @param slug
   */
  fetchTournamentDetails = async (slug) => {
    try {
      this.isLoaded = false;
      const query = JSON.stringify({ slug });
      const select = `&select=_id,name,logo,banner,participantType,teamSize,substituteMemberSize,isSeeded,isRegStartDate,isRegistrationClosed,allowSubstituteMember,slug,startDate,tournamentType,isIncludeSponsor,maxParticipants,bracketType,isPaid,regFee,regFeeCurrency,regStartDate,regEndDate,isSeason`;
      const tournament = await this.tournamentService
        .getSeasonTournaments({ query }, select)
        .toPromise();
      this.tournament = tournament?.data?.length ? tournament.data[0] : null;
      this.setRegistrationOpenStatus();
      if (!this.tournament) {
        throw new Error(
          this.translateService.instant(
            'VIEW_TOURNAMENT.JOIN_PARTICIPANT.INVALID_TOURNAMENT'
          )
        );
      }
      if (this.user) {
        this.tournament.username = this.user.username
          ? this.user.username
          : this.user.username;
        this.tournament.loggedInStatus = 'LoggedIn';
      } else {
        this.tournament.username = '';
        this.tournament.loggedInStatus = 'Guest';
      }
      // const participantId = this.activatedRoute?.snapshot?.params?.pId;
      // const {
      //   data,
      // } = await this.tournamentService.fetchSeasonParticipantRegistrationStatus(
      //   this.tournament?._id,
      //   participantId
      // );

      let data = null;
      if (this.tournament?.isSeason) {
        data =
          await this.tournamentService.fetchSeasonParticipantRegistrationStatus(
            this.tournament?._id
          );
        this.participantRS =
          data?.data?.isRegistered == null ? 'join' : 'already-registered';
        if (data?.data?.isRegistered) {
          this.router.navigate([`games/match-season/${this.tournament?._id}`]);
        }
      } else {
        data = await this.tournamentService.fetchParticipantRegistrationStatus(
          API,
          this.tournament?._id
        );
        this.participantRS = data?.data?.type;
      }

      this.showParticipantForm = [
        'join',
        'already-registered',
        'checked-in',
        'already-checked-in',
      ].includes(this.participantRS);

      // if (data?.isRegistered) {
      // } else {
      //   this.showParticipantForm = true;
      // }

      this.createForm();

      this.fetchParticipant(data?.participantId);
      this.isLoaded = true;
    } catch (error) {
      this.isLoaded = true;
      this.eSportsToastService.showError(error?.statusText || error?.message);
    }
  };

  fetchParticipant = async (pid) => {
    try {
      if (pid) {
        const query = {
          _id: pid,
          tournamentId: this.tournament?._id,
        };
        const encodeUrl = `?query=${encodeURIComponent(JSON.stringify(query))}`;
        const participant = await this.tournamentService
          .getParticipants1(encodeUrl)
          .toPromise();
        if (participant?.data?.length) {
          this.participantForm.addControl(
            'id',
            new FormControl(participant.data[0]._id)
          );
          await this.addTeamControls(
            participant.data[0]?.teamMembers?.length || 0,

            participant.data[0]?.substituteMembers?.length || 0
          );
          this.participantForm.patchValue({
            ...participant.data[0],
          });
        }
      } else {
        //this.getPaymentAccountStatus();
        if (this.tournament.participantType == 'team') {
          if (this.teamData) {
            await this.addTeamControls(
              this.teamData?.teamMembers?.length || 0,
              this.teamData?.substituteMembers?.length || 0
            );

            this.participantForm.patchValue({
              ...this.teamData,
            });
          } else {
            this.location.back();
            // this.router.navigate(['team-registration', this.tournament.slug]);
          }
        } else {
          await this.patchFormValues();
        }

        if (this.tournament?.isPaid) {
          return;
        }

        if (this.participantForm.valid) {
          this.requestProcessing = true;
          this.submit();
        }
      }
    } catch (error) {
      this.eSportsToastService.showError(
        error?.error?.message || error?.message
      );
    }
  };

  /**
   * Create Form
   */
  createForm() {
    this.participantForm = this.fb.group({
      teamId: [''],
      logo: ['', Validators.required],
      teamName: [
        '',
        Validators.compose([
          Validators.required,
          this.emptyCheckAndRequestProccess,
        ]),
      ],
      name: ['', Validators.compose([Validators.required, this.isEmptyCheck])],
      // phoneNumber: [''],
      email: [''],
      inGamerUserId: [
        '',
        Validators.compose([
          Validators.required,
          this.emptyCheckAndRequestProccess,
        ]),
      ],
      teamMembers: this.fb.array([]),
      substituteMembers: this.fb.array([]),
    });
  }

  /**
   * Empty validation
   * @param control formControl
   */
  isEmptyCheck = (control: FormControl) => {
    return control.value && control.value.trim() ? null : { required: true };
  };

  emptyCheckAndRequestProccess = (control: FormControl) => {
    return control.value && control.value.trim() ? null : { required: true };
  };

  /** Add Member For Team */
  addMember(field): void {
    const teamMembers = this.participantForm.get(field) as FormArray;
    const createMemberForm = (): FormGroup => {
      return this.fb.group({
        teamParticipantId: [],
        userId: [],
        name: [
          '',
          Validators.compose([Validators.required, this.isEmptyCheck]),
        ],
        // phoneNumber: [
        //   '',
        //   Validators.compose([
        //     Validators.required,
        //     // this.isValidPhoneNumber
        //   ]),
        // ],
        email: [
          '',
          Validators.compose([Validators.required, Validators.email]),
        ],
        inGamerUserId: [
          '',
          Validators.compose([
            Validators.required,
            this.emptyCheckAndRequestProccess,
          ]),
        ],
      });
    };
    teamMembers.push(createMemberForm());
  }

  /**
   * Get Unique Team Name, tournamentUsername and GameUserId Before Registeration
   * @param name text name
   * @param field field name
   * @param index member index
   */

  isUniqueName = async (name, field, index = null, arrayField = null) => {
    try {
      const checkUniqueNameInForm = async () => {
        const value = this.participantForm.value;
        const matchArray = [];
        matchArray.push(value[field].trim() === name.trim());
        value.teamMembers.forEach((el) => {
          matchArray.push(el[field] && el[field].trim() === name.trim());
        });
        value.substituteMembers.forEach((el) => {
          matchArray.push(el[field] && el[field].trim() === name.trim());
        });
        return matchArray.filter((el) => el).length >= 2;
      };

      const isAvailable = async () => {
        try {
          const response = await this.tournamentService
            .searchParticipant(
              API,
              field,
              name,
              this.tournament?._id,
              this.participantForm.value?.id
            )
            .toPromise();
          const isNameAvailable =
            response.data.isExist || (await checkUniqueNameInForm());
          if (typeof index === 'number') {
            const array = this.participantForm.get(arrayField) as FormArray;
            const value = array.at(index).get(field).value.trim();
            array
              .at(index)
              .get(field)
              .setErrors(
                !value
                  ? { required: true }
                  : isNameAvailable
                  ? { isNameAvailable }
                  : response.data.invalidId
                  ? { invalidId: true }
                  : null
              );
          } else {
            const value = this.participantForm.get(field).value.trim();
            this.participantForm
              .get(field)
              .setErrors(
                !value
                  ? { required: true }
                  : isNameAvailable
                  ? { isNameAvailable }
                  : response.data.invalidId
                  ? { invalidId: true }
                  : null
              );
            if (
              this.tournament?.participantType === 'individual' &&
              field === 'inGamerUserId'
            ) {
              this.participantForm.get('teamName').setValue(value);
              this.participantForm.get('teamName').clearValidators();
              this.participantForm.get('teamName').updateValueAndValidity();
            }
          }
          this.participantForm.updateValueAndValidity();
        } catch (error) {
          throw error;
        } finally {
          clearTimeout(this.timeoutId);
        }
      };

      if (typeof index === 'number') {
        const array = this.participantForm.get(arrayField) as FormArray;
        array.at(index).get(field).setErrors({ wait: true });
      } else {
        this.participantForm.get(field).setErrors({ wait: true });
      }

      if (!this.timeoutId) {
        this.timeoutId = setTimeout(isAvailable, 500);
      } else {
        clearTimeout(this.timeoutId);
        this.timeoutId = setTimeout(isAvailable, 500);
      }
    } catch (error) {
      this.eSportsToastService.showError(
        error?.error?.message || error?.message
      );
    }
  };

  buildQuery = async (field, text) => {
    switch (field) {
      case 'teamName':
        return {
          teamName: text,
          tournamentId: this.tournament?._id,
        };
      default:
        return {
          $or: [
            { [field]: text },
            { teamMembers: { $elemMatch: { [field]: text } } },
          ],
          tournamentId: this.tournament?._id,
        };
    }
  };

  // convert image to base64
  teamLogo(event) {
    const files = event.target.files;
    if (files && files[0] && files[0].size <= 1048576) {
      const reader = new FileReader();
      reader.onload = this._handleReaderLoaded.bind(this);
      reader.readAsBinaryString(files[0]);
    } else if (files[0].size > 1048576) {
      this.eSportsToastService.showInfo(
        this.translateService.instant(
          'VIEW_TOURNAMENT.JOIN_PARTICIPANT.AVATAR_LIMIT'
        )
      );
    }
  }

  _handleReaderLoaded(readerEvt) {
    const binaryString = readerEvt.target.result;
    this.participantForm
      .get('logo')
      .setValue('data:image/png;base64,' + btoa(binaryString));
  }

  /**
   * Save Participant Details
   */
  submit = async () => {
    try {
      if (this.tournament?.isPrize) {
        if (this.user && !this.user?.accountDetail?.isPaymentAccountVerified) {
          this.eSportsToastService.showInfo(
            this.translateService.instant(
              'VIEW_TOURNAMENT.JOIN_PARTICIPANT.ACCOUNT'
            )
          );
          return;
        }
      }
      if (this.participantForm.invalid) {
        this.participantForm.markAllAsTouched();
        return;
      }

      if (!this.user) {
        this.eSportsToastService.showError('Please login');
        return;
      }
      this.isProcessing = true;
      this.isLoaded = true;
      const { value } = this.participantForm;
      value.participantType = this.tournament?.participantType;
      value.tournamentId = this.tournament?._id;
      value.seasonId = this.tournament?._id;
      // value.phoneNumber = value.phoneNumber.e164Number;
      // value.teamMembers = value.teamMembers.map((tM) => {
      //   tM.phoneNumber = tM.phoneNumber.e164Number;
      //   return tM;
      // });
      // value.substituteMembers = value.substituteMembers.map((tM) => {
      //   tM.phoneNumber = tM.phoneNumber.e164Number;
      //   return tM;
      // });
      const response = value?.id
        ? await this.tournamentService
            .updateParticipant(API, value?.id, value)
            .toPromise()
        : await this.tournamentService.saveParticipant(API, value).toPromise();
      if (response) {
        if (this.user) {
          this.tournament.username = this.user.username
            ? this.user.username
            : this.user.username;
          this.tournament.loggedInStatus = 'LoggedIn';
        } else {
          this.tournament.username = '';
          this.tournament.loggedInStatus = 'Guest';
        }
      }
      this.userService.refreshCurrentUser(API, TOKEN);
      this.eSportsToastService.showSuccess(response?.message);
      this.isProcessing = false;
      this.isLoaded = false;
      this.requestProcessing = false;
      this.participantForm.reset();
      this.navigate();
    } catch (error) {
      this.isProcessing = false;
      this.isLoaded = false;
      this.requestProcessing = false;
      this.eSportsToastService.showError(
        error?.error?.message || error?.message
      );
    }
  };

  navigate() {
    const pId = this.activatedRoute?.snapshot?.params?.pId;
    const joinEdit = this.activatedRoute?.snapshot?.queryParams?.success;
    if (this.tournament.isPaid) {
      this.router.navigate(['/tournament/' + this.tournament?.slug + '/pay']);
      return;
    }

    if (pId) {
      this.router.navigate(['/tournament/manage/' + this.tournament?.slug]);
    } else if (joinEdit) {
      this.router.navigate(['profile/my-tournament/joined'], {
        queryParams: { activeTab: 1 },
      });
    } else if (this.tournament.isSeason) {
      this.router.navigate(['/games/match-season/' + this.tournament?._id], {
        queryParams: { playnow: 'true' },
      });
    } else {
      this.router.navigate(['tournament/' + this.tournament?.slug]);
    }
    // this.router.navigate(['/games/match-season/' + this.tournament?._id]);
  }

  patchFormValues = async () => {
    // populate user game id only for matching game
    const userGames = this.user?.preference?.gameDetails || [];
    const game = userGames.find((game) => {
      return game._id === this.tournament?.gameDetail?._id;
    });

    if (this.participantForm && this.user) {
      const formValue = {
        name: this.user?.username,
        // phoneNumber: this.user?.phoneisVerified ? this.user?.phoneNumber : '',
        email: this.user?.emailisVerified ? this.user?.email : '',
        logo: this.user?.profilePicture || '',
        inGamerUserId: game?.userGameId || '',
      };

      this.participantForm.patchValue({
        ...formValue,
      });

      if (game?.userGameId) {
        await this.checkGamerId();
      }
      if (!this.user.emailisVerified) {
        this.participantForm
          .get('email')
          .setValidators([Validators.required, Validators.email]);
        this.participantForm.get('email').updateValueAndValidity();
      }
      return true;
    }
  };

  fetchParticipantRegistrationStatus = async (id: string) => {
    try {
      const { data } =
        await this.tournamentService.fetchSeasonParticipantRegistrationStatus(
          id
        );

      this.participantJoinStatus = data?.isRegistered;
    } catch (error) {
      this.eSportsToastService.showError(
        error?.error?.message || error?.message
      );
    }
  };

  /*getPaymentAccountStatus = async () => {
    try {
      const auth = await this.authServices.getPaymentAccountVerfiedStatus();
    } catch (error) {
      this.eSportsToastService.showError(
        error?.error?.message || error?.message
      );
    }
  };*/

  addTeamControls = async (member, substituteMember) => {
    for (let i = 0; i < member; i++) {
      this.addMember('teamMembers');
    }

    for (let i = 0; i < substituteMember; i++) {
      this.addMember('substituteMembers');
    }
  };

  checkGamerId = async () => {
    try {
      const value = this.participantForm.get('inGamerUserId').value.trim();
      if (!value) return;
      const response = await this.tournamentService
        .searchParticipant(
          API,
          'inGamerUserId',
          value,
          this.tournament?._id,
          this.participantForm.value?.id
        )
        .toPromise();
      const isNameAvailable = response.data.isExist;
      this.participantForm
        .get('inGamerUserId')
        .setErrors(
          !value
            ? { required: true }
            : isNameAvailable
            ? { isNameAvailable }
            : response.data.invalidId
            ? { invalidId: true }
            : null
        );

      if (this.tournament?.participantType === 'individual') {
        this.participantForm.get('teamName').setValue(value);
        this.participantForm.get('teamName').clearValidators();
        this.participantForm.get('teamName').updateValueAndValidity();
      }
    } catch (error) {
      this.eSportsToastService.showError(
        error?.error?.message || error?.message
      );
    }
  };

  showPaymentOptions() {
    if (this.tournament?.isPrize) {
      if (this.user && !this.user?.accountDetail?.isPaymentAccountVerified) {
        this.eSportsToastService.showInfo(
          this.translateService.instant(
            'VIEW_TOURNAMENT.JOIN_PARTICIPANT.ACCOUNT'
          )
        );
        return;
      }
    }

    if (this.participantForm.invalid) {
      this.participantForm.markAllAsTouched();
      return;
    }

    if (!this.user) {
      this.eSportsToastService.showError('Please login');
      return;
    }
    this.showPayment = true;
  }

  joinPaidParticipant({ error, response }) {
    this.showPayment = false;
    if (!error) {
      this.submit();
    }
  }

  isFieldReadOnly(
    _formGroupName: string,
    _formGroup: FormGroup,
    _formControl: string
  ) {
    if (
      !Object.prototype.hasOwnProperty.call(this.originalValues, _formGroupName)
    ) {
      this.originalValues[_formGroupName] = {};
    }
    if (
      !Object.prototype.hasOwnProperty.call(
        this.originalValues[_formGroupName],
        _formControl
      )
    ) {
      this.originalValues[_formGroupName][_formControl] =
        _formGroup.get(_formControl).value;
    }
    if (this.originalValues[_formGroupName][_formControl]) {
      return true;
    } else {
      return false;
    }
  }
  setRegistrationOpenStatus() {
    const now = new Date().getTime();
    const tournamentStartTime = new Date(this.tournament?.startDate).getTime();
    const regStartTime = new Date(this.tournament?.regStartDate).getTime();
    const regEndTime = new Date(this.tournament?.regEndDate).getTime();

    if (this.tournament?.isRegistrationClosed) {
      this.registrationStatus.isClosed = true;
      return;
    }
    if (this.tournament.isSeeded || now > tournamentStartTime) {
      this.registrationStatus.isClosed = true;
      return;
    }

    if (this.tournament['isRegStartDate'] && now > regEndTime) {
      this.registrationStatus.isClosed = true;
      return;
    }

    if (this.tournament['isRegStartDate'] && regStartTime > now) {
      this.registrationStatus.notYetStarted = true;
      return;
    }
    this.registrationStatus.isOpen = true;
  }
}
