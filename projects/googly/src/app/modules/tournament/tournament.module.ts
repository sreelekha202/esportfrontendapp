import { NgModule } from '@angular/core';
import { TournamentRoutingModule } from './tournament-routing.module';
@NgModule({
  declarations: [],
  imports: [TournamentRoutingModule]
})
export class TournamentModule { }
