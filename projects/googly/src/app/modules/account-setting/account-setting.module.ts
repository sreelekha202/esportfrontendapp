import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AccountSettingRoutingModule } from './account-setting-routing.module';
import { AccountSettingComponent } from './account-setting/account-setting.component';

import { RouterBackModule } from './../../shared/directives/router-back.module';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ClipboardModule } from '@angular/cdk/clipboard';
import { InlineSVGModule } from 'ng-inline-svg';
import { LazyLoadImageModule } from 'ng-lazyload-image';

import { FormComponentModule } from '../../shared/components/form-component/form-component.module';
import { I18nModule, EsportsLoaderModule,  PipeModule } from 'esports';
import { MaterialModule } from '../../shared/modules/material.module';
import { CardItemComponent } from './components/card-item/card-item.component';
import { FooterGooglyModule } from '../../shared/components/footer-googly/footer-googly.module';

/* import { SettingsLightboxComponent } from './settings-lightbox.component';
import { AccountsComponent } from './accounts/accounts.component';
import { PasswordsComponent } from './passwords/passwords.component';
import { PreferencesComponent } from './preferences/preferences.component';
import { ProfileComponent } from './profile/profile.component';
import { ReferralsComponent } from './referrals/referrals.component';
import { GameAccountsComponent } from './accounts/game-accounts/game-accounts.component';
import { SocialAccountsComponent } from './accounts/social-accounts/social-accounts.component';
import { AccountItemComponent } from './components/account-item/account-item.component';
import { CardItemComponent } from './components/card-item/card-item.component';
import { ReferralCardComponent } from './components/referral-card/referral-card.component';
import { ChangePhonenumberComponent } from './passwords/components/change-phonenumber/change-phonenumber.component';
import { ChangeEmailComponent } from './passwords/components/change-email/change-email.component';
import { ChangePasswordComponent } from './passwords/components/change-password/change-password.component';
 */

import { environment } from '../../../environments/environment';
import { ProfileComponent } from './components/profile/profile.component';
import { GameAccountsComponent } from './components/game-accounts/game-accounts.component';
import { BlockedListComponent } from './components/blocked-list/blocked-list.component';
import { AccountPreferenceComponent } from './components/account-preference/account-preference.component';
import { AccountsComponent } from './components/accounts/accounts.component';

@NgModule({
  declarations: [
    AccountSettingComponent,
    ProfileComponent,
    GameAccountsComponent,
    BlockedListComponent,
    AccountPreferenceComponent,
    AccountsComponent,
    CardItemComponent,
  ],

  imports: [
    AccountSettingRoutingModule,
    ClipboardModule,
    CommonModule,
    FormsModule,
    I18nModule.forRoot(environment),
    InlineSVGModule,
    LazyLoadImageModule,
    MaterialModule,
    ReactiveFormsModule,
    RouterBackModule,
    PipeModule,
    FooterGooglyModule,
    EsportsLoaderModule
  ],
})
export class AccountSettingModule {}
