import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AccountSettingComponent } from './account-setting/account-setting.component';
import { ProfileComponent } from './components/profile/profile.component';
import { GameAccountsComponent } from './components/game-accounts/game-accounts.component';
import { BlockedListComponent } from './components/blocked-list/blocked-list.component';
import { AccountPreferenceComponent } from './components/account-preference/account-preference.component';
import { AccountsComponent } from './components/accounts/accounts.component';

const routes: Routes = [
  {
    path: '',
    component: AccountSettingComponent,

    children: [
      {
        path: '',
        component: ProfileComponent,
      },
      {
        path: 'profile',
        component: ProfileComponent,
      },
      {
        path: 'gameAccounts',
        component: GameAccountsComponent,
      },
      {
        path: 'blockedList',
        component: BlockedListComponent,
      },
      {
        path: 'accounts',
        component: AccountsComponent,
      },
      {
        path: 'Account_Preference',
        component: AccountPreferenceComponent,
      },
    ],
  },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AccountSettingRoutingModule {}
