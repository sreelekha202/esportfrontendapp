import {
  EsportsToastService,
  EsportsUserService,
  EsportsInfoPopupComponentData,
  EsportsInfoPopupComponentType,
  EsportsInfoPopupComponent,
} from 'esports';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';

import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { TemplateRef, ViewChild } from '@angular/core';
import { environment } from '../../../../../environments/environment';
import { TranslateService } from '@ngx-translate/core';

const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;
@Component({
  selector: 'app-accounts',
  templateUrl: './accounts.component.html',
  styleUrls: ['./accounts.component.scss'],
})
export class AccountsComponent implements OnInit {
  phonenumber = false;
  passworddiv = true;
  emailchange = false;
  passwordchange = false;
  selectedOption: string;
  showLoader: boolean;
  currentUser: any;
  userSubscription: Subscription;
  addForm: FormGroup;
  editImageUrl: any = null;
  selectedFile: any;
  name = 'Dynamic Add Fields';
  values = [];
  isNotConfirm: boolean;
  dialogtype: any;
  // gender = [
  //   { title: 'Female', value: 'female' },
  //   { title: 'Male', value: 'male' },
  // ]
  gender = [
    { title: 'She/Her', value: 'she' },
    { title: 'He/Him', value: 'he' },
    { title: 'They/Them', value: 'they' },
    { title: 'Custom', value: 'Custom' },
  ];

  stateList: any = [];
  countryList: any = [];
  base64textString: any;
  isExist: any;

  form = this.formBuilder.group({
    dobPolicy: [false],
  });
  todayDate: Date = new Date(
    new Date().setFullYear(new Date().getFullYear() - 10)
  );
  parentalDate: Date = new Date(
    new Date().setFullYear(new Date().getFullYear() - 18)
  );
  parental = false;
  @Output() dateChange = new EventEmitter();
  @ViewChild('confirmationDialog') confirmationDialog: TemplateRef<any>;

  constructor(
    private userService: EsportsUserService,
    private fb: FormBuilder,
    private toastService: EsportsToastService,
    private dialog: MatDialog,
    private formBuilder: FormBuilder,
    private translateService: TranslateService
  ) {
    this.addForm = this.fb.group({
      email: [null, Validators.compose([Validators.required])],
      phonenumber: [null, Validators.compose([Validators.required])],
      password: [null, Validators.compose([Validators.required])],
      oldpass: [null, Validators.compose([Validators.required])],
      newpass: [null, Validators.compose([Validators.required])],
      confpass: [null, Validators.compose([Validators.required])],
    });
    this.userService.getAllCountries().subscribe((res) => {
      this.countryList = res.countries;
    });
  }

  ngOnInit(): void {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.currentUser = data;
        this.setUserData();
      }
    });
  }

  onphoneChange(data) {}
  onemailChange(data) {}
  passwordChange(data) {}
  setUserData() {
    (this.editImageUrl = this.currentUser.profilePicture),
      this.addForm.patchValue({
        email: this.currentUser.email,
        phonenumber: this.currentUser.phoneNumber,
        password: this.currentUser.isPassword,
        oldpass: this.currentUser.oldpass,
        newpass: this.currentUser.newpass,
        confpass: this.currentUser.confpass,
      });
  }

  phonenumberComp() {
    this.passworddiv = false;
    this.phonenumber = true;
  }
  emailComp() {
    this.passworddiv = false;
    this.emailchange = true;
  }
  passwordComp() {
    this.passworddiv = false;
    this.passwordchange = true;
  }
  onSubmitEmail(data) {
    this.showLoader = true;
    if (data) {
      const queryData = {
        email: data.email,
      };
      // for (let key in this.addForm.value) { formData.append(`${key}`, this.addForm.value[key]); }
      this.userService.addEmail(API, queryData).subscribe(
        (res) => {
          this.showLoader = false;
          this.toastService.showSuccess(res.message);
          this.userService.refreshCurrentUser(API, TOKEN);
        },
        (err) => {
          this.showLoader = false;
          this.toastService.showError(err.message);
        }
      );
    }
  }

  onSubmit(formData: any) {
    this.showLoader = true;
    if (this.addForm.valid) {
      // for (let key in this.addForm.value) { formData.append(`${key}`, this.addForm.value[key]); }
      let data = {
        old_password: this.addForm.value.oldpass,
        new_password: this.addForm.value.newpass,
      };

      this.userService.changePassword(API, data).subscribe(
        (res) => {
          this.showLoader = false;
          this.setUserData();
          this.toastService.showSuccess(res.message);
          this.onClose();
          this.userService.refreshCurrentUser(API, TOKEN);
        },
        (err) => {
          this.showLoader = false;
          this.toastService.showError(err.message);
        }
      );
    }
  }

  confirmpassword() {
    if (
      this.addForm.controls.newpass.value !=
      this.addForm.controls.confpass.value
    ) {
      this.isNotConfirm = true;
    } else {
      this.isNotConfirm = false;
    }
  }

  function() {
    this.selectedOption;
  }
  addvalue() {
    this.values.push({ value: '' });
  }

  onClose(): void {}

  fileChangeEvent(fileInput: any) {
    const reg = /(.*?)\.(jpg|jpeg|png|gif|giff)$/;
    if (!fileInput.target.files[0].name.match(reg)) {
      this.toastService.showError('Please select valid file');
      this.removeFile();
      return false;
    } else {
      this.removeFile();
      this.selectedFile = fileInput.target.files[0];
      var reader = new FileReader();

      reader.readAsBinaryString(this.selectedFile);
    }
  }

  title: any;
  msg: any;
  label: any;
  inputType: any;

  //used behalf of  isShowPopup
  showEditPopUp(input) {
    if (input == 'email') {
      this.title = this.translateService.instant(
        'ACCOUNT_SETTINGS.ACCOUNTS.VERIFYEMAIL'
      );
      this.msg = this.translateService.instant(
        'ACCOUNT_SETTINGS.ACCOUNTS.EMAILOTPMESSAGE'
      );
      this.label = this.translateService.instant(
        'ACCOUNT_SETTINGS.ACCOUNTS.EMAILADDRESS'
      );
      this.inputType= "email"
    }
    if (input == 'phone') {
      this.title = this.translateService.instant(
        'ACCOUNT_SETTINGS.ACCOUNTS.VERIFYPHONE'
      );
      this.msg = this.translateService.instant(
        'ACCOUNT_SETTINGS.ACCOUNTS.PHONEOTPMESSAGE'
      );
      this.label = this.translateService.instant(
        'ACCOUNT_SETTINGS.ACCOUNTS.PHONENUMBER'
      );
      this.inputType= "phone"
    }

    const Data: EsportsInfoPopupComponentData = {
      title: this.title,
      text: this.msg,
      label: this.label,
      type: EsportsInfoPopupComponentType.editInfo,
      btnText: this.translateService.instant('HEADER_SPARK.CONFIRM'),
      cancelBtnText: this.translateService.instant('HEADER_SPARK.CANCEL'),
      inputType: this.inputType
    };

    const dialogRef = this.dialog.open(EsportsInfoPopupComponent, {
      data: Data,
    });
    dialogRef.afterClosed().subscribe((confirmed) => {
      if (confirmed) {
      }
    });
  }

  removeFile() {
    this.selectedFile = null;
  }
  onresetForm() {
    this.addForm.reset();
    // this.setUserData();
  }

  ngOnDestroy(): void {
    if (this.userSubscription) this.userSubscription.unsubscribe();
  }
}
