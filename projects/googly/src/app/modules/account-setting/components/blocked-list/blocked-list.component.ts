import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import {
  EsportsUserService,
  EsportsInfoPopupComponent,
  EsportsInfoPopupComponentData,
  EsportsInfoPopupComponentType,
} from 'esports';
import { environment } from '../../../../../environments/environment';

const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;

import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-blocked-list',
  templateUrl: './blocked-list.component.html',
  styleUrls: ['./blocked-list.component.scss'],
})
export class BlockedListComponent implements OnInit {
  constructor(
    public dialog: MatDialog,
    private userService: EsportsUserService,
    private translateService: TranslateService
  ) {}

  ngOnInit(): void {}

  show = false;

  unblock_Popup() {
    const blockData: EsportsInfoPopupComponentData = {
      title: 'ACCOUNT_SETTINGS.BLOCKED.POPUP_TITLE',
      text: 'ACCOUNT_SETTINGS.BLOCKED.POPUP_QSTN',
      type: EsportsInfoPopupComponentType.info,
      btnText: 'BUTTON.CONFIRM',
      cancelBtnText: 'BUTTON.CANCEL',
    };

    const dialogRef = this.dialog.open(EsportsInfoPopupComponent, {
      data: blockData,
    });

    dialogRef.afterClosed().subscribe((confirmed) => {
      if (confirmed) {
      }
    });
  }
}
