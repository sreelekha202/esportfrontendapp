import { Component, Input, OnInit } from '@angular/core';
import { EsportsHomeService } from 'esports';
import { AppHtmlRoutes } from '../../../app-routing.model';
@Component({
  selector: 'app-tournament-banner',
  templateUrl: './tournament-banner.component.html',
  styleUrls: ['./tournament-banner.component.scss'],
})
export class TournamentBannerComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;
  @Input() params
  constructor(private homeService: EsportsHomeService) {}

  ngOnInit(): void {
    this.getTournamentsSlides();
  }

  getTournamentsSlides() {
    this.homeService.getUpcomingSliderTournament().subscribe(
      (res) => {},
      (err) => {}
    );
  }
}
