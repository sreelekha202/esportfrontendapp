import { Component, OnInit, Input } from '@angular/core';
import { AppHtmlRoutes } from '../../../app-routing.model';

@Component({
  selector: 'app-spotlight-news',
  templateUrl: './spotlight-news.component.html',
  styleUrls: ['./spotlight-news.component.scss'],
})
export class SpotlightNewsComponent implements OnInit {
  @Input() spoLightList;
  AppHtmlRoutes = AppHtmlRoutes;
  constructor() {}

  ngOnInit(): void {
  }
}
