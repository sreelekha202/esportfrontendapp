import { Router } from '@angular/router';
import { Component, Inject, Input, OnInit, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import {
  EsportsHomeService,
  EsportsToastService,
  IUser,
  EsportsLeaderboardService,
  EsportsUserPreferenceService,
  EsportsTournamentService,
  EsportsOptionService,
  EsportsUserService,
  EsportsUtilsService,
  EsportsArticleService,
  EsportsSeasonService,
  EsportsGameService,
  GlobalUtils,
  IPagination,
} from 'esports';

import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';
import { environment } from '../../../environments/environment';
import { tokenName } from '@angular/compiler';
import { AppHtmlGetMatchRoutes, AppHtmlRoutes, AppHtmlProfileRoutes } from '../../app-routing.model';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';
import { LeaderBoard } from '../leaderboard/leaderboard-table/leaderboard-table.component';

const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;
export interface PeriodicElement {
  name: string;
  position: number;
  weight: string;
  symbol: string;
  Sliver: string;
  Bronze: string;
  Score: string;
  action: string
}

@AutoUnsubscribe()
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  @Input() data: LeaderBoard = {
    page: 'leaderboard',
    title: 'All Games',
  };

  AppHtmlRoutes = AppHtmlRoutes;
  AppHtmlProfileRoutes = AppHtmlProfileRoutes;
  AppHtmlGetMatchRoutes = AppHtmlGetMatchRoutes;
  showLoader: boolean = true;
  isBrowser: boolean;
  currentUser: IUser;
  userSubscription: Subscription;
  categoryList;
  categoryId;
  tournaments = [];
  featuredTournaments = [];
  featuredSeasons = [];
  prizeTournaments = [];
  // FEATURED CONTENT
  trendingNews = [];
  spoLightList: any;
  params: any = {};
  seassons = [];
  games = [];
  text = '';
  paginationDetails: any;
  pageSizeOptions = environment.pageSizeOptions;
  prefData = [];
  prefrenceData;
  paginationData = {
    page: 1,
    limit: 4,
    sort: 'startDate',
  };

  participantPage: IPagination = {
    activePage: 1,
    totalItems: 1,
    maxSize: 10,
    itemsPerPage: 10,
  };
  selectedCountry: any = { all: true };
  selectedGame: any = { all: true };
  selectedGames = 'all';
  selectedGameName = '';
  countryList = [];
  ongoingTournaments: any;
  selectedCountryName = '';
  leaderboardData: any = [];
  gameList = [];
  istTournament: any;
  sortedArray;
  gamesShort;
  constructor(
    @Inject(PLATFORM_ID) private platformId,
    private articleService: EsportsArticleService,
    private optionService: EsportsOptionService,
    private userService: EsportsUserService,
    public translateService: TranslateService,
    public utilsService: EsportsUtilsService,
    private leaderboardService: EsportsLeaderboardService,
    private toastService: EsportsToastService,
    private router: Router,
    private homeService: EsportsHomeService,
    private esportsSeasonService: EsportsSeasonService,
    private gameService: EsportsGameService,
    private esportsTournamentService: EsportsTournamentService
  ) {
    this.isBrowser = isPlatformBrowser(platformId);
  }

  ngOnInit(): void {
    this.getAllGames();
    this.getCountries();
    this.filterLeaderboard();
    this.getAllTournaments();
    this.getUserData();
    this.getSpotlightList();
    this.getSeasons();
  }

  getAllTournaments() {
    const params: any = {
      limit: 4,
      page: this.paginationData?.page,
      sort: {"startDate":1},
    }

    this.esportsTournamentService.getPaginatedTournaments(API,params).subscribe(
      (res: any) => {
        this.tournaments = res?.data?.docs;
        this.istTournament = this.tournaments;
        this.setFeaturedTournaments();
      },
      (err: any) => {
        this.showLoader = false;
      }
    );
  }

  getSeasons() {
    this.esportsSeasonService
      .getSeasons(
        API,
        'isActive=true&fields=isFeatured,name,slug,endDate,startDate,image,duration&sort=-createdOn&page=1&limit=4'
      )
      .subscribe((res) => {
        this.seassons = res?.data?.data?.docs;
      });
  }

  async getTournaments() {
    this.showLoader = true;
    const params: any = {
      limit: 4,
      page: this.paginationData?.page,
      status: 1,
    };

    try {
      if (this.currentUser) {
        const tournament =
          await this.esportsTournamentService.fetchMyTournament(params);
        if (tournament) {
          this.ongoingTournaments = tournament?.data?.docs;
          this.paginationDetails = tournament?.data;
          this.paginationData.limit = this.paginationDetails?.limit;
          this.paginationData.page = this.paginationDetails?.page;
          this.showLoader = false;
        }
      }
    } catch (error) {
      this.showLoader = false;
    }
  }

  getUserData(): void {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.currentUser = data;
        this.getTournaments();
        this.currentUser?.preference?.game.forEach((game) => {
          this.prefData.push({ gameDetails: game });
        });
        const preference = JSON.stringify({
          prefernce: this.prefData,
        });

        const pagination = JSON.stringify({
          pagination: this.paginationData,
        });
        let param: any = {
          pagination: pagination,
          prefernce: preference,
        };

        this.homeService.getTournament(param).subscribe(
          (res: any) => {
            this.prefrenceData = res?.data?.docs.slice(0, 4);
          },
          (err: any) => {
            this.showLoader = false;
          }
        );
      }
    });
  }

  getSpotlightList() {
    this.articleService.getTrendingPosts(API).subscribe((res: any) => {
      this.spoLightList = res?.data;
    });
  }

  followUser(apiEndPoint, followUserId) {
    if (followUserId && followUserId?._id && this.currentUser) {
      this.showLoader = true;
      this.leaderboardService
        .followUser(apiEndPoint, followUserId)
        .subscribe((res) => {
          if (res?.success) {
            this.toastService.showSuccess(res?.message);
            this.userService.refreshCurrentUser(API, tokenName);
          }
          this.showLoader = true;
        });
    } else {
      this.router.navigateByUrl('/user/email-login');
    }
  }

  setFeaturedTournaments() {
    this.featuredTournaments = [];
    this.featuredSeasons = [];
    this.prizeTournaments = [];
    this.tournaments.map((item) => {
      if (true) {
        this.featuredTournaments.push(item)
      } else if (item?.isFeature) {

        this.featuredTournaments.push(item);
      }
      item?.isFeature ? this.featuredSeasons.push(item) : '';
      item?.isPrize ? this.prizeTournaments.push(item) : '';
    });

  }
  ngOnDestroy(): void {
    if (this.userSubscription) this.userSubscription.unsubscribe();
  }

  getCountries() {
    this.userService.getAllCountries().subscribe((data) => {
      this.countryList = data.countries;
    });
  }

  getAllGames() {
    this.gameService.getAllGames(API, TOKEN).subscribe(
      (res) => (this.gameList = res.data),
      (err) => { }
    );
  }

  changeTab(value, type) {
    switch (type) {
      case 'regions': {
        if (value != 'all') {
          const country = this.countryList.find((o) => o.id === value);
          this.selectedCountryName = country.name;
          this.filterLeaderboard();
        } else {
          this.selectedCountryName = '';
          this.filterLeaderboard();
        }
        break;
      }
      case 'games': {
        if (value != 'all') {
          const game = this.gameList.find((o) => o._id === value);
          this.selectedGameName = game.name;
          this.selectedGames = game._id;
          this.filterLeaderboard();
        } else {
          this.selectedGameName = value;
          this.selectedGames = 'all';
          this.filterLeaderboard();
        }
        break;
      }
    }
  }

  filterLeaderboard(page = 1) {
    this.selectedGame = {};
    this.selectedGame[this.selectedGames] = true;
    this.showLoader = true;

    const params = {
      country: this.selectedCountryName,
      gameId: this.selectedGames,
      limit: this.paginationData.limit,
      page: page,
      state: null,
    };

    if (this.data.page === 'home') {
      this.homeService._leaderBoard().subscribe((data) => {
        this.showLoader = false;
        this.leaderboardData = data.data.leaderboardData;

        if (this.leaderboardData) {
          this.leaderboardData.forEach((obj, index) => {
            obj.rank = index + 1;
          });
        }
      });
    } else {
      this.leaderboardService.getGameLeaderboard(API, params).subscribe(
        (res: any) => {
          this.showLoader = false;
          this.leaderboardData = [];

          if (res.data.docs) {
            let count = 0;
            let userData: any = {};

            for (const data of res.data.docs) {
              if (data.user.length > 0) {
                userData.id = data._id.user;
                userData.rank = count + 1;
                count++;
                userData.img =
                  data.user[0].profilePicture == ''
                    ? './assets/images/Profile/formation.svg'
                    : data.user[0].profilePicture;
                userData.name = data.user[0].username;
                userData.region = data.user[0].country
                  ? data.user[0].country
                  : '';
                userData.trophies = {};
                userData.trophies.first = data.firstPosition;
                userData.trophies.second = data.secondPosition;
                userData.trophies.third = data.thirdPosition;
                userData.points = data.points;
                userData.amount = data.amountPrice;
                userData.gamesCount = data.gamesCount;
                this.paginationDetails = res?.data;
                this.participantPage.itemsPerPage = this.paginationDetails?.limit;
                this.participantPage.totalItems = this.paginationDetails?.totalDocs;
                this.leaderboardData.push({ ...userData });
                userData = {};
              } else {
                this.participantPage.itemsPerPage = this.paginationDetails?.limit;
                this.participantPage.totalItems = 0;
              }
            }
          }
        },
        (err) => {
          this.showLoader = false;
        }
      );
    }
  }

}
