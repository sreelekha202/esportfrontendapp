import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TournamentCardHomeComponent } from './tournament-card-home.component';

describe('TournamentCardHomeComponent', () => {
  let component: TournamentCardHomeComponent;
  let fixture: ComponentFixture<TournamentCardHomeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TournamentCardHomeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TournamentCardHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
