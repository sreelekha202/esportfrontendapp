import { Component, OnInit } from '@angular/core';
import {ThemePalette} from '@angular/material/core';
import {IPagination, UserReportsService} from 'esports';
import {  ActivatedRoute} from '@angular/router';
import { environment } from '../../../../environments/environment';

import { Location } from '@angular/common';

export interface Task {
  name: string;
  completed: boolean;
  color: ThemePalette;
  subtasks?: Task[];
}

@Component({
  selector: 'app-user-reports',
  templateUrl: './user-reports.component.html',
  styleUrls: ['./user-reports.component.scss'],
})
export class UserReportsComponent implements OnInit {
  userReport = [];
  currentLang: string = 'english';
  userReportPerPage = [5, 10, 20];
  showLoader: boolean = true;
  slicesuserReport = [];
  searchText: string = '';
  tournamentDetails: any;
  tournamentId:any;
  paginationData = {
    page: 1,
    limit: 10,
    sort: 'startDate',
  };
  paginationDetails: any;
  pageSizeOptions = environment.pageSizeOptions;

  participantPage: IPagination = {
    activePage: 1,
    totalItems: 1,
    maxSize: 10,
    itemsPerPage: 10,
  };



  constructor(
    private location:Location,
    private activeRoute: ActivatedRoute,
    private UserReportsService: UserReportsService
  ) {}

  ngOnInit(): void {


    const tournamentId  = this.activeRoute.snapshot.params;
    if(tournamentId){
      this.tournamentId=tournamentId?.tournamentId
      this.getContent();
    }

  }
  onSelectAll(): void {}

  getContent(){
    // this.participantPage.itemsPerPage = this.tournaments?.limit;
    // this.participantPage.totalItems = this.tournaments?.totalDocs;
    this.UserReportsService.getTournamentsUserReport(environment.apiEndPoint,
      this.tournamentId,
      this.participantPage.itemsPerPage,
      this.participantPage.totalItems
    ).subscribe((res: any) => {
      this.showLoader = false;
         this.userReport = res?.data?.docs;
      },
      (err) => {
         this.showLoader = false;
      }
    );
  }

  onPageChange(event): void {
    const startIndex = event.pageIndex * event.pageSize;
    let endIndex = startIndex + event.pageSize;
    if (endIndex > this.userReport.length) {
      endIndex = this.userReport.length;
    }
    this.slicesuserReport = this.userReport.slice(startIndex, endIndex);
  }
  currentPage(page): void {
    this.participantPage.activePage = page;
     this. getContent();
  }

  onTextChange(data: string) {
    this.searchText = data;
  }

  filterSlicesUserReport = () => {
    return this.slicesuserReport.filter((obj) => obj.fullText.includes(this.searchText.trim()))
  }

  pageChanged(page): void {
    this.paginationData.page = page.pageIndex + 1;
    this.paginationData.limit = page.pageSize;
    this.getContent();

  }

  task: Task = {
    name: 'Indeterminate',
    completed: false,
    color: 'primary',
    subtasks: [
      {name: 'Primary', completed: false, color: 'primary'},
      {name: 'Accent', completed: false, color: 'accent'},
      {name: 'Warn', completed: false, color: 'warn'}
    ]
  };

  allComplete: boolean = false;

  updateAllComplete() {
    this.allComplete = this.task.subtasks != null && this.task.subtasks.every(t => t.completed);
  }

  someComplete(): boolean {
    if (this.task.subtasks == null) {
      return false;
    }
    return this.task.subtasks.filter(t => t.completed).length > 0 && !this.allComplete;
  }

  setAll(completed: boolean) {
    this.allComplete = completed;
    if (this.task.subtasks == null) {
      return;
    }
    this.task.subtasks.forEach(t => t.completed = completed);
  }

  toAttachment(){
    // this.router.navigateByUrl('/manage-tournament/reports/single-item')
  }
}
