import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-edit-scoring',
  templateUrl: './edit-scoring.component.html',
  styleUrls: ['./edit-scoring.component.scss']
})
export class EditScoringComponent implements OnInit {

  constructor() { }
  active = 1;
  nextId: number = 1;
  ngOnInit(): void {
  }
  myTeams = [
    {
      image:
        'https://i115.fastpic.ru/big/2021/0711/d5/e284e76b3d0de8760861e2d97642e5d5.png',
      name: 'Nexplay Empress',
      teamsLength: 5,
      createtdAt: 1,
    },
    {
      image:
        'https://i115.fastpic.ru/big/2021/0711/bc/3c1e95f844d101e292be66deca3b09bc.png',
      name: 'Dark Ninja',
      teamsLength: 4,
      createtdAt: 1,
    },
  ];
  navChanges = (e) => {
    this.nextId = e.nextId;
    switch (this.nextId) {
      case 1:
        // this.getMyTeams();
        break;
      case 2:
        // this.getMyInvite();
        break;
    }
  }
}
