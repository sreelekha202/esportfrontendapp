import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditScoringComponent } from './edit-scoring.component';

describe('EditScoringComponent', () => {
  let component: EditScoringComponent;
  let fixture: ComponentFixture<EditScoringComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditScoringComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditScoringComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
