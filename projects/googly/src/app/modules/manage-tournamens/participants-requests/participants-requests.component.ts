import { Component, OnInit } from '@angular/core';
import { AppHtmlRoutes } from '../../../app-routing.model';
import { Router } from '@angular/router';

import { environment } from '../../../../environments/environment';
import {
  EsportsParticipantService,
  EsportsToastService,
  EsportsTournamentService,
  EsportsUserService,
  IPagination,
  ITournament,
  IUser,
} from 'esports';
const API = environment.apiEndPoint;
@Component({
  selector: 'app-participants-requests',
  templateUrl: './participants-requests.component.html',
  styleUrls: ['./participants-requests.component.scss'],
})
export class ParticipantsRequestsComponent implements OnInit {

  tournament: ITournament;
  currentTab: string;
  user: IUser;
  params: any = {};
  paginationDetails: any;
  pageSizeOptions = environment.pageSizeOptions;
  paginationData = {
    page: 1,
    limit: 10,
    sort: 'startDate',
  };

  participantPage: IPagination = {
    activePage: 1,
    totalItems: 1,
    maxSize: 10,
    itemsPerPage: 100,
  };
  selectedTeam = null;
  isRegistrationClosed = false;
  showRegCloseBtn = false;
  isProccessing: boolean = false;
  participantData: any;
  AppHtmlRoutes = AppHtmlRoutes;
  active = 1;
  nextId: number = 1;
  myTeams = [];
  requestsData = [];
  showLoader: boolean = true;
  tournamentDetails: any;
  slug: any

  constructor(
    private esportsTournamentService: EsportsTournamentService,
    private userService: EsportsUserService,
    private participantService: EsportsParticipantService,
    private eSportsToastService: EsportsToastService,
    private router: Router,
  ) {}

  ngOnInit(): void {
    this.setData();
  }

  setData() {
    this.currentTab = 'pendingParticipants';
    this.esportsTournamentService.manageTournament.subscribe((data) => {
      if (data) {
        this.tournament = data;
        if (this.currentTab && this.tournament?._id) {
          this.selectedTeam = null;
          this.setParticipant(this.participantPage?.activePage);
        }

        if (this.tournament?._id) {
          this.isRegistrationClosed = this.tournament?.isRegistrationClosed;

          if (this.tournament?.regStartDate && this.tournament?.regEndDate) {
            const currentDate = new Date();
            const registrationStartDt = new Date(this.tournament?.regStartDate);
            const registrationEndDt = new Date(this.tournament?.regEndDate);

            /** Check current date is lie between registartion start date and registration end date */
            const withInRange =
              registrationEndDt > currentDate &&
              currentDate > registrationStartDt;
            this.showRegCloseBtn = withInRange;
          }
        }
      }
    });
    this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.user = data;
      }
    });
  }

  setParticipant(page: number = 1) {
    switch (this.currentTab) {
      case 'pendingParticipants':
        this.fetchParticipantByStatus(2, page);
        break;
      case 'approvedParticipants':
        this.fetchParticipantByStatus(1, page);
        break;
      case 'rejectedParticipants':
        this.fetchParticipantByStatus(2, page);
        break;
      default:
        this.fetchParticipantByStatus(0, page);
        break;
    }
  }

  navChanges = (e) => {
    this.nextId = e.nextId;
    switch (this.nextId) {
      case 1:
        this.params.type = 0;
        this.getData();
        break;
      case 2:
        this.params.type = 2;
        this.getData();
        break;
    }
  };

  getData() {
    // this.showLoader = true;
    // this.profileService.getParticipant(this.params).subscribe(
    //   (res) => {
    //     this.showLoader = false;
    //     this.requestsData = res?.data?.docs;
    //   },
    //   (err) => {
    //     this.showLoader = false;
    //   }
    // );
  }

  fetchParticipantByStatus = async (type: number, page = 1) => {
    try {
      if (this.isProccessing) return;
      this.isProccessing = true;
      const { data } = await this.participantService.fetchParticipantByStatus(
        API,
        type,
        this.tournament._id,
        page
      );
      this.participantPage.itemsPerPage = data?.limit;
      this.participantPage.totalItems = data?.totalDocs;
      this.participantData = data;
      this.paginationDetails = data;
      this.paginationData.limit = this.paginationDetails.limit;
      this.paginationData.page = this.paginationDetails.page;
      this.isProccessing = false;
    } catch (error) {
      this.isProccessing = false;
      this.eSportsToastService.showError(
        error?.error?.message || error?.message
      );
    }
  };

  pageChanged(page): void {
    this.paginationData.page = page.pageIndex + 1;
    this.paginationData.limit = page.pageSize;
    this.navChanges({ nextId: this.nextId });
  }

  onReload = (a) => {
     a ? this.setParticipant() : '';
  };

  refresh(id) {
    this.participantData.docs.forEach((e, i) => {
      if (e._id == id) {
        this.participantData.docs.splice(i, 1)
      }
    });
  };
 
  currentPage(page: number) {
    this.setParticipant(page);
  }

  checkBlank() {
    return this.router.url == "/manage-tournament/participants-requests-blank";
  }


}
