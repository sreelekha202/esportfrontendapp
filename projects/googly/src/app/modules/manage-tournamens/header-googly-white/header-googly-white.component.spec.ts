import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderGooglyWhiteComponent } from './header-googly-white.component';

describe('HeaderGooglyWhiteComponent', () => {
  let component: HeaderGooglyWhiteComponent;
  let fixture: ComponentFixture<HeaderGooglyWhiteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HeaderGooglyWhiteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderGooglyWhiteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
