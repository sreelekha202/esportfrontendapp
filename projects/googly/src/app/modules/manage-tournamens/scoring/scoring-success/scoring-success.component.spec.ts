import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ScoringSuccessComponent } from './scoring-success.component';

describe('ScoringSuccessComponent', () => {
  let component: ScoringSuccessComponent;
  let fixture: ComponentFixture<ScoringSuccessComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ScoringSuccessComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ScoringSuccessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
