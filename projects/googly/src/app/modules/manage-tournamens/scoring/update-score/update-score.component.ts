import { Component, OnInit } from '@angular/core';
import { FlatTreeControl } from '@angular/cdk/tree';
import {
  MatTreeFlatDataSource,
  MatTreeFlattener,
} from '@angular/material/tree';
import {
  EsportsBracketService,
  EsportsChatService,
  UserReportsService,
  EsportsToastService,
  EsportsTournamentService,
  EsportsUserService,
  EsportsUtilsService,
  IPagination,
} from 'esports';
import { MatDialog } from '@angular/material/dialog';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { FormGroup } from '@angular/forms';
import { Location } from '@angular/common';
import { environment } from '../../../../../environments/environment';
const API = environment.apiEndPoint;

@Component({
  selector: 'app-update-score',
  templateUrl: './update-score.component.html',
  styleUrls: ['./update-score.component.scss'],
})
export class UpdateScoreComponent implements OnInit {
  round: any;
  windowposition: String = 'chat_window chat_window_right';
  matchAndTournamentID: any;
  active = 0;
  nextId: number = 1;
  tournament: any;
  imgurl;
  page: IPagination = {
    activePage: 1,
    totalItems: 1,
    itemsPerPage: 1,
    maxSize: 10,
  };
  enableWebview: boolean = false;
  tReport: any;
  matchDetails: any;
  rounds:any;
  participantId;
  hasParticipantOneTimeAccess: boolean = true;
  teamList: Array<any> = [];
  disQualifiedTeam: any = {
    reason: '',
    team: null,
  };
  isLoaded: boolean = false;
  token;
  form: FormGroup;
  addForm: FormGroup;

  roundMatches: any;
  defaultWinnerLogo: string =
    'https://i115.fastpic.ru/big/2021/0711/d5/e284e76b3d0de8760861e2d97642e5d5.png';
  participantSet: any;
  isAdmin: boolean;
  count: number;
  votes: number = 0;
  votes1: number = 0;
  votes2: number = 0;
  src = '';
  constructor(
    private toastService: EsportsToastService,
    public esportsChatService: EsportsChatService,
    private bracketService: EsportsBracketService,
    private utilsService: EsportsUtilsService,
    public matDialog: MatDialog,
    private translateService: TranslateService,
    private router: Router,
    private _esportsUserService: EsportsUserService,
    private location: Location,
    private esportsTournamentService: EsportsTournamentService,
    private _userReportsService: UserReportsService
  ) {}

  ngOnInit(): void {

    this.esportsTournamentService.manageTournament.subscribe((data) => {
      if (data) {
        this.tournament = data;
        this.getMatches();
        this.fetchRoundMatches();
      }
    });

    if (this.location.getState()) {
      this.matchAndTournamentID = this.location.getState();
      this.fetchMatchDetails();
    }
  }

  fetchRoundMatches = async () => {
    this.roundMatches = this.bracketService.macthRoundData;
    if (this.roundMatches) {
      this.rounds=this.roundMatches?.round
    } else {
      this.router.navigate([
        'manage-tournament',
        this.tournament.slug,
        'scoring',
      ]);
    }
  };

  showCreatedPopup(): void {
    // this.matDialog.open(PopupCreatedComponent);
  }

  fetchMatchDetails = async () => {
    try {
      this.isLoaded = true;
      const queryParam = `?query=${encodeURIComponent(
        JSON.stringify({
          tournamentId: this.matchAndTournamentID?.tournamentId,
          _id: this.matchAndTournamentID?.matchId,
        })
      )}`;

      const match = await this.bracketService.fetchAllMatches(queryParam);
      this.matchDetails = match?.data?.length ? match.data[0] : null;
      if (this.matchDetails) {
        if (
          this.participantId == this.matchDetails.teamA?._id &&
          this.matchDetails?.matchUpdatedByTeamACount > 0
        ) {
          this.hasParticipantOneTimeAccess = false;
        }

        if (
          this.participantId == this.matchDetails.teamB?._id &&
          this.matchDetails?.matchUpdatedByTeamBCount > 0
        ) {
          this.hasParticipantOneTimeAccess = false;
        }
        this.teamList = [
          { ...this.matchDetails.teamA },
          { ...this.matchDetails.teamB },
        ];
        this.disQualifiedTeam = this.matchDetails?.disQualifiedTeam;
      }
      this.isLoaded = false;
    } catch (error) {
      this.isLoaded = false;
      this.showAlertBasedOnPlatform(
        error?.error?.message || error?.message,
        'showError'
      );
    }
  };

  updateScoree(set, score, team_name, n, team_id) {
    let s, value, key, delta, teamId;
    s = set;
    value = score;
    key = team_name;
    delta = n;
    teamId = team_id;
    if (this.isAdmin && this.roundMatches.isNextMatchStart) {
      // return;
    } else if (!this.isAdmin) {
      if (this.participantId !== teamId) {
        // return;
      }

      if (
        this.participantId === teamId &&
        this.roundMatches.matchStatus === 'completed'
      ) {
        // return;
      }

      if (this.tReport?.tournaments?.scoreReporting <= 1) {
        // return;
      }

      if (s?.status === 'completed') {
        // return;
      }
    }
    // s[key] = s[key] <= 0 && delta === -1 ? value : value + delta;
    s[key] = value;
    s.modify = true;
    this.participantSet = { ...this.participantSet, [key]: s[key], id: s?.id };
  }

  isAllowScore = async (setA, setB) => {
    const isInValid =
      setA == setB &&
      ['swiss_safeis', 'ladder'].includes(
        this.tReport?.tournaments?.bracketType
      );
    if (isInValid) {
      this.toastService.showInfo(
        this.translateService.instant('ELIMINATION.SCORE_ALERT')
      );
    }
    return !isInValid;
  };

  submitParticipantScore = async (set, match) => {
    try {
      this.isLoaded = true;
      const status = await this.isAllowScore(set.teamAScore, set.teamBScore);
      if (!status) return;

      var params = {
        _id: match?._id,
        'sets.id': set.id,
        tournamentId: match?.tournamentId?._id,
      };
      const queryParam = `?query=${encodeURIComponent(JSON.stringify(params))}`;

      // const payload = {
      //   $set: this.convertObject(set),
      // };
      const payload = {
        teamAScore: set.teamAScore,
        teamAScreenShot: null,
        teamBScore: set.teamBScore,
        teamBScreenShot: null,
      };
      const response = await this.bracketService.updateParticipantScore(
        queryParam,
        payload
      );

      this.showAlertBasedOnPlatform(response?.message, 'showSuccess');
      this.isLoaded = false;
      this.router.navigate(['profile/tournaments-created']);
      // this.fetchMatchDetails();
    } catch (error) {
      this.isLoaded = false;
      this.showAlertBasedOnPlatform(
        error?.error?.message || error?.message,
        'showError'
      );
    }
  };

  showAlertBasedOnPlatform = (message, type) => {
    this.enableWebview
      ? this.utilsService.showNativeAlert(message)
      : this.toastService[type](message);
  };

  submitScore = async (set, matchDetail) => {
    try {
      this.isLoaded = true;
      let params = {
        _id: matchDetail?._id,
        'sets.id': set.id,
        tournamentId: matchDetail?.tournamentId?._id,
      };
      const queryParam = `?query=${encodeURIComponent(JSON.stringify(params))}`;

      const payload = {
        teamAScore: set.teamAScore,
        teamAScreenShot: null,
        teamBScore: set.teamBScore,
        teamBScreenShot: null,
      };

      const match = await this.bracketService.updateMatch(queryParam, payload);
      if (!match.success) {
        this.showAlertBasedOnPlatform(match.message, 'showInfo');
      }
      if (match.success) {
        this.toastService.showSuccess(
          this.translateService.instant(
            'MANAGE_TOURNAMENT.MATCHES.SCORE_UPDATED'
          )
        );
        this.router.navigate([
          'manage-tournament',
          this.tournament.slug,
          'scoring',
        ]);
      }

      this.isLoaded = false;
      this.fetchMatchDetails();
    } catch (error) {
      this.isLoaded = false;
      this.token
        ? this.utilsService.showNativeAlert(
            error?.error?.message || error?.message
          )
        : this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  myTeams = [
    {
      image:
        'https://i115.fastpic.ru/big/2021/0711/d5/e284e76b3d0de8760861e2d97642e5d5.png',
      name: 'Nexplay Empress',
      teamsLength: 5,
      createtdAt: 1,
    },
    {
      image:
        'https://i115.fastpic.ru/big/2021/0711/bc/3c1e95f844d101e292be66deca3b09bc.png',
      name: 'Dark Ninja',
      teamsLength: 4,
      createtdAt: 1,
    },
  ];

  navChanges = (e) => {
    this.nextId = e.nextId;
    switch (this.nextId) {
      case 1:
        // this.getMyTeams();
        break;
      case 2:
        // this.getMyInvite();
        break;
    }
  };

  datatoimg(data) {
    this.imgurl = data;
  }

  genrateReport() {
    if (this.matchAndTournamentID) {
      this.router.navigate([`/report-issue`], {
        queryParams: {
          matchId: this.matchAndTournamentID?.matchId,
          tournamentId: this.matchAndTournamentID?.tournamentId,
        },
      });
    }
    // if (!this.enableWebview) {
    //   this.exit.emit({ refresh: true, isOpenScoreCard: false });
    //   this.router.navigate([`/report-issue/${this.matchDetails?._id}/${this.matchDetails?.tournamentId?._id}`,]);

    // }
  }

  chatFunction(id) {
    this.esportsChatService.getMatchInformation(id).subscribe((result: any) => {
      const mtchdetails = {
        matchid: result.matchdetails._id,

        teamAId: result.matchdetails.teamA.userId._id,

        teamBId: result.matchdetails.teamB.userId._id,

        tournamentid: result.matchdetails.tournamentId,

        typeofchat: 'tournament',

        users: [
          {
            _id: result.matchdetails.teamA.userId._id,
            fullName: result.matchdetails.teamA.userId.fullName,
            profilePicture: result.matchdetails.teamA.userId.profilePicture,
          },
          {
            _id: result.matchdetails.teamB.userId._id,
            fullName: result.matchdetails.teamB.userId.fullName,
            profilePicture: result.matchdetails.teamB.userId.profilePicture,
          },
        ],
      };
      // this.matchdetails = mtchdetails;
      // this.showChat = true;
      // this.typeofchat = 'tournament';
      this.esportsChatService.setWindowPos(this.windowposition);

      let firstChat = this.esportsChatService.getChatStatus();

      if (firstChat == true) {
        this.esportsChatService.setChatStatus(false);
        this.esportsChatService?.disconnectMatch(
          mtchdetails.matchid,
          'tournament'
        );

        this.esportsChatService.setTextButtonStatus(false);
      } else {
        this.esportsChatService.setCurrentMatch(mtchdetails);

        this.esportsChatService.setTypeOfChat('tournament');

        this.esportsChatService.setChatStatus(true);

        this.esportsChatService.setTextButtonStatus(true);
      }
    });
  }

  deleteTeam(team_id) {
    const data = {
      id: team_id,
      admin: true,
      query: {
        condition: { _id: team_id, status: 'active' },
        update: { status: 'inactive' },
      },
    };

    this._esportsUserService.update_team_admin(API, data).subscribe((data) => {
      if (data) {
        // this.refresh.emit(data)
        this.toastService.showSuccess(data.message);
        // this.router.navigate(['profile/my-teams']);
      }
    });
  }

  expandedIndex = 0;

  onIncrement(id,score,team,matchIndex,index,roundId): void {
      if(team=='teamAScore'){
        if(this.rounds?.teamA?._id==id){
          this.rounds.sets.forEach(round => {
            if(round?.id==roundId){
              this.rounds.sets[index].teamAScore +=1
            }
          });
        }
      }

      if(team=='teamBScore'){
        if(this.rounds?.teamB?._id==id){
          this.rounds.sets.forEach(round => {
            if(round?.id==roundId){
            this.rounds.sets[index].teamBScore +=1;
            }
          });

        }
      }

  }

  onDecrement(id,score,team,matchIndex,index,roundId): void {
    if(score>0){
      if(team=='teamAScore'){
        if(this.rounds?.teamA?._id==id){
          this.rounds.sets.forEach(round => {
            if(round?.id==roundId){
              // round.teamAScore -=1;
            this.rounds.sets[index].teamAScore -=1
            }

          });

        }
      }

      if(team=='teamBScore'){
        if(this.rounds?.teamB?._id==id){
          this.rounds.sets.forEach(round => {
            if(round?.id==roundId){
               // this.votes-=1
               this.rounds.sets[index].teamBScore-=1;
            }

          });

        }
      }
    }


  }



  updateScore(e) {
    var reader = new FileReader();
    reader.onload = (event: any) => {
      this.src = event.target.result;
    };
    reader.readAsDataURL(e.target.files[0]);
  }
  getMatches = async () => {
    try {
      const queryParam = `&page=${this.page}`;
      const matchStatus = 'completed';
      const res = await this._userReportsService.getUserMatchesByStatus(
        matchStatus,
        queryParam
      );
      if (res) {
        this.tReport = res?.data?.docs;
      }
    } catch (error) {}
  };
}
