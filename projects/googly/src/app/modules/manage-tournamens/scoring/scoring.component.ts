import { Component, OnInit, Input, OnChanges, OnDestroy } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import {
  EsportsToastService,
  EsportsUtilsService,
  EsportsTournamentService,
  EsportsBracketService,
  IUser,
  EsportsUserService,
} from 'esports';
import { Subscription } from 'rxjs';
import { environment } from '../../../../environments/environment';
const API = environment.apiEndPoint;

@Component({
  selector: 'app-scoring',
  templateUrl: './scoring.component.html',
  styleUrls: ['./scoring.component.scss'],
})
export class ScoringComponent implements OnInit {
  tournament: any = {};
  @Input() participantId: string | null;
  apiLoaded: Array<boolean> = [];
  isLoaded = false;
  structure: any;
  active = 1;
  hideMessage = true;
  user: IUser;
  hasRequiredAccess: boolean = true;
  startDateIntervalId = null;
  endDateIntervalId = null;
  tournamentStartTime = '';
  noStart = false;
  userSubscription: Subscription;
  matchAndTournamentID: any;

  constructor(
    private bracketService: EsportsBracketService,
    private utilsService: EsportsUtilsService,
    private tournamentService: EsportsTournamentService,
    private toastService: EsportsToastService,
    private userService: EsportsUserService,
    private esportsTournamentService: EsportsTournamentService,
    private translateService: TranslateService,
  ) {}

  ngOnInit(): void {}

  ngOnDestroy() {}


  ngAfterViewInit(): void {
    this.esportsTournamentService.manageTournament.subscribe((data) => {
      if (data) {
        this.tournament = data;
      }
    });

    if (this.tournament?._id) {
      this.fetchParticipantRegisterationStatus(this.tournament?._id);
      this.fetchBracket();
    }
  }

   /**
 * Get Current User
 */
    getCurrentUserDetails = async () => {
      this.userSubscription = this.userService.currentUser.subscribe((data) => {
        if (data) {
          this.user = data;
          this.fetchTournamentDetails();
        }
      });
    }

  fetchParticipantRegisterationStatus = async (id) => {
    try {
      this.apiLoaded.push(false);
      const { data } =
        await this.tournamentService.fetchParticipantRegistrationStatus(
          API,
          id
        );
      this.participantId = data?.participantId || null;
      this.apiLoaded.push(true);
    } catch (error) {
      this.apiLoaded.push(true);
      this.toastService.showError(error?.error?.message || error?.message);
    } finally {
      this.checkAllApiDataLoaded();
    }
  };

  /**
   * Fetch All Match If Seed Else Fetch Mock Structure Of The Tournament
   */
  fetchBracket = async () => {
    try {
      this.apiLoaded.push(false);
      if (
        this.tournament?.isSeeded &&
        ['single', 'double'].includes(this.tournament?.bracketType)
      ) {
        const queryParam = `?query=${this.utilsService.encodeQuery({
          tournamentId: this.tournament._id,
        })}`;
        const response = await this.bracketService.fetchAllMatches(queryParam);
        this.structure = this.bracketService.assembleStructure(response.data);
      } else if (!this.tournament?.isSeeded) {
        const payload = {
          bracketType: this.tournament?.bracketType,
          maximumParticipants: this.tournament?.maxParticipants,
          noOfSet: this.tournament?.noOfSet,
          ...(['round_robin', 'battle_royale'].includes(
            this.tournament?.bracketType
          ) && {
            noOfTeamInGroup: this.tournament?.noOfTeamInGroup,
            noOfWinningTeamInGroup: this.tournament?.noOfWinningTeamInGroup,
            noOfRoundPerGroup: this.tournament?.noOfRoundPerGroup,
            stageBracketType: this.tournament?.stageBracketType,
          }),
        };
        const response = await this.bracketService.generateBracket(payload);
        this.structure = response.data;
      }
      this.apiLoaded.push(true);
    } catch (error) {
      this.apiLoaded.push(true);
      this.toastService.showError(error?.error?.message || error?.message);
    } finally {
      this.checkAllApiDataLoaded();
    }
  };

  checkAllApiDataLoaded() {
    const startedApiCount = this.apiLoaded.filter((el) => !el).length;
    const finishedApiCount = this.apiLoaded.filter((el) => el).length;
    this.isLoaded = startedApiCount === finishedApiCount;
  }

  fetchTournamentDetails = async (isTournamentStart: boolean = false, isTournamentFinished: boolean = false) => {
    try {
      this.apiLoaded.push(false);
      const tournament = await this.tournamentService.getTournamentBySlug(this.tournament?.slug)
      this.tournament = tournament?.data;

      if (!this.tournament) {
        this.hideMessage = false;
        throw new Error(this.translateService.instant('VIEW_TOURNAMENT.ERROR'));
      }

      const isEventOrganizer = this.user?._id === this.tournament?.organizerDetail?._id;
      const hasAdminAccess = this.user?.accountType == 'admin' && this.user?.accessLevel?.includes('em');

      this.hasRequiredAccess = isEventOrganizer || hasAdminAccess;
      // if (!this.hasRequiredAccess) {
      //   throw new Error(
      //     this.translateService.instant('VIEW_TOURNAMENT.ACCESS_DENIED')
      //   )
      // }

      if (this.tournament?.isFinished) {
        if (isTournamentFinished) {
          this.toastService.showInfo(
            this.translateService.instant('ELIMINATION.TOURNAMENT_FINISHED'),
            false
          );
        }
        this.tournamentStartTime = this.translateService.instant(
          'MANAGE_TOURNAMENT.TOURNAMENT_FINISHED'
        );
      } else if (this.tournament?.isSeeded) {
        if (isTournamentStart) {
          this.toastService.showInfo(
            this.translateService.instant('ELIMINATION.TOURNAMENT_STARTED'),
            false
          );
        }

        this.tournamentStartTime = this.translateService.instant(
          'MANAGE_TOURNAMENT.TOURNAMENT_STARTED'
        );
        this.endDateTimer();
      } else {
        this.startDateTimer();
        this.noStart = true;
      }
      this.fetchRegisteredParticipant('registeredParticipantCount');
      this.fetchRegisteredParticipant('checkedInParticipantCount', {
        checkedIn: true,
      });
      this.apiLoaded.push(true);
    } catch (error) {
      this.hasRequiredAccess = false;
      this.apiLoaded.push(true);
      this.toastService.showError(error?.error?.message || error?.message);
    } finally {
      this.checkAllApiDataLoaded();
    }
  };

 /**
 * Fatch Register Or CheckedIn Participant
 * @param field registeredParticipantCount, checkedInParticipantCount
 * @param obj for checkedIn users
 */
  fetchRegisteredParticipant = async (field, obj = {}) => {
    try {
      this.apiLoaded.push(false);
      const encodeUrl = `?query=${encodeURIComponent(
        JSON.stringify({ tournamentId: this.tournament?._id, ...obj })
      )}&select=''&option=${encodeURIComponent(
        JSON.stringify({ count: true })
      )}`;
      const response = await this.tournamentService
        .getParticipants(API,encodeUrl)
        .toPromise();
      this[field] = response?.totals?.count;
      this.apiLoaded.push(true);
    } catch (error) {
      this.apiLoaded.push(true);
      this.toastService.showError(error?.error?.message || error?.message);
    } finally {
      this.checkAllApiDataLoaded();
    }
  };

    /**
   * Start date Timer
   */
     startDateTimer = () => {
      const startDate = new Date(this.tournament.startDate);
      const currentDate = new Date();
      if (startDate.getTime() - currentDate.getTime() > 0) {
        const millisecond = startDate.getTime() - currentDate.getTime();
        const dd = (millisecond / (1000 * 60 * 60 * 24)) | 0;
        if (dd) {
          const currentLanguage = this.translateService.currentLang;
  
          if (currentLanguage == 'en') {
            this.tournamentStartTime = `${dd} ${dd === 1
              ? this.translateService.instant('MANAGE_TOURNAMENT.DAYS_LEFT.01')
              : this.translateService.instant('MANAGE_TOURNAMENT.DAYS_LEFT.02')
              }`;
          } else {
            if (dd === 1) {
              this.tournamentStartTime = `${dd} ${this.translateService.instant('MANAGE_TOURNAMENT.DAYS_LEFT.01')}`
            }
            else if (dd === 2) {
              this.tournamentStartTime = `${this.translateService.instant('MANAGE_TOURNAMENT.DAYS_LEFT.02')}`
            }
            else if (dd === 3) {
              this.tournamentStartTime = `${dd} ${this.translateService.instant('MANAGE_TOURNAMENT.DAYS_LEFT.03')}`
            }
            else if (dd === 11) {
              this.tournamentStartTime = `${dd} ${this.translateService.instant('MANAGE_TOURNAMENT.DAYS_LEFT.04')}`
            }
            else {
              this.tournamentStartTime = `${dd} ${this.translateService.instant('MANAGE_TOURNAMENT.DAYS_LEFT.04')}`
            }
          }
          return;
        }
  
        const hh = (millisecond / (1000 * 60 * 60)) | 0;
        const mm = ((millisecond - hh * 1000 * 60 * 60) / (1000 * 60)) | 0;
        const ss = ((millisecond - hh * 1000 * 60 * 60) / 1000) % 60 | 0;
        this.tournamentStartTime = `${hh > 9 ? hh : `0${hh}`}: ${mm > 9 ? mm : `0${mm}`}: ${ss > 9 ? ss : `0${ss}`}`;
  
        if (!this.startDateIntervalId) {
          this.startDateIntervalId = setInterval(() => this.startDateTimer(), 1000);
        }
      } else {
        if (this.startDateIntervalId) {
          this.fetchTournamentDetails(true);
        } else {
          this.tournamentStartTime = this.translateService.instant(
            'MANAGE_TOURNAMENT.TOURNAMENT_EXPIRED'
          );
        }
        clearInterval(this.startDateIntervalId);
        this.startDateIntervalId = null;
      }
    }

    
  /**
   * End date timer
   */
  endDateTimer = () => {
    const endDate = new Date(this.tournament?.endDate);
    const currentDate = new Date();
    const timeDiff = endDate.getTime() - currentDate.getTime();
    if (timeDiff > 0) {
      this.endDateIntervalId = setInterval(() => this.endDateTimer(), 1000);
    } else if (['swiss_safeis', 'ladder'].includes(this.tournament?.bracketType)) {
      if (this.endDateIntervalId) {
        this.fetchTournamentDetails(false, true);
        clearInterval(this.endDateIntervalId);
        this.endDateIntervalId = null;
      }
    }
  }

  set_1 = ['Round 1'];
  set_2 = ['Round 2'];
  set_3 = ['Round 3'];
  expandedIndex = 0;

  onTextChange(data) {}
  data: any = [
    {
      description: {
        english:
          'How are my Fortnite replays stored? Replays are stored locally on your console or PC. On console your last 10...',
        french: '',
      },
      slug: 'testt--600ecd33089c510008c6e57b',
      thumbnailUrl: 'assets/icons/video-player/image 59.png',
      title: { english: 'fortnite battle royale - replay system', french: '' },
      youtubeUrl: 'https://www.youtube.com/watch?v=VPe6-LoYXyQ',
      _id: '600ecd33089c510008c6e57b',
    },
    {
      description: {
        english:
          'ESRB Rating: Teen with violence sign up for the fortnite defending the Fort- Fortnite gameplay...',
        french: '',
      },
      slug: 'testt--600ecd33089c510008c6e57b',
      thumbnailUrl: 'assets/icons/video-player/image 60.png',
      title: { english: 'defending the fort - fortnite gameplay', french: '' },
      youtubeUrl: 'https://www.youtube.com/watch?v=VPe6-LoYXyQ',
      _id: '600ecd33089c510008c6e57b',
    },
    {
      description: {
        english:
          'Checkout some of the new features in Fortnite chapter 2, and we even got a Victory Royale to celebrate...',
        french: '',
      },
      slug: 'testt--600ecd33089c510008c6e57b',
      thumbnailUrl: 'assets/icons/video-player/image 61.png',
      title: {
        english: 'fortnite chapter 2 - victory royale gamepaly',
        french: '',
      },
      youtubeUrl: 'https://www.youtube.com/watch?v=VPe6-LoYXyQ',
      _id: '600ecd33089c510008c6e57b',
    },
  ];

  updateScore(){
    // this.router.navigateByUrl('/manage-tournament/update-score');
  }
}
