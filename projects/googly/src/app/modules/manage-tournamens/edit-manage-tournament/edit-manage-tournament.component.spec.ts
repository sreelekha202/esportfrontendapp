import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditManageTournamentComponent } from './edit-manage-tournament.component';

describe('EditManageTournamentComponent', () => {
  let component: EditManageTournamentComponent;
  let fixture: ComponentFixture<EditManageTournamentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditManageTournamentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditManageTournamentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
