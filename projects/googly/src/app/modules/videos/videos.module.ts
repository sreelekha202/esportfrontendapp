import { VideosRoutingModule } from "./videos-routing.module";
import { CoreModule } from "../../core/core.module";
import { NgModule } from "@angular/core";
import { CreateVideoLibraryComponent } from "./create-video-library/create-video-library.component";
import { VideoViewComponent } from "./video-view/video-view.component";
import { VideosComponent } from "./videos.component";
import { FormComponentModule } from "../../shared/components/form-component/form-component.module";
import { SharedModule } from "../../shared/modules/shared.module";
import { EsportsLoaderModule, EsportsHeaderModule } from 'esports';
import { HeaderGooglyModule } from "../../shared/components/header-googly/header-googly.module";
@NgModule({
  declarations: [
    VideosComponent,
    VideoViewComponent,
    CreateVideoLibraryComponent,
  ],
  imports: [CoreModule, EsportsLoaderModule, VideosRoutingModule, SharedModule, FormComponentModule, HeaderGooglyModule, EsportsHeaderModule],
  providers: [],
})
export class VideosModule { }
