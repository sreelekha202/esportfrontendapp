import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { GlobalUtils } from 'esports';

@Component({
  selector: 'app-popup',
  templateUrl: './popup.component.html',
  styleUrls: ['./popup.component.scss']
})
export class PopupComponent implements OnInit {
  @Output() hidePopup = new EventEmitter();
  date: Date;
  time: any;
  constructor() { }

  ngOnInit(): void {
  }
  onDateChange(data) {
    if (GlobalUtils.isBrowser()) {
      this.date = data;
      localStorage.setItem('date', data);
    }

  }
  onTimeChange(data) {
    this.time = data;
    localStorage.setItem('time', data);
  }
}
