import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppHtmlRoutes } from '../../../../app-routing.model';
import { TranslateService } from '@ngx-translate/core';
import {
  EsportsToastService,
  EsportsTournamentService,
  EsportsGtmService,
} from 'esports';
import { environment } from '../../../../../environments/environment';
const API = environment.apiEndPoint;
@Component({
  selector: 'app-create-quick',
  templateUrl: './create-quick.component.html',
  styleUrls: ['./create-quick.component.scss'],
})
export class CreateQuickComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;
  text: string = '';
  participants: any = [];
  emailsforinvite: any = [];
  tournamentDetails: any = {};
  isLoaded: boolean = false;
  hideMessage: boolean = false;
  tournamentId: any;

  constructor(
    private router: Router,
    private tournamentService: EsportsTournamentService,
    private translateService: TranslateService,
    private gtmService: EsportsGtmService,
    private eSportsToastService: EsportsToastService,
  ) {}

  ngOnInit(): void {
    /* get the tournament details here */
    this.tournamentId = this.router.url.split('CreateQuick')[1];
    this.tournamentId = this.tournamentId.split('/')[1];
    this.fetchTournamentDetails(this.tournamentId);
  }

  onTextChange = (data) => {
    this.text = data;
    // search on API
    //  this.getGames();
  };

  bracket1(data) {
    this.participants = data;
  }

  emitValuefor(data) {
    this.emailsforinvite.push(data);
  }

  async sendEmail() {
    let query = {
      participants: this.participants,
      emails: this.emailsforinvite,
      tournamentSlug: this.tournamentId,
    };
    const tournament = await this.tournamentService.tournamentInvites(query);
    this.eSportsToastService.showSuccess(tournament.message);
  }

  onCloseClick() {
    this.router.navigate([AppHtmlRoutes.play]);
  }

  fetchTournamentDetails = async (slug) => {
    try {
      this.isLoaded = false;
      const query = JSON.stringify({
        slug: slug,
        tournamentStatus: 'publish',
      });
      const tournament = await this.tournamentService
        .getTournaments(API, { query })
        .toPromise();
      this.tournamentDetails = tournament.data.length
        ? tournament.data[0]
        : null;

      if (!this.tournamentDetails) {
        this.hideMessage = false;
        throw new Error(this.translateService.instant('VIEW_TOURNAMENT.ERROR'));
      }

      if (
        (this.tournamentDetails && this.tournamentDetails.banner) ||
        this.tournamentDetails.gameDetail.logo
      ) {
        //this.gtmService.gtmEvent('viewed_tournament', this.tournamentDetails);
      }

      this.isLoaded = true;
    } catch (error) {
      this.isLoaded = true;
      this.eSportsToastService.showError(
        error?.error?.message || error?.message
      );
    }
  };

  back(data) {
    this.router.navigateByUrl('create-tournament');
  }

  close(data) {
    this.router.navigateByUrl('play');
  }
}
