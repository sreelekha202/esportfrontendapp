import { Component, OnInit } from '@angular/core';
import { AppHtmlRoutes } from '../../app-routing.model';
import { environment } from "../../../environments/environment";
import { EsportsArticleService, EsportsOptionService } from 'esports';
const API = environment.apiEndPoint;

@Component({
  selector: 'app-featured-content',
  templateUrl: './featured-content.component.html',
  styleUrls: ['./featured-content.component.scss'],
})
export class FeaturedContentComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;

  trendingNews = [];

  categoryList;
  categoryId;

  constructor(
    private articleService: EsportsArticleService,
    private optionService: EsportsOptionService
  ) { }

  ngOnInit(): void {
    this.fetchOptions();
  }

  fetchOptions = async () => {
    const option = await Promise.all([this.optionService.fetchAllCategories(API)]);
    this.categoryList = option[0]?.data;

    for (const iterator of this.categoryList) {
      if (iterator.name == 'News') {
        this.categoryId = iterator._id;
      }
    }

    this.getArticleNews(this.categoryId);
  };

  getArticleNews(categoryId) {
    const query = JSON.stringify({
      articleStatus: 'publish',
      category: categoryId,
    });
    const option = JSON.stringify({ limit: 8, sort: { createdDate: -1 } });
    this.articleService.getArticles_PublicAPI(API, { query, option }).subscribe(
      (res: any) => {
        this.trendingNews = res.data
          .filter((_, idx) => idx < 5)
          // MOCK TYPE
          .map((article, i) => {
            if (i === 2) {
              return { ...article, type: 'opinion' };
            } else if (i === 3) {
              return { ...article, type: 'guide' };
            } else if (i === 4) {
              return { ...article, type: 'listicle' };
            }
            return { ...article, type: 'feature' };
          });
      },
      (err) => { }
    );
  }

  getArticleIconByType(type: string): string {
    const path = 'assets/icons/news';

    switch (type) {
      case 'opinion': return `${path}/opinion-block.svg`;
      case 'guide': return `${path}/guide-block.svg`;
      case 'listicle': return `${path}/listicle-block.svg`;
    }
  }
}
