import { Component, OnInit } from '@angular/core';
import { AppHtmlProfileRoutes, AppHtmlRoutes, AppHtmlRoutesLoginType } from '../../app-routing.model';
import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss'],
  providers: [NgbCarouselConfig],
})
export class LandingComponent implements OnInit {
  AppHtmlProfileRoutes = AppHtmlProfileRoutes;
  AppHtmlRoutes = AppHtmlRoutes;
  AppHtmlRoutesLoginType = AppHtmlRoutesLoginType;



  constructor(
    private router: Router
  ) {
  }

  ngOnInit(): void {
  }

  homepagefunction() {
    this.router.navigateByUrl('/home')
  }
}
