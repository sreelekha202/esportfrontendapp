import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CountdownModule } from "ngx-countdown";
import { PlayRoutingModule } from './play-routing.module';
import { PlayComponent } from './play.component';
import { EsportsCustomPaginationModule, EsportsLoaderModule } from 'esports';
import { SharedModule } from '../../shared/modules/shared.module';
import { FooterMenuMobileModule } from '../../shared/components/footer-menu-mobile/footer-menu-mobile.module';
import { FooterGooglyModule } from '../../shared/components/footer-googly/footer-googly.module';
import { HeaderBottomModule } from '../../shared/components/header-bottom/header-bottom.module';
import { PrizePoollModule } from '../tournament/pipe/prize-pooll.module';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { CoreModule } from '../../core/core.module';

@NgModule({
  declarations: [PlayComponent],
  imports: [
    CommonModule,
    PlayRoutingModule,
    CountdownModule,
    MatProgressSpinnerModule,
    SharedModule,
    CoreModule,
    PrizePoollModule,
    HeaderBottomModule,
    FooterGooglyModule,
    FooterMenuMobileModule,
    EsportsCustomPaginationModule,
    EsportsLoaderModule.setColor('#1d252d')],
})
export class PlayModule { }
