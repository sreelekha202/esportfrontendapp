import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SettingsLightboxComponent } from './settings-lightbox.component';

describe('SettingsLightboxComponent', () => {
  let component: SettingsLightboxComponent;
  let fixture: ComponentFixture<SettingsLightboxComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SettingsLightboxComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingsLightboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
