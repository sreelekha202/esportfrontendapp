import { EsportsToastService, EsportsUserService } from 'esports';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { SettingsLightboxComponent } from '../../../settings-lightbox.component';
// import { MatDatepickerInputEvent } from '@angular/material/datepicker';

@Component({
  selector: 'app-change-phonenumber',
  templateUrl: './change-phonenumber.component.html',
  styleUrls: ['./change-phonenumber.component.scss']
})
export class ChangePhonenumberComponent implements OnInit {
  selectedOption: string;
  showLoader: boolean;
  currentUser: any;
  userSubscription: Subscription;
  addForm: FormGroup;
  editImageUrl: any = null;
  selectedFile: any;
  name = 'Dynamic Add Fields';
  values = [];
  phonenumber = false
  passworddiv = true
  // gender = [
  //   { title: 'Female', value: 'female' },
  //   { title: 'Male', value: 'male' },
  // ]
  pronouns = [
    { title: 'She/her', value: 'she' },
    { title: 'He/His', value: 'he' },
    { title: 'They/them', value: 'they', },
    { title: 'Custom', value: 'Custom', },
  ]
  code: string;
  codeEntered: boolean;
  interval;
  stateList: any = [];
  countryList: any = [];
  base64textString: any;
  isExist: any;
  form = this.formBuilder.group({ dobPolicy: [false] });
  todayDate: Date = new Date(new Date().setFullYear(new Date().getFullYear() - 10));
  parentalDate: Date = new Date(new Date().setFullYear(new Date().getFullYear() - 18));
  parental = false;
  // @Input() phone: any = new Date();
  @Output() dateChange = new EventEmitter();

  constructor(private userService: EsportsUserService,
    private fb: FormBuilder,
    private toastService: EsportsToastService,
    public dialogRef: MatDialogRef<SettingsLightboxComponent>,
    private formBuilder: FormBuilder,) {
    this.addForm = this.fb.group({
      email: [null, Validators.compose([Validators.required])],
      phonenumber: [null, Validators.compose([Validators.required])],
      password: [null, Validators.compose([Validators.required])]
    });
    this.userService.getAllCountries().subscribe((res) => { this.countryList = res.countries; })
  }

  ngOnInit(): void {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) { this.currentUser = data; this.setUserData() }
    });
  }
  setUserData() {
    this.editImageUrl = this.currentUser.profilePicture,
      this.addForm.patchValue({
        'email': this.currentUser.email,
        'phonenumber': this.currentUser.phonenumber,
        'password': this.currentUser.password,
      })
  }
  onSubmit(formData: any) {
    // this.showLoader = true;
    // if (this.addForm.valid) {
    //   // for (let key in this.addForm.value) { formData.append(`${key}`, this.addForm.value[key]); }
    //   this.userService.updateProfile(this.addForm.value).subscribe((res) => {
    //     this.showLoader = false;
    //     this.setUserData();
    //     this.toastService.showSuccess(res.message)
    //     this.onClose()
    //     this.userService.refreshCurrentUser();

    //   }, (err) => {
    //     this.showLoader = false;
    //     this.toastService.showError(err.message)
    //   })
    // }
  }
  onOtpChange(code: string): void {
    this.codeEntered = code.length === 4;
    if (this.codeEntered) { this.code = code; }
  }
  function() { this.selectedOption }
  addvalue() { this.values.push({ value: "" }); }
  back() {
    this.passworddiv = false;
    this.phonenumber = true;
  }
  onClose(): void { this.dialogRef.close(); }
  // onDateChange = () => { this.dateChange.emit(this.phone) }
  minDate = new Date();
  onphoneChange(data) { }
}
