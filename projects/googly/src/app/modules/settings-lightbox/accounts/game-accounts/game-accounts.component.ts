import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import {
  EsportsToastService,
  EsportsUserService,
  EsportsGameService,
} from 'esports';
import { SettingsLightboxComponent } from '../../settings-lightbox.component';
import { Subscription } from 'rxjs';
import { MatDialogRef } from '@angular/material/dialog';
import { environment } from '../../../../../environments/environment';

const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;
@Component({
  selector: 'app-game-accounts',
  templateUrl: './game-accounts.component.html',
  styleUrls: ['./game-accounts.component.scss'],
})
export class GameAccountsComponent implements OnInit {
  userSubscription: Subscription;
  // selectedGames = new FormControl();
  accounts = [];
  gamesList = [];
  addForm: FormGroup;
  gameArray: FormArray;
  selectedGames = [];
  showLoader: boolean;
  currentUser: any;

  constructor(
    private gameService: EsportsGameService,
    private userService: EsportsUserService,
    private fb: FormBuilder,
    private toastService: EsportsToastService,
    public dialogRef: MatDialogRef<SettingsLightboxComponent>
  ) {
    this.addForm = this.fb.group({
      game_array: new FormArray([]),
    });
    this.gameArray = this.addForm.get('game_array') as FormArray;
  }

  ngOnInit(): void {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.currentUser = data;
        this.setUserData();
      }
    });
    this.getGamseList();

    // MOCK DATA
    // this.accounts = [
    //   {
    //     icon: 'https://i115.fastpic.ru/big/2021/0630/41/f7bfe0a25d1c8f94c85cf11ba0677b41.png',
    //     label: 'Valorant',
    //   },
    //   {
    //     icon: 'https://i115.fastpic.ru/big/2021/0630/2e/a8c03e5970828657d0de3dfa730c8e2e.png',
    //     label: 'Overwatch',
    //   },
    //   {
    //     icon: 'https://i115.fastpic.ru/big/2021/0630/af/fc6103455b8ead1de141a1fd5cd90caf.png',
    //     label: 'Fortnite',
    //   },
    // ];
  }

  getGamseList() {
    this.gameService
      .getGames({ isTournamentAllowed: true })
      .subscribe((res) => {
        this.gamesList = res.data;
      });
  }

  onChangeGame(games) {
    if (games && games.value) {
      this.gamesList.map((o) => {
        if (o._id == games.value) {
          if (!this.selectedGames.includes(games.value)) {
            this.selectedGames.push(games.value);
            this.gameArray.push(
              this.fb.group({
                userGameId: ['', Validators.compose([Validators.required])],
                name: [o.name],
                _id: [o._id],
                image: [o.image],
              })
            );
          }
        }
      });
    }
  }

  setUserData() {
    this.currentUser.preference.gameDetails.map((o) => {
      this.selectedGames.push(o._id);
      this.gameArray.push(
        this.fb.group({
          userGameId: [o.userGameId, Validators.compose([Validators.required])],
          name: [o.name],
          _id: [o._id],
          image: [o.image],
        })
      );
    });
  }

  deleteObjGameArray(index) {
    this.gameArray.removeAt(index);
    const i = this.selectedGames.indexOf(index);
    if (index > -1) {
      this.selectedGames.splice(index, 1);
    }
  }

  onClose(): void {
    this.dialogRef.close();
  }

  onSubmit() {
    this.showLoader = true;
    if (this.addForm.valid) {
      this.userService
        .updateProfile(API, {
          gameDetails: this.addForm.value.game_array,
        })
        .subscribe(
          (res) => {
            this.showLoader = false;
            this.setUserData();
            this.toastService.showSuccess(res.message);
            this.addForm.reset();
            // this.addForm.value.game_array.map((d,i)=>{this.gameArray.removeAt(i-1),i})
            while (this.gameArray.length !== 0) {
              this.gameArray.removeAt(0);
            }
            this.userService.refreshCurrentUser(API, TOKEN);
          },
          (err) => {
            this.showLoader = false;
            this.toastService.showError(err.error.message);
          }
        );
    }
  }

  ngOnDestroy(): void {
    if (this.userSubscription) this.userSubscription.unsubscribe();
  }
}
