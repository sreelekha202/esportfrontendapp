import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-social-accounts',
  templateUrl: './social-accounts.component.html',
  styleUrls: ['./social-accounts.component.scss'],
})
export class SocialAccountsComponent implements OnInit {
  accounts = [];

  constructor() {}

  ngOnInit(): void {
    // MOCK DATA
    this.accounts = [
      {
        icon: 'https://i115.fastpic.ru/big/2021/0720/89/0c7dfc1df135a1a79bc6e7542db96289.png',
        label: 'Instagram',
      },
      {
        icon: 'https://i115.fastpic.ru/big/2021/0720/53/8db2503e84631f7c7d1fe756c8471c53.png',
        label: 'Youtube',
      },
      {
        icon: 'https://i115.fastpic.ru/big/2021/0630/65/7a457917ed0aef758a736f3a23594d65.png',
        label: 'Twitch',
      },
    ];
  }
}
