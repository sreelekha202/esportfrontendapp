import { Component, OnInit, Input } from '@angular/core';

interface AccountItem {
  icon: string;
  label: string;
}

@Component({
  selector: 'app-account-item',
  templateUrl: './account-item.component.html',
  styleUrls: ['./account-item.component.scss'],
})
export class AccountItemComponent implements OnInit {
  @Input() data: AccountItem;
  @Input() isSocial: boolean = false;
  value = 'xxinfinity01';
  constructor() {}

  ngOnInit(): void {}
}
