import { Component, OnInit } from '@angular/core';
import {
  ControlContainer,
  FormControl,
  FormGroupDirective,
  Validators,
} from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { EsportsToastService, EsportsTournamentService } from 'esports';

@Component({
  selector: 'app-entrance-fee',
  templateUrl: './entrance-fee.component.html',
  styleUrls: ['./entrance-fee.component.scss'],
  viewProviders: [
    { provide: ControlContainer, useExisting: FormGroupDirective },
  ],
})
export class EntranceFeeComponent implements OnInit {
  weightageInput: String = '';
  Step6;
  payouts = [{from:'',to:'',weightage:''}];
  sponsors = [{from:'',to:'',weightage:''}];
  isPaidFlag = false;
  entranceFee: number = null;
  numberOfParticipants: number = null;
  selectCurrency: string = 'NZD';
  selectCurrencyWagger: string = 'NZD';
  calculatedWaggerPoolAmount: number;
  selectPay: string = '';
  selectPayC1: string = '';
  waggerPoolData:any;
  fromSponporsData:any;
  sumFromOrganizer:number = 0;
  list = [
    { name: 'Yes', value: true },
    { name: 'No', value: false },
  ];
  typeOfChecked = {};
  waggerPool = {
    fee: this.entranceFee,
    payout: '',
    currency: 'NZD',
    participants: this.numberOfParticipants,
    amounts: this.payouts,
    prize: [],
  };
  fromSponpors = {
    entranceFee: 0,
    payout: '',
    currency: 'NZD',
    amounts: this.sponsors,
    prize: [],
  };
  dataCashPrizesActive = false;
  dataCashPrizesIDItem = null;
  dataCashPrizesPool = false;
  dataCashPrizesSponsors = false;
  dataCashPrizesNoCash = false;
  dataCashPrizes = [
    {
      id: 0,
      textTitle: 'From a wager pool',
      img: 'assets/icons/matchmaking/cash-prize/Icon-pool.svg',
    },
    {
      id: 1,
      textTitle: 'From organizer',
      img: 'assets/icons/matchmaking/cash-prize/Icon-sponsors.svg',
    },
    {
      id: 2,
      textTitle: 'No cash prizes',
      img: 'assets/icons/matchmaking/cash-prize/Icon-No-cash.svg',
    },
  ];
  currencies = ['NZD', 'VND', 'DKK'];
  payoutsMethod = [
    'Select payout method 1',
    'Select payout method 2',
    'Select payout method 3',
  ];
  payoutsMethodC1 = [
    'Select payout method 1',
    'Select payout method 2',
    'Select payout method 3',
  ];

  constructor(
    private tournament: FormGroupDirective,
    private eSportsTournamentService: EsportsTournamentService,
    private toastService: EsportsToastService,
    private translateService: TranslateService
  ) { }

  ngOnInit(): void {
    this.Step6 = this.tournament.form;
    this.Step6.addControl('isPaid', new FormControl(false));

    if (!this.Step6.value.waggerPoolDetails)
      this.Step6.addControl('waggerPoolDetails', new FormControl(''));

    if (!this.Step6.value.organizerDetails)
      this.Step6.addControl('organizerDetails', new FormControl(''));

    if (!this.Step6.value.prizeAmount)
      this.Step6.addControl('prizeAmount', new FormControl(''));

    if (!this.Step6.value.prizeAmount){
      this.Step6.addControl('organizerAmount', new FormControl(''));
    }else{
      this.sumFromOrganizer = (this.Step6.value.organizerAmount ? this.Step6.value.organizerAmount : 0);
    }

    if (this.Step6.value.isPaid){
      this.clickChecked(this.Step6.value.isPaid)
    }else{
      this.typeOfChecked = this.dataCashPrizes[2]; // default set to 'No cash prize'
      this.dataCashPrizesIDItem = this.dataCashPrizes[2].id;
      this.Step6.get('isPaid').setValue(this.typeOfChecked);
    }

    if (this.Step6.get('isPaid').value) this.isPaidFlag = true;

    this.waggerPoolData = this.Step6?.value?.waggerPoolDetails;
    this.payouts = (this.waggerPoolData.prize ? this.waggerPoolData.prize : [{from:'',to:'',weightage:''}]);
    /* wagger pool setting here */
    this.waggerPool.participants = this.numberOfParticipants = this.waggerPoolData.participants;
    this.waggerPool['entranceFee'] = this.waggerPool.fee = this.entranceFee = this.waggerPoolData.entranceFee;
    this.calculatedWaggerPoolAmount = (this.waggerPoolData.prizeAmount);
    this.waggerPool.payout = this.selectPayC1 = (this.waggerPoolData.payout ? this.waggerPoolData.payout : '');
    this.waggerPool.prize = (this.waggerPoolData.prize ? this.waggerPoolData.prize : [{from:'',to:'',weightage:''}]);

    /* from organiser setting here */
    this.fromSponporsData = this.Step6?.value?.organizerDetails;
    this.selectPay = this.fromSponpors.payout = (this.fromSponporsData.payout ? this.fromSponporsData.payout : '');
    this.fromSponpors.prize = (this.fromSponporsData.prize ? this.fromSponporsData.prize : []);
    this.sponsors = (this.fromSponporsData.prize ? this.fromSponporsData.prize : [{from:'',to:'',weightage:''}]);

  }

  addPayout(): void {
    let idP = this.payouts[this.payouts.length - 1];
    this.payouts.push({from:'',to:'',weightage:''});
  }

  addSponsor(): void {
    let idP = this.sponsors[this.sponsors.length - 1];
    this.sponsors.push({from:'',to:'',weightage:''});
  }

  onToggleDelete(id): void {
    const indexPayout = this.payouts.findIndex((p) => p === id);
    if (indexPayout < 0) return;

    const newPayout = this.payouts;
    if (newPayout.length > 1) {
      newPayout.splice(indexPayout, 1);
      this.waggerPool.prize.splice(indexPayout, 1);
    }
    this.payouts = newPayout;
  }

  onToggleDeleteSponsor(id): void {
    const indexSponsor = this.sponsors.findIndex((p) => p === id);
    if (indexSponsor < 0) return;

    const newSponsor = this.sponsors;
    if (newSponsor.length > 1) {
      newSponsor.splice(indexSponsor, 1);
      this.fromSponpors.prize.splice(indexSponsor, 1);
    }
    this.sponsors = newSponsor;
  }

  onToggleEntranceFee(value): void {
    this.Step6.get('isPaid').setValue(value);
    this.paidRegistrationHandler(value);
  }

  paidRegistrationHandler(check) {
    if (check) {
      const regCurrencyValidators = Validators.compose([Validators.required]);
      const regFeeValidators = Validators.compose([
        Validators.required,
        Validators.min(1),
      ]);
      this.Step6.addControl(
        'regFeeCurrency',
        new FormControl('KWD', regCurrencyValidators)
      );
      this.Step6.addControl('regFee', new FormControl(1, regFeeValidators));
    } else {
      this.Step6.removeControl('regFeeCurrency');
      this.Step6.removeControl('regFee');
    }
  }

  clickChecked(item) {
    this.typeOfChecked = item;
    if (item.id == 0) {
      this.dataCashPrizesIDItem = item.id;
      this.dataCashPrizesActive = true;
      this.dataCashPrizesPool = true;
      this.dataCashPrizesSponsors = false;
      this.dataCashPrizesNoCash = false;
      this.Step6.get('isPaid').setValue(item);
    }
    if (item.id == 1) {
      this.dataCashPrizesIDItem = item.id;
      this.dataCashPrizesActive = true;
      this.dataCashPrizesPool = false;
      this.dataCashPrizesSponsors = true;
      this.dataCashPrizesNoCash = false;
      this.Step6.get('isPaid').setValue(item);
    }
    if (item.id == 2) {
      this.dataCashPrizesIDItem = item.id;
      this.dataCashPrizesActive = true;
      this.dataCashPrizesPool = false;
      this.dataCashPrizesSponsors = false;
      this.dataCashPrizesNoCash = true;
      this.Step6.get('isPaid').setValue(item);
    }
  }

  onStepChange(step) {
    // if (
    //   this.Step6.get('isPaid').value &&
    //   step > this.Step6.get('step').value &&
    //   !this.isPaidFlag
    // ) {
    //   this.eSportsTournamentService.totalPages.next(
    //     this.eSportsTournamentService.totalPages.value + 1
    //   );
    //   this.Step6.get('step').setValue(step + 1);
    // }
    if (!this.Step6.value.isPaid || !this.Step6.value.isPaid.textTitle) {
      this.toastService.showError('Please choose cash prize.');
      return;
    }

    /* prize amount check */
    if(this.Step6.value.isPaid == 0){
      let sumFromWagger:number = 0;
      for(var i=0;i<this.waggerPool.prize.length;i++){
        if(this.waggerPool.prize[i].weightage){
          sumFromWagger += parseInt(this.waggerPool.prize[i].weightage);
        }
      }
      /*if(sumFromWagger!=100){
        this.toastService.showError(this.translateService.instant('CREATE_TOURNMENT.MANDATORY_REQUIRED'));
        return;
      }*/
    }

    if(this.Step6.value.isPaid == 1){
      let sumFromSponsor:number = 0;
      for(var i=0;i<this.fromSponpors.prize.length;i++){
        if(this.fromSponpors.prize[i].weightage){
          sumFromSponsor += parseInt(this.fromSponpors.prize[i].weightage);
        }
      }
      /*if(sumFromSponsor!=100){
        this.toastService.showError(this.translateService.instant('CREATE_TOURNMENT.BALANCED_PRIZE_AMOUNT'));
        return;
      }*/
    }


    this.Step6.get('organizerDetails').setValue(this.fromSponpors);
    this.Step6.get('waggerPoolDetails').setValue(this.waggerPool);
    /*this.eSportsTournamentService.totalPages.next(
      this.eSportsTournamentService.totalPages.value + 1
    );*/
    this.Step6.get('step').setValue(step + 1);
  }

  // Handlechange currency
  onToggleChangeCurrency(cur): void {
    this.selectCurrency = cur;
    this.fromSponpors = { ...this.fromSponpors, currency: cur };
  }

  // Handlechange currency for waggerpool
  onToggleChangeCurrencyWagger(cur): void {
    this.selectCurrencyWagger = cur;
    this.waggerPool = { ...this.waggerPool, currency: cur };
  }

  // Handlechange payloadMethod Component 2
  onToggleChangePayouts(pay): void {
    this.selectPay = pay;
    this.fromSponpors = { ...this.fromSponpors, payout: pay };
  }

  // Handlechange payloadMethod Component 1
  onToggleChangePayoutsC1(pay): void {
    this.selectPayC1 = pay;
    this.waggerPool = { ...this.waggerPool, payout: pay };
  }

  handleChangeComponent1(e) {
    this.calculatedWaggerPoolAmount = (((this.entranceFee && this.entranceFee != 0) && (this.numberOfParticipants && this.numberOfParticipants != 0)) ? (this.numberOfParticipants * this.entranceFee) : null)
    this.Step6.get('prizeAmount').setValue(this.calculatedWaggerPoolAmount);
    this.waggerPool = {
      ...this.waggerPool,
      [e.target.name]: e.target.value,
    };
  }
  // Handlechange waggerPool Component 1
  handleChangeWeightC1(e, ind) {
    const name = e.target.name;
    const value = e.target.value;
    let prizeVo: any = (this.waggerPool.prize.length ? this.waggerPool.prize : []);
    let prize: any = {};
    if (!this.waggerPool.prize[ind] || !this.waggerPool.prize[ind].from || !this.waggerPool.prize[ind].to || !this.waggerPool.prize[ind].weightage) {
      prize = {
        from: (name === 'fromNth' ? value : ((this.waggerPool.prize.length && this.waggerPool.prize[ind]) ? this.waggerPool.prize[ind].from : '')),
        to: (name === 'toNth' ? value : ((this.waggerPool.prize.length && this.waggerPool.prize[ind]) ? this.waggerPool.prize[ind].to : '')),
        weightage: (name === 'weightage' ? value : ((this.waggerPool.prize.length && this.waggerPool.prize[ind]) ? this.waggerPool.prize[ind].weightage : ''))
      };
    } else {
      prize.from = (name === 'fromNth' ? value : ((this.waggerPool.prize.length && this.waggerPool.prize[ind]) ? this.waggerPool.prize[ind].from : ''));
      prize.to = (name === 'toNth' ? value : ((this.waggerPool.prize.length && this.waggerPool.prize[ind]) ? this.waggerPool.prize[ind].to : ''));
      prize.weightage = (name === 'weightage' ? value : ((this.waggerPool.prize.length && this.waggerPool.prize[ind]) ? this.waggerPool.prize[ind].weightage : ''));
    }

    prizeVo[ind] = prize;

    this.waggerPool = {
      ...this.waggerPool,
      prize: prizeVo,
    };
  }

  // Handlechange sponsors Component 2
  handleChangeWeightC2(e, ind) {
    const name = e.target.name;
    const value = e.target.value;
    let prizeVo: any = (this.fromSponpors.prize.length ? this.fromSponpors.prize : []);
    let prize: any = {};
    if (!this.fromSponpors.prize[ind] || !this.fromSponpors.prize[ind].from || !this.fromSponpors.prize[ind].to || !this.fromSponpors.prize[ind].weightage) {
      prize = {
        from: (name === 'fromNth' ? value : ((this.fromSponpors.prize.length && this.fromSponpors.prize[ind]) ? this.fromSponpors.prize[ind].from : '')),
        to: (name === 'toNth' ? value : ((this.fromSponpors.prize.length && this.fromSponpors.prize[ind]) ? this.fromSponpors.prize[ind].to : '')),
        weightage: (name === 'weightage' ? value : ((this.fromSponpors.prize.length && this.fromSponpors.prize[ind]) ? this.fromSponpors.prize[ind].weightage : '')),
      };
    } else {
      prize.from = (name === 'fromNth' ? value : ((this.fromSponpors.prize.length && this.fromSponpors.prize[ind]) ? this.fromSponpors.prize[ind].from : ''));
      prize.to = (name === 'toNth' ? value : ((this.fromSponpors.prize.length && this.fromSponpors.prize[ind]) ? this.fromSponpors.prize[ind].to : ''));
      prize.weightage = (name === 'weightage' ? value : ((this.fromSponpors.prize.length && this.fromSponpors.prize[ind]) ? this.fromSponpors.prize[ind].weightage : ''));
    }

    prizeVo[ind] = prize;

    this.fromSponpors = {
      ...this.fromSponpors,
      prize: prizeVo,
    };

    this.sumFromOrganizer = 0;
    for(var i=0;i<this.fromSponpors.prize.length;i++){
      if(this.fromSponpors.prize[i].weightage){
        this.sumFromOrganizer += parseInt(this.fromSponpors.prize[i].weightage);
      }
    }
    this.Step6.get('organizerAmount').setValue(this.sumFromOrganizer);
  }

}
