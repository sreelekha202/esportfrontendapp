import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TournamentTimeComponent } from './tournament-time.component';

describe('TournamentTimeComponent', () => {
  let component: TournamentTimeComponent;
  let fixture: ComponentFixture<TournamentTimeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TournamentTimeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TournamentTimeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
