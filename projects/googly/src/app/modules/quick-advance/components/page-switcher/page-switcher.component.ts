import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  OnChanges,
} from '@angular/core';
import { EsportsTournamentService } from 'esports';
import e from 'express';

@Component({
  selector: 'app-page-switcher',
  templateUrl: './page-switcher.component.html',
  styleUrls: ['./page-switcher.component.scss'],
})
export class PageSwitcherComponent implements OnInit, OnChanges {
  @Input() step: number = 0;
  @Input() waggerPool: any;
  @Input() fromSponpors: any;
  @Input() typeOfChecked: any;
  @Input() isSelectedGame;
  @Output() onStepChange = new EventEmitter<number>();

  totalPages: number = 0;
  activePage: number = 0;
  numberStep: number = null;

  constructor(private eSportsTournamentService: EsportsTournamentService) {}

  ngOnChanges(): void {
    this.numberStep = this.step;
  }

  ngOnInit(): void {
    this.numberStep = this.step;
    
    this.eSportsTournamentService.totalPages.subscribe((totalPages) => {
      this.totalPages = totalPages;
    });

    this.eSportsTournamentService.activePage.subscribe((activePage) => {
      this.activePage = activePage;
    });
  }

  onStepChangeEvent(step) {
    this.numberStep = step;
    if (step === 6) {
      if (this.typeOfChecked.id === 0) {
        sessionStorage.setItem('step6', JSON.stringify(this.waggerPool));
      } else if (this.typeOfChecked.id === 1) {
        sessionStorage.setItem('step6', JSON.stringify(this.fromSponpors));
      } else {
        sessionStorage.setItem('step6', JSON.stringify('none'));
      }
    }
    this.onStepChange.emit(step);
  }
}
