import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

interface Participant {
  country: string;
  email: string;
  fullName: string;
  _id: string;
  profilePicture: string;
}

@Component({
  selector: 'app-participant',
  templateUrl: './participant.component.html',
  styleUrls: ['./participant.component.scss'],
})
export class ParticipantComponent implements OnInit {
  @Input() isRemove: boolean = false;
  @Input() data: Participant;
  @Output() handleId = new EventEmitter();

  constructor() {}

  ngOnInit(): void {}

  onHandle(_id: string): void {
    this.handleId.emit(_id);
  }
}
