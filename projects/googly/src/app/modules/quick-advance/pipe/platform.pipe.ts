import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'platformPipe',
})
export class PlatformPipe implements PipeTransform {
  transform(value) {
    const platforms = [
      {
        image: 'assets/icons/tournament/platforms/pc.svg',
        name: 'pc',
      },
      {
        image: 'assets/icons/tournament/platforms/mobile.svg',
        name: 'mobile',
      },
      {
        image: 'assets/icons/tournament/platforms/console.svg',
        name: 'ps4',
      },
      {
        image: 'assets/icons/tournament/platforms/console.svg',
        name: 'ps5',
      },
      {
        image: 'assets/icons/tournament/platforms/console.svg',
        name: 'others',
      },
    ];
    const p = platforms.find(
      (p) => p.name.toLowerCase() === value.toLowerCase()
    );
    return p ? p.image : 'assets/icons/tournament/platforms/console.svg';
  }
}
