import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TeamRegistrationRoutingModule } from './team-registration-routing.module';
import { CoreModule } from '../../core/core.module';
import { FormComponentModule } from '../../shared/components/form-component/form-component.module';
import { TeamRegistrationComponent } from './team-registration.component';
import { AboutTournamentComponent } from './components/about-tournament/about-tournament.component';
import { NewTeamComponent } from './pages/new-team/new-team.component';
import { SelectTeammatesComponent } from './pages/select-teammates/select-teammates.component';
import { TournamentRegistrationComponent } from './pages/tournament-registration/tournament-registration.component';
import { MatExpansionModule } from '@angular/material/expansion';
import { HeaderInfoModule } from '../../shared/components/header-info/header-info.module';
import { UploadImageModule } from '../../shared/components/upload-image/upload-image.module';
import { SharedModule } from '../../shared/modules/shared.module';
import { HeaderGooglyModule } from '../../shared/components/header-googly/header-googly.module';
import { EsportsLoaderModule } from 'esports';
@NgModule({
  declarations: [
    AboutTournamentComponent,
    NewTeamComponent,
    SelectTeammatesComponent,
    TeamRegistrationComponent,
    TournamentRegistrationComponent,
  ],
  imports: [
    CommonModule,
    CoreModule,
    FormComponentModule,
    SharedModule,
    MatExpansionModule,
    TeamRegistrationRoutingModule,
    HeaderInfoModule,
    UploadImageModule,
    HeaderGooglyModule,
    EsportsLoaderModule.setColor('#1d252d'),
  ],
})
export class TeamRegistrationModule {}
