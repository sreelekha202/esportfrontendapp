import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { AboutTournamentComponent } from "./about-tournament.component";

describe("AboutTournamentComponent", () => {
  let component: AboutTournamentComponent;
  let fixture: ComponentFixture<AboutTournamentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AboutTournamentComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AboutTournamentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
