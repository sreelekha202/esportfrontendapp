import { Component, OnInit } from '@angular/core';
import {
  IPagination,
  IUser,
  EsportsUserService,
  EsportsArticleService,
  EsportsLanguageService,
  EsportsToastService,
  EsportsOptionService,
  EsportsGameService,
  EsportsVideoLibraryService,
  EsportsUtilsService,
} from 'esports';
import { environment } from '../../../../environments/environment';
import { AppHtmlRoutes } from '../../../app-routing.model';

const API = environment.apiEndPoint;

@Component({
  selector: 'app-article-news',
  templateUrl: './article-news.component.html',
  styleUrls: ['./article-news.component.scss'],
})
export class ArticleNewsComponent implements OnInit {
  user: IUser;
  articles: any = [];
  trendingArticles :any = [];
  articlescategory: any = [];
  showLoader: boolean = true;
  AppHtmlRoutes = AppHtmlRoutes;
  selectTypeList: any = '';
  paginationData = {
    page: 1,
    limit: 10,
    sort: { createdDate: -1 },
  };
  checkList = [
    {
      id: 1,
      title: 'Fortnite',
    },
    {
      id: 2,
      title: 'Overwatch',
    },
    {
      id: 3,
      title: 'Valorant',
    },
    {
      id: 4,
      title: 'Call of Duty',
    },
    {
      id: 5,
      title: 'Dota 2',
    },
    {
      id: 6,
      title: 'League of Legends',
    },
    {
      id: 7,
      title: 'Rocket League',
    },
    {
      id: 8,
      title: 'Destiny 2',
    },
    {
      id: 9,
      title: 'Starfield',
    },
    {
      id: 10,
      title: 'Starfield 2',
    },
  ];
  typeList = [
    {
      id: 1,
      title: 'Artices',
    },
    {
      id: 2,
      title: 'Videos',
    },
  ];
  selectedCategory: any = 'all';
  categories = [
    {
      id: 1,
      name: 'Games',
    },
    {
      id: 2,
      name: 'Esports',
    },
    {
      id: 3,
      name: 'Top picks',
    },
    {
      id: 4,
      name: 'Shop promos',
    },
  ];
  categoryList: any = [];
  page: IPagination;
  hottestArticle = null;
  hasNextPage;
  participantPage: IPagination = {
    activePage: 1,
    totalItems: 1,
    maxSize: 10,
    itemsPerPage: 10,
  };
  relatedArticles = [];
  filterShow: boolean = false;
  gameArray = [];
  sortOrderArray: any = [
    // { label: 'Default', value: 0 },
    { label: 'Newest to Oldest', value: 'createdOn' },
    { label: 'Oldest to Newest', value: '-createdOn' },
  ];
  sortOrderType = this.sortOrderArray[0]['value'];
  gameList = [];
  param: any = {};
  videoLibrary = [];
  isVideo = false;
  activeVideo = null;
  isContent = true;
  isVideoApply:any;
  isContentApply=true;

  constructor(
    private articleService: EsportsArticleService,
    private userService: EsportsUserService,
    private articleApiService: EsportsArticleService,
    public language: EsportsLanguageService,
    private toastService: EsportsToastService,
    private optionService: EsportsOptionService,
    private esportsGameService: EsportsGameService,
    private videoLibraryService: EsportsVideoLibraryService,
    public utilsService: EsportsUtilsService,
  ) {}

  ngOnInit(): void {
    this.getHottestArticle();
    this.getLatestPost();
    this.getRecentPost('');
    this.getGames();
    this.getAllCategory();
    this.getVideoLibrary();
  }


  filterShowM() {
    this.filterShow = true;
  }
  filterCloseM() {
    this.filterShow = false;
  }

  apply(){
    
    this.isVideoApply = this.isVideo

    this.isContentApply = this.isContent
  }

  videoContent(selectedData){
   if(selectedData == 'content'){
    
    this.isVideo = false
    this.isContent = true
   
   }
   if(selectedData == 'video'){

    this.isVideo = true
    this.isContent = false
   }
  }

  
  getGames() {
    this.esportsGameService.getGames(API).subscribe((data: any) => {
      this.gameList = data.data;
    });
  }


  getAllCategory = async () => {
    const res = await this.optionService.fetchAllCategories(API);
    
    this.categoryList = [{ name: 'All Updates', _id: 'all' }, ...res?.data];
    
  };

  getRecentPost(id) {
    if (id) {     
      let isChecked = id?.source?.checked ? true : false;
      const gameid = { gameDetails: id?.source?.value };     
      if (isChecked && id !== '') {
        this.gameArray.push(gameid);
      } else {
        for (let i = 0; i < this.gameArray.length; i++) {
          if (JSON.stringify(this.gameArray[i]) == JSON.stringify(gameid)) {
            this.gameArray.splice(i, 1);
            if (this.gameArray == []) {
              id = '';
            }
          }
        }
      }
    }
    let gameDetailsList = '';
    this.gameArray.map(({ gameDetails }, i) => {
      if (i == 0) {
        gameDetailsList = gameDetails;   
      } else {
        gameDetailsList = gameDetailsList + '||' + gameDetails;    
      }
    });
    this.showLoader = true;
    let param: any = {
      articleStatus: 'publish',
      page: this.participantPage.activePage,
      limit: this.participantPage.itemsPerPage, 
      sort: this.sortOrderType,
      gameDetails: gameDetailsList,
    };
    this.selectedCategory != 'all'
      ? (param.category = this.selectedCategory)
      : '';
    param.category ? param.category : delete param.category;
    param.gameDetails ? param.gameDetails : delete param.gameDetails;
    this.articleApiService
      .getLatestArticleAll(API, param).subscribe(
      (res: any) => {
        this.articles = res['data']['docs'];
        this.showLoader = false;
        this.participantPage.itemsPerPage = res?.data?.limit;
        this.participantPage.totalItems = res?.data?.totalDocs;
      },
      (err) => {
        this.showLoader = false;
      }
    );
  }
  getLatestPost() {
    const pagination = JSON.stringify({
      page: this.paginationData.page,
      limit: 20,
      sort: { createdDate: -1 },
    });
    const query = JSON.stringify({
      articleStatus: "publish",
    });

    const perfernce = JSON.stringify({
      prefernce: this.user?.preference?.game
        ? this.user?.preference?.game.map((item) => {
          return { gameDetails: item };
        })
        : "",
    });
    this.showLoader = true;
    this.articleApiService
      .getLatestArticle(API, {
        pagination: pagination,
        query: query,
        preference: perfernce,
      })
      .subscribe(
        (res: any) => {
          this.trendingArticles = res["data"]["docs"];
          this.showLoader = false;
          this.page = {
            totalItems: res?.data?.totalDocs,
            itemsPerPage: res?.data?.limit,
            maxSize: 5,
          };
        },
        (err) => {
          this.showLoader = false;
        }
      );
  }
  getHottestArticle() {
    this.articleService.getArticleList(API).subscribe(
      (res) => {
        // MOCK HOTTEST ARTICLE TYPE
        this.hottestArticle = { ...res['data'][0], type: 'guide' };
        this.showLoader = false;
      },
      (err) => {
        this.showLoader = false;
      }
    );
  }

  getVideoLibrary = async () => {
    try {
      const pagination = {
        page: this.participantPage.activePage,
        limit: this.participantPage.itemsPerPage, 
        sort: '-updatedOn',
        projection: [
          '_id',
          'slug',
          'title',
          'description',
          'youtubeUrl',
          'thumbnailUrl',
          'createdBy',
        ],
      };

      const query = `?pagination=${encodeURIComponent(
        JSON.stringify(pagination)
      )}`;

      const response = await this.videoLibraryService.fetchVideoLibrary(
        API,
        query
      );
      this.videoLibrary = await Promise.all(
        response?.data?.docs.map(async (item) => {
          return {
            ...item,
            youtubeUrl: await this.utilsService.getEmbbedUrl(item.youtubeUrl),
          };
        })
      );
      this.participantPage.itemsPerPage = response?.data?.limit;
      this.participantPage.totalItems = response?.data?.totalDocs;
      this.videoLibrary.length ? this.setActive(this.videoLibrary[0]) : null;
    } catch (error) {
      this.toastService.showError(error.error?.message || error?.message);
    }
  };

  setActive(video) {
    this.activeVideo = false;
    setTimeout(() => {
      this.activeVideo = video;
    }, 500);

  }
  pageChanged(page): void {
    this.participantPage.activePage = page;
    this.getVideoLibrary();
  }

  getArticleIconByType(type: string): string {
    const path = 'assets/icons/news';
    switch (type) {
      case 'feature':
        return `${path}/feature.svg`;
      case 'opinion':
        return `${path}/opinion.svg`;
      case 'guide':
        return `${path}/guide.svg`;
      case 'listicle':
        return `${path}/listicle.svg`;
      case 'hot':
        return `${path}/hot.svg`;
      case 'trending':
        return `${path}/trending.svg`;
    }
  }

  getCurrentUserDetails() {
    this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.user = data;
      }
    });
  }

  currentPage(page): void {
    this.participantPage.activePage = page;
    this.getRecentPost(page);
  }
  onSelect(item) {
    this.selectTypeList = item;
  }
}
