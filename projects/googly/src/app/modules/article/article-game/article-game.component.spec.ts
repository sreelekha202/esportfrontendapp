import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { ArticleGameComponent } from "./article-game.component";

describe("ArticleGameComponent", () => {
  let component: ArticleGameComponent;
  let fixture: ComponentFixture<ArticleGameComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ArticleGameComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArticleGameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
