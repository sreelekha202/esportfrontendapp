import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FooterGooglyModule } from '../../shared/components/footer-googly/footer-googly.module';

import { ArticleAuthorComponent } from './article-author/article-author.component';
import { ArticleGameComponent } from './article-game/article-game.component';
import { ArticleMainComponent } from './article-main/article-main.component';
import { ArticleMoreComponent } from './article-more/article-more.component';
import { ArticleNewsComponent } from './article-news/article-news.component';
import { ArticlePostComponent } from './article-post/article-post.component';

export const contentRoutes: Routes = [
  {
    path: '',
    component: ArticleMainComponent,
    data: {
      tags: [
        {
          name: 'title',
          content:
            'Create an account with Spark today and never miss any news or update ! Join, follow, and share your love for esports!',
        },
      ],
      title: 'Game highlight',
    },
  },

  {
    path: 'news',
    component: ArticleNewsComponent,
    data: {
      tags: [
        {
          name: 'title',
          content:
            'Create an account with Spark today and never miss any news or update ! Join, follow, and share your love for esports!',
        },
      ],
      title: 'Game highlight',
    },
  },
  { path: 'author/:id', component: ArticleAuthorComponent },
  { path: 'author/:id/:articleId', component: ArticlePostComponent },
  { path: 'more', component: ArticleMoreComponent },
  { path: 'more/:id', component: ArticleMoreComponent },
  { path: ':id', component: ArticlePostComponent },
  {
    path: 'game/:id',
    component: ArticleGameComponent,
    data: {
      tags: [
        {
          name: 'title',
          content:
            'Create an account with Spark today and never miss any news or update ! Join, follow, and share your love for esports!',
        },
      ],
      title: 'Game highlight',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(contentRoutes)],
  exports: [RouterModule, FooterGooglyModule,],
})
export class ArticleRoutingModule { }
