import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit, Inject } from '@angular/core';
import { DOCUMENT, Location } from '@angular/common';
import { MatDialog } from '@angular/material/dialog';
import { Title } from '@angular/platform-browser';
import { TranslateService } from '@ngx-translate/core';
import {
  AppHtmlRoutes,
  AppHtmlRoutesArticleMoreType,
} from '../../../app-routing.model';
import {
  InfoPopupComponent,
  InfoPopupComponentData,
  InfoPopupComponentType,
} from '../../../shared/popups/info-popup/info-popup.component';
import { toggleHeight } from '../../../animations';
import {
  EsportsUserService,
  GlobalUtils,
  EsportsToastService,
  EsportsLanguageService,
  EsportsArticleService,
  EsportsLeaderboardService,
  IUser,
  IArticle,
  EsportsOptionService,
  CustomTranslatePipe,
  EsportsCommentService,
  EsportsUserPreferenceService,
  EsportsHomeService
} from 'esports';
import { environment } from '../../../../environments/environment';

const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;
@Component({
  selector: 'app-article-post',
  templateUrl: './article-post.component.html',
  styleUrls: ['./article-post.component.scss'],
  animations: [toggleHeight],
  providers: [CustomTranslatePipe],
})
export class ArticlePostComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;
  AppHtmlRoutesArticleMoreType = AppHtmlRoutesArticleMoreType;
  showLoader: boolean = true;
  article: IArticle;
  user: IUser;
  relatedArticles = [];
  latestArticle = [];
  highlighted = 'any';
  categoryname: 'any';
  followStatus: any = 'follow';
  hottestArticle = null;
  apiEndPoint = environment.apiEndPoint;
  categoryList: any;
  totalComments :any;
  constructor(
    @Inject(DOCUMENT) private document: Document,
    private activatedRoute: ActivatedRoute,
    private articleService: EsportsArticleService,
    private commentService: EsportsCommentService,
    private customTranslatePipe: CustomTranslatePipe,
    private globalUtils: GlobalUtils,
    private homeService: EsportsHomeService,
    private location: Location,
    private router: Router,
    private titleService: Title,
    private toastService: EsportsToastService,
    private translateService: TranslateService,
    private userService: EsportsUserService,
    public languageService: EsportsLanguageService,
    public matDialog: MatDialog,
    public userPreferenceService: EsportsUserPreferenceService,
    private leaderboardService: EsportsLeaderboardService,
    private optionService: EsportsOptionService
  ) {}

  // selectedArticle='All';
  // onFilterArticle(selectedArticleName){
  //   this.selectedArticle=selectedArticleName;
  // }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe((params) => {
      if (params?.id) {
        this.getArticleBySlug(params?.id);
      }
    });
    this.getCurrentUserDetails();
    this.getRecentPost();
  }

  totalComment(totalComments){
   this.totalComments = totalComments.length
  }

  onselect() {
    this.highlighted;
  }

  goBack() {
    this.location.back();
  }

  getRecentPost() {
    const pagination = JSON.stringify({ limit: 4, sort: { createdDate: -1 } });
    const query = JSON.stringify({
      articleStatus: 'publish',
    });

    const perfernce = JSON.stringify({
      prefernce: this.user?.preference?.game
        ? this.user?.preference?.game.map((item) => {
            return { gameDetails: item };
          })
        : '',
    });

    this.articleService
      .getLatestArticle(API, {
        pagination: pagination,
        query: query,
        preference: perfernce,
      })
      .subscribe(
        (res) => {
          this.latestArticle = res['data']['docs'];
          this.showLoader = false;
        },
        (err) => {
          this.showLoader = false;
        }
      );
  }

  getArticleBySlug = async (slug) => {
    try {
      this.showLoader = true;
      const article: any = await this.articleService.getArticleBySlug(
        API,
        slug
      );
      this.article = article.data;
      this.setcategory();
      if (!this.article) this.router.navigate(['/404']);
      this.showLoader = false;
      this.setMetaTags();
      this.updateView();
      this.trendingPost();
      this.showLoader = false;
    } catch (error) {
      this.showLoader = false;
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  getCurrentUserDetails() {
    this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.user = data;
      }
    });
  }

  setMetaTags() {
    this.titleService.setTitle(
      this.customTranslatePipe.transform(this.article?.title)
    );
    if (this.article?.image) {
      this.globalUtils.setMetaTags([
        {
          property: 'twitter:image',
          content: this.article.image,
        },
        {
          property: 'og:image',
          content: this.article.image,
        },
        {
          property: 'og:image:secure_url',
          content: this.article.image,
        },
        {
          property: 'og:image:url',
          content: this.article.image,
        },
        {
          property: 'og:image:width',
          content: '1200',
        },
        {
          property: 'og:image:height',
          content: '630',
        },
        {
          name: 'description',
          content: this.customTranslatePipe.transform(
            this.article?.shortDescription
          ),
        },
        {
          name: 'title',
          content: this.customTranslatePipe.transform(this.article?.title),
        },
        {
          property: 'og:description',
          content: this.customTranslatePipe.transform(
            this.article?.shortDescription
          ),
        },
        {
          property: 'twitter:description',
          content: this.customTranslatePipe.transform(
            this.article?.shortDescription
          ),
        },
        {
          property: 'og:title',
          content: this.customTranslatePipe.transform(this.article?.title),
        },
        {
          property: 'twitter:title',
          content: this.customTranslatePipe.transform(this.article?.title),
        },
        {
          property: 'og:url',
          content:
            this.document.location.protocol +
            '//' +
            this.document.location.hostname +
            this.router.url,
        },
      ]);
    }
  }

  updateView = async () => {
    const payload = {
      _id: this.article?._id,
      modalName: 'article',
    };
    await this.homeService.updateView(payload);
  };

  trendingPost = async () => {
    this.showLoader = true;
    try {
      const date = new Date();
      date.setDate(date.getDate() - 30);
      const pagination = JSON.stringify({ limit: 8, sort: { views: -1 } });
      const query = JSON.stringify({
        $and: [
          { articleStatus: 'publish' },
          { createdDate: { $gte: date, $lt: new Date() } },
        ],
      });
      const prefernce = JSON.stringify({
        prefernce: this.user?.preference?.game
          ? this.user?.preference?.game.map((item) => {
              return { gameDetails: item };
            })
          : '',
      });
      this.articleService
        .getPaginatedArticles(API, { pagination, query, preference: prefernce })
        .subscribe(
          (res: any) => {
            this.showLoader = false;
            this.relatedArticles = res['data']['docs'];
          },
          (err) => {}
        );
    } catch (error) {
      this.showLoader = false;
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  upsertBookmark = async () => {
    try {
      if (!this.user?._id) {
        this.toastService.showInfo(
          this.translateService.instant('ARTICLE.BOOKMARK_ALERT')
        );
        return;
      }

      if (this.article?.bookmarkProccesing) return;

      // Bookmark field
      const removeBookmark = `articleId=${this.article?._id}`;
      const addBookmark = { articleId: this.article._id };
      this.article.bookmarkProccesing = true;

      const bookmark: any = this.article?.isBookmarked
        ? await this.userPreferenceService
            .removeBookmark(removeBookmark)
            .toPromise()
        : await this.userPreferenceService.addBookmark(addBookmark).toPromise();
      this.article = {
        ...this.article,
        ...(bookmark?.data?.nModified == 1 && {
          isBookmarked: !this.article?.isBookmarked,
        }),
      };
      this.toastService.showSuccess(bookmark?.message);
      this.article.bookmarkProccesing = false;
    } catch (error) {
      this.article.bookmarkProccesing = false;
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };
  // bookMarkArticle(){
  //   this.onselect.map(item => {
  //     return {
  //       relatedArticles: item.relatedArticles
  //     }
  //   }).forEach(item => this.relatedArticles.push(item));
  // }
  //  bookMarkArticle() {
  //   let articles = this.article
  //   for(var i=0; i<= articles.length;i++){
  //     if(articles[i].isBookmarked == true){
  //       addBookmark.push(i,1);
  //     }

  //   }
  // }

  openSocialShareComponent() {
    const data: InfoPopupComponentData = {
      title: this.translateService.instant('ARTICLE_POST.SHARE'),
      text: ``,
      type: InfoPopupComponentType.socialSharing,
      cancelBtnText: 'Close',
    };

    this.matDialog.open(InfoPopupComponent, {
      data,
    });
  }

  likeOrUnLikeArticle = async (
    objectId: string,
    objectType: string,
    type: number
  ) => {
    try {
      if (!this.user?._id) {
        this.toastService.showInfo(
          this.translateService.instant('ARTICLE.LIKE_ALERT')
        );
        return;
      }

      if (this.article.likeProccesing) return;

      this.article.likeProccesing = true;
      const payload = {
        objectId,
        objectType,
        type: type == -1 ? 1 : Number(!type),
      };

      const like = await this.commentService.upsertLike(payload);
      this.article = { ...this.article, ...like.data };
      this.article.likeProccesing = false;
    } catch (error) {
      this.article.likeProccesing = false;
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  // onFollow() {
  //   if ( this.user) {
  //     this.showLoader = true;
  //     this.leaderboardService.followUser(this.user._id).subscribe((res) => {
  //       if (res.success) {

  //         this.toastService.showSuccess(res.message)
  //         this.userService.refreshCurrentUser();
  //       }
  //       this.showLoader = false;
  //     })
  //   } else {
  //     this.router.navigateByUrl('/user/email-login')
  //   }
  // }
  // User(){
  // }

  // Follow and Unfollow a user
  followUser() {
    if (this.article) {
      if (this.followStatus == 'follow') {
        this.leaderboardService.followUser(API, this.article.author).subscribe(
          (res: any) => {
            this.followStatus = 'unfollow';
            this.toastService.showSuccess(res.message);
            this.userService.refreshCurrentUser(API, TOKEN);
          },
          (err) => {
            this.showLoader = false;
          }
        );
      } else if (this.followStatus == 'unfollow') {
        this.leaderboardService
          .unfollowUser(API, this.article.author)
          .subscribe(
            (res: any) => {
              this.followStatus = 'follow';
              this.toastService.showSuccess(res.message);
              this.userService.refreshCurrentUser(API, TOKEN);
            }
            // (err) => {
            //   this.showLoader = false;
            // }
          );
      }
    } else {
      // Navigate to the login page with extras
      this.router.navigate(['/user/email-login']);
    }
  }

  setcategory = async () => {
    const option = await Promise.all([
      this.optionService.fetchAllCategories(API),
    ]);
    this.categoryList = option[0]?.data;
    for (const iterator of this.categoryList) {
      if (iterator._id == this.article.category) {
        this.categoryname = iterator.name;
      }
    }
  };

  getArticleIconByType(type: string): string {
    const path = 'assets/icons/news';

    switch (type) {
      case 'feature':
        return `${path}/feature.svg`;
      case 'opinion':
        return `${path}/opinion.svg`;
      case 'guide':
        return `${path}/guide.svg`;
      case 'listicle':
        return `${path}/listicle.svg`;
      case 'hot':
        return `${path}/hot.svg`;
      case 'trending':
        return `${path}/trending.svg`;
    }
  }
}
