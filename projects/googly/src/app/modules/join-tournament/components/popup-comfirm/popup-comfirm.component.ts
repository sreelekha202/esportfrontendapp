import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-popup-comfirm',
  templateUrl: './popup-comfirm.component.html',
  styleUrls: ['./popup-comfirm.component.scss']
})
export class PopupComfirmComponent implements OnInit {
  @Output() hidePopup = new EventEmitter();
  constructor() { }

  ngOnInit(): void {
  }

}
