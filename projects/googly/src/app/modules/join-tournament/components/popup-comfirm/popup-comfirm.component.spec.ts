import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PopupComfirmComponent } from './popup-comfirm.component';

describe('PopupComfirmComponent', () => {
  let component: PopupComfirmComponent;
  let fixture: ComponentFixture<PopupComfirmComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PopupComfirmComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PopupComfirmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
