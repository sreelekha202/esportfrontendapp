import { Component, OnInit } from '@angular/core';
import { AppHtmlRoutes } from '../../../../app-routing.model';
import { Router } from '@angular/router';
import { GlobalUtils, EsportsProfileService } from 'esports';
@Component({
  selector: 'app-solo-registration',
  templateUrl: './solo-registration.component.html',
  styleUrls: ['./solo-registration.component.scss'],
})
export class SoloRegistrationComponent implements OnInit {
  AppHtmlRoutes: AppHtmlRoutes;
  tournamentDetails: any;
  selectedteam: any;

  constructor(
    private profileService: EsportsProfileService,
    private router: Router
  ) {}

  selected: any;
  showLoader = false;
  ngOnInit(): void {
    if (GlobalUtils.isBrowser()) {
      this.tournamentDetails = localStorage.getItem('tournamentDetails');
      this.tournamentDetails = JSON.parse(this.tournamentDetails);
    }
  }
}
