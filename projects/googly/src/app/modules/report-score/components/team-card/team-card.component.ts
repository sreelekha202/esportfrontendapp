import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
@Component({
  selector: 'app-team-card',
  templateUrl: './team-card.component.html',
  styleUrls: ['./team-card.component.scss'],
})
export class TeamCardComponent implements OnInit {
  @Input() isManage: boolean = false;
  @Input() team;
  @Input() score;
  @Input() set;
  @Input() team_name;
  @Output() updateScore = new EventEmitter();
  constructor() { }
  ngOnInit(): void { }
  updateScoree(set, score, team_name, n, team_id) {
    this.updateScore.emit({
      set: set,
      score: score,
      team_name: team_name,
      n: n,
      team_id: team_id
    })
  }
}
