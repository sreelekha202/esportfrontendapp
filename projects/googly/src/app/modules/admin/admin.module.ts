import { FormsModule } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { AdminRoutingModule } from './admin-routing.module';
import { CoreModule } from '../../core/core.module';
import { AccessManagementComponent } from './components/access-management/access-management.component';
import { AdminComponent } from './admin.component';
import { AdminHeaderComponent } from './admin-header/admin-header.component';
import { AdminMenuComponent } from './admin-menu/admin-menu.component';
import { AnnouncementConfigurationComponent } from './components/announcement-configuration/announcement-configuration.component';
import { AutomatedDialogComponent } from './components/esports-payment-details/automated-dialog/automated-dialog.component';
import { BannerComponent } from './components/banner/banner.component';
import { ContentManagementComponent } from './components/content-management/content-management.component';
import { EmailNotificationsComponent } from './components/user-notifications/email-notifications/email-notifications.component';
import { EsportsManagementComponent } from './components/esports-management/esports-management.component';
import { EsportsPaymentComponent } from './components/esports-payment/esports-payment.component';
import { EsportsPaymentDetailsComponent } from './components/esports-payment-details/esports-payment-details.component';
import { GameListComponent } from './components/game-configuration/game-list/game-list.component';
import { UpsertGameComponent } from './components/game-configuration/upsert-game/upsert-game.component';
import { InboxNotificationsComponent } from './components/user-notifications/inbox-notifications/inbox-notifications.component';
import { LeaderboardConfigurationComponent } from './components/leaderboard-configuration/leaderboard-configuration.component';
import { PaymentDisbursementDialogComponent } from './components/esports-payment-details/payment-disbursement-dialog/payment-disbursement-dialog.component';
import { PaymentSuccessfullDialogComponent } from './components/esports-payment-details/payment-successfull-dialog/payment-successfull-dialog.component';
import { PrizeDisbursalComponent } from './components/prize-disbursal/prize-disbursal.component';
import { PushNotificationsComponent } from './components/user-notifications/push-notifications/push-notifications.component';
import { RegionConfigurationComponent } from './components/region-configuration/region-configuration.component';
import { RewardConfigurationComponent } from './components/reward-configuration/reward-configuration.component';
import { SendCouponDialogComponent } from './components/esports-payment-details/send-coupon-dialog/send-coupon-dialog.component';
import { SentNotificationsComponent } from './components/user-notifications/sent-notifications/sent-notifications.component';
import { ShopConfigurationComponent } from './components/shop-configuration/shop-configuration.component';
import { SiteConfigurationComponent } from './components/site-configuration/site-configuration.component';
import { TeamManagementComponent } from './components/team-management/team-management.component';
import { TeamManagementEditComponent } from './components/team-management-edit/team-management-edit.component';
import { TournamentDetailsComponent } from './components/tournament-details/tournament-details.component';
import { UserDetailsComponent } from './components/user-details/user-details.component';
import { UserManagementComponent } from './components/user-management/user-management.component';
import { UserNotificationsComponent } from './components/user-notifications/user-notifications.component';
import { ViewUserAccessComponent } from './components/view-user-access/view-user-access.component';
import { ViewUserAccessPopupComponent } from './popups/view-user-access/view-user-access-popup.component';
import { ViewUserDetailsComponent } from './components/view-user-details/view-user-details.component';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { PlatformFeeConfigurationComponent } from './components/platform-fee-configuration/platform-fee-configuration.component';
import { RegFeeDisbursalComponent } from './components/reg-fee-disbursal/reg-fee-disbursal.component';
import { RegFeeDisbursalDetailsComponent } from './components/reg-fee-disbursal-details/reg-fee-disbursal-details.component';
import { PrizeMoneyRefundComponent } from './components/prize-money-refund/prize-money-refund.component';
import { PrizeMoneyRefundDetailsComponent } from './components/prize-money-refund/prize-money-refund-details/prize-money-refund-details.component';
import { AvatarConfigComponent } from './components/avatar-config/avatar-config.component';
import { RegFeeRefundComponent } from './components/reg-fee-refund/reg-fee-refund.component';
import { RegFeeRefundDetailsComponent } from './components/reg-fee-refund/reg-fee-refund-details/reg-fee-refund-details.component';
import { SeasonManagementComponent } from './components/season-management/season-management.component';
import { environment } from '../../../environments/environment';
import { FormComponentModule } from '../../shared/components/form-component/form-component.module';
import { NgCircleProgressModule } from 'ng-circle-progress';
import { NgSelectModule } from '@ng-select/ng-select';
import { SharedModule } from '../../shared/modules/shared.module';
import { EsportsModule, EsportsSeasonModule, WYSIWYGEditorModule } from 'esports';
import { RouterBackModule } from '../../shared/directives/router-back.module';
import { BracketFormComponent } from './components/game-configuration/bracket-form/bracket-form.component';

// AoT requires an exported function for factories
export function HttpLoaderFactory(httpClient: HttpClient) {
  return new TranslateHttpLoader(httpClient);
}
@NgModule({
  declarations: [
    AccessManagementComponent,
    SeasonManagementComponent,
    AdminComponent,
    AdminHeaderComponent,
    AdminMenuComponent,
    AnnouncementConfigurationComponent,
    AutomatedDialogComponent,
    BannerComponent,
    ContentManagementComponent,
    EmailNotificationsComponent,
    EsportsManagementComponent,
    EsportsPaymentComponent,
    EsportsPaymentDetailsComponent,
    UpsertGameComponent,
    GameListComponent,
    InboxNotificationsComponent,
    LeaderboardConfigurationComponent,
    PaymentDisbursementDialogComponent,
    PaymentSuccessfullDialogComponent,
    PrizeDisbursalComponent,
    PushNotificationsComponent,
    RegionConfigurationComponent,
    RewardConfigurationComponent,
    SendCouponDialogComponent,
    SentNotificationsComponent,
    ShopConfigurationComponent,
    SiteConfigurationComponent,
    TeamManagementComponent,
    TeamManagementEditComponent,
    TournamentDetailsComponent,
    UserDetailsComponent,
    UserManagementComponent,
    UserNotificationsComponent,
    ViewUserAccessComponent,
    ViewUserAccessPopupComponent,
    ViewUserDetailsComponent,
    PrizeDisbursalComponent,
    PlatformFeeConfigurationComponent,
    RegFeeDisbursalComponent,
    RegFeeDisbursalDetailsComponent,
    PrizeMoneyRefundComponent,
    PrizeMoneyRefundDetailsComponent,
    AvatarConfigComponent,
    RegFeeRefundComponent,
    RegFeeRefundDetailsComponent,
    BracketFormComponent,
  ],
  imports: [
    AngularMultiSelectModule,
    AdminRoutingModule,
    CoreModule,
    FormComponentModule,
    FormsModule,
    NgCircleProgressModule.forRoot({}),
    SharedModule,
    RouterBackModule,
    NgSelectModule,
    WYSIWYGEditorModule.forRoot(environment),
    EsportsModule.forRoot(environment),
    EsportsSeasonModule.forRoot(environment),
  ],
})
export class AdminModule {}
