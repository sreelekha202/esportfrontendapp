import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { IUser, EsportsUserService } from 'esports';

import { AppHtmlAdminRoutes, AppHtmlRoutes } from '../../../app-routing.model';

import { filter, take } from 'rxjs/operators';

import { faBell } from '@fortawesome/free-solid-svg-icons';
import { environment } from '../../../../environments/environment';
const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;
@Component({
  selector: 'app-admin-menu',
  templateUrl: './admin-menu.component.html',
  styleUrls: ['./admin-menu.component.scss'],
})
export class AdminMenuComponent implements OnInit {
  AppHtmlAdminRoutes = AppHtmlAdminRoutes;

  currentUser: IUser;

  faBell = faBell;

  constructor(private userService: EsportsUserService, private router: Router) {}

  ngOnInit(): void {
    this.userService.currentUser
      .pipe(
        filter((data) => !!data),
        take(1)
      )
      .subscribe((data) => {
        if (data) {
          this.currentUser = data;
          if (this.currentUser.accessLevel.length > 0) {
            let route = '';
            switch (this.currentUser.accessLevel[0]) {
              case 'sc': {
                route = '/admin/site-configuration';
                break;
              }
              case 'acm': {
                route = '/admin/access-management';
                break;
              }
              case 'em': {
                route = '/admin/esports-management';
                break;
              }
              case 'cm': {
                route = '/admin/content-management';
                break;
              }
              case 'un': {
                route = '/admin/user-notifications';
                break;
              }
              case 'um': {
                route = '/admin/user-management';
                break;
              }
              case 'tm': {
                route = '/admin/team-management';
                break;
              }
              default: {
                route = undefined;
                break;
              }
            }
            if (route && this.router.url == '/admin') {
              this.router.navigate([route]);
            }
          }
        }
      });
  }

  isAllowed(_moduleType) {
    if (this.currentUser && this.currentUser.accessLevel) {
      return this.currentUser.accessLevel.includes(_moduleType) ? true : false;
    }
    return false;
  }

  onLogOut(): void {
    this.userService.logout(API, TOKEN);
    this.router.navigate([AppHtmlRoutes.login]);
  }
}
