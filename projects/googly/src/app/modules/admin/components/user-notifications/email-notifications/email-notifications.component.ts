import { Component, OnInit, ViewChild, OnDestroy } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { GlobalUtils, EsportsNotificationsService } from "esports";
import {
  InfoPopupComponentData,
  InfoPopupComponentType,
  InfoPopupComponent,
} from "../../../../../shared/popups/info-popup/info-popup.component";
import { TranslateService, LangChangeEvent } from "@ngx-translate/core";
import { MatDialog } from "@angular/material/dialog";
import { environment } from '../../../../../../environments/environment';
const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;

@Component({
  selector: "app-email-notifications",
  templateUrl: "./email-notifications.component.html",
  styleUrls: [
    "./email-notifications.component.scss",
    "../user-notifications.component.scss",
  ],
})
export class EmailNotificationsComponent implements OnInit, OnDestroy {
  emailNotificationForm = this.fb.group({
    userSegment: ["", Validators.required],
    emailTitle: ["", Validators.required],
    headerText: ["", Validators.required],
    message: ["", Validators.required],
  });
  isLoading = false;
  @ViewChild("bannerUpload", { static: false }) set bannerUpload(element) {
    if (GlobalUtils.isBrowser() && element && this.headerImageUri) {
      document.getElementById("bannerUpload").style.background =
        "url(" + this.headerImageUri + ")";
    }
  }
  headerImageUri = "";
  userSegementValues = {
    ALL_USERS: this.translateService.instant(
      "ADMIN.USER_NOTIFICATION.LABEL.ALL_USERS"
    ),
    TOURNAMENT_PLAYERS: this.translateService.instant(
      "ADMIN.USER_NOTIFICATION.LABEL.TOURNAMENT_PLAYERS"
    ),
    TOURNAMENT_ORGANIZERS: this.translateService.instant(
      "ADMIN.USER_NOTIFICATION.LABEL.TOURNAMENT_ORGANIZER"
    ),
  };
  isEmailSent = false;

  constructor(
    private fb: FormBuilder,
    private userNotificationsService: EsportsNotificationsService,
    private translateService: TranslateService,
    private dialog: MatDialog
  ) {
    translateService.onLangChange.subscribe((event: LangChangeEvent) => {
      this.userSegementValues = {
        ALL_USERS: this.translateService.instant(
          "ADMIN.USER_NOTIFICATION.LABEL.ALL_USERS"
        ),
        TOURNAMENT_PLAYERS: this.translateService.instant(
          "ADMIN.USER_NOTIFICATION.LABEL.TOURNAMENT_PLAYERS"
        ),
        TOURNAMENT_ORGANIZERS: this.translateService.instant(
          "ADMIN.USER_NOTIFICATION.LABEL.TOURNAMENT_ORGANIZER"
        ),
      };
    });
  }

  ngOnInit(): void {}

  ngOnDestroy(): void {
    this.resetControls();
  }

  sendEmails() {
    this.isLoading = true;
    this.isEmailSent = true;
    const inputData = {
      title: this.emailNotificationForm.value.emailTitle,
      message: this.emailNotificationForm.value.message,
      userSegment: this.emailNotificationForm.value.userSegment,
      headerText: this.emailNotificationForm.value.headerText,
      headerImage: this.headerImageUri,
    };
    this.userNotificationsService.sendEmails(API,inputData).subscribe(
      (response: any) => {
        this.resetControls();
        this.isLoading = false;
        this.isEmailSent = false;
        const afterBlockData: InfoPopupComponentData = {
          title: this.translateService.instant(
            "ADMIN.USER_NOTIFICATION.RESPONSE.SUCCESS"
          ),
          text: response.message,
          type: InfoPopupComponentType.info,
        };
        this.dialog.open(InfoPopupComponent, { data: afterBlockData });
      },
      (err: any) => {
        this.isLoading = false;
        this.isEmailSent = false;
        const afterBlockData: InfoPopupComponentData = {
          title: this.translateService.instant(
            "ADMIN.USER_NOTIFICATION.RESPONSE.ERROR"
          ),
          text: err.error.message,
          type: InfoPopupComponentType.info,
        };
        this.dialog.open(InfoPopupComponent, { data: afterBlockData });
      }
    );
  }

  get userSegment() {
    return this.emailNotificationForm.get("userSegment");
  }

  markFormGroupTouched(formGroup: FormGroup) {
    (<any>Object).values(formGroup.controls).forEach((control) => {
      control.markAsTouched();

      if (control.controls) {
        this.markFormGroupTouched(control);
      }
    });
  }

  selectType(e) {
    this.emailNotificationForm.controls["userSegment"].setValue(e);
  }
  updateEmail() {
    if (!this.emailNotificationForm.valid) {
      this.markFormGroupTouched(this.emailNotificationForm);
      return;
    }
    if (!this.isEmailSent) {
      this.sendEmails();
    }
  }
  toggleEditPane() {}
  resetControls() {
    this.emailNotificationForm.reset();
    this.headerImageUri = "";
    this.isLoading = false;
    document.getElementById("bannerUpload").style["background"] = "";
  }
  uploadImage() {
    const banner: any = document.getElementById("bannerUploader");
    banner.click();
    banner.onchange = function () {
      const reader = new FileReader();
      reader.onload = function (event: any) {
        const img = new Image();
        img.onload = function () {
          this.headerImageUri = event.target.result;
          document.getElementById("bannerUpload").style["background"] =
            "url(" + event.target.result + ")";
        }.bind(this);
        img.onerror = function () {
          alert("not a valid file: " + banner.files[0].type);
        };
        img.src = window.URL.createObjectURL(banner.files[0]);
      }.bind(this);
      reader.readAsDataURL(banner.files[0]);
    }.bind(this);
  }
  get emailTitle() {
    return this.emailNotificationForm.get("emailTitle");
  }

  public hasError = (controlName: string, errorName: string) => {
    return this.emailNotificationForm.controls[controlName].hasError(errorName);
  };

  get headerText() {
    return this.emailNotificationForm.get("headerText");
  }

  get message() {
    return this.emailNotificationForm.get("message");
  }
}
