import { Component, OnInit, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";

@Component({
  selector: "app-automated-dialog",
  templateUrl: "./automated-dialog.component.html",
  styleUrls: [
    "./automated-dialog.component.scss",
    "../esports-payment-details.component.scss",
  ],
})
export class AutomatedDialogComponent implements OnInit {
  constructor(
    @Inject(MAT_DIALOG_DATA) public data,
    public dialogRef: MatDialogRef<AutomatedDialogComponent>
  ) {}

  ngOnInit(): void {}

  onClose(text): void {
    this.dialogRef.close(text);
  }

  onConfirm() {}
}
