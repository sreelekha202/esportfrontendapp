import { Component, OnInit, ViewChild } from '@angular/core';

import { DatatableComponent } from '@swimlane/ngx-datatable';
import { MatDialog } from '@angular/material/dialog';
import { TranslateService } from '@ngx-translate/core';
import { EsportsToastService, GlobalUtils, IGame, EsportsGameService } from 'esports';


import {
  InfoPopupComponent,
  InfoPopupComponentData,
  InfoPopupComponentType,
} from '../../../../../shared/popups/info-popup/info-popup.component';
import { environment } from '../../../../../../environments/environment';

const API = environment.apiEndPoint;
@Component({
  selector: 'game-list',
  templateUrl: './game-list.component.html',
  styleUrls: ['./game-list.component.scss'],
})
export class GameListComponent implements OnInit {
  @ViewChild(DatatableComponent) table: DatatableComponent;
  isLoading: boolean = true;
  showGameTable: boolean = true;
  editableGame: any = null;
  platformList = [];
  rowsFirstTab = [];
  tempTable = [];

  columnsFirstTab = [
    { name: 'name' },
    { name: 'bracketTypes' },
    { name: 'platforms' },
  ];

  constructor(
    private gameService: EsportsGameService,
    private translateService: TranslateService,
    public dialog: MatDialog,
    public globalUtils: GlobalUtils,
    public eSportsToastService: EsportsToastService
  ) {}

  ngOnInit(): void {
    this.fetchPlatformsAndGames();
  }

  fetchPlatformsAndGames = async () => {
    try {
      this.isLoading = true;
      const [game, platform] = await Promise.all([
        this.gameService.getAdminAllGames(API).toPromise(),
        this.gameService.fetchPlatforms(API).toPromise(),
      ]);
      const mapCallback = (item: any) => {
        return {
          id: item._id,
          itemName: item.name,
        };
      };
      this.rowsFirstTab = game?.data || [];
      this.tempTable = game?.data || [];
      this.platformList = platform?.data?.map(mapCallback) || [];
      this.isLoading = false;
    } catch (error) {
      this.isLoading = false;
      const errorMessage = error?.error?.message || error?.message;
      const afterBlockData: InfoPopupComponentData = {
        title: this.translateService.instant('API.GAME.GET.ERROR_HEADER'),
        text: errorMessage,
        type: InfoPopupComponentType.info,
      };
      this.dialog.open(InfoPopupComponent, { data: afterBlockData });
    }
  };

  updateGameOrder = async (order: any, game: any) => {
    try {
      if (!order) return;
      const isOrderNumberExist = this.rowsFirstTab.find(
        (el) => el.order == order
      );
      if (isOrderNumberExist) {
        this.eSportsToastService.showError(
          this.translateService.instant(
            'ADMIN.SITE_CONFIGURATION.SUB_PAGE.UPDATE_ORDER'
          )
        );
        return;
      }
      this.isLoading = true;
      const updatedGame = await this.gameService
        .saveGame(API, game?._id, { order })
        .toPromise();
      const title = this.translateService.instant(
        'API.GAME.PUT.SUCCESS_HEADER'
      );
      const type = InfoPopupComponentType.info;
      this.showDialogueBlock(title, updatedGame?.message, type);
      game.isEdit = false;
      game.order = order;
      this.isLoading = false;
    } catch (error) {
      this.isLoading = false;
      const title = this.translateService.instant('API.GAME.PUT.ERROR_HEADER');
      const errorMessage = error?.error?.message || error?.message;
      const type = InfoPopupComponentType.info;
      this.showDialogueBlock(title, errorMessage, type);
    }
  };

  /**
   * method to be called to delete the game
   * @param _gameId
   */
  onRemoveGame = async (_gameId: string) => {
    const title = this.translateService.instant('API.GAME.DELETE.ERROR_HEADER');
    const type = InfoPopupComponentType.info;
    try {
      this.isLoading = true;
      const deleteGame = await this.gameService
        .disableGame(API,_gameId)
        .toPromise();
      this.showDialogueBlock(title, deleteGame?.message, type);
      this.isLoading = false;
    } catch (error) {
      this.isLoading = false;
      const errorMessage = error?.error?.message || error?.message;
      this.showDialogueBlock(title, errorMessage, type);
    }
  };

  searchByValueFilter(event) {
    const value = event.target.value.toLowerCase().trim();
    const filterData = this.tempTable.filter((item) => {
      return item && item['name'].toLowerCase().includes(value);
    });
    this.rowsFirstTab = [...filterData];
    this.table.offset = 0;
  }

  showDialogueBlock(title: string, text: string, type: InfoPopupComponentType) {
    const afterBlockData: InfoPopupComponentData = {
      title,
      text,
      type,
    };
    this.dialog.open(InfoPopupComponent, { data: afterBlockData });
  }

  /**
   * method to toggle the edit panel
   */
  toggleGamePanel(game: any = null) {
    this.showGameTable = !this.showGameTable;
    this.editableGame = game;
  }

  getChildResponse(isRefresh: boolean = false) {
    if (isRefresh) {
       this.fetchPlatformsAndGames();
    }
    this.showGameTable = true;
    this.editableGame = null;
  }
}
