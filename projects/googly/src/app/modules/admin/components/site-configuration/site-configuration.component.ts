import { faTrash } from "@fortawesome/free-solid-svg-icons";
import { MatTabChangeEvent } from "@angular/material/tabs";
import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  EventEmitter,
} from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { debounceTime } from "rxjs/operators";

enum SiteConfigurationTabs {
  siteCustomize,
  rewards,
  Alerts,
  banner,
}

@Component({
  selector: "app-site-configuration",
  templateUrl: "./site-configuration.component.html",
  styleUrls: ["./site-configuration.component.scss"],
})
export class SiteConfigurationComponent implements OnInit {
  siteConfigurationTabs = SiteConfigurationTabs;
  faTrash = faTrash;
  backgroundColour = "#FC4500";
  activeButtonColor = "#0C0E0F";
  nonActiveButtonColor = "#FC4500";
  viewIconShow = true;
  deleteIconShow = true;
  checkIconShow = false;
  activeTabIndex = 0;
  customMatTabEvent = new EventEmitter<MatTabChangeEvent>();
  constructor() {}

  ngOnInit(): void {
    this.customMatTabEvent
      .pipe(debounceTime(30))
      .subscribe((tabChangeEvent) => {
        this.activeTabIndex = tabChangeEvent.index;
      });
  }

  onTabChange(tabChangeEvent: MatTabChangeEvent): void {
    this.customMatTabEvent.emit(tabChangeEvent);
  }
}
