import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FooterGooglyModule } from '../../shared/components/footer-googly/footer-googly.module';
import { HeaderBottomModule } from "../../shared/components/header-bottom/header-bottom.module";
import { SharedModule } from '../../shared/modules/shared.module';
import { OverviewRoutingModule } from './overview-routing.module';
import { OverviewComponent } from './overview.component';
import { SeasonCardOverviewComponent } from './season-card-overview/season-card-overview.component';
import { TournamentCardOverviewComponent } from './tournament-card-overview/tournament-card-overview.component';

import {FooterMenuMobileModule } from '../../shared/components/footer-menu-mobile/footer-menu-mobile.module';
import { EsportsLoaderModule } from 'esports';
const components = [
  OverviewComponent,
  TournamentCardOverviewComponent,
  SeasonCardOverviewComponent
];

const modules = [
  SharedModule,
  RouterModule,
  OverviewRoutingModule,
  HeaderBottomModule,
  FooterGooglyModule,
  FooterMenuMobileModule,
  EsportsLoaderModule
];

@NgModule({
  declarations: components,
  imports: modules,
})
export class OverviewModule { }
