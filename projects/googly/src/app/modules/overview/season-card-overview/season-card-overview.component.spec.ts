import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SeasonCardOverviewComponent } from './season-card-overview.component';

describe('SeasonCardOverviewComponent', () => {
  let component: SeasonCardOverviewComponent;
  let fixture: ComponentFixture<SeasonCardOverviewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SeasonCardOverviewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SeasonCardOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
