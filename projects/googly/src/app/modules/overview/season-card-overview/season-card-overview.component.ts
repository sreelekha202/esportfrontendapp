import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-season-card-overview',
  templateUrl: './season-card-overview.component.html',
  styleUrls: ['./season-card-overview.component.scss']
})
export class SeasonCardOverviewComponent implements OnInit {
  @Input() dataList;
  constructor(public router: Router,) { }

  ngOnInit(): void {
  }
}
