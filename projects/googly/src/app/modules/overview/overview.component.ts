import { Component, OnInit } from '@angular/core';
import {
  AppHtmlOverviewRoutes,
  AppHtmlRoutes,
  AppHtmlGetMatchRoutes,
} from '../../app-routing.model';
import {
  EsportsHomeService,
  EsportsTournamentService,
  EsportsGameService,
  GlobalUtils,
  EsportsSeasonService,
  EsportsToastService,
} from 'esports';
import { environment } from '../../../environments/environment';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;
@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.scss'],
})
export class OverviewComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;
  AppHtmlOverviewRoutes = AppHtmlOverviewRoutes;
  AppHtmlGetMatchRoutes = AppHtmlGetMatchRoutes;
  onGoingTournaments = [];
  showLoader: boolean = true;
  selectedGame: any;
  TournamentsList = [];
  params: any = {};
  gameId: any;
  selectedTournament: any;
  gameDetail: any;
  game: any;
  text: string = '';

  selected_game: any = '';
  gameList = [];
  seasonsList = [];
  season_period_list = [
    {
      id: 1,
      name: '20 days',
    },
  ];
  season_period = this.season_period_list[0].id;
  paginationData = {
    page: 1,
    limit: 10,
    sort: 'startDate',
  };
  paginationDetails: any;
  pageSizeOptions = environment.pageSizeOptions;
  selectedGameType: null;
  bannerId: any;
  gameImage: string;
  seasonImage: string;
  constructor(
    private tournamentService: EsportsTournamentService,
    private homeService: EsportsHomeService,
    private gameService: EsportsGameService,
    private activatedRoute: ActivatedRoute,
    public router: Router,
    private esportsSeasonService: EsportsSeasonService,
    private translateService: TranslateService,
    private toastService: EsportsToastService
  ) { }

  ngOnInit(): void {
    this.showLoader = true;
    this.getOnGoingTournaments();
    this.getAllTournaments();
    this.getAllSeasons();
    this.getGames();
  }

  nextPage(game) {
    this.selectedGameType = game.type;
    if (this.selectedGameType) {
      if (game.type == 'team') {
        this.router.navigateByUrl('/get-match/platforms');
      } else {
        this.router.navigateByUrl('/get-match/platforms-single');
      }
    } else {
      this.toastService.showError(
        this.translateService.instant('GAMES.MATCHMAKING.STEP4.TITLE2')
      );
    }
  }

  getAllTournaments() {
    this.showLoader = true;
    this.homeService.getTournament(this.params).subscribe(
      (res: any) => {
        this.showLoader = false;
        if (res?.data && res?.data?.docs && res?.data?.docs.length > 0) {
          this.TournamentsList = [];
          let TournamentsList = res?.data?.docs;
          TournamentsList.map((item) => {
            this.gameDetail = item?.gameDetail?._id
            // this.gameImage=`https://spark-image-dev.s3.ap-south-1.amazonaws.com/dev/banner/${this.gameDetail}.jpg`
            // this.seasonImage=`https://spark-image-dev.s3.ap-south-1.amazonaws.com/dev/season/${this.gameDetail}.jpg`
            item?.isFeature ? this.TournamentsList.push(item) : '';
          });
          this.gameDetail = res?.data?.docs[0].gameDetail;
          this.gameService.createMatchMakingSubject.next({
            selectedGame: this.gameDetail,
            selectedPlatForm: res?.data?.docs[0].platform,
          });
        }
      },
      (err: any) => {
        this.showLoader = false;
      }
    );
  }

  getOnGoingTournaments() {
    this.showLoader = true;
    this.tournamentService
      .getPaginatedTournaments(API, {
        status: 1,
      })
      .subscribe(
        (res: any) => {
          if (res?.data && res?.data?.docs && res?.data?.docs.length > 0) {
            this.onGoingTournaments = [];
            let onGoingTournaments = res?.data?.docs;
            onGoingTournaments.map((item) => {
              item?.isFeature ? this.onGoingTournaments.push(item) : '';
            });
          }
          this.showLoader = false;
        },
        (err) => {
          this.showLoader = false;
        }
      );
  }

  getAllSeasons() {
    this.showLoader = true;
    let query: any = `isActive=true&fields=isFeatured,name,slug,endDate,startDate,image,duration&sort=-createdOn&limit=${this.paginationData.limit}&page=${this.paginationData.page}&game=${this.gameId}`;
    this.esportsSeasonService.getSeasons(API, query).subscribe(
      (res) => {
        this.showLoader = false;
        this.seasonsList = res.data.data.docs;
      },
      (err) => {
        this.showLoader = false;
      }
    );
  }

  getGames() {
    this.showLoader = true;
    this.gameService.getGames(API).subscribe(
      (res) => {
        this.showLoader = false;
        this.gameList = res.data;
      },
      (err) => {
        this.showLoader = false;
      }
    );
  }
}
