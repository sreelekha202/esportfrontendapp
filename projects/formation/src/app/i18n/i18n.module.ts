import { HttpClient, HttpClientModule } from "@angular/common/http";
import { NgModule } from "@angular/core";
import { TransferState } from "@angular/platform-browser";

import { LanguageService, ConstantsService } from "../core/service";

import { GlobalUtils } from "../shared/service/global-utils/global-utils";
import { TranslateFsLoader } from "./translate-fs-loader";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";
import {
  TranslateLoader,
  TranslateModule,
  TranslateService,
} from "@ngx-translate/core";

@NgModule({
  imports: [
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: translateLoaderFactory,
        deps: [HttpClient, TransferState],
      },
    }),
  ],
  exports: [TranslateModule],
})
export class I18nModule {
  constructor(
    translateService: TranslateService,
    languageService: LanguageService
  ) {
    translateService.addLangs(ConstantsService?.language.map((el) => el.code));
    const browserLang = GlobalUtils.isBrowser()
      ? localStorage.getItem("currentLanguage") ||
        ConstantsService?.defaultLangCode
      : ConstantsService?.defaultLangCode || "en";
    languageService.setLanguage(browserLang);
    translateService.use(browserLang || ConstantsService?.defaultLangCode);
  }
}

export function translateLoaderFactory(httpClient: HttpClient) {
  return new TranslateHttpLoader(
    httpClient,
    "/assets/i18n/",
    ".json?cb=" + new Date().getTime()
  );
}
