import { Injectable } from "@angular/core";
import { makeStateKey, TransferState } from "@angular/platform-browser";

import { join } from "path";
import { Observable, of } from "rxjs";
import { readFileSync } from "fs";
import { TranslateLoader } from "@ngx-translate/core";

@Injectable({
  providedIn: "root",
})
export class TranslateFsLoader implements TranslateLoader {
  private prefix = "i18n";
  private suffix = ".json";

  constructor(private transferState: TransferState) {}

  public getTranslation(lang: string): Observable<any> {
    let randomString = new Date().getTime();
    const path = join(
      __dirname,
      "../browser/assets/",
      this.prefix,
      `${lang}${this.suffix}?cb=${randomString}`
    );

    const data = JSON.parse(readFileSync(path, "utf8"));
    // ADDED: store the translations in the transfer state:
    const key = makeStateKey<any>("transfer-translate-" + lang);
    this.transferState.set(key, data);
    return of(data);
  }
}
