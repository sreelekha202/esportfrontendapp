import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { AuthGuard } from "./shared/guard/auth.guard";

import { GamesComponent } from "./modules/games/games.component";
import { Home2Component } from "./modules/home2/home2.component";
import { LeaderBoardComponent } from "./modules/leader-board/leader-board.component";
import { LeaderBoardPlayerComponent } from "./modules/leader-board-player/leader-board-player.component";
import { NotfoundComponent } from "./modules/not-found/not-found.component";
import { PrivacyPolicyComponent } from "./modules/privacy-policy/privacy-policy.component";

const routes: Routes = [
  { path: "leaderboard", component: LeaderBoardComponent },
  { path: "leaderboard/:userId", component: LeaderBoardPlayerComponent },
  { path: "games", component: GamesComponent },
  { path: "home2", component: Home2Component },
  {
    path: "home",
    component: Home2Component,
    data: {
      tags: [
        {
          name: "description",
          content:
            "FORMATION.gg is the esports platform for mobile gaming, supporting all game titles, for all tournament organizers, casual and competitive gamers, and esports fans. Formation: A place where esports community can grow, create  teams, test your mettle against the best. And win the prizes you  deserve. ",
        },
        {
          name: "og:description",
          content:
            "FORMATION.gg is the esports platform for mobile gaming, supporting all game titles, for all tournament organizers, casual and competitive gamers, and esports fans. Formation: A place where esports community can grow, create  teams, test your mettle against the best. And win the prizes you  deserve. ",
        },
        {
          name: "twitter:description",
          content:
            "FORMATION.gg is the esports platform for mobile gaming, supporting all game titles, for all tournament organizers, casual and competitive gamers, and esports fans. Formation: A place where esports community can grow, create  teams, test your mettle against the best. And win the prizes you  deserve. ",
        },
        {
          name: "title",
          content:
            "FORMATION.gg is the esports platform for mobile gaming, supporting all game titles, for all tournament organizers, casual and competitive gamers, and esports fans. Formation: A place where esports community can grow, create  teams, test your mettle against the best. And win the prizes you  deserve. ",
        },
        {
          name: "keywords ",
          content: "esports , Formation",
        },
      ],
      title: "FORMATION.gg | FORMATION",
    },
  },
  {
    path: "tournament",
    loadChildren: () =>
      import("./modules/tournament/tournament.module").then(
        (m) => m.TournamentModule
      ),
    data: {
      tags: [
        {
          name: "description",
          content:
            "FORMATION.gg supports and covers more than [99] esports games. Create/Organize, join and follow your favourite esports tournaments today!",
        },
        {
          name: "og:description",
          content:
            "FORMATION.gg supports and covers more than [99] esports games. Create/Organize, join and follow your favourite esports tournaments today!",
        },
        {
          name: "twitter:description",
          content:
            "FORMATION.gg supports and covers more than [99] esports games. Create/Organize, join and follow your favourite esports tournaments today!",
        },
        {
          name: "title",
          content:
            "FORMATION.gg supports and covers more than [99] esports games. Create/Organize, join and follow your favourite esports tournaments today!",
        },
        { name: "keywords ", content: "esports, tournament" },
      ],
      title: "FORMATION TOURNAMENTS",
    },
  },
  {
    path: "article/create",
    loadChildren: () =>
      import("./modules/article/create-article/create-article.module").then(
        (m) => m.CreateArticleModule
      ),
    data: {
      tags: [
        {
          name: "title",
          content: "FORMATION",
        },
      ],
      title: "FORMATION",
    },
  },
  {
    path: "article/edit/:id",
    loadChildren: () =>
      import("./modules/article/create-article/create-article.module").then(
        (m) => m.CreateArticleModule
      ),
  },
  {
    path: "article",
    loadChildren: () =>
      import("./modules/article/article.module").then((m) => m.ArticleModule),
    data: {
      tags: [
        {
          name: "description",
          content:
            "Get the latest news and updates on everything about esports on FORMATION.gg.",
        },
        {
          name: "og:description",
          content:
            "Get the latest news and updates on everything about esports on FORMATION.gg.",
        },
        {
          name: "twitter:description",
          content:
            "Get the latest news and updates on everything about esports on FORMATION.gg.",
        },
        {
          name: "title",
          content:
            "Get the latest news and updates on everything about esports on FORMATION.gg.",
        },
        { name: "keywords ", content: "esports, news, esportsnews" },
      ],
      title: "GAMING NEWS & ARTICLES | FORMATION",
    },
  },
  {
    path: "user/:pageType",
    loadChildren: () =>
      import("./modules/log-reg/log-reg.module").then((m) => m.LogRegModule),
    data: {
      isRootPage: true,
      tags: [
        {
          name: "description",
          content:
            "Start your esports journey with  FORMATION.gg. A complete esports tournament management platform",
        },
        {
          name: "og:description",
          content:
            "Start your esports journey with  FORMATION.gg. A complete esports tournament management platform",
        },
        {
          name: "twitter:description",
          content:
            "Start your esports journey with  FORMATION.gg. A complete esports tournament management platform",
        },
        {
          name: "title",
          content:
            "Start your esports journey with  FORMATION.gg. A complete esports tournament management platform",
        },
      ],
      title: "FORMATION",
    },
  },
  {
    path: "privacy-policy",
    component: PrivacyPolicyComponent,
  },
  {
    path: "create-team",
    loadChildren: () =>
      import("./modules/create-team/create-team.module").then(
        (m) => m.CreateTeamModule
      ),
    canActivate: [AuthGuard],
    data: {
      isRootPage: true,
    },
  },
  {
    path: "edit-team/:id",
    loadChildren: () =>
      import("./modules/create-team/create-team.module").then(
        (m) => m.CreateTeamModule
      ),
    canActivate: [AuthGuard],
    data: {
      isRootPage: true,
    },
  },
  {
    path: "manage-team",
    loadChildren: () =>
      import("./modules/manage-team/manage-team.module").then(
        (m) => m.ManageTeamModule
      ),
    canActivate: [AuthGuard],
    data: {
      isRootPage: true,
    },
  },
  {
    path: "team-registration/:id",
    loadChildren: () =>
      import("./modules/team-registration/team-registration.module").then(
        (m) => m.TeamRegistrationModule
      ),
    canActivate: [AuthGuard],
    data: {
      isRootPage: true,
    },
  },
  {
    path: "videos",
    loadChildren: () =>
      import("./modules/videos/videos.module").then((m) => m.VideosModule),
  },
  {
    path: "search",
    loadChildren: () =>
      import("./modules/search/search.module").then((m) => m.SearchModule),
  },
  {
    path: "bracket",
    canActivate: [AuthGuard],
    loadChildren: () =>
      import("./modules/bracket/bracket.module").then((m) => m.BracketModule),
  },
  {
    path: "profile",
    canActivate: [AuthGuard],
    loadChildren: () =>
      import("./modules/profile/profile.module").then((m) => m.ProfileModule),
  },
  {
    path: "admin",
    loadChildren: () =>
      import("./modules/admin/admin.module").then((m) => m.AdminModule),
  },
  {
    path: "web-view",
    loadChildren: () =>
      import("./modules/web-view/web-view.module").then((m) => m.WebViewModule),
  },
  { path: "", redirectTo: "/home", pathMatch: "full" },
  { path: "404", component: NotfoundComponent },
  { path: "**", redirectTo: "/404", pathMatch: "full" },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      scrollPositionRestoration: "enabled",
      initialNavigation: "enabled",
    }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
