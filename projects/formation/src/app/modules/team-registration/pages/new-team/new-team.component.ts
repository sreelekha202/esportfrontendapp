import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import {
  AppHtmlTeamRegRoutes,
  AppHtmlRoutes,
} from "../../../../app-routing.model";

@Component({
  selector: "app-new-team",
  templateUrl: "./new-team.component.html",
  styleUrls: ["./new-team.component.scss"],
})
export class NewTeamComponent implements OnInit {
  AppHtmlTeamRegRoutes = AppHtmlTeamRegRoutes;
  AppHtmlRoutes = AppHtmlRoutes;

  @Output() submit = new EventEmitter();
  @Input() teams;

  constructor() {}

  ngOnInit(): void {}

  ngOnChanges() {}

  selectTeam(i) {
    this.submit.emit(this.teams[i]);
  }
}
