import { Component, OnDestroy, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { TranslateService } from "@ngx-translate/core";
import { Subscription } from "rxjs";
import { TournamentService, UserService } from "../../core/service";
import { IUser } from "../../shared/models";
import { ToastService } from "../../shared/toast/toast.service";

@Component({
  selector: "app-team-registration",
  templateUrl: "./team-registration.component.html",
  styleUrls: ["./team-registration.component.scss"],
})
export class TeamRegistrationComponent implements OnInit, OnDestroy {
  teamRegistrationActiveStep = 0;
  userSubscription: Subscription;
  user: IUser;
  tournament: any = {};
  teamList: any = [];
  teamSelected: any = {};
  teamMemberList: any = {};
  isLoaded = false;
  participantRS: string | null;
  showParticipantForm = false;
  prizeSum;
  constructor(
    private userService: UserService,
    private tournamentService: TournamentService,
    private activatedRoute: ActivatedRoute,
    private translateService: TranslateService,
    private toastService: ToastService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.getCurrentUserDetails();
  }

  ngOnDestroy() {
    this.userSubscription?.unsubscribe();
  }

  getCurrentUserDetails() {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.user = data;
        const pId = this.activatedRoute.snapshot.params.pId;
        if (pId) {
          this.tournament.slug = this.router.url.split("/").reverse()[2];
          this.fetchTournamentDetails(this.tournament?.slug);
          this.getMyTeams();
        } else {
          this.tournament.slug = this.router.url.split("/").reverse()[0];
          this.fetchTournamentDetails(this.tournament?.slug);
          this.getMyTeams();
        }
      }
    });
  }

  /**
   * Fetch Some tournament Details
   * @param slug
   */
  fetchTournamentDetails = async (slug) => {
    try {
      this.isLoaded = false;
      const tournament = await this.tournamentService
        .getTournamentBySlug(slug)
        .toPromise();
      this.tournament = tournament.data;
      const totalPrice = this.tournament.prizeList || [];
      this.prizeSum = totalPrice.reduce((acc, el) => acc + el.value, 0);

      if (!this.tournament) {
        throw new Error(
          this.translateService.instant(
            "VIEW_TOURNAMENT.JOIN_PARTICIPANT.INVALID_TOURNAMENT"
          )
        );
      }
      const participantId = this.activatedRoute?.snapshot?.params?.pId;
      const {
        data,
      } = await this.tournamentService.fetchParticipantRegistrationStatus(
        this.tournament?._id,
        participantId
      );
      this.participantRS = data?.type;
      this.showParticipantForm = [
        "join",
        "already-registered",
        "checked-in",
        "already-checked-in",
      ].includes(this.participantRS);
      this.getMyTeams();
      this.isLoaded = true;
    } catch (error) {
      this.isLoaded = true;
      this.toastService.showError(error?.statusText || error?.message);
    }
  };

  getMyTeams = async () => {
    if (this.tournament?._id) {
      const data = await this.userService.getJoinMyTeam(this.tournament?._id);
      if (data) {
        data?.data.forEach((control, index) => {
          let totalMemberSize = 0;

          if (this.tournament?.substituteMemberSize > 0) {
            totalMemberSize +=
              this.tournament?.teamSize + this.tournament?.substituteMemberSize;
          } else {
            totalMemberSize = this.tournament?.teamSize;
          }
          if (control.teamSize >= totalMemberSize) {
            this.teamList.push(control);
          }
        });
      }
    }
  };

  async selectedTeam(data) {
    const teamMembersData: any = await this.userService.getTeamMember(data._id);
    this.teamRegistrationActiveStep = 1;
    this.teamSelected = data;
    this.teamMemberList = teamMembersData.data;

    // let teamMembers = [];

    // teamMembersData.data.forEach((control) => {
    //  let newData = {
    //    name: control.userId.fullName,
    //    phoneNumber: control.userId.phoneNumber,
    //    email: control.userId.email,
    //  };
    //     teamMembers.push(newData);
    // });

    // this.tournamentService.setSelectedTeam({
    //   logo: data.logo,
    //   teamName: data.teamName,
    //   teamMembers: teamMembers,
    // });

    // this.router.navigate(['tournament', this.tournament?.slug, 'join']);
  }
}
