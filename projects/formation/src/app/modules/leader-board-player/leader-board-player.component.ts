import { Component, OnDestroy, OnInit, ViewChild } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";

import { ToastService } from "../../shared/toast/toast.service";
import {
  AuthServices,
  LeaderboardService,
  UserService,
} from "../../core/service";
import { IUser } from "../../shared/models";

import Chart from "chart.js/dist/Chart.min.js";
import { TranslateService } from "@ngx-translate/core";
import { Subscription } from "rxjs";

@Component({
  selector: "app-leader-board-player",
  templateUrl: "./leader-board-player.component.html",
  styleUrls: ["./leader-board-player.component.scss"],
})
export class LeaderBoardPlayerComponent implements OnInit, OnDestroy {
  @ViewChild("lineChart", { static: true }) private chartRef;

  showLoader: boolean = true;

  goldIcon = "assets/icons/gold.svg";
  silverIcon = "assets/icons/silver.svg";
  bronzeIcon = "assets/icons/bronze.svg";

  chart: any;
  followStatus: any = "follow";
  user: IUser;
  userId;

  leaderboardData: any = {};
  streakGraphData: any = {};

  days: any = [];
  wins: any = [];

  monthMapping = {
    0: "LEADERBOARD.PLAYER_DETAILS.MONTHS.JANUARY",
    1: "LEADERBOARD.PLAYER_DETAILS.MONTHS.FEBRUARY",
    2: "LEADERBOARD.PLAYER_DETAILS.MONTHS.MARCH",
    3: "LEADERBOARD.PLAYER_DETAILS.MONTHS.APRIL",
    4: "LEADERBOARD.PLAYER_DETAILS.MONTHS.MAY",
    5: "LEADERBOARD.PLAYER_DETAILS.MONTHS.JUNE",
    6: "LEADERBOARD.PLAYER_DETAILS.MONTHS.JULY",
    7: "LEADERBOARD.PLAYER_DETAILS.MONTHS.AUGUST",
    8: "LEADERBOARD.PLAYER_DETAILS.MONTHS.SEPTEMBER",
    9: "LEADERBOARD.PLAYER_DETAILS.MONTHS.OCTOBER",
    10: "LEADERBOARD.PLAYER_DETAILS.MONTHS.NOVEMBER",
    11: "LEADERBOARD.PLAYER_DETAILS.MONTHS.DECEMBER",
  };

  userSubscription: Subscription;

  constructor(
    private activatedRoute: ActivatedRoute,
    private authService: AuthServices,
    private leaderboardService: LeaderboardService,
    private router: Router,
    private translateService: TranslateService,
    private userService: UserService,
    public toastService: ToastService
  ) {}

  ngOnInit(): void {
    this.userId = this.activatedRoute.snapshot.params.userId; // get id from route
    this.getCurrentUserDetails();
    this.filterByUser();
  }

  ngOnDestroy() {
    this.userSubscription?.unsubscribe();
  }

  ordinal_suffix_of(i: any) {
    const j = i % 10,
      k = i % 100;
    if (j == 1 && k != 11) {
      return i + "st";
    }
    if (j == 2 && k != 12) {
      return i + "nd";
    }
    if (j == 3 && k != 13) {
      return i + "rd";
    }
    return i + "th";
  }

  // Get user details if user is logged in
  getCurrentUserDetails() {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.user = data;
        this.checkFollowStatus();
      }
    });
  }

  // check if the logged in user is following the users of stats
  checkFollowStatus() {
    if (this.user) {
      if (this.user._id == this.userId) {
        this.followStatus = "hide";
      } else {
        this.leaderboardService.checkFollowStatus(this.userId).subscribe(
          (res: any) => {
            if (res.data.isFollowing) {
              this.followStatus = "unfollow";
            } else {
              this.followStatus = "follow";
            }
          },
          (err) => {
            this.showLoader = false;
          }
        );
      }
    } else {
      this.followStatus = "follow";
    }
  }

  // Follow and Unfollow a user
  followUser() {
    if (this.user) {
      if (this.followStatus == "follow") {
        this.leaderboardService.followUser(this.userId).subscribe(
          (res: any) => {
            this.followStatus = "unfollow";
          },
          (err) => {
            this.showLoader = false;
          }
        );
      } else if (this.followStatus == "unfollow") {
        this.leaderboardService.unfollowUser(this.userId).subscribe(
          (res: any) => {
            this.followStatus = "follow";
          },
          (err) => {
            this.showLoader = false;
          }
        );
      }
    } else {
      this.authService.redirectUrl = `/leaderboard/${this.userId}`;
      this.toastService.showError(
        this.translateService.instant("TOURNAMENT.TOURNAMENT.PLEASE_LOGIN")
      );
      // Navigate to the login page with extras
      this.router.navigate(["/user/phone-login"]);
    }
  }

  filterByUser(month = new Date().getMonth(), year = new Date().getFullYear()) {
    this.showLoader = true;
    const params = {
      userId: this.userId,
      month,
      year,
    };
    this.leaderboardService.getUserLeaderboard(params).subscribe(
      (res: any) => {
        this.showLoader = false;
        this.leaderboardData = { ...res.data };
        const today: any = new Date();
        if (res.data) {
          this.leaderboardData.img =
            res.data.user[0].profilePicture == ""
              ? "./assets/images/Profile/formation.svg"
              : res.data.user[0].profilePicture;
          this.leaderboardData.name = res.data.user[0].fullName;
          this.leaderboardData.region = res.data.user[0].country
            ? res.data.user[0].country
            : "";
          this.leaderboardData.ordinalRank =
            res.data.rank != null
              ? this.ordinal_suffix_of(res.data.rank)
              : "No Rank";
          this.leaderboardData.selectedMonth = this.monthMapping[month - 1];
          this.leaderboardData.selectedYear = year.toString();
          this.leaderboardData.previousData = [];
          for (let i = 0; i < 6; i++) {
            const myDate = new Date(
              new Date().getFullYear(),
              new Date().getMonth() - i
            );
            this.leaderboardData.previousData.push({
              textMonth: this.monthMapping[myDate.getMonth()],
              textYear: myDate.getFullYear().toString(),
              year: myDate.getFullYear(),
              month: myDate.getMonth() + 1,
            });
          }

          //Graph Data
          const lastDay = new Date(year, month, 0).getDate();
          this.wins = [];
          this.days = [];
          let isCurrentMonth = false;

          if (today.getMonth() + 1 == month && today.getFullYear() == year) {
            isCurrentMonth = true;
          }
          for (let i = 0; i < lastDay; i++) {
            this.days.push(i + 1);
            if (!isCurrentMonth) {
              this.wins.push(0);
            } else {
              if (today.getDate() > i) {
                this.wins.push(0);
              }
            }
          }
          for (const gameWins of res.data.gameStreakData) {
            this.wins[gameWins._id.day - 1] = gameWins.winCount;
          }
        }
        // if (this.chart) this.chart.destroy();
        // Plotting points on chart
        if (this.chart) this.chart.destroy();
        this.initCharts(this.leaderboardData, this.translateService);
      },
      (err) => {
        this.showLoader = false;
      }
    );
  }

  initCharts(leaderData, translateService): void {
    this.chart = new Chart(this.chartRef.nativeElement, {
      type: "line",
      data: {
        labels: this.days,
        datasets: [
          {
            label: "Games Streak:",
            data: this.wins,
            borderColor: "#FFCE07",
            fill: false,
          },
        ],
      },
      options: {
        tooltips: {
          enabled: true,
          mode: "single",
          callbacks: {
            title: function (tooltipItems, data) {
              //Return value for title
              return (
                tooltipItems[0].xLabel +
                " " +
                translateService.instant(leaderData.selectedMonth) +
                " " +
                leaderData.selectedYear
              );
            },
            label: function (tooltipItems, data) {
              return "Game Streak " + tooltipItems.yLabel.toString();
            },
          },
        },
        maintainAspectRatio: false,
        responsive: true,
        legend: { display: false },
        scales: {
          xAxes: [{ display: true }],
          yAxes: [{ display: false }],
        },
      },
    });
  }
}
