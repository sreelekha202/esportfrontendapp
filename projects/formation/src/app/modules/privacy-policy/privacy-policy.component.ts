import { Component, OnInit } from "@angular/core";
import { Location } from "@angular/common";

import { TranslateService } from "@ngx-translate/core";
import { LanguageService } from "../../core/service";

@Component({
  selector: "app-privacy-policy",
  templateUrl: "./privacy-policy.component.html",
  styleUrls: ["./privacy-policy.component.scss"],
})
export class PrivacyPolicyComponent implements OnInit {
  arebicText: boolean = false;

  public currentLang: string;

  currLanguage = "english";

  constructor(
    private location: Location,
    public language: LanguageService,
    public translate: TranslateService
  ) {}

  ngOnInit(): void {
    this.language.language.subscribe((lang) => {
      if (lang) {
        this.arebicText = lang === "ms" ? true : false;
      }
    });
  }

  goBack() {
    this.location.back();
  }
}
