import { NgModule } from "@angular/core";

import { CoreModule } from "../../core/core.module";
import { LogRegRoutingModule } from "./log-reg-routing.module";
import { SharedModule } from "../../shared/modules/shared.module";

import { CongratulationComponent } from "./congratulation-form/congratulation.component";

import { PasswordFormComponent } from "./password-form/password-form.component";
import { PhoneNoFormComponent } from "./phone-no/phone-no.component";

import { LogRegComponent } from "./log-reg.component";

import { SocialMediaLoginComponent } from "../../core/social-media-login/social-media-login.component";
import { SubmitFormComponent } from "./submit-form/submit-form.component";
import { VerificationFormComponent } from "./verification-form/verification-form.component";

@NgModule({
  declarations: [
    CongratulationComponent,
    LogRegComponent,
    PasswordFormComponent,
    PhoneNoFormComponent,
    SocialMediaLoginComponent,
    SubmitFormComponent,
    VerificationFormComponent,
  ],
  imports: [CoreModule, LogRegRoutingModule, SharedModule],
  exports: [PhoneNoFormComponent, VerificationFormComponent],
})
export class LogRegModule {}
