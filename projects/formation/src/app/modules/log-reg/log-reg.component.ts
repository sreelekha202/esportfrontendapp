import {
  MatSnackBar,
  MatSnackBarHorizontalPosition,
  MatSnackBarVerticalPosition,
} from "@angular/material/snack-bar";
import { Component, OnDestroy, OnInit } from "@angular/core";
import { AngularFireMessaging } from "@angular/fire/messaging";
import { STEPPER_GLOBAL_OPTIONS } from "@angular/cdk/stepper";
import { ActivatedRoute, Router } from "@angular/router";

import {
  SubmitFormComponentDataItem,
  SubmitFormComponentOutputData,
} from "./submit-form/submit-form.component";
import {
  PhoneNoFormComponentDataItem,
  PhoneNoFormComponentOutputData,
} from "./phone-no/phone-no.component";
import { AppHtmlRoutes, AppHtmlRoutesLoginType } from "../../app-routing.model";
import {
  AuthServices,
  UserNotificationsService,
  UserService,
} from "../../core/service";
import { VerificationFormComponentData } from "./verification-form/verification-form.component";
import { CongratulationComponentData } from "./congratulation-form/congratulation.component";
import { UpdateFormComponentDataItem } from "./password-form/password-form.component";
import { environment } from "../../../environments/environment";
import { ToastService } from "../../shared/toast/toast.service";
import { TranslateService } from "@ngx-translate/core";
import { HostListener } from "@angular/core";
import { Subscription } from "rxjs";

enum ForgotStep {
  form,
  steps,
}

@Component({
  selector: "app-log-reg",
  templateUrl: "./log-reg.component.html",
  styleUrls: ["./log-reg.component.scss"],
  providers: [
    {
      provide: STEPPER_GLOBAL_OPTIONS,
      useValue: { displayDefaultIndicatorType: false },
    },
  ],
})
export class LogRegComponent implements OnInit, OnDestroy {
  AppHtmlRoutes = AppHtmlRoutes;
  AppHtmlRoutesLoginType = AppHtmlRoutesLoginType;
  ForgotStep = ForgotStep;
  forgotStep = ForgotStep.form;
  pageType: AppHtmlRoutesLoginType;

  registrationData: SubmitFormComponentOutputData;
  phoneNoData: PhoneNoFormComponentOutputData;
  registrationActiveStep = 0;
  emailLoginActiveStep = 0;
  forgotActiveStep = 0;
  id = "";
  token = "";
  email = "";
  horizontalPosition: MatSnackBarHorizontalPosition = "center";
  verticalPosition: MatSnackBarVerticalPosition = "top";
  loading = false;
  isReferel = false;
  referredBy: string = "";
  registrationStepperList = [
    "LOG_REG.STEPPER.STEP4",
    "LOG_REG.STEPPER.STEP5",
    "LOG_REG.STEPPER.STEP6",
    "LOG_REG.STEPPER.STEP7",
  ];

  emailLoginStepperList = [
    "LOG_REG.STEPPER.STEP1",
    "LOG_REG.STEPPER.STEP2",
    "LOG_REG.STEPPER.STEP3",
  ];

  forgotPassStepperList = [
    "LOG_REG.STEPPER.STEP8",
    "LOG_REG.STEPPER.STEP9",
    "LOG_REG.STEPPER.STEP10",
    "LOG_REG.STEPPER.STEP11",
  ];

  returnUrl: string;

  registrationForm: SubmitFormComponentDataItem = {
    title: "LOG_REG.REGISTER.TITLE",
    subtitle: "LOG_REG.REGISTER.SUBTITLE",
    checkboxLabel: "LOG_REG.REGISTER.checkboxLabel",
    checkboxLabelpp: "LOG_REG.REGISTER.checkboxLabelpp",
    submitBtn: "BUTTON.SIGNUP",
    or: "LOG_REG.REGISTER.or",
    orLabel: "LOG_REG.REGISTER.orLabel",
    footerText: "LOG_REG.REGISTER.footerText",
    footerLink: "LOG_REG.REGISTER.footerLink",
    footerLinkUrl: [
      AppHtmlRoutes.userPageType,
      AppHtmlRoutesLoginType.emailLogin,
    ],
    firstInputType: "text",
    firstInputName: "fullName",
    firstInputPlaceholder: "LOG_REG.REGISTER.firstInputPlaceholder",
    firstInputIcon: "person",
    fifthInputName: "username",
    fifthInputType: "text",
    fifthInputPlaceholder: "LOG_REG.REGISTER.fifthInputPlaceholder",
    fifthInputIcon: "person",
    secondInputName: "email",
    secondInputType: "email",
    secondInputPlaceholder: "LOG_REG.REGISTER.thirdInputPlaceholder",
    secondInputIcon: "email",
    fourthInputName: "dob",
    fourthInputType: "dob",
    fourthInputPlaceholder: "LOG_REG.REGISTER.fourthInputPlaceholder",
    fourthInputIcon: "calendar_today",
  };

  onlyPhoneNo: PhoneNoFormComponentDataItem = {
    title: "LOG_REG.LOGIN.TITLE1",
    subtitle: "LOG_REG.LOGIN.SUBTITLE1",
    submitBtn: "BUTTON.NEXT",
    cancelBtn: "BUTTON.CANCEL",
    InputName: "phone",
    InputType: "phone",
    InputPlaceholder: "LOG_REG.LOGIN.firstInputPlaceholder",
    InputIcon: "phone",
    error: "",
  };

  forgotPhoneNo: PhoneNoFormComponentDataItem = {
    title: "LOG_REG.LOGIN.TITLE2",
    subtitle: "LOG_REG.LOGIN.SUBTITLE1",
    submitBtn: "BUTTON.NEXT",
    cancelBtn: "BUTTON.CANCEL",
    InputName: "email",
    InputType: "email",
    InputPlaceholder: "LOG_REG.LOGIN.firstInputPlaceholder1",
    InputIcon: "email",
    error: "",
  };

  setPassword: UpdateFormComponentDataItem = {
    title: "LOG_REG.REGISTER.TITLE1",
    subtitle: "LOG_REG.REGISTER.SUBTITLE1",
    firstInputType: "password",
    firstInputPlaceholder: "LOG_REG.REGISTER.firstInputPlaceholder1",
    firstInputError: "LOG_REG.REGISTER.firstInputError",
    secondInputType: "password",
    secondInputPlaceholder: "LOG_REG.REGISTER.secondInputPlaceholder1",
    secondInputError: "LOG_REG.REGISTER.secondInputError",
    submitBtn: "BUTTON.NEXT",
    error: "",
  };

  forgotPassword: UpdateFormComponentDataItem = {
    title: "LOG_REG.FORGOT.TITLE",
    subtitle: "",
    firstInputType: "password",
    firstInputPlaceholder: "LOG_REG.FORGOT.firstInputPlaceholder",
    firstInputError: "LOG_REG.FORGOT.firstInputError",
    secondInputType: "password",
    secondInputPlaceholder: "LOG_REG.FORGOT.secondInputPlaceholder",
    secondInputError: "LOG_REG.FORGOT.secondInputError",
    submitBtn: "BUTTON.NEXT",
    error: "",
  };

  emailVerification: VerificationFormComponentData = {
    title: "LOG_REG.FORGOT.TITLE1",
    subtitle: "LOG_REG.FORGOT.SUBTITLE1",
    footerTitle: "LOG_REG.FORGOT.footerTitle",
    footerLinkTitle: "LOG_REG.FORGOT.footerLinkTitle",
    timerMs: 240,
    submitBtn: "BUTTON.NEXT",
  };
  registrationComplete: CongratulationComponentData = {
    title: "LOG_REG.REGISTER.TITLE2",
    subtitle: "LOG_REG.REGISTER.SUBTITLE2",
  };
  forgotComplete: CongratulationComponentData = {
    title: "LOG_REG.FORGOT.TITLE2",
    subtitle: "LOG_REG.FORGOT.SUBTITLE2",
  };

  phoneLogin: SubmitFormComponentDataItem = {
    title: "LOG_REG.LOGIN.TITLE",
    subtitle: "LOG_REG.LOGIN.SUBTITLE",
    checkboxLabel: "LOG_REG.LOGIN.checkboxLabel",
    checkboxLabelpp: "LOG_REG.REGISTER.checkboxLabelpp",
    submitBtn: "LOG_REG.LOGIN.LOGIN_BTN",
    or: "LOG_REG.LOGIN.or",
    orLabel: "LOG_REG.LOGIN.orLabel",
    forgotPass: "LOG_REG.LOGIN.forgotPass",
    footerText: "LOG_REG.LOGIN.footerText",
    footerLink: "LOG_REG.LOGIN.footerLink",
    footerLinkUrl: [
      AppHtmlRoutes.userPageType,
      AppHtmlRoutesLoginType.registration,
    ],
    firstInputName: "phone",
    firstInputType: "phone",
    firstInputPlaceholder: "LOG_REG.LOGIN.firstInputPlaceholder",
    firstInputIcon: "email",
    fifthInputName: "",
    fifthInputType: "",
    fifthInputPlaceholder: "",
    fifthInputIcon: "",
    secondInputName: "password",
    secondInputType: "password",
    secondInputPlaceholder: "LOG_REG.LOGIN.secondInputPlaceholder",
    secondInputIcon: "lock",
    fourthInputName: "dob",
    fourthInputType: "",
    fourthInputPlaceholder: "LOG_REG.REGISTER.fourthInputPlaceholder",
    fourthInputIcon: "dob",
    error: "",
  };
  emailLogin: SubmitFormComponentDataItem = {
    title: "LOG_REG.LOGIN.TITLE",
    subtitle: "",
    checkboxLabel: "LOG_REG.LOGIN.checkboxLabel",
    checkboxLabelpp: "LOG_REG.REGISTER.checkboxLabelpp",
    submitBtn: "LOG_REG.LOGIN.LOGIN_BTN",
    or: "LOG_REG.LOGIN.or",
    orLabel: "LOG_REG.LOGIN.orLabel",
    forgotPass: "LOG_REG.LOGIN.forgotPass",
    footerText: "LOG_REG.LOGIN.footerText",
    footerLink: "LOG_REG.LOGIN.footerLink",
    footerLinkUrl: [
      AppHtmlRoutes.userPageType,
      AppHtmlRoutesLoginType.registration,
    ],
    firstInputName: "email",
    firstInputType: "email",
    firstInputPlaceholder: "LOG_REG.LOGIN.firstInputPlaceholder1",
    firstInputIcon: "email",
    fifthInputName: "",
    fifthInputType: "",
    fifthInputPlaceholder: "",
    fifthInputIcon: "",
    secondInputName: "password",
    secondInputType: "password",
    secondInputPlaceholder: "LOG_REG.LOGIN.secondInputPlaceholder",
    secondInputIcon: "lock",
    fourthInputName: "dob",
    fourthInputType: "",
    fourthInputPlaceholder: "LOG_REG.REGISTER.fourthInputPlaceholder",
    fourthInputIcon: "calendar_today",
    error: "",
  };

  loginPhoneVerification: VerificationFormComponentData = {
    title: "LOG_REG.LOGIN.TITLE1",
    subtitle: "LOG_REG.REGISTER.SUBTITLE1",
    footerTitle: "LOG_REG.FORGOT.footerTitle",
    footerLinkTitle: "LOG_REG.FORGOT.footerLinkTitle",
    timerMs: 240,
    submitBtn: "BUTTON.NEXT",
    error: "",
  };

  registrationUpdate: UpdateFormComponentDataItem = {
    title: "RESET PASSWORD",
    firstInputType: "text",
    firstInputPlaceholder: "New Password",
    firstInputError: "Password is to short (minimum 8 characters)",
    secondInputType: "text",
    secondInputPlaceholder: "Confirm New Password",
    secondInputError: "Password doesn't match",
    submitBtn: "CHANGE PASSWORD",
  };

  userSubscription: Subscription;

  constructor(
    private activatedRoute: ActivatedRoute,
    private authService: AuthServices,
    private router: Router,
    private afMessaging: AngularFireMessaging,
    private userNotificationsService: UserNotificationsService,
    private snackBar: MatSnackBar,
    private userService: UserService,
    public toastService: ToastService,
    private translateService: TranslateService
  ) {}

  ngOnInit(): void {
    if (localStorage.getItem(environment.currentToken)) {
      this.router.navigate(["/home"]);
    }
    this.activatedRoute.params.subscribe((params) => {
      if (params && params.pageType) {
        if (params.pageType === AppHtmlRoutesLoginType.phoneLogin) {
          this.pageType = AppHtmlRoutesLoginType.emailLogin;
        } else if (
          AppHtmlRoutesLoginType[params.pageType] == undefined &&
          params.pageType.substring(
            params.pageType.indexOf(":") + 1,
            params.pageType.length
          ).length == 8 &&
          params.pageType.substring(0, params.pageType.indexOf(":")) ==
            "referral"
        ) {
          this.pageType = params.pageType;
          this.referredBy = params.pageType.substring(
            params.pageType.indexOf(":") + 1,
            params.pageType.length
          );
          //i am testing
          this.isReferel = true;
        } else {
          this.pageType = params.pageType;
        }
      }
    });
    this.returnUrl = this.authService.redirectUrl || "/";
  }

  ngOnDestroy() {
    this.userSubscription?.unsubscribe();
  }

  onRegistrationNext(data): void {
    this.loading = true;
    // if (data.secondInputType === 'phone') {
    //   data.mailPhone = data.phone.e164Number;
    //   this.registrationStepperList = [
    //     'LOG_REG.STEPPER.STEP12',
    //     'LOG_REG.STEPPER.STEP5',
    //     'LOG_REG.STEPPER.STEP3',
    //     'LOG_REG.STEPPER.STEP7',
    //   ];
    // }
    // if (data.secondInputType === 'email') {
    //   data.mailPhone = data.email;
    // }

    data.mailPhone = data.email;

    this.registrationData = data;
    this.registrationActiveStep = 1;
    this.loading = false;
  }
  onRegistrationStepTwo(data): void {
    this.loading = true;
    if (data) {
      if (data.formValue) {
        const register = {
          fullName: data.formValue.fullName,
          username: data.formValue.username,
          password: data.password,
          phoneNumber: data.formValue.email,
          email: data.formValue.email,
          dob: data.formValue.dob,
          referredBy: this.referredBy.length == 8 ? this.referredBy : undefined,
        };
        this.authService.register(register).subscribe(
          (AuthData) => {
            this.registrationForm.error = "";
            this.setPassword.error = "";
            this.token = AuthData.data.token;
            this.email = register.email;
            this.registrationActiveStep = 2;
            this.emailVerification.timerMs = 240;
            this.emailVerification.title = "LOG_REG.LOGIN.TITLE1";
            this.registrationData.mailPhone = register.phoneNumber;
            this.registrationData.type = "email";
            this.loading = false;
          },
          (error) => {
            this.registrationActiveStep = 0;
            this.registrationForm.error =
              error?.error?.messageCode === "R_E003"
                ? this.translateService.instant(
                    "register.EMAIL_ID_Already_Exist"
                  )
                : error?.error?.message;
            this.loading = false;
          }
        );
      }
    }
  }

  registrationOtp(data): void {
    this.loading = true;
    if (data.otp) {
      this.authService.confirmUser(this.token, data.otp, "phone").subscribe(
        (authData) => {
          this.emailVerification.error = "";
          this.registrationActiveStep = 3;
          this.emailVerification.timerMs = 240;
          this.emailVerification.title = "LOG_REG.LOGIN.TITLE2";
          this.registrationData.mailPhone = this.email;
          this.registrationData.type = "email";
          this.loading = false;
        },
        (error) => {
          this.emailVerification.error =
            error?.error?.message || error?.message;
          this.loading = false;
        }
      );
      // if (data.formValue.secondInputType === 'email') {
      //   this.emailVerification.error = '';
      //   this.authService.confirmUser(this.token, data.otp, 'email').subscribe(
      //     (data) => {
      //       this.emailVerification.error = '';
      //       this.registrationActiveStep = 3;
      //       this.loading = false;
      //     },
      //     (error) => {
      //       this.emailVerification.error =
      //         error?.error?.message || error?.message;
      //       this.loading = false;
      //     }
      //   );
      // }
    }
  }

  registrationEmailOtp(data): void {
    this.loading = true;
    if (data.otp) {
      this.emailVerification.error = "";
      this.authService.confirmUser(this.token, data.otp, "email").subscribe(
        async (authData) => {
          this.emailVerification.error = "";
          this.phoneLogin.error = "";
          // this.registrationActiveStep = 3;
          // this.loading = false;

          if (authData.code === "ER1001") {
            this.loading = false;
          } else {
            this.afMessaging.requestToken.subscribe(
              (token) => {
                this.userNotificationsService
                  .registerPushToken(token)
                  .subscribe((responseData) => {
                    if (responseData.success) {
                      this.afMessaging.messages.subscribe((payload) => {
                        this.snackBar.open(payload["data"]["body"], "Ok", {
                          duration: 5000,
                          horizontalPosition: this.horizontalPosition,
                          verticalPosition: this.verticalPosition,
                        });
                      });
                    }
                  });
              },
              (error) => {}
            );
            this.authService.setCookie(authData.data.token, "accessToken");
            // store user details and jwt token in local storage to keep user logged in between page refreshes
            localStorage.setItem(environment.currentToken, authData.data.token);
            localStorage.setItem("refreshToken", authData.data.refreshToken);
            this.userService.startRefreshTokenTimer();
            this.toastService.showSuccess(
              this.translateService.instant("LOG_REG.REGISTER.REG_SUCC")
            );
            await this.userService.getAuthenticatedUser();
            this.userSubscription = this.userService.currentUser.subscribe(
              (sdata) => {
                if (sdata) {
                  if (authData.data.type === "admin") {
                    this.router.navigate(["/profile"]);
                    this.loading = false;
                  } else {
                    if (authData.data.firstLogin == 1) {
                      this.router.navigate(["/profile"]);
                      this.loading = false;
                    } else {
                      this.router.navigateByUrl(this.returnUrl);
                      this.loading = false;
                    }
                  }
                }
              }
            );
          }
        },
        (error) => {
          this.emailVerification.error =
            error?.error?.message || error?.message;
          this.loading = false;
        }
      );
    }
  }

  onLoginNext(data): void {
    this.loading = true;
    if (data.firstInputType) {
      if (data.firstInputType === "phone") {
        const login = {
          password: data.password,
          phoneNumber: data.phone.e164Number,
          type: "phone",
        };
        this.phoneLogin.error = "";
        this.authService.login(login).subscribe(
          async (user) => {
            this.phoneLogin.error = "";

            if (user.code == "ER1001") {
              this.loading = false;
            } else {
              this.afMessaging.requestToken.subscribe(
                (token) => {
                  this.userNotificationsService
                    .registerPushToken(token)
                    .subscribe((responseData) => {
                      if (responseData.success) {
                        this.afMessaging.messages.subscribe((payload) => {
                          this.snackBar.open(payload["data"]["body"], "Ok", {
                            duration: 5000,
                            horizontalPosition: this.horizontalPosition,
                            verticalPosition: this.verticalPosition,
                          });
                        });
                      }
                    });
                },
                (error) => {}
              );
              this.authService.setCookie(user.data.token, "accessToken");
              // store user details and jwt token in local storage to keep user logged in between page refreshes
              localStorage.setItem(environment.currentToken, user.data.token);
              localStorage.setItem("refreshToken", user.data.refreshToken);
              this.userService.startRefreshTokenTimer();
              await this.userService.getAuthenticatedUser();
              this.userSubscription = this.userService.currentUser.subscribe(
                (sdata) => {
                  if (sdata) {
                    if (user.data.type === "admin") {
                      this.router.navigate(["/profile"]);
                      this.loading = false;
                    } else {
                      if (user.data.firstLogin == 1) {
                        this.router.navigate(["/profile"]);
                        this.loading = false;
                      } else {
                        this.router.navigateByUrl(this.returnUrl);
                        this.loading = false;
                      }
                    }
                  }
                }
              );
            }
          },
          (error) => {
            this.loading = false;
            this.phoneLogin.error = error?.error?.message;
          }
        );
      }
      if (data.firstInputType === "email") {
        const login = {
          password: data.password,
          email: data.data.firstInput,
          type: "email",
        };
        this.emailLogin.error = "";
        this.authService.login(login).subscribe(
          async (user) => {
            this.loading = false;
            this.emailLogin.error = "";
            if (user.code == "ER1001") {
              this.emailLoginActiveStep = 1;
              this.id = user.data.id;
              // this.stepZero = false;
              // this.stepOne = true;
            } else {
              this.authService.setCookie(user.data.token, "accessToken");
              // store user details and jwt token in local storage to keep user logged in between page refreshes
              localStorage.setItem(environment.currentToken, user.data.token);
              localStorage.setItem("refreshToken", user.data.refreshToken);
              localStorage.setItem("refreshToken", user.data.refreshToken);
              this.userService.startRefreshTokenTimer();
              await this.userService.getAuthenticatedUser();
              this.userSubscription = this.userService.currentUser.subscribe(
                (sdata) => {
                  if (sdata) {
                    if (user.data.type === "admin") {
                      this.router.navigate(["/profile"]);
                      this.loading = false;
                    } else {
                      if (user.data.firstLogin == 1) {
                        this.router.navigate(["/profile"]);
                        this.loading = false;
                      } else {
                        this.router.navigateByUrl(this.returnUrl);
                        this.loading = false;
                      }
                    }
                  }
                }
              );
            }
          },
          (error) => {
            this.loading = false;
            this.emailLogin.error = error?.error?.message;
          }
        );
      }
    }
  }

  onPhoneNoNext(data): void {
    this.loading = true;
    if (data.phone) {
      const queryData = {
        phoneNumber: data.phone.e164Number,
        id: this.id,
      };
      this.onlyPhoneNo.error = "";
      this.authService.update(queryData).subscribe(
        (data) => {
          this.onlyPhoneNo.error = "";
          this.emailLoginActiveStep = 2;
          this.loginPhoneVerification.timerMs = 240;
          this.phoneNoData = {
            mailPhone: queryData.phoneNumber,
          };
          this.loading = false;

          // this.interval = setInterval(() => {
          //   if (this.timeLeft > 0) {
          //     this.timeLeft--;
          //   }
          //   // else {
          //   //   this.timeLeft = 60;
          //   // }
          // }, 1000);
        },
        (error) => {
          this.onlyPhoneNo.error = error?.error?.message;
          this.loading = false;
        }
      );
    }
  }

  phonLoginOtp(data): void {
    this.loading = true;
    if (data.otp) {
      this.authService.confirm(this.id, data.otp, "phone").subscribe(
        async (data) => {
          this.loading = false;
          this.authService.setCookie(data.data.token, "accessToken");
          localStorage.setItem(environment.currentToken, data.data.token);
          localStorage.setItem("refreshToken", data.data.refreshToken);
          this.userService.startRefreshTokenTimer();
          await this.userService.getAuthenticatedUser();
          this.userSubscription = this.userService.currentUser.subscribe(
            (sdata) => {
              if (sdata) {
                if (data.data.type === "admin") {
                  this.router.navigate(["/profile"]);
                  this.loading = false;
                } else {
                  if (data.data.firstLogin == 1) {
                    this.router.navigate(["/profile"]);
                    this.loading = false;
                  } else {
                    this.router.navigateByUrl(this.returnUrl);
                    this.loading = false;
                  }
                }
              }
            }
          );
        },
        (error) => {
          this.loginPhoneVerification.error = error?.error?.message;
          this.loading = false;
        }
      );
    }
  }

  onForgotNext(data): void {
    this.loading = true;

    if (data) {
      if (data.phone) {
        if (this.forgotPhoneNo.InputType === "phone" && data.phone.e164Number) {
          let queryData = {
            type: "phone",
            phoneNumber: data.phone.e164Number,
          };

          this.forgotPhoneNo.error = "";
          this.authService.forgotPassword(queryData).subscribe(
            (user) => {
              this.forgotPhoneNo.error = "";
              this.loginPhoneVerification.timerMs = 240;

              this.loginPhoneVerification.title = "LOG_REG.LOGIN.forgotPass";
              this.loginPhoneVerification.subtitle = "LOG_REG.FORGOT.SUBTITLE3";
              this.phoneNoData = {
                mailPhone: queryData.phoneNumber,
                formValue: data,
              };

              this.forgotActiveStep = 1;
              this.loading = false;
            },
            (error) => {
              this.forgotPhoneNo.error = error?.error?.message;

              this.loading = false;
            }
          );
        }
      }

      if (this.forgotPhoneNo.InputType === "email" && data.email) {
        let queryData = {
          type: "email",
          email: data.email,
        };

        this.forgotPhoneNo.error = "";
        this.authService.forgotPassword(queryData).subscribe(
          (user) => {
            this.forgotPhoneNo.error = "";
            this.loginPhoneVerification.timerMs = 240;

            if (this.forgotPhoneNo.InputType === "email") {
              this.loginPhoneVerification.title = "LOG_REG.LOGIN.forgotPass";
              this.loginPhoneVerification.subtitle = "LOG_REG.FORGOT.SUBTITLE4";
              this.phoneNoData = {
                mailPhone: queryData.email,
                formValue: data,
              };
            }
            this.forgotPassStepperList = [
              "LOG_REG.STEPPER.STEP8",
              "LOG_REG.STEPPER.STEP6",
              "LOG_REG.STEPPER.STEP10",
              "LOG_REG.STEPPER.STEP11",
            ];
            this.forgotActiveStep = 1;
            this.loading = false;
          },
          (error) => {
            this.forgotPhoneNo.error = error?.error?.message;
            this.loading = false;
          }
        );
      }
    }
  }
  forgotOtp(data): void {
    this.loading = true;
    if (data) {
      let queryData;
      if (data.formValue.formValue.type === "phone") {
        queryData = {
          phoneNumber: data.formValue.formValue.phone.e164Number,
          type: "phone_otp",
          phoneOTP: data.otp,
        };
      }
      if (data.formValue.formValue.type === "email") {
        queryData = {
          email: data.formValue.formValue.email,
          type: "email_otp",
          emailOTP: data.otp,
        };
      }
      this.loginPhoneVerification.error = "";
      this.authService.forgotPassword(queryData).subscribe(
        (resData) => {
          this.phoneNoData.otp = data.otp;
          this.forgotActiveStep = 2;
          this.loading = false;
        },
        (error) => {
          this.loginPhoneVerification.error = error?.error?.message;
          this.loading = false;
        }
      );
    }
  }
  forgotSetPassword(data): void {
    this.loading = true;
    if (data) {
      let queryData;
      if (data.formValue) {
        if (data.formValue.formValue.type === "phone") {
          queryData = {
            type: "update",
            phoneOTP: data.formValue.otp,
            phoneNumber: data.formValue.formValue.phone.e164Number,
            password: data.password,
          };
        }
        if (data.formValue.formValue.type === "email") {
          queryData = {
            type: "update",
            emailOTP: data.formValue.otp,
            email: data.formValue.formValue.email,
            password: data.password,
          };
        }

        this.forgotPassword.error = "";
        this.authService.forgotPassword(queryData).subscribe(
          (user) => {
            this.forgotActiveStep = 3;
            this.loading = false;
          },
          (error) => {
            this.forgotPassword.error = error?.error?.message;
            this.loading = false;
          }
        );
      }
    }
  }

  @HostListener("window:popstate", ["$event"])
  onPopState(event) {
    //  history.pushState({}, "", "registration");
    //   this.router
    //     .navigateByUrl("/home", { skipLocationChange: true })
    //     .then(() => {
    //       this.router.navigate(["/user/registration"]);
    //     });

    this.registrationActiveStep = 0;
    this.emailLoginActiveStep = 0;
    this.forgotActiveStep = 0;
    this.id = "";
    this.token = "";
    this.email = "";
  }
}
