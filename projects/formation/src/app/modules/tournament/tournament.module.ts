import { NgModule } from "@angular/core";

import { CoreModule } from "../../core/core.module";
import { SharedModule } from "../../shared/modules/shared.module";
import { TournamentRoutingModule } from "./tournament-routing.module";

import { TournamentComponent } from "./tournament.component";
import { TournamentInfoComponent } from "./tournament-info/tournament-info.component";

import { MatProgressSpinnerModule } from "@angular/material/progress-spinner";

@NgModule({
  declarations: [TournamentComponent, TournamentInfoComponent],
  imports: [
    CoreModule,
    MatProgressSpinnerModule,
    SharedModule,
    TournamentRoutingModule,
  ],
  providers: [],
})
export class TournamentModule {}
