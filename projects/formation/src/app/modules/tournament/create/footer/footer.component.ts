import {
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
} from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";
import { TranslateService } from "@ngx-translate/core";
import { ToastService } from "../../../../shared/toast/toast.service";
import {
  TournamentService,
  ConstantsService,
  UtilsService,
} from "../../../../core/service";
import { Router } from "@angular/router";
import {
  InfoPopupComponent,
  InfoPopupComponentData,
  InfoPopupComponentType,
} from "../../../../shared/popups/info-popup/info-popup.component";
import { MatDialog } from "@angular/material/dialog";
import { Subject } from "rxjs";

@Component({
  selector: "tournament-footer",
  templateUrl: "./footer.component.html",
  styleUrls: ["./footer.component.scss"],
})
export class FooterComponent implements OnInit, OnDestroy {
  @Input() form: FormGroup;
  @Input() nav;
  @Input() active;
  @Input() user;
  @Input() fallBack;
  @Input() fallBackIndex;
  @Input() childNotifier: Subject<any>;

  @Output() emitMessage = new EventEmitter<string>();
  @Output() getActiveId = new EventEmitter<number>();

  showLoader = false;
  timObjClone;
  requiredFields = {
    step1: [
      { name: "TOURNAMENT.CREATE.NAME", value: "name" },
      { name: "TOURNAMENT.CREATE.START_DATE", value: "startDate" },
      { name: "TOURNAMENT.CREATE.START_TIME", value: "startTime" },
      { name: "TOURNAMENT.CREATE.END_DATE", value: "endDate" },
      { name: "TOURNAMENT.CREATE.END_TIME", value: "endTime" },
      { name: "CREATE_TOURNAMENT_VALIDATION.KEY11", value: "regStartDate" },
      { name: "CREATE_TOURNAMENT_VALIDATION.KEY12", value: "regEndDate" },
      { name: "CREATE_TOURNAMENT_VALIDATION.KEY13", value: "regStartTime" },
      { name: "CREATE_TOURNAMENT_VALIDATION.KEY14", value: "regEndTime" },
      { name: "TOURNAMENT.CREATE.GAME", value: "gameDetail" },
      { name: "TOURNAMENT.CREATE.T_TYPE", value: "participantType" },
      { name: "TOURNAMENT.CREATE.TEAM_TYPE", value: "teamFormat" },
      { name: "TOURNAMENT.CREATE.TEAM_SIZE", value: "teamSize" },
      { name: "TOURNAMENT.CREATE.SM_SIZE", value: "substituteMemberSize" },
      { name: "TOURNAMENT.CREATE.PLATFORM", value: "platform" },
      { name: "TOURNAMENT.CREATE.BRACKET", value: "bracketType" },
      { name: "TOURNAMENT.CREATE.MAX_PARTICIPANT", value: "maxParticipants" },
      {
        name: "TOURNAMENT.CREATE.NUMBER_OF_TEAMS_PER_GROUP",
        value: "noOfTeamInGroup",
      },
      {
        name: "TOURNAMENT.CREATE.WINNING_TEAMS_PER_GROUP",
        value: "noOfWinningTeamInGroup",
      },
      {
        name: "TOURNAMENT.CREATE.NUMBER_OF_MATCH_PER_GROUP",
        value: "noOfRoundPerGroup",
      },
      { name: "TOURNAMENT.CREATE.NO_OF_PLACEMENT", value: "noOfPlacement" },
      { name: "TOURNAMENT.CREATE.INPUT_PP", value: "placementPoints" },
      { name: "TOURNAMENT.CREATE.POINT_PER_KILL", value: "pointsKills" },
      { name: "TOURNAMENT.CREATE.MATCH_FORMAT", value: "noOfSet" },
      { name: "TOURNAMENT.CREATE.CURRENCY", value: "regFeeCurrency" },
      { name: "TOURNAMENT.CREATE.ENTRY_FEE", value: "regFee" },
      { name: "TOURNAMENT.CREATE.CHECK_IN_ST", value: "checkInStartDate" },
      { name: "TOURNAMENT.CREATE.NSB_TYPE", value: "stageBracketType" },
      { name: "TOURNAMENT.CREATE.SELECT_STAGE", value: "stageMatch" },
      { name: "TOURNAMENT.CREATE.NS_FORMAT", value: "stageMatchNoOfSet" },
      {
        name: "TOURNAMENT.SET_DURATION",
        value: "setDuration",
      },
    ],
    step2: [
      { name: "TOURNAMENT.TOURNAMENT_TYPE", value: "tournamentType" },
      { name: "TOURNAMENT.ADD_MONEY_PRIZE", value: "prizeList" },
      { name: "TOURNAMENT.SPONSORS", value: "sponsors" },
      { name: "TOURNAMENT.ADD_CURRENCY", value: "prizeCurrency" },
      { name: "TOURNAMENT.VENUE_ADDRESS", value: "venueAddress" },
    ],
    step3: [
      { name: "TOURNAMENT.TOURNAMENT_URL", value: "url" },
      { name: "TOURNAMENT.TWITCH_VIDEO_LINK", value: "twitchVideoLink" },
      { name: "TOURNAMENT.YOUTUBE_VIDEO_LINK", value: "youtubeVideoLink" },
      { name: "TOURNAMENT.FACEBOOK_VIDEO_LINK", value: "facebookVideoLink" },
      { name: "TOURNAMENT.CONTACT_DETAILS", value: "contactDetails" },
    ],
    step4: [],
  };

  constructor(
    private translateService: TranslateService,
    private toastService: ToastService,
    public tournamentService: TournamentService,
    private utilsService: UtilsService,
    private route: Router,
    private matDialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.childNotifier.subscribe((stepper) => {
      if (stepper?.activeId < stepper?.nextId) {
        let step = stepper?.activeId;
        while (step < stepper?.nextId) {
          const valid = this.isStepValid(step);
          if (valid) {
            ++step;
          } else {
            stepper.preventDefault();
            break;
          }
        }
      }
    });
  }

  ngOnDestroy() {
    this.childNotifier?.unsubscribe();
  }

  isStepValid(step) {
    if(step == 1){
      this.repatchFormValues();
    }
    const invalidFieldList = [];
    const invalidFieldValueList = [];
    for (let obj of this.requiredFields[`step${step}`]) {
      if (this.form?.controls?.[obj["value"]]?.invalid) {
        invalidFieldList.push(obj.name);
        invalidFieldValueList.push(obj.value);
      }
    }

    if (invalidFieldList.length > 0) {
      for (let key in this.form.controls) {
        if (invalidFieldValueList.includes(key)) {
          this.markAsTouched(this.form.controls[key]);
        }
      }

      let message = `<h2>Step ${step}</h2><div>${this.translateService.instant(
        "TOURNAMENT.ERROR_TITLE"
      )}</div><div class='invalidFields'><ol>`;

      for (let obj of invalidFieldList)
        message += `<li> ${this.translateService.instant(obj)} </li>`;
      message += "</ol></div>";

      this.emitMessage.emit(message);
      this.getActiveId.emit(step);
      return false;
    } else {
      this.form.get("step").setValue(step + 1);
      return true;
    }
  }

  markAsTouched(control: any) {
    control.markAsTouched();
    if (control.controls) {
      if (Array.isArray(control.controls)) {
        for (let obj of control.controls) {
          for (let key in obj.controls) {
            obj.controls[key].markAsTouched();
          }
        }
      }
    }
  }

  submitTournament = async (status) => {
    try {
      this.repatchFormValues();
      if (!this.user?._id) {
        this.toastService.showError(
          this.translateService.instant("TOURNAMENT.PLEASE_LOGIN")
        );
        return;
      }
      if (this.form.value.startTime) {
        this.timObjClone = Object.assign(
          {},
          this.form.value.startTime
        );
      }
      if (!this.form.valid) {
        this.markFormGroupTouched(this.form);
        this.toastService.showError(
          this.translateService.instant("TOURNAMENT.INVALID_FORM")
        );
        return;
      }
      const formValues = JSON.parse(JSON.stringify(this.form?.value));

      formValues.createdBy = this.user?._id;
      formValues.updatedBy = this.user?._id;
      formValues.organizerDetail = this.user?._id;
      formValues.tournamentStatus = status;

      if (formValues.isCheckInRequired) {
        formValues.checkInStartDate = this.getCheckInDt(
          formValues.startDate,
          formValues.startTime,
          formValues.checkInStartDate
        );
      }
      let { hour, minute } = formValues.startTime;
      formValues.startTime = this.utilsService.convertTimeIntoString(
        (hour < 10 ? "0" + hour : hour) +
          ":" +
          (minute < 10 ? "0" + minute : minute) +
          ""
      );
      hour = formValues.regStartTime?.hour;
      minute = formValues.regStartTime?.minute;
      formValues.regStartTime = this.utilsService.convertTimeIntoString(
        (hour < 10 ? "0" + hour : hour) +
          ":" +
          (minute < 10 ? "0" + minute : minute) +
          ""
      );
      hour = formValues.regEndTime?.hour;
      minute = formValues.regEndTime?.minute;
      formValues.regEndTime = this.utilsService.convertTimeIntoString(
        (hour < 10 ? "0" + hour : hour) +
          ":" +
          (minute < 10 ? "0" + minute : minute) +
          ""
      );
      const currentDate = new Date();
      const regEndDateChanged = this.form.get("regEndDate");
      const regEndTimeChanged = this.form.get("regEndTime");
      if (
        (regEndTimeChanged.dirty || regEndDateChanged.dirty) &&
        this.setStartDate("regEndDate", "regEndTime") >
          JSON.stringify(currentDate)
      ) {
        formValues.isRegistrationClosed = false;
      }
      formValues.startDate = this.setStartDate("startDate", "startTime");
      formValues.startTime = this.utilsService.formatAMPM(formValues.startDate);
      formValues.endDate = this.setStartDate("endDate", "endTime");
      formValues.regStartDate = this.setStartDate(
        "regStartDate",
        "regStartTime"
      );
      formValues.regEndDate = this.setStartDate("regEndDate", "regEndTime");
      if (formValues.contactOn == "WhatsApp") {
        formValues.whatsApp = formValues?.contactDetails?.e164Number;
      }

      if (typeof formValues.contactDetails == "object") {
        formValues.contactDetails = formValues?.contactDetails?.e164Number;
      }

      // delete formValues.startTime;
      const data: InfoPopupComponentData = {
        ...this.popUpTitleAndText(formValues),
        type: InfoPopupComponentType.confirm,
        btnText: this.translateService.instant("TOURNAMENT.CONFIRM"),
      };

      const confirmed = await this.matDialog
        .open(InfoPopupComponent, { data })
        .afterClosed()
        .toPromise();

      if (confirmed) {
        this.showLoader = true;
        delete formValues.teamFormat;
        delete formValues.endTime;
        const response = formValues?._id
          ? await this.tournamentService
              .updateTournament(formValues, formValues?._id)
              .toPromise()
          : await this.tournamentService.saveTournament(formValues).toPromise();

        this.toastService.showSuccess(response?.message);

        if (response?.data?.enablePayment) {
          this.active = 4;
          this.form.get("step").setValue(5);
          this.form.addControl("_id", new FormControl(response?.data?._id));
        } else if (this.fallBack) {
          this.route.navigate([this.fallBack], {
            queryParams: { activeTab: this.fallBackIndex },
          });
        } else {
          this.route.navigate(["/profile/my-tournament/created"]);
        }

        this.showLoader = false;
      }
    } catch (error) {
      this.showLoader = false;
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  markFormGroupTouched(formGroup: FormGroup) {
    (<any>Object).values(formGroup.controls).forEach((control) => {
      control.markAsTouched();

      if (control.controls) {
        this.markFormGroupTouched(control);
      }
    });
  }

  popUpTitleAndText = (data) => {
    if (data?._id) {
      return {
        title: this.translateService.instant("TOURNAMENT.UPDATE"),
        text: this.translateService.instant("TOURNAMENT.UPDATE_TXT"),
      };
    } else {
      return {
        title: this.translateService.instant("TOURNAMENT.SAVE_TOURNAMENT"),
        text: this.translateService.instant("TOURNAMENT.STATUS_2"),
      };
    }
  };

  setStartDate(f1, f2) {
    const date = this.form.value[f1];
    const time = this.form.value[f2];
    const dateTime = new Date(date);
    dateTime.setHours(time.hour);
    dateTime.setMinutes(time.minute);
    return dateTime.toISOString();
  }

  /* Start Date Minus CheckIn Hour */
  getCheckInDt(startDate, startTime, checkInStartDate) {
    const startDt = new Date(startDate);
    startDt.setHours(startTime.hour || 0, startTime.minute || 0);
    const duration = ConstantsService.checkInTimeOptions.find(
      (item) => item.name == checkInStartDate
    ).value;
    const checkInDt = new Date(startDt);
    checkInDt.setHours(
      checkInDt.getHours() - duration.hour,
      checkInDt.getMinutes() - duration.minute
    );
    return checkInDt.toISOString();
  }
  repatchFormValues(){
    const {
      startDate,
      startTime,
      endDate,
      endTime,
      regStartDate,
      regEndDate,
      regStartTime,
      regEndTime,
    } = this.form.value;
    this.form.patchValue({
      startDate,
      startTime,
      endDate,
      endTime,
      regStartDate,
      regEndDate,
      regStartTime,
      regEndTime,
    });
  }
}
