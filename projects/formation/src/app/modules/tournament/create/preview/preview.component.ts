import {
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
} from "@angular/core";
import { ConstantsService, UtilsService } from "../../../../core/service";
import { EsportsTimezone } from 'esports';

@Component({
  selector: "tournament-preview",
  templateUrl: "./preview.component.html",
  styleUrls: ["./preview.component.scss"],
})
export class PreviewComponent implements OnInit, OnChanges {
  @Input() tournament;
  @Input() userFullName;
  @Input() active;
  timezone;

  checkInDt: Date;
  startDt: Date;
  gameList = [];

  constructor(private utilsService: UtilsService, private esportsTimezone: EsportsTimezone) {}

  ngOnInit(): void {
    this.timezone = this.esportsTimezone.getTimeZoneName();
  }

  isNumber(val): boolean { const str: string = val.toString(); return str.startsWith("+"); }

  ngOnChanges(simpleChanges: SimpleChanges) {
    if (simpleChanges.hasOwnProperty("active") && this.active > 3) {
      this.gameList = this.utilsService.getGame();
      this.getCheckInAndStartDt(this.tournament);
    }
  }

  getCheckInAndStartDt(data) {
    this.startDt = new Date(data.startDate);
    this.startDt.setHours(data.startTime.hour || 0, data.startTime.minute || 0);

    if (this.tournament?.isCheckInRequired) {
      const duration = ConstantsService.checkInTimeOptions.find(
        (item) => item.name == data.checkInStartDate
      ).value;
      this.checkInDt = new Date(this.startDt);
      this.checkInDt.setHours(
        this.checkInDt.getHours() - duration.hour,
        this.checkInDt.getMinutes() - duration.minute
      );
    }
  }
}
