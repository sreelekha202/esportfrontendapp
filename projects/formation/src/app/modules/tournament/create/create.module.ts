import { NgModule } from "@angular/core";
import { NgbNavModule } from "@ng-bootstrap/ng-bootstrap";
import { CommonModule } from "@angular/common";
import { CreateComponent } from "./create.component";
// Module
import { SharedModule } from "../../../shared/modules/shared.module";
import { CoreModule } from "../../../core/core.module";
import { MatProgressSpinnerModule } from "@angular/material/progress-spinner";
// Routing
import { CreateRoutingModule } from "./create-routing.module";
import { FormComponentModule } from "../../../shared/components/form-component/form-component.module";

import { PreviewComponent } from "./preview/preview.component";
import { VenueAddressComponent } from "./venue-address/venue-address.component";

import { OverviewComponent } from "./preview/overview/overview.component";
import { BasicComponent } from "./basic/basic.component";
import { FooterComponent } from "./footer/footer.component";
import { ConditionsComponent } from "./conditions/conditions.component";
import { FinalComponent } from "./final/final.component";
import { PaymentComponent } from "./preview/payment/payment.component";
import { PaymentModule } from "../../payment/payment.module";

import {
  FontAwesomeModule,
  FaIconLibrary,
} from "@fortawesome/angular-fontawesome";
import {
  faTrash,
  faEdit,
  faCopy,
  faPlay,
  faStop,
  faExpand,
  faPlus,
} from "@fortawesome/free-solid-svg-icons";

@NgModule({
  declarations: [
    CreateComponent,
    PreviewComponent,
    VenueAddressComponent,
    OverviewComponent,
    BasicComponent,
    FooterComponent,
    ConditionsComponent,
    FinalComponent,
    PaymentComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    CoreModule,
    MatProgressSpinnerModule,
    CreateRoutingModule,
    FormComponentModule,
    FontAwesomeModule,
    PaymentModule,
    NgbNavModule,
  ],
})
export class CreateModule {
  constructor(private library: FaIconLibrary) {
    library.addIcons(faTrash, faEdit, faCopy, faPlay, faStop, faExpand, faPlus);
  }
}
