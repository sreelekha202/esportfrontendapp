import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { VenueAddressComponent } from "./venue-address.component";

describe("VenueAddressComponent", () => {
  let component: VenueAddressComponent;
  let fixture: ComponentFixture<VenueAddressComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [VenueAddressComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VenueAddressComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
