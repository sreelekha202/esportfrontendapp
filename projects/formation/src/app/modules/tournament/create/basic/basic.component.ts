import { DOCUMENT } from "@angular/common";
import { Observable } from "rxjs";
import {
  Component,
  Inject,
  Input,
  OnInit,
  OnChanges,
  SimpleChanges,
} from "@angular/core";
import {
  FormGroup,
  FormControl,
  ControlContainer,
  FormGroupDirective,
  Validators,
  AbstractControl,
  FormArray,
  ValidatorFn,
  FormBuilder,
} from "@angular/forms";
import {
  ConstantsService,
  LanguageService,
  UserService,
  UtilsService,
} from "../../../../core/service";
import { ToastService } from "../../../../shared/toast/toast.service";
import { TranslateService } from "@ngx-translate/core";
import { SwiperConfigInterface } from "ngx-swiper-wrapper";
import { Subject } from "rxjs";
// import momentTz from "moment-timezone";
import { ActivatedRoute, Router } from "@angular/router";
@Component({
  selector: "tournament-basic",
  templateUrl: "./basic.component.html",
  styleUrls: ["./basic.component.scss"],
  viewProviders: [
    { provide: ControlContainer, useExisting: FormGroupDirective },
  ],
})
export class BasicComponent implements OnInit, OnChanges {
  @Input() basicDetails: Subject<any>;
  @Input() isAllDataLoaded: boolean;
  @Input() isParticipantRegistered: boolean = false;
  currentUrlToCheck = "";
  active = 1;
  condition_active = "description";
  gameArray = [];
  userFullName;
  basicInfo;
  isArabic: boolean;
  selectedGameIndex = 0;
  showLoader = true;
  bracketList = [];
  platform = [];
  venueAddress: any = [];
  sponsors: any = [];

  requiredFields = {
    step1: [
      { name: "CREATE_TOURNAMENT_VALIDATION.KEY1", value: "name" },
      { name: "CREATE_TOURNAMENT_VALIDATION.KEY2", value: "gameDetail" },
      { name: "CREATE_TOURNAMENT_VALIDATION.KEY3", value: "participantType" },
      { name: "CREATE_TOURNAMENT_VALIDATION.KEY4", value: "startDate" },
      { name: "CREATE_TOURNAMENT_VALIDATION.KEY5", value: "startTime" },
      { name: "CREATE_TOURNAMENT_VALIDATION.KEY6", value: "bracketType" },
      { name: "CREATE_TOURNAMENT_VALIDATION.KEY7", value: "noOfSet" },
      { name: "CREATE_TOURNAMENT_VALIDATION.KEY8", value: "stageMatch" },
      { name: "CREATE_TOURNAMENT_VALIDATION.KEY9", value: "stageMatchNoOfSet" },
      { name: "CREATE_TOURNAMENT_VALIDATION.KEY11", value: "regStartDate" },
      { name: "CREATE_TOURNAMENT_VALIDATION.KEY12", value: "regEndDate" },
      { name: "CREATE_TOURNAMENT_VALIDATION.KEY13", value: "regStartTime" },
      { name: "CREATE_TOURNAMENT_VALIDATION.KEY14", value: "regEndTime" },
      { name: "TOURNAMENT.CREATE.NUMBER_OF_ROUND", value: "noOfRoundPerGroup" },
      { name: "TOURNAMENT.CHECK_IN_START_TIME", value: "checkInStartDate" },
      { name: "TOURNAMENT.REG_FEE_CURRENCY", value: "regFeeCurrency" },
      { name: "TOURNAMENT.REG_FEE", value: "regFee" },
      { name: "TOURNAMENT.TEAM_SIZE", value: "teamSize" },
      { name: "TOURNAMENT.PARTICIPANT_LIMIT", value: "maxParticipants" },
      { name: "TOURNAMENT.NUMBER_OF_PALCEMENT", value: "noOfPlacement" },
      { name: "TOURNAMENT.POINT_PER_KILLS", value: "pointsKills" },
      { name: "TOURNAMENT.INPUT_PLACEMENT_POINTS", value: "placementPoints" },
      {
        name: "TOURNAMENT.NO_OF_PARTICIPANT_PER_GROUP",
        value: "noOfTeamInGroup",
      },
      {
        name: "TOURNAMENT.NO_OF_WINNING_PARTICIPANT_PER_GROUP",
        value: "noOfWinningTeamInGroup",
      },
      { name: "TOURNAMENT.NO_OF_ROUNDS_PER_GROUP", value: "noOfRoundPerGroup" },
      { name: "TOURNAMENT.PLATFORM", value: "platform" },
      { name: "TOURNAMENT.END_DATE", value: "endDate" },
      { name: "TOURNAMENT.END_TIME", value: "endTime" },
      { name: "TOURNAMENT.TEAM_FORMAT", value: "teamFormat" },
      { name: "TOURNAMENT.STAGE_BRACKET_TYPE", value: "stageBracketType" },
      {
        name: "TOURNAMENT.SUBSTITUTE_MEMBER_SIZE",
        value: "substituteMemberSize",
      },
      {
        name: "TOURNAMENT.CREATE.NUMBER_OF_ROUND",
        value: "noOfRound",
      },
      {
        name: "TOURNAMENT.NUMBER_OF_LOSS",
        value: "noOfLoss",
      },
      {
        name: "TOURNAMENT.SET_DURATION",
        value: "setDuration",
      },
    ],
    step2: [
      { name: "TOURNAMENT.TOURNAMENT_TYPE", value: "tournamentType" },
      { name: "TOURNAMENT.ADD_MONEY_PRIZE", value: "prizeList" },
      { name: "TOURNAMENT.SPONSORS", value: "sponsors" },
      { name: "TOURNAMENT.ADD_CURRENCY", value: "prizeCurrency" },
      { name: "TOURNAMENT.VENUE_ADDRESS", value: "venueAddress" },
    ],
    step3: [
      { name: "TOURNAMENT.TOURNAMENT_URL", value: "url" },
      { name: "TOURNAMENT.TWITCH_VIDEO_LINK", value: "twitchVideoLink" },
      { name: "TOURNAMENT.YOUTUBE_VIDEO_LINK", value: "youtubeVideoLink" },
      { name: "TOURNAMENT.FACEBOOK_VIDEO_LINK", value: "facebookVideoLink" },
      { name: "TOURNAMENT.CONTACT_DETAILS", value: "whatsApp" },
      { name: "TOURNAMENT.CONTACT_REQUIRED", value: "contactDetails" },
    ],
    step4: [],
  };

  facebookRegex: any = /(?:(?:http|https):\/\/)?(?:www.|m.)?facebook.com\/(?!home.php)(?:(?:\w)*#!\/)?(?:pages\/)?(?:[?\w\-]*\/)?(?:profile.php\?id=(?=\d.*))?([\w\.-]+)/;
  emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  phoneRegex = /^\s*(?:\+?(\d{1,3}))?[-. (]*(\d{3})[-. )]*(\d{3})[-. ]*(\d{4})(?: *x(\d+))?\s*$/;
  youtubeRegex = /^(https?\:\/\/)?((www\.)?youtube\.com|youtu\.?be)\/.+$/;
  twitchRegex = /(?:https:\/\/)?((www\.)?twitch\.tv)\/(\S+)/;
  playStoreUrlRegex = /^(https?\:\/\/)?((www\.)?play\.google\.com)\/.+$/;
  appStoreUrlRegex = /^(https?\:\/\/)?((www\.)?itunes\.apple\.com)\/.+$/;
  webUrlRegex = /(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})/;

  config: SwiperConfigInterface = {
    // a11y: true,
    direction: "horizontal",
    slidesPerView: "auto",
    keyboard: false,
    mousewheel: false,
    scrollbar: false,
    navigation: true,
    pagination: true,
    centeredSlides: true,
    slideToClickedSlide: true,
    //autoHeight: true,
    breakpoints: {
      320: {
        slidesPerView: 3,
        spaceBetween: 15,
      },
      575: {
        slidesPerView: 4,
        spaceBetween: 15,
        autoHeight: false,
      },

      1199: {
        slidesPerView: "auto",
        autoHeight: true,
      },
    },
  };
  minStartDateValue = this.setMinDate();
  enableteamCounter = false;
  participantType;
  teamFormat;
  matchFormat;
  userId;
  activeTab: number;
  prizeLimit = 6;
  currencyList = [];
  checkInTimeOptions = [
    { name: "TOURNAMENT.30_MINUTES_PRIOR", value: { hour: 0, minute: 30 } },
    { name: "TOURNAMENT.1_HOUR_PRIOR", value: { hour: 1, minute: 0 } },
    { name: "TOURNAMENT.2_HOURS_PRIOR", value: { hour: 2, minute: 0 } },
    { name: "TOURNAMENT.3_HOURS_PRIOR", value: { hour: 3, minute: 0 } },
    { name: "TOURNAMENT.4_HOURS_PRIOR", value: { hour: 4, minute: 0 } },
    { name: "TOURNAMENT.5_HOURS_PRIOR", value: { hour: 5, minute: 0 } },
  ];
  stageMatchFormat;
  stageMatchType;
  fallbackURL: string;
  stageBracketType = ["single", "double"];
  tzNames: string[];
  // banner config
  dimension = { width: 1010, height: 586 };
  bannerSize = 1000 * 1000 * 2;

  isWagerTournament = false;

  constructor(
    private tournament: FormGroupDirective,
    private toastService: ToastService,
    private translateService: TranslateService,
    private utilsService: UtilsService,
    public fb: FormBuilder,
    private userService: UserService,
    private activeRoute: ActivatedRoute,
    private languageService: LanguageService,

    @Inject(DOCUMENT) private document: Document
  ) {}

  ngOnInit(): void {
    this.languageService.language.subscribe((lang) => {
      this.isArabic = Boolean(lang === "ar");
    });
    this._initiateTournament();
    this.getCurrentUserDetails();
    this.basicInfo = this.tournament.form;
    const { id } = this.activeRoute.snapshot.params;
    this.participantType = ConstantsService?.participantType;
    this.teamFormat = ConstantsService?.teamFormat;
    this.matchFormat = ConstantsService?.matchFormat;
    this.currencyList = ConstantsService?.currencyList;
    this.checkInTimeOptions = ConstantsService.checkInTimeOptions;
    this.stageMatchType = ConstantsService?.stageMatchType;
    this.stageMatchFormat = ConstantsService?.matchFormat;
    // this.tzNames = momentTz.tz.names();
    this.activeRoute.queryParams.subscribe((params) => {
      if (params["fallback"]) {
        this.fallbackURL = "/admin/esports-management";
        this.activeTab = params["activeTab"] || 0;
      }
      if (params["step"] == 5) {
        this.active = 4;
      }
    });
    this.addControls();
    this.getGames();
  }
  emptyCheck = (control: FormControl) => {
    return control.value && control.value.trim() ? null : { required: true };
  };  
  _initiateTournament() {
    this.basicInfo = this.fb.group({
      name: [
        "",
        Validators.compose([Validators.required, Validators.maxLength(100)]),
      ],
      url: ["", Validators.compose([Validators.required])],
      participantType: ["", Validators.compose([Validators.required])],
      startDate: [
        "",
        Validators.compose([
          Validators.required,
          this.ValidateStartDate.bind(this),
        ]),
      ],
      startTime: [
        "",
        Validators.compose([
          Validators.required,
          this.ValidateStartTime.bind(this),
        ]),
      ],
      endDate: [
        "",
        Validators.compose([
          Validators.required,
          this.ValidateEndDate.bind(this),
        ]),
      ],
      endTime: [
        "",
        Validators.compose([
          Validators.required,
          this.ValidateEndTime.bind(this),
        ]),
      ],
      regStartDate: [
        "",
        Validators.compose([
          Validators.required,
          this.ValidateRegStartDate.bind(this),
        ]),
      ],
      regStartTime: [
        "",
        Validators.compose([
          Validators.required,
          this.ValidateRegStartTime.bind(this),
        ]),
      ],
      regEndDate: [
        "",
        Validators.compose([
          Validators.required,
          this.ValidateRegEndDate.bind(this),
        ]),
      ],
      regEndTime: [
        "",
        Validators.compose([
          Validators.required,
          this.ValidateRegEndTime.bind(this),
        ]),
      ],
      isPaid: [false, Validators.compose([])],
      isCheckInRequired: [false, Validators.compose([])],
      description: ["", Validators.compose([])],
      rules: ["", Validators.compose([])],
      criticalRules: ["", Validators.compose([])],
      isPrize: [false, Validators.compose([])],
      faqs: ["", Validators.compose([])],
      schedule: ["", Validators.compose([])],
      isIncludeSponsor: [false, Validators.compose([])],
      tournamentType: ["", Validators.compose([Validators.required])],
      isScreenshotRequired: [false, Validators.compose([])],
      isShowCountryFlag: [false, Validators.compose([])],
      isManualApproverParticipant: [false, Validators.compose([])],
      isSpecifyAllowedRegions: [false, Validators.compose([])],
      isParticipantsLimit: [true, Validators.compose([])],
      scoreReporting: [1, Validators.compose([])],
      invitationLink: ["", Validators.compose([])],
      youtubeVideoLink: [
        "",
        Validators.compose([Validators.pattern(this.youtubeRegex)]),
      ],
      participants: [[], Validators.compose([])],
      facebookVideoLink: [
        "",
        Validators.compose([Validators.pattern(this.facebookRegex)]),
      ],
      contactDetails: [""],
      twitchVideoLink: [
        "",
        Validators.compose([Validators.pattern(this.twitchRegex)]),
      ],
      visibility: [1, Validators.compose([])],
      checkInStartDate: ["", Validators.compose([])],
      checkInEndDate: ["", Validators.compose([])],
      regionsAllowed: [[], Validators.compose([])],
      sponsors: this.fb.array([]),
      banner: ["", Validators.compose([])],
      // country: ['', Validators.compose([Validators.required])],
      maxParticipants: [
        2,
        Validators.compose([
          Validators.required,
          Validators.min(2),
          Validators.max(1000),
          this.teamInGroupValidation("maxParticipants").bind(this),
        ]),
      ],
      bracketType: ["", Validators.compose([Validators.required])],
      noOfSet: ["", Validators.compose([Validators.required])],
      stageMatch: [""],
      stageMatchNoOfSet: [""],
      noOfRoundPerGroup: [1],
      isAllowMultipleRounds: [false],
      //commenting because none of the type is selected by default
      //venueAddress: this.fb.array([this.createVenueAddress('offline')]),
      venueAddress: this.fb.array([]),
      contactOn: ["", Validators.compose([])],
      gameDetail: ["", Validators.compose([Validators.required])],
      teamSize: [
        "",
        // Validators.compose([Validators.max(10), Validators.min(2)]),
      ],
      whatsApp: ["", Validators.compose([])],
      prizeList: this.fb.array([], this.customPrizeValidator()),
      noOfPlacement: [2],
      placementPoints: this.fb.array([]),
      substituteMemberSize: [0],
      isKillPointRequired: [false, Validators.required],
      platform: ["", Validators.required],
      teamFormat: [""],
      allowSubstituteMember: [false],
      allowAdvanceStage: [false],
      stageBracketType: [""],
      step: [1],
    });
  }
  ValidateRegStartDate(
    control: AbstractControl
  ): { [key: string]: any } | null {
    return !control.value
      ? null
      : this.validateRegStartDateAndTime(
          control["_parent"],
          "pastRegStartDate"
        );
  }
  ValidateRegStartTime(
    control: AbstractControl
  ): { [key: string]: any } | null {
    return !control.value
      ? null
      : this.validateRegStartDateAndTime(
          control["_parent"],
          "pastRegStartTime"
        );
  }
  ValidateRegEndTime(control: AbstractControl): { [key: string]: any } | null {
    return !control.value
      ? null
      : this.validateRegEndDateAndTime(control["_parent"], "pastRegEndTime");
  }
  ValidateRegEndDate(control: AbstractControl): { [key: string]: any } | null {
    return !control.value
      ? null
      : this.validateRegEndDateAndTime(control["_parent"], "pastRegEndDate");
  }
  validateRegStartDateAndTime(
    formGroup: FormGroup,
    ekey: string
  ): { [key: string]: boolean } {
    const {
      regStartDate,
      regStartTime,
      startDate,
      startTime,
    } = formGroup.controls;
    const time = startTime.value || {};
    const { hour, minute } = time;

    if (
      (ekey == "pastRegStartDate" && !regStartDate?.value) ||
      (ekey == "pastRegStartTime" && !regStartTime?.value)
    ) {
      return { required: true };
    } else if (
      startDate?.value &&
      startTime?.value &&
      regStartDate?.value &&
      regStartTime?.value
    ) {
      const currentDate = new Date();
      const startDt = new Date(startDate.value);
      startDt.setHours(hour);
      startDt.setMinutes(minute);
      const regstartDt = new Date(regStartDate.value);
      regstartDt.setHours(regStartTime?.value?.hour);
      regstartDt.setMinutes(regStartTime?.value?.minute);
      if (startDt > regstartDt && regstartDt > currentDate) {
        this.basicInfo
          .get(ekey == "pastRegStartDate" ? "regEndTime" : "regEndDate")
          .setErrors(null);
        this.basicInfo
          .get(ekey == "pastRegStartTime" ? "regStartDate" : "regStartTime")
          .setErrors(null);
        return null;
      } else if (ekey == "pastRegStartDate" && regstartDt < currentDate) {
        return null;
      } else {
        return {
          [ekey]: true,
        };
      }
    }
    return null;
  }
  validateRegEndDateAndTime(
    formGroup: FormGroup,
    ekey: string
  ): { [key: string]: boolean } {
    const {
      regStartDate,
      regStartTime,
      regEndDate,
      regEndTime,
      startDate,
      startTime,
      noOfRound,
      setDuration,
      bracketType,
      isCheckInRequired,
      checkInStartDate,
    } = formGroup.controls;
    if (
      (ekey == "pastRegEndDate" && !regEndDate?.value) ||
      (ekey == "pastRegEndTime" && !regEndTime?.value)
    ) {
      return { required: true };
    } else if (
      regEndDate?.value &&
      regEndTime?.value &&
      regStartDate?.value &&
      regStartTime?.value &&
      startDate?.value &&
      startTime?.value
    ) {
      const regEndDt = new Date(regEndDate.value);
      regEndDt.setHours(regEndTime?.value?.hour);
      regEndDt.setMinutes(regEndTime?.value?.minute);

      const regstartDt = new Date(regStartDate.value);
      regstartDt.setHours(regStartTime?.value?.hour);
      regstartDt.setMinutes(regStartTime?.value?.minute);

      let tournamentStartDt, tournamentStartDate;
      tournamentStartDt = new Date(startDate.value);
      tournamentStartDt.setHours(startTime?.value?.hour);
      tournamentStartDt.setMinutes(startTime?.value?.minute);
      if (bracketType?.value === "swiss_safeis") {
        const tournamentDuration =
          setDuration?.value.days * 24 * 60 +
          setDuration?.value.hours * 60 +
          setDuration?.value.mins;
        const timeToBeAdded = (noOfRound?.value - 1) * tournamentDuration;
        tournamentStartDate = new Date(
          tournamentStartDt.getTime() + timeToBeAdded * 60000
        );
      } else {
        tournamentStartDate = tournamentStartDt;
      }
      if (isCheckInRequired?.value && checkInStartDate?.value) {
        const checkInStart = new Date(startDate.value);
        checkInStart.setHours(startTime.value.hour);
        checkInStart.setMinutes(startTime.value.minute);
        const checkInTime = this.checkInTimeOptions.find(
          (el) => el.name === checkInStartDate.value
        );
        checkInStart.setHours(
          checkInStart.getHours() - checkInTime?.value?.hour
        );
        checkInStart.setMinutes(
          checkInStart.getMinutes() - checkInTime?.value?.minute
        );
        tournamentStartDate = checkInStart;
      }
      if (regEndDt > regstartDt && regEndDt <= tournamentStartDate) {
        this.basicInfo
          .get(ekey == "pastRegEndDate" ? "regEndTime" : "regEndDate")
          .setErrors(null);
        return null;
      } else if (
        ekey == "pastRegEndDate" &&
        regstartDt.toDateString() === regEndDt.toDateString()
      ) {
        return null;
      } else {
        return {
          [ekey]: true,
        };
      }
    }
    return null;
  }
  customPrizeValidator(): ValidatorFn {
    return (formArray: FormArray): { [key: string]: any } | null => {
      let total = 0;
      for (let item of formArray.controls) {
        total = total + item.value.value;
      }
      const maxParticipants =
        formArray["_parent"]?.controls?.maxParticipants?.value;
      const prizeLimit = maxParticipants > 6 ? 6 : maxParticipants;
      if (formArray.controls.length > prizeLimit) {
        return { prizeLimit: true };
      }
      const isPrizeValid = formArray.controls.length ? total > 0 : true;
      return isPrizeValid ? null : { prizeMoneyRequired: true };
    };
  }
  createVenueAddress(type): FormGroup {
    if (["offline", "hybrid"].includes(type)) {
      return this.fb.group({
        country: ["", Validators.compose([Validators.required])],
        region: ["", Validators.compose([Validators.required])],
        venue: ["", Validators.compose([Validators.required])],
        ...(type == "hybrid" && {
          stage: ["", Validators.compose([Validators.required])],
        }),
      });
    }
  }
  getCurrentUserDetails() {
    this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.userFullName = data["fullName"];
      }
      this.userId = data?._id;
    });
  }
  durationValidator = (
    control: AbstractControl
  ): { [key: string]: boolean } => {
    const { days, hours, mins } = control.value;
    const regEndTimeValidate = this.validateRegEndDateAndTime(
      this.basicInfo,
      "pastRegEndTime"
    );
    this.basicInfo.get("regEndTime").setErrors(regEndTimeValidate);
    if (days || hours || mins) return null;
    return {
      required: true,
    };
  };
  ValidateStartTime(control: AbstractControl): { [key: string]: any } | null {
    return !control.value
      ? null
      : this.ValidateBothStartDateAndTime(control["_parent"], "pastStartTime");
  }

  ValidateStartDate(control: AbstractControl): { [key: string]: any } | null {
    return !control.value
      ? null
      : this.ValidateBothStartDateAndTime(control["_parent"], "pastStartDate");
  }

  ValidateEndTime(control: AbstractControl): { [key: string]: any } | null {
    return !control.value
      ? null
      : this.validateBothEndDateAndTime(control["_parent"], "pastEndTime");
  }

  ValidateEndDate(control: AbstractControl): { [key: string]: any } | null {
    return !control.value
      ? null
      : this.validateBothEndDateAndTime(control["_parent"], "pastEndDate");
  }

  validateBothEndDateAndTime(
    formGroup: FormGroup,
    ekey: string
  ): { [key: string]: boolean } {
    const { startDate, startTime, endDate, endTime } = formGroup.controls;
    if (
      (ekey == "pastEndDate" && !endDate?.value) ||
      (ekey == "pastEndTime" && !endTime?.value)
    ) {
      return { required: true };
    } else if (
      startDate?.value &&
      startTime?.value &&
      endDate?.value &&
      endTime?.value
    ) {
      const startDt = new Date(startDate.value);
      startDt.setHours(startTime.value?.hour);
      startDt.setMinutes(startTime.value?.minute);

      const endDt = new Date(endDate.value);
      endDt.setHours(endTime?.value?.hour);
      endDt.setMinutes(endTime?.value?.minute);
      if (endDt > startDt) {
        this.basicInfo
          .get(ekey == "pastEndDate" ? "endTime" : "endDate")
          .setErrors(null);
        return null;
      } else if (
        ekey == "pastEndDate" &&
        endDt.toDateString() === startDt.toDateString()
      ) {
        return null;
      } else {
        return {
          [ekey]: true,
        };
      }
    }
    return null;
  }

  ValidateBothStartDateAndTime(
    formGroup: FormGroup,
    ekey: string
  ): { [key: string]: boolean } {
    const { startDate, startTime, isCheckInRequired } = formGroup.controls;
    const time = startTime.value || {};
    const { hour, minute } = time;

    if (startTime.value && startDate.value) {
      const date = new Date(startDate.value);
      date.setHours(hour);
      date.setMinutes(minute);
      const diff = date.getTime() - Date.now();
      if (diff < 300000) {
        return { [ekey]: true };
      }
      const isCheckInValid = this.validateCheckedIn(formGroup);
      const endDateValidate = this.validateBothEndDateAndTime(
        formGroup,
        "pastEndDate"
      );
      const endTimeValidate = this.validateBothEndDateAndTime(
        formGroup,
        "pastEndTime"
      );
      const regStartDateValidate = this.validateRegStartDateAndTime(
        formGroup,
        "pastRegStartDate"
      );
      const regStartTimeValidate = this.validateRegStartDateAndTime(
        formGroup,
        "pastRegStartTime"
      );
      const regEndTimeValidate = this.validateRegEndDateAndTime(
        formGroup,
        "pastRegEndTime"
      );
      const regEndDateValidate = this.validateRegEndDateAndTime(
        formGroup,
        "pastRegEndDate"
      );
      this.basicInfo.get("endTime").setErrors(endTimeValidate);
      this.basicInfo.get("endDate").setErrors(endDateValidate);
      this.basicInfo.get("regStartDate").setErrors(regStartDateValidate);
      this.basicInfo.get("regStartTime").setErrors(regStartTimeValidate);
      this.basicInfo.get("regEndTime").setErrors(regEndTimeValidate);
      this.basicInfo.get("regEndDate").setErrors(regEndDateValidate);
      if (isCheckInRequired?.value) {
        this.basicInfo.get("checkInStartDate").setErrors(isCheckInValid);
      }
      this.basicInfo.get("startTime").setErrors(null);
      this.basicInfo.get("startDate").setErrors(null);
    }

    return null;
  }

  validateCheckedIn(formGroup: FormGroup): { [key: string]: boolean } {
    const {
      startDate,
      startTime,
      isCheckInRequired,
      checkInStartDate,
    } = formGroup.controls;
    if (
      startDate?.value &&
      startTime?.value &&
      isCheckInRequired?.value &&
      checkInStartDate?.value
    ) {
      const checkInStart = new Date(startDate.value);
      checkInStart.setHours(startTime.value.hour);
      checkInStart.setMinutes(startTime.value.minute);
      const checkInTime = ConstantsService.checkInTimeOptions.find(
        (el) => el.name === checkInStartDate.value
      );

      checkInStart.setHours(checkInStart.getHours() - checkInTime?.value?.hour);
      checkInStart.setMinutes(
        checkInStart.getMinutes() - checkInTime?.value?.minute
      );
      if (checkInStart.getTime() < Date.now()) {
        return { checkInExpired: true };
      }
    }
    return null;
  }

  teamInGroupValidation = (field?: string): ValidatorFn => {
    return (control: AbstractControl): { [key: string]: any } | null => {
      if (control["_parent"]) {
        const {
          noOfTeamInGroup,
          noOfWinningTeamInGroup,
          maxParticipants,
          noOfStage,
        } = control?.["_parent"]?.controls;

        if (
          maxParticipants?.valid &&
          noOfTeamInGroup?.value &&
          noOfWinningTeamInGroup?.value &&
          maxParticipants?.value &&
          noOfStage
        ) {
          const fieldValidation = (
            maxParticipants,
            noOfTeamInGroup,
            noOfWinningTeam
          ) => {
            if (maxParticipants < noOfTeamInGroup) {
              return { max: { max: maxParticipants } };
            } else if (noOfTeamInGroup > noOfWinningTeam) {
              const check = noOfTeamInGroup % noOfWinningTeam != 0;
              return check ? { multiStageConfigError: true } : null;
            } else {
              return { multiStageConfigError: true };
            }
          };
          const errorConfig = fieldValidation(
            maxParticipants?.value,
            noOfTeamInGroup?.value,
            noOfWinningTeamInGroup?.value
          );
          if (!errorConfig) {
            const totalStage = (maxP, teamPerG, winPerG, stage) => {
              const playerForNextRound = (maxP, teamPerG, winPerG) => {
                const diff = Math.ceil(maxP / teamPerG) - maxP / teamPerG;
                return diff < 0.5
                  ? Math.ceil(maxP / teamPerG) * winPerG
                  : Math.floor(maxP / teamPerG) * winPerG;
              };

              const createNoOfTeamInGroup = (
                pLen,
                noOfTeamInGroup,
                noOfWinningTeamInGroup
              ) => {
                const expectedNoOfGroup = Math.ceil(pLen / noOfTeamInGroup);
                const expectedMinPlayerInGroup = Math.floor(
                  pLen / expectedNoOfGroup
                );
                return expectedMinPlayerInGroup > noOfWinningTeamInGroup
                  ? expectedMinPlayerInGroup
                  : createNoOfTeamInGroup(
                      pLen,
                      noOfTeamInGroup + 1,
                      noOfWinningTeamInGroup
                    );
              };

              const teamPerGp = createNoOfTeamInGroup(maxP, teamPerG, winPerG);
              const nextStageP = playerForNextRound(maxP, teamPerGp, winPerG);

              return nextStageP == winPerG
                ? stage
                : nextStageP < teamPerG
                ? stage + 1
                : totalStage(nextStageP, teamPerG, winPerG, stage + 1);
            };

            const noOfStage = totalStage(
              maxParticipants?.value,
              noOfTeamInGroup?.value,
              noOfWinningTeamInGroup?.value,
              1
            );
            this.basicInfo.get("noOfStage").setValue(noOfStage);
          }

          if (field == "noOfTeamInGroup") {
            return errorConfig;
          } else {
            this.basicInfo.get("noOfTeamInGroup").setErrors(errorConfig);
            return null;
          }
        }
      } else {
        return null;
      }
    };
  };

  ValidateCheckInDate(control: AbstractControl): { [key: string]: any } | null {
    return control.value ? this.validateCheckedIn(control["_parent"]) : null;
  }

  /**
   *
   * @param field
   * @param fieldType 'STRING', 'BOOLEAN', 'NUMBER'
   * @param defaultValue
   * @param min
   * @param max
   */
  addNewControlAndUpdateValidity = (
    field,
    fieldType,
    defaultValue,
    min?,
    max?,
    pattern?,
    customValidation?
  ) => {
    const formControl = this.basicInfo.get(field);
    const validation = [];

    switch (fieldType) {
      case "NUMBER":
        validation.push(Validators.required);
        if (typeof min == "number") validation.push(Validators.min(min));
        if (typeof max == "number") validation.push(Validators.max(max));
        if (pattern) validation.push(pattern);
        if (customValidation) validation.push(customValidation);
        break;
      case "BOOLEAN":
        validation.push(Validators.required);
        break;
      case "STRING":
        validation.push(Validators.required);
        break;
    }

    if (formControl) {
      formControl.setValue(defaultValue);
      formControl.setValidators(validation);
      formControl.updateValueAndValidity();
    } else {
      this.basicInfo.addControl(
        field,
        new FormControl(defaultValue, Validators.compose(validation))
      );
    }
  };
  removeControlOrValidity = (field) => {
    const formControl = this.basicInfo.get(field);
    if (formControl) {
      this.basicInfo.removeControl(field);
    } else {
    }
  };

  // Event Handlers ...

  tournamentNameChangeHandler(name) {
    const domain = `${this.document.location.protocol}//${this.document.location.hostname}/tournament/`;
    const url = `${domain}${name.replace(/\s+/g, "_")}`;
    this.basicInfo.get("url").setValue(url);
  }

  participantTypeHandler(val) {
    if (val == "team") {
      this.basicInfo.controls["teamFormat"].setValidators([
        Validators.required,
      ]);
      this.addNewControlAndUpdateValidity("teamSize", "NUMBER", 2, 1, 10);
    } else {
      this.removeControlOrValidity("teamSize");
      this.basicInfo.controls["teamFormat"].clearValidators();
      this.basicInfo.controls["substituteMemberSize"].clearValidators();
      this.basicInfo.controls["substituteMemberSize"].setValue(0);
    }
    this.basicInfo.controls["teamFormat"].updateValueAndValidity();
  }

  teamFormatHandler = (event, field) => {
    this.enableteamCounter = event == "n";
    if (!this.enableteamCounter) {
      this.basicInfo.get(field).setValue(event);
    } else {
      this.basicInfo.get(field).setValue(2);
    }
  };

  allowSubstituteMemberHandler = (check) => {
    if (check) {
      this.basicInfo.controls["substituteMemberSize"].setValidators([
        Validators.required,
        Validators.max(10),
        Validators.min(1),
      ]);
    } else {
      this.basicInfo.controls["substituteMemberSize"].clearValidators();
      this.basicInfo.controls["substituteMemberSize"].setValue(0);
    }
    this.basicInfo.get("substituteMemberSize").updateValueAndValidity();
  };
  bracketChangeHandler(
    type,
    noOfPlacement?,
    maxPlacement = 2,
    maxParticipants?,
    noOfRound?,
    noOfLoss?
  ) {
    let maximumParticipants = 1024;

    if (["round_robin", "battle_royale"].includes(type)) {
      const maxLimit = type == "battle_royale" ? 10 : 3;
      this.addNewControlAndUpdateValidity(
        "noOfRoundPerGroup",
        "NUMBER",
        1,
        1,
        maxLimit
      );

      type == "battle_royale"
        ? this.removeControlOrValidity("noOfSet")
        : this.addNewControlAndUpdateValidity("noOfSet", "STRING", "");
      type == "battle_royale"
        ? this.addNewControlAndUpdateValidity(
            "noOfPlacement",
            "NUMBER",
            2,
            2,
            this.basicInfo?.value?.noOfTeamInGroup || maxPlacement || 2
          )
        : this.removeControlOrValidity("noOfPlacement");
      type == "battle_royale"
        ? this.placementCounter(noOfPlacement || 2)
        : this.removeControlOrValidity("placementPoints");
      type == "battle_royale"
        ? this.addNewControlAndUpdateValidity(
            "isKillPointRequired",
            "BOOLEAN",
            false
          )
        : this.removeControlOrValidity("isKillPointRequired");
      type == "battle_royale"
        ? this.removeControlOrValidity("stageBracketType")
        : this.addNewControlAndUpdateValidity("stageBracketType", "STRING", "");
      type == "battle_royale"
        ? this.removeControlOrValidity("allowAdvanceStage")
        : this.addNewControlAndUpdateValidity(
            "allowAdvanceStage",
            "BOOLEAN",
            false
          );

      this.multiStageTournamentHandler(true, type, maxParticipants);
    } else {
      this.multiStageTournamentHandler(false);
      this.removeControlOrValidity("noOfRoundPerGroup");
      this.addNewControlAndUpdateValidity("noOfSet", "STRING", "");
      this.removeControlOrValidity("noOfPlacement");
      this.removeControlOrValidity("placementPoints");
      this.removeControlOrValidity("isKillPointRequired");
      this.removeControlOrValidity("stageBracketType");
      this.addNewControlAndUpdateValidity(
        "allowAdvanceStage",
        "BOOLEAN",
        false
      );
    }

    switch (type) {
      case "single":
        maximumParticipants = 1024;
        break;
      case "double":
        maximumParticipants = 512;
        break;
      case "round_robin":
        maximumParticipants = 40;
        break;
      case "battle_royale":
        maximumParticipants = 1024;
        break;
    }

    if (type == "swiss_safeis") {
      this.addNewControlAndUpdateValidity(
        "noOfRound",
        "NUMBER",
        noOfRound || 1,
        1
      );
      this.addNewControlAndUpdateValidity(
        "noOfLoss",
        "NUMBER",
        noOfLoss || 0,
        0,
        (noOfRound || 1) - 1
      );
      this.addNewControlAndUpdateValidity("enableTiebreaker", "BOOLEAN", false);
      this.basicInfo.addControl(
        "setDuration",
        this.fb.group(
          {
            days: [1, Validators.required],
            hours: [0, Validators.required],
            mins: [0, Validators.required],
          },
          { validator: this.durationValidator }
        )
      );
      this.basicInfo.get("isParticipantsLimit").setValue(false);
      this.removeControlOrValidity("maxParticipants");
      this.prizeLimit = 20;
    } else {
      this.removeControlOrValidity("noOfRound");
      this.removeControlOrValidity("noOfLoss");
      this.removeControlOrValidity("setDuration");
      this.removeControlOrValidity("enableTiebreaker");

      this.basicInfo.get("isParticipantsLimit").setValue(true);
      this.addNewControlAndUpdateValidity(
        "maxParticipants",
        "NUMBER",
        2,
        2,
        maximumParticipants,
        false,
        this.teamInGroupValidation("maxParticipants").bind(this)
      );
    }
  }
  maxParticipantHandler(value, bracketType) {
    if (["battle_royale", "round_robin"].includes(bracketType)) {
      const maxLimit =
        bracketType == "battle_royale" ? value : value >= 24 ? 24 : value;
      this.addNewControlAndUpdateValidity(
        "noOfTeamInGroup",
        "NUMBER",
        2,
        2,
        maxLimit,
        false,
        this.teamInGroupValidation("noOfTeamInGroup").bind(this)
      );
    }
  }

  killingPointHandler = (isAllowKillingPoint) => {
    isAllowKillingPoint
      ? this.addNewControlAndUpdateValidity("pointsKills", "NUMBER", 1, 1)
      : this.removeControlOrValidity("pointsKills");
  };

  paidRegistrationHandler(check) {
    if (check) {
      this.addNewControlAndUpdateValidity("regFeeCurrency", "STRING", "");
      this.addNewControlAndUpdateValidity("regFee", "NUMBER", 1, 1);
    } else {
      this.removeControlOrValidity("regFeeCurrency");
      this.removeControlOrValidity("regFee");
    }
  }

  wagerTournamentHandler(check) {
    this.basicInfo.get("isPaid").setValue(check);
    this.paidRegistrationHandler(check);
  }

  checkInDateHandler(check) {
    if (check) {
      this.basicInfo.addControl(
        "checkInStartDate",
        new FormControl(
          "",
          Validators.compose([
            Validators.required,
            this.ValidateCheckInDate.bind(this),
          ])
        )
      );
    } else {
      this.basicInfo.removeControl("checkInStartDate");
    }
  }

  allowAdvanceStageHandler = (allowAdvanceStage) => {
    if (allowAdvanceStage) {
      this.addNewControlAndUpdateValidity("stageMatch", "STRING", "");
      this.addNewControlAndUpdateValidity("stageMatchNoOfSet", "STRING", "");
    } else {
      this.removeControlOrValidity("stageMatch");
      this.removeControlOrValidity("stageMatchNoOfSet");
    }
  };
  placementCounter = async (value) => {
    this.basicInfo.addControl("placementPoints", this.fb.array([]));
    const placementPointsList = this.basicInfo.get(
      "placementPoints"
    ) as FormArray;

    const createPlacementForm = (position): FormGroup => {
      return this.fb.group({
        position: [position],
        value: [
          "",
          Validators.compose([
            Validators.required,
            Validators.pattern("^[0-9]*$"),
          ]),
        ],
      });
    };

    // remove placement node
    const deletePlacementForm = async (count, value) => {
      placementPointsList.removeAt(count);
      return count <= value ? true : await deletePlacementForm(--count, value);
    };

    // add placement node
    const addPlacementForm = async (count, value) => {
      placementPointsList.push(createPlacementForm(count));
      return count >= value ? true : await addPlacementForm(++count, value);
    };

    let count = placementPointsList.length;

    if (count > value) {
      await deletePlacementForm(count - 1, value);
    }

    if (count < value) {
      await addPlacementForm(count + 1, value);
    }
  };
  roundHandler(noOfRound) {
    if (noOfRound) {
      const regEndTimeValidate = this.validateRegEndDateAndTime(
        this.basicInfo,
        "pastRegEndTime"
      );
      this.basicInfo.get("regEndTime").setErrors(regEndTimeValidate);
      this.addNewControlAndUpdateValidity(
        "noOfLoss",
        "NUMBER",
        0,
        0,
        noOfRound - 1
      );
    }
  }
  multiStageTournamentHandler = (
    isEnableFields: boolean,
    bracketType?,
    maxParticipants?
  ): void => {
    if (isEnableFields) {
      const noOfRound = bracketType == "round_robin" ? 2 : 10;
      this.addNewControlAndUpdateValidity(
        "noOfTeamInGroup",
        "NUMBER",
        2,
        2,
        maxParticipants || this.basicInfo.value?.maxParticipants || 2,
        false,
        this.teamInGroupValidation("noOfTeamInGroup").bind(this)
      );
      this.addNewControlAndUpdateValidity(
        "noOfWinningTeamInGroup",
        "NUMBER",
        1,
        1,
        false,
        false,
        this.teamInGroupValidation("noOfWinningTeamInGroup").bind(this)
      );
      this.addNewControlAndUpdateValidity(
        "noOfRoundPerGroup",
        "NUMBER",
        1,
        1,
        noOfRound
      );
      this.addNewControlAndUpdateValidity("noOfStage", "NUMBER", 1);
    } else {
      this.removeControlOrValidity("noOfTeamInGroup");
      this.removeControlOrValidity("noOfWinningTeamInGroup");
      this.removeControlOrValidity("noOfRoundPerGroup");
      this.removeControlOrValidity("noOfStage");
    }
  };

  noOfTeamsPerGroupHandler(value, bracketType) {
    if (bracketType === "battle_royale") {
      this.addNewControlAndUpdateValidity(
        "noOfPlacement",
        "NUMBER",
        1,
        2,
        value || 2
      );
    }
  }

  // add controls
  addControls() {
    this.basicInfo.addControl(
      "name",
      new FormControl(
        "",
        Validators.compose([
          Validators.required,
          Validators.maxLength(100),
          this.emptyCheck,
        ])
      )
    );
    this.basicInfo.addControl("url", new FormControl(""));
    this.basicInfo.addControl(
      "gameDetail",
      new FormControl("", Validators.compose([Validators.required]))
    );

    this.basicInfo.addControl(
      "startDate",
      new FormControl(
        "",
        Validators.compose([
          Validators.required,
          this.ValidateStartDate.bind(this),
        ])
      )
    );
    this.basicInfo.addControl(
      "startTime",
      new FormControl(
        "",
        Validators.compose([
          Validators.required,
          this.ValidateStartTime.bind(this),
        ])
      )
    );
    this.basicInfo.addControl(
      "endDate",
      new FormControl(
        "",
        Validators.compose([
          Validators.required,
          this.ValidateEndDate.bind(this),
        ])
      )
    );
    this.basicInfo.addControl(
      "endTime",
      new FormControl(
        "",
        Validators.compose([
          Validators.required,
          this.ValidateEndTime.bind(this),
        ])
      )
    );
    this.basicInfo.addControl(
      "regStartDate",
      new FormControl(
        "",
        Validators.compose([
          Validators.required,
          this.ValidateRegStartDate.bind(this),
        ])
      )
    );
    this.basicInfo.addControl(
      "regStartTime",
      new FormControl(
        "",
        Validators.compose([
          Validators.required,
          this.ValidateRegStartTime.bind(this),
        ])
      )
    );
    this.basicInfo.addControl(
      "regEndDate",
      new FormControl(
        "",
        Validators.compose([
          Validators.required,
          this.ValidateRegEndDate.bind(this),
        ])
      )
    );
    this.basicInfo.addControl(
      "regEndTime",
      new FormControl(
        "",
        Validators.compose([
          Validators.required,
          this.ValidateRegEndTime.bind(this),
        ])
      )
    );
    this.basicInfo.addControl(
      "participantType",
      new FormControl("", Validators.compose([Validators.required]))
    );
    this.basicInfo.addControl(
      "teamFormat",
      new FormControl("", Validators.compose([]))
    );
    this.basicInfo.addControl("allowSubstituteMember", new FormControl(false));
    this.basicInfo.addControl("substituteMemberSize", new FormControl(""));

    this.basicInfo.addControl(
      "platform",
      new FormControl("", Validators.compose([Validators.required]))
    );
    this.basicInfo.addControl(
      "bracketType",
      new FormControl("", Validators.compose([Validators.required]))
    );

    this.basicInfo.addControl(
      "maxParticipants",
      new FormControl(
        2,
        Validators.compose([
          Validators.required,
          Validators.max(1000),
          Validators.min(2),
        ])
      )
    );

    this.basicInfo.addControl("isPaid", new FormControl(false));
    this.basicInfo.addControl("isCheckInRequired", new FormControl(false));
    this.basicInfo.addControl("isWager", new FormControl(false));
    this.basicInfo.addControl("banner", new FormControl(""));
    // this.basicInfo.addControl("timezone", new FormControl(momentTz.tz.guess()));
    this.basicInfo.addControl("isParticipantsLimit", new FormControl(false));

    this.basicDetails.subscribe((basic) => {
      if (basic) {
        this.updateBasicDetails(basic);
      }
    });
  }

  updateBasicDetails(basic) {
    const getDateTime = (f1, f2, date) => {
      const d = new Date(date);
      const formatToTwoDigit = (number) =>
        number < 10 ? `0${number}` : number;

      return {
        [f1]: `${d.getFullYear()}-${formatToTwoDigit(
          d.getMonth() + 1
        )}-${formatToTwoDigit(d.getDate())}`,
        [f2]: {
          hour: d.getHours(),
          minute: d.getMinutes(),
          second: 0,
        },
      };
    };
    // get check-in time for tournament
    const setStartCheckedInDate = (startDate, checkInStartDate) => {
      const d1 = new Date(startDate).getTime();
      const d2 = new Date(checkInStartDate).getTime();
      const hour = Math.floor((d1 - d2) / (3600 * 1000));
      const minute = Math.floor((d1 - d2) / (60 * 1000)) - hour * 60;
      const value = ConstantsService.checkInTimeOptions.find((el) => {
        return el.value.minute == minute && el.value.hour == hour;
      });
      return value.name;
    };

    const getTeamFormatValues = (data) => {
      const n = data.teamSize.toString();
      if (["2", "4"].includes(n)) {
        this.enableteamCounter = false;
        return {
          teamFormat: n,
          teamSize: n,
        };
      } else {
        this.enableteamCounter = true;
        return {
          teamFormat: "n",
          teamSize: n,
        };
      }
    };

    this.participantTypeHandler(basic?.participantType);
    // this.paidRegistrationHandler(basic?.isPaid);
    this.wagerTournamentHandler(basic?.isPaid);
    this.checkInDateHandler(basic?.isCheckInRequired);
    this.killingPointHandler(basic?.isKillPointRequired);
    this.allowAdvanceStageHandler(basic?.allowAdvanceStage);

    if (basic?.bracketType) {
      if (["single", "double"].includes(basic?.bracketType)) {
        this.bracketChangeHandler(basic?.bracketType);
      } else {
        this.bracketChangeHandler(
          basic?.bracketType,
          basic?.noOfPlacement,
          basic?.noOfTeamInGroup,
          basic?.maxParticipants
        );
      }
    }

    this.basicInfo.patchValue({
      ...basic,
      ...(basic.startDate &&
        getDateTime("startDate", "startTime", basic.startDate)),
      ...(basic.endDate && getDateTime("endDate", "endTime", basic.endDate)),
      ...(basic.regStartDate &&
        getDateTime("regStartDate", "regStartTime", basic.regStartDate)),
      ...(basic.regEndDate &&
        getDateTime("regEndDate", "regEndTime", basic.regEndDate)),
      ...(basic.isCheckInRequired && {
        checkInStartDate: setStartCheckedInDate(
          basic?.startDate,
          basic?.checkInStartDate
        ),
      }),
      ...(basic?.participantType == "team" && getTeamFormatValues(basic)),
      gameDetail: basic.gameDetail?._id || basic?.gameDetail,
    });
    this.getGames();
  }

  setMinDate() {
    const date = new Date();
    const year: any = date.getFullYear();
    let month: any = date.getMonth() + 1;
    month = month < 10 ? "0" + month : month;
    let day: any = date.getDate();
    day = day < 10 ? "0" + day : day;
    const d = `${year}-${month}-${day}`;
    return d;
  }

  getGames = async (enableToaster = false) => {
    try {
      if (this.utilsService.getGame().length) {
        this.gameArray = this.utilsService.getGame();
        if (this.basicInfo.value.gameDetail) {
          const gameIndex = this.gameArray.findIndex(
            (item) =>
              item._id ==
              (this.basicInfo?.value?.gameDetail?._id ||
                this.basicInfo?.value?.gameDetail)
          );
          this.setGameOnSlide(gameIndex, true, enableToaster);
        } else {
          this.setGameOnSlide(
            Math.floor(this.gameArray.length / 2),
            false,
            enableToaster
          );
        }
      }
      this.showLoader = false;
    } catch (error) {
      this.showLoader = false;
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  setGameOnSlide(index, updateSlider, enableToast = true) {
    this.selectedGameIndex = index;
    this.basicInfo.get("gameDetail").setValue(this.gameArray[index]?._id);
    this.bracketList = ConstantsService.bracketType.filter(
      (item) => this.gameArray[index].bracketTypes[item.value]
    );
    this.platform = this.gameArray[index]?.platform;

    if (!this.basicInfo?.value?._id && updateSlider) {
      this.basicInfo.get("bracketType").setValue("");
      this.basicInfo.get("platform").setValue("");

      this.basicInfo.get("bracketType").updateValueAndValidity();
      this.basicInfo.get("platform").updateValueAndValidity();
    }

    if (enableToast) {
      this.toastService.showSuccess(
        `<h6>${this.gameArray[index]["name"]} ${this.translateService.instant(
          "TOURNAMENT.SELECTED"
        )}</h6>`
      );
    }
  }

  ngOnChanges(simpleChanges: SimpleChanges) {
    if (
      simpleChanges.hasOwnProperty("isAllDataLoaded") &&
      this.isAllDataLoaded
    ) {
      this.getGames();
    }
  }
}
