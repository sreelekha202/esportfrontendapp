import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  OnChanges,
  SimpleChanges,
} from "@angular/core";
import { TournamentService, UtilsService } from "../../../../core/service";
import { ToastService } from "../../../../shared/toast/toast.service";
import { TranslateService } from "@ngx-translate/core";

import {
  Validators,
  FormGroup,
  FormBuilder,
  AbstractControl,
} from "@angular/forms";
import { GlobalUtils } from "../../../../shared/service/global-utils/global-utils";

@Component({
  selector: "app-edit",
  templateUrl: "./edit.component.html",
  styleUrls: [
    "./edit.component.scss",
    "../tournament-management.component.scss",
  ],
})
export class EditComponent implements OnInit, OnChanges {
  @Input() tournament;
  @Input() minParticipant;
  @Output() response = new EventEmitter<any>();
  tournamentForm: FormGroup;
  minStartDateValue = this.setMinDate();
  origin;
  maximumParticipants = 1024;
  timeoutId = null;

  constructor(
    private fb: FormBuilder,
    private tournamentService: TournamentService,
    private toastService: ToastService,
    private utilsService: UtilsService,
    private translateService: TranslateService
  ) {
    if (GlobalUtils.isBrowser()) {
      this.origin = window.parent.location.origin;
    }
  }

  ngOnInit(): void {
    this.setMaxParticipantLimit();
    this.tournamentForm = this.fb.group(
      {
        name: ["", Validators.required],
        url: ["", Validators.required],
        organiserName: [],
        maxParticipants: [
          "",
          Validators.compose([
            Validators.required,
            Validators.min(this.minParticipant >= 2 ? this.minParticipant : 2),
            Validators.max(this.maximumParticipants),
          ]),
        ],
        startDate: ["", Validators.required],
        startTime: ["", Validators.required],
        participantType: ["", Validators.required],
      },
      { validator: this.formValidator }
    );
    this.setEditForm();
  }

  setMinDate() {
    const date = new Date();
    const year: any = date.getFullYear();
    let month: any = date.getMonth() + 1;
    month = month < 10 ? "0" + month : month;
    let day: any = date.getDate();
    day = day < 10 ? "0" + day : day;
    const d = `${year}-${month}-${day}`;
    return d;
  }

  setEditForm() {
    const date = new Date(this.tournament.startDate);
    const formatToTwoDigit = (number) => {
      if (number < 10) {
        return "0" + number;
      }
      return number;
    };

    this.tournamentForm.patchValue({
      ...this.tournament,
      organiserName: this.tournament.organizerDetail.fullName,
      startDate: `${date.getFullYear()}-${formatToTwoDigit(
        date.getMonth() + 1
      )}-${formatToTwoDigit(date.getDate())}`,
      startTime: {
        hour: date.getHours(),
        minute: date.getMinutes(),
        second: 0,
      },
    });
  }

  setMaxParticipantLimit() {
    switch (this.tournament.bracketType) {
      case "single":
        this.maximumParticipants = 1024;
        break;
      case "double":
        this.maximumParticipants = 512;
        break;
      case "round_robin":
        this.maximumParticipants = 40;
        break;
    }
  }

  createTournamentUrl = async () => {
    try {
      const checkUniqueUrl = async () => {
        const { name } = this.tournamentForm.value;
        if (!name) {
          return;
        }
        const nameArray = name.split(" ");
        const url = this.origin + "/" + nameArray.join("_");
        const query = JSON.stringify({
          url,
          _id: { $ne: this.tournament?._id },
        });
        const response = await this.tournamentService
          .getTournaments({ query })
          .toPromise();

        if (response.data.length > 0) {
          this.tournamentForm.get("url").setErrors({ urlAlreadyExist: true });
        } else {
          this.tournamentForm.get("url").setValue(url);
        }
      };
      if (!this.timeoutId) {
        this.timeoutId = setTimeout(checkUniqueUrl, 500);
      } else {
        clearTimeout(this.timeoutId);
        this.timeoutId = setTimeout(checkUniqueUrl, 500);
      }
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  formValidator(c: AbstractControl): { invalidTime: boolean } {
    const d1 = new Date();
    const d2 = new Date(c.get("startDate").value);
    d2.setHours(
      c.get("startTime").value.hour,
      c.get("startTime").value.minute,
      0
    );
    if (d1 > d2) {
      return { invalidTime: true };
    }
  }

  submit = async () => {
    try {
      if (this.tournamentForm.invalid) {
        this.tournamentForm.markAllAsTouched();
        return;
      }

      if (this.tournament?.isFinished) {
        this.toastService.showInfo(
          this.translateService.instant(
            "MANAGE_TOURNAMENT.EDIT.EDIT_FINISHED_TOURNAMENT"
          )
        );
        return;
      }

      if (this.tournament?.isSeeded) {
        this.toastService.showInfo(
          this.translateService.instant(
            "MANAGE_TOURNAMENT.EDIT.EDIT_SEEDED_TOURNAMENT"
          )
        );
        return;
      }

      const value = this.tournamentForm.value;
      const d1 = new Date();
      const d2 = new Date(value.startDate);
      d2.setHours(value.startTime.hour, value.startTime.minute, 0);
      if (d1 > d2) {
        this.tournamentForm.setErrors({ invalid: true });
        return;
      }
      const { hour, minute } = value.startTime;
      value.startTime = this.utilsService.convertTimeIntoString(
        (hour < 10 ? "0" + hour : hour) +
          ":" +
          (minute < 10 ? "0" + minute : minute) +
          ""
      );
      value.startDate = d2;
      const response = await this.tournamentService
        .updateTournament(value, this.tournament._id)
        .toPromise();
      this.toastService.showSuccess(response?.message);
      this.response.emit({ refresh: true, active: 1 });
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  cancel() {
    this.response.emit({ refresh: false, active: 1 });
  }

  ngOnChanges(simpleChanges: SimpleChanges) {}
}
