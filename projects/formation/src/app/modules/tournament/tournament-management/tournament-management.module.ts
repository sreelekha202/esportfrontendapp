import { NgModule } from "@angular/core";
import { FaIconLibrary } from "@fortawesome/angular-fontawesome";
import {
  faSortDown,
  faSortUp,
  faTrash,
  faEdit,
  faCheck,
  faCheckCircle,
  faTimesCircle,
  faCaretDown,
  faThumbsUp,
  faThumbsDown,
} from "@fortawesome/free-solid-svg-icons";

import { SharedModule } from "../../../shared/modules/shared.module";

import { TournamentManagementComponent } from "./tournament-management.component";
import { SeedComponent } from "./seed/seed.component";
import { PaginationModule, PaginationConfig } from "ngx-bootstrap/pagination";
import { TournamentManagementRoutingModule } from "./tournament-management-routing.module";
import { ParticipantsComponent } from "./participants/participants.component";
import { EditComponent } from "./edit/edit.component";

// import { ParticipantTypePipe } from "./pipe/participant-type.pipe";
import { ParticipantStatusPipe } from "./pipe/participant-status.pipe";

@NgModule({
  declarations: [
    TournamentManagementComponent,
    SeedComponent,
    ParticipantsComponent,
    EditComponent,
    // ParticipantTypePipe,
    ParticipantStatusPipe,
  ],
  imports: [
    TournamentManagementRoutingModule,
    PaginationModule.forRoot(),
    SharedModule,
  ],
  exports: [TournamentManagementComponent],
  providers: [{ provide: PaginationConfig }],
})
export class TournamentManagementModule {
  constructor(private library: FaIconLibrary) {
    library.addIcons(
      faSortDown,
      faSortUp,
      faTrash,
      faEdit,
      faCheck,
      faCheckCircle,
      faTimesCircle,
      faCaretDown,
      faThumbsUp,
      faThumbsDown
    );
  }
}
