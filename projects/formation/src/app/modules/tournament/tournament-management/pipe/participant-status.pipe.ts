import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "pStatus",
})
export class ParticipantStatusPipe implements PipeTransform {
  transform(value) {
    switch (value) {
      case "approvedParticipants":
        return "PARTICIPANT_EXPORT.APPROVAL";
      case "pendingParticipants":
        return "PARTICIPANT_EXPORT.PENDING";
      case "rejectedParticipants":
        return "PARTICIPANT_EXPORT.REJECTED";
      default:
        return "N/A";
    }
  }
}
