import { Title } from '@angular/platform-browser';
import { TranslateService } from '@ngx-translate/core';
import { MatDialog } from '@angular/material/dialog';
import { Component, OnInit, Inject, PLATFORM_ID } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import 'rxjs/add/operator/filter';

import {
  InfoPopupComponent,
  InfoPopupComponentData,
  InfoPopupComponentType,
} from '../../../shared/popups/info-popup/info-popup.component';
import {
  LanguageService,
  RatingService,
  TournamentService,
  UtilsService,
} from '../../../core/service';
import { GlobalUtils } from '../../../shared/service/global-utils/global-utils';
import { ToastService } from '../../../shared/toast/toast.service';
import { DOCUMENT } from '@angular/common';
import { UserService } from '../../../core/service';
import { IUser } from '../../../shared/models';
import { isPlatformBrowser, Location } from '@angular/common';
import { EsportsTimezone } from 'esports';

@Component({
  selector: 'app-view-tournament',
  templateUrl: './view-tournament.component.html',
  styleUrls: ['./view-tournament.component.scss'],
})
export class ViewTournamentComponent implements OnInit {
  tournamentDetails;
  active = 1;
  isLoaded = false;
  hideMessage = true;
  domain;
  user: IUser;
  isBrowser: boolean;
  enableComment: boolean = false;
  text: string;
  participantId: string | null;
  isOrganizer: boolean = false;
  isParticipant: boolean = false;
  timezone;

  constructor(
    private tournamentService: TournamentService,
    private activatedRoute: ActivatedRoute,
    private toastService: ToastService,
    private utilsService: UtilsService,
    private matDialog: MatDialog,
    private ratingService: RatingService,
    private translateService: TranslateService,
    private globalUtils: GlobalUtils,
    private languageService: LanguageService,
    private router: Router,
    private titleService: Title,
    @Inject(DOCUMENT) private document: Document,
    private userService: UserService,
    private esportsTimezone: EsportsTimezone,
    @Inject(PLATFORM_ID) private platformId,
    private location: Location
  ) {
    this.isBrowser = isPlatformBrowser(platformId);
  }

  ngOnInit(): void {
    this.timezone = this.esportsTimezone.getTimeZoneName();
    this.languageService.language.subscribe((lang) =>
      this.translateService.use(lang || 'en')
    );
    this.getCurrentUserDetails();
    const slug = this.activatedRoute.snapshot.params.id; // get slug from route
    // this.tournamentId = this.activatedRoute.snapshot.queryParams.id;
    // if (!this.tournamentId) {
    //   this.router.navigate(["/404"]);
    // } else {
    //   this.fetchTournamentDetails(this.tournamentId);
    // }
    this.fetchTournamentDetails(slug);
    this.domain =
      this.document.location.protocol + '//' + this.document.location.hostname;
    // this.activatedRoute.queryParams
    //   .filter((params) => params.id)
    //   .subscribe((params) => {
    //     this.tournamentId = params.id;
    //     this.fetchTournamentDetails(this.tournamentId);
    //   });
    // if (GlobalUtils.isBrowser()) {
    //   if (this.tournamentId) {
    //     this.deeplinkService.deeplink({
    //       objectType: "tournament",
    //       objectId: this.tournamentId,
    //     });
    //   }
    // }
  }

  fetchTournamentDetails = async (slug) => {
    try {
      this.isLoaded = false;
      const tournament = await this.tournamentService
        .getTournamentBySlug(slug)
        .toPromise();
      // this.tournamentDetails = tournament.data.length
      //   ? tournament.data[0]
      //   : null;
      this.tournamentDetails = tournament.data;
      if (this.tournamentDetails?.organizerDetail?._id == this.user?._id) {
        this.isOrganizer = true;
      }

      if (!this.tournamentDetails) {
        this.hideMessage = false;
        this.toastService.showError(
          this.translateService.instant('VIEW_TOURNAMENT.ERROR')
        );
        this.location.back();
      } else {
        if (
          (this.tournamentDetails && this.tournamentDetails.banner) ||
          this.tournamentDetails.gameDetail.logo
        ) {
          this.titleService.setTitle(this.tournamentDetails.name);
          this.globalUtils.setMetaTags([
            {
              property: 'twitter:image',
              content:
                this.tournamentDetails.banner ||
                this.tournamentDetails.gameDetail.logo,
            },
            {
              property: 'og:image',
              content:
                this.tournamentDetails.banner ||
                this.tournamentDetails.gameDetail.logo,
            },
            {
              property: 'og:image:secure_url',
              content:
                this.tournamentDetails.banner ||
                this.tournamentDetails.gameDetail.logo,
            },
            {
              property: 'og:image:url',
              content:
                this.tournamentDetails.banner ||
                this.tournamentDetails.gameDetail.logo,
            },
            {
              property: 'og:image:width',
              content: '1200',
            },
            {
              property: 'og:image:height',
              content: '630',
            },
            {
              name: 'description',
              content: this.tournamentDetails.description,
            },
            {
              property: 'og:description',
              content: this.tournamentDetails.description,
            },
            {
              property: 'twitter:description',
              content: this.tournamentDetails.description,
            },
            {
              name: 'title',
              content: this.tournamentDetails.name,
            },
            {
              name: 'title',
              content: this.tournamentDetails.name,
            },
            {
              property: 'og:title',
              content: this.tournamentDetails.name,
            },
            {
              property: 'twitter:title',
              content: this.tournamentDetails.name,
            },
            {
              property: 'og:url',
              content: this.domain + this.router.url,
            },
          ]);
        }
        const data = await this.fetchParticipantRegisterationStatus(
          this.tournamentDetails?._id
        );
        if (data.type == 'already-registered') {
          this.isParticipant = true;
        }
      }
      this.isLoaded = true;
    } catch (error) {
      this.isLoaded = true;
      this.toastService.showError(error?.error?.message || error?.message);
      this.router.navigate(['/404']);
    }
  };

  getCheckInDuration() {
    if (
      this.tournamentDetails?.checkInStartDate &&
      this.tournamentDetails?.checkInEndDate
    ) {
      const startDate = new Date(this.tournamentDetails.checkInStartDate);
      const endDate = new Date(this.tournamentDetails.checkInEndDate);
      if (
        this.tournamentDetails?.checkInStartDate ===
        this.tournamentDetails?.checkInEndDate
      ) {
        return `${startDate.getDate()} ${startDate.toLocaleString('default', {
          month: 'long',
        })} ${startDate.getFullYear()}`;
      } else if (startDate.getFullYear() < endDate.getFullYear()) {
        return `${startDate.getDate()} ${startDate.toLocaleString('default', {
          month: 'long',
        })} ${startDate.getFullYear()} to ${endDate.getDate()} ${endDate.toLocaleString(
          'default',
          { month: 'long' }
        )} ${endDate.getFullYear()}`;
      } else if (startDate.getMonth() < endDate.getMonth()) {
        return `${startDate.getDate()} ${startDate.toLocaleString('default', {
          month: 'long',
        })} - ${endDate.getDate()} ${endDate.toLocaleString('default', {
          month: 'long',
        })} ${endDate.getFullYear()}`;
      } else if (startDate.getDate() < endDate.getDate()) {
        return `${startDate.getDate()} - ${endDate.getDate()} ${endDate.toLocaleString(
          'default',
          { month: 'long' }
        )} ${endDate.getFullYear()}`;
      } else {
        return 'N/A';
      }
    }
  }

  isNumber(val): boolean { const str: string = val.toString(); return str.startsWith("+"); }

  getStartTime() {
    return this.utilsService.convertStringIntoTime(
      this.tournamentDetails?.startDate,
      this.tournamentDetails?.startTime
    );
  }

  buttonResponse(data) {
    if (data) {
      this.fetchTournamentDetails(this.tournamentDetails?.slug);
    }
  }

  enableRating(data) {
    if (data?.isAuthorized) {
      // add field for ratings
    }
  }

  rateUs = async () => {
    try {
      const query = `?query=${encodeURIComponent(
        JSON.stringify({
          posterId: this.tournamentDetails._id,
          type: 'Tournament',
        })
      )}`;
      const rating = await this.ratingService.getRating(query);
      const rate = rating.data || {
        raterId: this.user?._id,
        posterId: this.tournamentDetails._id,
        value: 3,
        type: 'Tournament',
      };
      const blockUserData: InfoPopupComponentData = {
        title: 'Rate US',
        text: `Your feedback is valuable for us.`,
        type: InfoPopupComponentType.rating,
        btnText: 'Confirm',
        rate: rate.value,
      };

      const dialogRef = this.matDialog.open(InfoPopupComponent, {
        data: blockUserData,
      });

      const confirmed = await dialogRef.afterClosed().toPromise();

      if (confirmed) {
        rate.value = confirmed;
        const addOrUpdateRating = rate._id
          ? await this.ratingService.updateRating(rate._id, rate)
          : await this.ratingService.addRating(rate);
        this.toastService.showSuccess(addOrUpdateRating?.message);
      }
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  shareTournament = async () => {
    try {
      const data: InfoPopupComponentData = {
        title: this.translateService.instant('ARTICLE_POST.SHARE_TOURNAMENT'),
        text: ``,
        type: InfoPopupComponentType.socialSharing,
        cancelBtnText: 'Close',
      };

      const dialogRef = this.matDialog.open(InfoPopupComponent, {
        data,
      });

      await dialogRef.afterClosed().toPromise();
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  getCurrentUserDetails() {
    this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.user = data;
      }
    });
  }

  fetchParticipantRegisterationStatus = async (id: string) => {
    try {
      const {
        data,
      } = await this.tournamentService.fetchParticipantRegistrationStatus(id);
      this.enableComment = this.tournamentDetails?.organizerDetail?._id == this.user._id || !!data?.participantId;
      this.participantId = data?.participantId;
      if (!this.enableComment) {
        if (
          !['tournament-finished', 'tournament-started', 'no-action'].includes(
            data.type
          )
        ) {
          this.text = 'DISCUSSION.PARTICIPANT_NA';
        }
      }
      return data;
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };
}
