import {
  Component,
  OnInit,
  Input,
  OnChanges,
  SimpleChanges,
  ViewChild,
} from "@angular/core";
import { TournamentService } from "../../../../core/service";
import { ToastService } from "../../../../shared/toast/toast.service";

@Component({
  selector: "app-participants",
  templateUrl: "./participants.component.html",
  styleUrls: [
    "./participants.component.scss",
    "../view-tournament.component.scss",
  ],
})
export class ParticipantsComponent implements OnInit, OnChanges {
  @Input() tournamentId: string;
  @Input() tournament;
  approvedParticipants = [];
  isLoaded = false;
  selectedTeam = null;
  currentTab="approvedParticipants"

  constructor(
    private tournamentService: TournamentService,
    private toastService: ToastService
  ) {}

  ngOnInit(): void {}

  fetchAllApprovedParticipants = async () => {
    try {
      this.isLoaded = false;
      this.selectedTeam = false;
      const encodeUrl = `?query=${encodeURIComponent(
        JSON.stringify({
          tournamentId: this.tournamentId,
          participantStatus: "approved",
        })
      )}`;
      const participants = await this.tournamentService
        .getParticipants(encodeUrl)
        .toPromise();
      this.approvedParticipants = participants.data;
      this.setParticipant();
      this.isLoaded = true;
    } catch (error) {
      this.isLoaded = true;
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  ngOnChanges(simpleChanges: SimpleChanges) {
    if (
      simpleChanges.hasOwnProperty("tournamentId") &&
      simpleChanges.tournamentId.currentValue
    ) {
      this.fetchAllApprovedParticipants();
    }
  }

  setParticipant() {
    this.currentTab === 'approvedParticipants'? this.approvedParticipants : this.approvedParticipants = [] ;
  }
}
