import { Component, OnInit, Input, OnChanges, OnDestroy } from "@angular/core";
import {
  BracketService,
  TournamentService,
  UtilsService,
} from "../../../../core/service";
import { ITournament } from "../../../../shared/models/tournament";
import { ToastService } from "../../../../shared/toast/toast.service";

@Component({
  selector: "app-brackets",
  templateUrl: "./brackets.component.html",
  styleUrls: ["./brackets.component.scss", "../view-tournament.component.scss"],
})
export class BracketsComponent implements OnInit, OnChanges, OnDestroy {
  @Input() tournament: ITournament;
  @Input() participantId: string | null;
  apiLoaded: Array<boolean> = [];
  isLoaded = false;
  structure: any;

  constructor(
    private bracketService: BracketService,
    private toastService: ToastService,
    private utilsService: UtilsService,
    private tournamentService: TournamentService
  ) {}

  ngOnInit(): void {}

  ngOnDestroy() {}

  ngOnChanges() {
    if (this.tournament?._id) {
      this.fetchParticipantRegisterationStatus(this.tournament?._id);
      this.fetchBracket();
    }
  }

  fetchParticipantRegisterationStatus = async (id) => {
    try {
      this.apiLoaded.push(false);
      const {
        data,
      } = await this.tournamentService.fetchParticipantRegistrationStatus(id);
      this.participantId = data?.participantId || null;
      this.apiLoaded.push(true);
    } catch (error) {
      this.apiLoaded.push(true);
      this.toastService.showError(error?.error?.message || error?.message);
    } finally {
      this.checkAllApiDataLoaded();
    }
  };

  /**
   * Fetch All Match If Seed Else Fetch Mock Structure Of The Tournament
   */
  fetchBracket = async () => {
    try {
      this.apiLoaded.push(false);
      if (
        this.tournament?.isSeeded &&
        ["single", "double"].includes(this.tournament?.bracketType)
      ) {
        const queryParam = `?query=${this.utilsService.encodeQuery({
          tournamentId: this.tournament._id,
        })}`;
        const response = await this.bracketService.fetchAllMatches(queryParam);
        this.structure = this.bracketService.assembleStructure(response.data);
      } else if (!this.tournament?.isSeeded) {
        const payload = {
          bracketType: this.tournament?.bracketType,
          maximumParticipants: this.tournament?.maxParticipants,
          noOfSet: this.tournament?.noOfSet,
          ...(["round_robin", "battle_royale"].includes(
            this.tournament?.bracketType
          ) && {
            noOfTeamInGroup: this.tournament?.noOfTeamInGroup,
            noOfWinningTeamInGroup: this.tournament?.noOfWinningTeamInGroup,
            noOfRoundPerGroup: this.tournament?.noOfRoundPerGroup,
            stageBracketType: this.tournament?.stageBracketType,
          }),
        };
        const response = await this.bracketService.generateBracket(payload);
        this.structure = response.data;
      }
      this.apiLoaded.push(true);
    } catch (error) {
      this.apiLoaded.push(true);
      this.toastService.showError(error?.error?.message || error?.message);
    } finally {
      this.checkAllApiDataLoaded();
    }
  };

  checkAllApiDataLoaded() {
    const startedApiCount = this.apiLoaded.filter((el) => !el).length;
    const finishedApiCount = this.apiLoaded.filter((el) => el).length;
    this.isLoaded = startedApiCount === finishedApiCount;
  }
}
