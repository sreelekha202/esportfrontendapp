import { NgModule } from "@angular/core";
import { FaIconLibrary } from "@fortawesome/angular-fontawesome";
import { faTrash, faShareAlt } from "@fortawesome/free-solid-svg-icons";

import { SharedModule } from "../../../shared/modules/shared.module";
import { ViewTournamenttRoutingModule } from "./view-tournament-routing.module";

import { ViewTournamentComponent } from "./view-tournament.component";
import { JoinParticipantComponent } from "./join-participant/join-participant.component";
import { OverviewComponent } from "./overview/overview.component";
import { ParticipantsComponent } from "./participants/participants.component";
import { BracketsComponent } from "./brackets/brackets.component";
import { JoinButtonComponent } from "./join-button/join-button.component";
import { PaymentComponent } from "./join-participant/payment/payment.component";
import { InfoComponent } from "./info/info.component";
import { FormComponentModule } from "../../../shared/components/form-component/form-component.module";
import { PaymentModule } from "../../payment/payment.module";
import { CountdownModule } from "ngx-countdown";

@NgModule({
  declarations: [
    ViewTournamentComponent,
    JoinParticipantComponent,
    OverviewComponent,
    ParticipantsComponent,
    BracketsComponent,
    JoinButtonComponent,
    InfoComponent,
    PaymentComponent,
  ],
  imports: [
    ViewTournamenttRoutingModule,
    SharedModule,
    FormComponentModule,
    PaymentModule,
    CountdownModule,
  ],

  providers: [],
})
export class ViewTournamentModule {
  constructor(private library: FaIconLibrary) {
    library.addIcons(faTrash, faShareAlt);
  }
}
