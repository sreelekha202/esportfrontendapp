import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { JoinParticipantComponent } from "./join-participant.component";

describe("JoinParticipantComponent", () => {
  let component: JoinParticipantComponent;
  let fixture: ComponentFixture<JoinParticipantComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [JoinParticipantComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JoinParticipantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
