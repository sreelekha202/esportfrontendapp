import { Component, OnInit, OnDestroy } from "@angular/core";
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormArray,
  FormControl,
} from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";
import {
  LanguageService,
  TournamentService,
  UserService,
  TransactionService,
} from "../../../../core/service";
import { ToastService } from "../../../../shared/toast/toast.service";
import { TranslateService } from "@ngx-translate/core";
import { IUser } from "../../../../shared/models";
import { Subscription } from "rxjs";

@Component({
  selector: "app-join-participant",
  templateUrl: "./join-participant.component.html",
  styleUrls: [
    "./join-participant.component.scss",
    "../view-tournament.component.scss",
  ],
})
export class JoinParticipantComponent implements OnInit, OnDestroy {
  participantForm: FormGroup;
  user: IUser;
  tournamentDetails: any = {};
  isProcessing = false;
  isLoaded = false;
  showParticipantForm = false;
  participantRS: string | null;
  teamData: any = {};
  timeoutId = null;
  // logo config
  logoDimension = { width: 180, height: 180 };
  logoSize = 1000 * 1000 * 5;
  showGameAccountIdFormat = true;
  gameAccountIdFormatresponse;
  paymentMethods = [
    {
      img: "../assets/images/payment/paypal.png",
      name: "Paypal",
      value: "paypal",
    },
  ];
  selectPaymentType = "";
  isProccesing = true;
  transaction = {
    totalAmount: "10",
    currencyCode: "USD",
  };
  isPaidTournament = false;

  userSubscription: Subscription;
  participantJoinStatus;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private tournamentService: TournamentService,
    private toastService: ToastService,
    private userService: UserService,
    private translateService: TranslateService,
    private languageService: LanguageService,
    private transactionService: TransactionService
  ) { }

  ngOnInit(): void {
    this.getCurrentUserDetails();
    this.tournamentService.joinDetail.subscribe((data) => {
      this.teamData = data;
    });
  }

  getPaymentResponse = async (order) => {
    try {
      const payload = {
        tournamentId: this.tournamentDetails?._id,
        txnId: order?.id,
        provider: "paypal",
      };
      this.submit(payload);
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
      this.isProccesing = false;
    }
  };

  getPaymentProgressStatus = (status) => {
    this.tournamentService.isTournamentPaymentProcessing = status;
  };

  ngOnDestroy() {
    this.userSubscription?.unsubscribe();
  }

  /**
   * Fetch Some tournament Details
   * @param id tournamentID
   */
  fetchTournamentDetails = async (slug) => {
    try {
      this.isLoaded = false;
      const query = JSON.stringify({ slug });
      const select =
        "&select=_id,name,logo,banner,participantType,teamSize,substituteMemberSize,allowSubstituteMember,slug,isPrize,isPaid,regFeeCurrency,regFee";

      /*
      const tournament = await this.tournamentService
        .getTournaments({ query }, select)
        .toPromise();
      this.tournamentDetails = tournament?.data?.length
        ? tournament.data[0]
        : null;
      */
        const tournament = await this.tournamentService
        .getTournamentBySlug(slug)
        .toPromise();

        this.tournamentDetails = tournament?.data ? tournament.data : null;

      if (this.tournamentDetails.isPaid) this.isPaidTournament = true;
      if (this.tournamentDetails.regFeeCurrency == "EURO") {
        this.tournamentDetails.regFeeCurrency = "EUR";
      }
      this.transaction.currencyCode = this.tournamentDetails.regFeeCurrency;
      this.transaction.totalAmount = this.tournamentDetails.regFee;

      // if (this.tournamentDetails?.isPrize) {
      //   if (this.user && !this.user?.accountDetail?.isPaymentAccountVerified) {
      //     this.toastService.showInfo(
      //       this.translateService.instant(
      //         "VIEW_TOURNAMENT.JOIN_PARTICIPANT.ACCOUNT"
      //       )
      //     );
      //     this.navigate();
      //     return;
      //   }
      // }

      if (!this.tournamentDetails) {
        throw new Error(
          this.translateService.instant(
            "VIEW_TOURNAMENT.JOIN_PARTICIPANT.INVALID_TOURNAMENT"
          )
        );
      }
      const participantId = this.activatedRoute?.snapshot?.params?.pId;
      const {
        data,
      } = await this.tournamentService.fetchParticipantRegistrationStatus(
        this.tournamentDetails?._id,
        participantId
      );
      this.participantRS = data?.type;
      this.showParticipantForm = [
        "join",
        "already-registered",
        "checked-in",
        "already-checked-in",
      ].includes(this.participantRS);

      this.createForm();
      if (
        this.tournamentDetails?.participantType &&
        this.tournamentDetails?.participantType.toLowerCase() === "team"
      ) {
        for (let i = 0; i < this.tournamentDetails?.teamSize - 1 || 0; i++) {
          this.addMember("teamMembers");
        }
        if (this.tournamentDetails?.allowSubstituteMember) {
          for (
            let i = 0;
            i < this.tournamentDetails?.substituteMemberSize || 0;
            i++
          ) {
            this.addMember("substituteMembers");
          }
        }
      }
      this.fetchParticipant(data?.participantId);
      this.isLoaded = true;
    } catch (error) {
      this.isLoaded = true;
      this.toastService.showError(error?.statusText || error?.message);
    }
  };

  /**
   * Create Form
   */
  createForm() {
    this.participantForm = this.fb.group({
      logo: ["", Validators.required],
      teamId: [""],
      teamName: [
        "",
        Validators.compose([
          Validators.required,
          this.emptyCheckAndRequestProccess,
        ]),
      ],
      name: ["", Validators.compose([Validators.required, this.isEmptyCheck])],
      email: [""],
      inGamerUserId: [
        "",
        Validators.compose([
          Validators.required,
          this.emptyCheckAndRequestProccess,
        ]),
      ],
      teamMembers: this.fb.array([]),
      substituteMembers: this.fb.array([]),
    });
  }

  /**
   * Empty validation
   * @param control formControl
   */
  isEmptyCheck = (control: FormControl) => {
    return control.value && control.value.trim() ? null : { required: true };
  };

  emptyCheckAndRequestProccess = (control: FormControl) => {
    return control.value && control.value.trim() ? null : { required: true };
  };

  /** Add Member For Team */
  addMember(field): void {
    const teamMembers = this.participantForm.get(field) as FormArray;
    const createMemberForm = (): FormGroup => {
      return this.fb.group({
        teamParticipantId: [],
        userId: [],
        name: [
          "",
          Validators.compose([Validators.required, this.isEmptyCheck]),
        ],
        email: [
          "",
          Validators.compose([Validators.required, Validators.email]),
        ],
        inGamerUserId: [
          "",
          Validators.compose([
            Validators.required,
            this.emptyCheckAndRequestProccess,
          ]),
        ],
      });
    };
    teamMembers.push(createMemberForm());
  }

  /**
   * Get Unique Team Name, tournamentUsername and GameUserId Before Registeration
   * @param name text name
   * @param field field name
   * @param index member index
   */

  isUniqueName = async (name, field, index = null, arrayField = null) => {
    try {
      const checkUniqueNameInForm = async () => {
        const value = this.participantForm.value;
        const matchArray = [];
        matchArray.push(value[field].trim() === name.trim());
        value.teamMembers.forEach((el) => {
          matchArray.push(el[field] && el[field].trim() === name.trim());
        });
        value.substituteMembers.forEach((el) => {
          matchArray.push(el[field] && el[field].trim() === name.trim());
        });
        return matchArray.filter((el) => el).length >= 2;
      };

      const isAvailable = async () => {
        try {
          const response = await this.tournamentService
            .searchParticipant(
              field,
              name,
              this.tournamentDetails?._id,
              this.participantForm.value?.id
            )
            .toPromise();
          const isNameAvailable =
            response.data.isExist || (await checkUniqueNameInForm());
          if (typeof index === "number") {
            const array = this.participantForm.get(arrayField) as FormArray;
            const value = array.at(index).get(field).value.trim();
            array
              .at(index)
              .get(field)
              .setErrors(
                !value
                  ? { required: true }
                  : isNameAvailable
                    ? { isNameAvailable }
                    : response.data.invalidId
                      ? { invalidId: true }
                      : null
              );
          } else {
            const value = this.participantForm.get(field).value.trim();
            this.participantForm
              .get(field)
              .setErrors(
                !value
                  ? { required: true }
                  : isNameAvailable
                    ? { isNameAvailable }
                    : response.data.invalidId
                      ? { invalidId: true }
                      : null
              );
            if (
              this.tournamentDetails?.participantType === "individual" &&
              field === "inGamerUserId"
            ) {
              this.participantForm.get("teamName").setValue(value);
              this.participantForm.get("teamName").clearValidators();
              this.participantForm.get("teamName").updateValueAndValidity();
            }
          }
          this.participantForm.updateValueAndValidity();
        } catch (error) {
          throw error;
        } finally {
          clearTimeout(this.timeoutId);
        }
      };

      if (typeof index === "number") {
        const array = this.participantForm.get(arrayField) as FormArray;
        array.at(index).get(field).setErrors({ wait: true });
      } else {
        this.participantForm.get(field).setErrors({ wait: true });
      }

      if (!this.timeoutId) {
        this.timeoutId = setTimeout(isAvailable, 500);
      } else {
        clearTimeout(this.timeoutId);
        this.timeoutId = setTimeout(isAvailable, 500);
      }
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  buildQuery = async (field, text) => {
    switch (field) {
      case "teamName":
        return {
          teamName: text,
          tournamentId: this.tournamentDetails?._id,
        };
      default:
        return {
          $or: [
            { [field]: text },
            { teamMembers: { $elemMatch: { [field]: text } } },
          ],
          tournamentId: this.tournamentDetails?._id,
        };
    }
  };

  /**
   * Save Participant Details
   */
  submit = async (transaction = null) => {
    try {
      if (!this.isPaidTournament) {
        if (this.participantForm.invalid) {
          this.participantForm.markAllAsTouched();
          return;
        }
        if (this.participantForm.invalid && !this.participantForm.value?.id) {
          this.participantForm.markAllAsTouched();
          return;
        }
        if (!this.user) {
          this.toastService.showError("Please login");
          return;
        }
      }
      this.isProcessing = true;
      this.isLoaded = true;
      const { value } = this.participantForm;
      value.participantType = this.tournamentDetails.participantType;
      value.tournamentId = this.tournamentDetails?._id;
      value.gameName = this.tournamentDetails.gameDetail.name;
      if (this.isPaidTournament) {
        value.transaction = transaction;
        value.regFeePaid = transaction.regFeePaid;
      }
      const response = value?.id
        ? await this.tournamentService
          .updateParticipant(value?.id, value)
          .toPromise()
        : await this.tournamentService.saveParticipant(value).toPromise();
      if (response?.messageCode == "PRT_S001" ||
        response?.messageCode == "PRT_S004") {
        this.userService.refreshCurrentUser();
        this.toastService.showSuccess(response?.message);
        this.isProcessing = false;
        this.isLoaded = false;
        this.participantForm.reset();
        this.navigate();
      } else {
        this.isProcessing = false;
        this.showGameAccountIdFormat = true;
        this.gameAccountIdFormatresponse = response?.data?.data?.format;
        this.toastService.showError(response.message);
        // this.router.navigate(["profile/my-tournament/joined"]);
      }
    } catch (error) {
      this.isProcessing = false;
      this.isLoaded = false;
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  validator = async (value) => {
    try {
      if (this.participantForm.invalid) {
        this.participantForm.markAllAsTouched();
        return;
      }
      if (!this.user) {
        this.toastService.showError("Please login");
        return;
      }
      // this.selectPaymentType = value;
      this.selectPaymentType = "paypal";
    } catch (error) {
      this.isProcessing = false;
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  /**
   * Get Current User
   */
  getCurrentUserDetails() {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.user = data;
        const pId = this.activatedRoute.snapshot.params.pId;
        if (pId) {
          //this.tournamentDetails.slug = this.router.url.split("/").reverse()[2];
          this.tournamentDetails.slug = this.activatedRoute.snapshot.params.id;
          this.fetchTournamentDetails(this.tournamentDetails?.slug);
        } else {
          //this.tournamentDetails.slug = this.router.url.split("/").reverse()[1];
          this.tournamentDetails.slug = this.activatedRoute.snapshot.params.id;
          this.fetchTournamentDetails(this.tournamentDetails?.slug);
        }
        const tournamentId = this.tournamentDetails.slug
          .split("-")
          .reverse()[0];
        this.fetchParticipantRegistrationStatus(tournamentId);
      }
    });
  }

  navigate() {
    const pId = this.activatedRoute?.snapshot?.params?.pId;
    const joinEdit = this.activatedRoute?.snapshot?.queryParams?.success;

    if (pId) {
      this.router.navigate([
        "/tournament/manage/" + this.tournamentDetails?.slug,
      ]);
    } else if (joinEdit) {
      this.router.navigate(["profile/my-tournament/joined"]);
    } else {
      this.router.navigate(["tournament/" + this.tournamentDetails?.slug]);
    }
  }

  patchFormValues = () => {
    // populate user game id only for matching game
    const userGames = this.user?.preference?.gameDetails || [];
    const game = userGames.find((game) => {
      return game._id === this.tournamentDetails?.gameDetail?._id;
    });
    if (this.participantForm && this.user) {
      const formValue = {
        name: this.user?.fullName,
        phoneNumber: this.user?.phoneisVerified ? this.user?.phoneNumber : "",
        email: this.user?.emailisVerified ? this.user?.email : "",
        logo: this.user?.profilePicture || "",
        inGamerUserId: game?.userGameId || "",
      };

      this.participantForm.patchValue({
        ...formValue,
      });
      if (game?.userGameId) {
        this.isUniqueName(game?.userGameId, "inGamerUserId");
      }
      if (!this.user.emailisVerified) {
        this.participantForm
          .get("email")
          .setValidators([Validators.required, Validators.email]);
        this.participantForm.get("email").updateValueAndValidity();
      }
    }
  };

  fetchParticipant = async (pid) => {
    try {
      if (pid) {
        const query = {
          _id: pid,
        };
        const encodeUrl = `?query=${encodeURIComponent(JSON.stringify(query))}`;
        const participant = await this.tournamentService
          .getParticipants(encodeUrl)
          .toPromise();
        if (participant.data.length) {
          this.participantForm.addControl(
            "id",
            new FormControl(participant.data[0]._id)
          );
          this.participantForm.patchValue({
            ...participant.data[0],
          });
        }
      } else {
        if (this.tournamentDetails.participantType == "team") {
          if (this.teamData) {
            this.participantForm.patchValue({
              ...this.teamData,
            });
          } else {
            this.router.navigate([
              "team-registration",

              this.tournamentDetails.slug,
            ]);
          }
        } else {
          this.patchFormValues();
        }
      }
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };
  patchValueForm() {
    this.participantForm.patchValue({
      ...this.teamData,
    });
  }
  fetchParticipantRegistrationStatus = async (id: string) => {
    try {
      const {
        data,
      } = await this.tournamentService.fetchParticipantRegistrationStatus(id);
      this.participantJoinStatus = data?.type;
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };
}
