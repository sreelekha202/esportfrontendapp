import { Component, OnInit, Input } from '@angular/core';
import { LanguageService } from '../../../../core/service';

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.scss', '../view-tournament.component.scss'],
})
export class OverviewComponent implements OnInit {
  @Input() tournamentDetails;

  information = [];
  rules = [];

  constructor(public languageService: LanguageService) {}

  ngOnInit(): void {
    // MOCK DATA
    this.information = [
      { text: 'Ut enim ad minima veniam quis nostrum' },
      { text: 'Exercitationem ullam corporis suscipit laboriosam' },
      { text: 'Nisi ut aliquid ex ea commodi consequatur ' },
      { text: 'Quis autem vel eum iure reprehenderit qui in ea ' },
      { text: 'Voluptate velit esse quam nihil molestiae consequatur' },
    ];

    // MOCK DATA
    this.rules = [
      { text: 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.' },
      { text: 'Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur.' },
    ];
  }
}




