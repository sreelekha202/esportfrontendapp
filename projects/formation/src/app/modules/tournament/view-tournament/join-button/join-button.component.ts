import {
  Component,
  OnInit,
  Input,
  OnChanges,
  Output,
  EventEmitter,
} from "@angular/core";
import { TournamentService } from "../../../../core/service";
import { ToastService } from "../../../../shared/toast/toast.service";
import { ITournament, IUser } from "../../../../shared/models";
import { Router } from "@angular/router";
import { TranslateService } from "@ngx-translate/core";
import { AppHtmlTeamRegRoutes } from "../../../../app-routing.model";
import {
  InfoPopupComponent,
  InfoPopupComponentData,
  InfoPopupComponentType,
} from "../../../../shared/popups/info-popup/info-popup.component";
import { MatDialog } from "@angular/material/dialog";

@Component({
  selector: "app-join-button",
  templateUrl: "./join-button.component.html",
  styleUrls: [
    "./join-button.component.scss",
    "../view-tournament.component.scss",
  ],
})
export class JoinButtonComponent implements OnInit, OnChanges {
  @Input() tournament: ITournament;
  @Input() user: IUser;
  @Output() isRefreshTournament = new EventEmitter<boolean>(false);
  @Output() isAuthorizedUser = new EventEmitter<any>();
  AppHtmlTeamRegRoutes = AppHtmlTeamRegRoutes;
  btnType: string = "";
  participantId: string | null;
  showRegTime;
  registrationEndedTxt;
  isRegistrationClosed = true;
  restrationStartDate: Date;
  remainingTime;
  constructor(
    private tournamentService: TournamentService,
    private translateService: TranslateService,
    private toastService: ToastService,
    private router: Router,
    public dialog: MatDialog
  ) {}

  ngOnInit(): void {}

  ngOnChanges() {
    if (this.tournament) {
      this.fetchParticipantRegisterationStatus(this.tournament?._id);
    }
  }

  fetchParticipantRegisterationStatus = async (id) => {
    try {
      const {
        data,
      } = await this.tournamentService.fetchParticipantRegistrationStatus(id);
      this.btnType = data?.type;
      this.participantId = data?.participantId;
      if (data?.isRegistrationClosed !== undefined) {
        this.isRegistrationClosed = data?.isRegistrationClosed;
      }
      this.registrationEndedTxt = data?.regEnded;
      this.remainingTime = data?.remainingTimeForRegStart / 1000;
      this.showRegTime = this.remainingTime > 0 ? true : false;
      if (this.remainingTime < 0) {
        this.showRegTime = false;
      }
      if (this.btnType == "tournament-started" && this.participantId) {
        this.isAuthorizedUser.emit(true);
      }
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };
  onTimerFinished(e: Event) {
    if (e["action"] == "done") {
      Promise.resolve(() => {
        this.showRegTime = false;
      });
      this.fetchParticipantRegisterationStatus(this.tournament?._id);
      // this.ngOnChanges();
    }
  }
  participantCheckIn = async () => {
    try {
      const participant = await this.tournamentService
        .updateParticipant(this.participantId, { checkedIn: true })
        .toPromise();
      this.toastService.showSuccess(participant.message);
      this.isRefreshTournament.emit(true);
      this.ngOnChanges();
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  joinParticipant() {
    if (this.tournament?.isPrize) {
      // if (this.user && !this.user?.accountDetail?.isPaymentAccountVerified) {
      //   this.toastService.showInfo(
      //     this.translateService.instant(
      //       "VIEW_TOURNAMENT.JOIN_PARTICIPANT.ACCOUNT"
      //     )
      //   );
      //   return;
      // }
    }
    this.router.navigate([`/tournament/${this.tournament?.slug}/join`]);
  }

  joinTournament() {
    // if (this.shouldShowPaymentPopup()) {
    //   this.showUpdatePaymentPopup();
    //   return;
    // }
    this.router.navigate([`/tournament/${this.tournament?.slug}/join`]);
  }

  showUpdatePaymentPopup() {
    const afterShowPopup: InfoPopupComponentData = {
      title: this.translateService.instant(
        "VIEW_TOURNAMENT.MODAL.PAYMENT_METHOD.TITLE"
      ),
      text: this.translateService.instant(
        "VIEW_TOURNAMENT.MODAL.PAYMENT_METHOD.MESSAGE"
      ),
      type: InfoPopupComponentType.confirm,
      btnText: this.translateService.instant(
        "VIEW_TOURNAMENT.MODAL.PAYMENT_METHOD.BUTTON"
      ),
    };
    const dialogRef = this.dialog.open(InfoPopupComponent, {
      data: afterShowPopup,
    });
    dialogRef.afterClosed().subscribe((confirmed) => {
      if (confirmed) {
        this.router.navigate([`/profile/profile-setting`], {
          queryParams: { activeTab: "tab3" },
        });
      }
    });
  }

  shouldShowPaymentPopup() {
    if (this.tournament?.isPrize) {
      if (this.user && !this.user?.accountDetail?.isPaymentAccountVerified) {
        return true;
      }
    }
    return false;
  }
}
