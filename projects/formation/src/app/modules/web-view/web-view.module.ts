import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { SharedModule } from "../../shared/modules/shared.module";
import { PaymentModule } from "../payment/payment.module";
import { CustomStreamingModule } from "../../shared/components/custom-streaming/custom-streaming.module";

import { PaymentComponent } from "./payment/payment.component";
import { BracketComponent } from "./bracket/bracket.component";
import { CustomStreamingComponent } from "./custom-streaming/custom-streaming.component";
import { PaymentSuccessComponent } from "./payment-success/payment-success.component";
import { PaymentFailureComponent } from "./payment-failure/payment-failure.component";
import { MobilePrivacyPolicyComponent } from "./mobile-privacy-policy/mobile-privacy-policy.component";
import { GlobalUtils } from "../../shared/service/global-utils/global-utils";

declare const zE: any;

const routes: Routes = [
  {
    path: "",
    children: [
      { path: "bracket", component: BracketComponent },
      { path: "payment", component: PaymentComponent },
      { path: "custom-streaming", component: CustomStreamingComponent },
      { path: "payment-success", component: PaymentSuccessComponent },
      { path: "payment-failure", component: PaymentFailureComponent },
      { path: "privacy-policy", component: MobilePrivacyPolicyComponent },
    ],
  },
];

@NgModule({
  declarations: [
    PaymentComponent,
    BracketComponent,
    CustomStreamingComponent,
    PaymentSuccessComponent,
    PaymentFailureComponent,
    MobilePrivacyPolicyComponent,
  ],
  imports: [
    SharedModule,
    PaymentModule,
    CustomStreamingModule,
    RouterModule.forChild(routes),
  ],
})
export class WebViewModule {
  constructor() {
    this.loadZendesk();
  }

  loadZendesk() {
    try {
      if (GlobalUtils.isBrowser()) {
        zE("webWidget", "hide");
      }
    } catch (error) {}
  }
}
