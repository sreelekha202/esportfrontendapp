import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { CreateTeamRoutingModule } from "./create-team-routing.module";

import { CoreModule } from "../../core/core.module";
import { FormComponentModule } from "../../shared/components/form-component/form-component.module";
import { SharedModule } from "../../shared/modules/shared.module";

import { CreateTeamComponent } from "./create-team.component";
import { HeaderInfoModule } from "../../shared/components/header-info/header-info.module";
import { UploadImageModule } from "../../shared/components/upload-image/upload-image.module";

@NgModule({
  declarations: [CreateTeamComponent],
  imports: [
    CommonModule,
    CoreModule,
    CreateTeamRoutingModule,
    FormComponentModule,
    SharedModule,
    HeaderInfoModule,
    UploadImageModule,
  ],
})
export class CreateTeamModule {}
