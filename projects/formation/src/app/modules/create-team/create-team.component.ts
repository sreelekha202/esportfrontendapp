import { Component, OnDestroy, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { GlobalUtils } from "../../shared/service/global-utils/global-utils";
import { environment } from "../../../environments/environment";
import { Router, ActivatedRoute } from "@angular/router";
import { TranslateService } from "@ngx-translate/core";
import { Observable, Subscription } from "rxjs";
import {
  catchError,
  debounceTime,
  distinctUntilChanged,
  switchMap,
} from "rxjs/operators";
import { UserService } from "../../core/service";
import { ToastService } from "../../shared/toast/toast.service";
import { IUser } from "../../shared/models";
import { AppHtmlRoutes, AppHtmlProfileRoutes } from "../../app-routing.model";
import { Location } from "@angular/common";
@Component({
  selector: "app-create-team",
  templateUrl: "./create-team.component.html",
  styleUrls: ["./create-team.component.scss"],
})
export class CreateTeamComponent implements OnInit, OnDestroy {
  AppHtmlRoutes = AppHtmlRoutes;
  AppHtmlProfileRoutes = AppHtmlProfileRoutes;
  form: FormGroup;
  currentUser: IUser;
  membersObj;
  emailInvitation = [];
  dropdown: any;
  txtValue: any;
  formatter = (result: any) => {
    return result;
  };
  get email(): any {
    return this.form.get("email");
  }
  hideDropdown: any;

  biographyLength = 250;

  mock_select = [
    {
      name: "facebook",
      placeholder: "Facebook url",
      value: "",
    },
    {
      name: "twitch",
      placeholder: "Twitch url",
      value: "",
    },
    {
      name: "instagram",
      placeholder: "Instagram url",
      value: "",
    },
    {
      name: "youtube",
      placeholder: "Youtube url",
      value: "",
    },
    {
      name: "discord",
      placeholder: "Discord url",
      value: "",
    },
    {
      name: "twitter",
      placeholder: "Twitter url",
      value: "",
    },
  ];
  isInEditMode: boolean = false;
  clicked: boolean = false;
  selectedSocial = [];
  teamData;
  teamId: string;
  userSubscription: Subscription;
  fallbackURL: string;
  constructor(
    private fb: FormBuilder,
    private userService: UserService,
    public toastService: ToastService,
    private translateService: TranslateService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private location: Location
  ) {}

  ngOnInit(): void {
    if (
      GlobalUtils.isBrowser() &&
      localStorage.getItem(environment.currentToken)
    ) {
      this.getUserData();
    } else {
      this.router.navigate(["/"]);
    }
    this.createTournamentForm();
    if (this.activatedRoute.snapshot.params["id"]) {
      this.isInEditMode = true;
      this.teamId = this.activatedRoute.snapshot.params["id"];
      this.fallbackURL = "/admin/team-management";
      this.getTeamData(this.activatedRoute.snapshot.params["id"]);
    }
  }
  getTeamData(id) {
    const query = `?query=${encodeURIComponent(JSON.stringify({ _id: id }))}`;
    this.userService.getTeamList(query).subscribe(
      (res) => {
        this.teamData = {
          teamLogo: res.data[0].logo,
          socialMedia: res.data[0]?.social,
          shortDescription: res.data[0].shortDescription,
          teamName: res.data[0].name,
          members: res.data[0].member,
        };
        var list = [];

        if (this.teamData?.socialMedia) {
          for (
            var i = 0;
            i < Object.keys(this.teamData?.socialMedia).length;
            i++
          ) {
            var item = {};
            item["name"] = Object.keys(this.teamData?.socialMedia)[i];
            item["placeholder"] =
              Object.keys(this.teamData?.socialMedia)[i] + " url";
            item["value"] = this.teamData?.socialMedia[
              Object.keys(this.teamData?.socialMedia)[i]
            ];

            let validatorFn = this.fb.control("", []);
            this.form.addControl(
              Object.keys(this.teamData?.socialMedia)[i],
              validatorFn
            );
            this.form.controls[
              Object.keys(this.teamData?.socialMedia)[i]
            ].setValue(
              this.teamData?.socialMedia[
                Object.keys(this.teamData?.socialMedia)[i]
              ]
            );
            this.form.updateValueAndValidity();

            list.push(item);
          }
        }

        this.selectedSocial = list;
        const indexm = this.teamData.members.findIndex(
          (x) => JSON.stringify(x._id) === JSON.stringify(this.currentUser?._id)
        );
        if (indexm > -1) {
          if (
            this.teamData.members[indexm].role != "player" ||
            this.currentUser.accountType == "admin"
          ) {
            this.form.patchValue(this.teamData);
          } else {
            this.toastService.showError(
              this.translateService.instant("PROFILE.TEAMS.ERRORS.ERROR6")
            );
            this.router.navigate(["/profile/teams"]);
          }
        } else if (this.currentUser.accountType == "admin") {
          this.form.patchValue(this.teamData);
        } else {
          this.toastService.showError(
            this.translateService.instant("PROFILE.TEAMS.ERRORS.ERROR6")
          );
          this.router.navigate(["/profile/teams"]);
        }
      },
      (err) => {}
    );
  }
  cancel() {
    this.location.back();
  }
  ngOnDestroy() {
    this.userSubscription?.unsubscribe();
  }

  getUserData() {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.currentUser = data;
      }
    });
  }

  filterFunction() {
    var input, filter, a, i;
    input = document.getElementById("myInput");
    filter = input.value.toUpperCase();
    this.dropdown = document.getElementById("myDropdown");
    a = this.dropdown.getElementsByTagName("a");
    for (i = 0; i < a.length; i++) {
      this.txtValue = a[i].textContent || a[i].innerText;
      if (this.txtValue.toUpperCase().indexOf(filter) > -1) {
        a[i].style.display = "";
      } else {
        a[i].style.display = "none";
      }
    }
  }

  getValue(value) {
    if (!this.isSocialAlreadyAdded(this.selectedSocial, value?.name)) {
      this.selectedSocial.push(value);
      let validatorFn = this.fb.control("", []);
      this.form.addControl(value.name, validatorFn);
      this.form.updateValueAndValidity();
    }
    this.hideDropdown = false;
  }
  removeSocial(i) {
    this.form.controls[
      this.selectedSocial[i].name
    ].reset()
    this.selectedSocial.splice(i, 1);
  }

  createTournamentForm() {
    this.form = this.fb.group({
      teamLogo: [""],
      teamName: ["", Validators.required],
      shortDescription: [""],
      members: [[], Validators.compose([])],
      socialMedia: [""],
      email: [""],
    });
  }

  createTeam() {
    const controls = this.form.controls;
    this.clicked = true;
    if (this.form.invalid) {
      Object.keys(controls).forEach((controlName) =>
        controls[controlName].markAsTouched()
      );
      return;
    }


    let social = {
      facebook: "",
      twitch: "",
      youtube: "",
      discord: "",
      twitter: "",
      instagram:""
    };
    let members = [];
    let that = this;
    this.mock_select.forEach(function (value) {
      if(that.form.value[value.name]!=null && that.form.value[value.name]){
        social[value.name] = that.form.value[value.name];
      }else{
        delete social[value.name]
      }
    });

    this.form.value.members.forEach(function (value) {
      members.push(value._id);
    });

    let payload = {
      logo: this.form.value.teamLogo,
      teamName: this.form.value.teamName,
      shortDescription: this.form.value.shortDescription,
      social: social,
      members: members,
      email: this.emailInvitation,
    };
    if (this.isInEditMode) {
      this.updateTeam(payload);
    } else {
      this.userService.create_team(payload).subscribe(
        (data) => {
          this.clicked = false;
          this.toastService.showSuccess(data?.message);
          this.cancel();
        },
        (error) => {
          this.clicked = false;
          this.toastService.showError(error?.error?.message);
        }
      );
    }
  }
  updateTeam(payload) {
    this.clicked = true;
    let teamData = {
      logo: payload.logo,
      teamName: payload.teamName,
      shortDescription: payload.shortDescription,
      social: payload.social,
    };
    const teamObj = {
      id: this.teamId,
      admin: true,
      member: this.form.value.members,
      userName: this.currentUser.fullName,
      email: payload.email,
      query: {
        condition: { _id: this.teamId },
        update: teamData,
      },
    };
    this.userService.team_update(teamObj).subscribe(
      (res) => {
        this.clicked = false;
        if (res.data) {
          this.toastService.showSuccess(res.message);
          this.cancel();
        } else {
          this.toastService.showError(res.message);
        }
      },
      (err) => {
        this.clicked = false;
        this.toastService.showError(err.error.message);
      }
    );
  }
  search = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      distinctUntilChanged(),
      switchMap((term) => {
        if (term) {
          return this.userService.searchUsers(term);
        } else {
          return [];
        }
      }),
      catchError(() => {
        this.toastService.showError(
          this.translateService.instant("TOURNAMENT.SEARCH_FAILED")
        );

        return [];
      })
    );

  onChangeMembers() {
    this.membersObj = "";
  }

  addMembers() {
    setTimeout(() => {
      if (this.membersObj && typeof this.membersObj == "object") {
        if (
          !this.isMembersAlreadyAdded(
            this.form.value.members,
            this.membersObj?._id
          )
        ) {
          if (this.membersObj._id != this.currentUser._id) {
            this.form.value.members.push(this.membersObj);
          } else {
            this.toastService.showError(
              this.translateService.instant("PROFILE.TEAMS.ERRORS.ERROR4")
            );
          }
        } else {
          this.toastService.showError(
            this.translateService.instant("PROFILE.TEAMS.ERRORS.ERROR3")
          );
        }
        this.membersObj = "";
      }
    }, 100);
  }

  isMembersAlreadyAdded(arr, _id) {
    const { length } = arr;
    const id = length + 1;
    const found = arr.some((el) => el?._id === _id);
    return found;
  }

  isSocialAlreadyAdded(arr, name) {
    const { length } = arr;
    const id = length + 1;
    const found = arr.some((el) => el?.name === name);
    return found;
  }

  removeMember(index) {
    // if (this.isInEditMode) {
    //   if (this.form.value.members[index].role == 'owner') {
    //     this.toastService.showError(
    //       this.translateService.instant('PROFILE.TEAMS.ERRORS.ERROR2')
    //     );
    //   } else {
    //     this.form.value.members[index].status = 'deleted';
    //   }
    // } else {
    this.form.value.members.splice(index, 1);
    // }
  }

  validateEmail(email) {
    const regularExpression = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (!regularExpression.test(String(email).toLowerCase())) {
      return 0;
    }
    return 1;
  }
  addEmails() {
    if (this.validateEmail(this.form.value.email)) {
      if(this.emailInvitation.indexOf(this.form.value.email)==-1){
        this.emailInvitation.push(this.form.value.email);
      }else{
        this.toastService.showError(
          this.translateService.instant('PROFILE.TEAMS.ERRORS.ERROR10')
        );
      }
    } else {
      this.toastService.showError(
        this.translateService.instant('PROFILE.TEAMS.ERRORS.ERROR5')
      );
    }
    this.email.reset();
  }
  removeEmail(index) {
    this.emailInvitation.splice(index, 1);
  }
}
