import { Component, Input, OnInit } from '@angular/core';

import {
  GameService,
  HomeService,
  LeaderboardService,
  UserService,
} from '../../core/service';

import { AppHtmlRoutes } from '../../app-routing.model';
import { IPagination } from '../../shared/models';

export interface leaderboardComponentDataItem {
  page: string;
  title: string;
}
@Component({
  selector: 'app-leader-board',
  templateUrl: './leader-board.component.html',
  styleUrls: ['./leader-board.component.scss'],
})
export class LeaderBoardComponent implements OnInit {
  @Input() data: leaderboardComponentDataItem = {
    page: 'leaderboard',
    title: 'All Games',
  };

  AppHtmlRoutes = AppHtmlRoutes;

  goldIcon = 'assets/icons/gold.svg';
  silverIcon = 'assets/icons/silver.svg';
  bronzeIcon = 'assets/icons/bronze.svg';

  showLoader: boolean = true;

  gameId = ' ';
  selectedCountryName = '';
  selectedGameName = '';
  selectedGames = 'all';

  countryList = [];
  gameList = [];
  leaderboardData: any = [];

  selectedCountry: any = { all: true };
  selectedGame: any = { all: true };

  page: IPagination;

  paginationData = {
    page: 1,
    limit: 100,
    sort: 'startDate',
  };

  leaderBoardColumns = [
    { name: 'RANK' },
    { name: 'Player Name' },
    { name: 'REGION' },
    { name: 'TROPHIES' },
    { name: 'POINTS' },
  ];

  constructor(
    private leaderboardService: LeaderboardService,
    private gameService: GameService,
    private userService: UserService,
    private homeService: HomeService
  ) {}

  ngOnInit(): void {
    this.getGames();
    this.getCountries();
    this.filterLeaderboard();
  }

  getGames() {
    this.gameService.getAllGames().subscribe(
      (res) => (this.gameList = res.data),
      (err) => {}
    );
  }

  getCountries() {
    this.userService.getAllCountries().subscribe((data) => {
      this.countryList = data.countries;
    });
  }

  changeTab(value, type) {
    switch (type) {
      case 'regions': {
        if (value != 'all') {
          const country = this.countryList.find((o) => o.id === value);
          this.selectedCountryName = country.name;
          this.filterLeaderboard();
        } else {
          this.selectedCountryName = '';
          this.filterLeaderboard();
        }
        break;
      }
      case 'games': {
        if (value != 'all') {
          const game = this.gameList.find((o) => o._id === value);
          this.selectedGameName = game.name;
          this.selectedGames = game._id;
          this.filterLeaderboard();
        } else {
          this.selectedGameName = value;
          this.selectedGames = 'all';
          this.filterLeaderboard();
        }
        break;
      }
    }
  }

  filterLeaderboard() {
    this.paginationData.page = 1;
    this.selectedGame = {};
    this.selectedGame[this.selectedGames] = true;
    this.showLoader = true;

    const params = {
      country: this.selectedCountryName,
      gameId: this.selectedGames,
      limit: this.paginationData.limit,
      page: this.paginationData.page,
      state: null,
    };

    if (this.data.page === 'home') {
      this.homeService._leaderBoard().subscribe((data) => {
        this.showLoader = false;
        this.leaderboardData = data.data.leaderboardData;

        if (this.leaderboardData) {
          this.leaderboardData.forEach((obj, index) => {
            obj.rank = index + 1;
          });
        }
      });
    } else {
      this.leaderboardService.getGameLeaderboard(params).subscribe(
        (res: any) => {
          this.showLoader = false;
          this.leaderboardData = [];

          if (res.data.docs) {
            let count = 0;
            let userData: any = {};

            for (const data of res.data.docs) {
              if (data.user.length > 0) {
                userData.id = data._id.user;
                userData.rank = count + 1;
                count++;
                userData.img =
                  data.user[0].profilePicture == ''
                    ? './assets/images/Profile/formation.svg'
                    : data.user[0].profilePicture;
                userData.name = data.user[0].fullName;
                userData.region = data.user[0].country
                  ? data.user[0].country
                  : '';
                userData.trophies = {};
                userData.trophies.first = data.firstPosition;
                userData.trophies.second = data.secondPosition;
                userData.trophies.third = data.thirdPosition;
                userData.points = data.points;
                userData.amount = data.amountPrice;
                userData.gamesCount = data.gamesCount;
                this.leaderboardData.push({ ...userData });
                userData = {};
              }
            }
          }
        },
        (err) => {
          this.showLoader = false;
        }
      );
    }
  }

  filterByGame(gameId) {
    this.paginationData.page = 1;
    this.selectedGame = {};
    this.selectedGame[gameId] = true;

    if (gameId === 'all') {
      this.gameId = 'all';
    } else {
      this.gameId = gameId;
    }

    this.showLoader = true;
    const params = {
      page: this.paginationData.page,
      limit: this.paginationData.limit,
      gameId: this.gameId,
    };
    if (this.gameId) {
      params['game'] = this.gameId;
    }
    this.leaderboardService.getGameLeaderboard(params).subscribe(
      (res: any) => {
        this.showLoader = false;
        this.leaderboardData = [];

        if (res.data.docs) {
          let count = 0;
          let userData: any = {};

          for (const data of res.data.docs) {
            if (data.user.length > 0) {
              userData.id = data._id.user;
              userData.rank = count + 1;
              count++;
              userData.img =
                data.user[0].profilePicture == ''
                  ? './assets/images/Profile/formation.svg'
                  : data.user[0].profilePicture;
              userData.name = data.user[0].fullName;
              userData.region = data.user[0].country
                ? data.user[0].country
                : '';
              userData.trophies = {};
              userData.trophies.first = data.firstPosition;
              userData.trophies.second = data.secondPosition;
              userData.trophies.third = data.thirdPosition;
              userData.points = data.points;
              userData.amount = data.amountPrice;
              userData.gamesCount = data.gamesCount;
              this.leaderboardData.push({ ...userData });
              userData = {};
            }
          }
        }
      },
      (err) => {
        this.showLoader = false;
      }
    );
  }
}
