import { Component, OnInit } from "@angular/core";

import { LanguageService } from "../../../core/service";
import { OptionService } from "./../../../core/service/option.service";
import { ToastService } from "../../../shared/toast/toast.service";
import {
  ArticleApiService,
  UserService,
  UtilsService,
  VideoLibraryService,
} from "../../../core/service";

import {
  AppHtmlRoutes,
  AppHtmlRoutesArticleMoreType,
} from "../../../app-routing.model";
import { IPagination, IUser } from "../../../shared/models";
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: "app-article-main",
  templateUrl: "./article-main.component.html",
  styleUrls: ["./article-main.component.scss"],
})
export class ArticleMainComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;
  AppHtmlRoutesArticleMoreType = AppHtmlRoutesArticleMoreType;

  showLoader: boolean = true;

  categoryList = [];
  hottestData = [];
  latestArticle = [];
  latestArticleData = [];
  latestNews = [];
  videoLibrary: Array<{}> = [];
  activeVideo = null;
  categoryId;
  user: IUser;
  trendingData = [];
  page: IPagination;
  paginationData = {
    limit: 20,
    page: 1,
    sort: "-views",
  };

  constructor(
    private articleService: ArticleApiService,
    private optionService: OptionService,
    private toastService: ToastService,
    private userService: UserService,
    private videoLibraryService: VideoLibraryService,
    public language: LanguageService,
    public translateService: TranslateService,
    public utilsService: UtilsService
  ) {}

  ngOnInit(): void {
    this.fetchOptions();
    this.fetchVideoLibrary();
    this.getArticleList();
    this.getCurrentUserDetails();
    this.getRecentPost();
  }

  fetchVideoLibrary = async () => {
    try {
      const pagination = {
        page: 1,
        limit: 5,
        sort: "-updatedOn",
        projection: [
          "_id",
          "slug",
          "title",
          "description",
          "youtubeUrl",
          "thumbnailUrl",
          "createdBy",
        ],
      };
      const query = `?pagination=${encodeURIComponent(
        JSON.stringify(pagination)
      )}`;

      const response = await this.videoLibraryService.fetchVideoLibrary(query);
      this.videoLibrary = await Promise.all(
        response?.data?.docs.map(async (item) => {
          return {
            ...item,
            youtubeUrl: await this.utilsService.getEmbbedUrl(item.youtubeUrl),
          };
        })
      );
      this.activeVideo = this.videoLibrary.length ? this.videoLibrary[0] : null;
    } catch (error) {
      this.toastService.showError(error.error?.message || error?.message);
    }
  };

  fetchOptions = async () => {
    const option = await Promise.all([this.optionService.fetchAllCategories()]);
    this.categoryList = option[0]?.data;

    for (const iterator of this.categoryList) {
      if (iterator.name == "News") {
        this.categoryId = iterator._id;
      }
    }

    this.getArticleNews(this.categoryId);
  };

  getArticleNews(categoryId) {
    const query = JSON.stringify({
      articleStatus: "publish",
      category: categoryId,
    });

    const pagination = JSON.stringify({ limit: 7, sort: { createdDate: -1 } });
    const prefernce = JSON.stringify({
      prefernce: this.user?.preference?.game
        ? this.user?.preference?.game.map((item) => {
            return { gameDetails: item };
          })
        : "",
    });

    this.articleService
      .getLatestArticle({ query, pagination, preference: prefernce })
      .subscribe(
        (res: any) => {
          this.latestNews = res["data"]["docs"].slice(0, 2);
        },
        (err) => {}
      );
  }

  getRecentPost() {
    const pagination = JSON.stringify({ limit: 4, sort: { createdDate: -1 } });
    const query = JSON.stringify({
      articleStatus: "publish",
    });

    const perfernce = JSON.stringify({
      prefernce: this.user?.preference?.game
        ? this.user?.preference?.game.map((item) => {
            return { gameDetails: item };
          })
        : "",
    });

    this.articleService
      .getLatestArticle({
        pagination: pagination,
        query: query,
        preference: perfernce,
      })
      .subscribe(
        (res) => {
          this.latestArticle = res["data"]["docs"];
          this.showLoader = false;
        },
        (err) => {
          this.showLoader = false;
        }
      );
  }

  getArticleList() {
    this.articleService.getArticleList().subscribe(
      (res) => {
        this.latestArticleData = res["data"] || [];
        for (let index = 0; index < this.latestArticleData.length; index++) {
          const element = this.latestArticleData[index];
          if (index == 0) {
            this.hottestData.push(element);
          } else if (index == 1 || index == 3) {
            this.trendingData.push(element);
          }
        }
        this.showLoader = false;
      },
      (err) => {
        this.showLoader = false;
      }
    );
  }

  getCurrentUserDetails() {
    this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.user = data;
      }
    });
  }
}
