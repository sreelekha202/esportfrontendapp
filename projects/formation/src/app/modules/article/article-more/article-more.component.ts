import { TranslateService } from "@ngx-translate/core";
import { Component, OnInit } from "@angular/core";

import { ArticleApiService, UserService } from "../../../core/service";
import { IPagination } from "../../../shared/models";
import { IUser } from "../../../shared/models";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-article-more",
  templateUrl: "./article-more.component.html",
  styleUrls: ["./article-more.component.scss"],
})
export class ArticleMoreComponent implements OnInit {
  user: IUser;
  paginationData = {
    page: 1,
    limit: 20,
    sort: "-views",
  };

  articles: any = [];
  dataLoaded = false;
  showLoader = true;
  page: IPagination;
  active;
  activeTab;

  constructor(
    private articleApiService: ArticleApiService,
    public translate: TranslateService,
    private userService: UserService,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.getCurrentUserDetails();
    this.activeTab = this.activatedRoute.snapshot.params["id"];
    if (this.activeTab == "trending-post") {
      this.active = 0;
      this.fetchArticles();
    }
    if (this.activeTab == "all-post") {
      this.active = 1;
      this.getRecentPost();
    }
  }

  pageChanged(page): void {
    this.paginationData.page = page;
    if (this.activeTab == "trending-post") {
      this.active = 0;
      this.fetchArticles();
    }
    if (this.activeTab == "all-post") {
      this.active = 1;
      this.getRecentPost();
    }
  }

  fetchArticles = async () => {
    this.showLoader = true;
    try {
      const date = new Date();
      date.setDate(date.getDate() - 30);
      const pagination = JSON.stringify(this.paginationData);
      const query = JSON.stringify({
        $and: [
          { articleStatus: "publish" },
          { createdDate: { $gte: date, $lt: new Date() } },
        ],
      });
      const prefernce = JSON.stringify({
        prefernce: this.user?.preference?.game
          ? this.user?.preference?.game.map((item) => {
              return { gameDetails: item };
            })
          : "",
      });
      this.articleApiService
        .getPaginatedArticles({ pagination, query, preference: prefernce })
        .subscribe(
          (res: any) => {
            this.showLoader = false;
            this.articles = res["data"]["docs"];
            this.dataLoaded = true;
            this.page = {
              totalItems: res?.data?.totalDocs,
              itemsPerPage: res?.data?.limit,
              maxSize: 5,
            };
          },
          (err) => {}
        );
    } catch (error) {
      this.showLoader = false;
    }
  };

  getCurrentUserDetails() {
    this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.user = data;
      }
    });
  }

  getRecentPost() {
    const pagination = JSON.stringify({
      page: this.paginationData.page,
      limit: 20,
      sort: { createdDate: -1 },
    });
    const query = JSON.stringify({
      articleStatus: "publish",
    });

    const perfernce = JSON.stringify({
      prefernce: this.user?.preference?.game
        ? this.user?.preference?.game.map((item) => {
            return { gameDetails: item };
          })
        : "",
    });

    this.articleApiService
      .getLatestArticle({
        pagination: pagination,
        query: query,
        preference: perfernce,
      })
      .subscribe(
        (res: any) => {
          this.articles = res["data"]["docs"];
          this.showLoader = false;
          this.page = {
            totalItems: res?.data?.totalDocs,
            itemsPerPage: res?.data?.limit,
            maxSize: 5,
          };
        },
        (err) => {
          this.showLoader = false;
        }
      );
  }
}
