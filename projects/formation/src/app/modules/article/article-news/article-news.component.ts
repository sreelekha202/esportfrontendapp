import { Component, OnInit } from "@angular/core";

import { ArticleApiService, UserService } from "../../../core/service";
import { IPagination, IUser } from "../../../shared/models";

@Component({
  selector: "app-article-news",
  templateUrl: "./article-news.component.html",
  styleUrls: ["./article-news.component.scss"],
})
export class ArticleNewsComponent implements OnInit {
  user: IUser;
  articles: any = [];
  paginationData = {
    page: 1,
    limit: 20,
    sort: { createdDate: -1 },
  };
  page: IPagination;
  constructor(
    private articleService: ArticleApiService,
    private userService: UserService
  ) {}

  ngOnInit(): void {
    this.fetchArticles();
  }

  pageChanged(page): void {
    this.paginationData.page = page;
    this.fetchArticles();
  }

  fetchArticles = async () => {
    try {
      const query = JSON.stringify({
        articleStatus: "publish",
      });
      const pagination = JSON.stringify(this.paginationData);
      const prefernce = JSON.stringify({
        prefernce: this.user?.preference?.game
          ? this.user?.preference?.game.map((item) => {
              return { gameDetails: item };
            })
          : "",
      });
      this.articleService
        .getLatestArticle({ query, pagination, preference: prefernce })
        .subscribe(
          (res: any) => {
            this.articles = res["data"]["docs"];
          },
          (err) => {}
        );
    } catch (error) {}
  };

  getCurrentUserDetails() {
    this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.user = data;
      }
    });
  }
}
