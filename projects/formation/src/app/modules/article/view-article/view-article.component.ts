import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";

import { ToastService } from "../../../shared/toast/toast.service";
import {
  ArticleApiService,
  LanguageService,
  LikeService,
  UserService,
} from "../../../core/service";

@Component({
  selector: "app-view-article",
  templateUrl: "./view-article.component.html",
  styleUrls: ["./view-article.component.scss"],
})
export class ViewArticleComponent implements OnInit {
  articleId: any;
  innerHeight: any;
  netHeight: any;
  offsetHeight: any;

  commentsArray = [];
  active = 1;
  userId;

  articleModel: any = { type: null };

  commentModel: any = {
    comment: "",
    userId: "",
    objectId: "",
    objectType: "article",
  };

  replyModel: any = {
    comment: "",
    userId: "",
    objectId: "",
    objectType: "comment",
  };

  constructor(
    private activatedRoute: ActivatedRoute,
    private articleApiService: ArticleApiService,
    private likeService: LikeService,
    private toastService: ToastService,
    private userService: UserService,
    public languageService: LanguageService
  ) {}

  ngOnInit(): void {
    this.getIdFromRoute();
    this.getArticle();
    this.getCurrentUserDetails();
  }

  getIdFromRoute() {
    this.activatedRoute.queryParams
      .filter((params) => params.id)
      .subscribe((params) => {
        this.articleId = params.id;
        this.commentModel.objectId = params.id;
      });
  }

  getArticle() {
    this.articleApiService.getArticleByID(this.articleId).subscribe(
      (data: any) => {
        this.articleModel = data.data[0];
        this.commentsArray = this.articleModel.comments.map((obj) => {
          return { _id: obj._id, isShowReplyButton: false };
        });
        this.getCommentsWithTypeAndCount();
      },
      (err) => {}
    );
  }

  saveComment() {
    if (!this.userId) {
      this.toastService.showError("Session Expired Please Login");
      return;
    }
    this.commentModel.userId = this.userId;
    this.articleApiService.saveComment(this.commentModel).subscribe(
      (res: any) => {
        this.commentModel.comment = "";
        this.getArticle();
      },
      (err) => {}
    );
  }

  saveReply(commentId, index) {
    if (!this.userId) {
      this.toastService.showError("Session Expired Please Login");
      return;
    }
    this.replyModel.userId = this.userId;
    this.replyModel.objectId = commentId;
    this.articleApiService.saveComment(this.replyModel).subscribe(
      (res) => {
        this.commentsArray[index]["isShowReplyButton"] = false;
        this.replyModel.objectId = "";
        this.replyModel.comment = "";
      },
      (err) => {}
    );
  }

  addBookmark() {
    if (!this.userId) {
      this.toastService.showError("Please Login");
      return;
    }
    this.articleApiService
      .addBookmark({ articleId: this.articleId, userId: this.userId })
      .subscribe(
        (res) => {
          this.toastService.showSuccess(res["message"]);
        },
        (err) => {}
      );
  }

  like(obj, objType) {
    if (!this.userId) {
      this.toastService.showError("Session Expired Please Login");
      return;
    }
    if (obj["type"] == null) {
      let likeObj = {
        objectId: obj["_id"],
        objectType: objType,
        type: 1,
        userId: this.userId,
        createdBy: this.userId,
      };
      this.likeService.saveLike(likeObj).subscribe(
        (res) => {
          this.toastService.showSuccess("Success");
          this.getCommentsWithTypeAndCount();
        },
        (err) => {}
      );
    } else {
      let likeObj = {};
      likeObj["type"] = obj["type"] == 1 ? 0 : 1;
      this.likeService.updateLike(likeObj, obj["likeId"]).subscribe(
        (res) => {
          this.toastService.showSuccess("Success");
          this.getCommentsWithTypeAndCount();
        },
        (err) => {}
      );
    }
  }

  isShowReplyButton(selectedIndex) {
    this.commentsArray.forEach((obj, index) => {
      if (index == selectedIndex) {
        this.commentsArray[index]["isShowReplyButton"] = !this.commentsArray[
          index
        ]["isShowReplyButton"];
      } else {
        this.commentsArray[index]["isShowReplyButton"] = false;
      }
    });
  }

  getCurrentUserDetails() {
    this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.userId = data["_id"];
      }
    });
  }

  getCommentsWithTypeAndCount() {
    this.likeService.getLike(this.articleId, this.userId).subscribe(
      (res) => {
        this.articleModel.comments = res["data"]["comments"];
        this.articleModel = Object.assign(
          this.articleModel,
          res["data"]["article"]
        );
      },
      (err) => {}
    );
  }
}
