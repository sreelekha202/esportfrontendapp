import { ActivatedRoute, Router } from "@angular/router";
import { Component, OnInit, Inject } from "@angular/core";
import { DOCUMENT } from "@angular/common";
import { Title } from "@angular/platform-browser";

import { ConstantsService } from "../../../core/service";
import { ToastService } from "../../../shared/toast/toast.service";

import {
  ArticleApiService,
  HomeService,
  LanguageService,
  UserPreferenceService,
  UserService,
  CommentService,
} from "../../../core/service";

import {
  InfoPopupComponent,
  InfoPopupComponentData,
  InfoPopupComponentType,
} from "../../../shared/popups/info-popup/info-popup.component";

import {
  AppHtmlRoutes,
  AppHtmlRoutesArticleMoreType,
} from "../../../app-routing.model";

import { CustomTranslatePipe } from "../../../shared/pipe/custom-translate.pipe";
import { GlobalUtils } from "../../../shared/service/global-utils/global-utils";
import { MatDialog } from "@angular/material/dialog";
import { toggleHeight } from "../../../animations";
import { TranslateService } from "@ngx-translate/core";
import { IArticle, IUser } from "../../../shared/models";
@Component({
  selector: "app-article-post",
  templateUrl: "./article-post.component.html",
  styleUrls: ["./article-post.component.scss"],
  animations: [toggleHeight],
  providers: [CustomTranslatePipe],
})
export class ArticlePostComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;
  AppHtmlRoutesArticleMoreType = AppHtmlRoutesArticleMoreType;
  article: IArticle;
  user: IUser;
  relatedArticles = [];
  showLoader = false;

  constructor(
    @Inject(DOCUMENT) private document: Document,
    private activatedRoute: ActivatedRoute,
    private articleApiService: ArticleApiService,
    private customTranslatePipe: CustomTranslatePipe,
    private globalUtils: GlobalUtils,
    private homeService: HomeService,
    private router: Router,
    private titleService: Title,
    private toastService: ToastService,
    private translateService: TranslateService,
    private userService: UserService,
    public languageService: LanguageService,
    public matDialog: MatDialog,
    public userPreferenceService: UserPreferenceService,
    private commentService: CommentService
  ) {}

  ngOnInit(): void {
    this.activatedRoute.params.subscribe((params) => {
      if (params?.id) {
        this.getArticleBySlug(params?.id);
      }
    });
    this.getCurrentUserDetails();
  }

  getArticleBySlug = async (slug) => {
    try {
      this.showLoader = true;

      const article: any = await this.articleApiService.getArticleBySlug(slug);
      this.article = article.data;

      if (!this.article) this.router.navigate(["/404"]);

      this.showLoader = false;
      this.setMetaTags();
      this.updateView();
      this.trendingPost();
      this.showLoader = false;
    } catch (error) {
      this.showLoader = false;
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  getCurrentUserDetails() {
    this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.user = data;
      }
    });
  }

  setMetaTags() {
    this.titleService.setTitle(
      this.customTranslatePipe.transform(this.article?.title)
    );
    if (this.article?.image) {
      this.globalUtils.setMetaTags([
        {
          property: "twitter:image",
          content: this.article.image,
        },
        {
          property: "og:image",
          content: this.article.image,
        },
        {
          property: "og:image:secure_url",
          content: this.article.image,
        },
        {
          property: "og:image:url",
          content: this.article.image,
        },
        {
          property: "og:image:width",
          content: "1200",
        },
        {
          property: "og:image:height",
          content: "630",
        },
        {
          name: "description",
          content: this.customTranslatePipe.transform(
            this.article?.shortDescription
          ),
        },
        {
          name: "title",
          content: this.customTranslatePipe.transform(this.article?.title),
        },
        {
          property: "og:description",
          content: this.customTranslatePipe.transform(
            this.article?.shortDescription
          ),
        },
        {
          property: "twitter:description",
          content: this.customTranslatePipe.transform(
            this.article?.shortDescription
          ),
        },
        {
          property: "og:title",
          content: this.customTranslatePipe.transform(this.article?.title),
        },
        {
          property: "twitter:title",
          content: this.customTranslatePipe.transform(this.article?.title),
        },
        {
          property: "og:url",
          content:
            this.document.location.protocol +
            "//" +
            this.document.location.hostname +
            this.router.url,
        },
      ]);
    }
  }

  updateView = async () => {
    const payload = {
      _id: this.article?._id,
      modalName: "article",
    };
    await this.homeService.updateView(payload);
  };

  trendingPost = async () => {
    this.showLoader = true;
    try {
      const date = new Date();
      date.setDate(date.getDate() - 30);
      const pagination = JSON.stringify({ limit: 8, sort: { views: -1 } });
      const query = JSON.stringify({
        $and: [
          { articleStatus: "publish" },
          { createdDate: { $gte: date, $lt: new Date() } },
        ],
      });
      const prefernce = JSON.stringify({
        prefernce: this.user?.preference?.game
          ? this.user?.preference?.game.map((item) => {
              return { gameDetails: item };
            })
          : "",
      });
      this.articleApiService
        .getPaginatedArticles({ pagination, query, preference: prefernce })
        .subscribe(
          (res: any) => {
            this.showLoader = false;
            this.relatedArticles = res["data"]["docs"];
          },
          (err) => {}
        );
    } catch (error) {
      this.showLoader = false;
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  upsertBookmark = async () => {
    try {
      if (!this.user?._id) {
        this.toastService.showInfo(
          this.translateService.instant("ARTICLE.BOOKMARK_ALERT")
        );
        return;
      }

      if (this.article?.bookmarkProccesing) return;

      // Bookmark field
      const removeBookmark = `articleId=${this.article?._id}`;
      const addBookmark = { articleId: this.article._id };
      this.article.bookmarkProccesing = true;

      const bookmark: any = this.article?.isBookmarked
        ? await this.userPreferenceService
            .removeBookmark(removeBookmark)
            .toPromise()
        : await this.userPreferenceService.addBookmark(addBookmark).toPromise();

      this.article = {
        ...this.article,
        ...(bookmark?.data?.nModified == 1 && {
          isBookmarked: !this.article?.isBookmarked,
        }),
      };
      this.toastService.showSuccess(bookmark?.message);
      this.article.bookmarkProccesing = false;
    } catch (error) {
      this.article.bookmarkProccesing = false;
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  openSocialShareComponent() {
    const data: InfoPopupComponentData = {
      title: this.translateService.instant("ARTICLE_POST.SHARE"),
      text: ``,
      type: InfoPopupComponentType.socialSharing,
      cancelBtnText: "Close",
    };

    this.matDialog.open(InfoPopupComponent, {
      data,
    });
  }

  likeOrUnLikeArticle = async (
    objectId: string,
    objectType: string,
    type: number
  ) => {
    try {
      if (!this.user?._id) {
        this.toastService.showInfo(
          this.translateService.instant("ARTICLE.LIKE_ALERT")
        );
        return;
      }

      if (this.article.likeProccesing) return;

      this.article.likeProccesing = true;
      const payload = {
        objectId,
        objectType,
        type: type == -1 ? 1 : Number(!type),
      };

      const like = await this.commentService.upsertLike(payload);
      this.article = { ...this.article, ...like.data };
      this.article.likeProccesing = false;
    } catch (error) {
      this.article.likeProccesing = false;
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };
}
