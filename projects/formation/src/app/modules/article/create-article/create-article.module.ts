import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { AuthorAccessGuard } from "../../../shared/guard/isAuthorInfluencer.guard";

import { SharedModule } from "../../../shared/modules/shared.module";
import { PipeModule } from "../../../shared/pipe/pipe.module";

import { CoreModule } from "../../../core/core.module";
import { CreateArticleComponent } from "./create-article.component";

import { PaginationModule, PaginationConfig } from "ngx-bootstrap/pagination";
import { ShareModule } from "ngx-sharebuttons";
// import { NgxSummernoteModule } from "ngx-summernote";

const routes: Routes = [
  {
    path: "",
    canActivate: [AuthorAccessGuard],
    component: CreateArticleComponent,
  },
];

@NgModule({
  declarations: [CreateArticleComponent],
  imports: [
    // NgxSummernoteModule,
    CoreModule,
    PaginationModule.forRoot(),
    PipeModule,
    RouterModule.forChild(routes),
    SharedModule,
    ShareModule,
  ],
  providers: [{ provide: PaginationConfig }],
})
export class CreateArticleModule {}
