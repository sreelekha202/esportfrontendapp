import { AfterViewInit, Component, OnDestroy, OnInit } from "@angular/core";
import { faBell } from "@fortawesome/free-solid-svg-icons";
import { MatSidenav } from "@angular/material/sidenav";
import { Router } from "@angular/router";

import { SidenavService } from "../../../shared/service/sidenav/sidenav.service";
import {
  AppHtmlAdminRoutes,
  AppHtmlRoutesLoginType,
  AppHtmlRoutes,
  AppHtmlProfileRoutes,
} from "../../../app-routing.model";

import {
  UserService,
  LanguageService,
  ConstantsService,
} from "../../../core/service";
import { IUser } from "../../../shared/models/user";
import { TranslateService } from "@ngx-translate/core";
import { GlobalUtils } from "../../../shared/service/global-utils/global-utils";
import { Subscription } from "rxjs";
enum AppLanguage {
  ms = "ms",
  en = "en",
}
@Component({
  selector: "app-admin-header",
  templateUrl: "./admin-header.component.html",
  styleUrls: ["./admin-header.component.scss"],
})
export class AdminHeaderComponent implements OnInit, AfterViewInit, OnDestroy {
  AppHtmlAdminRoutes = AppHtmlAdminRoutes;
  faBell = faBell;
  isMenuOpened = false;
  currentUser: IUser;

  AppLanguage = [];
  activeLang = ConstantsService?.defaultLangCode;

  AppHtmlRoutesLoginType = AppHtmlRoutesLoginType;
  AppHtmlRoutes = AppHtmlRoutes;
  AppHtmlProfileRoutes = AppHtmlProfileRoutes;

  userSubscription: Subscription;

  constructor(
    private sidenavService: SidenavService,
    private userService: UserService,
    private router: Router,
    public translate: TranslateService,
    private languageService: LanguageService
  ) {}

  ngOnInit(): void {
    this.AppLanguage = ConstantsService?.language;
    this.activeLang = this.translate.currentLang;
    this.languageService.setLanguage(this.activeLang);
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.currentUser = data;
      }
    });
  }

  ngOnDestroy() {
    this.userSubscription?.unsubscribe();
  }

  onLanguageChange(lang: AppLanguage): void {
    if (GlobalUtils.isBrowser()) {
      localStorage.setItem("currentLanguage", lang);
      this.activeLang = lang;
      this.translate.use(lang);
      this.languageService.setLanguage(lang);
    }
  }

  async ngAfterViewInit(): Promise<void> {
    const sidenav: MatSidenav = await this.sidenavService.getSidenav();

    if (sidenav) {
      sidenav.openedChange.subscribe((isOpened) => {
        this.isMenuOpened = isOpened;
      });
    }
  }

  onLogOut(): void {
    this.userService.logout();
    this.router.navigate([
      AppHtmlRoutes.userPageType,
      AppHtmlRoutesLoginType.emailLogin,
    ]);
  }

  onMenuToggle(): void {
    this.sidenavService.toggle();
  }
}
