import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { PrizeMoneyRefundComponent } from "./prize-money-refund.component";

describe("PrizeMoneyRefundComponent", () => {
  let component: PrizeMoneyRefundComponent;
  let fixture: ComponentFixture<PrizeMoneyRefundComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PrizeMoneyRefundComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrizeMoneyRefundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
