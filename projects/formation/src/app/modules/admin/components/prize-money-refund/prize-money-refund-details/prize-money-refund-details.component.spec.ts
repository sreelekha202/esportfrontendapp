import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { PrizeMoneyRefundDetailsComponent } from "./prize-money-refund-details.component";

describe("PrizeMoneyRefundDetailsComponent", () => {
  let component: PrizeMoneyRefundDetailsComponent;
  let fixture: ComponentFixture<PrizeMoneyRefundDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PrizeMoneyRefundDetailsComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrizeMoneyRefundDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
