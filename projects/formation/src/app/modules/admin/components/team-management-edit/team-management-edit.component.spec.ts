import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { TeamManagementEditComponent } from "./team-management-edit.component";

describe("TeamManagementEditComponent", () => {
  let component: TeamManagementEditComponent;
  let fixture: ComponentFixture<TeamManagementEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TeamManagementEditComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamManagementEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
