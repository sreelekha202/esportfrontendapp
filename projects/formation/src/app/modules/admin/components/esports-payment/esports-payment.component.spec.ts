import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { EsportsPaymentComponent } from "./esports-payment.component";

describe("EsportsPaymentComponent", () => {
  let component: EsportsPaymentComponent;
  let fixture: ComponentFixture<EsportsPaymentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EsportsPaymentComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EsportsPaymentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
