import { MatDialog } from "@angular/material/dialog";
import { Component, OnInit } from "@angular/core";
import { Location } from "@angular/common";
import { UserService } from "../../../../core/service/user.service";
import { AccessType } from "../access-management/access-management.model";
import {
  InfoPopupComponent,
  InfoPopupComponentData,
  InfoPopupComponentType,
} from "../../../../shared/popups/info-popup/info-popup.component";

import { ActivatedRoute } from "@angular/router";
import { FormControl, Validators, FormGroup } from "@angular/forms";
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: "app-view-user-access",
  templateUrl: "./view-user-access.component.html",
  styleUrls: ["./view-user-access.component.scss"],
})
export class ViewUserAccessComponent implements OnInit {
  AccessType = AccessType;
  data: any;
  isLoading = false;

  userObject: any = {
    fullName: "",
    email: "",
    _id: "",
    accessLevel: [],
  };
  userId: any;
  isControlDisabled = false;
  ownerForm: FormGroup;
  profilePicture: any = "";

  constructor(
    public dialog: MatDialog,
    private activatedRoute: ActivatedRoute,
    private location: Location,
    private translateService: TranslateService,
    private userService: UserService
  ) {}

  ngOnInit(): void {
    this.getIdFromRoute();
    this.ownerForm = new FormGroup({
      name: new FormControl(
        { value: this.userObject.name, disabled: this.isControlDisabled },
        [Validators.required]
      ),
      email: new FormControl(
        { value: this.userObject.email, disabled: this.isControlDisabled },
        [Validators.required, Validators.email]
      ),
      // phoneNumber: new FormControl(
      //   {
      //     value: this.userObject.phoneNumber,
      //     disabled: this.isControlDisabled,
      //   },
      // [Validators.required]
      // ),
    });
  }

  public hasError = (controlName: string, errorName: string) => {
    return this.ownerForm.controls[controlName].hasError(errorName);
  };

  getIdFromRoute() {
    this.activatedRoute.params.subscribe((params) => {
      this.userId = params["id"];
      if (this.userId !== "0123456789ab") {
        this.isControlDisabled = true;
        this.getUserListRecenltyUpdated(this.userId);
      }
    });
  }

  includeAccessType(key, isAdded) {
    if (isAdded) {
      this.userObject.accessLevel.push(key);
    } else {
      const index = this.userObject.accessLevel.indexOf(key);
      if (index > -1) {
        this.userObject.accessLevel.splice(index, 1);
      }
    }
  }

  getUserListRecenltyUpdated(id) {
    const query = `?query=${encodeURIComponent(JSON.stringify({ _id: id }))}`;
    this.userService.getAdminUsers(query).subscribe((res) => {
      if (res.data[0] && !res.data[0].accessLevel) {
        res.data[0]["accessLevel"] = [];
      }
      this.userObject = res.data.map((element) => {
        return {
          profilePicture: element.profilePicture,
          fullName: element.fullName,
          email: element.email,
          accessLevel: element.accessLevel,
        };
      });
      this.profilePicture = this.userObject[0].profilePicture;
      this.userObject = this.userObject[0];
      this.ownerForm.setValue({
        name: this.userObject.fullName,
        email: this.userObject.email,
      });
      this.ownerForm.setErrors(null);
    });
  }

  goBack() {
    this.location.back();
  }

  checkValueIncludes(value) {
    return this.userObject.accessLevel
      ? this.userObject.accessLevel.includes(value)
      : false;
  }

  onGrantAccess() {
    if (this.userObject.accessLevel.length == 0) {
      const afterBlockData: InfoPopupComponentData = {
        title: this.translateService.instant("ADMIN_NEW.GRANT_ACCESS_ALERT"),
        text: this.translateService.instant("ADMIN_NEW.GRANT_ACCESS"),
        type: InfoPopupComponentType.info,
      };
      this.dialog.open(InfoPopupComponent, { data: afterBlockData });
      return;
    }
    if (this.userId === "0123456789ab") {
      const _userObject = {
        fullName: this.ownerForm.controls["name"].value,
        email: this.ownerForm.controls["email"].value,
        password: "defaultAdmin",
        phoneNumber: this.ownerForm.controls["email"].value,
        accessLevel: this.userObject.accessLevel,
      };
      this.isLoading = true;
      this.userService.createAdminUser(_userObject).subscribe(
        (res: any) => {
          this.isLoading = false;
          const afterBlockData: InfoPopupComponentData = {
            title: this.translateService.instant("API.AM.GET.SUCCESS_HEADER"),
            text: res.message,
            type: InfoPopupComponentType.info,
          };
          this.dialog.open(InfoPopupComponent, { data: afterBlockData });
          this.userService.refreshCurrentUser();
          this.goBack();
        },
        (err: any) => {
          this.isLoading = false;
          if (
            err.error.messageCode == "USER_ERROR_004" ||
            err.error.messageCode == "USER_ERROR_003"
          ) {
            const afterBlockData: InfoPopupComponentData = {
              title: this.translateService.instant(
                "API.AM.MODAL.DUPLICATE_MODAL.TITLE"
              ),
              text: this.translateService.instant(
                "API.AM.MODAL.DUPLICATE_MODAL.TEXT"
              ),
              type: InfoPopupComponentType.confirm,
              btnText: this.translateService.instant(
                "API.AM.MODAL.DUPLICATE_MODAL.BUTTONTEXT"
              ),
            };
            const dialogRef = this.dialog.open(InfoPopupComponent, {
              data: afterBlockData,
            });
            dialogRef.afterClosed().subscribe((confirmed) => {
              if (confirmed) {
                this.isLoading = true;
                this.userService
                  .updateUser(
                    {
                      accessLevel: this.userObject.accessLevel,
                      accountType: "admin",
                    },
                    err.error.data
                  )
                  .subscribe(
                    (res: any) => {
                      this.isLoading = false;
                      this.goBack();
                      const afterBlockData: InfoPopupComponentData = {
                        title: this.translateService.instant(
                          "API.AM.GET.SUCCESS_HEADER"
                        ),
                        text: res.message,
                        type: InfoPopupComponentType.info,
                      };
                      this.dialog.open(InfoPopupComponent, {
                        data: afterBlockData,
                      });
                    },
                    (err: any) => {
                      this.isLoading = false;
                      const afterBlockData: InfoPopupComponentData = {
                        title: this.translateService.instant(
                          "API.AM.DELETE.ERROR_HEADER"
                        ),
                        text: err.error.message,
                        type: InfoPopupComponentType.info,
                      };
                      this.dialog.open(InfoPopupComponent, {
                        data: afterBlockData,
                      });
                    }
                  );
              }
            });
          } else {
            this.isLoading = false;
            const afterBlockData: InfoPopupComponentData = {
              title: this.translateService.instant("API.AM.GET.ERROR_HEADER"),
              text: err.error.message,
              type: InfoPopupComponentType.info,
            };
            this.dialog.open(InfoPopupComponent, { data: afterBlockData });
          }
        }
      );
    } else {
      this.isLoading = true;
      this.userService
        .updateUser({ accessLevel: this.userObject.accessLevel }, this.userId)
        .subscribe(
          (res: any) => {
            this.isLoading = false;
            const afterBlockData: InfoPopupComponentData = {
              title: this.translateService.instant("API.AM.GET.SUCCESS_HEADER"),
              text: res.message,
              type: InfoPopupComponentType.info,
            };
            this.dialog.open(InfoPopupComponent, { data: afterBlockData });
            this.goBack();
          },
          (err: any) => {
            this.isLoading = false;
            const afterBlockData: InfoPopupComponentData = {
              title: this.translateService.instant("API.AM.GET.ERROR_HEADER"),
              text: err.error.message,
              type: InfoPopupComponentType.info,
            };
            this.dialog.open(InfoPopupComponent, { data: afterBlockData });
          }
        );
    }
  }
}
