import { Component, OnInit } from "@angular/core";
import { ToastService } from "../../../../shared/toast/toast.service";
import { TournamentService } from "../../../../core/service";
import { FormControl } from "@angular/forms";
import { debounceTime } from "rxjs/operators";

@Component({
  selector: "app-reg-fee-refund",
  templateUrl: "./reg-fee-refund.component.html",
  styleUrls: ["./reg-fee-refund.component.scss"],
})
export class RegFeeRefundComponent implements OnInit {
  columns = [
    { name: "Tournament Name" },
    { name: "Game" },
    { name: "End Date" },
    { name: "Participant(s)" },
    { name: "Disbursal Status" },
    { name: "Details" },
  ];
  rows: any = [];
  showDetails = false;
  isLoading = false;

  pagination = {
    page: 1,
    limit: 20,
  };

  tournamentDetails;
  tournamentId;
  pageDetail;
  search = new FormControl("");

  constructor(
    private tournamentService: TournamentService,
    public toastService: ToastService
  ) {}

  ngOnInit(): void {
    this.getTournamentList();
    this.attachDebounce();
  }

  attachDebounce() {
    this.search.valueChanges.pipe(debounceTime(1000)).subscribe(() => {
      this.pagination.page = 1;
      this.getTournamentList();
    });
  }

  pageChanged(page): void {
    this.pagination.page = page;
    this.getTournamentList();
  }

  getPaginationQuery() {
    return {
      pagination: this.pagination,
    };
  }

  async getTournamentList() {
    try {
      const payload = JSON.stringify({
        searchKey: this.search.value,
        ...this.getPaginationQuery(),
      });
      const res = await this.tournamentService.getRegFeeRefundDisbursals(
        payload
      );
      this.pageDetail = res.data;
      this.rows = res.data["docs"];
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
    }
  }

  setPage(event) {
    this.pagination.page = event.offset + 1;
    this.isLoading = true;
    this.rows.length = 0;
    this.getTournamentList();
    this.isLoading = false;
  }

  navigate(tournamentDetails) {
    this.tournamentDetails = tournamentDetails;
    this.showDetails = true;
  }

  async isToggle(e) {
    this.showDetails = e.toggle;
    if (!e.toggle) {
      this.getTournamentList();
    }
  }
}
