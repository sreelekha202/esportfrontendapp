import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { RegFeeRefundDetailsComponent } from "./reg-fee-refund-details.component";

describe("RegFeeRefundDetailsComponent", () => {
  let component: RegFeeRefundDetailsComponent;
  let fixture: ComponentFixture<RegFeeRefundDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RegFeeRefundDetailsComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegFeeRefundDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
