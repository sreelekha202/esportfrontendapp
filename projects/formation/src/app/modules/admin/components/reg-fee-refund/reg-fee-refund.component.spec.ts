import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { RegFeeRefundComponent } from "./reg-fee-refund.component";

describe("RegFeeRefundComponent", () => {
  let component: RegFeeRefundComponent;
  let fixture: ComponentFixture<RegFeeRefundComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RegFeeRefundComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegFeeRefundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
