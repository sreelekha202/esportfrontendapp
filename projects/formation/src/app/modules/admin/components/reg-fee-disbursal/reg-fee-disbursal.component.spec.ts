import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { RegFeeDisbursalComponent } from "./reg-fee-disbursal.component";

describe("RegFeeDisbursalComponent", () => {
  let component: RegFeeDisbursalComponent;
  let fixture: ComponentFixture<RegFeeDisbursalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RegFeeDisbursalComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegFeeDisbursalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
