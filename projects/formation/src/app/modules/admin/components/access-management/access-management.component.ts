import { Component, OnInit, ViewChild } from "@angular/core";
import { DatatableComponent } from "@swimlane/ngx-datatable";
import { MatDialog } from "@angular/material/dialog";

import {
  ViewUserAccessPopupComponent,
  ViewUserAccessPopupComponentData,
} from "../../popups/view-user-access/view-user-access-popup.component";
import {
  InfoPopupComponent,
  InfoPopupComponentData,
  InfoPopupComponentType,
} from "../../../../shared/popups/info-popup/info-popup.component";
import { AccessManagementUser } from "./access-management.model";
import { UserAccessType } from "./access-management.model";
import { UserService } from "../../../../core/service";
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: "app-access-management",
  templateUrl: "./access-management.component.html",
  styleUrls: ["./access-management.component.scss"],
})
export class AccessManagementComponent implements OnInit {
  @ViewChild(DatatableComponent) table: DatatableComponent;

  tempFirstTab = [];
  rowsFirstTab = [];
  isLoading = false;
  columnsFirstTab = [
    { name: "User ID" },
    { name: "Name" },
    { name: "Access Level" },
    { name: "Phone" },
    { name: "" },
  ];

  constructor(
    public dialog: MatDialog,
    private userService: UserService,
    private translateService: TranslateService
  ) {}

  ngOnInit(): void {
    // By default - To get the list of user for recently Updated
    this.getUserListRecenltyUpdated();
  }

  // Call an API to get the user details for Recently Updated Tab
  getUserListRecenltyUpdated() {
    this.isLoading = true;
    const query = `?query=${encodeURIComponent(
      JSON.stringify({ accountType: "admin" })
    )}`;
    this.userService.getAdminUsers(query).subscribe((res: any) => {
      this.isLoading = false;
      const response = res.data.map((element) => {
        return {
          id: element._id,
          name: element.fullName,
          email: element.email,
          phone: element.phoneNumber,
          accessLevel: element.accessLevel.filter((value) =>
            Object.keys(UserAccessType).includes(value)
          ),
          createdOn: element.createdOn,
        };
      });
      // cache our list
      this.tempFirstTab = [...response];
      // push our inital complete list
      this.rowsFirstTab = response;
    });
  }

  onViewUserAccess(user: AccessManagementUser): void {
    const blockUserData: ViewUserAccessPopupComponentData = { user };

    const dialogRef = this.dialog.open(ViewUserAccessPopupComponent, {
      data: blockUserData,
      panelClass: "custom-dialog-container",
    });

    dialogRef.afterClosed().subscribe(() => {});
  }

  onRemoveUser(_id) {
    const blockUserData: InfoPopupComponentData = {
      title: this.translateService.instant("API.AM.MODAL.REMOVE_MODAL.TITLE"),
      text: this.translateService.instant("API.AM.MODAL.REMOVE_MODAL.TEXT"),
      type: InfoPopupComponentType.confirm,
      btnText: this.translateService.instant(
        "API.AM.MODAL.REMOVE_MODAL.BUTTONTEXT"
      ),
    };
    const dialogRef = this.dialog.open(InfoPopupComponent, {
      data: blockUserData,
    });

    dialogRef.afterClosed().subscribe((confirmed) => {
      if (confirmed) {
        this.userService.updateUser({ accountType: "user" }, _id).subscribe(
          (res: any) => {
            this.getUserListRecenltyUpdated();
            const afterBlockData: InfoPopupComponentData = {
              title: this.translateService.instant(
                "API.AM.DELETE.SUCCESS_HEADER"
              ),
              text: res.message,
              type: InfoPopupComponentType.info,
            };
            this.dialog.open(InfoPopupComponent, { data: afterBlockData });
          },
          (err: any) => {
            const afterBlockData: InfoPopupComponentData = {
              title: this.translateService.instant(
                "API.AM.DELETE.ERROR_HEADER"
              ),
              text: err.error.message,
              type: InfoPopupComponentType.info,
            };
            this.dialog.open(InfoPopupComponent, { data: afterBlockData });
          }
        );
      }
    });
  }
}
