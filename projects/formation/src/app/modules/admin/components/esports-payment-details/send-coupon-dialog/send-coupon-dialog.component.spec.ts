import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { SendCouponDialogComponent } from "./send-coupon-dialog.component";

describe("SendCouponDialogComponent", () => {
  let component: SendCouponDialogComponent;
  let fixture: ComponentFixture<SendCouponDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SendCouponDialogComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SendCouponDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
