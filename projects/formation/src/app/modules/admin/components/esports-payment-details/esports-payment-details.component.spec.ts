import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { EsportsPaymentDetailsComponent } from "./esports-payment-details.component";

describe("EsportsPaymentDetailsComponent", () => {
  let component: EsportsPaymentDetailsComponent;
  let fixture: ComponentFixture<EsportsPaymentDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EsportsPaymentDetailsComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EsportsPaymentDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
