import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { RegionConfigurationComponent } from "./region-configuration.component";

describe("RegionConfigurationComponent", () => {
  let component: RegionConfigurationComponent;
  let fixture: ComponentFixture<RegionConfigurationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RegionConfigurationComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegionConfigurationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
