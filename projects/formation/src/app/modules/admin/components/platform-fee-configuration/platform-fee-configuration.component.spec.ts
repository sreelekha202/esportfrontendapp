import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { PlatformFeeConfigurationComponent } from "./platform-fee-configuration.component";

describe("PlatformFeeConfigurationComponent", () => {
  let component: PlatformFeeConfigurationComponent;
  let fixture: ComponentFixture<PlatformFeeConfigurationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PlatformFeeConfigurationComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlatformFeeConfigurationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
