import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { RegFeeDisbursalDetailsComponent } from "./reg-fee-disbursal-details.component";

describe("RegFeeDisbursalDetailsComponent", () => {
  let component: RegFeeDisbursalDetailsComponent;
  let fixture: ComponentFixture<RegFeeDisbursalDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RegFeeDisbursalDetailsComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegFeeDisbursalDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
