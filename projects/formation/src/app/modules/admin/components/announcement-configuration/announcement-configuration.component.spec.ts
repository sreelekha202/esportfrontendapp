import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { AnnouncementConfigurationComponent } from "./announcement-configuration.component";

describe("AnnouncementConfigurationComponent", () => {
  let component: AnnouncementConfigurationComponent;
  let fixture: ComponentFixture<AnnouncementConfigurationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AnnouncementConfigurationComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnnouncementConfigurationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
