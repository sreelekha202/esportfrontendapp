import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { ViewUserAccessPopupComponent } from "./view-user-access-popup.component";

describe("CounterComponent", () => {
  let component: ViewUserAccessPopupComponent;
  let fixture: ComponentFixture<ViewUserAccessPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ViewUserAccessPopupComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewUserAccessPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
