import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IPagination } from 'projects/esports/src/lib/models/paginations';
import { HomeService } from '../../../core/service';

@Component({
  selector: 'app-search-article',
  templateUrl: './search-article.component.html',
  styleUrls: ['./search-article.component.scss'],
})
export class SearchArticleComponent implements OnInit {
  isArticleFlag = false;
  constructor(public homeService: HomeService,public activatedRoute: ActivatedRoute,) {}
  page: IPagination;
  paginationData = {
    page: 1,
    limit: 6,
    sort: "-startDate",
  };
  text;

  ngOnInit(): void {
    this.activatedRoute.queryParamMap.subscribe((e) => {
      if (e.get('text')) {
        this.text = e.get('text');
      }
    });
    this.getSearchArticle();
  }

  getSearchArticle(){
    this.homeService.searchedArticle.subscribe((res:any) => {
      if (res?.docs?.length > 0) this.isArticleFlag = true;
      this.page = {
        totalItems: res?.totalDocs,
        itemsPerPage: res?.limit,
        maxSize: 5,
      };
    });
  }

  pageChanged(page): void {
    this.paginationData.page = page;
    this.homeService.updateSearchParams(this.text,this.paginationData);
    this.homeService.searchArticle();
  }
}
