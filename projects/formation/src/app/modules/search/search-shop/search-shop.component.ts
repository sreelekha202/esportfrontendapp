import { TranslateService } from "@ngx-translate/core";
import { Component, OnInit } from "@angular/core";

import { TopUpItemComponentData } from "../../../core/top-up-item/top-up-item.component";

@Component({
  selector: "app-search-shop",
  templateUrl: "./search-shop.component.html",
  styleUrls: ["./search-shop.component.scss"],
})
export class SearchShopComponent implements OnInit {
  topOffers: TopUpItemComponentData[] = [
    {
      logo: "./assets/images/Home/top1.png",
      name: "Free Fire",
      productName: "NA",
    },
    {
      logo: "./assets/images/Home/top2.png",
      name: "PUBG",
      productName: "NA",
    },
    {
      logo: "./assets/images/Home/top3.png",
      name: "Call of Duty",
      productName: "NA",
    },
    {
      logo: "./assets/images/Home/top4.png",
      name: "Mobile Legends",
      productName: "NA",
    },
    {
      logo: "./assets/images/Home/top5.png",
      name: "Lord fo Estera",
      productName: "NA",
    },
    {
      logo: "./assets/images/Home/top6.png",
      name: "Shellfire",
      productName: "NA",
    },
    {
      logo: "./assets/images/Home/top7.png",
      name: "Speed Drifters",
      productName: "NA",
    },
    {
      logo: "./assets/images/Home/top8.png",
      name: "Speed Drifters",
      productName: "NA",
    },
  ];

  constructor(public translate: TranslateService) {}

  ngOnInit(): void {}
}
