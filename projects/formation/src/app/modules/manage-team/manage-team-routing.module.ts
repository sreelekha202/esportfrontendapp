import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { ManageTeamComponent } from "./manage-team.component";

const routes: Routes = [
  {
    path: ":id",
    component: ManageTeamComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ManageTeamRoutingModule {}
