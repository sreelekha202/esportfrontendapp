import { NgModule } from "@angular/core";

import { ModalModule } from "ngx-bootstrap/modal";

import { CoreModule } from "../../core/core.module";
import { LogRegModule } from "../log-reg/log-reg.module";
import { ProfileRoutingModule } from "./profile-routing.module";
import { SharedModule } from "../../shared/modules/shared.module";

import { BasicInfoComponent } from "./basic-info/basic-info.component";
import { ChangePasswordComponent } from "./profile-setting/change-password/change-password.component";
import { InboxComponent } from "./inbox/inbox.component";
import { InboxMessageComponent } from "./inbox/inbox-message/inbox-message.component";
import { InboxMessageItemComponent } from "./inbox/inbox-message-item/inbox-message-item.component";
import { MyBookmarksComponent } from "./my-bookmarks/my-bookmarks.component";
import { MyBracketComponent } from "./my-bracket/my-bracket.component";
import { MyContentComponent } from "./my-content/my-content.component";
import { MyTeamsComponent } from "./my-teams/my-teams.component";
import { MyTournamentComponent } from "./my-tournament/my-tournament.component";
import { MyTransactionsComponent } from "./my-transactions/my-transactions.component";
import { ProfileComponent } from "./profile.component";
import { ProfileSettingComponent } from "./profile-setting/profile-setting.component";
import { MyTeamsViewComponent } from "./my-teams-view/my-teams-view.component";

import { MembersTableComponent } from "./components/members-table/members-table.component";
import { TeamCardControlComponent } from "./components/team-card-control/team-card-control.component";
import { TeamCardInfoComponent } from "./components/team-card-info/team-card-info.component";
import { TeamCardInviteModule } from "./components/team-card-invite/team-card-invite.module";
import { TournamentMatchesComponent } from "./components/tournament-matches/tournament-matches.component";
import { MyMatchesModule } from '../my-matches/my-matches.module';

import { RouterBackModule } from "../../shared/directives/router-back.module";

import { faTrash, faEdit, faEye } from "@fortawesome/free-solid-svg-icons";
import { NgCircleProgressModule } from "ng-circle-progress";
import {
  FontAwesomeModule,
  FaIconLibrary,
} from "@fortawesome/angular-fontawesome";
import { ReferralComponent } from "./app-referral/referral/referral.component";

@NgModule({
  declarations: [
    BasicInfoComponent,
    ChangePasswordComponent,
    InboxComponent,
    InboxMessageComponent,
    InboxMessageItemComponent,
    MembersTableComponent,
    MyBookmarksComponent,
    MyBracketComponent,
    MyContentComponent,
    MyTeamsComponent,
    MyTeamsViewComponent,
    MyTournamentComponent,
    MyTransactionsComponent,
    ProfileComponent,
    ProfileSettingComponent,
    ReferralComponent,
    TeamCardControlComponent,
    TeamCardInfoComponent,
    TournamentMatchesComponent,
  ],
  imports: [
    CoreModule,
    ModalModule.forRoot(),
    ProfileRoutingModule,
    SharedModule,
    // Specify ng-circle-progress as an import
    NgCircleProgressModule.forRoot({
      // set defaults here
    }),
    LogRegModule,
    FontAwesomeModule,
    RouterBackModule,
    TeamCardInviteModule,
    MyMatchesModule
  ],
})
export class ProfileModule {
  constructor(private library: FaIconLibrary) {
    library.addIcons(faTrash, faEdit, faEye);
  }
}
