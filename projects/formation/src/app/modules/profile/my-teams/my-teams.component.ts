import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup } from "@angular/forms";
import { AppHtmlRoutes } from "../../../app-routing.model";
import { IPagination } from "../../../shared/models";
import { UserService } from "../../../core/service";

@Component({
  selector: "app-my-teams",
  templateUrl: "./my-teams.component.html",
  styleUrls: ["./my-teams.component.scss"],
})
export class MyTeamsComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;
  paginationData = {
    page: 1,
    limit: 5,
    sort: { _id: -1 },
  };
  paginationData2 = {
    page: 1,
    limit: 5,
    sort: { _id: -1 },
  };
  page: IPagination;
  page2: IPagination;

  public form: FormGroup;

  mock_select = [];
  mock_teams_control = [];
  mock_teams_invites = [];
  constructor(private fb: FormBuilder, private userService: UserService) {}

  ngOnInit(): void {
    this.form = this.fb.group({
      view: [""],
    });

    this.getMyTeams();
    this.getMyInvites();
  }

  selectType(event): void {}

  getMyTeams = async () => {
    const pagination = JSON.stringify(this.paginationData);
    const data = await this.userService.getMyTeam({
      pagination: pagination,
    });
    if (data) {
      this.page = {
        totalItems: data?.data?.totalDocs,
        itemsPerPage: data?.data?.limit,
        maxSize: 5,
      };
    }
    this.mock_teams_control = data.data.docs;
  };

  getMyInvites = async () => {
    const pagination = JSON.stringify(this.paginationData2);
    const data = await this.userService.getMyInvite({
      pagination: pagination,
    });
    if (data) {
      this.page2 = {
        totalItems: data?.data?.totalDocs,
        itemsPerPage: data?.data?.limit,
        maxSize: 5,
      };
    }
    this.mock_teams_invites = data.data.docs;
  };

  pageChanged(page): void {
    this.paginationData.page = page;
    this.getMyTeams();
  }

  pageChanged2(page): void {
    this.paginationData2.page = page;
    this.getMyInvites();
  }
}
