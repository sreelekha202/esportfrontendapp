import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { TeamCardInviteComponent } from "./team-card-invite.component";
import { I18nModule } from "../../../../i18n/i18n.module";

@NgModule({
  declarations: [TeamCardInviteComponent],
  imports: [CommonModule, I18nModule],
  exports: [TeamCardInviteComponent],
})
export class TeamCardInviteModule {}
