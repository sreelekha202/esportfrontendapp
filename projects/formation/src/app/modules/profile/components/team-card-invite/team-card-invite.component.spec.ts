import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { TeamCardInviteComponent } from "./team-card-invite.component";

describe("TeamCardInviteComponent", () => {
  let component: TeamCardInviteComponent;
  let fixture: ComponentFixture<TeamCardInviteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TeamCardInviteComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamCardInviteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
