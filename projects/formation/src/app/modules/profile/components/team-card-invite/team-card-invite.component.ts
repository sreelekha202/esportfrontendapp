import { Component, OnInit, Input } from "@angular/core";
import { Router } from "@angular/router";
import { AppHtmlProfileRoutes } from "projects/formation/src/app/app-routing.model";
import { UserService } from "../../../../core/service";
import { ToastService } from "../../../../shared/toast/toast.service";

interface TeamCardInvite {
  _id: number | string;
  teamName: string;
  logo: string;
  teamId: {
    teamName: string;
    logo: string;
  };
}

@Component({
  selector: "app-team-card-invite",
  templateUrl: "./team-card-invite.component.html",
  styleUrls: ["./team-card-invite.component.scss"],
})
export class TeamCardInviteComponent implements OnInit {
  @Input() data: TeamCardInvite;
  @Input() showControls: boolean = true;
  AppHtmlProfileRoutes = AppHtmlProfileRoutes;

  constructor(
    private userService: UserService,
    private router: Router,
    public toastService: ToastService
  ) {}

  ngOnInit(): void {}

  async updateInvitees(teamId, status) {
    await this.userService.update_invitees({ teamId, status }).subscribe(
      (data) => {
        this.toastService.showSuccess(data?.message);
        this.router
          .navigateByUrl("/profile/basic-info", { skipLocationChange: true })
          .then(() => {
            this.router.navigate(["/profile/teams"]);
          });
      },
      (error) => {}
    );
  }

  view(data) {
    this.router.navigate([AppHtmlProfileRoutes.teamsView, data._id]);
  }
}
