import { Component, OnInit, Input } from "@angular/core";
import { AppHtmlRoutes, AppHtmlProfileRoutes } from "../../../../app-routing.model";

interface TeamCardInfo {
  _id: number | string;
  teamName: string;
  logo: string;
  social: any;
  // info: {
  //   games: number;
  //   w: number;
  //   l: number;
  // };
  // trophies: {
  //   gold: number;
  //   silver: number;
  //   bronze: number;
  // };
}

@Component({
  selector: "app-team-card-info",
  templateUrl: "./team-card-info.component.html",
  styleUrls: ["./team-card-info.component.scss"],
})
export class TeamCardInfoComponent implements OnInit {
  @Input() data: TeamCardInfo;

  AppHtmlRoutes = AppHtmlRoutes;
  AppHtmlProfileRoutes = AppHtmlProfileRoutes;

  socials = [
    {
      name: "facebook",
      link: "",
      icon: "assets/icons/socials/facebook.svg",
    },
    {
      name: "instagram",
      link: "",
      icon: "assets/icons/socials/instagram.svg",
    },
    {
      name: "twitch",
      link: "",
      icon: "assets/icons/socials/twitch.svg",
    },
    {
      name: "linkedin",
      link: "",
      icon: "assets/icons/socials/linkedin.svg",
    },
    {
      name: "twitter",
      link: "",
      icon: "assets/icons/socials/twitter.svg",
    },
    {
      name: "youtube",
      link: "",
      icon: "assets/icons/youtube-blue.svg",
    },
    {
      name: "discord",
      link: "",
      icon: "assets/icons/discord-blue.svg",
    },
    {
      name: 'instagram',
      link: '',
      icon: 'assets/icons/instagram-blue.svg',
    }
  ];

  constructor() {}

  ngOnInit(): void {}
}
