import { Router } from "@angular/router";
import { Component, OnInit, Input } from "@angular/core";
import { ToastService } from "../../../../shared/toast/toast.service";
import { UserService } from "../../../../core/service/user.service";
import { TranslateService } from "@ngx-translate/core";
import { MatDialog } from "@angular/material/dialog";
import {
  AppHtmlRoutes,
  AppHtmlProfileRoutes,
  AppHtmlAdminRoutes,
} from "../../../../app-routing.model";
import { IUser } from "../../../../shared/models";
import {
  InfoPopupComponent,
  InfoPopupComponentData,
  InfoPopupComponentType,
} from "../../../../shared/popups/info-popup/info-popup.component";

interface TeamCardControl {
  _id: number | string;
  teamName: string;
  logo: string;
  slug: string;
  detail: any;
}

@Component({
  selector: "app-team-card-control",
  templateUrl: "./team-card-control.component.html",
  styleUrls: ["./team-card-control.component.scss"],
})
export class TeamCardControlComponent implements OnInit {
  @Input() data: TeamCardControl;

  AppHtmlRoutes = AppHtmlRoutes;
  AppHtmlProfileRoutes = AppHtmlProfileRoutes;
  AppHtmlAdminRoutes = AppHtmlAdminRoutes;
  user: IUser;

  constructor(
    private userService: UserService,
    public toastService: ToastService,
    private router: Router,
    public translate: TranslateService,
    public dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.getCurrentUserDetails();
  }
  deleteTeam() {
    const teamObj = {
      id: this.data._id,
      admin: true,
      query: {
        condition: { _id: this.data._id, status: "active" },
        update: { status: "inactive" },
      },
    };
    this.userService.update_team_admin(teamObj).subscribe(
      (res) => {
        this.toastService.showSuccess(res.message);
        this.router
          .navigateByUrl("/home", { skipLocationChange: true })
          .then(() => {
            this.router.navigate(["/profile/teams"]);
          });
      },
      (err) => {
        this.toastService.showError(err.error.message);
      }
    );
  }

  getCurrentUserDetails() {
    this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.user = data;
      }
    });
  }
  leaveTeam() {
    this.userService
      .update_member({
        teamId: this.data._id,
        userId: this.user._id,
        name: this.user.fullName,
        status: "deleted",
      })
      .subscribe(
        (data) => {
          this.toastService.showSuccess(data?.message);
          this.router
          .navigateByUrl("/home", { skipLocationChange: true })
          .then(() => {
            this.router.navigate(["/profile/teams"]);
          });
        },
        (error) => {}
      );
  }

  checkMemberTournamentStatus() {
    this.userService
      .getTournamentByTeamId(this.data._id)
      .subscribe((res: any) => {
        if (res?.data?.docs.length) {
          this.toastService.showError(
            this.translate.instant("PROFILE.TEAMS.ERRORS.ERROR11")
          );
        } else {
          this.onRemoveUser(this.data._id);
        }
      });
  }

  onRemoveUser(_id) {
    const confirmMessage = `${this.translate.instant(
      "API.AM.MODAL.REMOVE_TEAM_MODAL.TEXT"
    )} ${this.data.teamName}?`;
    const blockUserData: InfoPopupComponentData = {
      title: this.translate.instant("API.AM.MODAL.REMOVE_TEAM_MODAL.TITLE"),
      text: confirmMessage,
      type: InfoPopupComponentType.confirm,
      btnText: this.translate.instant(
        "API.AM.MODAL.REMOVE_TEAM_MODAL.BUTTONTEXT"
      ),
    };
    const dialogRef = this.dialog.open(InfoPopupComponent, {
      data: blockUserData,
    });

    dialogRef.afterClosed().subscribe((confirmed) => {
      if (confirmed) {
        this.userService
          .update_member({
            teamId: this.data._id,
            userId: this.user._id,
            name: this.user.fullName,
            status: "deleted",
          })
          .subscribe(
            (data) => {
              this.toastService.showSuccess(data?.message);
              window.location.reload();
            },
            (error) => {}
          );
      }
    });
  }
}
