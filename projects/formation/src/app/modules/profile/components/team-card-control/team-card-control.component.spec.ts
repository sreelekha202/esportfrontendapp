import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { TeamCardControlComponent } from "./team-card-control.component";

describe("TeamCardControlComponent", () => {
  let component: TeamCardControlComponent;
  let fixture: ComponentFixture<TeamCardControlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TeamCardControlComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamCardControlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
