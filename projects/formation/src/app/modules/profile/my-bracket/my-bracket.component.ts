import { Component, OnInit } from "@angular/core";

import { ToastService } from "../../../shared/toast/toast.service";
import { BracketService } from "../../../core/service";

import {
  InfoPopupComponent,
  InfoPopupComponentData,
  InfoPopupComponentType,
} from "../../../shared/popups/info-popup/info-popup.component";

import { IPagination } from "../../../shared/models";

import { MatDialog } from "@angular/material/dialog";
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: "app-my-bracket",
  templateUrl: "./my-bracket.component.html",
  styleUrls: ["./my-bracket.component.scss"],
})
export class MyBracketComponent implements OnInit {
  bracketData;
  page: IPagination;

  paginationData = {
    page: 1,
    limit: 4,
    sort: "-updatedOn",
    projection: [
      "_id",
      "name",
      "bracketType",
      "updatedOn",
      "maximumParticipants",
    ],
  };

  constructor(
    private bracketService: BracketService,
    private matDialog: MatDialog,
    private toastService: ToastService,
    private translateService: TranslateService
  ) {}

  ngOnInit(): void {
    this.fetchAllBracket();
  }

  fetchAllBracket = async () => {
    try {
      const query = `?pagination=${encodeURIComponent(
        JSON.stringify(this.paginationData)
      )}`;
      this.bracketData = await this.bracketService.fetchAllBracket(query);
      this.page = {
        totalItems: this.bracketData?.data?.totalDocs,
        itemsPerPage: this.bracketData?.data?.limit,
        maxSize: 5,
      };
    } catch (error) {
      this.toastService.showError(error.error?.message || error?.message);
    }
  };

  pageChanged(page): void {
    this.paginationData.page = page;
    this.fetchAllBracket();
  }

  onRemoveBracket(bracket) {
    const blockUserData: InfoPopupComponentData = {
      title: this.translateService.instant("PROFILE.BRACKETS.POP_UP.TITLE"),
      text: `${this.translateService.instant("PROFILE.BRACKETS.POP_UP.BODY")} ${
        bracket?.name
      }.`,
      type: InfoPopupComponentType.confirm,
      btnText: `${this.translateService.instant("BUTTON.CONFIRM")}`,
    };
    const dialogRef = this.matDialog.open(InfoPopupComponent, {
      data: blockUserData,
    });

    dialogRef.afterClosed().subscribe(async (confirmed) => {
      if (confirmed) {
        try {
          const response = await this.bracketService.deleteBracket(
            bracket?._id
          );
          this.toastService.showSuccess(response?.message);
          this.fetchAllBracket();
        } catch (error) {
          this.toastService.showError(error.error?.message || error?.message);
        }
      }
    });
  }
}
