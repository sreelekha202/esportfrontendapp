import {
  Component,
  OnInit,
  Inject,
  HostListener,
  OnDestroy,
} from "@angular/core";
import { UserService, TransactionService } from "../../../../core/service";
import { DatePipe } from "@angular/common";
import { TranslateService } from "@ngx-translate/core";
import { DOCUMENT } from "@angular/common";
import { ToastService } from "../../../../shared/toast/toast.service";
import { IUser } from "../../../../shared/models";
import {
  NgbModal,
  NgbModalConfig,
  ModalDismissReasons,
} from "@ng-bootstrap/ng-bootstrap";
import { IPagination } from "../../../../shared/models";
import { Subscription } from "rxjs";
@Component({
  selector: "app-referral",
  templateUrl: "./referral.component.html",
  styleUrls: ["./referral.component.scss"],
})
export class ReferralComponent implements OnInit, OnDestroy {
  transactionDetail;
  rewardDetails;
  clicked: boolean = false;
  showLoader = false;
  closeResult: boolean;
  user: IUser;
  page: IPagination;
  URL: string;
  paginationData = {
    page: 1,
    limit: 5,
    sort: { createdOn: -1 },
  };
  rows = [];
  referral = {
    rows: [],
    columns: [{ name: "DATE" }, { name: "Invitation" }],
  };
  totalCount = 0;
  userSubscription: Subscription;
  public innerWidth: any;
  constructor(
    @Inject(DOCUMENT) private document: Document,
    private userService: UserService,
    public datePipe: DatePipe,
    private transactionServices: TransactionService,
    private translateService: TranslateService,
    private modalService: NgbModal,
    public toastService: ToastService
  ) {}
  @HostListener("window:resize", ["$event"])
  onResize(event) {
    this.innerWidth = window.innerWidth;
  }
  ngOnInit(): void {
    this.innerWidth = window.innerWidth;
    this.getUserData();
  }

  getUserData() {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.user = Object.assign({}, data);
        this.URL =
          this.document.location.origin +
          "/user/referral:" +
          data.accountDetail.referralId;
        if (data.accountDetail.referralId) {
          this.getUserinviteesList();
        }
      } else {
        // const newVal = this.userService.getAuthenticatedUser();
      }
    });
  }

  getUserinviteesList = () => {
    const pagination = JSON.stringify(this.paginationData);
    this.userService
      .getUserinviteesList({
        pagination: pagination,
      })
      .subscribe(
        (res) => {
          this.showLoader = false;
          this.referral.rows = res.data;
          this.referral.rows.forEach((obj, index) => {
            obj.DATE = this.datePipe.transform(obj.DATE, "d/M/yyyy");
          });
          this.totalCount = res.totals.count;
          this.page = {
            totalItems: res.totals.count,
            itemsPerPage: 5,
            maxSize: 5,
          };
        },
        (err) => {}
      );
  };
  open(content) {
    if (this.user.accountDetail.referralId) {
      this.clicked = false;
      this.modalService
        .open(content, { ariaLabelledBy: "modal-basic-title", centered: true })
        .result.then(
          (result) => {
            this.copyToClipboard(this.URL);
          },
          (reason) => {
            this.closeResult = false;
          }
        );
    } else {
      this.toastService.showError(
        this.translateService.instant(
          "PROFILE.BASIC_INFO.REFERRAL_ERROR_MESSAGE"
        )
      );
    }
  }
  setPage1(event) {
    this.paginationData.page = event;
    this.showLoader = false;
    this.rows.length = 0;
    this.getUserinviteesList();
  }
  copyToClipboard(event: any) {
    const dummyElement = this.document.createElement("textarea");
    var element = document.getElementById("myDIV");
    element.classList.toggle("disableBtn");
    this.document.body.appendChild(dummyElement);
    dummyElement.value = this.URL;
    event.target.disabled = true;
    dummyElement.select();
    this.document.execCommand("copy");
    this.document.body.removeChild(dummyElement);
  }

  ngOnDestroy() {
    this.modalService.dismissAll();
    this.userSubscription?.unsubscribe();
  }
}
