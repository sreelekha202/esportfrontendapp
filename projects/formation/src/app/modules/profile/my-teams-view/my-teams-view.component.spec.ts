import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { MyTeamsViewComponent } from "./my-teams-view.component";

describe("MyTeamsViewComponent", () => {
  let component: MyTeamsViewComponent;
  let fixture: ComponentFixture<MyTeamsViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MyTeamsViewComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyTeamsViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
