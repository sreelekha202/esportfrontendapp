import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { UserService } from "../../../core/service";
import { IUser } from "../../../shared/models";
import { ToastService } from "../../../shared/toast/toast.service";
import { Location } from "@angular/common";
@Component({
  selector: "app-my-teams-view",
  templateUrl: "./my-teams-view.component.html",
  styleUrls: ["./my-teams-view.component.scss"],
})
export class MyTeamsViewComponent implements OnInit {
  user: IUser;
  showLoader = false;

  mock_team = {
    _id: "6034e2909472c10008caad62",
    teamName: "Ashutosh Team",
    logo:
      "https://stc-img.s3.me-south-1.amazonaws.com/dev/team/1614078608346.jpeg",
    shortDescription: "A winning team combo",
    teamCreator: "5fa1188be12d14000994d68a",
    slug: "ashutosh-team-6034e2909472c10008caad62",
    __v: 0,
  };

  mock_members = [
    {
      role: "captain",
      _id: "6034e2909472c10008caad66",
      userId: {
        _id: "6032886a0e98b00008d35215",
        fullName: "User8",
        phoneNumber: "+966500001407",
        email: "user8@gmail.com",
        profilePicture: "",
      },
      statistic: {
        games: 200,
        wins: 123,
        points: 2345,
      },
      invited_by: "5fa1188be12d14000994d68a",
      __v: 0,
    },
    {
      role: "player",
      _id: "6034e5fa9472c10008caad6e",
      userId: {
        _id: "5fad834d305f820008c729c8",
        fullName: "Parag",
        phoneNumber: "+966500000222",
        email: "parag@dynasty-esports.com",
        profilePicture:
          "https://stc-img.s3.me-south-1.amazonaws.com/dev/profile/1606467986131.png",
      },
      statistic: {
        games: 120,
        wins: 99,
        points: 1255,
      },
      invited_by: "5f8eef3006aed100084ca6f3",
      __v: 0,
    },
    {
      role: "captain",
      _id: "6034e2909472c10008caad68",
      userId: {
        _id: "5fd8a60fcc87760007adeb42",
        fullName: "Haunter",
        phoneNumber: "+96555555556",
        email: "haunter@gmail.com",
        profilePicture:
          "https://stc-img.s3.me-south-1.amazonaws.com/dev/profile/1613730902663.png",
      },
      statistic: {
        games: 90,
        wins: 80,
        points: 945,
      },
      invited_by: "5fa1188be12d14000994d68a",
      __v: 0,
    },
    {
      role: "captain",
      _id: "6034e2909472c10008caad64",
      userId: {
        _id: "5f8eef3006aed100084ca6f3",
        fullName: "Pradeep",
        phoneNumber: "+97317001235",
        email: "ashutosh@dynasty-esports.com",
        profilePicture:
          "https://stc-img.s3.me-south-1.amazonaws.com/dev/profile/1614077499526.png",
      },
      statistic: {
        games: 80,
        wins: 43,
        points: 845,
      },
      invited_by: "5fa1188be12d14000994d68a",
      __v: 0,
    },
  ];

  mock_matches = {
    title: "Pubg global tournament",
    matches: [
      {
        result: "win",
        team: "Brightforce",
        info: {
          day: "Monday",
          date: " 08/02/21",
          time: "04:15",
        },
      },
      {
        result: "loss",
        team: "Peanutbutter",
        info: {
          day: "Monday",
          date: " 08/02/21",
          time: "04:15",
        },
      },
      {
        result: "win",
        team: "Fuelpower",
        info: {
          day: "Monday",
          date: " 08/02/21",
          time: "04:15",
        },
      },
      {
        result: "win",
        team: "Cheesezy",
        info: {
          day: "Monday",
          date: " 08/02/21",
          time: "04:15",
        },
      },
    ],
  };

  constructor(
    private activatedRoute: ActivatedRoute,
    private userService: UserService,
    private toastService: ToastService,
    private location: Location
  ) {}

  ngOnInit() {
    this.activatedRoute.params.subscribe((params) => {
      if (params?.id) {
        this.getTeamById(params?.id);
      }
    });
  }

  getTeamById = async (id) => {
    try {
      this.showLoader = true;

      const team: any = await this.userService.getTeamById(id);
      this.mock_team = team?.data?.team;
      this.mock_members = team?.data?.members;
      this.showLoader = false;
    } catch (error) {
      this.showLoader = false;
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };
  cancel() {
    this.location.back();
  }
}
