import { Component, OnDestroy, OnInit } from "@angular/core";
import { Router } from "@angular/router";

import { TournamentService, UserService } from "../../../core/service";
import { ToastService } from "../../../shared/toast/toast.service";

import { AppHtmlRoutes } from "../../../app-routing.model";
import { IPagination, IUser } from "../../../shared/models";
import { Subscription } from "rxjs";

@Component({
  selector: 'app-my-tournament',
  templateUrl: './my-tournament.component.html',
  styleUrls: ['./my-tournament.component.scss'],
})
export class MyTournamentComponent implements OnInit, OnDestroy {
  AppHtmlRoutes = AppHtmlRoutes;

  isDraft: boolean = false;
  isHidden: boolean = false;
  isLoaded: boolean = false;

  tournamentList = [];
  page: IPagination;
  user: IUser;
  type;
  cPagination;

  tab = 'upcoming';

  pagination = {
    page: 1,
    limit: 5,
    projection: ['_id', 'name', 'description', 'startDate'],
  };

  userSubscription: Subscription;

  constructor(
    private router: Router,
    private toastService: ToastService,
    private tournamentService: TournamentService,
    private userService: UserService
  ) {}

  ngOnInit(): void {
    this.type = this.router.url.split('/').reverse()[0];
    this.getCurrentUserDetails();
  }

  ngOnDestroy() {
    this.userSubscription?.unsubscribe();
  }

  getCurrentUserDetails() {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.user = data;
        this.cPagination = Object.assign({}, this.pagination);
        this.fetchTournaments(this.type, this.tab);
      }
    });
  }

  /**
   * Get tournament list
   */

  fetchTournaments = async (type, tab) => {
    try {
      this.type = type;
      this.tab = tab;
      this.isDraft = tab == 'draft';
      this.tournamentList = [];
      let tournament;

      if (type === 'joined') {
        this.isLoaded = false;
        const payload = await this.getJoinedTournamentQuery(tab);
        tournament = await this.tournamentService
          .getParticipantTournament(payload)
          .toPromise();
        tournament ? (this.isLoaded = true) : (this.isLoaded = false);
      } else if (type === 'created') {
        this.isLoaded = false;
        const payload = await this.getCreatedTournamentQuery(tab);
        tournament = await this.tournamentService.fetchMyTournament(payload);
        tournament ? (this.isLoaded = true) : (this.isLoaded = false);
      }
      this.page = {
        totalItems: tournament?.data?.totalDocs,
        itemsPerPage: tournament?.data?.limit,
        maxSize: 5,
      };
      this.tournamentList = tournament.data.docs || [];
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  beforeTabChange(event) {
    const navtab = event.nextId.split('-');
    this.cPagination = Object.assign({}, this.pagination);
    if (navtab[0] == 'created' && navtab[1] == 'ongoing') {
      this.isHidden = true;
    } else if (navtab[0] == 'created' && navtab[1] == 'past') {
      this.isHidden = true;
    } else if (navtab[0] == 'joined' && navtab[1] == 'ongoing') {
      this.isHidden = true;
    } else if (navtab[0] == 'joined' && navtab[1] == 'past') {
      this.isHidden = true;
    } else {
      this.isHidden = false;
    }
    this.fetchTournaments(navtab[0], navtab[1]);
  }

  pageChanged(page): void {
    this.cPagination.page = page;
    this.fetchTournaments(this.type, this.tab);
  }

  getJoinedTournamentQuery(tab) {
    switch (true) {
      case tab === 'upcoming': //upcoming
        return {
          pagination: JSON.stringify(this.cPagination),
          type: 0,
        };
      case tab === 'ongoing': //ongoing
        return {
          pagination: JSON.stringify(this.cPagination),
          type: 1,
          isEdit: true,
        };
      case tab === 'past': //past
        return {
          pagination: JSON.stringify(this.cPagination),
          type: 2,
          isEdit: true,
        };
    }
  }

  getCreatedTournamentQuery(tab) {
    switch (true) {
      case tab === 'upcoming':
        return {
          page: this.cPagination.page,
          status: '0',
          limit: 5,
        };
      case tab === 'ongoing':
        return {
          page: this.cPagination.page,
          status: '1',
          limit: 5,
        };
      case tab === 'past':
        return {
          page: this.cPagination.page,
          status: '2',
          limit: 5,
        };
      case tab === 'draft':
        return {
          page: this.cPagination.page,
          status: '4',
          limit: 5,
        };
      case tab === 'unpublished':
        return {
          page: this.cPagination.page,
          status: '5',
          limit: 5,
        };
    }
  }
}
