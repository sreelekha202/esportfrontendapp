import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { InboxMessageReplyComponent } from "./inbox-message-reply.component";

describe("InboxMessageReplyComponent", () => {
  let component: InboxMessageReplyComponent;
  let fixture: ComponentFixture<InboxMessageReplyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [InboxMessageReplyComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InboxMessageReplyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
