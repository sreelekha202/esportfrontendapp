import { Component, OnInit } from "@angular/core";
import { DatePipe } from "@angular/common";

import { UserService, TransactionService } from "../../../core/service";
import { TranslateService } from "@ngx-translate/core";

enum MyTransactionsComponentTab {
  purchase = "PROFILE.TRANSACTIONS.TAB1",
  rewards = "PROFILE.TRANSACTIONS.TAB2",
}

@Component({
  selector: "app-my-transactions",
  templateUrl: "./my-transactions.component.html",
  styleUrls: ["./my-transactions.component.scss"],
})
export class MyTransactionsComponent implements OnInit {
  MyTransactionsComponentTab = MyTransactionsComponentTab;

  showLoader: boolean = false;

  rows = [];
  activetab = "rewards";

  rewardDetails;
  transactionDetail;

  paginationData = {
    page: 1,
    limit: 5,
    sort: { createdOn: -1 },
  };

  purchase = {
    columns: [
      { name: "Bill Number" },
      { name: "TransactionId" },
      { name: "Amount" },
      { name: "Currency code" },
      { name: "OrderId" },
      { name: "Created Date" },
      { name: "Status" },
    ],
  };

  rewards = {
    columns: [
      { name: "REWARD" },
      { name: "DESCRIPTION" },
      { name: "BALANCE" },
      { name: "STATUS" },
      { name: "DATE" },
      { name: "EXPIRY" },
    ],
  };

  constructor(
    private transactionServices: TransactionService,
    private translateService: TranslateService,
    private userService: UserService,
    public datePipe: DatePipe
  ) {}

  ngOnInit(): void {
    this.getRewardsDetails();
  }

  beforeTabChange(event) {
    if (event.nextId === "rewards") {
      this.showLoader = true;
      this.rows.length = 0;
      this.getRewardsDetails();
    } else if (event.nextId === "payment") {
      this.showLoader = true;
      this.rows.length = 0;
      this.getPaymentDetail();
    }
  }

  getRewardsDetails = async () => {
    let that = this;
    const pagination = JSON.stringify(this.paginationData);
    const data = await this.userService.getRewardTransaction({
      pagination: pagination,
    });
    this.rewardDetails = data.data;
    this.rows = data.data["docs"];
    this.rows.forEach((obj, index) => {
      let status;
      if (obj.status == 0) {
        status = this.translateService.instant("PROFILE.TRANSACTIONS.REDEEMED");
      } else if (obj.status == 1) {
        status = this.translateService.instant(
          "PROFILE.TRANSACTIONS.NOT_REDEEMED"
        );
      } else if (obj.status == 2) {
        status = this.translateService.instant(
          "PROFILE.TRANSACTIONS.PARTIAL_REDEEMED"
        );
      } else {
        status = this.translateService.instant("PROFILE.TRANSACTIONS.EXPIRED");
      }
      obj.type =
        obj.type.charAt(0).toUpperCase() + obj.type.slice(1).toLowerCase();
      obj.status = status;
      obj.reward_type = obj.reward_type + " +" + obj.reward_value;
      obj.date = that.datePipe.transform(obj.createdOn, "d MMM, yyyy");
      obj.expiryDate = that.datePipe.transform(obj.expiredOn, "d MMM, yyyy");
    });
    this.showLoader = false;
  };

  getPaymentDetail = async () => {
    const pagination = JSON.stringify(this.paginationData);
    const data = await this.transactionServices.paymentDetail({
      pagination: pagination,
    });
    this.transactionDetail = data.data;
    this.rows = data.data["docs"];
    this.rows.forEach((obj, index) => {
      if (obj.txnId === "na") {
        obj.txnId = "NA";
      }
    });
    this.showLoader = false;
  };

  setPage1(event) {
    this.paginationData.page = event.offset + 1;
    this.showLoader = true;
    this.rows.length = 0;
    this.getPaymentDetail();
  }

  setPage2(event) {
    this.paginationData.page = event.offset + 1;
    this.showLoader = true;
    this.rows.length = 0;
    this.getRewardsDetails();
  }
}
