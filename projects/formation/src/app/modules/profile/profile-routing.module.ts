import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { BasicInfoComponent } from "./basic-info/basic-info.component";
import { InboxComponent } from "./inbox/inbox.component";
import { InboxMessageComponent } from "./inbox/inbox-message/inbox-message.component";
import { MyBookmarksComponent } from "./my-bookmarks/my-bookmarks.component";
import { MyBracketComponent } from "./my-bracket/my-bracket.component";
import { MyContentComponent } from "./my-content/my-content.component";
import { MyTeamsComponent } from "./my-teams/my-teams.component";
import { MyTeamsViewComponent } from "./my-teams-view/my-teams-view.component";
import { MyTournamentComponent } from "./my-tournament/my-tournament.component";
import { MyTransactionsComponent } from "./my-transactions/my-transactions.component";
import { ProfileComponent } from "./profile.component";
import { ProfileSettingComponent } from "./profile-setting/profile-setting.component";
import { ReferralComponent } from "./app-referral/referral/referral.component";
import { MyMatchesComponent } from '../my-matches/my-matches.component';

const routes: Routes = [
  {
    path: "",
    component: ProfileComponent,
    children: [
      {
        path: "",
        redirectTo: "basic-info",
        pathMatch: "full",
        data: {
          tags: [
            {
              name: "title",
              content:
                "Complete your profile with the latest details for relevant and specific tournament matching and participation. Stay abreast with new FORMATION.gg updates sent to your inbox or mobile phone. ",
            },
          ],
          title: "My Profile | FORMATION.gg",
        },
      },
      {
        path: "basic-info",
        component: BasicInfoComponent,
        data: {
          tags: [
            {
              name: "title",
              content:
                "Complete your profile with the latest details for relevant and specific tournament matching and participation. Stay abreast with new FORMATION.gg updates sent to your inbox or mobile phone. ",
            },
          ],
          title: "My Profile | FORMATION.gg",
        },
      },
      {
        path: "teams",
        children: [
          {
            path: "",
            component: MyTeamsComponent,
          },
          {
            path: ":id",
            component: MyTeamsViewComponent,
          },
        ],
      },
      { path: "inbox", component: InboxComponent },
      {
        path: "my-content",
        component: MyContentComponent,
      },
      {
        path: "my-matches",
        component: MyMatchesComponent,
      },
      {
        path: "my-bookmarks",
        component: MyBookmarksComponent,
        data: {
          tags: [
            {
              name: "title",
              content:
                "View your saved articles and videos. Share your favourite links with your family and friends!",
            },
          ],
          title: "My Saved Bookmarks | FORMATION.gg",
        },
      },
      {
        path: "my-bracket",
        component: MyBracketComponent,
        data: {
          tags: [
            {
              name: "title",
              content:
                "Organize your own tournaments today from the games that you love! Create brackets for the tournaments created on",
            },
          ],
          title: "Organize your own tournaments today on",
        },
      },
      { path: "my-tournament/created", component: MyTournamentComponent },
      { path: "my-tournament/joined", component: MyTournamentComponent },
      {
        path: "my-transactions",
        component: MyTransactionsComponent,
        data: {
          tags: [
            {
              name: "title",
              content:
                "View past transaction and earned rewards from your activities on FORMATION.gg. Organize and join tournaments, purchase game vouchers and get rewarded on FORMATION.gg! ",
            },
          ],
          title: "View Past Transactions & Rewards|  FORMATION.gg",
        },
      },
      {
        path: "app-referral",
        component: ReferralComponent,
        data: {
          tags: [
            {
              name: "title",
              content: "Get the list of invitees ",
            },
          ],
          title: "Get the list of invitees",
        },
      },
      {
        path: "profile-setting",
        component: ProfileSettingComponent,
        data: {
          tags: [
            {
              name: "title",
              content:
                "Change and update FORMATION.gg account settings to your preference.",
            },
          ],
          title: "My Account Settings| FORMATION.gg",
        },
      },
      {
        path: "inbox/:id",
        component: InboxMessageComponent,
        data: {
          tags: [
            {
              name: "title",
              content:
                "Check your inbox for official announcements and updates from FORMATION.gg administrators and tournament organizers.",
            },
          ],
          title: "My Inbox| FORMATION.gg",
        },
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProfileRoutingModule {}
