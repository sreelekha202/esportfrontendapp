import { Router } from "@angular/router";
import { FormControl, FormGroup, Validators } from "@angular/forms";
// import { CustomValidators } from "ngx-custom-validators";
import { Component, EventEmitter, OnInit, Output } from "@angular/core";

import { MYCustomValidators } from "../../../log-reg/password-form/custom-validators";
import { UserService } from "../../../../core/service";
import { ToastService } from "../../../../shared/toast/toast.service";

@Component({
  selector: "app-change-password",
  templateUrl: "./change-password.component.html",
  styleUrls: ["./change-password.component.scss"],
})
export class ChangePasswordComponent implements OnInit {
  @Output() changePassword = new EventEmitter();
  isChangePassword = false;

  oldPassword = new FormControl("", [
    Validators.required,
    // check whether the entered password has a number
    MYCustomValidators.patternValidator(/\d/, { hasNumber: true }),
    // check whether the entered password has upper case letter
    MYCustomValidators.patternValidator(/[A-Z]/, { hasCapitalCase: true }),
    // check whether the entered password has a lower case letter
    MYCustomValidators.patternValidator(/[a-z]/, { hasSmallCase: true }),
    // check whether the entered password has a special character
    MYCustomValidators.patternValidator(
      /[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/,
      { hasSpecialCharacters: true }
    ),
    Validators.minLength(8),
  ]);
  error = "";
  password = new FormControl("", [
    Validators.required,
    // check whether the entered password has a number
    MYCustomValidators.patternValidator(/\d/, { hasNumber: true }),
    // check whether the entered password has upper case letter
    MYCustomValidators.patternValidator(/[A-Z]/, { hasCapitalCase: true }),
    // check whether the entered password has a lower case letter
    MYCustomValidators.patternValidator(/[a-z]/, { hasSmallCase: true }),
    // check whether the entered password has a special character
    MYCustomValidators.patternValidator(
      /[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/,
      { hasSpecialCharacters: true }
    ),
    Validators.minLength(8),
  ]);
  certainPassword = new FormControl(
    "",
    // CustomValidators.equalTo(this.password)
  );

  form = new FormGroup({
    oldPassword: this.oldPassword,
    password: this.password,
    certainPassword: this.certainPassword,
  });

  constructor(
    private userService: UserService,
    public toastService: ToastService,
    private router: Router
  ) {}

  ngOnInit(): void {}

  onVisibilityToggle(el: HTMLInputElement): void {
    el.type = el.type === "text" ? "password" : "text";
  }

  onSubmitEvent(): void {
    this.error = "";
    const updatePassword = {
      old_password: this.form.value.oldPassword,
      new_password: this.form.value.password,
    };

    this.userService.changePassword(updatePassword).subscribe(
      (data) => {
        this.error = "";
        this.toastService.showSuccess(data.message);
        this.changePassword.emit();
      },
      (error) => {
        this.error = error.error.message;
      }
    );
  }

  goBack() {
    this.router
      .navigateByUrl("/home", { skipLocationChange: true })
      .then(() => {
        this.router.navigate(["/profile/profile-setting"]);
      });
  }
}
