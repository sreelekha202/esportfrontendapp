import {
  Component,
  OnDestroy,
  OnInit,
  TemplateRef,
  ViewChild,
} from "@angular/core";
import { FormBuilder, Validators } from "@angular/forms";
import { Router } from "@angular/router";

import { ToastService } from "../../../shared/toast/toast.service";
import { UserService, UserPreferenceService } from "../../../core/service";

import { VerificationFormComponentData } from "../../log-reg/verification-form/verification-form.component";
import {
  PhoneNoFormComponentDataItem,
  PhoneNoFormComponentOutputData,
} from "../../log-reg/phone-no/phone-no.component";

import { GlobalUtils } from "../../../shared/service/global-utils/global-utils";
import { environment } from "../../../../environments/environment";
import { IUser } from "../../../shared/models";

import { NgbModal, NgbModalConfig } from "@ng-bootstrap/ng-bootstrap";
import { SwiperConfigInterface } from "ngx-swiper-wrapper";

import {
  SocialAuthService,
  FacebookLoginProvider,
  GoogleLoginProvider,
  SocialUser,
} from 'angularx-social-login';

import {
  faFacebookF,
  faTwitter,
  faGoogle,
} from "@fortawesome/free-brands-svg-icons";

import {
  faDesktop,
  faMobileAlt,
  faCheck,
  faGamepad,
  faEdit,
} from "@fortawesome/free-solid-svg-icons";
import { Subscription } from "rxjs";

enum ProfileSettingComponentTab {
  content = "PROFILE.PROFILE_SETTING.TAB2",
  account = "PROFILE.PROFILE_SETTING.TAB1",
  payment = "PROFILE.PROFILE_SETTING.TAB3",
}

declare var $: any;
@Component({
  selector: 'app-profile-setting',
  templateUrl: './profile-setting.component.html',
  styleUrls: ['./profile-setting.component.scss'],
  providers: [NgbModalConfig, NgbModal],
})
export class ProfileSettingComponent implements OnInit, OnDestroy {
  @ViewChild('EmailContent')
  private EmailContent: TemplateRef<any>;

  ProfileSettingComponentTab = ProfileSettingComponentTab;

  isChangePassword: boolean = false;
  isDesktopPlatform: boolean = true;
  isDisabled: boolean = true;
  isLoading: boolean = false;
  loading: boolean = false;
  stepTwo: boolean = false;

  currentUser: IUser;
  user: IUser;

  emailOutputData: PhoneNoFormComponentOutputData;

  game = [];
  genre = [];
  platform = [];
  prefer = [];

  error = '';
  input = '';
  ngxPlaceholder = '5XXXXXXXX';
  selectedPrefrence = 'tab1';

  faCheck = faCheck;
  faDesktop = faDesktop;
  faEdit = faEdit;
  faFacebookF = faFacebookF;
  faGamepad = faGamepad;
  faGoogle = faGoogle;
  faMobileAlt = faMobileAlt;
  faTwitter = faTwitter;

  swiperConfig: SwiperConfigInterface = {
    width: 150,
    spaceBetween: 30,
    navigation: true,
  };

  social = {
    facebook: false,
    google: false,
  };

  emailData: PhoneNoFormComponentDataItem = {
    title: 'PROFILE.PROFILE_SETTING.ACCOUNT.MODAL.TITLE1',
    subtitle: 'LOG_REG.LOGIN.SUBTITLE1',
    submitBtn: 'BUTTON.NEXT',
    cancelBtn: 'BUTTON.CANCEL',
    InputName: 'email',
    InputType: 'email',
    InputPlaceholder: 'LOG_REG.firstInputPlaceholder1',
    InputIcon: 'email',
    error: '',
  };

  emailVerification: VerificationFormComponentData = {
    title: 'LOG_REG.LOGIN.TITLE2',
    subtitle: 'PROFILE.PROFILE_SETTING.ACCOUNT.MODAL.SUBTITLE1',
    footerTitle: 'LOG_REG.FORGOT.footerTitle',
    footerLinkTitle: 'LOG_REG.FORGOT.footerLinkTitle',
    timerMs: 0,
    submitBtn: 'BUTTON.NEXT',
    error: '',
  };

  paymentInfo = {
    paypal: 'paypal',
  };

  selected = {
    type: '',
    paymentAccountId: '',
  };

  form = this.formBuilder.group({
    paymentType: ['', Validators.required],
    paymentAccountId: ['', Validators.required],
  });

  userSubscription: Subscription;

  constructor(
    config: NgbModalConfig,
    private authService: SocialAuthService,
    private formBuilder: FormBuilder,
    private modalService: NgbModal,
    private router: Router,
    private userService: UserService,
    public preference: UserPreferenceService,
    public toastService: ToastService
  ) {
    config.backdrop = 'static';
    config.keyboard = false;
    config.centered = true;
    config.backdropClass = 'dark-modal';
  }

  ngOnInit(): void {
    if (
      GlobalUtils.isBrowser() &&
      localStorage.getItem(environment.currentToken)
    ) {
      this.getUserData();
      this.getAllPreferedData();
    } else {
      this.router.navigate(['/']);
    }
  }

  ngAfterViewInit() {
    this.updateBootstrapSelect();
  }

  ngOnDestroy() {
    this.modalService.dismissAll();
    this.userSubscription?.unsubscribe();
  }

  getUserData() {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.currentUser = data;
        this.user = { ...this.currentUser };

        let that = this;
        this.user.identifiers.forEach(function (value) {
          if (value.idenType === 'GOOGLE') {
            that.social.google = true;
          }
          if (value.idenType === 'FACEBOOK') {
            that.social.facebook = true;
          }
        });
      }
    });
  }

  getAllPreferedData() {
    let that = this;
    this.isLoading = true;
    this.preference.getAllSettingPageData().subscribe(
      (data) => {
        this.isLoading = false;
        this.game = data.data.game;
        this.genre = data.data.genre;
        this.prefer = data.data.prefer;
        this.platform = data.data.platform;
        this.preference.getLoginPrefrence().subscribe(
          (data) => {
            if (data.data.game) {
              data.data.game.forEach(function (value) {
                let index = that.game.findIndex((x) => x._id === value);

                that.game[index].isChecked = true;
              });
            }
            if (data.data.gamegenre) {
              data.data.gamegenre.forEach(function (value) {
                let index = that.genre.findIndex((x) => x._id === value);

                that.genre[index].isChecked = true;
              });
            }
            if (data.data.prefercontent) {
              data.data.prefercontent.forEach(function (value) {
                let index = that.prefer.findIndex((x) => x._id === value);

                that.prefer[index].isChecked = true;
              });
            }
            if (data.data.platform) {
              data.data.platform.forEach(function (value) {
                let index = that.platform.findIndex((x) => x._id === value);

                that.platform[index].isChecked = true;
              });
            }
          },
          (error) => {
            this.isLoading = false;
            this.error = error.error.message;
          }
        );
      },
      (error) => {
        this.error = error.error.message;
      }
    );
  }

  onToggleItem(item): void {
    item.isChecked = !item.isChecked;
  }

  changePlatform(platform) {
    let index = this.platform.findIndex((x) => x === platform);
    if (index >= 0) {
      this.platform.splice(index, 1);
    } else {
      this.platform.push(platform);
    }
  }

  savePrefrences() {
    let game = [];
    let genre = [];
    let prefer = [];
    let platform = [];

    this.game.forEach(function (value) {
      if (value.isChecked == true) {
        game.push(value._id);
      }
    });
    this.genre.forEach(function (value) {
      if (value.isChecked == true) {
        genre.push(value._id);
      }
    });
    this.prefer.forEach(function (value) {
      if (value.isChecked == true) {
        prefer.push(value._id);
      }
    });
    this.platform.forEach(function (value) {
      if (value.isChecked == true) {
        platform.push(value._id);
      }
    });
    let updateData = {
      game: game,
      genre: genre,
      prefer: prefer,
      platform: platform,
    };
    this.preference.addPrefrence(updateData).subscribe(
      (data) => {
        this.toastService.showSuccess(data.message);
        this.router
          .navigateByUrl('/home', { skipLocationChange: true })
          .then(() => {
            this.router.navigate(['/profile/profile-setting']);
          });
      },
      (error) => {
        this.error = error.error.message;
      }
    );
  }

  public socialConnect(socialProvider: string) {
    let socialPlatformProvider;
    if (socialProvider === 'facebook') {
      socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
    } else if (socialProvider === 'google') {
      socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
    }
    this.authService.signIn(socialPlatformProvider).then((socialusers) => {
      if (socialusers) {
        this.Savesresponse(socialusers.authToken, socialusers.provider);
      }
    });
  }

  Savesresponse(authToken, provider) {
    this.userService.add_social(authToken, provider).subscribe(
      (data) => {
        this.userService.refreshCurrentUser();
        this.toastService.showSuccess(data.message);
        this.router
          .navigateByUrl('/home', { skipLocationChange: true })
          .then(() => {
            this.router.navigate(['/profile/profile-setting']);
          });
      },
      (error) => {
        this.toastService.showError(error.error.message);
      }
    );
  }

  add_Email() {
    this.stepTwo = false;
    this.emailData.title = 'PROFILE.PROFILE_SETTING.ACCOUNT.MODAL.TITLE1';
    this.emailData.InputName = 'email';
    this.emailData.InputType = 'email';
    this.emailData.InputPlaceholder = 'LOG_REG.LOGIN.firstInputPlaceholder1';
    this.emailData.InputIcon = 'email';
    this.emailData.error = '';
    this.emailVerification.title = 'LOG_REG.LOGIN.TITLE2';
    this.emailVerification.subtitle =
      'PROFILE.PROFILE_SETTING.ACCOUNT.MODAL.SUBTITLE1';
    this.modalService.open(this.EmailContent, {
      size: 'md',
      centered: true,
      windowClass: 'email-modal-content',
    });
  }

  add_Phone() {
    this.stepTwo = false;
    this.emailData.title = 'PROFILE.PROFILE_SETTING.ACCOUNT.MODAL.TITLE2';
    this.emailData.InputName = 'phone';
    this.emailData.InputType = 'phone';
    this.emailData.InputPlaceholder = 'LOG_REG.LOGIN.firstInputPlaceholder';
    this.emailData.InputIcon = 'phone';
    this.emailData.error = '';
    this.emailVerification.title = 'LOG_REG.LOGIN.TITLE1';
    this.emailVerification.subtitle =
      'PROFILE.PROFILE_SETTING.ACCOUNT.MODAL.SUBTITLE2';
    this.modalService.open(this.EmailContent, {
      size: 'md',
      centered: true,
      windowClass: 'email-modal-content',
    });
  }

  addEmailNext(data) {
    this.loading = true;
    if (data) {
      if (data.type === 'email') {
        this.emailOutputData = data;

        const queryData = {
          email: data.email,
        };
        this.emailData.error = '';
        this.userService.addEmail(queryData).subscribe(
          (response) => {
            this.emailData.error = '';
            this.emailVerification.timerMs = 240;
            this.emailOutputData.mailPhone = queryData.email;
            this.emailOutputData.type = 'add_email';
            this.stepTwo = true;
            this.loading = false;
          },
          (error) => {
            this.emailData.error = error.error.message;
            this.stepTwo = false;
            this.loading = false;
          }
        );
      }
      if (data.type === 'phone') {
        this.emailOutputData = data;
        const queryData = {
          phoneNumber: data.phone?.e164Number,
        };
        this.emailData.error = '';
        this.userService.addEmail(queryData).subscribe(
          (response) => {
            this.emailData.error = '';
            this.emailVerification.timerMs = 240;
            this.emailOutputData.mailPhone = data.phone?.e164Number;
            this.emailOutputData.type = 'add_phone';
            this.stepTwo = true;
            this.loading = false;
          },
          (error) => {
            this.emailData.error = error.error.message;
            this.stepTwo = false;
            this.loading = false;
          }
        );
      }
    }
  }

  submitEmail(data) {
    this.loading = true;
    let queryData;
    if (data.otp) {
      if (data.formValue.type === 'add_email') {
        queryData = {
          type: 'email',
          otp: data.otp,
        };
      }
      if (data.formValue.type === 'add_phone') {
        queryData = {
          type: 'phone',
          otp: data.otp,
        };
      }
      this.emailVerification.error = '';
      this.userService.verifyEmail(queryData).subscribe(
        (response) => {
          this.loading = false;
          this.emailVerification.error = '';
          this.modalService.dismissAll();
          this.userService.refreshCurrentUser();
          this.toastService.showSuccess(response.message);

          this.router
            .navigateByUrl('/home', { skipLocationChange: true })
            .then(() => {
              this.router.navigate(['/profile/profile-setting']);
            });
        },
        (error) => {
          this.emailVerification.error = error.error.message;
          this.loading = false;
        }
      );
    }
  }

  onChangePassword(): void {
    this.isChangePassword = true;
  }

  onChangePasswordSubmit(): void {
    this.isChangePassword = false;
  }

  onToggleEdit() {
    this.isDisabled = !this.isDisabled;
    this.form.controls['paymentAccountId'].enable();
    this.updateBootstrapSelect();
  }

  cancel() {
    this.selected.type = this.user.accountDetail.paymentType
      ? this.user.accountDetail.paymentType
      : '';
    this.selected.paymentAccountId = this.user.accountDetail.paymentAccountId
      ? this.user.accountDetail.paymentAccountId
      : '';
    this.isDisabled = true;
    this.form.controls['paymentAccountId'].disable();
    this.updateBootstrapSelect();
  }

  private updateBootstrapSelect(): void {
    setTimeout(() => {
      // update bootstrap select
      $('.selectpicker').selectpicker('refresh');
    });
  }

  onChangePaymentType(type) {
    this.form.get('paymentAccountId').reset();
    if (type === 'stcplay') {
      this.form.get('paymentAccountId').setValidators([Validators.required]);
      this.form.get('paymentAccountId').updateValueAndValidity();
    }
    if (type === 'paypal') {
      this.form
        .get('paymentAccountId')
        .setValidators([Validators.required, Validators.email]);
      this.form.get('paymentAccountId').updateValueAndValidity();
    }
  }

  savePaymentAccount() {
    this.loading = true;
    try {
      if (this.form.invalid && !this.form.value?.id) {
        this.form.markAllAsTouched();
        this.loading = false;
        return;
      }

      const { value } = this.form;
      const paymentAccountId =
        value.paymentType === 'stcpay'
          ? value.paymentAccountId.e164Number
          : value.paymentAccountId;
      const res = this.userService
        .updatePaymentInfo({
          paymentType: value.paymentType,
          paymentAccountId: paymentAccountId,
        })
        .subscribe(
          (data) => {
            this.userService.refreshCurrentUser();
            this.selectedPrefrence = 'tab3';
            this.router
              .navigateByUrl('/home', { skipLocationChange: true })
              .then(() => {
                this.router.navigate(['/profile/profile-setting']);
              });
            this.toastService.showSuccess(data.message);
            this.loading = false;
          },
          (error) => {
            this.loading = false;
            return false;
          }
        );
    } catch (error) {}
  }

  fetchNews(evt: any) {
    this.selectedPrefrence = evt.nextId;
    this.form.controls['paymentAccountId'].disable();
    this.selected.type = this.user.accountDetail.paymentType;
    this.selected.paymentAccountId = this.user.accountDetail.paymentAccountId;
    this.updateBootstrapSelect();
  }
}
