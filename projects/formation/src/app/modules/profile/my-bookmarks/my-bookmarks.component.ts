import { Component, OnDestroy, OnInit } from "@angular/core";

import { ToastService } from "../../../shared/toast/toast.service";
import {
  LanguageService,
  UserPreferenceService,
  UserService,
  UtilsService,
} from "../../../core/service";

import { TranslateService } from "@ngx-translate/core";
import { Subscription } from "rxjs";

enum MyBookmarksComponentTab {
  article = "PROFILE.BOOKMARKS.TAB1",
  video = "PROFILE.BOOKMARKS.TAB2",
}

@Component({
  selector: "app-my-bookmarks",
  templateUrl: "./my-bookmarks.component.html",
  styleUrls: ["./my-bookmarks.component.scss"],
})
export class MyBookmarksComponent implements OnInit, OnDestroy {
  MyBookmarksComponentTab = MyBookmarksComponentTab;

  currentLang = "en";
  currLanguage = "english";

  userId;

  articleBookmarkList: Array<{}> = [];
  videoBookmarkList: Array<{}> = [];

  userSubscription: Subscription;

  constructor(
    private toastService: ToastService,
    private userPreferenceService: UserPreferenceService,
    private userService: UserService,
    public language: LanguageService,
    public translate: TranslateService,
    public utilsService: UtilsService
  ) {}

  ngOnInit(): void {
    this.language.language.subscribe((lang) => {
      if (lang) {
        this.currentLang = lang;
      }
      this.currLanguage = lang === "ms" ? "malay" : "english";
    });

    this.getCurrentUserDetails();
  }

  ngOnDestroy() {
    this.userSubscription?.unsubscribe();
  }

  getBookmarks = async () => {
    try {
      const filter = JSON.stringify({ userId: this.userId });
      const projection = "articleBookmarks,videoLibrary";
      const userPreference = await this.userPreferenceService
        .getPreferences(filter, projection)
        .toPromise();
      this.articleBookmarkList =
        userPreference?.data[0]?.articleBookmarks || [];
      this.videoBookmarkList = userPreference?.data[0]?.videoLibrary || [];
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  getCurrentUserDetails() {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.userId = data._id;
        this.getBookmarks();
      }
    });
  }

  deleteBookmark = async (id, idType) => {
    try {
      if (!this.userId) {
        this.toastService.showError("Session Expired Please Login");
        return;
      }
      const queryParam = `${idType}=${id}`;
      const userPreference = await this.userPreferenceService
        .removeBookmark(queryParam)
        .toPromise();
      this.toastService.showSuccess(userPreference?.message);
      this.getBookmarks();
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  beforeTabChange(event) {
    const navtab = event.nextId;
  }
}
