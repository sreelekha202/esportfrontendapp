import {
  Component,
  OnInit,
  ViewChild,
  TemplateRef,
  OnDestroy,
  AfterViewInit,
  Inject,
} from "@angular/core";
import { NgbModal, NgbModalConfig } from "@ng-bootstrap/ng-bootstrap";
import { Router } from "@angular/router";
import { DOCUMENT } from "@angular/common";
import { GlobalUtils } from "../../../shared/service/global-utils/global-utils";
import { environment } from "../../../../environments/environment";
import { ToastService } from "../../../shared/toast/toast.service";
import {
  AdminService,
  AuthServices,
  GameService,
  UserService,
} from "../../../core/service";
import { IUser } from "../../../shared/models";
// import momentTz from "moment-timezone";
import { Subscription } from "rxjs";
import { MatDialog } from "@angular/material/dialog";
import { AvatarUpdateComponent } from "../../../shared/popups/avatar-update/avatar-update.component";
import { TranslateService } from "@ngx-translate/core";
import { NgForm, Validators } from "@angular/forms";
declare var $: any;

@Component({
  selector: "app-basic-info",
  templateUrl: "./basic-info.component.html",
  styleUrls: ["./basic-info.component.scss"],
})
export class BasicInfoComponent implements OnInit, OnDestroy, AfterViewInit {
  currentUser: IUser;
  user: IUser;
  isDisabled = true;
  countries = [];
  states = [];
  gameList = [];
  selectedGame = [];
  hideDropdown: any;
  dropdown: any;
  txtValue: any;
  clicked: boolean = false;
  URL: string;
  closeResult: boolean;
  startDate = new Date(1990, 0, 1);
  todayDate: Date = new Date(
    new Date().setFullYear(new Date().getFullYear() - 10)
  );
  tzNames: string[];

  @ViewChild("content")
  private content: TemplateRef<any>;
  public base64textString;
  public innerWidth: any;
  userSubscription: Subscription;

  isExist = false;
  timeoutId = null;

  f: NgForm; // f is nothing but the template reference of the Template Driven Form
  @ViewChild("f") currentForm: NgForm;

  constructor(
    @Inject(DOCUMENT) private document: Document,
    private userService: UserService,
    private router: Router,
    public toastService: ToastService,
    private modalService: NgbModal,
    public avatar: MatDialog,
    config: NgbModalConfig,
    private translateService: TranslateService,
    private gameService: GameService,
    private authServices: AuthServices,
    private adminService: AdminService
  ) {
    config.backdrop = "static";
    config.keyboard = false;
    config.centered = true;
    config.backdropClass = "dark-modal";
  }
  onResize(event) {
    this.innerWidth = window.innerWidth;
  }

  ngOnInit(): void {
    this.innerWidth = window.innerWidth;
    // this.tzNames = momentTz.tz.names();
    this.userService.getAllCountries().subscribe((data) => {
      this.countries = data.countries;
      this.updateBootstrapSelect();
    });
    if (
      GlobalUtils.isBrowser() &&
      localStorage.getItem(environment.currentToken)
    ) {
      this.getUserData();
    } else {
      this.router.navigate(["/"]);
    }
  }

  filterFunction() {
    var input, filter, a, i;
    input = document.getElementById("myInput");
    filter = input.value.toUpperCase();
    this.dropdown = document.getElementById("myDropdown");
    a = this.dropdown.getElementsByTagName("a");
    for (i = 0; i < a.length; i++) {
      this.txtValue = a[i].textContent || a[i].innerText;
      if (this.txtValue.toUpperCase().indexOf(filter) > -1) {
        a[i].style.display = "";
      } else {
        a[i].style.display = "none";
      }
    }
  }

  addGame(value, i) {
    this.gameList.splice(i, 1);
    this.selectedGame.push(value);
    let game = {
      _id: value._id,
      name: value.name,
      userGameId: "",
    };
    this.user?.preference.gameDetails.push(game);
    this.hideDropdown = false;
  }
  removeGame(i, game) {
    this.gameList.splice(i, 0, game);
    this.selectedGame.splice(i, 1);
    this.user.preference.gameDetails.splice(i, 1);
  }

  async uploadAvatar() {
    try {
      const avatars = await this.getAvatars();
      const dialogAvatar = await this.avatar.open(AvatarUpdateComponent, {
        data: { avatars, userAvatar: this.user.profilePicture },
      });
      dialogAvatar.afterClosed().subscribe((res) => {
        this.base64textString = res;
      });
    } catch (error) {}
  }

  async getAvatars() {
    try {
      const res = await this.adminService.getAvatars().toPromise();
      return res?.["data"][0].avatarUrls;
    } catch (error) {}
  }

  dateformat(date) {
    return new Date(date);
  }

  complete() {
    this.isDisabled = !this.isDisabled;
    this.updateBootstrapSelect();
  }
  copyToClipboard(event) {
    const dummyElement = this.document.createElement("textarea");
    var element = document.getElementById("myDIV");
    element.classList.toggle("disableBtn");
    this.document.body.appendChild(dummyElement);
    dummyElement.value = this.URL;
    event.target.disabled = true;
    dummyElement.select();
    this.document.execCommand("copy");
    this.document.body.removeChild(dummyElement);
  }
  copyreferralCode(code) {
    const dummyElement = this.document.createElement("textarea");
    this.document.body.appendChild(dummyElement);
    dummyElement.value = code;
    dummyElement.select();
    this.document.execCommand("copy");
    this.document.body.removeChild(dummyElement);
  }
  ngOnDestroy() {
    if (this.user?.firstLogin == 1) {
      const data = {
        firstLogin: 0,
      };
      const res = this.userService.updateProfile(data).subscribe(
        () => {
          //this.userService.refreshCurrentUser();
        },
        (error) => {
          return false;
        }
      );
    }
    this.modalService.dismissAll();
    this.userSubscription?.unsubscribe();
  }

  isFullName() {
    this.currentForm.controls["fullName"].setValidators([
      Validators.required,
      Validators.minLength(3),
      Validators.maxLength(100),
      Validators.pattern(
        /^[a-zA-Z0-9\u0600-\u06FF,!@#$&()\\-`.+,/\"][\sa-zA-Z0-9\u0600-\u06FF,!@#$*%&()\\-`.+,/\"]*$/
      ),
    ]);
    this.currentForm.controls["fullName"].updateValueAndValidity();
  }

  ngAfterViewInit() {
    if (this.user && this.user.firstLogin === 1) {
      //this.openModal();
    }
  }

  getUserData() {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        if (data.preference && data.preference.gameDetails) {
          this.selectedGame = [...data.preference.gameDetails];
        }
        this.getGames(this.selectedGame);
        this.currentUser = data;
        this.user = Object.assign({}, this.currentUser);
        this.URL =
          this.document.location.origin +
          "/user/referral:" +
          data.accountDetail.referralId;
        // this.user.timezone = momentTz.tz.guess();
        this.updateBootstrapSelect();
        this.onChangeCountry(this.user.country);
      } else {
        // const newVal = this.userService.getAuthenticatedUser();
      }
    });
  }

  openModal() {
    this.modalService.open(this.content, {
      size: "md",
      centered: true,
      // scrollable: true,
      windowClass: "preference-modal-content",
    });
  }

  onToggleEdit() {
    // this.userService.refreshCurrentUser();
    this.isDisabled = !this.isDisabled;
    this.updateBootstrapSelect();
  }

  cancel() {
    this.userService.refreshCurrentUser();
    this.isDisabled = true;
    this.user = Object.assign({}, this.currentUser);
    this.onChangeCountry(this.user.country);
    this.updateBootstrapSelect();
    if (this.base64textString != "") {
      this.base64textString = "";
    }
    this.removeGameWithEmptyUserGameId();
    $(".searchDropdown")
      .focusout(() => {
        this.hideDropdown = false;
      })
      .trigger("focusout");
  }

  //remove game if the userGameId is empty
  removeGameWithEmptyUserGameId() {
    this.user.preference.gameDetails = this.user.preference.gameDetails.filter(
      (game) => {
        const index = this.user.preference.gameDetails.indexOf(game._id);
        if (game.userGameId == "") {
          this.selectedGame.splice(index, 1);
        }
        return game.userGameId != "";
      }
    );
  }
  //convert image to binary
  // handleFileSelect(evt) {
  //   var files = evt.target.files;
  //   var file = files[0];

  //   if (files && file) {
  //     var reader = new FileReader();

  //     reader.onload = this._handleReaderLoaded.bind(this);

  //     reader.readAsBinaryString(file);
  //   }
  // }
  // _handleReaderLoaded(readerEvt) {
  //   var binaryString = readerEvt.target.result;
  //   this.base64textString = "data:image/png;base64," + btoa(binaryString);
  // }

  isUniqueName = async (name) => {
    try {
      const isAvailable = async () => {
        try {
          const response = await this.authServices
            .searchUsername(name, this.user._id)
            .toPromise();
          this.isExist = response.data?.isExist;
          if (this.isExist) {
            this.currentForm.form.controls["username"].setErrors({
              incorrect: true,
            });
          } else {
            this.currentForm.controls["username"].setErrors(null);
            this.currentForm.controls["username"].setValidators([
              Validators.required,
              Validators.minLength(3),
              Validators.pattern(/^[a-zA-Z0-9_]+$/),
            ]);
            this.currentForm.controls["username"].updateValueAndValidity();
          }
        } catch (error) {
          throw error;
        } finally {
          clearTimeout(this.timeoutId);
        }
      };
      if (!this.timeoutId) {
        this.timeoutId = setTimeout(isAvailable, 500);
      } else {
        clearTimeout(this.timeoutId);
        this.timeoutId = setTimeout(isAvailable, 500);
      }
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  saveProfile() {
    try {
      this.currentForm.controls["username"].setValidators([
        Validators.required,
        Validators.minLength(3),
        Validators.pattern(/^[a-zA-Z0-9_]+$/),
      ]);
      this.currentForm.controls["fullName"].setValidators([
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(100),
        Validators.pattern(
          /^[a-zA-Z0-9\u0600-\u06FF,!@#$&()\\-`.+,/\"][\sa-zA-Z0-9\u0600-\u06FF,!@#$*%&()\\-`.+,/\"]*$/
        ),
      ]);
      this.currentForm.controls["fullName"].updateValueAndValidity();
      this.currentForm.controls["username"].updateValueAndValidity();
      if (this.currentForm.invalid) {
        let error = [];
         if (this.currentForm.controls["username"]?.errors?.required) {
           error.push(
             this.translateService.instant(
               "PROFILE.BASIC_INFO.FORM.NICKNAME.ERROR"
             )
           );
         }
         if (this.currentForm.controls["fullName"]?.errors?.required) {
           error.push(
             this.translateService.instant(
               "PROFILE.BASIC_INFO.FORM.FULL_NAME.ERROR"
             )
           );
        }
        if (this.currentForm.controls["country"]?.errors?.required) {
          error.push(
            this.translateService.instant(
              "PROFILE.BASIC_INFO.FORM.COUNTRY.ERROR"
            )
          );
        }
        if (this.currentForm.controls["state"]?.errors?.required) {
          error.push(
            this.translateService.instant("PROFILE.BASIC_INFO.FORM.STATE.ERROR")
          );
        }
        if (this.currentForm.controls["postalCode"]?.errors?.required) {
          error.push(
            this.translateService.instant(
              "PROFILE.BASIC_INFO.FORM.POSTAL_CODE.ERROR"
            )
          );
        }
        if (
          this.currentForm.controls["username"]?.errors?.minlength ||
          this.currentForm.controls["fullName"]?.errors?.minlength
        ) {
          error.push(
            this.translateService.instant("LOG_REG.LOGIN.VALIDATION3")
          );
        }
        if (this.currentForm.controls["username"]?.errors?.pattern) {
          error.push(
            this.translateService.instant("LOG_REG.LOGIN.VALIDATION5")
          );
        }
        if (this.currentForm.controls["fullName"]?.errors?.maxLength) {
          error.push(
            this.translateService.instant("LOG_REG.LOGIN.VALIDATION4")
          );
        }
        if (this.currentForm.controls["fullName"]?.errors?.pattern) {
          error.push(
            this.translateService.instant("LOG_REG.LOGIN.VALIDATION6")
          );
        }
        this.toastService.showError(error.join("<br/>"));
        return;
      }

      if (this.isExist) {
        this.toastService.showError(
          this.translateService.instant("LOG_REG.LOGIN.ERROR9")
        );
        return;
      }
      if (this.base64textString == "") {
        this.base64textString = this.user.profilePicture;
      }
      this.removeGameWithEmptyUserGameId();
      const data = {
        fullName: this.user.fullName,
        username: this.user.username,
        profilePicture: this.base64textString,
        martialStatus: this.user.martialStatus,
        parentalStatus: this.user.parentalStatus,
        dob: this.user.dob,
        country: this.user.country,
        state: this.user.state,
        shortBio: this.user.shortBio,
        gender: this.user.gender,
        profession: this.user.profession,
        postalCode: this.user.postalCode,
        updatedBy: "user",
        gameDetails: this.user.preference.gameDetails,
      };

      const res = this.userService.updateProfile(data).subscribe(
        (data: any) => {
          if (data.messageCode == "USER_SUCCESS_002") {
            this.userService.refreshCurrentUser();
            this.toastService.showSuccess("Profile Updated");
            this.router
              .navigateByUrl("/home", { skipLocationChange: true })
              .then(() => {
                this.router.navigate(["/profile/basic-info"]);
              });
          } else {
            this.toastService.showError(data.message);
            for (let index = 0; index < data.data?.data?.length; index++) {
              if (
                this.selectedGame[index].name === data.data.data[index].name &&
                data.data.data[index].valid == false
              ) {
                this.selectedGame[index].errorMsg =
                  data.data.data[index].format;
              } else {
                delete this.selectedGame[index].errorMsg;
              }
            }
          }
        },
        (error) => {
          return false;
        }
      );
    } catch (err) {
      this.toastService.showError(err);
    }
  }

  private updateBootstrapSelect(): void {
    setTimeout(() => {
      // update bootstrap select
      $(".selectpicker").selectpicker("refresh");
    });
  }

  getGames(existingGame) {
    const encodeUrl = `?query=${encodeURIComponent(
      JSON.stringify({ isTournamentAllowed: true })
    )}`;
    existingGame.filter((game) => {
      delete game.errorMsg;
    });
    this.gameService.getAllGames().subscribe(
      (res) => {
        if (res && res.data) {
          this.gameList = res.data.filter(function (obj) {
            return !this.has(obj._id);
          }, new Set(existingGame.map((obj) => obj._id)));
        }
      },
      (err) => {}
    );
  }

  onChangeCountry(countryValue) {
    if (countryValue !== this.currentUser.country) {
      this.user.state = "";
    }
    this.states.length = 0;
    let countriesList = [];
    let that = this;
    this.userService.getAllCountries().subscribe((data) => {
      countriesList = data.countries;

      let index = countriesList.findIndex((x) => x.name === countryValue);
      that.userService.getStates().subscribe((data) => {
        data.states.forEach(function (value) {
          if (value.country_id == countriesList[index]?.id) {
            that.states.push(value);
          }
        });
        that.updateBootstrapSelect();
      });
    });
  }
  open(content) {
    if (this.user.accountDetail.referralId) {
      this.clicked = false;
      this.modalService
        .open(content, { ariaLabelledBy: "modal-basic-title" })
        .result.then(
          (result) => {
            this.copyToClipboard(this.URL);
          },
          (reason) => {
            this.closeResult = false;
          }
        );
    } else {
      this.toastService.showError(
        this.translateService.instant(
          "PROFILE.BASIC_INFO.REFERRAL_ERROR_MESSAGE"
        )
      );
    }
  }
}
