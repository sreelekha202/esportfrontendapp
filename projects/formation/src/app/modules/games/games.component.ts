import { Component, OnDestroy, OnInit } from "@angular/core";
import { UserService, GameService, ChatService } from "../../core/service";
import { MatDialog } from "@angular/material/dialog";
import { MatchmakerDialogComponent } from "./matchmaker-dialog/matchmaker-dialog.component";
import { IUser } from "../../shared/models";
import { Router } from "@angular/router";
import { TranslateService } from "@ngx-translate/core";
import { LanguageService } from "../../core/service";
import { AppHtmlProfileRoutes } from "../../app-routing.model";
import { Subscription } from "rxjs";

@Component({
  selector: "app-games",
  templateUrl: "./games.component.html",
  styleUrls: ["./games.component.scss"],
})
export class GamesComponent implements OnInit, OnDestroy {
  AppHtmlProfileRoutes = AppHtmlProfileRoutes;

  gameList = [];
  windowposition: String = "chat_window chat_window_right_drawer";
  currenUser: IUser;
  isUserLoggedIn = false;
  currentUserId: String = "";
  currentUserName: String = "";
  matchdetails: any;

  userSubscription: Subscription;

  constructor(
    private gameService: GameService,
    public gamesMatchmaking: MatDialog,
    private chatService: ChatService,
    private userService: UserService,
    private router: Router,
    public language: LanguageService,
    public translateService: TranslateService
  ) {}

  ngOnInit(): void {
    this.getGames();
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.currenUser = data;
        this.isUserLoggedIn = !!this.currenUser;
        this.currentUserId = this.currenUser._id;
      }
    });
  }

  ngOnDestroy() {
    this.userSubscription?.unsubscribe();
  }

  getGames() {
    this.gameService.getAllGames().subscribe(
      (res) => {
        this.gameList = res.data;
      },
      (err) => {}
    );
  }

  openDialog(game) {
    const dialogRef = this.gamesMatchmaking.open(MatchmakerDialogComponent, {
      data: game,
      panelClass: "matchMaking",
      disableClose: true,
    });

    dialogRef.afterClosed().subscribe((result) => {
      // integrate with chat
      if (!(result == null || result == "")) {
        let matchid = result.id + "-" + this.currentUserId;
        this.matchdetails = {
          matchid: matchid,
          image: result.image,
          matchName: result.matchName,
          name: result.name,
        };

        this.chatService.setWindowPos(this.windowposition);
        this.chatService.setCurrentMatch(this.matchdetails);
        this.chatService.setTypeOfChat("game");
        this.chatService.setChatStatus(true);
        if (this.chatService.getChatStatus() == false) {
          this.chatService?.disconnectMatch(matchid, "game");
        }
        this.router.navigateByUrl("/tournament/info");
      }
    });
  }
}
