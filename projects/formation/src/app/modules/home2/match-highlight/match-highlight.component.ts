import { Component, OnInit, Input } from "@angular/core";
import { UtilsService, LanguageService } from "../../../core/service";
import { EsportsTimezone } from 'esports';

@Component({
  selector: "app-match-highlight",
  templateUrl: "./match-highlight.component.html",
  styleUrls: ["./match-highlight.component.scss"],
})
export class MatchHighlightComponent implements OnInit {
  @Input() params;
  currlanguage = "english";
  timezone;

  constructor(
    public utilsService: UtilsService,
    private languageService: LanguageService,
    private esportsTimezone: EsportsTimezone
  ) {}

  ngOnInit(): void {
    this.timezone = this.esportsTimezone.getTimeZoneName();
    this.languageService.language.subscribe((lang) => {
      this.currlanguage = lang == "en" ? "english" : "malay";
    });
    this.utilsService.getLanguageWiseValue;
  }

  isNumber(val): boolean { const str: string = val.toString(); return str.startsWith("+"); }
}
