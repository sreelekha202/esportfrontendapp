import { Component, OnInit, Inject, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';

import { LanguageService } from '../../core/service';
import { ToastService } from '../../shared/toast/toast.service';

import {
  AppHtmlRoutes,
  AppHtmlRoutesArticleMoreType,
} from '../../app-routing.model';

import {
  ArticleApiService,
  HomeService,
  TournamentService,
  UtilsService,
  VideoLibraryService,
  OptionService,
} from '../../core/service';

import { leaderboardComponentDataItem } from '../leader-board/leader-board.component';
import {
  InfoPopupComponent,
  InfoPopupComponentData,
  InfoPopupComponentType,
} from '../../shared/popups/info-popup/info-popup.component';

import { TranslateService } from '@ngx-translate/core';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-home2',
  templateUrl: './home2.component.html',
  styleUrls: ['./home2.component.scss'],
})
export class Home2Component implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;
  AppHtmlRoutesArticleMoreType = AppHtmlRoutesArticleMoreType;

  isAnnouncementAvailabale: boolean = false;
  isBrowser: boolean;

  currLanguage = 'english';

  gamesCards = [];
  featureTournamentsData = [];
  tournamentsData = [];
  tournamentsSlides = [];
  trendingNews = [];
  videoLibrary = [];
  leaderboard = [];

  gameModes = [{ value: 'test1 ' }, { value: 'test2 ' }, { value: 'test3 ' }];

  activeGameMode = null;
  activeVideo = null;
  announcementData: any;
  categoryList;
  categoryId;

  gameMode = [
    {
      value: 'Game mode',
    },
  ];

  leaderboardComponentData: leaderboardComponentDataItem = {
    page: 'home',
    title: 'LEADERBOARD',
  };

  constructor(
    @Inject(PLATFORM_ID) private platformId,
    private articleService: ArticleApiService,
    private homeService: HomeService,
    private optionService: OptionService,
    private toastService: ToastService,
    private tournamentService: TournamentService,
    private videoLibraryService: VideoLibraryService,
    public language: LanguageService,
    public matDialog: MatDialog,
    public translateService: TranslateService,
    public utilsService: UtilsService
  ) {
    this.isBrowser = isPlatformBrowser(platformId);
  }

  ngOnInit(): void {
    if (this.isBrowser) {
      this.language.language.subscribe((lang) => {
        if (lang) {
          this.currLanguage = lang === 'ms' ? 'malay' : 'english';
        }
      });

      this.getAnnouncement();
      this.getBanner();
      this.getOnGoingTournaments();
      this.getTournament();
      this.getLeaderboard();
      this.getVideoLibrary();
      this.fetchOptions();
    }
  }

  getVideoLibrary = async () => {
    try {
      const pagination = {
        page: 1,
        limit: 5,
        sort: '-updatedOn',
        projection: [
          '_id',
          'title',
          'description',
          'youtubeUrl',
          'thumbnailUrl',
        ],
      };

      const query = `?pagination=${encodeURIComponent(
        JSON.stringify(pagination)
      )}`;

      const response = await this.videoLibraryService.fetchVideoLibrary(query);
      this.videoLibrary = await Promise.all(
        response?.data?.docs.slice(0, 4).map(async (item) => {
          return {
            ...item,
            youtubeUrl: await this.utilsService.getEmbbedUrl(item.youtubeUrl),
          };
        })
      );
      this.activeVideo = this.videoLibrary.length ? this.videoLibrary[0] : null;
    } catch (error) {
      this.toastService.showError(error.error?.message || error?.message);
    }
  };

  getAnnouncement() {
    this.homeService.getAnnouncements().subscribe(
      (res: any) => {
        if (res.data && res.data.length > 0) {
          let data = res.data.map((ele) => {
            return {
              uniqueId: ele._id,
              image: ele.announcementFileUrl,
              header: ele.header,
              description: ele.description,
              destination: ele.destination,
            };
          });
          this.announcementData = data[0];

          if (this.announcementData.uniqueId) {
            let itemId = localStorage.getItem('announcement');
            if (itemId != this.announcementData.uniqueId) {
              this.isAnnouncementAvailabale = true;
            } else {
              this.isAnnouncementAvailabale = false;
            }
          }
        }
      },
      (err: any) => {}
    );
  }

  getBanner() {
    this.homeService._getBanner().subscribe(
      (res: any) => (this.tournamentsSlides = res.data),
      (err: any) => {}
    );
  }

  getLeaderboard() {
    this.homeService
      ._leaderBoard()
      .subscribe(({ data: { leaderboardData: data } }) => {
        this.leaderboard = data.sort((a, b) => b.points - a.points);
      });
  }

  addEndingToPlace(place: number): string {
    switch (place) {
      case 1:
        return `1st`;
      case 2:
        return `2nd`;
      case 3:
        return `3rd`;
      default:
        return `${place}th`;
    }
  }

  getTournament() {
    this.tournamentService
      .getPaginatedTournaments({
        status: '0',
        limit: 6,
        sort: 'participantJoined',
      })
      .subscribe(
        (res: any) => {
          this.tournamentsData = [...res.data.docs, ...res.data.docs];
        },
        (err) => {
          console.error(err);
        }
      );
  }

  fetchOptions = async () => {
    const option = await Promise.all([this.optionService.fetchAllCategories()]);
    this.categoryList = option[0]?.data;
    for (const iterator of this.categoryList) {
      if (iterator.name == 'News') {
        this.categoryId = iterator._id;
      }
    }
    this.getArticleNews(this.categoryId);
  };

  getArticleNews(categoryId) {
    const query = JSON.stringify({
      articleStatus: 'publish',
      category: categoryId,
    });
    const option = JSON.stringify({ limit: 7, sort: { createdDate: -1 } });
    this.articleService.getArticles_PublicAPI({ query, option }).subscribe(
      (res: any) => {
        this.trendingNews = res.data.filter((_, idx) => idx < 5);
      },
      (err) => {}
    );
  }

  getOnGoingTournaments() {
    this.tournamentService
      .getPaginatedTournaments({
        status: '6',
        limit: 5,
        sort: 'participantJoined',
      })
      .subscribe(
        (res: any) => {
          this.featureTournamentsData = res.data.docs;
        },
        (err) => {
          console.error(err);
        }
      );
  }

  getRegistrationStatus(
    startDate,
    startTime,
    isSeeded,
    isFinished,
    maxParticipants,
    participantsJoined
  ) {
    if (!startDate || !startTime) {
      return;
    }
    let timeObj = this.convert12HrsTo24HrsFormat(startTime);
    let sDate = new Date(startDate);

    sDate.setHours(timeObj['hour']);
    sDate.setMinutes(timeObj['minute']);

    if (isSeeded == true || isFinished == true) {
      return 'Closed';
    } else if (participantsJoined >= maxParticipants) {
      return 'Closed';
    } else if (sDate.getTime() < Date.now()) {
      return 'Closed';
    } else {
      return 'Open';
    }
  }

  convert12HrsTo24HrsFormat(time) {
    if (!time) {
      return;
    }
    var hours = Number(time.match(/^(\d+)/)[1]);
    var minutes = Number(time.match(/:(\d+)/)[1]);
    var AP = time.match(/\s(.*)$/);
    if (!AP) AP = time.slice(-2);
    else AP = AP[1];
    if (AP == 'PM' && hours < 12) hours = hours + 12;
    if (AP == 'AM' && hours == 12) hours = hours - 12;
    var Hours24 = hours.toString();
    var Minutes24 = minutes.toString();
    if (hours < 10) Hours24 = '0' + Hours24;
    if (minutes < 10) Minutes24 = '0' + Minutes24;

    return { hour: parseInt(Hours24), minute: parseInt(Minutes24) };
  }

  openSocialShareComponent() {
    const data: InfoPopupComponentData = {
      title: `Share Tournament via`,
      text: ``,
      type: InfoPopupComponentType.socialSharing,
      cancelBtnText: 'Close',
    };

    this.matDialog.open(InfoPopupComponent, {
      data,
    });
  }
}
