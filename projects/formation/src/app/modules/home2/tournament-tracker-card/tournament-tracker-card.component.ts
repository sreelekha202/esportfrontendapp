import { Component, OnInit, Input } from "@angular/core";

interface Card {
  tournamentName: string;
  gamer1Name: string;
  gamer2Name: string;
  score1: number | string;
  score2: number | string;
  city: string;
}

@Component({
  selector: "app-tournament-tracker-card",
  templateUrl: "./tournament-tracker-card.component.html",
  styleUrls: ["./tournament-tracker-card.component.scss"],
})
export class TournamentTrackerCardComponent implements OnInit {
  @Input() params: any;
  winnerIndex;
  maxTeam;

  constructor() {}

  ngOnInit(): void {
    if (!this.params?.teamA && !this.params?.teamB) {
      let maxScore = 0;
      for (const team of this.params.battleRoyalTeams) {
        if (maxScore < team.score) {
          maxScore = team.score;
          this.maxTeam = { ...team };
        }
      }
    }
  }
}
