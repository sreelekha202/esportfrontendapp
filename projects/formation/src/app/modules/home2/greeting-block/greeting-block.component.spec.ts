import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { GreetingBlockComponent } from "./greeting-block.component";

describe("GreetingBlockComponent", () => {
  let component: GreetingBlockComponent;
  let fixture: ComponentFixture<GreetingBlockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GreetingBlockComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GreetingBlockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
