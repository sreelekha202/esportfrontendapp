import { AppHtmlRoutes } from '../../../app-routing.model';
import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { NgbCarousel, NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
import { LanguageService } from '../../../core/service';

@Component({
  selector: 'app-greeting-block',
  templateUrl: './greeting-block.component.html',
  styleUrls: ['./greeting-block.component.scss'],
  providers: [NgbCarouselConfig],
})
export class GreetingBlockComponent implements OnInit {
  @Input() slides: any[];
  @ViewChild('carousel') carousel: NgbCarousel;
  AppHtmlRoutes = AppHtmlRoutes;

  public currentLang: string = 'english';
  bannerList: any = [];
  nextSlide = null;

  constructor(
    private config: NgbCarouselConfig,
    private translate: TranslateService,
    public language: LanguageService
  ) {
    this.config.interval = 60000000;
    config.showNavigationArrows = true;
    config.showNavigationIndicators = true;
    this.currentLang = translate.currentLang == 'ms' ? 'malay' : 'english';

    translate.onLangChange.subscribe((event: LangChangeEvent) => {
      this.currentLang = event.lang == 'ms' ? 'malay' : 'english';
    });
  }

  ngOnChanges() {
    this.bannerList = this.slides;
  }

  /**
   * destination url
   * @param obj
   */
  redirectLink(obj) {
    window.location.href = obj;
  }

  ngOnInit(): void {
    this.nextSlide = this.bannerList[1];
  }

  onToggleSlide(value: any) {
    const activeIndex = +value.activeId.split('-')[2];
    const maxIndex = this.bannerList.length - 1;

    if (activeIndex === maxIndex) {
      this.nextSlide = this.bannerList[0];
    }

    if (activeIndex < maxIndex) {
      this.nextSlide = this.bannerList[activeIndex + 1];
    }
  }
}
