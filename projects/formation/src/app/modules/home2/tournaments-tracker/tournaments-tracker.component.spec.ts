import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { TournamentsTrackerComponent } from "./tournaments-tracker.component";

describe("TournamentsTrackerComponent", () => {
  let component: TournamentsTrackerComponent;
  let fixture: ComponentFixture<TournamentsTrackerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TournamentsTrackerComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TournamentsTrackerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
