import { Component, Input, OnInit } from "@angular/core";

@Component({
  selector: "app-announcement",
  templateUrl: "./announcement.component.html",
  styleUrls: ["./announcement.component.scss"],
})
export class AnnouncementComponent implements OnInit {
  constructor() {}
  @Input() data: any;
  @Input() showBox: any;
  ngOnInit(): void {}
  closeModal() {
    localStorage.setItem("announcement", this.data.uniqueId);
    this.showBox = false;
  }

  /**
   * destination url
   * @param obj
   */
  redirectLink(obj) {
    this.closeModal();
    window.location.href = obj;
  }
}
