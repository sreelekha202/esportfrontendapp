import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-games-cards',
  templateUrl: './games-cards.component.html',
  styleUrls: ['./games-cards.component.scss'],
})
export class GamesCardsComponent implements OnInit {
  idx: number = 0;

  cards = [
    {
      id: this.idx++,
      name: 'All content',
      image: 'https://a.radikal.ru/a01/2105/28/d68c124ba3ea.png',
      isSelected: true,
    },
  ];

  responsiveOptions = [
    {
      breakpoint: '1199px',
      numVisible: 5,
      numScroll: 5,
    },
    {
      breakpoint: '767px',
      numVisible: 4,
      numScroll: 4,
    },
    {
      breakpoint: '575px',
      numVisible: 3,
      numScroll: 3,
    },
  ];

  constructor() {}

  ngOnInit(): void {
    // MOCK DATA
    this.cards.push(
      ...[
        {
          id: this.idx++,
          name: 'Rocket League',
          image: 'https://a.radikal.ru/a29/2104/f1/d9bf552c9ef0.png',
          isSelected: false,
        },
        {
          id: this.idx++,
          name: 'Overwatch',
          image: 'https://a.radikal.ru/a29/2104/7d/503f13d46dd8.png',
          isSelected: false,
        },
        {
          id: this.idx++,
          name: 'LoL',
          image: 'https://b.radikal.ru/b14/2104/20/1e46b943d01c.png',
          isSelected: false,
        },
        {
          id: this.idx++,
          name: 'FiFA21',
          image: 'https://c.radikal.ru/c02/2104/ec/824fb1875003.png',
          isSelected: false,
        },
        {
          id: this.idx++,
          name: 'DOTA2',
          image: 'https://b.radikal.ru/b18/2104/94/a57b0633bcbb.png',
          isSelected: false,
        },
        {
          id: this.idx++,
          name: 'Valorant',
          image: 'https://d.radikal.ru/d09/2104/75/c0642ae0062c.png',
          isSelected: false,
        },
        {
          id: this.idx++,
          name: 'Fortnine',
          image: 'https://b.radikal.ru/b37/2104/79/d141bdab2c52.png',
          isSelected: false,
        },
        {
          id: this.idx++,
          name: 'CS:GO',
          image: 'https://c.radikal.ru/c09/2104/2a/5fe5ed585992.png',
          isSelected: false,
        },
        {
          id: this.idx++,
          name: 'Call of Duty',
          image: 'https://d.radikal.ru/d27/2104/58/62c97ccc6af9.png',
          isSelected: false,
        },
        {
          id: this.idx++,
          name: 'Rocket League',
          image: 'https://a.radikal.ru/a29/2104/f1/d9bf552c9ef0.png',
          isSelected: false,
        },
        {
          id: this.idx++,
          name: 'Overwatch',
          image: 'https://a.radikal.ru/a29/2104/7d/503f13d46dd8.png',
          isSelected: false,
        },
        {
          id: this.idx++,
          name: 'LoL',
          image: 'https://b.radikal.ru/b14/2104/20/1e46b943d01c.png',
          isSelected: false,
        },
        {
          id: this.idx++,
          name: 'FiFA21',
          image: 'https://c.radikal.ru/c02/2104/ec/824fb1875003.png',
          isSelected: false,
        },
        {
          id: this.idx++,
          name: 'DOTA2',
          image: 'https://b.radikal.ru/b18/2104/94/a57b0633bcbb.png',
          isSelected: false,
        },
        {
          id: this.idx++,
          name: 'Valorant',
          image: 'https://d.radikal.ru/d09/2104/75/c0642ae0062c.png',
          isSelected: false,
        },
        {
          id: this.idx++,
          name: 'Fortnine',
          image: 'https://b.radikal.ru/b37/2104/79/d141bdab2c52.png',
          isSelected: false,
        },
        {
          id: this.idx++,
          name: 'CS:GO',
          image: 'https://c.radikal.ru/c09/2104/2a/5fe5ed585992.png',
          isSelected: false,
        },
        {
          id: this.idx++,
          name: 'Call of Duty',
          image: 'https://d.radikal.ru/d27/2104/58/62c97ccc6af9.png',
          isSelected: false,
        },
      ]
    );
  }

  onSelectGame(id: number): void {
    this.cards.forEach((game) => {
      game.isSelected = game.id === id;
    });
  }
}
