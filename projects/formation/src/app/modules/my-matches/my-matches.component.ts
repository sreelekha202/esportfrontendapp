import { Component, OnInit, OnDestroy } from "@angular/core";
import { UserReportsService } from "../../core/service/user-reports.service";
import { Subscription } from "rxjs";
import { UserService } from "../../core/service";
@Component({
  selector: "app-my-matches",
  templateUrl: "./my-matches.component.html",
  styleUrls: ["./my-matches.component.scss"],
})
export class MyMatchesComponent implements OnInit, OnDestroy {
  mock_cards = [];
  isLoaded = false;
  isAdmin = false;
  userSubscription: Subscription;

  constructor(
    private UserReportsService: UserReportsService,
    private userService: UserService
  ) {}

  ngOnInit(): void {
    this.getCurrentUserDetails();
    this.getMyMatches();
  }

  ngOnDestroy() {
    this.userSubscription?.unsubscribe();
  }

  getMyMatches() {
    this.isLoaded = true;
    this.UserReportsService.getMyMatches().subscribe(
      (res) => {
        const data = res.data;
        for (let index = 0, len = data.length; index < len; index++) {
          const element = data[index];
          if (
            element?.match?.currentMatch?.round &&
            element?.opponentPlayer?.teamName
          ) {
            this.mock_cards.push({
              title: element?.match?.currentMatch?.round
                ? `Round ${element?.match?.currentMatch?.round}`
                : 'N/A',
              opponentName: element?.opponentPlayer?.teamName || 'N/A',
              gameAccount: element?.opponentPlayer?.inGamerUserId || 'N/A',
              image: element?.match?.tournamentId?.gameDetail?.logo || 'N/A',
              tournamentId: element?.match?.tournamentId?._id || '',
              tournamentSlug: element?.match?.tournamentId?.slug || '',
              matchId: element?.match?._id || '',
              participantId: element?.userParticepentId || '',
              match: element?.match,
              isAdmin: this.isAdmin,
              tournamentName: element?.match?.tournamentId?.name
                ? element?.match?.tournamentId?.name.length > 28
                  ? `${element?.match?.tournamentId?.name
                      .toLowerCase()
                      .substring(0, 28)
                      .slice(0, -3)}...`
                  : element?.match?.tournamentId?.name.toLowerCase()
                : '',
            });
          }
        }
        this.isLoaded = false;
      },
      (err) => {
        this.isLoaded = false;
      }
    );
  }

  getCurrentUserDetails() {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        if (data?.accountType === "admin") {
          this.isAdmin = true;
        }
      }
    });
  }
}
