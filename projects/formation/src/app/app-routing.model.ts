import { MetaDefinition } from "@angular/platform-browser";

export interface AppRoutesData {
  isRootPage?: boolean;
  tags?: MetaDefinition[];
  title?: string;
}

export enum AppHtmlRoutes {
  aboutUs = "/about-us",
  admin = "/admin",
  article = "/article",
  articleCreate = "/article/create",
  articleCreateVideoLibrary = "/article/create-video-library",
  articleEdit = "/article/edit",
  articleMore = "/article/more",
  articleNews = "/article/news",
  bracket = "/bracket",
  bracketCreate = "/bracket/create",
  createTeam = "/create-team",
  dec = "/dec",
  games = "/games",
  home = "/home",
  leaderBoard = "/leaderboard",
  login = "/login",
  loginEmail = "/login-email",
  manageTeam = "/manage-team/",
  notFound = "/404",
  privacyPolicy = "/privacy-policy",
  register = "/register",
  registerEmail = "/register-email",
  search = "/search",
  searchArticle = "/search/article",
  searchShop = "/search/shop",
  searchTournament = "/search/tournament",
  searchVideo = "/search/video",
  specialOffer = "/special-offer",
  topUp = "/top-up",
  tournament = "/tournament",
  tournamentCreate = "/tournament/create",
  tournamentEdit = "/tournament/edit",
  tournamentInfo = "/tournament/info",
  tournamentManage = "/tournament/manage",
  tournamentView = "/tournament",
  userPageType = "/user",
  userPhoneLogin = "/user/phone-login",
  userRegistration = "/user/registration",
  videos = "/videos",
  videosView = "/videos/view",
  view = "/view",
}

export enum AppHtmlRoutesLoginType {
  createPass = "createPass",
  emailLogin = "email-login",
  forgotPass = "forgotPass",
  phoneLogin = "phone-login",
  registration = "registration",
}

export enum AppHtmlProfileRoutes {
  basicInfo = "/profile/basic-info",
  inbox = "/profile/inbox",
  myBookmarks = "/profile/my-bookmarks",
  myBracket = "/profile/my-bracket",
  myContent = "/profile/my-content",
  myNewTournamentCreated = "/tournament/create",
  myTournamentCreated = "/profile/my-tournament/created",
  myTournamentJoined = "/profile/my-tournament/joined",
  myTransactions = "/profile/my-transactions",
  ReferralComponent = "/profile/app-referral",
  setting = "/profile/profile-setting",
  teams = "/profile/teams",
  teamsView = "/profile/teams/",
  myMatches = '/profile/my-matches',
}

export enum AppHtmlAdminRoutes {
  accessManagement = "/admin/access-management",
  contentManagement = "/admin/content-management",
  dashboard = "/admin/dashboard",
  editTeam = "/edit-team",
  esportsManagement = "/admin/esports-management",
  siteConfiguration = "/admin/site-configuration",
  teamManagement = "/admin/team-management",
  teamManagementEdit = "/admin/team-management/edit",
  userManagement = "/admin/user-management",
  userManagementView = "/admin/user-management/view",
  userNotifications = "/admin/user-notifications",
}

export enum AppHtmlRoutesArticleMoreType {
  trendingPost = "trending-post",
  allPost = "all-post",
}

export enum AppHtmlTeamRegRoutes {
  newTeam = "/team-registration",
  teammates = "/team-registration/teammates",
  registration = "/team-registration/registration",
}
