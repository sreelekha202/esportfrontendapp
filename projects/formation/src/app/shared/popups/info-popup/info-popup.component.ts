import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";
import { Component, Inject, OnInit } from "@angular/core";

export enum InfoPopupComponentType {
  info,
  confirm,
  rating,
  socialSharing,
}

export interface InfoPopupComponentData {
  btnText?: string;
  cancelBtnText?: string;
  rate?: number;
  text: string;
  title: string;
  type: InfoPopupComponentType;
}

@Component({
  selector: "app-info-popup",
  templateUrl: "./info-popup.component.html",
  styleUrls: ["./info-popup.component.scss"],
})
export class InfoPopupComponent implements OnInit {
  InfoPopupComponentType = InfoPopupComponentType;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: InfoPopupComponentData,
    public dialogRef: MatDialogRef<InfoPopupComponent>
  ) {}

  ngOnInit(): void {}

  onClose(): void {
    this.dialogRef.close();
  }

  onConfirm(): void {
    this.dialogRef.close(this.data?.rate || true);
  }

  message(value) {
    this.data.rate = value;
  }
}
