interface IGame {
  _id?: string;
  activeTournament?: number;
  image?: string;
  logo?: string;
  name?: string;
}

export { IGame };
