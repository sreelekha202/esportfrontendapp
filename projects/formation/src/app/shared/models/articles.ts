import { IGame } from "./game";
import { IUser } from "./user";

interface IArticle {
  _id?: string;
  author?: string;
  authorDetails?: IUser;
  category?: string;
  content?: string;
  createdDate?: Date;
  game?: string;
  genre?: string;
  highlight?: number;
  id?: string;
  image?: string;
  isInfluencer?: boolean;
  isInfluencerHighlight?: boolean;
  isSponsor?: boolean;
  likes?: number;
  location?: string;
  minRead?: number;
  platform?: string;
  publishDate?: Date;
  shortDescription?: string;
  slug?: string;
  status?: string;
  tags?: string;
  title?: string;
  totalLikes?: string;
  gameDetails?: IGame;
  isBookmarked?: boolean;
  bookmarkProccesing?: boolean;
  type?: number;
  likeProccesing?: boolean;
  createdBy?: string;
}

export { IArticle };
