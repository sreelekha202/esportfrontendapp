export interface IPagination {
  itemsPerPage: number;
  maxSize: number;
  totalItems: number;
}
