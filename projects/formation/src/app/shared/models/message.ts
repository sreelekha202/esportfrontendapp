import { IUser } from "./user";

export interface IMessage {
  _id: string;
  createdAt: string;
  createdBy: string;
  isChecked: boolean;
  message: string;
  seen: boolean;
  senderDetails: IUser;
  sentBy: string;
  status: number;
  subject: string;
  toUser: string;
  updatedAt: string;
}
