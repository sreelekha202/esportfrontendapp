import { IAdvertisementManagement } from "./advertisement";
import { IArticle } from "./articles";
import { IBracket } from "./bracket";
import { IChat } from "./chat";
import { IGame } from "./game";
import { IMatch, IMatchSet } from "./match";
import { IMessage } from "./message";
import { IPagination } from "./pagination";
import { IParticipant } from "./participant";
import { IReward } from "./reward";
import { ITournament } from "./tournament";
import { ITournamentDetails } from "./tournaments";
import { IUser } from "./user";
import { IVideoLibrary } from "./video-library";
import { Tmatch } from "./tmatch";

export {
  IAdvertisementManagement,
  IArticle,
  IBracket,
  IChat,
  IGame,
  IMatch,
  IMatchSet,
  IMessage,
  IPagination,
  IParticipant,
  IReward,
  ITournamentDetails,
  ITournament,
  IUser,
  IVideoLibrary,
  Tmatch,
};
