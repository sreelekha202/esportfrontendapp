export interface IVideoLibrary {
  category?: string;
  createdBy?: string;
  createdOn?: Date;
  description?: string;
  game?: string;
  genres?: Genre[];
  id?: string;
  platforms?: Platform[];
  status?: number;
  tags?: Tag[];
  thumbnailUrl?: string;
  title?: string;
  updatedBy?: string;
  updatedOn?: Date;
  youtubeUrl?: string;
}

interface Tag {
  name?: string;
  value?: string;
}

interface Platform {
  name?: string;
  value?: string;
}

interface Genre {
  name?: string;
  value?: string;
}
