export class IUser {
  _id: String; // the unique key from backend is _id
  aboutMe: String;
  accountDetail: {
    reward: Number;
    paymentType: string;
    paymentAccountId: string;
    isPaymentAccountVerified: boolean;
    referralId: string;
  };
  accessLevel: String[];
  accountType: String;
  country: string;
  coverPicture: String;
  createdOn: Date;
  dob: Date;
  email: String;
  emailisVerified: Boolean;
  emailSecond: String;
  firstLogin: Number;
  fullName: String;
  username: string;
  gamesPlayed: String;
  gender: String;
  havePin: Boolean;
  id: Number;
  identifiers: [
    {
      idenType: String;
      uuid: String;
    }
  ];
  isInfluencer: Number;
  isPassword: Boolean;
  isVerified: Number;
  martialStatus: String;
  organizerRating: Number;
  parentalStatus: String;
  password: String;
  phoneisVerified: Boolean;
  phoneNumber: String;
  postalCode: Number;
  postalcode: String;
  preference: any;
  profession: String;
  profilePicture: String;
  progress: Number;
  shortBio: String;
  state: String;
  status: Number;
  token: String;
  updatedOn: Date;
  userName: String;
  timezone: string;
  isEO?: any;
}
