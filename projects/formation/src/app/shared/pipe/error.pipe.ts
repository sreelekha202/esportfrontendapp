import { Pipe, PipeTransform } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";

@Pipe({
  name: "errorPipe",
})
export class ErrorPipe implements PipeTransform {
  errorString = {
    appStoreUrl: "ERROR.APPSTORE_URL",
    checkInExpired: "ERROR.CHECKIN_EXPIRED",
    email: "ERROR.EMAIL",
    facebookVideoUrl: "ERROR.FACEBOOK_VIDEO_URL",
    invalidUrl: "ERROR.YOUTUBE_URL_INVALID",
    isNameAvailable: "ERROR.IS_ALREADY_EXIST",
    isValidPhoneNumber: "ERROR.PHONE_NUMBER",
    max: "ERROR.MAX",
    maxlength: "ERROR.MAX_LENGTH_",
    min: "ERROR.MIN",
    multiStageConfigError: "ERROR.PERFECT_DIVISION",
    none: null,
    pastEndDate: "ERROR.PAST_END_DATE",
    pastEndTime: "ERROR.PAST_END_TIME",
    pastStartDate: "ERROR.PAST_START_DATE",
    pastStartTime: "ERROR.PAST_START_TIME",
    pastRegStartDate: "ERROR.PAST_REG_START_DATE",
    pastRegStartTime: "ERROR.PAST_REG_START_TIME",
    pastRegEndTime: "ERROR.PAST_REG_END_TIME",
    pastRegEndDate: "ERROR.PAST_REG_END_DATE",
    pattern: "ERROR.PATTERN", // UNWANTED PATTERN
    playStoreUrl: "ERROR.PLAYSTORE_URL",
    prizeLimit: "ERROR.PRIZELIMIT",
    required: "ERROR.REQUIRED",
    twitchVideoUrl: "ERROR.TWITCH_VIDEO_URL",
    wait: "ERROR.WAIT",
    website: "ERROR.WEBSITE_URL",
    youtubeUrl: "ERROR.YOUTUBE_URL",
  };

  constructor(private translateService: TranslateService) {}

  transform(value, type?): string {
    if (value) {
      const error = Object.keys(value);
      const key = error.length ? error[0] : "none";
      if (key === "maxlength") {
        return `${this.translateService.instant("ERROR.MAX")} ${
          value?.maxlength?.requiredLength || value?.max?.max
        }`;
      } else if (key === "minlength") {
        return `${this.translateService.instant("ERROR.MIN")} ${
          value?.minlength?.requiredLength || value?.min?.min
        }`;
      } else if (key === "min") {
        return `${this.translateService.instant("ERROR.MIN")} ${
          value?.min?.requiredLength || value?.min?.min
        }`;
      } else if (key === "max") {
        return `${this.translateService.instant("ERROR.MAX")} ${
          value?.max?.requiredLength || value?.max?.max
        }`;
      } else if (key === "pattern") {
        return this.errorString[type] || this.errorString.pattern;
      } else if (key === "validatePhoneNumber") {
        const isValidPhoneNumber = value?.validatePhoneNumber?.valid;
        return isValidPhoneNumber ? null : this.errorString?.isValidPhoneNumber;
      } else {
        return this.errorString[key] || this.errorString.required;
      }
    } else {
      return "";
    }
  }
}
