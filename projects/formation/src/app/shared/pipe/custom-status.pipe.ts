import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "customStatus",
})
export class CustomStatusPipe implements PipeTransform {
  transform(value) {
    switch (value) {
      case 1:
        return "initiated";

      case 2:
        return "passed";

      case 3:
        return "cancel";

      case 4:
        return "failed";

      default:
        return "initiated";
    }
  }
}
