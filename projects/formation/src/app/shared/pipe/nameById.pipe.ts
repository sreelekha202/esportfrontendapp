import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "nameById",
})
export class NameByIdPipe implements PipeTransform {
  transform(_id: string, field: string, list: any) {
    if (_id) {
      const obj = list.find((item) => item._id == _id);
      return obj?.[field] || _id;
    } else {
      return null;
    }
  }
}
