import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "basicFilter",
})
export class BasicFilterPipe implements PipeTransform {
  transform(value: any, key: any, keyword: any): any {
    if (!value) return null;
    if (!keyword) return value;

    keyword = keyword.toLowerCase();

    return value.filter((obj) => {
      return obj[key]
        ? JSON.stringify(obj[key]).toLowerCase().includes(keyword)
        : value;
    });
  }
}
