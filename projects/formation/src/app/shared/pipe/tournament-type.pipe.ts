import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "tournamentType",
})
export class TournamentTypePipe implements PipeTransform {
  transform(value) {
    switch (value) {
      case "online":
        return "OPTIONS.P_TYPE.ONLINE";

      case "offline":
        return "OPTIONS.P_TYPE.OFFLINE";

      case "hybrid":
        return "OPTIONS.P_TYPE.HYBRID";

      default:
        return "N/A";
    }
  }
}
