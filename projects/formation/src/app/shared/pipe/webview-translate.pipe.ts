import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "webviewTranslate",
})
export class WebviewTranslatePipe implements PipeTransform {
  transalte = {
    en: {
      PAYMENT_LABEL: "Top Prizes",
      PAYMENT_TITLE1: "Select payment method",
      PAYMENT_TITLE2: "Prize pool",
      PLAFORM_FEE: "Platform fee",
      PRIZE_AMOUNT: "Total prize amount",
      PROVIDER_NOT_AVAILABLE: "Provider is not available",
      SELECT_BUTTON: "Select",
      TOTAL_AMOUNT: "Total amount",
      0: "1st place prize",
      1: "2nd place prize",
      2: "3rd place prize",
      3: "4th place prize",
      4: "5th place prize",
      5: "6th place prize",
    },
    ar: {
      PAYMENT_LABEL: "أفضل الجوائز",
      PAYMENT_TITLE1: "اختار طريقة الدفع",
      PAYMENT_TITLE2: "مجموع جوائز",
      PLAFORM_FEE: "رسوم المنصة",
      PRIZE_AMOUNT: "إجمالي مبلغ الجائزة",
      PROVIDER_NOT_AVAILABLE: "الموفر غير متوفر",
      SELECT_BUTTON: "تحديد",
      TOTAL_AMOUNT: "المبلغ الإجمالي",
      0: "جائزة المركز 1ST",
      1: "2 جائزة المركز",
      2: "3 جائزة المركز",
      3: "4 جائزة المركز",
      4: "5 جائزة المركز",
      5: "6 جائزة المركز",
    },
    fr: {
      PAYMENT_LABEL: "Top Prizes",
      PAYMENT_TITLE1: "Select payment method",
      PAYMENT_TITLE2: "Prize pool",
      PLAFORM_FEE: "Platform fee",
      PRIZE_AMOUNT: "Total prize amount",
      PROVIDER_NOT_AVAILABLE: "Provider is not available",
      SELECT_BUTTON: "Select",
      TOTAL_AMOUNT: "Total amount",
      0: "1st place prize",
      1: "2nd place prize",
      2: "3rd place prize",
      3: "4th place prize",
      4: "5th place prize",
      5: "6th place prize",
    },
  };

  transform(value, currLang) {
    return this.transalte[currLang || "en"][value] || "N/A";
  }
}
