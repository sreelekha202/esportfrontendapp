import { Pipe, PipeTransform } from "@angular/core";
import {
  DomSanitizer,
  SafeHtml,
  SafeResourceUrl,
  SafeScript,
  SafeStyle,
  SafeUrl,
} from "@angular/platform-browser";

@Pipe({
  name: "safe",
})
export class SafePipe implements PipeTransform {
  constructor(protected domSanitizer: DomSanitizer) {}

  transform(
    value: string,
    type: string,
    isHtmlDecoded: boolean = true
  ): SafeHtml | SafeStyle | SafeScript | SafeUrl | SafeResourceUrl {
    switch (type) {
      case "html":
        return this.domSanitizer.bypassSecurityTrustHtml(
          isHtmlDecoded ? value : this.decodeHTMLEntities(value)
        );
      case "style":
        return this.domSanitizer.bypassSecurityTrustStyle(value);

      case "script":
        return this.domSanitizer.bypassSecurityTrustScript(value);

      case "url":
        return this.domSanitizer.bypassSecurityTrustResourceUrl(value);

      case "resourceUrl":
        return this.domSanitizer.bypassSecurityTrustResourceUrl(value);

      default:
        return this.domSanitizer.bypassSecurityTrustHtml(value);
    }
  }

  decodeHTMLEntities(text) {
    if (!text) return text;

    const entities = [
      ["amp", "&"],
      ["apos", "'"],
      ["#x27", "'"],
      ["#x2F", "/"],
      ["#39", "'"],
      ["#47", "/"],
      ["lt", "<"],
      ["gt", ">"],
      ["nbsp", " "],
      ["quot", '"'],
    ];

    for (var i = 0, max = entities.length; i < max; ++i)
      text = text.replace(
        new RegExp("&" + entities[i][0] + ";", "g"),
        entities[i][1]
      );

    return text;
  }
}
