import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "merge",
})
export class MergePipe implements PipeTransform {
  transform(value, type, check = false, limit = 25) {
    switch (type) {
      case "arrayOfString":
        return !Array.isArray(value)
          ? "N/A"
          : value.length
          ? value.join(",")
          : "ESPORTS.SECTIONBV2.ALL";

      case "arrayOfObject":
        return !Array.isArray(value)
          ? "N/A"
          : value.length
          ? value
              .map(
                (item) =>
                  `<p>
                  ${item?.stage?.toUpperCase() || ""}
                  ${item.venue},
                  ${item.region},
                  ${item.country}
                  </p>`
              )
              .join("")
          : "N/A";
      case "truncate":
        const trunc = (array) => {
          const str = array.join(",");
          return str.length > limit ? `${str.substring(0, limit)}...` : str;
        };
        return !check
          ? "ESPORTS.SECTIONBV2.ALL"
          : !Array.isArray(value)
          ? "N/A"
          : value.length
          ? trunc(value)
          : "N/A";
      default:
        return null;
    }
  }
}
