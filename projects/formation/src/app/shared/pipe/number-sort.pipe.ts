import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "numberSort",
})
export class NumberSortPipe implements PipeTransform {
  transform(value: any): Array<any> {
    if (value) {
      value.sort((a, b) => a.key - b.key);
      return value;
    } else {
      return [];
    }
  }
}
