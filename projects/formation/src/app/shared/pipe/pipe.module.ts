import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { ArticleStatusPipe } from "./article-status.pipe";
import { BasicFilterPipe } from "./basic-filter.pipe";
import { BracketPipe } from "./bracket.pipe";
import { CapitalizeFirstPipe } from "./capitalizefirst.pipe";
import { CustomStatusPipe } from "./custom-status.pipe";
import { CustomTranslatePipe } from "./custom-translate.pipe";
import { DateAgoPipe } from "./date-to-days-ago.pipe";
import { ErrorPipe } from "./error.pipe";
import { FlagPipe } from "./flag.pipe";
import { MergePipe } from "./merge.pipe";
import { MinuteSecondsPipe } from "./minuteSeconds.pipe";
import { NameByIdPipe } from "./nameById.pipe";
import { NumberSortPipe } from "./number-sort.pipe";
import { PrizePoolPipe } from "./prize-pool.pipe";
import { RegionPipe } from "./region.pipe";
import { SafePipe } from "./safe.pipe";
import { ShortNumberPipe } from "./short-number.pipe";
import { TournamentTypePipe } from "./tournament-type.pipe";
import { WebviewTranslatePipe } from "./webview-translate.pipe";
import { limitStringPipe } from "./limitString";
import { ParticipantTypePipe } from "../pipe/participant-type.pipe";
const pipeList = [
  ArticleStatusPipe,
  BasicFilterPipe,
  BracketPipe,
  CapitalizeFirstPipe,
  CustomStatusPipe,
  CustomTranslatePipe,
  DateAgoPipe,
  ErrorPipe,
  FlagPipe,
  MergePipe,
  MinuteSecondsPipe,
  NameByIdPipe,
  NumberSortPipe,
  PrizePoolPipe,
  RegionPipe,
  SafePipe,
  ShortNumberPipe,
  TournamentTypePipe,
  WebviewTranslatePipe,
  limitStringPipe,
  ParticipantTypePipe
];
@NgModule({
  declarations: [...pipeList],
  imports: [CommonModule],
  providers: [RegionPipe],
  exports: [...pipeList],
})
export class PipeModule {}
