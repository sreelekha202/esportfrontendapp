import { Injectable } from "@angular/core";
import { MatSidenav } from "@angular/material/sidenav";
import { MatDrawerToggleResult } from "@angular/material/sidenav/drawer";

@Injectable({ providedIn: "root" })
export class SidenavService {
  sidenav: MatSidenav;

  public setSidenav(sidenav: MatSidenav) {
    this.sidenav = sidenav;
  }

  public open(): Promise<MatDrawerToggleResult> {
    return this.sidenav.open();
  }

  public close(): Promise<MatDrawerToggleResult> {
    return this.sidenav.close();
  }

  public toggle(): void {
    this.sidenav.toggle();
  }

  public getSidenav(): Promise<MatSidenav | null> {
    return new Promise((resolve, reject) => {
      let count = 0;
      const interval = setInterval(() => {
        count++;

        if (this.sidenav) {
          clearInterval(interval);
          resolve(this.sidenav);
        }

        if (count === 100) {
          reject(null);
        }
      }, 100);
    });
  }
}
