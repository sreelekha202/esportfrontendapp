import { TestBed } from "@angular/core/testing";

import { GlobalUtils } from "./global-utils";

describe("GlobalUtils", () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it("should be created", () => {
    const service: GlobalUtils = TestBed.get(GlobalUtils);
    expect(service).toBeTruthy();
  });
});
