import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { LanguageService } from '../../core/service';
import { SidenavService } from '../service/sidenav/sidenav.service';

import { CustomPaginationModule } from '../../core/custom-pagination/custom-pagination.module';
import { DirectivesModule } from '../../shared/directives/directive.module';
import { DiscussionModule } from '../../shared/components/discussion/discussion.module';
import { EliminationModule } from '../components/elimination/elimination.module';
import { I18nModule } from '../../i18n/i18n.module';
import { LoadingModule } from '../../core/loading/loading.module';
import { MatchListModule } from '../../shared/components/match-list/match-list.module';
import { MaterialModule } from './material.module';
import { PipeModule } from '../../shared/pipe/pipe.module';
import { RatingModule } from '../../shared/components/rating/rating.module';
import { StreamModule } from '../../shared/components/stream/stream.module';

import { FullscreenLoadingComponent } from '../../core/fullscreen-loading/fullscreen-loading.component';
import { InfoPopupComponent } from '../popups/info-popup/info-popup.component';
import { SocialShareComponent } from '../popups/social-share/social-share.component';
import { TournamentItemComponent } from '../../core/tournament-item/tournament-item.component';

import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { InlineSVGModule } from 'ng-inline-svg';
import { MomentModule } from 'ngx-moment';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgOtpInputModule } from 'ng-otp-input';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { ShareButtonModule } from 'ngx-sharebuttons/button';
import { ShareButtonsModule } from 'ngx-sharebuttons/buttons';
import { ShareIconsModule } from 'ngx-sharebuttons/icons';
import { TranslateService } from '@ngx-translate/core';
import { CarouselModule } from 'primeng/carousel';

import {
  SWIPER_CONFIG,
  SwiperConfigInterface,
  SwiperModule,
} from 'ngx-swiper-wrapper';
import { LazyLoadImageModule } from 'ng-lazyload-image';
import { AvatarUpdateComponent } from '../popups/avatar-update/avatar-update.component';

const DEFAULT_SWIPER_CONFIG: SwiperConfigInterface = {
  direction: 'horizontal',
  slidesPerView: 'auto',
};

const components = [TournamentItemComponent, FullscreenLoadingComponent];
const directives = [];
const popups = [
  InfoPopupComponent,
  SocialShareComponent,
  AvatarUpdateComponent,
];
const sheets = [];

const modules = [
  BsDropdownModule,
  CarouselModule,
  CollapseModule.forRoot(),
  CommonModule,
  CustomPaginationModule,
  DirectivesModule,
  DiscussionModule,
  EliminationModule,
  FontAwesomeModule,
  FormsModule,
  I18nModule,
  InlineSVGModule.forRoot(),
  LazyLoadImageModule,
  LoadingModule,
  MatchListModule,
  MaterialModule,
  MomentModule,
  NgbModule,
  NgOtpInputModule,
  NgxDatatableModule,
  PipeModule,
  RatingModule,
  ReactiveFormsModule,
  RouterModule,
  ShareButtonModule,
  ShareButtonsModule,
  ShareIconsModule,
  StreamModule,
  SwiperModule,
];

const providers = [
  SidenavService,
  {
    provide: SWIPER_CONFIG,
    useValue: DEFAULT_SWIPER_CONFIG,
  },
];

@NgModule({
  imports: [...modules],
  declarations: [...components, ...directives, ...sheets, ...popups],
  exports: [...modules, ...components, ...directives, ...sheets, ...popups],
  providers: [...providers],
})
export class SharedModule {
  constructor(
    private languageService: LanguageService,
    private translateService: TranslateService
  ) {
    this.languageService.language.subscribe((lang) => {
      this.translateService.use(lang);
    });
  }
}
