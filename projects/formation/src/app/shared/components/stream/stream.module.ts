import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";

import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { PipeModule } from "../../../shared/pipe/pipe.module";
import { StreamComponent } from "./stream.component";

import { MatProgressSpinnerModule } from "@angular/material/progress-spinner";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";

import {
  FaIconLibrary,
  FontAwesomeModule,
} from "@fortawesome/angular-fontawesome";

import {
  faCopy,
  faEdit,
  faExpand,
  faPlay,
  faStop,
  faTrash,
} from "@fortawesome/free-solid-svg-icons";
import { I18nModule } from "../../../i18n/i18n.module";

@NgModule({
  declarations: [StreamComponent],
  imports: [
    CommonModule,
    FontAwesomeModule,
    FormsModule,
    MatProgressSpinnerModule,
    NgbModule,
    PipeModule,
    ReactiveFormsModule,
    I18nModule,
  ],
  exports: [StreamComponent],
})
export class StreamModule {
  constructor(private library: FaIconLibrary) {
    library.addIcons(faTrash, faEdit, faCopy, faPlay, faStop, faExpand);
  }
}
