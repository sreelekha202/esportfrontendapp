import { DOCUMENT } from "@angular/common";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import {
  Component,
  Inject,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
} from "@angular/core";

import { ToastService } from "../../../shared/toast/toast.service";
import {
  LanguageService,
  TournamentService,
  UtilsService,
} from "../../../core/service";

import { ConferenceApi, Utils } from "avcore/client/dist";
import { GlobalUtils } from "../../service/global-utils/global-utils";
import { MediasoupSocketApi, API_OPERATION, ERROR } from "avcore";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: "app-stream",
  templateUrl: "./stream.component.html",
  styleUrls: ["./stream.component.scss"],
})
export class StreamComponent implements OnInit, OnChanges {
  @Input() isAdmin: boolean;
  @Input() tournament;
  @Input() tournamentId;

  customStreamLoader: boolean = false;
  isOpenForm: boolean = false;
  isProccessing: boolean = false;

  preProviderList: Array<object> = [];
  preTournamentStream: Array<object> = [];
  providerList: Array<object> = [];
  streamList: Array<any> = [];

  streamForm: FormGroup;

  editIndex = -1;

  playVideo;
  playback;

  constructor(
    @Inject(DOCUMENT) private document: Document,
    private fb: FormBuilder,
    private languageService: LanguageService,
    private modalService: NgbModal,
    private toastService: ToastService,
    private tournamentService: TournamentService,
    private translateService: TranslateService,
    private utilsService: UtilsService
  ) {}

  ngOnInit(): void {
    this.languageService.language.subscribe((lang) =>
      this.translateService.use(lang || "en")
    );

    this.streamForm = this.fb.group({
      type: ["", Validators.required],
      videoUrl: this.fb.group({
        providerName: [null, Validators.required],
        channelName: ["", Validators.required],
      }),
      title: ["", Validators.required],
      description: ["", Validators.required],
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    if (
      changes.hasOwnProperty("tournamentId") &&
      changes.tournamentId.currentValue &&
      changes.hasOwnProperty("isAdmin") &&
      this.isAdmin
    ) {
      this.fetchTournamentDetails(true);
    }

    if (this.tournament) {
      this.fetchTournamentDetails(false);
    }
  }

  /** Fetch tournament Streams */
  fetchTournamentDetails = async (check) => {
    try {
      let tournamentDetails;
      let response;

      const tournament404 = async () => {
        throw new Error("Tournament is not Found");
      };

      if (check) {
        const query = JSON.stringify({ _id: this.tournamentId });
        const select = `&select=stream,youtubeVideoLink,facebookVideoLink,twitchVideoLink`;

        response = await this.tournamentService
          .getTournaments({ query }, select)
          .toPromise();
      }

      tournamentDetails = !check
        ? this.tournament
        : response?.data?.length
        ? response.data[0]
        : await tournament404();

      if (tournamentDetails?.youtubeVideoLink) {
        try {
          const channelName = await this.utilsService.getVideoId(
            tournamentDetails?.youtubeVideoLink
          );
          const embedUrl: any = this.utilsService.createEmbedUrl("YOUTUBE", {
            channelName,
          });
          this.preTournamentStream.push(embedUrl);
        } catch (error) {
          this.toastService.showInfo(
            this.translateService.instant(
              "MANAGE_TOURNAMENT.STREAM.ERROR.YOUTUBE_URL"
            )
          );
        }
      }

      if (tournamentDetails?.twitchVideoLink) {
        try {
          const channelName = tournamentDetails?.twitchVideoLink
            .split("/")
            .reverse()[0];
          const embedUrl: any = this.utilsService.createEmbedUrl("TWITCH", {
            channelName,
          });
          this.preTournamentStream.push(embedUrl);
        } catch (error) {
          this.toastService.showInfo(
            this.translateService.instant(
              "MANAGE_TOURNAMENT.STREAM.ERROR.TWITCH_URL"
            )
          );
        }
      }

      if (tournamentDetails?.facebookVideoLink) {
        try {
          const match = /videos\/(\d+)+|v=(\d+)|vb.\d+\/(\d+)/;
          const channelName = tournamentDetails?.facebookVideoLink.match(
            match
          )[1];
          const embedUrl: any = this.utilsService.createEmbedUrl("FACEBOOK", {
            channelName,
          });
          this.preTournamentStream.push(embedUrl);
        } catch (error) {
          this.toastService.showInfo(
            this.translateService.instant(
              "MANAGE_TOURNAMENT.STREAM.ERROR.FACEBOOK_URL"
            )
          );
        }
      }

      this.streamList = tournamentDetails?.stream || [];
      this.providerList = [
        { key: "YOUTUBE", value: "https://www.youtube.com/watch?v=" },
        { key: "TWITCH", value: "https://www.twitch.tv/" },
        { key: "SMASHCAST", value: "https://www.smashcast.tv/" },
        { key: "MOBCRUSH", value: "https://www.mobcrush.com/" },
        { key: "LIVESTREAM", value: "rtmp://streaming.exaltaretech.com/live/" },
        { key: "FACEBOOK", value: "https://www.facebook.com/" },
      ];
      const promiseArray = this.streamList.map((item) => {
        const i = this.providerList.findIndex(
          (item2: any) => item2.key === item.type
        );

        if (i >= 0) {
          this.providerList.splice(i, 1);
        }

        return {
          ...item,
          url: this.utilsService.createEmbedUrl(item.type, item.videoUrl),
        };
      });

      this.streamList = await Promise.all(promiseArray);
      this.preProviderList = JSON.parse(JSON.stringify(this.providerList));
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  /** Edit And Reset Form-value Handler */
  formAction(check: boolean) {
    this.isOpenForm = check;
    this.editIndex = -1;
    this.streamForm.reset();
    this.providerList = this.preProviderList;
  }

  /** Save Stream */
  saveStream() {
    if (this.streamForm.invalid) {
      this.streamForm.markAllAsTouched();
      return;
    }

    const data = this.streamForm.value;
    const streami = this.streamList.findIndex(
      (item: any) => item.type === data.type
    );

    if (this.editIndex >= 0) {
      this.streamList[this.editIndex] = data;
    } else if (streami < 0) {
      this.streamList.push(data);
    } else {
      this.streamList[streami] = data;
    }
    this.updateStream();
  }

  changeProvider(e) {
    this.streamForm
      .get("videoUrl")
      .get("providerName")
      .setValue(e.target.value, {
        onlySelf: true,
      });

    const streamP: any = this.providerList.find(
      (item: any) => item.value === e.target.value
    );

    this.streamForm.get("type").setValue(streamP.key);
    this.streamForm.get("videoUrl").get("channelName").reset("");
    if (streamP?.key === "LIVESTREAM") {
      this.fetchUniqueStreamKey();
    }
  }

  updateStream = async () => {
    try {
      await this.tournamentService
        .updateTournament({ stream: this.streamList }, this.tournamentId)
        .toPromise();
      this.fetchTournamentDetails(true);
      this.formAction(false);
    } catch (error) {
      this.fetchTournamentDetails(true);
      this.toastService.showError(
        error?.error?.message || error?.statusText || error?.message
      );
    }
  };

  removeStream = async (content, i) => {
    try {
      const response = await this.modalService.open(content, {
        windowClass: "confirm-modal-content",
        centered: true,
      }).result;

      if (response) {
        this.streamList.splice(i, 1);
        this.updateStream();
      }
    } catch (error) {}
  };

  editStream(i) {
    this.editIndex = i;
    this.isOpenForm = true;
    this.streamForm.patchValue({
      ...this.streamList[i],
    });
    this.providerList.push({
      key: this.streamList[i].type,
      value: this.streamList[i].videoUrl.providerName,
    });
  }

  fetchUniqueStreamKey = async () => {
    try {
      const streamKey = await this.tournamentService.getStreamKey();
      this.streamForm
        .get("videoUrl")
        .get("channelName")
        .patchValue(streamKey.data, {
          onlySelf: true,
        });

      this.streamForm.patchValue({
        videoUrl: {
          channelName: streamKey.data,
        },
      });
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  copyToClipboard(text) {
    const dummyElement = this.document.createElement("textarea");
    this.document.body.appendChild(dummyElement);
    dummyElement.value = text;
    dummyElement.select();
    this.document.execCommand("copy");
    this.document.body.removeChild(dummyElement);
    this.toastService.showInfo("Copied");
  }

  startStreaming = async (id, streamKey) => {
    if (!GlobalUtils.isBrowser()) return;

    try {
      this.customStreamLoader = true;
      this.playVideo = this.document.getElementById(id);

      const url = "https://streaming.exaltaretech.com";
      const { data } = await this.tournamentService.getStreamToken(streamKey);
      const worker = 0;
      const socketApi = new MediasoupSocketApi(
        url,
        API_OPERATION.STREAMING,
        data
      );
      await socketApi.stopFileStreaming({ stream: streamKey });

      const { kinds } = await socketApi.kindsByFile({
        filePath: `rtmp://streaming.exaltaretech.com/live/${streamKey}`,
      });

      await socketApi.liveStreaming({
        kinds,
        stream: streamKey,
        url: `rtmp://streaming.exaltaretech.com/live/${streamKey}`,
        videoBitrate: "4000",
      });

      this.playback = new ConferenceApi({
        kinds,
        origin: null,
        simulcast: false,
        stream: streamKey,
        token: data,
        url,
        worker,
      })
        .on("bitRate", ({ bitRate, kind }) => {
          this.customStreamLoader = false;
        })
        .on("connectionstatechange", ({ state }) => {
          this.customStreamLoader = false;
        });

      const play = () => {
        const playPromise = this.playVideo.play();
        if (playPromise !== undefined) {
          playPromise
            .then((_) => {})
            .catch((e1) => {
              this.playVideo.muted = true;
              this.playVideo.play().then(
                () => {},
                (e2) => {}
              );
            });
        }
      };

      const mediaStream = await this.playback.subscribe();
      this.playVideo.srcObject = mediaStream;

      if (Utils.isSafari) {
        const onStreamChange = () => {
          this.playVideo.srcObject = new MediaStream(mediaStream.getTracks());
          play();
        };
        this.playback
          .on("addtrack", onStreamChange)
          .on("removetrack", onStreamChange);
      } else if (Utils.isFirefox) {
        this.playVideo.addEventListener("pause", play);
      }
      play();
    } catch (error) {
      this.customStreamLoader = false;

      if (error && ERROR[error.errorId]) {
        this.toastService.showError(
          this.translateService.instant(
            "MANAGE_TOURNAMENT.STREAM.ERROR.CUSTOM_STREAMING"
          )
        );
      } else {
        this.toastService.showError(error?.error?.message || error?.message);
      }

      if (this.playback) {
        await this.playback.close();
      }
    }
  };

  /** Stop Custom streaming */
  stopStreaming = async () => {
    this.customStreamLoader = false;
    if (this.playback) {
      this.playback.close();
    }
  };

  /** Full Custom Streaming */
  fullScreenStreaming = () => {
    if (this.playVideo?.requestFullscreen) {
      this.playVideo.requestFullscreen();
    } else if (this.playVideo?.mozRequestFullScreen) {
      this.playVideo.mozRequestFullScreen();
    } else if (this.playVideo?.webkitRequestFullscreen) {
      this.playVideo?.webkitRequestFullscreen();
    } else if (this.playVideo?.msRequestFullscreen) {
      this.playVideo?.msRequestFullscreen();
    }
  };
}
