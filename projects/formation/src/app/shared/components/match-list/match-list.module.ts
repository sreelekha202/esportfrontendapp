import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";

import { MatchListComponent } from "./match-list.component";
import { LoadingModule } from "../../../core/loading/loading.module";

import { faSortDown } from "@fortawesome/free-solid-svg-icons";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import {
  FontAwesomeModule,
  FaIconLibrary,
} from "@fortawesome/angular-fontawesome";
import { I18nModule } from "../../../i18n/i18n.module";
import { EliminationModule } from "../elimination/elimination.module";
import { OpenTeamDetailsComponent } from "./open-team-details/open-team-details.component";
@NgModule({
  declarations: [MatchListComponent, OpenTeamDetailsComponent],
  imports: [
    CommonModule,
    FontAwesomeModule,
    LoadingModule,
    NgbModule,
    I18nModule,
    EliminationModule,
  ],
  exports: [MatchListComponent],
})
export class MatchListModule {
  constructor(private library: FaIconLibrary) {
    library.addIcons(faSortDown);
  }
}
