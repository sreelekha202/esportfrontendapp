import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { CustomStreamingComponent } from "./custom-streaming.component";

import { faPlay, faStop, faExpand } from "@fortawesome/free-solid-svg-icons";
import { MatProgressSpinnerModule } from "@angular/material/progress-spinner";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import {
  FaIconLibrary,
  FontAwesomeModule,
} from "@fortawesome/angular-fontawesome";
import { I18nModule } from "../../../i18n/i18n.module";

@NgModule({
  declarations: [CustomStreamingComponent],
  imports: [
    CommonModule,
    FontAwesomeModule,
    MatProgressSpinnerModule,
    NgbModule,
    I18nModule,
  ],
  exports: [CustomStreamingComponent],
})
export class CustomStreamingModule {
  constructor(private library: FaIconLibrary) {
    library.addIcons(faPlay, faStop, faExpand);
  }
}
