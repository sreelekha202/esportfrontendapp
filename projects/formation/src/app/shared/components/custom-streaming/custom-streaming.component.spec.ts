import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { CustomStreamingComponent } from "./custom-streaming.component";

describe("CustomStreamingComponent", () => {
  let component: CustomStreamingComponent;
  let fixture: ComponentFixture<CustomStreamingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CustomStreamingComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomStreamingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
