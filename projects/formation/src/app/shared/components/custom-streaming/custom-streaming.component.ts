import { Component, Inject, Input, OnInit } from "@angular/core";
import { DOCUMENT } from "@angular/common";

import { ToastService } from "../../../shared/toast/toast.service";
import { TournamentService, UtilsService } from "../../../core/service";

import { ConferenceApi, Utils } from "avcore/client/dist";
import { GlobalUtils } from "../../../shared/service/global-utils/global-utils";
import { MediasoupSocketApi, API_OPERATION, ERROR } from "avcore";
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: "app-streaming",
  templateUrl: "./custom-streaming.component.html",
  styleUrls: ["./custom-streaming.component.scss"],
})
export class CustomStreamingComponent implements OnInit {
  @Input() channelName;
  @Input() enableWebView: Boolean;

  customStreamLoader: boolean = false;

  playback;
  playVideo;

  constructor(
    @Inject(DOCUMENT) private document: Document,
    private toastService: ToastService,
    private tournamentService: TournamentService,
    private translateService: TranslateService,
    private utilsService: UtilsService
  ) {}

  ngOnInit(): void {}

  /** Start Custom streaming */
  startStreaming = async (id, streamKey) => {
    if (!GlobalUtils.isBrowser()) {
      return;
    }

    try {
      this.customStreamLoader = true;
      this.playVideo = this.document.getElementById(id);

      const url = "https://streaming.exaltaretech.com/";
      const { data } = await this.tournamentService.getStreamToken(streamKey);
      const worker = 0;
      const socketApi = new MediasoupSocketApi(
        url,
        API_OPERATION.STREAMING,
        data
      );

      await socketApi.stopFileStreaming({ stream: streamKey });

      const { kinds } = await socketApi.kindsByFile({
        filePath: `rtmp://streaming.exaltaretech.com/live/${streamKey}`,
      });

      await socketApi.liveStreaming({
        kinds,
        stream: streamKey,
        url: `rtmp://streaming.exaltaretech.com/live/${streamKey}`,
        videoBitrate: "4000",
      });

      this.playback = new ConferenceApi({
        kinds,
        origin: null,
        simulcast: false,
        stream: streamKey,
        token: data,
        url,
        worker,
      })
        .on("bitRate", ({ bitRate, kind }) => {
          this.customStreamLoader = false;
        })
        .on("connectionstatechange", ({ state }) => {
          this.customStreamLoader = false;
        });

      const play = () => {
        const playPromise = this.playVideo.play();
        if (playPromise !== undefined) {
          playPromise
            .then((_) => {})
            .catch((e1) => {
              this.playVideo.muted = true;
              this.playVideo.play().then(
                () => {},
                (e2) => {}
              );
            });
        }
      };

      const mediaStream = await this.playback.subscribe();

      this.playVideo.srcObject = mediaStream;

      if (Utils.isSafari) {
        const onStreamChange = () => {
          this.playVideo.srcObject = new MediaStream(mediaStream.getTracks());
          play();
        };

        this.playback
          .on("addtrack", onStreamChange)
          .on("removetrack", onStreamChange);
      } else if (Utils.isFirefox) {
        this.playVideo.addEventListener("pause", play);
      }
      play();
    } catch (error) {
      this.customStreamLoader = false;

      if (error && ERROR[error.errorId]) {
        if (!this.enableWebView) {
          this.toastService.showError(
            this.translateService.instant(
              "MANAGE_TOURNAMENT.STREAM.ERROR.CUSTOM_STREAMING"
            )
          );
        } else {
          this.utilsService.showNativeAlert(
            this.translateService.instant(
              "MANAGE_TOURNAMENT.STREAM.ERROR.CUSTOM_STREAMING"
            )
          );
        }
      } else {
        if (!this.enableWebView) {
          this.toastService.showError(error?.error?.message || error?.message);
        } else {
          this.utilsService.showNativeAlert(
            error?.error?.message || error?.message
          );
        }
      }

      if (this.playback) {
        await this.playback.close();
      }
    }
  };

  /** Stop Custom streaming */
  stopStreaming = async () => {
    this.customStreamLoader = false;

    if (this.playback) {
      this.playback.close();
    }
  };

  /** Full Custom Streaming */
  fullScreenStreaming = () => {
    if (this.playVideo?.requestFullscreen) {
      this.playVideo.requestFullscreen();
    } else if (this.playVideo?.mozRequestFullScreen) {
      this.playVideo.mozRequestFullScreen();
    } else if (this.playVideo?.webkitRequestFullscreen) {
      this.playVideo?.webkitRequestFullscreen();
    } else if (this.playVideo?.msRequestFullscreen) {
      this.playVideo?.msRequestFullscreen();
    }
  };
}
