import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { NgModule } from "@angular/core";

import { LoadingModule } from "../../../core/loading/loading.module";
import { PipeModule } from "../../pipe/pipe.module";

import { CounterComponent } from "./counter/counter.component";
import { DropDownComponent } from "./drop-down/drop-down.component";
import { InputComponent } from "./input/input.component";
import { ToggleSwitchComponent } from "./toggle-switch/toggle-switch.component";
import { UploadComponent } from "./upload/upload.component";

import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { I18nModule } from "../../../i18n/i18n.module";
import { DurationCounterComponent } from "./duration-counter/duration-counter.component";

@NgModule({
  declarations: [
    CounterComponent,
    DropDownComponent,
    InputComponent,
    ToggleSwitchComponent,
    UploadComponent,
    DurationCounterComponent,
  ],
  imports: [
    CommonModule,
    FontAwesomeModule,
    FormsModule,
    LoadingModule,
    NgbModule,
    PipeModule,
    ReactiveFormsModule,
    I18nModule,
  ],
  exports: [
    CounterComponent,
    DropDownComponent,
    InputComponent,
    ToggleSwitchComponent,
    UploadComponent,
    DurationCounterComponent,
  ],
})
export class FormComponentModule {}
