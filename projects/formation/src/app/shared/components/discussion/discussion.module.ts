import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { DiscussionComponent } from "./discussion.component";
import { MessageComponent } from "./message/message.component";
import { NeedLoginComponent } from "./need-login/need-login.component";
import { RouterModule } from "@angular/router";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MomentModule } from "ngx-moment";
import { I18nModule } from "../../../i18n/i18n.module";
import { MaterialModule } from "../../modules/material.module";
import { LazyLoadImageModule } from "ng-lazyload-image";
@NgModule({
  declarations: [DiscussionComponent, MessageComponent, NeedLoginComponent],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    MomentModule,
    I18nModule,
    MaterialModule,
    LazyLoadImageModule,
  ],
  exports: [DiscussionComponent, MessageComponent, NeedLoginComponent],
})
export class DiscussionModule {}
