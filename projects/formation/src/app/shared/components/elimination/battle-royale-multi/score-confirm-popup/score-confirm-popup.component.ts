import { MatDialogRef } from "@angular/material/dialog";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-score-confirm-popup",
  templateUrl: "./score-confirm-popup.component.html",
  styleUrls: ["./score-confirm-popup.component.scss"],
})
export class ScoreConfirmPopupComponent implements OnInit {
  constructor(public dialogRef: MatDialogRef<any>) {}

  ngOnInit(): void {}

  onEdit(): void {
    this.dialogRef.close(false);
  }

  onConfirm(): void {
    this.dialogRef.close(true);
  }
}
