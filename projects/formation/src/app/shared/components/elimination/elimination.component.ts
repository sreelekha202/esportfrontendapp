import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
} from "@angular/core";

import { LanguageService, UserService } from "../../../core/service";
import { ToastService } from "../../../shared/toast/toast.service";

import { IMatch, ITournament } from "../../../shared/models";
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: "app-elimination",
  templateUrl: "./elimination.component.html",
  styleUrls: ["./elimination.component.scss"],
})
export class EliminationComponent implements OnInit, OnChanges {
  @Input() enableWebview: boolean;
  @Input() isAdmin: boolean;
  @Input() isFinished: boolean;
  @Input() isLoaded: boolean;
  @Input() isScreenshotRequired: boolean = false;
  @Input() isSeeded: boolean;
  @Input() isShowCountryFlag: boolean = false;
  @Input() participantId: string;
  @Input() scoreReporting: number = 1;
  @Input() stageBracketType: string;
  @Input() stageType: string;
  @Input() structure: any;
  @Input() tournamentId: string;
  @Input() type: string;
  @Input() participantType: string;
  @Input() disableStanding: boolean;
  @Input() tournament: ITournament;
  @Input() hideMockStructure: boolean;
  @Output() isRefresh = new EventEmitter<boolean>(false);
  isUpdateMatchScore: boolean = false;
  countryList = [];
  match: IMatch;

  constructor(
    private languageService: LanguageService,
    private toastService: ToastService,
    private translateService: TranslateService,
    private userService: UserService
  ) {}

  ngOnInit(): void {
    this.languageService.language.subscribe((lang) =>
      this.translateService.use(lang || "en")
    );
    this.getAllCountries();
  }

  getAllCountries = async () => {
    try {
      const data = await this.userService.getAllCountries().toPromise();
      this.countryList = data?.countries || [];
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  updatedScoreCard(data) {
    this.isUpdateMatchScore = data.isOpenScoreCard;
    this.match = data.match;
  }

  exitFromScoreCard(data) {
    if (typeof data === "object") {
      this.isUpdateMatchScore = data.isOpenScoreCard;
      if (data.refresh) {
        this.isRefresh.emit(data.refresh);
      }
    } else {
      this.isRefresh.emit(data);
    }
  }

  ngOnChanges(simpleChanges: SimpleChanges) {
    if (simpleChanges.hasOwnProperty("isSeeded") && !this.hideMockStructure) {
      if (typeof this.isSeeded === "boolean" && !this.isSeeded) {
        this.toastService.showInfo(
          this.translateService.instant("ELIMINATION.MOCK_STRUCTURE")
        );
      }
    }
  }
}
