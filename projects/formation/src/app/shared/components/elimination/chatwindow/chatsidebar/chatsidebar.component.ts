import { isPlatformBrowser } from "@angular/common";
import {
  AfterViewInit,
  Component,
  ElementRef,
  Inject,
  OnDestroy,
  OnInit,
  PLATFORM_ID,
  ViewChild,
} from "@angular/core";
import { ChatSidenavService } from "../../../../../shared/service/chat/chat-sidenav.service";
import { ChatService, UserService } from "../../../../../core/service";
import { MatSidenav } from "@angular/material/sidenav";
import { IUser } from "../../../../../shared/models";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { Subscription } from "rxjs";

@Component({
  selector: "app-chatsidebar",
  templateUrl: "./chatsidebar.component.html",
  styleUrls: ["./chatsidebar.component.scss"],
})
export class ChatsidebarComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild("menuSidenav") public menuSidenav: MatSidenav;
  @ViewChild("chatSidenav") public chatSidenav: MatSidenav;
  @ViewChild("searchinput") searchinput: ElementRef;
  isBrowser: boolean;
  showChat: boolean = false;
  matchdetails: any;
  matchlist: any;
  spamcomment: any;
  typeofchat: any;
  useridtoblock: any;
  chatWindows = [];
  initialUserList = [];
  userlist: any;
  windowposition: String = "chat_window chat_window_right_drawer";
  currentUserName: String = "";
  isSpin: boolean = false;
  isSpin2: boolean = false;
  isUserLoggedIn: boolean = false;
  currentUserId: String = "";
  currenUser: IUser;
  showSearchListTitle: boolean = false;

  userSubscription: Subscription;

  constructor(
    @Inject(PLATFORM_ID) private platformId,
    private chatSidenavService: ChatSidenavService,
    public chatService: ChatService,
    private userService: UserService,
    private modalService: NgbModal
  ) {
    this.isBrowser = isPlatformBrowser(platformId);
  }

  ngOnInit(): void {
    if (this.isBrowser) {
      this.userSubscription = this.userService.currentUser.subscribe((data) => {
        if (data) {
          this.currenUser = data;
          this.isUserLoggedIn = !!this.currenUser;
          this.currentUserId = this.currenUser._id;

          this.chatService.mtches.subscribe((matcList) => {
            this.isSpin2 = true;
            this.chatWindows = [];
            this.chatWindows = matcList;
          });

          if (this.chatWindows?.length == 0) {
            this.chatWindows = [];
            this.chatService?.getAllMatches(this.currentUserId, 1);
          }

          this.chatService.chatStatus.subscribe((cstatus) => {
            this.showChat = cstatus;
          });
        } else {
          //this.userService.getAuthenticatedUser();
        }
      });
    }
  }

  ngAfterViewInit(): void {
    this.chatSidenavService.setChatSidenav(this.chatSidenav);
  }

  ngOnDestroy(): void {
    this.chatService.setChatStatus(false);
    this.chatService?.disconnectMatch(this.matchdetails?._id, this.typeofchat);
    this.userSubscription?.unsubscribe();
  }

  onMenuClick(): void {
    this.menuSidenav.toggle();
  }

  searchUsers(event) {
    if (event.keyCode == 13) {
      this.isSpin = true;
      this.showSearchListTitle = false;
      this.chatService
        .searchUsers(this.searchinput?.nativeElement.value)
        .subscribe(
          (res: any) => {
            this.isSpin = false;
            this.showSearchListTitle = true;
            this.userlist = res.data;
          },
          (err) => {}
        );
    }
  }

  toggleChat(chatwindow) {
    if (this.currentUserId) {
      this.chatService?.getAllMatches(this.currentUserId, 1);
    }

    this.typeofchat = chatwindow.type;

    if (chatwindow.type == "tournament") {
      this.matchdetails = chatwindow;
    } else {
      let matchid = chatwindow._id + "-" + this.currentUserId;
      this.matchdetails = matchid;
      this.showChat = true;
    }

    this.chatService.setWindowPos(this.windowposition);
    this.chatService.setCurrentMatch(this.matchdetails);
    this.chatService.setTypeOfChat(this.typeofchat);
    this.chatService.setChatStatus(true);

    if (this.chatService.getChatStatus() == false) {
      this.chatService?.disconnectMatch(
        this.matchdetails?._id,
        this.typeofchat
      );
    }
  }

  blockUser(content, userid) {
    this.useridtoblock = userid;

    this.modalService.open(content, {
      centered: true,
      windowClass: "custom-modal-content",
    });
  }

  onConfirmSpam() {
    this.chatService
      ?.blockUser(this.useridtoblock, this.spamcomment)
      .subscribe((resp: any) => {
        if (resp.status == "success") {
          this.chatService?.getAllMatches(this.currentUserId, 1);
          this.useridtoblock = "";
          this.modalService.dismissAll();
          this.spamcomment = "";
        }
      });
  }
}
