import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { UploadImageComponent } from "./upload-image.component";
import { ReactiveFormsModule } from "@angular/forms";
import { I18nModule } from "../../../i18n/i18n.module";
import { PipeModule } from "../../pipe/pipe.module";

@NgModule({
  declarations: [UploadImageComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    I18nModule,
    PipeModule, //make sure to remove later we need error pipe onlu
  ],
  exports: [UploadImageComponent],
})
export class UploadImageModule {}
