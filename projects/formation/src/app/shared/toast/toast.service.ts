import { Injectable, TemplateRef } from "@angular/core";

import { MatSnackBar } from "@angular/material/snack-bar";
import { GlobalUtils } from "../service/global-utils/global-utils";

import {
  SnackComponent,
  SnackComponentData,
  SnackComponentType,
} from "../../core/snack/snack.component";

@Injectable({ providedIn: "root" })
export class ToastService {
  toasts: any[] = [];

  constructor(public matSnackBar: MatSnackBar) {}

  showSuccess(message) {
    const data: SnackComponentData = {
      text: message,
      type: SnackComponentType.success,
    };

    this.matSnackBar.openFromComponent(SnackComponent, {
      data,
      horizontalPosition: "end",
      panelClass: "global-Snack",
      verticalPosition: "top",
    });
  }

  showError(message) {
    const data: SnackComponentData = {
      text: message,
      type: SnackComponentType.error,
    };

    this.matSnackBar.openFromComponent(SnackComponent, {
      data,
      horizontalPosition: "end",
      panelClass: "global-Snack",
      verticalPosition: "top",
    });
  }

  showInfo(message) {
    const data: SnackComponentData = {
      text: message,
      type: SnackComponentType.info,
    };

    this.matSnackBar.openFromComponent(SnackComponent, {
      data,
      horizontalPosition: "end",
      panelClass: "global-Snack",
      verticalPosition: "top",
    });
  }

  show(textOrTpl: string | TemplateRef<any>, options: any = {}) {
    this.toasts.push({ textOrTpl, ...options });
  }

  remove(toast) {
    this.toasts = this.toasts.filter((t) => t !== toast);
  }
}
