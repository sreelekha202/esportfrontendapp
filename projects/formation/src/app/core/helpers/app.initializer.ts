import { environment } from "../../../environments/environment";
import { GlobalUtils } from "../../shared/service/global-utils/global-utils";
import { UserService } from "../service";

const token = environment.currentToken;

export function appInitializer(userService: UserService) {
  return () =>
    new Promise((resolve) => {
      // attempt to refresh token on app start up to auto authenticate
      if (
        GlobalUtils.isBrowser() &&
        localStorage.getItem(environment.currentToken)
      ) {
        const jwtToken = JSON.parse(
          atob(localStorage.getItem(token).split(".")[1])
        );

        const expires: Date = new Date(jwtToken.exp * 1000);

        if (expires > new Date()) {
          userService.startRefreshTokenTimer();
          userService.getAllCountries().subscribe().add(resolve);
        } else {
          userService
            .refreshToken()
            .subscribe(
              (data) => {},
              (error) => {
                userService.logout();
              }
            )
            .add(resolve);
        }
      } else {
        userService.getAllCountries().subscribe().add(resolve);
      }
    });
}
