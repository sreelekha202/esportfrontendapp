import { Injectable } from "@angular/core";
import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from "@angular/common/http";

import { ConstantsService } from "../../service";

import { Observable } from "rxjs";
import { tap } from "rxjs/operators";

import { environment } from "../../../../environments/environment";
import { GlobalUtils } from "../../../shared/service/global-utils/global-utils";

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
  constructor(private constantsService: ConstantsService) {}

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    // add auth header with jwt if user is logged in and request is to api url

    let index = request.url.lastIndexOf("/");
    let url = request.url.substring(index + 1);

    const temp = this.constantsService.getWebViewMetaData();

    if (GlobalUtils.isBrowser()) {
      if (url == "file-upload") {
        if (localStorage.getItem(environment.currentToken)) {
          request = request.clone({
            setHeaders: {
              Authorization: `Bearer ${localStorage.getItem(
                environment.currentToken
              )}`,
            },
          });
        }
      } else {
        if (localStorage.getItem(environment.currentToken)) {
          request = request.clone({
            setHeaders: {
              "Content-Type": "application/json",
              Authorization: `Bearer ${localStorage.getItem(
                environment.currentToken
              )}`,
            },
          });
        } else if (temp?.token) {
          request = request.clone({
            setHeaders: {
              "Content-Type": "application/json",
              Authorization: `Bearer ${temp?.token}`,
            },
          });
        }
      }
      // Add current language
      const locale =
        localStorage.getItem("currentLanguage") ||
        ConstantsService.defaultLangCode;
      request = request.clone({
        setHeaders: { locale },
      });
    }

    // return next.handle(request);
    return next.handle(request).pipe(
      tap(
        (event) => {},
        (error) => {
          if (error instanceof HttpErrorResponse) {
            if (error.status === 0) {
              throw new Error(
                "Please check your internet connectivity or try after sometime."
              );
            } else if (
              error?.status === 401 &&
              error?.statusText === "Unauthorized"
            ) {
              throw new Error(
                "Your token has been expired. Please login again."
              );
            }
          }
        }
      )
    );
  }
}
