import { Component, Input, OnInit, Output, EventEmitter } from "@angular/core";
import { AppHtmlRoutes } from "../../app-routing.model";
import { LanguageService } from "../../core/service";

export interface ArticleItemComponentData {
  _id: string;
  author?: string;
  createdDate: string;
  id: number;
  image: string;
  shortDescription: any;
  slug: string;
  title: any;
}

@Component({
  selector: "app-article-item",
  templateUrl: "./article-item.component.html",
  styleUrls: ["./article-item.component.scss"],
})
export class ArticleItemComponent implements OnInit {
  @Output() onTop = new EventEmitter();
  @Input() data;

  AppHtmlRoutes = AppHtmlRoutes;

  constructor(public languageService: LanguageService) {}

  ngOnInit(): void {}
}
