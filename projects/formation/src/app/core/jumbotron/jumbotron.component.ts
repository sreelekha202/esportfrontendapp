import { Component, Input, OnInit } from "@angular/core";
import { AppHtmlRoutes } from "../../app-routing.model";
import { LanguageService } from "../../core/service";

export interface JumbotronComponentData {
  _id: string;
  btn: string;
  dateTitle: string;
  isBtnHidden?: boolean;
  ribbon: string;
  slug: string;
  title: string;
  user: {
    img: string;
    name: string;
  };
}

@Component({
  selector: "app-jumbotron",
  templateUrl: "./jumbotron.component.html",
  styleUrls: ["./jumbotron.component.scss"],
})
export class JumbotronComponent implements OnInit {
  @Input() backgroundUrl: any;
  @Input() data: JumbotronComponentData;

  AppHtmlRoutes = AppHtmlRoutes;

  constructor(public languageService: LanguageService) {}

  ngOnInit(): void {}

  ngOnChanges() {
    this.data = this.data;
  }
}
