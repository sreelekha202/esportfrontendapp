import { NgModule } from '@angular/core';

import { SharedModule } from '../shared/modules/shared.module';

import { provideConfig } from './service/social-media-auth.service';
import { environment } from '../../environments/environment';

import { AllTournamentsComponent } from '../modules/home2/all-tournaments/all-tournaments.component';
import { AnnouncementComponent } from './../modules/home2/announcement/announcement.component';
import { ArticleItemComponent } from './article-item/article-item.component';
import { BannerComponent } from '../modules/home2/banner/banner.component';
import { DropDownSelectComponent } from './drop-down-select/drop-down-select.component';
import { GamesCardsComponent } from '../modules/home2/games-cards/games-cards.component';
import { GreetingBlockComponent } from '../modules/home2/greeting-block/greeting-block.component';
import { JumbotronComponent } from './jumbotron/jumbotron.component';
import { LeaderBoardCardComponent } from '../modules/leader-board/leader-board-card/leader-board-card.component';
import { LeaderBoardPlayerComponent } from './../modules/leader-board-player/leader-board-player.component';
import { MatchHighlightComponent } from '../modules/home2/match-highlight/match-highlight.component';
import { ScrollTopComponent } from './scroll-top/scroll-top.component';
import { SearchInputComponent } from './search-input/search-input.component';
import { SnackComponent } from './snack/snack.component';
import { StepperComponent } from './stepper/stepper.component';
import { TournamentsCardComponent } from '../modules/home2/tournaments-card/tournaments-card.component';
import { TournamentsTrackerComponent } from '../modules/home2/tournaments-tracker/tournaments-tracker.component';
import { TournamentTrackerCardComponent } from '../modules/home2/tournament-tracker-card/tournament-tracker-card.component';
import { VideoItemComponent } from './video-item/video-item.component';

import {
  SocialAuthServiceConfig,
  GoogleLoginProvider,
  FacebookLoginProvider,
} from 'angularx-social-login';

const components = [
  AllTournamentsComponent,
  AnnouncementComponent,
  ArticleItemComponent,
  BannerComponent,
  DropDownSelectComponent,
  GamesCardsComponent,
  GreetingBlockComponent,
  JumbotronComponent,
  LeaderBoardCardComponent,
  LeaderBoardPlayerComponent,
  MatchHighlightComponent,
  ScrollTopComponent,
  SearchInputComponent,
  SnackComponent,
  StepperComponent,
  TournamentsCardComponent,
  TournamentsTrackerComponent,
  TournamentTrackerCardComponent,
  VideoItemComponent,
];

@NgModule({
  declarations: [...components],
  imports: [SharedModule],
  exports: [...components],
  providers: [
    {
      provide: 'SocialAuthServiceConfig',
      useValue: {
        autoLogin: false,
        providers: [
          {
            id: GoogleLoginProvider.PROVIDER_ID,
            provider: new GoogleLoginProvider(environment.googleAPPID),
          },
          {
            id: FacebookLoginProvider.PROVIDER_ID,
            provider: new FacebookLoginProvider(environment.facebookAPPID),
          },
        ],
      } as SocialAuthServiceConfig,
    },
  ],
})
export class CoreModule {}
