import {
  Component,
  ElementRef,
  Inject,
  OnDestroy,
  OnInit,
  PLATFORM_ID,
} from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { Router } from '@angular/router';

import { ChatService } from '../../core/service/chat.service';
import { ChatSidenavService } from '../../shared/service/chat/chat-sidenav.service';
import { ToastService } from '../../shared/toast/toast.service';
import {
  ConstantsService,
  LanguageService,
  UserService,
} from '../../core/service';

import { fromEvent, Subscription } from 'rxjs';

import { GlobalUtils } from '../../shared/service/global-utils/global-utils';
import { environment } from '../../../environments/environment';

import { AppHtmlProfileRoutes, AppHtmlRoutes } from '../../app-routing.model';
import { AppHtmlRoutesLoginType } from '../../app-routing.model';
import { IUser } from '../../shared/models';

import { TranslateService } from '@ngx-translate/core';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';
import {
  faBars,
  faTimes,
  faSearch,
  faBell,
} from '@fortawesome/free-solid-svg-icons';

enum AppLanguage {
  fr = 'fr',
  en = 'en',
}

@AutoUnsubscribe()
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit, OnDestroy {
  activeLang = ConstantsService?.defaultLangCode;
  AppHtmlProfileRoutes = AppHtmlProfileRoutes;
  AppHtmlRoutes = AppHtmlRoutes;
  AppHtmlRoutesLoginType = AppHtmlRoutesLoginType;

  collapsed: boolean = true;
  isAdmin: boolean = false;
  isBrowser: boolean;
  isUserLoggedIn: boolean = false;
  mobMenuOpened: boolean = false;

  currenUser: IUser;

  tournaments: any;
  matchlist: any;
  matchcount: number;
  chatWins = [];
  currentUserId: any;

  AppLanguage = [];

  faBell = faBell;
  faSearch = faSearch;
  faBars = faBars;
  faTimes = faTimes;

  links = [
    {
      title: 'HEADER.HOME',
      url: AppHtmlRoutes.home,
    },
    {
      title: 'HEADER.ARTICLES',
      url: AppHtmlRoutes.article,
    },
    {
      title: 'HEADER.LEADERBOARD',
      url: AppHtmlRoutes.leaderBoard,
    },
    {
      title: 'HEADER.ESPORTS',
      url: AppHtmlRoutes.tournament,
    },
  ];

  private sub1: Subscription;
  userSubscription: Subscription;

  constructor(
    @Inject(PLATFORM_ID) private platformId,
    private chatService: ChatService,
    private chatSidenavService: ChatSidenavService,
    private elementRef: ElementRef,
    private languageService: LanguageService,
    private router: Router,
    private userService: UserService,
    public toastService: ToastService,
    public translate: TranslateService
  ) {
    this.isBrowser = isPlatformBrowser(platformId);
  }

  ngOnInit(): void {
    this.matchlist = [];
    this.activeLang = this.translate.currentLang;
    this.AppLanguage = ConstantsService?.language;
    this.matchcount = 0;

    if (
      this.isBrowser &&
      GlobalUtils.isBrowser() &&
      localStorage.getItem(environment.currentToken)
    ) {
      this.userSubscription = this.userService.currentUser.subscribe(
        (data) => {
          if (data) {
            this.currenUser = data;
            this.isUserLoggedIn = !!this.currenUser;
            this.currentUserId = this.currenUser._id;
            this.isAdmin =
              data?.accountType === ConstantsService.AccountType.Admin;

            this.chatService?.login.subscribe((data) => {
              if (data.accessToken != 'undefined' && data.accessToken != null) {
                this.chatService?.connectUser();

                this.chatService?.getUnReadCount().subscribe((res: any) => {
                  this.matchcount = 0;
                  for (let i = 0; i < res[0].length; i++) {
                    if (res[0][i].matchid.includes('-')) {
                      this.matchcount++;
                    }
                  }
                });

                this.chatService?.getAllUnreadCount().subscribe((res: any) => {
                  this.matchcount = 0;
                  for (let i = 0; i < res[0].length; i++) {
                    if (res[0][i].matchid.includes('-')) {
                      this.matchcount++;
                    }
                  }
                  this.matchcount = res[0].length;
                });
              }
            });
          } else {
            this.userService.getAuthenticatedUser();
          }
        },
        (error) => {
          this.toastService.showError(error.message);
        }
      );
    }
    this.onHeaderScroll();
  }

  ngOnDestroy() {
    this.userSubscription?.unsubscribe();
  }

  onChatSidenavToggle(): void {
    this.chatSidenavService.toggle();

    if (this.currentUserId) {
      this.chatService?.getAllMatches(this.currentUserId, 1);
    }

    if (this.chatSidenavService.checkOpen()) {
      if (this.chatService.getChatStatus()) {
        this.chatService.setChatStatus(false);
      }
    } else {
      if (this.chatService.getChatStatus()) {
        this.chatService.setChatStatus(false);
      }
    }
  }

  onLogOut(): void {
    if (GlobalUtils.isBrowser()) {
      this.chatService.setChatStatus(false);
      this.userService.logout();
      this.chatSidenavService.close();
      this.chatService.doLogout();
      this.chatService?.disconnectUser();
    }

    this.router.navigate([
      AppHtmlRoutes.userPageType,
      AppHtmlRoutesLoginType.emailLogin,
    ]);
  }

  onLanguageChange(lang): void {
    if (GlobalUtils.isBrowser()) {
      localStorage.setItem('currentLanguage', lang);
      this.activeLang = lang;
      this.translate.use(lang);
      this.languageService.setLanguage(lang);
    }
  }

  private onHeaderScroll(): void {
    if (GlobalUtils.isBrowser()) {
      const scrollingContent = document.querySelector('mat-sidenav-content');

      if (scrollingContent) {
        const header = this.elementRef.nativeElement;

        this.sub1 = fromEvent(scrollingContent, 'scroll').subscribe(
          (e: Event) => {
            if ((e.target as Element).scrollTop > header.clientHeight) {
              header.classList.add('is-scrolling');
            } else {
              header.classList.remove('is-scrolling');
            }
          }
        );
      }
    }
  }
}
