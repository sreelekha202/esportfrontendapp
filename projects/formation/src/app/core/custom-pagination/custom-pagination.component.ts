import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
} from "@angular/core";

import { IPagination } from "../../shared/models";
import { PageChangedEvent } from "ngx-bootstrap/pagination";
import { PaginationService } from "../service/pagination.service";
import { GlobalUtils } from "../../shared/service/global-utils/global-utils";

@Component({
  selector: "app-pagination",
  templateUrl: "./custom-pagination.component.html",
  styleUrls: ["./custom-pagination.component.scss"],
})
export class CustomPaginationComponent implements OnInit, OnChanges {
  @Input() activePage: Number = 1;
  @Input() page: IPagination;
  @Output() currentPage = new EventEmitter<Number>();

  constructor(
    private paginationService: PaginationService,
    private globalUtils: GlobalUtils
  ) {}

  ngOnInit(): void {}

  pageChanged(event: PageChangedEvent) {
    this.currentPage.emit(event.page);
    this.paginationService.setPageChanged(true);
  }

  ngOnChanges(simpleChanges: SimpleChanges) {}

  onScrollTop() {
    if (GlobalUtils.isBrowser()) {
      window.scroll(0, 0);
    }
  }
}
