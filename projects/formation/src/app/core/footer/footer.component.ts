import { Component, OnInit } from '@angular/core';

import { AppHtmlRoutes } from '../../app-routing.model';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
})
export class FooterComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;

  buildConfig = environment.buildConfig || 'N/A';

  formationLogo = 'assets/global-images/formation-header-logo.svg';

  socials = [
    {
      link: 'https://www.facebook.com/formation.gg',
      icon: 'assets/images/Footer/facebook.svg',
    },
    {
      link: 'https://www.instagram.com/formationgg/',
      icon: 'assets/images/Footer/instagram.svg',
    },
    {
      link: 'https://www.twitch.tv/formationgg',
      icon: 'assets/images/Footer/twitch.svg',
    },
    {
      link: 'https://www.linkedin.com/company/formation-gg',
      icon: 'assets/images/Footer/linkedin.svg',
    },
    {
      link: 'https://twitter.com/formation_gg?s=11',
      icon: 'assets/images/Footer/twitter.svg',
    },
  ];

  constructor() {}

  ngOnInit(): void {}
}
