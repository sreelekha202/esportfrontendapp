import { Injectable } from "@angular/core";

import { ScriptLoadingService } from "./script-loading.service";
import { environment } from "../../../environments/environment";

@Injectable({
  providedIn: "root",
})
export class PaypalScriptService {
  private baseUrl: string = "https://www.paypal.com/sdk/js";
  private globalVar: string = "paypal";

  constructor(private scriptLoadingService: ScriptLoadingService) {}

  registerScript(loaded: (payPalApi: any) => void): void {
    this.scriptLoadingService.registerScript(
      this.getPayPalUrl(),
      this.globalVar,
      loaded
    );
  }

  getPayPalUrl(): string {
    return `${this.baseUrl}?client-id=${environment.paypal_client_id}`;
  }
}
