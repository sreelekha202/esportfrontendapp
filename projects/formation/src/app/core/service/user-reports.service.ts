import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class UserReportsService {
  constructor(private http: HttpClient) {}

  getUserReports(): Observable<any> {
    return this.http.get(
      environment.apiEndPoint + `tournament/reports/all_reports`
    );
  }

  getTournamentsUserReport(id): Observable<any> {
    return this.http.get(
      environment.apiEndPoint + `tournament/reports_by_tournament/${id}`
    );
  }

  getMyMatches(): Observable<any> {
    return this.http.get(environment.apiEndPoint + `match/my_matches`);
  }

  getUserReportById(id): Observable<any> {
    return this.http.get(environment.apiEndPoint + `tournament/reports/${id}`);
  }

  replyOnUserReport(data): Observable<any> {
    return this.http.post(
      environment.apiEndPoint + 'tournament/report/reply',
      data
    );
  }

  closeReport(id): Observable<any> {
    return this.http.get(
      environment.apiEndPoint + `tournament/close_report/${id}`
    );
  }

  createReport(data): Observable<any> {
    return this.http.post(
      environment.apiEndPoint + 'tournament/create_report',
      data
    );
  }
}
