import { TestBed } from "@angular/core/testing";

import { CodaShopService } from "./coda-shop.service";

describe("CodaShopService", () => {
  let service: CodaShopService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CodaShopService);
  });

  it("should be created", () => {
    expect(service).toBeTruthy();
  });
});
