import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";

import { Observable, SubscribableOrPromise } from "rxjs";
import { environment } from "../../../environments/environment";

@Injectable({
  providedIn: "root",
})
export class TransactionService {
  constructor(private http: HttpClient) {}

  getTransactionDetail(tDetail): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set("Content-Type", "application/json");

    return this.http.post(
      environment.apiEndPoint + "transaction/getDetail",
      tDetail,
      {
        headers: httpHeaders,
      }
    );
  }

  createOrUpdateTransaction(payload): SubscribableOrPromise<any> {
    return this.http
      .patch(`${environment.apiEndPoint}transaction/pay`, payload)
      .toPromise();
  }

  verifyStcTransaction(payload): SubscribableOrPromise<any> {
    return this.http
      .post(`${environment.apiEndPoint}transaction/stcVerifyPay`, payload)
      .toPromise();
  }

  getProduct(type): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set("Content-Type", "application/json");
    const url = `${environment.apiEndPoint}transaction/getProduct?type=${type}`;
    return this.http.get(url);
  }

  paymentDetail(params): SubscribableOrPromise<any> {
    const encodedPagination = encodeURIComponent(params["pagination"]);
    const url = `${environment.apiEndPoint}transaction/paymenthitstory?pagination=${encodedPagination}`;
    return this.http.get(url).toPromise();
  }
  paymentHistory(params): SubscribableOrPromise<any> {
    const encodedPagination = encodeURIComponent(params["pagination"]);
    const url = `${environment.apiEndPoint}transaction/admin/paymentHistory?userId=${params.userId}&pagination=${encodedPagination}`;
    return this.http.get(url).toPromise();
  }
  getRegFeeDisbursalStatus(tournamentId): SubscribableOrPromise<any> {
    const url = `${environment.apiEndPoint}transaction/disburse_reg_fee_status?tournamentId=${tournamentId}`;
    return this.http.get(url).toPromise();
  }

  disburseRegFee(payload): SubscribableOrPromise<any> {
    const url = `${environment.apiEndPoint}transaction/disburse_reg_fee`;
    return this.http.post(url, payload).toPromise();
  }

  getPrizeMoneyRefundStatus(tournamentId): SubscribableOrPromise<any> {
    const url = `${environment.apiEndPoint}transaction/prize_money_refund_status?tournamentId=${tournamentId}`;
    return this.http.get(url).toPromise();
  }
  refundPrizeMoney(payload): SubscribableOrPromise<any> {
    const url = `${environment.apiEndPoint}transaction/refund_prize_money`;
    return this.http.post(url, payload).toPromise();
  }

  getRegFeeRefundStatus(query): SubscribableOrPromise<any> {
    const url = `${environment.apiEndPoint}transaction/reg_fee_refund_status`;
    return this.http.get(url, { params: query }).toPromise();
  }
  refundRegFee(payload): SubscribableOrPromise<any> {
    const url = `${environment.apiEndPoint}transaction/refund_reg_fee`;
    return this.http.post(url, payload).toPromise();
  }
}
