import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";

import { SubscribableOrPromise, Observable } from "rxjs";
import { environment } from "../../../environments/environment";

@Injectable({
  providedIn: "root",
})
export class CommentService {
  constructor(private http: HttpClient) {}

  upsertComment(data): SubscribableOrPromise<any> {
    return this.http
      .patch(`${environment.apiEndPoint}comment`, data)
      .toPromise();
  }

  deleteComment(id): SubscribableOrPromise<any> {
    return this.http
      .delete(`${environment.apiEndPoint}comment/${id}`)
      .toPromise();
  }

  upsertLike(data): SubscribableOrPromise<any> {
    return this.http
      .patch(environment.apiEndPoint + "comment/like", data)
      .toPromise();
  }

  getAllComment(type, id, page): SubscribableOrPromise<any> {
    return this.http
      .get(`${environment.apiEndPoint}comment/${type}/${id}?page=${page}`)
      .toPromise();
  }
}
