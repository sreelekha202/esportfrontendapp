import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";

import { Observable, SubscribableOrPromise, BehaviorSubject } from "rxjs";
import { environment } from "../../../environments/environment";

@Injectable({
  providedIn: "root",
})
export class TournamentService {
  public isTournamentPaymentProcessing: boolean = false;

  constructor(private http: HttpClient) {
    this.joinDetailSubject = new BehaviorSubject(null);
    this.joinDetail = this.joinDetailSubject.asObservable();
  }

  public joinDetailSubject: BehaviorSubject<any>;
  public joinDetail: Observable<any>;

  saveTournament(formValues): Observable<any> {
    const data = formValues;
    const httpHeaders = new HttpHeaders();
    httpHeaders.set("Content-Type", "application/json");

    return this.http.post(environment.apiEndPoint + "tournament", data, {
      headers: httpHeaders,
    });
  }

  public setSelectedTeam(value: any) {
    this.joinDetailSubject.next(value);
  }

  updateTournament(formValues, id): Observable<any> {
    const data = formValues;
    const httpHeaders = new HttpHeaders();
    httpHeaders.set("Content-Type", "application/json");

    return this.http.put(environment.apiEndPoint + `tournament/${id}`, data, {
      headers: httpHeaders,
    });
  }

  getTournaments(params, selectParams = ``, option = ""): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set("Content-Type", "application/json; charset=utf-8");
    const encodedUrl = encodeURIComponent(params.query);
    let url =
      environment.apiEndPoint + `tournament?query=${encodedUrl}${selectParams}`;

    if (option) {
      const opt = encodeURIComponent(option);
      url += `&option=${opt}`;
    }

    return this.http.get(url, { headers: httpHeaders });
  }

  getTournament(id): Observable<any> {
    return this.http.get(environment.apiEndPoint + `tournament/${id}`);
  }

  getTournamentBySlug(slug): Observable<any> {
    return this.http.get(environment.apiEndPoint + `tournament/slug/${slug}`);
  }

  saveParticipant(data): Observable<any> {
    return this.http.post(environment.apiEndPoint + "participant", data);
  }

  getParticipants(encodedUrl): Observable<any> {
    return this.http.get(environment.apiEndPoint + `participant${encodedUrl}`);
  }

  searchParticipant(field, text, tournamentId, pId?): Observable<any> {
    let urlString = pId
      ? `participant/search?field=${field}&text=${encodeURIComponent(
          text
        )}&tournamentId=${tournamentId}&pId=${pId}`
      : `participant/search?field=${field}&text=${encodeURIComponent(
          text
        )}&tournamentId=${tournamentId}`;
    return this.http.get(environment.apiEndPoint + urlString);
  }

  updateParticipant(id, data): Observable<any> {
    return this.http.patch(environment.apiEndPoint + `participant/${id}`, data);
  }

  updateParticipantBulk(data): Observable<any> {
    return this.http.put(
      environment.apiEndPoint + `participant/updateAll`,
      data
    );
  }

  getParticipantTournament(params): Observable<any> {
    const encodedUrl = encodeURIComponent(params.query);
    const encodedPagination = encodeURIComponent(params.pagination);
    const url = `${environment.apiEndPoint}participant/tournament?query=${encodedUrl}&pagination=${encodedPagination}`;
    return this.http.get(url);
  }

  getJoinedTournamentsByUser(params): Observable<any> {
    const encodedUrl = encodeURIComponent(params.query);
    const encodedPagination = encodeURIComponent(params.pagination);
    const url = `${environment.apiEndPoint}admin/participant/tournament?query=${encodedUrl}&pagination=${encodedPagination}`;
    return this.http.get(url);
  }

  /**
   * Delete Tournament details By request Id
   * @param id: Request Id
   */
  deleteTournament(id): Observable<any> {
    return this.http.delete(environment.apiEndPoint + `tournament/${id}`);
  }

  getGames(): Observable<any> {
    return this.http.get(environment.apiEndPoint + `game`);
  }

  getStreamToken(streamId): SubscribableOrPromise<any> {
    return this.http
      .get(`${environment.apiEndPoint}video-streaming?streamId=${streamId}`)
      .toPromise();
  }

  getStreamKey(): SubscribableOrPromise<any> {
    return this.http
      .get(`${environment.apiEndPoint}video-streaming/generate-stream-key`)
      .toPromise();
  }

  getPaginatedTournaments(params) {
    let { status, limit, sort, page, game, organizer, prefernce } = params;
    let url = `${environment.apiEndPoint}home/tournament?`;

    if (status != null && status != undefined && status != "") {
      url += `&status=${status}`;
    }

    if (limit != null && limit != undefined && limit != "") {
      url += `&limit=${limit}`;
    }

    if (page) {
      url += `&page=${page}`;
    }

    if (sort) {
      url += `&sort=${sort}`;
    }

    if (game) {
      url += `&game=${game}`;
    }

    if (organizer) {
      url += `&organizer=${organizer}`;
    }

    if (prefernce) {
      url += `&preference=${encodeURIComponent(JSON.stringify(prefernce))}`;
    }

    return this.http.get(url);
  }

  fetchMyTournament(payload): SubscribableOrPromise<any> {
    const { status, page, limit } = payload;
    const url = `${environment.apiEndPoint}tournament/my_tournaments?status=${status}&page=${page}&limit=${limit}`;
    return this.http.get(url).toPromise();
  }

  fetchTournamentByStatus(payload): SubscribableOrPromise<any> {
    const { status, page, limit } = payload;
    const url = `${environment.apiEndPoint}tournament/get_all_tournament_by_status?status=${status}&page=${page}&limit=${limit}`;
    return this.http.get(url).toPromise();
  }

  fetchParticipantRegistrationStatus(
    id,
    participantId = null
  ): SubscribableOrPromise<any> {
    let url = `${environment.apiEndPoint}participant/join-status?tournamentId=${id}`;
    if (participantId) {
      url += `&guestId=${participantId}`;
    }
    return this.http.get(url).toPromise();
  }

  getRegFeeDisbursals(payload): SubscribableOrPromise<any> {
    const encodedPayload = encodeURIComponent(payload);
    const url = `${environment.apiEndPoint}tournament/disbursals/reg_fee_disbursals?query=${encodedPayload}`;
    return this.http.get(url).toPromise();
  }

  fetchParticipantRegistrationStatusMyTeams(
    id,
    participantId
  ): Observable<any> {
    return this.http.get(
      `${environment.apiEndPoint}participant/join-status-my-teams?tournamentId=${id}&participantId=${participantId}`
    );
  }
  /**
   * Reject Tournament details By request Id
   * @param id: Request Id
   */

  rejectTournament(formValues): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set("Content-Type", "application/json");

    return this.http.patch(
      environment.apiEndPoint + `tournament/reject`,
      formValues,
      {
        headers: httpHeaders,
      }
    );
  }

  getPrizeMoneyRefundDisbursals(query): SubscribableOrPromise<any> {
    const encodedQuery = encodeURIComponent(query);
    const url = `${environment.apiEndPoint}tournament/disbursals/prize_money_refund_disbursals?query=${encodedQuery}`;
    return this.http.get(url).toPromise();
  }

  getRegFeeRefundDisbursals(query): SubscribableOrPromise<any> {
    const encodedQuery = encodeURIComponent(query);
    const url = `${environment.apiEndPoint}tournament/disbursals/reg_fee_refund_disbursals?query=${encodedQuery}`;
    return this.http.get(url).toPromise();
  }

  abortTournament(formValues): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set("Content-Type", "application/json");

    return this.http.patch(
      environment.apiEndPoint + `tournament/abort`,
      formValues,
      {
        headers: httpHeaders,
      }
    );
  }
}
