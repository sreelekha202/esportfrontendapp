import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

import { environment } from "../../../environments/environment";

@Injectable({
  providedIn: "root",
})
export class RewardService {
  BaseURL = environment.apiEndPoint;

  constructor(private httpClient: HttpClient) {}

  public getRewardsData() {
    return this.httpClient.get(`${this.BaseURL}general/reward`);
  }

  public updateRewards(formArrayData) {
    return this.httpClient.post(`${this.BaseURL}general/reward`, formArrayData);
  }
}
