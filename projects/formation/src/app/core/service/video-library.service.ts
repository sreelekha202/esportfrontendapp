import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";

import { environment } from "../../../environments/environment";
import { IVideoLibrary } from "../../shared/models";
import { SubscribableOrPromise } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class VideoLibraryService {
  constructor(private http: HttpClient) {}

  createVideoLibrary(payload: IVideoLibrary): SubscribableOrPromise<any> {
    return this.http
      .post(`${environment.apiEndPoint}video-library`, payload)
      .toPromise();
  }

  fetchVideoLibrary(query): SubscribableOrPromise<any> {
    return this.http
      .get(`${environment.apiEndPoint}video-library${query}`)
      .toPromise();
  }

  fetchVideoLibraryById(id): SubscribableOrPromise<any> {
    return this.http
      .get(`${environment.apiEndPoint}video-library/${id}`)
      .toPromise();
  }

  fetchVideoLibraryBySlug(slug): SubscribableOrPromise<any> {
    return this.http
      .get(`${environment.apiEndPoint}video-library/slug/${slug}`)
      .toPromise();
  }

  updateVideoLibrary(payload: IVideoLibrary): SubscribableOrPromise<any> {
    return this.http
      .put(`${environment.apiEndPoint}video-library/${payload.id}`, payload)
      .toPromise();
  }

  deleteVideoLibrary(id): SubscribableOrPromise<any> {
    return this.http
      .delete(`${environment.apiEndPoint}video-library/${id}`)
      .toPromise();
  }

  getVideoThumbnail(videoId) {
    this.http
      .get(`https://img.youtube.com/vi/${videoId}/hqdefault.jpg`)
      .toPromise();
  }
}
