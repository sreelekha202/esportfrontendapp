import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";

import { Observable } from "rxjs";
import { environment } from "../../../environments/environment";

@Injectable({
  providedIn: "root",
})
export class LikeService {
  constructor(private http: HttpClient) {}

  saveLike(formValues): Observable<any> {
    const data = formValues;
    const httpHeaders = new HttpHeaders();
    httpHeaders.set("Content-Type", "application/json");

    return this.http.post(environment.apiEndPoint + "like", data, {
      headers: httpHeaders,
    });
  }

  updateLike(formValues, id): Observable<any> {
    const data = formValues;
    const httpHeaders = new HttpHeaders();
    httpHeaders.set("Content-Type", "application/json");

    return this.http.put(environment.apiEndPoint + `like/${id}`, data, {
      headers: httpHeaders,
    });
  }

  getLike(articleId, userId): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set("Content-Type", "application/json");

    return this.http.get(
      environment.apiEndPoint +
        `like/article?articleId=${articleId}&userId=${userId}`,
      {
        headers: httpHeaders,
      }
    );
  }

  getTournamentCommentsWithLikes(params) {
    return this.http.get(
      environment.apiEndPoint +
        `like/tournament?userId=${params["userId"]}&tournamentId=${params["tournamentId"]}&limit=${params["limit"]}&skip=${params["skip"]}`
    );
  }
}
