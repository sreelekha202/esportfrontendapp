import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";

import { environment } from "../../../environments/environment";

@Injectable({
  providedIn: "root",
})
export class AdvertisementService {
  BaseURL = environment.apiEndPoint;

  constructor(private httpClient: HttpClient) {}

  public addAdvertisement(id, formData) {
    formData.isSystemAd = true;
    formData.advertisementType = "Banner Adds";
    return this.httpClient.post(`${this.BaseURL}advertisement/` + id, formData);
  }

  public getPendingAd() {
    const query = `?query=${encodeURIComponent(
      JSON.stringify({ approve: 0, status: 1 })
    )}`;

    return this.httpClient.get(`${this.BaseURL}advertisement/type${query}`);
  }

  public getRunningAd() {
    const query = `?query=${encodeURIComponent(
      JSON.stringify({ approve: 1, status: 1, startDate: { $lt: new Date() } })
    )}`;

    return this.httpClient.get(`${this.BaseURL}advertisement/type${query}`);
  }

  public getPausedAd() {
    const query = `?query=${encodeURIComponent(
      JSON.stringify({
        approve: 1,
        isPaused: 1,
        status: 1,
        startDate: { $lt: new Date() },
      })
    )}`;

    return this.httpClient.get(`${this.BaseURL}advertisement/type${query}`);
  }

  public getFinishedAd() {
    const query = `?query=${encodeURIComponent(
      JSON.stringify({
        approved: 1,
        status: 1,
        startDate: { $lt: new Date() },
        endDate: { $lt: new Date() },
      })
    )}`;

    return this.httpClient.get(`${this.BaseURL}advertisement/type${query}`);
  }

  public getBannerAd() {
    const query = `?query=${encodeURIComponent(
      JSON.stringify({
        advertisementType: "Banner Adds",
        isSystemAd: true,
      })
    )}`;

    return this.httpClient.get(`${this.BaseURL}advertisement/type${query}`);
  }
}
