import { Injectable, Inject, PLATFORM_ID } from "@angular/core";
import { environment } from "./../../../environments/environment";
import { Observable, BehaviorSubject } from "rxjs";
//https://github.com/angular/universal/issues/1236
//import { Socket } from 'ngx-socket-io';
import io from "socket.io-client";
import { isPlatformBrowser } from "@angular/common";
import { HttpClient, HttpHeaders, HttpBackend } from "@angular/common/http";
import { IChat } from "../../shared/models";

import { TournamentService } from "./tournament.service";
import { ToastService } from "./../../shared/toast/toast.service";
import { UserService } from "./user.service";

@Injectable({
  providedIn: "root",
})
export class ChatService {
  private tournamentList = [];
  private url = environment.socketEndPoint;
  private userId: any;
  public accessToken: String;
  public chatStatus: Observable<any>;
  public chatStatusSub: BehaviorSubject<boolean>;
  public chatWindowPos: Observable<any>;
  public chatWindowPosSub: BehaviorSubject<any>;
  public cid: string;
  public currentMatch: Observable<any>;
  public currentMatchSub: BehaviorSubject<any>;
  public currentUserId: any;
  public currenUser: any;
  public isUserLoggedIn: any;
  public login: Observable<any>;
  public loginSub: BehaviorSubject<any>;
  public mtches: Observable<any>;
  public mtchesSub: BehaviorSubject<any>;
  public refreshToken: String;
  public showStatus: boolean = false;
  public socket: any;
  public totalMatches: number;
  public typeOfChat: Observable<any>;
  public typeOfChatSub: BehaviorSubject<any>;

  constructor(
    @Inject(PLATFORM_ID) private platformId: Object, //private socket: Socket
    private handler: HttpBackend,
    private httpClient: HttpClient,
    private toastService: ToastService,
    private tournamentService: TournamentService,
    private userService: UserService
  ) {
    if (isPlatformBrowser(this.platformId)) {
      // check if this is runing in browser
      this.socket = io(this.url); // call io constructor
      //this.init(); // do the all initilization, e.g. this.socket.on('message', msg => {})
    }

    this.httpClient = new HttpClient(handler);
    this.currentMatchSub = new BehaviorSubject([]);
    this.currentMatch = this.currentMatchSub.asObservable();
    this.typeOfChatSub = new BehaviorSubject([]);
    this.typeOfChat = this.typeOfChatSub.asObservable();
    this.mtchesSub = new BehaviorSubject([]);
    this.mtches = this.mtchesSub.asObservable();
    this.loginSub = new BehaviorSubject([]);
    this.login = this.loginSub.asObservable();

    this.chatStatusSub = new BehaviorSubject<boolean>(false);
    this.chatStatus = this.chatStatusSub.asObservable();

    this.chatWindowPosSub = new BehaviorSubject<string>(
      "chat_window chat_window_right"
    );

    this.chatWindowPos = this.chatWindowPosSub.asObservable();
    this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.currenUser = data;
        this.isUserLoggedIn = !!this.currenUser;
        this.currentUserId = this.currenUser._id;
        if (this.isUserLoggedIn) {
          this.doLogin(this.currenUser.phoneNumber, "", "withoutpassword");
        }
      }
    });

    const tk = localStorage.getItem(environment.currentToken);

    if (tk) {
      this.getCurrentUserDetails();
    }
  }

  public getCurrentUserDetails() {
    this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.userId = data["_id"];
      }
    });
  }
  public startTournament(matchid) {
    if (typeof this.accessToken != "undefined" && this.accessToken != null) {
      const httpOptions = {
        headers: new HttpHeaders({
          Authorization: `Bearer ` + this.accessToken,
          "Content-Type": "application/json",
        }),
      };
      const gurl = `${this.url}/addMatchDetails/${matchid}`;
      return this.httpClient?.get(gurl, httpOptions);
    }
  }
  public doLogin(pnumber, password, type) {
    let gurl = `${this.url}/login`;
    const body = { phone_number: pnumber, password: password, type: type };

    this.httpClient.post<any>(gurl, body).subscribe((data) => {
      this.accessToken = data.accessToken;
      this.refreshToken = data.refreshToken;
      this.loginSub.next(data);
    });
  }

  public doRefresh() {
    if (this.refreshToken != "") {
      let gurl = `${this.url}/token`;
      const body = { token: this.refreshToken };

      this.httpClient.post<any>(gurl, body).subscribe((data) => {
        this.accessToken = data.accessToken;
      });
    }
  }

  public doLogout() {
    let gurl = `${this.url}/logout`;
    const body = { token: this.accessToken };

    this.httpClient.post<any>(gurl, body).subscribe((data) => {
      this.accessToken = "";
      this.refreshToken = "";
    });
  }

  public connectUser() {
    if (isPlatformBrowser(this.platformId)) {
      this.socket?.emit("connectuser", this.currentUserId);
    }
  }

  public disconnectUser() {
    if (isPlatformBrowser(this.platformId)) {
      this.socket?.emit("disconnectuser", this.currentUserId);
    }
  }

  public connectMatch(matchid, type) {
    if (isPlatformBrowser(this.platformId)) {
      const body = { matchid: matchid, type: type };
      this.socket?.emit("matchConnected", body);
    }
  }

  public disconnectMatch(matchid, type) {
    if (isPlatformBrowser(this.platformId)) {
      const body = { matchid: matchid, type: type };
      this.socket?.emit("matchDisconnected", body);
    }
  }

  public sendMessage(message: IChat, type) {
    if (isPlatformBrowser(this.platformId)) {
      const body = { message: message, type: type };
      this.socket?.emit("new-message", body);
      if (this.currentUserId) {
        this.getAllMatches(this.currentUserId, 1);
      }
    }
  }

  public updateUnread() {
    if (isPlatformBrowser(this.platformId)) {
      this.socket?.emit("new-message-for-user", this.currentUserId);
    }
  }

  public deleteUnread(matchid) {
    if (isPlatformBrowser(this.platformId)) {
      const body = { userid: this.currentUserId, matchid: matchid };
      this.socket?.emit("delete-unread", body);
    }
  }

  public sendTyping(typingObject, type) {
    if (isPlatformBrowser(this.platformId)) {
      const body = { typingObject: typingObject, type: type };
      this.socket?.emit("typing", body);
    }
  }

  public stopTyping(typingObject, type) {
    if (isPlatformBrowser(this.platformId)) {
      const body = { typingObject: typingObject, type: type };
      this.socket?.emit("stopTyping", body);
    }
  }

  public getMessages = () => {
    return Observable.create((observer) => {
      if (isPlatformBrowser(this.platformId)) {
        this.socket?.on("received", (message) => {
          if (this.getChatStatus() == true) {
            if (message.receiverid == this.currentUserId) {
              this.deleteUnread(message.matchid);
            }
          }
          this.updateUnread();
          observer.next(message);
        });
      }
    });
  };

  public getUnReadCount = () => {
    return Observable.create((observer) => {
      if (isPlatformBrowser(this.platformId)) {
        this.socket?.on("received-message-for-user", (receivemessage) => {
          this.getAllUnreadCount().subscribe((res: any) => {
            this.getAllMatches(this.currentUserId, 0);
            observer.next(res);
          });
        });
      }
    });
  };

  public deleteChat(chatid) {
    if (typeof this.accessToken != "undefined" && this.accessToken != null) {
      const httpOptions = {
        headers: new HttpHeaders({
          Authorization: `Bearer ` + this.accessToken,
          "Content-Type": "application/json",
        }),
      };

      let gurl = `${this.url}/chats-v2/deletechat/${chatid}/${this.currentUserId}`;
      return this.httpClient?.get(gurl, httpOptions);
    }
  }

  public blockUser(userid, comment) {
    if (typeof this.accessToken != "undefined" && this.accessToken != null) {
      const httpOptions = {
        headers: new HttpHeaders({
          Authorization: `Bearer ` + this.accessToken,
          "Content-Type": "application/json",
        }),
      };

      let gurl = `${this.url}/blockuser/${this.currentUserId}/blck/${userid}/${comment}`;
      return this.httpClient?.get(gurl, httpOptions);
    }
  }

  public getAllUnreadCount() {
    if (typeof this.accessToken != "undefined" && this.accessToken != null) {
      const httpOptions = {
        headers: new HttpHeaders({
          Authorization: `Bearer ` + this.accessToken,
          "Content-Type": "application/json",
        }),
      };

      let gurl = `${this.url}/unreadcount/${this.currentUserId}`;
      return this.httpClient?.get(gurl, httpOptions);
    }
  }

  public getAllMatches(userid, update) {
    if (typeof this.accessToken != "undefined" && this.accessToken != null) {
      const httpOptions = {
        headers: new HttpHeaders({
          "Content-Type": "application/json",
          Authorization: `Bearer ` + this.accessToken,
        }),
      };

      let gurl = `${this.url}/matchlist-v2/${userid}`;
      this.httpClient?.get(gurl, httpOptions).subscribe((res: any) => {
        if (update == 1) {
          this.updateUnread();
        }
        let mlist = [];
        for (let i = 0; i < res.allchatwindows.length; i++) {
          if (res.allchatwindows[i].type == "user") {
            mlist.push(res.allchatwindows[i]);
          }
        }
        this.mtchesSub.next(mlist);
      });
    }
  }

  public getTypingInfo = () => {
    return Observable.create((observer) => {
      if (isPlatformBrowser(this.platformId)) {
        this.socket?.on("notifyTyping", (typingObject) => {
          observer.next(typingObject);
        });
      }
    });
  };

  public getStopTypingInfo = () => {
    return Observable.create((observer) => {
      if (isPlatformBrowser(this.platformId)) {
        this.socket?.on("notifyStopTyping", (typingObject) => {
          observer.next(typingObject);
        });
      }
    });
  };

  public getAllChatMessages(matchid) {
    if (typeof this.accessToken != "undefined" && this.accessToken != null) {
      const httpOptions = {
        headers: new HttpHeaders({
          "Content-Type": "application/json",
          Authorization: `Bearer ` + this.accessToken,
        }),
      };

      let gurl = `${this.url}/chats-v2/${matchid}/${this.currentUserId}`;
      return this.httpClient?.get(gurl, httpOptions);
    }
  }

  public getAllUserChatMessages(matchid) {
    if (typeof this.accessToken != "undefined" && this.accessToken != null) {
      const httpOptions = {
        headers: new HttpHeaders({
          "Content-Type": "application/json",
          Authorization: `Bearer ` + this.accessToken,
        }),
      };
      let gurl = `${this.url}/user-v2/get-one-to-one-all-chats?uniqueId=${matchid}&userid=${this.currentUserId}`;
      return this.httpClient?.get(gurl, httpOptions);
    }
  }

  public getAllSpamList() {
    if (typeof this.accessToken != "undefined" && this.accessToken != null) {
      const httpOptions = {
        headers: new HttpHeaders({
          "Content-Type": "application/json",
          Authorization: `Bearer ` + this.accessToken,
        }),
      };
      let gurl = `${this.url}/spamlist`;
      return this.httpClient?.get(gurl, httpOptions);
    }
  }

  public getOwnerForTournament(tournamentid) {
    if (typeof this.accessToken != "undefined" && this.accessToken != null) {
      const httpOptions = {
        headers: new HttpHeaders({
          Authorization: `Bearer ` + this.accessToken,
          "Content-Type": "application/json",
        }),
      };

      let gurl = `${this.url}/ownerfortournament/${tournamentid}`;
      return this.httpClient?.get(gurl, httpOptions);
    }
  }

  public getMatchInformation(matchid) {
    if (typeof this.accessToken != "undefined" && this.accessToken != null) {
      const httpOptions = {
        headers: new HttpHeaders({
          Authorization: `Bearer ` + this.accessToken,
          "Content-Type": "application/json",
        }),
      };

      let gurl = `${this.url}/matchdetails/${matchid}`;
      return this.httpClient?.get(gurl, httpOptions);
    }
  }

  public searchUsers(text) {
    if (typeof this.accessToken != "undefined" && this.accessToken != null) {
      const httpOptions = {
        headers: new HttpHeaders({
          Authorization: `Bearer ` + this.accessToken,
          "Content-Type": "application/json",
        }),
      };

      let gurl = `${this.url}/user-v2/search_users?text=${text}`;
      return this.httpClient?.get(gurl, httpOptions);
    }
  }

  public upload(formData) {
    if (typeof this.accessToken != "undefined" && this.accessToken != null) {
      const httpOptions = {
        headers: new HttpHeaders({
          Authorization: `Bearer ` + this.accessToken,
        }),
      };
      let gurl = `${this.url}/uploadfiles`;
      return this.httpClient.post<any>(gurl, formData, httpOptions);
    }
  }

  public setChatStatus(cstatus: boolean) {
    this.chatStatusSub.next(cstatus);
  }

  public setTypeOfChat(ty) {
    this.typeOfChatSub.next(ty);
  }

  public getTypeOfChat() {
    return this.typeOfChatSub.getValue();
  }

  public setCurrentMatch(md) {
    this.currentMatchSub.next(md);
  }

  public getCurrentMatch() {
    return this.currentMatchSub.getValue();
  }

  public getChatStatus() {
    return this.chatStatusSub.getValue();
  }

  public toggleChat() {
    if (this.chatStatusSub.getValue() == true) {
      this.chatStatusSub.next(false);
    } else {
      this.chatStatusSub.next(true);
    }
  }

  public setWindowPos(winpos) {
    this.chatWindowPosSub.next(winpos);
  }

  public getWindowPos() {
    return this.chatWindowPosSub.getValue();
  }

  public fetchTournaments = async () => {
    try {
      this.tournamentList = [];
      const query = JSON.stringify({
        createdBy: this.userId,
        isFinished: false,
        tournamentStatus: "publish",
      });

      const projection = `&select=_id,name,description,startDate,gameDetail,createdBy,isFinished,tournamentStatus`;

      const tournament = await this.tournamentService
        .getTournaments({ query }, projection)
        .toPromise();

      this.tournamentList = [...tournament?.data] || [];

      return this.tournamentList;
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
      return this.tournamentList;
    }
  };

  public getAllTournamentsForTheUser = () => {
    return Observable.create(async (observer) => {
      observer.next(await this.fetchTournaments());
    });
  };
}
