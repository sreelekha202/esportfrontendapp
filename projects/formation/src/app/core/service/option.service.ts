import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";

import { SubscribableOrPromise } from "rxjs";
import { environment } from "../../../environments/environment";

@Injectable({
  providedIn: "root",
})
export class OptionService {
  constructor(private http: HttpClient) {}

  fetchAllGenres(): SubscribableOrPromise<any> {
    return this.http
      .get(`${environment.apiEndPoint}option/all-genres`)
      .toPromise();
  }

  fetchAllCategories(): SubscribableOrPromise<any> {
    return this.http
      .get(`${environment.apiEndPoint}option/all-categories`)
      .toPromise();
  }

  fetchAllTags(): SubscribableOrPromise<any> {
    return this.http
      .get(`${environment.apiEndPoint}option/all-tags`)
      .toPromise();
  }
}
