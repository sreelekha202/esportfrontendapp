import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
} from "@angular/common/http";

import { ToastService } from "../../shared/toast/toast.service";

import {
  BehaviorSubject,
  Observable,
  SubscribableOrPromise,
  throwError,
} from "rxjs";
import { map, catchError } from "rxjs/operators";

import { environment } from "../../../environments/environment";
import { GlobalUtils } from "../../shared/service/global-utils/global-utils";
import { IUser } from "../../shared/models";

const API = environment.apiEndPoint + "user/";
const token = environment.currentToken;

@Injectable({
  providedIn: 'root',
})
export class UserService {
  public currentUserSubject: BehaviorSubject<IUser>;
  public currentUser: Observable<IUser>;

  constructor(
    private http: HttpClient,
    private router: Router,
    public toastService: ToastService
  ) {
    this.currentUserSubject = new BehaviorSubject(null);
    this.currentUser = this.currentUserSubject.asObservable();
  }

  
  
  refreshToken() {
    if (GlobalUtils.isBrowser()) {
      let data = {
        refreshToken: localStorage.getItem('refreshToken'),
      };

      return this.http
        .post<any>(environment.apiEndPoint + 'auth/web_refresh_token', data, {})
        .pipe(
          map((user) => {
            if (GlobalUtils.isBrowser()) {
              localStorage.setItem(environment.currentToken, user.data);
            }

            this.startRefreshTokenTimer();
            return user;
          })
        );
    }
  }

  /**
   * Get Profile details
   * @param token: profile data based on token Id
   */
  getProfile(): Observable<any> {
    let currToken;

    if (GlobalUtils.isBrowser()) {
      currToken = localStorage.getItem(token);
    }

    return this.http.get(API + 'profile').pipe(
      map((value) => {
        return value;
      }),
      catchError(this.handleError)
    );
  }

  // Return the current User details
  public getAuthenticatedUser(): IUser {
    let currToken;

    if (GlobalUtils.isBrowser()) {
      currToken = localStorage.getItem(token);
    }

    if (this.currentUserSubject.value && currToken) {
      // Check if current user details and token are available
      return this.currentUserSubject.value;
    } else if (currToken && !this.currentUserSubject.value) {
      // if token is available but user details is not available in observables
      this.getProfile().subscribe(
        (res) => {
          this.currentUserSubject.next(res.data);
          return this.currentUserSubject.value;
        },
        (error) => {
          this.logout();
          this.router.navigate(['/home']);
          this.toastService.showError(error);
        }
      );
      return this.currentUserSubject.value;
    } else {
      // return null if token and user details not available
      return null;
    }
  }

  updatePaymentInfo(data): Observable<any> {
    return this.http.post(API + 'add_payment_info', data);
  }

  getUserinviteesList(params): Observable<any> {
    const encodedPagination = encodeURIComponent(params['pagination']);
    return this.http.get(API + `get_reffered_user?query=${encodedPagination}`);
  }

  updateProfile(data) {
    return this.http.post(API + 'user_update', data).pipe(
      map((value) => {
        return value;
      }),
      catchError(this.handleError)
    );
  }

  refreshCurrentUser() {
    this.getProfile().subscribe((res) => {
      this.currentUserSubject.next(res.data);
      return this.currentUserSubject.value;
    });
  }

  removeUser(id): Observable<any> {
    return this.http.delete(API + `remove/${id}`);
  }

  getRecentlyUpdatedUsers(params): Observable<any> {
    const encodedUrl = encodeURIComponent(params['query']);
    const encodedPagination = encodeURIComponent(params['pagination']);
    return this.http.get(
      API + `usersPaginated?query=${encodedUrl}&pagination=${encodedPagination}`
    );
  }
  // Error
  handleTokenError(error: HttpErrorResponse) {
    let that = this;
    that.logout();
    let msg = '';

    if (error?.error instanceof ErrorEvent) {
      // client-side error
      msg = error?.error?.message;
    } else {
      // server-side error
      msg = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }

    return throwError(msg);
  }

  handleError(error: HttpErrorResponse) {
    if (GlobalUtils.isBrowser() && error?.error?.message == 'Unauthorized') {
      localStorage.removeItem(token);
      window.location.reload();
    }

    let msg = '';

    if (error?.error instanceof ErrorEvent) {
      // client-side error
      msg = error?.error?.message;
    } else {
      // server-side error
      msg = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }

    return throwError(msg);
  }

  /**
   * Use to set/update the current user profile
   * @param value : update user requests
   */
  public setCurrentUser(value: IUser) {
    this.currentUserSubject.next(value);
  }

  logout(): any {
    if (GlobalUtils.isBrowser()) {
      // remove user from local storage to log user out
      let data = {
        refreshToken: localStorage.getItem('refreshToken'),
      };

      this.http.post<any>(API + 'logout', data, {}).subscribe(
        () => {
          this.stopRefreshTokenTimer();
          this.currentUserSubject.next(null);

          localStorage.removeItem(token);
          localStorage.removeItem('refreshToken');

          window.location.reload();
        },
        (error) => {
          this.stopRefreshTokenTimer();
          this.currentUserSubject.next(null);

          localStorage.removeItem(token);
          localStorage.removeItem('refreshToken');

          window.location.reload();
        }
      );
    }
  }

  searchUsers(searchKeyword) {
    let query = JSON.stringify({
      text: { $regex: searchKeyword, $options: 'i' },
    });
    let projection = 'fullName,phoneNumber,email,profilePicture';
    let encodedUrl = encodeURIComponent(query);
    return this.http
      .get(API + `search_users?query=${encodedUrl}&select=${projection}`)
      .pipe(
        map((value) => {
          return value['data'];
        }),
        catchError((err) => err)
      );
  }

  add_social(authtoken, sprovider) {
    const data = {
      provider: sprovider,
      token: authtoken,
    };
    return this.http.post<any>(API + 'add_social', data);
  }

  addEmail(data): Observable<any> {
    return this.http.post(API + 'user_update', data);
  }

  createAdminUser(formData): Observable<any> {
    return this.http.post(API + 'create_admin_user', formData);
  }

  verifyEmail(data): Observable<any> {
    return this.http.post(API + 'user_verify', data);
  }

  //#region - Get the user details for Admin Panel
  getRecentlyUpdatedUserList(): Observable<any> {
    return this.http.get(API + `users`);
  }

  getAdminUsers(query): Observable<any> {
    return this.http.get(API + `users${query}`);
  }

  getTopSpendingUserList(): Observable<any> {
    return this.http.get(API + `users-account`);
  }
  //#endregion

  getAllCountries(): Observable<any> {
    return this.http.get('./assets/json/countries.json');
  }

  getStates(): Observable<any> {
    return this.http.get<any>('./assets/json/states.json');
  }

  getTeamList(query): Observable<any> {
    return this.http.get(API + `teamsDetail${query}`);
  }

  create_team(data): Observable<any> {
    return this.http.post<any>(API + 'create_team', data);
  }

  team_update(team): Observable<any> {
    return this.http.patch(API + 'team_update', team);
  }

  getJoinMyTeam(id): SubscribableOrPromise<any> {
    const url = `${API}join_my_teams/${id}`;
    return this.http.get(url).toPromise();
  }

  getMyInvite(params): SubscribableOrPromise<any> {
    const encodedPagination = encodeURIComponent(params['pagination']);
    const url = `${API}my_invites?pagination=${encodedPagination}`;
    return this.http.get(url).toPromise();
  }

  getMyTeam(params): SubscribableOrPromise<any> {
    const encodedPagination = encodeURIComponent(params['pagination']);
    const url = `${API}my_teams?pagination=${encodedPagination}`;
    return this.http.get(url).toPromise();
  }

  public getTeamById(id): SubscribableOrPromise<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json; charset=utf-8');
    return this.http
      .get(`${environment.apiEndPoint}home/team/${id}`, {
        headers: httpHeaders,
      })
      .toPromise();
  }

  public getTeamMember(id): SubscribableOrPromise<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json; charset=utf-8');
    return this.http
      .get(`${API}team_member/${id}`, {
        headers: httpHeaders,
      })
      .toPromise();
  }

  public update_member(data): Observable<any> {
    return this.http.post<any>(API + 'teammember_update', data);
  }

  public update_invitees(data): Observable<any> {
    return this.http.post<any>(API + 'update_invites', data);
  }

  getRewardTransaction(params): SubscribableOrPromise<any> {
    const encodedPagination = encodeURIComponent(params['pagination']);
    const url = `${API}reward_transaction?pagination=${encodedPagination}`;
    return this.http.get(url).toPromise();
  }

  updateUser(formValues, id): Observable<any> {
    const data = formValues;
    return this.http.patch(API + `admin_user_update/${id}`, data);
  }

  getRewardTransactionByUserId(params): Observable<any> {
    let encodedUrl = encodeURIComponent(params['query']);
    return this.http.get(API + `admin_reward_tran?query=${encodedUrl}`);
  }

  private refreshTokenTimeout;

  public startRefreshTokenTimer() {
    let jwtToken;

    if (GlobalUtils.isBrowser()) {
      // parse json object from base64 encoded jwt token
      jwtToken = JSON.parse(atob(localStorage.getItem(token).split('.')[1]));
    }

    // set a timeout to refresh the token a minute before it expires
    const expires = new Date(jwtToken.exp * 1000);
    const timeout = expires.getTime() - Date.now() - 60 * 1000;
    this.refreshTokenTimeout = setTimeout(
      () =>
        this.refreshToken().subscribe(
          (data) => {},
          (error) => {
            this.logout();
          }
        ),
      timeout
    );
  }

  private stopRefreshTokenTimer() {
    clearTimeout(this.refreshTokenTimeout);
  }

  getUserPrefernces(id): Observable<any> {
    return this.http.get(API + `admin_user_preference/${id}`);
  }

  changePassword(data): Observable<any> {
    return this.http.post(API + 'change_password', data);
  }

  teamsDetail_Admin(params): Observable<any> {
    const encodedPagination = encodeURIComponent(params['pagination']);
    return this.http.get(API + `teamsDetail_Admin?query=${encodedPagination}`);
  }

  update_team_admin(team): Observable<any> {
    return this.http.patch(API + 'update_team_admin', team);
  }
  update_team_owner(team): Observable<any> {
    return this.http.patch(API + 'update_team_owner', team);
  }
  getTeamLogs(id): Observable<any> {
    return this.http.get(API + `get_team_activity_log/${id}`);
  }
  getUserAccountDetails(userId): Observable<any> {
    return this.http.get(API + `accountDetails?userId=${userId}`);
  }
  getUserDetails(id): Observable<any> {
    return this.http.get(API + `users/${id}`);
  }
  getTournamentByTeamId(teamId): Observable<any> {
    return this.http.get(API + `tournament/${teamId}`);
  }
}
