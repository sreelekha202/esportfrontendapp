import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";

import { environment } from "../../../environments/environment";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class UserPreferenceService {
  BaseURL = environment.apiEndPoint;

  constructor(private httpClient: HttpClient) {}

  public getPreferences(query, projection = ""): Observable<any> {
    let filter = encodeURIComponent(query);
    return this.httpClient.get(
      `${this.BaseURL}userpreference?query=${filter}&select=${projection}`
    );
  }

  removeBookmark(queryParam): Observable<any> {
    return this.httpClient.delete(
      `${this.BaseURL}userpreference/remove_bookmark?${queryParam}`
    );
  }

  addBookmark(data): Observable<any> {
    return this.httpClient.patch(
      `${this.BaseURL}userpreference/add_bookmark`,
      data
    );
  }

  getAllSettingPageData(): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set("Content-Type", "application/json");

    return this.httpClient.get(
      environment.apiEndPoint + `userpreference/get_all_setting_preference`,
      {
        headers: httpHeaders,
      }
    );
  }

  getLoginPrefrence(): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set("Content-Type", "application/json");

    return this.httpClient.get(
      environment.apiEndPoint + `userpreference/login_user`
    );
  }

  addPrefrence(data): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set("Content-Type", "application/json");

    return this.httpClient.post(
      environment.apiEndPoint + `userpreference/add_preference`,
      data
    );
  }
}
