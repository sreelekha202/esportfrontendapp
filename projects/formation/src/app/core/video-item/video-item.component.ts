import { Component, Input, OnInit } from "@angular/core";
import { LanguageService, UtilsService } from "../../core/service";

import { AppHtmlRoutes } from "../../app-routing.model";

export interface VideoItemComponentData {
  date: string;
  id: number;
  img: string;
  text: string;
  title: string;
}

@Component({
  selector: "app-video-item",
  templateUrl: "./video-item.component.html",
  styleUrls: ["./video-item.component.scss"],
})
export class VideoItemComponent implements OnInit {
  @Input() data;

  AppHtmlRoutes = AppHtmlRoutes;

  constructor(public languageService: LanguageService) {}

  ngOnInit(): void {}
}
