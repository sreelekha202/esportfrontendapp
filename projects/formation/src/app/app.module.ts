import { AngularFireMessagingModule } from "@angular/fire/messaging";
import { AngularFireModule } from "@angular/fire";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { DatePipe } from "@angular/common";
import { HTTP_INTERCEPTORS } from "@angular/common/http";
import { NgModule, APP_INITIALIZER } from "@angular/core";

import { JwtInterceptor } from "./core/helpers/interceptors/token-interceptors.service";
import { PaginationService } from "./core/service/pagination.service";
import { provideConfig } from "./core/service/social-media-auth.service";
import { S3UploadService } from "./shared/service/s3Upload.service";
import { ToastService } from "./shared/toast/toast.service";
import { UserService } from "./core/service";

import { AppRoutingModule } from "./app-routing.module";
import { CoreModule } from "./core/core.module";
import { I18nModule } from "./i18n/i18n.module";
import { PipeModule } from "./shared/pipe/pipe.module";
import { SharedModule } from "./shared/modules/shared.module";

import { AppComponent } from "./app.component";
import { FooterComponent } from "./core/footer/footer.component";
import { GamesComponent } from "./modules/games/games.component";
import { HeaderComponent } from "./core/header/header.component";
import { Home2Component } from "./modules/home2/home2.component";
import { LeaderBoardComponent } from "./modules/leader-board/leader-board.component";
import { MatchmakerDialogComponent } from "./modules/games/matchmaker-dialog/matchmaker-dialog.component";
import { NotfoundComponent } from "./modules/not-found/not-found.component";
import { PrivacyPolicyComponent } from "./modules/privacy-policy/privacy-policy.component";
import { ToastsContainer } from "./shared/toast/toast-container.component";

import { appInitializer } from "./core/helpers/app.initializer";
import { environment } from "../environments/environment";

import {
  SocialAuthServiceConfig,
  SocialLoginModule,
  GoogleLoginProvider,
  FacebookLoginProvider,
} from 'angularx-social-login';
import { CookieService } from "ngx-cookie-service";
import { InlineSVGModule } from "ng-inline-svg";
import { SocketIoModule, SocketIoConfig } from "ngx-socket-io";

import {
  BrowserModule,
  BrowserTransferStateModule,
} from "@angular/platform-browser";

const config: SocketIoConfig = { url: environment.socketEndPoint, options: {} };

@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    GamesComponent,
    HeaderComponent,
    Home2Component,
    LeaderBoardComponent,
    MatchmakerDialogComponent,
    NotfoundComponent,
    PrivacyPolicyComponent,
    ToastsContainer,
  ],
  imports: [
    AngularFireMessagingModule,
    AngularFireModule.initializeApp(environment.firebase),
    AppRoutingModule,
    BrowserAnimationsModule,
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    BrowserTransferStateModule,
    CoreModule,
    I18nModule,
    InlineSVGModule.forRoot(),
    //NgxIntlTelInputModule,
    PipeModule,
    SharedModule,
    SocialLoginModule,
    SocketIoModule.forRoot(config),
  ],
  exports: [HeaderComponent, FooterComponent, ToastsContainer],
  providers: [
    CookieService,
    DatePipe,
    PaginationService,
    S3UploadService,
    ToastService,
    {
      provide: APP_INITIALIZER,
      useFactory: appInitializer,
      multi: true,
      deps: [UserService],
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptor,
      multi: true,
    },
    {
      provide: 'SocialAuthServiceConfig',
      useValue: {
        autoLogin: false,
        providers: [
          {
            id: GoogleLoginProvider.PROVIDER_ID,
            provider: new GoogleLoginProvider(environment.googleAPPID),
          },
          {
            id: FacebookLoginProvider.PROVIDER_ID,
            provider: new FacebookLoginProvider(environment.facebookAPPID),
          },
        ],
      } as SocialAuthServiceConfig,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
