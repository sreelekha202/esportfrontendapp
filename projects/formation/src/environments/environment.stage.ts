export const environment = {
  production: false,
  buildConfig: "stage",
  // apiEndPoint: 'https://w5chwyhsr3.execute-api.ap-southeast-1.amazonaws.com/dev/',
  apiEndPoint: "https://0oyg0452m9.execute-api.eu-west-3.amazonaws.com/stage/",
  currentToken: "DUID",
  facebookAPPID: "1417432915264232",
  googleAPPID:
    "346336302247-93msahs9cjrh82dfefgb824sske1g1uo.apps.googleusercontent.com",
  socketEndPoint: "https://chat-formation.dynasty-staging.com",
  cookieDomain: ".dynasty-staging.com",
  eCommerceAPIEndPoint:
    "https://tuub60cp6h.execute-api.ap-southeast-1.amazonaws.com/dev/",
  firebase: {
    apiKey: "AIzaSyB1mEgrCbtK1vvE4zR7VcOzXtcWs4ysvLw",
    authDomain: "dynasty-test.firebaseapp.com",
    databaseURL: "https://dynasty-test.firebaseio.com",
    projectId: "dynasty-test",
    storageBucket: "dynasty-test.appspot.com",
    messagingSenderId: "415594613035",
    appId: "1:415594613035:web:c0a1d274208530618add6e",
    measurementId: "G-YBTGYLZ7RN",
  },
  articleS3BucketName: "article",
  tournamentS3BucketName: "tournament",
  iOSBundelName: "",
  iOSAppstoreURL: "",
  paypal_client_id:
    "ASf-mIwkHu2j6hchcTquNOcfFtBbfzxC5qhbjOiXrQ1v-DTAlsUFnge0XHEJrUC_UtDiSBR7UvTK48WE&currency=EUR",
};
