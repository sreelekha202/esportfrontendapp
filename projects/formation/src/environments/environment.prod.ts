export const environment = {
  production: true,
  buildConfig: "prod",
  // apiEndPoint: 'https://w5chwyhsr3.execute-api.ap-southeast-1.amazonaws.com/dev/',
  apiEndPoint: "https://ks4vikwi12.execute-api.eu-west-3.amazonaws.com/prod/",
  currentToken: "DUID",
  facebookAPPID: "1417432915264232",
  googleAPPID:
    "346336302247-93msahs9cjrh82dfefgb824sske1g1uo.apps.googleusercontent.com",
  socketEndPoint: "https://chat.formation.gg",
  cookieDomain: ".formation.gg",
  eCommerceAPIEndPoint:
    "https://tuub60cp6h.execute-api.ap-southeast-1.amazonaws.com/dev/",
  firebase: {
    apiKey: "AIzaSyB1mEgrCbtK1vvE4zR7VcOzXtcWs4ysvLw",
    authDomain: "dynasty-test.firebaseapp.com",
    databaseURL: "https://dynasty-test.firebaseio.com",
    projectId: "dynasty-test",
    storageBucket: "dynasty-test.appspot.com",
    messagingSenderId: "415594613035",
    appId: "1:415594613035:web:c0a1d274208530618add6e",
    measurementId: "G-YBTGYLZ7RN",
  },
  articleS3BucketName: "article",
  tournamentS3BucketName: "tournament",
  iOSBundelName: "",
  iOSAppstoreURL: "",
  paypal_client_id:
    "AdMFsBVl_2MVoO2KRT9feGxThC2h8nwUo2mANwlp1lqfY5S1RyG-82f99-filmDdpMXOQbnx8pli8WPd&currency=EUR",
};
