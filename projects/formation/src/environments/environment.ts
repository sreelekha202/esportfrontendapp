// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json` .

export const environment = {
  production: false,
  buildConfig: "dev",
  apiEndPoint: "https://5x1bvvraji.execute-api.eu-west-3.amazonaws.com/dev/",
  currentToken: "DUID",
  facebookAPPID: "1417432915264232",
  googleAPPID:
    "346336302247-93msahs9cjrh82dfefgb824sske1g1uo.apps.googleusercontent.com",
  socketEndPoint: "https://chat-formation.dynasty-dev.com",
  cookieDomain: "localhost",
  eCommerceAPIEndPoint:
    "https://tuub60cp6h.execute-api.ap-southeast-1.amazonaws.com/dev/",
  firebase: {
    apiKey: "AIzaSyB1mEgrCbtK1vvE4zR7VcOzXtcWs4ysvLw",
    authDomain: "dynasty-test.firebaseapp.com",
    databaseURL: "https://dynasty-test.firebaseio.com",
    projectId: "dynasty-test",
    storageBucket: "dynasty-test.appspot.com",
    messagingSenderId: "415594613035",
    appId: "1:415594613035:web:c0a1d274208530618add6e",
    measurementId: "G-YBTGYLZ7RN",
  },
  articleS3BucketName: "article",
  tournamentS3BucketName: "tournament",
  iOSBundelName: "",
  iOSAppstoreURL: "",
  paypal_client_id:
    "ASf-mIwkHu2j6hchcTquNOcfFtBbfzxC5qhbjOiXrQ1v-DTAlsUFnge0XHEJrUC_UtDiSBR7UvTK48WE&currency=EUR",
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
