import { Injectable } from "@angular/core";

import { EsportsScriptLoadingService } from "esports";
import { environment } from "../../../environments/environment";

@Injectable({
  providedIn: "root",
})
export class PaypalScriptService {
  private baseUrl: string = "https://www.paypal.com/sdk/js";
  private globalVar: string = "paypal";

  constructor(private scriptLoadingService: EsportsScriptLoadingService) {}

  registerScript(loaded: (payPalApi: any) => void): void {
    this.scriptLoadingService.registerScript(
      this.getPayPalUrl(),
      this.globalVar,
      loaded
    );
  }

  getPayPalUrl(): string {
    return `${this.baseUrl}?client-id=${environment.paypal_client_id}`;
  }
}
