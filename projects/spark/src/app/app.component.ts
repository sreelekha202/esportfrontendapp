import {
  AfterViewInit,
  Component,
  ElementRef,
  Inject,
  OnInit,
  PLATFORM_ID,
} from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { Direction } from '@angular/cdk/bidi';
import { isPlatformBrowser, DOCUMENT } from '@angular/common';
import { Meta, Title } from '@angular/platform-browser';
import {
  EsportsUserService,
  EsportsChatService,
  EsportsPaginationService,
  EsportsConstantsService,
  EsportsAuthServices,
  IUser,
  GlobalUtils,
  EsportsChatSidenavService,
} from 'esports';
import { filter, map, take } from 'rxjs/operators';
import { AppHtmlRoutes } from './app-routing.model';
import { AppRoutesData } from './app-routing.model';
import { CookieService } from 'ngx-cookie-service';
import { environment } from '../environments/environment';

declare var gtag;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit, AfterViewInit {
  AppHtmlRoutes = AppHtmlRoutes;

  isAdminPage: boolean = false;
  isBrowser: boolean;
  isRootPage: boolean = false;
  isUserLoggedIn: boolean = false;

  direction: Direction = 'ltr';
  currentUserId: String = '';

  currentUser: IUser;
  public scroll: ElementRef;
  cookiePolicy;

  constructor(
    @Inject(DOCUMENT) private document: Document,
    @Inject(PLATFORM_ID) private platformId,
    private activatedRoute: ActivatedRoute,
    private authService: EsportsAuthServices,
    private cookie: CookieService,
    private meta: Meta,
    private paginationService: EsportsPaginationService,
    private titleService: Title,
    private userService: EsportsUserService,
    public router: Router,
    private chatService: EsportsChatService,
    private chatSidenavService: EsportsChatSidenavService
  ) {
    this.isBrowser = isPlatformBrowser(platformId);

    if (this.isBrowser && GlobalUtils.isBrowser()) {
      this.checkAdmin();
      this.globalRouterEvents();

      window.addEventListener('storage', (event) => {
        if (event.newValue && event.key === environment.currentToken) {
          window.location.reload();
        }

        if (event.oldValue && event.key === environment.currentToken) {
          window.location.reload();
        }
      });

      const naveEndEvents = router.events.pipe(
        filter((event) => event instanceof NavigationEnd)
      );

      // naveEndEvents.subscribe((event: NavigationEnd) => {
      //   gtag('config', 'G-FB4CHLFXC0', {
      //     page_path: event.urlAfterRedirects,
      //   });
      // });
    }
  }

  ngOnInit(): void {
    if (this.isBrowser) {
      this.cookiePolicy = this.cookie.get('enable_Cookie');
      this.paginationService.pageChanged.subscribe(
        (res) => {
          if (res) {
            if (this.scroll) this.scroll.nativeElement.scrollTop = 0;
          }
        },
        (err) => {}
      );

      this.userService.currentUser.subscribe((data) => {
        if (data) {
          this.currentUser = data;
          this.isUserLoggedIn = !!this.currentUser;
          this.currentUserId = this.currentUser._id;
          this.chatService.initialiseSocket();
        } else {
          //this.userService.getAuthenticatedUser();
        }
      });
    }
  }

  ngAfterViewInit(): void {
    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
        if (this.scroll?.nativeElement?.scrollTop) {
          this.scroll.nativeElement.scrollTop = 0;
        }
        return;
      }

      if (this.scroll?.nativeElement?.scrollTop) {
        this.scroll.nativeElement.scrollTop = 0;
      }
    });
  }

  checkSmTeamView() {
    return window.screen.width > 414 && this.router.url.match('view-team');
  }

  onActivate(): void {}

  checkAdmin(): void {
    this.userService.currentUser
      .pipe(
        filter((data) => !!data),
        take(1)
      )
      .subscribe((data) => {
        if (data) {
          if (data.accountType === EsportsConstantsService.AccountType.Admin) {
            this.isAdminPage = true;
          }
        } else {
          this.isAdminPage = false;
        }
      });
  }

  private globalRouterEvents(): void {
    this.router.events
      .pipe(
        filter((event) => event instanceof NavigationEnd),
        map(() => this.activatedRoute.root.firstChild.snapshot.data)
      )
      .subscribe((data: AppRoutesData) => {
        this.isRootPage = data && data.isRootPage;

        const title = data && data.title;
        const tags = data && data.tags;
        if (title) {
          this.titleService.setTitle(title);
        }
        if (tags) {
          tags.forEach((tag) => {
            this.meta.updateTag(tag);
          });
        }
      });
  }

  clickAccept() {
    if (GlobalUtils.isBrowser()) {
      this.authService.setCookie('true', 'enable_Cookie');
      this.cookiePolicy = this.cookie.get('enable_Cookie');
    }
  }

  onChatSidenavToggle() {
    if (this.chatSidenavService.checkOpen()) {
      this.chatSidenavService.toggle();
      if (this.chatService.getChatStatus()) {
        this.chatService.setChatStatus(false);
      }
    }
  }
}
