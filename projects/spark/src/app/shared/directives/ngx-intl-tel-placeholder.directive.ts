import {
  AfterViewInit,
  Directive,
  ElementRef,
  Input,
  OnDestroy,
} from "@angular/core";

@Directive({
  selector: "ngx-intl-tel-input[ngxIntlTelPlaceholder]",
})
export class NgxIntlTelPlaceholderDirective
  implements AfterViewInit, OnDestroy {
  @Input() ngxIntlTelPlaceholder: string;

  private observer: MutationObserver;

  constructor(private el: ElementRef) {}

  ngAfterViewInit(): void {
    const input = this.el.nativeElement.querySelector('input[type="tel"]');
    if (input && this.ngxIntlTelPlaceholder) {
      input.placeholder = this.ngxIntlTelPlaceholder;

      this.observer = new MutationObserver((mutations) => {
        mutations.forEach((mutation) => {
          if (mutation.type === "attributes") {
            if (input.placeholder !== this.ngxIntlTelPlaceholder) {
              input.placeholder = this.ngxIntlTelPlaceholder;
            }
          }
        });
      });

      this.observer.observe(input, {
        attributes: true, // configure it to listen to attribute changes
      });
    }
  }

  ngOnDestroy(): void {
    this.observer.disconnect();
  }
}
