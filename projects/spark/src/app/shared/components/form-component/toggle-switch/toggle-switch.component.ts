import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-toggle-switch',
  templateUrl: './toggle-switch.component.html',
  styleUrls: ['./toggle-switch.component.scss'],
})
export class ToggleSwitchComponent implements OnInit {
  @Input() customFormControlName: string;
  @Input() customFormGroup: FormGroup;
  @Input() readOnly: boolean;
  @Input() title: string;
  @Input() toolTipMessage: string;
  @Input() select: boolean
  @Output() valueEmit = new EventEmitter<number>();


  constructor() {}

  ngOnInit(): void {}

  emitChangedValue(val) {
    this.valueEmit.emit(val);
  }


}
