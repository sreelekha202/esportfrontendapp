import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';
import {
  EsportsUserService,IMatch,ITournament, EsportsToastService, EsportsLanguageService } from 'esports';
import { TranslateService } from '@ngx-translate/core';
import { environment } from '../../../../environments/environment';
const API = environment.apiEndPoint;

@Component({
  selector: 'app-elimination',
  templateUrl: './elimination.component.html',
  styleUrls: ['./elimination.component.scss'],
})
export class EliminationComponent implements OnInit, OnChanges {
  @Input() enableWebview: boolean;
  @Input() isAdmin: boolean;
  @Input() isFinished: boolean;
  @Input() isLoaded: boolean;
  @Input() isScreenshotRequired: boolean = false;
  @Input() isSeeded: boolean;
  @Input() isShowCountryFlag: boolean = false;
  @Input() participantId: string;
  @Input() scoreReporting: number = 2;
  @Input() stageBracketType: string;
  @Input() stageType: string;
  @Input() structure: any;
  @Input() tournamentId: string;
  @Input() type: string;
  @Input() participantType: string;
  @Input() disableStanding: boolean;
  @Input() tournament: ITournament;
  @Input() hideMockStructure: boolean;
  @Input() allowEditScore: boolean = true;
  @Output() isRefresh = new EventEmitter<boolean>(false);
  API = environment.apiEndPoint;
  isUpdateMatchScore: boolean = false;
  countryList = [];
  match: IMatch;

  constructor(
    private languageService: EsportsLanguageService,
    private toastService: EsportsToastService,
    private translateService: TranslateService,
    private userService: EsportsUserService
  ) {}

  ngOnInit(): void {
    this.isLoaded = false;
    this.languageService.language.subscribe((lang) =>
      this.translateService.use(lang || 'en')
    );

    this.getAllCountries();
  }

  getAllCountries = async () => {
    try {
      const data = await this.userService.getAllCountries().toPromise();
      this.countryList = data?.countries || [];
      this.isLoaded = true;
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
      this.isLoaded = true;
    }
  };

  updatedScoreCard(data) {
    this.isUpdateMatchScore = data.isOpenScoreCard;
    this.match = data.match;
  }

  exitFromScoreCard(data) {
    if (typeof data === 'object') {
      this.isUpdateMatchScore = data.isOpenScoreCard;
      if (data.refresh) {
        this.isRefresh.emit(data.refresh);
      }
    } else {
      this.isRefresh.emit(data);
    }
  }


  ngOnChanges(simpleChanges: SimpleChanges) {
    if (simpleChanges.hasOwnProperty('isSeeded') && !this.hideMockStructure) {
      if (typeof this.isSeeded === 'boolean' && !this.isSeeded) {
        this.toastService.showInfo(
          this.translateService.instant('ELIMINATION.MOCK_STRUCTURE')
        );
      }
    }
  }
}
