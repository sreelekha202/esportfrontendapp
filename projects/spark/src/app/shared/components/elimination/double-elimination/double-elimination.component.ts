import {
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
  TemplateRef,
} from "@angular/core";

import { TranslateService } from "@ngx-translate/core";
import { EsportsLanguageService } from "esports";

@Component({
  selector: "app-double-elimination",
  templateUrl: "./double-elimination.component.html",
  styleUrls: ["./double-elimination.component.scss"],
})
export class DoubleEliminationComponent implements OnInit, OnChanges {
  @Input() matchTemplate: TemplateRef<any>;
  @Input() tournament: any;

  finalBracket: Array<{}> = [];
  losersBracket: Array<{}> = [];
  winnerRound = [];
  winnersBracket: Array<any> = [];

  constructor(
    private languageService: EsportsLanguageService,
    private translateService: TranslateService
  ) {}

  ngOnInit(): void {
    this.languageService.language.subscribe((lang) =>
      this.translateService.use(lang || "en")
    );
  }

  ngOnChanges(changes: SimpleChanges) {
    if (
      changes.hasOwnProperty("tournament") &&
      changes.tournament.currentValue
    ) {
      this.losersBracket = [];
      this.finalBracket = [];
      this.winnersBracket = [];
      this.tournament = this.tournament.round || this.tournament;

      const roundlen = Object.keys(this.tournament).length;
      const mid = Math.ceil(roundlen / 2);
      
      let i = 1;

      while (i < mid) {
        this.winnerRound.push(i);
        this.winnersBracket.push(this.tournament[`${i}`]);
        this.losersBracket.push(this.tournament[`-${i}`]);
        ++i;
      }

      this.finalBracket.push(
        this.tournament[`${mid}`],
        this.tournament[`${mid + 1}`]
      );

      const existRoundLength = this.winnerRound.length;

      this.finalBracket.forEach(
        (el, index) => el && this.winnerRound.push(existRoundLength + index + 1)
      );
    }
  }

  /**
   * Add dummy Block to place real block properly
   */
  blockSettlement(i, key, value) {
    if (i + 1 === 1 && !isNaN(key) && Number(key) === 1) {
      while (Math.ceil(this.winnersBracket[i].length / 2) !== value.length) {
        value.unshift({ bye: true });
      }
      return value;
    } else {
      return value;
    }
  }
}
