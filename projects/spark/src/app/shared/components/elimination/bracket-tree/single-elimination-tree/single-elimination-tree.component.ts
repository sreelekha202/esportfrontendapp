import {
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
  TemplateRef,
} from '@angular/core';

import { NgttRound, NgttTournament } from '../bracket-tree.component';

@Component({
  selector: 'ngtt-single-elimination-tree',
  templateUrl: './single-elimination-tree.component.html',
  styleUrls: ['./single-elimination-tree.component.scss'],
})
export class SingleEliminationTreeComponent implements OnInit, OnChanges {
  @Input() matchTemplate: TemplateRef<any>;
  @Input() tournament: any;

  round: Array<any> = [];
  set = 3;
  public final: NgttRound;
  public winnersBracket: NgttRound[];
  constructor() { }

  ngOnInit(): void {

  }
  ngOnChanges(changes: SimpleChanges) {
    if (
      changes.hasOwnProperty('tournament') &&
      changes.tournament.currentValue
    ) {
      this.tournament = this.tournament.round || this.tournament;
      const t = JSON.parse(JSON.stringify(this.tournament));
      this.round = Object.keys(t).filter((k) => {
        const lastMatch = t[k].reverse()[0];
        this.set = lastMatch?.totalSet;
        return !lastMatch?.isThirdPlaceMatch;
      });
    }
  }


  ngOnChanges5(changes: SimpleChanges) {
    if (
      changes.hasOwnProperty('tournament') &&
      changes.tournament.currentValue
    ) {
      this.winnersBracket = this.tournament.rounds.filter((round) => {
        return round.type === 'Winnerbracket';
      });
    }
    this.final = this.tournament.rounds
      .filter((round) => {
        return round.type === 'Final';
      })
      .shift();
  }



}
