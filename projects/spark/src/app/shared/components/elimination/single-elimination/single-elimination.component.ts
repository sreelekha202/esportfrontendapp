import {
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
  TemplateRef,
} from "@angular/core";

export interface NgttRound {
  type: 'Winnerbracket' | 'Loserbracket' | 'Final';
  matches: any[];
}

@Component({
  selector: "app-single-elimination",
  templateUrl: "./single-elimination.component.html",
  styleUrls: [
    "./single-elimination.component.scss",
    "../elimination.component.scss",
  ],
})
export class SingleEliminationComponent implements OnInit, OnChanges {
  @Input() matchTemplate: TemplateRef<any>;
  @Input() tournament: any;

  round: Array<any> = [];
  set = 3;
  constructor() {}

  ngOnInit(): void {}

  ngOnChanges(changes: SimpleChanges) {
    if (
      changes.hasOwnProperty("tournament") &&
      changes.tournament.currentValue
    ) {
      this.tournament = this.tournament.round || this.tournament;
      const t = JSON.parse(JSON.stringify(this.tournament));

      this.round = Object.keys(t).filter((k) => {
        const lastMatch = t[k].reverse()[0];
        this.set = lastMatch?.totalSet;
        return !lastMatch?.isThirdPlaceMatch;
      });
    }
  }
}
