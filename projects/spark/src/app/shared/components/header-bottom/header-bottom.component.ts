import { Component, OnInit,Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppHtmlRoutes, AppHtmlGetMatchRoutes, AppHtmlOverviewRoutes } from '../../../app-routing.model';
import { Location } from '@angular/common';
import { GlobalUtils } from 'esports';
@Component({
  selector: 'app-header-bottom',
  templateUrl: './header-bottom.component.html',
  styleUrls: ['./header-bottom.component.scss']
})
export class HeaderBottomComponent implements OnInit {
  @Input() gameDetail;
  AppHtmlRoutes = AppHtmlRoutes;
  AppHtmlGetMatchRoutes = AppHtmlGetMatchRoutes;
  AppHtmlOverviewRoutes = AppHtmlOverviewRoutes;
  gameId: any;
  
  constructor(private activeRoute: ActivatedRoute,
    private router: Router,
    private location:Location
  ) { }

 ngOnInit(): void {
    // this.activeRoute.paramMap.subscribe(params => {

    //   if(params.get('_id')){
    //     this.gameId = params.get('_id')

    //   }
    // })
    if (GlobalUtils.isBrowser()) {
      this.gameId = localStorage.getItem('gameId');
    }
  }

  isMatchMakingRoute() {
    return (
      this.router.url === '/get-match/platforms' ||
      this.router.url === '/get-match/platforms-single' ||
      this.router.url === '/get-match/team-game' ||
      this.router.url === '/get-match/game-loading' ||
      this.router.url === '/get-match/casual-matchmaking' ||
      this.router.url === '/get-match/game-lobby' ||
      this.router.url === '/get-match/match-lobby-api' ||
      this.router.url === '/get-match/match-lobby-non-api' ||
      this.router.url === '/get-match/match-detail' ||
      this.router.url === '/get-match/match-completed' ||
      this.router.url === '/get-match/tournament-scoring' ||
      this.router.url === '/get-match/update-score' ||
      this.router.url === '/get-match/scoring-success' ||
      this.router.url === '/get-match/season/select-team'
    );
  }

  isGetReason() {
    const url = this.router.url;
    if (url.search('season') != -1)
      return true
    return false
  }

  isGetMatch() {
    if (this.isGetReason())
      return false
    const url = this.router.url;
    if (url.search('get-match') == -1)
      return false
    return true
  }
  isGetTournament() {
    const url = this.router.url;
    if (url.search('tournament') == -1)
      return false
    return true
  }
  isGetOverView() {
    const url = this.router.url;
    if (url.search('overview') == -1)
      return false
    return true
  }
  isGetLeaderboard() {
    const url = this.router.url;
    if (url.search('leaderboard') == -1)
      return false
    return true
  }
  isOverview() {
    return this.router.url === AppHtmlOverviewRoutes.overview;
  }

}
