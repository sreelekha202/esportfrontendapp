import {
  EsportsUserService,
  EsportsToastService,
  EsportsCommentService,
  EsportsLeaderboardService,
  EsportsChatService,
  EsportsGtmService,
  SuperProperties,
  EventProperties,
  EsportsInfoPopupComponentData,
  EsportsInfoPopupComponentType,
  EsportsInfoPopupComponent,
} from 'esports';
import {
  Component,
  Input,
  OnInit,
  Output,
  EventEmitter,
  OnChanges,
} from '@angular/core';
import { toggleOpacity, VisibilityState } from '../../../animations';

import { Subscription } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { TranslateService } from '@ngx-translate/core';
import { environment } from '../../../../environments/environment';
const API = environment.apiEndPoint;
const TOKEN = environment.currentToken;
@Component({
  selector: 'app-discussion',
  templateUrl: './discussion.component.html',
  styleUrls: ['./discussion.component.scss'],
  animations: [toggleOpacity],
})
export class DiscussionComponent implements OnInit, OnChanges {
  @Input() allowComment: boolean = true;
  @Input() creatorId;
  @Input() objectId: string;
  @Input() text: string;
  @Input() type: string;
  @Input() userId: string;
  @Output() totalComment = new EventEmitter();

  allowDelete: boolean = false;
  VisibilityState = VisibilityState;
  collapseOrExpand: VisibilityState = VisibilityState.Visible;

  comments: Array<any> = [];
  page: number = 1;

  isLoadingNextComments: boolean = false;
  hasNextComment: boolean = true;
  show = false;
  isRemove = false;
  currentUser: any;
  currentUserId: any;
  matchdetails: any;
  typeofchat = 'individual';
  windowposition: String = 'chat_window chat_window_right_drawer';
  tournamentDetails: any;
  show1 = false;
  show2 = false;
  show3 = false;
  idIndex: any;
  showChat: boolean = false;
  contentChat = false;
  isBrowser: boolean;
  userSubscription: Subscription;
  getStatusInterval;
  matchId: string = null;
  constructor(
    private commentService: EsportsCommentService,
    private matDialog: MatDialog,
    private toastService: EsportsToastService,
    private translateService: TranslateService,
    private userService: EsportsUserService,
    private leaderboardService: EsportsLeaderboardService,
    private chatService: EsportsChatService,
    private gtmService: EsportsGtmService,
    public esportsChatService: EsportsChatService,
  ) {}

  ngOnInit(): void {
    this.getCurrentUserDetails();
  }

  isShowPopup(_id, i) {
    const Data: EsportsInfoPopupComponentData = {
      title: this.translateService.instant('HEADER_SPARK.DELETE?'),
      text: this.translateService.instant('HEADER_SPARK.DELETE?'),
      type: EsportsInfoPopupComponentType.info,
      btnText: this.translateService.instant('HEADER_SPARK.CONFIRM'),
      cancelBtnText: this.translateService.instant('HEADER_SPARK.CANCEL'),
    };
    const dialogRef = this.matDialog.open(EsportsInfoPopupComponent, {
      data: Data,
    });
    dialogRef.afterClosed().subscribe((confirmed) => {
      if (confirmed) {
        // this.router.navigateByUrl('play');
        this.deleteCommentOrReply(_id, i);
      }
    });

    this.idIndex = {
      id: _id,
      index: i,
      evnt: true,
    };
    this.show = !this.show;
  }

  showPopup(event) {
    this.show = !this.show;
    // this.isRemove = event.evnt;
    this.deleteCommentOrReply(event.id, event.index);
  }

  getCurrentUserDetails() {
    this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.currentUser = data;
        this.currentUserId = data?._id;
        this.getAllComment();
        this.chatService.chatStatus.subscribe((cstatus) => {
          this.showChat = cstatus;
        });

        if (data?.accountType == 'admin') {
          this.allowDelete = true;
        }
        if (data?._id == this.creatorId) {
          this.allowDelete = true;
        }
      }
    });
  }

  followUser(userVo) {
    if (this.currentUser) {
      if (!userVo.isUserFollowed) {
        this.leaderboardService
          .followUser(API, userVo?.user?._id)
          .subscribe(
            (res: any) => {
              this.checkFollowStatus(userVo);
              this.toastService.showSuccess(res?.message);
              this.userService.refreshCurrentUser(API, TOKEN);
            },
            (err) => {
              //  this.showLoader = false;
            }
          );
      } else if (userVo.isUserFollowed) {
        this.leaderboardService
          .unfollowUser(API, userVo?.user?._id)
          .subscribe(
            (res: any) => {
              this.checkFollowStatus(userVo);
              this.toastService.showSuccess(res?.message);
              this.userService.refreshCurrentUser(API, TOKEN);
            },
            (res: any) => {
              //  this.showLoader = false;
            }
          );
      }
    } else {
      // Navigate to the login page with extras
      //this.router.navigate(['/user/email-login']);
    }
  }

  checkFollowStatus(userVo) {
    this.leaderboardService
      .checkFollowStatus(API, userVo?.user?._id)
      .subscribe(
        (res: any) => {
          for(let i=0;i<this.comments.length;i++){
            if(userVo?.user?._id == this.comments[i]?.user?._id && res?.data[0]?.status == 1)
              this.comments[i].isUserFollowed = true;
            else if(userVo?.user?._id == this.comments[i]?.user?._id && res?.data[0]?.status == 0)
              this.comments[i].isUserFollowed = false;
          }
        },
        (err) => {
          //this.showLoader = false;
        }
      );
  }

  ngOnChanges() {
    if (this.type && this.objectId) {
      this.comments = [];
      this.page = 1;
      this.currentUser && this.currentUser?._id ? this.getAllComment() : '';
    }
  }

  getAllComment = async () => {
    try {
      if (this.isLoadingNextComments) return;
      this.isLoadingNextComments = true;
      const { data, hasNext } = await this.commentService.getAllComment(
        this.type,
        this.objectId,
        this.page
      );

      this.comments = [...this.comments, ...data];
      this.totalComment.emit(this.comments);
      this.page += this.page;
      this.hasNextComment = hasNext;
      this.isLoadingNextComments = false;
    } catch (error) {
      this.isLoadingNextComments = false;
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  getAllReply = async (commentId, index) => {
    try {
      if (this.comments[index].isLoadingNextReplies) return;

      this.comments[index].page = this.comments[index].page || 1;
      this.comments[index].isLoadingNextReplies =
        this.comments[index].isLoadingNextReplies || true;

      const { data, hasNext } = await this.commentService.getAllComment(
        'comment',
        commentId,
        this.comments[index].page
      );

      this.comments[index].replies = [
        ...(this.comments[index].replies || []),
        ...data,
      ];
      this.comments[index].page += this.comments[index].page;
      this.comments[index].hasNextReply = hasNext;

      this.comments[index].isLoadingNextReplies =
        !this.comments[index].isLoadingNextReplies;

      this.comments[index].showReplies = true;
    } catch (error) {
      this.comments[index].isLoadingNextReplies =
        !this.comments[index].isLoadingNextReplies;
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  getCommentResponse = (payload) => {
    this.comments.unshift(payload);
    this.totalComment.emit(this.comments);
  };

  getUpdatedCommentResponse = (payload, i) => {
    this.comments[i] = { ...this.comments[i], ...payload };
    this.comments[i].isEdit = false;
  };

  getReplyResponse = (payload, i) => {
    if (this.comments[i]?.showReplies) {
      if (!this.comments[i]?.replies?.length) {
        this.comments[i].replies = [payload];
      } else {
        this.comments[i].replies.unshift(payload);
      }
    }
    this.comments[i].totalReplies = (this.comments[i].totalReplies || 0) + 1;
    this.comments[i].openReply = false;
  };

  getReplyCancelResponse = (event, i) => {
    this.comments[i].openReply = false;
  };

  getUpdatedReplyResponse = (payload, i, j) => {
    this.comments[i].replies[j] = {
      ...this.comments[i].replies[j],
      ...payload,
    };
    this.comments[i].replies[j].isEdit = false;
  };

  likeOrUnLikeComment = async (
    objectId: string,
    objectType: string,
    type: number,
    i = 0,
    j = 0,
    isReply = false
  ) => {
    try {
      if (!this.userId) {
        this.toastService.showInfo('Please login to like this comment');
        return;
      }

      // if (!this.allowComment) {
      //   this.toastService.showInfo(
      //     'Please Join this tournament to like this comment'
      //   );
      //   return;
      // }

      const payload = {
        objectId,
        objectType,
        type: type == -1 ? 1 : Number(!type),
      };

      const like = await this.commentService.upsertLike(payload);
      if (isReply) {
        this.comments[i].replies[j] = {
          ...this.comments[i].replies[j],
          ...like.data,
        };
      } else {
        this.comments[i] = { ...this.comments[i], ...like.data };
      }
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  pushGTMTags(eventName: string, eventProps = false) {
    let superProperties: SuperProperties = {};
    if (this.currentUser) {
      superProperties = this.gtmService.assignLoggedInUsedData(
        this.currentUser
      );
    }
    let eventProperties: EventProperties = {};
    if (eventProps) {
      eventProperties.fromPage = 'Chats';
    }
    this.gtmService.gtmEventWithSuperProp({
      eventName: eventName,
      superProps: superProperties,
      eventProps: eventProperties,
    });
  }

  public toggleChat(chatwindow) {
    this.pushGTMTags('Chat_Opened');
    this.matchdetails = chatwindow?.user?._id + '-' + this.currentUserId;
    this.showChat = true;
    this.chatService.setWindowPos(this.windowposition);
    this.chatService.setCurrentMatch(this.matchdetails);
    this.chatService.setTypeOfChat(this.typeofchat);
    this.chatService.setChatStatus(true);
    this.chatService.setTextButtonStatus(true);
  }

  deleteCommentOrReply = async (id, i, j = -1) => {
    try {
      // const data: InfoPopupComponentData = {
      //   title: this.translateService.instant('DISCUSSION.DELETE_COMMENT_TITLE'),
      //   text: this.translateService.instant('DISCUSSION.DELETE_COMMENT_TEXT'),
      //   type: InfoPopupComponentType.confirm,
      //   btnText: this.translateService.instant('BUTTON.CONFIRM'),
      // };

      // const confirmed = await this.matDialog
      //   .open(InfoPopupComponent, { data })
      //   .afterClosed()
      //   .toPromise();

      if (true) {
        const deleteComment = await this.commentService.deleteComment(id);
        if (deleteComment.data) {
          if (j > -1) {
            this.comments[i].replies.splice(j, 1);
            this.comments[i].totalReplies = this.comments[i].totalReplies - 1;
          } else {
            this.comments.splice(i, 1);
          }
          this.toastService.showSuccess(deleteComment?.message);
          this.totalComment.emit(this.comments);
        }
      }
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };
}
