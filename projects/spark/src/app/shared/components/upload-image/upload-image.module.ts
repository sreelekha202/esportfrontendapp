import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { UploadImageComponent } from "./upload-image.component";
import { ReactiveFormsModule } from "@angular/forms";
import { I18nModule, PipeModule } from "esports";
import { environment } from "../../../../environments/environment";

@NgModule({
  declarations: [UploadImageComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    I18nModule.forRoot(environment),
    PipeModule, //make sure to remove later we need error pipe onlu
  ],
  exports: [UploadImageComponent],
})
export class UploadImageModule { }
