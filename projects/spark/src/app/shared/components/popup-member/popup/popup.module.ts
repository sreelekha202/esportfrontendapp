import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { PopupComponent} from './popup.component'
import { MatCheckboxModule } from '@angular/material/checkbox'; 
import {TranslateModule} from '@ngx-translate/core';
@NgModule({
  declarations: [PopupComponent],
  imports: [CommonModule, MatCheckboxModule ,TranslateModule],
  exports: [PopupComponent],
})
export class PopupModule {}
