import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EarnPointsComponent } from './earn-points.component';
import { RouterModule } from '@angular/router';
import { I18nModule } from 'esports';
import { environment } from '../../../../environments/environment';

@NgModule({
  declarations: [EarnPointsComponent],
  imports: [CommonModule,  I18nModule.forRoot(environment), RouterModule],
  exports: [EarnPointsComponent],
})
export class EarnPointsModule {}
