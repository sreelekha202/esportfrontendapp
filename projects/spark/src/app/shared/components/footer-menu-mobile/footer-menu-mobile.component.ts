import { Component, OnInit, Input } from '@angular/core';
import { AppHtmlRoutes } from '../../../app-routing.model';
import { GlobalUtils } from 'esports';
@Component({
  selector: 'app-footer-menu-mobile',
  templateUrl: './footer-menu-mobile.component.html',
  styleUrls: ['./footer-menu-mobile.component.scss']
})
export class FooterMenuMobileComponent implements OnInit {
  @Input() gameDetail;
  show= false;
  AppHtmlRoutes = AppHtmlRoutes;
  gameId: any;
  constructor() { }

  ngOnInit(): void {
    if (GlobalUtils.isBrowser()) {
      this.gameId = localStorage.getItem('gameId');
    }
  }

}
