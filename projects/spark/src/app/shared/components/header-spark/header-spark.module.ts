import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderSparkComponent } from './header-spark.component';
import { RouterModule } from '@angular/router';

import { MatIconModule } from '@angular/material/icon';
import { I18nModule } from 'esports';
import { environment } from '../../../../environments/environment';

@NgModule({
  declarations: [HeaderSparkComponent],
  imports: [CommonModule, RouterModule, MatIconModule,  I18nModule.forRoot(environment),],
  exports: [HeaderSparkComponent],
})
export class HeaderSparkModule {}
