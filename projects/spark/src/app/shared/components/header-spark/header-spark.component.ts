import {
  Component,
  OnInit,
  Input,
  OnChanges,
  SimpleChanges,
} from '@angular/core';
import { AppHtmlRoutes } from '../../../app-routing.model';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import {
  EsportsGameService,
  EsportsInfoPopupComponentData,
  EsportsInfoPopupComponentType,
  EsportsInfoPopupComponent,
} from 'esports';
import { MatDialog } from '@angular/material/dialog';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-header-spark',
  templateUrl: './header-spark.component.html',
  styleUrls: ['./header-spark.component.scss'],
  host: { '[join-tournament]': 'isJoinTournament()' },
})
export class HeaderSparkComponent implements OnInit, OnChanges {
  @Input() currentPageLink: string;
  @Input() routerLink: string;
  @Input() title: string;
  @Input() titleCode: string;
  matchmakingDetails: any;
  selectedGame: any;
  checkQuick8 = false;
  check_lobby = false;
  AppHtmlRoutes = AppHtmlRoutes;
  popup = false;
  popupjoin = false;
  isCheck_lobby = false;
  constructor(
    private _location: Location,
    private router: Router,
    private gameService: EsportsGameService,
    private dialog: MatDialog,
    private translateService: TranslateService
  ) {}

  checkTitle: string = 'Create Tournament';
  titleCodes = {
    joinSeason: 'SEASON.JOIN_SEASON',
    joinTournament: 'TOURNAMENT_CARD.JOIN',
  };

  ngOnInit(): void {
    this.matchmakingDetails = this.gameService.createMatchMaking.subscribe(
      (data: any) => {
        if (data) {
          this.selectedGame = data.selectedGame;
        }
      }
    );
    this.checkRouter();
    this.router.events.subscribe((val) => {
      this.checkRouter();
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.titleCode) {
      this.title = this.titleCodes?.[changes?.titleCode?.currentValue];
    }
  }

  checkRouter() {
    if (this.router.url === '/get-match/match-lobby') {
      this.routerLink = '/matchmaking';
      this.check_lobby = true;
    }
    if (this.router.url === '/get-match/game-lobby') {
      this.routerLink = '/matchmaking';
      this.check_lobby = true;
    }
    if (this.router.url === '/get-match/match-lobby-non-api') {
      this.check_lobby = true;
      this.isCheck_lobby = true;
    }
    if (this.router.url === '/get-match/match-completed') {
      this.check_lobby = true;
    }
    if (this.router.url === '/create-tournament/CreateQuick') {
      this.checkQuick8 = true;
    } else {
      this.checkQuick8 = false;
    }
    if (this.router.url === '/get-match/casual-matchmaking') {
      this.check_lobby = true;
    }
  }
  backClicked() {
    this._location.back();
  }
  isMatchmakingRoute() {
    let gameLobbyFlag = this.router.url.includes('/get-match/game-lobby');
    gameLobbyFlag ? (this.check_lobby = true) : '';
    return (
      this.router.url === '/get-match/platforms' ||
      this.router.url === '/get-match/platforms-single' ||
      this.router.url === '/get-match/team-game' ||
      this.router.url === '/get-match/game-loading' ||
      this.router.url === '/get-match/casual-matchmaking' ||
      this.router.url === '/get-match/match-lobby-api' ||
      this.router.url === '/get-match/match-lobby-non-api' ||
      this.router.url === '/get-match/match-lobby' ||
      this.router.url === '/get-match/match-detail' ||
      this.router.url === '/get-match/tournament-scoring' ||
      this.router.url === '/get-match/update-score' ||
      this.router.url === '/get-match/match-completed' ||
      this.router.url === '/get-match/season/select-team' ||
      gameLobbyFlag
    );
  }
  isMatchmakingLobby() {
    return this.router.url === '/get-match/game-lobby';
  }
  isMatchLobby() {
    return (
      this.router.url === '/get-match/game-lobby' ||
      this.router.url === '/get-match/match-lobby-api'
    );
  }
  haveBtnBack() {
    return (
      this.router.url === '/get-match/platforms' ||
      this.router.url === '/get-match/platforms-single' ||
      this.router.url === '/get-match/game-loading' ||
      this.router.url === '/get-match/casual-matchmaking' ||
      this.router.url === '/get-match/game-lobby' ||
      this.router.url === '/get-match/match-lobby-api' ||
      this.router.url === '/get-match/match-lobby-non-api' ||
      this.router.url === '/get-match/match-completed' ||
      this.router.url === '/get-match/season/select-team'
    );
  }
  noBtnClose() {
    return this.router.url === '/get-match/scoring-success';
  }
  havePopupConfirmExit() {
    return (
      this.router.url === '/get-match/game-loading' ||
      this.router.url === '/get-match/casual-matchmaking' ||
      this.router.url === '/get-match/tournament-scoring' ||
      this.router.url === '/get-match/select-platforms' ||
      this.router.url === '/get-match/platforms-single' ||
      this.router.url === '/get-match/game-lobby' ||
      this.router.url === '/get-match/match-lobby-api' ||
      this.router.url === '/get-match/match-lobby-non-api' ||
      this.router.url === '/get-match/match-completed' ||
      this.router.url === '/get-match/match-detail' ||
      this.router.url === '/get-match/update-score' ||
      this.router.url === '/get-match/season/select-team'
    );
  }
  isJoinTournament() {
    return (
      this.router.url === '/join-tournament/checkout' ||
      this.router.url === '/join-tournament/payment-successful' ||
      this.router.url === '/join-tournament/teammembers' ||
      this.router.url === '/join-tournament/teamdetails' ||
      this.router.url === '/join-tournament/registration' ||
      this.router.url === '/join-tournament/soloregistration' ||
      this.router.url === '/join-tournament'
    );
  }
  isTitleJoin() {
    return this.router.url === '/join';
  }
  isEditVideo() {
    return this.router.url.includes('/profile/edit-video');
  }
  isCreateVideo() {
    return this.router.url === '/profile/add-video';
  }

  isMatchDetails() {
    return this.router.url.includes('/get-match/match-detail');
  }

  closePopup(type: boolean) {
    if(this.router.url.includes('/get-match/game-lobby')){
      this.router.navigate(['/play']);
    }
    else{
      const blockData: EsportsInfoPopupComponentData = {
        title: type
          ? this.translateService.instant('HEADER_SPARK.EXIT')
          : this.translateService.instant('HEADER_SPARK.EXIT?'),
        text: this.translateService.instant('HEADER_SPARK.EXIT_SUBTEXT'),
        type: EsportsInfoPopupComponentType.info,
        btnText: this.translateService.instant('HEADER_SPARK.CONFIRM'),
        cancelBtnText: this.translateService.instant('HEADER_SPARK.CANCEL'),
        isCancelledRequired:true
      };

      const dialogRef = this.dialog.open(EsportsInfoPopupComponent, {
        data: blockData,
      });

      dialogRef.afterClosed().subscribe((confirmed) => {
        if (confirmed) {
          this.router.navigate(['/play']);
        }
      });
    }
  }
}
