import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderSparkComponent } from './header-spark.component';

describe('PaidiaHeaderComponent', () => {
  let component: HeaderSparkComponent;
  let fixture: ComponentFixture<HeaderSparkComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HeaderSparkComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderSparkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
