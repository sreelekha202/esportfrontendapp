import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FooterSparkComponent } from './footer-spark.component';

describe('FooterSparkComponent', () => {
  let component: FooterSparkComponent;
  let fixture: ComponentFixture<FooterSparkComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FooterSparkComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FooterSparkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
