import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FooterSparkComponent } from './footer-spark.component';
import { RouterModule } from '@angular/router';

import { MatIconModule } from '@angular/material/icon';
import { I18nModule } from 'esports';
import { environment } from '../../../../environments/environment';

@NgModule({
  declarations: [FooterSparkComponent],
  imports: [CommonModule, RouterModule, MatIconModule,  I18nModule.forRoot(environment),],
  exports: [FooterSparkComponent],
})
export class FooterSparkModule {}
