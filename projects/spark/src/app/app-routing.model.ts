import { MetaDefinition } from '@angular/platform-browser';

export interface AppRoutesData {
  isRootPage?: boolean;
  tags?: MetaDefinition[];
  title?: string;
}

export enum AppHtmlRoutes {
  aboutUs = '/about-us',
  admin = '/admin',
  article = '/content',
  articleCreate = '/content/create',
  articleCreateVideoLibrary = '/content/create-video-library',
  articleEdit = '/content/edit',
  articleMore = '/content/more',
  articleNews = '/content/news',
  bracket = '/bracket',
  bracketCreate = '/bracket/create',
  content = '/content',
  createTeam = '/create-team',
  createTournament = '/create-tournament',
  joinTournament = '/join-tournament',
  dec = '/dec',
  games = '/games',
  home = '/home',
  leaderBoard = '/leaderboard',
  leaderboard_pop = '/leaderboard-popup',
  login = '/login',
  loginEmail = '/login-email',
  manageTeam = '/manage-team/',
  myteam = '/my-teams',
  matchmaking = '/matchmaking',
  notFound = '/404',
  play = '/play',
  profile = '/profile',
  privacyPolicy = '/privacy-policy',
  register = '/register',
  registerEmail = '/register-email',
  search = '/search',
  searchArticle = '/search/article',
  searchShop = '/search/shop',
  searchTournament = '/search/tournament',
  searchVideo = '/search/video',
  specialOffer = '/special-offer',
  terms = '/terms-of-use',
  topUp = '/top-up',
  tournament = '/tournament',
  tournamentCreate = '/tournament/create',
  tournamentEdit = '/tournament/edit',
  tournamentInfo = '/tournament/info',
  tournamentManage = '/tournament/manage',
  tournamentView = '/tournament',
  userPageType = '/user',
  userPhoneLogin = '/user/phone-login',
  userRegistration = '/user/registration',
  videoCreate = '/videos/create-video-library',
  videoEdit = '/videos/edit-video-library',
  videos = '/videos',
  videosView = '/videos/view',
  view = '/view',
  shop = '/shop',
  matchstats = '/get-match/match-detail'
}

export enum AppHtmlRoutesLoginType {
  createPass = 'createPass',
  emailLogin = 'email-login',
  forgotPass = 'forgotPass',
  phoneLogin = 'phone-login',
  registration = 'registration',
  userRegistration = '/user/registration',
}

export enum AppHtmlProfileRoutes {
  accountSetting = '/profile/account-settings',
  bookmarks = '/profile/bookmarks',
  connections = '/404',
  content = '/profile/content',
  dashboard = '/profile/dashboard',
  inbox = '/profile/inbox',
  matches = '/profile/matches',
  myStats = '/profile/my-stats',
  viewStats = '/profile/view-stats',
  myTeams = '/profile/my-teams',
  myTeamsView = '/profile/my-teams/',
  referrals = '/profile/referrals/',
  tournamentsCreated = '/profile/tournaments-created',
  tournamentsJoined = '/profile/tournaments-joined',
  transactions = '/profile/transactions',
}

export enum AppHtmlAccountSettingRoutes {
  accountSetting = '/account_settings/gameAccounts',
}

export enum AppHtmlManageTeams {
  teamMembers = '/manage-team/team-members',
  invitesSent = '/manage-team/invites-sent',
  editTeam = '/manage-team/edit-team',
  viewTeam = '/view-team',
  addMembers = '/manage-team/add-members',
  TeamMatchmaking = '/team/Matchmaking',
}

export enum AppHtmlAdminRoutes {
  accessManagement = '/admin/access-management',
  contentManagement = '/admin/content-management',
  dashboard = '/admin/dashboard',
  editTeam = '/edit-team',
  esportsManagement = '/admin/esports-management',
  siteConfiguration = '/admin/site-configuration',
  teamManagement = '/admin/team-management',
  teamManagementEdit = '/admin/team-management/edit',
  userManagement = '/admin/user-management',
  userManagementView = '/admin/user-management/view',
  userNotifications = '/admin/user-notifications',
  seasonManagement = '/admin/season-management',
  seasonManagementCreate = '/admin/season-management/create',
}

export enum AppHtmlRoutesArticleMoreType {
  trendingPost = 'trending-post',
  allPost = 'all-post',
}

export enum AppHtmlTeamRegRoutes {
  newTeam = '/team-registration',
  teammates = '/team-registration/teammates',
  registration = '/team-registration/registration',
}

export enum AppHtmlGetMatchRoutes {
  start = '/get-match',
  platforms = '/get-match/platforms',
  teams = '/get-match/teams',
  members = '/get-match/members',
  gameLobby = '/get-match/game-lobby',
  season = '/get-match/season',
  matchDetails = '/get-match/match-detail'
}

export enum AppHtmlCreateTournamentRoutes {
  date = '/create-tournament/date',
  details = '/create-tournament/details',
  game = '/create-tournament/game',
  name = '/create-tournament/name',
  platform = '/create-tournament/platform',
}

export enum AppHtmlJoinTournamentRoutes {
  // date = '/create-tournament/date',
  // details = '/create-tournament/details',
  // entryFee = '/create-tournament/entry-fee',
  // format = '/create-tournament/format',
  // game = '/create-tournament/game',
  // name = '/create-tournament/name',
  // platform = '/create-tournament/platform',
  // prizes = '/create-tournament/prizes',
  registration = '/join-tournament/registration',
  teammembers = '/join-tournament/teammembers',
  teamdetails = '/join-tournament/teamdetails',
  participantsrequest = '/join-tournament/participantsrequest',
}

export enum AppHtmlOverviewRoutes {
  overview = '/overview',
}
export enum AppHtmlWalletRoutes {
  wallet = '/wallet',
}

export enum AppHtmlManageTournamentRoutes {
  participantsRequests = '/manage-tournament/participants-requests',
}
