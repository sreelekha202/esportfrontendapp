import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { MobilePrivacyPolicyComponent } from "./mobile-privacy-policy.component";

describe("MobilePrivacyPolicyComponent", () => {
  let component: MobilePrivacyPolicyComponent;
  let fixture: ComponentFixture<MobilePrivacyPolicyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MobilePrivacyPolicyComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MobilePrivacyPolicyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
