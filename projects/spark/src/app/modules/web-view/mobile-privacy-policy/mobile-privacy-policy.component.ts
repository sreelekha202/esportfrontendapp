import { Component, OnInit } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { EsportsLanguageService } from "esports";

@Component({
  selector: "app-mobile-privacy-policy",
  templateUrl: "./mobile-privacy-policy.component.html",
  styleUrls: ["./mobile-privacy-policy.component.scss"],
})
export class MobilePrivacyPolicyComponent implements OnInit {
  arebicText: boolean = false;

  public currentLang: string;

  currLanguage = "english";

  constructor(
    public language: EsportsLanguageService,
    public translate: TranslateService
  ) {}

  ngOnInit(): void {
    this.language.language.subscribe((lang) => {
      if (lang) {
        this.arebicText = lang === "ms" ? true : false;
      }
    });
  }
}
