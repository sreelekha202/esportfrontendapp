import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CountdownModule } from "ngx-countdown";
import { PlayRoutingModule } from './play-routing.module';
import { PlayComponent } from './play.component';
import { EsportsLoaderModule } from 'esports';
import { SharedModule } from '../../shared/modules/shared.module';
@NgModule({
  declarations: [PlayComponent],
  imports: [
    CommonModule,
    SharedModule,
    PlayRoutingModule,
    CountdownModule,
    EsportsLoaderModule.setColor('#00a851')],
})
export class PlayModule { }
