import { Component, OnInit } from '@angular/core';
import { EsportsGameService, EsportsTournamentService } from 'esports';

import { environment } from '../../../environments/environment';
import { Router } from '@angular/router';
import { AppHtmlOverviewRoutes, AppHtmlRoutes } from '../../app-routing.model';

const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;

@Component({
  selector: 'app-play',
  templateUrl: './play.component.html',
  styleUrls: ['./play.component.scss'],
})
export class PlayComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes
  AppHtmlOverviewRoutes = AppHtmlOverviewRoutes
  constructor(public gameService: EsportsGameService,
    private router: Router,
    private tournamentService: EsportsTournamentService,) {}
  gameList: any;
  games = [];
  onGoingTournaments = [];
  showLoader: boolean = true;
  selectedGame: any;
  gamesShort = [];
  ngOnInit(): void {
    this.getGamesList();
  }

  createMatchMaking: any;
  matchmakingDetails: any;

  getGamesList() {
    this.gameService.getGames(API).subscribe(
      (res) => {
        this.games = res?.data.splice(0, 4);
        this.gameList = res.data;
        this.showLoader = false;
      },
      (err) => {}
    );
  }


  // getGames() {
  //   this.showLoader = true;
  //   let a = [];
  //   let b = [];
  //   this.gameService.getGames(API).subscribe(
  //     (res) => {
  //       this.games = res?.data.splice(0, 4);
  //       this.showLoader = false;
  //       for (let i = 0; i < this.games.length; i++) {
  //         if (this.games[i]?.name == 'Valorant') {
  //           a[0] = this.games[i];
  //         }

  //         if (this.games[i]?.name == 'League of Legends') {
  //           a[1] = this.games[i];
  //         }

  //         if (this.games[i]?.name == 'DOTA 2') {
  //           a[2] = this.games[i];
  //         }

  //         if (this.games[i]?.name == 'CS:GO') {
  //           a[3] = this.games[i];
  //         }

         

  //         if (
  //           this.games[i]?.name != 'Valorant' &&
  //           this.games[i]?.name != 'League of Legends' &&
  //           this.games[i]?.name != 'Halo MCC' &&
  //           this.games[i]?.name != 'World of Warcraft'
  //         ) {
  //           b[i] = this.games[i];
  //         }
  //       }
  //       this.gamesShort = a;
        //  var newArray = b.filter(value => Object.keys(value).length !== 0)
        //  this.gamesShort = [ ...this.gamesShort, ...newArray]
  //     },
  //     (err) => {
  //       this.showLoader = false;
  //     }
  //   );
  // }



  onselectedGame(gameId){
    localStorage.setItem('gameId', gameId)
  }

   gotoTournament() {
    this.router.navigate(['/overview']);
   }

  onGameSelect(selectedGame) {
    if (selectedGame) {
      this.gameService.createMatchMakingSubject.next({
        ...this.createMatchMaking,
        selectedGame: selectedGame,
      });
      this.router.navigateByUrl('/overview');
    }
  }
}
