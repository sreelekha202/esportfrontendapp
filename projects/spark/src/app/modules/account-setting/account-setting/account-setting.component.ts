import { Component, OnInit, OnDestroy } from '@angular/core';
import { AppHtmlAccountSettingRoutes } from '../../../app-routing.model';
import { MatIconRegistry } from '@angular/material/icon';
import { IUser, EsportsUserService } from 'esports';
import { Subscription } from 'rxjs';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';
export class MatMenuListItem {
  menuLinkText: string;
  menuIcon: string;
  isDisabled: boolean;
}
@AutoUnsubscribe()
@Component({
  selector: 'app-account-setting',
  templateUrl: './account-setting.component.html',
  styleUrls: ['./account-setting.component.scss'],
})
export class AccountSettingComponent implements OnInit,OnDestroy {

  AppHtmlAccountSettingRoutes = AppHtmlAccountSettingRoutes;
  userSubscription: Subscription;
  currentUser: IUser;

  constructor(iconRegistry: MatIconRegistry,
    private userService: EsportsUserService) {
    iconRegistry.addSvgIcon(
      'people',
      '../assets/icons/accounts-settings/blocked.svg'
    );
  }

  ngOnInit(): void {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.currentUser = data;
      }
    });
    this.getmenu();
  }
  ngOnDestroy() {}
  menuListItems: MatMenuListItem[];
  isFooter: true;

  getmenu() {
    this.menuListItems = [
      {
        menuLinkText: 'Settings',
        menuIcon: 'settings',
        isDisabled: false,
      },
      {
        menuLinkText: 'AboutUs',
        menuIcon: 'people',
        isDisabled: false,
      },
      {
        menuLinkText: 'Help',
        menuIcon: 'help',
        isDisabled: false,
      },
      {
        menuLinkText: 'Contact',
        menuIcon: 'contact',
        isDisabled: true,
      },
    ];
  }
}
