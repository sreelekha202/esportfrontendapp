import { EsportsUserService, EsportsToastService,EsportsAuthServices } from 'esports';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Subscription } from 'rxjs';
import { FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { environment } from '../../../../../environments/environment';
import { TranslateService } from '@ngx-translate/core';

const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
})
export class ProfileComponent implements OnInit {
  @Output() dateChange: EventEmitter<MatDatepickerInputEvent<any>>;
  selectedOption: string;
  showLoader: boolean;
  currentUser: any;
  userSubscription: Subscription;
  addForm: FormGroup;
  editImageUrl: any = null;
  selectedFile: any;
  isShown: boolean = false;
  url: any;
  editprofile = false;
  normal = true;
  cityList: any = [];
  stateList: any = [];
  countryList: any = [];
  base64textString: any;
  isExist: any;
  tempImg: any;
  accounts = [];
  gameArray: FormArray;
  selectedGames = [];
  socialList = [
    {
      icon: 'assets/icons/socials/mdi_twitter.svg',
      media: 'Twitter',
    },
    {
      icon: 'assets/icons/socials/mdi_fb.svg',
      media: 'Facebook',
    },
    {
      icon: 'assets/icons/twitch.svg',
      media: 'Twitch',
    },
    {
      icon: 'assets/icons/youtube.svg',
      media: 'Youtube',
    },
    {
      icon: 'assets/icons/discord.svg',
      media: 'Discord',
    },
  ];
  socialAccounts: any = [];

  constructor(
    private userService: EsportsUserService,
    private fb: FormBuilder,
    private toastService: EsportsToastService,
    private formBuilder: FormBuilder,
    private translateService: TranslateService,
    private authServices: EsportsAuthServices,
  ) {
    this.addForm = this.fb.group({
      fullName: [null, Validators.compose([Validators.required])],
      country: [null, Validators.compose([])],
      username: [
        null,
        Validators.compose([
          Validators.required,
          Validators.minLength(3),
          Validators.pattern(/^[a-zA-Z0-9_]+$/),
        ]),
      ],
      // gender: [null, Validators.compose([Validators.required])],
      dob: [null, Validators.compose([Validators.required])],
      state: [null, Validators.compose([])],
      city: [null, Validators.compose([])],
      // postal_code: ['123456', Validators.compose([Validators.required])],
      shortBio: [null],
      profilePicture: [''],
      game: [''],
      socialAccounts: new FormArray([]),
    });

    this.gameArray = this.addForm.get('socialAccounts') as FormArray;
  }

  ngOnInit(): void {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.currentUser = data;
        this.url = data.profilePicture;
        this.userService.getAllCountries().subscribe((res) => {
          this.countryList = [ {
            "id": 157,
            "sortname": "NZ",
            "name": "New Zealand",
            "phoneCode": 64
          }];
          this.setUserData();
          this.setIcon();
        });
      }
    });
  }

  setUserData() {
      this.addForm.patchValue({
        fullName: this.currentUser?.fullName,
        username: this.currentUser?.username,
        gender: this.currentUser?.gender,
        //'pronouns': this.currentUser['pronouns'],
        dob: this.currentUser?.dob,
        country: this.currentUser?.country,
        state: this.currentUser?.state,
        city: this.currentUser?.city,
        profilePicture: this.currentUser?.profilePicture,
        shortBio: this.currentUser?.shortBio,
        socialAccounts: this.currentUser?.socialAccounts,
      });
    this.onCountryChange(true);
    this.addForm.patchValue({
      state: this.currentUser?.state,
      city: this.currentUser?.city,
    });
    
  }
  setUserData2() {
    let tempdata = [];
    this.socialAccounts.map((o) => {
      this.selectedGames.push(o.media);
      tempdata.push(o.media);

      this.gameArray.push(
        this.fb.group({
          profileUrlLink: [
            o.profileUrlLink,
            Validators.compose([Validators.required]),
          ],
          media: [o.media],
          _id: [o._id],
          icon: [o.icon],
        })
      );
    });
    this.addForm.patchValue({
      game: tempdata,
    });
  }

  deleteObjGameArray(item, index) {
    let tempdata = this.addForm.value.game;
    tempdata.map((obj, i) => {
      if (obj == item.value.media) {
        tempdata.splice(i, 1);
      }
    });
    this.gameArray.removeAt(index);
    const i = this.selectedGames.indexOf(index);
    if (index > -1) {
      this.selectedGames.splice(index, 1);
    }
    this.addForm.patchValue({
      game: tempdata,
    });
  }

  getCountryWiseState(country_id: any, isEdit = false) {
    let allState: any = [];
    this.stateList = [];
    this.userService.getStates().subscribe((res) => {
      allState = res.states;

      allState.filter((obj) => {
        if (obj['country_id'] == country_id) {
          this.stateList.push(obj);
        }
      });
      isEdit ? this.onStateChange() : '';
    });
  }

  onCountryChange = (isEdit = false) => {
    this.countryList.map((obj, i) => {
      if (obj.name == this.addForm.value.country) {
        this.getCountryWiseState(obj.id, isEdit);
      }
    });
  };

  getStateWiseCities(state_id: any) {
    let allCity: any = [];
    this.cityList = [];
    this.userService.getCity().subscribe((res) => {
      allCity = res.cities;
      allCity.filter((obj) => {
        if (obj['state_id'] == state_id) {
          this.cityList.push(obj);
        }
      });
    });
  }

  onStateChange = () => {
    this.stateList.map((obj, i) => {
      if (obj.name == this.addForm.value.state) {
        this.getStateWiseCities(obj.id);
      }
    });
  };

  openprofile() {
    this.normal = false;
    this.editprofile = true;
  }

  close() {
    this.normal = true;
    this.editprofile = false;
  }

  setIcon() {
    this.socialAccounts = this.currentUser.socialAccounts;
    this.socialList.map((icon) => {
      this.currentUser.socialAccounts.map((obj, i) => {
        if (icon.media == obj.media) {
          this.socialAccounts[i]['icon'] = icon.icon;
        }
      });
    });
    this.setUserData2();
  }

  onChangeGame() {
    if (this.addForm.value.game.length < this.selectedGames.length) {
      let difference = this.selectedGames.filter(
        (x) => !this.addForm.value.game.includes(x)
      );
      this.gameArray.value.map((obj, index) => {
        if (obj.media == difference[0]) {
          this.gameArray.removeAt(index);
          const i = this.selectedGames.indexOf(index);
          if (index > -1) {
            this.selectedGames.splice(index, 1);
          }
        }
      });
    }

    if (true) {
      this.addForm.value.game.map((value) => {
        this.socialList.map((o: any) => {
          if (o.media == value) {
            if (!this.selectedGames.includes(value)) {
              this.selectedGames.push(value);
              this.gameArray.push(
                this.fb.group({
                  profileUrlLink: ['', Validators.compose([])],
                  media: [o.media],
                  _id: [o._id],
                  icon: [o.icon],
                })
              );
            }
          }
        });
      });
    }
  }

  addPlatform() {
    this.onChangeGame();
  }

  
  onSubmit() {
    if (this.addForm.valid) {
      this.showLoader = true;
      this.userService.updateProfile(API, this.addForm.value).subscribe(
        (res) => {
          this.showLoader = false;
          this.setUserData();
          this.setUserData2();
          this.toastService.showSuccess(res.message);
          while (this.gameArray.length !== 0) {
            this.gameArray.removeAt(0);
          }
          this.userService.refreshCurrentUser(API, TOKEN);
        },
        (err) => {
          this.showLoader = false;
          this.toastService.showError(err.message);
        }
      );
    }
  }

  onClose(): void {}

  fileChangeEvent(fileInput: any) {
    const reg = /(.*?)\.(jpg|jpeg|png|gif|giff)$/;
    if (!fileInput.target.files[0].name.match(reg)) {
      this.toastService.showError(
        this.translateService.instant('ADMIN.SELECT')
      );
      this.removeFile();
      return false;
    } else {
      this.removeFile();
      this.selectedFile = fileInput.target.files[0];
      var reader = new FileReader();
      reader.onload = this.handleReaderLoaded.bind(this);
      reader.readAsBinaryString(this.selectedFile);
    }
  }

  handleReaderLoaded(readerEvt) {
    var binaryString = readerEvt.target.result;
    this.base64textString = 'data:image/png;base64,' + btoa(binaryString);
    this.setImage(this.base64textString);
  }

  setImage(data) {
    this.url = data;
    this.addForm.get('profilePicture').setValue(this.url);
  }

  removeFile() {
    this.selectedFile = null;
  }
  onresetForm() {
    this.addForm.reset();
  }

  isUniqueName = async (name) => {
        this.authServices.searchUsername(name).subscribe((res) => {
      this.isExist = res.data?.isExist;
      if (this.isExist) {
        this.addForm.controls['username'].setErrors({ incorrect: true });
        this.toastService.showError("Username already exist");
      } else {
        this.addForm.controls['username'].setErrors(null);
        this.addForm.controls['username'].setValidators([
          Validators.required,
          Validators.minLength(3),
          Validators.pattern(/^[a-zA-Z0-9_]+$/),
        ]);
        this.addForm.controls['username'].updateValueAndValidity();
      }
    });
  };

  ngOnDestroy(): void {
    if (this.userSubscription) this.userSubscription.unsubscribe();
  }
}
