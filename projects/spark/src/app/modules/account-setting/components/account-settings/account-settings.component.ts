import { Component, OnInit } from '@angular/core';
import { EsportsUserService,IUser } from 'esports';
import { environment } from '../../../../../environments/environment';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-account-settings',
  templateUrl: './account-settings.component.html',
  styleUrls: ['./account-settings.component.scss']
})
export class AccountSettingsComponent implements OnInit {
  emailChangeLink = environment.emailChangeLink;
  passwordChangeLink = environment.passwordChangeLink;
  userSubscription:Subscription;
  currentUser: IUser;
  constructor(private userService: EsportsUserService) { }

  ngOnInit(): void {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.currentUser = data;
      }
    });
  }

}
