import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import {
  EsportsUserService,
  EsportsInfoPopupComponent,
  EsportsInfoPopupComponentData,
  EsportsInfoPopupComponentType,
  EsportsChatService,
  IUser,
  EsportsToastService,
} from 'esports';
import { environment } from '../../../../../environments/environment';

const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;

import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-blocked-list',
  templateUrl: './blocked-list.component.html',
  styleUrls: ['./blocked-list.component.scss'],
})
export class BlockedListComponent implements OnInit {
  userSubscription: Subscription;
  currentUser: IUser;
  blockedUsers: any = [];
  blockedUserCount: any;
  constructor(
    public dialog: MatDialog,
    private userService: EsportsUserService,
    private translateService: TranslateService,
    private chatService: EsportsChatService,
    public eSportsToastService: EsportsToastService
  ) {}

  ngOnInit(): void {  
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.currentUser = data;
      }
    });
    this.getAllBlockedUser();
  }

  show = false;

  unblock_Popup( id ) {
    const blockData: EsportsInfoPopupComponentData = {
      title: this.translateService.instant('ACCOUNT_SETTINGS.BLOCKED.POPUP_TITLE'),
      text: this.translateService.instant('ACCOUNT_SETTINGS.BLOCKED.POPUP_QSTN'),
      type: EsportsInfoPopupComponentType.info,
      btnText: this.translateService.instant('BUTTON.CONFIRM'),
      cancelBtnText: this.translateService.instant('BUTTON.CANCEL'),
    };

    const dialogRef = this.dialog.open(EsportsInfoPopupComponent, {
      data: blockData,
    });

    const payload = {
          userid: this.currentUser._id,
          blockedUserId: id,
        };

    dialogRef.afterClosed().subscribe((confirmed) => {
      if (confirmed) {
        this.chatService.unblockUser(payload).subscribe(
          (res: any) => {
            if (res && res?.unBlockedUser) {
              this.getAllBlockedUser();
            }
          },
          (error) => {
            this.eSportsToastService.showError(
              error?.error?.message || error?.message
            );
          }
        );
      }
    });
  }

  getAllBlockedUser = async () => {
    
    await this.chatService.getBlockedUsers(this.currentUser?._id).subscribe(
      (res: any) => {
        this.blockedUserCount = res?.blockedUserCount
        this.blockedUsers = res?.blockedUserList;
      },
      (error) => {
        this.eSportsToastService.showError(
          error?.error?.message || error?.message
        );
      }
    );
  };
}
