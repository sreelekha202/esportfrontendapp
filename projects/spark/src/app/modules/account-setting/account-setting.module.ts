import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AccountSettingRoutingModule } from './account-setting-routing.module';
import { AccountSettingComponent } from './account-setting/account-setting.component';

import { RouterBackModule } from './../../shared/directives/router-back.module';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ClipboardModule } from '@angular/cdk/clipboard';
import { InlineSVGModule } from 'ng-inline-svg';
import { LazyLoadImageModule } from 'ng-lazyload-image';

import { I18nModule, EsportsLoaderModule,  PipeModule } from 'esports';
import { MaterialModule } from '../../shared/modules/material.module';
import { CardItemComponent } from './components/card-item/card-item.component';
import { FooterSparkModule } from '../../shared/components/footer-spark/footer-spark.module';


import { environment } from '../../../environments/environment';
import { ProfileComponent } from './components/profile/profile.component';
import { GameAccountsComponent } from './components/game-accounts/game-accounts.component';
import { BlockedListComponent } from './components/blocked-list/blocked-list.component';
import { AccountPreferenceComponent } from './components/account-preference/account-preference.component';
import { AccountsComponent } from './components/accounts/accounts.component';
import { AccountSettingsComponent } from './components/account-settings/account-settings.component';

@NgModule({
  declarations: [
    AccountSettingComponent,
    ProfileComponent,
    GameAccountsComponent,
    BlockedListComponent,
    AccountPreferenceComponent,
    AccountsComponent,
    CardItemComponent,
    AccountSettingsComponent,
  ],

  imports: [
    AccountSettingRoutingModule,
    ClipboardModule,
    CommonModule,
    FormsModule,
    I18nModule.forRoot(environment),
    InlineSVGModule,
    LazyLoadImageModule,
    MaterialModule,
    ReactiveFormsModule,
    RouterBackModule,
    PipeModule,
    FooterSparkModule,
    EsportsLoaderModule.setColor('#00a851'),
  ],
})
export class AccountSettingModule {}
