import { Component, Input,Output, OnInit, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-social-accounts',
  templateUrl: './social-accounts.component.html',
  styleUrls: ['./social-accounts.component.scss'],
})
export class SocialAccountsComponent implements OnInit {
  @Input() listData;
  @Output() onToggleChangeSpecialText:any = new EventEmitter();
  accounts = [];

  constructor() {}

  t = {
    icon: '../../../../../../assets/socialNetwork/twitter.png',
    label: 'Twitter',
  };
  f = {
    icon: '../../../../../../assets/socialNetwork/facebook.png',
    label: 'Facebook',
  };
  Twitch = {
    icon: '../../../../../../assets/socialNetwork/twitch.png',
    label: 'Twitch',
  };

  ngOnInit(): void {
    // MOCK DATA
    this.accounts = this.listData;
  }

  addfunction5() {
    if (this.accounts.length == 0) {
      this.addfunction();
    } else if (this.accounts.length == 1) {
      this.addfunction1();
    } else if (this.accounts.length == 2) {
      this.addfunction2();
    }
  }

  addfunction() {
    if (this.accounts.length == 0) {
      this.accounts.push(this.t);
    }
  }
  addfunction1() {
    if (this.accounts.length == 1) {
      this.accounts.push(this.f);
    }
  }
  addfunction2() {
    if (this.accounts.length == 2) {
      this.accounts.push(this.Twitch);
    }
  }

  emitBackText(evt){
    this.onToggleChangeSpecialText.emit(evt);
  }
}
