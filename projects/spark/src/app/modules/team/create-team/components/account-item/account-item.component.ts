import { Component, OnInit, Input, Output,EventEmitter } from '@angular/core';

interface AccountItem {
  icon: string;
  label: string;
  placeHolder:string;
}

@Component({
  selector: 'app-account-item',
  templateUrl: './account-item.component.html',
  styleUrls: ['./account-item.component.scss'],
})
export class AccountItemComponent implements OnInit {
  @Input() data: AccountItem;
  @Output() onInputChange = new EventEmitter();
  value = '';
  socialObject:any = {};

  constructor() {}

  ngOnInit(): void {}

  savevalue(data:any,d:any){
    /*if(d.label == 'Twitter'){
      sessionStorage.setItem("link",data.target.value)
    }
    if(d.label == 'Facebook'){
      sessionStorage.setItem("Fblink",data.target.value)
    }
    if(d.label == 'Twitch'){
      sessionStorage.setItem("twlink",data.target.value)
    }*/
    this.socialObject[d.label] = data.target.value;
    this.socialObject.labelText = d.label;
    this.socialObject.deleted = false;
    this.onInputChange.emit(this.socialObject);
  }
  
  clearSocial(data,d){
    this.socialObject[d.label] = data.target.value;
    this.socialObject.labelText = d.label;
    this.socialObject.deleted = true;
    this.onInputChange.emit(this.socialObject);
  }
}
