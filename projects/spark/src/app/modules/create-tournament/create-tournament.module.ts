import { RouterBackModule } from './../../shared/directives/router-back.module';
import { LoadingModule } from './../../core/loading/loading.module';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
//import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { MatExpansionModule } from '@angular/material/expansion';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { FormComponentModule } from '../../shared/components/form-component/form-component.module';
import { HeaderSparkModule } from '../../shared/components/header-spark/header-spark.module';
import { CreateTournamentComponent } from './create-tournament.component';
import { CreateTournamentRoutingModule } from './create-tournament-routing.module';

// page component
import { SelectTypeComponent } from './pages/select-type/select-type.component';
import { PopupComponent } from './pages/popup/popup.component';
// import { Detail7Component } from './pages/detail7/detail7.component';
// END page component

import { InputInviteComponent } from './components/input-invite/input-invite.component';
import { PopupCreatedComponent } from './components/popup-created/popup-created.component';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/modules/shared.module';

import { EsportsQuickTournamentType1Module, EsportsHeaderModule } from 'esports';

import { environment } from '../../../environments/environment';
import { QuickTournamentComponent } from './pages/quick-tournament/quick-tournament.component';
import { CreateQuickComponent } from './pages/create-quick/create-quick.component';

const components = [
  CreateTournamentComponent,
  InputInviteComponent,
  PopupCreatedComponent,
  SelectTypeComponent,
  CreateQuickComponent,
  PopupComponent,
  QuickTournamentComponent
  // Detail7Component,
];

const modules = [
  //CKEditorModule,
  CommonModule,
  CreateTournamentRoutingModule,
  FormComponentModule,
  HeaderSparkModule,
  MatExpansionModule,
  NgxMaterialTimepickerModule,
  SharedModule,
  ReactiveFormsModule,
  RouterBackModule,
  EsportsQuickTournamentType1Module.forRoot(environment),
  EsportsHeaderModule.forRoot({
    borderColor: "#772bcb",
    backgroundColor: '#121212',
    fontColor: '#FFFFFF'
  })
];

@NgModule({
  declarations: components,
  imports: modules,
})
export class CreateTournamentModule { }
