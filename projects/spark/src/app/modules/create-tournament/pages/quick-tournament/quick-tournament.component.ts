import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import {
  GlobalUtils,
  QuickTournamnetComponentData,
  EsportsInfoPopupComponent,
  EsportsInfoPopupComponentData,
  EsportsInfoPopupComponentType,
} from 'esports';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-quick-tournament',
  templateUrl: './quick-tournament.component.html',
  styleUrls: ['./quick-tournament.component.scss'],
})
export class QuickTournamentComponent implements OnInit {
  quickData: QuickTournamnetComponentData = {
    step: 1,
  };

  constructor(
    private router: Router,
    private translateService: TranslateService,
    public dialog: MatDialog
  ) {
    if (GlobalUtils.isBrowser()) {
      this.quickData.step = 1;
    }
  }

  ngOnInit(): void {}

  tournamentCreated(data) {
    if (data?.data?.step) {
      this.quickData.step = data.data.step;
    } else {
      this.router.navigateByUrl(
        '/create-tournament/CreateQuick/' + data.tournamentdetails.slug
      );
    }
  }

  back(data) {
    if (GlobalUtils.isBrowser()) {
      if (data.back) {
        if (this.quickData.step === 1) {
          this.router.navigateByUrl('create-tournament');
        } else {
          this.quickData.step = this.quickData.step - 1;
        }
      }
    }
  }

  close(data) {
    const Data: EsportsInfoPopupComponentData = {
      title: this.translateService.instant('HEADER_SPARK.EXIT?'),
      text: this.translateService.instant('HEADER_SPARK.EXIT?'),
      type: EsportsInfoPopupComponentType.info,
      btnText: this.translateService.instant('HEADER_SPARK.CONFIRM'),
      cancelBtnText: this.translateService.instant('HEADER_SPARK.CANCEL'),
    };

    const dialogRef = this.dialog.open(EsportsInfoPopupComponent, {
      data: Data,
    });
    dialogRef.afterClosed().subscribe((confirmed) => {
      if (confirmed) {
        this.router.navigateByUrl('play');
      }
    });
  }
}
