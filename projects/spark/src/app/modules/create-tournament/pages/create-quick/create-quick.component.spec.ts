import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateQuickComponent } from './create-quick.component';

describe('CreateQuickComponent', () => {
  let component: CreateQuickComponent;
  let fixture: ComponentFixture<CreateQuickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateQuickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateQuickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
