import { Component, OnInit, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { Subscription } from 'rxjs';
import { EsportsUserService, IPagination, UserReportsService } from 'esports';
import { environment } from '../../../../environments/environment';
import { ActivatedRoute } from '@angular/router';
const API = environment.apiEndPoint;

@Component({
  selector: 'app-matches',
  templateUrl: './matches.component.html',
  styleUrls: ['./matches.component.scss'],
})
export class MatchesComponent implements OnInit, OnDestroy {
  mock_cards = [];
  isLoaded = false;
  isAdmin = false;
  userSubscription: Subscription;
  active = 1;
  nextId = 0;
  tab = ['current', 'completed'];

  participantPage: IPagination = {
    activePage: 1,
    totalItems: 1,
    maxSize: 10,
    itemsPerPage: 10,
  };
  page: IPagination;
  paginationDetails: any;
  pageSizeOptions = environment.pageSizeOptions;
  showLoader: boolean = true;
  sortBy='Ongoing matches';
  constructor(
    private userService: EsportsUserService,
    private UserReportsService: UserReportsService,
    // private profileService: ProfileService,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.getCurrentUserDetails();
    if (this.activatedRoute?.snapshot?.params?.activeTab == 2) {
      this.active = 2;
      // this.tabChanged({ nextId: 2 })
    } else {
    }
    this.getMyMatches();
  }

  ngOnDestroy() {
    this.userSubscription?.unsubscribe();
  }

  tabChanged = (e) => {
    this.nextId = e.nextId;
    // this.paginationData = { activePage: 1, itemsPerPage: 10, sort: 'startDate', };
    this.getMyMatches();
  };

  getCurrentUserDetails() {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        if (data?.accountType === 'admin') {
          this.isAdmin = true;
        }
      }
    });
  }
  selectNextId(status){
    
  this.nextId = status
  if(status == 0){
  this.sortBy = 'Ongoing matches'
  }else{
    this.sortBy = 'Completed matches'
  }
  this.getMyMatches();
  
  }
  getMyMatches = async () => {
    try {
      const queryParam = `&limit=${this.participantPage.itemsPerPage}&page=${this.participantPage.activePage}`;
      const status = this.tab[this.nextId];
      const { data } = await this.UserReportsService.getUserMatchesByStatus(
        status,
        queryParam
      );
      this.mock_cards = [];
      this.isLoaded = false;
      const data2 = data?.docs;
      this.paginationDetails = data2;
      this.participantPage.itemsPerPage = data?.limit;
      this.participantPage.totalItems = data?.totalDocs;
      let a = [];
      for (let index = 0, len = data2.length; index < len; index++) {
        const element = data2[index];
        element?.match?.currentMatch?.round &&
          element?.opponentPlayer?.teamName;
        a.push({
          title: element?.match?.currentMatch?.round
            ? `Round ${element?.match?.currentMatch?.round}`
            : '',
          opponentName: element?.opponentPlayer?.teamName || 'N/A',
          gameAccount: element?.opponentPlayer?.inGamerUserId || 'N/A',
          image: element?.tournament?.gameDetail?.logo || 'N/A',
          tournament: element?.tournament?._id || '',
          tournaments: element?.tournament,
          tournamentSlug: element?.tournament?.slug || '',
          matchId: element?.match?._id || '',
          participantId: element?.userParticepentId || '',
          match: element?.match,
          isAdmin: this.isAdmin,
          tournamentName: element?.tournament?.name
            ? element?.tournament?.name.length > 28
              ? `${element?.tournament?.name
                  .toLowerCase()
                  .substring(0, 28)
                  .slice(0, -3)}...`
              : element?.tournament?.name.toLowerCase()
            : '',
        });
      }
      this.mock_cards = a;
    } catch (error) {
      this.isLoaded = false;
    }
  };



  currentPage(page): void {
    this.participantPage.activePage = page;
    this.getMyMatches();
  }
}
