import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EsportsUserService,IPagination } from 'esports';
import { environment } from '../../../../environments/environment';
const API = environment.apiEndPoint;
@Component({
  selector: 'app-my-teams',
  templateUrl: './my-teams.component.html',
  styleUrls: ['./my-teams.component.scss'],
})
export class MyTeamsComponent implements OnInit {
  active = 1;
  nextId: number = 1;
  myTeams = [];
  teamRequests = [];
  showLoader: boolean = true;
  pageSizeOptions=environment.pageSizeOptions;
  paginationDetails: any;
  paginationData = {
    page: 1,
    limit: 10,
    sort: { _id: -1 },
    text: '',
  };
  participantPage: IPagination = {
    activePage: 1,
    totalItems: 1,
    maxSize: 10,
    itemsPerPage: 10,
  };
  constructor(
    private esportsUserService: EsportsUserService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.getMyTeams();
    this.getMyInvite();
  }
  navChanges = (e) => {
    this.nextId = e.nextId;
    switch (this.nextId) {
      case 1:
        this.getMyTeams();
        break;
      case 2:
        this.getMyInvite();
        break;
    }
  };

  getMyInvite = async () => {
    const params: any = { pagination: JSON.stringify(this.paginationData) };
    const res = await this.esportsUserService.getMyInvite(API, params);
    if (res) {
      this.teamRequests = res?.data;
      this.teamRequests = [];
      for (let d of res.data.docs) {
        let d1 = {
          image: d.logo,
          name: d.teamName,
          teamsLength: 5,
          createtdAt: 1,
          _id: d._id,
        };
        this.teamRequests.push(d1);
      }
      this.showLoader = false;
    } else {
      this.showLoader = false;
    }
  };

  getMyTeams = async (pageNum=1) => {
    this.paginationData.page = pageNum;
    const pagination = JSON.stringify(this.paginationData);
    const res = await this.esportsUserService.getMyTeam(API, {
      pagination: pagination,
    });
    if (res) {
      this.myTeams = [];
      if(res.success){
        for (let d of res.data.docs) {
          let d1 = {
            image: d.logo,
            name: d.teamName,
            id: d._id,
            teamBanner: d.teamBanner,
            teamsLength: 1,
            createtdAt: 1,
          };
          this.myTeams.push(d1);
        }
      }else{

      }
      this.paginationDetails = res?.data;
      this.paginationData.limit=this.paginationDetails?.limit;
      this.paginationData.page=this.paginationDetails?.page;
      this.participantPage.itemsPerPage = this.paginationDetails?.limit;
      this.participantPage.totalItems = this.paginationDetails?.totalDocs;
      this.showLoader = false;
    } else {
      this.showLoader = false;
    }
  };
  acceptfunction(data) {
    const datatosend = {
      teamId: data._id,
      status: 'active',
    };
    this.esportsUserService
      .update_invitees(API, datatosend)
      .subscribe((data: any) => {});
  }
  goToDetailTeam(id) {
    this.router.navigateByUrl('/view-team/' + id);
  }

  pageChanged(page){
    this.getMyTeams(page);
  }
}
