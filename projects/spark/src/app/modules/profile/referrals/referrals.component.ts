import { Component, OnInit, Input } from '@angular/core';
import { EsportsUserService } from 'esports';
import { environment } from '../../../../environments/environment';
const API = environment.apiEndPoint;

@Component({
  selector: 'app-referrals',
  templateUrl: './referrals.component.html',
  styleUrls: ['./referrals.component.scss'],
})
export class ReferralsComponent implements OnInit {
  cardLink=environment.cardLink;
  referralID: string;
  referralLink: string = '';
  currentUser: any;
  accepted = [];
  sended = [];
  isCopied: boolean = false;
  paginationData = {
    page: 1,
    limit: 50,
    sort: { createdOn: -1 },
  };
  constructor(private userService: EsportsUserService) {
    this.userService.currentUser.subscribe((res) => {
      if (res) {
        this.currentUser = res;
        this.referralLink =
          this.cardLink + this.currentUser.accountDetail.referralId;
        this.referralID = this.currentUser.accountDetail.referralId;
      }
    });
  }
  ngOnInit(): void {
    this.getReferralUserList();
  }

  getReferralUserList() {
    const pagination = JSON.stringify(this.paginationData);
    this.userService
      .getUserinviteesList(API, { pagination })
      .subscribe((res) => {
        this.accepted = res?.data?.accepted;
        this.sended = res?.data?.sended;
      });
  }

  userClicked(event: any) {
    var text = document.getElementById('textClipboard');
    var wrapText = document.getElementById('wrapText');
    var icon = document.getElementById('icon');
    text.textContent = 'Linked copied';
    wrapText.classList.add('coppied');
    icon.textContent = 'done';
    setTimeout(() => {
      text.textContent = this.referralLink
      icon.textContent = 'file_copy'
      wrapText.classList.remove('coppied')
      this.isCopied = !this.isCopied
    }, 15000
    )
  }
}
