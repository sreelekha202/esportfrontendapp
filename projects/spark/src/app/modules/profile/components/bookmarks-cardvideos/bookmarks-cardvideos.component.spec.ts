import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BookmarksCardvideosComponent } from './bookmarks-cardvideos.component';

describe('BookmarksCardvideosComponent', () => {
  let component: BookmarksCardvideosComponent;
  let fixture: ComponentFixture<BookmarksCardvideosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BookmarksCardvideosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BookmarksCardvideosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
