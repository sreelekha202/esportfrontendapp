import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-bookmarks-table',
  templateUrl: './bookmarks-table.component.html',
  styleUrls: ['./bookmarks-table.component.scss'],
})
export class BookmarksTableComponent implements OnInit {
  @Input() data = [];
  @Input() isVideo: boolean = false;

  constructor() {}

  ngOnInit(): void {
  }

  refresh(id){
  this.data.forEach((e,i)=>{
    if(e?._id==id){
      this.data.splice(i,1)
    }
  })
  }
}
