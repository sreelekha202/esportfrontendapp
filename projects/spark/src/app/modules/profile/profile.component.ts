import { Component, OnInit, OnDestroy } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { AppHtmlProfileRoutes } from '../../app-routing.model';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';
import { TranslateService } from '@ngx-translate/core';
import { environment } from '../../../environments/environment';
import { MatDialog } from '@angular/material/dialog';
import {
  IUser,
  GlobalUtils,
  EsportsUserService,
  EsportsLanguageService,
  EsportsMessageService,
  EsportsGameService,
  EsportsToastService
} from 'esports';
import { Subscription } from 'rxjs';
import { FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';
const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;
AutoUnsubscribe();
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
})
export class ProfileComponent implements OnInit, OnDestroy {
  AppHtmlProfileRoutes = AppHtmlProfileRoutes;
  msgCount: any;
  // currentUser: IUser;
  currentUser: any;
  userSubscription: Subscription;
  country: any;
  state: any;
  isAdmin: boolean = false;
  isAuthor: boolean = false;
  viewStatsFlag: any = null;
  userId: any;
  statisticsData: any;
  statisticsDetails: any;
  hasDataBg = 'assets/images/Profile/dashboard/has-data-bg.jpg';
  noDataBg = 'assets/images/Profile/dashboard/no-data-bg.jpg';
  selectedFile: any;
  base64textString: any;
  url: any;
  showLoader: boolean;
  addForm: FormGroup;
  constructor(
    private languageService: EsportsLanguageService,
    private translateService: TranslateService,
    private messageService: EsportsMessageService,
    public location: Location,
    public router: Router,
    public matDialog: MatDialog,
    private userService: EsportsUserService,
    private gameService: EsportsGameService,
    private toastService: EsportsToastService,
    private fb: FormBuilder,
  ) {
    this.addForm = this.fb.group({
      fullName: [null, Validators.compose([Validators.required])],
      country: [null, Validators.compose([])],
      username: [
        null,
        Validators.compose([
          Validators.required,
          Validators.minLength(3),
          Validators.pattern(/^[a-zA-Z0-9_]+$/),
        ]),
      ],
      gender: [null, Validators.compose([Validators.required])],
      dob: [null, Validators.compose([Validators.required])],
      state: [null, Validators.compose([])],
      // city: [null, Validators.compose([])],
      // postal_code: ['123456', Validators.compose([Validators.required])],
      shortBio: [null],
      profilePicture: [''],
      game: [''],
      socialAccounts: new FormArray([]),
    });
    if (
      GlobalUtils.isBrowser() &&
      !localStorage.getItem(environment.currentToken)
    ) {
      this.router.navigate(['/login']);
    }
  }

  ngOnInit(): void {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.currentUser = data;
        this.getCountry();
        this.getState();
        if (this.currentUser.hasOwnProperty('isAuthor')) {
          this.currentUser.isAuthor == 0 ? (this.isAuthor = true) : '';
        }
      }
    });
    this.getMessages();
    this.languageService.language.subscribe((lang) =>
      this.translateService.use(lang || 'en')
    );
    this.messageService.messageCount.subscribe((count) => { this.msgCount = count });

    /* check for view stats here based on URL */
    this.viewStatsFlag = false;
    if (this.router.url.includes('view-stats')) {
      this.viewStatsFlag = true;
      this.userId = this.router.url.split('/').reverse()[0];
      this.getStatData();
    }

  }

  openSettingsLightbox(selectedTab = 0) {
    const data = { selectedTab };
    this.router.navigate(['/account_settings/profile']);
  }
  fileChangeEvent(fileInput: any) {
    const reg = /(.*?)\.(jpg|jpeg|png|gif|giff)$/;
    if (!fileInput.target.files[0].name.match(reg)) {      
      this.toastService.showError(
        this.translateService.instant('ADMIN.SELECT')
      );
      
      return false;
    } else {
      
      this.selectedFile = fileInput.target.files[0];
      var reader = new FileReader();
      reader.onload = this.handleReaderLoaded.bind(this);
      reader.readAsBinaryString(this.selectedFile);
    }
  }

  handleReaderLoaded(readerEvt) {
    var binaryString = readerEvt.target.result;
    this.base64textString = 'data:image/png;base64,' + btoa(binaryString);
    this.setImage(this.base64textString);
  }

  setUserData() {
      this.addForm.patchValue({
        fullName: this.currentUser?.fullName,
        username: this.currentUser?.username,
        gender: this.currentUser?.gender,
        //'pronouns': this.currentUser['pronouns'],
        dob: this.currentUser?.dob,
        country: this.currentUser?.country,
        state: this.currentUser?.state,
        city: this.currentUser?.city,
        profilePicture: this.currentUser?.profilePicture,
        shortBio: this.currentUser?.shortBio,
        socialAccounts: this.currentUser?.socialAccounts,
      });
  }
  setImage(data) {
    this.showLoader = true;
    this.url = data;
    this.currentUser.profilePicture = this.url
    this.setUserData();
    if (this.addForm.valid) {
      this.userService.updateProfile(API, this.addForm.value).subscribe(
        (res) => {
          this.showLoader = false;
          this.toastService.showSuccess(res.message);
          this.userService.refreshCurrentUser(API, TOKEN);
        },
        (err) => {
          this.showLoader = false;
          this.toastService.showError(err.message);
        }
      );
    }

  }

  getMessages(): void {
    this.messageService.getInboxMessages().subscribe(
      (response) => {
        this.messageService.messageCountSubject.next(response?.data.length);
      },
      (error) => { }
    );
  }

  ngOnDestroy(): void { }

  getCountry() {
    this.userService.getAllCountries().subscribe((res) => {
      if (res && res?.countries) {
        res.countries.filter((obj) => {
          if (obj.id == this.currentUser?.country) {
            this.country = obj;
          }
        });
      }
    });
  }

  getState() {
    this.userService.getStates().subscribe((res) => {
      if (res && res?.states) {
        res?.states.filter((obj) => {
          if (obj.id == this.currentUser?.state) {
            this.state = obj;
          }
        });
      }
    });
  }
  isUserContainData(): boolean {
    const {
      profilePicture: picture,
      preference: { followersCount: folowers, followingCount: following },
    } = this.currentUser;
    return picture && folowers && following;
  }

  getStatData() {
    this.statisticsData = this.gameService.createMatchMaking.subscribe(
      (data: any) => {
        if (data) {
          this.statisticsDetails = data.statisticsDetails;
          if (this.router.url.includes('my-stats')) {
            this.viewStatsFlag = false;
          }
        }
      }
    );
  }
  haveBg() {
    return (
      this.router.url === '/profile/add-video' ||
      this.router.url === '/profile/edit-video'
    )
  }
}
