import { Component, Input } from '@angular/core';
import { ChartType } from 'chart.js';
import { MultiDataSet, Label } from 'ng2-charts';
@Component({
  selector: 'app-bar-chart',
  templateUrl: './bar-chart.component.html',
  styleUrls: ['./bar-chart.component.scss']
})
export class BarChartComponent {

  public chartType: string = 'doughnut';

  public chartDatasets: Array<any> = [
    { data: [300, 50, 100, 40, 120], label: 'My First dataset' }
  ];

  public chartLabels: Array<any> = ['Red', 'Green', 'Yellow', 'Grey', 'Dark Grey'];

  public chartColors: Array<any> = [
    {
      backgroundColor: ['#F7464A', '#46BFBD', '#FDB45C', '#949FB1', '#4D5360'],
      hoverBackgroundColor: ['#FF5A5E', '#5AD3D1', '#FFC870', '#A8B3C5', '#616774'],
      borderWidth: 2,
    }
  ];

  public chartOptions: any = {
    responsive: true
  };
  public chartClicked(e: any): void { }
  public chartHovered(e: any): void { }

  chart = {
    options: {
      responsive: true,
      legend: {
        display: true,
        position: 'bottom'
      },
      cutoutPercentage: 80,
      radius: 20
    },
    labels: ['Win', 'Lose', 'Draw'],
    type: 'doughnut',
    data: [{
      data: [30, 10, 60],
      borderWidth: 0,
    }],
    colors: [{ backgroundColor: ['#007136', '#FF8E26', '#772BCB'] }],

  };

  @Input() statisticsDetails: any;
  // Doughnut
  public doughnutChartLabels: Label[] = ['Win', 'Draw', 'Lose'];
  public doughnutChartData: MultiDataSet = [
    [350, 450, 100],
    // [50, 150, 120],
    // [250, 130, 70],

  ];
  public doughnutChartType: ChartType = 'doughnut';


  selectedMonth: any = 'Select month';

  months = [
    'JANUARY',
    'FEBRUARY',
    'MARCH',
    'APRIL',
    'MAY',
    'JUNE',
    'JULY',
    'AUGUST',
    'SEPTEMBER',
    'OCTOBER',
    'NOVEMBER',
    'DECEMBER',
  ];
  threemonth: any;

  constructor() {
    this.threemonth = "THIS_MONTH";
    this.selectedMonth = this.months[new Date().getMonth()]
  }

  ngOnInit(): void {

  }
  selecteMonths(type) {
    this.threemonth = type;
  }




}
