import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import {
  EsportsUserService,
  IUser,
  EsportsProfileService,
  EsportsGameService,
} from 'esports';
import { environment } from '../../../../environments/environment';
import { Router } from '@angular/router';

const API = environment.apiEndPoint;
@Component({
  selector: 'app-my-stats',
  templateUrl: './my-stats.component.html',
  styleUrls: ['./my-stats.component.scss'],
})
export class MyStatsComponent implements OnInit {
  games = [];
  showLoader: boolean = true;
  viewStatsFlag: any;
  userId: any;
  months = [
    'JANUARY',
    'FEBRUARY',
    'MARCH',
    'APRIL',
    'MAY',
    'JUNE',
    'JULY',
    'AUGUST',
    'SEPTEMBER',
    'OCTOBER',
    'NOVEMBER',
    'DECEMBER',
  ];
  selectedMonth = this.months[new Date().getMonth()];
  selectedYear = new Date().getFullYear();
  statisticsDetails: any;
  userSubscription: Subscription;
  currentUser: IUser;
  params: any = {};
  logoDetails:any;
  selectedGame:any;
  constructor(
    private userService: EsportsUserService,
    private profileService: EsportsProfileService,
    private gameService: EsportsGameService,
    public router: Router
  ) {
    // MOCK DATA
    this.games = [{ name: 'valorant' }, { name: 'fortnine' }];
  }

  ngOnInit(): void {
    // this.params.month = this.months.indexOf(this.selectedMonth) + 1;
    this.params.days = 30;
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.currentUser = data;

        /* check for view stats here based on URL */
        this.viewStatsFlag = false;
        if (this.router.url.includes('/view-stats/')) {
          this.viewStatsFlag = true;
          this.userId = this.router.url.split('/').reverse()[0];
        }

        this.logoDetails = this.gameService.createMatchMaking.subscribe(
          (data: any) => {
            if (data) {
              this.selectedGame = data.selectedGame;
            }
          }
        );

        this.getMyStatistics();
      }
    });
  }

  getMyStatistics() {
    this.showLoader = true;
    // this.params.year = this.selectedYear;
    this.profileService
      .getMyStatistics(
        this.viewStatsFlag ? this.userId : this.currentUser._id,
        this.params
      )
      .subscribe(
        (res) => {
          this.statisticsDetails = res?.data;
          this.showLoader = false;
          this.gameService.createMatchMakingSubject.next({
            statisticsDetails: this.statisticsDetails,
            selectedGame: this.selectedGame
          });
        },
        (err) => {
          this.showLoader = false;
        }
      );
  }
  onMonthChange(data) {
    this.selectedMonth = data;
    this.getMyStatistics();
  }

  monthData(month) {
    this.params.days = month;
    this.getMyStatistics();
  }

  ngOnDestroy(): void {
    this.userSubscription.unsubscribe();
  }
}
