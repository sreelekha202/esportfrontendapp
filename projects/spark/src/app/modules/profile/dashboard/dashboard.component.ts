import { Component, OnDestroy, OnInit } from '@angular/core';
import {
  EsportsUserService,
  IUser,
  UserReportsService,
  EsportsUserPreferenceService,
  EsportsToastService,
  EsportsGameService,
} from 'esports';
import { Subscription } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { SettingsLightboxComponent } from '../../settings-lightbox/settings-lightbox.component';
import { environment } from '../../../../environments/environment';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';
import { AppHtmlRoutes } from '../../../app-routing.model';


const API = environment.apiEndPoint;

@AutoUnsubscribe()
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit, OnDestroy {

  AppHtmlRoutes = AppHtmlRoutes;
  hasDataBg = 'assets/images/Profile/dashboard/has-data-bg.jpg';
  noDataBg = 'assets/images/Profile/dashboard/no-data-bg.jpg';
  upcomingMatch = "Upcoming match";
  date = new Date();
  matches = [];
  currentUser: IUser;
  userSubscription: Subscription;
  country: any;
  state: any;
  showLoader: boolean;
  games: any = [];
  gameslist: any = [];
  teamRequests = [];
  showText = false;
  showText1 = false;
  paginationDetails: any;
  page = 1;
  pageSizeOptions = environment.pageSizeOptions;
  paginationData = {
    page: 1,
    limit: 3,
    sort: { _id: -1 },
    text: '',
  };
  allgames: any;
  shortBioLessMore: any;
  gamesList: any;
  isFollows: boolean = false;
  public socialAccounts: any;
  public customPronoun: any;
  public gameDetail: any;
  constructor(
    private _esportsUserService: EsportsUserService,
    private _userReportsService: UserReportsService,
    private _esportsuserPreferenceService: EsportsUserPreferenceService,
    private _esportsToastService: EsportsToastService,
    private gameService: EsportsGameService,
    public matDialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.getMatches();
    this.getGamesList()
  }


  getGamesList() {
    this.gameService.getGames(environment.apiEndPoint).subscribe((res) => {
      if (res) {
        this.gamesList = res?.data;
        this.getUserData();
      }
    })
  }


  getUserData(): void {
    this.userSubscription = this._esportsUserService.currentUser.subscribe(
      (data) => {
        if (data) {
          this.currentUser = data;
          let gameArray = [];
          if (data.preference) {
            data.preference.gameDetails.map((o) => {
              let gameLogo = this.gamesList.find(ele =>
                ele?._id == o?._id)
              gameArray.push(
                {
                  name: o?.name,
                  _id: o?._id,
                  image: gameLogo?.logo,
                  userGameId: o?.userGameId
                }
              );
            });
          }
          this.gameDetail = gameArray;
          this.allgames = this.gameDetail;
          if (this.allgames && this.allgames.length > 2) {
            this.allgames = this.allgames.slice(0, 2)
          }
          this.getCountry();
          this.getState();
          this.getMyInvite();
          this.shortBioLessMore = (this.currentUser.shortBio ? this.currentUser.shortBio : '');
          if (this.shortBioLessMore)
            this.shortBioLessMore = this.shortBioLessMore.slice(0, 70);
        }
      }
    );
  }


  getCountry() {
    this._esportsUserService.getAllCountries().subscribe((res) => {
      if (res && res?.countries) {
        res.countries.filter((obj) => {
          if (obj.id == this.currentUser?.country) {
            this.country = obj;
          }
        });
      }
    });
  }

  getState() {
    this._esportsUserService.getStates().subscribe((res) => {
      if (res && res?.states) {
        res.states.filter((obj) => {
          if (obj.id == this.currentUser?.state) {
            this.state = obj;
          }
        });
      }
    });
  }

  isUserContainData(): boolean {
    const {
      profilePicture: picture,
      preference: { followersCount: folowers, followingCount: following },
    } = this.currentUser;

    return picture && folowers && following;
  }

  openSettingsLightbox(selectedTab = 0) {
    const data = { selectedTab };
    this.matDialog.open(SettingsLightboxComponent, {
      data,
      position: {
        top: '34px',
      },
    });
  }

  getMatches = async () => {
    try {
      this.showLoader = true;
      const queryParam = `&page=${this.page}`;
      const matchStatus = 'current';
      const res = await this._userReportsService.getUserMatchesByStatus(
        matchStatus,
        queryParam
      );
      if (res) {
        this.matches = res?.data?.docs.splice(0, 3);
        this.showLoader = false;
      }
    } catch (error) {
      this.showLoader = false;
    }
  };

  getMyInvite = async () => {
    const params: any = { pagination: JSON.stringify(this.paginationData) };
    const res = await this._esportsUserService.getMyInvite(API, params);
    if (res) {
      this.teamRequests = res?.data;
      this.teamRequests = [];
      for (let d of res.data.docs) {
        let d1 = {
          image: d.logo,
          name: d.teamName,
          teamsLength: 5,
          createtdAt: 1,
          _id: d._id,
        };
        this.teamRequests.push(d1);
      }
      this.showLoader = false;
    } else {
      this.showLoader = false;
    }
  };

  acceptRejectfunction(data, status) {
    const datatosend = {
      teamId: data._id,
      status: '',
    };
    if (status == 'active') {
      datatosend.status = 'active';
      this._esportsUserService
        .update_invitees(API, datatosend)
        .subscribe((data: any) => {
          this._esportsToastService.showSuccess(data.message);
          this.getMyInvite();
        });
    } else {
      datatosend.status = 'rejected';
      this._esportsUserService
        .update_invitees(API, datatosend)
        .subscribe((data: any) => {
          this._esportsToastService.showSuccess(data.message);
          this.getMyInvite();
        });
    }
  }
  lessMoreText() {
    this.showText1 = !this.showText1;
    if (this.showText1) {
      this.shortBioLessMore = this.currentUser?.shortBio || []
    } else {
      this.shortBioLessMore = this.shortBioLessMore.slice(0, 70);
    }
  }

  lessMore() {
    this.showText = !this.showText;
    if (this.showText) {
      this.allgames = this.gameDetail || []
    } else {
      this.allgames = this.allgames.slice(0, 2);
    }
  }


  // resfresh(id){
  //   this.teamRequests.forEach((el,i)=> {
  //     if(el._id==id){
  //       this.teamRequests.splice(i,1)
  //     }
  //   });

  // }

  ngOnDestroy(): void { }

  ToFollow() {
    this.isFollows = !this.isFollows;
  }
}
