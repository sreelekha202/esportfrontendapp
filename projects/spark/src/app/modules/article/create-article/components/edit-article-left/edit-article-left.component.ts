import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Subscription } from 'rxjs';
import {
  EsportsUserService,
  IUser
} from 'esports';
@Component({
  selector: 'app-edit-left-article',
  templateUrl: './edit-article-left.component.html',
  styleUrls: ['./edit-article-left.component.scss'],
})
export class EditArticleLeftComponent implements OnInit {
  @Input() data: any;
  @Output() LeftAction = new EventEmitter();

  currentUser: IUser;
  userSubscription: Subscription;

  constructor(
    private userService: EsportsUserService
  ){}

  ngOnInit(): void {
    
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.currentUser = data;
      }
    });
  }

  ngOnDestroy() {
    if (this.userSubscription)
      this.userSubscription.unsubscribe();
  }

  viewDeleteAction(val){
    this.LeftAction.emit(val);
  }
}
