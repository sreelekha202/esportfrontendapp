import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthorAccessGuard } from '../../../shared/guard/isAuthorInfluencer.guard';
import { CoreModule } from '../../../core/core.module';
import { CreateArticleComponent } from './create-article.component';
import { PaginationModule, PaginationConfig } from 'ngx-bootstrap/pagination';
import { ShareModule } from 'ngx-sharebuttons';
import { SharedModule } from '../../../shared/modules/shared.module';
import { HeaderSparkModule } from '../../../shared/components/header-spark/header-spark.module';
import { EditArticleLeftComponent } from './components/edit-article-left/edit-article-left.component';
import { EsportsHeaderModule,EsportsLoaderModule,WYSIWYGEditorModule } from 'esports';
const routes: Routes = [
  {
    path: '',
    canActivate: [AuthorAccessGuard],
    component: CreateArticleComponent,
  },
];

@NgModule({
  declarations: [CreateArticleComponent, EditArticleLeftComponent],
  imports: [
    CoreModule,
    PaginationModule.forRoot(),
    // PipeModule,
    RouterModule.forChild(routes),
    SharedModule,
    ShareModule,
    HeaderSparkModule,
    EsportsLoaderModule.setColor('#00a851'),
    WYSIWYGEditorModule,
    EsportsHeaderModule.forRoot({
      borderColor: "#772bcb",
      backgroundColor: '#121212',
      fontColor: '#FFFFFF'
    })
  ],
  providers: [{ provide: PaginationConfig }],
})
export class CreateArticleModule {}
