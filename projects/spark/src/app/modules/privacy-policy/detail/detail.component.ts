import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {
  privacypolicy = 'personal';
  list = true;
  actionName = "";
  constructor(
    public translate: TranslateService
  ) {

  }

  ngOnInit(): void {
  }
  changevalues(val: any) {
    this.privacypolicy = val;
  }
  checkInvite() {
    if (this.privacypolicy == `personal`) {
      return this.actionName = this.translate.instant('PRIVACY_POLICY.DETAIL.PERSONAL')
    } else if (this.privacypolicy == `collect`) {
      return this.actionName = this.translate.instant('PRIVACY_POLICY.DETAIL.COLLECT')
    } else if (this.privacypolicy == `use`) {
      return this.actionName = this.translate.instant('PRIVACY_POLICY.DETAIL.USE')
    } else if (this.privacypolicy == `share`) {
      return this.actionName = this.translate.instant('PRIVACY_POLICY.DETAIL.SHARE')
    } else if (this.privacypolicy == `keepSafe`) {
      return this.actionName = this.translate.instant('PRIVACY_POLICY.DETAIL.KEEPSAFE')
    } else if (this.privacypolicy == `privacy`) {
      return this.actionName = this.translate.instant('PRIVACY_POLICY.DETAIL.PRIVACY')
    }
  }

}
