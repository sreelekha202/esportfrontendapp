import { CdkAccordionModule } from '@angular/cdk/accordion';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FooterSparkModule } from '../../shared/components/footer-spark/footer-spark.module';
import { SharedModule } from '../../shared/modules/shared.module';
import { PrivacyPolicyRoutingModule } from './privacy-policy-routing.module';
import { PrivacyPolicyComponent } from './privacy-policy.component';
import { DetailComponent } from './detail/detail.component';

const components = [
  PrivacyPolicyComponent,
  DetailComponent
];

const modules = [
  CommonModule,
  SharedModule,
  PrivacyPolicyRoutingModule,
  FooterSparkModule,
  CdkAccordionModule,
];

@NgModule({
  declarations: components,
  imports: modules,
})
export class PrivacyPolicyModule { }
