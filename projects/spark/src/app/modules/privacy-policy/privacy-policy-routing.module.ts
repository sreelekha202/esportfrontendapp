import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { PrivacyPolicyComponent } from './privacy-policy.component';
import { DetailComponent } from './detail/detail.component';

const routes: Routes = [
  { path: '', component: PrivacyPolicyComponent },
  { path: 'detail', component: DetailComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PrivacyPolicyRoutingModule { }
