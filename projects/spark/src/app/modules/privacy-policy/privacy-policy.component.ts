import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { TranslateService } from '@ngx-translate/core';
import { EsportsLanguageService } from 'esports';

@Component({
  selector: 'app-privacy-policy',
  templateUrl: './privacy-policy.component.html',
  styleUrls: ['./privacy-policy.component.scss'],
})
export class PrivacyPolicyComponent implements OnInit {
  arebicText: boolean = false;

  public currentLang: string;

  currLanguage = 'english';

  privacypolicy = 'overview';

  items = [
    {
      title: 'Who this policy applies to',
      description: 'This policy explains how we collect, use and share your personal information and how we keep it safe.'
    },
    {
      title: 'Other privacy policies',
      description: 'We will update this policy in advance of such changes. If the changes are significant, we will take extra steps to make you aware of them, such as communicating with you directly.'
    },
  ];
  expandedIndex = 0;
  constructor(
    private location: Location,
    public language: EsportsLanguageService,
    public translate: TranslateService
  ) { }

  ngOnInit(): void {
    this.language.language.subscribe((lang) => {
      if (lang) {
        this.arebicText = lang === 'ms';
      }
    });
  }

  goBack() {
    this.location.back();
  }
  changevalues(val: any) {
    this.privacypolicy = val;
  }
}
