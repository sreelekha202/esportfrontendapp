import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TournamentNumberplayerComponent } from './tournament-numberplayer.component';

describe('TournamentNumberplayerComponent', () => {
  let component: TournamentNumberplayerComponent;
  let fixture: ComponentFixture<TournamentNumberplayerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TournamentNumberplayerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TournamentNumberplayerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
