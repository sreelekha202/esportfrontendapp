import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AppHtmlRoutes } from '../../../../app-routing.model';
import { 
  EsportsInfoPopupComponentData,
  EsportsInfoPopupComponentType,
  EsportsInfoPopupComponent } from 'esports';
import { MatDialog } from '@angular/material/dialog';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  @Input() isFirstPage: boolean = false;
  @Input() isAdvance: boolean = false;
  @Output() backClick = new EventEmitter();

  AppHtmlRoutes = AppHtmlRoutes;
  checkQuick8 = false;
  
  constructor(
    private router: Router,
    private activeRoute: ActivatedRoute,
    private dialog: MatDialog,
    private translateService: TranslateService
  ) { }

  ngOnInit(): void { }

  navigateToInfoPage(params: string) {
    this.router.navigate([params], { relativeTo: this.activeRoute });
  }

  onBackClick() {
    this.backClick.emit();
  }

  closePopup(){
    const popUpData: EsportsInfoPopupComponentData = {
      title: this.translateService.instant('HEADER_SPARK.EXIT'),
      text: this.translateService.instant('HEADER_SPARK.EXIT_SUBTEXT'),
      type: EsportsInfoPopupComponentType.info,
      btnText: this.translateService.instant('HEADER_SPARK.CONFIRM'),
      cancelBtnText: this.translateService.instant('HEADER_SPARK.CANCEL'),
    };

    const dialogRef = this.dialog.open(EsportsInfoPopupComponent, {
      data: popUpData,
    });

    dialogRef.afterClosed().subscribe((confirmed) => {
      if (confirmed) {
        this.router.navigate([AppHtmlRoutes.createTournament]);
      }
    });
  }
}
