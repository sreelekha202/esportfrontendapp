import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TournamentTypeComponent } from './tournament-type.component';

describe('TournamentTypeComponent', () => {
  let component: TournamentTypeComponent;
  let fixture: ComponentFixture<TournamentTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TournamentTypeComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TournamentTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
