import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InputDateTwoComponent } from './input-date-two.component';

describe('InputDateTwoComponent', () => {
  let component: InputDateTwoComponent;
  let fixture: ComponentFixture<InputDateTwoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [InputDateTwoComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InputDateTwoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
