import { CoreModule } from './../../core/core.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  NgxMatDatetimePickerModule,
  NgxMatNativeDateModule,
  NgxMatTimepickerModule,
} from '@angular-material-components/datetime-picker';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatRadioModule } from '@angular/material/radio';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatIconModule } from '@angular/material/icon';
import { InlineSVGModule } from 'ng-inline-svg';
import { Routes, RouterModule } from '@angular/router';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';

// Component
import { QuickAdvanceTournamentComponent } from './quick-advance.component';

import { HeaderComponent } from './components/header/header.component';
import { AddPrizesButtonComponent } from './components/add-prizes-button/add-prizes-button.component';
import { InputDateComponent } from './components/input-date/input-date.component';
import { InputInviteComponent } from './components/input-invite/input-invite.component';
import { InputSearchComponent } from './components/input-search/input-search.component';
import { InputGameComponent } from './components/input-game/input-game.component';
import { InputSelectComponent } from './components/input-select/input-select.component';
import { InputTextComponent } from './components/input-text/input-text.component';
import { InputTooltipComponent } from './components/input-tooltip/input-tooltip.component';
import { PageSwitcherComponent } from './components/page-switcher/page-switcher.component';
import { ParticipantComponent } from './components/participant/participant.component';
import { TournamentTypeComponent } from './components/tournament-type/tournament-type.component';
import { TypeCardComponent } from './components/type-card/type-card.component';
import { UploadImageComponent } from './components/upload-image/upload-image.component';
import { InputNumberComponent } from './components/input-number/input-number.component';
import { InputDateTwoComponent } from './components/input-date-two/input-date-two.component';
import { TournamentNumberplayerComponent } from './components/tournament-numberplayer/tournament-numberplayer.component';

import { SelectTypeComponent } from './pages/select-type/select-type.component';
import { TournamentNameComponent } from './pages/tournament-name/tournament-name.component';
import { SelectGameComponent } from './pages/select-game/select-game.component';
import { SelectPlatformComponent } from './pages/select-platform/select-platform.component';
import { SelectDateComponent } from './pages/select-date/select-date.component';
import { AddPrizesComponent } from './pages/add-prizes/add-prizes.component';
import { EntranceFeeComponent } from './pages/entrance-fee/entrance-fee.component';
import { BracketTypeComponent } from './pages/bracket-type/bracket-type.component';
import { SendInviteComponent } from './pages/send-invite/send-invite.component';
import { TournamentDetailsComponent } from './pages/tournament-details/tournament-details.component';
import { PaymentComponent } from './pages/payment/payment.component';
import { PrizeMoneyComponent } from './pages/prize-money/prize-money.component';
import { EntranceFeeAmountComponent } from './pages/entrance-fee-amount/entrance-fee-amount.component';
import { Detail7Component } from './pages/detail7/detail7.component';

import { PlatformPipe } from './pipe/platform.pipe';
import { BracketPipe } from './pipe/bracket.pipe';
import { CustomFilterPipe } from './pipe/customFilter.pipe';
import { ConfirmComponent } from './popups/confirm/confirm.component';
import { PaymentModule } from '../../modules/payment/payment.module';
import { EliminationModule } from '../../shared/components/elimination/elimination.module';
import { FormComponentModule } from '../../shared/components/form-component/form-component.module';
import { I18nModule, PipeModule, WYSIWYGEditorModule, EsportsLoaderModule } from 'esports';
import { PrizePoolPipe } from './pipe/prize-pool.pipe';
import { HeaderSparkModule } from '../../shared/components/header-spark/header-spark.module';
import { TournamentImageComponent } from './components/tournament-image/tournament-image.component';
import { TournamentDateComponent } from './components/tournament-date/tournament-date.component';
import { TournamentTimeComponent } from './components/tournament-time/tournament-time.component';
import { SharedModule } from '../../shared/modules/shared.module';
import { environment } from '../../../environments/environment';
import { DatePickerHeaderComponent } from './components/date-picker-header/date-picker-header.component';

const routes: Routes = [
  { path: '', component: QuickAdvanceTournamentComponent },
];

@NgModule({
  declarations: [
    QuickAdvanceTournamentComponent,
    HeaderComponent,
    AddPrizesButtonComponent,
    InputDateComponent,
    InputInviteComponent,
    TournamentImageComponent,
    InputSearchComponent,
    InputGameComponent,
    InputSelectComponent,
    InputTextComponent,
    InputTooltipComponent,
    PageSwitcherComponent,
    ParticipantComponent,
    TournamentTypeComponent,
    TypeCardComponent,
    UploadImageComponent,
    InputNumberComponent,
    InputDateTwoComponent,
    SelectTypeComponent,
    TournamentNameComponent,
    SelectGameComponent,
    SelectPlatformComponent,
    SelectDateComponent,
    AddPrizesComponent,
    EntranceFeeComponent,
    EntranceFeeAmountComponent,
    BracketTypeComponent,
    SendInviteComponent,
    TournamentDetailsComponent,
    PaymentComponent,
    PrizeMoneyComponent,
    PlatformPipe,
    BracketPipe,
    CustomFilterPipe,
    PrizePoolPipe,
    ConfirmComponent,
    TournamentDateComponent,
    TournamentTimeComponent,
    TournamentNumberplayerComponent,
    Detail7Component,
    DatePickerHeaderComponent,
  ],
  imports: [
    HeaderSparkModule,
    CommonModule,
    FormComponentModule,
    NgxMatDatetimePickerModule,
    NgxMatTimepickerModule,
    NgxMatNativeDateModule,
    WYSIWYGEditorModule,
    PaymentModule,
    MatExpansionModule,
    MatRadioModule,
    I18nModule.forRoot(environment),
    FormsModule,
    MatDatepickerModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatIconModule,
    PipeModule,
    InlineSVGModule,
    RouterModule.forChild(routes),
    EliminationModule,
    SharedModule,
    CoreModule,
    NgxMaterialTimepickerModule,
    EsportsLoaderModule.setColor('#a168df'),
  ],
  exports: [QuickAdvanceTournamentComponent],
})
export class QuickAdvanceTournamentModule { }
