import { Pipe, PipeTransform } from '@angular/core';
@Pipe({
  name: 'customFilterPipe',
})
export class CustomFilterPipe implements PipeTransform {
  transform(array: any, type: string) {
    if (type == 'quick') {
      return array?.filter((item) => item?.isQuickFormatAllow) || [];
    }
    return array || [];
  }
}
