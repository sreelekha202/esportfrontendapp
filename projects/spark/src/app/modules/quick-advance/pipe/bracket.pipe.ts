import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'bracketPipe',
})
export class BracketPipe implements PipeTransform {
  transform(value) {
    const bracketTypes = {
      single: 'assets/icons/matchmaking/format/icon-Single-Elimination.svg',
      double: 'assets/icons/matchmaking/format/icon-Double-Elimination.svg',
      round_robin: 'assets/icons/matchmaking/format/icon-Round-Robin.svg',
      battle_royale: 'assets/icons/matchmaking/format/icon-Battle-Royale.svg',
      swiss_safeis: 'assets/icons/matchmaking/format/icon-Swiss-System.svg',
      ladder: 'assets/icons/matchmaking/format/icon-Ladder.svg',
    };

    return (
      bracketTypes[value] || 'assets/icons/matchmaking/format/Round-robin.svg'
    );
  }
}
