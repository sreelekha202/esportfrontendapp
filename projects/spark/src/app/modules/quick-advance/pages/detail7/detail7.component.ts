import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs-compat';
import { AppHtmlRoutes } from '../../../../app-routing.model';
import { GlobalUtils, WYSIWYGEditorConfig } from 'esports';

@Component({
  selector: 'app-detail7',
  templateUrl: './detail7.component.html',
  styleUrls: ['./detail7.component.scss'],
})
export class Detail7Component implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;
  // date: Date = new Date();
  // time: any = timeFormatAMPM(new Date())
  editorConfig: WYSIWYGEditorConfig = {};
  date: Date;
  time: any;
  tournamentDetails: Subscription;
  platforms = [];
  payouts = [0];
  createTournament: any;
  rules: any;
  text: string;
  ckConfig: any;
  dataTournamentTypeValue = 0;
  fileToUploadLogo: File | null = null;
  fileToUploadBanner: File | null = null;
  checkFileNameLogo: boolean = false;
  checkFileNameBanner: boolean = false;
  toggleInputFileLogo: boolean = false;
  toggleInputFileBanner: boolean = false;
  toggleChangePrizePayout: boolean = false;
  toggleChangeTournamentSettings: boolean = false;
  toggleChangeRulesAndInfo: boolean = false;
  toggleChangeSponsors: boolean = false;
  selectCurrency: string = '';
  selectPay: string = '';
  selectContact: string = '';
  screenshotRequired: boolean = false;
  showCountryFlag: boolean = false;

  participants: boolean = true;
  myselfOnly: boolean = false;
  noLimit: boolean = true;
  addLimit: boolean = false;
  private: boolean = true;
  public: boolean = false;
  noCheckIn: boolean = true;
  addCheckIn: boolean = false;
  noSubstitute: boolean = true;
  addSubstitute: boolean = false;

  showAdvanceStage = false;
  showCustomRegistrationEndDate = false;
  showCustomTournamentEndDate = false;
  selectRegion: string = '';
  selectRound: string = '';
  selectPlaceMatch: string = '';
  selectMatchFormat: string = '';
  selectStageBracket: string = '';
  selectRoundRobin: string = '';
  waggerPool = JSON.parse(sessionStorage.getItem('step6'));
  dataTournamentType = [
    {
      id: 0,
      textTitle: 'Online',
      textDes: 'Your tournament will be held as an online event',
    },
    {
      id: 1,
      textTitle: 'Offline',
      textDes: 'Your tournament will be held as an offline event',
    },
    {
      id: 2,
      textTitle: 'Hybrid',
      textDes:
        'Your tournament will be held as a hybrid (online and offline) event',
    },
  ];

  currencies = ['USD', 'VND', 'DKK'];
  payoutsMethod = [
    'Select payout method 1',
    'Select payout method 2',
    'Select payout method 3',
  ];
  contactOptions = [
    'Select contact option 1 ',
    'Select contact option 2 ',
    'Select contact option 3',
  ];

  regions = [
    'Select participating regions 1',
    'Select participating regions 2',
    'Select participating regions 3',
  ];

  numberOfRounds = ['Round 1', 'Round 2', 'Round 3'];

  placeMatchs = ['Round 1', 'Round 2', 'Round 3'];

  matchFormats = ['Round 1', 'Round 2', 'Round 3'];

  stageBrackets = ['Round 1', 'Round 2', 'Round 3'];

  roundRobins = ['Round 1/11', 'Round 2/11', 'Round 3/11'];

  constructor() {}

  ngOnInit(): void {
    this.editorConfig = {
      editable: true,
      spellcheck: true,
      height: '200px',
      width: 'auto',
      minWidth: '0',
      translate: 'false',
      enableToolbar: true,
      showToolbar: true,
      defaultParagraphSeparator: '',
      defaultFontName: '',
      defaultFontSize: '',
      fonts: [
        { class: 'arial', name: 'Arial' },
        { class: 'times-new-roman', name: 'Times New Roman' },
        { class: 'calibri', name: 'Calibri' },
        { class: 'comic-sans-ms', name: 'Comic Sans MS' },
      ],
      customClasses: [
        {
          name: 'quote',
          class: 'quote',
        },
        {
          name: 'redText',
          class: 'redText',
        },
        {
          name: 'titleText',
          class: 'titleText',
          tag: 'h1',
        },
      ],
      sanitize: true,
      toolbarPosition: 'top',
      toolbarHiddenButtons: [['bold', 'italic'], ['fontSize']],
    };
  }
  onDateChange(data) {
    if (GlobalUtils.isBrowser()) {
      this.date = data;
      localStorage.setItem('date', data);
    }
  }
  onTimeChange(data) {
    if (GlobalUtils.isBrowser()) {
      this.time = data;
      localStorage.setItem('time', data);
    }
  }

  setShowAdvanceStage(): void {
    this.showAdvanceStage = true;
  }

  setShowCustomRegistrationEndDate(): void {
    this.showCustomRegistrationEndDate = true;
  }

  setShowCustomTournamentEndDate(): void {
    this.showCustomTournamentEndDate = true;
  }

  checkDateTime(): Boolean {
    return true;
  }

  ngOnDestroy(): void {
    if (this.tournamentDetails) this.tournamentDetails.unsubscribe();
  }
  clickChecked(checked) {
    this.dataTournamentTypeValue = checked.id;
    checked.ichecked = true;
    // this.Step6.get('step').setValue(step);
  }

  handleFileInputLogo(files: FileList) {
    this.toggleInputFileLogo = !this.toggleInputFileLogo;
    this.checkFileNameLogo = true;
    this.fileToUploadLogo = files.item(0);
  }

  handleFileInputBanner(files: FileList) {
    this.toggleInputFileBanner = !this.toggleInputFileBanner;
    this.checkFileNameBanner = true;
    this.fileToUploadBanner = files.item(0);
  }
  // Handlechange currency
  onToggleChangeCurrency(currency): void {
    this.selectCurrency = currency;
  }

  // Handlechange payloadMethod Component 2
  onToggleChangePayouts(pay): void {
    this.selectPay = pay;
  }
  onToggleChangeContact(contact): void {
    this.selectContact = contact;
  }

  addPayout(): void {
    if (this.waggerPool === 'none') {
      let idP = this.payouts[this.payouts.length - 1];
      this.payouts.push(idP + 1);
    } else {
      let idP = this.waggerPool.amounts[this.waggerPool.amounts.length - 1];
      this.waggerPool.amounts.push(idP + 1);
    }
  }

  onToggleDelete(id): void {
    if (this.waggerPool === 'none') {
      const indexPayout = this.payouts.findIndex((p) => p === id);
      if (indexPayout < 0) return;

      const newPayout = this.payouts;
      if (newPayout.length > 1) {
        newPayout.splice(indexPayout, 1);
      }
      this.payouts = newPayout;
    } else {
      const indexPayout = this.waggerPool.amounts.findIndex((p) => p === id);
      if (indexPayout < 0) return;

      const newPayout = this.waggerPool.amounts;
      if (newPayout.length > 1) {
        newPayout.splice(indexPayout, 1);
      }
      this.waggerPool.amounts = newPayout;
    }
  }

  onToggleChangePrizePayout(): void {
    this.toggleChangePrizePayout = !this.toggleChangePrizePayout;
  }

  onToggleChangeTournamentSettings(): void {
    this.toggleChangeTournamentSettings = !this.toggleChangeTournamentSettings;
  }

  onToggleChangeRulesAndInfo(): void {
    this.toggleChangeRulesAndInfo = !this.toggleChangeRulesAndInfo;
  }

  onToggleChangeSponsors(): void {
    this.toggleChangeSponsors = !this.toggleChangeSponsors;
  }

  onCheckscreenshotRequired(): void {
    this.screenshotRequired = !this.screenshotRequired;
  }

  onCheckShowCountryFlag(): void {
    this.showCountryFlag = !this.showCountryFlag;
  }

  onCheckScoreReporting(): void {
    this.participants = !this.participants;
    this.myselfOnly = !this.myselfOnly;
  }

  onCheckParticipantLimit(): void {
    this.noLimit = !this.noLimit;
    this.addLimit = !this.addLimit;
  }

  onCheckVisibility(): void {
    this.private = !this.private;
    this.public = !this.public;
  }

  onCheckCheckInRequired(): void {
    this.noCheckIn = !this.noCheckIn;
    this.addCheckIn = !this.addCheckIn;
  }

  onCheckMandatorySubstitute(): void {
    this.noSubstitute = !this.noSubstitute;
    this.addSubstitute = !this.addSubstitute;
  }

  onToggleChangeregion(region): void {
    this.selectRegion = region;
  }

  onToggleChangeNumberOfRounds(numberOfRound): void {
    this.selectRound = numberOfRound;
  }

  onToggleChangePlaceMatchs(placeMatch): void {
    this.selectPlaceMatch = placeMatch;
  }

  onToggleChangeMatchFormats(matchFormat): void {
    this.selectMatchFormat = matchFormat;
  }

  onToggleChangeStageBrackets(stageBracket): void {
    this.selectStageBracket = stageBracket;
  }

  onToggleChangeRoundRobins(roundRobin): void {
    this.selectRoundRobin = roundRobin;
  }
}
