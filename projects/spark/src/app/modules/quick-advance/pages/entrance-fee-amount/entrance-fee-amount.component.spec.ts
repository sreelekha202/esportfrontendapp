import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EntranceFeeAmountComponent } from './entrance-fee-amount.component';

describe('EntranceFeeComponent', () => {
  let component: EntranceFeeAmountComponent;
  let fixture: ComponentFixture<EntranceFeeAmountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EntranceFeeAmountComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EntranceFeeAmountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
