import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EsportsToastService, GlobalUtils } from 'esports';
import { AppHtmlRoutes } from '../../../app-routing.model';

@Component({
  selector: 'app-voucher-purchase-successful',
  templateUrl: './voucher-purchase-successful.component.html',
  styleUrls: ['./voucher-purchase-successful.component.scss'],
})
export class VoucherPurchaseSuccessfulComponent implements OnInit {
  @Input() shopItem;
  @Input() purchaseResponse;
  AppHtmlRoutes = AppHtmlRoutes;
  cardLink = 'AQW97EO019OI';
  referralID: string;
  referralLink: string = '';
  constructor(private toast: EsportsToastService, private router: Router) { }

  ngOnInit(): void {
    window.scroll(0, 0);
    

  }

  copyInputMessage(inputElement) {
    if (GlobalUtils.isBrowser()) {
      inputElement.select();
      document.execCommand('copy');
      inputElement.setSelectionRange(0, 0);
      this.toast.showSuccess('Copied!');
    }
  }

  navigatefunc() {
    let flag = '1';
    sessionStorage.setItem('flagforpruchase', flag);
    this.router.navigateByUrl('wallet/transactions');
  }
  userClicked(event: any) {
    var text = document.getElementById('textClipboard');
    var wrapText = document.getElementById('wrapText');
    var icon = document.getElementById('icon');
    text.textContent = 'Linked copied';
    wrapText.classList.add('coppied');
    icon.textContent = 'done';
  }
}
