import { Component, Input, OnInit } from '@angular/core';
import { GlobalUtils } from 'esports';
import { AppHtmlRoutes,AppHtmlWalletRoutes } from '../../../app-routing.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-purchase-un-successful',
  templateUrl: './purchase-un-successful.component.html',
  styleUrls: ['./purchase-un-successful.component.scss'],
})
export class PurchaseUnSuccessfulComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;
  AppHtmlWalletRoutes=AppHtmlWalletRoutes
  cardLink = 'AQW97EO019OI';
  referralID: string;
  referralLink: string = '';
  constructor(private router: Router) { }

  ngOnInit(): void {
    if (GlobalUtils.isBrowser()) {
      window.scroll(0, 0);
    }
  }
  userClicked(event: any) {
    var text = document.getElementById('textClipboard');
    var wrapText = document.getElementById('wrapText');
    var icon = document.getElementById('icon');
    text.textContent = 'Linked copied';
    wrapText.classList.add('coppied');
    icon.textContent = 'done';
  }

  navigatefunc() {
    let flag = '0';
    sessionStorage.setItem('flagforpruchase', flag);
    this.router.navigateByUrl(AppHtmlWalletRoutes.wallet);
  }
}
