import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { ShopComponent } from './shop.component';
import { ItemDetailTopUpComponent } from './item-detail-top-up/item-detail-top-up.component';
import { ItemDetailVoucherComponent } from './item-detail-voucher/item-detail-voucher.component';
import { AuthGuard } from '../../shared/guard/auth.guard';

const routes: Routes = [
  { path: '', component: ShopComponent },
  {
    path: 'topup/:id',
    // canActivate: [AuthGuard],
    component: ItemDetailTopUpComponent
  },
  {
    path: 'voucher/:id',
    // canActivate: [AuthGuard],
    component: ItemDetailVoucherComponent
  },
  {
    path: 'voucher/:id/completed',
    component: ItemDetailVoucherComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ShopRoutingModule { }
