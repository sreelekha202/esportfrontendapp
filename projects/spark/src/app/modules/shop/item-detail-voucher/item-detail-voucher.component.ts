import { Component, Input, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ReloadComponent } from '../../reload/reload.component';
import { Subscription } from 'rxjs/Subscription';
import {
  EsportsToastService,
  EsportsUserService,
  EsportsWalletService,
  EsportsShopService,
} from 'esports';
import { AppHtmlRoutes, AppHtmlWalletRoutes } from '../../../app-routing.model';



@Component({
  selector: 'app-item-detail-voucher',
  templateUrl: './item-detail-voucher.component.html',
  styleUrls: ['./item-detail-voucher.component.scss'],
})
export class ItemDetailVoucherComponent implements OnInit {

  show = false;
  isShowVoucherForm: boolean = true;
  userSubscription: Subscription;
  walletSubscription: Subscription;
  AppHtmlRoutes = AppHtmlRoutes;
  AppHtmlWalletRoutes=AppHtmlWalletRoutes
  denom_des: boolean = false;
  @ViewChild('purchaseConfirmationModal')
  public purchaseConfirmationModal: ModalDirective;
  isDiscounted = false;
  shopItem: any = {
    UserID: '',
    ZoneID: '',
    TopUp: {
      Amount: 230,
      DisplayText: '',
    },
    UserEmail: '',
    CurrentBalance: 1000000,
  };
  list: any;
  selected: any;
  allDenominationsData: any;
  denomination_coins: any;
  denomination_name: any;
  productId: any;
  discription: any;
  denomination_code: any;
  isPopup: boolean = false;
  isError: boolean = false;
  islow: boolean = false;
  isEmailEmpty: string;
  toggleStoreContainer: boolean = false;
  isUserLoggedID: boolean = false;
  isVoucherSelected: boolean = false;
  isInsuficientBalance: boolean = true;
  isPurchaseSuccessful: boolean = false;
  isPurchaseUnSuccessful: boolean = false;
  currentBalance: number = 0;
  newBalance: number = 0;
  selectedVoucher: any = {};
  denominations: any;
  iconUrl;
  purchaseResponse: any;
  isLoaded: boolean = false;
  reactiveForm: FormGroup;
  contentHeight: number;
  showmoreHide: boolean = false;
  isComplete = false;
  roruterComplete : string;
  formInvalid = false;
  isServer = false;
  isZoneid = false;
  isUsername = false;
  isUserid = false;
  formValidMsg: any = '';
  selectedShopItem:any;
  servers: any = [];
  serverName: any;
  vouchers: any = [
    { uc: 60, gc: 140 },
    { uc: 325, gc: 700 },
    { uc: 660, gc: 1400 },
    { uc: 1800, gc: 3500 },
  ];
  reloadOptions: any = [
    { rm: 5, gc: 500 },
    { rm: 10, gc: 1000 },
    { rm: 25, gc: 2500 },
    { rm: 50, gc: 5000 },
    { rm: 75, gc: 7500 },
    { rm: 100, gc: 10000 },
    { rm: 150, gc: 15000 },
    { rm: 200, gc: 20000 },
    { rm: 250, gc: 25000 },
  ];

  @Input() shop: any = {};
  currentUser: any = {};
  productVerifyDetails: any;
  isLogin: boolean = false;
  @ViewChild('elementHeight') elementView: ElementRef<HTMLInputElement>;


  items = [
    {
      title: 'About NBA 2K22',
      description: 'About NBA 2K22, sit amet consectetur adipisicing elit. Perferendis excepturi incidunt ipsum deleniti labore, tempore non nam doloribus blanditiis veritatis illo autem iure aliquid ullam rem tenetur deserunt velit culpa?'
    },
    {
      title: 'Minimum System Requirements PC',
      description: 'Minimum System Requirements PC, sit amet consectetur adipisicing elit. Perferendis excepturi incidunt ipsum deleniti labore, tempore non nam doloribus blanditiis veritatis illo autem iure aliquid ullam rem tenetur deserunt velit culpa?'
    },
    {
      title: 'Recommend System Requirements PC',
      description: 'Recommend System Requirements PC, sit amet consectetur adipisicing elit. Perferendis excepturi incidunt ipsum deleniti labore, tempore non nam doloribus blanditiis veritatis illo autem iure aliquid ullam rem tenetur deserunt velit culpa?'
    },
  ];
  expandedIndex = 0;
  shopsList: any = [];

  constructor(
    private route: ActivatedRoute,
    public toastService: EsportsToastService,
    private walletService: EsportsWalletService,
    private shopService: EsportsShopService,
    private formBuilder: FormBuilder,
    private userService: EsportsUserService,
    private matDialog: MatDialog,
    public router: Router,
  ) {
    this.reactiveForm = this.formBuilder.group({
      UserEmail: [null, Validators.required],
      isInsuficientBalance: [null, Validators.required],
    });
    this.isUserLoggedID = true;
    this.newBalance = this.currentBalance;
    this.isInsuficientBalance = this.checkInsuficientBalance();
  }

  ngOnInit(): void {
    this.roruterComplete = `${this.router.url}/completed`
    this.isComplete = this.router.url.includes('completed')
    this.disableForm();
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.isLogin = true;
        this.disableForm();
        this.currentUser = data;
        this.reactiveForm.patchValue({ UserEmail: this.currentUser.email });
      }
    });
    this.walletService.loadWalletDetails();
    this.walletSubscription = this.walletService.recordsLoaded.subscribe(
      (data: any) => {
        if (data) {
          this.currentBalance = data.data.currentBalanceCoins;
          this.newBalance = this.currentBalance;
          this.isInsuficientBalance = this.checkInsuficientBalance();
        }
      }
    );
    this.getProductsByCategory()
  }

  disableForm() {
    for (var control in this.reactiveForm.controls) {
      !this.isLogin
        ? this.reactiveForm.controls[control].disable()
        : this.reactiveForm.controls[control].enable();
    }
  }

  ngOnDestroy() {
    if (this.userSubscription) this.userSubscription?.unsubscribe();
    if (this.walletSubscription) this.walletSubscription?.unsubscribe();
  }
  async ngAfterViewInit(): Promise<void> {
    let productId = this.route.snapshot.paramMap.get('id');
    if (this.currentUser && this.currentUser?._id) {
      this.productDetails(productId);
    } else {
      this.isLogin = false;
      if (productId) {
        this.anonymousDetails(productId);
      }
    }

    setTimeout(() => {
      if (this.elementView?.nativeElement.offsetHeight >= 63) {
        this.showmoreHide = true;
      }
    }, 1000);
  }
  anonymousDetails(productId) {
    this.isLogin = false;
    this.shopService.anonymousDetails(productId).subscribe(
      (data) => {
        this.isLoaded = true;
        this.setProductDetails(data);
      },
      (err) => {
        this.isLoaded = true;
      }
    );
  }
  setProductDetails(res: any) {
    this.isLoaded = true;
    this.allDenominationsData = res.data;
    this.iconUrl = res.data.iconUrl;
    this.denominations = this.allDenominationsData.denominations;
    if (
      this.allDenominationsData.description != '' &&
      this.allDenominationsData.description != undefined
    ) {
      this.denom_des = true;
    }
  }

  checkInsuficientBalance(): boolean {
    return this.newBalance < -1;
  }

  voucherSelect(voucher): void {

    this.isVoucherSelected = true;
    this.selectedVoucher = voucher;
    this.productId = voucher.sku;
    if (voucher.isDiscounted == true) {
      this.isDiscounted = true;
    } else {
      this.isDiscounted = false;
    }

    this.denomination_coins = voucher.denomination_coins;
    this.denomination_code = voucher.denomination_code;
    this.denomination_name = voucher.denomination_name;

    this.newBalance =
      this.currentBalance -
      (voucher?.isDiscounted
        ? voucher.discountedPriceCoins
        : voucher.denomination_coins);
    this.isInsuficientBalance = this.checkInsuficientBalance();
    this.reactiveForm.patchValue({
      isInsuficientBalance: this.isInsuficientBalance ? null : true,
    });
    this.selected = voucher;
  }


  getProductValidateUser() {
    this.setServerName();
    if (
      this.reactiveForm?.get('userid')?.valid &&
      this.reactiveForm?.get('zoneid')?.valid &&
      this.reactiveForm?.get('serverid')?.valid &&
      this.reactiveForm?.get('username')?.valid
    ) {
      let validateJson: any = {
        sku: this.productId,
        denominationCode: this.denomination_code
      };
      this.isUserid
        ? (validateJson['userid'] = this.reactiveForm.value.userid)
        : '';
      this.isZoneid
        ? (validateJson['zoneid'] = this.reactiveForm.value.zoneid)
        : '';
      this.isServer
        ? (validateJson['server'] = this.reactiveForm.value.serverid)
        : '';
      this.isUsername
        ? (validateJson['username'] = this.reactiveForm.value.username)
        : '';
      this.shopService.getProductValidateUser(validateJson).subscribe(
        (res: any) => {
          if (res.success) {
            // this.reactiveForm.setErrors(null);
            this.formInvalid = false;
          } else {
            this.reactiveForm.setErrors({ userid: true });
            this.formValidMsg = res.message;
            this.formInvalid = true;
          }
        },
        (err) => {
          this.formInvalid = true;
          this.reactiveForm.setErrors({ userid: true });
          this.formValidMsg = err.error.message;
        }
      );
    }
  }
  setServerName() {
    if (this.isServer) {
      this.servers.map((server) => {
        server.value == this.reactiveForm.value.serverid
          ? (this.serverName = server?.name)
          : '';
      });
    }
  }





  isActive(item) {
    return this.selected === item;
  }
  confirmPurchase(shopItem): void {
    // this.shopItem.UserID = this.allDenominationsData.vendorId._id;
    this.shopItem.TopUp.DisplayText = this.denomination_name;
    this.shopItem.TopUp.Amount = this.denomination_coins;
    this.shopItem.UserEmail = this.reactiveForm.value.UserEmail;

    this.selectedShopItem=shopItem
    this.isPopup = false;
    this.islow = true;
    //show be upadate when data get response
    this.show = !this.show;
    // let data = {
    //   sku: this.productId,
    //   denominationCode: this.denomination_code,
    //   voucherEmail: shopItem?.UserEmail,
    //   ruleCategory: this.selectedVoucher?.ruleCategory,
    //   ruleDetailId: this.selectedVoucher?.ruleDetailId,
    //   ruleId: this.selectedVoucher?.ruleId,
    // };
    // this.shopService.confirmPurchase(data, {}).subscribe(
    //   (res: any) => {
    //     this.purchaseResponse = res.data;
    //     console.log(" this.purchaseResponse", this.purchaseResponse)
    //     this.isPurchaseSuccessful = true;
    //     this.isShowVoucherForm = false;

    //     //this.walletService.loadWalletDetails();
    //   },
    //   (error) => {
    //     if (error.error && error.error.error && error.error.error.message)
    //       this.toastService.showError(error.error.error.message);
    //     else this.toastService.showError(error.error.message);
    //     this.isPurchaseUnSuccessful = true;
    //     this.isShowVoucherForm = false;
    //   }
    // );
  }


  confirmPurchase2(shopItem){
    this.show = !this.show;
    let data = {
      sku: this.productId,
      denominationCode: this.denomination_code,
      voucherEmail: shopItem?.UserEmail,
      ruleCategory: this.selectedVoucher?.ruleCategory,
      ruleDetailId: this.selectedVoucher?.ruleDetailId,
      ruleId: this.selectedVoucher?.ruleId,
    };
    this.shopService.confirmPurchase(data, {}).subscribe(
      (res: any) => {
        this.purchaseResponse = res.data;
        this.isPurchaseSuccessful = true;
        this.isShowVoucherForm = false;

        //this.walletService.loadWalletDetails();
      },
      (error) => {
        if (error.error && error.error.error && error.error.error.message)
          this.toastService.showError(error.error.error.message);
        else this.toastService.showError(error.error.message);
        this.isPurchaseUnSuccessful = true;
        this.isShowVoucherForm = false;
      }
    );
  }

  openLoginDialog(): void {
    if (!this.isLogin) {
      // use lib popup for login
    }
  }
  toggleDisplayStore(): void {
    this.toggleStoreContainer = !this.toggleStoreContainer;
  }

  productDetails(productId) {
    this.productId = productId;
    this.shopService.productDetails(productId).subscribe(
      (res: any) => {
        this.setProductDetails(res);
      },
      (err) => {
        this.isLoaded = true;
      }
    );
  }

  // byNow() {
  //   this.isLoaded = false;
  //   this.isPopup = true;

  //   let data = {
  //     sku: this.productId,
  //     denominationCode: this.denomination_code,
  //     voucherEmail: this.reactiveForm.value.UserEmail,
  //     ruleCategory: this.selectedVoucher?.ruleCategory,
  //     ruleDetailId: this.selectedVoucher?.ruleDetailId,
  //     ruleId: this.selectedVoucher?.ruleId,
  //   };
  //   this.shopService.confirmPurchase(data, { verify: true }).subscribe(
  //     (res: any) => {
  //       this.productVerifyDetails = res;
  //       this.isLoaded = true;
  //     },
  //     (err) => {
  //       this.isLoaded = true;
  //     }
  //   );
  //  // this.shopItem.UserID = this.allDenominationsData.vendorId._id;
  //   this.shopItem.TopUp.DisplayText = this.denomination_name;
  //   this.shopItem.TopUp.Amount = this.denomination_coins;
  //   this.shopItem.UserEmail = this.reactiveForm.value.UserEmail;
  // }

  showReloadPopup(): void {
    this.matDialog.open(ReloadComponent, {
      disableClose: true,
      panelClass: 'reload_popup',
    });
  }
  onClose(): void {
    this.matDialog.openDialogs[this.matDialog.openDialogs.length - 1].close();
  }

  checkPaymentCompleted() {
    return this.router.url === "/store/voucher-purchase/payment-completed"
  }
  showPopup($event) {
    this.show = true;
  }
  hidePopup($event) {
    this.show = false;
  }

  getProductsByCategory() {
      this.shopService.getProductsByCategory().subscribe(
        (res) => {
          this.shopsList = this.shopsList.concat(res.data.docs);
          // optimize by just query 3 element of your store' shop
          this.shopsList = this.shopsList.slice(0, 3)
        },
        (err) => {
        }
      );
  }
}
