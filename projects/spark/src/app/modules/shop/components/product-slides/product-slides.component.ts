import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { SwiperComponent, SwiperConfigInterface } from 'ngx-swiper-wrapper';
import { EsportsGtmService, EsportsUserService,EventProperties,SuperProperties } from 'esports';
import { Subscription } from 'rxjs';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';

@AutoUnsubscribe()
@Component({
  selector: 'app-product-slides',
  templateUrl: './product-slides.component.html',
  styleUrls: ['./product-slides.component.scss'],
})
export class ProductSlidesComponent implements OnInit {
  @Input() item: any;
  selectedGameIndex = 0;
  userSubscription: Subscription;
  currentUser: any;
  @ViewChild(SwiperComponent, { static: false }) compRef?: SwiperComponent;
  public config: SwiperConfigInterface = {
    observer: true,
    observeParents: true,
    autoHeight: true,
    setWrapperSize: true,
    direction: 'horizontal',
    breakpoints: {
      320: {
        slidesPerView: 2.4,
        spaceBetween: 16,
        autoHeight: false,
      },
      480: {
        slidesPerView: 2.4,
        spaceBetween: 16,
        autoHeight: true,
      },
      640: {
        slidesPerView: 3.4,
        spaceBetween: 16,
        autoHeight: true,
      },
      800: {
        slidesPerView: 4.4,
        spaceBetween: 16,
        autoHeight: true,
      },
      992: {
        slidesPerView: 4.4,
        spaceBetween: 16,
        autoHeight: true,
      },
      1280: {
        slidesPerView: 5.3,
        spaceBetween: 16,
        autoHeight: true,
      },
    },
  };

  constructor(
    private router: Router,
    private userService: EsportsUserService,
    public gtmService: EsportsGtmService
  ) {}
  ngOnInit(): void {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.currentUser = data;
      }
    });
  }

  prevSlide() {
    this.compRef.directiveRef.prevSlide();
  }
  nextSlide() {
    this.compRef.directiveRef.nextSlide();
  }
  // redirectToupOrVoucher(type: any, id) {
  //   // if (this.currentUser) {
  //   if (type == 'VOUCHER') this.router.navigateByUrl('/home/voucher/' + id);
  //   if (type == 'TOPUP') this.router.navigateByUrl('/home/topup/' + id);
  //   // } else {
  //   //   this.toastService.showInfo(
  //   //     this.translateService.instant('BASIC_INFO_POPUP.PLEASE_LOGIN')
  //   //   );
  //   // }
  // }

  redirectToupOrVoucher(type: any, id, categoryName: any) {
    if (type == 'VOUCHER') this.router.navigateByUrl('/shop/voucher/' + id);
    if (type == 'TOPUP') this.router.navigateByUrl('/shop/topup/' + id);
    let eventProperties: EventProperties = {};
    eventProperties['gameTitle'] = categoryName;
    eventProperties['purchaseType'] = type;
    this.pushGTMTags('game_card_click', eventProperties)
}

pushGTMTags(eventName: string, eventProperties: any) {
  let superProperties: SuperProperties = {};
  if (this.currentUser) {
    superProperties = this.gtmService.assignLoggedInUsedData(
      this.currentUser
    );
  }
  this.gtmService.gtmEventWithSuperProp({
    eventName: eventName,
    superProps: superProperties,
    eventProps: eventProperties
  });
}

  ngOnDestroy(): void {  }
}
