import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShopComponent } from './shop.component';
import { SharedModule } from '../../shared/modules/shared.module';
import { MatExpansionModule } from '@angular/material/expansion';
import { ShopRoutingModule } from './shop-routing.module';
import { ViewShopItemComponent } from './view-shop-item/view-shop-item.component';
import { ShopCardComponent } from './shop-card/shop-card.component';
import { ShopTopupCardComponent } from './shop-topup-card/shop-topup-card.component';
import { PurchaseSuccessfulComponent } from './purchase-successful/purchase-successful.component';
import { PurchaseUnSuccessfulComponent } from './purchase-un-successful/purchase-un-successful.component';
import { StoreContainerComponent } from './store-container/store-container.component';
import { ItemDetailTopUpComponent } from './item-detail-top-up/item-detail-top-up.component';
import { ItemDetailVoucherComponent } from './item-detail-voucher/item-detail-voucher.component';
import { VoucherPurchaseSuccessfulComponent } from './voucher-purchase-successful/voucher-purchase-successful.component';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { ReloadComponent } from '../reload/reload.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { ProductSlidesComponent } from './components/product-slides/product-slides.component';
import { CoreModule } from '../../core/core.module';
import { CountdownModule } from 'ngx-countdown';
import { EsportsLoaderModule } from 'esports';
import { FooterSparkModule } from '../../shared/components/footer-spark/footer-spark.module';
import { CdkAccordionModule } from '@angular/cdk/accordion';
@NgModule({
  declarations: [
    ShopComponent,
    ViewShopItemComponent,
    ShopCardComponent,
    ShopTopupCardComponent,
    PurchaseSuccessfulComponent,
    PurchaseUnSuccessfulComponent,
    StoreContainerComponent,
    ItemDetailTopUpComponent,
    ItemDetailVoucherComponent,
    VoucherPurchaseSuccessfulComponent,
    ReloadComponent,
    ProductSlidesComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    CoreModule,
    FormsModule,
    ReactiveFormsModule,
    MatExpansionModule,
    SlickCarouselModule,
    ShopRoutingModule,
    InfiniteScrollModule,
    EsportsLoaderModule.setColor('#00a851'),
    CountdownModule,
    FooterSparkModule,
    CdkAccordionModule
  ],
})
export class ShopModule { }
