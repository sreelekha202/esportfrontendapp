import { Component, Inject, OnInit, PLATFORM_ID } from '@angular/core';
import {
  EsportsHomeService,
  EsportsToastService,
  EsportsShopService,
  EsportsWalletService,
} from 'esports';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
// import { environment } from '../../../environments/environment';
import { isPlatformBrowser } from '@angular/common';
// const TOKEN = environment.currentToken;
// const API = environment.apiEndPoint;
@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.scss'],
})
export class ShopComponent implements OnInit {
  shop_slider = {
    slidesToShow: 1,
    slidesToScroll: 1,
    centerMode: true,
    dots: true,
    autoplay: true,
    autoplaySpeed: 3000,
    variableWidth: true,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true,
          dots: true,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  };
  title = [
    {
      name: 'All (16)',
      isActive: true
    },
    {
      name: 'Trending now',
      isActive: false
    },
    {
      name: 'Most popular',
      isActive: false
    },
  ]
  Categories = [
    'All types',
    'Direct top-ups',
    'Vouchers'
  ]
  shopsList: any = [];
  categoryList: any = [];
   totalShopList:any
  isShowVideo: boolean = false;
  isShowVouchers = false;
  isAllGames = true;
  isDirectPopUp = false;
  isVouchers = false;
  showLoader: boolean = false;
  selectedSortBy: any = '';
  sortOrderArray: any = [
    { label: 'Featured', value: 0 },
    { label: 'A-Z', value: 1 },
    { label: 'Z-A', value: -1 },
  ];
  sortOrderType = this.sortOrderArray[0]['value'];
  searchText: string = '';
  paginationDetails: any;
  param: any = {
    type: '',
    page: 1,
    pageSize: 20,
     //sortBy: 'categoryName',
     sortBy: 'featuredListOrder',
    sortOrder: this.sortOrderType,
    search: this.searchText,
  };
  slides: any;
  isBrowser: boolean;
  isSpinner: boolean;
  constructor(
    @Inject(PLATFORM_ID) private platformId,
    private homeService: EsportsHomeService,
    public toastService: EsportsToastService,
    public matDialog: MatDialog,
    private shopService: EsportsShopService,
    private walletService: EsportsWalletService,
    private router: Router
  ) {
    this.isBrowser = isPlatformBrowser(platformId);
  }
  ngOnInit(): void {
    this.walletService.loadWalletDetails();
    this.showLoader = true;
    if (this.isBrowser) {
      this.showAllGames();
      this.getProductCategoryGroups();
      this.homeService._getBanner().subscribe(
        (res) => {
          this.showLoader = false;
          this.slides = res.data;
        },
        (err) => {
          this.showLoader = false;
        }
      );
    }
  }

  getProductCategoryGroups() {
    this.showLoader = true;
    this.shopService.getProductCategoryGroups().subscribe(
      (res) => {
        this.showLoader = false;
        this.categoryList = res.data;
      },
      (err) => {
        this.showLoader = false;
      }
    );
  }

  onFilter = (item) => {
    this.shopsList = [];
    this.param.page = 1;
    !this.isSpinner ? this.getProductsByCategory() : '';
    this.selectedSortBy = item;
  };

  getProductsByCategory() {
    this.isSpinner = true;
    setTimeout(() => {
      this.param.search = this.searchText;
      this.param.sortOrder = this.sortOrderType;
      this.sortOrderType == 0
        ? delete this.param.sortOrder
        : this.sortOrderType;
      this.shopService.getProductsByCategory(this.param).subscribe(
        (res) => {
          this.isSpinner = false;
          this.showLoader = false;
          this.shopsList = this.shopsList.concat(res.data.docs);
          this.totalShopList=this.shopsList.length
          this.paginationDetails = res.data;
        },
        (err) => {
          this.isSpinner = false;
          this.showLoader = false;
        }
      );
    }, 500);
  }

  showArticles() {
    this.isShowVideo = false;
    this.isShowVouchers = false;
  }

  showVideos() {
    this.isShowVideo = true;
  }

  showAllGames() {
    this.isAllGames = true;
    this.isDirectPopUp = false;
    this.isVouchers = false;
    this.showLoader = true;
    this.param.type = '';
    this.shopsList = [];
    this.param.page = 1;
    this.getProductsByCategory();
  }
  //
  showDirectPopUp() {
    this.isAllGames = false;
    this.isDirectPopUp = true;
    this.isVouchers = false;
    this.showLoader = true;
    this.param.type = 'topup';
    this.shopsList = [];
    this.param.page = 1;
    this.getProductsByCategory();
  }

  showVouchers() {
    this.isAllGames = false;
    this.isDirectPopUp = false;
    this.isVouchers = true;
    this.showLoader = true;
    this.param.type = 'voucher';
    this.shopsList = [];
    this.param.page = 1;
    this.getProductsByCategory();
  }

  pageChanged(page): void {
    if (this.paginationDetails?.totalDocs > 0) {
      this.param.page = page.pageIndex + 1;
      this.param.pageSize = page.pageSize;
      this.getProductsByCategory();
    }
  }

  onScroll() {
    if (this.paginationDetails?.hasNextPage) {
      this.isSpinner = true;
      this.param.page++;
      this.getProductsByCategory();
    }
  }

  redirectToupOrVoucher(category: any, id) {
    if (category == 'VOUCHER') this.router.navigateByUrl('/shop/voucher/' + id);
    if (category == 'TOPUP') this.router.navigateByUrl('/shop/topup/' + id);
  }
  ngOnDestroy() { }
}
