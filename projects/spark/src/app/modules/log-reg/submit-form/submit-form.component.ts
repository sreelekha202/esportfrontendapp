import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { faFacebookF, faTwitter } from '@fortawesome/free-brands-svg-icons';
import { faEnvelope } from '@fortawesome/free-solid-svg-icons';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import {
  AppHtmlRoutes,
  AppHtmlRoutesLoginType,
  AppHtmlProfileRoutes,
} from '../../../app-routing.model';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { EsportsToastService, GlobalUtils, EsportsAuthServices } from 'esports';

export interface SubmitFormComponentOutputData {
  [key: string]: string;
}

export interface SubmitFormComponentDataItem {
  title: string;
  subtitle: string;
  checkboxLabel: string;
  checkboxLabelpp: string;
  // checkboxLabelpp2: string;
  //checkboxLabelP :string;

  submitBtn: string;
  or: string;
  orLabel: string;
  footerText: string;
  footerLink: string;
  footerLinkUrl: string[];
  firstInputName: string;
  firstInputType: string;
  firstInputPlaceholder: string;
  firstInputIcon: string;
  fifthInputName: string;
  fifthInputType: string;
  fifthInputPlaceholder: string;
  fifthInputIcon: string;
  secondInputName: string;
  secondInputType: string;
  secondInputPlaceholder: string;
  secondInputIcon: string;
  fourthInputName: string;
  fourthInputType: string;
  fourthInputPlaceholder: string;
  fourthInputIcon: string;

  forgotPass?: string;
  error?: string;
  // event:any;
}

@Component({
  selector: 'app-submit-form',
  templateUrl: './submit-form.component.html',
  styleUrls: ['./submit-form.component.scss'],
})
export class SubmitFormComponent implements OnInit {
  @Output() dateChange: EventEmitter<MatDatepickerInputEvent<any>>;
  @Output() submit = new EventEmitter();
  @Input() data: SubmitFormComponentDataItem;
  timeoutId = null;
  show = false;
  regType: string = 'email';
  @Input() set type(type: AppHtmlRoutesLoginType) {
    this.activeTypeValue = type;

    if (this.activeTypeValue === AppHtmlRoutesLoginType.registration) {
      this.form
        .get('email')
        .setValidators([
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(100),
          Validators.email,
        ]);
      this.form.get('firstNameInput').setValidators([Validators.required]);
      this.form.get('lastNameInput').setValidators([Validators.required]);
      // this.form
      //   .get('secondInput')
      //   .setValidators([
      //     Validators.required,
      //     Validators.pattern(
      //       /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[!@#$%^&*()_+-={}|'])[A-Za-z\d!@#$%^&*()_+-={}|']{8,}$/
      //     ),
      //   ]);
      this.form.get('privacyPolicy').setValidators(Validators.pattern('true'));
      this.form.get('username').setValidators([Validators.required]);
      this.form.get('dobPolicy').setValidators(Validators.pattern('false'));
      this.form.get('dobPolicy').updateValueAndValidity();
    }
    if (this.activeTypeValue === AppHtmlRoutesLoginType.emailLogin) {
      this.form
        .get('email')
        .setValidators([Validators.required, Validators.email]);
      this.form
        .get('secondInput')
        .setValidators([
          Validators.required,
          Validators.pattern(
            /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[!@#$%^&*()_+-={}|'])[A-Za-z\d!@#$%^&*()_+-={}|']{8,}$/
          ),
        ]);
      this.form.get('dobPolicy').updateValueAndValidity();
    }
  }

  AppHtmlRoutes = AppHtmlRoutes;
  AppHtmlRoutesLoginType = AppHtmlRoutesLoginType;
  AppHtmlProfileRoutes = AppHtmlProfileRoutes;
  activeTypeValue: AppHtmlRoutesLoginType;

  form = this.formBuilder.group({
    firstNameInput: [''],
    lastNameInput: [''],
    email: ['', Validators.required],
    secondInput: [''],
    fourthInput: [''],
    fifthInput: [''],
    username:[''],
    keepMeLoggedIn: [false],
    privacyPolicy: [false],
    dobPolicy: [false],
  });

  todayDate: Date = new Date(
    new Date().setFullYear(new Date().getFullYear() - 10)
  );
  parentalDate: Date = new Date(
    new Date().setFullYear(new Date().getFullYear() - 18)
  );
  parental = false;
  isExist = false;

  faFacebookF = faFacebookF;
  faTwitter = faTwitter;
  faEnvelope = faEnvelope;

  constructor(
    private formBuilder: FormBuilder,
    public router: Router,
    public toastService: EsportsToastService,
    private authServices: EsportsAuthServices
  ) {
    this.checkForKeepMeLoggedIn();
  }

  ngOnInit() {
    // if (this.activeTypeValue === this.AppHtmlRoutesLoginType.registration) {
    //   this.toastService.showError(
    //     'You need to be at least 10 years old to register on this platform.'
    //   );
    // }
    if (this.activeTypeValue == AppHtmlRoutesLoginType.registration) {
      this.authServices.formData.subscribe((data) => {
        if (data && data.regType) {
          this.updateRegType(data.regType);
          this.form.patchValue(data);
          if (data.fourthInput) this.parentalCheckBox(data.fourthInput);
        }
      });
    }
    this.isLogginPage();
  }
  isUniqueName = async (name) => {
    try {
      const isAvailable = async () => {
        try {
          const response = await this.authServices
            .searchUsername(name)
            .toPromise();
          this.isExist = response.data?.isExist;
          if (this.isExist) {
            this.form.controls['username'].setErrors({
              incorrect: true,
            });
          } else {
            this.form.controls['username'].setErrors(null);
            this.form.controls['username'].setValidators([
              Validators.required,
              Validators.minLength(3),
              Validators.pattern(/^[a-zA-Z0-9_]+$/),
            ]);
            this.form.controls['username'].updateValueAndValidity();
          }
        } catch (error) {
          throw error;
        } finally {
          clearTimeout(this.timeoutId);
        }
      };
      if (!this.timeoutId) {
        this.timeoutId = setTimeout(isAvailable, 500);
      } else {
        clearTimeout(this.timeoutId);
        this.timeoutId = setTimeout(isAvailable, 500);
      }
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  onCountryChange() {
    this.onKeepLoggedIn();
  }

  password() {
    this.show = !this.show;
  }
  parentalCheckBox(date: any) {
    const selectedDate = new Date(date);
    if (selectedDate > this.parentalDate && selectedDate < this.todayDate) {
      this.parental = true;
      this.form.get('dobPolicy').setValidators(Validators.pattern('true'));
      this.form.get('dobPolicy').setValue(true);
    } else {
      this.parental = false;
      this.form.get('dobPolicy').setValidators(Validators.pattern('false'));
      this.form.get('dobPolicy').setValue(false);
    }

    this.form.get('dobPolicy').updateValueAndValidity();
  }

  onKeepLoggedIn() {
    if (GlobalUtils.isBrowser()) {
      if (this.form.get('privacyPolicy').value) {
        const keepMeLoggedIn = {
          keepMeLoggedIn: true,
        };
        localStorage.setItem('keepMeLoggedIn', JSON.stringify(keepMeLoggedIn));
      } else {
        localStorage.removeItem('keepMeLoggedIn');
      }
    }
  }

  onKeepdob(date: any) {}

  onSubmitEvent(): void {
    this.submit.emit({
      [this.data.firstInputName]: this.form.value.email,
      [this.data.secondInputName]: this.form.value.secondInput,
      [this.data.fourthInputName]: this.form.value.fourthInput,
      [this.data.fifthInputName]: this.form.value.fifthInput,
      firstInputType: this.data.firstInputName,
     // secondInputType: this.data.secondInputName,
      fourthInputType: this.data.fourthInputName,
      fifthInputType: this.data.fifthInputName,
      data: this.form.value,
      regType: this.regType,
    });
  // this.router.navigate([AppHtmlProfileRoutes.dashboard]);
  }

  changeInput(input): void {
    if (input === 'phone') {
      this.data.secondInputName = 'email';
      this.data.secondInputType = 'email';
      this.data.secondInputPlaceholder = 'LOG_REG.LOGIN.firstInputPlaceholder1';
      this.data.secondInputIcon = 'email';
      // this.form
      //   .get('secondInput')
      //   .setValidators([Validators.required, Validators.email]);
      // this.form.get('secondInput').reset();
    } else {
      this.data.secondInputName = 'phone';
      this.data.secondInputType = 'phone';
      this.data.secondInputPlaceholder = 'LOG_REG.LOGIN.firstInputPlaceholder';
      this.data.secondInputIcon = 'phone';
      // this.form.get('secondInput').reset();
      // this.form.get('secondInput').setValidators([Validators.required]);
    }
  }

  changeLoginType(type) {
    if (type === 'email') {
      this.router
        .navigateByUrl('/home', { skipLocationChange: true })
        .then(() => {
          this.router.navigate(['/user', AppHtmlRoutesLoginType.emailLogin]);
        });
    }
    if (type === 'phone') {
      this.router
        .navigateByUrl('/home', { skipLocationChange: true })
        .then(() => {
          this.router.navigate(['/user', AppHtmlRoutesLoginType.phoneLogin]);
        });
    }
  }

  forgot() {
    this.router
      .navigateByUrl('/home', { skipLocationChange: true })
      .then(() => {
        this.router.navigate(['/user', AppHtmlRoutesLoginType.forgotPass]);
      });
  }

  private checkForKeepMeLoggedIn(): void {
    if (GlobalUtils.isBrowser()) {
      const keepMeLoggedIn = JSON.parse(localStorage.getItem('keepMeLoggedIn'));
      if (keepMeLoggedIn) {
        this.form.get('privacyPolicy').setValue(keepMeLoggedIn.keepMeLoggedIn);
      }
    }
  }

  isLogginPage(): boolean {
    return this.router.url === '/user/email-login';
  }
  updateRegType(type) {
    this.regType = type;
    if (type === 'email') {
      this.data.secondInputName = 'email';
      this.data.secondInputType = 'email';
      this.data.secondInputPlaceholder = 'LOG_REG.LOGIN.firstInputPlaceholder1';
      // this.form
      //   .get('secondInput')
      //   .setValidators([Validators.required, Validators.email]);

      // this.form.get('secondInput').reset();
      // this.form.get('secondInput').updateValueAndValidity();
    } else {
      this.data.secondInputName = 'phone';
      this.data.secondInputType = 'phone';
      this.data.secondInputPlaceholder = 'LOG_REG.LOGIN.firstInputPlaceholder';
      // this.form.get('secondInput').reset();
      // this.form.get('secondInput').clearValidators();
      // this.form.get('secondInput').updateValueAndValidity();
      // this.form
      //   .get('secondInput')
      //   .setValidators([Validators.required, Validators.required]);
    }
  }

  goto(type: number) {
    this.authServices.formDataSubject.next({
      ...this.form.value,
      regType: this.regType,
    });
    if (type == 1) this.router.navigateByUrl(AppHtmlRoutes.terms);
    else if (type == 2) this.router.navigateByUrl(AppHtmlRoutes.privacyPolicy);
  }
}
