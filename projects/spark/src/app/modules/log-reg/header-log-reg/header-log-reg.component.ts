import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppHtmlRoutes } from '../../../app-routing.model';

@Component({
  selector: 'app-header-log-reg',
  templateUrl: './header-log-reg.component.html',
  styleUrls: ['./header-log-reg.component.scss'],
})
export class HeaderLogRegComponent implements OnInit {
  checkReg = false;
  AppHtmlRoutes = AppHtmlRoutes;
  constructor(private router: Router) { }

  ngOnInit(): void {
    this.checkRoute();
    this.router.events.subscribe((val) => {
      this.checkRoute();
    });
  }
  goToLandingPage() {
    this.router.navigateByUrl('/play');
  }
  checkRoute() {
    if (this.router.url == '/user/email-login') {
      this.checkReg = false;
    } else {
      this.checkReg = true;
    }
  }
}
