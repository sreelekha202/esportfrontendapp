import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderLogRegComponent } from './header-log-reg.component';

describe('HeaderLogRegComponent', () => {
  let component: HeaderLogRegComponent;
  let fixture: ComponentFixture<HeaderLogRegComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HeaderLogRegComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderLogRegComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
