import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
// import { CustomValidators } from "ngx-custom-validators";
import { MYCustomValidators } from "./custom-validators";

import { SubmitFormComponentOutputData } from "../submit-form/submit-form.component";

export interface UpdateFormComponentDataItem {
  title: string;
  submitBtn: string;
  firstInputType: string;
  firstInputPlaceholder: string;
  firstInputError: string;
  secondInputType: string;
  secondInputPlaceholder: string;
  secondInputError: string;

  subtitle?: string;
  error?: string;

}

@Component({
  selector: "app-password-form",
  templateUrl: "./password-form.component.html",
  styleUrls: ["./password-form.component.scss"],
})
export class PasswordFormComponent implements OnInit {
  @Output() submit = new EventEmitter();
  @Input() formValue: SubmitFormComponentOutputData;
  @Input() data: UpdateFormComponentDataItem;


  password = new FormControl("", [
    Validators.required,
    // check whether the entered password has a number
    MYCustomValidators.patternValidator(/\d/, { hasNumber: true }),
    // check whether the entered password has upper case letter
    MYCustomValidators.patternValidator(/[A-Z]/, { hasCapitalCase: true }),
    // check whether the entered password has a lower case letter
    MYCustomValidators.patternValidator(/[a-z]/, { hasSmallCase: true }),
    // check whether the entered password has a special character
    MYCustomValidators.patternValidator(
      /[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/,
      { hasSpecialCharacters: true }
    ),
    Validators.minLength(8),
  ]);
   certainPassword = new FormControl(
     "",
    //  CustomValidators.equalTo(this.password)

    // [
    //  Validators.required,

    // ]
  );

  form = new FormGroup({
    password: this.password,
    certainPassword: this.certainPassword,

  },
  { validators: MYCustomValidators.passwordMatchValidator }
  );
  formGroup: any;

  constructor() {}

  ngOnInit() {
    this.password.markAsTouched();
  }



  onVisibilityToggle(el: HTMLInputElement): void {
    el.type = el.type === "text" ? "password" : "text";
  }

  onSubmitEvent(): void {
    if(this.form.valid){
      this.submit.emit({
        formValue: this.formValue,
        password: this.form.value.password,
      });
    }
  }
}
