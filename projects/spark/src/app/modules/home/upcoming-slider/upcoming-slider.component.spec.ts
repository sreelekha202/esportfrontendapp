import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpcomingSliderComponent } from './upcoming-slider.component';

describe('UpcomingSliderComponent', () => {
  let component: UpcomingSliderComponent;
  let fixture: ComponentFixture<UpcomingSliderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UpcomingSliderComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpcomingSliderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
