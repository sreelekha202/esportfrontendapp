import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PreviewSliderComponent } from './preview-slider.component';

describe('PreviewSliderComponent', () => {
  let component: PreviewSliderComponent;
  let fixture: ComponentFixture<PreviewSliderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PreviewSliderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PreviewSliderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
