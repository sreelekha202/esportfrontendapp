import { NgModule } from '@angular/core';
import { HomeComponent } from './home.component';
import { HomeRoutingModule } from './home-routing.module';
import { PreviewSliderComponent } from './preview-slider/preview-slider.component';
import { UpcomingSliderComponent } from './upcoming-slider/upcoming-slider.component';
import { UserAsideComponent } from './user-aside/user-aside.component';
import { UserLevelComponent } from './user-level/user-level.component';
import { SharedModule } from '../../shared/modules/shared.module';
import { TournamentBannerComponent } from './tournament-banner/tournament-banner.component';
import { CoreModule } from '../../core/core.module';
import { SpotlightNewsComponent } from './spotlight-news/spotlight-news.component';
import { FeaturedComponent } from './featured/featured.component';
import { shopDisplayComponent } from './shop-display/shop-display.component';
import { CdkAccordionModule } from '@angular/cdk/accordion';
import { FAQComponent } from './faq/faq.component';
import { FooterSparkModule } from '../../shared/components/footer-spark/footer-spark.module';
import { LatestNewsComponent } from './latest-news/latest-news.component';

const components = [
  HomeComponent,
  PreviewSliderComponent,
  UpcomingSliderComponent,
  UserAsideComponent,
  UserLevelComponent,
  TournamentBannerComponent,
  SpotlightNewsComponent,
  FeaturedComponent,
  shopDisplayComponent,
  FAQComponent,
  LatestNewsComponent
];

const modules = [CoreModule,SharedModule, HomeRoutingModule,CdkAccordionModule, FooterSparkModule];

@NgModule({
  declarations: [...components],
  imports: [...modules],
})
export class HomeModule { }
