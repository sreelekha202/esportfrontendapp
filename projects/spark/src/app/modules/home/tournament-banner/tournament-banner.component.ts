import { Component, Input, OnInit } from '@angular/core';
import { AppHtmlRoutes } from '../../../app-routing.model';
@Component({
  selector: 'app-tournament-banner',
  templateUrl: './tournament-banner.component.html',
  styleUrls: ['./tournament-banner.component.scss'],
})
export class TournamentBannerComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;
  @Input() params
  constructor() {}

  ngOnInit(): void {
  }
  onselectedGame(gameId){
    localStorage.setItem('gameId', gameId)
  }

}
