import { TranslateService } from '@ngx-translate/core';
import { Component, OnInit, Input } from '@angular/core';
import { AppHtmlRoutes } from '../../../app-routing.model';
import {InfoPopupComponent,InfoPopupComponentData,InfoPopupComponentType} from '../../../shared/popups/info-popup/info-popup.component';
import { MatDialog } from '@angular/material/dialog';
import { EsportsTimezone, GlobalUtils } from 'esports';

@Component({
  selector: 'app-tournaments-card',
  templateUrl: './tournaments-card.component.html',
  styleUrls: ['./tournaments-card.component.scss'],
})
export class TournamentsCardComponent implements OnInit {
  @Input() params;
  @Input() type;
  @Input() dataList;
  @Input() ongoingTournament;
   timezone;
   AppHtmlRoutes = AppHtmlRoutes;
    ongoing
  constructor(
    public matDialog: MatDialog,
    public translateService: TranslateService,
    private esportsTimezone: EsportsTimezone
  ) {}

  ngOnInit(): void {
    this.timezone = this.esportsTimezone.getTimeZoneName();
      if(this.ongoingTournament){
              this.ongoing=this.ongoingTournament
         }
  }

  isNumber(val): boolean {
    const str: string = val.toString();
    return str.startsWith('+');
  }

  onselectedGame(gameId){
    localStorage.setItem('gameId', gameId)
  }

  getRegistrationStatus(
    startDate,
    startTime,
    isSeeded,
    isFinished,
    maxParticipants,
    participantsJoined
  ) {
    if (!startDate || !startTime) {
      return;
    }
    let timeObj = this.convert12HrsTo24HrsFormat(startTime);
    let sDate = new Date(startDate);
    sDate.setHours(timeObj['hour']);
    sDate.setMinutes(timeObj['minute']);
    if (isSeeded == true || isFinished == true) {
      return 'Closed';
    } else if (participantsJoined >= maxParticipants) {
      return 'Closed';
    } else if (sDate.getTime() < Date.now()) {
      return 'Closed';
    } else {
      return 'Open';
    }
  }

  convert12HrsTo24HrsFormat(time) {
    if (!time) {
      return;
    }
    var hours = Number(time.match(/^(\d+)/)[1]);
    var minutes = Number(time.match(/:(\d+)/)[1]);
    var AP = time.match(/\s(.*)$/);
    if (!AP) AP = time.slice(-2);
    else AP = AP[1];
    if (AP == 'PM' && hours < 12) hours = hours + 12;
    if (AP == 'AM' && hours == 12) hours = hours - 12;
    var Hours24 = hours.toString();
    var Minutes24 = minutes.toString();
    if (hours < 10) Hours24 = '0' + Hours24;
    if (minutes < 10) Minutes24 = '0' + Minutes24;

    return { hour: parseInt(Hours24), minute: parseInt(Minutes24) };
  }

  openSocialShareComponent() {
    const data: InfoPopupComponentData = {
      title: this.translateService.instant('ARTICLE_POST.SHARE_TOURNAMENT'),
      text: ``,
      type: InfoPopupComponentType.socialSharing,
      cancelBtnText: 'Close',
    };
    this.matDialog.open(InfoPopupComponent, {
      data,
    });
  }
}
