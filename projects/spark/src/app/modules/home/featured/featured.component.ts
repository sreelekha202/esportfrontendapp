import { Component, OnInit ,Input} from '@angular/core';
import { GlobalUtils } from 'esports';
import { AppHtmlOverviewRoutes} from '../../../app-routing.model';

@Component({
  selector: 'app-featured',
  templateUrl: './featured.component.html',
  styleUrls: ['./featured.component.scss']
})
export class FeaturedComponent implements OnInit {
@Input() games;
AppHtmlOverviewRoutes = AppHtmlOverviewRoutes
  constructor() { }

  ngOnInit(): void {
  }
  onselectedGame(gameId){
    if (GlobalUtils.isBrowser()) {
      localStorage.setItem('gameId', gameId)
    }
  }
}
