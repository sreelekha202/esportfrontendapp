import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SpotlightNewsComponent } from './spotlight-news.component';

describe('SpotlightNewsComponent', () => {
  let component: SpotlightNewsComponent;
  let fixture: ComponentFixture<SpotlightNewsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SpotlightNewsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SpotlightNewsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
