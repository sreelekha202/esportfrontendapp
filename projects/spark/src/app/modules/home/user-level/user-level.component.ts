import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-user-level',
  templateUrl: './user-level.component.html',
  styleUrls: ['./user-level.component.scss'],
})
export class UserLevelComponent implements OnInit {
  @Input() level: number = 0;

  constructor() {}

  ngOnInit(): void {}
}
