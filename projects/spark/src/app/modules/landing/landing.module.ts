import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/modules/shared.module';
import { LandingRoutingModule } from './landing-routing.module';
import { LandingComponent } from './landing.component';
@NgModule({
  declarations: [LandingComponent],
  imports: [CommonModule, SharedModule, LandingRoutingModule],
})
export class LandingModule {}
