import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { SettingsLightboxComponent } from '../settings-lightbox.component';
@Component({
  selector: 'app-accounts',
  templateUrl: './accounts.component.html',
  styleUrls: ['./accounts.component.scss'],
})
export class AccountsComponent implements OnInit {
  constructor(public dialogRef: MatDialogRef<SettingsLightboxComponent>) {}

  ngOnInit(): void {}
  onClose(): void { this.dialogRef.close(); }
}
