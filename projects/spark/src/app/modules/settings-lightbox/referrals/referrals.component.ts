import { Component, OnInit } from '@angular/core';
import { EsportsUserService } from 'esports';
import { environment } from '../../../../environments/environment';

const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;
@Component({
  selector: 'app-referrals',
  templateUrl: './referrals.component.html',
  styleUrls: ['./referrals.component.scss'],
})
export class ReferralsComponent implements OnInit {
  cardLink: string = 'https://paidia.dynasty-dev.com/user/referral:';
  currentUser: any;
  accepted = [];
  sended = [];

  paginationData = {
    page: 1,
    limit: 50,
    sort: { createdOn: -1 },
  };
  constructor(private userService: EsportsUserService) {
    this.userService.currentUser.subscribe((res) => {
      this.currentUser = res;
      this.cardLink = this.cardLink + this.currentUser.accountDetail.referralId;
    });
    // MOCK DATA
    this.accepted = [
      // {
      //   email: 'janesdoes@gmail.com',
      //   date: '11 August 2021, 3:41 PM',
      //   points: 10,
      // },
      // {
      //   email: 'courtneydoes@gmail.com',
      //   date: '29 July 2021, 1:02 PM',
      //   points: 10,
      // },
    ];

    this.sended = [
      // {
      //   email: 'janesdoes@gmail.com',
      //   date: '11 August 2021, 3:41 PM',
      //   points: 24,
      // },
      // {
      //   email: 'courtneydoes@gmail.com',
      //   date: '14 August 2021, 3:41 PM',
      //   points: 15,
      // },
      // {
      //   email: 'janesdoes@gmail.com',
      //   date: '17 August 2021, 3:41 PM',
      //   points: 10,
      // },
      // {
      //   email: 'courtneydoes@gmail.com',
      //   date: '23 August 2021, 3:41 PM',
      //   points: 8,
      // },
    ];
  }

  ngOnInit(): void {
    this.getReferralUserList();
  }

  getReferralUserList() {
    const pagination = JSON.stringify(this.paginationData);
    this.userService
      .getUserinviteesList(API, { pagination })
      .subscribe((res) => {
        // this.accepted = res.data.accepted;
        // this.sended = res.data.sended;
      });
  }
}
