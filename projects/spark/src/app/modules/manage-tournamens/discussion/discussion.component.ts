import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { toggleOpacity, VisibilityState } from '../../../animations';
import { Subscription } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import {
  EsportsUserService,
  GlobalUtils,
  EsportsCommentService,
  EsportsLeaderboardService,
  EsportsGtmService,
  EsportsToastService,
  EsportsChatService,
  SuperProperties,
  EventProperties,
  EsportsTournamentService,
} from 'esports';
import {
  InfoPopupComponentData,
  InfoPopupComponentType,
} from '../../../shared/popups/info-popup/info-popup.component';
import { environment } from '../../../../environments/environment';
const API = environment.apiEndPoint;
const TOKEN = environment.currentToken;
@Component({
  selector: 'app-discussion',
  templateUrl: './discussion.component.html',
  styleUrls: ['./discussion.component.scss'],
  animations: [toggleOpacity],
})
export class DiscussionComponent implements OnInit {
  VisibilityState = VisibilityState;
  collapseOrExpand: VisibilityState = VisibilityState.Visible;
  @Input() isReplying: boolean = false;
  @Input() placeholder: string = '';
  @Input() creatorId;
  comments = [];
  allowDelete: boolean = false;
  currentUser: any;
  currentUserId: any;
  userSubscription: Subscription;
  matchdetails: any;
  typeofchat = "user"
  windowposition: String = 'chat_window chat_window_right_drawer';
  isLoadingNextComments: boolean = false;
  hasNextComment: boolean = true;
  tournamentDetails: any;
  showLoader
  isProcessing:any = false;

  constructor(
    private userService: EsportsUserService,
    private commentService: EsportsCommentService,
    private leaderboardService: EsportsLeaderboardService,
    private gtmService: EsportsGtmService,
    private chatService: EsportsChatService,
    private toastService: EsportsToastService,
    private translateService: TranslateService,
    private esportsTournamentService: EsportsTournamentService,
  ) {}

  ngOnInit(): void {
    if (GlobalUtils.isBrowser()) {
      this.esportsTournamentService.manageTournamentSubject.subscribe(res=>{
        if(res){
          this.tournamentDetails=res
          this.getComment();
        }
      })
      this.userSubscription = this.userService.currentUser.subscribe((data) => {
        if (data) {
          this.currentUser = data;
        }
      });
      this.getCurrentUserDetails();
    }
  }

  getComment() {
    this.showLoader=true
    this.commentService
      .getAllComment1('tournament', this.tournamentDetails?._id, 1)
      .subscribe((data) => {
        if(data){
          this.comments = data?.data;
          this.showLoader = false;
          for(let i=0;i<this.comments.length;i++){
            this.comments[i].isParentComment = true;
          }
        }
      });
  }

  onSaveCommentt(msg) {
    let data = {
      comment: msg,
      objectId: this.tournamentDetails?._id,
      objectType: 'tournament',
    };
    this.commentService.saveComment(data).subscribe((res) => {
      this.getComment();
    });
  }

  onReplyCommentt = async (msg: string,id,indx) => {
    try {
      const payload = {
        comment: msg,
        objectId: id?._id,
        objectType: "comment"
      };

      this.isProcessing = true;
      const message = await this.commentService.upsertComment(payload);
      this.isProcessing = false;
      this.comments[indx].openReply = false;
      this.comments[indx].totalReplies += 1;
      this.getAllReply(id?._id,indx);
    } catch (error) {
      this.isProcessing = false;
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  getCurrentUserDetails() {
    this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.currentUser = data;
        this.currentUserId = data?._id

        if (data?.accountType == 'admin') {
          this.allowDelete = true;
        }
        if (data?._id == this.creatorId) {
          this.allowDelete = true;
        }
      }
    });
  }

  followUser(userVo) {
    if (this.currentUser) {
      if (!userVo.isUserFollowed) {
        this.leaderboardService.followUser(API, userVo?.user?._id).subscribe((res: any) => {
          this.checkFollowStatus(userVo);
          this.toastService.showSuccess(res?.message)
          this.userService.refreshCurrentUser(API, TOKEN);
        },
          (err) => {
            //  this.showLoader = false;
          }
        );
      } else if (userVo.isUserFollowed) {
        this.leaderboardService.unfollowUser(API, userVo?.user?._id).subscribe(
          (res: any) => {            
            this.checkFollowStatus(userVo);
            this.toastService.showSuccess(res?.message)
            this.userService.refreshCurrentUser(API, TOKEN);
          },
          (res: any) => {
            //  this.showLoader = false;
          }
        );
      }
    } else {

      // Navigate to the login page with extras
      //this.router.navigate(['/user/email-login']);
    }
  }

  checkFollowStatus(userVo) {
    this.leaderboardService.checkFollowStatus(API, userVo?.user?._id).subscribe(
      (res: any) => {
        for(let i=0;i<this.comments.length;i++){
          if(userVo?.user?._id == this.comments[i]?.user?._id && res?.data[0]?.status == 1)
            this.comments[i].isUserFollowed = true;
          else if(userVo?.user?._id == this.comments[i]?.user?._id && res?.data[0]?.status == 0)
            this.comments[i].isUserFollowed = false;
        }
      },
      (err) => {
        //this.showLoader = false;
      }
    );
  }

  pushGTMTags(eventName: string, eventProps = false) {
    let superProperties: SuperProperties = {};
    if (this.currentUser) {
      superProperties = this.gtmService.assignLoggedInUsedData(this.currentUser);
    }
    let eventProperties: EventProperties = {};
    if (eventProps) {
      eventProperties.fromPage = 'Chats';
    }
    this.gtmService.gtmEventWithSuperProp({
      eventName: eventName,
      superProps: superProperties,
      eventProps: eventProperties,
    });
  }

  public toggleChat(chatwindow) {
    this.pushGTMTags('Chat_Opened');
    this.matchdetails = chatwindow._id + '-' + this.currentUserId;
    // this.showChat = true;
    this.chatService.setWindowPos(this.windowposition);
    this.chatService.setCurrentMatch(this.matchdetails);
    this.chatService.setTypeOfChat(this.typeofchat);
    this.chatService.setChatStatus(true);
    this.chatService.setTextButtonStatus(true);
  }

  deleteCommentOrReply = async (id, i, j = -1) => {
    try {
      const data: InfoPopupComponentData = {
        title: this.translateService.instant('DISCUSSION.DELETE_COMMENT_TITLE'),
        text: this.translateService.instant('DISCUSSION.DELETE_COMMENT_TEXT'),
        type: InfoPopupComponentType.confirm,
        btnText: this.translateService.instant('BUTTON.CONFIRM'),
      };

      if (true) {
        const deleteComment = await this.commentService.deleteComment(id);
        if (deleteComment.data) {
          if (j > -1) {
            this.comments[i].replies.splice(j, 1);
            this.comments[i].totalReplies = this.comments[i].totalReplies - 1;
          } else {
            this.comments.splice(i, 1);
          }
          this.toastService.showSuccess(deleteComment?.message);
        }
      }
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  likeOrUnLikeComment = async (
    objectId: string,
    objectType: string,
    type: number,
    i = 0,
    j = 0,
    isReply = false
  ) => {
    try {
      /*if (!this.userId) {
        this.toastService.showInfo('Please login to like this comment');
        return;
      }*/

      // if (!this.allowComment) {
      //   this.toastService.showInfo(
      //     'Please Join this tournament to like this comment'
      //   );
      //   return;
      // }

      const payload = {
        objectId,
        objectType,
        type: type == -1 ? 1 : Number(!type),
      };
      
      const like = await this.commentService.upsertLike(payload);
      if (!isReply) {
        for(let m=0;m<this.comments.length;m++){
          if(this.comments[m].replies){
            for(let n=0;n<this.comments[m].replies.length;n++){
              if(this.comments[m].replies[n]._id == objectId)
                j = m;
            }
          }
        }
        this.comments[j].replies[i] = {
          ...this.comments[j].replies[i],
          ...like.data,
        };
      } else {
        this.comments[i] = { ...this.comments[i], ...like.data };
      }
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
    }
  }

  getAllReply = async (commentId, index) => {
    try {
      if (this.comments[index].isLoadingNextReplies) return;

      this.comments[index].page = this.comments[index].page || 1;
      this.comments[index].isLoadingNextReplies =
        this.comments[index].isLoadingNextReplies || true;

        this.showLoader = true;
      const { data, hasNext } = await this.commentService.getAllComment(
        'comment',
        commentId,
        this.comments[index].page
      );

      this.showLoader = false;
      this.comments[index].replies = [
        ...((hasNext ? this.comments[index].replies : []) || []),
        ...data,
      ];
      if(hasNext){
        this.comments[index].page += (this.comments[index].page);
      }
      else{
        this.comments[index].page = 1;
      }

      this.comments[index].hasNextReply = hasNext;

      this.comments[index].isLoadingNextReplies =
        !this.comments[index].isLoadingNextReplies;

      this.comments[index].showReplies = true;
    } catch (error) {
      this.showLoader = false;
      this.comments[index].isLoadingNextReplies =
        !this.comments[index].isLoadingNextReplies;
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  onHideComment(indx,j=0){

    for(let m=0;m<this.comments.length;m++){
      if(this.comments[m].replies){
        for(let n=0;n<this.comments[m].replies.length;n++){
          if(n == indx)
            j = m;
        }
      }
    }
    this.comments[j].openReply = false;
  }

}
