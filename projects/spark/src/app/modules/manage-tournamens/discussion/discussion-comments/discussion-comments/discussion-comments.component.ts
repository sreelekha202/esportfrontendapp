import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { EsportsUserService, IUser } from 'esports';

@Component({
  selector: 'app-discussion-comments',
  templateUrl: './discussion-comments.component.html',
  styleUrls: ['./discussion-comments.component.scss'],
})
export class DiscussionCommentsComponent implements OnInit {
  @Input() placeholder: string = '';
  @Input() isReplying: boolean = false;
  @Input() enableInput: boolean;
  @Input() enableCancelEvent: boolean = true;
  @Input() isProcessing: boolean;
  @Output() onSaveComment = new EventEmitter();
  @Output() onReplyComment = new EventEmitter();
  text: any;
  currentUser: IUser;

  constructor(private userService: EsportsUserService) {}

  ngOnInit(): void {
    this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.currentUser = data;
      }
    });
  }

  comment() {
    this.onSaveComment.emit(this.text);
    this.text='';
  }

  hideReplyDiv(){
    this.onReplyComment.emit(true);
  }
}
