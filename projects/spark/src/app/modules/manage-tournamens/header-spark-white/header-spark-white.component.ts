import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
@Component({
  selector: 'app-header-spark-white',
  templateUrl: './header-spark-white.component.html',
  styleUrls: ['./header-spark-white.component.scss']
})
export class HeaderSparkWhiteComponent implements OnInit {

  constructor(private router : Router,
    private _location : Location) { }

  ngOnInit(): void {
  }

  noBtnClose(){
    return (
      this.router.url === '/manage-tournament/scoring-success' ||
      this.router.url === '/manage-tournament/update-score')
  }

  backClicked() {
    this._location.back();
  }

}
