import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderSparkWhiteComponent } from './header-spark-white.component';

describe('HeaderSparkWhiteComponent', () => {
  let component: HeaderSparkWhiteComponent;
  let fixture: ComponentFixture<HeaderSparkWhiteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HeaderSparkWhiteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderSparkWhiteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
