import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { EsportsToastService } from 'esports';

@Component({
  selector: 'app-clips',
  templateUrl: './clips.component.html',
  styleUrls: ['./clips.component.scss']
})
export class ClipsComponent implements OnInit {
  show = false;
  constructor(
    private eSportsToastService: EsportsToastService,
    private translateService: TranslateService,
  ) { }

  ngOnInit(): void {
  }
  showPopup($event) {
    this.show = true;
  }
  hidePopup($event) {
    this.show = false;
    this.eSportsToastService.showSuccess(
      this.translateService.instant('MANAGE_TOURNAMENT.CLIPS.POPUP.SUCCESS')
    );
  }
  onTextChange(data) {
  }
  data: any = [
    {
      'description': {
        english: "How are my Fortnite replays stored? Replays are stored locally on your console or PC. On console your last 10...",
        french: ""
      },
      "slug": "testt--600ecd33089c510008c6e57b",
      "thumbnailUrl": "assets/icons/video-player/image 59.png",
      "title": { english: "fortnite battle royale - replay system", french: "" },
      "youtubeUrl": "https://www.youtube.com/watch?v=VPe6-LoYXyQ",
      "_id": "600ecd33089c510008c6e57b"
    },
    {
      'description': {
        english: "ESRB Rating: Teen with violence sign up for the fortnite defending the Fort- Fortnite gameplay...",
        french: ""
      },
      "slug": "testt--600ecd33089c510008c6e57b",
      "thumbnailUrl": "assets/icons/video-player/image 60.png",
      "title": { english: "defending the fort - fortnite gameplay", french: "" },
      "youtubeUrl": "https://www.youtube.com/watch?v=VPe6-LoYXyQ",
      "_id": "600ecd33089c510008c6e57b"
    },
    {
      'description': {
        english: "Checkout some of the new features in Fortnite chapter 2, and we even got a Victory Royale to celebrate...",
        french: ""
      },
      "slug": "testt--600ecd33089c510008c6e57b",
      "thumbnailUrl": "assets/icons/video-player/image 61.png",
      "title": { english: "fortnite chapter 2 - victory royale gamepaly", french: "" },
      "youtubeUrl": "https://www.youtube.com/watch?v=VPe6-LoYXyQ",
      "_id": "600ecd33089c510008c6e57b"
    }

  ]
}
