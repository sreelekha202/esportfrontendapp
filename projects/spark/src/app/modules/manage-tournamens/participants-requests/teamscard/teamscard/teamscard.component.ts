import { Component, OnInit, Input,Output, EventEmitter } from '@angular/core';
import { EsportsTournamentService,EsportsToastService} from 'esports';
import { environment } from '../../../../../../environments/environment';

const API = environment.apiEndPoint;

@Component({
  selector: 'app-teamscard',
  templateUrl: './teamscard.component.html',
  styleUrls: ['./teamscard.component.scss']
})
export class TeamscardComponent implements OnInit {
  @Input() isManage: boolean = false;
  @Input() data;
  @Output() onReload = new EventEmitter();
  @Output() refresh = new EventEmitter();
  constructor(
    private _esportsTournamentService: EsportsTournamentService,
    private _esportsToastService:EsportsToastService) { }

  ngOnInit(): void {
  }

 updateParticipant(id, status) {
    this._esportsTournamentService.updateParticipant(API, id, { participantStatus: status }).subscribe((res) => {
      if (res?.success) { 
       this.onReload.emit(true);
        this.refresh.emit(id);
        this._esportsToastService.showSuccess(res?.message);
       }else{
        this._esportsToastService.showSuccess(res?.message);
       }
    });

  }


  
}
