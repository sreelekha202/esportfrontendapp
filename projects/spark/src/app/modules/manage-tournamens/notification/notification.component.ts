

import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { environment } from '../../../../environments/environment';
import { EsportsMessageService, EsportsToastService,IPagination,EsportsNotificationsService,EsportsTournamentService } from 'esports';
import { finalize } from 'rxjs/operators';
const API = environment.apiEndPoint;

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss']
})
export class NotificationComponent implements OnInit {
  showLoader: boolean = true;
  slicesMessages = [];
  messages: any = [];
  show = false;
  tounamentSlug:any;
  tournamentVo:any;

  participantPage: IPagination = {
    activePage: 1,
    totalItems: 1,
    maxSize: 10,
    itemsPerPage: 10,
  };

  constructor(
    private messageService: EsportsMessageService,
    private userNotificationsService: EsportsNotificationsService,
    private eSportsToastService: EsportsToastService,
    private translateService: TranslateService,
    private esportsTournamentService: EsportsTournamentService
  ) { }

  ngOnInit(): void {

    this.esportsTournamentService.manageTournament.subscribe((data) => {
      if (data) {
        this.tournamentVo = data;
      }
    });
    
    this.getMessages();
  }

  getMessages(loader:boolean = true): void {
    if(loader)
    this.showLoader = true;
    
    this.messageService.getInboxMessages().subscribe(
      (response) => {
        this.messages = response.data;
        this.slicesMessages = this.messages.slice(0, this.participantPage.itemsPerPage);
        this.participantPage.totalItems = this.messages.length;
        this.showLoader = false;
      },
      (error) => {
        this.showLoader = false;
      }
    );
  }

  onSelectAll(): void { }

  onPageChange(page): void {
    const startIndex = ((page-1) * this.participantPage.itemsPerPage);
    let endIndex = startIndex + this.participantPage.itemsPerPage;

    if (endIndex > this.messages.length) {
      endIndex = this.messages.length;
    }

    this.slicesMessages = this.messages.slice(startIndex, endIndex);
  }
  onTextChange(data) {
  }
  showPopup($event) {
    this.show = true;
  }
  hidePopup($event) {
    this.show = false;
    
  }

  submitNotification(evt){
    
    const inputData = {
      message: evt.message,
      title: evt.title,
      tournamentId: this.tournamentVo?._id,
      participantType: 'approved',
      userSegment: 'TOURNAMENT_PLAYERS_BY_ID',
    };

    this.submitEmailNotification(inputData);
    this.showLoader = true;
    this.userNotificationsService
      .sendPushNotifications(API, inputData)
      .pipe(
        finalize(() => {
          this.showLoader = false;
        })
      )
      .subscribe(
        (res: any) => {
          if (res) {
            
          }
        },
        (err: any) => {
          this.showLoader = false;
          this.eSportsToastService.showError(
            err?.error?.message || err?.message
          );
        }
      );
    
  }

  submitEmailNotification(payload){

    const inputData = {
      message: payload.message,
      title: payload.title,
      tournamentId: this.tournamentVo?._id,
      participantType: 'approved',
      userSegment: 'TOURNAMENT_PLAYERS_BY_ID',
      headerText: this.translateService.instant("ADMIN.USER_NOTIFICATION.TABS.EMAIL_NOTIFICATION")
    };
    
    this.userNotificationsService
      .sendEmails(API, inputData)
      .pipe(
        finalize(() => {
          this.showLoader = false;
        })
      )
      .subscribe(
        (res: any) => {
          if (res) {
            this.eSportsToastService.showSuccess(
              this.translateService.instant('MANAGE_TOURNAMENT.NOTIFICATIONS.POPUP.SUCCESS')
            );
            this.getMessages();
          }
        },
        (err: any) => {
          this.showLoader = false;
          this.eSportsToastService.showError(
            err?.error?.message || err?.message
          );
        }
      );
    
  }
}
