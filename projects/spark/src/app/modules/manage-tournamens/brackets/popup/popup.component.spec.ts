import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PopupBracketsComponent } from './popup.component';

describe('PopupComponent', () => {
  let component: PopupBracketsComponent;
  let fixture: ComponentFixture<PopupBracketsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PopupBracketsComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PopupBracketsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
