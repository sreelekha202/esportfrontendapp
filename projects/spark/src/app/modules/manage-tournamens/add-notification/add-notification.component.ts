import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {FormBuilder,FormGroup,Validators} from '@angular/forms';

@Component({
  selector: 'app-add-notification',
  templateUrl: './add-notification.component.html',
  styleUrls: ['./add-notification.component.scss']
})
export class AddNotificationComponent implements OnInit {
  @Output() hidePopup = new EventEmitter();
  @Output() submitNotification = new EventEmitter();

  notifiyForm:any = new FormGroup({});
  notificationTitle:any = '';
  notificationMsg:any = '';
  
  constructor(private builder: FormBuilder,) { }

  ngOnInit(): void {
    this.notifiyForm.addControl(
      'titleName',
      this.builder.control(null, [Validators.required])
    );
    this.notifiyForm.addControl(
      'titleMsg',
      this.builder.control(null, [Validators.required])
      );
  }

  submit(){
    this.submitNotification.emit({message:this.notificationMsg,title:this.notificationTitle});
    this.notificationTitle = '';
    this.notificationMsg = '';
  }

}
