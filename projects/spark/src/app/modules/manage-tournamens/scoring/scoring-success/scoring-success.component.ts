import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-scoring-success',
  templateUrl: './scoring-success.component.html',
  styleUrls: ['./scoring-success.component.scss']
})
export class ScoringSuccessComponent implements OnInit {

  

  constructor(
    private router : Router
  ) { }

  ngOnInit(): void {
  }
  backToManagementTOur(){
    return this.router.navigateByUrl('/manage-tournament')
  }

}
