import { Component, OnInit,ElementRef, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {MatChipInputEvent} from '@angular/material/chips';
import {COMMA, ENTER} from '@angular/cdk/keycodes';
import { environment } from '../../../../../environments/environment';
const API = environment.apiEndPoint;
import { UserReportsService} from 'esports';
interface status {
  value: string;
  viewValue: string;
}

export interface Attachment {
  name: string;
}


@Component({
  selector: 'app-single-item',
  templateUrl: './single-item.component.html',
  styleUrls: ['./single-item.component.scss']
})
export class SingleItemComponent implements OnInit {
  userReport:any;
  show = false;
  showLoader: boolean = true;
  reportId:any;
  ngOnInit(): void {


    this.activeRoute.queryParams.subscribe(params => {

        
        if(params.id){
          this.reportId  = params.id;
          this.getUserReport();
        }

      }
    );
  }

  constructor(
    private router : Router,
    private UserReportsService: UserReportsService,
    private activeRoute: ActivatedRoute,
  ){}

  previousPage(){
    this.router.navigateByUrl("manage-tournament/reports")
  }

  statuss: status[] = [
    {value: 'open', viewValue: 'Open'},
    {value: 'close', viewValue: 'Close'}
  ];

  selectable = true;
  removable = true;
  addOnBlur = true;
  readonly separatorKeysCodes = [ENTER, COMMA] as const;
  attachments: Attachment[] = [
    {name: 'Image_0012'},
    {name: 'Image_101'},
    {name: 'Image_645'},
    {name: 'Image_1e22'},
  ];

  add(event: MatChipInputEvent): void {
    const value = (event.value || '').trim();

    // Add our fruit
    if (value) {
      this.attachments.push({name: value});
    }

    // Clear the input value
    // event.chipInput!.clear();
  }

  remove(fruit: Attachment): void {
    const index = this.attachments.indexOf(fruit);

    if (index >= 0) {
      this.attachments.splice(index, 1);
    }
  }

  getUserReport(){
    this.showLoader = true;
    this.UserReportsService.getUserReportById(API,
      this.reportId
    ).subscribe((res: any) => {
      this.showLoader = false;
         this.userReport = res?.data;
      },
      (err) => {
         this.showLoader = false;
      }
    );
  }

}
