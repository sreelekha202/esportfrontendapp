import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-isearch',
  templateUrl: './isearch.component.html',
  styleUrls: ['./isearch.component.scss'],
})
export class IsearchComponent implements OnInit {
  @Input() placeholder: string = '';
  @Input() text: string = '';
  @Output() onTextChange = new EventEmitter<string>();
  constructor() { }
  ngOnInit(): void { }
  onTextChang = () => this.onTextChange.emit(this.text)
}
