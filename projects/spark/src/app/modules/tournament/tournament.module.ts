import { NgModule } from '@angular/core';
import { CoreModule } from '../../core/core.module';
import { TournamentRoutingModule } from './tournament-routing.module';
import { TournamentComponent } from './tournament.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { SharedModule } from '../../shared/modules/shared.module';
import { PrizePoollModule } from './pipe/prize-pooll.module';
import { HeaderBottomModule } from "../../shared/components/header-bottom/header-bottom.module";
import { FooterMenuMobileModule } from '../../shared/components/footer-menu-mobile/footer-menu-mobile.module';
import { FooterSparkModule } from '../../shared/components/footer-spark/footer-spark.module';
import { EsportsLoaderModule, EsportsCustomPaginationModule } from 'esports';

@NgModule({
  declarations: [
    TournamentComponent,
  ],
  imports: [
    CoreModule,
    MatProgressSpinnerModule,
    SharedModule,
    PrizePoollModule,
    TournamentRoutingModule,
    HeaderBottomModule,
    FooterSparkModule,
    FooterMenuMobileModule,
    EsportsLoaderModule.setColor('#00a851'),
    EsportsCustomPaginationModule
  ],
  providers: [],
})
export class TournamentModule { }
