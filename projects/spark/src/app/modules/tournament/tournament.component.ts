import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { AppHtmlRoutes } from '../../app-routing.model';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { TranslateService } from '@ngx-translate/core';
import {
  IPagination,
  EsportsLanguageService,
  EsportsTournamentService,
  EsportsGameService,
  GlobalUtils,
} from 'esports';
import { environment } from '../../../environments/environment';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';

const API = environment.apiEndPoint;

@AutoUnsubscribe()
@Component({
  selector: 'app-tournament',
  templateUrl: './tournament.component.html',
  styleUrls: ['./tournament.component.scss'],
})
export class TournamentComponent implements OnInit {
  @ViewChild('mainContainer', { read: ElementRef })
  public scroll: ElementRef<any>;
  AppHtmlRoutes = AppHtmlRoutes;
  showLoader: boolean = false;
  currentTab = 'upcoming';
  page: IPagination;
  allTournamentList: any = [];
  gameId;
  tournamentData: any;
  curentPage = 1;
  activeIndex = 0;
  showFilter = true;
  selectedSortBy: any = '';
  tournamentType:any = '';
  tournamentFlagType = '';
  paginationData = {
    page: 1,
    limit: 8,
    sort: 'startDate',
  };
  participantPage: IPagination = {
    activePage: 1,
    totalItems: 1,
    maxSize: 10,
    itemsPerPage: 9,
  };
  checkboxFilter = [];
  selected_platform: any;
  faSearch = faSearch;
  isShown: boolean = false;
  params: any = {};
  gameDetail: any;
  paginationDetails: any;
  pageSizeOptions = environment.pageSizeOptions;
  individualTournaments = [];
  prizeMoneyTournaments = [];
  teamTournaments = [];
  hidePagination = false;
  sortby = [
    { value: '0', viewValue: 'Upcoming tournaments' },
    { value: '1', viewValue: 'Ongoing tournaments' },
    { value: '2', viewValue: 'Completed tournaments' },
    { value: '3', viewValue: 'All tournaments' },
  ];
  selected_sortby_tournament = this.sortby[3].value;
  listFilter = [
    {
      label: 'ADMIN.SEASON_MANAGEMENT.TABLE_HEADER.MOBILE',
      value: 'mobile',
      check: false,
    },
    {
      label: 'ADMIN.SEASON_MANAGEMENT.TABLE_HEADER.CONSOLE',
      value: 'console',
      check: false,
    },
    {
      label: 'ADMIN.SEASON_MANAGEMENT.TABLE_HEADER.PC',
      value: 'pc',
      check: false,
    },
  ];
  sortByList = [
    {
      label: 'ADMIN.SEASON_MANAGEMENT.TABLE_HEADER.PRIZE',
      value: 'Prize',
    },
    {
      label: 'ADMIN.SEASON_MANAGEMENT.TABLE_HEADER.TEAM',
      value: 'Team',
    },
  ];
  isAll = false;
  isOngoing = false;
  isUpcoming = true;
  tournamentDetails: any;
  selectedGame: any;
  selectedGameLogo: any;
  constructor(
    private languageService: EsportsLanguageService,
    private tournamentService: EsportsTournamentService,
    private translateService: TranslateService,
    private gameService: EsportsGameService
  ) {}

  ngOnInit(): void {
    this.isUpcoming = true;
    this.languageService.language.subscribe((lang) =>
      this.translateService.use(lang || 'en')
    );

    // if (
    //   this.activeRoute.snapshot.params &&
    //   this.activeRoute.snapshot.params.gameID
    // ) {
    //   this.gameId = this.activeRoute.snapshot.params.gameID;
    //   this.getAllTournaments();
    // }
    if (GlobalUtils.isBrowser()) {
      this.gameId = localStorage.getItem('gameId');
      // this._activateRoute.paramMap.subscribe((params)=>{
      //   this.gameId = params.get('slug');
      // })

      if (this.gameId) {
        this.getUpcomingTournamentsPaginated();
        this.getGames();
      }
    } 
  }
  
  getGames() {
    // this.showLoader = true;
    this.gameService.getGames(API).subscribe(
      (res) => {
        this.showLoader = false;
        this.selectedGame = res?.data;
        this.selectedGame.forEach(selectGame => {
          if(selectGame?._id == this.gameId){
            this.selectedGameLogo = selectGame;
          }
        });
      },
      (err) => {
        this.showLoader = false;
      }
    );
  }
  clearFilter() {
    this.showFilter = !this.showFilter;
  }

  isSelected(isactive) {
    this.hidePagination = true;
    this.participantPage.activePage = 1;
    this.curentPage = 1;
    if (isactive == 'isAll') {
      this.isAll = true;
      this.isOngoing = false;
      this.isUpcoming = false;
    }
    if (isactive == 'isOngoing') {
      this.isAll = false;
      this.isOngoing = true;
      this.isUpcoming = false;
    }
    if (isactive == 'isUpcoming') {
      this.isAll = false;
      this.isOngoing = false;
      this.isUpcoming = true;
    }
  }
  getAllTournaments() {
    this.showLoader = true;
    this.tournamentType = '';
    this.tournamentFlagType = 'All';
    this.params = {
      status: '3',
      limit: this.participantPage.itemsPerPage,
      page: this.curentPage,
      game: this.gameId,
      sort: {"startDate":1},
    };

    this.tournamentService.getPaginatedTournaments(API,this.params).subscribe(
      (res: any) => {
        this.tournamentData = res?.data;
        this.showLoader = false;
        this.participantPage.itemsPerPage = this.tournamentData?.limit;
        this.participantPage.totalItems = this.tournamentData?.totalDocs;
        // this.gameDetail = res?.data?.docs[0].gameDetail;
        this.hidePagination = false;
        
      },
      (err: any) => {
        this.showLoader = false;
        this.hidePagination = false;
      }
    );
  }

  getOnGoingTournamentsPaginated() {
    this.showLoader = true;
    this.tournamentType = 'onGoing';
    this.tournamentFlagType = 'OnGoing';
    let query: any = {
      $and: [{ tournamentStatus: 'publish' }],
      $or: [{ startDate: { $lt: new Date() } }, { isSeeded: true }],
    };
    if (this.gameId) {
      query.gameDetail = this.gameId;
    }
    query = JSON.stringify(query);
    let params = {
      status: '1',
      limit: this.participantPage.itemsPerPage,
      page: this.curentPage,
      game: this.gameId,
      sort: {"endDate":1},
    };

    if (this.gameId) {
      params['game'] = this.gameId;
    }

    this.tournamentService.getPaginatedTournaments(API, params).subscribe(
      (res: any) => {
        this.showLoader = false;
        this.tournamentData = res?.data;
        this.participantPage.itemsPerPage = this.tournamentData?.limit;
        this.participantPage.totalItems = this.tournamentData?.totalDocs;
        this.hidePagination = false;
      },
      (err) => {
        this.showLoader = false;
        this.hidePagination = false;
      }
    );
  }

  getUpcomingTournamentsPaginated() {
    this.showLoader = true;
    this.tournamentType = '';
    this.tournamentFlagType = 'UpComing';
    let query: any = {
      $and: [
        { tournamentStatus: 'publish' },
        { startDate: { $gt: new Date() } },
        { isSeeded: false },
        { isFinished: false },
      ],
    };

    if (this.gameId) {
      query.gameDetail = this.gameId;
    }

    query = JSON.stringify(query);
    let params = {
      status: '0',
      limit: this.participantPage.itemsPerPage,
      page: this.curentPage,
      game: this.gameId,
      sort: {"startDate":1},
    };

    if (this.gameId) {
      params['game'] = this.gameId;
    }

    this.tournamentService.getPaginatedTournaments(API, params).subscribe(
      (res: any) => {
        this.showLoader = false;
        this.tournamentData = res?.data;
        this.participantPage.itemsPerPage = this.tournamentData?.limit;
        this.participantPage.totalItems = this.tournamentData?.totalDocs;
        this.hidePagination = false;
        this.gameDetail = res?.data?.docs[0]?.gameDetail;
        this.gameService.createMatchMakingSubject.next({
          selectedGame: this.gameDetail,
          selectedPlatForm: res?.data?.docs[0]?.platform,
        });
      },
      (err) => {
        this.showLoader = false;
        this.hidePagination = false;
      }
    );
  }

  currentPage(page): void {
    this.participantPage.activePage = Number(page);
    this.curentPage = Number(page);
    if (this.isAll) this.getAllTournaments();
    if (this.isOngoing) this.getOnGoingTournamentsPaginated();
    if (this.isUpcoming) this.getUpcomingTournamentsPaginated();
  }

  // pageChanged(page): void {
  //   this.paginationData.page = page.pageIndex + 1;
  //   this.paginationData.limit = page.pageSize;
  //   this.getAllTournaments();
  // }

  scrollToTop() {
    this.scroll.nativeElement.scrollIntoView();
  }

  showPopup() {
    this.isShown = true;
  }
  hidePopup() {
    this.isShown = false;
  }
  onSortBy(item) {
    const tournament = [];
    this.selectedSortBy = item;
    this.tournamentData.docs.forEach((t) => {
      if (item.value.toLowerCase() == t.participantType.toLowerCase()) {
        tournament.push(t);
      }
      if (item.value.toLowerCase() == 'prize' && t.isPrize) {
        tournament.push(t);
      }
    });
    this.tournamentData.docs = tournament;
    // this.getAllTournaments();
  }
  onSortByoption(event, option) {
    const index = this.listFilter.findIndex((item, i) => {
      return item.value == option.value;
    });
    this.listFilter[index] = {
      ...this.listFilter[index],
      check: event.checked,
    };
    this.selectedSortBy = option;

    this.onSortByLogic(option);
    // this.getAllTournaments();
  }

  onSortByLogic(option) {
    let t = [];
    this.tournamentData.docs.forEach((tournament, i) => {
      this.listFilter.forEach((opValueFromList) => {
        if (opValueFromList.check) {
          if (
            opValueFromList.value.toLowerCase() ==
            tournament.platform.name.toLowerCase()
          ) {
            t.push(tournament);
          }
        }
      });
    });
    this.tournamentData.docs = t;
  }
  removeSortBy(event) {
    const index = this.listFilter.findIndex((item) => {
      return item.value == event.value;
    });
    this.listFilter[index] = {
      ...this.listFilter[index],
      check: false,
    };
    // this.selectedSortBy = '';
    this.onSortByLogic(event);
  }
  clearFilterBtn() {
    this.listFilter = this.listFilter.map((item) => {
      return {
        ...item,
        check: false,
      };
    });

    this.getAllTournaments();
  }
  ngOnDestroy(): void {}
}
