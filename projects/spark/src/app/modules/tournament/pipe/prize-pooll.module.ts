import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PrizePoollPipe } from './prize-pooll.pipe';



@NgModule({
  declarations: [PrizePoollPipe],
  imports: [
    CommonModule
  ], exports: [PrizePoollPipe]
})
export class PrizePoollModule { }
