import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { EsportsUtilsService, EsportsToastService,  EsportsBracketService,EsportsUserService,IUser} from 'esports';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';

@AutoUnsubscribe()
@Component({
  selector: 'app-matches',
  templateUrl: './matches.component.html',
  styleUrls: ['./matches.component.scss']
})
export class MatchesComponent implements OnInit {
  @Input() tournament;
  @Input() tournamentId;
  isLoaded: boolean = false;
  selectedRound: any;
  stage: Array<any> = [];
  standingRound: Array<any> = [];
  round=[];
  matchList =[];
  currentUser: IUser;

  userSubscription: Subscription;
  constructor(
    private router : Router,
    private utilsService: EsportsUtilsService,
    private bracketService: EsportsBracketService,
    private toastService: EsportsToastService,
    private userService: EsportsUserService,
    ) {}

  ngOnInit(): void {
    this.fetchDistinctRound();
    this.userSubscription = this.userService.currentUser.subscribe(
      (data) => {
        if (data) {
          this.currentUser = data;
        }
      }
    )
  }

  onTextChange(data) {
  }

  ngOnChanges() {
    if (this.tournament?._id && this.tournament?.bracketType) {
      if (['single', 'double'].includes(this.tournament?.bracketType)) {
        this.fetchDistinctRound();
      } else if (
        ['round_robin', 'battle_royale'].includes(this.tournament?.bracketType)
      ) {
        this.fetchDistinctStage();
      } else if (this.tournament?.bracketType == 'swiss_safeis') {
        this.isLoaded = true;
      }
    }
  }

  ngOnDestroy() {}

  getFieldType = (type) => {
    if (['single', 'double'].includes(type)) {
      return 'currentMatch.round';
    } else if (['round_robin', 'battle_royale'].includes(type)) {
      return 'currentMatch.stage';
    } else {
      return 'currentMatch.round';
    }
  };

  fetchDistinctRound = async () => {
    try {
      const queryParam = `?query=${this.utilsService.encodeQuery({
        tournamentId: this.tournament?._id,
      })}`;

      const field = this.getFieldType(this.tournament?.bracketType);
      const response = await this.bracketService.fetchDistinctValue(
        queryParam,
        field
      );
      this.round = response.data.map((r) => {
        return {
          id: r,
          isCollapsed: false,
          isLoaded: false,
        };
      });
      this.selectedRound = this.round[0];
      this.fetchMatches(this.round[0], 0);
      this.isLoaded = true;
    } catch (error) {
      this.isLoaded = true;
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  fetchDistinctStage = async () => {
    try {
      const queryParam = `?query=${this.utilsService.encodeQuery({
        tournamentId: this.tournament?._id,
      })}`;

      const field = this.getFieldType(this.tournament?.bracketType);
      const response = await this.bracketService.fetchDistinctValue(
        queryParam,
        field
      );

      this.stage = response.data.map((r) => {
        return {
          id: r,
          isCollapsed: false,
          isLoaded: false,
        };
      });
      this.isLoaded = true;
    } catch (error) {
      this.isLoaded = true;
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  fetchDistictStageGroup = async (stage, i) => {
    try {
      if (stage.isCollapsed && !stage.isLoaded) {
        const queryParam = `?query=${encodeURIComponent(
          JSON.stringify({
            tournamentId: this.tournament?._id,
            'currentMatch.stage': stage.id,
          })
        )}`;

        const response = await this.bracketService.fetchDistinctValue(
          queryParam,
          'currentMatch.group'
        );

        this.stage[i].isLoaded = true;
        this.stage[i].group = response.data.map((g) => {
          return {
            id: g,
            isCollapsed: false,
            isLoaded: false,
          };
        });
      }
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  fetchDistictStageGroupRound = async (stage, group, i, j) => {
    try {
      if (group.isCollapsed && !group.isLoaded) {
        if (this.tournament?.bracketType == 'battle_royale') {
          const queryParam = `?stage=${stage.id}&group=${group.id}&tournamentId=${this.tournament?._id}`;
          const response = await this.bracketService.fetchStanding(queryParam);

          this.stage[i].group[j].standings = response.data;
          this.standingRound = response.data.length
            ? response.data[0].round
            : [];
        } else {
          const queryParam = `?query=${encodeURIComponent(
            JSON.stringify({
              tournamentId: this.tournament?._id,
              'currentMatch.stage': stage.id,
              'currentMatch.group': group.id,
            })
          )}`;

          const response = await this.bracketService.fetchDistinctValue(
            queryParam,
            'currentMatch.round'
          );

          this.stage[i].group[j].round = response.data.map((r) => {
            return {
              id: r,
              isCollapsed: false,
              isLoaded: false,
            };
          });
        }
        this.stage[i].group[j].isLoaded = true;
      }
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  fetchDistictStageGroupRoundMatch = async (stage, group, round, i, j, k) => {
    try {
      if (round.isCollapsed && !round.isLoaded) {
        const queryParam = `?query=${encodeURIComponent(
          JSON.stringify({
            tournamentId: this.tournament?._id,
            'currentMatch.stage': stage.id,
            'currentMatch.group': group.id,
            'currentMatch.round': round.id,
            matchStatus: { $ne: 'inactive' },
            bye: false,
          })
        )}&select=sets,totalSet,matchNo,winnerTeam,loserTeam,disQualifiedTeam,battleRoyalTeams`;
        const response = await this.bracketService.fetchAllMatches(queryParam);
        this.stage[i].group[j].round[k].match = response.data;
        this.stage[i].group[j].round[k].isLoaded = true;

        const totalSet = response.data.reduce((accumulator, currentValue) => {
          const max =
            currentValue.sets.length > currentValue.totalSet
              ? currentValue.sets.length
              : currentValue.totalSet;
          accumulator = max > accumulator ? max : accumulator;
          return accumulator;
        }, 0);
        // response.data.length
        //   ? this.createSetHeader(totalSet)
        //   : this.createSetHeader(0);
      }
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  fetchMatches = async (round, i) => {
    this.isLoaded = false;
    this.matchList = [];
    try {
      //   const queryParam = `?tournamentId=${this.tournament?._id}&page=1&round=1`;
      // const response = await this.bracketService.fetchAllMatches(queryParam);
      const queryParam = `?query=${encodeURIComponent(
        JSON.stringify({
          tournamentId: this.tournament?._id,
          'currentMatch.round': round.id,
          matchStatus: { $ne: 'inactive' },
          bye: false,
        })
      )}&select=sets,totalSet,matchNo,winnerTeam,loserTeam,disQualifiedTeam,battleRoyalTeams`;
      const response = await this.bracketService.fetchAllMatches(queryParam);
      if (this.tournament?.bracketType == "battle_royale") {
      }

      else {
        this.matchList = response.data;

        this.selectedRound = this.round[i];
        // const totalSet = _.reduce(response.data, function (accumulator, currentValue) {
        //   const max =
        //     currentValue.sets.length > currentValue.totalSet
        //       ? currentValue.sets.length
        //       : currentValue.totalSet;
        //   accumulator = max > accumulator ? max : accumulator;
        //   return accumulator;
        // }, 0);
        this.isLoaded = true;
        // this.round[i].match = response.data;
        this.matchList = response.data;
        // if (response.data) {
        //   this.createSetHeader(totalSet);
        // } else {
        //   this.createSetHeader(0);
        // }
      }

      this.round[i].isLoaded = true;
      this.round[i].match = response.data;
      this.selectedRound = this.round[i];
    } catch (error) {
      this.isLoaded = true;
    }
  };
}
