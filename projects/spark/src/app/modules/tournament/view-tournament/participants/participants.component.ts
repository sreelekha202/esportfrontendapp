import {
  Component,
  OnInit,
  Input,
  OnChanges,
  SimpleChanges,
  ViewChild,
} from '@angular/core';
import { EsportsTournamentService, IPagination } from 'esports';
import { environment } from '../../../../../environments/environment';

const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;
@Component({
  selector: 'app-participants',
  templateUrl: './participants.component.html',
  styleUrls: [
    './participants.component.scss',
    '../view-tournament.component.scss',
  ],
})
export class ParticipantsComponent implements OnInit, OnChanges {
  @Input() isManage: boolean = false;
  @Input() data;
  @Input() tournamentId: string;
  @Input() tournament;
  approvedParticipants = [];
  isLoaded = false;
  selectedTeam = null;
  currentTab = 'approvedParticipants';
  paginationDetails: any;
  paginationData = {
    page: 1,
    limit: 10,
    sort: 'startDate',
  };
  participantPage: IPagination = {
    activePage: 1,
    totalItems: 1,
    maxSize: 10,
    itemsPerPage: 10,
  };

  active = 1;
  nextId: number = 1;
  teamRequests = [];
  showLoader: boolean = true;

  constructor(
    private tournamentService: EsportsTournamentService,
  ) { }

  ngOnInit(): void {
  }

  fetchAllApprovedParticipants = async () => {
    try {
      this.isLoaded = false;
      this.selectedTeam = false;
      let params = this.tournamentId
      //   type: "1",
      const participants = await this.tournamentService
        .getParticipants(API, params)
        .toPromise();
      this.approvedParticipants = participants?.data?.docs;
      this.setParticipant();
      this.isLoaded = true;
    } catch (error) {
      this.isLoaded = true;
      // this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  ngOnChanges(simpleChanges: SimpleChanges) {
    if (
      simpleChanges.hasOwnProperty('tournamentId') &&
      simpleChanges.tournamentId.currentValue
    ) {
      this.fetchAllApprovedParticipants();
    }
  }

  refresh(id) {
    this.approvedParticipants.forEach((e, i) => {
      if (e._id == id) {
        this.approvedParticipants.splice(i, 1);
      }
    });
  }

  setParticipant() {
    this.currentTab === 'approvedParticipants'
      ? this.approvedParticipants
      : (this.approvedParticipants = []);
  }

  pageChanged(page): void {
    if (this.paginationDetails?.totalDocs > 0) {
      this.paginationData.page = page.pageIndex + 1;
      this.paginationData.limit = page.pageSize;
      this.navChanges({ nextId: this.nextId });
    }
  }
  currentPage(page): void {
    this.participantPage.activePage = page;
    this.navChanges({ nextId: this.nextId });
    this.fetchAllApprovedParticipants();
  }
  navChanges = (e) => {
    this.nextId = e.nextId;
    this.fetchAllApprovedParticipants();
  };
}
