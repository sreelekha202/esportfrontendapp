import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClipsViewComponent } from './clips-view.component';

describe('ClipsViewComponent', () => {
  let component: ClipsViewComponent;
  let fixture: ComponentFixture<ClipsViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ClipsViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ClipsViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
