import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-clips-view',
  templateUrl: './clips-view.component.html',
  styleUrls: ['./clips-view.component.scss']
})
export class ClipsViewComponent implements OnInit {
  itemsPerPage = [3, 6, 10];

  slicesItems = [];

  constructor() {}

  ngOnInit(): void {
    // this.slicesItems = this.data.slice(0, this.itemsPerPage[0]);
  }

  onPageChange(event): void {
    const startIndex = event.pageIndex * event.pageSize;
    let endIndex = startIndex + event.pageSize;
    // if (endIndex > this.data.length) {
    //   endIndex = this.data.length;
    // }
    // this.slicesItems = this.data.slice(startIndex, endIndex);
  }

}

