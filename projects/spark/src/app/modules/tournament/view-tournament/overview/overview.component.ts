import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { EsportsGameService, EsportsLanguageService, EsportsUserService, GlobalUtils, IUser } from 'esports';
import { environment } from '../../../../../environments/environment';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';
import {
  AppHtmlRoutes,
  AppHtmlRoutesLoginType,
} from '../../../../app-routing.model';
const API = environment.apiEndPoint;

@AutoUnsubscribe()
@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.scss', '../view-tournament.component.scss'],
})
export class OverviewComponent implements OnInit, OnDestroy {
  AppHtmlRoutes = AppHtmlRoutes;
  AppHtmlRoutesLoginType = AppHtmlRoutesLoginType;
  @Input() tournamentDetails;
  registrationStatus = {
    isOpen: false,
    isClosed: false,
    notYetStarted: false,
  };
  platformList: any = [];
  date=new Date() ;
  tournament_endDate:any;
  isEnd: boolean = false;
  isNotEnded: boolean = false;
  isLogin: boolean = false;

  constructor(
    public languageService: EsportsLanguageService,
    private esportsGameService: EsportsGameService,
    private userService: EsportsUserService,
    ) {}

    user: IUser;

  ngOnInit(): void {
    // MOCK DATA
    // this.tournamentDetails = localStorage.getItem("tournamentDetails")
    // this.tournamentDetails = JSON.parse(this.tournamentDetails)
  }

  ngOnChanges() {
    this.tournament_endDate=new Date(this.tournamentDetails?.endDate);
    if (this.tournament_endDate<this.date){
      this.isEnd = true;
    }else if (this.tournament_endDate>=this.date){
      this.isNotEnded = true;
    }
    this.setRegistrationOpenStatus();
    this.getPlatformList();
    this.getCurrentUserDetails();
  }

  getCurrentUserDetails() {
    this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.isLogin = true;
        this.user = data;
      }
    });
  }

  ngOnDestroy() {}

  getPlatformList() {
    this.esportsGameService.fetchPlatforms(API).subscribe((res) => {
      this.platformList = res.data;
    });
  }

  getPlatformName(p_id) {
    let name = '';
    this.platformList.map((res) => {
      if (p_id == res?._id) {
        name = res.name;
      }
    });
    return name;
  }

  setRegistrationOpenStatus() {

    const now = new Date().getTime();
    const tournamentStartTime = new Date(this.tournamentDetails?.startDate).getTime();
    const regStartTime = new Date(this.tournamentDetails?.regStartDate).getTime();
    const regEndTime = new Date(this.tournamentDetails?.regEndDate).getTime();

    if(this.tournamentDetails?.isRegistrationClosed) {
      this.registrationStatus.isClosed = true;
      return;
    }
    if(this.tournamentDetails?.isSeeded || now > tournamentStartTime){
      this.registrationStatus.isClosed = true;
      return;
    }

    if (this.tournamentDetails?.isRegStartDate && now > regEndTime) {
      this.registrationStatus.isClosed = true;
      return;
    }

    if(this.tournamentDetails?.isRegStartDate && regStartTime > now) {
      this.registrationStatus.notYetStarted = true;
      return;
    }
    this.registrationStatus.isOpen = true;
  }
}
