import { Component, OnInit, Input, OnChanges, OnDestroy,  SimpleChanges} from '@angular/core';
import { Subscription } from 'rxjs';
import {
  EsportsToastService,
  EsportsUtilsService,
  EsportsBracketService,
  EsportsUserService
} from 'esports';

@Component({
  selector: 'app-brackets',
  templateUrl: './brackets.component.html',
  styleUrls: ['./brackets.component.scss', '../view-tournament.component.scss'],
})
export class BracketsComponent implements OnInit, OnChanges, OnDestroy {
  @Input() tournamentDetails;
  @Input() type;
  @Input() participantId: string | null;
  apiLoaded: Array<boolean> = [];
  isLoaded = false;
  structure: any;
  isSeeded = false;
  userId;
  userSubscription: Subscription;

  filterList = [
    { name: 'SEED.ALL_PLAYERS', value: 'all-players' },
  ];

  constructor(
    private bracketService: EsportsBracketService,
    private userService: EsportsUserService,
    private eSportsToastService: EsportsToastService,
    private utilsService: EsportsUtilsService,
  ) { }

  ngOnInit(): void { }

  ngOnDestroy() {
    this.userSubscription?.unsubscribe();
  }

  ngOnChanges(simpleChanges: SimpleChanges) {
    if (
      simpleChanges.hasOwnProperty('tournamentDetails') &&
      simpleChanges.tournamentDetails.currentValue
    ) {
      this.fetchBracket();
      this.getCurrentUserDetails();
    }
  }

  getCurrentUserDetails() {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.userId = data._id;
      }
    });
  }

   /**
   * Fetch All Match If Seed Else Fetch Mock Structure Of The Tournament
   */
    fetchBracket = async () => {
      try {
        this.apiLoaded.push(true);
        if (
          this.tournamentDetails?.isSeeded &&
          ['single', 'double'].includes(this.tournamentDetails?.bracketType)
        ) {
          const queryParam = `?query=${this.utilsService.encodeQuery({
            tournamentId: this.tournamentDetails._id,
          })}`;
          const response = await this.bracketService.fetchAllMatches(queryParam);
          this.structure = this.bracketService.assembleStructure(response.data);
        } else if  (
          !this.tournamentDetails?.isSeeded &&
           ['single', 'double', 'round_robin', 'battle_royale'].includes(
            this.tournamentDetails?.bracketType
          )
        ) {
          const payload = {
            bracketType: this.tournamentDetails?.bracketType,
            maximumParticipants: this.tournamentDetails?.maxParticipants,
            noOfSet: this.tournamentDetails?.noOfSet,
            ...(['round_robin', 'battle_royale'].includes(
              this.tournamentDetails?.bracketType
            ) && {
              noOfTeamInGroup: this.tournamentDetails?.noOfTeamInGroup,
              noOfWinningTeamInGroup: this.tournamentDetails
              ?.noOfWinningTeamInGroup,
              noOfRoundPerGroup: this.tournamentDetails?.noOfRoundPerGroup,
              stageBracketType: this.tournamentDetails?.stageBracketType,
            }),
          };
          const response = await this.bracketService.generateBracket(payload);
          this.structure = response.data;
        }
        this.apiLoaded.push(true);
      } catch (error) {
        this.apiLoaded.push(true);
        this.eSportsToastService.showError(error?.error?.message || error?.message);
      } finally {
        this.checkAllApiDataLoaded();
      }

    };

    checkAllApiDataLoaded() {
      const startedApiCount = this.apiLoaded.filter((el) => !el).length;
      const finishedApiCount = this.apiLoaded.filter((el) => el).length;
      this.isLoaded = startedApiCount === finishedApiCount;
    }

    getResponseMessage(data) {
      this.fetchBracket();
    }

}
