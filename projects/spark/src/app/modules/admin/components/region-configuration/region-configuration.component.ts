import { Component, OnInit, ViewChild } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";

import { EsportsAdminService, GlobalUtils } from 'esports';
import { MatDialog } from "@angular/material/dialog";
import { TranslateService, LangChangeEvent } from "@ngx-translate/core";
import { environment } from '../../../../../environments/environment';
const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;

@Component({
  selector: "app-region-configuration",
  templateUrl: "./region-configuration.component.html",
  styleUrls: [
    "./region-configuration.component.scss",
    "../site-configuration/site-configuration.component.scss",
  ],
})
export class RegionConfigurationComponent implements OnInit {
  bannerUri: any = "";
  public ownerForm: FormGroup;
  regionList: any = [];
  addNewRegion = false;
  showAdList = true;
  currentLang: string = "english";
  isEditable = {};
  isLoading = true;
  addedRegions = [];
  _id = 0;
  /**
   * ViewChild check of banner image object
   */
  @ViewChild("bannerUpload", { static: false }) set bannerUpload(element) {
    if (GlobalUtils.isBrowser() && element && this.bannerUri) {
      document.getElementById("bannerUpload").style.background =
        "url(" + this.bannerUri + ")";
    }
  }

  /**
   * constructor of the page
   * @param dialog
   * @param adminService
   */
  constructor(
    private dialog: MatDialog,
    private adminService: EsportsAdminService,
    private translateService: TranslateService
  ) {
    this.currentLang =
      translateService.currentLang == "ms" ? "malay" : "english";

    translateService.onLangChange.subscribe((event: LangChangeEvent) => {
      this.currentLang = event.lang == "ms" ? "malay" : "english";
    });
  }

  clickHandler(type, rowData) {
    if (type == "view") {
      this.ownerForm.setValue({
        globalCountryName: rowData.countryName.globalName,
        localCountryName: rowData.countryName.localName,
        globalRegionName: "",
        localRegionName: "",
      });
      const temp = [...rowData.regionName];
      this.addedRegions = temp;
      this._id = rowData._id;
      this.showAdList = false;
    } else if (type == "delete") {
      this.isLoading = true;
      this.adminService.updateRegion(API,rowData._id, { status: 0 }).subscribe(
        (res: any) => {
          this.isLoading = false;
          this.getRegions();
        },
        (err: any) => {
          this.isLoading = false;
        }
      );
    }
  }

  /**
   * Page Initialization
   */
  ngOnInit(): void {
    this.ownerForm = new FormGroup({
      globalCountryName: new FormControl("", [Validators.required]),
      localCountryName: new FormControl("", [Validators.required]),
      globalRegionName: new FormControl(""),
      localRegionName: new FormControl(""),
    });
    this.getRegions();
  }

  /**
   * method to get list of all the regions with active status
   */
  getRegions() {
    this.isLoading = true;
    this.adminService.getRegions(API).subscribe((res: any) => {
      this.regionList = [...res.data];
      this.isLoading = false;
      this.showAdList = true;
    });
  }

  /**
   * method to toggle the components on the page i.e. the table and the form
   */
  toggleBannerList() {
    this.resetRegionControls();
    this.showAdList = !this.showAdList;
    this.addedRegions = [];
  }

  removeRegion(index) {
    if (index > -1) {
      this.addedRegions.splice(index, 1);
    }
  }

  /**
   * method to be called before to reset the form controls
   */
  resetRegionControls() {
    this.addNewRegion = false;
    this.ownerForm.reset();
    this._id = 0;
  }

  addRegion() {
    this.addNewRegion = true;
    if (
      this.ownerForm.value.globalRegionName &&
      this.ownerForm.value.localRegionName
    ) {
      this.addedRegions.push({
        globalName: this.ownerForm.value.globalRegionName,
        localName: this.ownerForm.value.localRegionName,
      });
      this.ownerForm.controls["globalRegionName"].setValue("");
      this.ownerForm.controls["localRegionName"].setValue("");
    }
  }

  /**
   * method to check the form controls the validations
   * @param controlName
   * @param errorName
   */
  public hasError = (controlName: string, errorName: string) => {
    return this.ownerForm.controls[controlName].hasError(errorName);
  };

  addData() {
    if (this.ownerForm.invalid) {
      return;
    } else if (this.addedRegions.length == 0) {
      return;
    }
    this.isLoading = true;
    const formData = {
      countryName: {
        globalName: this.ownerForm.value.globalCountryName,
        localName: this.ownerForm.value.localCountryName,
      },
      regionName: this.addedRegions,
    };
    let subscriber;
    subscriber =
      this._id != 0
        ? this.adminService.updateRegion(API,this._id, formData)
        : this.adminService.addRegions(API,formData);
    subscriber.subscribe(
      (res: any) => {
        this.showAdList = true;
        this.isLoading = false;
        this.resetRegionControls();
        this.getRegions();
      },
      (err: any) => {
        this.isLoading = false;
      }
    );
  }
}
