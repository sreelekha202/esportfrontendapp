import { Component, OnInit } from "@angular/core";
import { EsportsAdminService } from 'esports';
import { environment } from '../../../../../environments/environment';
const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;
import {
  InfoPopupComponent,
  InfoPopupComponentData,
  InfoPopupComponentType,
} from "../../../../shared/popups/info-popup/info-popup.component";

import { MatDialog } from "@angular/material/dialog";
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: "app-leaderboard-configuration",
  templateUrl: "./leaderboard-configuration.component.html",
  styleUrls: ["./leaderboard-configuration.component.scss"],
})
export class LeaderboardConfigurationComponent implements OnInit {
  levelboard = [];
  levelState = 0;
  badgeState = 0;
  isLoading = false;
  levelObj: any = {};
  badgeObj: any = {};
  buttonTextArray = [
    "ADMIN.SITE_CONFIGURATION.BUTTON.LEADERBOARD_ADD_NEW",
    "ADMIN.SITE_CONFIGURATION.BUTTON.LEADERBOARD_SAVE",
  ];
  badgesboard = [];

  constructor(
    private adminService: EsportsAdminService,
    private dialog: MatDialog,
    private translateService: TranslateService
  ) {}

  ngOnInit(): void {
    this.getLeaderBoardData();
  }

  getLeaderBoardData() {
    this.isLoading = true;
    this.adminService.getLeaderboardConfig(API).subscribe(
      (res: any) => {
        this.isLoading = false;
        if (res.data[0] && res.data[0].leaderboardLevel) {
          this.levelboard = res.data[0].leaderboardLevel.map((ele) => {
            return {
              _id: ele._id,
              levelName: ele.levelName,
              code: ele.code,
              singleWinPoints: ele.singleWinPoints,
              rankThirdPoints: ele.rankThirdPoints,
              rankSecondPoints: ele.rankSecondPoints,
              rankFirstPoints: ele.rankFirstPoints,
              singleWinCoachPercent: ele.singleWinCoachPercent,
              rankThirdCoachPercent: ele.rankThirdCoachPercent,
              rankSecondCoachPercent: ele.rankSecondCoachPercent,
              rankFirstCoachPercent: ele.rankFirstPoints,
              createdBy: ele.createdBy,
              updatedBy: ele.updatedBy,
            };
          });
        }
        if (res.data[0] && res.data[0].leaderboardBadge) {
          this.badgesboard = res.data[0].leaderboardBadge.map((ele) => {
            return {
              _id: ele._id,
              badgeName: ele.badgeName,
              code: ele.code,
              minMatchWins: ele.minMatchWins,
              minLevel: ele.minLevel,
              minRank: ele.minRank,
              createdBy: ele.createdBy,
              updatedBy: ele.updatedBy,
            };
          });
        }
      },
      (err: any) => {
        this.isLoading = false;
        const afterBlockData: InfoPopupComponentData = {
          title: this.translateService.instant("API.PRODUCT.PUT.ERROR_HEADER"),
          text:err.error.message,
          type: InfoPopupComponentType.info,
        };
        this.dialog.open(InfoPopupComponent, { data: afterBlockData });
      }
    );
  }

  resetLevelBoard() {
    let tempData = this.levelboard.filter((item) => {
      return item && !item.newValue;
    });
    this.levelboard = [...tempData];
    this.levelState = 0;
  }

  resetBadgeBoard() {
    let tempData = this.badgesboard.filter((item) => {
      return item && !item.newValue;
    });
    this.badgesboard = [...tempData];
    this.badgeState = 0;
  }

  updateValue(event, col, row, type) {
    if (!row.newValue && event.target.value != row[col].toString()) {
      this.disableinputFields(true);
      let jsonData = { ...row };
      jsonData[col] = +event.target.value;
      this.isLoading = true;
      this.adminService.updateleaderboard(API,type, row._id, jsonData).subscribe(
        (res: any) => {
          this.disableinputFields(false);
          this.isLoading = false;
          this.getLeaderBoardData();
          const afterBlockData: InfoPopupComponentData = {
            title: this.translateService.instant(`ADMIN.SUCCESS.OK`),
            text:res?.message,
            type: InfoPopupComponentType.info,
          };
          this.dialog.open(InfoPopupComponent, { data: afterBlockData });
        },
        (err: any) => {
          this.disableinputFields(false);
          this.getLeaderBoardData();
          this.isLoading = false;
          const afterBlockData: InfoPopupComponentData = {
            title: this.translateService.instant(`ADMIN.ERROR.FAIL`),
            text:err.error.message,
            type: InfoPopupComponentType.info,
          };
          this.dialog.open(InfoPopupComponent, { data: afterBlockData });
        }
      );
    } else if (row.newValue) {
      if (type == "level") {
        this.levelObj[col] = event.target.value;
      } else if (type == "badge") {
        this.badgeObj[col] = event.target.value;
      }
    }
  }

  /**
   * toogle the input fields
   * @param type
   */
  disableinputFields(type: boolean) {
    let elements: any = document.getElementsByClassName(
      "LeaderboardConfigurations-boxB-item-input"
    );
    for (let i = 0; i < elements.length; i++) {
      elements[i].disabled = type;
    }
  }

  /**
   * mehtod to add the new row in the table
   */
  addNewLevelRow() {
    if (this.levelState == 0) {
      if (this.levelboard.length < 3) {
        this.levelObj = {
          levelName: null,
          code: null,
          singleWinPoints: null,
          rankThirdPoints: null,
          rankSecondPoints: null,
          rankFirstPoints: null,
          singleWinCoachPercent: null,
          rankThirdCoachPercent: null,
          rankSecondCoachPercent: null,
          rankFirstCoachPercent: null,
          newValue: true,
        };
        this.levelboard.push(this.levelObj);
        this.levelboard = [...this.levelboard];
        this.levelState = 1;
      } else {
        const afterBlockData: InfoPopupComponentData = {
          title: this.translateService.instant(
            `ADMIN.SITE_CONFIGURATION.SUB_PAGE.LEADERBOARD_CONFIG_LEVEL_COUNT_TITLE`
          ),
          text: this.translateService.instant(
            `ADMIN.SITE_CONFIGURATION.SUB_PAGE.LEADERBOARD_CONFIG_LEVEL_COUNT_TEXT`
          ),
          type: InfoPopupComponentType.info,
        };
        this.dialog.open(InfoPopupComponent, { data: afterBlockData });
      }
    } else if (this.levelState == 1) {
      if (this.levelObj.levelName) {
        this.isLoading = true;
        Object.keys(this.levelObj).forEach((item) => {
          if (item != "levelName") {
            this.levelObj[item] = +this.levelObj[item];
          }
        });
        this.adminService.addLeaderboard(API,"level", this.levelObj).subscribe(
          (res: any) => {
            this.isLoading = false;
            this.getLeaderBoardData();
            this.levelState = 0;
            const afterBlockData: InfoPopupComponentData = {
              title: this.translateService.instant(`ADMIN.SUCCESS.OK`),
              text:res?.message,
              type: InfoPopupComponentType.info,
            };
            this.dialog.open(InfoPopupComponent, { data: afterBlockData });
          },
          (err: any) => {
            const afterBlockData: InfoPopupComponentData = {
              title: this.translateService.instant(`ADMIN.ERROR.FAIL`),
              text:err.error.message,
              type: InfoPopupComponentType.info,
            };
            this.dialog.open(InfoPopupComponent, { data: afterBlockData });
            this.isLoading = false;
            this.getLeaderBoardData();
            this.levelState = 0;
          }
        );
      }
    }
  }

  onRemove(row, type) {
    const blockUserData: InfoPopupComponentData = {
      title: this.translateService.instant(
        "ADMIN.SITE_CONFIGURATION.SUB_PAGE.REMOVE_ITEM"
      ),
      text: this.translateService.instant(
        "ADMIN.SITE_CONFIGURATION.SUB_PAGE.REMOVE_ITEM_TEXT"
      ),
      type: InfoPopupComponentType.confirm,
      btnText: this.translateService.instant(
        "ADMIN.SITE_CONFIGURATION.BUTTON.CONFIRM"
      ),
    };
    const dialogRef = this.dialog.open(InfoPopupComponent, {
      data: blockUserData,
    });
    dialogRef.afterClosed().subscribe((confirmed) => {
      if (confirmed) {
        this.isLoading = true;
        this.adminService.deleteleaderboardItem(API,type, row._id).subscribe(
          (res: any) => {
            this.isLoading = false;
            type == "badge" ? (this.badgeState = 0) : (this.levelState = 0);
            const afterBlockData: InfoPopupComponentData = {
              title: this.translateService.instant(`ADMIN.SUCCESS.OK`),
              text:res?.message,
              type: InfoPopupComponentType.info,
            };
            this.dialog.open(InfoPopupComponent, { data: afterBlockData });
            this.getLeaderBoardData();
          },
          (err: any) => {
            const afterBlockData: InfoPopupComponentData = {
              title: this.translateService.instant(`ADMIN.ERROR.FAIL`),
              text:err.error.message,
              type: InfoPopupComponentType.info,
            };
            this.dialog.open(InfoPopupComponent, { data: afterBlockData });
            type == "badge" ? (this.badgeState = 0) : (this.levelState = 0);
            this.isLoading = false;
            this.getLeaderBoardData();
          }
        );
      }
    });
  }

  /**
   * mehtod to add the new row in the table
   */
  addNewBadgeRow() {
    if (this.badgeState == 0) {
      this.badgeObj = {
        badgeName: "",
        code: "",
        minMatchWins: "",
        minLevel: "",
        minRank: "",
        newValue: true,
      };
      this.badgesboard.push(this.badgeObj);
      this.badgesboard = [...this.badgesboard];
      this.badgeState = 1;
    } else {
      if (this.badgeObj.badgeName) {
        this.isLoading = true;
        Object.keys(this.badgeObj).forEach((item) => {
          if (item != "badgeName") {
            this.badgeObj[item] = +this.badgeObj[item];
          }
        });
        this.adminService.addLeaderboard(API,"badge", this.badgeObj).subscribe(
          (res: any) => {
            this.isLoading = false;
            this.badgeState = 0;
            this.getLeaderBoardData();
            const afterBlockData: InfoPopupComponentData = {
              title: this.translateService.instant(`ADMIN.SUCCESS.OK`),
              text:res?.message,
              type: InfoPopupComponentType.info,
            };
            this.dialog.open(InfoPopupComponent, { data: afterBlockData });
          },
          (err: any) => {
            this.isLoading = false;
            this.badgeState = 0;
            this.getLeaderBoardData();
            const afterBlockData: InfoPopupComponentData = {
              title: this.translateService.instant(`ADMIN.ERROR.FAIL`),
              text:err.error.message,
              type: InfoPopupComponentType.info,
            };
            this.dialog.open(InfoPopupComponent, { data: afterBlockData });
          }
        );
      }
    }
  }
}
