import { Component, OnInit, OnDestroy } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";

import { EsportsNotificationsService } from "esports";
import {
  InfoPopupComponentData,
  InfoPopupComponentType,
  InfoPopupComponent,
} from "../../../../../shared/popups/info-popup/info-popup.component";
import { TranslateService, LangChangeEvent } from "@ngx-translate/core";
import { MatDialog } from "@angular/material/dialog";
import { environment } from '../../../../../../environments/environment';
const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;

@Component({
  selector: "app-inbox-notifications",
  templateUrl: "./inbox-notifications.component.html",
  styleUrls: [
    "./inbox-notifications.component.scss",
    "../user-notifications.component.scss",
  ],
})
export class InboxNotificationsComponent implements OnInit, OnDestroy {
  inboxNotificationForm = this.fb.group({
    userSegment: ["", Validators.required],
    title: ["", Validators.required],
    message: ["", Validators.required],
  });
  isLoading = false;

  messageSent = false;
  // userSegementValues = {
  //   ALL_USERS: 'All users',
  //   PAYING_USERS: 'Paying users',
  //   TOURNAMENT_PLAYERS: 'Tournament players',
  //   TOURNAMENT_ORGANIZERS: 'Tournament organizers',
  //   RECENTLY_PURCHASED: 'Recently purchased',
  //   NO_PURCHASE_LAST_30_DAYS: 'No purchase in the last 30 days',
  // };
  userSegementValues = {
    ALL_USERS: this.translateService.instant(
      "ADMIN.USER_NOTIFICATION.LABEL.ALL_USERS"
    ),
    TOURNAMENT_PLAYERS: this.translateService.instant(
      "ADMIN.USER_NOTIFICATION.LABEL.TOURNAMENT_PLAYERS"
    ),
    TOURNAMENT_ORGANIZERS: this.translateService.instant(
      "ADMIN.USER_NOTIFICATION.LABEL.TOURNAMENT_ORGANIZER"
    ),
  };
  constructor(
    private fb: FormBuilder,
    private userNotificationsService: EsportsNotificationsService,
    private translateService: TranslateService,
    private dialog: MatDialog
  ) {
    translateService.onLangChange.subscribe((event: LangChangeEvent) => {
      this.userSegementValues = {
        ALL_USERS: this.translateService.instant(
          "ADMIN.USER_NOTIFICATION.LABEL.ALL_USERS"
        ),
        TOURNAMENT_PLAYERS: this.translateService.instant(
          "ADMIN.USER_NOTIFICATION.LABEL.TOURNAMENT_PLAYERS"
        ),
        TOURNAMENT_ORGANIZERS: this.translateService.instant(
          "ADMIN.USER_NOTIFICATION.LABEL.TOURNAMENT_ORGANIZER"
        ),
      };
    });
  }

  ngOnInit(): void {}

  ngOnDestroy(): void {
    this.resetControls();
  }

  selectType(e) {
    this.inboxNotificationForm.controls["userSegment"].setValue(e);
  }
  updateInbox() {
    if (!this.inboxNotificationForm.valid) {
      this.markFormGroupTouched(this.inboxNotificationForm);
      return;
    }
    if (!this.messageSent) {
      this.sendInboxMessages();
    }
  }
  toggleEditPane() {}
  resetControls() {
    this.isLoading = false;
    this.inboxNotificationForm.reset();
  }
  public hasError = (controlName: string, errorName: string) => {
    return this.inboxNotificationForm.controls[controlName].hasError(errorName);
  };

  markFormGroupTouched(formGroup: FormGroup) {
    (<any>Object).values(formGroup.controls).forEach((control) => {
      control.markAsTouched();

      if (control.controls) {
        this.markFormGroupTouched(control);
      }
    });
  }
  sendInboxMessages() {
    this.isLoading = true;
    this.messageSent = true;
    const inputData = {
      message: this.inboxNotificationForm.value.message,
      title: this.inboxNotificationForm.value.title,
      userSegment: this.inboxNotificationForm.value.userSegment,
    };
    this.userNotificationsService.sendInboxMessages(API,inputData).subscribe(
      (response: any) => {
        this.resetControls();
        this.isLoading = false;
        this.messageSent = false;
        const afterBlockData: InfoPopupComponentData = {
          title: this.translateService.instant(
            "ADMIN.USER_NOTIFICATION.RESPONSE.SUCCESS"
          ),
          text: response.message,
          type: InfoPopupComponentType.info,
        };
        this.dialog.open(InfoPopupComponent, { data: afterBlockData });
      },
      (err: any) => {
        this.isLoading = false;
        this.messageSent = false;
        const afterBlockData: InfoPopupComponentData = {
          title: this.translateService.instant(
            "ADMIN.USER_NOTIFICATION.RESPONSE.ERROR"
          ),
          text: err.error.message,
          type: InfoPopupComponentType.info,
        };
        this.dialog.open(InfoPopupComponent, { data: afterBlockData });
      }
    );
  }

  get userSegment() {
    return this.inboxNotificationForm.get("userSegment");
  }

  get message() {
    return this.inboxNotificationForm.get("message");
  }
}
