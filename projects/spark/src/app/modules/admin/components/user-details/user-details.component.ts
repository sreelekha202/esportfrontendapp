import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-user-details",
  templateUrl: "./user-details.component.html",
  styleUrls: ["./user-details.component.scss"],
})
export class UserDetailsComponent implements OnInit {
  rows = [
    {
      date: new Date(),
      fullName: "David Scott",
      email: "davidscoott@gmail.com",
      phoneNumber: "+919284847482",
      signUpFrom: "Gmail",
      amountSpend: "Rp. 450",
    },
    {
      id: 2,
      fullName: "MR. David Scott",
      email: "davidscoott@gmail.com",
      phoneNumber: "+919284847482",
      signUpFrom: "Gmail",
      amountSpend: "Rp. 400",
    },
    {
      id: 3,
      fullName: "Scott David",
      email: "davidscoott@gmail.com",
      phoneNumber: "+919284847482",
      signUpFrom: "Gmail",
      amountSpend: "Rp. 300",
    },
    {
      id: 4,
      fullName: "MR. David Scott",
      email: "davidscoott@gmail.com",
      phoneNumber: "+919284847482",
      signUpFrom: "Gmail",
      amountSpend: "Rp. 400",
    },
    {
      id: 5,
      fullName: "Scott David",
      email: "davidscoott@gmail.com",
      phoneNumber: "+919284847482",
      signUpFrom: "Gmail",
      amountSpend: "Rp. 300",
    },
    {
      id: 6,
      fullName: "MR. David Scott",
      email: "davidscoott@gmail.com",
      phoneNumber: "+919284847482",
      signUpFrom: "Gmail",
      amountSpend: "Rp. 400",
    },
    {
      id: 7,
      fullName: "Scott David",
      email: "davidscoott@gmail.com",
      phoneNumber: "+919284847482",
      signUpFrom: "Gmail",
      amountSpend: "Rp. 300",
    },
    {
      id: 8,
      fullName: "MR. David Scott",
      email: "davidscoott@gmail.com",
      phoneNumber: "+919284847482",
      signUpFrom: "Gmail",
      amountSpend: "Rp. 400",
    },
    {
      id: 9,
      fullName: "Scott David",
      email: "davidscoott@gmail.com",
      phoneNumber: "+919284847482",
      signUpFrom: "Gmail",
      amountSpend: "Rp. 300",
    },
  ];
  columns = [
    { name: "Date" },
    { name: "Item" },
    { name: "Purchase ID" },
    { name: "Payment" },
    { name: "Total" },
    { name: "Status" },
  ];

  constructor() {}

  ngOnInit(): void {}

  onSelect({ selected }) {}
}
