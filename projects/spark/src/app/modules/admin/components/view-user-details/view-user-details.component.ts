import { MatDialog } from '@angular/material/dialog';
import { Component, OnInit } from '@angular/core';
import { MatTabChangeEvent } from '@angular/material/tabs';
import {
  InfoPopupComponent,
  InfoPopupComponentData,
  InfoPopupComponentType,
} from '../../../../shared/popups/info-popup/info-popup.component';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { AppHtmlRoutes } from '../../../../app-routing.model';
import { TranslateService } from '@ngx-translate/core';
import {
  IPagination,
  IReward,
  EsportsUserService,
  EsportsConstantsService,
  EsportsTransactionService,
  EsportsTournamentService,
} from 'esports';

import { environment } from 'projects/paidia/src/environments/environment';
const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;
@Component({
  selector: 'app-view-user-details',
  templateUrl: './view-user-details.component.html',
  styleUrls: ['./view-user-details.component.scss'],
})
export class ViewUserDetailsComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;
  // rowsFirstTab = [];
  userId: string;
  userObject: any;
  rewardList: IReward[] = [];
  createdTournamentList: any[] = [];
  joinedTournamentList: any[] = [];
  isLoading = false;
  // Hide OR Show the button based on Role
  isUserActive = false;
  isUserVerified = false;
  isUserAuthor = false;
  isUserInfluencer = false;
  isEO = true;
  isUserMarkEO = false;
  page: IPagination;
  columnsFirstTab = [
    { name: 'Id' },
    { name: 'Full Name' },
    { name: 'Email' },
    { name: 'Phone Number' },
    { name: 'Sign Up From' },
    { name: 'Member Since' },
  ];
  rewardTransColumnTab = [
    { name: 'Date' },
    { name: 'Balance Reward' },
    { name: 'Reward Type' },
    { name: 'Description' },
    { name: 'Status' },
    { name: 'Expiry Date' },
  ];

  tournamentColumnTab = [
    { name: 'Tournament Name' },
    { name: 'Registration' },
    { name: 'Game' },
    { name: 'Region' },
    { name: 'Type' },
    { name: 'Start Date' },
    { name: '' },
  ];

  rows = [];
  purchase = {
    columns: [
      { name: 'Bill Number' },
      { name: 'Tournament Name' },
      { name: 'Provider' },
      { name: 'Entry Fee' },
      { name: 'Currency code' },
      { name: 'OrderId' },
      { name: 'Created Date' },
      { name: 'Status' },
    ],
  };
  transactionDetail;
  paginationData = {
    page: 1,
    limit: 5,
    sort: { createdOn: -1 },
  };

  constructor(
    public dialog: MatDialog,
    private location: Location,
    private activatedRoute: ActivatedRoute,
    private userService: EsportsUserService,
    private tournamentService: EsportsTournamentService,
    private translateService: TranslateService,
    private transactionServices: EsportsTransactionService
  ) {
    // this.rowsFirstTab = [...UserFirstTab];
  }

  ngOnInit(): void {
    this.getUserIdFromActiveRoute();
  }

  // Get the user details with all other tabs
  getUserIdFromActiveRoute() {
    this.activatedRoute.params.subscribe((params) => {
      this.userId = params['id'];
      if (this.userId) {
        this.getUserDetailsById();
      }
    });
  }

  getJoinedTournamentQuery() {
    return {
      pagination: JSON.stringify(this.pagination),
      query: JSON.stringify({
        userId: this.userId,
      }),
    };
  }
  pagination = {
    page: 1,
    limit: 50,
    sort: 'startDate',
    projection: [
      '_id',
      'name',
      'description',
      'slug',
      'startDate',
      'regionsAllowed',
      'tournamentType',
    ],
  };

  pageChanged(page): void {
    this.pagination.page = page;
    this.getJoinedTournamentDetails();
  }

  // Get Joined tournament details
  async getJoinedTournamentDetails() {
    this.isLoading = true;
    const payload = await this.getJoinedTournamentQuery();
    const tournament = await this.tournamentService
      .getJoinedTournamentsByUser(environment.apiEndPoint, payload)
      .toPromise();
    this.page = {
      totalItems: tournament?.data?.totalDocs,
      itemsPerPage: tournament?.data?.limit,
      maxSize: 5,
    };
    this.joinedTournamentList =
      tournament?.data?.docs.map(
        (element) => {
          return {
            id: element.tournament._id,
            tournamentName: element.tournament.name,
            slug: element.tournament.slug,
            registration: element.tournament.isPaid,
            game: element?.game?.name,
            region: element?.tournament?.regionsAllowed,
            type: element?.tournament?.tournamentType,
            startDate: element.tournament.startDate,
          };
        },
        (err) => {
          this.isLoading = false;
        }
      ) || [];
    this.isLoading = false;
  }

  // Get Created tournament details
  getCreatedTournamentDetails() {
    this.isLoading = true;
    const query = JSON.stringify({
      tournamentStatus: 'publish',
      createdBy: this.userId,
    });
    this.tournamentService.getTournaments(environment.apiEndPoint, { query: query }).subscribe(
      (data) => {
        this.isLoading = false;
        const tournamentDetails = data['data'].slice();
        const response = tournamentDetails.map((element) => {
          return {
            id: element?._id,
            tournamentName: element?.name,
            registration: element?.isPaid,
            slug: element?.slug,
            game: element?.gameDetail?.name,
            region: element?.regionsAllowed,
            type: element?.tournamentType,
            startDate: element?.startDate,
          };
        });
        this.createdTournamentList = response || [];
      },
      (err) => {
        this.isLoading = false;
      }
    );
  }
  getRewardTransaction() {
    this.isLoading = true;
    const query = JSON.stringify({ user: this.userId });
    this.userService
      .getRewardTransactionByUserId(API, { query: query })
      .subscribe(
        (res) => {
          this.isLoading = false;
          const response = res.data.map((element) => {
            if (element.status == 0) {
              status = this.translateService.instant(
                'PROFILE.TRANSACTIONS.REDEEMED'
              );
            } else if (element.status == 1) {
              status = this.translateService.instant(
                'PROFILE.TRANSACTIONS.NOT_REDEEMED'
              );
            } else if (element.status == 2) {
              status = this.translateService.instant(
                'PROFILE.TRANSACTIONS.PARTIAL_REDEEMED'
              );
            } else {
              status = this.translateService.instant(
                'PROFILE.TRANSACTIONS.EXPIRED'
              );
            }
            return {
              date: element.createdOn,
              rewardId: element._id,
              rewardType:
                element.type.charAt(0).toUpperCase() +
                element.type.slice(1).toLowerCase(),
              description: element.description,
              status: status,
              balanceReward: element.reward_value,
              expiryDate: element.expiredOn,
            };
          });
          this.rewardList = response || [];
        },
        (err) => {
          this.isLoading = false;
        }
      );
  }

  getUserDetailsById() {
    this.isLoading = true;
    this.userService.getUserDetails(API, this.userId).subscribe(
      (res) => {
        this.isLoading = false;
        this.userObject = res.data[0];

        if (this.userObject && this.userObject.Status) {
          this.isUserActive = this.userObject.Status === 1;
          this.isUserVerified = this.userObject.isVerified === 1;
          this.isUserInfluencer = this.userObject.isInfluencer === 1;
          this.isUserAuthor = this.userObject.isAuthor === 1;
          this.isUserMarkEO = this.userObject.isEO === 1;
        }
        this.getUserPrefenrece();
      },
      (err) => {
        this.isLoading = false;
      }
    );
  }

  getUserPrefenrece() {
    this.isLoading = true;
    // Get User Preferences
    this.userService.getUserPrefernces(API, this.userId).subscribe(
      (res) => {
        if (res && res.data) {
          this.getRewardTransaction();
          Object.assign(this.userObject, res.data);
        }
        this.isLoading = false;
      },
      (err) => {
        this.isLoading = false;
      }
    );
  }

  goBack() {
    this.location.back();
  }

  onBlockUser() {
    const blockUserData: InfoPopupComponentData = {
      title: this.translateService.instant(
        EsportsConstantsService.Model.Block_Confirmation.header
      ),
      text: this.translateService.instant(
        EsportsConstantsService.Model.Block_Confirmation.title
      ),
      type: InfoPopupComponentType.confirm,
      btnText: this.translateService.instant(
        EsportsConstantsService.Model.Block_Confirmation.button
      ),
    };
    const dialogRef = this.dialog.open(InfoPopupComponent, {
      data: blockUserData,
    });
    dialogRef.afterClosed().subscribe((confirmed) => {
      if (confirmed) {
        this.blockUser();
      }
    });
  }

  blockUser() {
    this.isLoading = true;
    const formValue = {
      Status: 2,
    };
    this.userService.updateUser(API, formValue, this.userId).subscribe(
      (res) => {
        this.isLoading = false;
        this.isUserActive = !this.isUserActive;
        const userName = this.userObject?.fullName;
        const afterBlockData: InfoPopupComponentData = {
          title: this.translateService.instant(
            EsportsConstantsService.Model.Block.title
          ),
          text: this.translateService.instant(
            EsportsConstantsService.Model.Block.description
          ),
          type: InfoPopupComponentType.info,
        };
        this.dialog.open(InfoPopupComponent, { data: afterBlockData });
      },
      (err) => {
        this.isLoading = false;
      }
    );
  }

  onUnBlockUser() {
    this.isLoading = true;
    const formValue = {
      Status: 1,
    };
    this.userService.updateUser(API, formValue, this.userId).subscribe(
      (res) => {
        this.isLoading = false;
        this.isUserActive = !this.isUserActive;
        const afterBlockData: InfoPopupComponentData = {
          title: this.translateService.instant(
            EsportsConstantsService.Model.Unblock.title
          ),
          text: this.translateService.instant(
            EsportsConstantsService.Model.Unblock.description
          ),
          type: InfoPopupComponentType.info,
        };
        this.dialog.open(InfoPopupComponent, { data: afterBlockData });
      },
      (err) => {
        this.isLoading = false;
      }
    );
  }

  /**
   * Method to mark as Influencer/remove influencer
   * @param boolVal : Mark user account as Influencer/UnInfluencer
   */
  onInfluencer(boolVal) {
    this.isLoading = true;
    const formValue = {
      isInfluencer: boolVal ? 0 : 1,
    };
    this.userService.updateUser(API, formValue, this.userId).subscribe(
      (res) => {
        this.isLoading = false;
        this.isUserInfluencer = !this.isUserInfluencer;
        if (boolVal) {
          this.SuccessModel(
            EsportsConstantsService.Model.UserInfluencer.title,
            EsportsConstantsService.Model.UserInfluencer.description
          );
        } else {
          this.SuccessModel(
            EsportsConstantsService.Model.UserUnInfluencer.title,
            EsportsConstantsService.Model.UserUnInfluencer.description
          );
        }
      },
      (err) => {
        this.isLoading = false;
      }
    );
  }

  onTabChanged = (tabChangeEvent: MatTabChangeEvent): void => {
    switch (tabChangeEvent.index) {
      case 0: {
        this.getRewardTransaction();
        break;
      }
      case 1: {
        this.getJoinedTournamentDetails();
        break;
      }
      case 2: {
        this.getCreatedTournamentDetails();
        break;
      }
      case 3: {
        this.getPaymentHistory();
        break;
      }
    }
  };

  /**
   * Method to verified/Unverified the account
   * @param boolVal : Mark user account ad verfied or unverified
   */
  onVerify(boolVal) {
    this.isLoading = true;
    const formValue = {
      isVerified: boolVal ? 0 : 1,
    };
    this.userService.updateUser(API, formValue, this.userId).subscribe(
      (res) => {
        this.isLoading = false;
        this.isUserVerified = !this.isUserVerified;
        if (boolVal) {
          this.SuccessModel(
            EsportsConstantsService.Model.UserVerified.title,
            EsportsConstantsService.Model.UserVerified.description
          );
        } else {
          this.SuccessModel(
            EsportsConstantsService.Model.UserUnVerified.title,
            EsportsConstantsService.Model.UserUnVerified.description
          );
        }
      },
      (err) => {
        this.isLoading = false;
      }
    );
  }

  onEO(boolVal) {
    this.isLoading = true;
    const formValue = {
      isEO: boolVal ? 0 : 1,
    };
    this.userService.updateUser(API, formValue, this.userId).subscribe(
      (res) => {
        this.isLoading = false;
        this.isUserMarkEO = !this.isUserMarkEO;
        if (boolVal) {
          this.SuccessModel(
            EsportsConstantsService.Model.UserMarkEO.title,
            EsportsConstantsService.Model.UserMarkEO.description
          );
        } else {
          this.SuccessModel(
            EsportsConstantsService.Model.UserUnMarkEO.title,
            EsportsConstantsService.Model.UserUnMarkEO.description
          );
        }
      },
      (err) => {
        this.isLoading = false;
      }
    );
  }

  /**
   * Method to mark as Author/UnAuthor
   * @param boolVal : Mark user account ad Author or Unauthor
   */
  onAuthor(boolVal) {
    this.isLoading = true;
    const formValue = {
      isAuthor: boolVal ? 0 : 1,
    };
    this.userService.updateUser(API, formValue, this.userId).subscribe(
      (res) => {
        this.isLoading = false;
        this.isUserAuthor = !this.isUserAuthor;
        if (boolVal) {
          this.SuccessModel(
            EsportsConstantsService.Model.UserAuthor.title,
            EsportsConstantsService.Model.UserAuthor.description
          );
        } else {
          this.SuccessModel(
            EsportsConstantsService.Model.RevokeAuthor.title,
            EsportsConstantsService.Model.RevokeAuthor.description
          );
        }
      },
      (err) => {
        this.isLoading = false;
      }
    );
  }

  /**
   * Model pop up
   * @param title : Display as header
   * @param text : Display as body text
   */
  SuccessModel(title, text) {
    const data: InfoPopupComponentData = {
      title: this.translateService.instant(title),
      text: this.translateService.instant(text),
      type: InfoPopupComponentType.info,
    };
    this.dialog.open(InfoPopupComponent, { data });
  }

  getPaymentHistory = async () => {
    const pagination = JSON.stringify(this.paginationData);
    const data = await this.transactionServices.paymentHistory(API, {
      userId: this.userId,
      pagination: pagination,
    });
    this.transactionDetail = data.data;
    this.rows = data.data['docs'];
    this.rows.forEach((obj, index) => {
      if (obj.txnId === 'na') {
        obj.txnId = 'NA';
      }
    });
    this.isLoading = false;
  };

  setPage(event) {
    this.paginationData.page = event.offset + 1;
    this.isLoading = true;
    this.rows.length = 0;
    this.getPaymentHistory();
  }
}
