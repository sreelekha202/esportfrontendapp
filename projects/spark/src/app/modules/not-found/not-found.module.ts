import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { CoreModule } from "../../core/core.module";
import { FooterSparkModule } from "../../shared/components/footer-spark/footer-spark.module";
import { SharedModule } from "../../shared/modules/shared.module";
import { NotfoundRoutingModule } from "./not-found-routing.module";
import { NotfoundComponent } from "./not-found.component";


const components = [
  NotfoundComponent
];

const modules = [
  RouterModule,
  SharedModule,
  CoreModule,
  NotfoundRoutingModule,
  FooterSparkModule,
];

@NgModule({
  declarations: components,
  imports: modules,
})
export class NotfoundModule { }
