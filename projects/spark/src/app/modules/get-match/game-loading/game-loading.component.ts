import { Subscription } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { AppHtmlGetMatchRoutes } from '../../../app-routing.model';
import { Router } from '@angular/router';
import {
  EsportsGameService, EsportsInfoPopupComponentData,
  EsportsInfoPopupComponentType,
  EsportsInfoPopupComponent,
} from 'esports';
import { TranslateService } from '@ngx-translate/core';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-game-loading',
  templateUrl: './game-loading.component.html',
  styleUrls: ['./game-loading.component.scss'],
})
export class GameLoadingComponent implements OnInit {
  AppHtmlGetMatchRoutes = AppHtmlGetMatchRoutes;
  showPopup = true;
  matchmakingDetails: Subscription;
  createMatchMaking: any;
  selectedGame: any = null;
  selectedPlatformDetails: any = null;

  constructor(
    private gameService: EsportsGameService,
    private router: Router,
    private translateService: TranslateService,
    private dialog: MatDialog,
  ) { }

  ngOnInit(): void {
    this.matchmakingDetails = this.gameService.createMatchMaking.subscribe(
      (data: any) => {
        if (data) {
          this.createMatchMaking = data;
          this.selectedGame = data.selectedGame;
        }
      }
    );
  }

  toMatchLobby() {
    this.router.navigateByUrl('/get-match/match-lobby');
  }
  PopupConfirm(type: boolean) {
    const blockData: EsportsInfoPopupComponentData = {
      title: type
        ? this.translateService.instant('MATCHMAKING.GAME_LOADING.POPUP.TITLE_POPUP')
        : this.translateService.instant('MATCHMAKING.GAME_LOADING.POPUP.TITLE_POPUP'),
      text: this.translateService.instant('MATCHMAKING.GAME_LOADING.POPUP.CONTENT'),
      type: EsportsInfoPopupComponentType.info,
      btnText: this.translateService.instant('BUTTON.ENTER'),
      cancelBtnText: this.translateService.instant('BUTTON.CANCEL'),
      isCancelledRequired: true
    };

    const dialogRef = this.dialog.open(EsportsInfoPopupComponent, {
      data: blockData,
    });

    dialogRef.afterClosed().subscribe((confirmed) => {
      if (confirmed) {
        this.router.navigate(['/play']);
      }
    });
  }
}
