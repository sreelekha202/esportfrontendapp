import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderMatchmakingComponent } from './header-matchmaking.component';

describe('HeaderMatchmakingComponent', () => {
  let component: HeaderMatchmakingComponent;
  let fixture: ComponentFixture<HeaderMatchmakingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HeaderMatchmakingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderMatchmakingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
