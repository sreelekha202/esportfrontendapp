import {
  Component,
  ElementRef,
  Inject,
  OnDestroy,
  OnInit,
  PLATFORM_ID,
  ViewChild,
} from '@angular/core';
import { DOCUMENT, isPlatformBrowser } from '@angular/common';
import { Router } from '@angular/router';
import { SwiperConfigInterface, SwiperComponent } from 'ngx-swiper-wrapper';
import { fromEvent, Subscription } from 'rxjs';
import { environment } from '../../../../environments/environment';
import {
  AppHtmlProfileRoutes,
  AppHtmlRoutes,
} from '../../../app-routing.model';
import { AppHtmlRoutesLoginType } from '../../../app-routing.model';
import { TranslateService } from '@ngx-translate/core';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';
import {
  faBars,
  faTimes,
  faSearch,
  faBell,
} from '@fortawesome/free-solid-svg-icons';
import { MatDialog } from '@angular/material/dialog';
import { SettingsLightboxComponent } from '../../../modules/settings-lightbox/settings-lightbox.component';
import {
  EsportsUserService,
  EsportsLanguageService,
  EsportsConstantsService,
  EsportsToastService,
  GlobalUtils,
  EsportsChatSidenavService,
  EsportsGameService,
  EsportsChatService,
  IUser,
} from 'esports';

const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;

enum AppLanguage {
  fr = 'fr',
  en = 'en',
}

@AutoUnsubscribe()
@Component({
  selector: 'app-header-matchmaking',
  templateUrl: './header-matchmaking.component.html',
  styleUrls: ['./header-matchmaking.component.scss'],
})
export class HeaderMatchmakingComponent implements OnInit, OnDestroy {
  @ViewChild(SwiperComponent, { static: false }) compRef?: SwiperComponent;
  games = [];
  selectedGameIndex: number = 0;
  public config: SwiperConfigInterface = {
    breakpoints: {
      320: {
        slidesPerView: 3,
        spaceBetween: 16,
        autoHeight: false,
      },
    },
  };
  public naviga: SwiperConfigInterface = {
    breakpoints: {
      375: {
        slidesPerView: 3,
        slidesPerColumn: 2,
        spaceBetween: 16,
        autoHeight: false,
      },
    },
  };
  createMatchMaking: any;
  matchmakingDetails: any;

  activeLang = this.constantsService?.defaultLangCode;
  AppHtmlProfileRoutes = AppHtmlProfileRoutes;
  AppHtmlRoutes = AppHtmlRoutes;
  AppHtmlRoutesLoginType = AppHtmlRoutesLoginType;

  collapsed: boolean = true;
  isAdmin: boolean = false;
  isBrowser: boolean;
  isUserLoggedIn: boolean = false;
  mobMenuOpened: boolean = false;
  mobMenuOpeneds: boolean = false;
  showLoader: boolean = false;

  currentUser: IUser;

  tournaments: any;
  matchlist: any;
  matchcount: number;
  chatWins = [];
  currentUserId: any;

  AppLanguage = [];

  faBell = faBell;
  faSearch = faSearch;
  faBars = faBars;
  faTimes = faTimes;

  navigation = [
    {
      title: 'HEADER.CONTENT',
      url: AppHtmlRoutes.content,
    },
    {
      title: 'HEADER.PLAY',
      url: AppHtmlRoutes.play,
    },
    {
      title: 'HEADER.STORE',
      url: AppHtmlRoutes.shop,
    },
    // {
    //   title: 'HEADER.MATCHMAKING',
    //   url: AppHtmlRoutes.matchmaking,
    // },
    // {
    //   title: 'HEADER.LEADERBOARD',
    //   url: AppHtmlRoutes.leaderBoard,
    // },
    // {
    //   title: 'HEADER.ABOUT',
    //   url: AppHtmlRoutes.aboutUs,
    // },
  ];

  userLinks = [
    {
      title: 'My Account',
      icon: 'person_outline',
      url: AppHtmlProfileRoutes.dashboard,
    },
    {
      title: 'My Teams',
      icon: 'people_outline',
      url: AppHtmlProfileRoutes.myTeams,
    },
    {
      title: 'My Stats',
      icon: 'show_chart',
      url: AppHtmlProfileRoutes.myStats,
    },
    {
      title: 'My Connections',
      icon: 'person_pin',
      url: AppHtmlProfileRoutes.connections,
    },
  ];

  private sub1: Subscription;
  userSubscription: Subscription;

  constructor(
    @Inject(PLATFORM_ID) private platformId,
    @Inject(DOCUMENT) private document: Document,
    private chatService: EsportsChatService,
    private chatSidenavService: EsportsChatSidenavService,
    private elementRef: ElementRef,
    private languageService: EsportsLanguageService,
    private router: Router,
    private userService: EsportsUserService,
    public toastService: EsportsToastService,
    public constantsService: EsportsConstantsService,
    public translate: TranslateService,
    public matDialog: MatDialog,
    private gameService: EsportsGameService
  ) {
    this.isBrowser = isPlatformBrowser(platformId);
  }

  ngOnInit(): void {
    this.matchmakingDetails = this.gameService.createMatchMaking.subscribe(
      (data) => {
        if (data) {
          this.createMatchMaking = data;
        }
      }
    );
    this.getGames();

    this.matchlist = [];
    this.activeLang = this.translate.currentLang;
    this.AppLanguage = this.constantsService?.language;
    this.matchcount = 0;

    if (
      this.isBrowser &&
      GlobalUtils.isBrowser() &&
      localStorage.getItem(environment.currentToken)
    ) {
      this.showLoader = true;

      this.userSubscription = this.userService.currentUser.subscribe(
        (data) => {
          if (data) {
            this.currentUser = data;
            this.isUserLoggedIn = !!this.currentUser;
            this.showLoader = false;

            this.currentUserId = this.currentUser._id;
            this.isAdmin =
              data?.accountType === EsportsConstantsService.AccountType.Admin;
            const accessToken = localStorage.getItem(environment.currentToken);

            this.chatService?.login.subscribe((data) => {
              // if (data.accessToken != 'undefined' && data.accessToken != null) {
              this.chatService?.connectUser();

              this.chatService.unreadCount.subscribe((unreadCount) => {
                this.matchcount = unreadCount;
              });

              this.chatService?.getUnReadCount().subscribe((res: any) => {});
              // }
            });
          } else {
            this.userService.getAuthenticatedUser(API, TOKEN);
          }
        },
        (error) => {
          this.showLoader = false;
          this.toastService.showError(error.message);
        }
      );
    }
    this.onHeaderScroll();
  }

  ready() {}

  ngOnDestroy() {
    this.userSubscription?.unsubscribe();
    if (this.currentUserId) {
      this.chatService?.disconnectUser();
    }
    if (this.matchmakingDetails) this.matchmakingDetails.unsubscribe();
  }

  onChatSidenavToggle(): void {
    this.chatSidenavService.toggle();

    if (this.currentUserId) {
      this.chatService?.getAllMatches(this.currentUserId, 1, 30);
    }

    if (this.chatSidenavService.checkOpen()) {
      if (this.chatService.getChatStatus()) {
        this.chatService.setChatStatus(false);
      }
    } else {
      if (this.chatService.getChatStatus()) {
        this.chatService.setChatStatus(false);
      }
    }
  }

  onLogOut(): void {
    if (GlobalUtils.isBrowser()) {
      this.chatService.setChatStatus(false);
      this.userService.logout(API, TOKEN);
      this.chatSidenavService.close();
      this.chatService.doLogout();
      this.chatService?.disconnectUser();
    }

    this.router.navigate([
      AppHtmlRoutes.userPageType,
      AppHtmlRoutesLoginType.emailLogin,
    ]);
  }

  onLanguageChange(lang): void {
    if (GlobalUtils.isBrowser()) {
      localStorage.setItem('currentLanguage', lang);
      this.activeLang = lang;
      this.translate.use(lang);
      this.languageService.setLanguage(lang);
    }
  }

  private onHeaderScroll(): void {
    if (GlobalUtils.isBrowser()) {
      const scrollingContent = document.querySelector('mat-sidenav-content');

      if (scrollingContent) {
        const header = this.elementRef.nativeElement;

        this.sub1 = fromEvent(scrollingContent, 'scroll').subscribe(
          (e: Event) => {
            if ((e.target as Element).scrollTop > header.clientHeight) {
              header.classList.add('is-scrolling');
            } else {
              header.classList.remove('is-scrolling');
            }
          }
        );
      }
    }
  }

  onScrollBody() {
    const hiddenOrVisible = this.mobMenuOpened ? 'hidden' : 'visible';
    this.document.body.style.overflow = hiddenOrVisible;
  }

  openSettingsLightbox(selectedTab = 0) {
    const data = { selectedTab };
    this.matDialog.open(SettingsLightboxComponent, {
      data,
      position: {
        top: '34px',
      },
    });
  }

  getGames() {
    this.showLoader = true;
    this.gameService.getGames(API).subscribe(
      (res) => {
        this.games = res.data;
        this.showLoader = false;
      },
      (err) => {
        this.showLoader = false;
      }
    );
  }

  isMatchmakingRoute() {
    return this.router.url === '/get-match';
  }

  isHomeRoute() {
    return this.router.url === '/';
  }

  isLeaderboardRoute() {
    return this.router.url === '/leaderboard';
  }

  prevSlide() {
    this.compRef.directiveRef.prevSlide();
  }

  nextSlide() {
    this.compRef.directiveRef.nextSlide();
  }

  onGameSelect(selectedGame) {
    if (selectedGame) {
      this.gameService.createMatchMakingSubject.next({
        ...this.createMatchMaking,
        selectedGame: selectedGame,
      });
      this.router.navigateByUrl('/get-match');
    }
  }
}
