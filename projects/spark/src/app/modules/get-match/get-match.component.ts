import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppHtmlRoutes, AppHtmlGetMatchRoutes } from '../../app-routing.model';
import { EsportsGameService,GlobalUtils } from 'esports';
import { environment } from '../../../environments/environment';
const API = environment.apiEndPoint;
@Component({
  selector: 'app-get-match',
  templateUrl: './get-match.component.html',
  styleUrls: ['./get-match.component.scss'],
})
export class GetMatchComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;
  AppHtmlGetMatchRoutes = AppHtmlGetMatchRoutes;
  matchmakingDetails: any;
  selectedGame: any;
  showLoader: boolean = false;
  selectedGameLogo: any;
  gameID: string;
  constructor(private gameService: EsportsGameService, public router: Router) {

    if (GlobalUtils.isBrowser()) {
      this.gameID = localStorage.getItem('gameId');
    }
  }

  ngOnInit(): void {
    // this.matchmakingDetails = this.gameService.createMatchMaking.subscribe(
    //   (data: any) => {
    //     if (data) {
    //       this.selectedGame = data.selectedGame;
    //     }
    //   }
    // );
    // this.checkData()
    // MOCK SELECTED GAME
    // this.selectedGame = {
    //   image:
    //     'https://i114.fastpic.ru/big/2021/0613/ea/1362412cbb852fd522e0ef0b781cafea.jpg',
    //   icon: 'https://i114.fastpic.ru/big/2021/0614/06/c2780510c7ee16fc8f04b5dc1403b306.png',
    //   name: 'Dota 2',
    // };
    this.getGames();
  }
  getGames() {
    // this.showLoader = true;
    this.gameService.getGames(API).subscribe(
      (res) => {
        this.showLoader = false;
        this.selectedGame = res?.data;
        this.selectedGame.forEach(selectGame => {
          if(selectGame?._id == this.gameID){
            this.selectedGameLogo = selectGame;
          }
        });
      },
      (err) => {
        this.showLoader = false;
      }
    );
  }
  next() {
    // if () {
    //   this.gameService.createMatchMakingSubject.next({ ...this.createMatchMaking, "selectedGame": selectedGame })
    //   this.router.navigateByUrl("/get-match")
    // }
  }
  checkData() {
    if (this.selectedGame) return;
    return this.router.navigateByUrl('/matchmaking');
  }
  isGetReason() {
    return this.router.url === '/get-match/season';
  }
  isSelectPlatformsRoute() {
    return this.router.url === '/get-match/platforms';
  }

  isTeamGameRoute() {
    return this.router.url === '/get-match/team-game';
  }

  isGameLoadingRoute() {
    return this.router.url === '/get-match/game-loading';
  }

  isMatchDetailRoute() {
    return this.router.url === '/get-match/match-detail';
  }

  isMatchMakingRoute() {
    return (
      this.router.url === '/get-match/platforms' ||
      this.router.url === '/get-match/platforms-single' ||
      this.router.url === '/get-match/team-game' ||
      this.router.url === '/get-match/game-loading' ||
      this.router.url === '/get-match/casual-matchmaking' ||
      this.router.url === '/get-match/match-lobby' ||
      this.router.url === '/get-match/match-lobby-api' ||
      this.router.url === '/get-match/match-lobby-non-api' ||
      this.router.url === '/get-match/match-detail' ||
      this.router.url === '/get-match/match-completed' ||
      this.router.url === '/get-match/tournament-scoring' ||
      this.router.url === '/get-match/update-score' ||
      this.router.url === '/get-match/scoring-success' ||
      this.router.url === '/get-match/season/select-team'
    );
  }

  isGetMatch() {
    return (
      this.router.url === '/get-match' ||
      this.router.url === '/get-match/matchmaking'
    );
  }

  isGetMatchRoute() {
    return (
      this.router.url === '/get-match' ||
      this.router.url === '/get-match/season' ||
      this.router.url === '/get-match/season/detail' ||
      this.router.url === '/get-match/matchmaking' ||
      this.router.url === '/get-match/season/select-team'
    );
  }
  isFooter() {
    return (
      this.router.url === '/get-match/matchmaking' ||
      this.router.url === '/get-match/season' ||
      this.router.url === '/get-match/season/detail'
    );
  }
  isBgImageGame() {
    return (
      this.router.url === '/get-match/match-lobby' //||
      // this.router.url === '/get-match/match-completed'
    );
  }
}
