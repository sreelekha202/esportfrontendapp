import { Component, ElementRef, HostListener, OnInit, ViewChild } from '@angular/core';
import { AppHtmlGetMatchRoutes } from '../../../app-routing.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-casual-matchmaking',
  templateUrl: './casual-matchmaking.component.html',
  styleUrls: ['./casual-matchmaking.component.scss']
})
export class CasualMatchmakingComponent implements OnInit {
  @ViewChild('popup') popup: ElementRef;
  @ViewChild('btnShowPopup') btnShowPopup: ElementRef;
  @ViewChild('btnShowPopupfoot') btnShowPopupfoot: ElementRef;
  AppHtmlGetMatchRoutes = AppHtmlGetMatchRoutes;
  show = false;
  contentChat = false;
  constructor(private router: Router,) { }

  ngOnInit(): void {
  }

  @HostListener('document:click',['$event'])
  clickOutside(event){
    if( !(this.popup.nativeElement.contains(event.target)) && this.show === true && !(this.btnShowPopup.nativeElement.contains(event.target)) && !(this.btnShowPopupfoot.nativeElement.contains(event.target))){
        this.show = false;
    }
  }

}
