import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CasualMatchmakingComponent } from './casual-matchmaking.component';

describe('CasualMatchmakingComponent', () => {
  let component: CasualMatchmakingComponent;
  let fixture: ComponentFixture<CasualMatchmakingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CasualMatchmakingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CasualMatchmakingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
