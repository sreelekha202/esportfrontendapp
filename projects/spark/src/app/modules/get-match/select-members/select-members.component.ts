import { Component, OnInit } from '@angular/core';
import { AppHtmlGetMatchRoutes } from '../../../app-routing.model';

@Component({
  selector: 'app-select-members',
  templateUrl: './select-members.component.html',
  styleUrls: ['./select-members.component.scss'],
})
export class SelectMembersComponent implements OnInit {
  AppHtmlGetMatchRoutes = AppHtmlGetMatchRoutes;

  constructor() {}

  ngOnInit(): void {}
}
