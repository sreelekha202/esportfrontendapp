import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MatchLobbyNonApiComponent } from './match-lobby-non-api.component';

describe('MatchLobbyNonApiComponent', () => {
  let component: MatchLobbyNonApiComponent;
  let fixture: ComponentFixture<MatchLobbyNonApiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MatchLobbyNonApiComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MatchLobbyNonApiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
