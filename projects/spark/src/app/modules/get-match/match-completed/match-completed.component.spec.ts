import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MatchCompletedComponent } from './match-completed.component';

describe('MatchCompletedComponent', () => {
  let component: MatchCompletedComponent;
  let fixture: ComponentFixture<MatchCompletedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MatchCompletedComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MatchCompletedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
