import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MatchLobbyApiComponent } from './match-lobby-api.component';

describe('MatchLobbyApiComponent', () => {
  let component: MatchLobbyApiComponent;
  let fixture: ComponentFixture<MatchLobbyApiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MatchLobbyApiComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MatchLobbyApiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
