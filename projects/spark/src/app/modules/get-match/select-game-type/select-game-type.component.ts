import { Subscription } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { AppHtmlGetMatchRoutes } from '../../../app-routing.model';
import { Router } from '@angular/router';
import { EsportsToastService, EsportsGameService, GlobalUtils } from 'esports';
import { TranslateService } from '@ngx-translate/core';
import { environment } from '../../../../environments/environment';
const API = environment.apiEndPoint;

@Component({
  selector: 'app-select-game-type',
  templateUrl: './select-game-type.component.html',
  styleUrls: ['./select-game-type.component.scss'],
})
export class SelectGameTypeComponent implements OnInit {
  AppHtmlGetMatchRoutes = AppHtmlGetMatchRoutes;
  gameTypes: any = [];
  selectedGameType: any;
  selectedGame: any;
  gameList: any;
  gameID: string;
  showLoader: boolean;
  selectedGameLogo: any;
  constructor(
    private translateService: TranslateService,
    private router: Router,
    private gameService: EsportsGameService,
    private toastService: EsportsToastService
  ) {
    if (GlobalUtils.isBrowser()) {
      this.gameID = localStorage.getItem('gameId');
    }
  }
  setGameType() {
    this.gameTypes.push({
      type: 'individual',
      icon: 'assets/icons/matchmaking/game-type/single-type.svg',
      title: 'MATCHMAKING.SELECT_PLATFORM.GAME_TYPES.SINGLE',
    });

    this.gameTypes.push({
      type: 'team',
      icon: 'assets/icons/matchmaking/game-type/team-type.svg',
      title: 'MATCHMAKING.SELECT_PLATFORM.GAME_TYPES.TEAMS',
    });
  }

  ngOnInit(): void {
    this.showLoader = true;
    this.setGameType();
    this.getGames();
  }

  getGames() {
    this.gameService.getGames(API).subscribe(
      (res) => {
        this.showLoader = false;
        this.gameList = res.data;
        this.selectedGame = res?.data;
        this.selectedGame.forEach(selectGame => {
          if(selectGame?._id == this.gameID){
            this.selectedGameLogo = selectGame;
          }
        });
        this.gameList.map((game) => {
          game._id == this.gameID ? (this.selectedGame = game) : '';
        });
      },
      (err) => {
        this.showLoader = false;
      }
    );
  }

  nextPage(game) {
    this.selectedGameType = game.type;
    if (this.selectedGameType) {
      if (game.type == 'team') {
        this.router.navigateByUrl('/get-match/platforms');
      } else {
        this.router.navigateByUrl('/get-match/platforms-single');
      }
    } else {
      this.toastService.showError(
        this.translateService.instant('GAMES.MATCHMAKING.STEP4.TITLE2')
      );
    }
  }
}
