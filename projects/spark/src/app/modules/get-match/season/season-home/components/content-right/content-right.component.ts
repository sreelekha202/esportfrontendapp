import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  OnChanges,
} from '@angular/core';
import { EsportsSeasonService } from 'esports';
import { environment } from '../../../../../../../environments/environment';

const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;
@Component({
  selector: 'app-content-right',
  templateUrl: './content-right.component.html',
  styleUrls: ['./content-right.component.scss'],
})
export class ContentRightComponent implements OnInit, OnChanges {
  @Output() getValueData = new EventEmitter();
  @Input() seasonCheck = [];
  seasonTypeSingle = false;
  seasonTypeTeam = false;
  data = [];
  seasonCkeck = [
    {
      id: 1,
      name: 'Single player',
      check: false,
      isShowIcon: true,
      season: 'season',
    },
    {
      id: 2,
      name: 'Team',
      check: false,
      isShowIcon: true,
      season: 'season',
    },
    {
      id: 3,
      name: 'Single players',
      check: false,
      isShowIcon: true,
      season: 'length',
    },
    {
      id: 4,
      name: 'Teams',
      check: false,
      isShowIcon: true,
      season: 'length',
    },
    {
      id: 5,
      name: 'PC',
      check: false,
      isShowIcon: true,
      season: 'platform',
    },
    {
      id: 6,
      name: 'Console',
      check: false,
      isShowIcon: true,
      season: 'platform',
    },
    {
      id: 7,
      name: 'Mobile',
      check: false,
      isShowIcon: true,
      season: 'platform',
    },
    {
      id: 8,
      name: 'Competitive',
      check: false,
      isShowIcon: true,
      season: 'mode',
    },
    {
      id: 9,
      name: 'Casual',
      check: false,
      isShowIcon: true,
      season: 'mode',
    },
    {
      id: 10,
      name: 'Deathmatch',
      check: false,
      isShowIcon: true,
      season: 'mode',
    },
    {
      id: 11,
      name: 'Arms Race',
      check: false,
      isShowIcon: true,
      season: 'mode',
    },
    {
      id: 12,
      name: 'Demolition',
      check: false,
      isShowIcon: true,
      season: 'mode',
    },
    {
      id: 13,
      name: 'Wingman',
      check: false,
      isShowIcon: true,
      season: 'mode',
    },
    {
      id: 14,
      name: 'Retakes',
      check: false,
      isShowIcon: true,
      season: 'mode',
    },
    {
      id: 15,
      name: 'Danger Zone',
      check: false,
      isShowIcon: true,
      season: 'mode',
    },
    {
      id: 16,
      name: 'Flying Scoutsman',
      check: false,
      isShowIcon: true,
      season: 'mode',
    },
  ];
  showLoader: boolean = true;
  seasonsList = [];
  constructor(private esportsSeasonService: EsportsSeasonService,) {}

  ngOnInit(): void {}


  getAllSeasons() {
    let query: any = `isActive=true&fields=isFeatured,name,slug,endDate,startDate,image,duration&sort=-createdOn&page=1&limit=8`;
    this.esportsSeasonService.getSeasons(API, query).subscribe(
      (res) => {
        this.showLoader = false;
        this.seasonsList = res.data.data.docs;
      (err) => {
        this.showLoader = false;
      }
      });
  }


  checkSeasonType(type) {
    if (type == 'single') {
      if (!this.seasonTypeSingle) {
        this.seasonTypeTeam = false;
      }
    } else {
      if (!this.seasonTypeTeam) {
        this.seasonTypeSingle = false;
      }
    }
  }
  onChangeSeason(event, id) {
    const index = this.seasonCkeck.findIndex((item) => {
      return item.id === id;
    });
    if (index === -1) {
      return;
    }
    this.seasonCkeck[index] = {
      ...this.seasonCkeck[index],
      check: event.checked,
    };

    this.getValueData.emit(this.seasonCkeck);
  }

  ngOnChanges(): void {
    if (this.seasonCheck.length === 0) return;
    this.seasonCkeck = this.seasonCheck;
  }
}
