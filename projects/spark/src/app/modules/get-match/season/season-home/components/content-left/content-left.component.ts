import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { EsportsGameService, EsportsSeasonService, GlobalUtils, IPagination } from 'esports';
import { environment } from '../../../../../../../environments/environment';
const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;
@Component({
  selector: 'app-content-left',
  templateUrl: './content-left.component.html',
  styleUrls: ['./content-left.component.scss'],
})
export class ContentLeftComponent implements OnInit {
  @Input() seasonData = [];
  isShown: boolean = false;
  @Output() getValueData = new EventEmitter();
  showLoader: boolean = true;
  selectedGame: any;
  params: any = {};
  gameId: any;
  game: any;
  text: string = '';
  selected_game: any = '';
  seasonsList = [];
  showFilter = true;
  paginationData = {
    page: 1,
    limit: 10,
    sort: 'startDate',
  };
  totalDocs:any;
  paginationDetails: any;
  pageSizeOptions = environment.pageSizeOptions;
  listFilter = [
    {
      id: 1,
      content: 'mobile',
      isShowIcon: true,
    },
    {
      id: 2,
      content: 'console',
      isShowIcon: true,
    },
    {
      id: 3,
      content: 'Team',
      isShowIcon: true,
    },
  ];
  filterGame = [
    {
      fieldGame: 'title',
      filterName: 'Sort by Title',
    },
    {
      fieldGame: 'name',
      filterName: ' Sort by Name',
    },
    {
      fieldGame: 'date',
      filterName: 'Sort by Date',
    },
    {
      fieldGame: 'player',
      filterName: 'Sort by Number Player',
    },
  ];
  itemGame = [
    {
      src: '../../../../../../../assets/images/season/image 59.png',
      title: 'CS:GO Chapter 2 - Monthly Ladder',
      date: 'End Monday, December 11, 15:00 • Elimination',
      srcPlayer: '../../../../../../../assets/images/Profile/img2.png',
      name: 'By jagong_gong',
      player: 231,
    },
    {
      src: '../../../../../../../assets/images/season/image 59 (1).png',
      title:
        'Long title example. Title should not exceed two line. Truncation will set to handle text overflow.',
      date: 'End Monday, December 11, 15:00 • Elimination',
      srcPlayer: '../../../../../../../assets/images/Profile/img2.png',
      name: 'By jagong_gong',
      player: 200,
    },
    {
      src: '../../../../../../../assets/images/season//image 59 (2).png',
      title: 'CS:GO Chapter 2 - Monthly Ladder',
      date: 'End Monday, December 11, 15:00 • Elimination',
      srcPlayer: '../../../../../../../assets/images/Profile/img2.png',
      name: 'By jagong_gong',
      player: 211,
    },
    {
      src: '../../../../../../../assets/images/season/image 59 (3).png',
      title: 'CS:GO Chapter 2 - Monthly Ladder',
      date: 'End Monday, December 11, 15:00 • Elimination',
      srcPlayer: '../../../../../../../assets/images/Profile/img2.png',
      name: 'By jagong_gong',
      player: 210,
    },
    {
      src: '../../../../../../../assets/images/season/image 59 (2).png',
      title: 'CS:GO Chapter 2 - Monthly Ladder',
      date: 'End Monday, December 11, 15:00 • Elimination',
      srcPlayer: '../../../../../../../assets/images/Profile/img2.png',
      name: 'By jagong_gong',
      player: 220,
    },
  ];
  page: IPagination;
  participantPage: IPagination = {
    activePage: 1,
    totalItems: 1,
    maxSize: 10,
    itemsPerPage: 10,
  };
  constructor(    private gameService: EsportsGameService,
    private esportsSeasonService: EsportsSeasonService,
    public router: Router,) {}

  ngOnInit(): void {
    if (GlobalUtils.isBrowser()) {
      this.gameId = localStorage.getItem('gameId');
      
      if (this.gameId) {

        this.getAllSeasons();
      }
    }
    
  }

  showFilterFC() {
    this.seasonData = this.seasonData.map((item) => {
      return {
        ...item,
        check: false,
      };
    });
    this.getValueData.emit(this.seasonData);
  }
  removeSeason(item) {
    const index = this.seasonData.findIndex((each) => {
      return item.name === each.name;
    });
    if (index === -1) return;
    this.seasonData[index] = {
      ...this.seasonData[index],
      check: !this.seasonData[index].check,
    };
    this.getValueData.emit(this.seasonData);
  }


  getAllSeasons() {
    let query: any = `isActive=true&fields=isFeatured,name,slug,endDate,startDate,image,duration&sort=-createdOn&limit=${this.participantPage.itemsPerPage}&page=${this.participantPage.activePage}`;
    this.esportsSeasonService
      .getSeasons(
        API,
        query
      )
      .subscribe((res) => {
        this.seasonsList = res?.data?.data?.docs;
        this.totalDocs = res?.data?.data?.totalDocs
        this.participantPage.itemsPerPage = res?.data?.data?.limit;
        this.participantPage.totalItems = res?.data?.data?.totalDocs;
      });
  }
  currentPage(page): void {
    this.participantPage.activePage = page;
    this.getAllSeasons();
  }
  showPopup() {
    this.isShown = true;
  }
  hidePopup() {
    this.isShown = false;
  }
  sort(item) {
    if (item?.fieldGame === 'player') {
      return this.sortByPlayer();
    }
    const newPlayers = this.itemGame.sort((a, b) => {
      return (
        a[item.fieldGame].codePointAt(0) - b[item.fieldGame].codePointAt(0)
      );
    });
    this.itemGame = newPlayers;
  }
  sortByPlayer() {
    const newPlayers = this.itemGame.sort((a, b) => {
      return a.player - b.player;
    });
    this.itemGame = newPlayers;
  }
}
