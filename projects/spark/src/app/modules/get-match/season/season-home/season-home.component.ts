import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { EsportsGameService, EsportsSeasonService, GlobalUtils } from 'esports';
import { environment } from '../../../../../environments/environment';
import { NgbCarousel } from '@ng-bootstrap/ng-bootstrap';
const API = environment.apiEndPoint;
@Component({
  selector: 'app-season-home',
  templateUrl: './season-home.component.html',
  styleUrls: ['./season-home.component.scss'],
})
export class SeasonHomeComponent implements OnInit {
  @ViewChild('ngcarousel', {
    static: true,
  })
  ngCarousel: NgbCarousel;
  @Input() data: any;
  gameId: any;
  seasonCheck = [];
  isClear = false;
  totalDocs: any;
  isShowSlider = false;
  showLoader: boolean = false;
  selectedGame: any;
  selectedGameLogo: any;
  constructor(
    private router: Router,
    private esportsSeasonService: EsportsSeasonService,
    private gameService: EsportsGameService
  ) { }
  seassons = [];

  ngOnInit(): void {
    if (GlobalUtils.isBrowser()) {
      this.gameId = localStorage.getItem('gameId');
    }
    this.getSeasons();
    this.getGames();
  }
  getGames() {
    // this.showLoader = true;
    this.gameService.getGames(API).subscribe(
      (res) => {
        this.showLoader = false;
        this.selectedGame = res?.data;
        this.selectedGame.forEach(selectGame => {
          if(selectGame?._id == this.gameId){
            this.selectedGameLogo = selectGame;
          }
        });
      },
      (err) => {
        this.showLoader = false;
      }
    );
  }
  getSeasons() {
    this.showLoader = true;
    this.esportsSeasonService
      .getSeasons(
        API,
        'isActive=true&fields=isFeatured,name,slug,endDate,startDate,image,duration&sort=-createdOn&page=1&limit=8'
      )
      .subscribe((res) => {
        this.seassons = res?.data?.data?.docs;
        this.showLoader = false;
      });
  }

  getToPrev() {
    this.ngCarousel.prev();
  }

  goToNext() {
    this.ngCarousel.next();
  }

  viewDtail() {
    this.router.navigateByUrl('/games/match-season/');
  }
  getValueData(event) {
    this.seasonCheck = event;
  }
  isClearFilter(event) {
    this.seasonCheck = event;
  }
}
