import { Component, OnInit } from '@angular/core';
import { AppHtmlGetMatchRoutes, AppHtmlRoutes } from '../../../app-routing.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-update-scoring-success',
  templateUrl: './update-scoring-success.component.html',
  styleUrls: ['./update-scoring-success.component.scss']
})
export class UpdateScoringSuccessComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;
  AppHtmlGetMatchRoutes = AppHtmlGetMatchRoutes;

  constructor() { }

  ngOnInit(): void {
  }

}
