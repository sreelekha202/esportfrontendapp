import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateScoringSuccessComponent } from './update-scoring-success.component';

describe('UpdateScoringSuccessComponent', () => {
  let component: UpdateScoringSuccessComponent;
  let fixture: ComponentFixture<UpdateScoringSuccessComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UpdateScoringSuccessComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateScoringSuccessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
