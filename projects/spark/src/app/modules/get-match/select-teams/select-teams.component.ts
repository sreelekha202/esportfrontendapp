import { Component, OnInit } from '@angular/core';
import { AppHtmlGetMatchRoutes } from '../../../app-routing.model';

@Component({
  selector: 'app-select-teams',
  templateUrl: './select-teams.component.html',
  styleUrls: ['./select-teams.component.scss'],
})
export class SelectTeamsComponent implements OnInit {
  AppHtmlGetMatchRoutes = AppHtmlGetMatchRoutes;

  teams = [];

  constructor() {}

  ngOnInit(): void {
    // MOCK TEAMS DATA
    this.teams = [
      {
        image:
          'https://i.pinimg.com/736x/f9/a8/c0/f9a8c0734c8e13adaa0d39fdf81274c2.jpg',
        name: 'Dark Ninja',
      },
      {
        image:
          'https://i.pinimg.com/originals/60/95/51/6095510eaff84568bf69dc0217f6e8b8.png',
        name: 'Brightforces',
      },
    ];
  }
}
