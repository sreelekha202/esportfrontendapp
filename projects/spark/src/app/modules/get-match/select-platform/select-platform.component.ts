import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { AppHtmlGetMatchRoutes } from '../../../app-routing.model';
import { Router } from '@angular/router';
import {
  EsportsToastService,
  GlobalUtils,
  EsportsGameService,
  EsportsSeasonService,
} from 'esports';
import { environment } from '../../../../environments/environment';
import { TranslateService } from '@ngx-translate/core';
const API = environment.apiEndPoint;

@Component({
  selector: 'app-select-platform',
  templateUrl: './select-platform.component.html',
  styleUrls: ['./select-platform.component.scss'],
  host: { class: 'full-height' },
})
export class SelectPlatformComponent implements OnInit {
  AppHtmlGetMatchRoutes = AppHtmlGetMatchRoutes;
  @ViewChild('team') team: ElementRef;
  // isLoaded=true;
  selectedPlatform = null;
  selectedGameType = null;
  selectedPlatformName = null;
  selectedTeam = null;
  subModule = null;
  platforms = [];
  selectedGame: any = null;
  selectedPlatformDetails: any = null;
  showLoader: boolean;
  popupCreateTeam = false;

  teams = [];
  gameList: any;
  gameID: any;

  constructor(
    private gameService: EsportsGameService,
    private esportsSeasonService: EsportsSeasonService,
    private router: Router,
    private toastService: EsportsToastService,
    private translateService: TranslateService
  ) {
    if (GlobalUtils.isBrowser()) {
      this.gameID = localStorage.getItem('gameId');
    }
  }

  ngOnInit(): void {
    // this.teams = [
    // {
    //   image:
    //     'https://i.pinimg.com/736x/f9/a8/c0/f9a8c0734c8e13adaa0d39fdf81274c2.jpg',
    //   name: 'Dark Ninja',
    // },
    // ];
    this.getGames();
  }

  getGames() {
    this.showLoader = true;
    this.gameService.getGames(API).subscribe(
      (res) => {
        this.showLoader = false;
        this.gameList = res.data;
        this.gameList.map((game) => {
          if (game._id == this.gameID) {
            this.selectedGame = game;
            this.selectedGame.platform && this.selectedGame.platform.length > 0
              ? this.setPlatForm()
              : '';
          }
        });
      },
      (err) => {
        this.showLoader = false;
      }
    );
  }

  checkRouter() {
    return this.router.url === '/get-match/platforms-single';
  }
  setPlatForm() {
    this.selectedGame.platform.map((obj) => {
      if (obj.name == 'pc') {
        this.platforms.push({
          ...obj,
          type: 'pc',
          icon: 'assets/icons/matchmaking/platforms/pc.svg',
          title: 'MATCHMAKING.SELECT_PLATFORM.PLATFORMS.PC',
        });
      } else if (obj.name == 'Mobile') {
        this.platforms.push({
          ...obj,
          type: 'mobile',
          icon: 'assets/icons/matchmaking/platforms/mobile.svg',
          title: 'MATCHMAKING.SELECT_PLATFORM.PLATFORMS.MOBILE',
        });
      } else if (obj.name == 'Other') {
        this.platforms.push({
          ...obj,
          type: 'console',
          icon: 'assets/icons/matchmaking/platforms/console.svg',
          title: 'MATCHMAKING.SELECT_PLATFORM.PLATFORMS.CONSOLE',
        });
      } else if (obj.name == 'Xbox') {
        this.platforms.push({
          ...obj,
          type: 'xbox',
          icon: 'assets/icons/matchmaking/platforms/xbox.svg',
          title: 'MATCHMAKING.SELECT_PLATFORM.PLATFORMS.XBOX',
        });
      } else if (obj.name == 'PS4') {
        this.platforms.push({
          ...obj,
          type: 'ps4',
          icon: 'assets/icons/matchmaking/platforms/ps4.svg',
          title: 'MATCHMAKING.SELECT_PLATFORM.PLATFORMS.PS4',
        });
      } else if (obj.name == 'PS5') {
        this.platforms.push({
          ...obj,
          type: 'ps5',
          icon: 'assets/icons/matchmaking/platforms/ps5.svg',
          title: 'MATCHMAKING.SELECT_PLATFORM.PLATFORMS.PS5',
        });
      } else {
        this.platforms.push({
          ...obj,
          type: 'pc',
          icon: 'assets/icons/matchmaking/platforms/pc.svg',
          title: 'MATCHMAKING.SELECT_PLATFORM.PLATFORMS.PC',
        });
      }
    });
  }

  selectPlatformType(platform) {
    this.selectedPlatform = platform.type;
    this.selectedPlatformName = platform.name;
    this.selectedPlatformDetails = platform;
  }

  // Notes : for Html/css Desigener
  // don't create new component all ready logical code compnent is avilable
  // redesign the exisiting compnents.

  // nextPage() {
  //   if (this.selectedPlatformDetails) {
  //     if (this.checkRouter()) {
  //       this.gameService.createMatchMakingSubject.next({
  //         selectedPlatform: this.selectedPlatformDetails,
  //         selectedGame: this.selectedGame,
  //         selectedGameType: 'individual',
  //       });
  //       this.router.navigateByUrl('/get-match/game-loading');
  //     } else {
  //       this.gameService.createMatchMakingSubject.next({
  //         selectedPlatform: this.selectedPlatformDetails,
  //         selectedGame: this.selectedGame,
  //         selectedGameType: 'team',
  //       });
  //       this.router.navigateByUrl('/get-match/team-game');
  //     }
  //   } else {
  //     this.toastService.showError('Select platforms.');
  //   }
  // }

  next() {
    if (this.selectedGame && this.selectedPlatformDetails) {
      let type = this.checkRouter() ? 'individual' : 'team';
      this.gameService.createMatchMakingSubject.next({
        selectedPlatform: this.selectedPlatformDetails,
        selectedGame: this.selectedGame,
        selectedGameType: type,
      });
      const params = {
        game: this.selectedGame._id,
        platform: this.selectedPlatformDetails._id,
        type: type,
      };
      // version 2.0 (Matchmaking)
      this.showLoader = true;
      this.esportsSeasonService.checkSeasons(API, params).subscribe(
        (res) => {
          this.showLoader = false;
          if (res?.data?.data?.season.length > 0) {
            this.toastService.showSuccess(res?.message);
            if (res?.data?.data?.participant) {
              this.router.navigate(
                ['/games/match-season/' + res?.data?.data?.season[0]?._id],
                { queryParams: { playnow: 'true' } }
              );
            } else {
              if (res?.data?.data?.season[0]?.participantType === 'team') {
                this.router.navigate(
                  [
                    `/team-registration/${res?.data?.data?.season[0]?.slug}-${res?.data?.data?.season[0]?._id}`,
                  ],
                  {
                    queryParams: { isSeason: 'true' },
                  }
                );
              } else {
                this.router.navigate([
                  `/tournament/${res?.data?.data?.season[0]?.slug}-${res?.data?.data?.season[0]?._id}/join`,
                ]);
              }
            }
          } else {
            this.toastService.showError(res?.message);
          }
        },
        (err) => {
          this.showLoader = false;
          this.toastService.showError(err.error.message);
        }
      );
    }

    else if(!this.selectedPlatformDetails){
      this.toastService.showError(
        this.translateService.instant(
          'MATCHMAKING.ERROR'
        )
      );
    }
  }

  activateClass(team) {
    team.active = !team.active;
  }
}
