import { Component, OnInit } from '@angular/core';
import { AppHtmlGetMatchRoutes, AppHtmlRoutes } from '../../../app-routing.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tour-scoring-update-score',
  templateUrl: './tour-scoring-update-score.component.html',
  styleUrls: ['./tour-scoring-update-score.component.scss']
})
export class TourScoringUpdateScoreComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;
  AppHtmlGetMatchRoutes = AppHtmlGetMatchRoutes;

  constructor(private router: Router) { }

  ngOnInit(): void {
  }
  endSession(){
    return this.router.navigateByUrl('/get-match/matchmaking')
  }
  backToLobby(){
    return this.router.navigateByUrl('/get-match/match-lobby-non-api')
  }
}
