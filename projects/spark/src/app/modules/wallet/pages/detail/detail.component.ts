import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EsportsUserService, EsportsWalletService } from 'esports';
import { environment } from '../../../../../environments/environment';
import { ActivatedRoute } from '@angular/router';
import { Location } from "@angular/common";
const API = environment.apiEndPoint;

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {

// @Input() showDatas
showDatas:any
  cardLink = environment.cardLink;
  referralID: string;
  referralLink: string = '';
  currentUser: any;
  url = this.router.url;

  constructor(
    private userService: EsportsUserService,
    private router: Router,
    private location: Location,
    private activatedRoute: ActivatedRoute,
    private walletService: EsportsWalletService,
  ) {
    this.userService.currentUser.subscribe((res) => {
      if (res) {
        this.currentUser = res;
        this.referralLink =
          this.cardLink + this.currentUser.accountDetail.referralId;
        this.referralID = this.currentUser.accountDetail.referralId;

      }
    });
  }

  ngOnInit(): void {
    this.activatedRoute.queryParams.subscribe(params=>{
      if(params){
        this.showDatas=params
        this.setTransactionType()
      }
    })
  }


  setTransactionType() {
    let tAtionType = '';
    if (this.showDatas.paymentMethod == 'Online banking') {
      tAtionType = 'Reload';
    } else if (this.showDatas.paymentMethod == 'Credit/Debit card') {
      tAtionType = 'Reload';
    } else if (this.showDatas.paymentMethod == 'DCB') {
      tAtionType = 'Reload';
    } else if (this.showDatas.paymentMethod == 'Cashback') {
      tAtionType = 'Cashback';
    } else {
      tAtionType = 'Purchase';
    }
    this.showDatas['transactionType'] = tAtionType;
  }

  userClicked(event: any) {
    var text = document.getElementById('textClipboard');
    var wrapText = document.getElementById('wrapText');
    var icon = document.getElementById('icon');
    text.textContent = 'Linked copied';
    wrapText.classList.add('coppied');
    icon.textContent = 'done';
  }
  walletReload() {
    return this.router.url === '/wallet/detail-reload';
  }
  Back(){
    this.location.back();
}
}
