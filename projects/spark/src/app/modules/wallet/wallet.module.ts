import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { EsportsCustomPaginationModule, EsportsLoaderModule } from 'esports';
import { CoreModule } from '../../core/core.module';
import { FooterSparkModule } from '../../shared/components/footer-spark/footer-spark.module';
import { SharedModule } from '../../shared/modules/shared.module';
import { PopupConfirmPurchaseComponent } from './components/popup-confirm-purchase/popup-confirm-purchase.component';
import { PurchaseSummaryComponent } from './components/purchase-summary/purchase-summary.component';
import { ReloadComponent } from './components/reload/reload.component';
import { TransactionDetailComponent } from './components/transaction-detail/transaction-detail.component';
import { DetailComponent } from './pages/detail/detail.component';
import { WalletRoutingModule } from './wallet-routing.module';
import { WalletComponent } from './wallet.component';


@NgModule({
  declarations: [
    WalletComponent,
    TransactionDetailComponent,
    PurchaseSummaryComponent,
    DetailComponent,
    ReloadComponent,
    PopupConfirmPurchaseComponent
  ],
  imports: [
    SharedModule,
    RouterModule,
    CoreModule,
    WalletRoutingModule,
    FooterSparkModule,
    EsportsLoaderModule.setColor('#00a851'),
    EsportsCustomPaginationModule,
  ],
  providers: [
  ],
})
export class WalletModule { }
