import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DetailComponent } from './pages/detail/detail.component';
import { WalletComponent } from './wallet.component';


const routes: Routes = [
  {
    path: '',
    component: WalletComponent,
  },
  {
    path: 'detail',
    component: DetailComponent,
  },
  {
    path: 'detail-reload',
    component: DetailComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class WalletRoutingModule { }
