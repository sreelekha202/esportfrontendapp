import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { EsportsProfileService, EsportsToastService, GlobalUtils,EsportsWalletService, IPagination } from 'esports';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';
import { Subscription } from 'rxjs/Subscription';
import { environment } from './../../../environments/environment';

@AutoUnsubscribe()
@Component({
  selector: 'app-wallet',
  templateUrl: './wallet.component.html',
  styleUrls: ['./wallet.component.scss']
})
export class WalletComponent implements OnInit,OnDestroy {
  showLoader: boolean = true;
  selectedItem: boolean = true;
  active = 1;
  chips = [
    { _id: 1, name: 'Show all', active: true },
    { _id: 2, name: 'Wallet reload', active: false },
    { _id: 3, name: 'Direct-top up', active: false },
    { _id: 4, name: 'Vouchers', active: false },
  ];
  showReload = false;
  showConfirm = false;

  isLoaded: boolean = false;
  tableFlag = true;
  showAllData: any = [];
  showAllDataPurchase: any = [];
  totalRecorders = 100;
  showData: any;
  viewReload: boolean = false;
  viewPurchase: boolean = false;
  pageSizeOptions = environment.pageSizeOptions;
  walletSubscription: Subscription;

  isReloadTab: boolean = true;
  isPurchasesTab: boolean = false;
  purchaseHistory: any;
  rewardseHistory: any;
  nextId: number = 1;
  pagination: any;
  paginationParams: any;
  showAllDataPurchase2: any = [];
  walletDetail
  selectedData:any;
  topUpParchaseDetail:any=[]
  voucherParchaseDetail:any=[]
  showAllDataResults:any=[]
  pagenum=1;
  pagesize=10;
  showAllPagination=false;

  participantPage: IPagination = {
    activePage: 1,
    totalItems: 1,
    maxSize: 10,
    itemsPerPage: 12,
  };

  constructor(
    private eSportsToastService: EsportsToastService,
    private translateService: TranslateService,
    private walletService: EsportsWalletService,
    private esportsProfileService:EsportsProfileService,
    private activatedRoute:ActivatedRoute,
    private router:Router,

  ) { }

  ngOnInit(): void {
    this.walletService.loadWalletDetails()
    this.getPurchaseTransactions(this.participantPage?.activePage, this.participantPage?.itemsPerPage);
    this.walletService.recordsLoaded.subscribe(res=>{
      if(res){
        this.walletDetail=res
      }
    })

  }

  chipsClick(e) {
    const index = this.chips.findIndex((item) => {
      return item._id === e._id
    })

    if (index === -1)
      return
    this.chips = this.chips.map((item) => {
      return (
        {
          ...item,
          active: false
        }
      )
    })
    this.chips[index] = {
      ...this.chips[index],
      active: true
    }
  }
  showPopup($event) {
    this.showReload = true;
  }
  hidePopupReload(selectedData) {
    this.selectedData=selectedData
    this.showReload = false;
    this.showConfirm = true;
    this.eSportsToastService.showSuccess(
      this.translateService.instant('Reload')
    );
  }
  hidePopupConfirm() {
    this.showLoader=false;
    this.showConfirm = false;
  }

  getPurchaseTransactions(pagenum, pagesize) {
    // this.showAllPagination=false
    this.walletService
      .getPurchaseTransactions(pagenum, pagesize)
      .subscribe((data: any) => {
        // this.showAllDataPurchase = data.data.docs;
        this.showAllDataPurchase2 = data?.data?.docs;

        this.participantPage.totalItems=data?.data?.totalDocs;
       // this.showAllPagination=true
        this.showAllDataPurchase2.forEach((parchaseDetail,i) => {
           if(parchaseDetail.category=='TOPUP'){
                this.topUpParchaseDetail.push(parchaseDetail)
           }
           if(parchaseDetail.category=='VOUCHER'){
            this.voucherParchaseDetail.push(parchaseDetail)
          }
        });

        this.totalRecorders = data?.data?.totalDocs;
        this.isLoaded = true;
      });

      this.getReloadTransactions(pagenum, pagesize);
  }

  ngOnDestroy(): void {
  }

  getReloadTransactions(pagenum, pagesize) {
    this.isReloadTab = true;
    this.isPurchasesTab = false;
    sessionStorage.setItem('flagforpruchase', '0');
    this.walletService
      .gettransactionswithpagination(pagenum, pagesize)
      .subscribe((data: any) => {
        this.showAllData = data?.data?.docs;
        if(this.showAllData && this.showAllDataPurchase2){
          this.showAllDataResults = [ ...this.showAllData,  ...this.showAllDataPurchase2];
          //this.showAllDataResults = Object.assign(this.showAllData, this.showAllDataPurchase2)
        }

        for (let purchaseDetail of this.showAllData) {
          if (purchaseDetail.paymentMethod == '10' || purchaseDetail.paymentMethod == 'Online banking') {
            purchaseDetail.type = 'Online Banking';
          }
          if (
            purchaseDetail.paymentMethod == '6' ||
            purchaseDetail.paymentMethod == 'Credit/Debit card'
          ) {
            purchaseDetail.type = 'Credit/Debit Card';
          }
          if (purchaseDetail.paymentMethod == 'DCB') {
            purchaseDetail.type = 'Pay with Maxis';
          }
          if (purchaseDetail.paymentMethod == 'Cashback') {
            purchaseDetail.type = 'Cashback';
          }
        }
        this.isLoaded = true;
        this.totalRecorders = data.data.totalDocs;
        this.participantPage.totalItems= data.data.totalDocs;
        this.scroll();
      });
  }


  scroll() {
    if (GlobalUtils.isBrowser()) {
      window.scrollTo({
        top: 0,
        left: 0,
        // behavior: 'smooth',
      });
    }
  }



  addItem(newItem: any) {
    this.showdetailfun(newItem);
  }

  showdetailfun(data) {
    if (GlobalUtils.isBrowser()) {
      window.scroll(0, 0);
    }
    this.showData = data;
    this.tableFlag = false;
    this.viewReload = true;
    this.viewPurchase = false;
    this.setTransactionType();
  }


  go(reloadPurchaseHystory){
    this.router.navigate(['detail']);

  }
  setTransactionType() {
    let tAtionType = '';
    if (this.showData.paymentMethod == 'Online banking') {
      tAtionType = 'Reload';
    } else if (this.showData.paymentMethod == 'Credit/Debit card') {
      tAtionType = 'Reload';
    } else if (this.showData.paymentMethod == 'DCB') {
      tAtionType = 'Reload';
    } else if (this.showData.paymentMethod == 'Cashback') {
      tAtionType = 'Cashback';
    } else {
      tAtionType = 'Purchase';
    }
    this.showData['transactionType'] = tAtionType;
  }
  viewPurchaseDetail(purchaseHystory){
    this.router.navigate(['wallet/detail'], purchaseHystory);
  }


  addItem1(newItem: any) {
    this.showdetailfun1(newItem);
  }

  showdetailfun1(data) {

    if (GlobalUtils.isBrowser()) {
      window.scroll(0, 0);
    }

    this.showData = data;
    this.tableFlag = false;
    this.viewPurchase = true;
    this.viewReload = false;
    this.setTransactionType();
  }


  currentPage1(page): void {
    this.participantPage.activePage = Number(page);
    this.getPurchaseTransactions( this.participantPage.activePage, this.participantPage?.itemsPerPage);
  }

  currentPage(page): void {
    this.participantPage.activePage = Number(page);
    this.getReloadTransactions( this.participantPage.activePage, this.participantPage?.itemsPerPage);
  }

}
