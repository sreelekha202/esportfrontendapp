import { Component, Input, OnInit } from '@angular/core';
import { EsportsUserService, GlobalUtils } from 'esports';


@Component({
  selector: 'app-transaction-detail',
  templateUrl: './transaction-detail.component.html',
  styleUrls: ['./transaction-detail.component.scss']
})
export class TransactionDetailComponent implements OnInit {
@Input()showdata

currenUser
isPrint: boolean = false;

  constructor(
    public userService: EsportsUserService) { }

  ngOnInit(): void {
      this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.currenUser = data;
      }
    });
  }


  myPrint() {
    if (GlobalUtils.isBrowser()) {
      this.isPrint = true;
      setTimeout(() => {
        var divContents = document.getElementById('myElementId').innerHTML;
        var printWindow = window.open('', '', 'height=800,width=800');
        printWindow.document.write('<html><head><title>Print</title>');
        printWindow.document.write('</head><body >');
        // printWindow.document.write(`${printCSS}`);
        printWindow.document.write(divContents);
        printWindow.document.write('</body></html>');
        printWindow.document.close();
        printWindow.print();
      }, 200);
      this.isPrint = false;
    }
  }

}
