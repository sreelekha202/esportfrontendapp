import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PopupConfirmPurchaseComponent } from './popup-confirm-purchase.component';

describe('PopupConfirmPurchaseComponent', () => {
  let component: PopupConfirmPurchaseComponent;
  let fixture: ComponentFixture<PopupConfirmPurchaseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PopupConfirmPurchaseComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PopupConfirmPurchaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
