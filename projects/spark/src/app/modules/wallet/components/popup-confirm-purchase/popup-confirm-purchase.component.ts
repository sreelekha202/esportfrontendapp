import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { EsportsWalletService } from 'esports';

@Component({
  selector: 'app-popup-confirm-purchase',
  templateUrl: './popup-confirm-purchase.component.html',
  styleUrls: ['./popup-confirm-purchase.component.scss']
})
export class PopupConfirmPurchaseComponent implements OnInit {
  @Output() hidePopupIssue = new EventEmitter();
  @Input() selectedData
  isShow = true;
  showLoader=false;
  constructor(private walletService: EsportsWalletService) { }

  ngOnInit(): void {
  }
  closePopup() {
    this.isShow = false;
    this.hidePopupIssue.emit()
  }
  confirm() {
    this.isShow=!this.isShow
    let data={
     billPrice:this.selectedData?.topUpValue
    }
    this.showLoader=true

this.walletService.getPayMark(data).subscribe((res)=>{
  if(res){
    this.showLoader=false
   window.location.href = res?.data
  }
})

  }
  confirmPopup2(){
    this.hidePopupIssue.emit()
  }
}
