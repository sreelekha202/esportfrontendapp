import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { WalletService } from 'projects/maxis/src/app/core/service/wallet.service';



@Component({
  selector: 'app-reload',
  templateUrl: './reload.component.html',
  styleUrls: ['./reload.component.scss']
})
export class ReloadComponent implements OnInit {
  @Output() hidePopup = new EventEmitter();
  isShow = true
  show = false;
  showCustomTournamentEndDate = false;
  dataTournamentTypeValue = 0;
  showAmount = false;
  dataAmountValue = 0;
  dataAmount: any=[];
  selectedData:any
  indexValue:any
  isSelected=false
  //  = [
  //   {
  //     id: 0,
  //     textTitle: '$5',
  //     textDes: '500 Credit',
  //   },
  //   {
  //     id: 1,
  //     textTitle: '$10',
  //     textDes: '1,000 Credit',
  //   },
  //   {
  //     id: 2,
  //     textTitle: '$25',
  //     textDes: '2,500 Credit',
  //   },
  //   {
  //     id: 3,
  //     textTitle: '$75',
  //     textDes: '7,500 Credit',
  //   },
  //   {
  //     id: 4,
  //     textTitle: '$100',
  //     textDes: '10,000 Credit',
  //   },
  //   {
  //     id: 5,
  //     textTitle: '$150',
  //     textDes: '15,000 Credit',
  //   },
  //   {
  //     id: 6,
  //     textTitle: '$250',
  //     textDes: '25,000 Credit',
  //   },

  // ];
  dataTournamentType = [
    {
      id: 0,
      textTitle: 'Charge to Telecom billing',
      textDes: 'Charge to bill/Spark credit',
    },
    {
      id: 1,
      textTitle: 'Credit and Debit Cards',
      textDes: '',
    },
    {
      id: 2,
      textTitle: 'Online Banking',
      textDes: '',
    },
  ];
  datafortopupmethod: any;
  constructor(private walletService: WalletService) { }

  ngOnInit(): void {
    this.getWallettopupamount();
  }

  clickChecked(checked) {
    this.dataTournamentTypeValue = checked.id;
    checked.ichecked = true;
    if (this.dataTournamentTypeValue == 2) {
      this.showCustomTournamentEndDate = true;
    }
  }
  clickAmountChecked(active,i) {
    this.indexValue=i
    this.isSelected=true;
    this.dataAmountValue = active.id;
    this.selectedData=active
    active.ichecked = true;
    if (this.dataAmountValue == 0) {
      this.showAmount = true;
    }
  }

  showPopup() {
    this.show = !this.show;
    this.hidePopup.emit(this.selectedData)
  }
  hidePopupC($event) {
    this.show = false;
  }
  closePopup(){
    this.isShow = !this.isShow;
  }

  getWallettopupamount(){
    this.walletService.getWallettopupamount().subscribe(
      (data: any) => {
        this.dataAmount = data?.data;
        for (let data of this.dataAmount) {
          data.selectflag = false;
        }
        this.getWallettopupmethods();
      },
      (error) => {}
    );
  }

  getWallettopupmethods() {
    this.walletService.getWallettopupmethods().subscribe(
      (data: any) => {
        this.datafortopupmethod = data?.data;
        let flag = true;
        for (let data of this.datafortopupmethod) {
          data.paymentMethod == 'DCB' ? (flag = false) : '';
          data.selectflag = false;
        }
      });
  }

}
