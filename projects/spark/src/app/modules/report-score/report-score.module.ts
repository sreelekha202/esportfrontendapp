import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReportScoreComponent } from './report-score.component';
import { Routes, RouterModule } from '@angular/router';
import { HeaderSparkModule } from '../../shared/components/header-spark/header-spark.module';
import { CoreModule } from '../../core/core.module';
import { PopupCreatedComponent } from './components/popup-created/popup-created.component';
import { TeamBannerComponent } from './components/team-banner/team-banner.component';
import { TeamCardComponent } from './components/team-card/team-card.component';
import { ResultOverviewComponent } from './components/result-overview/result-overview.component';
import { SharedModule } from '../../shared/modules/shared.module';
import { NewReportScoreComponent } from './components/new-report-score/new-report-score.component';
import { EsportsLoaderModule } from 'esports';

export const routes: Routes = [
  {
    path: '',
    component: ReportScoreComponent,
  },
  {
    path: ':matchId',
    component: NewReportScoreComponent,
  },
];

@NgModule({
  declarations: [
    ReportScoreComponent,
    PopupCreatedComponent,
    TeamBannerComponent,
    TeamCardComponent,
    ResultOverviewComponent,
    NewReportScoreComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    HeaderSparkModule,
    CoreModule,
    RouterModule.forChild(routes),
    EsportsLoaderModule.setColor('#00a851'),
  ],
})
export class ReportScoreModule {}
