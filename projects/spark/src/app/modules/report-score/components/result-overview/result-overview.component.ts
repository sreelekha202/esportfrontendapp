import { Component, OnInit, Input } from '@angular/core';
import { GlobalUtils } from 'esports';

@Component({
  selector: 'app-result-overview',
  templateUrl: './result-overview.component.html',
  styleUrls: ['./result-overview.component.scss'],
})
export class ResultOverviewComponent implements OnInit {
  @Input() data;
  constructor() {}
  tReport: any;
  ngOnInit(): void {
    if (GlobalUtils.isBrowser()) {
      this.tReport = JSON.parse(localStorage.getItem('t_report'));
    }
  }
}
