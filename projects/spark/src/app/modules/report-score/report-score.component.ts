import {
  Component,
  EventEmitter,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { AppHtmlProfileRoutes } from '../../app-routing.model';
import { PopupCreatedComponent } from './components/popup-created/popup-created.component';
import {
  EsportsLanguageService,
  EsportsToastService,
  EsportsUtilsService,
  GlobalUtils,
  EsportsBracketService
} from 'esports';
import { Router } from '@angular/router';

@Component({
  selector: 'app-report-score',
  templateUrl: './report-score.component.html',
  styleUrls: ['./report-score.component.scss'],
})
export class ReportScoreComponent implements OnInit, OnChanges {
  enableWebview: boolean = false;
  isAdmin: boolean;
  isScreenshotRequired: boolean = false;
  match: any;
  participantId;
  scoreReporting: number = 2;
  token;
  tournament: any;
  isShowTitle: boolean = true;
  isShowChat: boolean = true;
  isShowReportBtn: boolean = true;
  exit = new EventEmitter<any>();
  isLoaded: boolean = false;
  screenShot: string;
  matchDetails: any;
  participantSet: any;
  disQualifyStatus: Array<string> = [
    'No show',
    'Walkout',
    'Rule violation',
    'Improper behavior',
  ];
  teamList: Array<any> = [];
  disQualifiedTeam: any = {
    reason: '',
    team: null,
  };
  tReport: any;

  active = 1;
  nextId: number = 1;
  AppHtmlProfileRoutes = AppHtmlProfileRoutes;
  isupdatedscore = false;
  teamName: string = '';
  constructor(
    private bracketService: EsportsBracketService,
    private languageService: EsportsLanguageService,
    private modalService: NgbModal,
    public matDialog: MatDialog,
    private toastService: EsportsToastService,
    private translateService: TranslateService,
    private utilsService: EsportsUtilsService,
    private router: Router
  ) {}

  ngOnInit(): void {
    if (GlobalUtils.isBrowser()) {
      this.languageService.language.subscribe((lang) =>
        this.translateService.use(lang || 'en')
      );
      this.match = this.tReport?.match;
      this.tReport = JSON.parse(localStorage.getItem('t_report'));
      this.participantId = this.tReport?.participantId;
      this.tournament = this.tReport?.tournamentId;
      this.isShowTitle = false;
      this.isShowReportBtn = false;
      this.isShowChat = false;
      this.isAdmin = this.tReport?.isAdmin;
      this.fetchMatchDetails();
    }
  }

  navChanges = (e) => {
    this.nextId = e.nextId;
    switch (this.nextId) {
      case 1: // this.getMyTeams();
        break;
      case 2: // this.getMyInvite();
        break;
    }
  };
  showCreatedPopup(): void {
    this.matDialog.open(PopupCreatedComponent);
  }

  setMatchDetails(md) {
    this.matchDetails = md;
  }

  // fetchMatchDetails = async () => {
  //   try {
  //     this.isLoaded = true;

  //     const queryParam = `?query=${encodeURIComponent(
  //       JSON.stringify({
  //         tournamentId: this.tReport?.tournamentId,
  //         _id: this.tReport?.matchId,
  //       })
  //     )}`;

  //     const match = await this.bracketService.fetchAllMatches(queryParam);
  //     this.matchDetails = match?.data?.length ? match.data[0] : null;
  //     if (this.matchDetails) {
  //       this.teamList = [
  //         { ...this.matchDetails.teamA },
  //         { ...this.matchDetails.teamB },
  //       ];
  //       this.disQualifiedTeam = this.matchDetails?.disQualifiedTeam;
  //     }
  //     this.isLoaded = false;
  //   } catch (error) {
  //     this.isLoaded = false;
  //     this.showAlertBasedOnPlatform(
  //       error?.error?.message || error?.message,
  //       'showError'
  //     );
  //   }
  // };
  fetchMatchDetails = async () => {
    try {
      this.isLoaded = true;

      const queryParam = `?query=${encodeURIComponent(
        JSON.stringify({
          tournamentId: this.tReport?.tournament,
          _id: this.tReport?.match?._id,
        })
      )}`;

      const match = await this.bracketService.fetchAllMatches(queryParam);
      this.matchDetails = match?.data?.length ? match.data[0] : null;
      if (this.matchDetails) {
        if (this.participantId == this.matchDetails.teamA?._id) {
          this.teamName = 'teamA';
        } else {
          this.teamName = 'teamB';
        }

        // this.isScreenShotRequired = this.matchDetails?.tournamentId?.isScreenshotRequired;

        if (
          this.matchDetails.scoreUpdateByParticipant == null ||
          this.matchDetails.scoreUpdatedByParticipantDt == null
        ) {
          this.isupdatedscore = true;
        } else {
          this.isupdatedscore = false;
        }
        if (
          this.participantId == this.matchDetails.teamA?._id &&
          this.matchDetails?.matchUpdatedByTeamACount > 0
        ) {
          // this.hasParticipantOneTimeAccess = false;
        }

        if (
          this.participantId == this.matchDetails.teamB?._id &&
          this.matchDetails?.matchUpdatedByTeamBCount > 0
        ) {
          // this.hasParticipantOneTimeAccess = false;
        }
        this.teamList = [
          { ...this.matchDetails.teamA },
          { ...this.matchDetails.teamB },
        ];
        this.disQualifiedTeam = this.matchDetails?.disQualifiedTeam;
      }
      this.isLoaded = false;
    } catch (error) {
      this.isLoaded = false;
      this.showAlertBasedOnPlatform(
        error?.error?.message || error?.message,
        'showError'
      );
    }
  };
  /**
   * Exit from Score screen by passing refresh(true) to fetch latest bracket Scores
   */
  exitFromPage() {
    this.exit.emit({ refresh: true, isOpenScoreCard: false });
  }

  /**
   * Update score by both admin and reletad participant
   * But after finishing the set only admin can update the set score
   * After next match start even admin can't update the score of previous match
   * @param s set
   * @param value value
   * @param key key
   * @param delta delta
   * @param teamId teamId
   */
  updateScoree(data) {
    let s, value, key, delta, teamId;
    s = data.set;
    value = data.score;
    key = data.team_name;
    delta = data.n;
    teamId = data.team_id;
    if (this.isAdmin && this.matchDetails.isNextMatchStart) {
      // return;
    } else if (!this.isAdmin) {
      if (this.participantId !== teamId) {
        // return;
      }

      if (
        this.participantId === teamId &&
        this.matchDetails.matchStatus === 'completed'
      ) {
        // return;
      }

      if (this.tournament?.scoreReporting <= 1) {
        // return;
      }

      if (s?.status === 'completed') {
        // return;
      }
    }
    s[key] = s[key] <= 0 && delta === -1 ? value : value ;
    s.modify = true;
    this.participantSet = { ...this.participantSet, [key]: s[key], id: s?.id };
  }

  /**
   * Admin submit the update score
   * @param set set
   */
  submitScore = async (set) => {
    try {
      this.isLoaded = true;
      const queryParam = `?query=${encodeURIComponent(
        JSON.stringify({
          _id: this.matchDetails._id,
          'sets.id': set.id,
          tournamentId: this.matchDetails?.tournamentId?._id,
        })
      )}`;

      const payload = {
        teamAScore: set.teamAScore,
        teamAScreenShot: null,
        teamBScore: set.teamBScore,
        teamBScreenShot: null,
      };

      // const payload = {
      //   $set: {
      //     "sets.$.teamAScore": set.teamAScore,
      //     "sets.$.teamBScore": set.teamBScore,
      //     ...(set.teamAScore == set.teamBScore && { winner: null }),
      //   },
      // };

      const match = await this.bracketService.updateMatch(queryParam, payload);
      if (!match.success) {
        this.showAlertBasedOnPlatform(match.message, 'showInfo');
      }
      if (match.success) {
        this.showCreatedPopup();
      }

      this.isLoaded = false;
      this.fetchMatchDetails();
    } catch (error) {
      this.isLoaded = false;
      this.token
        ? this.utilsService.showNativeAlert(
            error?.error?.message || error?.message
          )
        : this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  /**
   * Get player name by _id
   * @param id teamId
   */
  getPlayerName(id) {
    if (this.matchDetails?.teamA?._id === id) {
      return this.matchDetails?.teamA?.teamName;
    } else if (this.matchDetails?.teamB?._id === id) {
      return this.matchDetails?.teamB?.teamName;
    } else {
      return null;
    }
  }

  /**
   * Open popUp for participants to upload screenshot/view
   * Open popup for Admin to view uploaded screenshot
   * @param content model template
   * @param set set
   * @param player player
   */
  open = async (content, set, player) => {
    try {
      this.participantSet = set;
      this.screenShot = player;
      const result = await this.modalService.open(content, {
        ariaLabelledBy: 'modal-basic-title',
        windowClass: 'modal-upload-screenshot',
        centered: true,
      }).result;
    } catch (error) {}
  };
  showAlertBasedOnPlatform = (message, type) => {
    this.enableWebview
      ? this.utilsService.showNativeAlert(message)
      : this.toastService[type](message);
  };
  ngOnChanges(changes: SimpleChanges) {
    if (changes.hasOwnProperty('match') && changes.match.currentValue) {
      // this.fetchMatchDetails();
    }
  }

  // convert image to base64
  participantScreenshot(event) {
    const files = event.target.files;

    if (files && files[0]) {
      const reader = new FileReader();

      reader.onload = this._handleReaderLoaded.bind(this);
      reader.readAsBinaryString(files[0]);
    }
  }

  _handleReaderLoaded(readerEvt) {
    const binaryString = readerEvt.target.result;
    this.participantSet[this.screenShot] =
      'data:image/png;base64,' + btoa(binaryString);
  }
  isAllowScore = async (setA, setB) => {
    const isInValid =
      setA == setB &&
      ['swiss_safeis', 'ladder'].includes(
        this.tReport?.tournaments?.bracketType
      );
    if (isInValid) {
      this.toastService.showInfo(
        this.translateService.instant('ELIMINATION.SCORE_ALERT')
      );
    }
    return !isInValid;
  };
  // submitParticipantScore = async () => {
  //   try {
  //     this.isLoaded = true;

  //     const queryParam = `?query=${encodeURIComponent(
  //       JSON.stringify({
  //         _id: this.matchDetails?._id,
  //         'sets.id': this.participantSet.id,
  //         tournamentId: this.matchDetails?.tournamentId?._id,
  //       })
  //     )}`;

  //     // const payload = {
  //     //   $set: this.convertObject(this.participantSet),
  //     // };
  //     const payload = {
  //       teamAScore: this.participantSet.teamAScore,
  //       teamAScreenShot: null,
  //       teamBScore: this.participantSet.teamBScore,
  //       teamBScreenShot: null,
  //     };

  //     const response = await this.bracketService.updateParticipantScore(
  //       queryParam,
  //       payload
  //     );

  //     this.showAlertBasedOnPlatform(response?.message, 'showSuccess');
  //     this.isLoaded = false;
  //     this.fetchMatchDetails();
  //   } catch (error) {
  //     this.isLoaded = false;
  //     this.showAlertBasedOnPlatform(
  //       error?.error?.message || error?.message,
  //       'showError'
  //     );
  //   }
  // };
  submitParticipantScore = async (set) => {
    try {
      this.isLoaded = true;
      const status = await this.isAllowScore(set.teamAScore, set.teamBScore);
      if (!status) return;
      const queryParam = `?query=${encodeURIComponent(
        JSON.stringify({
          _id: this.matchDetails?._id,
          'sets.id': set.id,
          tournamentId: this.matchDetails?.tournamentId?._id,
        })
      )}`;

      // const payload = {
      //   $set: this.convertObject(set),
      // };
      const payload = {
        teamAScore: set.teamAScore,
        teamAScreenShot: set.teamAScreenShot,
        teamBScore: set.teamBScore,
        teamBScreenShot: set.teamBScreenShot,
      };

      const response = await this.bracketService.updateParticipantScore(
        queryParam,
        payload
      );

      this.showAlertBasedOnPlatform(response?.message, 'showSuccess');
      this.isLoaded = false;
      this.router.navigate(['/profile/matches', { activeTab: 2 }]);
      // this.fetchMatchDetails();
    } catch (error) {
      this.isLoaded = false;
      this.showAlertBasedOnPlatform(
        error?.error?.message || error?.message,
        'showError'
      );
    }
  };
  /**
   * Modify object based on request payload
   * @param obj obj
   */
  convertObject(obj) {
    const modifiedObj = {};
    for (const key in obj) {
      modifiedObj[`sets.$.${key}`] = obj[key];
    }
    return modifiedObj;
  }

  disqualify = async () => {
    try {
      const invalid = Object.values(this.disQualifiedTeam).some(
        (item) => !item
      );
      if (invalid) {
        this.showAlertBasedOnPlatform(
          this.translateService.instant('ELIMINATION.DISQUALIFY_REQUIRED'),
          'showInfo'
        );
        return;
      }
      this.isLoaded = true;

      const loserTeam = this.disQualifiedTeam.team;
      const winnerTeam = this.teamList.find(
        (team) => team._id !== loserTeam
      )?._id;

      const payload = {
        disQualifiedTeam: this.disQualifiedTeam,
        winnerTeam,
        loserTeam,
        matchStatus: 'completed',
      };

      const queryParam = `?query=${encodeURIComponent(
        JSON.stringify({
          _id: this.matchDetails?._id,
          tournamentId: this.matchDetails?.tournamentId?._id,
        })
      )}`;
      const match = await this.bracketService.updateMatch(queryParam, payload);
      this.showAlertBasedOnPlatform(
        match.message,
        match.success ? 'showSuccess' : 'showInfo'
      );

      this.isLoaded = false;
      this.fetchMatchDetails();
    } catch (error) {
      this.isLoaded = false;
      this.showAlertBasedOnPlatform(
        'showError',
        error?.error?.message || error?.message
      );
    }
  };
}
