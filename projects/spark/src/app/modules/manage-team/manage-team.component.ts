import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppHtmlProfileRoutes } from '../../app-routing.model';
import { Subscription } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { Location } from '@angular/common';
import { IUser, EsportsToastService, EsportsUserService } from 'esports';
import { environment } from '../../../environments/environment';

const API = environment.apiEndPoint;
@Component({
  selector: 'app-team-manage',
  templateUrl: './manage-team.component.html',
  styleUrls: ['./manage-team.component.scss'],
})
export class ManageTeamComponent implements OnInit, OnDestroy {
  mock_members = [];
  AppHtmlProfileRoutes = AppHtmlProfileRoutes;

  teamId;
  currentUser: IUser;
  userSubscription: Subscription;
  currentUserRole;
  currentUserPermission;

  constructor(
    private activatedRoute: ActivatedRoute,
    private userService: EsportsUserService,
    private toastService: EsportsToastService,
    private router: Router,
    private location: Location,
    private translateService: TranslateService
  ) {}

  ngOnInit() {
    this.activatedRoute.params.subscribe((params) => {
      if (params?.id) {
        this.teamId = params?.id;
        this.getCurrentUserDetails(params?.id);
      }
    });
  }
  translateRole(role: string): string {
    switch (role.trim().toLowerCase()) {
      case 'owner': {
        return 'MANAGE_TEAM.TABLE.ROLE.OWNER';
      }
      case 'captain': {
        return 'MANAGE_TEAM.TABLE.ROLE.CAPTAIN';
      }
      default: {
        return 'MANAGE_TEAM.TABLE.ROLE.PLAYER';
      }
    }
  }

  getTeamMembers = async (id) => {
    try {
      const teamMembers: any = await this.userService.getTeamMember(API, id);
      this.mock_members = teamMembers?.data;

      const indexm = this.mock_members.findIndex(
        (x) =>
          JSON.stringify(x.userId._id) === JSON.stringify(this.currentUser?._id)
      );
      if (indexm > -1) {
        if (this.mock_members[indexm].role != 'player') {
          const currentTeamMember = this.mock_members.find(
            (el) => el.userId._id == this.currentUser._id
          );
          this.currentUserRole = currentTeamMember.role;

          //   if (this.currentUserRole == 'owner') {
          //     this.currentUserPermission.removeCaptain = true;
          //     this.currentUserPermission.removePlayer = true;
          //     this.currentUserPermission.demoteCaptain = true;
          //     this.currentUserPermission.makeCaptain = true;
          //   } else if (this.currentUserRole == 'captain') {
          //     this.currentUserPermission.removeCaptain = false;
          //     this.currentUserPermission.removePlayer = true;
          //     this.currentUserPermission.demoteCaptain = false;
          //     this.currentUserPermission.makeCaptain = true;
          // }
          for (const member of this.mock_members) {
            if (member.role == 'owner') {
              member.removePlayer = false;
              member.demoteCaptain = false;
              member.makeCaptain = false;
              member.dots = false;
            }
            if (member.role == 'captain') {
              if (this.currentUserRole == 'captain') {
                member.removePlayer = false;
                member.demoteCaptain = false;
                member.makeCaptain = false;
                member.dots = false;
              }
              if (this.currentUserRole == 'player') {
                member.removePlayer = false;
                member.demoteCaptain = false;
                member.makeCaptain = false;
                member.dots = false;
              }
              if (this.currentUserRole == 'owner') {
                member.removePlayer = true;
                member.demoteCaptain = true;
                member.makeCaptain = false;
                member.dots = true;
              }
            }

            if (member.role == 'player') {
              if (this.currentUserRole == 'captain') {
                member.removePlayer = true;
                member.demoteCaptain = false;
                member.makeCaptain = true;
                member.dots = true;
              }
              if (this.currentUserRole == 'player') {
                member.removePlayer = false;
                member.demoteCaptain = false;
                member.makeCaptain = false;
                member.dots = false;
              }
              if (this.currentUserRole == 'owner') {
                member.removePlayer = true;
                member.demoteCaptain = false;
                member.makeCaptain = true;
                member.dots = true;
              }
            }
          }
        } else if (this.currentUser.accountType != 'admin') {
          this.toastService.showError(
            this.translateService.instant('PROFILE.TEAMS.ERRORS.ERROR7')
          );
          this.cancel();
        }
      } else if (this.currentUser.accountType != 'admin') {
        this.toastService.showError(
          this.translateService.instant('PROFILE.TEAMS.ERRORS.ERROR7')
        );
        this.cancel();
      }
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  async changeRole(player, role) {
    await this.userService
      .update_member(API, {
        teamId: this.teamId,
        userId: player._id,
        role: role,
        name: player.fullName,
      })
      .subscribe(
        (data) => {
          this.toastService.showSuccess(data?.message);
          this.router
            .navigateByUrl('/profile/teams', { skipLocationChange: true })
            .then(() => {
              this.router.navigate(['/manage-team', this.teamId]);
            });
        },
        (error) => {}
      );
  }

  async changeStatus(player, status) {
    await this.userService
      .update_member(API, {
        teamId: this.teamId,
        userId: player._id,
        name: player.fullName,
        status: status,
      })
      .subscribe(
        (data) => {
          this.toastService.showSuccess(data?.message);
          this.router
            .navigateByUrl('/profile/teams', { skipLocationChange: true })
            .then(() => {
              this.router.navigate(['/manage-team', this.teamId]);
            });
        },
        (error) => {}
      );
  }

  getCurrentUserDetails(id) {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.currentUser = data;
        this.getTeamMembers(id);
      }
    });
  }
  updateTeam(info) {
    const teamObj = {
      id: this.teamId,
      admin: true,
      userId: info.userId._id,
      action: 'owner change',
      query: {
        condition: { _id: this.teamId },
        update: { teamCreator: info.userId._id },
      },
    };
    this.userService.update_team_owner(API, teamObj).subscribe(
      (res) => {
        this.router
          .navigateByUrl('/profile/teams', { skipLocationChange: true })
          .then(() => {
            this.router.navigate(['/manage-team', this.teamId]);
          });
      },
      (err) => {
        this.toastService.showError(err.error.message);
      }
    );
  }
  cancel() {
    this.location.back();
  }
  ngOnDestroy() {
    if (this.userSubscription) {
      this.userSubscription?.unsubscribe();
    }
  }
}
