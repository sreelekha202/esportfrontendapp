import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LeaderBoardUserCardComponent } from './leaderboard-user-card.component';

describe('LeaderBoardUserCardComponent', () => {
  let component: LeaderBoardUserCardComponent;
  let fixture: ComponentFixture<LeaderBoardUserCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LeaderBoardUserCardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LeaderBoardUserCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
