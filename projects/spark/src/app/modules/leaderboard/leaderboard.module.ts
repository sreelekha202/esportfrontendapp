import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoreModule } from '../../core/core.module';
import { LeaderboardComponent } from './leaderboard.component';
import { LeaderboardRoutingModule } from './leaderboard-routing.module';
import { LeaderboardPlayerComponent } from './leaderboard-player/leaderboard-player.component';
import { LeaderboardTableComponent } from './leaderboard-table/leaderboard-table.component';
import { LeaderboardTrophiesCardComponent } from './leaderboard-trophies-card/leaderboard-trophies-card.component';
import { LeaderboardUserCardComponent } from './leaderboard-user-card/leaderboard-user-card.component';
import { PopupCreatedComponent } from './popup-created/popup-created.component';
import { SharedModule } from '../../shared/modules/shared.module';
import { HeaderBottomModule } from '../../shared/components/header-bottom/header-bottom.module';
import { FooterSparkModule } from '../../shared/components/footer-spark/footer-spark.module';
import { EsportsLoaderModule,EsportsCustomPaginationModule } from 'esports';

const components = [
  LeaderboardComponent,
  LeaderboardPlayerComponent,
  LeaderboardTableComponent,
  LeaderboardTrophiesCardComponent,
  LeaderboardUserCardComponent,
  PopupCreatedComponent
];

const modules = [
  CommonModule,
  CoreModule,
  LeaderboardRoutingModule,
  SharedModule,
  HeaderBottomModule,
  FooterSparkModule,
  EsportsCustomPaginationModule,
  EsportsLoaderModule.setColor('#00a851')
];

@NgModule({
  declarations: components,
  imports: modules,
})
export class LeaderboardModule {}
