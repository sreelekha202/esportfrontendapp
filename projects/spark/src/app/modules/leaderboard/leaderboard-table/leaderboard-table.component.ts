import { Component, OnInit, Input } from '@angular/core';
import { AppHtmlRoutes,AppHtmlProfileRoutes,AppHtmlManageTeams } from '../../../app-routing.model';

export interface LeaderBoard {
  page: string;
  title: string;
}

@Component({
  selector: 'app-leaderboard-table',
  templateUrl: './leaderboard-table.component.html',
  styleUrls: ['./leaderboard-table.component.scss'],
})
export class LeaderboardTableComponent implements OnInit {
  @Input() leaderboardData = [];
  @Input() data: LeaderBoard;
  @Input() teamFlag:boolean;

  AppHtmlRoutes = AppHtmlRoutes;
  AppHtmlProfileRoutes = AppHtmlProfileRoutes;
  AppHtmlManageTeams = AppHtmlManageTeams

  constructor() {}

  ngOnInit(): void {
  }

}
