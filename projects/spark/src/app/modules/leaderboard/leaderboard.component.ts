import { Component, Input, OnInit } from '@angular/core';
import { environment } from '../../../environments/environment';
import { AppHtmlRoutes } from '../../app-routing.model';
import { LeaderBoard } from './leaderboard-table/leaderboard-table.component';
import {
  IPagination,
  EsportsUserService,
  EsportsLeaderboardService,
  EsportsGameService,
  EsportsHomeService,
} from 'esports';

const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;
@Component({
  selector: 'app-leaderboard',
  templateUrl: './leaderboard.component.html',
  styleUrls: ['./leaderboard.component.scss'],
})
export class LeaderboardComponent implements OnInit {
  @Input() data: LeaderBoard = {
    page: 'leaderboard',
    title: 'All Games',
  };

  AppHtmlRoutes = AppHtmlRoutes;
  active = 1;
  showLoader: boolean = true;
  gameId = ' ';
  selectedCountryName = '';
  selectedGameName = '';
  selectedGames = 'all';
  countryList = [];
  gameList = [];
  leaderboardData: any = [];
  selectedCountry: any = { all: true };
  selectedGame: any = { all: true };
  paginationDetails: any;
  gameServiceData: any;
  teamFlag:boolean = false;
  participantPage: IPagination = {
    activePage: 1,
    totalItems: 1,
    maxSize: 10,
    itemsPerPage: 10,
  };
  participantTeamPage: IPagination = {
    activePage: 1,
    totalItems: 1,
    maxSize: 10,
    itemsPerPage: 10,
  };
  paginationData = {
    page: 1,
    limit: 10,
    sort: 'startDate',
  };

  constructor(
    private leaderboardService: EsportsLeaderboardService,
    private gameService: EsportsGameService,
    private userService: EsportsUserService,
    private homeService: EsportsHomeService
  ) {}

  ngOnInit(): void {
    this.getGames();
    this.getCountries();
    this.filterLeaderboard();

    this.gameServiceData = this.gameService.createMatchMaking.subscribe(
      (data: any) => {
        if (data) {
          this.selectedGame = (data.selectedGame ? data.selectedGame : data.selectedTournament);
          if(this.selectedGame && !this.selectedGame.logo){
            this.selectedGame.logo = this.selectedGame?.gameDetail.logo;
          }
        }
      }
    );
  }

  getGames() {
    this.gameService.getAllGames(API, TOKEN).subscribe(
      (res) => (this.gameList = res.data),
      (err) => {}
    );
  }

  getCountries() {
    this.userService.getAllCountries().subscribe((data) => {
      this.countryList = data.countries;
    });
  }

  changeTab(value, type) {
    switch (type) {
      case 'regions': {
        if (value != 'all') {
          const country = this.countryList.find((o) => o.id === value);
          this.selectedCountryName = country.name;
          this.filterLeaderboard();
        } else {
          this.selectedCountryName = '';
          this.filterLeaderboard();
        }
        break;
      }
      case 'games': {
        if (value != 'all') {
          const game = this.gameList.find((o) => o._id === value);
          this.selectedGameName = game.name;
          this.selectedGames = game._id;
          this.filterLeaderboard();
        } else {
          this.selectedGameName = value;
          this.selectedGames = 'all';
          this.filterLeaderboard();
        }
        break;
      }
    }
  }

  filterLeaderboard(page = 1) {
    this.teamFlag = false;
    //this.selectedGame = {};
    //this.selectedGame[this.selectedGames] = true;
    this.showLoader = true;

    const params = {
      country: this.selectedCountryName,
      gameId: this.selectedGames,
      limit: this.paginationData.limit,
      page: page,
      state: null,
    };

    if (this.data.page === 'home') {
      this.homeService._leaderBoard().subscribe((data) => {
        this.showLoader = false;
        this.leaderboardData = data.data.leaderboardData;

        if (this.leaderboardData) {
          this.leaderboardData.forEach((obj, index) => {
            obj.rank = index + 1;
          });
        }
      });
    } else {
      this.leaderboardService.getGameLeaderboard(API, params).subscribe(
        (res: any) => {
          this.showLoader = false;
          this.leaderboardData = [];

          if (res.data.docs) {
            let count = (params.page - 1) * params.limit;;
            let userData: any = {};
            let leaderboardDataVo: any = res.data.docs;
            /* sort gold by decending */
            leaderboardDataVo.sort((a, b) => {
              return b.firstPosition - a.firstPosition;
            });

            for (const data of leaderboardDataVo) {
              if (data.user && data.user.length > 0) {
                userData = {};
                userData.id = data._id.user;
                userData.rank = count + 1;
                count++;
                userData.img =
                  data.user[0].profilePicture == ''
                    ? './assets/images/Profile/formation.svg'
                    : data.user[0].profilePicture;
                userData.name = data.user[0].username;
                userData.region = data?.user?.[0]?.city ? data.user[0].city : '';
                userData.trophies = {};
                userData.trophies.first = data.firstPosition;
                userData.trophies.second = data.secondPosition;
                userData.trophies.third = data.thirdPosition;
                userData.points = data.points;
                userData.amount = data.amountPrice;
                userData.gamesCount = data.gamesCount;
                this.leaderboardData.push(userData);
              } else {
                this.participantPage.itemsPerPage =
                  this.paginationDetails?.limit;
                this.participantPage.totalItems = 0;
              }
            }
            this.paginationDetails = res?.data;
            this.participantPage.itemsPerPage = this.paginationDetails.limit;
            this.participantPage.totalItems = this.paginationDetails.totalDocs;
          }
        },
        (err) => {
          this.showLoader = false;
          this.leaderboardData = [];
        }
      );
    }
  }

  filterByGame(gameId) {
    this.paginationData.page = 1;
    //this.selectedGame = {};
    //this.selectedGame[gameId] = true;

    if (gameId === 'all') {
      this.gameId = 'all';
    } else {
      this.gameId = gameId;
    }

    this.showLoader = true;
    const params = {
      page: this.paginationData.page,
      limit: this.paginationData.limit,
      gameId: this.gameId,
    };
    if (this.gameId) {
      params['game'] = this.gameId;
    }
    this.leaderboardService.getGameLeaderboard(API, params).subscribe(
      (res: any) => {
        this.showLoader = false;
        this.leaderboardData = [];

        if (res.data.docs) {
          let count = 0;
          let userData: any = {};

          for (const data of res.data.docs) {
            if (data.user.length > 0) {
              userData.id = data._id.user;
              userData.rank = count + 1;
              count++;
              userData.img =
                data.user[0].profilePicture == ''
                  ? './assets/images/Profile/formation.svg'
                  : data.user[0].profilePicture;
              userData.name = data.user[0].fullName;
              userData.region = data.user[0].country
                ? data.user[0].country
                : '';
              userData.trophies = {};
              userData.trophies.first = data.firstPosition;
              userData.trophies.second = data.secondPosition;
              userData.trophies.third = data.thirdPosition;
              userData.points = data.points;
              userData.amount = data.amountPrice;
              userData.gamesCount = data.gamesCount;
              this.leaderboardData.push({ ...userData });
              userData = {};
            }
          }
        }
      },
      (err) => {
        this.showLoader = false;
      }
    );
  }

  onPageChange(evt) {
    if(!this.teamFlag)
      this.filterLeaderboard(evt);
    else
      this.filterLeaderboardTeam(evt);
  }

  filterLeaderboardTeam(page = 1) {
    this.showLoader = true;
    this.teamFlag = true;

    const params = {
      country: this.selectedCountryName,
      gameId: this.selectedGames,
      limit: this.paginationData.limit,
      page: page,
      state: null,
    };
    this.leaderboardService.getGameLeaderboard(API, params, 'team').subscribe(
      (res: any) => {
        this.showLoader = false;
        this.leaderboardData = [];

        if (res.data.docs) {
          let count = (params.page - 1) * params.limit;
          let userData: any = {};
          let leaderboardDataVo: any = res.data.docs;
          /* sort gold by decending */
          leaderboardDataVo.sort((a, b) => {
            return b.firstPosition - a.firstPosition;
          });

          for (const data of leaderboardDataVo) {
            if(data.team && data.team.length > 0){
              userData = {};
              userData.id = data._id.team;
              userData.rank = count + 1;
              count++;
              userData.img =
                (!data.team[0].logo
                  ? './assets/images/Profile/formation.svg'
                  : data.team[0].logo);
              userData.name = data.team[0].teamName;
              userData.region = data.team[0].country
                ? data.team[0].country
                : '';
              userData.trophies = {};
              userData.trophies.first = data.firstPosition;
              userData.trophies.second = data.secondPosition;
              userData.trophies.third = data.thirdPosition;
              userData.points = data.points;
              userData.amount = data.amountPrice;
              userData.gamesCount = data.gamesCount;
              this.leaderboardData.push(userData);
            } else {
              this.participantTeamPage.itemsPerPage =
                this.paginationDetails?.limit;
              this.participantTeamPage.totalItems = 0;
            }
          }
          this.paginationDetails = res?.data;
          this.participantTeamPage.itemsPerPage = this.paginationDetails.limit;
          this.participantTeamPage.totalItems = this.paginationDetails.totalDocs;
        }
      },
      (err) => {
        this.showLoader = false;
        this.leaderboardData = [];
      }
    );
  }
  
}
