import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LeaderboardTrophiesCardComponent } from './leaderboard-trophies-card.component';

describe('LeaderboardTrophiesCardComponent', () => {
  let component: LeaderboardTrophiesCardComponent;
  let fixture: ComponentFixture<LeaderboardTrophiesCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LeaderboardTrophiesCardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LeaderboardTrophiesCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
