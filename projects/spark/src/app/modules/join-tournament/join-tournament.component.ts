import { Component, OnInit } from '@angular/core';
import { GlobalUtils, EsportsTournamentService } from 'esports';
import { AppHtmlRoutes } from '../../app-routing.model';
@Component({
  selector: 'app-join-tournament',
  templateUrl: './join-tournament.component.html',
  styleUrls: ['./join-tournament.component.scss'],
})
export class JoinTournamentComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;
  tournamentDetails: any;
  constructor(private tournamentService: EsportsTournamentService) {}
  ngOnInit(): void {
    if (GlobalUtils.isBrowser()) {
      this.tournamentDetails = localStorage.getItem('tournamentDetails');
      this.tournamentDetails = JSON.parse(this.tournamentDetails);
    }
  }
}
