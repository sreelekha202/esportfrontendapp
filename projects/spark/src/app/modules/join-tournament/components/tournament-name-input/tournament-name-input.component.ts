import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-tournament-name-input',
  templateUrl: './tournament-name-input.component.html',
  styleUrls: ['./tournament-name-input.component.scss'],
})
export class TournamentNameInputComponent implements OnInit {
  @Input() placeholder: string = '';
  @Input() text: string = '';
  @Output() onTextChange = new EventEmitter<string>();
  constructor() { }
  ngOnInit(): void { }
  onTextChang = () => this.onTextChange.emit(this.text)
}
