import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TournamentDateComponent } from './tournament-date.component';

describe('TournamentDateComponent', () => {
  let component: TournamentDateComponent;
  let fixture: ComponentFixture<TournamentDateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TournamentDateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TournamentDateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
