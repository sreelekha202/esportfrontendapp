import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TournamentFieldTimeComponent } from './tournament-field-time.component';

describe('TournamentFieldTimeComponent', () => {
  let component: TournamentFieldTimeComponent;
  let fixture: ComponentFixture<TournamentFieldTimeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TournamentFieldTimeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TournamentFieldTimeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
