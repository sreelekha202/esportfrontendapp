import { Component, OnInit } from '@angular/core';
import { AppHtmlJoinTournamentRoutes  } from '../../../../app-routing.model';
import { Router } from '@angular/router';
import { EsportsToastService, EsportsUserService, EsportsTournamentService } from 'esports';
import { AppHtmlRoutes } from '../../../../app-routing.model';
import { environment } from '../../../../../environments/environment';
const API = environment.apiEndPoint;

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;
  selectedteam = null;
  joinTournament: any;
  AppHtmlJoinTournamentRoutes  = AppHtmlJoinTournamentRoutes ;

  teams = [];
  paginationData = {
    page: 1,
    limit: 100,
    sort: { _id: -1 },
    text: '',
  };
  showLoader = false;

  constructor(    private route:Router,
    private tournamentService:EsportsTournamentService,
    private toastService: EsportsToastService,
    private userService: EsportsUserService) {}

  ngOnInit(): void {
    this.getMyTeams();
    this.showLoader = true;
  }

  getMyTeams= async () => {
    const pagination = JSON.stringify(this.paginationData);
    const res = await this.userService.getMyTeam(API, {
      pagination: pagination,
    });
      this.teams = []
      for(let d of res.data.docs){
          let d1 = {
            image:
            d.logo,
          name: d.teamName,
          teamsLength: 5,
          createtdAt: 1,
          _id:d._id
          }
          this.teams.push(d1)

      }
      this.showLoader = false;
    
    // this.showLoader = true;
    // const params: any = {
    //   "page":1,
    //   "limit":100,
    //   "hasNextPage":true
    // };
    // this.profileService.getMyTeams(params).subscribe((res) => {
    //   // this.myTeams = res.data;
    //   this.showLoader = false;
    // }, (err) => { this.showLoader = false; })
  }

  confirm() {
    if (this.selectedteam) {
      sessionStorage.setItem("selectedteam",JSON.stringify(this.selectedteam))
      this.tournamentService.joinTournamentSubject.next({ ...this.joinTournament, "selectedteam": this.selectedteam })
      this.route.navigateByUrl("join-tournament/teammembers")
    } else {
      this.toastService.showError("Please select the team.")
    }
  }


  selectedteamfunction(gameIndex: number): void {
    this.teams.forEach((game, idx) => { game.isSelected = gameIndex === idx; });
  }


}
