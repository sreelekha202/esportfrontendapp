import { Component, OnInit, Input } from '@angular/core';
import { environment } from '../../../../../environments/environment';
import { EsportsUserService } from 'esports';
const API = environment.apiEndPoint;
@Component({
  selector: 'app-participants-request',
  templateUrl: './participants-request.component.html',
  styleUrls: ['./participants-request.component.scss']
})
export class ParticipantsRequestComponent implements OnInit {
  @Input() isManage: boolean = false;
  @Input() data;

  active = 1;
  nextId: number = 1;
  teamId : number = 1;
  myTeams = [];
  teamRequests = [];
  showLoader: boolean = true;
  constructor(private userService: EsportsUserService,) { }

  ngOnInit(): void {
    this.getMyTeams();
  }
  navChanges = (e) => {
    this.nextId = e.nextId;
    switch (this.nextId) {
      case 1:
        this.getMyTeams();
        break;
      case 2:
        this.getMyInvite();
        break;
    }
  }
  getMyInvite = async () => {
    const params: any = {};
    const data = await this.userService.getMyInvite(API, params);
    if (data) {
      this.teamRequests = data.data;
    }
  }
  getMyTeams() {
    this.showLoader = true;
    const params: any = {};
    this.userService.getMyTeams(params).subscribe((res) => {
    this.myTeams = res.data;
      this.showLoader = false;
    }, (err) => { this.showLoader = false; })
  }
  deleteTeams(teamId: number) {
    this.userService.deleteTeam(teamId)
      .subscribe((data: void) => {
        let index: number = this.myTeams.findIndex(teamId => teamId.teamId === teamId);
        this.myTeams.splice(index, 1);
      }
      )


  }


}
''
