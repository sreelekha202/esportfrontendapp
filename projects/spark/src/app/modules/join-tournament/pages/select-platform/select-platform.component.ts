import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AppHtmlRoutes } from '../../../../app-routing.model';
import { EsportsToastService, EsportsTournamentService } from 'esports';

@Component({
  selector: 'app-select-platform',
  templateUrl: './select-platform.component.html',
  styleUrls: ['./select-platform.component.scss'],
})
export class SelectPlatformComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;
  selectedPlatformIndex = null;
  tournamentDetails: Subscription;
  platforms = [];
  createTournament: any;

  constructor(
    private tournamentService: EsportsTournamentService,
    private toastService: EsportsToastService,
    private router: Router) { }

  ngOnInit(): void {
    this.tournamentDetails = this.tournamentService.createTournament.subscribe((data) => {
      if (data) {
        this.createTournament = data;

        if (this.createTournament.selectedGame && this.createTournament.selectedGame.platform && this.createTournament.selectedGame.platform.length > 0)
          this.createTournament.selectedGame.platform.map((obj) => {
            if (obj.name == 'pc') {
              this.platforms.push({
                ...obj,
                type: 'pc',
                icon: 'assets/icons/matchmaking/platforms/pc.svg',
                title: 'MATCHMAKING.SELECT_PLATFORM.PLATFORMS.PC',
              })
            }
            else if (obj.name == 'Mobile') {
              this.platforms.push({
                ...obj,
                type: 'mobile',
                icon: 'assets/icons/matchmaking/platforms/mobile.svg',
                title: 'MATCHMAKING.SELECT_PLATFORM.PLATFORMS.MOBILE',
              })
            }
            else if (obj.name == 'Other') {
              this.platforms.push({
                ...obj,
                type: 'console',
                icon: 'assets/icons/matchmaking/platforms/console.svg',
                title: 'MATCHMAKING.SELECT_PLATFORM.PLATFORMS.CONSOLE',
              })
            }
            else {
              this.platforms.push({
                ...obj,
                type: 'pc',
                icon: 'assets/icons/matchmaking/platforms/pc.svg',
                title: 'MATCHMAKING.SELECT_PLATFORM.PLATFORMS.PC',
              })
            }
          })
      }
    });
  }
  next = () => {
    if (this.selectedPlatformIndex != null && this.selectedPlatformIndex != undefined && this.createTournament.selectedGame.platform[this.selectedPlatformIndex]) {
      this.tournamentService.createTournamentSubject.next({ ...this.createTournament, "selected_platform": this.createTournament.selectedGame.platform[this.selectedPlatformIndex] })
      this.router.navigateByUrl("create-tournament/date");
    } else {
      this.toastService.showError("Please select the platform.")
    }
  }

  ngOnDestroy(): void {
    if (this.tournamentDetails)
      this.tournamentDetails.unsubscribe();
  }
}



