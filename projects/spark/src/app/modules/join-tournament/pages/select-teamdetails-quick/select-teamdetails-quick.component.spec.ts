import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectTeamdetailsQuickComponent } from './select-teamdetails-quick.component';

describe('SelectTeamdetailsQuickComponent', () => {
  let component: SelectTeamdetailsQuickComponent;
  let fixture: ComponentFixture<SelectTeamdetailsQuickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SelectTeamdetailsQuickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectTeamdetailsQuickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
