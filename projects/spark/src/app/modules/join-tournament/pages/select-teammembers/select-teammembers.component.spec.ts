import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectTeammembersComponent } from './select-teammembers.component';

describe('SelectTeammembersComponent', () => {
  let component: SelectTeammembersComponent;
  let fixture: ComponentFixture<SelectTeammembersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SelectTeammembersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectTeammembersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
