import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.scss']
})
export class CheckoutComponent implements OnInit {
  show = false;
  constructor() { }

  ngOnInit(): void {
  }
  showPopup($event) {
    this.show = true;
  }
  hidePopup($event) {
    this.show = false;
  }
}
