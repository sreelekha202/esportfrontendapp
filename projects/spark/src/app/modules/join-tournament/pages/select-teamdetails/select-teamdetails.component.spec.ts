import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectTeamdetailsComponent } from './select-teamdetails.component';

describe('SelectTeamdetailsComponent', () => {
  let component: SelectTeamdetailsComponent;
  let fixture: ComponentFixture<SelectTeamdetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SelectTeamdetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectTeamdetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
