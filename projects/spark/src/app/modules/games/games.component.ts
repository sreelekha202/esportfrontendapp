import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatchmakerDialogComponent } from './matchmaker-dialog/matchmaker-dialog.component';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AppHtmlProfileRoutes } from '../../app-routing.model';
import { Subscription } from 'rxjs';
import {
  EsportsUserService,
  EsportsLanguageService,
  EsportsGameService,
  EsportsChatService,
  IUser,
} from 'esports';
import { environment } from '../../../environments/environment';

const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;
@Component({
  selector: 'app-games',
  templateUrl: './games.component.html',
  styleUrls: ['./games.component.scss'],
})
export class GamesComponent implements OnInit, OnDestroy {
  AppHtmlProfileRoutes = AppHtmlProfileRoutes;

  gameList = [];
  windowposition: String = 'chat_window chat_window_right_drawer';
  currentUser: IUser;
  isUserLoggedIn = false;
  currentUserId: String = '';
  currentUserName: String = '';
  matchdetails: any;

  userSubscription: Subscription;
  showLoader: boolean = true;
  constructor(
    private gameService: EsportsGameService,
    public gamesMatchmaking: MatDialog,
    private chatService: EsportsChatService,
    private userService: EsportsUserService,
    private router: Router,
    public language: EsportsLanguageService,
    public translateService: TranslateService
  ) { }

  ngOnInit(): void {
    this.getGames();
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.currentUser = data;
        this.isUserLoggedIn = !!this.currentUser;
        this.currentUserId = this.currentUser._id;
      }
    });
  }

  ngOnDestroy() {
    this.userSubscription?.unsubscribe();
  }

  getGames() {
    this.gameService.getAllGames(API, TOKEN).subscribe(
      (res) => {
        this.gameList = res.data;
      },
      (err) => { }
    );
  }

  openDialog(game) {
    const dialogRef = this.gamesMatchmaking.open(MatchmakerDialogComponent, {
      data: game,
      panelClass: 'matchMaking',
      disableClose: true,
    });

    dialogRef.afterClosed().subscribe((result) => {
      // integrate with chat
      if (!(result == null || result == '')) {
        let matchid = result.id + '-' + this.currentUserId;
        this.matchdetails = {
          matchid: matchid,
          image: result.image,
          matchName: result.matchName,
          name: result.name,
        };

        this.chatService.setWindowPos(this.windowposition);
        this.chatService.setCurrentMatch(this.matchdetails);
        this.chatService.setTypeOfChat('game');
        this.chatService.setChatStatus(true);
        if (this.chatService.getChatStatus() == false) {
          this.chatService?.disconnectMatch(matchid, 'game');
        }
        this.router.navigateByUrl('/tournament/info');
      }
    });
  }
}
