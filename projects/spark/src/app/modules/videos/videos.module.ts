import { VideosRoutingModule } from "./videos-routing.module";
import { CoreModule } from "../../core/core.module";
import { NgModule } from "@angular/core";
import { CreateVideoLibraryComponent } from "./create-video-library/create-video-library.component";
import { VideoViewComponent } from "./video-view/video-view.component";
import { VideosComponent } from "./videos.component";
import { FormComponentModule } from "../../shared/components/form-component/form-component.module";
import { SharedModule } from "../../shared/modules/shared.module";
import { EsportsLoaderModule, EsportsHeaderModule } from 'esports';
import { HeaderSparkModule } from "../../shared/components/header-spark/header-spark.module";
import { FooterMenuMobileModule } from '../../shared/components/footer-menu-mobile/footer-menu-mobile.module';
import { FooterSparkModule } from '../../shared/components/footer-spark/footer-spark.module';
@NgModule({
  declarations: [
    VideosComponent,
    VideoViewComponent,
    CreateVideoLibraryComponent,
  ],
  imports: [CoreModule, EsportsLoaderModule.setColor('#00a851'), VideosRoutingModule, SharedModule, FormComponentModule, HeaderSparkModule, EsportsHeaderModule, FooterSparkModule, FooterMenuMobileModule],
  providers: [],
})
export class VideosModule { }
