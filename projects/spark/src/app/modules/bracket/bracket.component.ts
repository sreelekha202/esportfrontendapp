import { Component, OnInit, OnDestroy } from "@angular/core";
import { EsportsBracketService } from "esports";
import { AppHtmlRoutes } from "../../app-routing.model";

@Component({
  selector: "app-bracket",
  templateUrl: "./bracket.component.html",
  styleUrls: [
    "./bracket.component.scss",
    "../tournament/tournament.component.scss",
  ],
})
export class BracketComponent implements OnInit, OnDestroy {
  AppHtmlRoutes = AppHtmlRoutes;

  constructor(private bracketService: EsportsBracketService) {}

  ngOnInit(): void {}

  ngOnDestroy() {
    this.bracketService.setBracketData(null);
  }
}
