import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { TranslateService } from '@ngx-translate/core';
import {
  EsportsLanguageService,
  EsportsToastService,
  EsportsBracketService,
  IBracket,
} from 'esports';

@Component({
  selector: 'app-bracket-preview',
  templateUrl: './bracket-preview.component.html',
  styleUrls: [
    './bracket-preview.component.scss',
  ],
})
export class BracketPreviewComponent implements OnInit {
  bracket: IBracket;
  structure: any;
  isProcessing = false;
  isLoaded = false;

  constructor(
    public location: Location,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private bracketService: EsportsBracketService,
    private toastService: EsportsToastService,
    private translateService: TranslateService,
    private languageService: EsportsLanguageService
  ) {}

  ngOnInit(): void {
    this.languageService.language.subscribe((lang) =>
      this.translateService.use(lang || 'en')
    );
    this.bracket = this.bracketService.getBracketData();
    if (!this.bracket) {
      this.router.navigate(['../create'], { relativeTo: this.activeRoute });
    } else {
      this.fetchBracketStructure(this.bracket);
    }
  }

  /**
   * Fetch Mock Bracket or Match Structure
   * @param payload content meta data to generate bracket
   */
  fetchBracketStructure = async (payload) => {
    try {
      this.isLoaded = false;
      const response = await this.bracketService.generateBracket(payload);
      this.structure = response.data;
      this.isLoaded = true;
    } catch (error) {
      this.isLoaded = true;
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  /**
   * Save Or Update Bracket Details
   */
  save = async () => {
    try {
      this.isProcessing = true;
      const response = this.bracket?.id
        ? await this.bracketService.updateBracket(this.bracket.id, this.bracket)
        : await this.bracketService.saveBracket(this.bracket);

      this.toastService.showSuccess(response?.message);

      this.router.navigate(['../../profile/my-bracket'], {
        relativeTo: this.activeRoute,
      });

      this.bracketService.setBracketData(null);
      this.isProcessing = false;
    } catch (error) {
      this.isProcessing = false;
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };
}
