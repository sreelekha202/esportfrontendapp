import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
} from '@angular/core';

import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { FormBuilder } from '@angular/forms';
import { filter } from 'rxjs/operators';
import { Subscription } from 'rxjs';

import { AppHtmlRoutes } from '../../app-routing.model';
import { IPagination, EsportsHomeService } from 'esports';

export enum OngoingTournamentFilter {
  upcoming,
  ongoing,
  completed,
  all,
}

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SearchComponent implements OnInit, OnDestroy {
  text: string;
  active: any;
  mock_cards = [];
  isAllPage: boolean;
  activeRoute: string;
  AppHtmlRoutes = AppHtmlRoutes;
  OngoingTournamentFilter = OngoingTournamentFilter;
  tournamentFilter: OngoingTournamentFilter = OngoingTournamentFilter.all;
  page: IPagination;
  paginationData = {
    page: 1,
    limit: 50,
    sort: '-startDate',
  };

  private sub: Subscription;
  private sub1: Subscription;

  constructor(
    public translate: TranslateService,
    public router: Router,
    public activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    public homeService: EsportsHomeService,
    private changeDetectorRef: ChangeDetectorRef
  ) {
    this.initSubscription();
  }

  ngOnInit(): void {
    // TODO: find why we have this bug, there some issue deeper
    // fix select title on init page load (refresh rtc.)
    setTimeout(() => (this.activeRoute = this.router.url));
    this.onSearchCategory(AppHtmlRoutes.search);
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
    this.sub1.unsubscribe();
  }

  onSearchCategory(path: AppHtmlRoutes): void {
    this.activeRoute = path;
    this.tournamentFilter = OngoingTournamentFilter.all;
    this.handleTournamentStatusChange();
  }

  /**
   * Function will update filter value to search service.
   */
  handleTournamentStatusChange() {
    this.homeService.updateTournamentStatusFilter(this.tournamentFilter);
  }

  private initSubscription(): void {
    this.sub = this.activatedRoute.queryParamMap.subscribe((e) => {
      if (e.get('text')) {
        this.text = e.get('text');
        this.homeService.updateSearchParams(this.text, this.paginationData);
        this.homeService.searchTournament();
        this.homeService.searchArticle();
        this.homeService.searchVideo();
      }
    });

    this.sub1 = this.router.events
      .pipe(filter((event) => event instanceof NavigationEnd))
      .subscribe((event: NavigationEnd) => {
        const shouldContainTwoOrMoreSlashes = new RegExp('(?:.*?/){2}');
        this.isAllPage = !Boolean(
          shouldContainTwoOrMoreSlashes.test(event.url)
        );
      });
  }
}
