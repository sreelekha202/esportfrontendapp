import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SearchAllComponent } from './search-all/search-all.component';
import { SearchArticleComponent } from './search-article/search-article.component';
import { SearchShopComponent } from './search-shop/search-shop.component';

import { SearchComponent } from './search.component';
import { SearchTournamentsComponent } from './search-tournaments/search-tournaments.component';
import { SearchVideoComponent } from './search-video/search-video.component';

const routes: Routes = [
  {
    path: '',
    component: SearchComponent,
    children: [
      {
        path: '',
        component: SearchAllComponent,
        data: {
          tags: [
            {
              name: 'title',
            },
          ],
          title: 'Search |  Spark',
        },
      },
      {
        path: 'tournament',
        component: SearchTournamentsComponent,
      },
      {
        path: 'article',
        component: SearchArticleComponent,
      },
      {
        path: 'video',
        component: SearchVideoComponent,
      },
      {
        path: 'shop',
        component: SearchShopComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SearchRoutingModule {}
