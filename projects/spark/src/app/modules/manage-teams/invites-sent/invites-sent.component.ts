import {
  Component,
  OnInit,
  Input
} from '@angular/core';
import {
  EsportsUserService,
  EsportsToastService,
  EsportsLeaderboardService,
  EsportsProfileService
} from 'esports';
import { AppHtmlProfileRoutes,AppHtmlRoutes } from '../../../app-routing.model';
import { Router,ActivatedRoute } from '@angular/router';
import { environment } from '../../../../environments/environment';
const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;

@Component({
  selector: 'app-invites-sent',
  templateUrl: './invites-sent.component.html',
  styleUrls: ['./invites-sent.component.scss']
})
export class InvitesSentComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;
  constructor(
    private leaderboardService: EsportsLeaderboardService,
    private router: Router,
    private userService: EsportsUserService,
    private toastService: EsportsToastService,
    private _activateRoute: ActivatedRoute,
    private profileService: EsportsProfileService,
  ) { }
  show = false;
  isRemove = false;
  isSelectedData:any;
  pendingInvite = [
    {
      src : '../../../../assets/images/create-tournament/image.png',
      name: 'sitikus96'
    },
    {
      src : '../../../../assets/images/create-tournament/image-2.png',
      name: 'kerisss99'
    },
    {
      src : '../../../../assets/images/Profile/Ellipse 592.png',
      name: 'x0x0youlose'
    },
  ];
  AppHtmlProfileRoutes=AppHtmlProfileRoutes
  showLoader: boolean = false;
  teamManageData: any;
  InvitesSent = [];
  totalInvitesSent = [];
  teamId:any;
  isRemovePerson:any = 'invited_person';

  @Input() data: any;


  ngOnInit(): void {
    if (this.data && this.data.image) {
      this.teamManageData = this.data;
    }

    this._activateRoute.paramMap.subscribe((params) => {
      this.teamId = params.get('id');
    });
    this.getPendingSentInvites();
  }

  getPendingSentInvites() {
    this.showLoader = true;
    this.profileService.getTeamIvitationsSent(this.teamId, { page: 1, limit: 50 }).then(res=>{
      this.totalInvitesSent = res?.['data']?.['docs'] || [];
    }).catch(err => {
      this.totalInvitesSent = [];
    }).finally(()=>{
      this.showLoader = false;
    });
  }
  
  followUser(data,index) {
    if (data) {
      if (data.followingStatus == 0) {
        this.leaderboardService.followUser(API, data.teamMemberId).subscribe(
          (res: any) => {
            this.checkFollowStatus(data,index);
            this.toastService.showSuccess(res.message);
            this.userService.refreshCurrentUser(API, TOKEN);
          },
          (err) => {
             this.showLoader = false;
          }
        );
      } else if (data.followingStatus == 1) {
        this.leaderboardService.unfollowUser(API, data.teamMemberId).subscribe(
          (res: any) => {
            this.checkFollowStatus(data,index);
            this.toastService.showSuccess(res.message);
            this.userService.refreshCurrentUser(API, TOKEN);
          },
          (res: any) => {
             this.showLoader = false;
          }
        );
      }
    } else {
      // Navigate to the login page with extras
      //this.router.navigate(['/user/email-login']);
    }
  }

  checkFollowStatus(dat,indx) {
    this.leaderboardService.checkFollowStatus(API, dat.teamMemberId).subscribe(
      (res: any) => {
        this.totalInvitesSent[indx].followingStatus = res.data[0].status;
      },
      (err) => {
         this.showLoader = false;
      }
    );
  }

  deleteTeam(player, status) {
    let data = {
      teamId: this.teamId,
      userId: player.userId,
      name: player.name,
      status: status,
    };
    this.showLoader = true;
    this.userService.update_member(API, data).toPromise().then((data) => {
      this.toastService.showSuccess(data?.message);
      this.getPendingSentInvites();
    }).catch(err=> {})
  }

  // MOCK DATA
  showPopup(event){
    this.show = !this.show
    this.isRemove = event;

    if(event){
      this.deleteTeam(this.isSelectedData, 'deleted')
    }
  }
  isShowPopup(){
    this.show = true;
  }

  isSelectedRow(data){
    this.isSelectedData = data;
  }
}
