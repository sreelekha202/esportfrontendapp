import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InputInviteComponent } from './input-invite.component';

describe('InputInviteComponent', () => {
  let component: InputInviteComponent;
  let fixture: ComponentFixture<InputInviteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [InputInviteComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InputInviteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
