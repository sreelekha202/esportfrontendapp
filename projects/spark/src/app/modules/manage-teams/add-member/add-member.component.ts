import { Component, OnInit,ViewChild } from '@angular/core';
import { AppHtmlManageTeams,AppHtmlRoutes } from '../../../app-routing.model';
import { Router,ActivatedRoute } from '@angular/router';
import { environment } from '../../../../environments/environment';
import { Location } from '@angular/common';
import { TranslateService } from '@ngx-translate/core';
import { FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { SearchPlayersComponent } from '../../../shared/components/search-players/search-players.component';
import {
  IUser,
  EsportsUserService,
  GlobalUtils,
  EsportsToastService,
} from 'esports';

const API = environment.apiEndPoint;
@Component({
  selector: 'app-add-member',
  templateUrl: './add-member.component.html',
  styleUrls: ['./add-member.component.scss'],
})
export class AddMemberComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;
  show = false;
  isRemove = false;
  emailsforinvite = [];
  limit = 2;
  members = [];
  teamMeamber=[];
  teamId: any;
  form: FormGroup;
  myTeams = [];
  emailInvitation: any = [];
  currentUser: IUser;
  team: any;
  membersObj;
  searchText;
  memberid:any;
  select: boolean;
  listUser:any = [];
  userSubscription: Subscription;
  showLoader:boolean = false;
  @ViewChild(SearchPlayersComponent ) searchPlay: SearchPlayersComponent;

  constructor(
    private userService: EsportsUserService,
    public eSportsToastService: EsportsToastService,
    private _activateRoute: ActivatedRoute,
    private router: Router,
    private location: Location,
    private translateService: TranslateService,
  ) {}
  AppHtmlManageTeams = AppHtmlManageTeams;
  ngOnInit(): void {
    this._activateRoute.paramMap.subscribe((params) => {
      this.teamId = params.get('id');
    });

    this.getUserData();

    const query =  {_id: this.teamId };
    const encodedUrl = '?query='+encodeURIComponent(JSON.stringify(query));
    this.userService.getTeamList(API,encodedUrl).subscribe((res: any) => {
      let teamData:any;
      if(res?.data && res?.data[0]){
        teamData = res?.data[0];
        this.members = res?.data[0]?.member
        this.memberid = res?.data[0]?._id
      }
      let team: any = {
        id: this.teamId,
        admin: true,
        email: [],
        member: this.members,
        query: {
          condition: { _id: this.teamId },
          update: {
            logo: (teamData ? teamData.logo : ''),
            shortDescription: (teamData ? teamData.shortDescription : ''),
            social: {},
            teamName: (teamData ? teamData.name : ''),
          },
        },
        userName: (this.currentUser ? this.currentUser.username : ''),
      };
      this.team = team
    });
  }

  ngOnDestroy() {
    if(this.userSubscription)
    this.userSubscription.unsubscribe();
  }

  showMore(){
    this.limit += 3
  }
  emitValuefor(data) {
    this.emailsforinvite.push(data);
    this.emailInvitation = [];
    /* send recently added email here */
    this.emailInvitation = this.emailsforinvite[this.emailsforinvite.length-1];
    this.submitMember(); /* send invite button action will send mail here */
  }

  getUserData() {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.currentUser = data;
      }
    });
  }

  submitData(player) {
    this.teamMeamber.push(player)
    let noDublicates = this.removeDuplicates(this.teamMeamber, '_id')
    this.teamMeamber = noDublicates;
    this.limit = 2;
    this.submitMember();
  }

  removeDuplicates = (array, key) => {
    return array.reduce((arr, item) => {
      const removed = arr.filter(i => i[key] !== item[key]);
      return [...removed, item];
    }, []);
  };

  submitMember() {
    this.showLoader = true;
    if(this.teamMeamber[0] && this.teamMeamber[0].length){
      this.teamMeamber[0].forEach(member => {
        this.team.member.push(member);
      });
    }

    /* add your non member invited mail ID here */
    if(this.emailInvitation && this.emailInvitation.length){
      this.team.email = this.emailInvitation;
    }

      this.userService.team_update(API, this.team).subscribe(
        (res) => {
          if (res.data) {
            this.eSportsToastService.showSuccess(this.translateService.instant('CREATED_TEAM_MEMBER_INVITE'));
            //this.location.back();
            this.router.navigateByUrl(AppHtmlManageTeams.invitesSent+'/'+this.teamId);
            this.team.email = [];
            this.teamMeamber = [];
            this.searchPlay.clearSelected();
          } else {
            this.eSportsToastService.showError(
              this.translateService.instant('PROFILE.TEAMS.ERRORS.ERROR4')
            );
          }
          this.showLoader = false;
        },
        (err) => {
          this.showLoader = false;
          this.eSportsToastService.showError(err.error.message);
        }),
      (err) => {
        this.showLoader = false;
        this.eSportsToastService.showError(err.error.message);
      }

  }

  addMembers(player) {
    setTimeout(() => {
      if (this.membersObj && typeof this.membersObj == 'object') {
        if (
          !this.isMembersAlreadyAdded(
            this.members,
            this.memberid
          )
        ) {
          if (this.memberid != this.currentUser._id) {
            this.members.push(player);
          } else {
            this.eSportsToastService.showError(
              this.translateService.instant('PROFILE.TEAMS.ERRORS.ERROR4')
            );
          }
        } else {
          this.eSportsToastService.showError(
            this.translateService.instant('PROFILE.TEAMS.ERRORS.ERROR3')
          );
        }
        this.membersObj = '';
        this.searchText = '';
      }
    }, 100);
  }

  isMembersAlreadyAdded(arr, _id) {
    const { length } = arr;
    const id = length + 1;
    const found = arr.some((el) => el?._id === _id);
    return found;
  }
  showPopup(param) {
    this.show = !this.show
    this.isRemove = param
  }
  isShowPopup(){
    this.show = !this.show
  }
}
