import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AppHtmlProfileRoutes } from '../../../app-routing.model';
import { AppHtmlRoutes } from '../../../app-routing.model';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Location } from '@angular/common';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';
const API = environment.apiEndPoint;
import {
  EsportsConstantsService,
  EsportsToastService,
  EsportsTournamentService,
  EsportsUserService,
  GlobalUtils,
  S3UploadService,
  IUser,
} from 'esports';
import { Router } from '@angular/router';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'app-edit-team',
  templateUrl: './edit-team.component.html',
  styleUrls: ['./edit-team.component.scss'],
})
export class EditTeamComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;
  AppHtmlProfileRoutes = AppHtmlProfileRoutes;
  form: FormGroup;
  teamData: any;
  selectedSpecial: string = '';
  selectedSpecialObj: any;
  listData = [];
  base64textString: string;
  selectedFile: any;
  editImageUrl: any = null;
  clicked: boolean = false;
  fileToUploadLogo: File | null = null;
  fileToUploadBanner: File | null = null;
  checkFileNameLogo: boolean = false;
  checkFileNameBanner: boolean = false;
  toggleInputFileLogo: boolean = false;
  toggleInputFileBanner: boolean = false;
  toggleChangePrizePayout: boolean = false;
  toggleChangeTournamentSettings: boolean = false;
  toggleChangeRulesAndInfo: boolean = false;
  toggleChangeSponsors: boolean = false;

  teamId: any;
  teamBannerSRCFile: String = '';
  showLoader: boolean = false;
  currentUser: IUser;
  userSubscription: Subscription;
  socialUpdate: any = [];

  specialList = [
    {
      icon: 'assets/icons/socials/mdi_twitter.svg',
      label: 'Twitter',
      placeHolder: "Twitter URL",
      dataText:""
    },
    {
      icon: 'assets/icons/socials/mdi_fb.svg',
      label: 'Facebook',
      placeHolder: "Facebook URL",
      dataText:""
    },
    {
      icon: 'assets/icons/socials/icon-insta.svg',
      label: 'Instagram',
      placeHolder: "Instagram URL",
      dataText:""
    },
    {
      icon: 'assets/icons/socials/twitch.svg',
      label: 'Twitch',
      placeHolder: "Twitch URL",
      dataText:""
    },
    {
      icon: 'assets/icons/socials/discord.svg',
      label: 'Discord',
      placeHolder: "Discord URL",
      dataText:""
    },
  ];
  constructor(
    private _activatedRoute: ActivatedRoute,
    private userService: EsportsUserService,
    private toastService: EsportsToastService,
    private location: Location,
    private router: Router,
    public s3Service: S3UploadService,
    private eSportsToastService: EsportsToastService,
    private eSportsTournamentService: EsportsTournamentService,
    private translateService: TranslateService,
  ) {
    this.form = new FormGroup({
      teamName: new FormControl('', [Validators.required]),
      shortDescription: new FormControl(''),
      logo: new FormControl(''),
      social: new FormControl({}),
    });
  }
  ngOnInit(): void {
    this._activatedRoute.paramMap.subscribe((params) => {
      this.teamId = params.get('id');
    });
    const encodedUrl = '?query=' + encodeURIComponent(JSON.stringify({ _id: this.teamId }));
    this.userService.getTeamList(API, encodedUrl).subscribe((res: any) => {
      this.teamData = {
        teamLogo: res.data[0].logo,
        socialMedia: res.data[0]?.social,
        shortDescription: res.data[0].shortDescription,
        teamName: res.data[0].name,
        members: res.data[0].member,
        teamBanner: res.data[0].teamBanner,
      };

      if(this.teamData.socialMedia){
        for(var i=0;i<this.specialList.length;i++){
          if(this.teamData.socialMedia[this.specialList[i].label.toLowerCase()]){
            this.specialList[i].dataText = this.teamData.socialMedia[this.specialList[i].label.toLowerCase()];
            this.listData.push(this.specialList[i]);
            this.updateSocial(this.teamData.socialMedia[this.specialList[i].label.toLowerCase()],this.specialList[i]);
          }
        }
      }
      

      (this.editImageUrl = this.teamData.teamLogo),
        this.form.patchValue({
          teamName: this.teamData.teamName,
          shortDescription: this.teamData.shortDescription,
        });
    });
    this.getUserData();
  }

  ngOnDestroy() {
    if (this.userSubscription)
      this.userSubscription.unsubscribe();
  }

  getUserData() {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.currentUser = data;
      }
    });
  }

  onToggleChangeSpecial(special) {
    this.selectedSpecial = special.label
    this.selectedSpecialObj = special
  }
  onAddPlatform() {
    //this.specialList = this.specialList.filter((item) => item.label != this.selectedSpecialObj.label)
    if (this.selectedSpecialObj.label) {
      if (this.listData.findIndex(item => item.label === this.selectedSpecialObj.label) == -1)
        this.listData.push(this.selectedSpecialObj);
      else
        this.toastService.showInfo("Already " + this.selectedSpecialObj.label + " has been added");
    }
  }

  removeSocial(a, i) {
    let socialIndexId = this.listData[i].label;
    this.listData.splice(i, 1);
    let getSocilaIndex = this.socialUpdate.findIndex(item => item[socialIndexId])
    this.socialUpdate.splice(getSocilaIndex,1);
  }

  updateSocial(a, data) {
    if (data.label) {
      if (this.socialUpdate.findIndex(item => item[data.label]) == -1) {
        let getSocialLabel = data.label;
        let socialObj = {};
        socialObj[getSocialLabel] = a
        this.socialUpdate.push(socialObj);
      } else {
        let getSocilaIndex = this.socialUpdate.findIndex(item => item[data.label])
        this.socialUpdate[getSocilaIndex][data.label] = a;
      }
    }
  }

  fileChangeEvent(fileInput: any) {
    const reg = /(.*?)\.(jpg|jpeg|png|gif|giff)$/;
    if (!fileInput.target.files[0].name.match(reg)) {
      this.toastService.showError('Please select valid file');
      this.removeFile();
      return false;
    } else {
      this.removeFile();
      this.selectedFile = fileInput.target.files[0];
      var reader = new FileReader();
      reader.onload = this.handleReaderLoaded.bind(this);
      reader.readAsBinaryString(this.selectedFile);
    }
  }
  handleReaderLoaded(readerEvt) {
    var binaryString = readerEvt.target.result;
    this.base64textString = 'data:image/png;base64,' + btoa(binaryString);
    this.form.get('profilePicture').setValue(this.base64textString);
  }
  removeFile() {
    this.selectedFile = null;
  }

  updateTeam() {

    /* validate URL here */
    if(this.socialUpdate && this.socialUpdate.length){
      for(let soc of this.socialUpdate){
        if(soc['Facebook'] && !EsportsConstantsService.facebookRegex.test(soc['Facebook'])){
          this.toastService.showError(this.translateService.instant('MANAGE_TOURNAMENT.STREAM.ERROR.FACEBOOK_URL'));
          return;
        }else if(soc['Twitter'] && !EsportsConstantsService.twitterRegex.test(soc['Twitter'])){
          this.toastService.showError(this.translateService.instant('MANAGE_TOURNAMENT.STREAM.ERROR.TWITTER_URL'));
          return;
        }else if(soc['Instagram'] && !EsportsConstantsService.instagramRegex.test(soc['Instagram'])){
          this.toastService.showError(this.translateService.instant('MANAGE_TOURNAMENT.STREAM.ERROR.INSTAGRAM_URL'));
          return;
        }else if(soc['Twitch'] && !EsportsConstantsService.twitchRegex.test(soc['Twitch'])){
          this.toastService.showError(this.translateService.instant('MANAGE_TOURNAMENT.STREAM.ERROR.TWITCH_URL'));
          return;
        }else if(soc['Discord'] && !EsportsConstantsService.discordURLRegex.test(soc['Discord'])){
          this.toastService.showError(this.translateService.instant('MANAGE_TOURNAMENT.STREAM.ERROR.DISCORD_URL'));
          return;
        }
      }
    }

    let social:any = null;
    if(this.socialUpdate && this.socialUpdate.length){
      social = {};
      for(var i=0;i<this.specialList.length;i++){
        for(var j=0;j<this.socialUpdate.length;j++){
          if(this.socialUpdate[j][this.specialList[i].label]){
            social[this.specialList[i].label.toLowerCase()] = this.socialUpdate[j][this.specialList[i].label];
          }
        }
      }
    }
    let teamObj: any = {
      admin: true,
      id: this.teamId,
      email: [],
      member: this.teamData.members,
      query: {
        condition: { _id: this.teamId },
        update: {
          shortDescription: this.form.value.shortDescription,
          social: (social ? social : {}),
          teamName: this.form.value.teamName,
          logo: (this.teamData.teamLogo ? this.teamData.teamLogo : ''),
          teamBanner : (this.teamBannerSRCFile),
        },
      },
      userName: this.currentUser.username,
    };
    this.showLoader = true;
    this.userService.team_update(API, teamObj).subscribe(
      (res: any) => {
        this.clicked = false;
        if (res.data) {
          this.toastService.showSuccess(res.message);
          this.actionCancel();
        } else {
          this.toastService.showError(res.message);
        }
        this.showLoader = false;
      },
      (err) => {
        this.clicked = false;
        this.showLoader = false;
        this.toastService.showError(err.message);
      }
    );
  }

  actionCancel() {
    this.router.navigateByUrl('/profile/my-teams');
  }

  async handleFileInputBanner(files: FileList) {
    this.toggleInputFileBanner = !this.toggleInputFileBanner;
    this.checkFileNameBanner = true;
    this.fileToUploadBanner = files.item(0);
    let fileDiamention: any = { width: 1600, height: 900 };
    const upload: any = await this.s3Service.isValidImage(files, fileDiamention, 'banner');
    if (upload && upload == 'invalid_size') {
      this.eSportsToastService.showError(
        this.translateService.instant('TOURNAMENT.ERROR_IMG_2')
      );
      return;
    }
    else if (upload && upload == 'valid_image') {
      this.eSportsToastService.showError(
        this.translateService.instant('TOURNAMENT.ERROR_IMG_1')
      );

      const file = files[0];
      const reader = new FileReader();
      reader.readAsDataURL(file);
      let th = this;
      reader.onload = (e) => {
        let image: any = new Image();
        image.src = e.target.result;
        image.onload = function () {
          let height = this.height;
          let width = this.width;
          if (height != fileDiamention['height'] && width != fileDiamention['width']) {
            th.eSportsToastService.showInfo(
              th.translateService.instant('TOURNAMENT.ERROR_IMG')
            );
          }
        };
      }

      this.upload(files, 'banner');
    }
    /*this.isValidImage(files, { width: 1600, height: 900 },'banner');*/
  }

  async upload(files, type: string = 'banner') {
    let promises = [];
    for (let obj of files) {
      promises.push(this.toBase64(obj));
    }
    let Base64String = await Promise.all(promises);
    let imageData = {
      path: environment.tournamentS3BucketName,
      files: Base64String,
    };
    this.teamBannerSRCFile = '';
    this.s3Service.fileUpload(environment.apiEndPoint, imageData).subscribe(
      (res) => {
        if (type == 'banner') {
          this.teamBannerSRCFile = res['data'][0]['Location'];

          this.toggleInputFileBanner = !this.toggleInputFileBanner;
          this.checkFileNameBanner = true;
          this.fileToUploadBanner = files.item(0);

        }

        this.eSportsToastService.showSuccess(
          this.translateService.instant('TOURNAMENT.BANNER_UPLOADED')
        );
      },
      (err) => {
        this.eSportsToastService.showError(
          this.translateService.instant('TOURNAMENT.ERROR_LOGO')
        );
      }
    );
  }

  toBase64(file) {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = (error) => reject(error);
    });
  }

  saveChanged() {
    return this.toastService.showSuccess('Changed');
  }
}
