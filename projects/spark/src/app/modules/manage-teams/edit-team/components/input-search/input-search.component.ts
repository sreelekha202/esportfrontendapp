import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-input-search',
  templateUrl: './input-search.component.html',
  styleUrls: ['./input-search.component.scss'],
})
export class InputSearchComponent implements OnInit {
  @Input() placeholder: string = '';
  @Input() text: string = '';
  @Output() onTextChange = new EventEmitter<string>();
  constructor() { }
  ngOnInit(): void { }
  onTextChang = () => this.onTextChange.emit(this.text)
}
