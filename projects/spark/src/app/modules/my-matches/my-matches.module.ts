import { NgModule } from "@angular/core";
import { CoreModule } from "../../core/core.module";

import { MyMatchesRoutingModule } from "./my-matches-routing.module";
import { MyMatchesComponent } from "./my-matches.component";
import { MatchCardComponent } from "./components/match-card/match-card.component";
import { SharedModule } from "../../shared/modules/shared.module";

@NgModule({
  declarations: [MyMatchesComponent, MatchCardComponent],
  imports: [SharedModule, CoreModule, MyMatchesRoutingModule],
  exports: [],
})
export class MyMatchesModule {}
