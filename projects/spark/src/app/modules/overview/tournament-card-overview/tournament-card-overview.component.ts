import { Component, OnInit,Input } from '@angular/core';
import { AppHtmlRoutes } from '../../../app-routing.model';


@Component({
  selector: 'app-tournament-card-overview',
  templateUrl: './tournament-card-overview.component.html',
  styleUrls: ['./tournament-card-overview.component.scss'],
})
export class TournamentCardOverviewComponent implements OnInit {
  @Input() params;
  @Input() dataList;
  AppHtmlRoutes = AppHtmlRoutes;
  constructor() { }

  ngOnInit(): void {
  }
}
