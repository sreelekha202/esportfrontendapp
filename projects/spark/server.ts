import 'zone.js/dist/zone-node';

import { ngExpressEngine } from '@nguniversal/express-engine';
import express from 'express';
import { join } from 'path';

import { AppServerModule } from './src/main.server';
import { APP_BASE_HREF } from '@angular/common';
import { existsSync } from 'fs';
const domino = require('domino');
const fs_1 = require('fs');
const compression = require('compression');
const helmet = require('helmet');

(global as any).WebSocket = require('ws');
(global as any).XMLHttpRequest = require('xhr2');

import { environment } from './src/environments/environment';

function createTransformOptions() {
  const value = () => ({
    enumerable: true,
    configurable: true,
  });
  return { value };
}
function getMockMutationObserver() {
  return {};
}

function applyDomino(global, templatePath) {
  const template = fs_1.readFileSync(templatePath).toString();
  const win = domino.createWindow(template);
  global['window'] = win;
  global['document'] = win.document;
  global['localStorage'] = win.localStorage || {
    getItem: function () {},
    setItem: function () {},
  };
  global['navigator'] = win.navigator;
  Object.defineProperty(
    win.document.body.style,
    'transform',
    createTransformOptions()
  );
  global['CSS'] = null;
  global['Prism'] = null;
  global['MutationObserver'] = getMockMutationObserver();
}

//exports.applyDomino = applyDomino;
const BROWSER_DIR = join(process.cwd(), 'dist/spark/browser');

applyDomino(global, join(BROWSER_DIR, 'index.html'));
// The Express app is exported so that it can be used by serverless Functions.
export function app(): express.Express {
  const server = express();
  const distFolder = join(process.cwd(), 'dist/spark/browser');
  const indexHtml = existsSync(join(distFolder, 'index.original.html'))
    ? 'index.original.html'
    : 'index';

  server.use(compression());

  // Our Universal express-engine (found @ https://github.com/angular/universal/tree/master/modules/express-engine)
  server.engine(
    'html',
    ngExpressEngine({
      bootstrap: AppServerModule,
    })
  );

  server.set('view engine', 'html');
  server.set('views', distFolder);

  server.use(
    helmet.contentSecurityPolicy({
      directives: {
        upgradeInsecureRequests: [],
        blockAllMixedContent: [],
        defaultSrc: ["'none'"],
        'connect-src': [
          "'self'",
          environment.apiEndPoint,
          'wss://chat-spark.dynasty-dev.com',
          'https://chat-spark.dynasty-dev.com',
          'wss://chat-spark.dynasty-staging.com',
          'https://chat-spark.dynasty-staging.com',
          'https:',
        ],
        scriptSrc: [
          "'self'",
          // "'unsafe-inline'",
          'googletagmanager.com',
          'https://chat-spark.dynasty-dev.com',
          'wss://chat-spark.dynasty-dev.com',
          'https://chat-spark.dynasty-staging.com',
          'wss://chat-spark.dynasty-staging.com',
          'dynasty-staging.com',
          'dynasty-dev.com',
        ],
        scriptSrcElem: [
          "'self'",
          //unsafe-inline
          'https://www.googletagmanager.com',
          'http://www.googletagmanager.com',
          "'sha256-xgqphM9ePVp3yPBxalkUw/t0M76+eTbOpyYwz4nnSsg='",
        ],
        objectSrc: ["'none'"],
        styleSrc: ["'self'", 'https:', "'unsafe-inline'"],
        mediaSrc: ["'self'", 'static.zdassets.com'],
        fontSrc: [
          "'self'",
          'fonts.googleapis.com',
          'fonts.gstatic.com',
          'data:',
          'https://www.google.com',
          'https://*.google.com',
          'https://*.facebook.net',
          'https://cdnjs.cloudflare.com',
          'https://netdna.bootstrapcdn.com',
        ],
        'frame-src': [
          "'self'",
          // "'unsafe-inline'",
          'https://www.google.com',
          'https://*.google.com',
          'https://*.facebook.net',
          'https://player.twitch.tv/',
          'http://11015140.fls.doubleclick.net',
          'http://*.amazon-adsystem.com/',
          'https://bid.g.doubleclick.net',
          'https://9689267.fls.doubleclick.net/',
          'http://9689267.fls.doubleclick.net/',
          'https://insight.adsrvr.org/',
          'http://11069257.fls.doubleclick.net/',
          'https://www.facebook.com/',
          'https://www.youtube.com',
        ],
        'img-src': [
          "'self'",
          // "'unsafe-inline'",
          'https://paidia-image.s3.ca-central-1.amazonaws.com',
          'https://*',
          'data:',
          'blob:',
          'https://www.google.com',
          'https://*.google.com',
          'https://*.facebook.net',
          'https://*.fls.doubleclick.net',
          'http://ad.doubleclick.net/',
          'https://*.youtube.com',
        ],
        'frame-ancestors': ["'none'"],
        'base-uri': ["'self'"],
        'form-action': [
          "'self'",
          // "'unsafe-inline'",
          environment.apiEndPoint,
          'https://www.google.com',
          'https://*.google.com',
          'https://*.facebook.net',
          'https://www.facebook.com/',
        ],
        'manifest-src': ["'self'"],
      },
      reportOnly: false,
    })
  );
  server.use(helmet.dnsPrefetchControl());
  server.use(helmet.expectCt());
  server.use(helmet.frameguard());
  server.use(helmet.hidePoweredBy());
  server.use(helmet.hsts());
  server.use(helmet.ieNoOpen());
  server.use(helmet.noSniff());
  server.use(helmet.permittedCrossDomainPolicies());
  server.use(helmet.referrerPolicy());
  server.use((req, res, next) => {
    res.setHeader('X-XSS-Protection', '1; mode=block');
    next();
  });
  // Example Express Rest API endpoints
  // server.get('/api/**', (req, res) => { });
  // Serve static files from /browser
  server.get(
    '*.*',
    express.static(distFolder, {
      maxAge: '1y',
    })
  );

  // All regular routes use the Universal engine
  server.get('*', (req, res) => {
    res.render(indexHtml, {
      req,
      providers: [{ provide: APP_BASE_HREF, useValue: req.baseUrl }],
    });
  });

  return server;
}

function run(): void {
  const port = process.env.PORT || 8080;

  // Start up the Node server
  const server = app();
  server.listen(port, () => {
    console.log(`Node Express server listening on http://localhost:${port}`);
  });
}

// Webpack will replace 'require' with '__webpack_require__'
// '__non_webpack_require__' is a proxy to Node 'require'
// The below code is to ensure that the server is run only when not requiring the bundle.
declare const __non_webpack_require__: NodeRequire;
const mainModule = __non_webpack_require__.main;
const moduleFilename = (mainModule && mainModule.filename) || '';
if (moduleFilename === __filename || moduleFilename.includes('iisnode')) {
  run();
}

export * from './src/main.server';
