import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { EsportsGtmService, EventProperties } from "esports";

@Component({
  selector: 'app-settings-lightbox',
  templateUrl: './settings-lightbox.component.html',
  styleUrls: ['./settings-lightbox.component.scss'],
})
export class SettingsLightboxComponent implements OnInit {
  constructor(
    @Inject(MAT_DIALOG_DATA) public data,
    private gtmService: EsportsGtmService,
    public dialogRef: MatDialogRef<SettingsLightboxComponent>
  ) {}

  ngOnInit(): void {}

  onClose(): void {
    this.dialogRef.close();
  }

  onTabChange(tabIndex: number){
    switch (tabIndex) {
      case 2:
        this.pushGTMTags('View_User_Preferences')
        break;

      default:
        break;
    }
  }

  pushGTMTags(eventName, eventData: EventProperties = null) {
    this.gtmService.pushGTMTags(eventName, eventData)
  }

}
