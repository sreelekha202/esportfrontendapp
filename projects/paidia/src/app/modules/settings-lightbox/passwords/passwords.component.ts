
import { EsportsUserService } from 'esports';
import { EsportsToastService } from 'esports';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { AuthServices } from './../../../core/service/auth.service';

import { Component, OnInit, Output, EventEmitter } from '@angular/core';

import { MatDialogRef } from '@angular/material/dialog';
import { SettingsLightboxComponent } from '../settings-lightbox.component';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
@Component({
  selector: 'app-passwords',
  templateUrl: './passwords.component.html',
  styleUrls: ['./passwords.component.scss'],
})
export class PasswordsComponent implements OnInit {
  phonenumber = false;
  passworddiv = true;
  emailchange = false;
  passwordchange = false;
  selectedOption: string;
  showLoader: boolean;
  currentUser: any;
  userSubscription: Subscription;
  addForm: FormGroup;
  editImageUrl: any = null;
  selectedFile: any;
  name = 'Dynamic Add Fields';
  values = [];
  // gender = [
  //   { title: 'Female', value: 'female' },
  //   { title: 'Male', value: 'male' },
  // ]
  gender = [
    { title: 'She/Her', value: 'she' },
    { title: 'He/Him', value: 'he' },
    { title: 'They/Them', value: 'they', },
    { title: 'Custom', value: 'Custom', },
  ]

  stateList: any = [];
  countryList: any = [];
  base64textString: any;
  isExist: any;

  form = this.formBuilder.group({
    dobPolicy: [false],
  });
  todayDate: Date = new Date(
    new Date().setFullYear(new Date().getFullYear() - 10)
  );
  parentalDate: Date = new Date(
    new Date().setFullYear(new Date().getFullYear() - 18)
  );
  parental = false;
  @Output() dateChange = new EventEmitter();
  constructor(
    private userService: EsportsUserService,
    private authServices: AuthServices,
    private fb: FormBuilder,
    private toastService: EsportsToastService,
    public dialogRef: MatDialogRef<SettingsLightboxComponent>,
    private formBuilder: FormBuilder,
  ) {
    this.addForm = this.fb.group({
      email: [null, Validators.compose([Validators.required])],
      phonenumber: [null, Validators.compose([Validators.required])],
      password: [null, Validators.compose([Validators.required])]
    });
    this.userService.getAllCountries().subscribe((res) => { this.countryList = res.countries; })
  }

  ngOnInit(): void {

    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) { this.currentUser = data; this.setUserData() }
    });
  }

  onphoneChange(data) {
  }
  onemailChange(data) {
  }
  passwordChange(data) {

  }
  setUserData() {
    this.editImageUrl = this.currentUser.profilePicture,
      this.addForm.patchValue({
        'email': this.currentUser.email,
        'phonenumber': this.currentUser.phoneNumber,
        'password': this.currentUser.isPassword,
      })
  }
  phonenumberComp() {
    this.passworddiv = false;
    this.phonenumber = true;
  }
  emailComp() {
    this.passworddiv = false;
    this.emailchange = true;
  }
  passwordComp() {
    this.passworddiv = false;
    this.passwordchange = true;
  }
  onSubmit(formData: any) {
    // this.showLoader = true;
    // if (this.addForm.valid) {
    //   // for (let key in this.addForm.value) { formData.append(`${key}`, this.addForm.value[key]); }
    //   this.userService.updateProfile(this.addForm.value).subscribe((res) => {
    //     this.showLoader = false;
    //     this.setUserData();
    //     this.toastService.showSuccess(res.message)
    //     this.onClose()
    //     this.userService.refreshCurrentUser();

    //   }, (err) => {
    //     this.showLoader = false;
    //     this.toastService.showError(err.message)
    //   })
    // }
  }

  function() {
    this.selectedOption
  }
  addvalue() {
    this.values.push({ value: "" });
  }

  onClose(): void {
    this.dialogRef.close();
  }

  fileChangeEvent(fileInput: any) {
    const reg = /(.*?)\.(jpg|jpeg|png|gif|giff)$/;
    if (!fileInput.target.files[0].name.match(reg)) {
      this.toastService.showError("Please select valid file")
      this.removeFile();
      return false;
    } else {
      this.removeFile();
      this.selectedFile = fileInput.target.files[0];
      var reader = new FileReader();

      reader.readAsBinaryString(this.selectedFile);
    }
  }

  removeFile() {
    this.selectedFile = null;
  }
  onresetForm() {
    this.addForm.reset();
    // this.setUserData();
  }

  ngOnDestroy(): void { if (this.userSubscription) this.userSubscription.unsubscribe(); }

}
