import { Component, OnInit, Input } from '@angular/core';

interface ReferralCard {
  email: string;
  date: string;
  points: number | string;
}

@Component({
  selector: 'app-referral-card',
  templateUrl: './referral-card.component.html',
  styleUrls: ['./referral-card.component.scss'],
})
export class ReferralCardComponent implements OnInit {
  @Input() data: ReferralCard;

  constructor() {}

  ngOnInit(): void {}
}
