import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { EsportsToastService } from 'esports';
import { S3UploadService } from 'projects/paidia/src/app/core/service';

@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.scss']
})
export class BannerComponent implements OnInit {
  @Input() customFormControlName: string;
  @Input() customFormGroup: FormGroup;
  @Input() title: string;
  @Input() subtitle: string;
  @Input() dimension;
  @Input() size;
  @Input() type;
  @Input() id;
  @Input() required: boolean = false;
  isProcessing: boolean = false;
  imgurl: any;
  constructor(
    private s3UploadService: S3UploadService,
    private eSportsToastService: EsportsToastService
  ) { }
  ngOnInit(): void {}
  eventChange = async (event) => {
    try {
      this.isProcessing = true;
      const upload: any = await this.s3UploadService.validateAndUploadImage(
        event,
        this.dimension,
        this.size,
        this.type
      );
      this.imgurl = upload.data[0].Location;
      sessionStorage.setItem("imgurl", this.imgurl)
       this.customFormGroup.get(this.customFormControlName).setValue(upload);
      this.customFormGroup
        .get(this.customFormControlName)
        .setValue(this.imgurl);
      this.eSportsToastService.showSuccess(upload?.message);
      this.isProcessing = false;
    } catch (error) {
      this.isProcessing = false;
      const errorMessage = error?.error?.message || error?.message;
      this.eSportsToastService.showError(errorMessage);
    }
  };

}
