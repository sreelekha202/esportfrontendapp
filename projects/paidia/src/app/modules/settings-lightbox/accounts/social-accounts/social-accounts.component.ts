import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { EsportsUserService } from 'esports';
// import { FormControl } from '@angular/forms';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EsportsToastService } from 'esports';
import { SettingsLightboxComponent } from '../../settings-lightbox.component';
import { Subscription } from 'rxjs';
import { MatDialogRef } from '@angular/material/dialog';
import { environment } from '../../../../../environments/environment';
const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;
@Component({
  selector: 'app-social-accounts',
  templateUrl: './social-accounts.component.html',
  styleUrls: ['./social-accounts.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class SocialAccountsComponent implements OnInit {
  userSubscription: Subscription;
  // selectedGames = new FormControl();
  accounts = [];
  addForm: FormGroup;
  gameArray: FormArray;
  selectedGames = [];
  showLoader: boolean;
  currentUser: any;
  socialList = [
    {
      icon: 'https://paidia-image.s3.ca-central-1.amazonaws.com/dev/images/mdi_twitter.svg',
      media: 'Twitter',
    },
    {
      icon: 'https://paidia-image.s3.ca-central-1.amazonaws.com/dev/images/mdi_instagram.svg',
      media: 'Facebook',
    },
    {
      icon: 'https://paidia-image.s3.ca-central-1.amazonaws.com/dev/images/bi_Twitch.svg',
      media: 'Twitch',
    },
    {
      icon: 'https://paidia-image.s3.ca-central-1.amazonaws.com/dev/images/brandico_youtube.svg',
      media: 'Youtube',
    },
    {
      icon: 'https://paidia-image.s3.ca-central-1.amazonaws.com/dev/images/bi_discord.svg',
      media: 'Discord',
    },
  ];
  socialAccounts: any = [];
  constructor(
    private userService: EsportsUserService,
    private fb: FormBuilder,
    private toastService: EsportsToastService,
    public dialogRef: MatDialogRef<SettingsLightboxComponent>
  ) {
    this.addForm = this.fb.group({
      game: [''],
      game_array: new FormArray([]),
    });
    this.gameArray = this.addForm.get('game_array') as FormArray;
  }

  ngOnInit(): void {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.currentUser = data;
        this.setIcon();
      }
    });
  }
  addPlatform() {}
  setIcon() {
    this.socialAccounts = this.currentUser.socialAccounts;
    this.socialList.map((icon) => {
      this.currentUser.socialAccounts.map((obj, i) => {
        if (icon.media == obj.media) {
          this.socialAccounts[i]['icon'] = icon.icon;
        }
      });
    });
    this.setUserData();
  }

  onChangeGame() {
    if (this.addForm.value.game.length < this.selectedGames.length) {
      let difference = this.selectedGames.filter(
        (x) => !this.addForm.value.game.includes(x)
      );
      this.gameArray.value.map((obj, index) => {
        if (obj.media == difference[0]) {
          this.gameArray.removeAt(index);
          const i = this.selectedGames.indexOf(index);
          if (index > -1) {
            this.selectedGames.splice(index, 1);
          }
        }
      });
    }

    if (true) {
      this.addForm.value.game.map((value) => {
        this.socialList.map((o: any) => {
          if (o.media == value) {
            if (!this.selectedGames.includes(value)) {
              this.selectedGames.push(value);
              this.gameArray.push(
                this.fb.group({
                  profileUrlLink: [
                    '',
                    Validators.compose([Validators.required]),
                  ],
                  media: [o.media],
                  _id: [o._id],
                  icon: [o.icon],
                })
              );
            }
          }
        });
      });
    }
  }

  setUserData() {
    let tempdata = [];
    this.socialAccounts.map((o) => {
      this.selectedGames.push(o.media);
      tempdata.push(o.media);

      this.gameArray.push(
        this.fb.group({
          profileUrlLink: [
            o.profileUrlLink,
            Validators.compose([Validators.required]),
          ],
          media: [o.media],
          _id: [o._id],
          icon: [o.icon],
        })
      );
    });
    this.addForm.patchValue({
      game: tempdata,
    });
  }

  deleteObjGameArray(item, index) {
    let tempdata = this.addForm.value.game;
    tempdata.map((obj, i) => {
      if (obj == item.value.media) {
        tempdata.splice(i, 1);
      }
    });
    this.gameArray.removeAt(index);
    const i = this.selectedGames.indexOf(index);
    if (index > -1) {
      this.selectedGames.splice(index, 1);
    }
    this.addForm.patchValue({
      game: tempdata,
    });
  }

  onClose(): void {
    this.dialogRef.close();
  }

  onSubmit() {
    this.showLoader = true;
    if (this.addForm.valid) {
      this.userService
        .updateProfile(API, {
          socialAccounts: this.addForm.value.game_array,
        })
        .subscribe(
          (res) => {
            this.showLoader = false;
            this.setUserData();
            this.toastService.showSuccess(res.message);
            this.addForm.reset();
            // this.addForm.value.game_array.map((d,i)=>{this.gameArray.removeAt(i-1),i})
            while (this.gameArray.length !== 0) {
              this.gameArray.removeAt(0);
            }
            this.userService.refreshCurrentUser(API, TOKEN);
          },
          (err) => {
            this.showLoader = false;
            this.toastService.showError(err.error.message);
          }
        );
    }
  }

  ngOnDestroy(): void {
    if (this.userSubscription) this.userSubscription.unsubscribe();
  }
}
