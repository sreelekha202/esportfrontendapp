import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { EsportsUserService, EsportsGameService } from 'esports';
// import { FormControl } from '@angular/forms';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EsportsToastService } from 'esports';
import { SettingsLightboxComponent } from '../../settings-lightbox.component';
import { Subscription } from 'rxjs';
import { MatDialogRef } from '@angular/material/dialog';
import { environment } from '../../../../../environments/environment';
const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;
@Component({
  selector: 'app-game-accounts',
  templateUrl: './game-accounts.component.html',
  styleUrls: ['./game-accounts.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class GameAccountsComponent implements OnInit {
  userSubscription: Subscription;
  // selectedGames = new FormControl();
  accounts = [];
  gamesList = [];
  addForm: FormGroup;
  gameArray: FormArray;
  selectedGames = [];
  showLoader: boolean;
  currentUser: any;

  constructor(
    private gameService: EsportsGameService,
    private userService: EsportsUserService,
    private fb: FormBuilder,
    private toastService: EsportsToastService,
    public dialogRef: MatDialogRef<SettingsLightboxComponent>
  ) {
    this.addForm = this.fb.group({
      game: [''],
      game_array: new FormArray([]),
    });
    this.gameArray = this.addForm.get('game_array') as FormArray;
  }

  ngOnInit(): void {
    this.getGamseList();
  }

  getGamseList() {
    this.gameService.getGames(API).subscribe((res) => {
      this.gamesList = res?.data;
      this.userSubscription = this.userService.currentUser.subscribe((data) => {
        if (data) {
          this.currentUser = data;
          this.setUserData();
        }
      });
    });
  }

  onChangeGame() {
    if (this.addForm.value.game.length < this.selectedGames.length) {
      let difference = this.selectedGames.filter(
        (x) => !this.addForm.value.game.includes(x)
      );
      this.gameArray.value.map((obj, index) => {
        if (obj._id == difference[0]) {
          this.gameArray.removeAt(index);
          const i = this.selectedGames.indexOf(index);
          if (index > -1) {
            this.selectedGames.splice(index, 1);
          }
        }
      });
    }

    if (true) {
      this.addForm.value.game.map((value) => {
        this.gamesList.map((o) => {
          if (o._id == value) {
            if (!this.selectedGames.includes(value)) {
              this.selectedGames.push(value);
              this.gameArray.push(
                this.fb.group({
                  userGameId: ['', Validators.compose([Validators.required])],
                  name: [o.name],
                  _id: [o._id],
                  image: [o.logo],
                })
              );
            }
          }
        });
      });
    }
  }

  setUserData() {
    let tempdata = [];
    this.currentUser?.preference?.gameDetails.map((o) => {
      let gameLogo = this.gamesList.find(ele => {
        return ele?._id == o?._id;
      })
      this.selectedGames.push(o?._id);
      tempdata.push(o?._id);
      this.gameArray.push(
        this.fb.group({
          userGameId: [o?.userGameId, Validators.compose([Validators.required])],
          name: [o?.name],
          _id: [o?._id],
          image: [gameLogo?.logo],
        })
      );
    });
    this.addForm.patchValue({
      game: tempdata,
    });
  }

  deleteObjGameArray(item, index) {
    let tempdata = this.addForm.value.game;
    tempdata.map((obj, i) => {
      if (obj == item.value._id) {
        tempdata.splice(i, 1);
      }
    });
    this.gameArray.removeAt(index);
    const i = this.selectedGames.indexOf(index);
    if (index > -1) {
      this.selectedGames.splice(index, 1);
    }
    this.addForm.patchValue({
      game: tempdata,
    });
  }

  onClose(): void {
    this.dialogRef.close();
  }

  onSubmit() {
    this.showLoader = true;
    if (this.addForm.valid) {
      this.userService
        .updateProfile(API, {
          gameDetails: this.addForm.value.game_array,
        })
        .subscribe(
          (res) => {
            this.showLoader = false;
            this.setUserData();
            this.toastService.showSuccess(res.message);
            this.addForm.reset();
            // this.addForm.value.game_array.map((d,i)=>{this.gameArray.removeAt(i-1),i})
            while (this.gameArray.length !== 0) {
              this.gameArray.removeAt(0);
            }
            this.userService.refreshCurrentUser(API, TOKEN);
          },
          (err) => {
            this.showLoader = false;
            this.toastService.showError(err.error.message);
          }
        );
    }
  }

  ngOnDestroy(): void {
    if (this.userSubscription) this.userSubscription.unsubscribe();
  }
}
