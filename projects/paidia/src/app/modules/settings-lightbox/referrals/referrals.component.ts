import { Component, OnInit } from '@angular/core';
import { EsportsUserService} from 'esports';
import { environment } from '../../../../environments/environment';
const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;
@Component({
  selector: 'app-referrals',
  templateUrl: './referrals.component.html',
  styleUrls: ['./referrals.component.scss'],
})
export class ReferralsComponent implements OnInit {
  cardLink: string = 'https://paidia.dynasty-dev.com/user/referral:';
  currentUser: any;
  accepted = [];
  sended = [];

  paginationData = {
    page: 1,
    limit: 50,
    sort: { createdOn: -1 },
  };
  constructor(private userService: EsportsUserService) {
    this.userService.currentUser.subscribe((res) => {
      this.currentUser = res;
      this.cardLink = this.cardLink + this.currentUser.accountDetail.referralId
    })

    this.accepted = [
    ];

    this.sended = [
    ];
  }

  ngOnInit(): void { this.getReferralUserList() }

  getReferralUserList() {
    const pagination = JSON.stringify(this.paginationData);
    this.userService.getUserinviteesList(API,{ pagination }).subscribe((res) => {
      // this.accepted = res.data.accepted;
      // this.sended = res.data.sended;
    })
  }
}
