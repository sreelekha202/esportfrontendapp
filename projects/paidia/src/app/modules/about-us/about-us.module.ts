import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CarouselModule } from 'ngx-owl-carousel-o';
import { AboutUsComponent } from './about-us.component';
import { RouterModule, Routes } from '@angular/router';import { RouterBackModule } from '../../shared/directives/router-back.module';
import { VimeoViewerModule } from '../../shared/directives/vimeo-viewer.module';
const routes: Routes = [
  {
    path: '',
    component: AboutUsComponent,
  },
];
@NgModule({
  declarations: [
    AboutUsComponent,
  ],
  imports: [
    CommonModule,
    CarouselModule,
    VimeoViewerModule,
    RouterModule.forChild(routes)
  ]
})
export class AboutUsModule {

}
