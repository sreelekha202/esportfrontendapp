import { Component, OnInit, HostListener, Inject } from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';
@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.scss']
})
export class AboutUsComponent implements OnInit {
  activeClass1 = 0;
  customOptions: OwlOptions = {
    loop: true,
    autoplay: false,
    autoplayTimeout: 4000,
    center: true,
    dots: false,
    nav: true,
    navText: ["<", ">"],
    autoHeight: true,
    autoWidth: true,
    responsive: {
      0: {
        items: 3
      },
      600: {
        items: 3,
      },
      900: {
        items: 5,
      }
    }
  }
  constructor(
  ) {

  }

  @HostListener('window:scroll', ['event'])

  ngOnInit(): void {
  }

  scroll() {
    document.body.style.height = "1600px";
    window.scrollTo(0, 1600);
  }

}
