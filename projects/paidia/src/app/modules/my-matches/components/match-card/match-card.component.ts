import { Component, OnInit, Input } from "@angular/core";
import { Router } from "@angular/router";
import { AppHtmlRoutes } from "../../../../app-routing.model";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
interface MatchCard {
  title: string;
  opponentName: string;
  gameAccount: number;
  image: string;
  tournamentId: string;
  tournamentSlug: string;
  tournamentName: string;
  matchId: string;
  match: object;
  participantId: string;
  isAdmin: boolean;
}

@Component({
  selector: "app-match-card",
  templateUrl: "./match-card.component.html",
  styleUrls: ["./match-card.component.scss"],
})
export class MatchCardComponent implements OnInit {
  @Input() data: MatchCard;

  constructor(private router: Router, private modalService: NgbModal) {}

  ngOnInit(): void {}

  genrateReport() {
    if (this.data?.tournamentId && this.data?.matchId) {
      this.router.navigate([
        `${AppHtmlRoutes.tournament}/report/${this.data?.matchId}/${this.data?.tournamentId}`,
      ]);
    }
  }
  openResult(content) {
    this.modalService.open(content, {
      centered: true,
      size: "lg",
      scrollable: false,
      windowClass: "custom-modal-content",
    });
  }

  exitFromScoreCard(data) {
    this.modalService.dismissAll();
  }

  viewMatch() {
    if (this.data?.tournamentSlug) {
      this.router.navigate([
        `${AppHtmlRoutes.tournament}/${this.data?.tournamentSlug}`,
      ]);
    }
  }
}
