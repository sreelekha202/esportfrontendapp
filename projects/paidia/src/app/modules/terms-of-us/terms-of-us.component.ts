import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-terms-of-us',
  templateUrl: './terms-of-us.component.html',
  styleUrls: ['./terms-of-us.component.scss'],
})
export class TermsOfUsComponent implements OnInit {
  
  privacypolicy = 'keydefinitions';

  constructor(private location: Location) {}

  ngOnInit(): void {}

  goBack() {
    this.location.back();
  }
  changevalues(val:any){
    this.privacypolicy = val;
    }
}
