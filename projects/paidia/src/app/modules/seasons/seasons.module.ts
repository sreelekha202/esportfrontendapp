import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SeasonsListComponent } from './seasons-list/seasons-list.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../shared/modules/shared.module';
import { CoreModule } from '../../core/core.module';
import { RouterBackModule } from '../../shared/directives/router-back.module';

export const routes: Routes = [
  {
    path: '',
    component: SeasonsListComponent,
  },
];

@NgModule({
  declarations: [SeasonsListComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule,
    CoreModule,
    RouterBackModule
  ],
})
export class SeasonsModule {}
