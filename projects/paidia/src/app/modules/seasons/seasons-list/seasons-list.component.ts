import { Component, OnInit } from '@angular/core';
import { EsportsGameService, EsportsSeasonService } from 'esports';
import { environment } from '../../../../environments/environment';
const API = environment.apiEndPoint;

@Component({
  selector: 'app-seasons-list',
  templateUrl: './seasons-list.component.html',
  styleUrls: ['./seasons-list.component.scss'],
})
export class SeasonsListComponent implements OnInit {
  text: string = '';
  selected_game: any = '';
  gameList = [];
  seasonsList = [];
  season_period_list = [
    {
      id: 1,
      name: '20 days',
    },
  ];
  season_period = this.season_period_list[0].id;
  showLoader: boolean = false;

  paginationData = {
    page: 1,
    limit: 10,
    sort: 'startDate',
  };
  paginationDetails: any;
  pageSizeOptions = environment.pageSizeOptions;

  constructor(
    private gameService: EsportsGameService,
    private esportsSeasonService: EsportsSeasonService
  ) {}

  ngOnInit(): void {
    this.showLoader = true;
    this.getGames();
    this.getAllSeasons();
  }

  onSearch = (event: any) => {
    this.text = String(event.target.value);
    this.text?.length > 3
      ? this.getAllSeasons()
      : this.text?.length == 0
      ? this.getAllSeasons()
      : '';
  };
  getGames() {
    this.showLoader = true;
    this.gameService.getGames(API).subscribe(
      (res) => {
        this.gameList = res.data;
        this.showLoader = false;
      },
      (err) => {
        this.showLoader = false;
      }
    );
  }

  getAllSeasons() {
    let query: any = `isActive=true&fields=isFeatured,name,slug,endDate,startDate,image,duration&sort=-createdOn&limit=${this.paginationData.limit}&page=${this.paginationData.page}`;
    query = this.selected_game ? `${query}&game=${this.selected_game}` : query;
    query = this.text ? `${query}&text=${this.text}` : query;
    this.esportsSeasonService.getSeasons(API, query).subscribe(
      (res) => {
        this.showLoader = false;
        this.seasonsList = res.data.data.docs;
        this.paginationDetails = res.data.data;
        this.paginationData.limit = this.paginationDetails.limit;
        this.paginationData.page = this.paginationDetails.page;
      },
      (err) => {
        this.showLoader = false;
      }
    );
  }

  pageChanged(page): void {
    this.paginationData.page = page.pageIndex + 1;
    this.paginationData.limit = page.pageSize;
    this.getAllSeasons();
  }
}
