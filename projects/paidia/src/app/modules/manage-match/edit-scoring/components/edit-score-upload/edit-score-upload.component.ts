import { Component, OnInit, Input, Output,EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { EsportsToastService } from 'esports';
import { S3UploadService } from 'projects/paidia/src/app/core/service';

@Component({
  selector: 'app-match-edit-score-upload',
  templateUrl: './edit-score-upload.component.html',
  styleUrls: ['./edit-score-upload.component.scss']
})
export class EditScoreUploadMatchComponent implements OnInit {


  @Input() customFormControlName: string;
  @Input() customFormGroup: FormGroup;

  @Input() title: string;
  @Input() subtitle: string;

  @Input() dimension;
  @Input() size;
  @Input() type;

  @Input() id;
  @Input() required: boolean = false;
  @Output() imgurltosave = new EventEmitter();

  isProcessing: boolean = false;
  imgurl: any = '';


  constructor(
    private s3UploadService: S3UploadService,
    private eSportsToastService: EsportsToastService
  ) { }
  ngOnInit(): void { }
  eventChange = async (event) => {
    try {

      this.isProcessing = true;

      const upload: any = await this.s3UploadService.validateAndUploadImage(
        event,
        this.dimension,
        this.size,
        this.type
      );
       this.imgurl = upload.data[0].Location;
        this.imgurltosave.emit(this.imgurl)
    } catch (error) {
      this.isProcessing = false;
      const errorMessage = error?.error?.message || error?.message;
      this.eSportsToastService.showError(errorMessage);
    }
  };


}
