import { Component, Inject, OnInit, PLATFORM_ID } from '@angular/core';
import { AppHtmlRoutes } from '../../app-routing.model';
import { ActivatedRoute } from '@angular/router';
import {
  Subscription,
} from 'rxjs';
import { isPlatformBrowser } from '@angular/common';
import {
  EsportsParticipantService,
  EsportsUserService,
  GlobalUtils,
  IUser,
} from 'esports';
import { TranslateService } from '@ngx-translate/core';
import { environment } from '../../../environments/environment';
const API = environment.apiEndPoint;
@Component({
  selector: 'app-manage-match',
  templateUrl: './manage-match.component.html',
  styleUrls: ['./manage-match.component.scss'],
})
export class ManageMatchComponent implements OnInit {
  userSubscription: Subscription;
  AppHtmlRoutes = AppHtmlRoutes;
  timezone;
  match: any = {};
  user: IUser;
  apiLoaded: Array<boolean> = [];
  hideMessage = true;
  hasRequiredAccess: boolean = true;
  tournamentStartTime = '';
  startDateIntervalId = null;
  endDateIntervalId = null;
  noStart = false;
  isLoaded = false;

  constructor(
    private activatedRoute: ActivatedRoute,
    @Inject(PLATFORM_ID) private platformId,
    private userService: EsportsUserService,
    private translateService: TranslateService,
    private participantService: EsportsParticipantService,
  ) {}

  ngOnInit(): void {
    const isBrowser = isPlatformBrowser(this.platformId);
    if (isBrowser && GlobalUtils.isBrowser()) {
      this.match._id = this.activatedRoute.snapshot.params.id; // get slug from route
      if (this.match._id) {
        this.getCurrentUserDetails();
      }
    }
  }

  getCurrentUserDetails = async () => {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.user = data;
        const hasAdminAccess =
          this.user?.accountType == 'admin' &&
          this.user?.accessLevel?.includes('em');

        this.hasRequiredAccess = hasAdminAccess;
        if (!this.hasRequiredAccess) {
          throw new Error(
            this.translateService.instant('VIEW_TOURNAMENT.ACCESS_DENIED')
          );
        } else {
          this.getMatchDetails()
        }
      }
    });
  };

  getMatchDetails() {
    const dataSubscription = this.participantService
      .getMatchDetails(API, this.match._id)
      .subscribe((res:any) => {
        this.match = res?.data || null;
        dataSubscription.unsubscribe();
      });
  }
}
