import { CoreModule } from '../../core/core.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { RouterBackModule } from './../../shared/directives/router-back.module';
import { HeaderPaidiaModule } from "../../shared/components/header-paidia/header-paidia.module";
import { SharedModule } from '../../shared/modules/shared.module';
import {BracketTreeModule} from '../../shared/components/bracket-tree/bracket-tree.module';
import { EsportsCustomPaginationModule } from 'esports';
import { ManageMatchComponent } from './manage-match.component';
import { HeaderComponent } from './header/header.component';
import { EditMatchScoringComponent } from './edit-scoring/edit-scoring.component';
import { TeamCardMatchComponent } from './edit-scoring/components/team-card/team-card.component';
import { EditScoreUploadMatchComponent } from './edit-scoring/components/edit-score-upload/edit-score-upload.component';

export const routes: Routes = [
  {
    path: '',
    component: ManageMatchComponent,
  }
]

@NgModule({
  declarations: [
    ManageMatchComponent,
    HeaderComponent,
    EditMatchScoringComponent,
    TeamCardMatchComponent,
    EditScoreUploadMatchComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule,
    HeaderPaidiaModule,
    CoreModule,
    RouterBackModule,
    BracketTreeModule,
    EsportsCustomPaginationModule
  ]
})
export class ManageMatchModule { }
