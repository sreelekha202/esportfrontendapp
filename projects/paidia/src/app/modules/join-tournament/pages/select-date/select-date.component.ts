import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TournamentService } from 'projects/paidia/src/app/core/service';
import { timeFormatAMPM } from 'projects/paidia/src/app/shared/comon';
import { Subscription } from 'rxjs-compat';
import { AppHtmlRoutes } from '../../../../app-routing.model';

@Component({
  selector: 'app-select-date',
  templateUrl: './select-date.component.html',
  styleUrls: ['./select-date.component.scss'],
})
export class SelectDateComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;
  date: Date = new Date();
  time: any = timeFormatAMPM(new Date())
  tournamentDetails: Subscription;
  platforms = [];
  createTournament: any;

  constructor(
    private tournamentService: TournamentService,
    private router: Router) { }

  ngOnInit(): void {
    this.tournamentDetails = this.tournamentService.createTournament.subscribe((data) => {
      if (data) {
        this.createTournament = data;
      }
    });
  }
  onDateChange(data) { }
  onTimeChange(data) { }
  next = () => {
    this.tournamentService.createTournamentSubject.next({ ...this.createTournament, "start_date": this.date, "start_time": this.time })
    this.router.navigateByUrl("create-tournament/details");
  }
  ngOnDestroy(): void {
    if (this.tournamentDetails)
      this.tournamentDetails.unsubscribe();
  }
}
