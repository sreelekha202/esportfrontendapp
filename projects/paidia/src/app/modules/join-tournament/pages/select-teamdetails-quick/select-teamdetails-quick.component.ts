import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from './../../../../core/service/user.service';
import { AppHtmlRoutes } from '../../../../app-routing.model';
import { ProfileService } from './../../../../core/service/profile.service';
import { EsportsToastService } from 'esports';

@Component({
  selector: 'app-select-teamdetails-quick',
  templateUrl: './select-teamdetails-quick.component.html',
  styleUrls: ['./select-teamdetails-quick.component.scss']
})
export class SelectTeamdetailsQuickComponent implements OnInit {

  d = ['a', 'b']
  error: boolean = false;
  AppHtmlRoutes = AppHtmlRoutes;
  constructor(private profileService: ProfileService,
    private router: Router,
    private userService: UserService,
    private toastService: EsportsToastService) { }
  selected: any;
  selectedteam: any;
  tournamentDetails: any;
  showLoader = false;
  ngOnInit(): void {
    this.tournamentDetails = localStorage.getItem("tournamentDetails")
    let data;
    data = localStorage.getItem("userDetails")
    this.tournamentDetails = JSON.parse(this.tournamentDetails)
    data = JSON.parse(data)
    //   this.userService.currentUser.subscribe((data) => {
    this.selectedteam = [{
      "name": data.fullName,
      "email": data.email,
      "phoneNumber": data.phoneNumber,
      "inGamerUserId": data.inGamerUserId
    }
    ]
    //   }]
    let data1 = Math.floor(Math.random() * 100)
    let id = 'SSS' + data1;
    // this.profileService.ckeckuserid(id,this.currentSlide._id).subscribe((data3:any)=>{
    //   if(data3.data.invalidId == false && data3.data.isExist == false){
    //     let datatosend = {
    //       "teamId": "",
    //       "logo": "https://stc-img.s3.me-south-1.amazonaws.com/dev/profile/1624437521923.png",
    //       "teamName": "",
    //       "name": data.fullName,
    //       "phoneNumber": data.phoneNumber,
    //       "email": data.email,
    //       "inGamerUserId": id,
    //       "teamMembers": [],
    //       "substituteMembers": [],
    //       "participantType": this.currentSlide.participantType,
    //       "tournamentId": this.currentSlide._id
    //   }
    //   this.profileService.jointournament(datatosend).subscribe((data:any)=>{
    //     this.showLoader = false;
    //     //this.router.navigateByUrl('/play')
    //   }, (err) => {
    //     this.showLoader = false;
    //     this.toastService.showError(err.error.message) })
    //   }
    // })
    //});

  }

  checkuserid(data) {
    this.showLoader = true;
    this.profileService.ckeckuserid(data, this.tournamentDetails._id).subscribe((data3: any) => {
      this.showLoader = false;
      if (data3.data.invalidId == false && data3.data.isExist == false) {
        this.error = false;
      } else {
        this.error = true;
      }
    })
  }

  submitdata() {
    this.showLoader = true;
    let datatosend = {
      "teamId": "",
      "logo": "https://stc-img.s3.me-south-1.amazonaws.com/dev/profile/1624437521923.png",
      "teamName": "",
      "name": this.selectedteam[0].name,
      "phoneNumber": this.selectedteam[0].phoneNumber,
      "email": this.selectedteam[0].email,
      "inGamerUserId": this.selectedteam[0].inGamerUserId,
      "teamMembers": [],
      "substituteMembers": [],
      "participantType": this.tournamentDetails.participantType,
      "tournamentId": this.tournamentDetails._id
    }
    this.profileService.jointournament(datatosend).subscribe((data: any) => {
      this.showLoader = false;
      this.toastService.showSuccess(data.message)
      this.router.navigateByUrl('/play')
    }, (err) => {
      this.showLoader = false;
      this.toastService.showError(err.error.message)
      this.router.navigateByUrl('/play')
    });

  }


}
