import { Component, OnInit } from '@angular/core';
import { AppHtmlJoinTournamentRoutes } from '../../../../app-routing.model';
import { Router } from '@angular/router';
import { TournamentService } from './../../../../core/service';
import { EsportsToastService, EsportsUserService } from 'esports';
import { AppHtmlRoutes } from '../../../../app-routing.model';
import { environment } from '../../../../../environments/environment';
const API = environment.apiEndPoint;
@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;
  selectedteam = null;
  joinTournament: any;
  AppHtmlJoinTournamentRoutes = AppHtmlJoinTournamentRoutes;

  teams = [];
  paginationData = {
    page: 1,
    limit: 100,
    sort: { _id: -1 },
    text: '',
  };
  showLoader = false;
  tournament: any;

  constructor(private route: Router,
    private tournamentService: TournamentService,
    private toastService: EsportsToastService,
    private userService: EsportsUserService) {
    this.tournament = JSON.parse(localStorage.getItem('tournamentDetails'))
  }

  ngOnInit(): void {
    this.getMyTeams();
    this.showLoader = true;

  }

  getMyTeams = async () => {
    const data = await this.userService.getJoinMyTeam(API, this.tournament?._id);
    if (data) {
      this.showLoader = false;
      data?.data.forEach((control, index) => {
        let totalMemberSize = 0;

        if (this.tournament?.substituteMemberSize > 0) {
          totalMemberSize +=
            this.tournament?.teamSize + this.tournament?.substituteMemberSize;
        } else {
          totalMemberSize = this.tournament?.teamSize;
        }
        if (control.teamSize >= totalMemberSize) {
          let d1 = {
            image: control.logo,
            name: control.teamName,
            teamsLength: 5,
            createtdAt: 1,
            _id: control._id
          }
          this.teams.push(d1)
          // this.teams.push(control);
        }
      });
    }
  };
  // getMyTeams() {
  // this.userService.getJoinMyTeam('API', 'this.tournament?._id').subscribe((res) => {
  //   this.teams = []
  //   for (let d of res.data.docs) {
  //     let d1 = {
  //       image:
  //         d.logo,
  //       name: d.teamName,
  //       teamsLength: 5,
  //       createtdAt: 1,
  //       _id: d._id
  //     }
  //     this.teams.push(d1)

  //   }
  //   this.showLoader = false;
  // }, (err) => {
  //   this.showLoader = false;
  // })
  // }

  confirm() {
    if (this.selectedteam) {
      sessionStorage.setItem("selectedteam", JSON.stringify(this.selectedteam))
      this.tournamentService.joinTournamentSubject.next({ ...this.joinTournament, "selectedteam": this.selectedteam })
      this.route.navigateByUrl("join-tournament/teammembers")
    } else {
      this.toastService.showError("Please select the team.")
    }
  }


  selectedteamfunction(gameIndex: number): void {
    this.teams.forEach((game, idx) => { game.isSelected = gameIndex === idx; });
  }


}
