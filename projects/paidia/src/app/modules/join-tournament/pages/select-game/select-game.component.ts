import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GameService, TournamentService } from 'projects/paidia/src/app/core/service';
import { Subscription } from 'rxjs';
import { AppHtmlRoutes } from '../../../../app-routing.model';
import { EsportsToastService } from 'esports';

@Component({
  selector: 'app-select-game',
  templateUrl: './select-game.component.html',
  styleUrls: ['./select-game.component.scss'],
})
export class SelectGameComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;
  tournamentDetails: Subscription;
  createTournament: any;
  games = [];
  text: string;
  selectedGame = null;
  showLoader: boolean = true;


  constructor(
    private tournamentService: TournamentService,
    private router: Router,
    private toastService: EsportsToastService,
    private gameService: GameService) { }

  ngOnInit(): void {
    this.tournamentDetails = this.tournamentService.createTournament.subscribe((data) => {
      if (data) {
        this.createTournament = data;
      }
    });
    this.getGames();
  }

  onTextChange = (data) => {
    this.text = data; this.getGames();
  }

  getGames() {
    this.showLoader = true;
    const param: any = {};
    this.text ? param.search = this.text : ''
    this.gameService.getGames(param).subscribe((res) => {
      this.games = res.data; this.showLoader = false;
    }, (err) => { this.showLoader = false; })
  }

  next() {
    if (this.selectedGame) {
      this.tournamentService.createTournamentSubject.next({ ...this.createTournament, "selectedGame": this.selectedGame })
      this.router.navigateByUrl("create-tournament/name")
    } else {
      this.toastService.showError("Please select the game.")
    }
  }

  ngOnDestroy(): void {
    if (this.tournamentDetails)
      this.tournamentDetails.unsubscribe();
  }
}
