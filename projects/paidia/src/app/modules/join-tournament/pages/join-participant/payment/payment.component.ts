import {Component,EventEmitter,Input,OnInit,Output,SimpleChanges} from '@angular/core';
import { IUser, EsportsToastService, EsportsTransactionService } from 'esports';
import { environment } from '../../../../../environments/environment';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss'],
})
export class PaymentComponent implements OnInit {
  @Output() onPaymentDone: EventEmitter<any> = new EventEmitter();
  @Input() tournament;
  selectPaymentType = '';
  transaction;
  isOtpValid = false;
  isProccesing = true;
  user: IUser;
  paymentMethods = [];
  isPaymentLoaded = false;
  currenUser: IUser;
  payfort = 'payfort';

  constructor(
    private transactionService: EsportsTransactionService,
    private toastService: EsportsToastService,
  ) {}

  ngOnInit() {}

  fetchTransaction = async () => {
    try {
      this.isPaymentLoaded = true;
      const payload = {
        tournamentId: this.tournament?._id,
        type: 'participant',
        participantRegistration: true,
      };

      const { data } = await this.transactionService.createOrUpdateTransaction(
        environment.apiEndPoint,
        payload
      );

      this.transaction = data;
      if (this.transaction.provider == 'paypal') {
        this.paymentMethods = [
          {
            img: '../assets/images/payment/paypal.png',
            name: 'Paypal',
            value: 'paypal',
          },
        ];
      } else if (this.transaction.provider == 'stcpay') {
        this.paymentMethods = [
          {
            img: '../assets/images/payment/stc_play.png',
            name: 'stc pay',
            value: 'stcpay',
          },
        ];
      }
      this.isProccesing = false;
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
      this.isProccesing = false;
    }
  };

  getPaymentResponse = async (order) => {
    try {
      this.isProccesing = true;
      const payload = {
        tournamentId: this.tournament?._id,
        transactionId: this.transaction?._id,
        txnId: order?.id,
        provider: 'paypal',
        participantRegistration: true,
      };

      const response = await this.transactionService.createOrUpdateTransaction(
        environment.apiEndPoint,
        payload
      );
      this.toastService.showSuccess(response?.message);
      this.onPaymentDone.emit({ error: null, response });

      this.isProccesing = false;
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
      this.isProccesing = false;
      this.onPaymentDone.emit({ error, response: null });
    }
  };

  getStcPaymentResponse = async (order) => {
    try {
      const payload = {
        tournamentId: this.tournament?._id,
        type: 'stcpay',
        mobileNo: order.e164Number,
        participantRegistration: true,
      };

      const response = await this.transactionService.createOrUpdateTransaction(
        environment.apiEndPoint,
        payload
      );
      this.transaction = response;
      this.toastService.showSuccess(response?.message);
    } catch (error) {
      this.toastService.showError(
        error?.error?.data?.Text || error?.error?.message || error?.message
      );
    }
  };

  getStcOtpVerify = async (params) => {
    try {
      const payload = {
        OtpReference:
          this.transaction['data']['DirectPaymentAuthorizeV4ResponseMessage'][
            'OtpReference'
          ],
        STCPayPmtReference:
          this.transaction['data']['DirectPaymentAuthorizeV4ResponseMessage'][
            'STCPayPmtReference'
          ],
        otp: params,
        id: this.transaction.data.id,
        tournamentId: this.tournament?._id,
        participantRegistration: true,
      };
      const response = await this.transactionService.verifyStcTransaction(
        environment.apiEndPoint,
        payload
      );
      this.toastService.showSuccess(response?.message);
      this.onPaymentDone.emit({ error: null, response });
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
      this.onPaymentDone.emit({ error, response: null });
    }
  };

  ngOnChanges(simpleChanges: SimpleChanges) {
    if (simpleChanges.hasOwnProperty('tournament') && this.tournament) {
      if (!this.isPaymentLoaded) {
        this.fetchTransaction();
      }
    }
  }
}
