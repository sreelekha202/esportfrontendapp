import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TournamentService } from 'projects/paidia/src/app/core/service';
import { Subscription } from 'rxjs';
import { AppHtmlRoutes } from '../../../../app-routing.model';
import { EsportsToastService } from 'esports';

@Component({
  selector: 'app-select-platform',
  templateUrl: './select-platform.component.html',
  styleUrls: ['./select-platform.component.scss'],
})
export class SelectPlatformComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;
  selectedPlatformIndex = null;
  tournamentDetails: Subscription;
  platforms = [];
  createTournament: any;

  constructor(
    private tournamentService: TournamentService,
    private toastService: EsportsToastService,
    private router: Router) { }

  ngOnInit(): void {
    this.tournamentDetails = this.tournamentService.createTournament.subscribe((data) => {
      if (data) {
        this.createTournament = data;

        if (this.createTournament.selectedGame && this.createTournament.selectedGame.platform && this.createTournament.selectedGame.platform.length > 0)
          this.createTournament.selectedGame.platform.map((obj) => {
            if (obj.name == 'pc') {
              this.platforms.push({
                ...obj,
                type: 'pc',
                icon: 'assets/icons/matchmaking/platforms/pc.svg',
                title: 'MATCHMAKING.SELECT_PLATFORM.PLATFORMS.PC',
              })
            }
            else if (obj.name == 'Mobile') {
              this.platforms.push({
                ...obj,
                type: 'mobile',
                icon: 'assets/icons/matchmaking/platforms/mobile.svg',
                title: 'MATCHMAKING.SELECT_PLATFORM.PLATFORMS.MOBILE',
              })
            }
            else if (obj.name == 'Other') {
              this.platforms.push({
                ...obj,
                type: 'console',
                icon: 'assets/icons/matchmaking/platforms/console.svg',
                title: 'MATCHMAKING.SELECT_PLATFORM.PLATFORMS.CONSOLE',
              })
            }
            else if (obj.name == 'Xbox Series X') {
              this.platforms.push({
                ...obj,
                type: 'Xbox Series X',
                icon: 'assets/icons/matchmaking/platforms/series.svg',
                title: 'MATCHMAKING.SELECT_PLATFORM.PLATFORMS.PS5',
              })
            }
            else if (obj.name == 'Nintendo Switch') {
              this.platforms.push({
                ...obj,
                type: 'Nintendo Switch',
                icon: 'assets/icons/matchmaking/platforms/vector.svg',
                title: 'MATCHMAKING.SELECT_PLATFORM.PLATFORMS.PS5',
              })
            }
            else if (obj.name == 'Nintendo Wii') {
              this.platforms.push({
                ...obj,
                type: 'Nintendo Wii',
                icon: 'assets/icons/matchmaking/platforms/nintendo.svg',
                title: 'MATCHMAKING.SELECT_PLATFORM.PLATFORMS.PS5',
              })
            }
            else {
              this.platforms.push({
                ...obj,
                type: 'pc',
                icon: 'assets/icons/matchmaking/platforms/pc.svg',
                title: 'MATCHMAKING.SELECT_PLATFORM.PLATFORMS.PC',
              })
            }
          })
      }
    });
  }
  next = () => {
    if (this.selectedPlatformIndex != null && this.selectedPlatformIndex != undefined && this.createTournament.selectedGame.platform[this.selectedPlatformIndex]) {
      this.tournamentService.createTournamentSubject.next({ ...this.createTournament, "selected_platform": this.createTournament.selectedGame.platform[this.selectedPlatformIndex] })
      this.router.navigateByUrl("create-tournament/date");
    } else {
      this.toastService.showError("Please select the platform.")
    }
  }

  ngOnDestroy(): void {
    if (this.tournamentDetails)
      this.tournamentDetails.unsubscribe();
  }
}



