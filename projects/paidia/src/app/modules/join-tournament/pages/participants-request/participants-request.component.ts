import { Component, OnInit, Input } from '@angular/core';
import { ProfileService } from './../../../../core/service/profile.service';
@Component({
  selector: 'app-participants-request',
  templateUrl: './participants-request.component.html',
  styleUrls: ['./participants-request.component.scss']
})
export class ParticipantsRequestComponent implements OnInit {
  @Input() isManage: boolean = false;
  @Input() data;


  active = 1;
  nextId: number = 1;
  teamId : number = 1;
  myTeams = [];
  teamRequests = [];
  showLoader: boolean = true;
  constructor(private profileService: ProfileService) { }

  ngOnInit(): void {
    this.getMyTeams();
  }
  navChanges = (e) => {
    this.nextId = e.nextId;
    switch (this.nextId) {
      case 1:
        this.getMyTeams();
        break;
      case 2:
        this.getMyInvite();
        break;
    }
  }
  getMyInvite() {
    this.showLoader = true;
    const params: any = {};
    this.profileService.getMyInvites(params).subscribe((res) => {
      this.showLoader = false;
      this.teamRequests = res.data;
    }, (err) => { this.showLoader = false; })
  }
  getMyTeams() {
    this.showLoader = true;
    const params: any = {};
    this.profileService.getMyTeams(params).subscribe((res) => {
    this.myTeams = res.data;
      this.showLoader = false;
    }, (err) => { this.showLoader = false; })
  }
  deleteTeams(teamId: number) {
    this.profileService.deleteTeam(teamId)
      .subscribe((data: void) => {
        let index: number = this.myTeams.findIndex(teamId => teamId.teamId === teamId);
        this.myTeams.splice(index, 1);
      }
      )


  }


}
''
