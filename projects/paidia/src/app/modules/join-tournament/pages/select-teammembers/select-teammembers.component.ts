import { Component, OnInit } from '@angular/core';
import {  AppHtmlJoinTournamentRoutes } from '../../../../app-routing.model';
import { AppHtmlRoutes } from '../../../../app-routing.model';
import { Router } from '@angular/router';
import { TournamentService } from './../../../../core/service';
import { EsportsToastService } from 'esports';
import { ProfileService } from './../../../../core/service/profile.service';


@Component({
  selector: 'app-select-teammembers',
  templateUrl: './select-teammembers.component.html',
  styleUrls: ['./select-teammembers.component.scss']
})
export class SelectTeammembersComponent implements OnInit {

  AppHtmlJoinTournamentRoutes = AppHtmlJoinTournamentRoutes;
  AppHtmlRoutes = AppHtmlRoutes;
  selectedplayer = null;
  joinTournament: any;
  paginationData = {
    page: 1,
    limit: 100,
    sort: { _id: -1 },
    text: '',
  };
  showLoader = false;
  teams = []

  players = [];
  text: string = '';
  selctedgame: any;


  constructor(private route:Router,
    private tournamentService:TournamentService,
    private toastService: EsportsToastService,private profileService: ProfileService) { }

  ngOnInit(): void {
     this.showLoader = true;
    this.selctedgame = sessionStorage.getItem("selectedteam")
    this.selctedgame = JSON.parse(this.selctedgame);
    this.getplayers(this.selctedgame._id)
    // this.players = [
    //   {
    //     image:
    //       'https://www.advertisingweek360.com/wp-content/uploads/2018/10/169girlgamers-1170x600.jpg',
    //     nickname: 'killmonger',
    //     isSelected: false,
    //   },
    //   {
    //     image: 'https://cdn6.dissolve.com/p/D538_291_180/D538_291_180_1200.jpg',
    //     nickname: 'xxinfinity01',
    //     isSelected: true,
    //   },
    //   {
    //     image:
    //       'https://media.suara.com/pictures/970x544/2020/05/10/60545-main-game.jpg',
    //     nickname: 'supergirl004',
    //     isSelected: false,
    //   },
    //   {
    //     image:
    //       'https://upload.wikimedia.org/wikipedia/commons/6/6a/GORDOx-BGS-2018.jpg',
    //     nickname: 'tinythor',
    //     isSelected: true,
    //   },
    // ];
    this.text="";
  }

  getplayers(id){
   this.profileService.getplayers(id).subscribe((data:any)=>{
     this.players = []
     for(let d of data.data.members){
       let d1 = {
         details:d.userId,
        image:d.userId.profilePicture,
      nickname: d.userId.fullName,
      isSelected: false,
      userId:d.userId._id,
      _id:d._id
       }
       this.players.push(d1)
     }
     this.showLoader = false;
   })
  }

  addPlayer(index: number): void {
    this.players.forEach((_, i) => {
      if (index === i) {
        this.players[i].isSelected = !this.players[i].isSelected;
      }
    });
  }
  filteredPlayer() {
    return this.players.filter((obj) => obj.nickname.includes(this.text.toLowerCase().trim()));
  }

  confirm() {
    let s_players = [];
    for(let d of this.players){
      if(d.isSelected == true){
          s_players.push(d)
      }
    }
    if (s_players.length != 0) {
      sessionStorage.setItem("selectedusers",JSON.stringify(s_players))
      this.tournamentService.joinTournamentSubject.next({ ...this.joinTournament, "selectedplayer": this.selectedplayer })
      this.route.navigateByUrl("join-tournament/teamdetails")
    } else {
      this.toastService.showError("Please select the player.")
    }
  }

  getMyTeams() {
    const pagination = JSON.stringify(this.paginationData);
    this.profileService.getMyTeam( {
      pagination: pagination,
    }).subscribe((res)=>{
      this.teams = []
      for(let d of res.data.docs){
          let d1 = {
            image:
            d.logo,
          name: d.teamName,
          teamsLength: 5,
          createtdAt: 1,
          }
          this.teams.push(d1)
      }
      this.showLoader = false;
    },(err) => { this.showLoader = false;
    })
  }

}
