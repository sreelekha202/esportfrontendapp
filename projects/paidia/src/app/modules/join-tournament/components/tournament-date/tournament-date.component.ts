import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import moment from 'moment';
@Component({
  selector: 'app-tournament-date',
  templateUrl: './tournament-date.component.html',
  styleUrls: ['./tournament-date.component.scss'],
})
export class TournamentDateComponent implements OnInit {
  @Input() date: any = new Date();
  @Output() dateChange = new EventEmitter();
  constructor() { }
  ngOnInit(): void { }
  onDateChange = () => { this.dateChange.emit(this.date) }
  minDate = new Date();
}
 