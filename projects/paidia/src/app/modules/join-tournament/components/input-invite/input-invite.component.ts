import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-input-invite',
  templateUrl: './input-invite.component.html',
  styleUrls: ['./input-invite.component.scss'],
})
export class InputInviteComponent implements OnInit {
  @Output() emitValue = new EventEmitter();

  timeoutId = null;

  constructor() {}

  ngOnInit(): void {}

  onChangeValue(event): void {
    const valid = event?.target?.validity?.valid;
    const value = event?.target?.value;

    const emitValue = () => {
      if (!value || !valid) return;
      this.emitValue.emit(event?.target?.value.trim());
      event.target.value = '';
    };

    clearTimeout(this.timeoutId);

    this.timeoutId = setTimeout(emitValue, 1000);
  }
}
