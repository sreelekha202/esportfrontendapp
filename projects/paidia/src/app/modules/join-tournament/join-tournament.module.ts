import { LoadingModule } from '../../core/loading/loading.module';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatExpansionModule } from '@angular/material/expansion';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { FormComponentModule } from '../../shared/components/form-component/form-component.module';
import { HeaderPaidiaModule } from '../../shared/components/header-paidia/header-paidia.module';
import { JoinTournamentComponent } from './join-tournament.component';
import { JoinTournamentRoutingModule } from './join-tournament-routing.module';
import { SelectDateComponent } from './pages/select-date/select-date.component';
import { SelectGameComponent } from './pages/select-game/select-game.component';
import { SelectPlatformComponent } from './pages/select-platform/select-platform.component';
import { SelectTypeComponent } from './pages/select-type/select-type.component';
import { TournamentDetailsComponent } from './pages/tournament-details/tournament-details.component';
import { TournamentNameComponent } from './pages/tournament-name/tournament-name.component';
import { InputDateComponent } from './components/input-date/input-date.component';
import { InputInviteComponent } from './components/input-invite/input-invite.component';
import { InputSearchComponent } from './components/input-search/input-search.component';
import { InputTimeComponent } from './components/input-time/input-time.component';
import { PopupCreatedComponent } from './components/popup-created/popup-created.component';
import { TournamentDateComponent } from './components/tournament-date/tournament-date.component';
import { TournamentImageComponent } from './components/tournament-image/tournament-image.component';
import { TournamentNameInputComponent } from './components/tournament-name-input/tournament-name-input.component';
import { TournamentTimeComponent } from './components/tournament-time/tournament-time.component';
import { RouterBackModule } from '../../shared/directives/router-back.module';
import { RegistrationComponent } from './pages/registration/registration.component';
import { SelectTeammembersComponent } from './pages/select-teammembers/select-teammembers.component';
import { SelectTeamdetailsComponent } from './pages/select-teamdetails/select-teamdetails.component';
import { ParticipantsRequestComponent } from './pages/participants-request/participants-request.component';
import { SoloRegistrationComponent } from './pages/solo-registration/solo-registration.component';
import { SharedModule } from '../../shared/modules/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CoreModule } from '../../core/core.module';
import { JoinSeasonParticipantComponent } from './pages/join-participant/join-participant.component';
import { WYSIWYGEditorModule } from 'esports';
const components = [
  JoinTournamentComponent,
  InputDateComponent,
  InputInviteComponent,
  InputSearchComponent,
  InputTimeComponent,
  PopupCreatedComponent,
  SelectDateComponent,
  SelectGameComponent,
  SelectPlatformComponent,
  SelectTypeComponent,
  TournamentDateComponent,
  TournamentDetailsComponent,
  TournamentImageComponent,
  TournamentNameComponent,
  TournamentNameInputComponent,
  TournamentTimeComponent,
  RegistrationComponent,
  SelectTeammembersComponent,
  SelectTeamdetailsComponent,
  ParticipantsRequestComponent,
  SoloRegistrationComponent,
  JoinSeasonParticipantComponent
];

const modules = [
  CommonModule,
  JoinTournamentRoutingModule,
  FormComponentModule,
  HeaderPaidiaModule,
  MatExpansionModule,
  NgxMaterialTimepickerModule,
  SharedModule,
  RouterBackModule,
  FormsModule,
  ReactiveFormsModule,
  CoreModule,
  WYSIWYGEditorModule
];

@NgModule({
  declarations: components,
  imports: modules,
})
export class JoinTournamentModule {}
