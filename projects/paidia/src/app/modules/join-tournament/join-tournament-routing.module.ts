import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { JoinTournamentComponent } from './join-tournament.component';
import { SelectTypeComponent } from './pages/select-type/select-type.component';
import { TournamentNameComponent } from './pages/tournament-name/tournament-name.component';
import { SelectGameComponent } from './pages/select-game/select-game.component';
import { SelectPlatformComponent } from './pages/select-platform/select-platform.component';
import { SelectDateComponent } from './pages/select-date/select-date.component';
import { TournamentDetailsComponent } from './pages/tournament-details/tournament-details.component';
import { RegistrationComponent } from './pages/registration/registration.component';
import { SelectTeammembersComponent } from './pages/select-teammembers/select-teammembers.component';
// import { SelectTeamdetailsComponent } from './pages/select-teamdetails/select-teamdetails.component';
import { ParticipantsRequestComponent } from './pages/participants-request/participants-request.component';
import { SoloRegistrationComponent } from './pages/solo-registration/solo-registration.component';
import { JoinSeasonParticipantComponent } from './pages/join-participant/join-participant.component';


const routes: Routes = [
  {
    path: '',
    component: JoinTournamentComponent,
    children: [
      {
        path: '',
        component: RegistrationComponent,
      },
      {
        path: 'registration',
        component: RegistrationComponent,
      },
      {
        path: 'teammembers',
        component: SelectTeammembersComponent,
      },
      {
        path: 'teamdetails',
        component: JoinSeasonParticipantComponent,
      },
      {
        path: 'participantsrequest',
        component: ParticipantsRequestComponent,
      },
      {
        path: 'soloregistration',
        component: SoloRegistrationComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class JoinTournamentRoutingModule { }
