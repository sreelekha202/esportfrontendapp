import { Component, OnInit } from '@angular/core';
import { AppHtmlRoutes } from '../../app-routing.model';
import { TournamentService } from '../../core/service/tournament.service';
@Component({
  selector: 'app-join-tournament',
  templateUrl: './join-tournament.component.html',
  styleUrls: ['./join-tournament.component.scss'],
})
export class JoinTournamentComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;
  tournamentDetails: any;
  constructor(private tournamentService: TournamentService) { }
  ngOnInit(): void {
    this.tournamentDetails = localStorage.getItem("tournamentDetails")
    this.tournamentDetails = JSON.parse(this.tournamentDetails)
  }
}
