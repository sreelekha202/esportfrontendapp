import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LeaderboardComponent } from './leaderboard.component';
import { LeaderboardPlayerComponent } from './leaderboard-player/leaderboard-player.component';

const routes: Routes = [
  {
    path: '',
    component: LeaderboardComponent,
  },
  {
    path: ':userId',
    component: LeaderboardPlayerComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LeaderboardRoutingModule {}
