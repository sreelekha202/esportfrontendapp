import { Component, OnInit, Input, Inject, PLATFORM_ID } from '@angular/core';
import { AppHtmlRoutes } from '../../../app-routing.model';
import {
  EsportsLeaderboardService,
  EsportsUserService,
  EsportsToastService,
  EsportsChatService,
  IUser,
  EsportsGtmService,
  SuperProperties,
  EventProperties
} from 'esports';
import { MatDialog } from '@angular/material/dialog';
import { PopupCreatedComponent } from '../popup-created/popup-created.component';
import { environment } from '../../../../environments/environment';
import { Subscription } from 'rxjs';
import { isPlatformBrowser } from '@angular/common';
const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;

@Component({
  selector: 'app-leaderboard-user-card',
  templateUrl: './leaderboard-user-card.component.html',
  styleUrls: ['./leaderboard-user-card.component.scss'],
})
export class LeaderboardUserCardComponent implements OnInit {
  @Input() data: any;
  userSubscription: Subscription;
  AppHtmlRoutes = AppHtmlRoutes;
  currenUser: IUser;
  currentUserId: String = '';
  windowposition: String = 'chat_window chat_window_right_drawer';
  showChat: boolean = false;
  matchdetails: any;
  typeofchat = 'user';
  isBrowser: boolean;
  followStatus: any;
  constructor(
    @Inject(PLATFORM_ID) private platformId,
    public matDialog: MatDialog,
    private leaderboardService: EsportsLeaderboardService,
    private toastService: EsportsToastService,
    private userService: EsportsUserService,
    public chatService: EsportsChatService,
    private gtmService: EsportsGtmService
  ) {
    this.isBrowser = isPlatformBrowser(platformId);
  }

  ngOnInit(): void {
    this.checkFollowStatus();
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.currenUser = data;
        this.currentUserId = this.currenUser._id;
      }
    });
  }

  commingSoon(): void {
    this.matDialog.open(PopupCreatedComponent);
  }

  pushGTMTags(eventName: string, eventProps = false) {
    let superProperties: SuperProperties = {};
    if (this.currenUser) {
      superProperties = this.gtmService.assignLoggedInUsedData(this.currenUser);
    }

    let eventProperties: EventProperties = {};
    if (eventProps) {
      eventProperties.fromPage = 'Chats';
    }

    this.gtmService.gtmEventWithSuperProp({
      eventName: eventName,
      superProps: superProperties,
      eventProps: eventProperties,
    });
  }

  public toggleChat(chatwindow) {
    this.pushGTMTags('Chat_Opened');
    this.matchdetails = chatwindow?.id + '-' + this.currentUserId;
    // this.showChat = true;
    this.chatService.setWindowPos(this.windowposition);
    this.chatService.setCurrentMatch(this.matchdetails);
    this.chatService.setTypeOfChat(this.typeofchat);
    this.chatService.setChatStatus(true);
    this.chatService.setTextButtonStatus(true);
  }

  followUser(id) {
    if (this.data) {
      if (this.followStatus == 'follow') {
        this.leaderboardService.followUser(API, this.data?.id).subscribe(
          (res: any) => {
            this.followStatus = 'unfollow';
            this.checkFollowStatus();
            this.toastService.showSuccess(res?.message);
            this.userService.refreshCurrentUser(API, TOKEN);
          },
          (err) => {
            // this.showLoader = false;
          }
        );
      } else if (this.followStatus == 'unfollow') {
        this.leaderboardService.unfollowUser(API, this.data?.id).subscribe(
          (res: any) => {
            this.followStatus = 'follow';
            this.checkFollowStatus();
            this.toastService.showSuccess(res?.message);
            this.userService.refreshCurrentUser(API, TOKEN);
          },
          (res: any) => {
            // this.showLoader = false;
          }
        );
      }
    } else {
      // Navigate to the login page with extras
      //this.router.navigate(['/user/email-login']);
    }
  }

  checkFollowStatus() {
    this.leaderboardService.checkFollowStatus(API, this.data?.id).subscribe(
      (res: any) => {
        if (res?.data[0]?.status == 1) {
          this.followStatus = 'unfollow';
        } else if (res?.data[0]?.status == 0) {
          this.followStatus = 'follow';
        }
      },
      (err) => {
        // this.showLoader = false;
      }
    );
  }
}
