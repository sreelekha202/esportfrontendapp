import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-leaderboard-trophies-card',
  templateUrl: './leaderboard-trophies-card.component.html',
  styleUrls: ['./leaderboard-trophies-card.component.scss'],
})
export class LeaderboardTrophiesCardComponent implements OnInit {
  @Input() tournaments: any = null;
  @Input() type: string = null;
  @Input() row;

  constructor() {}

  ngOnInit(): void {
    if(this.type == "gold"){
      this.tournaments = this.row.firstPositionTournaments
    }
    if(this.type == "silver"){
      this.tournaments = this.row.secondPositionTournaments
    }
    if(this.type == "bronze"){
      this.tournaments = this.row.thirdPositionTournaments
    }
 // MOCK TOURNAMENTS
    // this.tournaments = [
    //   {
    //     image:
    //       'https://i114.fastpic.ru/big/2021/0614/3a/bfcffec3a224fc0df048206aab080e3a.jpg',
    //     title: 'Fortnite first strike tournament',
    //     game: 'Fortnite',
    //   },
    //   {
    //     image:
    //       'https://i114.fastpic.ru/big/2021/0614/db/7c78c50e2d8c6ccf1f4b2fe797166fdb.jpg',
    //     title: '2021 Fornite Summer Championships!',
    //     game: 'Fortnite',
    //   },
    //   {
    //     image:
    //       'https://i114.fastpic.ru/big/2021/0614/cd/23524330f59910c45369e7e1fa3b98cd.jpg',
    //     title: 'Fornite All-Stars July 2021!',
    //     game: 'Fortnite',
    //   },
    //   {
    //     image:
    //       'https://i114.fastpic.ru/big/2021/0614/3a/a43e578ea5960a5e4abe61e2b3dff23a.jpg',
    //     title: 'TRU FRENZ Fortnite tournament',
    //     game: 'Fortnite',
    //   },
    //   {
    //     image:
    //       'https://i114.fastpic.ru/big/2021/0614/e7/9464ea9ab556c601bc8d096540200de7.jpg',
    //     title: 'Fortnite first strike tournamentку',
    //     game: 'Fortnite',
    //   },
    // ];
  }

  getTrophyIcon(type: string): string {
    return `assets/icons/trophies/${type}.svg`;
  }

  getPlace(type: string): string {
    switch(type) {
      case "gold": return '1st'
      case "silver": return '2nd'
      case "bronze": return '3rd'
    }
  }
}
