import { Component, Input, OnInit, ChangeDetectorRef } from '@angular/core';
import { HomeService } from '../../core/service';
import { EsportsGameService } from 'esports';
import { environment } from '../../../environments/environment';
import { AppHtmlRoutes } from '../../app-routing.model';
import { IPagination } from 'esports';
import { LeaderBoard } from './leaderboard-table/leaderboard-table.component';
import { EsportsUserService, EsportsLeaderboardService } from 'esports';
const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;
@Component({
  selector: 'app-leaderboard',
  templateUrl: './leaderboard.component.html',
  styleUrls: ['./leaderboard.component.scss'],
})
export class LeaderboardComponent implements OnInit {
  @Input() data: LeaderBoard = {
    page: 'leaderboard',
    title: 'All Games',
  };

  AppHtmlRoutes = AppHtmlRoutes;
  active = 1;
  showLoader: boolean = true;
  gameId = ' ';
  selectedCountryName = '';
  selectedGameName = '';
  selectedGames = 'all';
  countryList = [];
  gameList = [];
  leaderboardData: any = [];
  selectedCountry: any = { all: true };
  selectedGame: any = { all: true };
  page: IPagination;
  paginationData = {
    page: 1,
    limit: 10,
    sort: 'startDate',
  };
  paginationDetails: any;
  pageSizeOptions = environment.pageSizeOptions;
  teamFlag:boolean = false;
  constructor(
    private leaderboardService: EsportsLeaderboardService,
    private gameService: EsportsGameService,
    private userService: EsportsUserService,
    private homeService: HomeService,
    private changeDetection: ChangeDetectorRef
  ) { }

  ngOnInit(): void {
    this.getGames();
    this.getCountries();
    this.filterLeaderboard();
  }

  getGames() {
    this.gameService.getAllGames(API, '').subscribe(
      (res) => (this.gameList = res?.data),
      (err) => { }
    );
  }

  getCountries() {
    this.userService.getAllCountries().subscribe((data) => {
      this.countryList = data?.countries;
    });
  }

  changeTab(value, type) {
    switch (type) {
      case 'regions': {
        if (value != 'all') {
          const country = this.countryList.find((o) => o?.id === value);
          this.selectedCountryName = country?.name;
          this.filterLeaderboard();
        } else {
          this.selectedCountryName = '';
          this.filterLeaderboard();
        }
        break;
      }
      case 'games': {
        if (value != 'all') {
          const game = this.gameList.find((o) => o?._id === value);
          this.selectedGameName = game?.name;
          this.selectedGames = game?._id;
          this.filterLeaderboard();
        } else {
          this.selectedGameName = value;
          this.selectedGames = 'all';
          this.filterLeaderboard();
        }
        break;
      }
    }
  }

  filterLeaderboard(page = 1) {
    this.selectedGame = {};
    this.selectedGame[this.selectedGames] = true;
    this.showLoader = true;
    const params = {
      country: this.selectedCountryName,
      gameId: this.selectedGames,
      limit: this.paginationData?.limit,
      page: 1,
      state: null,
    };

    if (this.data?.page === 'home') {
      this.homeService._leaderBoard().subscribe((data) => {
        this.showLoader = false;
        this.leaderboardData = data?.data?.leaderboardData;
        if (this.leaderboardData) {
          this.leaderboardData.forEach((obj, index) => {
            obj.rank = index + 1;
          });
        }
      });
    } else {
      this.leaderboardService.getGameLeaderboard(API, params).subscribe(
        (res: any) => {
          this.showLoader = false;
          let leaderboardData = [];
          this.paginationDetails = res?.data;
          this.paginationData.limit = this.paginationDetails?.limit;
          this.paginationData.page = this.paginationDetails?.page;
          this.paginationDetails.page -= 1;
          if (res?.data?.docs) {
            let count =
              (+this.paginationData.page - 1) * +this.paginationData.limit;
            for (const data of res?.data?.docs) {
              if (data?.user.length > 0) {
                let userData: any = {};
                userData.id = data?._id?.user;
                userData.rank = count + 1;
                count++;
                userData.img =
                  data?.user[0]?.profilePicture == ''
                    ? './assets/images/Profile/article01.png'
                    : data?.user[0]?.profilePicture;
                userData.name = data?.user[0]?.username;
                userData.region = data?.user[0]?.state ? data?.user[0]?.state : '';
                userData.trophies = {};
                userData.trophies.first = data?.firstPosition;
                userData.trophies.second = data?.secondPosition;
                userData.trophies.third = data?.thirdPosition;
                userData.firstPositionTournaments = [];
                userData.firstPositionTournaments = data?.firstPositionTournaments;
                userData.secondPositionTournaments = [];
                userData.secondPositionTournaments = data?.secondPositionTournaments;
                userData.thirdPositionTournaments = [];
                userData.thirdPositionTournaments = data?.thirdPositionTournaments;
                userData.points = data?.points;
                userData.amount = data?.amountPrice;
                userData.gamesCount = data?.gamesCount;
                leaderboardData.push({ ...userData });
              }
            }
            this.leaderboardData = leaderboardData;
          }
        },
        (err) => {
          this.showLoader = false;
        }
      );
    }
  }

  filterLeaderboardTeam(page = 1) {
    this.showLoader = true;
    this.teamFlag = true;

    const params = {
      country: this.selectedCountryName,
      gameId: this.selectedGames,
      limit: this.paginationData.limit,
      page: page,
      state: null,
    };
    this.leaderboardService.getGameLeaderboard(API, params, 'team').subscribe(
      (res: any) => {
        this.showLoader = false;
        this.leaderboardData = [];

        if (res.data.docs) {
          let count = 0;
          let userData: any = {};
          let leaderboardDataVo: any = res.data.docs;
          /* sort gold by decending */
          leaderboardDataVo.sort((a, b) => {
            return b.firstPosition - a.firstPosition;
          });

          for (const data of leaderboardDataVo) {
            if(data.team && data.team.length > 0){
              userData.id = data._id.team;
              userData.rank = count + 1;
              count++;
              userData.img =
                (!data.team[0].logo
                  ? './assets/images/Profile/formation.svg'
                  : data.team[0].logo);
              userData.name = data.team[0].teamName;
              userData.region = data.team[0].country
                ? data.team[0].country
                : '';
              userData.trophies = {};
              userData.trophies.first = data.firstPosition;
              userData.trophies.second = data.secondPosition;
              userData.trophies.third = data.thirdPosition;
              userData.points = data.points;
              userData.amount = data.amountPrice;
              userData.gamesCount = data.gamesCount;
              this.leaderboardData.push({ ...userData });
              userData = {};
            } else {
              this.paginationData.limit =
                this.paginationDetails?.limit;
                this.paginationData.page = 0;
            }
          }
          this.paginationDetails = res?.data;
          this.paginationData.limit= this.paginationDetails.limit;
          this.paginationData.page = this.paginationDetails.totalDocs;
        }
      },
      (err) => {
        this.showLoader = false;
        this.leaderboardData = [];
      }
    );
  }

  filterByGame(gameId) {
    this.paginationData.page = 1;
    this.selectedGame = {};
    this.selectedGame[gameId] = true;

    if (gameId === 'all') {
      this.gameId = 'all';
    } else {
      this.gameId = gameId;
    }

    this.showLoader = true;
    const params = {
      page: this.paginationData?.page,
      limit: this.paginationData?.limit,
      gameId: this.gameId,
    };
    if (this.gameId) {
      params['game'] = this.gameId;
    }
    this.leaderboardService.getGameLeaderboard(API, params).subscribe(
      (res: any) => {
        this.showLoader = false;
        this.leaderboardData = [];
        if (res?.data?.docs) {
          let count = 0;
          let userData: any = {};

          for (const data of res?.data?.docs) {
            if (data?.user.length > 0) {
              userData.id = data?._id?.user;
              userData.rank = count + 1;
              count++;
              userData.img =
                data?.user[0]?.profilePicture == ''
                  ? './assets/images/Profile/formation.svg'
                  : data?.user[0]?.profilePicture;
              userData.name = data?.user[0]?.username;
              userData.region = data?.user[0]?.country
                ? data?.user[0]?.country
                : '';
              userData.trophies = {};
              userData.trophies.first = data?.firstPosition;
              userData.trophies.second = data?.secondPosition;
              userData.trophies.third = data?.thirdPosition;

              userData.points = data?.points;
              userData.amount = data?.amountPrice;
              userData.gamesCount = data?.gamesCount;
              this.leaderboardData.push({ ...userData });
              userData = {};
            }
          }
        }
      },
      (err) => {
        this.showLoader = false;
      }
    );
  }


  pageChanged(page): void {
    this.paginationData.page = page?.pageIndex + 1;
    this.paginationData.limit = page?.pageSize;

    if(!this.teamFlag){

    this.filterLeaderboard();
    }
  else{
    this.filterLeaderboardTeam(page);
  }


  }
}
