import { Component, OnInit, Input, AfterViewInit } from '@angular/core';
import { AppHtmlRoutes } from '../../../app-routing.model';

export interface LeaderBoard {
  page: string;
  title: string;
}

@Component({
  selector: 'app-leaderboard-table',
  templateUrl: './leaderboard-table.component.html',
  styleUrls: ['./leaderboard-table.component.scss'],
})
export class LeaderboardTableComponent implements OnInit {
  @Input() leaderboardData;
  @Input() data: LeaderBoard;
  @Input() playerTeam

  AppHtmlRoutes = AppHtmlRoutes;

  // itemsPerPage = [10, 20, 30];

  // slicesItems = [];

  constructor() { }

  ngOnInit(): void {
    // this.slicesItems = this.leaderboardData.slice(0, this.itemsPerPage[0]);
  }

  // onPageChange(event): void {
  //   const startIndex = event.pageIndex * event.pageSize;
  //   let endIndex = startIndex + event.pageSize;

  //   if (endIndex > this.leaderboardData.length) {
  //     endIndex = this.leaderboardData.length;
  //   }

  //   this.slicesItems = this.leaderboardData.slice(startIndex, endIndex);
  // }
}
