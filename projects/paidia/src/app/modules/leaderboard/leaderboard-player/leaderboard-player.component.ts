import {
  EsportsToastService,
  EsportsGtmService,
  EventProperties,
  SuperProperties,
  EsportsLeaderboardService,
  EsportsChatService,
  IParticipant,
  EsportsUserService,
  IUser
} from 'esports';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';

import { environment } from '../../../../environments/environment';
import { Subscription } from 'rxjs';
const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;
@Component({
  selector: 'app-leaderboard-player',
  templateUrl: './leaderboard-player.component.html',
  styleUrls: ['./leaderboard-player.component.scss'],
})
export class LeaderboardPlayerComponent implements OnInit {
  userId: any = null;
  showLoader: boolean = true;
  games = [];
  user: IUser;
  followStatus: any = 'follow';
  player: IParticipant;
  playerData = null;
  userSubscription: Subscription;
  currentUser: IUser;
  currentUserId: String = '';
  windowposition: String = 'chat_window chat_window_right_drawer';
  showChat: boolean = false;
  matchdetails: any;
  typeofchat = 'user';
  isBrowser: boolean;
  constructor(
    private activatedRoute: ActivatedRoute,
    private leaderboardService: EsportsLeaderboardService,
    private location: Location,
    private toastService: EsportsToastService,
    private userService: EsportsUserService,
    private router: Router,
    private gtmService: EsportsGtmService,
    public chatService: EsportsChatService
  ) {
    this.userId = this.activatedRoute.snapshot.params.userId;
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.currentUser = data;
      }
    });
  }

  ngOnInit(): void {
    this.getPlayerDetails();
    this.checkFollowStatus();

    // MOCK DATA
    this.games = [
      { name: 'valorant' },
      { name: 'dota 2' },
      { name: 'fortnine' },
    ];
  }
  checkFollowStatus() {
    this.leaderboardService.checkFollowStatus(API, this.userId).subscribe(
      (res: any) => {
        if (res && res.data && res.data[0].followerID) {
          if (res.data[0].followerID == this.currentUser._id) {
            this.followStatus = 'unfollow';
          }
        } else {
          this.followStatus = 'follow';
        }
      },
      (err) => {
        this.showLoader = false;
      }
    );
  }

  goBack = () => this.location.back();
  getPlayerDetails() {
    this.leaderboardService
      .getUserLeaderboard(API, { userId: this.userId })
      .subscribe(
        (res: any) => {
          this.playerData = res.data;
          this.showLoader = false;
        },
        (err) => {
          this.showLoader = false;
        }
      );
  }
  followUser(user) {
    this.user = user;
    if (this.user) {
      if (this.followStatus == 'follow') {
        this.leaderboardService.followUser(API, this.user).subscribe(
          (res: any) => {
            this.followStatus = 'unfollow';
            this.getPlayerDetails();
            this.checkFollowStatus();
            this.toastService.showSuccess(res.message);
            this.userService.refreshCurrentUser(API, TOKEN);
          },
          (err) => {
            this.showLoader = false;
          }
        );
      } else if (this.followStatus == 'unfollow') {
        this.leaderboardService.unfollowUser(API, this.user).subscribe(
          (res: any) => {
            this.followStatus = 'follow';
            this.getPlayerDetails();
            this.checkFollowStatus();
            this.toastService.showSuccess(res.message);
            this.userService.refreshCurrentUser(API, TOKEN);
          },
          (res: any) => {
            this.showLoader = false;
          }
        );
      }
    } else {
      // Navigate to the login page with extras
      // this.router.navigate(['/user/email-login']);
    }
  }

  // followUser() {
  //   this.leaderboardService.followUser(API,this.user).subscribe((res) => {
  //     if (res.success) {
  //       this.toastService.showSuccess(res.message)
  //       this.userService.refreshCurrentUser(API, TOKEN);
  //     }
  //   })
  // }

  public toggleChat(chatwindow_id) {
    this.pushGTMTags('Chat_Opened');
    this.matchdetails = chatwindow_id + '-' + this.currentUserId;
    // this.showChat = true;
    this.chatService.setWindowPos(this.windowposition);
    this.chatService.setCurrentMatch(this.matchdetails);
    this.chatService.setTypeOfChat(this.typeofchat);
    this.chatService.setChatStatus(true);
    this.chatService.setTextButtonStatus(true);
  }

  pushGTMTags(eventName: string, eventProps = false) {
    let superProperties: SuperProperties = {};
    if (this.currentUser) {
      superProperties = this.gtmService.assignLoggedInUsedData(
        this.currentUser
      );
    }

    let eventProperties: EventProperties = {};
    if (eventProps) {
      eventProperties.fromPage = 'Chats';
    }

    this.gtmService.gtmEventWithSuperProp({
      eventName: eventName,
      superProps: superProperties,
      eventProps: eventProperties,
    });
  }
}
