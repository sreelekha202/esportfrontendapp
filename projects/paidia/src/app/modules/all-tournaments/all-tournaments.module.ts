import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../shared/modules/shared.module';
import { CoreModule } from '../../core/core.module';
import { RouterBackModule } from '../../shared/directives/router-back.module';
import { AllTournamentsComponent } from './all-tournaments.component';

export const routes: Routes = [
  {
    path: '',
    component: AllTournamentsComponent,
  },
];

@NgModule({
  declarations: [AllTournamentsComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule,
    CoreModule,
    RouterBackModule
  ],
})
export class AllTournamentsModule { }
