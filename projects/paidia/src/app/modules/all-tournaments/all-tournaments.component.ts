import { Component, OnInit } from '@angular/core';
import { HomeService } from '../../core/service';
import {
  EsportsGameService,
  EsportsGtmService,
  EventProperties,
} from 'esports';
import { Router } from '@angular/router';
import { environment } from '../../../environments/environment';
const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;
@Component({
  selector: 'app-all-tournaments',
  templateUrl: './all-tournaments.component.html',
  styleUrls: ['./all-tournaments.component.scss'],
})
export class AllTournamentsComponent implements OnInit {
  showLoader: boolean = false;
  text = '';
  selected_platform: any;
  params: any = {};
  selected_sortby_game: any;
  sortby = [
    { value: '0', viewValue: 'Upcoming tournaments' },
    { value: '1', viewValue: 'Ongoing tournaments' },
    { value: '2', viewValue: 'Completed tournaments' },
    { value: '3', viewValue: 'All tournaments' },
  ];
  selected_sortby_tournament = this.sortby[3].value;
  individualTournaments = [];
  prizeMoneyTournaments = [];
  teamTournaments = [];
  TournamentsList = [];
  mobFiltersMenuOpened: boolean = false;
  platformList: any = [];
  games = [];
  gamesShort = [];
  isTournamentFlag = false;
  paginationData = {
    page: 1,
    limit: 8,
    sort: 'startDate',
  };
  paginationDetails: any;
  pageSizeOptions = environment.pageSizeOptions;

  constructor(
    private homeService: HomeService,
    private gameService: EsportsGameService,
    private gtmService: EsportsGtmService,
    private router: Router,
    private esportsGameService: EsportsGameService
  ) {}

  ngOnInit(): void {
    this.getAllTournaments();
    this.getGames();
    this.getPlatformList();
  }

  getGames() {
    this.showLoader = true;
    let a = [];
    let b = [];
    this.gameService.getGames(API).subscribe(
      (res) => {
        this.games = res?.data;
        this.showLoader = false;
        for (let i = 0; i < this.games.length; i++) {
          if (this.games[i]?.name == 'The Sims 4') {
            a[0] = this.games[i];
          }

          if (this.games[i]?.name == 'Valorant') {
            a[1] = this.games[i];
          }

          if (this.games[i]?.name == 'League of Legends') {
            a[2] = this.games[i];
          }

          if (this.games[i]?.name == 'Fortnite') {
            a[3] = this.games[i];
          }

          if (this.games[i]?.name == 'Minecraft') {
            a[4] = this.games[i];
          }

          if (this.games[i]?.name == 'Pokémon TCG Online') {
            a[5] = this.games[i];
          }

          if (this.games[i]?.name == 'Apex Legends') {
            a[6] = this.games[i];
          }

          if (this.games[i]?.name == 'Call of Duty Mobile') {
            a[7] = this.games[i];
          }

          if (this.games[i]?.name == 'World of Warcraft') {
            a[8] = this.games[i];
          }

          if (this.games[i]?.name == 'Halo MCC') {
            a[9] = this.games[i];
          }

          if (this.games[i]?.name == 'Rocket League') {
            a[10] = this.games[i];
          }

          if (this.games[i]?.name == 'Overwatch') {
            a[11] = this.games[i];
          }

          if (
            this.games[i]?.name != 'Overwatch' &&
            this.games[i]?.name != 'Rocket League' &&
            this.games[i]?.name != 'Halo MCC' &&
            this.games[i]?.name != 'World of Warcraft' &&
            this.games[i]?.name != 'Call of Duty Mobile' &&
            this.games[i]?.name != 'Apex Legends' &&
            this.games[i]?.name != 'Pokémon TCG Online' &&
            this.games[i]?.name != 'Minecraft' &&
            this.games[i]?.name != 'Fortnite' &&
            this.games[i]?.name != 'League of Legends' &&
            this.games[i]?.name != 'Valorant' &&
            this.games[i]?.name != 'The Sims 4'
          ) {
            b[i] = this.games[i];
          }
        }
        this.gamesShort = a;
        //  var newArray = b.filter(value => Object.keys(value).length !== 0)
        //  this.gamesShort = [ ...this.gamesShort, ...newArray]
      },
      (err) => {
        this.showLoader = false;
      }
    );
  }

  getPlatformList() {
    this.esportsGameService.fetchPlatforms(API).subscribe((res) => {
      this.platformList = res?.data;
    });
  }

  onSearch = (event: any) => {
    this.text = String(event.target.value);
    this.getAllTournaments();
  };
  getAllTournaments(gtmEvent = false) {
    if (this.selected_platform != '619f989c1c24ed06740ff936') {
      this.selected_platform
        ? (this.params.platform = this.selected_platform)
        : '';
      this.selected_sortby_game
        ? (this.params.game = this.selected_sortby_game)
        : '';
      this.selected_sortby_tournament
        ? (this.params.status = this.selected_sortby_tournament)
        : '';
      this.text ? (this.params.text = this.text) : '';
    } else {
      delete this.params['platform'];
      this.selected_sortby_game
        ? (this.params.game = this.selected_sortby_game)
        : '';
      this.text ? (this.params.text = this.text) : '';
    }
    // this.selected_platform ? (this.params.platform = this.selected_platform) : '';
    // this.selected_sortby_game ? (this.params.game = this.selected_sortby_game) : '';
    // this.selected_sortby_tournament ? (this.params.status = this.selected_sortby_tournament) : '';
    // this.text ? (this.params.text = this.text) : '';
    this.params.limit = this.paginationData.limit;
    this.params.page = this.paginationData.page;

    if (gtmEvent) {
      this.pushAllTournamentViewTags();
    }

    this.homeService.getTournament(this.params).subscribe(
      (res: any) => {
        this.showLoader = false;
        this.TournamentsList = res?.data?.docs;
        this.paginationDetails = res?.data;
        this.paginationData.limit = this.paginationDetails.limit;
        this.paginationData.page = this.paginationDetails.page;
        for (let d of res?.data?.docs) {
          if (d?.participantType == 'individual') {
            this.individualTournaments.push(d);
          } else if (d?.isPrize == true && d?.isPaid == true) {
            this.prizeMoneyTournaments.push(d);
          } else if (d?.participantType == 'team') {
            this.teamTournaments.push(d);
          }
        }
      },
      (err: any) => {
        this.showLoader = false;
      }
    );
  }
  pageChanged(page): void {
    this.paginationData.page = page.pageIndex + 1;
    this.paginationData.limit = page.pageSize;
    this.getAllTournaments();
  }

  selectGames() {
    this.individualTournaments = [];
    this.prizeMoneyTournaments = [];
    this.teamTournaments = [];
    // this.showLoader = true;

    this.selected_platform
      ? (this.params.platform = this.selected_platform)
      : '';
    this.selected_sortby_game
      ? (this.params.game = this.selected_sortby_game)
      : '';
    this.selected_sortby_tournament
      ? (this.params.status = this.selected_sortby_tournament)
      : '';
    this.text ? (this.params.text = this.text) : '';
  }

  addGames() {
    this.params.limit = this.paginationData.limit;
    this.params.page = this.paginationData.page;
    this.homeService.getTournament(this.params).subscribe(
      (res: any) => {
        this.showLoader = false;
        this.TournamentsList = res?.data?.docs;
        this.paginationDetails = res?.data;
        this.paginationData.limit = this.paginationDetails.limit;
        this.paginationData.page = this.paginationDetails.page;
        for (let d of res?.data?.docs) {
          if (d?.participantType == 'individual') {
            this.individualTournaments.push(d);
          } else if (d?.isPrize == true && d?.isPaid == true) {
            this.prizeMoneyTournaments.push(d);
          } else if (d?.participantType == 'team') {
            this.teamTournaments.push(d);
          }
        }
      },
      (err: any) => {
        this.showLoader = false;
      }
    );
  }
  pushAllTournamentViewTags() {
    switch (this.selected_sortby_tournament) {
      case '1':
        this.pushGTMTags('View_All_Ongoing_Tournaments', {
          fromPage: this.router.url,
        });
        break;
      case '0':
        this.pushGTMTags('View_All_Upcoming_Tournaments', {
          fromPage: this.router.url,
        });
        break;
      default:
        break;
    }
  }

  pushTournamentViewTag(tournament) {
    switch (this.selected_sortby_tournament) {
      case '1':
        this.pushGTMTags('View_Ongoing_Tournament', {
          fromPage: this.router.url,
          tournamentName: tournament?.name,
        });
        break;
      case '0':
        this.pushGTMTags('View_Upcoming_Tournament', {
          fromPage: this.router.url,
          tournamentName: tournament?.name,
        });
        break;
      default:
        break;
    }
  }

  pushGTMTags(eventName, eventData: EventProperties = null) {
    this.gtmService.pushGTMTags(eventName, eventData);
  }
}
