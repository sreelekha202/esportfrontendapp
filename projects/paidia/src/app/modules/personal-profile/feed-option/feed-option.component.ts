import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-feed-option',
  templateUrl: './feed-option.component.html',
  styleUrls: ['./feed-option.component.scss'],
})
export class FeedOptionComponent implements OnInit {
  constructor() {}

  dataFeed = ['Feed'];

  menuFeed = [
    'Feed',
    'Statistics',
    'Teams',
    'Tournaments',
    'Photos/Videos',
    'Content',
  ];

  ngOnInit(): void {}

  handleCLick(item): void {
    let a = this.menuFeed.filter((data, index) => data === item);
    this.dataFeed = a;
  }
}
