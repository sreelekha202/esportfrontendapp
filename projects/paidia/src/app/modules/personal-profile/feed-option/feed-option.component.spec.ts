import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FeedOptionComponent } from './feed-option.component';

describe('FeedOptionComponent', () => {
  let component: FeedOptionComponent;
  let fixture: ComponentFixture<FeedOptionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FeedOptionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FeedOptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
