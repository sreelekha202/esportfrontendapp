import { Component, Input, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { GENDER, IUser } from 'esports';
import { onLoadImgError, TYPE_IMG_LOAD } from '../../../shared/comon';

@Component({
  selector: 'app-banner-person',
  templateUrl: './banner-person.component.html',
  styleUrls: ['./banner-person.component.scss'],
})
export class BannerPersonComponent implements OnInit {
  public typeImgLoad = TYPE_IMG_LOAD;
  public onLoadImgError: Function = onLoadImgError;
  @Input() currentUser: IUser;
  @Input() username: any;
  @Input() profilePicture: any;
  @Input() createdOn: any;
  @Input() gender: any;
  @Input() customPronoun: any;

  constructor(private _translateService: TranslateService) {}

  ngOnInit(): void {}

  public getGender(gender: GENDER, customPronoun) {
    let pronoun: string;
    switch (gender) {
      case GENDER.HE:
        pronoun = GENDER.HE.toUpperCase();
        break;
      case GENDER.SHE:
        pronoun = GENDER.SHE.toUpperCase();
        break;
      case GENDER.THEY:
        pronoun = GENDER.THEY.toUpperCase();
        break;
      default:
        return customPronoun;
    }
    return this._translateService.instant(`SOCIAL_FEED.USER.GENDER.${pronoun}`);
  }
}
