import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BannerPersonComponent } from './banner-person.component';

describe('BannerPersonComponent', () => {
  let component: BannerPersonComponent;
  let fixture: ComponentFixture<BannerPersonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BannerPersonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BannerPersonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
