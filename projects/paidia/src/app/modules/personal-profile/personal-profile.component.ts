import { Component, Inject, OnInit, PLATFORM_ID } from '@angular/core';
import { BannerPersonComponent } from './banner-person/banner-person.component';
import { Subscription } from 'rxjs';
import { isPlatformBrowser } from '@angular/common';
import {
  EsportsUserService,
  EsportsGameService,
  IGame,
  EsportsSocialService,
  IUser,
  IPOST,
} from 'esports';
import { environment } from '../../../environments/environment';
const LIMIT_POSTS = 10;
@Component({
  selector: 'app-personal-profile',
  templateUrl: './personal-profile.component.html',
  styleUrls: ['./personal-profile.component.scss'],
})
export class PersonalProfileComponent implements OnInit {
  public currentUser: IUser;
  public posts: Array<IPOST> = [];
  public page: number = 0;
  public pageTotal: number = 0;
  public isBrowser: boolean;
  public gamesList: any[];
  public boxGames = [
    {
      img: '../../../../assets/images/Profile/apex.png',
      text_1: 'xxinfinity01',
      text_2: 'Apex Legends',
    },
    {
      img: '../../../../assets/images/Profile/dota.png',
      text_1: 'moondust',
      text_2: 'Dota 1',
    },
    {
      img: '../../../../assets/images/Profile/fortnite.png',
      text_1: 'xxinfinity01',
      text_2: 'Fortnite',
    },
  ];

  public boxGamesExpand = [
    {
      img: '../../../../assets/images/Profile/apex2.png',
      text_1: 'Apex Legends 3v3 face off',
      text_2: '1 week ago',
    },
    {
      img: '../../../../assets/images/Profile/dota2.png',
      text_1: 'Valorant weekly challenge',
      text_2: '3 weeks ago',
    },
    {
      img: '../../../../assets/images/Profile/fortnite2.png',
      text_1: 'Fortnite team 4v4 august',
      text_2: '3 weeks ago',
    },
  ];

  constructor(
    @Inject(PLATFORM_ID) private platformId,
    private _userService: EsportsUserService,
    private _socialService: EsportsSocialService,
    private gameService: EsportsGameService
  ) {
    this.isBrowser = isPlatformBrowser(platformId);
  }

  ngOnInit(): void {
    this.getCurrentUser();
    this.getGamesList();
  }

  private getGamesList() {
    this.gameService.getGames(environment.apiEndPoint).subscribe((res) => {
      this.gamesList = res?.data;
    });
  }

  private getCurrentUser() {
    this._userService.currentUser.subscribe((data: IUser) => {
      this.currentUser = data;
      this.getPosts(0, LIMIT_POSTS);
    });
  }

  private getPosts(skip: number, limit: number) {
    this._socialService
      .getPostPrivate(this.currentUser?._id, skip, limit)
      .subscribe(
        (res) => {
          const { posts, total } = res?.data;
          if (!posts) throw Error();
          this.posts = [...this.posts, ...posts];
          this.page = Math.ceil(this.posts.length / 10);
          this.pageTotal = this.pageTotal = Math.ceil(total / 10);
        },
        (err) => {}
      );
  }
}
