import {
  Component,
  OnInit,
  Input,
  EventEmitter,
  ViewChild,
} from '@angular/core';
import {
  MatCalendar,
  MatCalendarCellCssClasses,
} from '@angular/material/datepicker';
import { ProfileService } from './../../../core/service/profile.service';
import { environment } from '../../../../environments/environment';
import {
  EsportsGtmService,
  EsportsToastService,
  EsportsTournamentService,
  EventProperties,
  IPagination,
  IUser,
} from 'esports';
import { MatTabChangeEvent } from '@angular/material/tabs';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
@Component({
  selector: 'app-about-profile',
  templateUrl: './about-profile.component.html',
  styleUrls: ['./about-profile.component.scss'],
})
export class AboutProfileComponent implements OnInit {
  @ViewChild(MatCalendar) calendar: MatCalendar<Date>;
  //selectedDate:any = false;
  @Input() currentUser: IUser;

  panelOpenState = false;
  isHidden = false;

  tab = 0; //ongoing
  activeTabIndex = 0;
  cPagination;
  pagination = {
    page: 1,
    limit: 10,
    sort: 'startDate',
  };
  active = 1;
  page: IPagination;
  pageSizeOptions = environment.pageSizeOptions;
  customTabEvent = new EventEmitter<MatTabChangeEvent>();
  tournamentSubscription: Subscription;

  userSubscription: Subscription;
  nextId = 1;

  paginationDetails: any;

  paginationData = {
    page: 1,
    limit: 10,
    sort: 'startDate',
  };
  mock_cards = [];
  tournamentList = [];
  selectedDate: any;
  fiveUpcomingActivities = [];
  onSlectTournamentList = [];
  startDatesToHighlight = [];
  constructor(
    private eSportsToastService: EsportsToastService,
    private profileService: ProfileService,
    private tournamentService: EsportsTournamentService,
    private gtmService: EsportsGtmService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.activeTabIndex =
      +this.activatedRoute.snapshot.queryParams?.activeTab || 0;
    this.active = this.activeTabIndex ? this.activeTabIndex + 1 : 1;
    this.cPagination = Object.assign({}, this.pagination);
    this.switchData(this.activeTabIndex);
    this.getMyMatches();
  }
  selectedChange() {}
  updateCalander() {
    this.calendar.updateTodaysDate();
  }

  switchData(index: number) {
    switch (index) {
      case 0:
        this.pushGTMTags('View_Ongoing_Tournament', {
          fromPage: this.router.url,
        });

        this.fetchTournaments(1); //ongoing
        break;
      case 1:
        this.pushGTMTags('View_Upcoming_Tournament', {
          fromPage: this.router.url,
        });
        this.fetchTournaments(1); //upcoming
        break;
      case 2:
        this.pushGTMTags('View_Past_Tournament ', {
          fromPage: this.router.url,
        });
        this.fetchTournaments(2); //past
        break;
      default:
        break;
    }
  }

  onSelect(event) {
    this.onSlectTournamentList = [];
    this.selectedDate = event;
    const date1 = new Date(this.selectedDate).getDate();
    const selected_date_time = new Date(this.selectedDate);
    let selected_date = selected_date_time.getDate();
    let selected_month = selected_date_time.getMonth();
    let selected_year = selected_date_time.getFullYear();
    this.tournamentList.forEach((tournament, i) => {
      //  if(new Date(tournament?.tournament?.startDate).getDate() == date1){
      //    this.onSlectTournamentList.push(tournament)
      //  }

      const tournament_date_time = new Date(tournament?.tournament?.startDate);
      let tournament_date = tournament_date_time.getDate();
      let tournament_month = tournament_date_time.getMonth();
      let tournament_year = tournament_date_time.getFullYear();
      if (
        selected_date == tournament_date &&
        selected_month == tournament_month &&
        selected_year == tournament_year
      ) {
        this.onSlectTournamentList.push(tournament);
      }
    });
  }

  dateClass() {
    return (date: Date): MatCalendarCellCssClasses => {
      // const highlightDate = this.datesToHighlight
      const highlightDate = this.startDatesToHighlight
        .map((strDate) => new Date(strDate))
        .some(
          (d) =>
            d.getDate() === date.getDate() &&
            d.getMonth() === date.getMonth() &&
            d.getFullYear() === date.getFullYear()
        );

      return highlightDate ? 'special-date' : '';
    };
  }

  fetchTournaments = async (tab) => {
    try {
      this.tab = tab;
      this.tournamentList = [];

      if (this.tournamentSubscription) {
        this.tournamentSubscription.unsubscribe();
      }

      let payload;
      payload = await this.getJoinedTournamentQuery(tab);
      this.tournamentSubscription = this.tournamentService
        .getParticipantTournament1(payload)
        .subscribe((tournament) => {
          if (tournament) {
            this.page = {
              totalItems: tournament?.data?.totalDocs,
              itemsPerPage: tournament?.data?.limit,
              maxSize: 5,
            };
            this.tournamentList = tournament?.data?.docs || [];
            if (tournament?.data?.docs.length > 2) {
              this.fiveUpcomingActivities =
                tournament?.data?.docs?.slice(0, 5) || [];
            } else {
              this.fiveUpcomingActivities = tournament?.data?.docs || [];
            }
            this.tournamentList.forEach((tournament, i) => {
              this.startDatesToHighlight.push(
                tournament?.tournament?.startDate
              );
            });
            if (this.startDatesToHighlight) {
              this.updateCalander();
            }
          }
        });
    } catch (error) {
      this.eSportsToastService.showError(
        error?.error?.message || error?.message
      );
    }
  };

  getJoinedTournamentQuery(tab) {
    switch (true) {
      case tab === 0: // ongoing
        this.isHidden = false;
        return {
          pagination: JSON.stringify(this.cPagination),
          type: 1,
        };
      case tab === 1: //upcoming
        this.isHidden = true;

        return {
          pagination: JSON.stringify(this.cPagination),
          type: 0,
          isEdit: true,
        };
      case tab === 2: //past
        this.isHidden = true;

        return {
          pagination: JSON.stringify(this.cPagination),
          type: 2,
          isEdit: true,
        };
    }
  }

  pushGTMTags(eventName: string, eventData: EventProperties = null) {
    this.gtmService.pushGTMTags(eventName, eventData);
  }

  getMyMatches() {
    //  this.isLoaded = true;
    const params = {
      status: 'current',
      limit: this.paginationData?.limit,
      page: this.paginationData?.page,
      includeBattleRoyale: true,
    };
    this.profileService.getMatchev2(params).subscribe(
      (res) => {
        this.mock_cards = [];
        // this.isLoaded = false;
        const data = res?.data?.docs;
        this.paginationDetails = res?.data;
        this.paginationData.limit = this.paginationDetails?.limit;
        this.paginationData.page = this.paginationDetails?.page;
        let a = [];
        for (let index = 0, len = data.length; index < len; index++) {
          const element = data[index];
          element?.match?.currentMatch?.round &&
            element?.opponentPlayer?.teamName;
          a.push({
            title: element?.match?.currentMatch?.round
              ? `Round ${element?.match?.currentMatch?.round}`
              : '',
            opponentName: element?.opponentPlayer?.teamName || 'N/A',
            gameAccount: element?.opponentPlayer?.inGamerUserId || 'N/A',
            image: element?.tournament?.gameDetail?.logo || 'N/A',
            tournament: element?.tournament?._id || '',
            tournaments: element?.tournament,
            tournamentSlug: element?.tournament?.slug || '',
            matchId: element?.match?._id || '',
            participantId: element?.userParticepentId || '',
            match: element?.match,
            //  isAdmin: this.isAdmin,
            tournamentName: element?.tournament?.name
              ? element?.tournament?.name.length > 28
                ? `${element?.tournament?.name
                    .toLowerCase()
                    .substring(0, 28)
                    .slice(0, -3)}...`
                : element?.tournament?.name.toLowerCase()
              : '',
          });
        }
        this.mock_cards = a;
      },
      (err) => {
        // this.isLoaded = false;
      }
    );
  }
}
