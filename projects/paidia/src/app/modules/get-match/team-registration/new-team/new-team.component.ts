import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AppHtmlRoutes, AppHtmlTeamRegRoutes } from '../../../../app-routing.model';

@Component({
  selector: 'app-season-new-team',
  templateUrl: './new-team.component.html',
  styleUrls: ['./new-team.component.scss'],
})
export class NewSeasonTeamComponent implements OnInit {
  AppHtmlTeamRegRoutes = AppHtmlTeamRegRoutes;
  AppHtmlRoutes = AppHtmlRoutes;

  @Output() submit = new EventEmitter();
  @Input() team;

  constructor() {}

  ngOnInit(): void {}

  ngOnChanges() {}

  selectTeam(i) {
    this.submit.emit(this.team[i]);
  }
}
