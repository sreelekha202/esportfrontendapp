import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppHtmlRoutes } from '../../app-routing.model';
import { GameService } from '../../core/service/game.service';

@Component({
  selector: 'app-get-match',
  templateUrl: './get-match.component.html',
  styleUrls: ['./get-match.component.scss'],
})
export class GetMatchComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;


  matchmakingDetails: any;
  selectedGame: any;

  constructor(private gameService: GameService, private router: Router) {

  }

  ngOnInit(): void {
    this.matchmakingDetails = this.gameService.createMatchMaking.subscribe((data:any) => {
      if (data) {
        this.selectedGame = data.selectedGame;
      }
    });
    // MOCK SELECTED GAME
    // this.selectedGame = {
    //   image:
    //     'https://i114.fastpic.ru/big/2021/0613/ea/1362412cbb852fd522e0ef0b781cafea.jpg',
    //   icon: 'https://i114.fastpic.ru/big/2021/0614/06/c2780510c7ee16fc8f04b5dc1403b306.png',
    //   name: 'Dota 2',
    // };
  }
  next() {
    // if () {
    //   this.gameService.createMatchMakingSubject.next({ ...this.createMatchMaking, "selectedGame": selectedGame })
    //   this.router.navigateByUrl("/get-match")
    // }
  }
}
