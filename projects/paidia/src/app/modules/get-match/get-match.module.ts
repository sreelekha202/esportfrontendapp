import { SelectSeasonTeammatesComponent } from './team-registration/select-teammates/select-teammates.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClipboardModule } from '@angular/cdk/clipboard';
import { CoreModule } from '../../core/core.module';
import { FormComponentModule } from '../../shared/components/form-component/form-component.module';
import { HeaderPaidiaModule } from '../../shared/components/header-paidia/header-paidia.module';
import { UploadImageModule } from '../../shared/components/upload-image/upload-image.module';
import { GetMatchComponent } from './get-match.component';
import { GetMatchRoutingModule } from './get-match-routing.module';
import { GameLobbyComponent } from './game-lobby/game-lobby.component';
import { SelectMembersComponent } from './select-members/select-members.component';
import { SelectPlatformComponent } from './select-platform/select-platform.component';
import { SelectTeamsComponent } from './select-teams/select-teams.component';
// GAME LOBBY COMPONENTS
import { MatchroomChatComponent } from './game-lobby/matchroom-chat/matchroom-chat.component';
import { PlayersComponent } from './game-lobby/players/players.component';
import { PostgameStatsComponent } from './game-lobby/postgame-stats/postgame-stats.component';
import { LoadingModule } from '../../core/loading/loading.module';
import { RouterBackModule } from '../../shared/directives/router-back.module';
import { GameheaderComponent } from './game-lobby/gameheader/gameheader.component';
// import { PopupCreatedComponent } from './popup-created/popup-created.component'; (matchmaking 1.0)
import { PopupCreatedComponent } from './manage-score/components/popup-created/popup-created.component';
import { TeamSeasonRegistrationComponent } from './team-registration/team-registration/team-registration.component';
import { NewSeasonTeamComponent } from './team-registration/new-team/new-team.component';
import { SharedModule } from '../../shared/modules/shared.module';
import { ManageScoreComponent } from './manage-score/manage-score.component';
import { TeamBannerComponent } from './manage-score/components/team-banner/team-banner.component';

const components = [
  GameLobbyComponent,
  GetMatchComponent,
  MatchroomChatComponent,
  PlayersComponent,
  PostgameStatsComponent,
  SelectMembersComponent,
  SelectPlatformComponent,
  SelectTeamsComponent,
  GameheaderComponent,
  PopupCreatedComponent,
  TeamSeasonRegistrationComponent,
  SelectSeasonTeammatesComponent,
  NewSeasonTeamComponent,
  ManageScoreComponent,
  TeamBannerComponent
];

const modules = [
  ClipboardModule,
  CommonModule,
  CoreModule,
  FormComponentModule,
  GetMatchRoutingModule,
  HeaderPaidiaModule,
  SharedModule,
  UploadImageModule,
  RouterBackModule
];

@NgModule({
  declarations: components,
  imports: modules,
})
export class GetMatchModule { }
