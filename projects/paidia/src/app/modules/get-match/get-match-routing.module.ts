import { AuthGuard } from './../../shared/guard/auth.guard';
import { TeamSeasonRegistrationComponent } from './team-registration/team-registration/team-registration.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GetMatchComponent } from './get-match.component';

import { GameLobbyComponent } from './game-lobby/game-lobby.component';
import { SelectMembersComponent } from './select-members/select-members.component';
import { SelectPlatformComponent } from './select-platform/select-platform.component';
import { SelectTeamsComponent } from './select-teams/select-teams.component';
import { ManageScoreComponent } from './manage-score/manage-score.component';

const routes: Routes = [
  {
    path: '',
    component: GetMatchComponent,
    children: [
      {
        path: '',
        component: SelectPlatformComponent,
      },
      {
        path: 'teams',
        component: SelectTeamsComponent,
      },
      {
        path: 'members',
        component: SelectMembersComponent,
      },
      {
        path: 'game-lobby',
        component: GameLobbyComponent,
      },
      {
        path: 'manage-score',
        component: ManageScoreComponent,
      },
      {
        path: 'team-registration/:id',
        component: TeamSeasonRegistrationComponent,
        canActivate: [AuthGuard],
        data: {
          isRootPage: true,
        },
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GetMatchRoutingModule {}
