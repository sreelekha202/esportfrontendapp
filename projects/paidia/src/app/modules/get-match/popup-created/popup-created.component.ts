import { EsportsUserService, EsportsChatService } from 'esports';
import { GameService } from './../../../core/service/game.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialogRef } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { EsportsChatSidenavService } from 'esports';

@Component({
  selector: 'app-popup-created',
  templateUrl: './popup-created.component.html',
  styleUrls: ['./popup-created.component.scss'],
})
export class PopupCreatedComponent implements OnInit {
  matchmakingDetails: Subscription;
  createMatchMaking: any;
  userSubscription: Subscription;
  currentUserId: any;
  currentUser: any;
  windowposition: String = 'chat_window chat_window_right_game_drawer';
  chatUser: any;

  constructor(
    public dialogRef: MatDialogRef<any>,
    private router: Router,
    private gameService: GameService,
    private chatService: EsportsChatService,
    private userService: EsportsUserService,
    private chatSidenavService: EsportsChatSidenavService,
  ) { }

  ngOnInit(): void {
    this.matchmakingDetails = this.gameService.createMatchMaking.subscribe((data: any) => {
      if (data) {
        this.createMatchMaking = data;
        this.chatUser = data.chatUser;
      }
    });



    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      this.currentUser = data;
      this.currentUserId = this.currentUser._id;
    })
  }

  onClose(): void {
    this.dialogRef.close();
  }

  onCloseAndRedirect(): void {
    this.onClose();

    setTimeout(() => {
      this.chatSidenavService.open();
      if (this.currentUserId) {
        this.chatService?.getAllMatches(this.currentUserId, 1, 30);
      }

      if (this.chatSidenavService.checkOpen()) {
        if (this.chatService.getChatStatus()) {
          this.chatService.setChatStatus(false);
        }
      } else {
        if (this.chatService.getChatStatus()) {
          this.chatService.setChatStatus(false);
        }
      }
    }, 1000);
  }



  closeDialog() {
    this.onClose();
    if (!(this.chatUser == null)) {
      let matchid = this.chatUser._id + '-' + this.currentUserId;
      let matchdetails = {
        matchid: matchid,
        image: this.chatUser.image,
        matchName: this.chatUser.matchName,
        name: this.chatUser.name,
        gameid: this.createMatchMaking.selectedGame._id,
      };
      this.chatService.setWindowPos(this.windowposition);
      this.chatService.setCurrentMatch(matchdetails);
      this.chatService.setTypeOfChat('game');
      this.chatService?.getAllMatches(this.currentUserId, 1, 30);
      this.chatService.setChatStatus(true);
      this.chatService.setTextButtonStatus(true);
      if (this.chatService.getChatStatus() == false) {
        this.chatService?.disconnectMatch(matchid, 'game');
      }
    }

  }


  ngOnDestroy() {
    if (this.userSubscription)
      this.userSubscription?.unsubscribe();
  }

}

