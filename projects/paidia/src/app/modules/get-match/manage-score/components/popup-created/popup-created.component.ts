import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialogRef } from '@angular/material/dialog';
import { AppHtmlRoutes } from 'projects/paidia/src/app/app-routing.model';
import { Location } from '@angular/common'

@Component({
  selector: 'app-popup-created',
  templateUrl: './popup-created.component.html',
  styleUrls: ['./popup-created.component.scss'],
})
export class PopupCreatedComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;

  constructor(public dialogRef: MatDialogRef<any>, private router: Router,private location: Location) {}

  ngOnInit(): void {}

  onClose(): void {
    this.dialogRef.close();
  }

  onCloseAndRedirect(): void {
    this.onClose();
    this.location.back()
  }
}
