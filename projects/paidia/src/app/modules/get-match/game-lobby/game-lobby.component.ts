import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { BracketService } from '../../../core/service/bracket.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { GameService } from '../../../core/service';
import {
  EsportsToastService,
  EsportsChatService,
  EsportsUtilsService,
  EsportsParticipantService,
  EsportsGameService,
  IUser,
  EsportsGtmService,
  EsportsBracketService
} from 'esports';
import { environment } from '../../../../environments/environment';
const API = environment.apiEndPoint;
import { EsportsUserService } from 'esports';
import { Subscription } from 'rxjs';
@Component({
  selector: 'app-game-lobby',
  templateUrl: './game-lobby.component.html',
  styleUrls: ['./game-lobby.component.scss'],
})
export class GameLobbyComponent implements OnInit, OnDestroy {
  @Input() isSingle: boolean = true;
  isImReady: boolean = false;
  isLoading: boolean = false;
  resultForm: FormGroup;
  myName: string = '';
  gameInfo = null;
  userSubscription: Subscription;
  messages = [];
  playersData = [];
  teamsData = [];
  matchId: string = null;
  matchData;
  totalPlayers = 0;
  outcome: 'win' | 'lose';
  myTeam: 'teamA' | 'teamB' = 'teamA';
  showUpdateScore: boolean = false;
  reportScore: boolean = false;
  getStatusInterval;
  currentUser: IUser;
  windowposition = 'chat_window chat_window_right_drawer';
  steps = [
    {
      isDone: true,
      text: 'GAME_LOBBY.STEPS.FIRST',
      icon: 'assets/icons/game-lobby/FIRST.svg',
    },
    {
      isDone: false,
      text: 'GAME_LOBBY.STEPS.SECOND',
      icon: 'assets/icons/game-lobby/SECOND.svg',
    },
    {
      isDone: false,
      text: 'GAME_LOBBY.STEPS.THIRD',
      icon: 'assets/icons/game-lobby/THIRD.svg',
    },
    {
      isDone: false,
      text: 'GAME_LOBBY.STEPS.FOURTH',
      icon: 'assets/icons/game-lobby/FOURTH.svg',
    },
  ];
  teamA: { name: any; players: any[] };
  teamB: { name: any; players: any[] };
  //isImReady1: boolean=false;
  constructor(
    private location: Location,
    private modalService: NgbModal,
    private fb: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private participantService: EsportsParticipantService,
    private gameService1: GameService,
    private bracketService: EsportsBracketService,
    private utilsService: EsportsUtilsService,
    private toastService: EsportsToastService,
    private translateService: TranslateService,
    public esportsChatService: EsportsChatService,
    public gameService: EsportsGameService,
    private gtmService: EsportsGtmService,
    private userService: EsportsUserService
  ) {}

  ngOnInit(): void {
    this.matchId = this.activatedRoute.snapshot.queryParams['matchId'] || null;
    if (this.matchId) {
      this.getMatchDetails();
      // this.currentuserdetails();
    }

    this.resultForm = this.fb.group({
      result: ['', [Validators.required, Validators.pattern(/(win|lose)/)]],
      screenShot: ['', Validators.required],
    });

    // MOCK GAME INFO
    this.gameInfo = {
      image:
        'https://i114.fastpic.ru/big/2021/0331/15/a8494278c27be0b22a3ff182fdbc5115.png',
      icon: 'https://i114.fastpic.ru/big/2021/0331/fd/c40ec959a18ae125d364cd1ce8d1f6fd.png',
    };

    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.currentUser = data;
      }
    });
  }

  listenScoreUpdateEvent() {
    this.esportsChatService.connectSocket();
    this.esportsChatService.connectUser();
    this.esportsChatService.listenShowUpdateScore((payload) => {
      this.showUpdateScore = true;
    });
  }

  goBack() {
    this.location.back();
  }

  onUploadResult(content) {
    this.modalService.open(content, {
      centered: true,
      size: 'xl',
    });
  }

  onOutcomeSelect(outcome, content) {
    this.outcome = outcome;
    this.modalService.open(content, {
      centered: true,
      size: 'xl',
    });
    this.resultForm.controls.result.patchValue(this.outcome);
  }
  currentuserdetails() {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.currentUser = data;
      }
    });
  }
  getMatchDetails() {
    const dataSubscription = this.participantService
      .getMatchDetails(API, this.matchId)
      .subscribe((res: { data; message }) => {
        this.matchData = res?.data[0] || null;
        if (this.matchData) {
          if (this.currentUser._id == this.matchData?.teamA?.userId?._id) {
            if (this.matchData.matchStatus == 'active') {
              if (
                this.matchData.scoreUpdateByParticipant ==
                this.matchData?.teamA._id
              ) {
                this.reportScore = false;
              } else {
                this.reportScore = true;
              }
            }

            // if (this.matchData?.isParticipantReady?.teamAReady) {
            //   this.isImReady = false;
            // } else {
            //   this.isImReady = true;
            //   this.reportScore = false;
            // }
          } else {
            if (this.matchData.matchStatus == 'active') {
              if (
                this.matchData.scoreUpdateByParticipant ==
                this.matchData?.teamB._id
              ) {
                this.reportScore = false;
              } else {
                this.reportScore = true;
              }
            }

            // if (this.matchData.isParticipantReady.teamBReady) {
            //   this.isImReady = false;
            // } else {
            //   this.isImReady = true;
            //   this.reportScore = false;
            // }
          }

          if (
            this.matchData.matchUpdatedByTeamACount > 0 &&
            this.matchData.matchUpdatedByTeamACount > 0
          ) {
            this.reportScore = false;
          }

          this.getParticipantDetails();
          this.fetchSeasonDetails();
        }

        // if (
        //   this.matchData?.scoreUpdateByParticipant == null ||
        //   this.matchData?.scoreUpdatedByParticipantDt == null
        // ) {
        //   this.reportScore = true;
        // } else {
        //   this.reportScore = false;
        // }
        dataSubscription.unsubscribe();
      });
  }

  setShowUploadResult(matchData) {
    // if (
    //   matchData?.isParticipantReady?.teamAReady == true &&
    //   matchData?.isParticipantReady?.teamBReady == true
    // ) {
    //   this.showUpdateScore = true;
    // } else {
    //   this.showUpdateScore = false;
    // }
  }

  getParticipantDetails() {
    this.isSingle = true;
    this.playersData = [];
    this.playersData.push(this.matchData.teamA);
    this.playersData.push(this.matchData.teamB);
    this.totalPlayers = 2;
    if (this.matchData.teamA?.teamMembers?.length > 0) {
      this.teamsData = [];
      let team1Players = [
        {
          id: this.matchData.teamA._id,
          name: this.matchData.teamA.name,
          inGamerUserId: this.matchData.teamA.inGamerUserId,
          image: this.matchData.teamA.logo,
        },
      ];
      const mapImage = (challenge, challenger, arrayName) => {
        challenge[challenger][arrayName] = challenge[challenger][arrayName].map(
          (ele) => {
            ele.image = challenge[challenger].logo;
            return ele;
          }
        );
      };
      mapImage(this.matchData, 'teamA', 'teamMembers');
      mapImage(this.matchData, 'teamA', 'substituteMembers');
      mapImage(this.matchData, 'teamB', 'teamMembers');
      mapImage(this.matchData, 'teamB', 'substituteMembers');
      const team1 = {
        name: this.matchData.teamA.teamName,
        players: [
          ...team1Players,
          ...this.matchData.teamA.teamMembers,
          ...this.matchData.teamA.substituteMembers,
        ],
      };
      let team2Players = [
        {
          id: this.matchData.teamB._id,
          name: this.matchData.teamB.name,
          inGamerUserId: this.matchData.teamB.inGamerUserId,
          image: this.matchData.teamB.logo,
        },
      ];
      const team2 = {
        name: this.matchData.teamB.teamName,
        players: [
          ...team2Players,
          ...this.matchData.teamB.teamMembers,
          ...this.matchData.teamB.substituteMembers,
        ],
      };

      this.teamA = team1;
      this.teamB = team2;
      this.teamsData = [team1, team2];
      this.isSingle = false;
      this.totalPlayers = this.teamsData.length;
    }
    this.getMyStatus();
  }
  chatFunction(id) {
    this.gtmService.pushGTMTags('Matchmaking_Chat_Started');
    this.esportsChatService.getMatchInformation(id).subscribe((result: any) => {
      const mtchdetails = {
        matchid: result.matchdetails._id,
        teamAId: result.matchdetails.teamA.userId._id,
        teamBId: result.matchdetails.teamB.userId._id,
        tournamentid: result.matchdetails.tournamentId,
        typeofchat: 'tournament',
        users: [
          {
            _id: result.matchdetails.teamA.userId._id,
            fullName: result.matchdetails.teamA.userId.fullName,
            profilePicture: result.matchdetails.teamA.userId.profilePicture,
          },
          {
            _id: result.matchdetails.teamB.userId._id,
            fullName: result.matchdetails.teamB.userId.fullName,
            profilePicture: result.matchdetails.teamB.userId.profilePicture,
          },
        ],
      };
      this.esportsChatService.setWindowPos(this.windowposition);
      let firstChat = this.esportsChatService.getChatStatus();
      if (firstChat == true) {
        this.esportsChatService.setChatStatus(false);
        this.esportsChatService?.disconnectMatch(
          mtchdetails.matchid,
          'tournament'
        );
        this.esportsChatService.setTextButtonStatus(false);
      } else {
        this.esportsChatService.setCurrentMatch(mtchdetails);
        this.esportsChatService.setTypeOfChat('tournament');
        this.esportsChatService.setChatStatus(true);
        this.esportsChatService.setTextButtonStatus(true);
      }
    });
  }
  fetchSeasonDetails() {
    const dataSubscription = this.gameService
      .fetchSeasonDetails(API, this.matchData.seasonId)
      .subscribe((res) => {
        const data = res['data']['data'];
        Object.assign(this.gameInfo, {
          image: data.game.image,
        });
        dataSubscription.unsubscribe();
      });
  }
  getMyStatus() {
    const dataSubscription = this.participantService
      .getSeasonJoinStatus(API, {
        seasonId: this.matchData.seasonId,
      })
      .subscribe((res: { data: { participant: any } }) => {
        const data = res?.data?.participant;
        if (this.matchData.teamA._id != data._id) {
          this.myTeam = 'teamB';
        }

        this.setShowUploadResult(this.matchData);
        if (this.showUpdateScore) {
          if (this.myTeam == 'teamA') {
            this.showUpdateScore =
              this.matchData.matchUpdatedByTeamACount > 0 ? false : true;
          } else {
            this.showUpdateScore =
              this.matchData.matchUpdatedByTeamBCount > 0 ? false : true;
          }
        }
        if (this.showUpdateScore) {
          if (this.getStatusInterval) {
            clearInterval(this.getStatusInterval);
          }
        }
        dataSubscription.unsubscribe();
      });
  }
  async submitScore() {
    try {
      let payload = {
        teamAScore: 0,
        teamBScore: 0,
        teamAScreenShot: null,
        teamBScreenShot: null,
      };
      if (this.myTeam == 'teamA') {
        if (this.outcome == 'win') {
          payload.teamAScore = 1;
        } else {
          payload.teamBScore = 1;
        }
        payload.teamAScreenShot = this.resultForm.value.screenShot;
      } else {
        if (this.outcome == 'win') {
          payload.teamBScore = 1;
        } else {
          payload.teamAScore = 1;
        }
        payload.teamBScreenShot = this.resultForm.value.screenShot;
      }
      this.modalService.dismissAll();
      this.isLoading = true;

    /**
      const query = {
        _id: this.matchData?._id,
        'sets.id': 1,
        tournamentId: this.matchData?.tournamentId?._id,
      };
      const queryParam = `?query=${this.utilsService.getEncodedQuery(query)}`;
      this.modalService.dismissAll();
      this.isLoading = true;
      let scoreUpdate = await this.bracketService.updateParticipantScore(
        queryParam,
        payload
      );
     */

      const queryParams = {
        matchId: this.matchData?._id,
        'setId': 1,
        tournamentId: this.matchData?.tournamentId?._id,
      };

      let scoreUpdate = await this.bracketService.updateParticipantScoreV2(
        queryParams,
        payload
      );

      this.isLoading = false;
      this.showAlertBasedOnPlatform(scoreUpdate?.message, 'showSuccess');
      this.showUpdateScore = false;
    } catch (error) {
      this.isLoading = false;
      this.showAlertBasedOnPlatform(
        error?.error?.message || error?.message,
        'showError'
      );
    }
  }
  showAlertBasedOnPlatform = (message, type) => {
    this.toastService[type](message);
  };
  participantReady() {
    this.listenScoreUpdateEvent();
    this.checkIAmReadyStatus();
    const dataSubscription = this.gameService
      .readyParticipant(API, { matchId: this.matchId })
      .subscribe((res) => {
        this.isImReady = false;
        dataSubscription.unsubscribe();
      });
  }
  checkIAmReadyStatus() {
    this.getStatusInterval = setInterval(() => {
      this.getMatchDetails();
      // this.getMyStatus();
    }, 1 * 1000 * 60);
  }
  ngOnDestroy() {
    if (this.getStatusInterval) {
      clearInterval(this.getStatusInterval);
    }
  }
  showCopySnackbar() {
    this.toastService.showInfo(
      this.translateService.instant('LADDER.GAME_ID_COPIED')
    );
  }
}
