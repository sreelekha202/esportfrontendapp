import { Component, OnInit, Input } from '@angular/core';
import { AppHtmlRoutes } from '../../../../app-routing.model';
@Component({
  selector: 'app-gameheader',
  templateUrl: './gameheader.component.html',
  styleUrls: ['./gameheader.component.scss']
})
export class GameheaderComponent implements OnInit {
  @Input() currentPageLink: string='/matchmaking';
  @Input() title: string;
  AppHtmlRoutes = AppHtmlRoutes;
  constructor() { }

  ngOnInit(): void {
  }

}
