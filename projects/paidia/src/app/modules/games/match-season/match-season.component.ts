import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DatePipe } from '@angular/common';
import { Observable, Subscription } from 'rxjs';
import { formatDistanceToNowStrict } from 'date-fns';
import { Location, formatDate } from '@angular/common';
import {
  EsportsChatService,
  EsportsToastService,
  EsportsTournamentService,
  EsportsUserService,
} from 'esports';
import { TranslateService } from '@ngx-translate/core';
import {
  LadderPopupComponent,
  LadderPopupComponentData,
} from '../../../shared/popups/ladder-popup/ladder-popup.component';
import { MatDialog } from '@angular/material/dialog';
import { EsportsGameService } from 'esports';
import { GameService } from '../../../core/service/game.service';
import { environment } from '../../../../environments/environment';
const API = environment.apiEndPoint;

@Component({
  selector: 'app-match-season',
  templateUrl: './match-season.component.html',
  styleUrls: ['./match-season.component.scss'],
  providers: [DatePipe],
})
export class MatchSeasonComponent implements OnInit {
  // tableData: any[];
  //matchSeason: any;
  headerImage: string;
  endDays: Number;

  matchSeason = null;
  gameInfo = null;
  tableData = [];
  page = 1;
  seasonJoinStatus = null;
  matchFoundListener: Subscription;
  seasonId: string = null;
  showLoader: boolean;
  isAdmin = false;
  paginationData = {
    page: 1,
    limit: 50,
  };
  constructor(
    private location: Location,
    private esportsTournamentService: EsportsTournamentService,
    private gameService: EsportsGameService,
    private gameService2: GameService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private userService: EsportsUserService,
    private toastService: EsportsToastService,
    private chatService: EsportsChatService,
    private matDialog: MatDialog,
    private translateService: TranslateService
  ) {}

  ngOnInit(): void {
    this.showLoader = true;
    if (this.activatedRoute.snapshot.params.id) {
      this.seasonId = this.activatedRoute.snapshot.params.id;
      this.fetchSeasonDetails();
      this.loadLadder();
    }

    this.matchSeason = {};
    this.gameInfo = {};
    this.seasonJoinStatus = {};

    if (this.activatedRoute.snapshot.queryParamMap.get('playnow')) {
      this.openPlayNowPopup();
      this.router
        .navigateByUrl('/games', { skipLocationChange: true })
        .then(() => {
          this.router.navigate(['/games/match-season/', this.seasonId]);
        });
    }

    this.getSeasonJoinStatus();
    this.getRecentMatches();
    this.getCurrentUserDetails();
  }

  getCurrentUserDetails() {
    this.userService.currentUser.subscribe((data) => {
      if (data) {
        if (data?.accountType === 'admin') {
          this.isAdmin = true;
        }
      }
    });
  }
  async loadLadder() {
    const parms = {
      id: this.seasonId,
      page: this.page,
    };

    this.esportsTournamentService
      .getLaderStanding(API, parms)
      .subscribe((res) => {
        this.tableData = res.data.standing.docs;
        this.showLoader = false;
      });
  }

  pageChanged(page): void {
    this.paginationData.page = page;
    //this.getLogData();
  }
  reportScore() {
    let tReport: any = {};
    tReport.match = { _id: '' };
    tReport.tournament = this.seasonId;
    localStorage.setItem('t_report', JSON.stringify(tReport));
    this.router.navigate([`/report-score`]);
  }
  // seasonDetails(seasonId) {
  //   this.gameService.getSeason(seasonId).subscribe((res) => {
  //     this.matchSeason = res.data.data;
  //     var currentDate = new Date();
  //     var convertedDate = this.datePipe.transform(this.matchSeason.endDate, 'yyyy-MM-dd');
  //     var Time = new Date(convertedDate).getTime() - currentDate.getTime();
  //     var Days = Time / (1000 * 3600 * 24); //Diference in Days
  //     this.endDays = Math.round(Days);
  //   })
  // }

  fetchSeasonDetails = async () => {
    this.gameService2.fetchSeasonDetails(this.seasonId).subscribe(
      (res) => {
        const data = res['data']['data'];
        let text;
        const endDate = new Date(data.endDate);
        const startDate = new Date(data.startDate);
        const currentDate = new Date();
        if (startDate > currentDate) {
          text = `Starts ${formatDistanceToNowStrict(startDate, {
            addSuffix: true,
            unit: 'day',
          })}`;
        } else if (endDate > currentDate) {
          text = `Ends ${formatDistanceToNowStrict(endDate, {
            addSuffix: true,
            unit: 'day',
          })}`;
        } else {
          text = 'Season ended';
        }
        //  this.matchSeason = data;
        Object.assign(this.matchSeason, {
          title: data?.name,
          text: text,
          logo: data?.game.logo,
          image: data?.image,
          id: data?._id,
          participantType: data?.participantType,
          slug: data?.slug,
          description: data?.description,
        });

        Object.assign(this.gameInfo, {
          image: data?.game?.image,
          icon: data?.game?.icon,
          details: {
            platform: data?.platform?.map((ele) => ele?.name).join(', '),
            duration: `${data?.duration} days`,
            endDate: formatDate(endDate, 'dd MMM yyyy', 'en'),
            totalPlayers: data?.participantCount,
          },
        });
        this.showLoader = false;
      },
      (err) => {
        this.toastService.showError(err.error.message);
        this.router.navigateByUrl('/seasons');
      }
    );
  };

  getRecentMatches() {
    const params = {
      query: JSON.stringify({
        seasonId: this.seasonId,
        limit: 5,
        page: 1,
      }),
    };
    this.gameService2.fetchRecentSeasonMatches(params).subscribe(
      (res) => {
        this.showLoader = false;
        this.gameInfo.matches = res.data.docs;
      },
      (err) => {
        this.showLoader = false;
      }
    );
  }

  goBack() {
    this.location.back();
  }

  async getSeasonJoinStatus() {
    this.gameService2.checkSeasonJoinStatus(this.seasonId).subscribe((res) => {
      this.seasonJoinStatus = res.data;
    });
  }

  playNow() {
    if (this.seasonJoinStatus?.isRegistered) {
      this.openPlayNowPopup();
    } else {
      if (this.matchSeason.participantType === 'team') {
        this.router.navigate([
          // `/get-match/team-registration/${this.matchSeason?.slug}-${this.matchSeason?.id}`,
          `/team-registration/${this.matchSeason?.slug}-${this.matchSeason?.id}`,
        ]);
      } else {
        this.router.navigate([
          `tournament/${this.matchSeason?.slug}-${this.matchSeason?.id}/join`
          // `/matchmaking/${this.matchSeason?.slug}-${this.matchSeason?.id}`,
        ]);
      }
    }
  }

  openPlayNowPopup() {
    this.chatService.connectSocket();
    let popupData: LadderPopupComponentData = {
      isShowLoading: true,
      payload: { leftTime: 180 },
    };

    const loadDialogRef = this.matDialog.open(LadderPopupComponent, {
      data: popupData,
      disableClose: true,
    });

    this.chatService.findSeasonMatch({
      seasonId: this.seasonId,
    });

    const successObservable = new Observable((subscriber) => {
      this.chatService.listenSeasonMatchFound(subscriber);
    });

    let matchFoundSubscription = successObservable.subscribe((data) => {
      if (data) {
        loadDialogRef.close(true);
        this.toastService.showSuccess(
          this.translateService.instant('Match Found')
        );
        this.router.navigate(['get-match/game-lobby'], {
          queryParams: { matchId: data['data']['newMatch']['_id'] },
        });
      }
    });

    const errorObservable = new Observable((subscriber) => {
      this.chatService.listenFindSeasonMatchError(subscriber);
    });

    let findSeasonMatchErrorSubscription = errorObservable.subscribe(
      (error) => {
        loadDialogRef.close(true);
        if (error['errorCode']) {
          this.toastService.showError(
            'You already have 1 active match in this tournament'
          );
        } else {
          this.toastService.showError(
            this.translateService.instant('LADDER.FIND_MATCH_ERROR')
          );
        }
      }
    );

    loadDialogRef.afterClosed().subscribe((loadCloseData) => {
      this.chatService.removeFromQueue('');
      matchFoundSubscription.unsubscribe();
      findSeasonMatchErrorSubscription.unsubscribe();
      if (loadCloseData == 'timeOut') {
        const notFoundPopupRef = this.matDialog.open(LadderPopupComponent, {
          data: { isShowNotFoundSeason: true },
        });
        notFoundPopupRef.afterClosed().subscribe((action) => {
          if (action == 'searchAgain') {
            this.openPlayNowPopup();
          }
        });
      }
    });
  }
}
