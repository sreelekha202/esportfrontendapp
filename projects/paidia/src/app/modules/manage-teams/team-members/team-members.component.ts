import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AppHtmlManageTeams, AppHtmlProfileRoutes } from '../../../app-routing.model';
import { AppHtmlRoutes } from '../../../app-routing.model';
import { TeamService } from '../../../core/service/team.service';
import { EsportsUserService } from 'esports';
@Component({
  selector: 'app-team-members',
  templateUrl: './team-members.component.html',
  styleUrls: ['./team-members.component.scss']
})
export class TeamMembersComponent implements OnInit , OnDestroy{
  AppHtmlRoutes = AppHtmlRoutes;
  AppHtmlProfileRoutes=AppHtmlProfileRoutes;
  AppHtmlManageTeams = AppHtmlManageTeams;
  active = 1;
  nextId: number = 1;
  myTeams = [];
  teamRequests = [];
  showLoader: boolean ;
  id : any;
  currentUserRole;
  constructor(private teamService : TeamService,private _activateRoute : ActivatedRoute, private userService: EsportsUserService) { }
  currentUserId;
  subscriptions = [];

  ngOnInit(): void {
    let userSub = this.userService.currentUser.subscribe(val=>{
      this.currentUserId = val?._id;
    });
    this.subscriptions.push(userSub);
    // let id = this._activateRoute.snapshot.paramMap.get('id');
    this._activateRoute.paramMap.subscribe((params)=>{
        this.id = params.get('id');
    })
    this.teamService.getTeamMemberList(this.id).subscribe((res : any)=>{
      // this.myTeams = res;
      this.myTeams = [];
       for (let d of res?.data) {
        let d1 = {
          image: d?.userId?.profilePicture,
          name: d?.userId?.fullName,
          role : d?.role,
          username:d?.userId?.username,
          userId:d?.userId?._id
        }

        if (this.currentUserId == d1?.userId) {
          this.currentUserRole = d1?.role;
        }
        this.myTeams.push(d1)
      }
    })
  }


  ngOnDestroy() {
    this.subscriptions.forEach(element => {
      if (element) {
        element.unsubscribe();
      }
    });
  }

}
