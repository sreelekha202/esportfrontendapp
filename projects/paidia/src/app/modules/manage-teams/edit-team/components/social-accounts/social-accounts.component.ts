import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  SimpleChanges,
  ViewEncapsulation,
} from '@angular/core';
import { FormControl } from '@angular/forms';
import { TeamService } from '../../../../../core/service/team.service';

@Component({
  selector: 'app-social-accounts',
  templateUrl: './social-accounts.component.html',
  styleUrls: ['./social-accounts.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class SocialAccountsComponent implements OnInit {
  // accounts = [];
  // @Input() accountsVaue = [];
  // @Input() twitter: any = '';
  // @Input() facebook: any = '';
  // @Input() twitch: any = '';
  @Output() changeTwitch = new EventEmitter();
  @Output() changeFacebook = new EventEmitter();
  @Output() changeTwitter = new EventEmitter();
  // constructor() { }

  // t = {
  //   icon: 'https://i115.fastpic.ru/big/2021/0630/8a/fbd83ffaa866e9861c383efbe9601a8a.png',
  //   label: 'Twitter',
  //   value: ''
  // }
  // f = {
  //   icon: 'https://i115.fastpic.ru/big/2021/0630/8d/fd933c6c38c0673ad6845233f677618d.png',
  //   label: 'Facebook',
  //   value: ''
  // }
  // Twitch = {
  //   icon: 'https://i115.fastpic.ru/big/2021/0630/65/7a457917ed0aef758a736f3a23594d65.png',
  //   label: 'Twitch',
  //   value: ''
  // }

  // ngOnInit(): void {
  //   this.accounts = [];
  // }

  // ngOnChanges(changes: SimpleChanges) {
  //   for (let propName in changes) {
  //     let chng = changes[propName];
  //     if (propName == 'twitter' && chng.currentValue) {
  //       this.t['value'] = chng.currentValue;
  //       this.addfunction();
  //       // let prev = JSON.stringify(chng.previousValue);
  //     }
  //     if (propName == 'facebook' && chng.currentValue) {
  //       this.f['value'] = chng.currentValue;
  //       this.addfunction1();
  //     }
  //     if (propName == 'twitch' && chng.currentValue) {
  //       this.Twitch['value'] = chng.currentValue;
  //       this.addfunction2();
  //     }
  //   }
  // }

  // addfunction5() {
  //   if (this.accounts.length == 0) {
  //     this.addfunction();
  //   } else if (this.accounts.length == 1) {
  //     this.addfunction1();
  //   } else if (this.accounts.length == 2) {
  //     this.addfunction2();
  //   }
  // }

  // addfunction() {
  //   if (this.accounts.length == 0) {
  //     this.accounts.push(this.t)
  //   }
  // }
  // addfunction1() {
  //   if (this.accounts.length == 1) {
  //     this.accounts.push(this.f)
  //   }
  // }
  // addfunction2() {
  //   if (this.accounts.length == 2) {
  //     this.accounts.push(this.Twitch)
  //   }
  // }
  // remove(a){
  //   var index = this.accounts.indexOf(a);
  //   this.accounts.splice(0,1)
  //  }

  onChnage(data) {
    if (data.label == 'Twitch') {
      this.changeTwitch.emit(data?.value);
    }
    if (data.label == 'Facebook') {
      this.changeFacebook.emit(data?.value);
    }
    if (data.label == 'Twitter') {
      this.changeTwitter.emit(data?.value);
    }
  }

  accounts = [];
  isTwitter = false;
  isFacebook = false;
  isTwitch = false;
  isYoutube = false;
  isDiscord = false;
  selectedAccounts: any = [];
  accounts2 = new FormControl();
  accounts2List: string[] = [
    'Twitter',
    'Facebook',
    'Twitch',
    'Youtube',
    'Discord',
  ];
  addPlatform = [];

  constructor(private teamService: TeamService) {}

  t = {
    icon: 'https://i115.fastpic.ru/big/2021/0630/8a/fbd83ffaa866e9861c383efbe9601a8a.png',
    value: ' ',
    label: 'Twitter',
  };
  f = {
    icon: 'https://i115.fastpic.ru/big/2021/0630/8d/fd933c6c38c0673ad6845233f677618d.png',
    value: ' ',
    label: 'Facebook',
  };
  Twitch = {
    icon: 'https://i115.fastpic.ru/big/2021/0630/65/7a457917ed0aef758a736f3a23594d65.png',
    value: ' ',
    label: 'Twitch',
  };
  y = {
    icon: 'https://i115.fastpic.ru/big/2021/0630/8a/fbd83ffaa866e9861c383efbe9601a8a.png',
    value: ' ',
    label: 'Youtube',
  };
  d = {
    icon: 'https://i115.fastpic.ru/big/2021/0630/8a/fbd83ffaa866e9861c383efbe9601a8a.png',
    value: ' ',
    label: 'Discord',
  };

  ngOnInit(): void {
    // MOCK DATA
    // this.accounts = []
    this.teamService.socialFiledSubject$.subscribe((val) => {
      if (val) {
        if (val) {
          if (val.hasOwnProperty('facebook')) {
            this.isFacebook = true;
            this.selectedAccounts.push(this.accounts2List[1]);
            this.accounts.push({ ...this.f, value: val['facebook'] });
          }
          if (val.hasOwnProperty('twitter')) {
            this.isTwitter = true;
            this.selectedAccounts.push(this.accounts2List[0]);
            this.accounts.push({ ...this.t, value: val['twitter'] });
          }
          if (val.hasOwnProperty('twitch')) {
            this.isTwitch = true;
            this.selectedAccounts.push(this.accounts2List[2]);
            this.accounts.push({ ...this.Twitch, value: val['twitch'] });
          }
          this.addPlatforms();
        }
      }
    });
  }

  addPlatforms() {
    this.addPlatform = this.accounts;
  }

  addsocial(a, i) {
    if (a == 'Twitter') {
      if (this.isTwitter) {
        this.isTwitter = false;
        this.accounts = this.accounts.filter((item) => item.label !== a);
      } else {
        this.isTwitter = true;
        this.accounts.push(this.t);
      }
    }

    if (a == 'Facebook') {
      if (this.isFacebook) {
        this.isFacebook = false;
        this.accounts = this.accounts.filter((item) => item.label !== a);
      } else {
        this.accounts.push(this.f);
        this.isFacebook = true;
      }
    }
    if (a == 'Twitch') {
      if (this.isTwitch) {
        this.isTwitch = false;
        this.accounts = this.accounts.filter((item) => item.label !== a);
      } else {
        this.isTwitch = true;
        this.accounts.push(this.Twitch);
      }
    }

    if (a == 'Youtube') {
      if (this.isYoutube) {
        this.isYoutube = false;
        this.accounts = this.accounts.filter((item) => item.label !== a);
      } else {
        this.isYoutube = true;
        this.accounts.push(this.y);
      }
    }

    if (a == 'Discord') {
      if (this.isDiscord) {
        this.isDiscord = false;
        this.accounts = this.accounts.filter((item) => item.label !== a);
      } else {
        this.isDiscord = true;
        this.accounts.push(this.d);
      }
    }
  }

  remove(a, i) {
    if (a == 'Twitter') {
      if (this.isTwitter) {
        this.isTwitter = false;
        this.selectedAccounts = this.selectedAccounts.filter(
          (item) => item !== a
        );
        this.accounts.splice(i, 1);
      }
    }
    if (a == 'Facebook') {
      if (this.isFacebook) {
        this.isFacebook = false;
        this.selectedAccounts = this.selectedAccounts.filter(
          (item) => item !== a
        );
        this.accounts.splice(i, 1);
      }
    }

    if (a == 'Twitch') {
      if (this.isTwitch) {
        this.isTwitch = false;
        this.selectedAccounts = this.selectedAccounts.filter(
          (item) => item !== a
        );
        this.accounts.splice(i, 1);
      }
    }

    if (a == 'Youtube') {
      if (this.isYoutube) {
        this.isYoutube = false;
        this.selectedAccounts = this.selectedAccounts.filter(
          (item) => item !== a
        );
        this.accounts.splice(i, 1);
      }
    }

    if (a == 'Discord') {
      if (this.isDiscord) {
        this.isDiscord = false;
        this.selectedAccounts = this.selectedAccounts.filter(
          (item) => item !== a
        );
        this.accounts.splice(i, 1);
      }
    }
  }
}
