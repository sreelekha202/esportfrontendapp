import {
  Component,
  OnInit,
  Input,
  SimpleChanges,
  Output,
  EventEmitter,
} from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { EsportsToastService } from 'esports';

interface AccountItem {
  icon: string;
  label: string;
  value: any;
}

@Component({
  selector: 'app-account-item',
  templateUrl: './account-item.component.html',
  styleUrls: ['./account-item.component.scss'],
})
export class AccountItemComponent implements OnInit {
  // @Input() data: AccountItem;
  @Output() chnage = new EventEmitter();
  // @Output() removesocialmedia = new EventEmitter();
  // value = '';
  // constructor() { }

  // ngOnInit(): void { }

  onInput(value) {
    this.data.value = value;
    this.chnage.emit(this.data);
  }
  // removeSocial(a){
  //   this.value='';
  //   this.removesocialmedia.emit(a)
  // }
  // ngOnChanges(simpleChanges: SimpleChanges) { }

  @Input() data: AccountItem;
  @Input() isSocial: boolean = false;
  @Output() removesocialmedia = new EventEmitter();
  value = '';
  form = this.formBuilder.group({
    firstInput: ['', Validators.required],
  });
  constructor(
    private formBuilder: FormBuilder,
    public toastService: EsportsToastService
  ) {}

  ngOnInit(): void {}
  //this.toastService.showError(
  //     'You need to be at least 10 years old to register on this platform.'
  //   );
  savevalue(data: any, d: any) {
    if (d.label == 'Twitter') {
      sessionStorage.setItem('link', data.target.value);
    }
    if (d.label == 'Facebook') {
      sessionStorage.setItem('Fblink', data.target.value);
    }
    if (d.label == 'Twitch') {
      sessionStorage.setItem('twlink', data.target.value);
    }

    if (d.label == 'Youtube') {
      sessionStorage.setItem('youtubelink', data.target.value);
    }

    if (d.label == 'Discord') {
      sessionStorage.setItem('discordlink', data.target.value);
    }
  }

  // savevalue(data){
  //   sessionStorage.setItem("link",data.target.value)
  // }
  removeSocial(a) {
    this.value = '';
    this.removesocialmedia.emit(a);
  }
}
