import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AppHtmlProfileRoutes } from '../../../app-routing.model';
import { AppHtmlRoutes } from '../../../app-routing.model';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { TeamService } from '../../../core/service/team.service';
import { Location } from '@angular/common';
import { environment } from '../../../../environments/environment';
import {
  EsportsUserService,
  EsportsToastService,
  IUser,
  EsportsGtmService,
  EventProperties,
} from 'esports';
import { Subscription } from 'rxjs';
const API = environment.apiEndPoint;
@Component({
  selector: 'app-edit-team',
  templateUrl: './edit-team.component.html',
  styleUrls: ['./edit-team.component.scss'],
})
export class EditTeamComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;
  AppHtmlProfileRoutes = AppHtmlProfileRoutes;
  teamId: any;
  form: FormGroup;
  base64textString: string;
  selectedFile: any;
  editImageUrl: any = null;
  clicked: boolean = false;
  twitte: any = '';
  fb: any = '';
  tw: any = '';
  currentUser: IUser;
  userSubscription: Subscription;
  teamData: any = {};
  selectedAccounts: any = [];

  constructor(
    private _activatedRoute: ActivatedRoute,
    private teamService: TeamService,
    private eSportsToastService: EsportsToastService,
    public toastService: EsportsToastService,
    private location: Location,
    private gtmService: EsportsGtmService,
    private userService: EsportsUserService
  ) {
    this.form = new FormGroup({
      email: new FormControl([]),
      member: new FormControl([]),
      shortDescription: new FormControl([]),
      social: new FormControl({}),
      teamName: new FormControl('', [Validators.required]),
      logo: new FormControl(''),
      teamBanner: new FormControl(''),
    });
  }
  ngOnInit(): void {
    this._activatedRoute.paramMap.subscribe((params) => {
      this.teamId = params.get('id');
    });

    this.getUserData();
    this.teamService.getEditTeamAutoFill(this.teamId).subscribe((res: any) => {
      this.teamData = res.data[0];
      this.teamService.socialFiledSubject$.next(this.selectedAccounts);
      this.editImageUrl = this.teamData.logo;
      this.twitte = this.teamData.social['twitter'];
      this.fb = this.teamData.social['facebook'];
      this.tw = this.teamData.social['twitch'];
      this.setSocialData();
      this.form.patchValue({
        // email: teamData.email,
        member: this.teamData.member,
        shortDescription: this.teamData.shortDescription,
        social: this.teamData.social,
        teamName: this.teamData.name,
        logo: this.teamData.logo,
        teamBanner: this.teamData.teamBanner,
      });
    });
  }
  setSocialData() {
    this.teamService.socialFiledSubject$.next(this.teamData.social);
  }

  getUserData() {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.currentUser = data;
      }
    });
  }

  fileChangeEvent(fileInput: any) {
    const reg = /(.*?)\.(jpg|jpeg|png|gif|giff)$/;
    if (!fileInput.target.files[0].name.match(reg)) {
      this.toastService.showError('Please select valid file');
      this.removeFile();
      return false;
    } else {
      this.removeFile();
      this.selectedFile = fileInput.target.files[0];
      var reader = new FileReader();
      reader.onload = this.handleReaderLoaded.bind(this);
      reader.readAsBinaryString(this.selectedFile);
    }
  }

  handleReaderLoaded(readerEvt) {
    var binaryString = readerEvt.target.result;
    this.base64textString = 'data:image/png;base64,' + btoa(binaryString);
    this.form.get('logo').setValue(this.base64textString);
  }
  removeFile() {
    this.selectedFile = null;
  }

  updateTeam() {
    this.clicked = true;
    let team: any = {
      admin: true,
      id: this.teamId,
      email: [],
      member: this.form.value.member,
      query: {
        condition: { _id: this.teamId },
        update: {
          shortDescription: this.form.value.shortDescription,
          social: this.form.value.social,
          teamName: this.form.value.teamName,
          logo: this.form.value.logo,
          teamBanner: this.form.value.teamBanner,
        },
      },
      userName: this.currentUser.fullName,
    };
    this.userService.team_update(API, team).subscribe(
      (res) => {
        this.clicked = false;
        if (res.data) {
          this.pushGTMTags('Edit_Team_Complete');
          this.eSportsToastService.showSuccess(res.message);
          this.cancel();
        } else {
          this.eSportsToastService.showError(res.message);
        }
      },
      (err) => {
        this.clicked = false;
        this.eSportsToastService.showError(err.error.message);
      }
    );
  }

  cancel() {
    this.pushGTMTags('Edit_Team_Cancellation');
    this.location.back();
  }
  changesTwitch(data) {
    this.tw = this.teamData.social['twitch'] = data;
    this.form.patchValue({
      social: { ...this.form.value.social, twitch: this.tw },
    });
    if (this.checkString(this.twitte)) {
      this.form.patchValue({
        social: { ...this.form.value.social, twitch: this.tw },
      });
    } else {
      let jsonObject = this.form.value.social;
      delete jsonObject['twitch'];
      this.form.patchValue({ social: jsonObject });
    }
  }
  changesFacebook(data) {
    this.fb = this.teamData.social['facebook'] = data;
    this.form.patchValue({
      social: { ...this.form.value.social, facebook: this.fb },
    });
    if (this.checkString(this.fb)) {
      this.form.patchValue({
        social: { ...this.form.value.social, facebook: this.fb },
      });
    } else {
      let jsonObject = this.form.value.social;
      delete jsonObject['facebook'];
      this.form.patchValue({ social: jsonObject });
    }
  }
  changesTwitter(data) {
    this.twitte = this.teamData.social['twitter'] = data;
    if (this.checkString(this.twitte)) {
      this.form.patchValue({
        social: { ...this.form.value.social, twitter: this.twitte },
      });
    } else {
      let jsonObject = this.form.value.social;
      delete jsonObject['twitter'];
      this.form.patchValue({ social: jsonObject });
    }
  }

  checkString(myString) {
    if (/\S/.test(myString)) return true;
    return false;
  }

  pushGTMTags(eventName, eventData: EventProperties = null) {
    this.gtmService.pushGTMTags(eventName, eventData);
  }
}
