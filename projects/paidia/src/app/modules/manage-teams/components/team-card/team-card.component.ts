import {
  Component,
  OnInit,
  Input,
  PLATFORM_ID,
  Inject,
  Output,
  EventEmitter,
} from '@angular/core';
import { Subscription } from 'rxjs';
import {
  AppHtmlProfileRoutes,
  AppHtmlRoutes,
} from '../../../../app-routing.model';
import { AppHtmlRoutesLoginType } from '../../../../app-routing.model';
import {
  EsportsUserService,
  EsportsToastService,
  EsportsLeaderboardService,
  EsportsChatService,
  EsportsConstantsService,
  GlobalUtils,
  IUser,
  EsportsGtmService,
  SuperProperties,
  EventProperties,
} from 'esports';
import { environment } from '../../../../../environments/environment';
const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import {
  faBars,
  faTimes,
  faSearch,
  faBell,
} from '@fortawesome/free-solid-svg-icons';
import { MatDialog } from '@angular/material/dialog';
import { isPlatformBrowser } from '@angular/common';

@Component({
  selector: 'app-team-card',
  templateUrl: './team-card.component.html',
  styleUrls: ['./team-card.component.scss'],
})
export class TeamCardComponent implements OnInit {
  @Input() isManage: boolean = false;
  @Input() isStatus: boolean = false;
  @Input() teamId;
  @Input() data: any;
  @Input() currentUserRole: string;
  @Output() refresh = new EventEmitter();
  activeLang: string = this.constantsService?.defaultLangCode;
  AppHtmlProfileRoutes = AppHtmlProfileRoutes;
  AppHtmlRoutes = AppHtmlRoutes;
  AppHtmlRoutesLoginType = AppHtmlRoutesLoginType;

  collapsed: boolean = true;
  isAdmin: boolean = false;
  isBrowser: boolean;
  isUserLoggedIn: boolean = false;
  mobMenuOpened: boolean = false;
  mobMenuOpeneds: boolean = false;
  showLoader: boolean = false;
  currentUser: IUser;
  tournaments: any;
  matchlist: any;
  matchcount: number;
  chatWins = [];
  currentUserId: any;
  AppLanguage = [];
  faBell = faBell;
  faSearch = faSearch;
  faBars = faBars;
  faTimes = faTimes;
  private sub1: Subscription;
  userSubscription: Subscription;
  windowposition: String = 'chat_window chat_window_right_drawer';
  showChat: boolean = false;
  matchdetails: any;
  typeofchat = 'user';
  followStatus: any;
  teamManageData: any;

  constructor(
    public chatService: EsportsChatService,
    @Inject(PLATFORM_ID) private platformId,
    // private elementRef: ElementRef,
    private leaderboardService: EsportsLeaderboardService,
    // private router: Router,
    private userService: EsportsUserService,
    private toastService: EsportsToastService,
    private gtmService: EsportsGtmService,
    private constantsService: EsportsConstantsService,
    public translate: TranslateService,
    private router: Router,
    public matDialog: MatDialog
  ) {
    this.isBrowser = isPlatformBrowser(this.platformId);
  }

  ngOnInit(): void {
    this.teamManageData = this.data;
    this.matchlist = [];
    this.activeLang = this.translate.currentLang;
    this.AppLanguage = this.constantsService?.language;
    this.matchcount = 0;

    if (
      this.isBrowser &&
      GlobalUtils.isBrowser() &&
      localStorage.getItem(environment.currentToken)
    ) {
      this.checkFollowStatus();
      this.userSubscription = this.userService.currentUser.subscribe((data) => {
        if (data) {
          this.currentUser = data;
          this.currentUserId = this.currentUser._id;
          this.isAdmin = this.currentUser?.accountType == "admin";
        }
      });
    }
  }

  pushGTMTags(eventName: string, eventProps: EventProperties = null) {
    this.gtmService.pushGTMTags(eventName, eventProps);
  }

  public toggleChat(chatwindow) {
    this.pushGTMTags('Chat_Opened', { fromPage: this.router.url });
    this.matchdetails = chatwindow.userId + '-' + this.currentUserId;
    this.showChat = true;
    this.chatService.setWindowPos(this.windowposition);
    this.chatService.setCurrentMatch(this.matchdetails);
    this.chatService.setTypeOfChat(this.typeofchat);
    this.chatService.setChatStatus(true);
    this.chatService.setTextButtonStatus(true);
  }

  followUser(id) {
    if (this.data) {
      if (this.followStatus == 'follow') {
        this.leaderboardService.followUser(API, this.data.userId).subscribe(
          (res: any) => {
            this.followStatus = 'unfollow';
            this.checkFollowStatus();
            this.toastService.showSuccess(res.message);
            this.userService.refreshCurrentUser(API, TOKEN);
          },
          (err) => {
            // this.showLoader = false;
          }
        );
      } else if (this.followStatus == 'unfollow') {
        this.leaderboardService.unfollowUser(API, this.data.userId).subscribe(
          (res: any) => {
            this.followStatus = 'follow';
            this.checkFollowStatus();
            this.toastService.showSuccess(res.message);
            this.userService.refreshCurrentUser(API, TOKEN);
          },
          (res: any) => {
            // this.showLoader = false;
          }
        );
      }
    } else {
      // Navigate to the login page with extras
      //this.router.navigate(['/user/email-login']);
    }
  }

  checkFollowStatus() {
    this.leaderboardService.checkFollowStatus(API, this.data.userId).subscribe(
      (res: any) => {
        if (res.data[0]?.status == 1) {
          this.followStatus = 'unfollow';
        } else if (res.data[0]?.status == 0) {
          this.followStatus = 'follow';
        }
      },
      (err) => {
        // this.showLoader = false;
      }
    );
  }

  ngOnDestroy() {
    this.userSubscription?.unsubscribe();
    if (this.currentUserId) {
      this.chatService?.disconnectUser();
    }
  }

  deleteTeamMember(player, status) {
    this.pushGTMTags('Remove_Player_Clicked')
    let data = {
      teamId: this.teamId,
      userId: player.userId,
      name: player.name,
      status: status,
    };

    this.userService.update_member(API, data).subscribe(
      (data) => {
        this.toastService.showSuccess(data?.message);
        this.router
          .navigateByUrl('/profile/teams', { skipLocationChange: true })
          .then(() => {
            this.router.navigate(['/manage-team/team-members', this.teamId]);
            //  this.router.navigate(['profile/my-teams']);
          });
      },
      (error) => {}
    );
  }

  async changeRole(player, role) {
    this.pushGTMTags('Make_Captain_Clicked')
    await this.userService
      .update_member(API, {
        teamId: this.teamId,
        userId: player.userId,
        role: role,
        name: player.fullName,
      })
      .subscribe(
        (data) => {
          this.toastService.showSuccess(data?.message);
          this.router
            .navigateByUrl('/profile/teams', { skipLocationChange: true })
            .then(() => {
              this.router.navigate(['/manage-team/team-members', this.teamId]);
            });
        },
        (error) => {}
      );
  }

  makeOwner(info) {
    const teamObj = {
      id: this.teamId,
      admin: true,
      userId: info.userId,
      action: 'owner change',
      query: {
        condition: { _id: this.teamId },
        update: { teamCreator: info.userId },
      },
    };
    this.userService.update_team_owner(API, teamObj).subscribe(
      (res) => {
        this.router
          .navigateByUrl('/profile/teams', { skipLocationChange: true })
          .then(() => {
            this.router.navigate(['/manage-team/team-members', this.teamId]);
          });
      },
      (err) => {
        this.toastService.showError(err.error.message);
      }
    );
  }

  async changeStatus(player, status) {}
}
