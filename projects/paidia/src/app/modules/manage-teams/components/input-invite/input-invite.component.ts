import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { EsportsToastService } from 'esports';
import { TranslateService } from "@ngx-translate/core";
@Component({
  selector: 'app-input-invite',
  templateUrl: './input-invite.component.html',
  styleUrls: ['./input-invite.component.scss'],
})
export class InputInviteComponent implements OnInit {
  @Output() emitValue = new EventEmitter();

  timeoutId = null;
  emailInvitation = [];
  singleEmail;

  get email(): any {
    // return this.form.get("email");
    return this.singleEmail;
  }
  constructor( public toastService: EsportsToastService,
     private translateService: TranslateService,) {}

  ngOnInit(): void {}

  onChangeValue(event): void {
    const valid = event?.target?.validity?.valid;
    const value = event?.target?.value;

    const emitValue = () => {
      if (!value || !valid) return;
      this.emitValue.emit(event?.target?.value.trim());
      event.target.value = '';
    };

    clearTimeout(this.timeoutId);

    this.timeoutId = setTimeout(emitValue, 1000);
  }

  validateEmail(email) {
    const regularExpression = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (!regularExpression.test(String(email).toLowerCase())) {
      return 0;
    }
    return 1;
  }
  removeEmail(index) {
    this.emailInvitation.splice(index, 1);
  }
  addEmails() {
    if (this.validateEmail(this.email)) {
      if (this.emailInvitation.indexOf(this.email) == -1) {
        this.emailInvitation.push(this.email);
        this.emitValue.emit(this.emailInvitation);
      } else {
        this.toastService.showError(
          this.translateService.instant('Duplicate invite by email address')
        );
      }
    } else {
      this.toastService.showError(
        this.translateService.instant('Invite by email address is not a valid email address')
      );
    }
    this.singleEmail = "";
  }

}
