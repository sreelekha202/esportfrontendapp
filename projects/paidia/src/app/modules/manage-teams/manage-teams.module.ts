import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ManageTeamsComponent } from './manage-teams.component';
import { Routes, RouterModule } from '@angular/router';
import { TeamMembersComponent } from './team-members/team-members.component';
import { InvitesSentComponent } from './invites-sent/invites-sent.component';
import { EditTeamComponent } from './edit-team/edit-team.component';
import { AddMemberComponent } from './add-member/add-member.component';
import { TeamCardComponent } from './components/team-card/team-card.component';
import { RouterBackModule } from './../../shared/directives/router-back.module';
import { HeaderPaidiaModule } from "../../shared/components/header-paidia/header-paidia.module";
import { InputSearchComponent } from './edit-team/components/input-search/input-search.component';
import { TeamIconComponent } from './edit-team/components/team-icon/team-icon.component';
import { TeamBannerComponent } from './edit-team/components/team-banner/team-banner.component';
import { AccountItemComponent } from './edit-team/components/account-item/account-item.component';
import { InputInviteComponent } from './components/input-invite/input-invite.component';
import { ManageteamHeaderComponent } from './manageteam-header/manageteam-header.component';
import { SharedModule } from '../../shared/modules/shared.module';
import { SocialAccountsComponent } from './edit-team/components/social-accounts/social-accounts.component';
import { CoreModule } from '../../core/core.module';
import { FormsModule } from '@angular/forms';

export const routes: Routes = [
  {
    path: '',
    component: ManageTeamsComponent,
    children: [
      {
        path: '',
        redirectTo: 'team-members',
        pathMatch: 'full',
      },
      {
        path: 'team-members/:id',
        component: TeamMembersComponent,
      },
      {
        path: 'invites-sent/:id',
        component: InvitesSentComponent,
      },
      {
        path: 'edit-team/:id',
        component: EditTeamComponent,
      },
      {
        path: 'add-members/:id',
        component: AddMemberComponent,
      },

    ]
  }
]

@NgModule({
  declarations: [
    ManageTeamsComponent,
    TeamMembersComponent,
    InvitesSentComponent,
    EditTeamComponent,
    AddMemberComponent,
    TeamCardComponent,
    InputSearchComponent,
    SocialAccountsComponent,
    AccountItemComponent,
    TeamIconComponent,
    TeamBannerComponent,
    InputInviteComponent,
    ManageteamHeaderComponent,

  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule,
    CoreModule,
    FormsModule,
    HeaderPaidiaModule,
    RouterBackModule
  ]
})
export class ManageTeamsModule { }
