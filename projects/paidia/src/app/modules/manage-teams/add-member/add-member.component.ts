import { Component, OnInit } from '@angular/core';
import { AppHtmlManageTeams } from '../../../app-routing.model';
import { AppHtmlRoutes } from '../../../app-routing.model';
import { Router } from '@angular/router';
import { environment } from '../../../../environments/environment';
import { TeamService } from '../../../core/service/team.service';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { TranslateService } from '@ngx-translate/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {
  IUser,
  EsportsUserService,
  GlobalUtils,
  EsportsToastService,
} from 'esports';

const API = environment.apiEndPoint;

@Component({
  selector: 'app-add-member',
  templateUrl: './add-member.component.html',
  styleUrls: ['./add-member.component.scss'],
})
export class AddMemberComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;
  members = [];
  teamMeamber=[];
  teamId: any;
  form: FormGroup;
  myTeams = [];
  emailInvitation: any = [];
  currentUser: IUser;
  team: any;
  membersObj;
  searchText;
  memberid:any;
  select: boolean;
  constructor(
    private userService: EsportsUserService,
    public eSportsToastService: EsportsToastService,
    private _activateRoute: ActivatedRoute,
    private teamService: TeamService,
    private router: Router,
    private location: Location,
    private translateService: TranslateService,
  ) { }
  AppHtmlManageTeams = AppHtmlManageTeams;

  ngOnInit(): void {
    this._activateRoute.paramMap.subscribe((params) => {
      this.teamId = params.get('id');
    });
    this.getUserData();
    this.teamService.getEditTeamAutoFill(this.teamId).subscribe((res: any) => {
      let teamData = res.data[0];
      this.members = res.data[0].member
      this.memberid = res.data[0]._id
      let team: any = {
        admin: true,
        email: this.emailInvitation,
        id: this.teamId,
        member: this.members,
        query: {
          condition: { _id: this.teamId },
          update: {
            logo: teamData.logo,
            shortDescription: teamData.shortDescription,
            social: {},
            teamName: teamData.name,
          },
        },
        userName: this.currentUser.fullName,
      };
      this.team = team
    });
  }

  getUserData() {
    this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.currentUser = data;
      }
    });
  }

  submitData(player) {
    this.teamMeamber.push(player)
    let noDublicates=  this.removeDuplicates(this.teamMeamber, '_id')
     this.teamMeamber=noDublicates
    // this.members.forEach(el =>{
    // if(el._id == player._id){
    //   this.select=false
    //   this.eSportsToastService.showError("already exists");
    // }else{
    //   this.select=!this.select
    //   this.teamMeamber.push(player)
    //    let noDublicates=  this.removeDuplicates(this.members, '_id')
    //    this.members=noDublicates;
    // }
    // })
    // this.members.push(player)

  }

  removeDuplicates = (array, key) => {
    return array.reduce((arr, item) => {
      const removed = arr.filter(i => i[key] !== item[key]);
      return [...removed, item];
    }, []);
  };
  
  emitValue(emailInvitation) {
    this.emailInvitation = emailInvitation;
  }

  submit() {
    this.teamMeamber.forEach(member => {
      this.team.member.push(member)
    });
      this.userService.team_update(API, this.team).subscribe(
        (res) => {
          if (res.data) {
            this.eSportsToastService.showSuccess(res.message);
            this.location.back()
          } else {
            this.eSportsToastService.showError(
              this.translateService.instant('PROFILE.TEAMS.ERRORS.ERROR4')
            );
          }
        },
        (err) => {
          this.eSportsToastService.showError(err.error.message);
        }),
      (err) => {
        this.eSportsToastService.showError(err.error.message);
      }

  }

  addMembers(player) {
    setTimeout(() => {
      if (this.membersObj && typeof this.membersObj == 'object') {
        if (
          !this.isMembersAlreadyAdded(
            this.members,
            this.memberid
          )
        ) {
          if (this.memberid != this.currentUser._id) {
            this.members.push(player);
          } else {
            this.eSportsToastService.showError(
              this.translateService.instant('PROFILE.TEAMS.ERRORS.ERROR4')
            );
          }
        } else {
          this.eSportsToastService.showError(
            this.translateService.instant('PROFILE.TEAMS.ERRORS.ERROR3')
          );
        }
        this.membersObj = '';
        this.searchText = '';
      }
    }, 100);
  }

  isMembersAlreadyAdded(arr, _id) {
    const { length } = arr;
    const id = length + 1;
    const found = arr.some((el) => el?._id === _id);
    return found;
  }
}
