import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import {
  AppHtmlManageTeams,
  AppHtmlProfileRoutes,
} from '../../app-routing.model';
import { AppHtmlRoutes } from '../../app-routing.model';
import { TeamService } from 'projects/paidia/src/app/core/service/team.service';

@Component({
  selector: 'app-manage-teams',
  templateUrl: './manage-teams.component.html',
  styleUrls: ['./manage-teams.component.scss'],
})
export class ManageTeamsComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;
  teamId: any;
  logoData: any;
  teamDetails: any;
  constructor(private teamService: TeamService) {}
  AppHtmlManageTeams = AppHtmlManageTeams;
  AppHtmlProfileRoutes = AppHtmlProfileRoutes;
  ngOnInit(): void {
    this.teamId = String(window.location.href).split('/').reverse()[0];
    this.teamService.getEditTeamAutoFill(this.teamId).subscribe((res: any) => {
      this.teamDetails = res.data[0];
    });
  }
}
