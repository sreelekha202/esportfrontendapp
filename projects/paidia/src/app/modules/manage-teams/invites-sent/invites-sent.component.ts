import { Component, OnInit } from '@angular/core';
import { AppHtmlProfileRoutes } from '../../../app-routing.model';
import { AppHtmlRoutes } from '../../../app-routing.model';
import { ProfileService } from '../../../core/service/profile.service';
import { TeamService } from '../../../core/service/team.service';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-invites-sent',
  templateUrl: './invites-sent.component.html',
  styleUrls: ['./invites-sent.component.scss'],
})
export class InvitesSentComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;
  showLoader: boolean = true;
  InvitesSent = [];
  totalInvitesSent = [];
  teamId;
  constructor(
    private profileService: ProfileService,
    private teamService: TeamService,
    private route: ActivatedRoute
  ) {}
  AppHtmlProfileRoutes = AppHtmlProfileRoutes;
  ngOnInit(): void {
    this.route.params.subscribe((params) => {
      this.teamId = params['id'];
    });
    this.getMyInvite();
  }

  getMyInvite() {
    this.showLoader = true;
    const params: any = {
      page: 1,
      limit: 5,
      sort: {
        _id: -1,
      },
    };
    this.teamService.getEditTeamAutoFill(this.teamId).subscribe((res: any) => {
      if (res && res.data && res.data[0] && res.data[0].member) {
        let b = [];
        res.data[0].member.map((invite) => {
          invite.status == 'invite_pending' ? b.push(invite) : '';
        });
        this.totalInvitesSent = b;
        this.InvitesSent = res.data[0].member;
      }
    });
  }
}
