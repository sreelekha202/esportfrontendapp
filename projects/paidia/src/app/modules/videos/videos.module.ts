import { VideosRoutingModule } from "./videos-routing.module";
import { CoreModule } from "../../core/core.module";
import { NgModule } from "@angular/core";
import { VideoViewComponent } from "./video-view/video-view.component";
import { VideosComponent } from "./videos.component";
import { SharedModule } from "../../shared/modules/shared.module";

@NgModule({
  declarations: [
    VideosComponent,
    VideoViewComponent
  ],
  imports: [CoreModule, VideosRoutingModule, SharedModule],
  providers: [],
})
export class VideosModule {}
