import { Component, Inject, OnInit, PLATFORM_ID } from '@angular/core';
import { AppHtmlRoutes } from '../../app-routing.model';
import { ActivatedRoute, Router } from '@angular/router';
import { TournamentService } from '../../core/service/tournament.service';
import { Subscription } from 'rxjs';
import { isPlatformBrowser } from '@angular/common';
import {
  EsportsChatService,
  EsportsTimezone,
  EsportsToastService,
  EsportsTournamentService,
  EsportsUserService,
  GlobalUtils,
  IUser,
  EsportsGtmService,
  EventProperties,
} from 'esports';
import { TranslateService } from '@ngx-translate/core';
@Component({
  selector: 'app-manage-tournamens',
  templateUrl: './manage-tournamens.component.html',
  styleUrls: ['./manage-tournamens.component.scss'],
})
export class ManageTournamensComponent implements OnInit {
  userSubscription: Subscription;
  AppHtmlRoutes = AppHtmlRoutes;
  timezone;
  tournament: any = {};
  user: IUser;
  apiLoaded: Array<boolean> = [];
  hideMessage = true;
  hasRequiredAccess: boolean = true;
  tournamentStartTime = '';
  startDateIntervalId = null;
  endDateIntervalId = null;
  noStart = false;
  isLoaded = false;
  allowEdit = true;

  constructor(
    private activatedRoute: ActivatedRoute,
    private tournamentService: TournamentService,
    @Inject(PLATFORM_ID) private platformId,
    private esportsTimezone: EsportsTimezone,
    private eSportsChatService: EsportsChatService,
    private userService: EsportsUserService,
    private translateService: TranslateService,
    private eSportsToastService: EsportsToastService,
    private esportsTournamentService: EsportsTournamentService,
    private gtmService: EsportsGtmService,
    private router: Router
  ) {}

  ngOnInit(): void {
    const isBrowser = isPlatformBrowser(this.platformId);
    if (isBrowser && GlobalUtils.isBrowser()) {
      this.timezone = this.esportsTimezone.getTimeZoneName();
      this.tournament.slug = this.activatedRoute.snapshot.params.slug; // get slug from route
      this.eSportsChatService.connectTournamentEvents(this.tournament.slug);
      if (this.tournament.slug) {
        this.getCurrentUserDetails();
      }
    }
  }

  getCurrentUserDetails = async () => {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.user = data;
        this.fetchTournamentDetails();
      }
    });
  };

  fetchTournamentDetails = async (
    isTournamentStart: boolean = false,
    isTournamentFinished: boolean = false
  ) => {
    try {
      this.apiLoaded.push(false);
      const tournament = await this.tournamentService
        .getTournamentBySlug(this.tournament?.slug)
        .toPromise();
      this.tournament = tournament?.data;
      this.esportsTournamentService.manageTournamentSubject.next(
        tournament?.data
      );

      if (
        this.tournament?.isSeeded ||
        Date.now() >= new Date(this.tournament?.startDate).getTime()
      ) {
        this.allowEdit = false;
      }

      if (!this.tournament) {
        this.hideMessage = false;
        throw new Error(this.translateService.instant('VIEW_TOURNAMENT.ERROR'));
      }

      const isEventOrganizer =
        this.user?._id === this.tournament?.organizerDetail?._id;
      const hasAdminAccess =
        this.user?.accountType == 'admin' &&
        this.user?.accessLevel?.includes('em');

      this.hasRequiredAccess = isEventOrganizer || hasAdminAccess;
      if (!this.hasRequiredAccess) {
        throw new Error(
          this.translateService.instant('VIEW_TOURNAMENT.ACCESS_DENIED')
        );
      }

      if (this.tournament?.isFinished) {
        if (isTournamentFinished) {
          this.eSportsToastService.showInfo(
            this.translateService.instant('ELIMINATION.TOURNAMENT_FINISHED'),
            false
          );
        }
        this.tournamentStartTime = this.translateService.instant(
          'MANAGE_TOURNAMENT.TOURNAMENT_FINISHED'
        );
      } else if (this.tournament?.isSeeded) {
        if (isTournamentStart) {
          this.eSportsToastService.showInfo(
            this.translateService.instant('ELIMINATION.TOURNAMENT_STARTED'),
            false
          );
        }

        this.tournamentStartTime = this.translateService.instant(
          'MANAGE_TOURNAMENT.TOURNAMENT_STARTED'
        );
        this.endDateTimer();
      } else {
        this.startDateTimer();
        this.noStart = true;
      }
      // this.fetchRegisteredParticipant('registeredParticipantCount');
      // this.fetchRegisteredParticipant('checkedInParticipantCount', {
      //   checkedIn: true,
      // });
      this.apiLoaded.push(true);
    } catch (error) {
      this.hasRequiredAccess = false;
      this.apiLoaded.push(true);
      this.eSportsToastService.showError(
        error?.error?.message || error?.message
      );
    } finally {
      this.checkAllApiDataLoaded();
    }
  };

  startDateTimer = () => {
    const startDate = new Date(this.tournament.startDate);
    const currentDate = new Date();
    if (startDate.getTime() - currentDate.getTime() > 0) {
      const millisecond = startDate.getTime() - currentDate.getTime();
      const dd = (millisecond / (1000 * 60 * 60 * 24)) | 0;
      if (dd) {
        const currentLanguage = this.translateService.currentLang;

        if (currentLanguage == 'en') {
          this.tournamentStartTime = `${dd} ${
            dd === 1
              ? this.translateService.instant('MANAGE_TOURNAMENT.DAYS_LEFT.01')
              : this.translateService.instant('MANAGE_TOURNAMENT.DAYS_LEFT.02')
          }`;
        } else {
          if (dd === 1) {
            this.tournamentStartTime = `${dd} ${this.translateService.instant(
              'MANAGE_TOURNAMENT.DAYS_LEFT.01'
            )}`;
          } else if (dd === 2) {
            this.tournamentStartTime = `${this.translateService.instant(
              'MANAGE_TOURNAMENT.DAYS_LEFT.02'
            )}`;
          } else if (dd === 3) {
            this.tournamentStartTime = `${dd} ${this.translateService.instant(
              'MANAGE_TOURNAMENT.DAYS_LEFT.03'
            )}`;
          } else if (dd === 11) {
            this.tournamentStartTime = `${dd} ${this.translateService.instant(
              'MANAGE_TOURNAMENT.DAYS_LEFT.04'
            )}`;
          } else {
            this.tournamentStartTime = `${dd} ${this.translateService.instant(
              'MANAGE_TOURNAMENT.DAYS_LEFT.04'
            )}`;
          }
        }
        return;
      }

      const hh = (millisecond / (1000 * 60 * 60)) | 0;
      const mm = ((millisecond - hh * 1000 * 60 * 60) / (1000 * 60)) | 0;
      const ss = ((millisecond - hh * 1000 * 60 * 60) / 1000) % 60 | 0;
      this.tournamentStartTime = `${hh > 9 ? hh : `0${hh}`}: ${
        mm > 9 ? mm : `0${mm}`
      }: ${ss > 9 ? ss : `0${ss}`}`;

      if (!this.startDateIntervalId) {
        this.startDateIntervalId = setInterval(
          () => this.startDateTimer(),
          1000
        );
      }
    } else {
      if (this.startDateIntervalId) {
        this.fetchTournamentDetails(true);
      } else {
        this.tournamentStartTime = this.translateService.instant(
          'MANAGE_TOURNAMENT.TOURNAMENT_EXPIRED'
        );
      }
      clearInterval(this.startDateIntervalId);
      this.startDateIntervalId = null;
    }
  };

  endDateTimer = () => {
    const endDate = new Date(this.tournament?.endDate);
    const currentDate = new Date();
    const timeDiff = endDate.getTime() - currentDate.getTime();
    if (timeDiff > 0) {
      this.endDateIntervalId = setInterval(() => this.endDateTimer(), 1000);
    } else if (
      ['swiss_safeis', 'ladder'].includes(this.tournament?.bracketType)
    ) {
      if (this.endDateIntervalId) {
        this.fetchTournamentDetails(false, true);
        clearInterval(this.endDateIntervalId);
        this.endDateIntervalId = null;
      }
    }
  };

  /**
   * Fatch Register Or CheckedIn Participant
   * @param field registeredParticipantCount, checkedInParticipantCount
   * @param obj for checkedIn users
   */
  fetchRegisteredParticipant = async (field, obj = {}) => {
    try {
      this.apiLoaded.push(false);
      const encodeUrl = `?query=${encodeURIComponent(
        JSON.stringify({ tournamentId: this.tournament?._id, ...obj })
      )}&select=''&option=${encodeURIComponent(
        JSON.stringify({ count: true })
      )}`;
      const response = await this.tournamentService
        .getParticipants(encodeUrl)
        .toPromise();
      this[field] = response?.totals?.count;
      this.apiLoaded.push(true);
    } catch (error) {
      this.apiLoaded.push(true);
      this.eSportsToastService.showError(
        error?.error?.message || error?.message
      );
    } finally {
      this.checkAllApiDataLoaded();
    }
  };

  checkAllApiDataLoaded() {
    const startedApiCount = this.apiLoaded.filter((el) => !el).length;
    const finishedApiCount = this.apiLoaded.filter((el) => el).length;
    this.isLoaded = startedApiCount === finishedApiCount;
  }

  editTournament() {
    this.pushGTMTags('View_Edit_Tournament');
    // this.router.navigate(['/tournament/edit/' + this.tournament.slug]);
    // this.router.navigate(['/advance-tournament/' + this.tournament.slug]);
  }
  // async ngAfterViewInit(): Promise<void> {

  //   var id;
  //   this._activateRoute.paramMap.subscribe((params) => {
  //     id = params.get('slug');
  //   })
  //   let paramsd = new HttpParams();
  //   paramsd = paramsd.append('query', JSON.stringify({ slug: id, tournamentStatus: 'publish' }));
  //   const tournament = await this.tournamentService
  //     .getTournamentDetails(paramsd)
  //     .toPromise();
  //   this.tournamentService.subscribeTournamentDetails(tournament.data[0]);

  //   this.tournamentDetails = tournament.data.length
  //     ? tournament.data[0]
  //     : null;
  //   localStorage.setItem("t_report", JSON.stringify(tournament.data))
  // }

  showEditError() {
    this.eSportsToastService.showError(
      this.translateService.instant('ERROR.TOURNAMENT_EDIT_ERROR')
    );
  }

  pushGTMTags(eventName: string, eventData: EventProperties = null) {
    this.gtmService.pushGTMTags(eventName, eventData);
  }
}
