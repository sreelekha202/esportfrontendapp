import { Component, OnInit, Input } from '@angular/core';
import { toggleOpacity, VisibilityState } from '../../../animations';
import { CommentService } from '../../../core/service';
import { Subscription } from 'rxjs';
import { EsportsUserService } from 'esports';
@Component({
  selector: 'app-discussion',
  templateUrl: './discussion.component.html',
  styleUrls: ['./discussion.component.scss'],
  animations: [toggleOpacity],
})
export class DiscussionComponent implements OnInit {
  VisibilityState = VisibilityState;
  collapseOrExpand: VisibilityState = VisibilityState.Visible;
 
  @Input() allowComment: boolean = false;
  @Input() isReplying: boolean = false;
  comments = [];

  currentUser: any;
  userSubscription: Subscription;

  //  comments = [];
  tournamentDetails: any;

  constructor(private userService:  EsportsUserService , private commentService: CommentService) { }

  ngOnInit(): void {
    // MOCK COMMENTS DATA

    this.tournamentDetails = localStorage.getItem("tournamentDetails")
    this.tournamentDetails = JSON.parse(this.tournamentDetails)
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.currentUser = data;
      }
    });

    this.getComment();
  }
  getComment() {
    this.commentService.getAllComment1("tournament", this.tournamentDetails._id, 1).subscribe((data) => {
      this.comments = data.data
    });
  }

  onSaveCommentt(msg) {
    let data = {
      comment: msg,
      objectId: this.tournamentDetails._id,
      objectType: "tournament"
    }
    this.commentService.saveComment(data).subscribe((res) => {
      this.getComment();
    })
  }

}
