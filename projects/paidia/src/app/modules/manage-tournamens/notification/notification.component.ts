import { Component, OnInit } from '@angular/core';
import { MessageService } from '../../../core/service';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss']
})
export class NotificationComponent implements OnInit {
  showLoader: boolean = true;
  messagesPerPage = [10, 15, 25];
  slicesMessages = [];
  messages: any = [];

  constructor(private messageService: MessageService) { }

  ngOnInit(): void {
    this.getMessages();
  }

  getMessages(): void {
    this.messageService.getInboxMessages().subscribe(
      (response) => {
        this.messages = response.data;
        this.slicesMessages = this.messages.slice(0, this.messagesPerPage[0]);
        this.showLoader = false;
      },
      (error) => {
        this.showLoader = false;
      }
    );
  }

  onSelectAll(): void { }

  onPageChange(event): void {
    const startIndex = event.pageIndex * event.pageSize;
    let endIndex = startIndex + event.pageSize;

    if (endIndex > this.messages.length) {
      endIndex = this.messages.length;
    }

    this.slicesMessages = this.messages.slice(startIndex, endIndex);
  }
  onTextChange(data) {
  }
}
