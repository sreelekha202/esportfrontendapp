import { Component, OnInit } from '@angular/core';
export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H'},
  {position: 2, name: 'Helium', weight: 4.0026, symbol: 'He'},
  {position: 3, name: 'Lithium', weight: 6.941, symbol: 'Li'},

];

@Component({
  selector: 'app-battle-edit-score',
  templateUrl: './battle-edit-score.component.html',
  styleUrls: ['./battle-edit-score.component.scss']
})
export class BattleEditScoreComponent implements OnInit {
  imgurl;

  constructor() { }

  ngOnInit(): void {
  }

  displayedColumns: string[] = ['position', 'name', 'weight', 'symbol'];
    dataSource = ELEMENT_DATA;

    datatoimg(data) {
      this.imgurl = data;
    }
}
