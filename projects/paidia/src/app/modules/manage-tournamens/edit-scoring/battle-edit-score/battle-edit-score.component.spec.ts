import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BattleEditScoreComponent } from './battle-edit-score.component';

describe('BattleEditScoreComponent', () => {
  let component: BattleEditScoreComponent;
  let fixture: ComponentFixture<BattleEditScoreComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BattleEditScoreComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BattleEditScoreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
