import { Component, OnInit } from '@angular/core';
import { EsportsTournamentService, IPagination } from 'esports';
import { Location } from '@angular/common';
import {
  EsportsChatService,
  EsportsUtilsService,
  EsportsToastService,
  EsportsBracketService,
} from 'esports';
import { MatDialog } from '@angular/material/dialog';
import { PopupCreatedComponent } from '../../create-tournament/components/popup-created/popup-created.component';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { FormGroup } from '@angular/forms';
@Component({
  selector: 'app-edit-scoring',
  templateUrl: './edit-scoring.component.html',
  styleUrls: ['./edit-scoring.component.scss'],
})
export class EditScoringComponent implements OnInit {
  round: any;
  windowposition: String = 'chat_window chat_window_right';
  matchAndTournamentID:any;
  constructor(
    private toastService: EsportsToastService,
    public esportsChatService: EsportsChatService,
    private bracketService: EsportsBracketService,
    private utilsService: EsportsUtilsService,
    public matDialog: MatDialog,
    private translateService: TranslateService,
    private router: Router,
    private location: Location,
    private esportsTournamentService: EsportsTournamentService
  ) {}
  active = 0;
  nextId: number = 1;
  tournament: any;
  imgurl;
  page: IPagination = {
    activePage: 1,
    totalItems: 1,
    itemsPerPage: 1,
    maxSize: 10,
  };
  enableWebview: boolean = false;
  tReport: any;
  matchDetails: any;
  participantId;
  hasParticipantOneTimeAccess: boolean = true;
  teamList: Array<any> = [];
  disQualifiedTeam: any = {
    reason: '',
    team: null,
  };
  isLoaded: boolean = false;
  token;
  form: FormGroup;
  addForm: FormGroup;
  rounds:any;
  roundMatches: any;
  defaultWinnerLogo: string =
    'https://i115.fastpic.ru/big/2021/0711/d5/e284e76b3d0de8760861e2d97642e5d5.png';
  participantSet: any;
  isAdmin: boolean;

  ngOnInit(): void {
    this.esportsTournamentService.manageTournament.subscribe((data) => {
      if (data) {
        this.tournament = data;
        this.tReport = JSON.parse(localStorage.getItem('t_report'));
        this.fetchRoundMatches();
      }
    });

    if(this.roundMatches){

    }
    if (this.location.getState()) {
      this.matchAndTournamentID=this.location.getState()

      this.fetchMatchDetails();
    }
  }

  fetchRoundMatches = async () => {
    this.roundMatches = this.bracketService.macthRoundData;
    if (this.roundMatches) {
      this.rounds=this.roundMatches?.round
    } else {
      this.router.navigate(['manage-tournament', this.tournament.slug, 'scoring']);
    }
  };

  showCreatedPopup(): void {
    this.matDialog.open(PopupCreatedComponent);
  }

  fetchMatchDetails = async () => {
    try {
      this.isLoaded = true;
      /**
      const queryParam = `?query=${encodeURIComponent(
        JSON.stringify({
          tournamentId: this.matchAndTournamentID?.tournamentId,
          _id: this.matchAndTournamentID?.matchId,
        })
      )}`;
      const match = await this.bracketService.fetchAllMatches(queryParam);
       */

      const queryParams = {
        tournamentId: this.matchAndTournamentID?.tournamentId,
        matchId: this.matchAndTournamentID?.matchId,
      }
      const match = await this.bracketService.fetchAllMatchesV3(queryParams);
      this.matchDetails = match?.data?.length ? match.data[0] : null;
      if (this.matchDetails) {
        if (
          this.participantId == this.matchDetails.teamA?._id &&
          this.matchDetails?.matchUpdatedByTeamACount > 0
        ) {
          this.hasParticipantOneTimeAccess = false;
        }

        if (
          this.participantId == this.matchDetails.teamB?._id &&
          this.matchDetails?.matchUpdatedByTeamBCount > 0
        ) {
          this.hasParticipantOneTimeAccess = false;
        }
        this.teamList = [
          { ...this.matchDetails.teamA },
          { ...this.matchDetails.teamB },
        ];
        this.disQualifiedTeam = this.matchDetails?.disQualifiedTeam;
      }
      this.isLoaded = false;
    } catch (error) {
      this.isLoaded = false;
      this.showAlertBasedOnPlatform(
        error?.error?.message || error?.message,
        'showError'
      );
    }
  };

  updateScoree(data) {
    let s, value, key, delta, teamId;
    s = data.set;
    value = data.score;
    key = data.team_name;
    delta = data.n;
    teamId = data.team_id;
    if (this.isAdmin && this.roundMatches.isNextMatchStart) {
      // return;
    } else if (!this.isAdmin) {
      if (this.participantId !== teamId) {
        // return;
      }

      if (
        this.participantId === teamId &&
        this.roundMatches.matchStatus === 'completed'
      ) {
        // return;
      }

      if (this.tReport?.tournaments?.scoreReporting <= 1) {
        // return;
      }

      if (s?.status === 'completed') {
        // return;
      }
    }
    // s[key] = s[key] <= 0 && delta === -1 ? value : value + delta;
    s[key] = value;
    s.modify = true;
    this.participantSet = { ...this.participantSet, [key]: s[key], id: s?.id };
  }

  isAllowScore = async (setA, setB) => {
    const isInValid =
      setA == setB &&
      ['swiss_safeis', 'ladder'].includes(
        this.tReport?.tournaments?.bracketType
      );
    if (isInValid) {
      this.toastService.showInfo(
        this.translateService.instant('ELIMINATION.SCORE_ALERT')
      );
    }
    return !isInValid;
  };
  submitParticipantScore = async (set, match) => {
    try {
      this.isLoaded = true;
      const status = await this.isAllowScore(set.teamAScore, set.teamBScore);
      if (!status) return;

      /*
      var params = {
        _id: match?._id,
        'sets.id': set.id,
        tournamentId: match?.tournamentId?._id,
      };
      const queryParam = `?query=${encodeURIComponent(JSON.stringify(params))}`;

      // const payload = {
      //   $set: this.convertObject(set),
      // };
      const payload = {
        teamAScore: set.teamAScore,
        teamAScreenShot: null,
        teamBScore: set.teamBScore,
        teamBScreenShot: null,
      };
      const response = await this.bracketService.updateParticipantScore(
        queryParam,
        payload
      );
*/
      const payload = {
        teamAScore: set.teamAScore,
        teamAScreenShot: null,
        teamBScore: set.teamBScore,
        teamBScreenShot: null,
      };

      const queryParams = {
        matchId: match?._id,
        'setId': set.id,
        tournamentId: match?.tournamentId?._id,
      };

      let response = await this.bracketService.updateParticipantScoreV2(
        queryParams,
        payload
      );


      this.showAlertBasedOnPlatform(response?.message, 'showSuccess');
      this.isLoaded = false;
      this.router.navigate(['profile/tournaments-created']);
      // this.fetchMatchDetails();
    } catch (error) {
      this.isLoaded = false;
      this.showAlertBasedOnPlatform(
        error?.error?.message || error?.message,
        'showError'
      );
    }
  };

  showAlertBasedOnPlatform = (message, type) => {
    this.enableWebview
      ? this.utilsService.showNativeAlert(message)
      : this.toastService[type](message);
  };

  submitScore = async (set, matchDetail) => {
    try {
      this.isLoaded = true;
      /**
      let params = {
        _id: matchDetail?._id,
        'sets.id': set.id,
        tournamentId: matchDetail?.tournamentId?._id,
      };
      const queryParam = `?query=${encodeURIComponent(JSON.stringify(params))}`;

      const payload = {
        teamAScore: set.teamAScore,
        teamAScreenShot: null,
        teamBScore: set.teamBScore,
        teamBScreenShot: null,
      };

      const match = await this.bracketService.updateMatch(queryParam, payload);
       */
      let queryParams = {
        matchId: matchDetail?._id,
        setId: set.id,
        tournamentId: matchDetail?.tournamentId?._id,
      };
      const payload = {
        teamAScore: set.teamAScore,
        teamAScreenShot: null,
        teamBScore: set.teamBScore,
        teamBScreenShot: null,
      };
      const match = await this.bracketService.updateMatchV2(queryParams, payload);
      if (!match.success) {
        this.showAlertBasedOnPlatform(match.message, 'showInfo');
      }
      if (match.success) {
        this.toastService.showSuccess(this.translateService.instant("MANAGE_TOURNAMENT.MATCHES.SCORE_UPDATED"));
        this.router.navigate(['manage-tournament', this.tournament.slug, 'scoring']);
      }

      this.isLoaded = false;
      this.fetchMatchDetails();
    } catch (error) {
      this.isLoaded = false;
      this.token
        ? this.utilsService.showNativeAlert(
            error?.error?.message || error?.message
          )
        : this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  myTeams = [
    {
      image:
        'https://i115.fastpic.ru/big/2021/0711/d5/e284e76b3d0de8760861e2d97642e5d5.png',
      name: 'Nexplay Empress',
      teamsLength: 5,
      createtdAt: 1,
    },
    {
      image:
        'https://i115.fastpic.ru/big/2021/0711/bc/3c1e95f844d101e292be66deca3b09bc.png',
      name: 'Dark Ninja',
      teamsLength: 4,
      createtdAt: 1,
    },
  ];
  navChanges = (e) => {
    this.nextId = e.nextId;
    switch (this.nextId) {
      case 1:
        // this.getMyTeams();
        break;
      case 2:
        // this.getMyInvite();
        break;
    }
  };

  datatoimg(data) {
    this.imgurl = data;
  }


  genrateReport() {
   if(this.matchAndTournamentID){
    this.router.navigate([`/report-issue`],{queryParams:
      { matchId: this.matchAndTournamentID?.matchId,
        tournamentId:this.matchAndTournamentID?.tournamentId}
      });
   }
    // if (!this.enableWebview) {
    //   this.exit.emit({ refresh: true, isOpenScoreCard: false });
    //   this.router.navigate([`/report-issue/${this.matchDetails?._id}/${this.matchDetails?.tournamentId?._id}`,]);

    // }
  }




  chatFunction(id) {
    this.esportsChatService.getMatchInformation(id).subscribe((result: any) => {
      const mtchdetails = {
        matchid: result.matchdetails._id,

        teamAId: result.matchdetails.teamA.userId._id,

        teamBId: result.matchdetails.teamB.userId._id,

        tournamentid: result.matchdetails.tournamentId,

        typeofchat: 'tournament',

        users: [
          {
            _id: result.matchdetails.teamA.userId._id,
            fullName: result.matchdetails.teamA.userId.fullName,
            profilePicture: result.matchdetails.teamA.userId.profilePicture,
          },
          {
            _id: result.matchdetails.teamB.userId._id,
            fullName: result.matchdetails.teamB.userId.fullName,
            profilePicture: result.matchdetails.teamB.userId.profilePicture,
          },
        ],
      };
      // this.matchdetails = mtchdetails;
      // this.showChat = true;
      // this.typeofchat = 'tournament';
      this.esportsChatService.setWindowPos(this.windowposition);

      let firstChat = this.esportsChatService.getChatStatus();

      if (firstChat == true) {
        this.esportsChatService.setChatStatus(false);
        this.esportsChatService?.disconnectMatch(
          mtchdetails.matchid,
          'tournament'
        );

        this.esportsChatService.setTextButtonStatus(false);
      } else {
        this.esportsChatService.setCurrentMatch(mtchdetails);

        this.esportsChatService.setTypeOfChat('tournament');

        this.esportsChatService.setChatStatus(true);

        this.esportsChatService.setTextButtonStatus(true);
      }
    });
  }
}
