import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { TeamService } from 'projects/paidia/src/app/core/service/team.service';
import { EsportsUserService, EsportsUtilsService, EsportsGtmService, EsportsToastService, EsportsLeaderboardService, SuperProperties, EventProperties } from 'esports';
import { BracketService } from "../../../../../core/service"
import { MatDialog } from "@angular/material/dialog";
import { PopupCreatedComponent } from '../../../../create-tournament/components/popup-created/popup-created.component';
@Component({
  selector: 'app-team-card',
  templateUrl: './team-card.component.html',
  styleUrls: ['./team-card.component.scss'],
})
export class TeamCardComponent implements OnInit {
  @Input() isManage: boolean = false;
  @Input() team;
  @Input() score;
  @Input() set;
  @Input() team_name;

  enableWebview: boolean = false;
  @Output() updateScore = new EventEmitter();
  tReport: any;
  matchDetails: any;
  participantId;
  hasParticipantOneTimeAccess: boolean = true;
  teamList: Array<any> = [];
  disQualifiedTeam: any = {
    reason: "",
    team: null,
  };
  isLoaded: boolean = false;
  token;

  constructor(private teamService: TeamService,
    private toastService: EsportsToastService,
    private bracketService: BracketService,
    private utilsService: EsportsUtilsService,
    public matDialog: MatDialog,
  ) { }

  ngOnInit(): void { }

  deleteTeam(team_id) {

    const data = {
      "id": team_id,
      "admin": true,
      "query": {
        "condition": { "_id": team_id, "status": "active" },
        "update": { "status": "inactive" }
      }
    };
    this.teamService.deleteTeam(data).then((data: any) => {
      if (data) {
        let res = JSON.stringify(data)
        // this.refresh.emit(data)
        this.toastService.showSuccess(data.message);
        // this.router.navigate(['profile/my-teams']);
      }
    })
      .catch((error) => {
      });
  }

  updateScoree(set, score, team_name, n, team_id) {
    this.updateScore.emit({
      set: set,
      score: score,
      team_name: team_name,
      n: n,
      team_id: team_id
    })
  }
}
