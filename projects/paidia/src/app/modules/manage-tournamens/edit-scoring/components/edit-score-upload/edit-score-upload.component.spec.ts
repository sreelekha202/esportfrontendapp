import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditScoreUploadComponent } from './edit-score-upload.component';

describe('EditScoreUploadComponent', () => {
  let component: EditScoreUploadComponent;
  let fixture: ComponentFixture<EditScoreUploadComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditScoreUploadComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditScoreUploadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
