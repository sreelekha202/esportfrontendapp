import { CoreModule } from './../../core/core.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { RouterBackModule } from './../../shared/directives/router-back.module';
import { HeaderPaidiaModule } from "../../shared/components/header-paidia/header-paidia.module";
import { ManageTournamensComponent } from './manage-tournamens.component';
import { ScoringComponent } from './scoring/scoring.component';
import { AddNotificationComponent } from './add-notification/add-notification.component';
import { NotificationComponent } from './notification/notification.component';
import { AddClipsComponent } from './add-clips/add-clips.component';
import { ClipsComponent } from './clips/clips.component';
import { BookmarkCardComponent } from './clips/components/bookmark-card/bookmark-card.component';
import { InputtSearchComponent } from './notification/components/input-search/inputt-search.component';
import { EditScoringComponent } from './edit-scoring/edit-scoring.component';
import { ManageScoreComponent } from './manage-score/manage-score.component';
import { TeamCardComponent } from './edit-scoring/components/team-card/team-card.component';
import { TeamBannerComponent } from './manage-score/components/team-banner/team-banner.component';
import { PopupCreatedComponent } from './manage-score/components/popup-created/popup-created.component';
import { ParticipantsRequestsComponent } from './participants-requests/participants-requests.component'
import { DiscussionComponent } from './discussion/discussion.component';
import { UserReportsComponent } from './user-reports/user-reports.component';
import { IsearchComponent } from './user-reports/component/isearch.component';
import { TeamscardComponent } from './participants-requests/teamscard/teamscard/teamscard.component';
import { DiscussionCommentsComponent } from './discussion/discussion-comments/discussion-comments/discussion-comments.component';
import { HeaderComponent } from './header/header.component';
import { BracketsComponent } from './brackets/brackets.component';
import { ParticipantCardComponent } from './brackets/participant-card/participant-card.component';
import { SharedModule } from '../../shared/modules/shared.module';
import {BracketTreeModule} from '../../shared/components/bracket-tree/bracket-tree.module';
import { EditScoreUploadComponent } from './edit-scoring/components/edit-score-upload/edit-score-upload.component';
import { EsportsCustomPaginationModule, WYSIWYGEditorModule, EsportsLoaderModule } from 'esports';
import { BattleEditScoreComponent } from './edit-scoring/battle-edit-score/battle-edit-score.component';
import { EditTournamentComponent } from './edit-tournament/edit-tournament.component';
import { FormComponentModule } from "../../shared/components/form-component/form-component.module";

export const routes: Routes = [
  {
    path: '',
    component: ManageTournamensComponent,
    children: [
      {
        path: '',
        redirectTo: 'participants-requests',
        pathMatch: 'full',
      },
      {
        path: 'scoring',
        component: ScoringComponent,
      },
      {
        path: 'clips',
        component: ClipsComponent,
      },
      {
        path: 'add-clips',
        component: AddClipsComponent,
      },
      {
        path: 'notification',
        component: NotificationComponent,
      },
      {
        path: 'add-notification',
        component: AddNotificationComponent,
      },
      {
        path: 'edit-score',
        component: EditScoringComponent,
      },
      {
        path: 'battle-edit-score',
        component: BattleEditScoreComponent,
      },

      {
        path: 'manage-score',
        component: ManageScoreComponent,
      },
      {
        path: 'participants-requests',
        component: ParticipantsRequestsComponent,
      },
      {
        path: 'discussion',
        component:  DiscussionComponent,
      },
      {
        path: 'reports/:tournamentId',
        component:  UserReportsComponent,
      },
      {
        path: 'brackets',
        component:  BracketsComponent,
      },
      {
        path: 'edit',
        component:  EditTournamentComponent,
      },
    ],

  }
]

@NgModule({
  declarations: [
    ManageTournamensComponent,
    ScoringComponent,
    AddNotificationComponent,
    NotificationComponent,
    AddClipsComponent,
    ClipsComponent,
    BookmarkCardComponent,
    InputtSearchComponent,
    EditScoringComponent,
    ManageScoreComponent,
    TeamCardComponent,
    TeamBannerComponent,
    PopupCreatedComponent,
    UserReportsComponent,
    DiscussionComponent,
    ParticipantsRequestsComponent,
    IsearchComponent,
    TeamscardComponent,
    DiscussionCommentsComponent,
    HeaderComponent,
    BracketsComponent,
    ParticipantCardComponent,
    EditScoreUploadComponent,
    BattleEditScoreComponent,
    EditTournamentComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule,
    HeaderPaidiaModule,
    CoreModule,
    RouterBackModule,
    BracketTreeModule,
    EsportsCustomPaginationModule,
    WYSIWYGEditorModule,
    FormComponentModule,
    EsportsLoaderModule,
  ]
})
export class ManageTournamensModule { }
