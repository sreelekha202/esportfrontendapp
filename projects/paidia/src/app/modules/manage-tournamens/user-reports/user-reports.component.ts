import { ProfileService } from './../../../core/service/profile.service';
import { Component, OnInit } from '@angular/core';
import { LangChangeEvent, TranslateService } from '@ngx-translate/core';
import { environment } from '../../../../environments/environment';
import { TournamentService } from '../../../core/service';
import { UserReportsService, EsportsUserService} from 'esports';
import {  ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-user-reports',
  templateUrl: './user-reports.component.html',
  styleUrls: ['./user-reports.component.scss']
})
export class UserReportsComponent implements OnInit {
  userReport = [];
  currentLang: string = 'english';
  userReportPerPage = [5, 10, 20];
  showLoader: boolean = true;
  slicesuserReport = [];
  searchText: string = '';
  tournamentDetails: any;
  tournamentId:any;

  paginationData = {
    page: 1,
    limit: 10,
    sort: 'startDate',
  };
  paginationDetails: any;
  pageSizeOptions = environment.pageSizeOptions;

  constructor(
    private profileService: ProfileService,
    private tournamentService: TournamentService,
    private translateService: TranslateService,
    private activeRoute: ActivatedRoute,
    private userService: EsportsUserService,private UserReportsService: UserReportsService) {
    this.currentLang =
      translateService.currentLang == 'ms' ? 'malay' : 'english';
    translateService.onLangChange.subscribe((event: LangChangeEvent) => {
      this.currentLang = event.lang == 'ms' ? 'malay' : 'english';
    });

  }
  ngOnInit(): void {
     const { tournamentId } = this.activeRoute.snapshot.params;
     this.tournamentId=tournamentId
     this.getContent()
  }
  onSelectAll(): void { }

  getContent(){
    this.UserReportsService.getTournamentsUserReport(environment.apiEndPoint,
      this.tournamentId,
      this.paginationData?.limit,
      this.paginationData?.page
    ).subscribe((res: any) => {
      this.showLoader = false;
         this.userReport = res?.data?.docs;
      },
      (err) => {
         this.showLoader = false;
      }
    ); 
  }

  onTextChange(data: string) {
    this.searchText = data;
  }
  filterSlicesUserReport(){
    return this.slicesuserReport.filter((obj) => obj.fullText.includes(this.searchText.trim()))
  }
  pageChanged(page): void {
    this.paginationData.page = page.pageIndex + 1;
    this.paginationData.limit = page.pageSize;
    this.getContent();

  }
}
