import { Component, OnInit, OnDestroy } from '@angular/core';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';
import { addMinutes } from 'date-fns';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import {
  EsportsTournamentService,
  ITournament,
  EsportsToastService,
  EsportsGtmService,
  EventProperties,
} from 'esports';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  Validators,
} from '@angular/forms';

@AutoUnsubscribe()
@Component({
  selector: 'app-edit-tournaments',
  templateUrl: './edit-tournament.component.html',
  styleUrls: ['./edit-tournament.component.scss'],
})
export class EditTournamentComponent implements OnInit, OnDestroy {
  tournamentDetails: ITournament | any;
  editForm: FormGroup;
  editorConfig = {};
  isLoading = true;
  detailSub;
  allowStartDateEdit = true;
  imgData = { imgurl: '' };

  constructor(
    private tournamentService: EsportsTournamentService,
    private fb: FormBuilder,
    private toastService: EsportsToastService,
    private router: Router,
    private gtmService: EsportsGtmService,
    private translateService: TranslateService
  ) {}

  ngOnInit(): void {
    this.setWyswygConfig();
    this.createTournamentEditForm();
    this.fetchTournamentDetails();
  }

  ngOnDestroy() {
    // Auto unsubscribe not working properly
    if (this.detailSub) {
      this.detailSub?.unsubscribe();
    }
  }

  fetchTournamentDetails() {
    this.detailSub = this.tournamentService.manageTournament.subscribe(
      (data) => {
        if (data) {
          this.tournamentDetails = data;
          /**
          if (
            data?.isSeeded ||
            new Date(data?.startDate).getTime() <= Date.now()
          ) {
            this.toastService.showError(
              this.translateService.instant('ERROR.TOURNAMENT_EDIT_ERROR')
            );
            this.router.navigate([`profile/tournaments-created`]);
          }
           */

          this.allowStartDateEdit =
            !data?.isSeeded &&
            !(new Date(data?.startDate).getTime() <= Date.now());

          if (!this.allowStartDateEdit) {
            this.editForm.get('startDate').clearValidators();
            this.editForm.get('startDate').updateValueAndValidity();
          }

          this.editForm.patchValue(data);
          this.datatoimg(data?.banner);
        }
        this.isLoading = false;
      },
      (err) => {
        this.toastService.showError(err.message);
        this.isLoading = false;
        this.router.navigate([`profile/tournaments-created`]);
      }
    );
  }

  createTournamentEditForm() {
    this.editForm = this.fb.group({
      name: ['', Validators.required],
      maxParticipants: ['', Validators.required],
      description: [''],
      rules: [''],
      startDate: ['', [Validators.required, this.validateStartDate]],
      banner: [''],
    });
  }

  validateStartDate = (startDate: AbstractControl) => {
    if (!startDate.value) {
      return null;
    }
    let startDateObject = new Date(startDate.value);
    const fiveMinsLater = addMinutes(new Date(), 5);
    if (fiveMinsLater.getTime() > startDateObject.getTime()) {
      return { invalidDate: true };
    }

    if (
      this.tournamentDetails?.endDate &&
      startDateObject.getTime() >
        new Date(this.tournamentDetails?.endDate).getTime()
    ) {
      return { invalidDate2: true };
    }
  };

  validateAndSubmit() {
    const invalidForm = this.editForm.invalid;
    if (invalidForm) {
      this.editForm.markAllAsTouched();
      const errorMessage = this.getInvalidMessage();
      this.toastService.showError(errorMessage);
      return;
    }
    this.submit();
  }

  submit() {
    this.isLoading = true;
    this.tournamentService
      .updateTournament2(this.editForm.value, this.tournamentDetails._id)
      .subscribe(
        (response) => {
          this.pushGTMTags('Publish_Complete', { fromPage: this.router.url });
          this.toastService.showSuccess(response.message);
          this.router.navigate([`tournament/${this.tournamentDetails?.slug}`]);
          this.isLoading = false;
        },
        (err) => {
          this.toastService.showError(err.message);
          this.isLoading = false;
        }
      );
  }

  getInvalidMessage() {
    let errorMessage = 'Please fill the required details: ';
    Object.keys(this.editForm.controls).forEach((element, idx) => {
      this.editForm.controls[element].status != 'VALID'
        ? (errorMessage += `<br> ${idx}. ${element}`)
        : null;
    });
    return errorMessage;
  }

  setWyswygConfig() {
    this.editorConfig = {
      editable: true,
      spellcheck: true,
      height: '180px',
      width: 'auto',
      minWidth: '0',
      translate: 'yes',
      enableToolbar: true,
      showToolbar: true,
      defaultParagraphSeparator: '',
      defaultFontName: '',
      defaultFontSize: '',
      fonts: [
        { class: 'arial', name: 'Arial' },
        { class: 'times-new-roman', name: 'Times New Roman' },
        { class: 'calibri', name: 'Calibri' },
        { class: 'comic-sans-ms', name: 'Comic Sans MS' },
      ],

      customClasses: [
        {
          name: 'quote',
          class: 'quote',
        },
        {
          name: 'redText',
          class: 'redText',
        },
        {
          name: 'titleText',
          class: 'titleText',
          tag: 'h1',
        },
      ],
      sanitize: true,
      toolbarPosition: 'top',
      toolbarHiddenButtons: [['bold', 'italic'], ['fontSize']],
    };
  }

  pushGTMTags(eventName: string, eventData: EventProperties = null) {
    this.gtmService.pushGTMTags(eventName, eventData);
  }
  datatoimg(data) {
    this.imgData.imgurl = data;
  }

}
