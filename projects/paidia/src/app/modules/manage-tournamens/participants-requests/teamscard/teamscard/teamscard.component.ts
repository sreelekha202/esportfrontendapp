import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { EsportsTournamentService, EsportsToastService } from 'esports';
import { environment } from '../../../../../../environments/environment';
const API = environment.apiEndPoint;
@Component({
  selector: 'app-teamscard',
  templateUrl: './teamscard.component.html',
  styleUrls: ['./teamscard.component.scss'],
})
export class TeamscardComponent implements OnInit {
  @Input() isManage: boolean = false;
  @Input() checkedIn: boolean = false;
  @Input() data;
  @Output() onReload = new EventEmitter();
  @Output() refresh = new EventEmitter();

  constructor(
    private esportsTournamentService: EsportsTournamentService,
    private eSportsToastService: EsportsToastService
  ) {}

  ngOnInit(): void {}

  updateParticipant(id, status) {
    let data = !this.checkedIn
      ? { participantStatus: status }
      : { checkedIn: true };
    this.esportsTournamentService
      .updateParticipant(API, id, data)
      .subscribe((res) => {
        // if (res.success) { this.onReload.emit(true); }
        if (res?.success) {
          this.onReload.emit(true);
          this.refresh.emit(id);
          this.eSportsToastService.showSuccess(res?.message);
        } else {
          this.eSportsToastService.showSuccess(res?.message);
        }
      });
  }
}
