import { Component, OnChanges, OnInit } from '@angular/core';
import { ProfileService } from './../../../core/service/profile.service';
import { AppHtmlRoutes } from '../../../app-routing.model';
import { environment } from '../../../../environments/environment';
import {
  EsportsParticipantService,
  EsportsToastService,
  EsportsTournamentService,
  EsportsUserService,
  IPagination,
  ITournament,
  IUser,
} from 'esports';
import { TranslateService } from '@ngx-translate/core';
import {
  InfoPopupComponent,
  InfoPopupComponentData,
  InfoPopupComponentType,
} from '../../../shared/popups/info-popup/info-popup.component';
import { MatDialog } from '@angular/material/dialog';
import { HttpParams } from '@angular/common/http';
import { TournamentService } from '../../../core/service';
const API = environment.apiEndPoint;

@Component({
  selector: 'app-participants-requests',
  templateUrl: './participants-requests.component.html',
  styleUrls: ['./participants-requests.component.scss'],
})
export class ParticipantsRequestsComponent implements OnInit, OnChanges {
  tournament: ITournament;
  currentTab: string;
  user: IUser;

  AppHtmlRoutes = AppHtmlRoutes;
  active = 1;
  nextId: number = 1;
  requestsData = [];
  showLoader: boolean = true;
  params: any = {};
  paginationDetails: any;
  pageSizeOptions = environment.pageSizeOptions;

  paginationData = {
    page: 1,
    limit: 10,
    sort: 'startDate',
  };

  participantPage: IPagination = {
    activePage: 1,
    totalItems: 1,
    maxSize: 10,
    itemsPerPage: 100,
  };
  selectedTeam = null;
  showRegCloseBtn = false;
  isProcessing: boolean = false;
  participantData: any;
  isRegClosed = false;
  allApprovedParticipants: any;
  allUnCheckedParticipants: any;
  isTournamentStarted = false;

  constructor(
    private profileService: ProfileService,
    private esportsTournamentService: EsportsTournamentService,
    private userService: EsportsUserService,
    private participantService: EsportsParticipantService,
    private translateService: TranslateService,
    private matDialog: MatDialog,
    private eSportsToastService: EsportsToastService,
    private tournamentService: TournamentService
  ) {}

  ngOnInit(): void {
    this.setData();
  }

  ngOnChanges() {}

  setData() {
    this.currentTab = 'pendingParticipants';
    this.esportsTournamentService.manageTournament.subscribe((data) => {
      if (data) {
        this.isRegClosed = data.isRegistrationClosed;
        this.isTournamentStarted =
          new Date(data?.startDate).getTime() <= Date.now();
        this.tournament = data;
        if (this.currentTab && this.tournament?._id) {
          this.selectedTeam = null;
          this.setParticipant(this.participantPage?.activePage);
        }

        if (this.tournament?._id) {
          this.isRegClosed = this.tournament?.isRegistrationClosed;

          if (this.tournament?.regStartDate && this.tournament?.regEndDate) {
            const currentDate = new Date();
            const registrationStartDt = new Date(this.tournament?.regStartDate);
            const registrationEndDt = new Date(this.tournament?.regEndDate);

            /** Check current date is lie between registartion start date and registration end date */
            const withInRange =
              registrationEndDt > currentDate &&
              currentDate > registrationStartDt;
            this.showRegCloseBtn = withInRange;
          }
        }
      }
    });
    this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.user = data;
      }
    });
  }

  setParticipant(page: number = 1) {
    switch (this.currentTab) {
      case 'pendingParticipants':
        this.fetchParticipantByStatus(0, page);
        break;
      case 'approvedParticipants':
        this.fetchParticipantByStatus(1, page);
        break;
      case 'rejectedParticipants':
        this.fetchParticipantByStatus(2, page);
        break;
      default:
        this.fetchParticipantByStatus(0, page);
        break;
    }
  }

  navChanges = (e) => {
    this.nextId = e.nextId;
    switch (this.nextId) {
      case 1:
        this.currentTab = 'pendingParticipants';
        this.setParticipant();
        break;
      case 2:
        this.currentTab = 'rejectedParticipants';
        this.setParticipant();
        break;
      case 3:
        this.currentTab = 'uncheckedParticipants';
        this.fetchAllApprovedParticipants();
      case 4:
        this.currentTab = 'approvedParticipants';
        this.setParticipant();
        break;
    }
  };

  fetchParticipantByStatus = async (type: number, page = 1) => {
    try {
      if (this.isProcessing) return;
      this.isProcessing = true;
      const { data } = await this.participantService.fetchParticipantByStatus(
        API,
        type,
        this.tournament._id,
        page
      );
      this.participantPage.itemsPerPage = data?.limit;
      this.participantPage.totalItems = data?.totalDocs;
      this.participantData = data;
      this.paginationDetails = data;
      this.paginationData.limit = this.paginationDetails.limit;
      this.paginationData.page = this.paginationDetails.page;
      this.isProcessing = false;
    } catch (error) {
      this.isProcessing = false;
      this.eSportsToastService.showError(
        error?.error?.message || error?.message
      );
    }
  };

  pageChanged(page): void {
    this.paginationData.page = page.pageIndex + 1;
    this.paginationData.limit = page.pageSize;
    this.navChanges({ nextId: this.nextId });
  }
  onReload = (a) => {
    a ? this.setParticipant() : '';
    // this.active == 3 ? this.fetchAllApprovedParticipants() : '';
  };

  refresh(id) {
    this.participantData.forEach((e, i) => {
      if (e._id == id) {
        this.participantData.splice(i, 1);
      }
    });
  }
  currentPage(page: number) {
    this.setParticipant(page);
  }

  async toggleRegistration() {
    const data: InfoPopupComponentData = {
      ...this.popUpTitleAndText(this.tournament),
      type: InfoPopupComponentType.confirm,
      btnText: this.translateService.instant('TOURNAMENT.CONFIRM'),
    };

    const confirmed = await this.matDialog
      .open(InfoPopupComponent, { data })
      .afterClosed()
      .toPromise();
    if (confirmed) {
      try {
        this.isProcessing = true;
        await this.esportsTournamentService.updateTournament(
          {
            isRegistrationClosed: this.isRegClosed,
          },
          this.tournament._id
        );
        this.isProcessing = false;
        this.eSportsToastService.showSuccess(
          this.translateService.instant(
            this.isRegClosed ? 'HOME.UPCOMING_SLIDER.REGISTRATION_CLOSE' : 'HOME.UPCOMING_SLIDER.REGISTRATION_OPEN'
          )
        );
      } catch (error) {
        this.isProcessing = false;
        this.eSportsToastService.showError(
          this.translateService.instant('ADMIN.ERROR.FAIL')
        );
      }
    } else {
      this.isRegClosed = !this.isRegClosed;
    }
  }
  popUpTitleAndText = (data) => {
    if (data?._id) {
      if (this.isRegClosed) {
        return {
          title: this.translateService.instant(
            'TOURNAMENT.CLOSE_REGISTRATION_HEADER'
          ),
          text: this.translateService.instant('TOURNAMENT.CLOSE_REGISTRATION'),
        };
      } else {
        return {
          title: this.translateService.instant(
            'TOURNAMENT.OPEN_REGISTRATION_HEADER'
          ),
          text: this.translateService.instant('TOURNAMENT.OPEN_REGISTRATION'),
        };
      }
    }
  };

  /** Get all approved participants of the tournament  */
  fetchAllApprovedParticipants = async () => {
    try {
      let params = new HttpParams();
      params = params.append(
        'query',
        JSON.stringify({
          tournamentId: this.tournament?._id,
          checkedIn: 'false',
        })
      );
      params = params.set(
        'select',
        'checkedIn,teamName,seed,participantType,teamMembers,matchWin,name'
      );
      const participants = await this.tournamentService
        .getParticipants(params)
        .toPromise();
      this.allApprovedParticipants = participants.data.map((obj, index) => {
        const record = obj;
        record.reg = index + 1;
        return record;
      });
      this.allUnCheckedParticipants = this.allApprovedParticipants.filter(
        (item: any) => {
          return item.participantStatus == 'approved' && !item.checkedIn;
        }
      );
    } catch (error) {
      this.eSportsToastService.showError(error.message);
    } finally {
    }
  };
}
