import { Component, OnInit, Input } from '@angular/core';
import { AppHtmlRoutes } from '../../../app-routing.model';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  @Input() currentPageLink: string;
  @Input() title: string;
  AppHtmlRoutes = AppHtmlRoutes;
  ngOnInit(): void {  }
}
