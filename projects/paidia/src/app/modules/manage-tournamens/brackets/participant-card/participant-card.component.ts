import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {TournamentService} from '../../../../core/service';
import { EsportsToastService} from 'esports';
@Component({
  selector: 'app-participant-card',
  templateUrl: './participant-card.component.html',
  styleUrls: ['./participant-card.component.scss']
})
export class ParticipantCardComponent implements OnInit {
  @Input() isManage: boolean = false;
  @Input() index;
  @Input() data;
  @Input() allApprovedParticipants_length;
  @Input() tournament;
  @Output() manualSeedValidation = new EventEmitter();
  @Output() refresh= new EventEmitter();
  constructor( 
    private tournamentService: TournamentService,
    private eSportsToastService: EsportsToastService) { }

  ngOnInit(): void {

  }

  manualSeedValidationn(index, _id, value){
    this.manualSeedValidation.emit(
      {
        index:index,
         _id:_id,
         value:value
      }
    )
  }

  reject(d){
    const data = {
      participantStatus: 'rejected',
    }
    this.tournamentService.updateParticipant(d?._id, data).subscribe(res => {

      if (res?.success) {
        this.eSportsToastService.showSuccess(res?.message);
        this.refresh.emit(d?._id)
      
      } else {
        this.eSportsToastService.showInfo(res?.message);
      }
    })
  }
}
