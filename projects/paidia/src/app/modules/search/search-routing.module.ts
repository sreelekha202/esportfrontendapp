import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SearchAllComponent } from './search-all/search-all.component';
import { SearchArticleComponent } from './search-article/search-article.component';
import { SearchComponent } from './search.component';
import { SearchTournamentsComponent } from './search-tournaments/search-tournaments.component';
import { SearchVideoComponent } from './search-video/search-video.component';
import { SearchUserComponent } from './search-user/search-user.component';

const routes: Routes = [
  {
    path: '',
    component: SearchComponent,
    children: [
      {
        path: '',
        component: SearchAllComponent,
        data: {
          tags: [
            {
              name: 'title',
              content:
                'Paidia.gg supports a multitude of esports games. Find specific game tournaments, articles, news, players, teams, tournament organizers and more!',
            },
          ],
          title: 'Search |  Paidia.gg',
        },
      },
      { path: 'tournament', component: SearchTournamentsComponent },
      { path: 'article', component: SearchArticleComponent },
      { path: 'video', component: SearchVideoComponent },
      { path: 'user', component: SearchUserComponent },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SearchRoutingModule {}
