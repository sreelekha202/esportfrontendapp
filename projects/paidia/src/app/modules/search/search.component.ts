import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
} from '@angular/core';

import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { FormBuilder } from '@angular/forms';

import { AppHtmlRoutes } from '../../app-routing.model';
import {
  EventProperties,
  IPagination,
  EsportsUserService,
  EsportsGtmService,
  EsportsHomeService,
} from 'esports';

export enum OngoingTournamentFilter {
  upcoming,
  ongoing,
  completed,
  all,
}
import { filter, map, sampleTime } from 'rxjs/operators';
import { Subscription, zip } from 'rxjs';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SearchComponent implements OnInit, OnDestroy {
  text: string;
  isAllPage: boolean;
  activeRoute: string;
  AppHtmlRoutes = AppHtmlRoutes;
  OngoingTournamentFilter = OngoingTournamentFilter;
  tournamentFilter: OngoingTournamentFilter = OngoingTournamentFilter.all;
  page: IPagination;
  paginationData = {
    page: 1,
    limit: 8,
    sort: '-startDate',
  };

  private sub: Subscription;
  private sub1: Subscription;
  private searchSubscription: Subscription;
  noOfSearches: number = 0;

  constructor(
    public translate: TranslateService,
    public router: Router,
    public activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private homeService: EsportsHomeService,
    private gtmService: EsportsGtmService,
    private userService: EsportsUserService,
    private changeDetectorRef: ChangeDetectorRef
  ) {
    this.initSubscription();
  }

  ngOnInit(): void {
    // TODO: find why we have this bug, there some issue deeper
    // fix select title on init page load (refresh rtc.)
    setTimeout(() => (this.activeRoute = this.router.url));
    this.onSearchCategory(AppHtmlRoutes.search);
    this.searchSubscribe();
  }

  ngOnDestroy(): void {
    this.homeService.updateSearchParams(' ', this.paginationData);
    this.sub.unsubscribe();
    this.sub1.unsubscribe();
    this.searchSubscription?.unsubscribe();
  }

  onSearchCategory(path: AppHtmlRoutes): void {
    this.activeRoute = path;
    this.tournamentFilter = OngoingTournamentFilter.all;
    this.handleTournamentStatusChange();
    switch (path) {
      case AppHtmlRoutes.searchArticle:
        this.pushGTMTags('Select_Search_Article_Section');
        break;
      case AppHtmlRoutes.searchTournament:
        this.pushGTMTags('Select_Search_Tournament_Section');
        break;
      case AppHtmlRoutes.searchVideo:
        this.pushGTMTags('Select_Search_Video_Section');
        break;
      default:
        this.pushGTMTags('Select_Search_All_Section');
        break;
    }
  }

  /**
   * Function will update filter value to search service.
   */
  handleTournamentStatusChange() {
    this.homeService.updateTournamentStatusFilter(this.tournamentFilter);
  }

  private initSubscription(): void {
    this.sub = this.activatedRoute.queryParamMap.subscribe((e) => {
      if (e.get('text')) {
        this.text = e.get('text');
        this.homeService.updateSearchParams(this.text, this.paginationData);
        this.homeService.searchTournament();
        this.homeService.searchArticle();
        this.homeService.searchVideo();
        this.userService.searchUsers(this.text).toPromise();
      }
    });

    this.sub1 = this.router.events
      .pipe(filter((event) => event instanceof NavigationEnd))
      .subscribe((event: NavigationEnd) => {
        const shouldContainTwoOrMoreSlashes = new RegExp('(?:.*?/){2}');
        this.isAllPage = !Boolean(
          shouldContainTwoOrMoreSlashes.test(event.url)
        );
      });
  }

  searchSubscribe() {
    this.searchSubscription = zip(
      this.homeService.searchedTournament,
      this.homeService.searchedArticle,
      this.homeService.searchedVideo
    )
      .pipe(
        sampleTime(1000),
        map(([tournament, article, video]) => ({ tournament, article, video }))
      )
      .subscribe((result) => {
        this.noOfSearches++;
        let totalNoOfResults = +(
          +result.article['totalDocs'] +
          +result.tournament['totalDocs'] +
          +result.video['totalDocs']
        );

        let eventProperties: EventProperties = {
          fromPage: this.router.url,
          searchTerms: this.text || '',
          noOfArticles: result?.article?.['totalDocs'],
          noOfResults: totalNoOfResults,
          noOfTournaments: result?.tournament?.['totalDocs'],
          noOfVideos: result?.video?.['totalDocs'],
          noOfSearches: this.noOfSearches,
          searchCharacterLength: this.text?.length || 0,
        };
        this.pushGTMTags('Search', eventProperties);
      });
  }

  pushGTMTags(eventName: string, eventData: EventProperties = null) {
    this.gtmService.pushGTMTags(eventName, eventData);
  }
}
