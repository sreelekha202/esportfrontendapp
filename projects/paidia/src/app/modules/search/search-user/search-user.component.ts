import { Component, OnInit, Inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { BehaviorSubject } from 'rxjs';
import { ChangeDetectorRef } from '@angular/core';
import {
  IPagination,
  EsportsToastService,
  EsportsSocialService,
  EsportsLeaderboardService,
  EsportsUserService,
  EsportsHomeService,
  GENDER,
  IUser,
} from 'esports';
import { onLoadImgError, TYPE_IMG_LOAD } from '../../../shared/comon';
import { AppHtmlProfileRoutes } from '../../../app-routing.model';
@Component({
  selector: 'app-search-user',
  templateUrl: './search-user.component.html',
  styleUrls: ['./search-user.component.scss'],
})
export class SearchUserComponent implements OnInit {
  public onLoadImgError: Function = onLoadImgError;
  public typeImgLoad = TYPE_IMG_LOAD;
  public users: Array<IUser> = [];
  public isLoadingUserDetail: boolean = false;
  public isFollow: boolean = false;
  public userInfo: any = '';
  public AppHtmlProfileRoutes = AppHtmlProfileRoutes;
  constructor(
    @Inject('env') private env,
    public _homeService: EsportsHomeService,
    public _userService: EsportsUserService,
    public _activatedRoute: ActivatedRoute,
    private _toastService: EsportsToastService,
    private _socialService: EsportsSocialService,
    private _leaderboardService: EsportsLeaderboardService,
    private _changeDetectorRef: ChangeDetectorRef,
    private _translateService: TranslateService,
    public router: Router
  ) {}
  userInfoEmitter$ = new BehaviorSubject<any>(this.userInfo);
  isFollowEmitter$ = new BehaviorSubject<boolean>(this.isFollow);
  page: IPagination;
  paginationData = { page: 1, limit: 6, sort: '-startDate' };
  text;
  idUser: any;
  showUser: string = '';
  public boxGames = [
    {
      img: '../../../../assets/images/Profile/apex.png',
      text_1: 'xxinfinity01',
      text_2: 'Apex Legends',
    },
    {
      img: '../../../../assets/images/Profile/dota.png',
      text_1: 'moondust',
      text_2: 'Dota 1',
    },
    {
      img: '../../../../assets/images/Profile/fortnite.png',
      text_1: 'xxinfinity01',
      text_2: 'Fortnite',
    },
  ];

  ngOnInit(): void {
    this._activatedRoute.queryParamMap.subscribe((e) => {
      if (e.get('text')) {
        this.text = e.get('text');
      }
    });
    this.getSearchUserInfoDefault();
  }

  private getSearchUserInfoDefault() {
    this._userService.searchedUser.subscribe((res: Array<IUser>) => {
      if (!res) throw Error();
      this.userInfo = res[0];
    });
  }

  public closeUserProfile() {
    this.userInfo = '';
  }

  public showDetail(userId) {
    this.isLoadingUserDetail = true;
    this.userInfo = '';
    this.isFollow = false;
    this.showUser = userId;
    this.getProfile(userId);
    this.followStatus(userId);
  }

  public onHandleFollowUser(userId) {
    this.isFollow ? this.unFollow(userId) : this.follow(userId);
  }

  private getProfile(userId: string) {
    this._socialService.getUserProfile(userId).subscribe((res: any) => {
      if (res?.data) {
        this.isLoadingUserDetail = false;
        this.userInfo = res?.data;
        this.userInfoEmitter$.next(this.userInfo);
        this._changeDetectorRef.markForCheck();
      }
    });
  }

  private follow(userId: string) {
    this._leaderboardService
      .followUser(this.env.apiEndPoint, userId)
      .subscribe((res) => {
        if (res.success) {
          this._toastService.showSuccess(
            this._translateService.instant('SOCIAL_FEED.MESSAGES.FOLLOW')
          );
          this.isFollow = true;
          this.isFollowEmitter$.next(this.isFollow);
          this._changeDetectorRef.markForCheck();
        }
      });
  }

  private unFollow(userId: string) {
    this._leaderboardService
      .unfollowUser(this.env.apiEndPoint, userId)
      .subscribe((res) => {
        if (res.success) {
          this._toastService.showSuccess(
            this._translateService.instant('SOCIAL_FEED.MESSAGES.UNFOLLOW')
          );
          this.isFollow = false;
          this.isFollowEmitter$.next(this.isFollow);
          this._changeDetectorRef.markForCheck();
        }
      });
  }

  private followStatus(userId: string) {
    this._leaderboardService
      .checkFollowStatus(this.env.apiEndPoint, userId)
      .subscribe((res: any) => {
        const status = res?.data[0].status;
        if (status) {
          this.isFollow = !!status;
          this.isFollowEmitter$.next(this.isFollow);
          this._changeDetectorRef.markForCheck();
        }
      });
  }

  public getGender(gender: GENDER, customPronoun) {
    let pronoun: string;
    switch (gender) {
      case GENDER.HE:
        pronoun = GENDER.HE.toUpperCase();
        break;
      case GENDER.SHE:
        pronoun = GENDER.SHE.toUpperCase();
        break;
      case GENDER.THEY:
        pronoun = GENDER.THEY.toUpperCase();
        break;
      default:
        return customPronoun;
    }
    return this._translateService.instant(`SOCIAL_FEED.USER.GENDER.${pronoun}`);
  }
}
