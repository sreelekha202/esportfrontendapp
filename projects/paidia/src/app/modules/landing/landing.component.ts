import { Component, OnInit, ViewChild } from '@angular/core';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
import {
  AppHtmlProfileRoutes,
  AppHtmlRoutes,
  AppHtmlRoutesLoginType,
} from '../../app-routing.model';
import { HomeService } from '../../core/service';
import { NgbCarousel, NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';
import {
  EsportsTimezone,
  EsportsUserService,
  EsportsGtmService,
} from 'esports';
import { Router } from '@angular/router';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss'],
  providers: [NgbCarouselConfig],
})
export class LandingComponent implements OnInit {
  AppHtmlProfileRoutes = AppHtmlProfileRoutes;
  AppHtmlRoutes = AppHtmlRoutes;
  AppHtmlRoutesLoginType = AppHtmlRoutesLoginType;

  constructor(
    private router: Router,
    private gtmService: EsportsGtmService,
    private userService: EsportsUserService
  ) {}

  ngOnInit(): void {
    this.getCurrentUserDetails();
  }

  getCurrentUserDetails() {
    this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.router.navigateByUrl('/home');
      }
    });
  }

  homepagefunction() {
    //  this.router.navigateByUrl('/home')
  }

  pushGTMTags(eventName: string, eventData = null) {
    this.gtmService.pushGTMTags(eventName, eventData)
  }

}
