import { GameService } from './../../core/service/game.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { AppHtmlGetMatchRoutes } from '../../app-routing.model';
import { SwiperConfigInterface, SwiperComponent } from 'ngx-swiper-wrapper';
import { ActivatedRoute, Router } from '@angular/router';
import { EsportsGameService, EsportsSeasonService,EsportsGtmService, EventProperties } from 'esports';
import { environment } from '../../../environments/environment';
const API = environment.apiEndPoint;
@Component({
  selector: 'app-matchmaking',
  templateUrl: './matchmaking.component.html',
  styleUrls: ['./matchmaking.component.scss'],
})
export class MatchmakingComponent implements OnInit {
  @ViewChild(SwiperComponent, { static: false }) compRef?: SwiperComponent;
  showLoader = true;
  AppHtmlGetMatchRoutes = AppHtmlGetMatchRoutes;
  games = [];
  seassons = [];
  selectedGameIndex: number = 0;
  public config: SwiperConfigInterface = {
    breakpoints: {
      320: {
        slidesPerView: 6,
        spaceBetween: 16,
        autoHeight: false,
      },
    },
  };
  createMatchMaking: any;
  matchmakingDetails: any;

  constructor(
    private esportsSeasonService: EsportsSeasonService,
    private gameService: EsportsGameService,
    private gameService2: GameService,
    private gtmService: EsportsGtmService,
    private router: Router
  ) // private activatedRoute: ActivatedRoute
  {
    // if (this.activatedRoute.snapshot.paramMap.get('id')) {
    // let id = this.activatedRoute.snapshot.paramMap.get('id');
    // if (location.path() == `/inbound/put-away/add/${id}`) {}
  }

  ngOnInit(): void {
    this.matchmakingDetails = this.gameService2.createMatchMaking.subscribe(
      (data) => {
        if (data) {
          this.createMatchMaking = data;
        }
      }
    );
    this.getGames();
    this.getSeasons();
  }

  getGames() {
    this.showLoader = true;
    this.gameService.getGames(API).subscribe(
      (res) => {
        this.games = res?.data;
        this.showLoader = false;
      },
      (err) => {
        this.showLoader = false;
      }
    );
  }

  getSeasons() {
    this.esportsSeasonService
      .getSeasons(
        API,
        'isActive=true&fields=isFeatured,name,slug,endDate,startDate,image,duration&sort=-createdOn&page=1&limit=8'
      )
      .subscribe((res) => {
        this.seassons = res?.data?.data?.docs;
      });
  }

  prevSlide() {
    this.compRef.directiveRef.prevSlide();
  }

  nextSlide() {
    this.compRef.directiveRef.nextSlide();
  }

  ngOnDestroy(): void {
    if (this.matchmakingDetails) this.matchmakingDetails.unsubscribe();
  }

  onGameSelect(selectedGame) {
    if (selectedGame) {
      this.pushGTMTags('Select_Matchmaking_Game', { gameTitle: selectedGame?.name })
      this.gameService2.createMatchMakingSubject.next({
        ...this.createMatchMaking,
        selectedGame: selectedGame,
      });
      this.router.navigateByUrl('/get-match');
    }
  }

  pushGTMTags(eventName: string, eventData: EventProperties = null) {
    this.gtmService.pushGTMTags(eventName, eventData);
  }

}
