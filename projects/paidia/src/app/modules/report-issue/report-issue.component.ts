import { Component, OnInit, Input,Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { S3UploadService } from 'projects/paidia/src/app/core/service';
import { UserReportsService, EsportsUserService, EsportsToastService, EsportImageService } from 'esports';
import { Subscription } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { Location } from '@angular/common';
import { environment } from '../../../environments/environment';
const API = environment.apiEndPoint;
import {  Router, ActivatedRoute } from '@angular/router';
import { TournamentService } from '../../../app/core/service';
@Component({
  selector: 'app-report-issue',
  templateUrl: './report-issue.component.html',
  styleUrls: ['./report-issue.component.scss']
})
export class ReportIssueComponent implements OnInit {
  type;
  form: FormGroup;
  addForm: FormGroup;
  base64textString;
  briefDescription;
  descrip1;
  user;
  isLoaded = false;
  matchId;
  tournamentId;
  userSubscription: Subscription;
  showLoader: boolean;
  id;
  imgurl = '';
  tReport: any;
  tournament: any;
  constructor(private s3UploadService: S3UploadService,
    private eSportsToastService: EsportsToastService,
    private userReportsService: UserReportsService,
    private translateService: TranslateService,
    private location: Location,
    private userService: EsportsUserService,
    private activeRoute: ActivatedRoute,
    private tournamentService: TournamentService,
    ) { }
  ngOnInit(): void {
    this.getCurrentUserDetails();
    this.tReport = JSON.parse(localStorage.getItem('t_report'));
  //  const { matchId, tournamentId } = this.activeRoute.snapshot.params;
    this.activeRoute.queryParamMap.subscribe(params => {
                this.matchId=params.get('matchId')
                this.tournamentId=params.get('tournamentId')

});


  }
  getCurrentUserDetails() {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.isLoaded = true;
        this.user = data;
      }
    });
  }

  goBack() {
    this.location.back();
  }

  selectType(reportType) {
    this.type = reportType;
  }

  datatoimg(data) {
    this.imgurl = data;
  }

  reportsubmit(descrip){


this.briefDescription = descrip
    if (
      this.briefDescription &&
      this.type &&
      this.matchId &&
      this.user &&
      this.tournamentId
      ) {
      this.isLoaded = false;
      const data = {
        createrId: this.user?._id, // userId
        tournamentId:  this.tournamentId,
        matchId: this.matchId,
        reportType: this.type,
        reportText: this.briefDescription,
        reportImage:  this.imgurl
      };

      this.userReportsService.createReport(API, data).subscribe(
          (res: any) => {

            if (res && res?.data) {
              this.eSportsToastService.showSuccess(res?.message)
                this.location.back();
            }
            this.isLoaded = true;
          },
          (err) => {
            this.isLoaded = true;
            this.eSportsToastService.showError(err?.error?.message)
          }
        );
    } else if (!this.type) {
      this.eSportsToastService.showError(
        this.translateService.instant('USERREPORT REQUIRED TYPE FIELD')
      );
    } else {
      this.eSportsToastService.showError(
        this.translateService.instant(
          'USERREPORT REQUIRED COMMENT FIELD'
        )
      );
    }
  }
}
