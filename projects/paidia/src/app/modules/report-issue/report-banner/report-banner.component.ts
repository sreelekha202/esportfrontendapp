import { Component, OnInit, Input, Output,EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { EsportsToastService } from 'esports';
import { S3UploadService } from 'projects/paidia/src/app/core/service';

@Component({
  selector: 'app-report-banner',
  templateUrl: './report-banner.component.html',
  styleUrls: ['./report-banner.component.scss']
})
export class ReportBannerComponent implements OnInit {
  @Input() customFormControlName: string;
  @Input() customFormGroup: FormGroup;

  @Input() title: string;
  @Input() subtitle: string;

  @Input() dimension;
  @Input() size;
  @Input() type;

  @Input() id;
  @Input() required: boolean = false;
  @Output() imgurltosave = new EventEmitter();

  isProcessing: boolean = false;
  imgurl: any = '';
  selectedFile: any;
  base64textString: any;
  constructor(
    private s3UploadService: S3UploadService,
    private eSportsToastService: EsportsToastService
  ) { }
  ngOnInit(): void { }
  eventChange = async (event) => {
    try {

      this.isProcessing = true;

      const upload: any = await this.s3UploadService.validateAndUploadImage(
        event,
        this.dimension,
        this.size,
        this.type
      );
        this.imgurl = upload.data[0].Location;
      this.selectedFile = event.target.files[0];
      var reader = new FileReader();
      reader.onload = this.handleReaderLoaded.bind(this);
      reader.readAsBinaryString(this.selectedFile);
        
      //  this.customFormGroup.get(this.customFormControlName).setValue(upload);
      // this.customFormGroup
      //   .get(this.customFormControlName)
      //   .setValue(upload?.data?.Location);
      // this.toastService.showSuccess(upload?.message);
      // this.isProcessing = false;
    } catch (error) {
      this.isProcessing = false;
      const errorMessage = error?.error?.message || error?.message;
      this.eSportsToastService.showError(errorMessage);
    }
  };
  handleReaderLoaded(readerEvt) {
    var binaryString = readerEvt.target.result;
    this.base64textString = 'data:image/png;base64,' + btoa(binaryString);
    this.imgurltosave.emit(this.base64textString)
  }

}
