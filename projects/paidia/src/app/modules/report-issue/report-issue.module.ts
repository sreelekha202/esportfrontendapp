import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReportIssueComponent } from './report-issue.component';

import { RouterModule, Routes } from '@angular/router';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatFormFieldModule } from '@angular/material/form-field';
import { ReportBannerComponent } from './report-banner/report-banner.component';

export const routes: Routes = [
  { path:'',component: ReportIssueComponent}
]

@NgModule({
  declarations: [
    ReportIssueComponent,
    ReportBannerComponent
  ],
  imports: [
    CommonModule, RouterModule.forChild(routes),
    MatCheckboxModule,
    MatFormFieldModule,

  ]
})
export class ReportIssueModule { }
