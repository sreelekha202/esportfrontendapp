import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ManageTeamRoutingModule } from "./manage-team-routing.module";
import { CoreModule } from "../../core/core.module";
import { FormComponentModule } from "../../shared/components/form-component/form-component.module";
import { ManageTeamComponent } from "./manage-team.component";
import { HeaderInfoModule } from "../../shared/components/header-info/header-info.module";
import { SharedModule } from "../../shared/modules/shared.module";

@NgModule({
  declarations: [ManageTeamComponent],
  imports: [
    CommonModule,
    CoreModule,
    ManageTeamRoutingModule,
    FormComponentModule,
    SharedModule,
    HeaderInfoModule,
  ],
})
export class ManageTeamModule {}
