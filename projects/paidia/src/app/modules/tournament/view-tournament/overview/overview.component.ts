import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { EsportsLanguageService } from 'esports';

//  '../view-tournament.component.scss'
@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.scss'],
})
export class OverviewComponent implements OnInit, OnChanges {
  @Input() tournamentDetails;

  information = [];
  rules = [];
  isActive: string = 'registeration';
  prizeImg = [
    'assets/icons/prizes/gold.png',
    'assets/images/Tournament/Prize-2.png',
    'assets/images/Tournament/Prize-3.png',
  ];

  today = new Date();
  tournament_startDate;
  tournament_endDate;

  timeZone;

  dates: any = {};

  constructor(public languageService: EsportsLanguageService) {}

  ngOnInit(): void {}

  ngOnChanges() {
    if (this.tournamentDetails) {
      const {
        startDate,
        endDate,
        regStartDate,
        regEndDate,
        checkInStartDate,
      } = this.tournamentDetails;

      const _startDate = new Date(startDate);
      const _endDate = new Date(endDate);
      const _regStartDate = new Date(regStartDate);
      const _regEndDate = new Date(regEndDate);

      this.dates['startDate'] = _startDate.getTime();
      this.dates['endDate'] = _endDate.getTime();
      this.dates['regStartDate'] = _regStartDate.getTime();
      this.dates['regEndDate'] = _regEndDate.getTime();
      this.dates['checkInStartDate'] = new Date(checkInStartDate).getTime();

      this.tournament_startDate = _startDate;
      this.tournament_endDate = _endDate;
    }
  }

}
