import { NgModule } from "@angular/core";
import { FaIconLibrary } from "@fortawesome/angular-fontawesome";
import { faTrash, faShareAlt } from "@fortawesome/free-solid-svg-icons";
import { ViewTournamenttRoutingModule } from "./view-tournament-routing.module";
import { ViewTournamentComponent } from "./view-tournament.component";
import { OverviewComponent } from "./overview/overview.component";
import { ParticipantsComponent } from "./participants/participants.component";
import { BracketsComponent } from "./brackets/brackets.component";
import { JoinButtonComponent } from "./join-button/join-button.component";
import { PaymentComponent } from "./join-participant/payment/payment.component";
import { InfoComponent } from "./info/info.component";
import { FormComponentModule } from "../../../shared/components/form-component/form-component.module";
import { PaymentModule } from "../../payment/payment.module";
import { CountdownModule } from "ngx-countdown";
import { ClipsViewComponent } from './clips-view/clips-view.component';
import { RulesViewComponent } from './rules-view/rules-view.component';
import { CardteamComponent } from './participants/compponent/cardteam/cardteam.component';
import { RouterBackModule } from "../../../shared/directives/router-back.module";
import { SharedModule } from "../../../shared/modules/shared.module";
import { PrizePoollModule } from "../pipe/prize-pooll.module";
import { JoinParticipantComponent } from "./join-participant/join-participant.component";
import { BracketTreeModule } from "../../../shared/components/bracket-tree/bracket-tree.module";
import { PayRegFeeComponent } from './pay-reg-fee/pay-reg-fee.component';
import { MatchViewComponent } from './match-view/match-view.component';
import { LadderMatchCardComponent } from './ladder-match-card/ladder-match-card.component';
import { HeaderPaidiaModule } from '../../../shared/components/header-paidia/header-paidia.module';

@NgModule({
  declarations: [
    ViewTournamentComponent,
    JoinParticipantComponent,
    OverviewComponent,
    ParticipantsComponent,
    BracketsComponent,
    InfoComponent,
    PaymentComponent,
    ClipsViewComponent,
    RulesViewComponent,
    CardteamComponent,
    JoinButtonComponent,
    PayRegFeeComponent,
    LadderMatchCardComponent,
    MatchViewComponent,
  ],
  imports: [
    ViewTournamenttRoutingModule,
    SharedModule,
    FormComponentModule,
    PrizePoollModule,
    PaymentModule,
    CountdownModule,
    RouterBackModule,
    BracketTreeModule,
    HeaderPaidiaModule,
  ],
  providers: [],
})
export class ViewTournamentModule {
  constructor(private library: FaIconLibrary) {
    library.addIcons(faTrash, faShareAlt);
  }
}
