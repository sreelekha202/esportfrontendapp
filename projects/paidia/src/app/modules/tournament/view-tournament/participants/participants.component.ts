import { HttpParams } from "@angular/common/http";
import {
  Component,
  OnInit,
  Input,
  OnChanges,
  SimpleChanges,
  ViewChild,
} from "@angular/core";
import { EsportsTournamentService, EsportsToastService } from 'esports';
import { ProfileService } from './../../../../core/service/profile.service';
import { environment } from '../../../../../environments/environment';
const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;
@Component({
  selector: "app-participants",
  templateUrl: "./participants.component.html",
  styleUrls: [
    "./participants.component.scss",
    "../view-tournament.component.scss",
  ],
})
export class ParticipantsComponent implements OnInit, OnChanges {
  @Input() isManage: boolean = false;
  @Input() data;
  @Input() tournamentId: string;
  @Input() tournament;
  approvedParticipants = [];
  isLoaded = false;
  selectedTeam = null;
  currentTab = "approvedParticipants"

  active = 1;
  nextId: number = 1;
  teamRequests = [];
  showLoader: boolean = true;


  constructor(
    private tournamentService: EsportsTournamentService,
    private toastService: EsportsToastService,
    private profileService: ProfileService
  ) { }

  ngOnInit(): void {
  }

  fetchAllApprovedParticipants = async () => {
    try {
      this.isLoaded = false;
      this.selectedTeam = false;

      let params = new HttpParams();
      params = params.append('?query', JSON.stringify({
        tournamentId: this.tournamentId,
      }));
      //   type: "1",

      const participants = await this.tournamentService
        .getParticipants(API,this.tournamentId)
        .toPromise();
      this.approvedParticipants = participants.data.docs;
      this.setParticipant();
      this.isLoaded = true;
    } catch (error) {
      this.isLoaded = true;
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  ngOnChanges(simpleChanges: SimpleChanges) {
    if (
      simpleChanges.hasOwnProperty("tournamentId") &&
      simpleChanges.tournamentId.currentValue
    ) {
      this.fetchAllApprovedParticipants();
    }
  }

  setParticipant() {
    this.currentTab === 'approvedParticipants' ? this.approvedParticipants : this.approvedParticipants = [];
  }


}
