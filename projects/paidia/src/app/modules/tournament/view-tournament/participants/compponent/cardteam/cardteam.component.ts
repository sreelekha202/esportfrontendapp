import { Component, OnInit, Input } from '@angular/core';
import { AppHtmlProfileRoutes } from 'projects/paidia/src/app/app-routing.model';
// import { AppHtmlRoutes } from '../../app-routing.model';

@Component({
  selector: 'app-cardteam',
  templateUrl: './cardteam.component.html',
  styleUrls: ['./cardteam.component.scss']
})
export class CardteamComponent implements OnInit {
  @Input() isManage: boolean = false;
  @Input() data;
  AppHtmlProfileRoutes = AppHtmlProfileRoutes;
  constructor() { }

  ngOnInit(): void {
  }

}
