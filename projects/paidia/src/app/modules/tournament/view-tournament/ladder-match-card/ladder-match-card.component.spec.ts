import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LadderMatchCardComponent } from './ladder-match-card.component';

describe('LadderMatchCardComponent', () => {
  let component: LadderMatchCardComponent;
  let fixture: ComponentFixture<LadderMatchCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LadderMatchCardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LadderMatchCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
