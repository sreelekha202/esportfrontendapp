import { Component, OnInit, Input, OnChanges, OnDestroy } from '@angular/core';
import {
  EsportsBracketService,
  EsportsToastService,
  EsportsTournamentService,
  EsportsUtilsService,
} from 'esports';
import { BracketService, TournamentService } from '../../../../core/service';

import { environment } from '../../../../../environments/environment';
const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;

@Component({
  selector: 'app-brackets',
  templateUrl: './brackets.component.html',
  styleUrls: ['./brackets.component.scss', '../view-tournament.component.scss'],
})
export class BracketsComponent implements OnInit, OnChanges, OnDestroy {
  @Input() tournament;
  @Input() participantId: string | null;
  apiLoaded: Array<boolean> = [];
  isLoaded = false;
  structure: any;

  constructor(
    private bracketService: EsportsBracketService,
    private toastService: EsportsToastService,
    private utilsService: EsportsUtilsService,
    private tournamentService: EsportsTournamentService
  ) {}

  ngOnInit(): void {}

  ngOnDestroy() {}

  ngOnChanges() {
    this.tournament = localStorage.getItem('tournamentDetails');
    // this.tournamentService.tournamentDetailsObs.subscribe((data) => {
    //   this.tournament = data;
    // });
    this.tournament = JSON.parse(this.tournament);
    if (this.tournament?._id) {
      this.fetchParticipantRegisterationStatus(API, this.tournament?._id);
      this.fetchBracket();
    }
  }

  fetchParticipantRegisterationStatus = async (API, id) => {
    try {
      this.apiLoaded.push(false);
      const { data } =
        await this.tournamentService.fetchParticipantRegistrationStatus(
          API,
          id
        );
      this.participantId = data?.participantId || null;
      this.apiLoaded.push(true);
    } catch (error) {
      this.apiLoaded.push(true);
      this.toastService.showError(error?.error?.message || error?.message);
    } finally {
      this.checkAllApiDataLoaded();
    }
  };

  /**
   * Fetch All Match If Seed Else Fetch Mock Structure Of The Tournament
   */
  fetchBracket = async () => {
    try {
      this.apiLoaded.push(false);
      if (
        this.tournament?.isSeeded &&
        ['single', 'double'].includes(this.tournament?.bracketType)
      ) {
        const response = await this.bracketService.fetchAllMatchesV3({
          tournamentId: this.tournament._id,
        });
        this.structure = this.bracketService.assembleStructure(response.data);
      } else if (!this.tournament?.isSeeded) {
        const payload = {
          bracketType: this.tournament?.bracketType,
          maximumParticipants: this.tournament?.maxParticipants,
          noOfSet: this.tournament?.noOfSet,
          ...(['round_robin', 'battle_royale'].includes(
            this.tournament?.bracketType
          ) && {
            noOfTeamInGroup: this.tournament?.noOfTeamInGroup,
            noOfWinningTeamInGroup: this.tournament?.noOfWinningTeamInGroup,
            noOfRoundPerGroup: this.tournament?.noOfRoundPerGroup,
            stageBracketType: this.tournament?.stageBracketType,
          }),
        };
        const response = await this.bracketService.generateBracket({
          tournamentId: this.tournament?._id,
        });
        this.structure = response.data;
      }
      this.apiLoaded.push(true);
    } catch (error) {
      this.apiLoaded.push(true);
      this.toastService.showError(error?.error?.error || error?.error?.message);
    } finally {
      this.checkAllApiDataLoaded();
    }
  };

  checkAllApiDataLoaded() {
    const startedApiCount = this.apiLoaded.filter((el) => !el).length;
    const finishedApiCount = this.apiLoaded.filter((el) => el).length;
    this.isLoaded = startedApiCount === finishedApiCount;
  }
}
