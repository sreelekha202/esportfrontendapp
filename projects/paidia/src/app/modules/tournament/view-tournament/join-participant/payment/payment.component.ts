import { Component, EventEmitter, Input, Output, OnInit } from "@angular/core";
import { EsportsToastService } from 'esports';
import { PlatformFeeService } from "../../../../../core/service";

@Component({
  selector: "app-payment",
  templateUrl: "./payment.component.html",
  styleUrls: ["./payment.component.scss"],
})
export class PaymentComponent implements OnInit {
  @Input() tournament;
  @Input() transaction;
  @Input() paymentMethods;
  @Output("submit") submit: EventEmitter<any> = new EventEmitter();
  @Output("getPaymentProgressStatus")
  getParentPaymentProgressStatus: EventEmitter<any> = new EventEmitter();
  selectPaymentType = "";
  isProccesing = true;
  isPaymentLoaded = false;
  entryFee = 0;
  platformFee;
  platformFeePercent;
  totalAmount;

  constructor(
    private toastService: EsportsToastService,
    private platformFeeService: PlatformFeeService
  ) {}

  ngOnInit(): void {
    this.calculateTotalAmount();
  }

  async getPlatformFee() {
    try {
      this.isProccesing = true;
      const res = (await this.platformFeeService.getPlatformFee())?.data;
      this.platformFeePercent = res.fee;
      this.isProccesing = false;
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
      this.isProccesing = false;
    }
  }

  calculateTotalAmount = async () => {
    await this.getPlatformFee();
    this.entryFee = this.tournament.regFee;
    this.platformFee = (this.entryFee * this.platformFeePercent) / 100;
    this.totalAmount = this.entryFee + this.platformFee;
    this.transaction.totalAmount = this.totalAmount;
  };

  getPaymentResponse = async (order) => {
    try {
      this.isProccesing = true;
      const payload = {
        tournamentId: this.tournament?._id,
        txnId: order?.id,
        provider: "paypal",
        regFeePaid: this.totalAmount,
      };

      this.submit.emit(payload);
      this.isProccesing = false;
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
      this.isProccesing = false;
    }
  };

  getPaymentProgressStatus = (status) => {
    this.getParentPaymentProgressStatus.emit(status);
  };
}
