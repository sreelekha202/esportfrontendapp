import { Component, OnInit, OnDestroy } from '@angular/core';
import {
  EsportsToastService,
  EsportsTransactionService,
  EsportsParticipantService,
  EsportsTournamentService,
  EsportsUserService,
  IUser,
} from 'esports';
import { TranslateService } from '@ngx-translate/core';
import { Router, ActivatedRoute } from '@angular/router';
import { environment } from '../../../../../environments/environment';
import { Subscription } from 'rxjs';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';
const API = environment.apiEndPoint;

@AutoUnsubscribe()
@Component({
  selector: 'app-pay-reg-fee',
  templateUrl: './pay-reg-fee.component.html',
  styleUrls: ['./pay-reg-fee.component.scss'],
})
export class PayRegFeeComponent implements OnInit, OnDestroy {
  selectedPaymentProvider = '';
  isProcessing = false;
  paymentMethods = [];
  paymentDetails;
  tournament;
  participant;
  participantId;
  tournamentId;
  tournamentSlug;
  currentUser: IUser;
  transaction;

  constructor(
    private toastService: EsportsToastService,
    private transactionService: EsportsTransactionService,
    private participantService: EsportsParticipantService,
    private tournamentService: EsportsTournamentService,
    private userService: EsportsUserService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private translateService: TranslateService
  ) {}

  async ngOnInit() {
    this.tournamentSlug = this.activatedRoute.snapshot.params.id || null;
    this.getUserDetails();
  }

  ngOnDestroy() {}

  getUserDetails() {
    this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.currentUser = data;
        this.initDataFetch();
      }
    });
  }

  async initDataFetch() {
    this.isProcessing = true;
    this.tournamentId = this.tournamentSlug.split('-').reverse()[0];
    await this.getTournamentDetails();
    await this.getParticipantDetails();
    await this.getPaymentDetails();
    this.isProcessing = false;
  }

  async getParticipantDetails() {
    const participantJoinStatus = await this.tournamentService.fetchParticipantRegistrationStatus(
      API,
      this.tournamentId
    );
    const participantStatus =
      participantJoinStatus?.['data']?.participantStatus;
    if (
      participantStatus == 'approved' ||
      participantStatus == 'already-registered'
    ) {
      const participantData = await this.participantService.getParticipantDetails(
        API,
        { tournamentId: this.tournamentId }
      );

      this.participant = participantData['data'];

      if (this.participant?.paymentStatus == 'completed') {
        this.toastService.showSuccess(this.translateService.instant('ERROR.REG_FEE_PAID'));
        this.router.navigate([`tournament/${this.tournamentSlug}`]);
      }
    } else {
      this.toastService.showInfo(this.translateService.instant('ERROR.REGISTER_TOUR_FIRST'));
      this.router.navigate([`tournament/${this.tournamentSlug}`]);
    }
  }

  async getTournamentDetails() {
    const data = await this.tournamentService.getTournamentBySlug(
      this.tournamentSlug
    );
    this.tournament = data['data'];
    if (!this.tournament.isPaid) {
      this.toastService.showError(this.translateService.instant('ERROR.INVALID_REG_FEE'));
      this.router.navigate([`tournament/${this.tournamentSlug}`]);
    }
  }

  async getPaymentDetails() {
    try {
      const payload = {
        purpose: 'participantRegistration',
        objectId: this.tournamentId,
      };
      this.paymentDetails = (
        await this.transactionService.getPaymentDetails(API, payload)
      ).data;
      this.paymentMethods = this.paymentDetails.providers;
    } catch (error) {
    }
  }

  selectProvider(paymentMethod) {
    this.selectedPaymentProvider = paymentMethod.provider;
    this.transaction = {
      totalAmount: paymentMethod.amount,
      currencyCode: paymentMethod.currencyCode,
    };
  }

  async getPaymentResponse(order) {
    this.isProcessing = true;
    const payload = {
      tournamentId: this.tournament?._id,
      participantId: this.participant?._id,
      orderId: order?.id,
      provider: 'paypal',
      purpose: 'participantRegistration',
      providerResponse: order,
    };

    let response = this.transactionService
      .createTransaction(API, payload)
      .then((response) => {
        this.toastService.showSuccess(
          this.translateService.instant('PAYMENTS.TRANSACTION_COMPLETE')
        );
        this.router.navigate([`tournament/${this.tournament?.slug}`]);
      })
      .catch((err) => {
        this.toastService.showError(
          this.translateService.instant('PAYMENTS.TRANSACTION_FAILURE')
        );
      })
      .finally(() => {
        this.isProcessing = false;
      });
  }

  getPaymentProgressStatus($event) {
    this.isProcessing = $event;
    this.toastService.showError(
      this.translateService.instant('PAYMENTS.TRANSACTION_FAILURE')
    );
  }
}
