import { Title } from '@angular/platform-browser';
import { TranslateService } from '@ngx-translate/core';
import { MatDialog } from '@angular/material/dialog';
import {
  Component,
  OnInit,
  Inject,
  PLATFORM_ID,
  ViewChild,
  OnChanges,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import 'rxjs/add/operator/filter';
import {
  InfoPopupComponent,
  InfoPopupComponentData,
  InfoPopupComponentType,
} from '../../../shared/popups/info-popup/info-popup.component';
import {
  LadderPopupComponent,
  LadderPopupComponentData,
} from '../../../shared/popups/ladder-popup/ladder-popup.component';

import {
  RatingService,
  TournamentService,
  HomeService,
} from '../../../core/service';
import { DOCUMENT } from '@angular/common';
import { isPlatformBrowser, Location } from '@angular/common';
import {
  EsportsTimezone,
  EsportsLanguageService,
  EsportsUtilsService,
  EsportsToastService,
  IUser,
  EsportsGameService,
  GlobalUtils,
  EsportsUserService,
  EsportsTournamentService,
  ITournament,
  EsportsChatService,
  EsportsGtmService,
  EsportsBracketService
} from 'esports';
import { AppHtmlRoutes } from '../../../app-routing.model';
import { ProfileService } from '../../../core/service/profile.service';
import { NgbCarousel } from '@ng-bootstrap/ng-bootstrap/carousel/carousel';
import { HttpParams } from '@angular/common/http';
import { CountdownFormatFn } from 'ngx-countdown';
import { Subscription } from 'rxjs';
import { environment } from '../../../../environments/environment';
const API = environment.apiEndPoint;
@Component({
  selector: 'app-view-tournament',
  templateUrl: './view-tournament.component.html',
  styleUrls: ['./view-tournament.component.scss'],
})
export class ViewTournamentComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;
  tournamentDetails: ITournament | any;
  active: any = 1;
  isLoaded: any = false;
  hideMessage: any = true;
  domain: any;
  user: IUser;
  isBrowser: boolean;
  enableComment: boolean = false;
  text: string;
  participantId: string | null;
  isOrganizer: boolean = false;
  isParticipant: boolean = false;
  timezone: any;
  showregbut: boolean;
  playNowKeys: any;
  structure;
  showPlayNow=false
  tournamentDetailsStatus;
  participantStatus: string | null;
  tournamentStatus: string | null;
  registrationStatus = {
    isOpen: false,
    isClosed: false,
    notYetStarted: false,
    enableCheckIn: false,
    isCheckInClosed: false,
  };

  @ViewChild('carousel') carousel: NgbCarousel;

  tournamentsSlides: any = [];

  currentLang: string = 'english';

  startDate: any;
  // timer: any;
  // timem: string;
  remainingTime: any;
  remainingTimeForTournamentStart: any;
  showRegTime: any;
  showTournamentTime: any;
  remainigCheckInTime: number = 0;
  showCountDowns = false;

  CountdownTimeUnits: Array<[string, number]> = [
    ['Y', 1000 * 60 * 60 * 24 * 365], // years
    ['M', 1000 * 60 * 60 * 24 * 30], // months
    ['D', 1000 * 60 * 60 * 24], // days
    ['H', 1000 * 60 * 60], // hours
    ['m', 1000 * 60], // minutes
    ['s', 1000], // seconds
  ];
  platformList: any = [];
  checkTournamentCloseFlag: boolean;

  date = new Date();
  tournament_endDate: any;
  tournament_startdate: any;
  isclosed: any;
  isyettoopen: boolean = false;
  isEndDateTime: boolean;
  isAdmin = false;
  isEO = false;
  showPlayNowBtn: boolean = true;
  userSubscription: Subscription;
  challengeListener: Subscription;
  challengeErrorListener: Subscription;
  startDateIntervalId = null;
  endDateIntervalId = null;
  slug: string | null;
  playNowDefaultKeys = {
    LADDER_PLAY_NOW_DURATION_MIN: 3,
    LADDER_PLAY_NOW_INTERVAL_DURATION_SEC: 30,
    LADDER_PLAY_NOW_ANY_INTERVAL_DURATION_SEC: 15,
    LADDER_PLAY_NOW_FIRST_INTERVAL_BRACKET: 20,
    LADDER_PLAY_NOW_RECURSIVE_INTERVAL_BRACKET: 50,
  };

  constructor(
    private tournamentService: TournamentService,
    private esportsGameService: EsportsGameService,
    private activatedRoute: ActivatedRoute,
    private toastService: EsportsToastService,
    private utilsService: EsportsUtilsService,
    private matDialog: MatDialog,
    private ratingService: RatingService,
    private translateService: TranslateService,
    private globalUtils: GlobalUtils,
    private languageService: EsportsLanguageService,
    private router: Router,
    private titleService: Title,
    @Inject(DOCUMENT) private document: Document,
    private userService: EsportsUserService,
    private esportsTimezone: EsportsTimezone,
    @Inject(PLATFORM_ID) private platformId,
    private location: Location,
    private homeService: HomeService,
    private profileService: ProfileService,
    private eSportsTournamentService: EsportsTournamentService,
    private gtmService: EsportsGtmService,
    private bracketService: EsportsBracketService,
    private esportsChatService: EsportsChatService,
  ) {
    this.isBrowser = isPlatformBrowser(platformId);
  }

  ngOnInit(): void {
    this.remainingTime = 0;
    this.remainingTimeForTournamentStart = 0;
    this.showRegTime = false;
    this.showTournamentTime = false;
    this.timezone = this.esportsTimezone.getTimeZoneName();
    this.languageService.language.subscribe((lang) =>
      this.translateService.use(lang || 'en')
    );
    this.getCurrentUserDetails();
    this.slug = this.activatedRoute.snapshot.params.id; // get slug from route
    this.esportsChatService.connectTournamentEvents(this.slug);
    this.fetchTournamentDetails(this.slug);
    this.domain =
      this.document.location.protocol + '//' + this.document.location.hostname;
    // this.activatedRoute.queryParams
    //   .filter((params) => params.id)
    //   .subscribe((params) => {
    //     this.tournamentId = params.id;
    //     this.fetchTournamentDetails(this.tournamentId);
    //   });
    // if (GlobalUtils.isBrowser()) {
    //   if (this.tournamentId) {
    //     this.deeplinkService.deeplink({
    //       objectType: "tournament",
    //       objectId: this.tournamentId,
    //     });
    //   }
    // }
    this.getPlayNowKeys();
    this.getPlatformList();
  }

  setRegistrationOpenStatus() {
    const now = new Date().getTime();
    const tournamentStartTime = new Date(
      this.tournamentDetails?.startDate
    ).getTime();
    const regStartTime = new Date(
      this.tournamentDetails?.regStartDate
    ).getTime();
    const regEndTime = new Date(this.tournamentDetails?.regEndDate).getTime();
    const checkInStartDate = new Date(
      this.tournamentDetails?.checkInStartDate
    ).getTime();

    if (this.tournamentDetails['isRegStartDate'] && regStartTime > now) {
      this.registrationStatus.notYetStarted = true;
      return;
    }

    if (this.tournamentDetails?.isRegistrationClosed) {
      this.registrationStatus.isClosed = true;
      return;
    }

    if (this.tournamentDetails.isSeeded || now > tournamentStartTime) {
      this.registrationStatus.isClosed = true;
      return;
    }

    if (
      this.tournamentDetails.isCheckInRequired &&
      now > checkInStartDate &&
      now < tournamentStartTime
    ) {
      this.registrationStatus.enableCheckIn = true;
      this.registrationStatus.isClosed = true;
      return;
    }

    if (this.tournamentDetails['isRegStartDate'] && now > regEndTime) {
      this.registrationStatus.isClosed = true;
      return;
    }
    this.registrationStatus.isOpen = true;
  }

  getAdminOrEo() {
    if (this.user) {
      this.isAdmin = this.user?.accountType == 'admin';
    }
    if (this.tournamentDetails && this.user) {
      this.isEO =
        this.user?._id == this.tournamentDetails?.organizerDetail?._id;
    }
  }

  fetchTournamentDetails = async (slug,
    isTournamentStart: boolean = false,
    isTournamentFinished: boolean = false
    ) => {
    try {
      this.isLoaded = false;
      let paramsd = new HttpParams();
      paramsd = paramsd.append(
        'query',
        JSON.stringify({ slug: slug, tournamentStatus: 'publish' })
      );
      const tournament = await this.tournamentService
        .getTournamentDetails(paramsd)
        .toPromise();
      if (tournament && tournament.data && tournament.data.length) {
        this.gtmService.pushGTMTags('Tournament_Details_Views');
        this.tournamentDetails = tournament.data.length
          ? tournament.data[0]
          : null;
        this.tournament_startdate = new Date(this.tournamentDetails.startDate);
        this.tournament_endDate = new Date(this.tournamentDetails.endDate);
        this.setRegistrationOpenStatus();
        this.getAdminOrEo();

        // if (this.tournamentDetails) {
        //   if (
        //     Date.now() <= new Date(this.tournamentDetails?.regEndDate).getTime()
        //   ) {
        //     this.isEndDateTime = true;
        //   } else {
        //     this.isEndDateTime = false;
        //   }
        // }
        this.showTournamentAlertAndStartTimer(
          isTournamentStart,
          isTournamentFinished
        );
        if (
          this.tournamentDetails.regStartDate >= this.date ||
          this.tournamentDetails.isSeeded == false
        ) {
          this.isclosed = true;
        } else if (
          this.tournamentDetails.regStartDate <= this.date ||
          !this.tournamentDetails.isSeeded == false
        ) {
          this.isyettoopen = true;
        } else if (this.tournamentDetails.regEndDate <= this.date) {
          this.isclosed = true;
        }

        this.checkTournamentClose();
        localStorage.setItem(
          'tournamentDetails',
          JSON.stringify(this.tournamentDetails)
        );
        this.tournamentService
          .fetchParticipantRegistrationStatus1(this.tournamentDetails?._id)
          .subscribe((data: any) => {
            if (data.data.participantStatus == null) {
              this.showregbut = true;
            } else {
              this.showregbut = false;
            }
          });
        if (this.tournamentDetails?.organizerDetail?._id == this.user?._id) {
          this.isOrganizer = true;
        }

        if (!this.tournamentDetails) {
          this.hideMessage = false;
          this.toastService.showError(
            this.translateService.instant('VIEW_TOURNAMENT.ERROR')
          );
          this.location.back();
        } else {
          if (
            (this.tournamentDetails && this.tournamentDetails.banner) ||
            this.tournamentDetails.gameDetail.logo
          ) {
            this.titleService.setTitle(this.tournamentDetails.name);
            this.globalUtils.setMetaTags([
              {
                property: 'twitter:image',
                content:
                  this.tournamentDetails.banner ||
                  this.tournamentDetails.gameDetail.logo,
              },
              {
                property: 'og:image',
                content:
                  this.tournamentDetails.banner ||
                  this.tournamentDetails.gameDetail.logo,
              },
              {
                property: 'og:image:secure_url',
                content:
                  this.tournamentDetails.banner ||
                  this.tournamentDetails.gameDetail.logo,
              },
              {
                property: 'og:image:url',
                content:
                  this.tournamentDetails.banner ||
                  this.tournamentDetails.gameDetail.logo,
              },
              {
                property: 'og:image:width',
                content: '1200',
              },
              {
                property: 'og:image:height',
                content: '630',
              },
              {
                name: 'description',
                content: this.tournamentDetails.description,
              },
              {
                property: 'og:description',
                content: this.tournamentDetails.description,
              },
              {
                property: 'twitter:description',
                content: this.tournamentDetails.description,
              },
              {
                name: 'title',
                content: this.tournamentDetails.name,
              },
              {
                name: 'title',
                content: this.tournamentDetails.name,
              },
              {
                property: 'og:title',
                content: this.tournamentDetails.name,
              },
              {
                property: 'twitter:title',
                content: this.tournamentDetails.name,
              },
              {
                property: 'og:url',
                content: this.domain + this.router.url,
              },
            ]);
          }
          const data = await this.fetchParticipantRegisterationStatus(
            this.tournamentDetails?._id
          );
          if(data){
            this.tournamentDetailsStatus=data
          }
          if (data.type == 'already-registered') {
            this.isParticipant = true;
          }
        }

        const payload = {
          bracketType: this.tournamentDetails?.bracketType,
          noOfSet: this.tournamentDetails?.noOfRound,
          maximumParticipants: this.tournamentDetails?.maxParticipants,
          noOfTeamInGroup: this.tournamentDetails?.noOfTeamInGroup,
          noOfWinningTeamInGroup:
            this.tournamentDetails?.noOfWinningTeamInGroup,
          noOfRoundPerGroup: this.tournamentDetails?.noOfRoundPerGroup,
          stageBracketType: this.tournamentDetails?.stageBracketType,
        };

        const response = await this.eSportsTournamentService.generateBracket(
          payload
        );
        this.structure = response.data;
      } else {
        // this.router.navigate(['/play']);
      }

      if (this.tournamentDetails?.isSeeded) {
        const queryParams = {
          tournamentId: this.tournamentDetails?._id,
          select: `select=bye,currentMatch,matchStatus,teamA,teamB,disQualifiedTeam,placeHolderA,placeHolderB,teamAWinSet,teamBWinSet,isThirdPlaceMatch,matchNo,battleRoyalTeams.noOfKill,battleRoyalTeams.placement,battleRoyalTeams.score,battleRoyalTeams._id,battleRoyalScoringOption,externalMatchId,isScoreConflict`,
          sortBy: 'matchNo',
          sortOrder: 1
        };
        const response = await this.bracketService.fetchAllMatchesV3(queryParams);
        this.structure = this.bracketService.assembleStructure(response.data);
      } else {
        const payload = {
          bracketType: this.tournamentDetails?.bracketType,
          noOfSet: this.tournamentDetails?.noOfRound,
          maximumParticipants: this.tournamentDetails?.maxParticipants,
          noOfTeamInGroup: this.tournamentDetails?.noOfTeamInGroup,
          noOfWinningTeamInGroup:
            this.tournamentDetails?.noOfWinningTeamInGroup,
          noOfRoundPerGroup: this.tournamentDetails?.noOfRoundPerGroup,
          stageBracketType: this.tournamentDetails?.stageBracketType,
        };

        const response = await this.eSportsTournamentService.generateBracket(
          payload
        );
        this.structure = response.data;
      }
      this.isLoaded = true;
    } catch (error) {
      this.isLoaded = true;
      //this.toastService.showError(error?.error?.message || error?.message);
      //this.router.navigate(['/play']);
    }
  };

  joint() {
    localStorage.setItem(
      'tournamentDetails',
      JSON.stringify(this.tournamentDetails)
    );
    // this.router.navigateByUrl('/join-tournament')
  }

  getCheckInDuration() {
    if (
      this.tournamentDetails?.checkInStartDate &&
      this.tournamentDetails?.checkInEndDate
    ) {
      const startDate = new Date(this.tournamentDetails.checkInStartDate);
      const endDate = new Date(this.tournamentDetails.checkInEndDate);
      if (startDate === endDate) {
        return `${startDate.getDate()} ${startDate.toLocaleString('default', {
          month: 'long',
        })} ${startDate.getFullYear()}`;
      } else if (startDate.getFullYear() < endDate.getFullYear()) {
        return `${startDate.getDate()} ${startDate.toLocaleString('default', {
          month: 'long',
        })} ${startDate.getFullYear()} to ${endDate.getDate()} ${endDate.toLocaleString(
          'default',
          { month: 'long' }
        )} ${endDate.getFullYear()}`;
      } else if (startDate.getMonth() < endDate.getMonth()) {
        return `${startDate.getDate()} ${startDate.toLocaleString('default', {
          month: 'long',
        })} - ${endDate.getDate()} ${endDate.toLocaleString('default', {
          month: 'long',
        })} ${endDate.getFullYear()}`;
      } else if (startDate.getDate() < endDate.getDate()) {
        return `${startDate.getDate()} - ${endDate.getDate()} ${endDate.toLocaleString(
          'default',
          { month: 'long' }
        )} ${endDate.getFullYear()}`;
      } else {
        return 'N/A';
      }
    }
  }

  isNumber(val): boolean {
    const str: string = val.toString();
    return str.startsWith('+');
  }

  getStartTime() {
    return this.utilsService.convertStringIntoTime(
      this.tournamentDetails?.startDate,
      this.tournamentDetails?.startTime
    );
  }

  buttonResponse(data) {
    if (data) {
      this.fetchTournamentDetails(this.tournamentDetails?.slug);
      this.active = 2;
    }
  }

  enableRating(data) {
    if (data?.isAuthorized) {
      // add field for ratings
    }
  }

  rateUs = async () => {
    try {
      const query = `?query=${encodeURIComponent(
        JSON.stringify({
          posterId: this.tournamentDetails?._id,
          type: 'Tournament',
        })
      )}`;
      const rating = await this.ratingService.getRating(query);
      const rate = rating.data || {
        raterId: this.user?._id,
        posterId: this.tournamentDetails?._id,
        value: 3,
        type: 'Tournament',
      };
      const blockUserData: InfoPopupComponentData = {
        title: 'Rate US',
        text: `Your feedback is valuable for us.`,
        type: InfoPopupComponentType.rating,
        btnText: 'Confirm',
        rate: rate.value,
      };

      const dialogRef = this.matDialog.open(InfoPopupComponent, {
        data: blockUserData,
      });

      const confirmed = await dialogRef.afterClosed().toPromise();

      if (confirmed) {
        rate.value = confirmed;
        const addOrUpdateRating = rate?._id
          ? await this.ratingService.updateRating(rate?._id, rate)
          : await this.ratingService.addRating(rate);
        this.toastService.showSuccess(addOrUpdateRating?.message);
      }
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  openSocialShareComponent() {
    const data: InfoPopupComponentData = {
      title: this.translateService.instant('ARTICLE_POST.SHARE'),
      text: ``,
      type: InfoPopupComponentType.socialSharing,
      cancelBtnText: 'Close',
    };

    this.matDialog.open(InfoPopupComponent, {
      data,
    });
  }

  shareTournament = async () => {
    try {
      this.gtmService.pushGTMTags('Tournament_Share');
      const data: InfoPopupComponentData = {
        title: this.translateService.instant('ARTICLE_POST.SHARE_TOURNAMENT'),
        text: ``,
        type: InfoPopupComponentType.socialSharing,
        cancelBtnText: 'Close',
      };

      const dialogRef = this.matDialog.open(InfoPopupComponent, {
        data,
      });

      await dialogRef.afterClosed().toPromise();
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  getCurrentUserDetails() {
    this.userSubscription =this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.user = data;
      }
    });
  }

  fetchParticipantRegisterationStatus = async (id: string) => {
    try {
      const { data } =
        await this.tournamentService.fetchParticipantRegistrationStatus(id);
      this.enableComment =
        this.tournamentDetails?.organizerDetail?._id == this.user?._id ||
        !!data?.participantId;
      this.participantId = data?.participantId;
      this.participantStatus = data?.participantStatus;
      this.showCountdowns();
      if (!this.enableComment) {
        if (
          !['tournament-finished', 'tournament-started', 'no-action'].includes(
            data.type
          )
        ) {
          this.text = 'DISCUSSION.PARTICIPANT_NA';
        }
      }
      return data;
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  showCountdowns() {
    const now = Date.now();
    this.remainingTime =
      (new Date(this.tournamentDetails?.regStartDate).getTime() - now) / 1000;
    this.remainingTimeForTournamentStart =
      (new Date(this.tournamentDetails?.startDate).getTime() - now) / 1000;
    this.showRegTime = this.remainingTime > 0 ? true : false;
    this.showTournamentTime =
      this.remainingTimeForTournamentStart > 0 ? true : false;

    this.showCountDowns =
      this.remainingTimeForTournamentStart > 0 ? true : false;

    if (this.remainingTime < 0) {
      this.showRegTime = false;
    }

    if (this.showRegTime) {
      this.showTournamentTime = false;
    }

    if (this.tournamentDetails?.checkInStartDate) {
      this.remainigCheckInTime =
        (new Date(this.tournamentDetails?.checkInStartDate).getTime() - now) /
          1000 || 0;
    }
  }

  shouldShowPaymentPopup() {
    if (this.tournamentDetails?.isPrize) {
      if (this.user && !this.user?.accountDetail?.isPaymentAccountVerified) {
        return true;
      }
    }
    return false;
  }
  showUpdatePaymentPopup() {
    const afterShowPopup: InfoPopupComponentData = {
      title: this.translateService.instant(
        'VIEW_TOURNAMENT.MODAL.PAYMENT_METHOD.TITLE'
      ),
      text: this.translateService.instant(
        'VIEW_TOURNAMENT.MODAL.PAYMENT_METHOD.MESSAGE'
      ),
      type: InfoPopupComponentType.confirm,
      btnText: this.translateService.instant(
        'VIEW_TOURNAMENT.MODAL.PAYMENT_METHOD.BUTTON'
      ),
    };
    const dialogRef = this.matDialog.open(InfoPopupComponent, {
      data: afterShowPopup,
    });
    dialogRef.afterClosed().subscribe((confirmed) => {
      if (confirmed) {
        this.router.navigate([`/profile/profile-setting`], {
          queryParams: { activeTab: '2' },
        });
      }
    });
  }
  saveParticipantDetails = async () => {
    try {
      if (this.shouldShowPaymentPopup()) {
        this.showUpdatePaymentPopup();
        return;
      }
      this.isLoaded = true;
      localStorage.setItem(
        'tournamentDetails',
        JSON.stringify(this.tournamentDetails)
      );
      // populate user game id only for matching game
      const userGames = this.user?.preference?.gameDetails || [];
      const game = userGames.find((game) => {
        return game?._id === this.tournamentDetails?.gameDetail?._id;
      });
      const payload = {
        name: this.user?.fullName,
        phoneNumber: this.user?.phoneisVerified ? this.user?.phoneNumber : '',
        email: this.user?.emailisVerified ? this.user?.email : '',
        logo: this.user?.profilePicture || '',
        inGamerUserId: game?.userGameId || '',
        teamName: game?.username || '',
        tournamentId: this.tournamentDetails?._id,
        participantType: this.tournamentDetails?.participantType,
        teamMembers: [],
        substituteMembers: [],
      };
      const { isProcessing, message } =
        await this.tournamentService.saveParticipantDetails(
          payload,
          this.tournamentDetails
        );
      if (!isProcessing) {
        this.toastService.showSuccess(message);
        this.showPlayNow=true
        this.isLoaded = false;
        this.tournamentService.tournamentActiveTab = 3;
      }
    } catch (error) {
      // this.isProcessing = false;
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  formatDate?: CountdownFormatFn = ({ date, formatStr }) => {
    let duration = Number(date || 0);
    return this.CountdownTimeUnits.reduce((current, [name, unit]) => {
      if (current.indexOf(name) !== -1) {
        const v = Math.floor(duration / unit);
        duration -= v * unit;
        return current.replace(new RegExp(`${name}+`, 'g'), (match: string) => {
          return v.toString().padStart(match.length, '0');
        });
      }
      return current;
    }, formatStr);
  };

  onTournamnetTimerFinished(e: Event) {
    if (e['action'] == 'done') {
      Promise.resolve(() => {
        this.showTournamentTime = false;
      });
      this.fetchParticipantRegisterationStatus(this.tournamentDetails?._id);
      // this.ngOnChanges();
    }
  }
  onTimerFinished(e: Event) {
    if (e['action'] == 'done') {
      Promise.resolve(() => {
        this.showRegTime = false;
      });
      this.fetchParticipantRegisterationStatus(this.tournamentDetails?._id);
      // this.ngOnChanges();
    }
  }

  getPlatformList() {
    this.esportsGameService.fetchPlatforms(API).subscribe((res) => {
      this.platformList = res.data;
    });
  }

  getPlatformName(p_id) {
    let name = '';
    this.platformList.map((res) => {
      if (p_id == res?._id) {
        name = res.name;
      }
    });
    return name;
  }
  convert12HrsTo24HrsFormat(time) {
    if (!time) return;

    var hours = Number(time.match(/^(\d+)/)[1]);
    var minutes = Number(time.match(/:(\d+)/)[1]);
    var AP = time.match(/\s(.*)$/);

    if (!AP) AP = time.slice(-2);
    else AP = AP[1];
    if (AP == 'PM' && hours < 12) hours = hours + 12;
    if (AP == 'AM' && hours == 12) hours = hours - 12;

    var Hours24 = hours.toString();
    var Minutes24 = minutes.toString();

    if (hours < 10) Hours24 = '0' + Hours24;
    if (minutes < 10) Minutes24 = '0' + Minutes24;

    return { hour: parseInt(Hours24), minute: parseInt(Minutes24) };
  }
  getRegistrationStatus(
    startDate,
    startTime,
    isSeeded,
    isFinished,
    maxParticipants,
    participantsJoined
  ) {
    if (!startDate || !startTime) {
      return;
    }
    const timeObj = this.convert12HrsTo24HrsFormat(startTime);
    const sDate = new Date(startDate);
    sDate.setHours(timeObj['hour']);
    sDate.setMinutes(timeObj['minute']);
    if (isSeeded == true || isFinished == true) {
      return 'Closed';
    } else if (participantsJoined >= maxParticipants) {
      return 'Closed';
    } else if (sDate.getTime() < Date.now()) {
      return 'Closed';
    } else {
      return 'Open';
    }
  }

  checkTournamentClose = () => {
    if (this.tournamentDetails.isSeeded) {
      this.checkTournamentCloseFlag = true; //close
    } else {
      let ToDate = new Date();
      if (
        ToDate.getTime() < new Date(this.tournamentDetails.endDate).getTime()
      ) {
        this.checkTournamentCloseFlag = true; //close
      } else {
        this.checkTournamentCloseFlag = false; //open
      }
    }
  };

/**
   * Start date Timer
   */

  startDateTimer = () => {
    const startDate = new Date(this.tournamentDetails.startDate);
    const currentDate = new Date();
    const timeDiff = startDate.getTime() - currentDate.getTime();
    if (timeDiff > 0) {
      if (!this.startDateIntervalId) {
        this.startDateIntervalId = setInterval(
          () => this.startDateTimer(),
          1000
        );
      }
    } else if (
      ['swiss_safeis', 'ladder'].includes(this.tournamentDetails?.bracketType)
    ) {
      if (this.startDateIntervalId) {
        this.fetchTournamentDetails(this.tournamentDetails?.slug, true, false);
        clearInterval(this.startDateIntervalId);
        this.startDateIntervalId = null;
      }
    }
  };


  async ladderPopup() {
    //this.matDialog.open(LadderPopupComponent);
    try {
      let popupData: LadderPopupComponentData = {
        isShowLoading: true,
        isShowNotFound: false,
        isShowScheduler: false,
      };

      this.matDialog.open(LadderPopupComponent, { data: popupData });
      const match = await this.tournamentService
        .getOpponentParticipantForMatch({
          tournamentid: this.tournamentDetails._id,
          participantid: this.participantId,
        })
        .toPromise();
      this.matDialog.closeAll();
      if (match && match.success && match.success == true) {
        //this.router.navigate([`/${match.data.challenge._id}`])
        this.router.navigate(
          [`tournament/${this.activatedRoute.snapshot.params.id}/ladder-card`],
          { queryParams: { chid: match.data.challenge._id } }
        ); //?cid=${match.data.challenge._id}
      } else {
        popupData.isShowLoading = false;
        popupData.isShowNotFound = true;
        const notFoundPopupRef = this.matDialog.open(LadderPopupComponent, {
          data: popupData,
        });
        notFoundPopupRef.afterClosed().subscribe((action) => {
          if (action == 'scheduleMatch') {
            this.ladderPopup();
          } else if (action == 'searchAgain') {
            this.ladderPlayNowPopup();
          }
        });
      }
    } catch (error) {
      this.matDialog.closeAll();
      this.toastService.showError(
        error?.error?.message || error?.message
      );
    }
  }

  unsubscribeListeners() {
    if (this.challengeListener) {
      this.challengeListener.unsubscribe();
    }
    if (this.challengeErrorListener) {
      this.challengeErrorListener.unsubscribe();
    }
  }


  getPlayNowKeys = async () => {
    this.homeService._playNowKeys().subscribe(
      (res: any) => {
        if (res?.data) {
          this.playNowKeys = res?.data;
        }
      },

      (err: any) => {}
    );
  };

  async ladderPlayNowPopup() {
    try {
      if (this.playNowKeys) {
        let leftTime = this.playNowKeys.LADDER_PLAY_NOW_DURATION_MIN
          ? this.playNowKeys.LADDER_PLAY_NOW_DURATION_MIN * 60
          : this.playNowDefaultKeys.LADDER_PLAY_NOW_DURATION_MIN * 60;
        let leftTimePlayWithTop20 =
          this.playNowKeys.LADDER_PLAY_NOW_INTERVAL_DURATION_SEC ||
          this.playNowDefaultKeys.LADDER_PLAY_NOW_INTERVAL_DURATION_SEC;
        let leftTimePlayWithTop50 =
          this.playNowKeys.LADDER_PLAY_NOW_INTERVAL_DURATION_SEC ||
          this.playNowDefaultKeys.LADDER_PLAY_NOW_INTERVAL_DURATION_SEC;
        let leftTimePlayWithAny =
          this.playNowKeys.LADDER_PLAY_NOW_ANY_INTERVAL_DURATION_SEC ||
          this.playNowDefaultKeys.LADDER_PLAY_NOW_ANY_INTERVAL_DURATION_SEC;
        let firstBracket =
          this.playNowKeys.LADDER_PLAY_NOW_FIRST_INTERVAL_BRACKET ||
          this.playNowDefaultKeys.LADDER_PLAY_NOW_FIRST_INTERVAL_BRACKET;
        let recursiveBracket =
          this.playNowKeys.LADDER_PLAY_NOW_RECURSIVE_INTERVAL_BRACKET ||
          this.playNowDefaultKeys.LADDER_PLAY_NOW_RECURSIVE_INTERVAL_BRACKET;
        let isLadderPopUpClosed = false;
        const tournamentEndsIn =
          (new Date(this.tournamentDetails.endDate).getTime() -
            new Date().getTime()) /
          1000;
        this.esportsChatService.connectSocket();
        this.esportsChatService.connectUser();
        // if tournament ends in less than a minute hide play now button
        if (tournamentEndsIn < leftTime) {
          if (tournamentEndsIn < 60) {
            this.showPlayNowBtn = false;
            leftTime = 0;
            this.toastService.showInfo(
              this.translateService.instant('LADDER.TOURNAMENT_ENDED')
            );
            return;
          } else {
            leftTime = tournamentEndsIn - 60;
          }
        }

        let popupData: LadderPopupComponentData = {
          isShowLoading: true,
          payload: { leftTime: leftTime },
        };

        const loadDialogRef = this.matDialog.open(LadderPopupComponent, {
          data: popupData,
          disableClose: true,
        });
        this.esportsChatService.findOpponent({
          tournamentId: this.tournamentDetails._id,
          bracket: firstBracket,
        });

        this.challengeErrorListener = this.esportsChatService
          .listenChallengeError()
          .subscribe((payload) => {
            loadDialogRef.close(true);
            if (payload.error) {
              if (payload.error.toString().indexOf('just finished') > -1) {
                this.toastService.showError(
                  this.translateService.instant('LADDER.MATCH_JUST_FINISHED')
                );
              } else if (payload.error.toString().indexOf('confliction') > -1) {
              this.toastService.showError(
                  this.translateService.instant('LADDER.MATCH_SCORE_CONFLICTED')
                );
              } else {
              this.toastService.showError(
                  this.translateService.instant(
                    'LADDER.MATCH_NOT_FOUND_PART1'
                  ) +
                    payload.error +
                    this.translateService.instant(
                      'LADDER.MATCH_NOT_FOUND_PART2'
                    )
                );
              }
            } else {
              this.toastService.showError(
                this.translateService.instant('LADDER.MATCH_FOUND_ERROR')
              );
            }
            this.unsubscribeListeners();
          });

        this.challengeListener = this.esportsChatService
          .listenChallengeFound()
          .subscribe((payload) => {
            loadDialogRef.close(true);
            this.toastService.showSuccess(
              this.translateService.instant('LADDER.MATCH_FOUND')
            );
            this.router.navigate(
              [
                `tournament/${this.activatedRoute.snapshot.params.id}/ladder-card`,
              ],
              { queryParams: { chid: payload.data.newChallenge._id } }
            );


            // this.router.navigate(['get-match/game-lobby'], {
            //   queryParams: { matchId: payload?.data?.newMatch?._id },
            // });
          });

        const loopCount = leftTime / leftTimePlayWithTop50 - 2; // first interval for 20 and last for any
        for (let index = 1; index <= loopCount; index++) {
          setTimeout(() => {
            if (isLadderPopUpClosed === false) {
              this.esportsChatService.findOpponent({
                tournamentId: this.tournamentDetails._id,
                bracket: recursiveBracket * index,
              });
            }
          }, leftTimePlayWithTop50 * index * 1000);
        }

        setTimeout(() => {
          if (isLadderPopUpClosed === false) {
            this.esportsChatService.findOpponent({
              tournamentId: this.tournamentDetails._id,
              bracket: 0,
            });
          }
        }, (leftTime - leftTimePlayWithAny) * 1000);

        loadDialogRef.afterClosed().subscribe((loadCloseData) => {
          isLadderPopUpClosed = true;
          this.esportsChatService.removeFromQueue(null);
          this.unsubscribeListeners();
          if (loadCloseData == 'timeOut') {
            if (new Date(this.tournamentDetails.endDate) > new Date()) {
              const notFoundPopupRef = this.matDialog.open(
                LadderPopupComponent,
                {
                  data: { isShowNotFound: true },
                }
              );
              notFoundPopupRef.afterClosed().subscribe((action) => {
                if (action == 'scheduleMatch') {
                  this.ladderPopup();
                } else if (action == 'searchAgain') {
                  this.ladderPlayNowPopup();
                }
              });
            } else {
              this.toastService.showInfo(
                this.translateService.instant('LADDER.TOURNAMENT_ENDED')
              );
            }
          }
        });
      } else {
        this.toastService.showInfo(
          this.translateService.instant('LADDER.PLAY_NOW_KEY_ERROR'),
          false
        );
      }
    } catch (error) {
      this.matDialog.closeAll();
      this.toastService.showError(
        error?.error?.message || error?.message
      );
    }
  }

  ngOnDestroy() {
    if(this.userSubscription){
      this.userSubscription.unsubscribe();
    }
    this.unsubscribeListeners();
    this.esportsChatService?.disconnectTournamentEvents(this.slug);
    clearInterval(this.startDateIntervalId);
    clearInterval(this.endDateIntervalId);
  }

  showTournamentAlertAndStartTimer = (
    isTournamentStart: boolean,
    isTournamentFinished: boolean
  ) => {
    if (this.tournamentDetails?.isFinished) {
      if (isTournamentFinished) {
        this.toastService.showInfo(
          this.translateService.instant('ELIMINATION.TOURNAMENT_FINISHED'),
          false
        );
      }
    } else if (this.tournamentDetails?.isSeeded) {
      if (isTournamentStart) {
        this.toastService.showInfo(
          this.translateService.instant('ELIMINATION.TOURNAMENT_STARTED'),
          false
        );
      }
      this.endDateTimer();
    } else {
      this.startDateTimer();
    }
  };

   /**
   * End date timer
   */
    endDateTimer = () => {
      const endDate = new Date(this.tournamentDetails?.endDate);
      const currentDate = new Date();
      const timeDiff = endDate.getTime() - currentDate.getTime();

      /* if tournament ends in less than a minute hide play now button */
      if (timeDiff / 1000 < 60) {
        if (this.showPlayNowBtn) {
          this.matDialog.closeAll();
          this.unsubscribeListeners();
          this.esportsChatService.removeFromQueue(true);
          this.toastService.showInfo(
            this.translateService.instant('LADDER.TOURNAMENT_ENDED')
          );
        }
        this.showPlayNowBtn = false;
      }
      if (timeDiff > 0) {
        if (!this.endDateIntervalId) {
          this.endDateIntervalId = setInterval(() => this.endDateTimer(), 1000);
        }
      } else if (
        ['swiss_safeis', 'ladder'].includes(this.tournamentDetails?.bracketType)
      ) {
        if (this.endDateIntervalId) {
          this.fetchTournamentDetails(this.tournamentDetails?.slug, false, true);
          clearInterval(this.endDateIntervalId);
          this.endDateIntervalId = null;
        }
      }
    };
}
