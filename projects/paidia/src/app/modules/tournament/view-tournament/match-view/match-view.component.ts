import { Component, OnInit, ViewChild, EventEmitter } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import {
  EsportsParticipantService,
  EsportsTournamentService,
  EsportsChatService,
  EsportsToastService,
} from 'esports';
import { environment } from '../../../../../environments/environment';
import { MatTabChangeEvent, MatTabGroup } from '@angular/material/tabs';
import { TranslateService } from '@ngx-translate/core';

const API = environment.apiEndPoint;
@Component({
  selector: 'app-match-view',
  templateUrl: './match-view.component.html',
  styleUrls: ['./match-view.component.scss'],
})
export class MatchViewComponent implements OnInit {
  playersData = [];
  matchData = [];
  isLoaded = false;
  MatchId: string = '';
  isLoading = false;
  tournamentDetails;
  gameInfo = null;
  tournamentId;
  activeTabIndex = -1;
  windowposition = 'chat_window chat_window_abuser_drawer';

  steps = [
    {
      isDone: false,
      text: 'LADDER.STEPS.FIRST',
      icon: 'assets/icons/ladder/FIRST.svg',
    },
    {
      isDone: false,
      text: 'LADDER.STEPS.SECOND',
      icon: 'assets/icons/ladder/SECOND.svg',
    },
    {
      isDone: false,
      text: 'LADDER.STEPS.THIRD',
      icon: 'assets/icons/ladder/THIRD.svg',
    },
    {
      isDone: false,
      text: 'LADDER.STEPS.FOURTH',
      icon: 'assets/icons/ladder/FOURTH.svg',
    },
  ];
  teamsData = [];
  matchdetailsHistory: any;
  typeofchatHistory: any;
  showAcceptButton: boolean = false;
  isSingle: boolean = true;
  @ViewChild('tabs') tabGroup: MatTabGroup;

  customTabEvent = new EventEmitter<MatTabChangeEvent>();
  constructor(
    private location: Location,
    private activatedRoute: ActivatedRoute,
    private esportsParticipantService: EsportsParticipantService,
    public tournamentService: EsportsTournamentService,
    private translateService: TranslateService,
    private eSportsToastService: EsportsToastService,
    public esportsChatService: EsportsChatService,
    private router: Router
  ) {}

  ngOnInit(): void {
    if (this.activatedRoute.snapshot.queryParams.matchId) {
      this.MatchId = this.activatedRoute.snapshot.queryParams.matchId;
      this.getMatchDetails();
    }
    this.customTabEvent.subscribe((tabChangeEvent: MatTabChangeEvent) => {
      this.switchData(tabChangeEvent.index);
    });

    this.activatedRoute.queryParams.subscribe((params) => {
      if (params.activeTabIndex) {
        this.activeTabIndex = +params.activeTabIndex;
        // if (this.activeTabIndex == 1) {
        //   this.chatFunction(this.MatchId);
        // }
        this.switchData(this.activeTabIndex);
      } else if (!params.activeTabIndex && this.activeTabIndex > -1) {
        this.location.back();
      } else {
        this.activeTabIndex = 0;
        this.switchData(this.activeTabIndex);
      }
    });
  }
  chatFunction(id) {
    this.esportsChatService.getMatchInformation(id).subscribe((result: any) => {
       const mtchdetails = {
         matchid: result.matchdetails._id,

         teamAId: result.matchdetails.teamA.userId._id,

         teamBId: result.matchdetails.teamB.userId._id,

         tournamentid: result.matchdetails.tournamentId,

         typeofchat: 'tournament',

         users: [
           {
             _id: result.matchdetails.teamA.userId._id,
             fullName: result.matchdetails.teamA.userId.fullName,
             profilePicture: result.matchdetails.teamA.userId.profilePicture,
           },
           {
             _id: result.matchdetails.teamB.userId._id,
             fullName: result.matchdetails.teamB.userId.fullName,
             profilePicture: result.matchdetails.teamB.userId.profilePicture,
           },
         ],
       };

      this.esportsChatService.setWindowPos(this.windowposition);
      let firstChat = this.esportsChatService.getChatStatus();
      if (firstChat == true) {
        this.esportsChatService.setChatStatus(false);
        this.esportsChatService?.disconnectMatch( mtchdetails.matchid, 'tournament');
        this.esportsChatService.setTextButtonStatus(false);
      } else {
        this.esportsChatService.setCurrentMatch(mtchdetails);
        this.esportsChatService.setTypeOfChat('tournament');
        this.esportsChatService.setChatStatus(true);
        this.esportsChatService.setTextButtonStatus(true);
      }
    });
  }
  getMatchDetails() {
    this.isLoading = true;
    this.esportsParticipantService.getMatchDetails(API, this.MatchId).subscribe(
      (res) => {
        this.matchData = res['data'];

        this.playersData.push(this.matchData[0].teamA);
        this.playersData.push(this.matchData[0].teamB);

        if (this.matchData[0].teamA?.teamMembers?.length > 0) {
          this.teamsData = [];
          let team1Players = [
            {
              id: this.matchData[0].teamA._id,
              name: this.matchData[0].teamA.name,
              inGamerUserId: this.matchData[0].teamA.inGamerUserId,
              image: this.matchData[0].teamA.logo,
            },
          ];

          const mapImage = (challenge, challenger, arrayName) => {
            challenge[challenger][arrayName] = challenge[challenger][
              arrayName
            ].map((ele) => {
              ele.image = challenge[challenger].logo;
              return ele;
            });
          };

          mapImage(this.matchData[0], 'teamA', 'teamMembers');
          mapImage(this.matchData[0], 'teamA', 'substituteMembers');
          mapImage(this.matchData[0], 'teamB', 'teamMembers');
          mapImage(this.matchData[0], 'teamB', 'substituteMembers');

          const team1 = {
            name: this.matchData[0].teamA.teamName,
            players: [
              ...team1Players,
              ...this.matchData[0].teamA.teamMembers,
              ...this.matchData[0].teamA.substituteMembers,
            ],
          };
          let team2Players = [
            {
              id: this.matchData[0].teamB._id,
              name: this.matchData[0].teamB.name,
              inGamerUserId: this.matchData[0].teamB.inGamerUserId,
              image: this.matchData[0].teamB.logo,
            },
          ];
          const team2 = {
            name: this.matchData[0].teamB.teamName,
            players: [
              ...team2Players,
              ...this.matchData[0].teamB.teamMembers,
              ...this.matchData[0].teamB.substituteMembers,
            ],
          };


          this.teamsData = [team1, team2];
          this.isSingle = false;
        }


        this.tournamentId = this.matchData[0].tournamentId._id;
        this.getTournamentDetails();
      },
      (err) => {},
      () => {
        this.isLoading = false;
      }
    );
  }

  getTournamentDetails() {
    this.isLoading = true;
    this.tournamentService.getTournament(API, this.tournamentId).subscribe(
      (res) => {
        this.tournamentDetails = res.data;
        this.gameInfo = res.data.gameDetail;
        if (this.tournamentDetails.participantType == 'team') {
          this.isSingle = false;
        }
      },
      (err) => {},
      () => {
        this.isLoading = false;
      }
    );
  }

  goBack() {
    this.location.back();
  }

  onTabChanged = (tabChangeEvent: MatTabChangeEvent): void => {
    //this.customTabEvent.emit(tabChangeEvent);
  };

  switchData(index: number) {
    switch (index) {
      case 0:
        this.router.navigate(['.'], {
          relativeTo: this.activatedRoute,
          queryParams: { activeTabIndex: index, matchId: this.MatchId },
        });
        break;
      case 1:
        this.router.navigate(['.'], {
          relativeTo: this.activatedRoute,
          queryParams: { activeTabIndex: index, matchId: this.MatchId },
        });
        break;
      default:
        break;
    }
  }

  showCopySnackbar() {
    this.eSportsToastService.showInfo(
      this.translateService.instant('LADDER.GAME_ID_COPIED')
    );
  }
}
