import { Component, Input, OnInit } from "@angular/core";
import {FormControl,ControlContainer,FormGroupDirective,Validators,FormBuilder} from "@angular/forms";
import { Observable, Subject } from "rxjs";
import {catchError,debounceTime,distinctUntilChanged,switchMap} from "rxjs/operators";
import { EsportsConstantsService, EsportsToastService,EsportsUserService} from "esports";
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: "tournament-final",
  templateUrl: "./final.component.html",
  styleUrls: ["./final.component.scss"],
  viewProviders: [
    { provide: ControlContainer, useExisting: FormGroupDirective },
  ],
})
export class FinalComponent implements OnInit {
  @Input() tFinal: Subject<any>;

  final;
  contactOptionsList = ["Facebook", "Email", "Twitter", "Discord", "None"];

  participantObj;
  formatter = (result: any) => {
    return result;
  };

  constructor(
    private tournament: FormGroupDirective,
    public fb: FormBuilder,
    private userService: EsportsUserService,
    private translateService: TranslateService,
    private toastService: EsportsToastService,
    private constantsService: EsportsConstantsService
  ) {}

  ngOnInit(): void {
    this.final = this.tournament.form;
    this.addControls();
  }

  // add controls
  addControls() {
    this.final.addControl("url", new FormControl(""));
    this.final.addControl("contactOn", new FormControl(""));
    this.final.addControl("participants", new FormControl([]));
    this.final.addControl(
      "youtubeVideoLink",
      new FormControl(
        "",
        Validators.compose([Validators.pattern(EsportsConstantsService.youtubeRegex)])
      )
    );
    this.final.addControl(
      "facebookVideoLink",
      new FormControl(
        "",
        Validators.compose([Validators.pattern(EsportsConstantsService.facebookRegex)])
      )
    );
    this.final.addControl(
      "twitchVideoLink",
      new FormControl(
        "",
        Validators.compose([Validators.pattern(EsportsConstantsService.twitchRegex)])
      )
    );
    this.final.addControl("visibility", new FormControl(1));
    this.final.addControl("contactDetails", new FormControl(""));
    this.tFinal.subscribe((final) => {});
  }

  // handlers

  setContactOn(val) {
    this.final.get("contactDetails").setValue("");
    this.final.get("contactOn").setValue(val);
    switch (val) {
      case "WhatsApp":
        this.final.get("contactDetails").setValidators([]);
        break;
      case "Email":
        this.final
          .get("contactDetails")
          .setValidators([
            Validators.required,
            Validators.pattern(EsportsConstantsService.emailRegex),
          ]);
        break;
      case "Facebook":
        this.final
          .get("contactDetails")
          .setValidators([
            Validators.required,
            Validators.pattern(EsportsConstantsService.facebookRegex),
          ]);
        break;
      default:
        this.final.get("contactDetails").setValue("");
        this.final.get("contactOn").setValue(val);
        this.final.get("contactDetails").clearValidators();
        break;
    }
    this.final.get("contactDetails").updateValueAndValidity();
  }

  search = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      distinctUntilChanged(),
      switchMap((term) => {
        if (term) {
          return this.userService.searchUsers(term);
        } else {
          return [];
        }
      }),
      catchError(() => {
        this.toastService.showError(
          this.translateService.instant("TOURNAMENT.SEARCH_FAILED")
        );

        return [];
      })
    );

  isParticipantAlreadyAdded(arr, _id) {
    const { length } = arr;
    const id = length + 1;
    const found = arr.some((el) => el._id === _id);
    return found;
  }

  addParticipants() {
    setTimeout(() => {
      if (this.participantObj && typeof this.participantObj == "object") {
        if (
          !this.isParticipantAlreadyAdded(
            this.final.value.participants,
            this.participantObj?._id
          )
        ) {
          this.final.value.participants.push(this.participantObj);
        }
        this.participantObj = "";
      }
    }, 100);
  }

  removeParticipant(index) {
    this.final.value.participants.splice(index, 1);
  }
}
