import { filter } from "rxjs/operators";
import {Component,OnInit,ViewChild,ElementRef,AfterViewChecked,ChangeDetectorRef,HostListener} from "@angular/core";
import { FormGroup, FormBuilder, FormControl } from "@angular/forms";
import {EsportsUserService,EsportsToastService} from "esports";
import { TournamentService } from "../../../core/service";
import { IUser } from "esports";
import { Subject } from "rxjs";
import { ActivatedRoute } from "@angular/router";
import "rxjs/add/operator/filter";
import { EsportsUtilsService } from "esports";
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: "app-create",
  templateUrl: "./create.component.html",
  styleUrls: ["./create.component.scss"],
})
export class CreateComponent implements OnInit, AfterViewChecked {
  @ViewChild("mainContainer", { read: ElementRef }) public scroll: ElementRef<
    any
  >;
  fallBack: string;
  fallBackIndex: any;
  @HostListener("window:beforeunload", ["$event"])
  resetChanges($event) {
    return ($event.returnValue =
      "You will have to start over. Do you want to go back?");
  }
  childNotifier: Subject<any> = new Subject<any>();
  tournamentDetails: Subject<any> = new Subject<any>();

  tournamentForm: FormGroup;
  hasRequiredAccess: boolean = true;
  user: IUser;

  active = 1;
  showLoader = true;
  editTournament = false;

  isAllDataLoaded = false;
  isParticipantRegistered: boolean = false;

  constructor(
    public toastService: EsportsToastService,
    private fb: FormBuilder,
    private userService: EsportsUserService,
    private tournamentService: TournamentService,
    private activatedRoute: ActivatedRoute,
    private utilsService: EsportsUtilsService,
    private readonly changeDetectorRef: ChangeDetectorRef,
    private translateService: TranslateService,
  ) {}

  ngOnInit(): void {
    this.tournamentForm = this.fb.group({ step: 1 });
    const tId = this.activatedRoute?.snapshot?.params?.id;
    // this.activatedRoute.queryParams
    // .filter((params) => params.id)
    // .subscribe((param) => {
    //   if(param.fallback){
    //     this.fallBack = '/admin/esports-management';
    //     this.fallBackIndex = this.activatedRoute?.snapshot?.params.activeTab ? this.activatedRoute?.snapshot?.params.activeTab : 0;
    //   }
    // });

    this.activatedRoute.queryParams.subscribe((params) => {
      if (params.fallback) {
        this.fallBack = "/admin/esports-management";
        this.fallBackIndex = params.activeTab ? params.activeTab : 0;
      }
    });
    this.editTournament = !!tId;
    this.fetchCurrentUserDetails();
    this.fetchAllData(tId);
  }

  ngAfterViewChecked(): void {
    this.changeDetectorRef.detectChanges();
  }

  fetchCurrentUserDetails() {
    this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.user = data;
      }
    });
  }

  scrollToTop() {
    this.scroll.nativeElement.scrollIntoView();
  }

  getFooterResponse(message) {
    this.toastService.showError(message);
    this.scrollToTop();
  }

  activeHandler(activeId) {
    this.active = activeId;
    this.scrollToTop();
  }

  navChangeHandler(event) {
    this.childNotifier.next(event);
  }

  fetchAllData = async (slug) => {
    try {
      if (slug) {
       
        const tournament = await this.tournamentService
        .getTournamentBySlug(slug)
        .toPromise();
        

        this.tournamentForm.addControl("_id", new FormControl(tournament?.data?._id));
        this.tournamentDetails.next(tournament?.data);
        this.fetchRegisteredParticipant(tournament?.data?._id);
     

      const isEventOrganizer =
        this?.user?.isEO == 0 && this.user?._id === tournament?.data?.organizerDetail?._id;
      const hasAdminAccess =
        this.user?.accountType == 'admin' &&
        this.user?.accessLevel?.includes('em');

      this.hasRequiredAccess = isEventOrganizer || hasAdminAccess;
      if (!this.hasRequiredAccess) {
        throw new Error(
          this.translateService.instant('VIEW_TOURNAMENT.ACCESS_DENIED')
        );
      }
    }
      const { data } = await this.tournamentService.getGames().toPromise();
      const game = data.filter((item) => item.isTournamentAllowed);
      this.utilsService.setGame(game);
      this.isAllDataLoaded = true;
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
      this.isAllDataLoaded = true;
    }
  };

  fetchRegisteredParticipant = async (tournamentId) => {
    try {
      const encodeUrl = `?query=${encodeURIComponent(
        JSON.stringify({ tournamentId })
      )}&select=''&option=${encodeURIComponent(
        JSON.stringify({ count: true })
      )}`;
      const response = await this.tournamentService
        .getParticipants(encodeUrl)
        .toPromise();
      this.isParticipantRegistered = !!response?.totals?.count;
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };
}
