import { Component, Input, OnInit, SimpleChanges, TemplateRef, ViewChild } from "@angular/core";
import { FormGroup, FormControl, ControlContainer, FormGroupDirective, Validators, AbstractControl, FormArray, ValidatorFn, FormBuilder, FormArrayName } from "@angular/forms";
import { EsportsToastService, EsportsUserService, EsportsLanguageService, EsportsConstantsService } from "esports";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { Subject } from "rxjs";

@Component({
  selector: "tournament-conditions",
  templateUrl: "./conditions.component.html",
  styleUrls: ["./conditions.component.scss"],
  viewProviders: [
    { provide: ControlContainer, useExisting: FormGroupDirective },
  ],
})
export class ConditionsComponent implements OnInit {
  @ViewChild("regionModel") private regionModal: TemplateRef<any>;
  @Input() tConditions: Subject<any>;
  @Input() isPaid: boolean;
  conditions;
  condition_active = "description";
  currencyList = [];
  isCharged: boolean = false;
  dPayment;
  disableManualApproval: boolean = false;

  // sponsor config
  logoDimension = { width: 180, height: 180 };
  logoSize = 1024 * 1024;
  bannerDimension = { width: 1200, height: 200 };
  bannerSize = 1024 * 1024;

  plaformType = [
    { name: "online", description: "TOURNAMENT.CREATE.OT_DESC" },
    { name: "offline", description: "TOURNAMENT.CREATE.OFF_T_DESC" },
    { name: "hybrid", description: "TOURNAMENT.CREATE.HT_DESC" },
  ];
  regionSearchKeyword: any;
  regionsArray = [];
  countryList = [];

  constructor(
    private tournament: FormGroupDirective,
    public fb: FormBuilder,
    private toastService: EsportsToastService,
    private modalService: NgbModal,
    private userService: EsportsUserService,
    public languageService: EsportsLanguageService,

  ) { }

  ngOnInit(): void {
    this.conditions = this.tournament.form;
    this.currencyList = EsportsConstantsService?.currencyList;
    this.addControls();
    this.getCountries();

    this.conditions.get("maxParticipants").valueChanges.subscribe((p) => {
      if (this.conditions?.value?.isPrize) {
        this.conditions.get("prizeList").updateValueAndValidity();
      }
    });
    this.disableManualApproval = this.isPaid;
  }

  ngOnChanges(simpleChanges: SimpleChanges) {
    if (simpleChanges.hasOwnProperty("isPaid")) {
      this.disableManualApproval = this.isPaid;
      if (this.isPaid) {
        this.conditions.isManualApproverParticipant = false;
      }
    }
  }

  // add controls
  addControls() {
    this.conditions.addControl("description", new FormControl(""));
    this.conditions.addControl("rules", new FormControl(""));
    this.conditions.addControl("isPrize", new FormControl(false));
    this.conditions.addControl("isIncludeSponsor", new FormControl(false));
    this.conditions.addControl(
      "tournamentType",
      new FormControl("", Validators.required)
    );
    this.conditions.addControl("isScreenshotRequired", new FormControl(false));
    this.conditions.addControl("isShowCountryFlag", new FormControl(false));
    this.conditions.addControl(
      "isManualApproverParticipant",
      new FormControl(false)
    );
    this.conditions.addControl(
      "isSpecifyAllowedRegions",
      new FormControl(false)
    );
    this.conditions.addControl("scoreReporting", new FormControl(1));

    this.tConditions.subscribe((condition) => {
      this.updatetConditions(condition);
    });
  }

  // handlers

  prizeClickHandler(checked, prizeLen = 1) {
    if (checked) {
      this.conditions.addControl(
        "prizeList",
        this.fb.array([], this.customPrizeValidator())
      );
      for (let i = 0; i < prizeLen; i++) this.addPrize(i);
      this.conditions.addControl(
        "prizeCurrency",
        new FormControl("", Validators.compose([Validators.required]))
      );
    } else {
      this.conditions.removeControl("prizeList");
      this.conditions.removeControl("prizeCurrency");
    }
  }

  sponsorClickHandler(checked, len = 1) {
    if (checked) {
      this.conditions.addControl("sponsors", this.fb.array([]));
      this.addSponsor(len);
    } else {
      this.conditions.removeControl("sponsors");
    }
  }

  venueClickHandler(type, venueLength = 1) {
    if (this.conditions?.value?.venueAddress?.length) {
      this.conditions.removeControl("venueAddress");
    }

    if (["offline", "hybrid"].includes(type)) {
      this.conditions.addControl("venueAddress", this.fb.array([]));
      for (let i = 0; i < venueLength; i++) this.addVenueAddress(type);
    }
  }

  regionHandler = async (check) => {
    try {
      if (check) {
        this.conditions.addControl("regionsAllowed", new FormControl([]));
        const result = await this.modalService.open(this.regionModal, {
          scrollable: true,
          windowClass: "custom-modal-content modal-regions-tournament",
          centered: true,
        }).result;
        this.updateRegionConfig();
      } else {
        this.conditions.value.regionsAllowed.length = 0;
      }
    } catch (error) {
      this.updateRegionConfig();
    }
  };

  // custom validators

  customPrizeValidator(): ValidatorFn {
    return (formArray: FormArray): { [key: string]: any } | null => {
      let total = 0;
      for (let item of formArray.controls) {
        total = total + item.value.value;
      }
      const maxParticipants =
        formArray["_parent"]?.controls?.maxParticipants?.value;

      if (formArray.controls.length > maxParticipants) {
        return { prizeLimit: true };
      }
      const isPrizeValid = formArray.controls.length ? total > 0 : true;
      return isPrizeValid ? null : { prizeMoneyRequired: true };
    };
  }

  emptyCheck = (control: FormControl) => {
    return control.value && control.value.trim() ? null : { required: true };
  };

  enablePrize() {
    if (this.conditions.value.isPrize != true) {
      this.conditions.controls.isPrize.setValue(true);
      this.prizeClickHandler(true);
    }
  }

  addPrize(i): void {
    const prizeList = this.conditions.get("prizeList") as FormArray;
    let name = "";
    switch (true) {
      case prizeList.controls.length == 0:
        name = "1st";
        break;
      case prizeList.controls.length == 1:
        name = "2nd";
        break;
      case prizeList.controls.length == 2:
        name = "3rd";
        break;
      default:
        name = `${prizeList.controls.length + 1}th`;
        break;
    }

    const createPrizeForm = (): FormGroup => {
      const validator = [Validators.required, Validators.pattern("^[0-9]*$")];
      if (i == 0) {
        validator.push(Validators.min(1));
      }
      return this.fb.group({
        name: [name],
        value: ["", Validators.compose(validator)],
      });
    };
    prizeList.push(createPrizeForm());
  }

  clearFieldFromArray(field, index): void {
    const array = this.conditions.get(field) as FormArray;
    array.removeAt(index);
    if (array.length < 1 && field == "sponsors") {
      this.conditions.controls.isIncludeSponsor.setValue(false);
    }
  }

  createSponsor(): FormGroup {
    return this.fb.group({
      sponsorName: [
        "",
        Validators.compose([Validators.required, this.emptyCheck]),
      ],
      website: [
        "",
        Validators.compose([
          Validators.required,
          this.emptyCheck,
          Validators.pattern(EsportsConstantsService?.webUrlRegex),
        ]),
      ],
      playStoreUrl: [
        "",
        Validators.compose([
          Validators.pattern(EsportsConstantsService?.playStoreUrlRegex),
        ]),
      ],
      appStoreUrl: [
        "",
        Validators.compose([
          Validators.pattern(EsportsConstantsService?.appStoreUrlRegex),
        ]),
      ],
      sponsorLogo: ["", Validators.compose([Validators.required])],
      sponsorBanner: ["", Validators.compose([Validators.required])],
    });
  }

  addSponsor(len = 1): void {
    const sponsor = this.conditions.get("sponsors") as FormArray;
    for (let i = 0; i < len; i++) {
      sponsor.push(this.createSponsor());
    }
  }

  createVenueAddress(type): FormGroup {
    if (["offline", "hybrid"].includes(type)) {
      return this.fb.group({
        country: ["", Validators.compose([Validators.required])],
        region: ["", Validators.compose([Validators.required])],
        venue: ["", Validators.compose([Validators.required])],
        ...(type == "hybrid" && {
          stage: ["", Validators.compose([Validators.required])],
        }),
      });
    }
  }

  addVenueAddress(type): void {
    const array = this.conditions.get("venueAddress") as FormArray;
    array.push(this.createVenueAddress(type));
  }

  updateRegionConfig = () => {
    if (!this.conditions?.value?.regionsAllowed?.length) {
      this.conditions.controls.isSpecifyAllowedRegions.setValue(false);
      this.conditions.value.regionsAllowed = [];
    }
  };

  addRegion(event, id) {
    if (event.target.checked) {
      this.conditions.value.regionsAllowed.push(id);
    } else {
      const index = this.conditions.value.regionsAllowed.indexOf(id);
      if (index >= 0) {
        this.conditions.value.regionsAllowed.splice(index, 1);
      }
    }
  }

  removeRegions(index) {
    this.conditions.value.regionsAllowed.splice(index, 1);
    this.updateRegionConfig();
  }

  getCountries = async () => {
    try {
      const data = await this.userService.getAllCountries().toPromise();
      this.countryList = data.countries;
    } catch (error) {
      this.toastService.showError(error?.message);
    }
  };

  updatetConditions(condition) {
    this.isCharged = condition.isCharged;
    this.disableManualApproval = condition.isPaid;
    this.prizeClickHandler(
      condition?.isPrize,
      condition?.prizeList?.length || 0
    );
    this.sponsorClickHandler(
      condition.isIncludeSponsor,
      condition?.sponsors?.length || 0
    );
    this.venueClickHandler(
      condition?.tournamentType,
      condition?.venueAddress?.length || 0
    );
    if (condition?.isSpecifyAllowedRegions) {
      this.conditions.addControl("regionsAllowed", new FormControl([]));
    }

    this.conditions.patchValue({
      ...(condition?.isPrize && {
        prizeList: condition?.prizeList,
        prizeCurrency: condition?.prizeCurrency,
      }),
      ...(condition?.isIncludeSponsor && { sponsors: condition?.sponsors }),
      ...(["offline", "hybrid"].includes(condition?.tournamentType) && {
        venueAddress: condition?.venueAddress,
      }),
      ...(condition?.isSpecifyAllowedRegions && {
        regionsAllowed: condition?.regionsAllowed,
      }),
    });
  }
}
