import { Component, Input, OnInit } from "@angular/core";
import { FormArray, FormGroup } from "@angular/forms";
import { EsportsUserService,EsportsToastService } from "esports";


@Component({
  selector: "app-venue-address",
  templateUrl: "./venue-address.component.html",
  styleUrls: ["./venue-address.component.scss"],
})
export class VenueAddressComponent implements OnInit {
  @Input() customFormArrayName: string;
  @Input() index: number;
  @Input() customFormGroup: FormGroup;
  @Input() type: string;

  countryList = [];
  stageMatchFormat = {
    quarterFinal: "TOURNAMENT.QUARTER_FINAL",
    semiFinal: "TOURNAMENT.SEMI_FINAL",
    final: "TOURNAMENT.FINAL",
  };

  stateList = [];

  constructor(
    private userService: EsportsUserService,
    private toastService: EsportsToastService
  ) {}

  ngOnInit(): void {
    this.fetchCountries();
  }

  fetchCountries = async () => {
    try {
      this.customFormGroup.value.venueAddress[this.index].region = "";
      const data = await this.userService.getAllCountries().toPromise();
      this.countryList = data.countries;
    } catch (error) {
      this.toastService.showError(error?.message);
    }
  };

  onCountryChange = async (countryname) => {
    this.stateList.length = 0;
    ((this.customFormGroup.get("venueAddress") as FormArray)?.controls[
      this.index
    ] as FormGroup)
      ?.get("region")
      .reset("");
    const country = this.countryList.find((item) => item.name == countryname);
    const data = await this.userService.getStates().toPromise();
    this.stateList = data.states.filter(
      (item) => item.country_id == country.id
    );
  };
}
