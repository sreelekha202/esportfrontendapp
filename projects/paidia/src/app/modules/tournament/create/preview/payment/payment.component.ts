import {Component,Input,OnChanges,OnInit,SimpleChanges} from "@angular/core";
import {EsportsTransactionService,EsportsTournamentService,EsportsLanguageService,EsportsToastService} from "esports";
import { Router } from "@angular/router";
import { IUser } from "esports";
import { environment } from '../../../../../../environments/environment';
const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;



@Component({
  selector: "app-payment",
  templateUrl: "./payment.component.html",
  styleUrls: ["./payment.component.scss"],
})
export class PaymentComponent implements OnInit, OnChanges {
  @Input() tournament;
  selectPaymentType = "";
  transaction;
  isProccesing = true;
  user: IUser;
  paymentMethods = [];
  prizePools = [];
  totalAmounts;
  isPaymentLoaded = false;
  platformFee;
  totalAmountInclPlatformFee;
  constructor(
    private transactionService: EsportsTransactionService,
    private tournamentService: EsportsTournamentService,
    private toastService: EsportsToastService,
    private router: Router,
    public languageService: EsportsLanguageService
  ) {}

  ngOnInit(): void {}

  fetchtransaction = async () => {
    try {
      this.isProccesing = true;
      this.isPaymentLoaded = true;
      const payload = {
        tournamentId: this.tournament?._id,
        type: "organizer",
      };

      const { data } = await this.transactionService.createOrUpdateTransaction(API,payload);
      this.platformFee = data.platformFee;
      this.totalAmounts = {
        prizeLabel: "PROFILE.TOURNAMENTS.PAYMENT.PRIZE_AMOUNT",
        amount: data.prizeList.reduce((ac, el) => ac + Number(el.value), 0),
      };
      this.totalAmountInclPlatformFee =
        (this.totalAmounts.amount * this.platformFee) / 100 +
        this.totalAmounts.amount;
      this.prizePools = data?.prizeList?.map((item) => {
        let img;

        if (item.name == "1st") {
          img = "../assets/images/payment/Gold.svg";
        } else if (item.name == "2nd") {
          img = "../assets/images/payment/Silver.svg";
        } else if (item.name == "3rd") {
          img = "../assets/images/payment/Bronze.svg";
        } else {
          img = "../assets/images/payment/Bronze.svg";
        }

        return {
          ...item,
          img,
        };
      });

      this.transaction = data;
      if (this.transaction.provider == "paypal") {
        this.paymentMethods = [
          {
            img: "../assets/images/payment/paypal.png",
            name: "Paypal",
            value: "paypal",
          },
        ];
      }
      this.isProccesing = false;
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
      this.isProccesing = false;
    }
  };

  getPaymentResponse = async (order) => {
    try {
      this.isProccesing = true;
      const payload = {
        tournamentId: this.tournament?._id,
        transactionId: this.transaction?._id,
        txnId: order?.id,
        provider: "paypal",
      };
      const response = await this.transactionService.createOrUpdateTransaction(API,payload);
      this.toastService.showSuccess(response?.message);
      this.router.navigate(["/profile/my-tournament/created"]);
      this.isProccesing = false;
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
      this.isProccesing = false;
      this.fetchtransaction();
    }
  };

  getPaymentProgressStatus = (status) => {
    this.tournamentService.isTournamentPaymentProcessing = status;
  };

  ngOnChanges(simpleChanges: SimpleChanges) {
    if (simpleChanges.hasOwnProperty("tournament") && this.tournament) {
      if (!this.isPaymentLoaded) {
        this.fetchtransaction();
      }
    }
  }
}
