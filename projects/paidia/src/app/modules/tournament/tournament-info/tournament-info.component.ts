import { Component, OnInit } from "@angular/core";
import { AppHtmlRoutes } from "../../../app-routing.model";
import { EsportsLanguageService } from "esports";
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: "app-tournament-info",
  templateUrl: "./tournament-info.component.html",
  styleUrls: ["./tournament-info.component.scss"],
})
export class TournamentInfoComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;

  slide1 = "assets/images/Tournament/slide-1.png";
  slide2 = "assets/images/Tournament/slide-2.png";
  slide3 = "assets/images/Tournament/slide-3.png";

  constructor(
    private translateService: TranslateService,
    private languageService: EsportsLanguageService
  ) {}

  ngOnInit(): void {
    this.languageService.language.subscribe((lang) =>
      this.translateService.use(lang || "en")
    );
  }
}
