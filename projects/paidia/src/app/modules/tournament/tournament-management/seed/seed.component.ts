import {
  Component,
  OnInit,
  Input,
  SimpleChanges,
  Output,
  EventEmitter,
  OnChanges,
} from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import {
  EsportsBracketService,
  EsportsLanguageService,
  EsportsToastService,
} from 'esports';
import { BracketService, TournamentService } from '../../../../core/service';
import { IPagination, ITournament } from '../../../../shared/models';

@Component({
  selector: 'app-seed',
  templateUrl: './seed.component.html',
  styleUrls: ['./seed.component.scss'],
})
export class SeedComponent implements OnInit, OnChanges {
  arebicText = false;
  @Output() message = new EventEmitter<boolean>(false);
  @Input() tournament: ITournament;
  isSeeded = false;
  isDisableCreateBracket: boolean = false;
  allApprovedParticipants: Array<any> = [];
  allSelectedParticipants: Array<any> = [];
  participantList: Array<{}> = [];
  structure;
  activePage = 1;
  pageSize = 10;
  seedingMethodList = [
    { name: 'SEED.BY_REGISTRATION', value: 'regOrder' },
    { name: 'SEED.RANDOM', value: 'random' },
  ];
  filterList = [
    { name: 'SEED.ALL_PLAYERS', value: 'all-players' },
    { name: 'SEED.CHECK_IN', value: 'checked-in-only' },
    // { name: 'Not Previously Seeded', value: 'not-previously-seeded' },
  ];
  listingMethod = [
    {
      number: 5,
      name: 'MANAGE_TOURNAMENT.SEED_BRACKET.ITEMS_PER_PAGE',
      value: '5',
    },
    {
      number: 10,
      name: 'MANAGE_TOURNAMENT.SEED_BRACKET.ITEMS_PER_PAGE',
      value: '10',
    },
    {
      number: 20,
      name: 'MANAGE_TOURNAMENT.SEED_BRACKET.ITEMS_PER_PAGE',
      value: '20',
    },
  ];
  selectFilterType = null;
  selectedSeedingMethod = null;
  itemPerPageList = [5, 10, 15, 20, 25];
  selectedPaginatingMethod = null;
  isChangeInSeedingOrder = false;
  apiLoaded: Array<boolean> = [];
  isLoaded = false;
  page: IPagination;
  searchKey: string | null = '';

  constructor(
    private tournamentService: TournamentService,
    private bracketService: EsportsBracketService,
    private toastService: EsportsToastService,
    private modalService: NgbModal,
    private translateService: TranslateService,
    public language: EsportsLanguageService
  ) {}

  ngOnInit(): void {
    this.language.language.subscribe((lang) => {
      if (lang) {
        this.arebicText = lang === 'fs' ? true : false;
      }
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (
      changes.hasOwnProperty('tournament') &&
      changes.tournament.currentValue
    ) {
      if (this.tournament?.isSeeded) {
        if (['single', 'double'].includes(this.tournament?.bracketType)) {
          this.fetchAllMatches();
        } else {
          this.isLoaded = true;
        }
      } else {
        this.fetchAllApprovedParticipants();
      }
    }
  }

  /** Initialize table after filter or search */
  initializeTable() {
    this.activePage = 1;
    this.page = {
      totalItems: this.allSelectedParticipants.length,
      itemsPerPage: this.pageSize,
      maxSize: 5,
    };
    this.participantList = this.allSelectedParticipants.slice(
      this.pageSize * (this.activePage - 1),
      this.pageSize * this.activePage
    );
  }

  /** Get all approved participants of the tournament  */
  fetchAllApprovedParticipants = async () => {
    try {
      this.apiLoaded.push(false);
      const encodeUrl = `?query=${encodeURIComponent(
        JSON.stringify({
          tournamentId: this.tournament?._id,
          participantStatus: 'approved',
        })
      )}&select=checkedIn,teamName,name,seed,participantType,teamMembers,matchWin,name`;
      const participants = await this.tournamentService
        .getParticipants(encodeUrl)
        .toPromise();
      this.allApprovedParticipants = participants.data.map((obj, index) => {
        const record = obj;
        record.reg = index + 1;
        return record;
      });
      this.allSelectedParticipants = this.allApprovedParticipants.filter(
        (item: any) => {
          if (
            !this.selectFilterType ||
            this.selectFilterType?.value !== 'checked-in-only'
          ) {
            return true;
          }
          return item.checkedIn;
        }
      );
      this.initializeTable();
      this.apiLoaded.push(true);
    } catch (error) {
      this.apiLoaded.push(true);
      this.toastService.showError(error.message);
    } finally {
      this.checkAllApiDataLoaded();
    }
  };

  /** Validate seeding value */
  isDisableSaveAll() {
    const teamSet = new Set();
    let isInvalid = false;
    let reason = '';
    isInvalid = this.allApprovedParticipants
      .filter((item: any) => {
        if (
          !this.selectFilterType ||
          this.selectFilterType?.value !== 'checked-in-only'
        ) {
          return true;
        }
        return item.checkedIn;
      })
      .some((obj: any, index, array) => {
        if (!obj.seed || obj.seed < 1 || obj.seed > array.length) {
          reason = this.translateService.instant(
            'MANAGE_TOURNAMENT.SEED_BRACKET.INCOMPLETE_SEEDING'
          );
          return true;
        } else if (teamSet.size === teamSet.add(obj.seed).size) {
          reason = this.translateService.instant(
            'MANAGE_TOURNAMENT.SEED_BRACKET.DUPLICATE_SEED_ID'
          );
          return true;
        }
      });
    return { isValid: !isInvalid, reason };
  }

  /** Save seeding value of participants */
  saveAllSeeds = async (isShowMessage = true) => {
    try {
      const { isValid, reason } = this.isDisableSaveAll();
      if (!isValid) {
        isShowMessage ? this.toastService.showError(reason) : null;
        return { isValid: false, reason };
      }
      const data = this.allApprovedParticipants.filter((item: any) => {
        if (
          !this.selectFilterType ||
          this.selectFilterType?.value !== 'checked-in-only'
        ) {
          return true;
        }
        return item.checkedIn;
      });
      const response = await this.tournamentService
        .updateParticipantBulk({ data })
        .toPromise();
      this.isChangeInSeedingOrder = false;
      this.toastService.showSuccess(response.message);
      return { isValid: true };
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
      return {
        isValid: false,
        reason: error?.error?.message || error?.message,
      };
    }
  };

  /** filter participant by checked-in, not-previously-seed */
  filterByType(data) {
    const isExist = (item) => {
      return this.tournament?.participantType == 'individual'
        ? item?.name.toLowerCase().indexOf(this.searchKey.trim()) !== -1
        : item?.teamName.toLowerCase().indexOf(this.searchKey.trim()) !== -1;
    };

    const value = this.searchKey.toLowerCase().trim();
    switch (data.value) {
      case 'all-players':
        this.allSelectedParticipants = this.allApprovedParticipants.filter(
          (item: any) => {
            if (!value) {
              return true;
            }
            return item?.name.indexOf(this.searchKey.trim()) !== -1;
          }
        );
        break;
      case 'not-previously-seeded':
        this.allSelectedParticipants = this.allApprovedParticipants
          .filter((item: any) => item.seed)
          .filter((item: any) => {
            if (!this.searchKey || !this.searchKey.trim()) {
              return true;
            }
            return item?.name.indexOf(this.searchKey.trim()) !== -1;
          });
        break;
      case 'checked-in-only':
        this.allSelectedParticipants = this.allApprovedParticipants
          .filter((item: any) => item.checkedIn)
          .filter((item: any) => {
            if (!this.searchKey || !this.searchKey.trim()) {
              return true;
            }
            return item?.name.indexOf(this.searchKey.trim()) !== -1;
          });
        break;
      default:
        this.allSelectedParticipants = [];
        break;
    }
    this.initializeTable();
  }
  /** Client-side pagination for client by change page no or page size */
  pageSizeChanged(pageSize) {
    this.participantList = this.allSelectedParticipants.slice(
      pageSize * 0,
      pageSize * 1
    );
    this.initializeTable();
  }

  pageChanged(page) {
    this.participantList = this.allSelectedParticipants.slice(
      this.pageSize * (page - 1),
      this.pageSize * page
    );
  }

  /** Generate mock bracket structure */
  generateBracket = async () => {
    try {
      const data: any = this.isChangeInSeedingOrder
        ? await this.saveAllSeeds(false)
        : this.isDisableSaveAll();

      if (!data.isValid) {
        throw new Error(data.reason);
      }

      const payload = {
        tournamentId: this.tournament?._id,
        ...(this.selectFilterType?.value === 'checked-in-only' && {
          checkedIn: true,
        }),
      };
      const response = await this.bracketService.generateBracket(payload);
      this.structure = response.data;
      this.isSeeded = true;
    } catch (error) {
      this.toastService.showError(
        error?.error?.error || error?.error?.message || error.message
      );
    }
  };

  /** Confirmation modal for bracket generation */
  openModal(content) {
    this.modalService.open(content, {
      windowClass: 'confirm-modal-content',
      centered: true,
    });
  }

  /** Get all saved matches of the tournamnet */
  fetchAllMatches = async () => {
    try {
      this.apiLoaded.push(false);
      const queryParams = {
        tournamentId: this.tournament?._id,
        select: `select=bye,currentMatch,matchStatus,teamA,teamB,disQualifiedTeam,placeHolderA,placeHolderB,teamAWinSet,teamBWinSet,isThirdPlaceMatch,matchNo,battleRoyalTeams.noOfKill,battleRoyalTeams.placement,battleRoyalTeams.score,battleRoyalTeams._id,battleRoyalScoringOption,externalMatchId`,
      };

      const response = await this.bracketService.fetchAllMatchesV3(queryParams);
      if (['single', 'double'].includes(this.tournament?.bracketType)) {
        this.structure = this.bracketService.assembleStructure(response.data);
      } else {
        this.structure = response.data;
      }
      this.apiLoaded.push(true);
    } catch (error) {
      this.apiLoaded.push(true);
      this.toastService.showError(error.message);
    } finally {
      this.checkAllApiDataLoaded();
    }
  };

  /** Confirm mock structure and save it in data base */
  confirmBracket = async () => {
    try {
      const payload = {
        tournamentId: this.tournament?._id,
        ...(this.selectFilterType?.value === 'checked-in-only' && {
          checkedIn: true,
        }),
      };
      const response = await this.bracketService.saveGeneratedBracket(payload);
      this.toastService.showSuccess(response.message);
      this.message.emit(true);
    } catch (error) {
      this.toastService.showError(error.message);
    }
  };

  /** Get response from child component */
  getResponseMessage(data) {
    if (data) {
      this.message.emit(true);
    }
  }

  /** Assign seed value via Registration order, random order or manually. */
  assignSeedValue(method) {
    switch (method.value) {
      case 'regOrder':
        this.seedByRegNo();
        break;
      case 'random':
        this.seedRandomly();
        break;
      default:
        break;
    }
  }

  seedByRegNo() {
    this.isChangeInSeedingOrder = true;
    this.allApprovedParticipants
      .filter((item: any) => {
        if (
          !this.selectFilterType ||
          this.selectFilterType?.value !== 'checked-in-only'
        ) {
          return true;
        }
        return item.checkedIn;
      })
      .forEach((participant: any, index) => {
        participant.seed = index + 1;
        const main_index = this.allApprovedParticipants.findIndex(
          (item: any) => item._id === participant._id
        );
        this.allApprovedParticipants[main_index].seed = index + 1;
      });
  }

  seedRandomly() {
    const tempArray = [];
    this.isChangeInSeedingOrder = true;
    const availableParticipant = this.allApprovedParticipants.filter(
      (item: any) => {
        if (
          !this.selectFilterType ||
          this.selectFilterType?.value !== 'checked-in-only'
        ) {
          return true;
        }
        return item.checkedIn;
      }
    );
    while (tempArray.length < availableParticipant.length) {
      const seed = Math.floor(Math.random() * availableParticipant.length) + 1;
      if (tempArray.indexOf(seed) === -1) {
        tempArray.push(seed);
      }
    }
    availableParticipant.forEach((participant: any, index) => {
      const mainIndex = this.allApprovedParticipants.findIndex(
        (item: any) => item._id === participant._id
      );
      const subIndex = this.allSelectedParticipants.findIndex(
        (item: any) => item._id === participant._id
      );
      if (mainIndex >= 0) {
        this.allApprovedParticipants[mainIndex].seed = tempArray[index];
      }
      if (subIndex >= 0) {
        this.allSelectedParticipants[subIndex].seed = tempArray[index];
      }
    });
  }

  manualSeedValidation(index, _id, value) {
    const tempArrayLength = this.allApprovedParticipants.filter((item: any) => {
      if (
        !this.selectFilterType ||
        this.selectFilterType?.value !== 'checked-in-only'
      ) {
        return true;
      }
      return item.checkedIn;
    }).length;
    const mainIndex = this.allApprovedParticipants.findIndex(
      (item: any) => item._id === _id
    );
    this.allApprovedParticipants[mainIndex].seed = value && Number(value);
    if (value) {
      this.isChangeInSeedingOrder = true;
      if (value < 1) {
        this.allSelectedParticipants[index].seed = 1;
        this.allApprovedParticipants[mainIndex].seed = 1;
      } else if (value > tempArrayLength) {
        this.allSelectedParticipants[index].seed = tempArrayLength;
        this.allApprovedParticipants[mainIndex].seed = tempArrayLength;
      }
    }
  }

  /**
   * Check All API's Are Finished.
   * If All APi's Finished Then Remove Full Page Loader
   */
  checkAllApiDataLoaded() {
    const startedApiCount = this.apiLoaded.filter((el) => !el).length;
    const finishedApiCount = this.apiLoaded.filter((el) => el).length;
    this.isLoaded = startedApiCount === finishedApiCount;
  }

  showInfo(type) {
    switch (type) {
      case 'reset':
        this.selectFilterType = '';
        this.selectedSeedingMethod = '';
        this.selectedPaginatingMethod = '';
        this.pageSize = 5;
        this.toastService.showInfo(
          this.translateService.instant(
            'MANAGE_TOURNAMENT.SEED_BRACKET.RESET_INFO'
          )
        );
        break;
      case 'shuffle':
        this.toastService.showInfo(
          this.translateService.instant(
            'MANAGE_TOURNAMENT.SEED_BRACKET.SHUFFLE_INFO'
          )
        );
        break;
      default:
        break;
    }
  }

  onSearch(text) {
    const value = text.toLowerCase().trim();
    this.allSelectedParticipants = this.allApprovedParticipants
      .filter((item: any) => {
        if (
          !this.selectFilterType ||
          this.selectFilterType?.value !== 'checked-in-only'
        ) {
          return true;
        }
        return item.checkedIn;
      })
      .filter(
        (item: any) => item?.teamName.toLowerCase().indexOf(value) !== -1
      );

    this.initializeTable();
  }

  onClearSearch(event) {
    setTimeout(() => {
      if (!event?.target?.value) {
        this.onSearch(''); // reset search
      }
    }, 100);
  }
}
