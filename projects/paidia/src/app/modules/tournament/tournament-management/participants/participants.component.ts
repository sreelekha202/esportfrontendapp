import {
  Component,
  OnInit,
  Input,
  OnChanges,
  SimpleChanges,
  Inject,
} from "@angular/core";
import { DOCUMENT } from "@angular/common";
import { Router } from "@angular/router";
import { TournamentService } from "../../../../core/service";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { TranslateService } from "@ngx-translate/core";
import { IUser } from "esports";
import {
  InfoPopupComponent,
  InfoPopupComponentData,
  InfoPopupComponentType,
} from "../../../../shared/popups/info-popup/info-popup.component";
import { MatDialog } from "@angular/material/dialog";
import { EsportsLanguageService, EsportsToastService } from "esports";
@Component({
  selector: "app-participants",
  templateUrl: "./participants.component.html",
  styleUrls: [
    "./participants.component.scss",
    "../tournament-management.component.scss",
  ],
})
export class ParticipantsComponent implements OnInit, OnChanges {
  @Input() tournamentDetails;
  @Input() currentTab: string;
  @Input() user: IUser;
  participantsListPending: Array<{}> = [];
  participantsListApproved: Array<{}> = [];
  participantsListRejected: Array<{}> = [];

  selectedList: Array<{}> = [];
  selectedTeam = null;
  selectedParticipantStatus = null;
  isLoaded = false;
  isOngoingOrCompletedTournament = false;

  allParticipantsList: Array<any> = [];
  csvData: Array<{}> = [];
  isEnable;
  buttonDisabled = false;
  showRegCloseBtn = false;
  constructor(
    private tournamentService: TournamentService,
    private toastService: EsportsToastService,
    private modalService: NgbModal,
    private languageService: EsportsLanguageService,
    private translateService: TranslateService,
    private router: Router,
    private matDialog: MatDialog,
    @Inject(DOCUMENT) private document: Document
  ) { }

  ngOnInit(): void {
    this.languageService.language.subscribe((lang) =>
      this.translateService.use(lang || "en")
    );
  }

  ngOnChanges(simpleChanges: SimpleChanges) {
    if (
      simpleChanges.hasOwnProperty("currentTab") &&
      simpleChanges.currentTab
    ) {
      this.selectedTeam = null;
      this.setParticipant();
    }

    if (
      simpleChanges.hasOwnProperty("tournamentDetails") &&
      simpleChanges.tournamentDetails
    ) {
      this.isOngoingOrCompletedTournament =
        this.tournamentDetails?.isSeeded || this.tournamentDetails?.isFinished;
      if (this.tournamentDetails?._id) {
        this.fetchParticipants();
      }
    }
    const currentDate = new Date();
    this.isEnable = this.tournamentDetails?.isRegistrationClosed;

    if (
      typeof this.tournamentDetails?.regEndDate == "string" ||
      typeof this.tournamentDetails?.regStartDate == "string"
    ) {
      this.tournamentDetails.regEndDate = new Date(
        this.tournamentDetails.regEndDate
      );
      this.tournamentDetails.regStartDate = new Date(
        this.tournamentDetails.regStartDate
      );
    }

    if (
      this.user?._id === this.tournamentDetails?.createdBy &&
      this.tournamentDetails?.regEndDate > currentDate &&
      currentDate > this.tournamentDetails?.regStartDate
    ) {
      this.showRegCloseBtn = true;
    }
    if (this.tournamentDetails?.isRegistrationClosed == true) {
      this.buttonDisabled = true;
      this.showRegCloseBtn = true;
    }
  }

  /**
   * Fetch All Participants Of the Tournament
   */
  fetchParticipants = async () => {
    try {
      this.isLoaded = false;
      const encodeUrl = `?query=${encodeURIComponent(
        JSON.stringify({ tournamentId: this.tournamentDetails?._id })
      )}`;
      const participants = await this.tournamentService
        .getParticipants(encodeUrl)
        .toPromise();

      this.allParticipantsList = participants?.data;
      this.participantsListPending = participants?.data.filter(
        (item) => !item?.participantStatus
      );

      this.participantsListApproved = participants?.data.filter(
        (item) => item?.participantStatus === "approved"
      );

      this.participantsListRejected = participants?.data.filter(
        (item) => item?.participantStatus === "rejected"
      );
      this.setParticipant();
      this.isLoaded = true;
    } catch (error) {
      this.isLoaded = true;
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  /**
   * Open Confirmation Modal To Approve Or Reject Participant
   * @param content model template
   * @param participant participant details
   * @param isIndividaul check
   */
  openModelOrUpdateTeam = async (content, participant, isIndividaul = true) => {
    this.selectedParticipantStatus = participant?.participantStatus;
    const result = isIndividaul
      ? await this.modalService.open(content, {
        windowClass: "confirm-modal-content",
        centered: true,
      }).result
      : content;
    if (result === "approved") {
      const data = {
        participantStatus: "approved",
      };
      this.updateParticipant(participant._id, data);
    } else if (result === "rejected") {
      const data = {
        participantStatus: "rejected",
      };

      this.updateParticipant(participant._id, data);
    } else if (result === "edit") {
      this.tournamentService.setSelectedTeam(participant);
      this.router.navigate([
        "/tournament/" +
        this.tournamentDetails?.slug +
        "/join" +
        "/" +
        participant._id,
      ]);
    } else {
      this.selectedTeam = null;
    }
    this.selectedParticipantStatus = null;
  };

  /**
   * Update Participant Status
   * @param id participantID
   * @param data participant status
   */
  updateParticipant = async (id, data) => {
    try {
      if (this.isOngoingOrCompletedTournament) {
        this.toastService.showError(
          this.translateService.instant(
            "MANAGE_TOURNAMENT.PARTICIPANT.DONT_UPDATE_PARTICIPANT_STATUS"
          )
        );
        this.selectedTeam = null;
        return;
      }
      const response = await this.tournamentService
        .updateParticipant(id, data)
        .toPromise();
      this.selectedTeam = null;
      this.toastService.showSuccess(response?.message);
      this.fetchParticipants();
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  /**
   * Set Participant List Based On Current Tab
   */
  setParticipant() {
    switch (true) {
      case this.currentTab === "pendingParticipants":
        this.selectedList = this.participantsListPending;
        break;
      case this.currentTab === "approvedParticipants":
        this.selectedList = this.participantsListApproved;
        break;
      case this.currentTab === "rejectedParticipants":
        this.selectedList = this.participantsListRejected;
        break;
      default:
        this.selectedList = [];
        break;
    }
  }

  exportToCSV() {
    this.csvData = [];

    const elimantionType = {
      single: "SINGLE ELIMINATION",
      double: "DOUBLE ELIMINATION",
      round_robin: "ROUND ROBIN",
      battle_royale: "BATTLE ROYAL",
    };

    const exportCSV = (
      data,
      teamName,
      captainName,
      status,
      pNameSubstr,
      isTeam
    ) => {
      return {
        ...(isTeam && {
          "Team Name": teamName || "",
          "Captain Name": captainName || "",
        }),
        "Player Name": data?.name ? `${pNameSubstr}${data?.name}` : "N/A",
        "Email Id": data?.email || "N/A",
        "Game Id": data?.inGamerUserId || "N/A",
        ...(status && { Status: status }),
      };
    };

    const tournament = {
      "Tournament Name": this.tournamentDetails.name,
      "Tournament Type": elimantionType[this.tournamentDetails.bracketType],
    };

    for (let item of this.allParticipantsList) {
      this.csvData.push(
        exportCSV(
          item,
          item?.teamName,
          item?.name,
          item?.participantStatus || "pending",
          "",
          true
        )
      );
      for (let teamMember of item?.teamMembers || []) {
        this.csvData.push(exportCSV(teamMember, "", "", "", "", true));
      }
      for (let substituteMember of item?.substituteMembers || []) {
        this.csvData.push(
          exportCSV(substituteMember, "", "", "", "(SUB) ", true)
        );
      }
    }

    this.JSONToCSVConvertor(
      this.csvData,
      this.tournamentDetails.name,
      true,
      tournament
    );
  }

  JSONToCSVConvertor(JSONData, ReportTitle, ShowLabel, tournamentDetail) {
    var arrData = typeof JSONData != "object" ? JSON.parse(JSONData) : JSONData;
    var row = "";
    var CSV = "";

    if (ShowLabel) {
      for (let i in tournamentDetail) {
        row += i + ",";
      }
      row = row.slice(0, -1);
      CSV += row + "\r\n";
      row = "";
      for (let i in tournamentDetail) {
        row += '"' + tournamentDetail[i] + '",';
      }
      row.slice(0, row.length - 1);
      CSV += row + "\r\n";

      CSV += ReportTitle + "\r\n\n";
      row = "";
      for (var index in arrData[0]) {
        row += index + ",";
      }
      row = row.slice(0, -1);
      CSV += row + "\r\n";
    }
    for (var i = 0; i < arrData.length; i++) {
      var row = "";
      for (var index in arrData[i]) {
        row += '"' + arrData[i][index] + '",';
      }
      row.slice(0, row.length - 1);
      CSV += row + "\r\n";
    }

    if (CSV == "") {
      this.toastService.showError("Invalid data");
      return;
    }
    var universalBOM = "\uFEFF";
    var fileName = "Participants_";
    fileName += ReportTitle.replace(/ /g, "_");
    var uri =
      "data:text/csv;charset=utf-8," + encodeURIComponent(universalBOM + CSV);
    var link = this.document.createElement("a");
    link.href = uri;
    link.download = fileName + ".csv";
    this.document.body.appendChild(link);
    link.click();
    this.document.body.removeChild(link);
  }
  async closeRegistration(value) {
    const currDate = new Date();
    const data: InfoPopupComponentData = {
      ...this.popUpTitleAndText(this.tournamentDetails),
      type: InfoPopupComponentType.confirm,
      btnText: this.translateService.instant("TOURNAMENT.CONFIRM"),
    };

    const confirmed = await this.matDialog
      .open(InfoPopupComponent, { data })
      .afterClosed()
      .toPromise();
    if (confirmed) {
      this.tournamentService
        .updateTournament(
          {
            isRegistrationClosed: value,
            regEndDate: currDate,
          },
          this.tournamentDetails._id
        )
        .subscribe(
          (res) => {
            if (res) {
              this.toastService.showSuccess("Registration closed");
              this.isEnable = res.data.isRegistrationClosed;
              this.buttonDisabled = true;
              let currentUrl = this.router.url;
              this.router
                .navigateByUrl("/", { skipLocationChange: true })
                .then(() => {
                  this.router.navigate([currentUrl]);
                });
            } else {
            }
          },
          (err: any) => {
            this.toastService.showError("Error");
            let currentUrl = this.router.url;
            this.router
              .navigateByUrl("/", { skipLocationChange: true })
              .then(() => {
                this.router.navigate([currentUrl]);
              });
          }
        );
    } else {
      this.isEnable = !value;
      let currentUrl = this.router.url;
      this.router.navigateByUrl("/", { skipLocationChange: true }).then(() => {
        this.router.navigate([currentUrl]);
      });
    }
  }
  popUpTitleAndText = (data) => {
    if (data?._id) {
      return {
        title: this.translateService.instant(
          "TOURNAMENT.CLOSE_REGISTRATION_HEADER"
        ),
        text: this.translateService.instant("TOURNAMENT.CLOSE_REGISTRATION"),
      };
    }
  };
}
