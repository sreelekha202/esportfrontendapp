import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { AppHtmlRoutes } from "../../app-routing.model";
import { IPagination } from "../../shared/models";
import { ArticleItemComponentData } from "../../core/article-item/article-item.component";
import { faSearch } from "@fortawesome/free-solid-svg-icons";
import { TranslateService } from "@ngx-translate/core";
import { ActivatedRoute, Router } from "@angular/router";
import { Location } from "@angular/common";
import { EsportsLanguageService, EsportsOptionService, EsportsTournamentService, EsportsArticleService } from 'esports';
import { environment } from '../../../environments/environment';
const API = environment.apiEndPoint;

@Component({
  selector: "app-tournament",
  templateUrl: "./tournament.component.html",
  styleUrls: ["./tournament.component.scss"],
})
export class TournamentComponent implements OnInit {

  @ViewChild("mainContainer", { read: ElementRef }) public scroll: ElementRef<any>;
  AppHtmlRoutes = AppHtmlRoutes;
  showLoader: boolean = true;
  currentTab = "upcoming";
  page: IPagination;
  allTournamentList: any = [];
  allTournamentListClone: any = [];
  trendingPosts: ArticleItemComponentData[] = [];
  test: String;
  gameId;
  tournamentData;
  current_page = "1";
  categoryList;
  categoryId;
  activeIndex = 0;

  paginationData = {
    page: 1,
    limit: 8,
    sort: "startDate",
  };
  faSearch = faSearch;
  constructor(
    private articleService: EsportsArticleService,
    private languageService: EsportsLanguageService,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private tournamentService: EsportsTournamentService,
    private translateService: TranslateService,
    private optionService: EsportsOptionService,
    private location: Location
  ) { }

  ngOnInit(): void {
    this.languageService.language.subscribe((lang) =>
      this.translateService.use(lang || "en")
    );
    this.activeRoute.queryParams.subscribe((params) => {
      if (params.activeTab && params.activeIndex) {
        this.currentTab = params.activeTab;
        this.changeTab({
          target: { value: params.activeTab, pageIndex: params.activeIndex },
        });
      } else if (!params.activeIndex && this.activeIndex > 0) {
        this.location.back();
      }
      else {
        this.activeIndex = 1;
        this.changeTab({ target: { value: this.currentTab } });
      }
    });
    this.fetchOptions();
  }

  filterByGame(gameId) {
    this.paginationData.page = 1;
    if (gameId === "all") {
      this.gameId = null;
    } else {
      this.gameId = gameId;
    }
    switch (this.currentTab) {
      case "ongoing":
        this.getOnGoingTournamentsPaginated();
        break;
      case "completed":
        this.getCompletedTournamentsPaginated();
        break;
      case "upcoming":
        this.getUpcomingTournamentsPaginated();
        break;
    }
  }

  convert12HrsTo24HrsFormat(time) {
    let hours = Number(time.match(/^(\d+)/)[1]);
    const minutes = Number(time.match(/:(\d+)/)[1]);
    let AP = time.match(/\s(.*)$/);
    if (!AP) {
      AP = time.slice(-2);
    } else {
      AP = AP[1];
    }
    if (AP === "PM" && hours < 12) {
      hours = hours + 12;
    }
    if (AP === "AM" && hours === 12) {
      hours = hours - 12;
    }
    let Hours24 = hours.toString();
    let Minutes24 = minutes.toString();
    if (hours < 10) {
      Hours24 = "0" + Hours24;
    }
    if (minutes < 10) {
      Minutes24 = "0" + Minutes24;
    }
    return { hour: parseInt(Hours24, 10), minute: parseInt(Minutes24, 10) };
  }

  fetchOptions = async () => {
    const option = await Promise.all([this.optionService.fetchAllCategories(API)]);
    this.categoryList = option[0]?.data;
    for (const iterator of this.categoryList) {
      if (iterator.name == "News") {
        this.categoryId = iterator._id;
      }
    }
    this.getNews(this.categoryId);
  };

  getNews(categoryId) {
    const query = JSON.stringify({
      articleStatus: "publish",
      category: categoryId,
    });
    const option = JSON.stringify({ limit: 4, sort: { createdDate: -1 } });
    this.articleService.getArticles_PublicAPI(API, { query, option }).subscribe(
      (res: any) => {
        this.trendingPosts = res.data;
      },
      (err) => { }
    );
  }

  getCompletedTournamentsPaginated() {
    this.current_page = "1";
    this.showLoader = true;
    let query: any = { tournamentStatus: "publish", isFinished: true };

    if (this.gameId) {
      query.gameDetail = this.gameId;
    }

    query = JSON.stringify(query);

    let params = {
      status: "2",
      page: this.paginationData.page,
      limit: this.paginationData.limit,
      sort: "Oldest",
    };

    if (this.gameId) {
      params["game"] = this.gameId;
    }

    this.tournamentService.getPaginatedTournaments(API, params).subscribe(
      (res: any) => {
        this.showLoader = false;
        this.tournamentData = res.data;
        this.page = {
          totalItems: res?.data?.totalDocs,
          itemsPerPage: res?.data?.limit,
          maxSize: 5,
        };
      },
      (err) => {
        this.showLoader = false;
      }
    );
  }

  getOnGoingTournamentsPaginated() {
    this.current_page = "1";
    this.showLoader = true;
    const pagination = JSON.stringify(this.paginationData);
    let query: any = {
      $and: [{ tournamentStatus: "publish" }],
      $or: [{ startDate: { $lt: new Date() } }, { isSeeded: true }],
    };

    if (this.gameId) {
      query.gameDetail = this.gameId;
    }

    query = JSON.stringify(query);

    let params = {
      status: "1",
      page: this.paginationData.page,
      limit: this.paginationData.limit,
      sort: "Latest",
    };

    if (this.gameId) {
      params["game"] = this.gameId;
    }

    this.tournamentService.getPaginatedTournaments(API, params).subscribe(
      (res: any) => {
        this.showLoader = false;
        this.tournamentData = res.data;
        this.page = {
          totalItems: res?.data?.totalDocs,
          itemsPerPage: res?.data?.limit,
          maxSize: 5,
        };
      },
      (err) => {
        this.showLoader = false;
      }
    );
  }

  getUpcomingTournamentsPaginated() {
    this.current_page = "1";
    this.showLoader = true;
    const pagination = JSON.stringify(this.paginationData);
    let query: any = {
      $and: [
        { tournamentStatus: "publish" },
        { startDate: { $gt: new Date() } },
        { isSeeded: false },
        { isFinished: false },
      ],
    };

    if (this.gameId) {
      query.gameDetail = this.gameId;
    }

    query = JSON.stringify(query);
    let params = {
      status: "0",
      page: this.paginationData.page,
      limit: this.paginationData.limit,
      sort: "Latest",
    };

    if (this.gameId) {
      params["game"] = this.gameId;
    }

    this.tournamentService.getPaginatedTournaments(API, params).subscribe(
      (res: any) => {
        this.showLoader = false;
        this.tournamentData = res.data;
        this.page = {
          totalItems: res?.data?.totalDocs,
          itemsPerPage: res?.data?.limit,
          maxSize: 5,
        };
      },
      (err) => {
        this.showLoader = false;
      }
    );
  }

  pageChanged(page): void {
    this.paginationData.page = page;
    this.test = String(this.paginationData.page);
    switch (this.currentTab) {
      case "ongoing":
        this.getOnGoingTournamentsPaginated();
        this.router.navigate(["."], {
          relativeTo: this.activeRoute,
          queryParams: {
            activeTab: "ongoing",
            activeIndex: this.paginationData.page,
          },
        });
        break;
      case "completed":
        //this.current_page= String(this.paginationData.page);
        this.getCompletedTournamentsPaginated();
        this.router.navigate(["."], {
          relativeTo: this.activeRoute,
          queryParams: {
            activeTab: "completed",
            activeIndex: this.paginationData.page,
          },
        });
        break;
      case "upcoming":
        //this.current_page= String(this.paginationData.page);
        this.getUpcomingTournamentsPaginated();
        this.router.navigate(["."], {
          relativeTo: this.activeRoute,
          queryParams: {
            activeTab: "upcoming",
            activeIndex: this.paginationData.page,
          },
        });
        break;
    }
  }

  changeTab(event) {
    this.router.navigate(["."], {
      relativeTo: this.activeRoute,
      queryParams: {
        activeTab: event.target.value,
        activeIndex: event.target.pageIndex ? event.target.pageIndex : 1,
      },
    });
    this.paginationData.page = event.target.pageIndex
      ? +event.target.pageIndex
      : 1;

    switch (event.target.value) {
      case "ongoing":
        this.currentTab = "ongoing";
        this.getOnGoingTournamentsPaginated();
        break;
      case "completed":
        this.currentTab = "completed";
        this.getCompletedTournamentsPaginated();
        break;
      case "upcoming":
        this.currentTab = "upcoming";
        this.getUpcomingTournamentsPaginated();
        break;
    }
  }

  scrollToTop() {
    this.scroll.nativeElement.scrollIntoView();
  }
}
