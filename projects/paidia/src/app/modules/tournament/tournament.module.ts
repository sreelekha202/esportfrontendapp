import { NgModule } from '@angular/core';
import { CoreModule } from '../../core/core.module';
import { TournamentRoutingModule } from './tournament-routing.module';
import { TournamentComponent } from './tournament.component';
import { TournamentInfoComponent } from './tournament-info/tournament-info.component';
import { TournamentSliderComponent } from './tournament-slider/tournament-slider.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { SharedModule } from '../../shared/modules/shared.module';
import { PrizePoollModule } from './pipe/prize-pooll.module';


@NgModule({
  declarations: [
    TournamentComponent,
    TournamentInfoComponent,
    TournamentSliderComponent,
  ],
  imports: [
    CoreModule,
    MatProgressSpinnerModule,
    SharedModule,
    PrizePoollModule,
    TournamentRoutingModule,
  ],
  providers: [],
})
export class TournamentModule { }
