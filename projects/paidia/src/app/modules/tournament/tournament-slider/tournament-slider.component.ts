import { Component, OnInit } from '@angular/core';
import { EsportsArticleService, EsportsOptionService } from 'esports';
import { environment } from '../../../../environments/environment';
const API = environment.apiEndPoint;

@Component({
  selector: 'app-tournament-slider',
  templateUrl: './tournament-slider.component.html',
  styleUrls: ['./tournament-slider.component.scss'],
})
export class TournamentSliderComponent implements OnInit {
  trendingNews = [];
  categoryList;
  categoryId;
  responsiveOptions = [
    {
      breakpoint: '1440px',
      numVisible: 3,
      numScroll: 1,
    },
    {
      breakpoint: '1199px',
      numVisible: 3,
      numScroll: 1,
    },
    {
      breakpoint: '767px',
      numVisible: 3,
      numScroll: 1,
    },
    {
      breakpoint: '575px',
      numVisible: 3,
      numScroll: 1,
    },
  ];

  constructor(
    private articleService: EsportsArticleService,
    private optionService: EsportsOptionService
  ) { }

  ngOnInit(): void {
    this.fetchOptions();
  }

  fetchOptions = async () => {
    const option = await Promise.all([this.optionService.fetchAllCategories(API)]);
    this.categoryList = option[0]?.data;
    for (const iterator of this.categoryList) {
      if (iterator.name == 'News') {
        this.categoryId = iterator._id;
      }
    }
    this.getArticleNews(this.categoryId);
  };

  getArticleNews(categoryId) {
    const query = JSON.stringify({
      articleStatus: 'publish',
      category: categoryId,
    });
    const option = JSON.stringify({ limit: 7, sort: { createdDate: -1 } });
    this.articleService.getArticles_PublicAPI(API, { query, option }).subscribe(
      (res: any) => {
        this.trendingNews = res.data;
      },
      (err) => { }
    );
  }
}
