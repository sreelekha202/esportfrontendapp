import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { environment } from '../../../environments/environment';
import {
  EsportsGameService,
  EsportsUserService,
  EsportsSocialService,
  EsportsToastService,
  IPOST,
  IUser,
} from 'esports';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
const LIMIT_POST = 10;

@Component({
  selector: 'app-public-profile',
  templateUrl: './public-profile.component.html',
  styleUrls: ['./public-profile.component.scss'],
})
export class PublicProfileComponent implements OnInit {
  constructor(
    private _router: Router,
    private _activeRoute: ActivatedRoute,
    private _userService: EsportsUserService,
    private _socialService: EsportsSocialService,
    private _gameService: EsportsGameService,
    private _toastService: EsportsToastService,
    private _translateService: TranslateService
  ) {}

  public userSubscription: Subscription;
  public currentUser: IUser;
  public user: IUser;
  public gamesList: any[];
  public posts: Array<IPOST> = [];
  public page: number = 0;
  public pageTotal: number = 0;

  ngOnInit(): void {
    this._activeRoute.params.subscribe(
      (params) => {
        this.user = { _id: params?.id };
        this.getUserProfile(this.user?._id);
        this.getPosts(0, LIMIT_POST);
      },
      (error) => {
        this.navigateWhenErr(error.status);
      }
    );
    this.getCurrentUser();
    this.getGamesList();
  }

  private getCurrentUser() {
    this._userService.currentUser.subscribe(
      (data) => (this.currentUser = data),
      (err) => this.navigateWhenErr(err.status)
    );
  }

  private getUserProfile(userId: string) {
    this._userService
      .getUserProfileById(environment.apiEndPoint, userId)
      .subscribe(
        (res: any) => {
          const profile: IUser = res?.data;
          if (!profile) {
            this._router.navigate(['/404']);
          }
          const { gameDetails } = profile?.preference;
          this.user = profile;
          this.user.preference.gameDetails = this.gameDetailList(gameDetails);
        },
        (err) => this.navigateWhenErr(err.status)
      );
  }

  private getPosts(skip: number, limit: number) {
    this._socialService.getPostPublic(this.user?._id, skip, limit).subscribe(
      (res) => {
        const { posts, total } = res?.data;
        if (!posts) throw Error();
        this.posts = [...this.posts, ...posts];
        this.page = Math.ceil(this.posts.length / limit);
        this.pageTotal = Math.ceil(total / limit);
      },
      (err) => {
        this._toastService.showError(
          this._translateService.instant('SOCIAL_FEED.MESSAGES.TRY_AGAIN')
        );
        throw err;
      }
    );
  }

  private getGamesList() {
    this._gameService
      .getGames(environment.apiEndPoint)
      .subscribe((res) => (this.gamesList = res?.data));
  }

  private gameDetailList(gameDetails: any[]): any[] {
    let gameArray: any[] = [];
    gameDetails?.map((gameDetail: any) => {
      let gameLogo: any = this.gamesList?.find(
        (game: any) => game?._id == gameDetail?._id
      );
      gameArray.push({
        _id: gameDetail._id,
        name: gameDetail?.name,
        userGameId: gameDetail?.userGameId,
        image: gameLogo?.logo,
      });
    });
    return gameArray;
  }

  private navigateWhenErr(statusCode: number) {
    switch (statusCode) {
      case 401:
        this._router.navigate(['/home']);
        break;
    }
  }
}
