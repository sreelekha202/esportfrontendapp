import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicAboutProfileComponent } from './public-about-profile.component';

describe('PublicAboutProfileComponent', () => {
  let component: PublicAboutProfileComponent;
  let fixture: ComponentFixture<PublicAboutProfileComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PublicAboutProfileComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicAboutProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
