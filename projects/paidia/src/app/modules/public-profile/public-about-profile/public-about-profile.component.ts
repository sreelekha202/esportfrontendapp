import { Component, OnInit, Input, Inject, OnChanges } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { EsportsToastService, EsportsLeaderboardService, IUser } from 'esports';
import { environment } from '../../../../environments/environment';

const MESSAGE = {
  FOLLOW: 'Follow successfully!',
  UNFOLLOW: 'Unfollow successfully!',
};

@Component({
  selector: 'app-public-about-profile',
  templateUrl: './public-about-profile.component.html',
  styleUrls: ['./public-about-profile.component.scss'],
})
export class PublicAboutProfileComponent implements OnInit, OnChanges {
  @Input() user: IUser;
  public isFollow: boolean = false;
  public isShowFollow: boolean = false;
  constructor(
    private _toastService: EsportsToastService,
    private _leaderboardService: EsportsLeaderboardService,
    private _translateService: TranslateService
  ) {}

  ngOnInit(): void {}

  ngOnChanges(): void {
    if (this.user?._id) {
      this.checkFollowUserStatus(this.user._id);
    }
  }

  public changeFollowUserStatus(followUserId: string) {
    this.isFollow ? this.unFollow(followUserId) : this.follow(followUserId);
  }

  public getIconSocialAccountSrc(typeAcount: string): string {
    switch (typeAcount) {
      case 'Twitter':
        return 'assets/images/Profile/twitter.svg';
      case 'Facebook':
        return 'assets/images/Profile/facebook.svg';
      case 'Instagram':
        return 'assets/images/Profile/instagram.svg';
      default:
        return '';
    }
  }

  private checkFollowUserStatus(followUserId: string) {
    this._leaderboardService
      .checkFollowStatus(environment.apiEndPoint, followUserId)
      .subscribe((res:any) => {
         if(res?.data) {
          this.isShowFollow = true;
          this.isFollow = res?.data[0].status === 0 ? false : true;
         }
      });
  }

  private follow(followUserId: string) {
    this._leaderboardService
      .followUser(environment.apiEndPoint, followUserId)
      .subscribe((res) => {
        if (res?.success) {
          this.isFollow = true;
          this.user.preference.followersCount++;
          this._toastService.showSuccess(
            this._translateService.instant('SOCIAL_FEED.MESSAGES.FOLLOW')
          );
        }
      });
  }

  private unFollow(followUserId: string) {
    this._leaderboardService
      .unfollowUser(environment.apiEndPoint, followUserId)
      .subscribe((res) => {
        if (res?.success) {
          this.isFollow = false;
          this.user.preference.followersCount--;
          this._toastService.showSuccess(
            this._translateService.instant('SOCIAL_FEED.MESSAGES.UNFOLLOW')
          );
        }
      });
  }
}
