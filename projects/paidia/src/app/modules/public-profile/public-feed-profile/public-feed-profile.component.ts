import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-public-feed-profile',
  templateUrl: './public-feed-profile.component.html',
  styleUrls: ['./public-feed-profile.component.scss'],
})
export class PublicFeedProfileComponent implements OnInit {
  constructor() {}
  dataFeed = ['Feed'];

  menuFeed = [
    'Feed',
    'Statistics',
    'Teams',
    'Tournaments',
    'Photos/Videos',
    'Content',
  ];

  ngOnInit(): void {}

  handleCLick(item): void {
    let a = this.menuFeed.filter((data, index) => data === item);
    this.dataFeed = a;
  }
}
