import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicFeedProfileComponent } from './public-feed-profile.component';

describe('PublicFeedProfileComponent', () => {
  let component: PublicFeedProfileComponent;
  let fixture: ComponentFixture<PublicFeedProfileComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PublicFeedProfileComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicFeedProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
