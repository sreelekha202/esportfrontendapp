import { Component, OnInit, Input } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { GENDER, IUser } from 'esports';
import { onLoadImgError, TYPE_IMG_LOAD } from '../../../shared/comon';
@Component({
  selector: 'app-public-banner-profile',
  templateUrl: './public-banner-profile.component.html',
  styleUrls: ['./public-banner-profile.component.scss'],
})
export class PublicBannerProfileComponent implements OnInit {
  @Input() user: IUser;
  public onLoadImgError: Function = onLoadImgError;
  public typeImgLoad = TYPE_IMG_LOAD;

  constructor(private _translateService: TranslateService) {}

  ngOnInit(): void {}

  public getGender(gender: GENDER, customPronoun) {
    let pronoun: string;
    switch (gender) {
      case GENDER.HE:
        pronoun = GENDER.HE.toUpperCase();
        break;
      case GENDER.SHE:
        pronoun = GENDER.SHE.toUpperCase();
        break;
      case GENDER.THEY:
        pronoun = GENDER.THEY.toUpperCase();
        break;
      default:
        return customPronoun;
    }
    return this._translateService.instant(`SOCIAL_FEED.USER.GENDER.${pronoun}`);
  }
}
