import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicBannerProfileComponent } from './public-banner-profile.component';

describe('PublicBannerProfileComponent', () => {
  let component: PublicBannerProfileComponent;
  let fixture: ComponentFixture<PublicBannerProfileComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PublicBannerProfileComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicBannerProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
