import { TranslateService } from "@ngx-translate/core";
import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { Location } from "@angular/common";
import { IPagination, IUser } from "../../../shared/models";
import { EsportsArticleService, EsportsLanguageService } from "esports";
import { environment } from "../../../../environments/environment";
const API = environment.apiEndPoint;

@Component({
  selector: "app-article-game",
  templateUrl: "./article-game.component.html",
  styleUrls: ["./article-game.component.scss"],
})
export class ArticleGameComponent implements OnInit {
  user: IUser;
  gameId;
  articles = [];
  jumbotron = {
    _id: "",
    slug: "",
    ribbon: "",
    dateTitle: "",
    title: "",
    isBtnHidden: true,
    btn: "",
    user: {
      img: "",
      name: "",
    },
  };
  hottestPost: any = {};
  paginationData = {
    page: 1,
    limit: 6,
    sort: { createdDate: -1 },
  };
  showLoader = true;
  page: IPagination;

  constructor(
    private location: Location,
    private activatedRoute: ActivatedRoute,
    private articleService: EsportsArticleService,
    public translate: TranslateService,
    public language: EsportsLanguageService
  ) { }

  ngOnInit(): void {
    this.gameId = this.activatedRoute.snapshot.params["id"];
    this.fetchArticles();
  }

  goBack(): void {
    this.location.back();
  }

  getHottestPost() {
    this.articleService.getHottestPost(API).subscribe(
      (res) => {
        if (res && res["data"] && res["data"].length > 0) {
          this.hottestPost = res["data"][0];
          const createdDate = new Date(this.hottestPost["createdDate"]);
          const date = createdDate.getDate();
          const month = createdDate.toLocaleString("default", {
            month: "short",
          });
          this.jumbotron = {
            _id: this.hottestPost?._id,
            slug: this.hottestPost.slug,
            ribbon: "Hottest post",
            dateTitle: `${this.hottestPost?.game} | ${date} ${month}`,
            title: this.hottestPost?.title,
            isBtnHidden: true,
            btn: "read post",
            user: {
              img: this.hottestPost["authorDetails"]["profilePicture"]
                ? this.hottestPost["authorDetails"]["profilePicture"]
                : "assets/images/Profile/avatar.png",
              name: this.hottestPost["authorDetails"]["fullName"],
            },
          };
        }
      },
      (err) => {
        this.showLoader = false;
      }
    );
  }

  pageChanged(page): void {
    this.paginationData.page = page;
    this.fetchArticles();
  }

  fetchArticles = async () => {
    this.showLoader = true;
    try {
      const pagination = JSON.stringify(this.paginationData);
      const query = JSON.stringify({
        gameDetails: this.gameId,
        articleStatus: "publish",
      });
      const prefernce = JSON.stringify({
        prefernce: this.user?.preference?.game
          ? this.user?.preference?.game.map((item) => {
            return { gameDetails: item?._id };
          })
          : "",
      });
      this.articleService
        .getLatestArticle(API, {
          pagination: pagination,
          query: query,
          preference: prefernce,
        })
        .subscribe(
          (res: any) => {
            this.showLoader = false;
            this.articles = res["data"]["docs"];
            this.page = {
              totalItems: res?.data?.totalDocs,
              itemsPerPage: res?.data?.limit,
              maxSize: 5,
            };
            this.getHottestPost();
          },
          (err) => {
            this.showLoader = false;
          }
        );
    } catch (error) {
      this.showLoader = false;
    }
  };
}
