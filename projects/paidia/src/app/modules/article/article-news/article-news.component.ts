import { Component, OnInit } from "@angular/core";
import { IPagination } from "../../../shared/models";
import { EsportsUserService, IUser, EsportsArticleService } from 'esports';
import { environment } from "../../../../environments/environment";
const API = environment.apiEndPoint;

@Component({
  selector: "app-article-news",
  templateUrl: "./article-news.component.html",
  styleUrls: ["./article-news.component.scss"],
})
export class ArticleNewsComponent implements OnInit {
  user: IUser;
  articles: any = [];
  paginationData = {
    page: 1,
    limit: 20,
    sort: { createdDate: -1 },
  };
  page: IPagination;
  constructor(
    private articleService: EsportsArticleService,
    private userService: EsportsUserService
  ) { }

  ngOnInit(): void {
    this.fetchArticles();
  }

  pageChanged(page): void {
    this.paginationData.page = page;
    this.fetchArticles();
  }

  fetchArticles = async () => {
    try {
      const query = JSON.stringify({
        articleStatus: "publish",
      });
      const pagination = JSON.stringify(this.paginationData);
      const prefernce = JSON.stringify({
        prefernce: this.user?.preference?.game
          ? this.user?.preference?.game.map((item) => {
            return { gameDetails: item };
          })
          : "",
      });
      this.articleService
        .getLatestArticle(API, { query, pagination, preference: prefernce })
        .subscribe(
          (res: any) => {
         if(res){
          this.articles = res["data"]["docs"];
         }
          },
          (err) => { }
        );
    } catch (error) { }
  };

  getCurrentUserDetails() {
    this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.user = data;
      }
    });
  }
}
