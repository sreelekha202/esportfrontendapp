import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { AuthorAccessGuard } from "../../../shared/guard/isAuthorInfluencer.guard";
// import { PipeModule } from "esport";
import { CoreModule } from "../../../core/core.module";
import { CreateArticleComponent } from "./create-article.component";
import { PaginationModule, PaginationConfig } from "ngx-bootstrap/pagination";
import { ShareModule } from "ngx-sharebuttons";
import { SharedModule } from "../../../shared/modules/shared.module";
const routes: Routes = [
  {
    path: "",
    canActivate: [AuthorAccessGuard],
    component: CreateArticleComponent,
  },
];

@NgModule({
  declarations: [CreateArticleComponent],
  imports: [
    CoreModule,
    PaginationModule.forRoot(),
    // PipeModule,
    RouterModule.forChild(routes),
    SharedModule,
    ShareModule,
  ],
  providers: [{ provide: PaginationConfig }],
})
export class CreateArticleModule {}
