import {
  Component,
  OnInit,
  Inject,
  PLATFORM_ID,
  OnDestroy,
} from "@angular/core";
import { DOCUMENT, isPlatformBrowser } from "@angular/common";
import { HttpClient } from "@angular/common/http";
import { Router, ActivatedRoute } from "@angular/router";
import { FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { FormService, S3UploadService, } from "../../../core/service";
import { EsportsUserService, EsportsConstantsService, EsportsLanguageService, EsportsArticleService, EsportsToastService, EsportsGameService, IUser, EsportsOptionService } from 'esports';
import { InfoPopupComponent, InfoPopupComponentData, InfoPopupComponentType } from "../../../shared/popups/info-popup/info-popup.component";
import { AutoUnsubscribe } from "ngx-auto-unsubscribe";
import { faTrash } from "@fortawesome/free-solid-svg-icons";
import { MatDialog } from "@angular/material/dialog";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { TranslateService } from "@ngx-translate/core";
import { Subscription } from "rxjs";
import { environment } from '../../../../environments/environment';
const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;

@AutoUnsubscribe()
@Component({
  selector: "app-create-article",
  templateUrl: "./create-article.component.html",
  styleUrls: ["./create-article.component.scss"],
})
export class CreateArticleComponent implements OnInit, OnDestroy {
  isBrowser: boolean;
  isEditorLoaded: boolean = true;
  isinfluenceFlag: boolean = true;
  isinfluencehighlightFlag: boolean = false;
  isLoaded: boolean = false;
  IsmodelShow: boolean = false;
  showLoader: boolean = false;

  articleForm: FormGroup;
  selectedFiles: FileList;
  user: IUser;

  display_user;
  public Editor; // = ClassicEditor;
  userImageSrc;

  articlePath = "article/";
  currLanguage = "";
  currLanguageDisplay = "";
  direction = "direction1";

  apiLoaded: Array<boolean> = [];
  categoryList = [];
  gameList = [];
  genreList = [];
  language = [];
  platformList = [];
  tagsList = [];
  config: any = {};

  authorList = [
    { name: "Mini Miltia", value: "Tseries" },
    { name: "PUBG", value: "Bluehole" },
    { name: "Fortnite", value: "fortnite" },
  ];

  active = 1;
  articleId;
  bannerSRC;
  domain;
  faTrash = faTrash;
  s3RefKey;

  fallBack: string;
  fallBackIndex: any;

  userSubscription: Subscription;

  constructor(
    @Inject(DOCUMENT) private document: Document,
    @Inject(PLATFORM_ID) private platformId,
    private articleService: EsportsArticleService,
    private constantsService: EsportsConstantsService,
    private fb: FormBuilder,
    private formService: FormService,
    private matDialog: MatDialog,
    private modalService: NgbModal,
    private optionService: EsportsOptionService,
    private toastService: EsportsToastService,
    private translateService: TranslateService,
    private userService: EsportsUserService,
    public activatedRoute: ActivatedRoute,
    public gameService: EsportsGameService,
    public http: HttpClient,
    public languageService: EsportsLanguageService,
    public router: Router,
    public s3Service: S3UploadService
  ) {
    this.isBrowser = isPlatformBrowser(platformId);
    // if (this.isBrowser) {
    //   const ClassicEditor = require("@ckeditor/ckeditor5-build-classic");
    //   this.Editor = ClassicEditor;
    // }
  }

  ngOnInit(): void {
    if (this.isBrowser) {
      this.language = this.constantsService?.language;
      const lang = this.translateService.currentLang;
      this.currLanguage = lang == "en" ? "english" : "french";
      this.currLanguageDisplay = this.language[0]?.value;
      this.languageService.language.subscribe((lang) => {
        if (lang) {
          this.config.language = lang;
        }
        // refresh editor
        this.isEditorLoaded = false;
        setTimeout(() => (this.isEditorLoaded = true));
      });

      this.articleId = this.activatedRoute.snapshot.params["id"];
      this.activatedRoute.queryParams.subscribe((params) => {
        if (params?.fallback) {
          this.fallBack = "/admin/content-management";
          this.fallBackIndex = params?.activeTab ? params.activeTab : 0;
        }
      });
      this.getCurrentUserDetails();
      this.createForm();
      this.initializeEditor();
      this.getGames();
      this.fetchOptions();
      this.domain =
        this.document.location.protocol +
        "//" +
        this.document.location.hostname +
        "/";
      this.config = {
        placeholder: "",
        tabsize: 2,
        height: "200px",
        toolbar: [
          ["misc", ["codeview", "undo", "redo"]],
          ["style", ["bold", "italic", "underline", "clear"]],
          [
            "font",
            [
              "bold",
              "italic",
              "underline",
              "superscript",
              "subscript",
              "clear",
            ],
          ],
          ["fontsize", ["fontname", "fontsize", "color"]],
          ["para", ["style", "ul", "ol", "paragraph", "height"]],
          ["insert", ["table", "picture", "link", "video", "hr"]],
        ],
        fontNames: [
          "Helvetica",
          "Arial",
          "Arial Black",
          "Comic Sans MS",
          "Courier New",
          "Roboto",
          "Times",
        ],
      };

      this.language = this.constantsService?.language;
    }
  }

  ngOnDestroy() {
    if (this.userSubscription) {
      this.userSubscription?.unsubscribe();
    }
  }

  fetchOptions = async () => {
    try {
      this.apiLoaded.push(false);
      const option = await Promise.all([
        this.gameService.getAllGames(API, TOKEN).toPromise(),
        this.optionService.fetchAllCategories(API),
        this.optionService.fetchAllGenres(API),
        this.optionService.fetchAllTags(API),
      ]);
      this.gameList = option[0]?.data;
      this.categoryList = option[1]?.data;
      this.genreList = option[2]?.data;
      this.tagsList = option[3]?.data;
      if (this.articleId) {
        this.getArticle(this.articleId);
      }
      this.apiLoaded.push(true);
    } catch (error) {
      this.apiLoaded.push(true);
      this.toastService.showError(error?.error?.message || error.message);
    } finally {
      this.allApiDataLoaded();
    }
  };

  open(content) {
    if (!this.user?._id) {
      this.toastService.showError("Session Expired, Please Login");
      return;
    }

    Object.keys(this.articleForm.controls).forEach((field) => {
      const control = this.articleForm.get(field);
      control.markAsTouched({ onlySelf: true });
    });

    if (this.articleForm.invalid) {
      this.toastService.showError(
        this.translateService.instant("ARTICLE_POST.BOTH")
      );
      return;
    }
    if (!this.s3RefKey) {
      this.toastService.showError(
        this.translateService.instant("ARTICLE_POST.BANNER")
      );
      return;
    }
    if (!this.articleForm.valid) {
      this.toastService.showError(
        this.translateService.instant("ARTICLE_POST.INVALID")
      );
      return;
    }

    this.modalService.open(content, {
      size: "lg",
      centered: true,
      scrollable: true,
      windowClass: "custom-modal-content",
    });
  }

  createForm() {
    this.articleForm = this.fb.group({
      game: ["", Validators.compose([Validators.required])],
      genre: [[], Validators.compose([])],
      tags: [[], Validators.compose([])],
      highlight: [0, Validators.compose([])],
      minRead: [2, Validators.compose([])],
      location: ["", Validators.compose([])],
      isSponsor: [false, Validators.compose([])],
      isInfluencer: [false, Validators.compose([])],
      isInfluencerHighlight: [false, Validators.compose([])],
      title: this.fb.group(this.constantsService.languageFC(), {
        validator: this.formService.groupFieldValidator(),
      }),
      slug: ["", Validators.compose([])],
      content: this.fb.group(this.constantsService.languageFC(), {
        validator: this.formService.groupFieldValidator(),
      }),
      category: ["", Validators.compose([Validators.required])],
      platform: [[], Validators.compose([Validators.required])],
      image: ["", Validators.compose([Validators.required])],
      shortDescription: this.fb.group(this.constantsService.languageFC(), {
        validator: this.formService.groupFieldValidator(),
      }),
      gameDetails: ["", Validators.compose([])],
    });
  }

  setSlug() {
    this.articleForm.value.slug = this.articleForm.value?.title?.english
      .replace(/ /g, "_")
      .toLowerCase();
  }

  initializeEditor() {
    // ClassicEditor
    //   .create( document.querySelector( '#editor' ), {
    //       extraPlugins: [ this.customAdapterPlugin ],
    //   } )
    //   .catch( error => {
    //   } );
  }

  popUpTitleAndText = (data) => {
    if (data?._id) {
      return {
        title: this.translateService.instant(
          "ADMIN_MODAL.ARTICLE_CONTENT_EDIT.HEADER"
        ),
        text: this.translateService.instant(
          "ADMIN_MODAL.ARTICLE_CONTENT_EDIT.DESCRIPTION"
        ),
      };
    } else {
      return {
        title: this.translateService.instant(
          "ADMIN_MODAL.ARTICLE_CONTENT_EDIT.HEADER"
        ),
        text: this.translateService.instant(
          "ADMIN_MODAL.ARTICLE_CONTENT_EDIT.DESCRIPTION"
        ),
      };
    }
  };

  saveArticle(status, modal?) {
    this.modalService.dismissAll();
    if (this.articleForm.invalid) {
      this.articleForm.markAllAsTouched();
      return;
    }
    if (!this.user?._id) {
      this.toastService.showError(
        this.translateService.instant("ARTICLE_POST.SESSION_EXPIRED")
      );
      return;
    }

    if (status == "submitted_for_approval")
      if (this.articleForm.invalid) {
        this.toastService.showError(
          this.translateService.instant("ARTICLE_POST.BOTH")
        );
        return;
      }
    if (!this.s3RefKey) {
      this.toastService.showError(
        this.translateService.instant("ARTICLE_POST.BANNER")
      );
      return;
    }
    if (!this.articleForm.valid) {
      Object.keys(this.articleForm.controls).forEach((field) => {
        const control = this.articleForm.get(field);
        control.markAsTouched({ onlySelf: true });
      });
      this.toastService.showError(
        this.translateService.instant("ARTICLE_POST.INVALID")
      );
      return;
    }
    let formValues = this.articleForm.value;
    formValues["author"] = this.user?._id;
    formValues["articleStatus"] = status;
    formValues["s3RefKey"] = this.s3RefKey;
    // formValues["createdBy"] = this.user?._id;
    if (!this.articleId) {
      formValues["createdBy"] = this.user?._id;
    }
    this.showLoader = false;
    if (this.articleId) {
      const data: InfoPopupComponentData = {
        ...this.popUpTitleAndText("formValues"),
        type: InfoPopupComponentType.confirm,
        btnText: this.translateService.instant("TOURNAMENT.CONFIRM"),
      };

      const confirmed = this.matDialog.open(InfoPopupComponent, { data });
      confirmed.afterClosed().subscribe((result) => {
        if (result) {
          this.articleService
            .updateArticle(API, this.articleId, formValues)
            .subscribe(
              (res) => {
                this.showLoader = false;
                if (status == "draft") {
                  this.toastService.showSuccess(
                    this.translateService.instant("ARTICLE_POST.DRAFT")
                  );
                } else {
                  this.toastService.showSuccess(
                    this.translateService.instant("ARTICLE_POST.UPDATED")
                  );
                }
                if (modal) {
                  modal.close();
                }
                if (this.fallBack) {
                  this.router.navigate([this.fallBack], {
                    queryParams: { activeTab: this.fallBackIndex },
                  });
                } else {
                  this.router.navigate(["/profile/my-content"]);
                }
              },
              (err) => {
                this.showLoader = false;
              }
            );
        } else {
          this.IsmodelShow = false;
        }
      });
    } else {
      this.articleService.saveArticle(API, formValues).subscribe(
        (res: any) => {
          this.showLoader = false;
          if (status == "draft") {
            this.toastService.showSuccess(
              this.translateService.instant("ARTICLE_POST.DRAFT")
            );
          } else {
            this.toastService.showSuccess(
              this.translateService.instant("ARTICLE_POST.SAVE")
            );
          }
          this.bannerSRC = "";
          this.s3RefKey = null;
          if (modal) {
            modal.close();
          }
          this.createForm();
          this.router.navigate(["/profile/my-content"]);
        },
        (err) => {
          this.showLoader = false;
          this.toastService.showError(
            `${this.translateService.instant("ARTICLE_POST.ERROR")},
              ${err["error"]["error"]}`
          );
        }
      );
    }
  }

  removeTag(tag) {
    let index = this.articleForm.value.tags.indexOf(tag);
    this.articleForm.value.tags.splice(index, 1);
  }

  isFormComplete(language) {
    let isComplete = true;
    for (let key in this.articleForm.value) {
      if (key == "title" || key == "content" || key == "shortDescription") {
        if (
          this.articleForm.value[key][language] == "" ||
          this.articleForm.value[key][language] == null ||
          this.articleForm.value[key][language] == undefined
        ) {
          isComplete = false;
          break;
        }
      }
    }
    return isComplete;
  }

  getGameName() {
    if (!this.articleForm.value?.game) {
      return "Select";
    } else {
      for (let obj of this.gameList) {
        if (obj?._id == this.articleForm.value?.gameDetails) {
          return obj?.name;
        }
      }
    }
  }

  removePlatform(platform) {
    let index = this.articleForm.value?.platform.indexOf(platform);
    this.articleForm.value?.platform.splice(index, 1);
    if (this.articleForm.value.platform.length < 1) {
      let control = this.articleForm.get("platform");
      control.setErrors({ required: true });
    }
  }

  getDate() {
    return Date();
  }

  getCurrentUserDetails() {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.user = data;
        this.userImageSrc = data?.profilePicture;
      }
    });
  }

  removeGenre(genre) {
    let index = this.articleForm.value?.genre.indexOf(genre);
    this.articleForm.value?.genre.splice(index, 1);
  }

  setDropdownValue(formControl, value, type?) {
    if (type == "array") {
      let arrayField = this.articleForm.get(formControl).value;
      arrayField.push(value);
      this.articleForm.controls[formControl].setValue(arrayField);
    } else {
      this.articleForm.controls[formControl].setValue(value);
    }
  }

  async upload(event) {
    // const bannerFormData = new FormData();
    // for (let i = 0; i < this.selectedFiles.length; i++) {
    //   bannerFormData.set(
    //     `file${i}`,
    //     this.selectedFiles[i],
    //     this.selectedFiles[i]['name']
    //   );
    // }
    this.showLoader = true;
    const files = event.target.files;
    let promises = [];
    for (let obj of files) {
      promises.push(this.toBase64(obj));
    }
    let Base64String = await Promise.all(promises);
    let imageData = {
      path: environment.articleS3BucketName,
      files: Base64String,
    };

    this.s3Service.fileUpload(imageData).subscribe(
      (res) => {
        this.showLoader = false;
        if (
          res &&
          res["data"] &&
          res["data"][0] &&
          res["data"][0]["Location"]
        ) {
          this.articleForm.controls.image.setValue(res["data"][0]["Location"]);
          let filePath = res["data"][0]["Key"].split(".")[0];
          let lastIndexSlash = filePath.lastIndexOf("/");
          this.s3RefKey = filePath.substring(lastIndexSlash + 1);
          this.toastService.showSuccess(
            this.translateService.instant("ARTICLE_POST.BANNER_UPLOAD")
          );
        }
      },
      (err) => {
        this.showLoader = false;
        if (
          err &&
          err.error &&
          err.error.statusCode &&
          err.error.statusCode == 401
        ) {
          this.toastService.showError(
            this.translateService.instant("ARTICLE_POST.SESSION_EXPIRED")
          );
        } else {
          this.toastService.showError(
            this.translateService.instant("ARTICLE_POST.ERROR_BANNER")
          );
        }
      }
    );
  }
  async selectFile(event) {
    this.isValidImage(event, { height: 560, width: 1088 });

    // let FILE = (event.target as HTMLInputElement).files;
    // this.selectedFiles = FILE;
    // this.readURL(event);
    // this.upload();
  }

  isValidImage(event: any, values): Boolean {
    if (this.isBrowser && event.target.files && event.target.files[0]) {
      if (event.target.files[0]["size"] / (1000 * 1000) > 2) {
        this.toastService.showError(
          this.translateService.instant("ARTICLE_POST.IMG_LESS")
        );
        return;
      }

      if (
        event.target.files[0].type != "image/jpeg" &&
        event.target.files[0].type != "image/jpg" &&
        event.target.files[0].type != "image/png"
      ) {
        this.toastService.showError(
          this.translateService.instant("ARTICLE_POST.UPLOAD_CORRECT")
        );
        return;
      }

      try {
        const file = event.target.files[0];

        const reader = new FileReader();
        reader.readAsDataURL(file);
        let toastService = this.toastService;
        let th = this;
        reader.onload = (e) => {
          let image: any = new Image();
          image.src = e.target.result;
          image.onload = function () {
            let height = this.height;
            let width = this.width;
            if (height == values["height"] && width == values["width"]) {
              th.toastService.showInfo(
                this.translateService.instant("ARTICLE_POST.PROPER")
              );
            }
            th.upload(event);
            th.readURL(event);
          };
        };
      } catch (err) {
        return false;
      }
    } else {
      return false;
    }
  }

  toBase64(file) {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = (error) => reject(error);
    });
  }

  readURL(event: any): void {
    if (event.target.files && event.target.files[0]) {
      const file = event.target.files[0];

      const reader = new FileReader();
      reader.onload = (e) => (this.bannerSRC = reader.result);

      reader.readAsDataURL(file);
    }
  }

  // onReady(eventData) {
  //   let s3Service = this.s3Service;
  //   let toastService = this.toastService;
  //   let translateService = this.translateService;
  //   eventData.plugins.get("FileRepository").createUploadAdapter = function (
  //     loader
  //   ) {
  //     return new UploadAdapter(
  //       loader,
  //       s3Service,
  //       toastService,
  //       translateService
  //     );
  //   };
  // }

  setMinRead(val) {
    this.articleForm.controls.minRead.setValue(val);
  }

  removeBanner() {
    this.bannerSRC = "";
    this.articleForm.controls.image.setValue("");
    this.s3RefKey = "";
  }

  getGames() {
    this.gameService.getGames(API).subscribe(
      (res) => {
        if(res){
          this.gameList = res["data"];
        }
        
      },
      (err) => { }
    );
  }

  getArticle(id) {
    this.articleService.getArticleByID(API, id).subscribe(
      (res) => {
      if(res){ 
        let data = res["data"][0];
        this.articleForm.addControl("id", new FormControl(id));
        const category = data?.category;
        const tag = data.tags.map((item) => item?._id);
        const platforms = data?.platform.map((item) => item?._id);
        const genres = data?.genre.map((item) => item?._id);
        const gameDetails = data?.gameDetails?._id;
        this.setPlatformList(gameDetails, this.gameList);
        this.articleForm.patchValue({
          ...data,
          category,
          tag,
          platforms,
          genres,
          gameDetails,
        });
        this.bannerSRC = data["image"];
        this.s3RefKey = data["s3RefKey"];
        if (data["createdBy"] != this.user?._id) {
          if (this.user?.accountType != "admin") {
            this.router.navigate(["/article"]);
          }
          if (this.user?.accountType == "admin") {
            this.display_user = data?.authorDetails?.fullName;
            this.userImageSrc = data?.authorDetails?.profilePicture;
          }
        } else {
          this.display_user = data?.authorDetails?.fullName;
          this.userImageSrc = data?.authorDetails?.profilePicture;
        }
      }
      },
      (err) => { }
    );
  }

  /**
   * Check All API's Are Finished.
   * If All APi's Finished Then Remove Full Page Loader
   */
  allApiDataLoaded() {
    const startedApiCount = this.apiLoaded.filter((el) => !el).length;
    const finishedApiCount = this.apiLoaded.filter((el) => el).length;
    this.isLoaded = startedApiCount === finishedApiCount;
  }

  /**
   * Get Platform List For Selected Game
   * @param gameList gameList
   * @param game game
   */
  setPlatformList(game, gameList) {
    if (gameList.length && game) {
      for (const iterator of gameList) {
        if (iterator?._id == game) {
          this.platformList = iterator?.platform;
        }
      }
    }
  }

  onChangePlatform(game, gameList) {
    this.articleForm.get("platform").setValue([]);
    this.setPlatformList(game, gameList);
  }

  /**
   * Array validation
   * @param control ArrayFormControl
   */
  arrayValidation(control: FormControl) {
    if (!control.value?.length) {
      return { required: true };
    }
    return null;
  }

  /**
   * Set Values In Form
   * @param formControl formControl
   * @param value value
   * @param isAllowMultiple isAllowMultiple
   * @param platform platform
   */
  addItem(formControl, value, isAllowMultiple = true, platform = null) {
    if (isAllowMultiple) {
      const array = this.articleForm.get(formControl).value;
      const index = array.indexOf(value);
      if (index < 0) {
        array.push(value);
        this.articleForm.controls[formControl].setValue(array);
      }
    } else {
      this.articleForm.get(formControl).setValue(value);
      if (platform) {
        for (const iterator of this.gameList) {
          if (iterator._id == value) {
            this.platformList = iterator.platform;
          }
        }
        this.articleForm.controls.platform.setValue([]);
        this.articleForm.get("platform").updateValueAndValidity();
      }
    }
    this.articleForm.get(formControl).updateValueAndValidity();
  }

  /**
   * Remove Value from the Form
   * @param formControl formControl
   * @param value value
   */
  removeItem(formControl, value) {
    const index = this.articleForm.value[formControl].indexOf(value);
    this.articleForm.value[formControl].splice(index, 1);
    this.articleForm.get(formControl).updateValueAndValidity();
  }

  /**
   * Set Marked As touch For Form Fields
   * @param formControl formControl
   */
  setFomControlTouched(formControl) {
    this.articleForm.controls[formControl].markAsTouched({
      onlySelf: true,
    });
  }

  changeLocalLanguage(lang) {
    this.currLanguage = lang?.key;
    this.config.lang = lang?.code;
    this.currLanguageDisplay = lang?.value;
    this.direction = this.constantsService?.rtl.includes(lang?.code)
      ? "direction2"
      : "direction1";
    this.isEditorLoaded = false;
    setTimeout(() => (this.isEditorLoaded = true));
  }
}
