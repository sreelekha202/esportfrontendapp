import { ProfileService } from './../../../core/service/profile.service';
import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import {
  AppHtmlRoutes,
  AppHtmlRoutesArticleMoreType,
} from '../../../app-routing.model';
import { TranslateService } from '@ngx-translate/core';
import {
  EsportsUserService,
  EsportsArticleService,
  EsportsLanguageService,
  IUser,
  EsportsUtilsService,
  EsportsGtmService,
  EventProperties
} from 'esports';
import { environment } from '../../../../environments/environment';
const API = environment.apiEndPoint;

@Component({
  selector: 'app-article-main',
  templateUrl: './article-main.component.html',
  styleUrls: ['./article-main.component.scss'],
})
export class ArticleMainComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;
  AppHtmlRoutesArticleMoreType = AppHtmlRoutesArticleMoreType;
  showLoader: boolean = true;
  user: IUser;
  hottestArticle = null;
  latestArticle = [];

  searchFilterForm = [
    {
      title: 'Select game',
      fields: [{ label: 'Select games', value: 'all' }],
    },
    {
      title: 'Article type',
      fields: [{ label: 'All', value: 'all' }],
    },
  ];
  categoryList: any = [];

  constructor(
    private articleService: EsportsArticleService,
    private userService: EsportsUserService,
    public language: EsportsLanguageService,
    public translateService: TranslateService,
    private profileService: ProfileService,
    private gtmService: EsportsGtmService,
    private router: Router,
    public utilsService: EsportsUtilsService
  ) {}

  ngOnInit(): void {
    this.getHottestArticle();
    this.getCurrentUserDetails();

    this.getAllCategory();
  }
  getAllCategory() {
    this.profileService.getAllCategories().subscribe((res) => {
      if (res?.data && res?.data) {
        let categoryList = res?.data;
        categoryList.map((cat: any) => {
          this.categoryList.push({ [cat?._id]: cat });
        });
        this.getRecentPost();
      }
    });
  }

  getRecentPost() {
    const pagination = JSON.stringify({ limit: 4, sort: { createdDate: -1 } });
    const query = JSON.stringify({
      articleStatus: 'publish',
    });

    const perfernce = JSON.stringify({
      prefernce: this.user?.preference?.game
        ? this.user?.preference?.game.map((item) => {
            return { gameDetails: item };
          })
        : '',
    });

    this.articleService
      .getLatestArticle(API, {
        pagination: pagination,
        query: query,
        preference: perfernce,
      })
      .subscribe(
        (res: any) => {
          if (res && res?.data && res?.data?.docs) {
            let latestArticle = res.data.docs;
            // MOCK ARTICLE TYPE
            // this.latestArticle = res['data']['docs'].map((article, i) => {
            //   if (i === 0) {
            //     return { ...article, type: 'opinion' };
            //   } else if (i === 1) {
            //     return { ...article, type: 'guide' };
            //   } else if (i === 2) {
            //     return { ...article, type: 'listicle' };
            //   }
            //   return { ...article, type: 'feature' };
            // });
            latestArticle.map((art: any) => {
              // if (art.isFeature) {
              this.latestArticle.push({
                ...art,
                category_name: this.categoryList[art.category],
              });
              // }
            });
          }
          this.showLoader = false;
        },
        (err) => {
          this.showLoader = false;
        }
      );
  }

  getHottestArticle() {
    this.articleService.getArticleList(API).subscribe(
      (res) => {
        // MOCK HOTTEST ARTICLE TYPE
        this.hottestArticle = { ...res['data'][0], type: 'guide' };
        this.showLoader = false;
      },
      (err) => {
        this.showLoader = false;
      }
    );
  }

  getCurrentUserDetails() {
    this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.user = data;
      }
    });
  }

  getArticleIconByType(type: string): string {
    const path = 'assets/icons/news';
    switch (type) {
      case 'feature':
        return `${path}/feature.svg`;
      case 'opinion':
        return `${path}/opinion.svg`;
      case 'guide':
        return `${path}/guide.svg`;
      case 'listicle':
        return `${path}/listicle.svg`;
      case 'hot':
        return `${path}/hot.svg`;
      case 'trending':
        return `${path}/trending.svg`;
    }
  }

  viewFeaturedPost(){
    this.pushGTMTags('View_Editor_Pick_Post', { fromPage: this.router.url })
  }

  pushGTMTags(eventName: string, eventData: EventProperties = null) {
    this.gtmService.pushGTMTags(eventName, eventData);
  }

}
