import { NgModule } from '@angular/core';

import { ArticleRoutingModule } from './article-routing.module';
import { CoreModule } from '../../core/core.module';
import { PipeModule } from 'esports';
import { ArticleAuthorComponent } from './article-author/article-author.component';
import { ArticleGameComponent } from './article-game/article-game.component';
import { ArticleMainComponent } from './article-main/article-main.component';
import { ArticleMoreComponent } from './article-more/article-more.component';
import { ArticleNewsComponent } from './article-news/article-news.component';
import { ArticlePostComponent } from './article-post/article-post.component';
import { ViewArticleComponent } from './view-article/view-article.component';
import { PaginationModule, PaginationConfig } from 'ngx-bootstrap/pagination';
import { ShareModule } from 'ngx-sharebuttons';
import { SharedModule } from '../../shared/modules/shared.module';

@NgModule({
  declarations: [
    ArticleAuthorComponent,
    ArticleGameComponent,
    ArticleMainComponent,
    ArticleMoreComponent,
    ArticleNewsComponent,
    ArticlePostComponent,
    ViewArticleComponent,
  ],
  imports: [
    ArticleRoutingModule,
    CoreModule,
    PaginationModule.forRoot(),
    PipeModule,
    SharedModule,
    ShareModule,
  ],
  providers: [{ provide: PaginationConfig }],
})
export class ArticleModule {}
