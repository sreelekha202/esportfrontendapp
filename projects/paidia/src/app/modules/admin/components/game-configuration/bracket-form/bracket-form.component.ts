import {
  AfterViewChecked,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnInit,
} from '@angular/core';
import {
  AbstractControl,
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  ValidatorFn,
  Validators,
} from '@angular/forms';
import { environment } from '../../../../../../environments/environment';
import { Subscription } from 'rxjs';

@Component({
  selector: 'bracket-form',
  templateUrl: './bracket-form.component.html',
  styleUrls: [
    './bracket-form.component.scss',
    '../../site-configuration/site-configuration.component.scss',
    '../upsert-game/upsert-game.component.scss',
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BracketFormComponent implements OnInit, AfterViewChecked {
  @Input() subForm: FormGroup;
  @Input() bracketDetails: any;
  @Input() bracketList: Array<any>;

  rrFormat = {
    1: 'OPTIONS.RR_1',
    2: 'OPTIONS.RR_2',
  };

  constructor(private fb: FormBuilder, private cdr: ChangeDetectorRef) {}

  bracketSubscriber: Subscription;

  ngOnInit(): void {
    if (this.bracketDetails?.bracketType) {
      this.bracketChangeHandler(
        this.bracketDetails?.bracketType,
        this.bracketDetails?.noOfPlacement,
        this.bracketDetails?.noOfTeamInGroup,
        this.bracketDetails?.maxParticipants,
        this.bracketDetails?.noOfRound,
        this.bracketDetails?.noOfLoss
      );
      this.maxParticipantHandler(
        this.bracketDetails?.maxParticipants,
        this.bracketDetails?.bracketType
      );
      this.noOfTeamsPerGroupHandler(
        this.bracketDetails?.maxParticipants,
        this.bracketDetails?.bracketType
      );
      this.killingPointHandler(this.bracketDetails?.isKillPointRequired);

      this.subForm.patchValue({
        ...this.bracketDetails,
      });
    }
    this.bracketSubscriber = this.subForm
      ?.get('bracketType')
      ?.valueChanges?.subscribe((item) => {
        this.bracketChangeHandler(item);
      });
  }

  ngAfterViewChecked() {
    this.cdr.detectChanges();
  }

  ngOnDestroy() {
    this.bracketSubscriber?.unsubscribe();
  }

  /**
   *
   * @param field
   * @param fieldType 'STRING', 'BOOLEAN', 'NUMBER'
   * @param defaultValue
   * @param min
   * @param max
   */
  addNewControlAndUpdateValidity = (
    field,
    fieldType,
    defaultValue,
    min?,
    max?,
    pattern?,
    customValidation?
  ) => {
    const formControl = this.subForm.get(field);
    const validation = [];

    switch (fieldType) {
      case 'NUMBER':
        validation.push(Validators.required);
        if (typeof min == 'number') validation.push(Validators.min(min));
        if (typeof max == 'number') validation.push(Validators.max(max));
        if (pattern) validation.push(pattern);
        if (customValidation) validation.push(customValidation);
        break;
      case 'BOOLEAN':
        validation.push(Validators.required);
        break;
      case 'STRING':
        validation.push(Validators.required);
        break;
    }

    if (formControl) {
      formControl.setValue(defaultValue);
      formControl.setValidators(validation);
      formControl.updateValueAndValidity();
    } else {
      this.subForm.addControl(
        field,
        new FormControl(defaultValue, Validators.compose(validation))
      );
    }
  };

  removeControlOrValidity = (field) => {
    const formControl = this.subForm.get(field);
    if (formControl) {
      this.subForm.removeControl(field);
    } else {
    }
  };

  // Validators
  teamInGroupValidation = (field?: string): ValidatorFn => {
    return (control: AbstractControl): { [key: string]: any } | null => {
      if (control['_parent']) {
        const {
          noOfTeamInGroup,
          noOfWinningTeamInGroup,
          maxParticipants,
          noOfStage,
        } = control?.['_parent']?.controls;

        if (
          maxParticipants?.valid &&
          noOfTeamInGroup?.value &&
          !noOfTeamInGroup?.errors?.max &&
          noOfWinningTeamInGroup?.value &&
          maxParticipants?.value &&
          noOfStage
        ) {
          const fieldValidation = (
            maxParticipants,
            noOfTeamInGroup,
            noOfWinningTeam
          ) => {
            if (maxParticipants < noOfTeamInGroup) {
              return { max: { max: maxParticipants } };
            } else if (noOfTeamInGroup > noOfWinningTeam) {
              const check = noOfTeamInGroup % noOfWinningTeam != 0;
              return check ? { multiStageConfigError: true } : null;
            } else {
              return { multiStageConfigError: true };
            }
          };
          const errorConfig = fieldValidation(
            maxParticipants?.value,
            noOfTeamInGroup?.value,
            noOfWinningTeamInGroup?.value
          );

          if (!errorConfig) {
            const totalStage = (
              totalPlayer,
              noOfPlayerPerGroup,
              noOfWinning,
              noOfStage,
              previousStagePlayer
            ) => {
              let nextStagePlayer;
              const createStage = () => {
                nextStagePlayer =
                  Math.ceil(totalPlayer / noOfPlayerPerGroup) * noOfWinning;
                if (nextStagePlayer < previousStagePlayer) {
                  // noOfPlayerPerGroup = noOfPlayerPerGroup + 1;
                  nextStagePlayer =
                    Math.ceil(totalPlayer / noOfPlayerPerGroup) * noOfWinning;
                } else {
                  createStage();
                }
              };

              if (totalPlayer !== noOfPlayerPerGroup) {
                createStage();
              }
              return totalPlayer == noOfPlayerPerGroup
                ? noOfStage
                : !(Math.ceil(nextStagePlayer / noOfPlayerPerGroup) > 1)
                ? noOfStage + 1
                : Math.ceil(nextStagePlayer / noOfPlayerPerGroup) == 2
                ? noOfStage + 2
                : totalStage(
                    nextStagePlayer,
                    noOfPlayerPerGroup,
                    noOfWinning,
                    noOfStage + 1,
                    previousStagePlayer
                  );
            };

            const noOfStage = totalStage(
              maxParticipants?.value,
              noOfTeamInGroup?.value,
              noOfWinningTeamInGroup?.value,
              1,
              maxParticipants?.value
            );
            this.subForm.get('noOfStage').setValue(noOfStage);
          }

          if (field == 'noOfTeamInGroup') {
            return errorConfig;
          } else {
            this.subForm.get('noOfTeamInGroup').setErrors(errorConfig);
            return null;
          }
        }
      } else {
        return null;
      }
    };
  };

  bracketChangeHandler(
    type: string,
    noOfPlacement?: any,
    maxPlacement = 2,
    maxParticipants?: any,
    noOfRound?: any,
    noOfLoss?: any
  ) {
    let maximumParticipants = 16384;

    if (['round_robin', 'battle_royale'].includes(type)) {
      this.updateRoundRobinAndBattleRoyaleFormFields(
        type,
        noOfPlacement,
        maxPlacement
      );
      this.multiStageTournamentHandler(true, type, maxParticipants);
    } else {
      this.multiStageTournamentHandler(false);
      this.removeControlOrValidity('noOfPlacement');
      this.removeControlOrValidity('placementPoints');
      this.removeControlOrValidity('isKillPointRequired');
      this.removeControlOrValidity('pointsKills');
      this.removeControlOrValidity('stageBracketType');
      this.addNewControlAndUpdateValidity(
        'allowAdvanceStage',
        'BOOLEAN',
        false
      );
    }

    switch (type) {
      case 'single':
        maximumParticipants = environment.maxparticipant.single;
        break;
      case 'double':
        maximumParticipants = environment.maxparticipant.double;
        break;
      case 'round_robin':
        maximumParticipants = environment.maxparticipant.roundRobin;
        break;
      case 'battle_royale':
        maximumParticipants = environment.maxparticipant.battleRoyale;
        break;
    }

    if (['swiss_safeis', 'ladder'].includes(type)) {
      if (type == 'swiss_safeis') {
        this.addNewControlAndUpdateValidity(
          'noOfRound',
          'NUMBER',
          noOfRound || 1,
          1
        );
        this.addNewControlAndUpdateValidity(
          'noOfLoss',
          'NUMBER',
          noOfLoss || 0,
          0,
          (noOfRound || 1) - 1
        );
        this.subForm.addControl(
          'setDuration',
          this.fb.group({
            days: [1, Validators.required],
            hours: [0, Validators.required],
            mins: [0, Validators.required],
          })
        );
      }
      this.addNewControlAndUpdateValidity('enableTiebreaker', 'BOOLEAN', false);
      this.subForm.get('isParticipantsLimit').setValue(false);
      this.removeControlOrValidity('maxParticipants');
    } else {
      this.removeControlOrValidity('noOfRound');
      this.removeControlOrValidity('noOfLoss');
      this.removeControlOrValidity('setDuration');
      this.removeControlOrValidity('enableTiebreaker');

      this.subForm.get('isParticipantsLimit').setValue(true);
      this.addNewControlAndUpdateValidity(
        'maxParticipants',
        'NUMBER',
        2,
        2,
        maximumParticipants,
        false,
        this.teamInGroupValidation('maxParticipants').bind(this)
      );
    }

    if (type == 'single') {
      this.addNewControlAndUpdateValidity(
        'isAllowThirdPlaceMatch',
        'BOOLEAN',
        false
      );
    } else {
      this.removeControlOrValidity('isAllowThirdPlaceMatch');
    }
  }

  updateRoundRobinAndBattleRoyaleFormFields = (
    type: string,
    noOfPlacement: number,
    maxPlacement: number
  ) => {
    if (type == 'battle_royale') {
      this.addNewControlAndUpdateValidity(
        'noOfPlacement',
        'NUMBER',
        2,
        2,
        this.subForm?.value?.noOfTeamInGroup || maxPlacement || 2
      );
      this.placementCounter(noOfPlacement || 2);
      this.addNewControlAndUpdateValidity(
        'isKillPointRequired',
        'BOOLEAN',
        false
      );
      this.removeControlOrValidity('stageBracketType');
      this.removeControlOrValidity('allowAdvanceStage');
    } else {
      this.removeControlOrValidity('noOfPlacement');
      this.removeControlOrValidity('placementPoints');
      this.removeControlOrValidity('isKillPointRequired');
      this.addNewControlAndUpdateValidity('stageBracketType', 'STRING', '');
      this.addNewControlAndUpdateValidity(
        'allowAdvanceStage',
        'BOOLEAN',
        false
      );
    }
  };

  placementCounter = async (value) => {
    this.subForm.addControl('placementPoints', this.fb.array([]));
    const placementPointsList = this.subForm.get(
      'placementPoints'
    ) as FormArray;

    const createPlacementForm = (position): FormGroup => {
      return this.fb.group({
        position: [position],
        value: [
          '',
          Validators.compose([
            Validators.required,
            Validators.pattern('^[0-9]*$'),
          ]),
        ],
      });
    };

    // remove placement node
    const deletePlacementForm = async (count, value) => {
      placementPointsList.removeAt(count);
      return count <= value ? true : await deletePlacementForm(--count, value);
    };

    // add placement node
    const addPlacementForm = async (count, value) => {
      placementPointsList.push(createPlacementForm(count));
      return count >= value ? true : await addPlacementForm(++count, value);
    };

    let count = placementPointsList.length;

    if (count > value) {
      await deletePlacementForm(count - 1, value);
    }

    if (count < value) {
      await addPlacementForm(count + 1, value);
    }
  };

  multiStageTournamentHandler = (
    isEnableFields: boolean,
    bracketType?,
    maxParticipants?
  ): void => {
    if (isEnableFields) {
      const noOfRound = bracketType == 'round_robin' ? 2 : 10;
      this.addNewControlAndUpdateValidity(
        'noOfTeamInGroup',
        'NUMBER',
        2,
        2,
        maxParticipants || this.subForm.value?.maxParticipants || 2,
        false,
        this.teamInGroupValidation('noOfTeamInGroup').bind(this)
      );
      this.addNewControlAndUpdateValidity(
        'noOfWinningTeamInGroup',
        'NUMBER',
        1,
        1,
        false,
        false,
        this.teamInGroupValidation('noOfWinningTeamInGroup').bind(this)
      );
      this.addNewControlAndUpdateValidity(
        'noOfRoundPerGroup',
        'NUMBER',
        1,
        1,
        noOfRound
      );
      this.addNewControlAndUpdateValidity('noOfStage', 'NUMBER', 1);
    } else {
      this.removeControlOrValidity('noOfTeamInGroup');
      this.removeControlOrValidity('noOfWinningTeamInGroup');
      this.removeControlOrValidity('noOfRoundPerGroup');
      this.removeControlOrValidity('noOfStage');
    }
  };

  maxParticipantHandler(value, bracketType) {
    if (['battle_royale', 'round_robin'].includes(bracketType)) {
      const maxValidator =
        bracketType == 'round_robin'
          ? [Validators.max(value >= 24 ? 24 : value)]
          : [Validators.max(value)];
      this.subForm
        .get('noOfTeamInGroup')
        .setValidators([
          Validators.required,
          Validators.min(2),
          ...maxValidator,
          this.teamInGroupValidation('noOfTeamInGroup').bind(this),
        ]);
      this.subForm
        .get('noOfWinningTeamInGroup')
        .setValidators([
          Validators.required,
          Validators.min(1),
          this.teamInGroupValidation('noOfWinningTeamInGroup').bind(this),
        ]);
      this.subForm.get('noOfTeamInGroup').updateValueAndValidity();
      this.subForm.get('noOfWinningTeamInGroup').updateValueAndValidity();
    }
  }

  noOfTeamsPerGroupHandler(value, bracketType) {
    if (bracketType === 'battle_royale') {
      this.subForm
        .get('noOfPlacement')
        .setValidators([
          Validators.required,
          Validators.min(2),
          Validators.max(value || 2),
        ]);
      this.subForm.get('noOfPlacement').updateValueAndValidity();
    }
  }

  killingPointHandler = (isAllowKillingPoint) => {
    isAllowKillingPoint
      ? this.subForm.addControl(
          'pointsKills',
          new FormControl(1, [Validators.required, Validators.min(1)])
        )
      : this.subForm.removeControl('pointsKills');
    this.subForm.updateValueAndValidity();
  };

  roundHandler(noOfRound) {
    if (noOfRound) {
      this.addNewControlAndUpdateValidity(
        'noOfLoss',
        'NUMBER',
        0,
        0,
        noOfRound - 1
      );
    }
  }
}
