import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { DatatableComponent } from "@swimlane/ngx-datatable";
import { IPagination } from "../../../../shared/models";
import { AppHtmlAdminRoutes } from "../../../../app-routing.model";
import { MatTabChangeEvent } from "@angular/material/tabs";
import {  UserService } from "../../../../core/service";
import { EsportsUserService } from 'esports';
import { TranslateService } from "@ngx-translate/core";
import { MatDialog } from "@angular/material/dialog";
import {
  InfoPopupComponent,
  InfoPopupComponentData,
  InfoPopupComponentType,
} from "../../../../shared/popups/info-popup/info-popup.component";
import { environment } from '../../../../../environments/environment';
import { fromEvent, of } from "rxjs";
import { debounceTime, distinctUntilChanged, filter, map } from "rxjs/operators";
const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;
@Component({
  selector: "app-user-management",
  templateUrl: "./user-management.component.html",
  styleUrls: ["./user-management.component.scss"],
})
export class UserManagementComponent implements OnInit {
  @ViewChild(DatatableComponent) table: DatatableComponent;
  @ViewChild('searchInput', { static: true }) searchInput: ElementRef;

  page: IPagination;
  AppHtmlAdminRoutes = AppHtmlAdminRoutes;
  isLoading = false;
  tempFirstTab = [];
  rowsFirstTab = [];
  columnsFirstTab = [
    { name: "Id" },
    { name: "Name" },
    { name: "Email" },
    { name: "Phone" },
    { name: "Sign Up From" },
    { name: "Member Since" },
    { name: "" },
  ];
  paginationData = {
    page: 1,
    limit: 50,
    sort: "-createdOn",
  };

  tempSecondTab = [];
  rowsSecondTab = [];
  columnsSecondTab = [
    { name: "Id" },
    { name: "Name" },
    { name: "Email" },
    { name: "Phone" },
    { name: "Sign Up From" },
    { name: "Member Since" },
    { name: "Amount Spend" },
    { name: "" },
  ];

  constructor(
    private userService: EsportsUserService,
    private userService2: UserService,
    public dialog: MatDialog,
    private translateService: TranslateService
  ) {}

  ngOnInit(): void {
    // By default - To get the list of user for recently Updated
    this.getUserListRecenltyUpdated();
    this.initializeSearchtracker();
    this.checkSearchStatus();
  }
  pageChanged(page): void {
    this.paginationData.page = page;
    this.getUserListRecenltyUpdated();
  }
  // Call an API to get the user details for Recently Updated Tab
  getUserListRecenltyUpdated() {
    this.isLoading = true;
    const query = JSON.stringify({
      Status: 1,
    });
    const pagination = JSON.stringify(this.paginationData);
    this.userService2
      .getRecentlyUpdatedUsers({ pagination: pagination, query: query })
      .subscribe(
        (res) => {
          this.isLoading = false;
          this.page = {
            totalItems: res?.data?.totalDocs,
            itemsPerPage: res?.data?.limit,
            maxSize: 5,
          };
          const response = res.data.docs.map((element) => {
            return {
              id: element._id,
              name: element.fullName,
              email: element.email,
              phoneNumber: element.phoneNumber,
              // signUpFrom: 'Mobile',
              createdOn: element.createdOn,
            };
          });
          // cache our list
          this.tempFirstTab = [...response];
          // push our inital complete list
          this.rowsFirstTab = response;
          this.isLoading = false;
        },
        (err) => {
          this.isLoading = false;
        }
      );
  }

  // Call an API to get the user details for Recently Updated Tab
  getUserListTopSpending() {
    this.isLoading = true;
    this.userService.getTopSpendingUserList(API).subscribe(
      (res) => {
        // res.data.forEach((element, index) => {
        //   element.signUpFrom = 'Gmail';
        // });
        // cache our list
        this.tempSecondTab = [...res.data];
        // push our inital complete list
        this.rowsSecondTab = res.data;
        this.isLoading = false;
      },
      (err) => {
        this.isLoading = false;
      }
    );
  }

  /**
   * method to remove user infomation from the system
   */
  removeUser(id) {
    this.userService.removeUser(API,id).subscribe(
      (res) => {
        const afterBlockData: InfoPopupComponentData = {
          title: this.translateService.instant(
            "ADMIN.USER_NOTIFICATION.RESPONSE.SUCCESS"
          ),
          text: res.message,
          type: InfoPopupComponentType.info,
        };
        this.dialog.open(InfoPopupComponent, { data: afterBlockData });
        this.rowsFirstTab = [];
        this.getUserListRecenltyUpdated();
      },
      (err) => {
        const afterBlockData: InfoPopupComponentData = {
          title: this.translateService.instant(
            "ADMIN.USER_NOTIFICATION.RESPONSE.ERROR"
          ),
          text: err.error.message,
          type: InfoPopupComponentType.info,
        };
        this.dialog.open(InfoPopupComponent, { data: afterBlockData });
      }
    );
  }

  /**
   * Get the user list based on Tab selection
   * @param tabChangeEvent : Event on Change of Tab
   */
  onTabChanged = (tabChangeEvent: MatTabChangeEvent): void => {
    if (tabChangeEvent.index === 0) {
      // Call an API to get the user details for Recently Updated Tab
      this.getUserListRecenltyUpdated();
    } else if (tabChangeEvent.index === 1) {
      // Call an API to get the user details for Top Spending Tab
      this.getUserListTopSpending();
    }
  };

  updateSecondTabFilter(event) {
    const value = event.target.value.toLowerCase().trim();
    // get the amount of columns in the table
    const count = this.columnsSecondTab.length;
    // get the key names of each column in the dataset
    const keys = Object.keys(this.tempSecondTab[0]);
    // assign filtered matches to the active datatable
    this.rowsSecondTab = this.tempSecondTab.filter((item) => {
      // iterate through each row's column data
      for (let i = 0; i < count; i++) {
        // check for a match
        if (
          (item[keys[i]] &&
            item[keys[i]].toString().toLowerCase().indexOf(value) !== -1) ||
          !value
        ) {
          // found match, return true to add to result set
          return true;
        }
      }
    });
    // Whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }

  searchgetCall(term) {
    if (term === '') {
      return of([]);
    }
    return this.userService.searchUsersAdminPanel(term, {
      state: true,
      read: 'createdOn,email,_id,fullName,username,phoneNumber',
    });
  }

  initializeSearchtracker() {
    fromEvent(this.searchInput.nativeElement, 'keyup')
      .pipe(
        map((event: any) => {
          return event.target.value;
        }),
        filter((res) => res?.length > 1),
        debounceTime(1000),
        distinctUntilChanged()
      )
      .subscribe((text: string) => {
        this.searchgetCall(text).subscribe(
          (res) => {
            const response = res.map((element) => ({
              createdOn: element?.createdOn,
              email: element?.email,
              id: element?._id,
              name: element?.fullName,
              username: element?.username,
              phoneNumber: element?.phoneNumber,
            }));
            this.rowsFirstTab = response;
          },
          (err) => {}
        );
      });
  }

  checkSearchStatus() {
    fromEvent(this.searchInput.nativeElement, 'keyup').subscribe(
      (res: any) => {
        if (res.target.value == '') {
          this.rowsFirstTab = this.tempFirstTab;
        }
      },
      (err) => {}
    );
  }

  clearSearch() {
    if (this.searchInput.nativeElement.value) {
      this.searchInput.nativeElement.value = '';
      this.rowsFirstTab = this.tempFirstTab;
    }
  }

}
