import { Component, OnInit } from "@angular/core";
import { FormControl, Validators, FormGroup } from "@angular/forms";
import { PlatformFeeService } from "../../../../core/service";
import {
  InfoPopupComponent,
  InfoPopupComponentData,
  InfoPopupComponentType,
} from "../../../../shared/popups/info-popup/info-popup.component";

import { MatDialog } from "@angular/material/dialog";
import { TranslateService } from "@ngx-translate/core";
import { EsportsToastService } from "esports";

@Component({
  selector: "app-platform-fee-configuration",
  templateUrl: "./platform-fee-configuration.component.html",
  styleUrls: ["./platform-fee-configuration.component.scss"],
})
export class PlatformFeeConfigurationComponent implements OnInit {
  isLoading: boolean = true;
  platform = new FormGroup({
    platformFee: new FormControl("", [Validators.required]),
  });

  constructor(
    private platformFeeService: PlatformFeeService,
    private dialog: MatDialog,
    private translateService: TranslateService,
    private toastService: EsportsToastService
  ) {}

  ngOnInit(): void {
    this.fetchPlatformFee();
    this.isLoading = false;
  }

  async fetchPlatformFee() {
    const data = (await this.platformFeeService.getPlatformFee()).data;
    this.platform.patchValue({
      platformFee: data.fee,
    });
  }

  async updatePlatformFee() {
    const updatePlatformFeeData: InfoPopupComponentData = {
      title: this.translateService.instant(
        "API.PLATFORM_FEE.MODAL.STATUS_CONFIRMATION_MODAL.TITLE"
      ),
      text: this.translateService.instant(
        "API.PLATFORM_FEE.MODAL.STATUS_CONFIRMATION_MODAL.TEXT"
      ),
      type: InfoPopupComponentType.confirm,
      btnText: this.translateService.instant(
        "API.PLATFORM_FEE.MODAL.STATUS_CONFIRMATION_MODAL.BUTTONTEXT"
      ),
    };
    const dialogRef = this.dialog.open(InfoPopupComponent, {
      data: updatePlatformFeeData,
    });

    dialogRef.afterClosed().subscribe(async (confirmed) => {
      if (confirmed) {
        try {
          this.isLoading = true;
          const res = await this.platformFeeService.updatePlatformFee(
            this.platform.value
          );
          const afterBlockData: InfoPopupComponentData = {
            title: this.translateService.instant(
              "API.PLATFORM_FEE.PATCH.SUCCESS_HEADER"
            ),
            text: res.message,
            type: InfoPopupComponentType.info,
          };
          this.dialog.open(InfoPopupComponent, { data: afterBlockData });
          await this.fetchPlatformFee();
          this.isLoading = false;
        } catch (error) {
          this.isLoading = false;
          this.toastService.showError(error?.error?.message || error?.message);
        }
      }
    });
  }
}
