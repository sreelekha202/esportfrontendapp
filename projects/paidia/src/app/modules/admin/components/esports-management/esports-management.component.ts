import { Component, EventEmitter, OnInit } from '@angular/core';
import { MatTabChangeEvent } from '@angular/material/tabs';
import {
  EsportsConstantsService,
  EsportsTournamentService,
} from 'esports';
import { environment } from '../../../../../environments/environment';
const API = environment.apiEndPoint;
import {
  InfoPopupComponentData,
  InfoPopupComponentType,
  InfoPopupComponent,
} from '../../../../shared/popups/info-popup/info-popup.component';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { AppHtmlRoutes } from '../../../../app-routing.model';
import { TranslateService } from '@ngx-translate/core';
import { IPagination } from '../../../../shared/models';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-esports-management',
  templateUrl: './esports-management.component.html',
  styleUrls: ['./esports-management.component.scss'],
})
export class EsportsManagementComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;
  checkIconShow = false;
  viewIconShow = false;
  deleteIconShow = false;
  editIconShow = false;
  manageIconShow = false;
  userId;
  page: IPagination;
  finishedTournament: any = [];
  activeTabIndex;
  paginationData = {
    page: 1,
    limit: 50,
    sort: 'createdOn',
  };
  statusText: string = EsportsConstantsService.Status.Live;
  tournamentList = [];
  allTournamentList: any = [];
  isLoading = false;
  selectedTab = 'ongoing';
  customMatTabEvent = new EventEmitter<MatTabChangeEvent>();
  text: string;

  constructor(
    private tournamentService: EsportsTournamentService,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private dialog: MatDialog,
    private translateService: TranslateService,
  ) {}

  ngOnInit(): void {
    this.activeRoute.queryParams
      .subscribe((params) => {
        if (params.activeTab) {
          this.activeTabIndex = +params.activeTab;
          this.switchData(this.activeTabIndex);
        } else {
          this.activeTabIndex = 0;
          this.getData('1', 'Latest');
          this.setConfiguration(true, true, false, false, true);
        }
      })
      .unsubscribe();

    this.customMatTabEvent
      .pipe(debounceTime(30))
      .subscribe((tabChangeEvent: MatTabChangeEvent) => {
        this.activeTabIndex = tabChangeEvent.index;
        this.router.navigate(['.'], {
          relativeTo: this.activeRoute,
          queryParams: { activeTab: tabChangeEvent.index },
        });
        this.switchData(tabChangeEvent.index);
      });
  }

  pageChanged(page): void {
    this.paginationData.page = page;
    switch (this.activeTabIndex) {
      case 0:
        this.getData('1', 'Latest'); //ongoing
        break;
      case 1:
        this.getData('0', 'Latest'); // upcoming
        break;
      case 2:
        this.getData('2', 'Latest'); //past
        break;
      case 3:
        this.getData('5', 'Latest'); // Approval pending
        break;
      case 4:
        this.getData('10', 'Oldest'); // payament  pending
        // this.getTournamentForPendingPayments();
        break;
      default:
        break;
    }
  }


  /**
   * method to update the modifications on tournaments
   * @param data
   */
  modifyTournamentHandler = async (data: any) => {
    try {
      this.text = data.text;
      if (data?.type === 'abort') {
        this.switchData(this.activeTabIndex);
      } else if (data.type === 'feature') {
        const response = await this.tournamentService.updateTournament(
          { isFeature: data?.value },
          data?.id
        );
        const afterBlockData: InfoPopupComponentData = {
          title: this.translateService.instant(
            'API.TOURNAMENT.PUT.SUCCESS_HEADER'
          ),
          text: response?.message,
          type: InfoPopupComponentType.info,
        };
        this.dialog.open(InfoPopupComponent, { data: afterBlockData });
        this.getData('0', 'Latest');
      } else if (data?.type === 'update') {
        this.switchData(data?.value);
        if (data.value == 3) {
          this.getData('5', 'Latest');
        } else {
          this.switchData(data?.value);
        }
      } else if (data?.type === 'updatePaymentTable') {
        this.getData('10', 'Oldest');
      } else if (data?.text != null && data?.text != undefined) {
        this.text = data?.text;
        this.paginationData.page = data?.page || 1;
        this.paginationData.limit = 50;
        this.getData(data?.status, 'Latest');
      }
    } catch (error) {
      const errorMessage = error?.error?.message || error?.message;
      const afterBlockData: InfoPopupComponentData = {
        title: this.translateService.instant('API.TOURNAMENT.PUT.ERROR_HEADER'),
        text: errorMessage,
        type: InfoPopupComponentType.info,
      };
      this.dialog.open(InfoPopupComponent, { data: afterBlockData });
    }
  };

  /**
   * Get the tournament list based on Tab selection
   * @param tabChangeEvent : Event on Change of Tab
   */
  onTabChanged = (tabChangeEvent: MatTabChangeEvent): void => {
    this.customMatTabEvent.emit(tabChangeEvent);
  };

  switchData(index) {
    switch (index) {
      case 0:
        this.resetPage();
        this.setConfiguration(true, true, false, false, true);
        this.getData('1', 'Latest'); //ongoing
        break;
      case 1:
        this.resetPage();
        this.setConfiguration(true, true, false, true, true);
        this.getData('0', 'Latest'); // upcoming
        break;
      case 2:
        this.resetPage();
        this.setConfiguration(true, true, false, false, true);
        this.getData('2', 'Latest'); //past
        break;
      case 3:
        this.resetPage();
        this.setConfiguration(true, true, true, true, false);
        this.getData('5', 'Latest');
        break;
      case 4:
        this.resetPage1();
        this.getData('10', 'Oldest');
        // this.getTournamentForPendingPayments();
        break;
      default:
        break;
    }
  }

  resetPage1() {
    this.paginationData = {
      page: 1,
      limit: 50,
      sort: 'Oldest',
    };
    this.tournamentList = [];
  }

  resetPage() {
    this.paginationData = {
      page: 1,
      limit: 50,
      sort: 'createdOn',
    };
    this.tournamentList = [];
  }

  getData = async (status, sortOrder) => {
    try {
      const params = {
        limit: this.paginationData.limit,
        page: this.paginationData.page,
        status: status,
        sort: sortOrder,
        ...(status == 5 && {
          tournamentStatus: 'submitted_for_approval',
        }),
        text: this.text || '',
      };

      let response;
      this.isLoading = true;
      if (status == 5 || status == 10) {
        this.tournamentList = [];
        response = await this.tournamentService
          .getPaginatedTournaments(API, params)
          .toPromise();
      } else {
        if (params.text) {
          response = await this.tournamentService.fetchTournamentByStatus(
            API,
            params
          );
        } else {
          this.tournamentList = [];
          response = await this.tournamentService.fetchTournamentByStatus(
            API,
            params
          );
        }
        this.text = '';
      }

      this.mapTournamentFilterList(response?.data?.docs);
      this.page = {
        totalItems: response?.data?.totalDocs,
        itemsPerPage: response?.data?.limit,
        maxSize: 5,
      };
      this.isLoading = false;
    } catch (error) {
      this.isLoading = false;
    }
  };

  /**
   * Map the tournament details to display in the table
   * @param tournamentList : response from method
   */
  mapTournamentFilterList(tournamentList) {
    const response = tournamentList.map((element) => {
      return {
        _id: element._id,
        slug: element.slug,
        name: element.name,
        isPaid: element.isPaid,
        isFeature: element.isFeature,
        game: element.gameDetail?.name,
        region: element.regionsAllowed,
        startDate: element.startDate,
        tournamentType: element.tournamentType,
        createdOn: element.createdOn,
        maxParticipants: element.maxParticipants,
        reason: element.reason,
        level: element.level,
      };
    });
    this.tournamentList = response;
  }

  /**
   * Configure icon based on boolean value
   * @param viewIcon : show/hide icon
   * @param deleteIcon:show/hide icon
   * @param checkIcon:show/hide icon
   */
  setConfiguration(viewIcon, deleteIcon, checkIcon, editIcon, manageIcon) {
    this.viewIconShow = viewIcon;
    this.deleteIconShow = deleteIcon;
    this.checkIconShow = checkIcon;
    this.editIconShow = editIcon;
    this.manageIconShow = manageIcon;
  }
}
