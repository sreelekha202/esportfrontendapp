import { Component, OnInit, OnDestroy } from "@angular/core";
import { BracketService } from "../../core/service/bracket.service";
import { AppHtmlRoutes } from "../../app-routing.model";

@Component({
  selector: "app-bracket",
  templateUrl: "./bracket.component.html",
  styleUrls: [
    "./bracket.component.scss",
    "../tournament/tournament.component.scss",
  ],
})
export class BracketComponent implements OnInit, OnDestroy {
  AppHtmlRoutes = AppHtmlRoutes;

  constructor(private bracketService: BracketService) {}

  ngOnInit(): void {}

  ngOnDestroy() {
    this.bracketService.setBracketData(null);
  }
}
