import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";
import { BracketService } from "../../../core/service";
import { TranslateService } from "@ngx-translate/core";
import { EsportsLanguageService, EsportsToastService } from "esports";

@Component({
  selector: "app-upsert-bracket",
  templateUrl: "./upsert-bracket.component.html",
  styleUrls: [
    "./upsert-bracket.component.scss",
    "../../tournament/create/create.component.scss",
  ],
})
export class UpsertBracketComponent implements OnInit {
  bracket: FormGroup;
  navUrl = "../preview";
  maximumParticipants = 1024;
  nfMatchBetweenTwoTeam = 3;
  isProcessing = false;

  constructor(
    private bracketService: BracketService,
    private fb: FormBuilder,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private toastService: EsportsToastService,
    private translateService: TranslateService,
    private languageService: EsportsLanguageService
  ) {}

  ngOnInit(): void {
    this.languageService.language.subscribe((lang) =>
      this.translateService.use(lang || "en")
    );
    this.bracket = this.fb.group({
      id: [""],
      name: ["", Validators.required],
      bracketType: ["single", Validators.required],
      maximumParticipants: [
        2,
        Validators.compose([
          Validators.required,
          Validators.min(2),
          Validators.max(1024),
        ]),
      ],
      isKnowParticipantName: [false, Validators.required],
      nfMatchBetweenTwoTeam: [1],
      isRoundRobin: [false],
      noOfSet: [3],
    });
    const bracket = this.bracketService.getBracketData();
    const id = this.activeRoute.snapshot.params.id;
    this.navUrl = bracket?.id || id ? "../" + this.navUrl : this.navUrl;
    if (bracket) {
      this.bracket.patchValue({
        ...bracket,
      });
    } else if (id) {
      this.fetchBracket(id);
    }
  }

  /**
   * Fatch bracket details By Id
   * @param id  Bracket ID
   */
  fetchBracket = async (id) => {
    try {
      this.isProcessing = true;
      const bracket = await this.bracketService.getBracketByID(id);
      this.bracket.patchValue({
        ...bracket.data[0],
        id: bracket.data[0]._id,
      });
      this.isProcessing = false;
    } catch (error) {
      this.isProcessing = false;
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  /**
   * Enable Round Robin no of match option
   * @param check boolean which contain decide is either round robin or (single, double elimination)
   * @param type single, double, round_robin
   */
  enableBattleRoyalRound(check: boolean, type: string) {
    if (check) {
      this.nfMatchBetweenTwoTeam = type == "battle_royale" ? 10 : 3;
      this.bracket.get("isRoundRobin").setValue(true);
      this.bracket
        .get("nfMatchBetweenTwoTeam")
        .setValidators([
          Validators.required,
          Validators.min(1),
          Validators.max(this.nfMatchBetweenTwoTeam),
        ]);
    } else {
      this.bracket.get("isRoundRobin").setValue(false);
      this.bracket.get("nfMatchBetweenTwoTeam").clearValidators();
    }
    switch (type) {
      case "single":
        this.maximumParticipants = 1024;
        break;
      case "double":
        this.maximumParticipants = 512;
        break;
      case "round_robin":
        this.maximumParticipants = 40;
        break;
      case "battle_royale":
        this.maximumParticipants = 100;
        break;
    }
    this.bracket
      .get("maximumParticipants")
      .setValidators([
        Validators.required,
        Validators.min(2),
        Validators.max(this.maximumParticipants),
      ]);
    this.bracket.get("maximumParticipants").updateValueAndValidity();
    this.bracket.get("nfMatchBetweenTwoTeam").updateValueAndValidity();
    this.bracket.get("isRoundRobin").updateValueAndValidity();
  }

  /**
   * Move to preview Screen
   */
  preview() {
    if (this.bracket.invalid) {
      this.bracket.markAllAsTouched();
      return;
    }
    const value = this.bracket.value;
    this.bracketService.setBracketData(value);
    this.router.navigate([this.navUrl], { relativeTo: this.activeRoute });
  }
}
