import { Component, OnInit,Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TeamService } from '../../../core/service/team.service';

@Component({
  selector: 'app-view-team',
  templateUrl: './view-team.component.html',
  styleUrls: ['./view-team.component.scss']
})
export class ViewTeamComponent implements OnInit {
  @Input() data;
  teamId: any;
  teamList = [];
  teamDatails: any;
  editImageUrl: any = null;
  constructor(
    private _activateRoute: ActivatedRoute,
    private teamService: TeamService) { }

  ngOnInit(): void {
    this._activateRoute.paramMap.subscribe((params) => {
      this.teamId = params.get('id');
    });

    this.teamService.getViewTeam(this.teamId).
      subscribe((res: any) => {
        this.teamList = [];
        for (let d of res.data.members) {
          this.teamList.push(
            {
              image: d.userId.profilePicture,
              name: d.userId?.username,
            }
          )
        }
        this.teamDatails = res.data.team;
      })
  }
}
