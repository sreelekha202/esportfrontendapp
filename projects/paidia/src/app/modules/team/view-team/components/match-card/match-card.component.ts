import { Component, OnInit, Input } from '@angular/core';

interface MatchCard {
  image: string;
  match: number;
  set: number;
  title: string;
  date: {
    when: string;
    report: string;
  };
}

@Component({
  selector: 'app-match-card',
  templateUrl: './match-card.component.html',
  styleUrls: ['./match-card.component.scss'],
})
export class MatchCardComponent implements OnInit {
  @Input() data: MatchCard;
  @Input() isEditable: boolean = false;

  constructor() {}

  ngOnInit(): void {}
}
