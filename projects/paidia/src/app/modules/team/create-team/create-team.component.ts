import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { Observable, Subscription } from 'rxjs';
import {
  catchError,
  debounceTime,
  distinctUntilChanged,
  switchMap,
} from 'rxjs/operators';
import {
  EsportsUserService,
  EsportsToastService,
  GlobalUtils,
  EsportsGtmService,
  EventProperties,
} from 'esports';
import { Location } from '@angular/common';
import {
  AppHtmlProfileRoutes,
  AppHtmlRoutes,
} from '../../../app-routing.model';
import { IUser } from 'esports';
import { TeamService } from '../../../core/service/team.service';

import { environment } from '../../../../environments/environment';
const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;
@Component({
  selector: 'app-create-team',
  templateUrl: './create-team.component.html',
  styleUrls: ['./create-team.component.scss'],
})
export class CreateTeamComponent implements OnInit, OnDestroy {
  name = '';
  bio = '';
  AppHtmlRoutes = AppHtmlRoutes;
  AppHtmlProfileRoutes = AppHtmlProfileRoutes;
  form: FormGroup;
  currentUser: IUser;
  membersObj;
  emailInvitation = [];
  singleEmail;
  dropdown: any;
  txtValue: any;
  playername = [];
  memberArray = [];
  formatter = (result: any) => {
    return result;
  };

  get email(): any {
    // return this.form.get("email");
    return this.singleEmail;
  }
  hideDropdown: any;

  biographyLength = 250;

  mock_select = [
    {
      name: 'facebook',
      placeholder: 'Facebook url',
      value: '',
    },
    {
      name: 'twitch',
      placeholder: 'Twitch url',
      value: '',
    },
    {
      name: 'instagram',
      placeholder: 'Instagram url',
      value: '',
    },
    {
      name: 'youtube',
      placeholder: 'Youtube url',
      value: '',
    },
    {
      name: 'discord',
      placeholder: 'Discord url',
      value: '',
    },
    {
      name: 'twitter',
      placeholder: 'Twitter url',
      value: '',
    },
  ];
  isInEditMode: boolean = false;
  clicked: boolean = false;
  selectedSocial = [];
  teamData;
  teamId: string;
  userSubscription: Subscription;
  fallbackURL: string;
  constructor(
    private fb: FormBuilder,
    private userService: EsportsUserService,
    public toastService: EsportsToastService,
    private translateService: TranslateService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private location: Location,
    private gtmService: EsportsGtmService,
    private teamService: TeamService
  ) {
    this.clearsession();
  }

  ngOnInit(): void {
    if (
      GlobalUtils.isBrowser() &&
      localStorage.getItem(environment.currentToken)
    ) {
      this.getUserData();
    } else {
      this.router.navigate(['/']);
    }
    this.createTournamentForm();
    if (this.activatedRoute.snapshot.params['id']) {
      this.isInEditMode = true;
      this.teamId = this.activatedRoute.snapshot.params['id'];
      this.fallbackURL = '/admin/team-management';
      this.getTeamData(this.activatedRoute.snapshot.params['id']);
    }
  }
  getTeamData(id) {
    const query = `?query=${encodeURIComponent(JSON.stringify({ _id: id }))}`;
    this.userService.getTeamList(API, query).subscribe(
      (res) => {
        this.teamData = {
          teamLogo: res.data[0].logo,
          teamBanner: res.data[0].teamBanner,
          socialMedia: res.data[0]?.social,
          shortDescription: res.data[0].shortDescription,
          teamName: res.data[0].name,
          members: res.data[0].member,
        };
        var list = [];

        if (this.teamData?.socialMedia) {
          for (
            var i = 0;
            i < Object.keys(this.teamData?.socialMedia).length;
            i++
          ) {
            var item = {};
            item['name'] = Object.keys(this.teamData?.socialMedia)[i];
            item['placeholder'] =
              Object.keys(this.teamData?.socialMedia)[i] + ' url';
            item['value'] =
              this.teamData?.socialMedia[
                Object.keys(this.teamData?.socialMedia)[i]
              ];

            let validatorFn = this.fb.control('', []);
            this.form.addControl(
              Object.keys(this.teamData?.socialMedia)[i],
              validatorFn
            );
            this.form.controls[
              Object.keys(this.teamData?.socialMedia)[i]
            ].setValue(
              this.teamData?.socialMedia[
                Object.keys(this.teamData?.socialMedia)[i]
              ]
            );
            this.form.updateValueAndValidity();

            list.push(item);
          }
        }

        this.selectedSocial = list;
        const indexm = this.teamData.members.findIndex(
          (x) => JSON.stringify(x._id) === JSON.stringify(this.currentUser?._id)
        );
        if (indexm > -1) {
          if (
            this.teamData.members[indexm].role != 'player' ||
            this.currentUser.accountType == 'admin'
          ) {
            this.form.patchValue(this.teamData);
          } else {
            this.toastService.showError(
              this.translateService.instant('PROFILE.TEAMS.ERRORS.ERROR6')
            );
            this.router.navigate(['/profile/teams']);
          }
        } else if (this.currentUser.accountType == 'admin') {
          this.form.patchValue(this.teamData);
        } else {
          this.toastService.showError(
            this.translateService.instant('PROFILE.TEAMS.ERRORS.ERROR6')
          );
          this.router.navigate(['/profile/teams']);
        }
      },
      (err) => {}
    );
  }
  cancel() {
    this.location.back();
  }
  ngOnDestroy() {
    this.userSubscription?.unsubscribe();
  }
  emitValue(emailInvitation) {
    this.emailInvitation = emailInvitation;
  }
  getUserData() {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.currentUser = data;
      }
    });
  }

  filterFunction() {
    var input, filter, a, i;
    input = document.getElementById('myInput');
    filter = input.value.toUpperCase();
    this.dropdown = document.getElementById('myDropdown');
    a = this.dropdown.getElementsByTagName('a');
    for (i = 0; i < a.length; i++) {
      this.txtValue = a[i].textContent || a[i].innerText;
      if (this.txtValue.toUpperCase().indexOf(filter) > -1) {
        a[i].style.display = '';
      } else {
        a[i].style.display = 'none';
      }
    }
  }

  getValue(value) {
    if (!this.isSocialAlreadyAdded(this.selectedSocial, value?.name)) {
      this.selectedSocial.push(value);
      let validatorFn = this.fb.control('', []);
      this.form.addControl(value.name, validatorFn);
      this.form.updateValueAndValidity();
    }
    this.hideDropdown = false;
  }
  removeSocial(i) {
    this.form.controls[this.selectedSocial[i].name].reset();
    this.selectedSocial.splice(i, 1);
  }

  createTournamentForm() {
    this.form = this.fb.group({
      teamLogo: [''],
      teamBanner: [''],
      teamName: ['', Validators.required],
      shortDescription: [''],
      members: [[], Validators.compose([])],
      socialMedia: [''],
      email: [''],
    });
  }

  createTeam() {
    const controls = this.form.controls;
    this.clicked = true;
    if (this.form.invalid) {
      Object.keys(controls).forEach((controlName) =>
        controls[controlName].markAsTouched()
      );
      return;
    }

    let social = {
      facebook: '',
      twitch: '',
      youtube: '',
      discord: '',
      twitter: '',
      instagram: '',
    };
    let members = [];
    let that = this;
    this.mock_select.forEach(function (value) {
      if (that.form.value[value.name] != null && that.form.value[value.name]) {
        social[value.name] = that.form.value[value.name];
      } else {
        delete social[value.name];
      }
    });

    this.form.value.members.forEach(function (value) {
      members.push(value._id);
    });

    let payload = {
      logo: this.form.value.teamLogo,
      teamBanner: this.form.value.teamBanner,
      teamName: this.form.value.teamName,
      shortDescription: this.form.value.shortDescription,
      social: social,
      members: members,
      email: this.emailInvitation,
    };
    if (this.isInEditMode) {
      this.updateTeam(payload);
    } else {
      this.userService.create_team(API, payload).subscribe(
        (data) => {
          this.clicked = false;
          this.toastService.showSuccess(data?.message);
          this.cancel();
        },
        (error) => {
          this.clicked = false;
          this.toastService.showError(error?.error?.message);
        }
      );
    }
  }
  updateTeam(payload) {
    this.clicked = true;
    let teamData = {
      logo: payload.logo,
      teamBanner: payload.teamBanner,
      teamName: payload.teamName,
      shortDescription: payload.shortDescription,
      social: payload.social,
    };
    const teamObj = {
      id: this.teamId,
      admin: true,
      member: this.form.value.members,
      userName: this.currentUser.fullName,
      email: payload.email,
      query: {
        condition: { _id: this.teamId },
        update: teamData,
      },
    };
    this.userService.team_update(API, teamObj).subscribe(
      (res) => {
        this.clicked = false;
        if (res.data) {
          this.toastService.showSuccess(res.message);
          this.cancel();
        } else {
          this.toastService.showError(res.message);
        }
      },
      (err) => {
        this.clicked = false;
        this.toastService.showError(err.error.message);
      }
    );
  }
  search = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      distinctUntilChanged(),
      switchMap((term) => {
        if (term) {
          return this.userService.searchUsers(term);
        } else {
          return [];
        }
      }),
      catchError(() => {
        this.toastService.showError(
          this.translateService.instant('TOURNAMENT.SEARCH_FAILED')
        );

        return [];
      })
    );

  onChangeMembers() {
    this.membersObj = '';
  }

  addMembers() {
    setTimeout(() => {
      if (this.membersObj && typeof this.membersObj == 'object') {
        if (
          !this.isMembersAlreadyAdded(
            this.form.value.members,
            this.membersObj?._id
          )
        ) {
          if (this.membersObj._id != this.currentUser._id) {
            this.form.value.members.push(this.membersObj);
          } else {
            this.toastService.showError(
              this.translateService.instant('PROFILE.TEAMS.ERRORS.ERROR4')
            );
          }
        } else {
          this.toastService.showError(
            this.translateService.instant('PROFILE.TEAMS.ERRORS.ERROR3')
          );
        }
        this.membersObj = '';
      }
    }, 100);
  }

  isMembersAlreadyAdded(arr, _id) {
    const { length } = arr;
    const id = length + 1;
    const found = arr.some((el) => el?._id === _id);
    return found;
  }

  isSocialAlreadyAdded(arr, name) {
    const { length } = arr;
    const id = length + 1;
    const found = arr.some((el) => el?.name === name);
    return found;
  }

  clickAddButton(addMember) {
    this.memberArray.push(addMember);
  }

  removeMember(index) {
    // if (this.isInEditMode) {
    //   if (this.form.value.members[index].role == 'owner') {
    //     this.toastService.showError(
    //       this.translateService.instant('PROFILE.TEAMS.ERRORS.ERROR2')
    //     );
    //   } else {
    //     this.form.value.members[index].status = 'deleted';
    //   }
    // } else {
    this.form.value.members.splice(index, 1);
    // }
  }

  validateEmail(email) {
    const regularExpression =
      /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (!regularExpression.test(String(email).toLowerCase())) {
      return 0;
    }
    return 1;
  }
  addEmails() {
    if (this.validateEmail(this.email)) {
      if (this.emailInvitation.indexOf(this.email) == -1) {
        this.emailInvitation.push(this.email);
      } else {
        this.toastService.showError(
          this.translateService.instant('Duplicate invite by email address')
        );
      }
    } else {
      this.toastService.showError(
        this.translateService.instant(
          'Invite by email address is not a valid email address'
        )
      );
    }
    this.singleEmail = '';
  }
  removeEmail(index) {
    this.emailInvitation.splice(index, 1);
  }
  onTextChange(a) {
    if (a.length >= 3) {
      this.teamService.searchUsers(a).subscribe((data: any) => {
        this.playername = data;
        for (let s of this.playername) {
          s.selected = false;
        }
      });
    }
  }

  submit() {
    let imgurl = sessionStorage.getItem('imgurl');
    let bannerurl = sessionStorage.getItem('bannerurl');
    let link = sessionStorage.getItem('link');
    let link1 = sessionStorage.getItem('Fblink');
    let link2 = sessionStorage.getItem('twlink');

    let sociall = new Object();
    link ? (sociall['twitter'] = link) : '';
    link1 ? (sociall['facebook'] = link1) : '';
    link2 ? (sociall['twitch'] = link2) : '';
    let d = [];
    for (let a of this.memberArray) {
      if (a.selected == true) {
        d.push(a._id);
      }
    }
    let datatosend = {
      email: this.emailInvitation,
      logo: imgurl,
      teamBanner: bannerurl,
      members: d,
      shortDescription: this.bio,
      social: sociall,
      teamName: this.name,
    };
    if (this.name) {
      this.teamService.saveteam(datatosend).subscribe(
        (data: any) => {
          this.pushGTMTags('Create_New_Team_Complete', {
            inviteExternalUsers: this.emailInvitation?.length,
          });
          this.cancel();
          this.clearsession();
        },
        ({ error }) => {
          this.toastService.showError(error.message);
          // this.clearsession();
        }
      );
    } else {
      let errroMSG = 'Please add ';
      let fag1 = false;
      if (!this.name) {
        errroMSG = errroMSG + 'team name';
        fag1 = true;
      }
      // if (Object.keys(sociall).length <= 0) {
      //   if (fag1)
      //     errroMSG = errroMSG + ', '
      //   errroMSG = errroMSG + 'social media details';
      // }
      this.toastService.showError(errroMSG);
    }
  }
  clearsession() {
    sessionStorage.removeItem('imgurl');
    sessionStorage.removeItem('link');
    sessionStorage.removeItem('Fblink');
    sessionStorage.removeItem('twlink');
  }

  onCancel() {
    this.pushGTMTags('Create_New_Team_Cancellation');
  }

  pushGTMTags(eventName: string, eventProps: EventProperties = null) {
    this.gtmService.pushGTMTags(eventName, eventProps);
  }
}
