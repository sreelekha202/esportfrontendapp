import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-social-accounts',
  templateUrl: './social-accounts.component.html',
  styleUrls: ['./social-accounts.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class SocialAccountsComponent implements OnInit {
  accounts = [];
  isTwitter = false
  isFacebook = false
  isTwitch = false
  isYoutube = false;
  isDiscord = false;
  selectedAccounts = [];
  accounts2 = new FormControl();
  accounts2List: string[] = ['Twitter', 'Facebook', 'Twitch', 'Youtube', 'Discord'];
  addPlatform = [];

  constructor() { }

  t = {
    icon: 'https://i115.fastpic.ru/big/2021/0630/8a/fbd83ffaa866e9861c383efbe9601a8a.png',
    label: 'Twitter',
  }
  f = {
    icon: 'https://i115.fastpic.ru/big/2021/0630/8d/fd933c6c38c0673ad6845233f677618d.png',
    label: 'Facebook',
  }
  Twitch = {
    icon: 'https://i115.fastpic.ru/big/2021/0630/65/7a457917ed0aef758a736f3a23594d65.png',
    label: 'Twitch',
  }
  y = {
    icon: 'https://i115.fastpic.ru/big/2021/0630/8a/fbd83ffaa866e9861c383efbe9601a8a.png',
    label: 'Youtube',
  }
  d = {
    icon: 'https://i115.fastpic.ru/big/2021/0630/8a/fbd83ffaa866e9861c383efbe9601a8a.png',
    label: 'Discord',
  }

  ngOnInit(): void {
    // MOCK DATA
    // this.accounts = []

  }

  addPlatforms() {
    this.addPlatform = this.accounts
  }

  addsocial(a, i) {
    if (a == 'Twitter') {
      if (this.isTwitter) {
        this.isTwitter = false
        this.accounts = this.accounts.filter(item => item.label !== a);
      }
      else {
        this.isTwitter = true
        this.accounts.push(this.t)
      }
    }

    if (a == 'Facebook') {
      if (this.isFacebook) {
        this.isFacebook = false
        this.accounts = this.accounts.filter(item => item.label !== a);
      }
      else {
        this.accounts.push(this.f)
        this.isFacebook = true
      }
    }
    if (a == 'Twitch') {
      if (this.isTwitch) {
        this.isTwitch = false
        this.accounts = this.accounts.filter(item => item.label !== a);
      }
      else {
        this.isTwitch = true
        this.accounts.push(this.Twitch)
      }
    }

    if (a == 'Youtube') {
      if (this.isYoutube) {
        this.isYoutube = false
        this.accounts = this.accounts.filter(item => item.label !== a);
      }
      else {
        this.isYoutube = true
        this.accounts.push(this.y)
      }
    }

    if (a == 'Discord') {
      if (this.isDiscord) {
        this.isDiscord = false
        this.accounts = this.accounts.filter(item => item.label !== a);
      }
      else {
        this.isDiscord = true
        this.accounts.push(this.d)
      }
    }

  }

  remove(a, i) {
    if (a == 'Twitter') {
      if (this.isTwitter) {
        this.isTwitter = false
        this.selectedAccounts = this.selectedAccounts.filter(item => item !== a);
        this.accounts.splice(i, 1)
      }
    }
    if (a == 'Facebook') {
      if (this.isFacebook) {
        this.isFacebook = false
        this.selectedAccounts = this.selectedAccounts.filter(item => item !== a);
        this.accounts.splice(i, 1)
      }
    }

    if (a == 'Twitch') {
      if (this.isTwitch) {
        this.isTwitch = false
        this.selectedAccounts = this.selectedAccounts.filter(item => item !== a);
        this.accounts.splice(i, 1)
      }
    }

    if (a == 'Youtube') {
      if (this.isYoutube) {
        this.isYoutube = false
        this.selectedAccounts = this.selectedAccounts.filter(item => item !== a);
        this.accounts.splice(i, 1)
      }
    }

    if (a == 'Discord') {
      if (this.isDiscord) {
        this.isDiscord = false
        this.selectedAccounts = this.selectedAccounts.filter(item => item !== a);
        this.accounts.splice(i, 1)
      }
    }

  }

}
