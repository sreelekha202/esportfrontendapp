import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
import { AppHtmlRoutes } from '../../app-routing.model';
import {
  GameService,
  HomeService,
  ParticipantService,
  TournamentService,
  UserService,
} from '../../core/service';
import { NgbCarousel, NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';
import { EsportsTimezone } from 'esports';
import { Router } from '@angular/router';
import { ProfileService } from '../../core/service/profile.service';
import { CountdownFormatFn } from 'ngx-countdown';
import {
  EsportsUserService,
  EsportsToastService,
  EsportsTournamentService,
  EsportsLanguageService,
  EsportsGameService,
  EsportsGtmService,
  EventProperties,
} from 'esports';
import { environment } from '../../../environments/environment';
import { MatSelect } from '@angular/material/select';

const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;

@Component({
  selector: 'app-play',
  templateUrl: './play.component.html',
  styleUrls: ['./play.component.scss'],
  providers: [NgbCarouselConfig],
  encapsulation: ViewEncapsulation.None,
})
export class PlayComponent implements OnInit {
  @ViewChild('slider') carousel: NgbCarousel;
  @ViewChild('mySelect') mySelect: MatSelect;
  AppHtmlRoutes = AppHtmlRoutes;
  showLoader: boolean = true;
  currentLang: string = 'english';
  mobFiltersMenuOpened: boolean = false;
  text = '';
  tournamentsSlides = [];
  currentSlide: any = null;
  timezone;
  games = [];
  gamesShort = [];
  onGoingTournaments = [];
  // timer: any;
  // timem: any;
  remainingTime;
  remainingTimeForTournamentStart;
  showRegTime;
  showTournamentTime;
  remainigCheckInTime: number = 0;
  selectedGame: any;
  selected_sortby_game: any;
  platforms = [];
  platformList: any = [];
  selected_platform: any;
  sortby = [
    { value: '0', viewValue: 'Upcoming tournaments' },
    { value: '1', viewValue: 'Ongoing tournaments' },
    { value: '2', viewValue: 'Completed tournaments' },
    { value: '3', viewValue: 'All tournaments' },
  ];
  selected_sortby_tournament = this.sortby[0].value;
  CountdownTimeUnits: Array<[string, number]> = [
    ['Y', 1000 * 60 * 60 * 24 * 365], // years
    ['M', 1000 * 60 * 60 * 24 * 30], // months
    ['D', 1000 * 60 * 60 * 24], // days
    ['H', 1000 * 60 * 60], // hours
    ['m', 1000 * 60], // minutes
    ['s', 1000], // seconds
  ];
  btnType: string = '';
  participantId: string | null;
  individualTournaments = [];
  prizeMoneyTournaments = [];
  teamTournaments = [];
  tournamentData: any;
  tournamentDetails: any;
  params: any = {};

  startDate: any;
  user: any;

  constructor(
    private esportsGameService: EsportsGameService,
    private config: NgbCarouselConfig,
    private translate: TranslateService,
    private esportsTimezone: EsportsTimezone,
    public language: EsportsLanguageService,
    private route: Router,
    private tournamentService: EsportsTournamentService,
    private homeService: HomeService,
    private gameService: EsportsGameService,
    private userService: EsportsUserService,
    private gtmService: EsportsGtmService,
    private profileService: ProfileService,
    private participantService: ParticipantService,
    private toastService: EsportsToastService
  ) {
    this.config.interval = 12000;
    this.currentLang = translate.currentLang == 'ms' ? 'malay' : 'english';

    translate.onLangChange.subscribe((event: LangChangeEvent) => {
      this.currentLang = event.lang == 'ms' ? 'malay' : 'english';
    });
    this.getCurrentUserDetails();
  }

  ngOnInit(): void {
    this.remainingTime = 0;
    this.remainingTimeForTournamentStart = 0;
    this.showRegTime = false;
    this.showTournamentTime = false;
    this.timezone = this.esportsTimezone.getTimeZoneName();
    this.getTournamentsSlides();
    this.getOnGoingTournaments();
    this.getAllTournaments();
    this.getGames();
    this.getPlatformList();
  }
  getCurrentUserDetails() {
    this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.user = data;
      }
    });
  }

  getGames() {
    this.showLoader = true;
    let a = [];
    let b = [];
    this.gameService.getGames(API).subscribe(
      (res) => {
        this.games = res?.data;
        this.showLoader = false;
        for (let i = 0; i < this.games.length; i++) {
          if (this.games[i]?.name == 'The Sims 4') {
            a[0] = this.games[i];
          }

          if (this.games[i]?.name == 'Valorant') {
            a[1] = this.games[i];
          }

          if (this.games[i]?.name == 'League of Legends') {
            a[2] = this.games[i];
          }

          if (this.games[i]?.name == 'Fortnite') {
            a[3] = this.games[i];
          }

          if (this.games[i]?.name == 'Minecraft') {
            a[4] = this.games[i];
          }

          if (this.games[i]?.name == 'Pokémon TCG Online') {
            a[5] = this.games[i];
          }

          if (this.games[i]?.name == 'Apex Legends') {
            a[6] = this.games[i];
          }

          if (this.games[i]?.name == 'Call of Duty Mobile') {
            a[7] = this.games[i];
          }

          if (this.games[i]?.name == 'World of Warcraft') {
            a[8] = this.games[i];
          }

          if (this.games[i]?.name == 'Halo MCC') {
            a[9] = this.games[i];
          }

          if (this.games[i]?.name == 'Rocket League') {
            a[10] = this.games[i];
          }

          if (this.games[i]?.name == 'Overwatch') {
            a[11] = this.games[i];
          }

          if (
            this.games[i]?.name != 'Overwatch' &&
            this.games[i]?.name != 'Rocket League' &&
            this.games[i]?.name != 'Halo MCC' &&
            this.games[i]?.name != 'World of Warcraft' &&
            this.games[i]?.name != 'Call of Duty Mobile' &&
            this.games[i]?.name != 'Apex Legends' &&
            this.games[i]?.name != 'Pokémon TCG Online' &&
            this.games[i]?.name != 'Minecraft' &&
            this.games[i]?.name != 'Fortnite' &&
            this.games[i]?.name != 'League of Legends' &&
            this.games[i]?.name != 'Valorant' &&
            this.games[i]?.name != 'The Sims 4'
          ) {
            b[i] = this.games[i];
          }
        }
        this.gamesShort = a;
        //  var newArray = b.filter(value => Object.keys(value).length !== 0)
        //  this.gamesShort = [ ...this.gamesShort, ...newArray]
      },
      (err) => {
        this.showLoader = false;
      }
    );
  }

  getPlatformList() {
    this.esportsGameService.fetchPlatforms(API).subscribe((res) => {
      this.platformList = res?.data;
    });
  }

  onSelectGame(gameIndex: number): void {
    this.games.forEach((game, idx) => {
      game.isSelected = gameIndex === idx;
    });
  }

  isNumber(val): boolean {
    const str: string = val.toString();
    return str.startsWith('+');
  }

  getTournamentsSlides() {
    const encodedOption = encodeURIComponent(
      JSON.stringify({ sort: { startDate: -1 } })
    );
    this.showLoader = true;
    this.tournamentService
      .getPaginatedTournaments(API, {
        status: 6,
        limit: 8,
        sort: encodedOption,
        // sort: 'participantJoined',
      })
      .subscribe(
        (res: any) => {
          this.tournamentsSlides = res?.data?.docs;
          if (this.tournamentsSlides && this.tournamentsSlides.length > 0) {
            this.currentSlide = this.tournamentsSlides[0];
            this.fetchParticipantRegisterationStatus(this.currentSlide._id);
          }
        },
        (err) => {
          this.showLoader = false;
        }
      );
  }

  getOnGoingTournaments() {
    const encodedOption = encodeURIComponent(
      JSON.stringify({ sort: { startDate: -1 } })
    );
    this.showLoader = true;
    const params = {
      status: 1,
      game: this.selectedGame,
      limit: 8,
      page: 1,
      sort: encodedOption,
      // sort: 'Latest',
      // sort :{"startDate":-1} or sort:{"startDate":1}
    };

    this.tournamentService.getPaginatedTournaments(API, params).subscribe(
      (res: any) => {
        this.onGoingTournaments = res?.data?.docs;
        let i = 0;
        this.showLoader = false;
      },
      (err) => {
        this.showLoader = false;
      }
    );
  }

  onSearch = (event: any) => {
    this.text = String(event.target.value);
    this.getAllTournaments();
  };

  getAllTournaments(gtmEvent = false) {
    // this.showLoader = true;

    if (this.selected_platform != '619f989c1c24ed06740ff936') {
      this.selected_platform
        ? (this.params.platform = this.selected_platform)
        : '';
      this.selected_sortby_game
        ? (this.params.game = this.selected_sortby_game)
        : '';
      this.selected_sortby_tournament
        ? (this.params.status = this.selected_sortby_tournament)
        : '';
      // this.selected_platform ? (this.params.sortby = this.selected_sortby) : '';
      this.text ? (this.params.text = this.text) : '';
    } else {
      delete this.params['platform'];
      this.selected_sortby_game
        ? (this.params.game = this.selected_sortby_game)
        : '';
      this.text ? (this.params.text = this.text) : '';
    }

    if (gtmEvent) {
      this.pushTournamentViewTags();
    }

    this.homeService.getTournament(this.params).subscribe(
      (res: any) => {
        this.individualTournaments = [];
        this.prizeMoneyTournaments = [];
        this.teamTournaments = [];
        this.showLoader = false;
        for (let d of res?.data?.docs) {
          if (d?.participantType == 'individual') {
            this.individualTournaments.push(d);
          } else if (d?.isPrize == true && d?.isPaid == true) {
            this.prizeMoneyTournaments.push(d);
          } else if (d?.participantType == 'team') {
            this.teamTournaments.push(d);
          }
        }
      },

      (err: any) => {
        this.showLoader = false;
      }
    );
  }

  fetchTournamentDetails = async (slug) => {
    try {
      const { data } = await this.tournamentService.getTournamentBySlug(
        `${slug}`
      );
      this.tournamentDetails = data;
      this.route.navigateByUrl(AppHtmlRoutes.joinTournament);
    } catch (error) {
      this.route.navigate(['/404']);
    }
  };

  onGameWiseFilterOngoingTournament(game) {
    if (game) {
      this.selectedGame = game['_id'];
      this.getOnGoingTournaments();
    }
  }
  setCurrentSlide(event): void {
    const index = this.getSlideIndex(event.current);
    this.currentSlide = this.tournamentsSlides[index];
    this.fetchParticipantRegisterationStatus(this.currentSlide._id);
  }

  getSlideIndex(str: string): number {
    return +str.replace(/[^0-9]/g, '');
  }

  getIconPath(selector: string): string {
    return `assets/icons/games/${selector}.svg`;
  }

  onToggleFilterMenu() {
    this.mobFiltersMenuOpened = !this.mobFiltersMenuOpened;
  }

  formatDate?: CountdownFormatFn = ({ date, formatStr }) => {
    let duration = Number(date || 0);
    return this.CountdownTimeUnits.reduce((current, [name, unit]) => {
      if (current.indexOf(name) !== -1) {
        const v = Math.floor(duration / unit);
        duration -= v * unit;
        return current.replace(new RegExp(`${name}+`, 'g'), (match: string) => {
          return v.toString().padStart(match.length, '0');
        });
      }
      return current;
    }, formatStr);
  };

  onTournamnetTimerFinished(e: Event) {
    if (e['action'] == 'done') {
      Promise.resolve(() => {
        this.showTournamentTime = false;
      });
      this.fetchParticipantRegisterationStatus(this.currentSlide?._id);
      // this.ngOnChanges();
    }
  }
  onTimerFinished(e: Event) {
    if (e['action'] == 'done') {
      Promise.resolve(() => {
        this.showRegTime = false;
      });
      this.fetchParticipantRegisterationStatus(this.currentSlide?._id);
      // this.ngOnChanges();
    }
  }
  fetchParticipantRegisterationStatus = async (id: string) => {
    try {
      const { data } =
        await this.tournamentService.fetchParticipantRegistrationStatus(
          API,
          id
        );

      this.participantId = data?.participantId;
      this.showCountdowns();
      /*
      this.remainingTime = data?.remainingTimeForRegStart / 1000;
      this.remainingTimeForTournamentStart =
        data?.remainingTimeForTournamentStart / 1000 || 0;
      this.showRegTime = this.remainingTime > 0 ? true : false;
      this.showTournamentTime =
        this.remainingTimeForTournamentStart > 0 ? true : false;
      if (this.remainingTime < 0) {
        this.showRegTime = false;
      }
      this.remainigCheckInTime = data?.remainingCheckInTime / 1000 || 0;
      */
      return data;
    } catch (error) {
      // this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  selectGames() {
    this.individualTournaments = [];
    this.prizeMoneyTournaments = [];
    this.teamTournaments = [];
    // this.showLoader = true;
    this.selected_platform
      ? (this.params.platform = this.selected_platform)
      : '';
    this.selected_sortby_game
      ? (this.params.game = this.selected_sortby_game)
      : '';
    this.selected_sortby_tournament
      ? (this.params.status = this.selected_sortby_tournament)
      : '';
    this.text ? (this.params.text = this.text) : '';
  }

  addGames() {
    this.homeService.getTournament(this.params).subscribe(
      (res: any) => {
        this.showLoader = false;
        for (let d of res?.data?.docs) {
          if (d?.participantType == 'individual') {
            this.individualTournaments.push(d);
          } else if (d?.isPrize == true && d?.isPaid == true) {
            this.prizeMoneyTournaments.push(d);
          } else if (d?.participantType == 'team') {
            this.teamTournaments.push(d);
          }
        }
      },
      (err: any) => {
        this.showLoader = false;
      }
    );
  }

  showCountdowns() {
    const now = Date.now();
    this.remainingTimeForTournamentStart =
      (new Date(this.currentSlide?.startDate).getTime() - now) / 1000;
    this.showTournamentTime =
      this.remainingTimeForTournamentStart > 0 ? true : false;
  }

  pushTournamentViewTags() {
    switch (this.selected_sortby_tournament) {
      case '1':
        this.pushGTMTags('View_Ongoing_Tournament', {
          fromPage: this.route.url,
        });
        break;
      case '0':
        this.pushGTMTags('View_Upcoming_Tournament', {
          fromPage: this.route.url,
        });
        break;
      default:
        break;
    }
  }

  viewOngoingTournament(tournament) {
    this.pushGTMTags('View_Tournament_By_Game', {
      tournamentName: tournament?.name,
    });
  }

  createTournamentView() {
    this.pushGTMTags('Create_Tournament_Click');
  }

  pushGTMTags(eventName, eventData: EventProperties = null) {
    this.gtmService.pushGTMTags(eventName, eventData);
  }
}
