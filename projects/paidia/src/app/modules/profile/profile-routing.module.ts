import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BookmarksComponent } from './bookmarks/bookmarks.component';
import { ContentComponent } from './content/content.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { InboxComponent } from './inbox/inbox.component';
import { InboxMessageComponent } from './inbox-message/inbox-message.component';
import { MatchesComponent } from './matches/matches.component';
import { MyStatsComponent } from './my-stats/my-stats.component';
import { MyTeamsComponent } from './my-teams/my-teams.component';
import { ProfileComponent } from './profile.component';
import { TournamentsCreatedComponent } from './tournaments-created/tournaments-created.component';
import { TournamentsJoinedComponent } from './tournaments-joined/tournaments-joined.component';
import { TransactionsComponent } from './transactions/transactions.component';
import { CreateVideoLibraryComponent } from './create-video-library/create-video-library.component';

const routes: Routes = [
  {
    path: '',
    component: ProfileComponent,
    children: [
      {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full',
      },
      {
        path: 'dashboard',
        component: DashboardComponent,
      },
      {
        path: 'matches',
        component: MatchesComponent,
      },
      {
        path: 'tournaments-joined',
        component: TournamentsJoinedComponent,
      },
      {
        path: 'tournaments-created',
        component: TournamentsCreatedComponent,
      },
      {
        path: 'content',
        component: ContentComponent,
      },
      {
        path: 'bookmarks',
        component: BookmarksComponent,
      },
      {
        path: 'transactions',
        component: TransactionsComponent,
      },
      {
        path: 'inbox',
        component: InboxComponent,
      },
      {
        path: 'inbox/:id',
        component: InboxMessageComponent,
      },
      {
        path: 'my-teams',
        component: MyTeamsComponent,
      },
      {
        path: 'my-stats',
        component: MyStatsComponent,
      },
      {
        path: 'add-content',
        loadChildren: () =>
          import('./add-content/add-content.module').then(
            (m) => m.AddContentModule
          ),
      },
      { path: 'add-video', component: CreateVideoLibraryComponent },
      { path: 'edit-video/:id', component: CreateVideoLibraryComponent },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProfileRoutingModule {}
