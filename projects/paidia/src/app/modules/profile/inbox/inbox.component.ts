import { Message } from '@angular/compiler/src/i18n/i18n_ast';
import { Component, Input, OnInit } from '@angular/core';
import { MessageService } from '../../../core/service';
import { IMessage } from '../../../shared/models/message';
import { SelectionModel } from '@angular/cdk/collections';
import { MatTableDataSource } from '@angular/material/table';
import { EsportsGtmService } from 'esports';
@Component({
  selector: 'app-inbox',
  templateUrl: './inbox.component.html',
  styleUrls: ['./inbox.component.scss'],
})
export class InboxComponent implements OnInit {
  showLoader: boolean = true;
  messages: Array<any> = [];
  selectedItems = [];
  messagesPerPage = [10, 15, 25];
  slicesMessages = [];

  constructor(private messageService: MessageService, private gtmService: EsportsGtmService) {}

  ngOnInit(): void {
    this.getMessages();
  }

  getMessages(): void {
    this.messageService.getInboxMessages().subscribe(
      (response) => {
        this.messages = response.data;
        this.slicesMessages = this.messages.slice(0, this.messagesPerPage[0]);
        this.showLoader = false;
        this.messageService.messageCountSubject.next(this.messages.length);
        this.dataSource = new MatTableDataSource(this.messages);
      },
      (error) => {
        this.showLoader = false;
      }
    );
  }
  displayedColumns: string[] = ['select', 'position', 'name'];
  dataSource: any;
  selection = new SelectionModel(true, []);

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected()
      ? this.selection.clear()
      : this.messages.forEach((row) => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${
      row.position + 1
    }`;
    // const removeIndex = this.slicesMessages.findIndex(item => item.isChecked == true);
    // this.slicesMessages.splice(removeIndex,1)
  }

  onSelectAll(): void {}
  onPageChange(event): void {
    const startIndex = event.pageIndex * event.pageSize;
    let endIndex = startIndex + event.pageSize;
    if (endIndex > this.messages.length) {
      endIndex = this.messages.length;
    }
    this.slicesMessages = this.messages.slice(startIndex, endIndex);
  }
  onSelect(row) {
    this.selectedItems = row;
  }

  removeSelectedRows(): void {
    this.gtmService.pushGTMTags('Delete_Inbox_Messages');
    this.messageService.deleteMultipleMessage(this.selectedItems).subscribe(
      (response) => {
        this.messages = response.data;
        this.getMessages();
        // this.slicesMessages = this.messages.splice(0, this.messagesPerPage[0]);
        this.showLoader = false;
        this.messageService.messageCountSubject.next(this.messages.length);
        // this.dataSource = new MatTableDataSource(this.messages);
      },
      (error) => {
        this.showLoader = false;
      }
    );
  }
}
