import { Component, EventEmitter, OnDestroy, OnInit } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { MatTabChangeEvent } from '@angular/material/tabs';
import { debounceTime } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import {
  EsportsToastService,
  EsportsTournamentService,
  IPagination,
  EsportsGtmService,
  EventProperties,
} from 'esports';

@Component({
  selector: 'app-tournaments-joined',
  templateUrl: './tournaments-joined.component.html',
  styleUrls: ['./tournaments-joined.component.scss'],
})
export class TournamentsJoinedComponent implements OnInit, OnDestroy {
  tournamentList = [];
  active = 1;
  nextId = 1;
  globalId = 1;
  showLoader: boolean = false;

  isHidden = false;

  tab = 0; //ongoing
  activeTabIndex = 0;
  cPagination;
  pagination = {
    page: 1,
    limit: 5,
    sort: 'startDate',
  };

  page: IPagination;
  pageSizeOptions = environment.pageSizeOptions;
  customTabEvent = new EventEmitter<MatTabChangeEvent>();
  tournamentSubscription: Subscription;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private tournamentService: EsportsTournamentService,
    private gtmService: EsportsGtmService,
    private eSportsToastService: EsportsToastService
  ) {}

  ngOnInit(): void {
    this.activeTabIndex =
      +this.activatedRoute.snapshot.queryParams?.activeTab || 0;
    this.active = this.activeTabIndex ? this.activeTabIndex + 1 : 1;
    this.cPagination = Object.assign({}, this.pagination);
    this.switchData(this.activeTabIndex);
  }

  ngOnDestroy() {
    this.tournamentSubscription?.unsubscribe();
  }
  attachTabChangeListener(id) {
    this.activeTabIndex = id;
    this.router.navigate(['.'], {
      relativeTo: this.activatedRoute,
      queryParams: { activeTab: id },
    });
    this.switchData(id);
  }

  switchData(index: number) {
    switch (index) {
      case 0:
        this.pushGTMTags('View_Ongoing_Tournament', {
          fromPage: this.router.url,
        });

        this.fetchTournaments(0); //ongoing
        break;
      case 1:
        this.pushGTMTags('View_Upcoming_Tournament', {
          fromPage: this.router.url,
        });
        this.fetchTournaments(1); //upcoming
        break;
      case 2:
        this.pushGTMTags('View_Past_Tournament ', {
          fromPage: this.router.url,
        });
        this.fetchTournaments(2); //past
        break;
      default:
        break;
    }
  }

  /**
   * Get tournament list
   */

  fetchTournaments = async (tab) => {
    try {
      this.tab = tab;
      this.tournamentList = [];

      if (this.tournamentSubscription) {
        this.tournamentSubscription.unsubscribe();
      }

      let payload;
      payload = await this.getJoinedTournamentQuery(tab);
      this.tournamentSubscription = this.tournamentService
        .getParticipantTournament1(payload)
        .subscribe((tournament) => {
          if (tournament) {
            this.page = {
              totalItems: tournament?.data?.totalDocs,
              itemsPerPage: tournament?.data?.limit,
              maxSize: 5,
            };
            this.tournamentList = tournament.data.docs || [];
          }
        });
    } catch (error) {
      this.eSportsToastService.showError(
        error?.error?.message || error?.message
      );
    }
  };

  navChanges = (e) => {
    this.attachTabChangeListener(e.nextId - 1);
  };

  pageChanged(page): void {
    this.cPagination.page = page.pageIndex + 1;
    this.cPagination.limit = page.pageSize;
    this.fetchTournaments(this.tab);
  }

  getJoinedTournamentQuery(tab) {
    switch (true) {
      case tab === 0: // ongoing
        this.isHidden = false;
        return {
          pagination: JSON.stringify(this.cPagination),
          type: 1,
        };
      case tab === 1: //upcoming
        this.isHidden = true;

        return {
          pagination: JSON.stringify(this.cPagination),
          type: 0,
          isEdit: true,
        };
      case tab === 2: //past
        this.isHidden = true;

        return {
          pagination: JSON.stringify(this.cPagination),
          type: 2,
          isEdit: true,
        };
    }
  }

  pushGTMTags(eventName: string, eventData: EventProperties = null) {
    this.gtmService.pushGTMTags(eventName, eventData);
  }
}
