import {
  EsportsUserService,
  EsportsVideoLibraryService,
  EsportsToastService,
} from 'esports';
import { ProfileService } from './../../../core/service/profile.service';
import { Component, OnInit } from '@angular/core';
import { LangChangeEvent, TranslateService } from '@ngx-translate/core';
import { HttpParams } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { environment } from '../../../../environments/environment';
const API = environment.apiEndPoint;
@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss'],
})
export class ContentComponent implements OnInit {
  articles = [];
  currentLang: string = 'english';
  pageSizeOptions = environment.pageSizeOptions;
  showLoader: boolean = true;
  slicesArticles = [];
  currentUser: any;
  isAdmin: boolean = false;
  active = 1;
  nextId = 1;
  paginationData = {
    page: 1,
    limit: 10,
    sort: '-updatedOn',
    projection: ['title', 'description', 'updatedOn', 'slug', 'views'],
  };
  paginationDetails: any;
  videoLibraryDetails: any;

  constructor(
    private profileService: ProfileService,
    private videoLibraryService: EsportsVideoLibraryService,
    private userService: EsportsUserService,
    private translateService: TranslateService,
    private eSportsToastService: EsportsToastService,
    private activatedRoute: ActivatedRoute
  ) {
    this.currentLang =
      translateService.currentLang == 'ms' ? 'malay' : 'english';
    translateService.onLangChange.subscribe((event: LangChangeEvent) => {
      this.currentLang = event.lang == 'ms' ? 'malay' : 'english';
    });
  }

  ngOnInit(): void {
    this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.currentUser = data;
        if (this.activatedRoute?.snapshot?.queryParams['activeTab'] == 2) {
          this.active = 2;
          this.getVideo();
        } else {
          this.active = 1;
          this.getArticle();
        }
        if (this.currentUser.hasOwnProperty('isAuthor')) {
          this.currentUser.isAuthor == 0 ? (this.isAdmin = true) : '';
        }
      }
    });
  }

  /** fetch videos */
  getVideo = async () => {
    try {
      const encodeURI = `?query=${encodeURIComponent(
        JSON.stringify({ createdBy: this.currentUser._id })
      )}&pagination=${encodeURIComponent(JSON.stringify(this.paginationData))}`;

      this.videoLibraryDetails =
        await this.videoLibraryService.fetchVideoLibrary(API, encodeURI);

      this.showLoader = false;
    } catch (error) {
      this.showLoader = false;
      this.eSportsToastService.showError(
        error?.error?.message || error?.message
      );
    }
  };

  getArticle() {
    let params = new HttpParams();
    params = params.append(
      'query',
      JSON.stringify({ author: this.currentUser._id })
    );
    params = params.append('sort', JSON.stringify({ createdDate: -1 }));
    this.profileService.getContent(params).subscribe(
      (res) => {
        this.articles = res.data;
        this.showLoader = false;
        this.slicesArticles = this.articles.slice(0, this.pageSizeOptions[0]);
      },
      (err) => {
        this.showLoader = false;
      }
    );
  }

  onPageChange(event): void {
    const startIndex = event.pageIndex * event.pageSize;
    let endIndex = startIndex + event.pageSize;
    if (endIndex > this.articles.length) {
      endIndex = this.articles.length;
    }
    this.slicesArticles = this.articles.slice(startIndex, endIndex);
  }

  videoPageChanged(page): void {
    this.paginationData.page = page.pageIndex + 1;
    this.paginationData.limit = page.pageSize;
    this.getVideo();
  }

  navChanges = (e) => {
    this.nextId = e.nextId;
    switch (this.nextId) {
      case 1:
        this.getArticle();
        break;
      case 2: {
        this.paginationData.page = 1;
        this.paginationData.limit = 10;
        this.getVideo();
        break;
      }
      default:
        break;
    }
  };
}
