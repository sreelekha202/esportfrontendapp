import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { NgCircleProgressModule } from 'ng-circle-progress';
import { MatExpansionModule } from '@angular/material/expansion';
import { CoreModule } from '../../core/core.module';
import { ProfileRoutingModule } from './profile-routing.module';
import { MaterialModule } from '../../shared/modules/material.module';
import { RouterBackModule } from '../../shared/directives/router-back.module';
import { BookmarksComponent } from './bookmarks/bookmarks.component';
import { ContentComponent } from './content/content.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { InboxComponent } from './inbox/inbox.component';
import { InboxMessageComponent } from './inbox-message/inbox-message.component';
import { MatchesComponent } from './matches/matches.component';
import { MyStatsComponent } from './my-stats/my-stats.component';
import { MyTeamsComponent } from './my-teams/my-teams.component';
import { ProfileComponent } from './profile.component';
import { TournamentsCreatedComponent } from './tournaments-created/tournaments-created.component';
import { TournamentsJoinedComponent } from './tournaments-joined/tournaments-joined.component';
import { TransactionsComponent } from './transactions/transactions.component';
import { BookmarkCardComponent } from './components/bookmark-card/bookmark-card.component';
import { BookmarksTableComponent } from './components/bookmarks-table/bookmarks-table.component';
import { MatchCardComponent } from './components/match-card/match-card.component';
import { MatchesTableComponent } from './components/matches-table/matches-table.component';
import { TeamCardComponent } from './components/team-card/team-card.component';
import { TournamentCardComponent } from './components/tournament-card/tournament-card.component';
import { TournamentsTableComponent } from './components/tournaments-table/tournaments-table.component';
import { TransactionsTableComponent } from './components/transactions-table/transactions-table.component';
import { UploadImageModule } from '../../shared/components/upload-image/upload-image.module';
import { SharedModule } from '../../shared/modules/shared.module';
import { S3UploadService } from '../../core/service';
import { CommonModule } from '@angular/common';
import { CreateVideoLibraryComponent } from './create-video-library/create-video-library.component';
// import { MbscModule } from '@mobiscroll/angular';

const modules = [
  CommonModule,
  CoreModule,
  MaterialModule,
  MatExpansionModule,
  NgCircleProgressModule.forRoot({}),
  ProfileRoutingModule,
  RouterBackModule,
  SharedModule,
  FormsModule,
  ReactiveFormsModule,
  UploadImageModule,
  // MbscModule,
];

@NgModule({
  declarations: [
    BookmarkCardComponent,
    BookmarksComponent,
    BookmarksTableComponent,
    ContentComponent,
    DashboardComponent,
    InboxComponent,
    InboxMessageComponent,
    MatchCardComponent,
    MatchesComponent,
    MatchesTableComponent,
    MyStatsComponent,
    MyTeamsComponent,
    ProfileComponent,
    TeamCardComponent,
    TournamentCardComponent,
    TournamentsCreatedComponent,
    TournamentsJoinedComponent,
    TournamentsTableComponent,
    TransactionsComponent,
    TransactionsTableComponent,
    CreateVideoLibraryComponent
  ],
  imports: modules,
  providers: [
    S3UploadService
  ]
})
export class ProfileModule { }
