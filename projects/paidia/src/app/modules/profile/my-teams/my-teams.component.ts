import { ProfileService } from './../../../core/service/profile.service';
import { Component, OnInit, Input } from '@angular/core';
import { environment } from '../../../../environments/environment';
import {
  EsportsToastService,
  EsportsGtmService,
  EventProperties,
} from 'esports';
import { Router } from '@angular/router';
@Component({
  selector: 'app-my-teams',
  templateUrl: './my-teams.component.html',
  styleUrls: ['./my-teams.component.scss'],
})
export class MyTeamsComponent implements OnInit {
  active = 1;
  nextId = 1;
  myTeams = [];
  teamRequests = [];
  showLoader: boolean = true;

  paginationDetails: any;
  pageSizeOptions = environment.pageSizeOptions;
  paginationData = {
    page: 1,
    limit: 10,
    sort: { _id: -1 },
    text: '',
  };

  constructor(
    private profileService: ProfileService,
    private toastService: EsportsToastService,
    private gtmService: EsportsGtmService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.getMyTeams();
    this.getMyInvite();
  }
  navChanges = (e) => {
    this.nextId = e.nextId;
    switch (this.nextId) {
      case 1:
        this.getMyTeams();
        break;
      case 2:
        this.pushGTMTags('View_Team_Invitation');
        this.getMyInvite();
        break;
    }
  };
  getMyInvite() {
    this.showLoader = true;
    const pagination = JSON.stringify(this.paginationData);
    this.profileService
      .getMyInvite({
        pagination: pagination,
        limit: this.paginationData.limit,
        page: this.paginationData.page,
      })
      .subscribe(
        (res) => {
          this.teamRequests = [];
          for (let d of res.data.docs) {
            let d1 = {
              image: d.logo,
              name: d.teamName,
              teamMemberCount: d?.teamMemberCount,
              createtdAt: d.createdOn,
              _id: d._id,
            };
            this.teamRequests.push(d1);
            this.paginationDetails = res.data;
            this.paginationData.limit = this.paginationDetails.limit;
            this.paginationData.page = this.paginationDetails.page;
          }
          this.showLoader = false;
          // this.teamRequests = res.data;
        },
        (err) => {
          this.showLoader = false;
        }
      );
  }
  getMyTeams() {
    this.showLoader = true;
    const pagination = JSON.stringify(this.paginationData);

    this.profileService
      .getMyTeam({
        pagination: pagination,
        limit: this.paginationData.limit,
        page: this.paginationData.page,
      })
      .subscribe(
        (res) => {
          this.myTeams = [];

          for (let d of res.data.docs) {
            let d1 = {
              image: d.logo,
              name: d.teamName,
              id: d._id,
              teamMemberCount: d?.teamMemberCount,
              createdOn: d.createdOn,
            };
            this.myTeams.push(d1);
            this.paginationDetails = res.data;
            this.paginationData.limit = this.paginationDetails.limit;
            this.paginationData.page = this.paginationDetails.page;
          }
          this.showLoader = false;
        },
        (err) => {
          this.showLoader = false;
        }
      );
  }

  acceptfunction(data) {
    this.pushGTMTags('Accept_Team_Invitation');
    const datatosend = {
      teamId: data._id,
      status: 'active',
    };
    this.profileService.accept_reject(datatosend).subscribe((data: any) => {
      this.toastService.showSuccess(data.message);
      // this.router.navigate(['manage-team/team-members'])
      this.getMyInvite();
    });
  }
  rejectfunction(data) {
    this.pushGTMTags('Reject_Team_Invitation');
    const datatosend = {
      teamId: data._id,
      status: 'rejected',
    };
    this.profileService.accept_reject(datatosend).subscribe((data: any) => {
      this.toastService.showSuccess(data.message);
      this.getMyInvite();
    });
  }

  pageChanged(page): void {
    if (this.paginationDetails?.totalDocs > 0) {
      this.paginationData.page = page.pageIndex + 1;
      this.paginationData.limit = page.pageSize;
      this.navChanges({ nextId: this.nextId });
    }
  }
  resfresh(a) {
    this.getMyTeams();
  }

  pushGTMTags(eventName, eventData: EventProperties = null) {
    this.gtmService.pushGTMTags(eventName, eventData);
  }
}
