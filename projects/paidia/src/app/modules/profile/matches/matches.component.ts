import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import {
  EsportsUserService,
  EsportsGtmService,
} from 'esports';
import { environment } from '../../../../environments/environment';
import { ProfileService } from './../../../core/service/profile.service';
import { ActivatedRoute } from '@angular/router';
const API = environment.apiEndPoint;

@Component({
  selector: 'app-matches',
  templateUrl: './matches.component.html',
  styleUrls: ['./matches.component.scss'],
})
export class MatchesComponent implements OnInit, OnDestroy {
  mock_cards = [];
  isLoaded = false;
  isAdmin = false;
  userSubscription: Subscription;
  active = 1;
  nextId = 1;
  tab = ['current', 'completed'];
  paginationData = {
    page: 1,
    limit: 10,
    sort: 'startDate',
  };
  paginationDetails: any;
  pageSizeOptions = environment.pageSizeOptions;

  constructor(
    private userService: EsportsUserService,
    private profileService: ProfileService,
    private gtmService: EsportsGtmService,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.getCurrentUserDetails();
    if (this.activatedRoute?.snapshot?.params?.activeTab == 2) {
      this.active = 2;
      // this.tabChanged({ nextId: 2 })
    } else {
    }
    this.pushViewMatchesTag();
    this.getMyMatches();
  }

  ngOnDestroy() {
    this.userSubscription?.unsubscribe();
  }

  tabChanged = (e) => {
    this.nextId = e.nextId;
    this.paginationData = { page: 1, limit: 10, sort: 'startDate' };
    this.pushViewMatchesTag();
    this.getMyMatches();
  };

  getCurrentUserDetails() {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        if (data?.accountType === 'admin') {
          this.isAdmin = true;
        }
      }
    });
  }

  getMyMatches() {
    this.isLoaded = true;
    const params = {
      status: this.tab[this.nextId - 1],
      limit: this.paginationData?.limit,
      page: this.paginationData?.page,
      includeBattleRoyale: true,
    };
    this.profileService.getMatchev2(params).subscribe(
      (res) => {
        this.mock_cards = [];
        this.isLoaded = false;
        const data = res?.data?.docs;
        this.paginationDetails = res?.data;
        this.paginationData.limit = this.paginationDetails?.limit;
        this.paginationData.page = this.paginationDetails?.page;
        let a = [];
        for (let index = 0, len = data.length; index < len; index++) {
          const element = data[index];
          element?.match?.currentMatch?.round &&
            element?.opponentPlayer?.teamName;
          a.push({
            title: element?.match?.currentMatch?.round
              ? `Round ${element?.match?.currentMatch?.round}`
              : '',
            opponentName: element?.opponentPlayer?.teamName || 'N/A',
            gameAccount: element?.opponentPlayer?.inGamerUserId || 'N/A',
            image: element?.tournament?.gameDetail?.logo || 'N/A',
            tournament: element?.tournament?._id || '',
            tournaments: element?.tournament,
            tournamentSlug: element?.tournament?.slug || '',
            matchId: element?.match?._id || '',
            participantId: element?.userParticepentId || '',
            match: element?.match,
            isAdmin: this.isAdmin,
            tournamentName: element?.tournament?.name
              ? element?.tournament?.name.length > 28
                ? `${element?.tournament?.name
                    .toLowerCase()
                    .substring(0, 28)
                    .slice(0, -3)}...`
                : element?.tournament?.name.toLowerCase()
              : '',
            credentials: element?.credentials
          });
        }
        this.mock_cards = a;
      },
      (err) => {
        this.isLoaded = false;
      }
    );
  }

  pageChanged(page): void {
    this.paginationData.page = page.pageIndex + 1;
    this.paginationData.limit = page.pageSize;
    this.getMyMatches();
  }

  pushViewMatchesTag() {
    switch (this.tab[this.nextId - 1]) {
      case 'current':
        this.pushGTMTags('My_Matches_Current_Tab_Clicked');
        break;
      case 'completed':
        this.pushGTMTags('My_Matches_Upcoming_Tab_Clicked');
        break;

      default:
        break;
    }
  }

  pushGTMTags(eventName: string, eventData = null) {
    this.gtmService.pushGTMTags(eventName, eventData);
  }
}
