import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { MessageService } from '../../../core/service';
import { Message } from '@angular/compiler/src/i18n/i18n_ast';
import { MatTableDataSource } from '@angular/material/table';
@Component({
  selector: 'app-inbox-message',
  templateUrl: './inbox-message.component.html',
  styleUrls: ['./inbox-message.component.scss'],
})
export class InboxMessageComponent implements OnInit {
  showLoader: boolean = true;

  message = null;
  messages: Array<any> = [];
  selectedItems = [];
  messagesPerPage = [10, 15, 25];
  slicesMessages = [];
  dataSource: any;
  constructor(
    private location: Location,
    private messageService: MessageService,
    private route: ActivatedRoute,

  ) {}

  ngOnInit(): void {
    this.getMessage(this.route.snapshot.params.id);
    this.getMessages();
  }

  getMessage(messageId: string): void {
    this.messageService.getMessage(messageId).subscribe(
      (response) => {
        this.message = response.data[0];
        this.showLoader = false;
      },
      (error) => {
        this.showLoader = false;
      }
    );
  }

  goBack() {
    this.location.back();
  }

  getMessages(): void {
    this.messageService.getInboxMessages().subscribe(
      (response) => {
        this.messages = response.data;
        this.slicesMessages = this.messages.slice(0, this.messagesPerPage[0]);
        this.showLoader = false;
        this.messageService.messageCountSubject.next(this.messages.length)
        this.dataSource = new MatTableDataSource(this.messages);
      },
      (error) => {
        this.showLoader = false;
      }
    );
  }

  deletemessages(message): void {
    this.messageService.deleteMultipleMessage(message).subscribe(
      (response) => {
        this.messages = response.data;
        // this.slicesMessages = this.messages.splice(0, this.messagesPerPage[0]);
        this.showLoader = false;
        this.messageService.messageCountSubject.next(this.messages.length)
        this.dataSource = new MatTableDataSource(this.messages);
        this.goBack();
        this.getMessages();
      },
      (error) => {
        this.showLoader = false;
      }
    );
  }
}
