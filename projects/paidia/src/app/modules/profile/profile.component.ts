import { Component, OnInit, OnDestroy } from '@angular/core';
import { Location } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { AppHtmlProfileRoutes } from '../../app-routing.model';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';
import { TranslateService } from '@ngx-translate/core';
import { MessageService } from '../../core/service';
import {
  EsportsLanguageService,
  GlobalUtils,
  EsportsGtmService,
  EventProperties,
} from 'esports';
import { environment } from '../../../environments/environment';
import { MatDialog } from '@angular/material/dialog';
import { SettingsLightboxComponent } from '../settings-lightbox/settings-lightbox.component';
AutoUnsubscribe();
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
})
export class ProfileComponent implements OnInit, OnDestroy {
  AppHtmlProfileRoutes = AppHtmlProfileRoutes;
  msgCount: any;

  constructor(
    private languageService: EsportsLanguageService,
    private translateService: TranslateService,
    private messageService: MessageService,
    public location: Location,
    public router: Router,
    public matDialog: MatDialog,
    private gtmService: EsportsGtmService,
    private route: ActivatedRoute
  ) {
    if (
      GlobalUtils.isBrowser() &&
      !localStorage.getItem(environment.currentToken)
    ) {
      this.router.navigate(['/login']);
    }
  }

  ngOnInit(): void {
    this.getMessages();
    this.languageService.language.subscribe((lang) =>
      this.translateService.use(lang || 'en')
    );
    this.messageService.messageCount.subscribe((count) => {
      this.msgCount = count;
    });
  }

  openSettingsLightbox(selectedTab = 0) {
    switch (selectedTab) {
      case 0:
        this.pushGTMTags('Account_Tab_Click');
        break;

      default:
        break;
    }
    const data = { selectedTab };
    this.matDialog.open(SettingsLightboxComponent, {
      data,
      position: {
        top: '34px',
      },
    });
  }

  getMessages(): void {
    this.messageService.getInboxMessages().subscribe(
      (response) => {
        this.messageService.messageCountSubject.next(response.data.length);
      },
      (error) => {}
    );
  }

  ngOnDestroy(): void {}

  pushProfileNavTags(eventName){
    this.pushGTMTags(eventName);
  }

  pushGTMTags(eventName, eventData: EventProperties = null){
    this.gtmService.pushGTMTags(eventName, eventData);
  }

}
