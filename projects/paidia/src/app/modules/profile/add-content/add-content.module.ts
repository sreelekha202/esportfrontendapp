import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { AddContentComponent } from './add-content.component';
import { ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../../../shared/modules/material.module';
import { WYSIWYGEditorModule } from 'esports';
import { TeamIconComponent } from './components/team-icon/team-icon.component';
import { SharedModule } from '../../../shared/modules/shared.module';
import { CoreModule } from '../../../core/core.module';
import { environment } from '../../../../environments/environment';

const routes: Routes = [
  {
    path: '',
    component: AddContentComponent,
  }
]
@NgModule({
  declarations: [AddContentComponent, TeamIconComponent],
  imports: [
    CommonModule,
    WYSIWYGEditorModule.forRoot(environment),
    CoreModule,
    MaterialModule,
    SharedModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes)
  ]
})
export class AddContentModule { }
