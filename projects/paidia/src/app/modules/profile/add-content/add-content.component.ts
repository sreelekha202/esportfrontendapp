
import { Router } from '@angular/router';
import { EsportsToastService } from 'esports';
import { EsportsUserService } from 'esports';
import { ProfileService } from './../../../core/service/profile.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'app-add-content',
  templateUrl: './add-content.component.html',
  styleUrls: ['./add-content.component.scss'],
})
export class AddContentComponent implements OnInit {
  imgurl = '';
  form: FormGroup;
  addForm: FormGroup;
  stateList: any = [];
  categoryList: any = [];
  gameList: any = [];
  tagList: any = [];
  platformList: any = [];
  genreList: any = [];
  data = '';
  articleStatus = ['draft', 'submitted_for_approval'];
  minReadList: any = [
    { _id: 2, name: '2 Minutes' },
    { _id: 3, name: '3 Minutes' },
    { _id: 4, name: '4 Minutes' },
    { _id: 5, name: '5 Minutes' },
    { _id: 6, name: '6 Minutes' },
    { _id: 7, name: '7 Minutes' },
  ];
  showLoader: boolean;
  currentUser: any;
  loader: any;
  editorConfig: any;
  ckConfig: {
    toolbar: { items: string[]; shouldNotGroupWhenFull: boolean };
    heading: {
      options: (
        | { model: string; title: string; class: string; view?: undefined }
        | { model: string; view: string; title: string; class: string }
      )[];
    };
    mediaEmbed: { previewsInData: boolean };
  };

  constructor(
    private fb: FormBuilder,
    private profileService: ProfileService,
    private userService: EsportsUserService,
    private toastService: EsportsToastService,
    private router: Router
  ) {
    //this.Editor.execute( 'fontColor', { value: 'rgb(30, 188, 97)' } );
    this.addForm = this.fb.group({
      title: [null, Validators.compose([Validators.required])],
      category: [null, Validators.compose([Validators.required])],
      gameDetails: [null, Validators.compose([Validators.required])],
      minRead: [null, Validators.compose([Validators.required])],
      tags: [null],
      platform: [null],
      genre: [null],
      shortDescription: [null, Validators.compose([Validators.required])],
      content: [''],
      image: [null, Validators.compose([Validators.required])],
      author: [null],
      game: [null],
      articleStatus: [''],
      comment: [''],
    });
  }

  ngOnInit(): void {
    this.userService.currentUser.subscribe((userdata) => {
      if (userdata) {
        this.currentUser = userdata;
        this.addForm.patchValue({ author: this.currentUser._id });
      }
    });
    this.getAllCategories();
    this.getAllGenres();
    this.getAllTags();
    this.getAllGame();

    // this.ckConfig = {
    //   toolbar: {
    //     items: [
    //       'heading', '|',
    //       'fontfamily', 'fontsize', '|',
    //       'alignment', '|',
    //       'fontColor', 'fontBackgroundColor', '|',
    //       'bold', 'italic', 'strikethrough', 'underline', 'subscript', 'superscript', '|',
    //       'link', '|',
    //       'outdent', 'indent', '|',
    //       'bulletedList', 'numberedList', 'todoList', '|',
    //       'code', 'codeBlock', '|',
    //       'insertTable', '|',
    //       'uploadImage', 'blockQuote', '|',
    //       'undo', 'redo',
    //       'mediaEmbed'
    //     ],
    //     shouldNotGroupWhenFull: true
    //   },
    //   heading: {
    //     options: [
    //       { model: 'paragraph', title: 'Paragraph', class: 'ck-heading_paragraph' },
    //       { model: 'heading1', view: 'h1', title: 'Heading 1', class: 'ck-heading_heading1' },
    //       { model: 'heading2', view: 'h2', title: 'Heading 2', class: 'ck-heading_heading2' }
    //     ]
    //   },
    //   mediaEmbed: {
    //     previewsInData: true
    //   }
    // }
    this.editorConfig = {
      editable: true,
      spellcheck: true,
      height: '180px',
      width: 'auto',
      minWidth: '0',
      translate: 'no',
      defaultParagraphSeparator: 'p',
      defaultFontName: 'Arial',
      fonts: [
        {class: 'Arial', name: 'Arial'},
        {class: 'redhat-regular', name: 'redhat-regular'},
        {class: 'redhat-bold', name: 'redhat-bold'},
        {class: 'times-new-roman', name: 'Times New Roman'},
        {class: 'calibri', name: 'Calibri'},
        {class: 'comic-sans-ms', name: 'Comic Sans MS'}
      ],
      customClasses: [
        {
          name: 'quote',
          class: 'quote',
        },
        {
          name: 'redText',
          class: 'redText',
        },
        {
          name: 'titleText',
          class: 'titleText',
          tag: 'h1',
        },
      ],
      sanitize: false,
      toolbarPosition: 'top',
      toolbarHiddenButtons: [['bold', 'italic'], ['fontSize']],
    };
  }

  getAllCategories() {
    this.profileService.getAllCategories().subscribe((res) => {
      if (res) this.categoryList = res.data;
    });
  }
  getAllTags() {
    this.profileService.getAllTags().subscribe((res) => {
      if (res) this.tagList = res.data;
    });
  }
  getAllGenres() {
    this.profileService.getAllGenres().subscribe((res) => {
      if (res) this.genreList = res.data;
    });
  }
  getAllGame() {
    this.profileService.getAllGame().subscribe((res) => {
      if (res) this.gameList = res.data;
    });
  }
  onChangeGame() {
    this.gameList.map((res) => {
      if (this.addForm.value.gameDetails == res._id) {
        this.platformList = res.platform;
      }
    });
  }
  onSubmit(type = null) {
    if (this.addForm.value.gameDetails) {
      this.gameList.map((g) => {
        if (g._id == this.addForm.value.gameDetails) {
          this.addForm.patchValue({ game: g.name });
        }
      });
    }
    if (type == this.articleStatus[0]) {
      this.addForm.patchValue({ articleStatus: this.articleStatus[0] });
    }
    if (type == this.articleStatus[1]) {
      this.addForm.patchValue({ articleStatus: this.articleStatus[1] });
    }
    if (this.addForm.valid) {
      this.showLoader = true;
      let data = {
        ...this.addForm.value,
        image: this.addForm.value.image.data[0].Location,
        shortDescription: { english: this.addForm.value.shortDescription },
        title: { english: String(this.addForm.value.title) },
        comment: { english: String(this.addForm.value.comment) },
        content: { english: String(this.addForm.value.content) },
      };
      this.profileService.saveContent(data).subscribe(
        (res) => {
          this.showLoader = false;
          this.toastService.showSuccess(res.message);
          this.router.navigateByUrl('/profile/content');
        },
        (err) => {
          this.showLoader = false;
        }
      );
    }
  }

  onReady(eventData) {
    eventData.plugins.get('FileRepository').createUploadAdapter = function (
      loader
    ) {
      return new UploadAdapter(loader);
    };
  }
}

class UploadAdapter {
  loader: any;
  constructor(loader) {
    this.loader = loader;
  }

  upload() {
    return this.loader.file.then(
      (file) =>
        new Promise((resolve, reject) => {
          var myReader = new FileReader();
          myReader.onloadend = (e) => {
            const imageData: any = {
              path: environment.articleS3BucketName,
              files: [myReader.result],
            };

            // resolve({ default: myReader.result });
            const accessToken = localStorage.getItem(environment.currentToken);

            fetch(`${environment.apiEndPoint}file-upload`, {
              method: 'POST',
              headers: {
                authorization: `Bearer ${accessToken}`,
                'Access-Control-Allow-Origin': '*',
              },
              body: JSON.stringify(imageData), // This is your file object
            })
              .then(
                (response) => response.json() // if the response is a JSON object
              )
              .then(
                (success) => {
                  resolve({ default: success.data[0]['Location'] });
                } // Handle the success response object
              )
              .catch(
                (error) => {} // Handle the error response object
              );
          };

          myReader.readAsDataURL(file);
        })
    );
  }
}
