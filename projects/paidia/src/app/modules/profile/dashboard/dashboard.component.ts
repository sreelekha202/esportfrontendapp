import { ProfileService } from './../../../core/service/profile.service';
import { Component, OnInit } from '@angular/core';
import { IUser} from 'esports';
import { EsportsUserService} from 'esports';
import { Subscription } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { SettingsLightboxComponent } from '../../settings-lightbox/settings-lightbox.component';
import { AppHtmlProfileRoutes } from '../../../app-routing.model';
// import { setOptions,  MbscEventcalendarView,  MbscCalendarEvent  } from '@mobiscroll/angular';

// setOptions({
//   theme: 'ios',
//   themeVariant: 'light',
//   clickToCreate: false,
//   dragToCreate: true,
//   dragToMove: true,
//   dragToResize: true
// });

// const now = new Date();
// const day = now.getDay();
// const monday = now.getDate() - day + (day == 0 ? -6 : 1);

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {
  AppHtmlProfileRoutes = AppHtmlProfileRoutes;
  hasDataBg = 'assets/images/Profile/dashboard/has-data-bg.jpg';
  noDataBg = 'assets/images/Profile/dashboard/no-data-bg.jpg';
  matches = [];
  currentUser: IUser;
  userSubscription: Subscription;
  country: any;
  state: any;
  showLoader: boolean;


  constructor(
    private userService: EsportsUserService,
    private profileService :ProfileService,
    public matDialog: MatDialog) { }

  ngOnInit(): void {
    this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.currentUser = data;
        this.getCountry();
        this.getState();
      }
    });
    this.getMatches();
  }

  getCountry() {
    this.userService.getAllCountries().subscribe((res) => {
      if (res && res.countries) {
        res.countries.filter((obj) => {
          if (obj.id == this.currentUser.country) {
            this.country = obj;
          }
        })
      }
    })
  }

  getState() {
    this.userService.getStates().subscribe((res) => {
      if (res && res.states) {
        res.states.filter((obj) => {
          if (obj.id == this.currentUser.state) {
            this.state = obj;
          }
        })
      }
    })
  }

  isUserContainData(): boolean {
    const {
      profilePicture: picture,
      preference: { followersCount: folowers, followingCount: following },
    } = this.currentUser;

    return picture && folowers && following;
  }

  openSettingsLightbox(selectedTab = 0) {
    const data = { selectedTab, };
    this.matDialog.open(SettingsLightboxComponent, {
      data,
      position: {
        top: '34px',
      },
    });
  }

  getMatches() {
    this.showLoader = true;
    const params: any = { page: 1, limit: 1 };
    params.status = 'completed';
    this.profileService.getMatchev2(params).subscribe(
      (res) => {
        let a = [];
        for (let index = 0, len = res.data.docs.length; index < len; index++) {
          const element = res.data.docs[index];
          element?.match?.currentMatch?.round &&
            element?.opponentPlayer?.teamName
          a.push({
            title: element?.match?.currentMatch?.round
              ? `Round ${element?.match?.currentMatch?.round}`
              : 'N/A',
            opponentName: element?.opponentPlayer?.teamName || 'N/A',
            gameAccount: element?.opponentPlayer?.inGamerUserId || 'N/A',
            image: element?.tournament?.gameDetail?.logo || 'N/A',
            tournament: element?.tournament?._id || '',
            tournamentSlug: element?.tournament?.slug || '',
            matchId: element?.match?._id || '',
            participantId: element?.userParticepentId || '',
            match: element?.match,
            tournamentName: element?.tournament?.name
              ? element?.tournament?.name.length > 28
                ? `${element?.tournament?.name
                  .toLowerCase()
                  .substring(0, 28)
                  .slice(0, -3)}...`
                : element?.tournament?.name.toLowerCase()
              : '',
            credentials: element?.credentials
          });
        }
        this.matches=a;
      },
      (err) => {
        this.showLoader = false;
      }
    );
  }

}
