import { ProfileService } from './../../../core/service/profile.service';
import { Component, OnInit } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { EsportsGtmService, EventProperties } from 'esports';
import { Router } from '@angular/router';
@Component({
  selector: 'app-tournaments-created',
  templateUrl: './tournaments-created.component.html',
  styleUrls: ['./tournaments-created.component.scss'],
})
export class TournamentsCreatedComponent implements OnInit {
  tournaments: any = [];
  showLoader: boolean = true;
  active = 1;
  nextId = 1;
  paginationDetails: any;
  pageSizeOptions = environment.pageSizeOptions;
  paginationData = {
    page: 1,
    limit: 10,
    sort: 'startDate',
  };
  constructor(
    private profileService: ProfileService,
    private gtmService: EsportsGtmService,
    private router: Router
  ) {}
  ngOnInit(): void {
    this.getTournaments();
    this.pushViewTournamentsTags();
  }
  navChanges = (e) => {
    this.nextId = e.nextId;
    this.pushViewTournamentsTags();
    this.getTournaments();
  };
  getTournaments() {
    this.showLoader = true;
    const params: any = {
      limit: this.paginationData.limit,
      page: this.paginationData.page,
      status: this.nextId,
    };
    // params.limit = this.paginationData.limit;
    // params.page = this.paginationData.page;
    // params.status = this.nextId;
    this.profileService.getMyCreatedTournament(params).subscribe(
      (res) => {
        this.tournaments = res.data.docs;
        this.paginationDetails = res.data;
        this.paginationData.limit = this.paginationDetails.limit;
        this.paginationData.page = this.paginationDetails.page;
        this.showLoader = false;
      },
      (err) => {
        this.showLoader = false;
      }
    );
  }
  pageChanged(page): void {
    if (this.paginationDetails?.totalDocs > 0) {
      this.paginationData.page = page.pageIndex + 1;
      this.paginationData.limit = page.pageSize;
      this.navChanges({ nextId: this.nextId });
    }
  }

  pushViewTournamentsTags() {
    switch (+this.nextId) {
      case 0:
        this.pushGTMTags('View_Upcoming_Tournament', {
          fromPage: this.router.url,
        });
        break;
      case 1:
        this.pushGTMTags('View_Ongoing_Tournament', {
          fromPage: this.router.url,
        });
        break;
      case 2:
        this.pushGTMTags('View_Past_Tournament ', {
          fromPage: this.router.url,
        });
        break;
      default:
        break;
    }
  }

  pushGTMTags(eventName: string, eventData: EventProperties = null) {
    this.gtmService.pushGTMTags(eventName, eventData);
  }
}
