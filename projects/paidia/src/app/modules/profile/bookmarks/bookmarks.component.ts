import { EsportsUserService } from 'esports';
import { ProfileService } from './../../../core/service/profile.service';
import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { IUser, EsportsGtmService } from 'esports';
import { HttpParams } from '@angular/common/http';

@Component({
  selector: 'app-bookmarks',
  templateUrl: './bookmarks.component.html',
  styleUrls: ['./bookmarks.component.scss'],
})
export class BookmarksComponent implements OnInit {
  articles = [];
  videos = [];
  active = 1;
  userSubscription: Subscription;
  currentUser: IUser;
  showLoader: boolean = true;
  articleItems = [];

  constructor(
    private userService: EsportsUserService,
    private gtmService: EsportsGtmService,
    private profileService: ProfileService
  ) {}

  ngOnInit(): void {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.currentUser = data;
        this.getBookmarks();
        this.onNavChange(null);
      }
    });
  }
  getBookmarks() {
    this.showLoader = true;

    let params = new HttpParams();
    params = params.append(
      'query',
      JSON.stringify({ userId: this.currentUser._id })
    );
    params = params.append('select', 'articleBookmarks,videoLibrary');

    this.profileService.getBookmarks(params).subscribe(
      (res) => {
        this.videos = res.data[0].videoLibrary;
        this.articles = res.data[0].articleBookmarks;
        this.showLoader = false;
      },
      (err) => {
        this.showLoader = false;
      }
    );
  }

  onNavChange($event) {
    switch ($event?.nextId) {
      case 1:
        this.gtmService.pushGTMTags('View_Article_Bookmarks');
        break;
      case 1:
        this.gtmService.pushGTMTags('View_Video_Bookmarks');
        break;
      default:
        this.gtmService.pushGTMTags('View_Article_Bookmarks');
        break;
    }
  }
}
