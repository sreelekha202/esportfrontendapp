import { ProfileService } from './../../../core/service/profile.service';
import { Component, OnInit } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { EsportsGtmService } from "esports";

@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.scss'],
})
export class TransactionsComponent implements OnInit {
  purchaseHistory: any = [];
  rewardseHistory: any;
  active = 1;
  nextId: number = 1;
  pagination: any;
  paginationParams: any;
  showLoader: boolean = true;

  paginationData = {
    page: 1,
    limit: 10,
    sort: 'startDate',
  };
  paginationDetails: any;
  pageSizeOptions = environment.pageSizeOptions;
  constructor(private profileService: ProfileService, private gtmService: EsportsGtmService) {}

  ngOnInit(): void {
    this.getPurchaseHistory();
  }
  testingarray: any = [];

  navChanges = (e) => {
    this.nextId = e.nextId;
    switch (this.nextId) {
      case 1:
        this.gtmService.pushGTMTags('View_Purchase_History_Section');
        this.getPurchaseHistory();
        break;
      case 2:
        this.gtmService.pushGTMTags('View_Rewards_History_Section');
        this.getRewardseHistory();
        break;
    }
  };

  getPurchaseHistory() {
    this.showLoader = true;
    const params: any = {
      limit: this.paginationData?.limit,
      page: this.paginationData?.page,
    };
    this.paginationParams ? (params.pagination = this.paginationParams) : '';
    this.profileService.getPurchaseHistory(params).subscribe(
      (res) => {
        this.pagination = res.data;
        this.purchaseHistory =
          res.data && res.data.docs.length > 0 ? res.data.docs : '';
        this.paginationDetails = res.data;
        this.paginationData.limit = this.paginationDetails.limit;
        this.paginationData.page = this.paginationDetails.page;
        this.showLoader = false;
      },
      (err) => {
        this.showLoader = false;
      }
    );
  }

  getRewardseHistory() {
    this.showLoader = true;
    const params: any = {
      limit: this.paginationData.limit,
      page: this.paginationData.page,
    };
    this.paginationParams ? (params.pagination = this.paginationParams) : '';
    this.profileService.getRewardseHistory(params).subscribe(
      (res) => {
        this.pagination = res.data;
        this.paginationDetails = res.data;
        this.paginationData.limit = this.paginationDetails.limit;
        this.paginationData.page = this.paginationDetails.page;
        this.rewardseHistory =
          res.data && res.data.docs.length > 0 ? res.data.docs : '';
        this.showLoader = false;
      },
      (err) => {
        this.showLoader = false;
      }
    );
  }

  onPageChanges(event) {
    this.getPurchaseHistory();
  }

  pageChanged(page): void {
    this.paginationData.page = page.pageIndex + 1;
    this.paginationData.limit = page.pageSize;
    this.navChanges({ nextId: this.nextId });
  }
}
