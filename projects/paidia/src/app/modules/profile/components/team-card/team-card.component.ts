import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { TeamService } from 'projects/paidia/src/app/core/service/team.service';
import {
  EsportsToastService,
  EsportsGtmService,
  EventProperties,
} from 'esports';

@Component({
  selector: 'app-team-card',
  templateUrl: './team-card.component.html',
  styleUrls: ['./team-card.component.scss'],
})
export class TeamCardComponent implements OnInit {
  @Input() isManage: boolean = false;
  @Input() data;
  @Output() refresh = new EventEmitter();
  myTeams: '/my-teams'[];

  constructor(
    private router: Router,
    private teamService: TeamService,
    private gtmService: EsportsGtmService,
    private eSportsToastService: EsportsToastService
  ) {}

  ngOnInit(): void {}

  deleteTeam(team_id) {
    this.pushGTMTags('Delete_Team');
    const data = {
      id: team_id,
      admin: true,
      query: {
        condition: { _id: team_id, status: 'active' },
        update: { status: 'inactive' },
      },
    };
    this.teamService
      .deleteTeam(data)
      .then((data: any) => {
        if (data) {
          let res = JSON.stringify(data);
          this.refresh.emit(data);
          this.eSportsToastService.showSuccess(data.message);
          this.router.navigate(['profile/my-teams']);
        }
      })
      .catch((error) => {});
  }

  pushGTMTags(eventName, eventData: EventProperties = null) {
    this.gtmService.pushGTMTags(eventName, eventData);
  }
}
