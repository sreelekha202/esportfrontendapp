import { Component, OnInit, Input, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';
import { TournamentService } from '../../../../core/service';
import { AppHtmlRoutes } from '../../../../app-routing.model';
import { EsportsGtmService, EventProperties } from 'esports';

@Component({
  selector: 'app-match-card',
  templateUrl: './match-card.component.html',
  styleUrls: ['./match-card.component.scss'],
})
export class MatchCardComponent implements OnInit {
  @Input() data: any;
  @Input() isEditable: boolean = false;
  @Input() index: number;
  AppHtmlRoutes = AppHtmlRoutes;
  isBattleRoyale = false;
  isCSGO = false;

  constructor(
    private router: Router,
    private gtmService: EsportsGtmService,
    private tournamentService: TournamentService
  ) {}

  ngOnInit(): void {
    this.isBattleRoyale =
      this.data?.tournaments?.bracketType == 'battle_royale' ? true : false;

    this.isCSGO = this.data?.tournaments?.gameDetail?.name == 'CS:GO';
  }

  genrateReport() {
    this.gtmService.pushGTMTags('View_Report_Match_Issue');
    if (this.data?.tournament && this.data?.matchId) {
      localStorage.setItem('t_report', JSON.stringify(this.data));
      this.router.navigate([`/report-issue`], {
        queryParams: {
          matchId: this.data?.matchId,
          tournamentId: this.data?.tournament,
        },
      });
    }
  }

  async reportScore(data) {
    this.gtmService.pushGTMTags('Enter_Match_Result_Clicked');
        if (this.isBattleRoyale) {
          return;
        }

    if (!(data.match && data?.match?.seasonId)) {
      const participantData =
        await this.tournamentService.fetchParticipantRegistrationStatus(
          data.tournaments._id
        );
      data.participantId = participantData?.data?.participantId;
    }

    localStorage.setItem('t_report', JSON.stringify(data));
    this.router.navigate([`/report-score`]);
  }

  exitFromScoreCard(data) {}

  viewMatch(_matchId) {
    this.gtmService.pushGTMTags('View_Match_Page');
    this.router.navigate([`/get-match/game-lobby`], {
      queryParams: { matchId: _matchId },
    });
  }
}
