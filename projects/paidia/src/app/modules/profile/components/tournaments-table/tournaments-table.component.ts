import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-tournaments-table',
  templateUrl: './tournaments-table.component.html',
  styleUrls: ['./tournaments-table.component.scss'],
})
export class TournamentsTableComponent implements OnInit {
  @Input() tournaments = [];
  @Input() isCardManage: boolean = false;


  constructor() {}

  ngOnInit(): void {

  }

 
}
