import { Component, OnInit, Input } from '@angular/core';
import { EsportsGtmService } from 'esports';
@Component({
  selector: 'app-tournament-card',
  templateUrl: './tournament-card.component.html',
  styleUrls: ['./tournament-card.component.scss'],
})
export class TournamentCardComponent implements OnInit {
  @Input() data: any;
  @Input() isManage: boolean = false;

  constructor(private gtmService: EsportsGtmService) {}
  ngOnInit(): void {}
  function(data) {
    localStorage.setItem('tournamentDetails', JSON.stringify(data));
  }
  startManage() {
    this.gtmService.pushGTMTags('Manage_Tournaments_Started');
  }
}
