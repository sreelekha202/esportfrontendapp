import { Component, OnInit, Input } from '@angular/core';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
import { MatDialog } from '@angular/material/dialog';
import {
  InfoPopupComponent,
  InfoPopupComponentData,
  InfoPopupComponentType,
} from '../../../../shared/popups/info-popup/info-popup.component';
import { UserPreferenceService } from '../../../../core/service';
import {
  EsportsToastService,
  EsportsUserService,
  IUser,
  EsportsGtmService,
} from 'esports';
import { Subscription } from 'rxjs';
import { HttpParams } from '@angular/common/http';
import { ProfileService } from './../../../../core/service/profile.service';
import { IArticle } from '../../../../shared/models';
interface BookmarkCard {
  image: string;
  thumbnailUrl: any;
  title: any;
  shortDescription: any;
  description: any;
  createdDate: any;
  author?: string;
  views?: string;
  _id: string;
  game: string;
}

@Component({
  selector: 'app-bookmark-card',
  templateUrl: './bookmark-card.component.html',
  styleUrls: ['./bookmark-card.component.scss'],
})
export class BookmarkCardComponent implements OnInit {
  @Input() data: BookmarkCard;
  @Input() isVideo: boolean = false;
  currentLang: string = 'english';
  showLoader: boolean = true;
  currentUser: IUser;
  articles = [];
  videos = [];
  userSubscription: Subscription;
  article: IArticle;

  constructor(
    private translateService: TranslateService,
    public matDialog: MatDialog,
    private toastService: EsportsToastService,
    private userPreferenceService: UserPreferenceService,
    private profileService: ProfileService,
    private gtmService: EsportsGtmService,
    private userService: EsportsUserService
  ) {
    this.currentLang =
      translateService.currentLang == 'ms' ? 'malay' : 'english';

    translateService.onLangChange.subscribe((event: LangChangeEvent) => {
      this.currentLang = event.lang == 'ms' ? 'malay' : 'english';
    });
  }
  ngOnInit(): void {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.currentUser = data;
        this.getBookmarks();
      }
    });
  }
  openSocialShareComponent() {
    const data: InfoPopupComponentData = {
      title: this.translateService.instant('ARTICLE_POST.SHARE'),
      text: String(this.data?.title?.english),
      type: InfoPopupComponentType.socialSharing,
      cancelBtnText: 'Close',
    };

    this.matDialog.open(InfoPopupComponent, {
      data,
    });
  }

  removeBookmark = async (_id) => {
    try {
      if(this.isVideo) {
        this.gtmService.pushGTMTags('Delete_Video_Bookmark');
      } else {
        this.gtmService.pushGTMTags('Delete_Article_Bookmark');
      }
      const queryParam = `articleId=${_id}`;
      const bookmark = await this.userPreferenceService
        .removeBookmark(queryParam)
        .toPromise();
      this.toastService.showSuccess(bookmark?.message);
      this.getBookmarks();
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  getBookmarks() {
    this.showLoader = true;

    let params = new HttpParams();
    params = params.append(
      'query',
      JSON.stringify({ userId: this.currentUser._id })
    );
    params = params.append('select', 'articleBookmarks,videoLibrary');

    this.profileService.getBookmarks(params).subscribe(
      (res) => {
        this.videos = res.data[0].videoLibrary;
        this.articles = res.data[0].articleBookmarks;
        this.showLoader = false;
      },
      (err) => {
        this.showLoader = false;
      }
    );
  }
}
