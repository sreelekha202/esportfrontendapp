import { Component, Input, OnInit } from "@angular/core";
import { Location } from "@angular/common";
import { TranslateService } from "@ngx-translate/core";
import { AppHtmlTeamRegRoutes } from "../../../../app-routing.model";
import { TournamentService, UserService } from "../../../../core/service";
import { IUser } from "esports";
import { Router } from "@angular/router";
import { EsportsUserService, EsportsToastService, EsportsGtmService } from 'esports';
@Component({
  selector: "app-select-teammates",
  templateUrl: "./select-teammates.component.html",
  styleUrls: ["./select-teammates.component.scss"],
})
export class SelectTeammatesComponent implements OnInit {
  AppHtmlTeamRegRoutes = AppHtmlTeamRegRoutes;

  user: IUser;
  @Input() tournament;
  @Input() team;
  @Input() members;
  isDisable: boolean = true;
  returnData: any = {};
  selected = [];

  mock_players = [
    {
      name: "Khalid Muhammad",
      photo: "assets/images/articles/pic1.jpg",
      isSelected: true,
    },
    {
      name: "Daniel Montaner",
      photo: "assets/images/articles/pic2.jpg",
      isSelected: false,
    },
    {
      name: "Patrik Lindberg",
      photo: "assets/images/articles/pic3.jpg",
      isSelected: false,
    },
    {
      name: "Christopher Alesund",
      photo: "assets/images/articles/pic1.jpg",
      isSelected: false,
    },
    {
      name: "Jordan Gilbert ",
      photo: "assets/images/articles/pic2.jpg",
      isSelected: false,
    },
    {
      name: "Oscar Torgersen",
      photo: "assets/images/articles/pic3.jpg",
      isSelected: false,
    },
    {
      name: "Fredrik Andersson	",
      photo: "assets/images/articles/pic1.jpg",
      isSelected: false,
    },
    {
      name: "Emil Christensen",
      photo: "assets/images/articles/pic2.jpg",
      isSelected: false,
    },
  ];

  constructor(
    private location: Location,
    private userService: EsportsUserService,
    private tournamentService: TournamentService,
    private router: Router,
    private toastService: EsportsToastService,
    private gtmService: EsportsGtmService,
    private translateService: TranslateService
  ) { }

  ngOnInit(): void { }
  ngAfterContentInit(): void {
    this.getCurrentUserDetails();
  }
  getCurrentUserDetails() {
    this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.user = data;
        this.membersFunction();
      }
    });
  }

  selectPlayer(playerIndex: number, player): void {
    if (player.userId._id !== this.user._id) {
      let flag = true;
      this.tournamentService
        .fetchParticipantRegistrationStatusMyTeams(
          this.tournament._id,
          player.userId._id
        )
        .subscribe(
          (data) => {
            this.toastService.showError(data?.message);
          },
          (err) => {
            let totalMemberSize = 0;
            let totalMemberSizeFalse = 0;
            if (this.tournament?.allowSubstituteMember) {
              if (this.tournament?.substituteMemberSize > 0) {
                totalMemberSize +=
                  this.tournament?.teamSize +
                  this.tournament?.substituteMemberSize;
              } else {
                totalMemberSize = this.tournament?.teamSize;
              }
            } else {
              totalMemberSize = this.tournament?.teamSize;
              totalMemberSizeFalse =
                this.tournament?.teamSize +
                this.tournament?.substituteMemberSize;
            }
            const indexSelected = this.selected.findIndex(
              (x) => x.teamParticipantId === player._id
            );
            if (indexSelected > -1) {
            } else {
              if (
                this.tournament?.allowSubstituteMember
                  ? this.selected.length >= totalMemberSize - 1
                  : this.selected.length >= totalMemberSizeFalse - 1
              ) {
                this.toastService.showError(
                  this.translateService.instant("PROFILE.TEAMS.ERRORS.ERROR8")
                );
                flag = false;
              } else {
              }
            }
            if (flag) {
              if (player.userId._id === this.user._id) {
              } else {
                this.members = this.members.map((player, idx) =>
                  playerIndex === idx
                    ? { ...player, isSelected: !player.isSelected }
                    : player
                );
                const index = this.selected.findIndex(
                  (x) => x.teamParticipantId === player._id
                );
                if (index > -1) {
                  this.selected.splice(index, 1);
                } else {
                  const userGames =
                    player?.userId?.preference?.gameDetails || [];
                  const game = userGames.find((game) => {
                    return game._id === this.tournament?.gameDetail?._id;
                  });
                  this.selected.push({
                    userId: player.userId._id,
                    name: player.userId?.username
                      ? player.userId?.username
                      : player.userId?.fullName,
                    phoneNumber: player.userId.phoneNumber,
                    email: player.userId.email,
                    teamParticipantId: player._id,
                    inGamerUserId: game?.userGameId || "",
                  });
                }
              }
              if (this.selected.length >= totalMemberSize - 1) {
                this.isDisable = false;
              } else {
                this.isDisable = true;
              }
            }
          }
        );
    }
  }

  membersFunction() {
    if (this.members) {
      const index = this.members?.findIndex(
        (x) => x.userId._id === this.user._id
      );

      const userGames = this.user?.preference?.gameDetails || [];
      const game = userGames.find((game) => {
        return game._id === this.tournament?.gameDetail?._id;
      });

      this.returnData = {
        teamId: this.team?._id,
        logo: this.team?.logo,
        teamName: this.team?.teamName,
        name: this.members[index].userId.username,
        phoneNumber: this.members[index].userId.phoneNumber,
        email: this.members[index].userId.email,
        inGamerUserId: game?.userGameId || "",
      };
      this.members[index].isSelected = true;
      //this.members.splice(index, 1);
    }
  }
  goBack() {
    this.gtmService.pushGTMTags('Cancel_Match_Teammates');
    this.location.back();
  }

  submitMember() {
    this.gtmService.pushGTMTags('Confirm_Match_Teammates');
    if (this.tournament?.substituteMemberSize > 0) {
      let array1 = this.selected.slice(0, this.tournament?.teamSize - 1);

      let array2 = this.selected.slice(
        this.tournament?.teamSize - 1,
        this.tournament?.teamSize - 1 + this.selected.length
      );
      this.returnData["teamMembers"] = array1;
      this.returnData["substituteMembers"] = array2;
      this.tournamentService.setSelectedTeam(this.returnData);
      this.router.navigate(["tournament", this.tournament?.slug, "join"]);
    } else {
      this.returnData["teamMembers"] = this.selected;
      this.tournamentService.setSelectedTeam(this.returnData);
      this.tournamentService.joinDetail.subscribe((data) => {
      })
      this.router.navigate(["tournament", this.tournament?.slug, "join"]);
    }
  }
}
