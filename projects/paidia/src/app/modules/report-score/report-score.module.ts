import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReportScoreComponent } from './report-score.component';
import { Routes, RouterModule } from '@angular/router';
import { HeaderPaidiaModule } from '../../shared/components/header-paidia/header-paidia.module';
import { CoreModule } from '../../core/core.module';
import { PopupCreatedComponent } from './components/popup-created/popup-created.component';
import { TeamBannerComponent } from './components/team-banner/team-banner.component';
import { TeamCardComponent } from './components/team-card/team-card.component';
import { ResultOverviewComponent } from './components/result-overview/result-overview.component';
import { SharedModule } from '../../shared/modules/shared.module';

export const routes: Routes = [
  {
    path: '',
    component: ReportScoreComponent
  }
]

@NgModule({
  declarations: [ReportScoreComponent, PopupCreatedComponent,
    TeamBannerComponent, TeamCardComponent, ResultOverviewComponent],
  imports: [
    CommonModule,
    SharedModule,
    HeaderPaidiaModule,
    CoreModule,
    RouterModule.forChild(routes)
  ]
})
export class ReportScoreModule { }
