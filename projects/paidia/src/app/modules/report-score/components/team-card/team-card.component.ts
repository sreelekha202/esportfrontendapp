import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { UploadScreenshotComponent } from '../../../../shared/components/elimination/score-card/upload-screenshot/upload-screenshot.component';
import { GlobalUtils } from 'esports';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-team-card',
  templateUrl: './team-card.component.html',
  styleUrls: ['./team-card.component.scss'],
})
export class TeamCardComponent implements OnInit {
  newscore;
  @Input() isManage: boolean = false;
  @Input() team;
  @Input() score;
  @Input() set;
  @Input() team_name;
  @Input() isScreenShotRequired;
  @Input() teamScreenShotUrl?;
  @Input() screenShotKey?;
  @Input() hasScreenshotUpdateAccess?;
  @Output() updateScore = new EventEmitter();
  constructor(private matDialog: MatDialog) {}
  ngOnInit(): void {}
  updateScoree(set, score, team_name, n, team_id) {
    this.updateScore.emit({
      set: set,
      score: score,
      team_name: team_name,
      n: n,
      team_id: team_id,
      teamScreenShot: this.teamScreenShotUrl,
      screenShotKey: this.screenShotKey
    });
  }

  openScreenshotUploadPopup = async () => {
    try {
      if (this.teamScreenShotUrl) {
        return this.goToUrl();
      }
      const data = {
        screenshotUrl: this.teamScreenShotUrl,
        hasScreenshotUpdateAccess: this.hasScreenshotUpdateAccess,
      };
      const response = await this.matDialog
        .open(UploadScreenshotComponent, {
          width: '900px',
          data,
        })
        .afterClosed()
        .toPromise();

      if (response?.hasScreenshotUpdateAccess) {
        this.teamScreenShotUrl = response?.screenshotUrl;
        this.updateScore.emit({
          set: this.set,
          score: this.score,
          team_name: this.team_name,
          n: 1,
          team_id: this.team?._id,
          teamScreenShotUrl: this.teamScreenShotUrl,
          screenShotKey: this.screenShotKey
        });
      }
    } catch (error) {}
  };

  goToUrl() {
    if (GlobalUtils.isBrowser()) {
      window.open(this.teamScreenShotUrl, '_blank');
    }
  }
}
