import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-result-overview',
  templateUrl: './result-overview.component.html',
  styleUrls: ['./result-overview.component.scss']
})
export class ResultOverviewComponent implements OnInit {
  @Input() data;
  constructor() { }
  tReport:any
  ngOnInit(): void {
    this.tReport = JSON.parse(localStorage.getItem('t_report'));
  }

}
