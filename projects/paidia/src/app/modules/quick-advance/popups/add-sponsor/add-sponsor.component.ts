import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { EsportsConstantsService } from 'esports';
@Component({
  selector: 'app-add-sponsor',
  templateUrl: './add-sponsor.component.html',
  styleUrls: ['./add-sponsor.component.scss'],
})
export class AddSponsorComponent implements OnInit {
  addForm: FormGroup;
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder,

    public dialogRef: MatDialogRef<AddSponsorComponent>
  ) {
    this.addForm = this.fb.group({
      sponsorName: ['', Validators.compose([Validators.required])],
      website: [
        '',
        Validators.compose([
          Validators.required,
          Validators.pattern(EsportsConstantsService?.webUrlRegex),
        ]),
      ],
      // playStoreUrl: [
      //   '',
      //   Validators.compose([
      //     Validators.required,
      //     Validators.pattern(EsportsConstantsService?.playStoreUrlRegex),
      //   ]),
      // ],
      // appStoreUrl: [
      //   '',
      //   Validators.compose([
      //     Validators.required,
      //     Validators.pattern(EsportsConstantsService?.appStoreUrlRegex),
      //   ]),
      // ],
      sponsorLogo: ['', Validators.compose([Validators.required])],
      sponsorBanner: ['', Validators.compose([Validators.required])],
    });
  }

  ngOnInit(): void {}

  onClose(): void {
    this.dialogRef.close(false);
  }
  onSubmit() {
    if (this.addForm.valid) {
      this.dialogRef.close(this.addForm.value);
    }
  }
}
