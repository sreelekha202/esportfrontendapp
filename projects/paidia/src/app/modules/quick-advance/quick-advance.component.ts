import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  FormControl,
  Validators,
} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import {
  EsportsToastService,
  EsportsTournamentService,
  ITournament,
} from 'esports';
import { Subscription } from 'rxjs';

@Component({
  selector: 'quick-advance-tournament',
  templateUrl: './quick-advance.component.html',
  styleUrls: ['./quick-advance.component.scss'],
})
export class QuickAdvanceTournamentComponent implements OnInit {
  @Input() slug: string | null;
  tournamentForm: FormGroup;
  isDataLoaded: boolean = false;
  editTournament: boolean = false;
  tournamentDetails?: ITournament | null;
  tournament;

  constructor(
    private fb: FormBuilder,
    private eSportsTournamentService: EsportsTournamentService,
    private eSportsToastService: EsportsToastService,
    private router: Router,
    private activeRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.tournamentForm = this.fb.group({
      step: 1,
      type: ['advance', Validators.required],
      name: [
        '',
        Validators.compose([Validators.required, Validators.maxLength(100)]),
      ],
      gameDetail: ['', Validators.required],
      platform: ['', Validators.required],
      participantType: ['', Validators.required],
      teamFormat: [''],
      teamSize: [''],
      startDate: ['', Validators.compose([Validators.required])],
      endDate: ['', Validators.compose([Validators.required])],
      startTime: [''],
      participants: [[]],
      emailInvite: [[]],
      isRegEndDate: [false],
      isRegStartDate: [false],
      bracketType: ['', Validators.required],
      isParticipantsLimit: [true, Validators.required],
      maxParticipants: [2, Validators.required],
      isScreenshotRequired: [false, Validators.required],
      isShowCountryFlags: [false, Validators.required],
      isSpecifyAllowedRegions: [false, Validators.required],
      scoreReporting: [2, Validators.required],
      visibility: [1, Validators.required],
      description: [''],
      rules: [''],
      allowSubstituteMember: [false],
      substituteMemberSize: [''],
      noOfSet: [1, Validators.required],
      banner: [''],
      isAllowThirdPlaceMatch: [false],
      allowAdvanceStage: [false],
      isCheckInRequired: [false],
      checkInStartDate: [''],
      stageMatch: [''],
      stageMatchNoOfSet: [''],
    });
    // const slug = this.activeRoute?.snapshot?.params?.id;
    const slug = this.activeRoute?.snapshot?.params?.slug;
    this.editTournament = !!slug;
    this.getGamesAndTournamentDetails(slug);
    this.tournamentTypeHandler('advance');
    return;
  }

  ngOnDestroy() {}

  setTournamentData(data) {
    this.tournament = data;
  }

  getGamesAndTournamentDetails = async (slug: string | null) => {
    try {
      const games = await this.eSportsTournamentService.getGames();
      this.eSportsTournamentService.setGameList = games?.data;

      if (slug) {
        const { data } =
          await this.eSportsTournamentService.getTournamentBySlug(
            `${slug}`
          );

        if (data.isSeeded || new Date(data.startDate) < new Date()) {
          this.router.navigate([`tournament/${slug}`]);
        }

        this.tournamentDetails = data;
        const game = this.eSportsTournamentService.getGameList?.find((item) => {
          return item?._id == data?.gameDetail?._id;
        });
        this.eSportsTournamentService.setPlatformList = game?.platform;
        this.eSportsTournamentService.setBracketTypes = game?.bracketTypes;
        this.tournamentForm.addControl('_id', new FormControl(''));
        this.tournamentTypeHandler(data?.type);
        this.tournamentForm.patchValue({
          ...data,
          gameDetail: data?.gameDetail?._id,
          prizeList: data?.prizeList,
        });
        this.eSportsTournamentService.manageTournamentSubject.next(this.tournamentDetails);
      }
      this.isDataLoaded = true;
    } catch (error) {
      this.isDataLoaded = true;
      const errorMessage = error?.error?.message || error?.message;
      this.eSportsToastService.showError(errorMessage);
    }
  };

  tournamentTypeHandler = (type: string | null) => {
    try {
      this.tournamentForm?.get('type')?.setValue(type);
      const noOfStep = this.getNoOfStep(type);
      this.eSportsTournamentService.totalPages.next(noOfStep);
      if (type == 'quick') {
        this.tournamentForm?.get('bracketType')?.setValue('single');
      } else if (type == 'advance') {
        this.tournamentForm.addControl(
          'regStartDate',
          new FormControl(false, Validators.required)
        );
        this.tournamentForm.addControl(
          'regEndDate',
          new FormControl('', Validators.required)
        );
        this.tournamentForm.addControl(
          'isPaid',
          new FormControl(false, Validators.required)
        );
        this.tournamentForm.addControl(
          'regFee',
          new FormControl('', Validators.required)
        );
        this.tournamentForm.addControl(
          'prizeCurrency',
          new FormControl('CAD', Validators.required)
        );
        this.tournamentForm.addControl(
          'isPrize',
          new FormControl(false, Validators.required)
        );
        this.tournamentForm.addControl(
          'tournamentType',
          new FormControl('online', Validators.required)
        );
        this.tournamentForm.addControl('youtubeVideoLink', new FormControl(''));
        this.tournamentForm.addControl(
          'facebookVideoLink',
          new FormControl('')
        );
        this.tournamentForm.addControl('twitchVideoLink', new FormControl(''));
      } else {
      }
    } catch (error) {}
  };

  getNoOfStep(type: string | null): number {
    switch (type) {
      case 'quick':
        return 5;
      case 'advance':
        return 7;
      default:
        return 3;
    }
  }
}
