import {
  Component,
  EventEmitter,
  OnInit,
  Output,
  OnDestroy,
} from '@angular/core';
import {
  AbstractControl,
  ControlContainer,
  FormControl,
  FormGroupDirective,
  ValidatorFn,
  Validators,
  FormBuilder,
  FormArray,
  FormGroup,
} from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { ConfirmComponent } from '../../popups/confirm/confirm.component';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import {
  EsportsConstantsService,
  EsportsToastService,
  EsportsTournamentService,
  EsportsGtmService,
  EventProperties
} from 'esports';
import { HttpClient } from '@angular/common/http';
import { AddSponsorComponent } from '../../popups/add-sponsor/add-sponsor.component';
import { MatTabChangeEvent } from '@angular/material/tabs';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';
import { differenceInMinutes, addMinutes } from 'date-fns';

@AutoUnsubscribe()
@Component({
  selector: 'app-tournament-details',
  templateUrl: './tournament-details.component.html',
  styleUrls: [
    '../send-invite/send-invite.component.scss',
    './tournament-details.component.scss',
  ],
  viewProviders: [
    { provide: ControlContainer, useExisting: FormGroupDirective },
  ],
})
export class TournamentDetailsComponent implements OnInit, OnDestroy {
  Step7;
  structure;
  games1: any = [];
  isBracketLoaded: boolean = false;
  userList = [];
  gameList = [];
  checkInTimeOptions = [];
  selectedcontries: any = [];
  createTournament: any;
  start_date: any;
  isSpecifyAllowedRegions: boolean = false;
  showLoader: boolean = false;
  tournamentDetails;
  platformList = [];
  isProcessing: boolean = false;
  isFormLoaded: boolean = false;
  minDate: Date | null;
  currentDate = new Date();
  prizeLimit: number = 6;
  maxCheckInOpenDateValue: any;
  maxCheckInClosingDateValue: any;
  minStartDateValue = this.setMinDate();
  matchFormat = [
    { _id: 1, name: 'TOURNAMENT.BEST_OF_1' },
    { _id: 3, name: 'TOURNAMENT.BEST_OF_3' },
    { _id: 5, name: 'TOURNAMENT.BEST_OF_5' },
    { _id: 7, name: 'TOURNAMENT.BEST_OF_7' },
  ];
  stageMatchFormat = [
    { _id: 'quarterFinal', name: 'TOURNAMENT.QUARTER_FINAL' },
    { _id: 'semiFinal', name: 'TOURNAMENT.SEMI_FINAL' },
    { _id: 'final', name: 'TOURNAMENT.FINAL' },
  ];

  participantType = [
    { _id: 'individual', name: 'Solo' },
    { _id: 'team', name: 'Team' },
  ];

  rrFormat = [
    { _id: 1, name: 'Once' },
    { _id: 2, name: 'Twice' },
  ];

  teamFormat = [
    {
      id: '2',
      name: '2v',
    },
    {
      id: '4',
      name: '4v',
    },
    {
      id: 'n',
      name: 'x vs x',
    },
  ];
  editorConfig = {};
  bracketTypes = [
    'single',
    'double',
    'round_robin',
    'ladder',
    'battle_royale',
    'swiss_safeis',
  ];
  selectedBracketIndex = 0;
  selectedBracketType = '';
  showGameBrackets: any = {
    battle_royale: true,
    double: true,
    ladder: true,
    round_robin: true,
    single: true,
    swiss_safeis: true,
  };
  nextStageBracketIndex = 0;

  @Output() tournamentEvent = new EventEmitter();
  selectedGameDetails: any;
  minCheckInClosingDateValue = this.setMinDate();
  sponsors: any = [];
  constructor(
    private tournament: FormGroupDirective,
    private fb: FormBuilder,
    private eSportsToastService: EsportsToastService,
    public eSportsTournamentService: EsportsTournamentService,
    private matDialog: MatDialog,
    private router: Router,
    private http: HttpClient,
    private translateService: TranslateService,
    private gtmService: EsportsGtmService,
    public dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.checkInTimeOptions = [
      {
        name: 'TOURNAMENT.30_MINUTES_PRIOR',
        value: { hour: 0, minute: 30 },
        _id: 30,
      },
      {
        name: 'TOURNAMENT.1_HOUR_PRIOR',
        value: { hour: 1, minute: 0 },
        _id: 60,
      },
      {
        name: 'TOURNAMENT.2_HOURS_PRIOR',
        value: { hour: 2, minute: 0 },
        _id: 120,
      },
      {
        name: 'TOURNAMENT.3_HOURS_PRIOR',
        value: { hour: 3, minute: 0 },
        _id: 180,
      },
      {
        name: 'TOURNAMENT.4_HOURS_PRIOR',
        value: { hour: 4, minute: 0 },
        _id: 240,
      },
      {
        name: 'TOURNAMENT.5_HOURS_PRIOR',
        value: { hour: 5, minute: 0 },
        _id: 300,
      },
    ];

    let countries = [];
    this.games1 = [];
    this.http.get('assets/json/countries.json').subscribe(
      (data: any) => {
        countries = data.countries;
        for (let d of countries) {
          d.checked = false;
          //  this.games1.push(d)
        }
        this.games1 = countries;
        this.showLoader = false;
        //this.products = data;
      },
      (err) => {
        this.showLoader = false;
      }
    );
    this.Step7 = this.tournament.form;
    this.gameList = this.eSportsTournamentService.getGameList;
    this.platformList = this.eSportsTournamentService.getPlatformList;
    this.fetchBracketMockStructure();
    this.createTournamentForm();
    this.setMinimumDate();
    this.editorConfig = {
      editable: true,
      spellcheck: true,
      height: '180px',
      width: 'auto',
      minWidth: '0',
      translate: 'yes',
      enableToolbar: true,
      showToolbar: true,
      defaultParagraphSeparator: '',
      defaultFontName: '',
      defaultFontSize: '',
      fonts: [
        { class: 'arial', name: 'Arial' },
        { class: 'times-new-roman', name: 'Times New Roman' },
        { class: 'calibri', name: 'Calibri' },
        { class: 'comic-sans-ms', name: 'Comic Sans MS' },
      ],

      customClasses: [
        {
          name: 'quote',
          class: 'quote',
        },
        {
          name: 'redText',
          class: 'redText',
        },
        {
          name: 'titleText',
          class: 'titleText',
          tag: 'h1',
        },
      ],
      sanitize: true,
      toolbarPosition: 'top',
      toolbarHiddenButtons: [['bold', 'italic'], ['fontSize']],
    };
    this.setGameImage();
    this.showGameBrackets = this.eSportsTournamentService.bracketTypes;
    this.bracketTypes = this.bracketTypes.filter((ele) => {
      return this.showGameBrackets[ele];
    });
    this.selectedBracketIndex = this.bracketTypes.indexOf(
      this.Step7.value.bracketType
    );
    this.selectedBracketType = this.Step7.value.bracketType;
    this.nextStageBracketIndex =
      this.Step7.value.stageBracketType == 'single' ? 0 : 1;
  }

  nextStageBracket($event: MatTabChangeEvent) {
    const stageBracketType = ['single', 'double'];
    const formControl = this.Step7?.get('stageBracketType');
    if (!formControl) {
      this.addNewControl('stageBracketType', 'single');
    }
    this.Step7?.get('stageBracketType').setValue(
      stageBracketType[$event.index]
    );
  }

  roundHandler(noOfRound) {
    if (noOfRound)
      this.addNewControlAndUpdateValidity(
        'noOfLoss',
        'NUMBER',
        0,
        0,
        noOfRound - 1
      );
  }

  onBracketChange($event: MatTabChangeEvent) {
    this.onSelectBracketType(this.bracketTypes[$event.index]);
    this.selectedBracketType = this.Step7.value.bracketType;
  }

  setGameImage() {
    this.gameList.map((game) => {
      if (game._id == this.Step7?.get('gameDetail').value) {
        this.selectedGameDetails = game;
      }
    });
  }
  setMinimumDate() {
    const date = new Date();
    date.setMinutes(date.getMinutes() + 5);
    date.setSeconds(0);
    this.minDate = date;
  }

  fetchBracketMockStructure = async () => {
    try {
      this.isBracketLoaded = false;
      const { value } = this.Step7;
      const payload = {
        bracketType: value?.bracketType,
        ...(value?.bracketType != 'swiss_safeis' && {
          maximumParticipants: value?.maxParticipants,
          noOfSet: value?.noOfSet,
        }),
        ...(['round_robin', 'battle_royale'].includes(value?.bracketType) && {
          noOfTeamInGroup: value?.noOfTeamInGroup,
          noOfWinningTeamInGroup: value?.noOfWinningTeamInGroup,
          noOfRoundPerGroup: value?.noOfRoundPerGroup,
          stageBracketType: value?.stageBracketType,
        }),
      };
      this.eSportsTournamentService
        .generateBracket(payload)
        .then((response) => {
          this.structure = response.data;
        })
        .catch((err) => { })
        .finally(() => {
          this.isBracketLoaded = true;
        });
    } catch (error) {
      this.isBracketLoaded = true;
      const errorMessage = error?.error?.message || error?.message;
      this.eSportsToastService.showError(errorMessage);
    }
  };

  // custom field controller
  addNewControl = (field, value, customValidation?) => {
    try {
      const formControl = this.Step7?.get(field);
      if (!formControl) {
        const control = customValidation
          ? new FormControl(value, customValidation)
          : new FormControl(value);

        this.Step7.addControl(field, control);
      }
    } catch (error) { }
  };

  prizeClickHandler(checked, prizeLen = 1) {
    if (checked) {
      const formControl = this.Step7?.get('prizeList');
      if (formControl) return;

      this.Step7.addControl(
        'prizeList',
        this.fb.array([], this.customPrizeValidator())
      );
      for (let i = 0; i < prizeLen; i++) this.addPrize(i);
      this.Step7.addControl(
        'prizeCurrency',
        new FormControl('CAD', Validators.compose([Validators.required]))
      );
    } else {
      this.Step7.removeControl('prizeList');
      this.Step7.removeControl('prizeCurrency');
    }
  }

  sponsorClickHandler(checked, len = 1) {
    if (checked) {
      const formControl = this.Step7?.get('sponsors');
      if (formControl) return;
      this.Step7.addControl('sponsors', this.fb.array([]));
      this.addSponsor(len);
    } else {
      this.Step7.removeControl('sponsors');
    }
  }

  createSponsor(): FormGroup {
    return this.fb.group({
      sponsorName: ['', Validators.compose([])],
      website: [
        '',
        Validators.compose([
          Validators.pattern(EsportsConstantsService?.webUrlRegex),
        ]),
      ],
      playStoreUrl: [
        '',
        Validators.compose([
          Validators.pattern(EsportsConstantsService?.playStoreUrlRegex),
        ]),
      ],
      appStoreUrl: [
        '',
        Validators.compose([
          Validators.pattern(EsportsConstantsService?.appStoreUrlRegex),
        ]),
      ],
      sponsorLogo: ['', Validators.compose([])],
      sponsorBanner: ['', Validators.compose([])],
    });
  }

  addSponsor(len = 1): void {
    const sponsor = this.Step7?.get('sponsors') as FormArray;
    for (let i = 0; i < len; i++) {
      sponsor.push(this.createSponsor());
    }
  }

  paidRegistrationHandler(check) {
    if (check) {
      const regCurrencyValidators = Validators.compose([Validators.required]);
      const regFeeValidators = Validators.compose([
        Validators.required,
        Validators.min(1),
      ]);
      this.addNewControl('regFeeCurrency', 'SAR', regCurrencyValidators);
      this.addNewControl('regFee', 1, regFeeValidators);
    } else {
      this.Step7.removeControl('regFeeCurrency');
      this.Step7.removeControl('regFee');
    }
  }

  createTournamentForm() {
    this.isFormLoaded = false;
    // for advance
    if (this.Step7.value?.type == 'advance') {
      const regStartDtValidation = Validators.compose([
        Validators.required,
        this.ValidateRegStartDate.bind(this),
      ]);

      const regStartDtTimeValidation = Validators.compose([
        Validators.required,
        this.ValidateRegStartTime.bind(this),
      ]);

      const regEndDtTimeValidation = Validators.compose([
        Validators.required,
        this.ValidateRegEndTime.bind(this),
      ]);

      const regEndDtValidation = Validators.compose([
        Validators.required,
        this.ValidateRegEndDate.bind(this),
      ]);

      const tournamentEndDtValidation = Validators.compose([
        Validators.required,
        this.ValidateEndDate.bind(this),
      ]);

      const tournamentTypeValidators = Validators.compose([
        Validators.required,
      ]);

      const youtubeVideoLinkValidators = Validators.compose([
        Validators.pattern(EsportsConstantsService.youtubeRegex),
      ]);

      const facebookVideoLinkValidators = Validators.compose([
        Validators.pattern(EsportsConstantsService.facebookRegex),
      ]);

      const twitchVideoLinkValidators = Validators.compose([
        Validators.pattern(EsportsConstantsService.twitchRegex),
      ]);

      this.addNewControl('regStartDate', '', regStartDtValidation);
      this.addNewControl('regStartTime', '', regStartDtTimeValidation);
      this.addNewControl('regEndDate', '', regEndDtValidation);
      this.addNewControl('regEndTime', '', regEndDtTimeValidation);
      this.addNewControl('endDate', '', tournamentEndDtValidation);
      this.addNewControl('endTime', '', tournamentEndDtValidation);
      this.addNewControl('pointsKills', 0);
      this.addNewControl('stageMatch', '');
      this.addNewControl('tournamentType', '', tournamentTypeValidators);

      this.addNewControl('youtubeVideoLink', '', youtubeVideoLinkValidators);
      this.addNewControl('facebookVideoLink', '', facebookVideoLinkValidators);
      this.addNewControl('twitchVideoLink', '', twitchVideoLinkValidators);
      this.paidRegistrationHandler(this.Step7?.value?.isPaid);
      this.Step7.get('startDate').valueChanges.subscribe((val) => {
        this.updateStartTime();
        this.Step7.get('regStartDate').updateValueAndValidity();
        this.Step7.get('regEndDate').updateValueAndValidity();
        this.Step7.get('endDate').updateValueAndValidity();
      });
      this.Step7.get('endDate').valueChanges.subscribe((val) => {
        this.Step7.get('endTime').updateValueAndValidity();
      });

      this.Step7.get('regStartDate').valueChanges.subscribe((val) => {
        this.Step7.get('regStartTime').updateValueAndValidity();
      });
      this.Step7.get('regEndDate').valueChanges.subscribe((val) => {
        this.Step7.get('regEndTime').updateValueAndValidity();
      });

      this.prizeClickHandler(this.Step7.value?.isPrize, 1);
      this.Step7.get('isRegStartDate')?.setValue(true);
      this.Step7.get('isRegEndDate')?.setValue(true);
      this.addNewControl('checkInTime', '');
      this.Step7.get('checkInTime').valueChanges.subscribe((val) => {
        this.handleCheckInDate(val);
      });
      this.Step7.get('isCheckInRequired').valueChanges.subscribe((val) => {
        if (val == false) {
          this.Step7.get('regEndDate').updateValueAndValidity();
        }
      });
    }

    this.patchEditFormValues();
    this.isFormLoaded = true;
  }

  onStepChange(step) {
    this.Step7?.get('step').setValue(step);
  }

  popUpTitleAndText = async (data) => {
    if (data?._id) {
      return {
        title: 'TOURNAMENT.UPDATE',
        text: 'TOURNAMENT.UPDATE_TXT',
      };
    } else {
      return {
        title: 'TOURNAMENT.SAVE_TOURNAMENT',
        text: 'TOURNAMENT.STATUS_2',
      };
    }
  };

  onGameChange = async (value: any) => {
    const games = this.eSportsTournamentService.getGameList;
    const selectedGame = games.find((elem) => {
      return elem?._id == value;
    });
    this.eSportsTournamentService.setPlatformList = selectedGame?.platform;
    this.platformList = this.eSportsTournamentService.getPlatformList;
  };
  setScore(score) {
    this.Step7.get('scoreReporting').setValue(score);
  }
  setVisibility(visibility) {
    this.Step7.get('visibility').setValue(visibility);
  }
  Submit2 = async () => {
    try {
      this.sponsors.length > 0
        ? this.Step7.addControl('sponsors', this.fb.control(this.sponsors))
        : '';
      if (!this.Step7.get('stageBracketType')?.value) {
        this.Step7.patchValue({
          stageBracketType: 'single',
        });
      }
      const { value, invalid } = this.Step7;
      if(value?.bracketType =='ladder'){
         value.isParticipantsLimit=false;
         value.substituteMemberSize="0";
      }
      const invalidKeys = Object.keys(this.Step7.controls).filter((element) => {
        return this.Step7.controls[element].status != 'VALID';
      });
      if (invalid) {
        this.eSportsToastService.showError('please fill required details');
        return;
      }
      const data = await this.popUpTitleAndText(value);
      const confirmed = await this.matDialog
        .open(ConfirmComponent, { data })
        .afterClosed()
        .toPromise();

      if (!confirmed) return;

      this.pushPublishTag()
      this.isProcessing = true;
      const response = value?._id
        ? await this.eSportsTournamentService.updateTournament(
          value,
          value?._id
        )
        : await this.eSportsTournamentService.saveTournament(value);

      this.eSportsToastService.showSuccess(response?.message);
      if (response?.data?.enablePayment) {
        this.tournamentDetails = response?.data;
        this.tournamentEvent.emit(this.tournamentDetails);
        this.Step7?.get('step').setValue(11);
      } else {
        this.router.navigateByUrl(`/tournament/${response?.data?.slug}`);
      }

      this.isProcessing = false;
    } catch (error) {
      this.isProcessing = false;
      const errorMessage = error?.error?.message || error?.message;
      this.eSportsToastService.showError(errorMessage);
    }
  };

  Submit() {
    this.setDates(this.Step7);
    let field1 = this.Step7.controls?.regStartDate?.value;
    let field2 = this.Step7.controls?.regEndDate?.value;
    let field3 = this.Step7.controls?.endDate?.value;
    let field4 = this.Step7.controls?.noOfSet?.value;
    this.Step7.markAllAsTouched();

    if (!field1 && !field2 && !field3) {
      this.eSportsToastService.showError(
        this.translateService.instant('CreateTournament.err.errors')
      );
    } else if (field1 && !field2 && !field3) {
      this.eSportsToastService.showError(
        this.translateService.instant(
          'CreateTournament.err.RegistrationEnd_Tournament_End_Date'
        )
      );
    } else if (field1 && field2 && !field3) {
      this.eSportsToastService.showError(
        this.translateService.instant(
          'CreateTournament.err.Tournament_End_Date'
        )
      );
    } else if (!field1 && field2 && field3) {
      this.eSportsToastService.showError(
        this.translateService.instant('CreateTournament.err.Registraion_Start')
      );
    } else if (!field1 && !field2 && field3) {
      this.eSportsToastService.showError(
        this.translateService.instant(
          'CreateTournament.err.Registraion_Start_End'
        )
      );
    } else if (!field4) {
      if (this.Step7.controls.bracketType.value == 'battle_royale') {
        this.Submit2();
      } else {
        this.eSportsToastService.showError(
          this.translateService.instant('CreateTournament.err.error')
        );
      }
    } else if (field1 && field2 && field3) {
      this.Submit2();
    }
  }

  addPrize(i): void {
    const prizeList = this.Step7.get('prizeList') as FormArray;
    const { maxParticipants } = this.Step7.value;
    let name = '';
    if (prizeList.controls.length < maxParticipants) {
      switch (true) {
        case prizeList.controls.length == 0:
          name = '1st';
          break;
        case prizeList.controls.length == 1:
          name = '2nd';
          break;
        case prizeList.controls.length == 2:
          name = '3rd';
          break;
        default:
          name = `${prizeList.controls.length + 1}th`;
          break;
      }
      const createPrizeForm = (): FormGroup => {
        const validator = [Validators.required, Validators.pattern('^[0-9]*$')];
        if (i == 0) {
          validator.push(Validators.min(1));
        }
        return this.fb.group({
          name: [name],
          value: ['', Validators.compose(validator)],
        });
      };
      prizeList.push(createPrizeForm());
    } else {
    }
  }

  // Custom validations

  ValidateRegStartDate(
    control: AbstractControl
  ): { [key: string]: any } | null {
    return !control.value
      ? null
      : this.validateRegStartDate(control['_parent'], 'pastRegStartDate');
  }

  ValidateRegEndDate(control: AbstractControl): { [key: string]: any } | null {
    return !control.value
      ? null
      : this.validateRegEndDate(control['_parent'], 'pastRegEndDate');
  }

  ValidateEndDate(control: AbstractControl): { [key: string]: any } | null {
    return !control.value
      ? null
      : this.validateBothEndDate(control['_parent'], 'pastEndDate');
  }

  validateRegStartDate(
    formGroup: FormGroup,
    ekey: string
  ): { [key: string]: boolean } {
    const { regStartDate, startDate, startTime } = formGroup.controls;

    if (ekey == 'pastRegStartDate' && !regStartDate?.value) {
      return { required: true };
    } else if (startDate?.value && regStartDate?.value) {
      const currentDate = new Date();
      const startDt = new Date(startDate.value);

      if (startTime && startTime.value && startTime.valid) {
        const { hour, minute } = this.getHoursAndMinutes(startTime);
        startDt.setHours(hour);
        startDt.setMinutes(minute);
      }

      const regstartDt = new Date(regStartDate.value);

      if (startDt > regstartDt && regstartDt > currentDate) {
        return null;
      } else {
        return {
          [ekey]: true,
        };
      }
    }
    return null;
  }

  validateRegEndDate(
    formGroup: FormGroup,
    ekey: string
  ): { [key: string]: boolean } {
    const { regStartDate, regEndDate, startDate } = formGroup.controls;
    if (ekey == 'pastRegEndDate' && !regEndDate?.value) {
      return { required: true };
    } else if (regEndDate?.value && regStartDate?.value && startDate?.value) {
      const regEndDt = new Date(regEndDate.value);
      const regstartDt = new Date(regStartDate.value);
      let tournamentStartDt, tournamentStartDate;
      tournamentStartDt = new Date(startDate.value);

      if (regEndDt > regstartDt && regEndDt <= tournamentStartDt) {
        return null;
      } else {
        return {
          [ekey]: true,
        };
      }
    }
    return null;
  }

  validateBothEndDate(
    formGroup: FormGroup,
    ekey: string
  ): { [key: string]: boolean } {
    const { startDate, startTime, endDate, endTime } = formGroup.controls;
    if (ekey == 'pastEndDate' && !endDate?.value) {
      return { required: true };
    } else if (startDate?.value && endDate?.value) {
      const startDt = new Date(startDate.value);

      if (startTime?.value && startTime?.valid) {
        const { hour, minute } = this.getHoursAndMinutes(startTime);
        startDt.setHours(hour);
        startDt.setMinutes(minute);
      }
      const endDt = new Date(endDate.value);
      if (endTime?.value) {
        const values = this.getHoursAndMinutes(endTime);
        if (values) {
          endDt.setHours(values?.hour);
          endDt.setMinutes(values?.minute);
        }
      }
      if (endDt > startDt) {
        return null;
      } else {
        return {
          [ekey]: true,
        };
      }
    }
    return null;
  }

  customPrizeValidator(): ValidatorFn {
    return (formArray: FormArray): { [key: string]: any } | null => {
      let total = 0;
      for (let item of formArray.controls) {
        total = total + item.value.value;
      }
      const maxParticipants =
        formArray['_parent']?.controls?.maxParticipants?.value;
      const prizeLimit = maxParticipants;
      if (formArray.controls.length > prizeLimit) {
        return { prizeLimit: true };
      }
      const isPrizeValid = formArray.controls.length ? total > 0 : true;
      return isPrizeValid ? null : { prizeMoneyRequired: true };
    };
  }

  emptyCheck = (control: FormControl) => {
    return control.value && control.value.trim() ? null : { required: true };
  };

  onEdit() {
    this.Step7.get('step').setValue(1);
  }

  getNotification5(event) {
    this.isSpecifyAllowedRegions = event.checked;
  }
  getNotification6(event) {
    this.selectedcontries = event;
  }

  openDialog() {
    const dialogRef = this.dialog.open(AddSponsorComponent, {
      width: '800px',
      height: '600px',
      disableClose: true,
      data: {},
    });

    dialogRef.afterClosed().subscribe((data) => {
      if (data && data.website) {
        this.sponsors.push(data);
      }
    });
  }
  removeSponsor(index) {
    this.sponsors.splice(index, 1);
  }
  onSelectBracketType(v) {
    this.Step7.get('bracketType').setValue(v);
    this.bracketChangeHandler(v);
  }
  bracketChangeHandler(
    type,
    noOfPlacement?,
    maxPlacement = 2,
    maxParticipants?,
    noOfRound?,
    noOfLoss?
  ) {
    let maximumParticipants = 1024;

    if (['round_robin', 'battle_royale'].includes(type)) {
      type == 'battle_royale'
        ? this.removeControlOrValidity('noOfSet')
        : this.addNewControlAndUpdateValidity('noOfSet', 'STRING', '');
      type == 'battle_royale'
        ? this.addNewControlAndUpdateValidity(
          'noOfPlacement',
          'NUMBER',
          2,
          2,
          this.Step7?.value?.noOfTeamInGroup || maxPlacement || 2
        )
        : this.removeControlOrValidity('noOfPlacement');
      type == 'battle_royale'
        ? this.placementCounter(noOfPlacement || 2)
        : this.removeControlOrValidity('placementPoints');
      type == 'battle_royale'
        ? this.addNewControlAndUpdateValidity(
          'isKillPointRequired',
          'BOOLEAN',
          false
        )
        : this.removeControlOrValidity('isKillPointRequired');
      type == 'battle_royale'
        ? this.addNewControlAndUpdateValidity('pointsKills', 'NUMBER', 1)
        : this.removeControlOrValidity('pointsKills');
      type == 'battle_royale'
        ? this.removeControlOrValidity('stageBracketType')
        : this.addNewControlAndUpdateValidity('stageBracketType', 'STRING', '');
      type == 'battle_royale'
        ? this.removeControlOrValidity('allowAdvanceStage')
        : this.addNewControlAndUpdateValidity(
          'allowAdvanceStage',
          'BOOLEAN',
          false
        );

      this.multiStageTournamentHandler(true, type, maxParticipants);
    } else {
      this.multiStageTournamentHandler(false);
      this.addNewControlAndUpdateValidity('noOfSet', 'STRING', '');
      this.removeControlOrValidity('noOfPlacement');
      this.removeControlOrValidity('placementPoints');
      this.removeControlOrValidity('isKillPointRequired');
      this.removeControlOrValidity('pointsKills');
      this.removeControlOrValidity('stageBracketType');
      this.addNewControlAndUpdateValidity(
        'allowAdvanceStage',
        'BOOLEAN',
        false
      );
    }

    switch (type) {
      case 'single':
        maximumParticipants = 1024;
        break;
      case 'double':
        maximumParticipants = 512;
        break;
      case 'round_robin':
        maximumParticipants = 40;
        break;
      case 'battle_royale':
        maximumParticipants = 1024;
        break;
    }

    if (type == 'swiss_safeis') {
      this.addNewControlAndUpdateValidity(
        'noOfRound',
        'NUMBER',
        noOfRound || 1,
        1
      );
      this.addNewControlAndUpdateValidity(
        'noOfLoss',
        'NUMBER',
        noOfLoss || 0,
        0,
        (noOfRound || 1) - 1
      );
      this.addNewControlAndUpdateValidity('enableTiebreaker', 'BOOLEAN', false);
      this.Step7.addControl(
        'setDuration',
        this.fb.group(
          {
            days: [1, Validators.required],
            hours: [0, Validators.required],
            mins: [0, Validators.required],
          },
          { validator: this.durationValidator }
        )
      );
      this.Step7.get('isParticipantsLimit').setValue(false);
      this.removeControlOrValidity('maxParticipants');
      this.prizeLimit = 20;
    } else {
      this.removeControlOrValidity('noOfRound');
      this.removeControlOrValidity('noOfLoss');
      this.removeControlOrValidity('setDuration');
      this.removeControlOrValidity('enableTiebreaker');

      this.Step7.get('isParticipantsLimit').setValue(true);
      this.addNewControlAndUpdateValidity(
        'maxParticipants',
        'NUMBER',
        2,
        2,
        maximumParticipants,
        false,
        this.teamInGroupValidation('maxParticipants').bind(this)
      );
      this.prizeLimit = 6;
    }
    // this.fetchBracketMockStructure();
  }
  durationValidator = (
    control: AbstractControl
  ): { [key: string]: boolean } => {
    const { days, hours, mins } = control.value;

    if (days || hours || mins) return null;

    return {
      required: true,
    };
  };
  multiStageTournamentHandler = (
    isEnableFields: boolean,
    bracketType?,
    maxParticipants?
  ): void => {
    if (isEnableFields) {
      const noOfRound = bracketType == 'round_robin' ? 2 : 10;
      this.addNewControlAndUpdateValidity(
        'noOfTeamInGroup',
        'NUMBER',
        2,
        2,
        maxParticipants || this.Step7.value?.maxParticipants || 2,
        false,
        this.teamInGroupValidation('noOfTeamInGroup').bind(this)
      );
      this.addNewControlAndUpdateValidity(
        'noOfWinningTeamInGroup',
        'NUMBER',
        1,
        1,
        false,
        false,
        this.teamInGroupValidation('noOfWinningTeamInGroup').bind(this)
      );
      this.addNewControlAndUpdateValidity(
        'noOfRoundPerGroup',
        'NUMBER',
        1,
        1,
        noOfRound
      );
      this.addNewControlAndUpdateValidity('noOfStage', 'NUMBER', 1);
    } else {
      this.removeControlOrValidity('noOfTeamInGroup');
      this.removeControlOrValidity('noOfWinningTeamInGroup');
      this.removeControlOrValidity('noOfRoundPerGroup');
      this.removeControlOrValidity('noOfStage');
    }
  };
  // custom field controller
  addNewControlAndUpdateValidity = (
    field,
    fieldType,
    defaultValue,
    min?,
    max?,
    pattern?,
    customValidation?
  ) => {
    const formControl = this.Step7.get(field);
    const validation = [];

    switch (fieldType) {
      case 'NUMBER':
        validation.push(Validators.required);
        if (typeof min == 'number') validation.push(Validators.min(min));
        if (typeof max == 'number') validation.push(Validators.max(max));
        if (pattern) validation.push(pattern);
        if (customValidation) validation.push(customValidation);
        break;
      case 'BOOLEAN':
        validation.push(Validators.required);
        break;
      case 'STRING':
        validation.push(Validators.required);
        break;
    }

    if (formControl) {
      formControl.setValue(defaultValue);
      formControl.setValidators(validation);
      formControl.updateValueAndValidity();
    } else {
      this.Step7.addControl(
        field,
        new FormControl(defaultValue, Validators.compose(validation))
      );
    }
  };

  placementCounter = async (value) => {
    const placementPoints = this.Step7.get('placementPoints');
    if (!placementPoints) {
      this.Step7.addControl('placementPoints', this.fb.array([]));
    }
    const placementPointsList = this.Step7.get('placementPoints') as FormArray;

    const createPlacementForm = (position): FormGroup => {
      return this.fb.group({
        position: [position],
        value: [
          1,
          Validators.compose([
            Validators.required,
            Validators.pattern('^[0-9]*$'),
          ]),
        ],
      });
    };

    // remove placement node
    const deletePlacementForm = async (count, value) => {
      placementPointsList.removeAt(count);
      return count <= value ? true : await deletePlacementForm(--count, value);
    };

    // add placement node
    const addPlacementForm = async (count, value) => {
      placementPointsList.push(createPlacementForm(count));
      return count >= value ? true : await addPlacementForm(++count, value);
    };

    let count = placementPointsList.length;

    if (count > value) {
      await deletePlacementForm(count - 1, value);
    }

    if (count < value) {
      await addPlacementForm(count + 1, value);
    }
  };
  // custom validators

  teamInGroupValidation = (field?: string): ValidatorFn => {
    return (control: AbstractControl): { [key: string]: any } | null => {
      if (control['_parent']) {
        const {
          noOfTeamInGroup,
          noOfWinningTeamInGroup,
          maxParticipants,
          noOfStage,
          bracketType,
        } = control?.['_parent']?.controls;

        if (
          maxParticipants?.valid &&
          noOfTeamInGroup?.value &&
          !noOfTeamInGroup?.errors?.max &&
          noOfWinningTeamInGroup?.value &&
          maxParticipants?.value &&
          noOfStage
        ) {
          const isPowerOfTwo = (n) => {
            if (n == 0) return false;
            return (
              parseInt(Math.ceil(Math.log(n) / Math.log(2)).toString()) ==
              parseInt(Math.floor(Math.log(n) / Math.log(2)).toString())
            );
          };

          const fieldValidation = (
            maxParticipants,
            noOfTeamInGroup,
            noOfWinningTeam
          ) => {
            if (maxParticipants < noOfTeamInGroup) {
              return { max: { max: maxParticipants } };
            } else if (noOfTeamInGroup > noOfWinningTeam) {
              if (bracketType.value == 'round_robin') {
                const totalWinners =
                  (maxParticipants / noOfTeamInGroup) * noOfWinningTeam;
                const pow2 = isPowerOfTwo(totalWinners);
                return pow2 ? null : { multiStageConfigError: true };
              }
              const check = noOfTeamInGroup % noOfWinningTeam != 0;
              return check ? { multiStageConfigError: true } : null;
            } else {
              return { multiStageConfigError: true };
            }
          };
          const errorConfig = fieldValidation(
            maxParticipants?.value,
            noOfTeamInGroup?.value,
            noOfWinningTeamInGroup?.value
          );

          if (!errorConfig) {
            const totalStage = (
              totalPlayer,
              noOfPlayerPerGroup,
              noOfWinning,
              noOfStage,
              previousStagePlayer
            ) => {
              let nextStagePlayer;
              const createStage = () => {
                nextStagePlayer =
                  Math.ceil(totalPlayer / noOfPlayerPerGroup) * noOfWinning;
                if (nextStagePlayer < previousStagePlayer) {
                  // noOfPlayerPerGroup = noOfPlayerPerGroup + 1;
                  nextStagePlayer =
                    Math.ceil(totalPlayer / noOfPlayerPerGroup) * noOfWinning;
                } else {
                  createStage();
                }
              };

              if (totalPlayer !== noOfPlayerPerGroup) {
                createStage();
              }
              return totalPlayer == noOfPlayerPerGroup
                ? noOfStage
                : !(Math.ceil(nextStagePlayer / noOfPlayerPerGroup) > 1)
                  ? noOfStage + 1
                  : Math.ceil(nextStagePlayer / noOfPlayerPerGroup) == 2
                    ? noOfStage + 2
                    : totalStage(
                      nextStagePlayer,
                      noOfPlayerPerGroup,
                      noOfWinning,
                      noOfStage + 1,
                      previousStagePlayer
                    );
            };

            const noOfStage = totalStage(
              maxParticipants?.value,
              noOfTeamInGroup?.value,
              noOfWinningTeamInGroup?.value,
              1,
              maxParticipants?.value
            );
            this.Step7.get('noOfStage').setValue(noOfStage);
          }

          if (field == 'noOfTeamInGroup') {
            return errorConfig;
          } else {
            this.Step7.get('noOfTeamInGroup').setErrors(errorConfig);
            return null;
          }
        }
      } else {
        return null;
      }
    };
  };
  removeControlOrValidity = (field) => {
    const formControl = this.Step7.get(field);
    if (formControl) {
      this.Step7.removeControl(field);
    } else {
    }
  };
  validateRegStartDateAndTime(
    formGroup: FormGroup,
    ekey: string
  ): { [key: string]: boolean } {
    const {
      regStartDate,
      regStartTime,
      startDate,
      startTime,
    } = formGroup.controls;
    // const time = startTime.value || {};
    // const { hour, minute } = time;

    if (
      (ekey == 'pastRegStartDate' && !regStartDate?.value) ||
      (ekey == 'pastRegStartTime' && !regStartTime?.value)
    ) {
      return { required: true };
    } else if (
      startDate?.value &&
      startTime?.value &&
      regStartDate?.value &&
      regStartTime?.value
    ) {
      const currentDate = new Date();
      const startDt = new Date(startDate.value);

      const startTimeHours = this.getHoursAndMinutes(startTime);
      startDt.setHours(startTimeHours.hour);
      startDt.setMinutes(startTimeHours.minute);
      const regstartDt = new Date(regStartDate.value);
      const regStartHours = this.getHoursAndMinutes(regStartTime);
      regstartDt.setHours(regStartHours?.hour);
      regstartDt.setMinutes(regStartHours?.minute);
      if (startDt > regstartDt && regstartDt > currentDate) {
        this.Step7.get(
          ekey == 'pastRegStartTime' ? 'regStartDate' : 'regStartTime'
        ).setErrors(null);
        return null;
      } else {
        return {
          [ekey]: true,
        };
      }
    }
    return null;
  }
  //Registration date time validator
  ValidateRegStartTime(
    control: AbstractControl
  ): { [key: string]: any } | null {
    return !control.value
      ? null
      : this.validateRegStartDateAndTime(
        control['_parent'],
        'pastRegStartTime'
      );
  }
  regStartDateHandler() {
    if (this.Step7.value.isRegStartDate) {
      const regStartDtValidatorFn = this.fb.control(
        '',
        Validators.compose([
          Validators.required,
          this.ValidateRegStartDate.bind(this),
        ])
      );

      const regStartTimeValidatorFn = this.fb.control(
        '',
        Validators.compose([
          Validators.required,
          this.ValidateRegStartTime.bind(this),
        ])
      );
      this.Step7.addControl('regStartDate', regStartDtValidatorFn);
      this.Step7.addControl('regStartTime', regStartTimeValidatorFn);
    } else {
      this.Step7.removeControl('regStartDate');
      this.Step7.removeControl('regStartTime');
    }
  }
  dateValidation(selectedDate) {
    if (selectedDate == 'start') {
      // this.insertCalenderStyle('.startDate', 'white');
      if (this.Step7.value.startDate) {
        this.maxCheckInOpenDateValue = this.Step7.value.startDate;
        this.maxCheckInClosingDateValue = this.Step7.value.startDate;
      }
    }

    if (selectedDate == 'open') {
      // this.insertCalenderStyle('.checkInOpeningDate', 'white');
      if (this.Step7.value.checkInStartDate) {
        this.minCheckInClosingDateValue = this.Step7.value.checkInStartDate;
        this.minStartDateValue = this.Step7.value.checkInEndDate
          ? this.Step7.value.checkInEndDate
          : this.Step7.value.checkInStartDate;
      }
    }
    if (selectedDate == 'close') {
      // this.insertCalenderStyle('.checkInClosingDate', 'white');
      if (this.Step7.value.checkInEndDate) {
        this.maxCheckInOpenDateValue = this.Step7.value.checkInEndDate;
        this.minStartDateValue = this.Step7.value.checkInEndDate;
      }
    }
  }
  setMinDate() {
    let date = new Date();
    let year: any = date.getFullYear();
    let month: any = date.getMonth() + 1;
    month = month < 10 ? '0' + month : month;
    let day: any = date.getDate();
    day = day < 10 ? '0' + day : day;
    let d = `${year}-${month}-${day}`;
    return d;
  }
  regEndDateHandler(isEndDtRequired: boolean = false) {
    if (this.Step7.value.isRegEndDate) {
      const regEndDateValidatorFn = this.fb.control(
        '',
        Validators.compose([
          Validators.required,
          this.ValidateRegEndDate.bind(this),
        ])
      );
      const regEndTimeValidatorFn = this.fb.control(
        '',
        Validators.compose([
          Validators.required,
          this.ValidateRegEndTime.bind(this),
        ])
      );

      this.Step7.addControl('regEndDate', regEndDateValidatorFn);
      this.Step7.addControl('regEndTime', regEndTimeValidatorFn);
    } else {
      this.Step7.removeControl('regEndDate');
      this.Step7.removeControl('regEndTime');
    }
  }

  ValidateRegEndTime(control: AbstractControl): { [key: string]: any } | null {
    return !control.value
      ? null
      : this.validateRegEndDateAndTime(control['_parent'], 'pastRegEndTime');
  }

  handleCheckInDate(val) {
    const checkInStartDate: FormControl = this.Step7.get('checkInStartDate');
    const startDate = this.Step7.get('startDate');
    const startTime = this.Step7.get('startTime');
    const { hour, minute } = this.getHoursAndMinutes(startTime);
    let startDt = new Date(startDate?.value);
    startDt.setHours(hour);
    startDt.setMinutes(minute);
    startDt = addMinutes(startDt, -val);
    checkInStartDate.setValue(startDt.toISOString());
    this.Step7.get('regEndDate').updateValueAndValidity();
  }

  validateRegEndDateAndTime(
    formGroup: FormGroup,
    ekey: string
  ): { [key: string]: boolean } {
    const {
      regStartDate,
      regStartTime: _regStartTime,
      regEndDate,
      regEndTime: _regEndTime,
      startDate,
      startTime: _startTime,
      noOfRound,
      setDuration,
      bracketType,
      isCheckInRequired,
      checkInStartDate,
      endDate,
      endTime: _endTime,
      isRegStartDate,
    } = formGroup.controls;

    let startTime = { value: null };
    let regStartTime = { value: null };
    let regEndTime = { value: null };
    let endTime = { value: null };
    startTime.value = this.getHoursAndMinutes(_startTime);
    regStartTime.value = this.getHoursAndMinutes(_regStartTime);
    endTime.value = this.getHoursAndMinutes(_endTime);
    regEndTime.value = this.getHoursAndMinutes(_regEndTime);

    if (
      (ekey == 'pastRegEndDate' && !regEndDate?.value) ||
      (ekey == 'pastRegEndTime' && !regEndTime?.value)
    ) {
      return { required: true };
    } else if (
      regEndDate?.value &&
      regEndTime?.value &&
      startDate?.value &&
      startTime?.value &&
      endDate?.value &&
      endTime?.value
    ) {
      const regEndDt = new Date(regEndDate?.value);
      regEndDt.setHours(regEndTime?.value?.hour);
      regEndDt.setMinutes(regEndTime?.value?.minute);

      let regstartDt = new Date();

      if (regStartDate?.value && regStartTime?.value) {
        regstartDt = new Date(regStartDate?.value);
        regstartDt.setHours(regStartTime?.value?.hour);
        regstartDt.setMinutes(regStartTime?.value?.minute);
      }

      let tournamentStartDt, tournamentStartDate;
      tournamentStartDt = new Date(startDate.value);
      tournamentStartDt.setHours(startTime?.value?.hour);
      tournamentStartDt.setMinutes(startTime?.value?.minute);
      if (bracketType?.value == 'swiss_safeis') {
        const tournamentDuration =
          setDuration?.value.days * 24 * 60 +
          setDuration?.value.hours * 60 +
          setDuration?.value.mins;
        const timeToBeAdded = (noOfRound?.value - 1) * tournamentDuration;
        tournamentStartDate = new Date(
          tournamentStartDt.getTime() + timeToBeAdded * 60000
        );
      } else if (bracketType?.value == 'ladder') {
        tournamentStartDt = new Date(endDate.value);
        tournamentStartDt.setHours(endTime?.value?.hour);
        tournamentStartDt.setMinutes(endTime?.value?.minute);
        tournamentStartDate = tournamentStartDt;
      } else {
        tournamentStartDate = tournamentStartDt;
      }
      let checkInStartDt;
      if (isCheckInRequired?.value && checkInStartDate?.value) {
        checkInStartDt = new Date(checkInStartDate?.value);
      }

      if (checkInStartDt < regEndDt) {
        return { pastCheckIn: true };
      }

      if (regEndDt > regstartDt && regEndDt <= tournamentStartDate) {
        this.Step7.get(
          ekey == 'pastRegEndDate' ? 'regEndTime' : 'regEndDate'
        ).setErrors(null);
        return null;
      } else if (ekey == 'pastRegEndDate' && regstartDt == regEndDt) {
        return null;
      } else {
        const invalidField = this.getErrorKey(bracketType?.value, ekey);
        return invalidField;
      }
    }
    return null;
  }
  getErrorKey = (
    brackeyType: string,
    key: string
  ): { [key: string]: boolean } => {
    const hasRegistrationEndDtKey =
      key == 'pastRegEndTime' || key == 'pastRegEndDate';
    if (brackeyType == 'swiss_safeis' && hasRegistrationEndDtKey) {
      const k =
        key == 'pastRegEndDate' ? 'swissPastRegEndDate' : 'swissPastRegEndTime';
      return {
        [k]: true,
      };
    } else if (brackeyType == 'ladder' && hasRegistrationEndDtKey) {
      const k =
        key == 'pastRegEndDate'
          ? 'ladderPastRegEndDate'
          : 'ladderPastRegEndTime';
      return {
        [k]: true,
      };
    } else {
      return { [key]: true };
    }
  };
  checkInDateHandler(event) {
    this.Step7.controls.checkInStartDate.setValue('');
    if (this.Step7.value.isCheckInRequired) {
      this.Step7.controls['checkInStartDate'].setValidators([
        Validators.required,
        this.ValidateCheckInDate.bind(this),
      ]);
      this.Step7.controls['checkInStartDate'].updateValueAndValidity();
    } else {
      this.Step7.controls['checkInStartDate'].setValidators([]);
      this.Step7.controls['checkInStartDate'].updateValueAndValidity();
    }
  }
  ValidateCheckInDate(control: AbstractControl): { [key: string]: any } | null {
    return control.value ? this.validateCheckedIn(control['_parent']) : null;
  }
  validateCheckedIn(formGroup: FormGroup): { [key: string]: boolean } {
    const {
      startDate,
      startTime,
      isCheckInRequired,
      checkInStartDate,
    } = formGroup.controls;

    if (
      startDate?.value &&
      startTime?.value &&
      isCheckInRequired?.value &&
      checkInStartDate?.value
    ) {
      const checkInStart = new Date(startDate.value);
      checkInStart.setHours(startTime.value.hour);
      checkInStart.setMinutes(startTime.value.minute);
      const checkInTime = this.checkInTimeOptions.find(
        (el) => el.name === checkInStartDate.value
      );
      checkInStart.setHours(checkInStart.getHours() - checkInTime?.value?.hour);
      checkInStart.setMinutes(
        checkInStart.getMinutes() - checkInTime?.value?.minute
      );
      if (checkInStart.getTime() < Date.now()) {
        return { checkInExpired: true };
      }
    }
    return null;
  }

  patchEditFormValues() {
    if (!this.Step7?.get('_id')?.value) {
      return;
    }
    const tournamentData = this.eSportsTournamentService?.manageTournamentSubject?.getValue();
    this.sponsors = tournamentData.sponsors;

    this.bracketChangeHandler(tournamentData.bracketType);
    if (!this.Step7?.get('stageBracketType')) {
      this.addNewControl('stageBracketType', '');
    }

    if (tournamentData.isCheckInRequired && tournamentData.checkInStartDate) {
      const minutes = differenceInMinutes(
        new Date(tournamentData.startDate),
        new Date(tournamentData.checkInStartDate)
      );
      if (!this.Step7.get('checkInTime')) {
        this.Step7.addControl('checkInTime', new FormControl(''));
      }
      this.Step7.get('checkInTime').setValue(minutes);
    }

    if (tournamentData?.noOfPlacement) {
      this.placementCounter(tournamentData.noOfPlacement).then(() => { });
    }
    this.Step7?.patchValue(tournamentData);
    this.Step7?.get('gameDetail')?.setValue(tournamentData?.gameDetail?._id);

    this.onGameChange(tournamentData?.gameDetail?._id);
    this.Step7?.get('platform')?.setValue(tournamentData?.platform);

    const teamFormatSelected = this.teamFormat.find((ele) => {
      return ele.id == tournamentData?.teamSize?.toString();
    });
    if (teamFormatSelected) {
      this.Step7?.get('teamFormat')?.setValue(teamFormatSelected.id);
    } else {
      this.Step7?.get('teamFormat')?.setValue(this.teamFormat[2].id);
    }
    if (!this.Step7.get('startTime')?.value) {
      this.updateStartTime();
    }

    if (!this.Step7?.value?.isPaid) {
      this.Step7?.removeControl('regFee');
      this.Step7?.removeControl('regFeeCurrency');
    }

    if (!this.Step7?.value?.isPrize) {
      this.Step7?.removeControl('prizeList');
      this.Step7?.removeControl('prizeCurrency');
    }

  }

  ngOnDestroy() {
    this.eSportsTournamentService.manageTournamentSubject.next(null);
  }

  updateStartTime() {
    const startDate = this.Step7.get('startDate')?.value;
    if (startDate) {
      const date = new Date(startDate);
      const hours24 = date?.getHours();
      const mins = date?.getMinutes();
      let hours12 = hours24;
      let AMPM = 'AM';
      if (hours24 > 12) {
        AMPM = 'PM';
        hours12 = hours24 - 12;
      }
      this.Step7.get('startTime')?.setValue(`${hours12}:${mins} ${AMPM}`);
    }
  }

  /**
   *
   * @param givenTime - format -> 12:30 AM
   * @returns {Object} { hour, minute }
   */
  getHoursAndMinutes(givenTime): { hour: number; minute: number } {
    if (!givenTime) {
      return null;
    }
    const myArr = givenTime?.value?.split(' ');
    let AMPM = myArr[1];
    let time = myArr[0]?.split(':');
    let hour: number = Number(time[0]);
    let minute = Number(time[1]);
    if (hour == 12 && AMPM.toUpperCase() == 'AM') {
      hour = 0;
    }

    if (hour != 12 && hour != 0) {
      AMPM == 'PM' || AMPM == 'pm' || AMPM == 'Pm' || AMPM == 'pM'
        ? (hour = hour + 12)
        : '';
    }
    return { hour, minute };
  }

  setDates(formGroup: FormGroup) {
    const {
      regStartDate,
      regStartTime: _regStartTime,
      regEndDate,
      regEndTime: _regEndTime,
      startDate,
      startTime: _startTime,
      endDate,
      endTime: _endTime,
    } = formGroup.controls;

    if (startDate?.valid && _startTime?.valid) {
      const startDt = new Date(startDate.value);
      const { hour, minute } = this.getHoursAndMinutes(_startTime);
      startDt.setHours(hour);
      startDt.setMinutes(minute);
      startDate.setValue(startDt.toISOString());
    }

    if (endDate?.valid && _endTime?.valid) {
      const endDt = new Date(endDate.value);
      const { hour, minute } = this.getHoursAndMinutes(_endTime);
      endDt.setHours(hour);
      endDt.setMinutes(minute);
      endDate.setValue(endDt.toISOString());
    }

    if (regEndDate?.valid && _regEndTime?.valid) {
      const regEndDt = new Date(regEndDate.value);
      const { hour, minute } = this.getHoursAndMinutes(_regEndTime);
      regEndDt.setHours(hour);
      regEndDt.setMinutes(minute);
      regEndDate.setValue(regEndDt.toISOString());
    }

    if (regStartDate?.valid && _regStartTime?.valid) {
      const regStartDt = new Date(regStartDate?.value);
      const { hour, minute } = this.getHoursAndMinutes(_regStartTime);
      regStartDt.setHours(hour);
      regStartDt.setMinutes(minute);
      regStartDate?.setValue(regStartDt?.toISOString());
    }
  }

  maxParticipantHandler(value, bracketType) {
    if (['battle_royale', 'round_robin'].includes(bracketType)) {
      const maxValidator =
        bracketType == 'round_robin'
          ? [Validators.max(value >= 24 ? 24 : value)]
          : [Validators.max(value)];
      this.Step7.get('noOfTeamInGroup').setValidators([
        Validators.required,
        Validators.min(2),
        ...maxValidator,
        this.teamInGroupValidation('noOfTeamInGroup').bind(this),
      ]);
      this.Step7.get('noOfWinningTeamInGroup').setValidators([
        Validators.required,
        Validators.min(1),
        this.teamInGroupValidation('noOfWinningTeamInGroup').bind(this),
      ]);
      this.Step7.get('noOfTeamInGroup').updateValueAndValidity();
      this.Step7.get('noOfWinningTeamInGroup').updateValueAndValidity();
    }
  }

  pushPublishTag(){
    this.pushGTMTags('Publish_Complete', {
      gameTitle: this.selectedGameDetails.name,
      platform: this.platformList.find(ele=> ele?._id == this.Step7?.value?.platform)?.name
    })
  }

  pushGTMTags(eventName: string, eventData: EventProperties = null) {
    this.gtmService.pushGTMTags(eventName, eventData);
  }
}
