// import { UserService } from './../../../../core/service/user.service';
import { Component, Input, OnInit } from '@angular/core';
import {
  ControlContainer,
  FormControl,
  FormGroupDirective,
  FormGroup,
} from '@angular/forms';
import { EsportsToastService ,EsportsUserService} from 'esports';

@Component({
  selector: 'app-send-invite',
  templateUrl: './send-invite.component.html',
  styleUrls: ['./send-invite.component.scss'],
  viewProviders: [
    { provide: ControlContainer, useExisting: FormGroupDirective },
  ],
})
export class SendInviteComponent implements OnInit {
  @Input() isOnPreviewPage: boolean = false;

  Step6: FormGroup|null;
  userList = [];
  invitedUser = [];
  emailInvite = [];

  constructor(
    private eSportsUserService: EsportsUserService,
    private eSportsToastService: EsportsToastService ,
    private tournament: FormGroupDirective
  ) {}

  ngOnInit(): void {
    this.Step6 = this.tournament?.form;
  }

  onSearch = async (v) => {
    try {
      if(!v) this.userList = [];
      const data = await this.eSportsUserService.searchUsers(v).toPromise();
      this.userList = data;
    } catch (error) {
      const errorMessage = error?.error?.message || error?.message;
      this.eSportsToastService.showError(errorMessage);
    }
  };

  selectedUser = (user) => {
    const invitedParticipant = this.Step6?.value?.participants;
    const isExist = invitedParticipant.find((el) => {
      return el?._id == user?._id;
    });
    if (isExist) return;
    invitedParticipant.push(user);
    this.Step6?.get('participants')?.setValue(invitedParticipant);
  };

  removeInvitedUser(i: number): void {
    const invitedParticipant = this.Step6?.value?.participants;
    invitedParticipant.splice(i, 1);
    this.Step6?.get('participants')?.setValue(invitedParticipant);
  }

  emitEmailInvite(value: string): void {
    const emailInvite = this.Step6?.value?.emailInvite;
    const isExist = emailInvite.find((el: string) => {
      return el == value;
    });
    if (isExist) return;
    emailInvite.push(value);
    this.Step6?.get('emailInvite')?.setValue(emailInvite);
  }

  removeEmailInvite(i: number): void {
    const emailInvite = this.Step6?.value?.emailInvite;
    emailInvite.splice(i, 1);
    this.Step6?.get('emailInvite')?.setValue(emailInvite);
  }

  onStepChange(step: number): void {
    this.Step6?.get('step')?.setValue(step);
  }
}
