import { Component, OnInit } from '@angular/core';
import { FormGroup, FormGroupDirective } from '@angular/forms';

@Component({
  selector: 'app-entrance-fee-amount',
  templateUrl: './entrance-fee-amount.component.html',
  styleUrls: ['./entrance-fee-amount.component.scss'],
})
export class EntranceFeeAmountComponent implements OnInit {

  Step8: FormGroup|null;
  currecnyTypes = [{_id:'CAD',name:'CAD'}]
  constructor(private tournament: FormGroupDirective) {}

  ngOnInit(): void {
    this.Step8 = this.tournament?.form;
  }

  onEnter(event: any): void {
    if (this.Step8.get('regFee').valid) {
      this.Step8.get('step').setValue(9);
    }
  }

  onStepChange(step: number): void {
    if (this.Step8.get('regFee').invalid) return;
    this.Step8.get('step').setValue(step);
  }
}
