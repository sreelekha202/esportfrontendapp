import { Component, OnInit } from '@angular/core';
import {
  ControlContainer,
  FormControl,
  FormGroupDirective,
  Validators,
} from '@angular/forms';
import { EsportsTournamentService } from 'esports';

@Component({
  selector: 'app-entrance-fee',
  templateUrl: './entrance-fee.component.html',
  styleUrls: ['./entrance-fee.component.scss'],
  viewProviders: [
    { provide: ControlContainer, useExisting: FormGroupDirective },
  ],
})
export class EntranceFeeComponent implements OnInit {
  Step6;
  isPaidFlag = false;
  list = [
    { name: 'Yes', value: true },
    { name: 'No', value: false },
  ];

  constructor(
    private tournament: FormGroupDirective,
    private eSportsTournamentService: EsportsTournamentService
  ) {}

  ngOnInit(): void {
    this.Step6 = this.tournament.form;
    this.Step6.addControl('isPaid', new FormControl(false));
    if (this.Step6.get('isPaid').value) this.isPaidFlag = true;
  }

  onToggleEntranceFee(value): void {
    this.Step6.get('isPaid').setValue(value);
    this.paidRegistrationHandler(value);
  }

  paidRegistrationHandler(check) {
    if (check) {
      const regCurrencyValidators = Validators.compose([Validators.required]);
      const regFeeValidators = Validators.compose([
        Validators.required,
        Validators.min(1),
      ]);
      this.Step6.addControl('regFeeCurrency', new FormControl('CAD', regCurrencyValidators));
      this.Step6.addControl('regFee', new FormControl(1, regFeeValidators));
    } else {
      this.Step6.removeControl('regFeeCurrency');
      this.Step6.removeControl('regFee');
    }
  }

  onStepChange(step) {
    if (this.Step6.get('isPaid').value
        && step > this.Step6.get('step').value
        && !this.isPaidFlag) {
      this.eSportsTournamentService.totalPages.next(
        this.eSportsTournamentService.totalPages.value + 1
      );
    }
    this.Step6.get('step').setValue(step);
  }
}
