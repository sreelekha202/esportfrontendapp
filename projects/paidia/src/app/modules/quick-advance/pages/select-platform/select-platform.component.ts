import { Component, OnInit } from '@angular/core';
import {
  ControlContainer,
  FormGroup,
  FormGroupDirective,
  Validators,
} from '@angular/forms';
import { EsportsTournamentService } from 'esports';
import { Router } from '@angular/router';
import { AppHtmlRoutes } from '../../../../app-routing.model';
@Component({
  selector: 'app-select-platform',
  templateUrl: './select-platform.component.html',
  styleUrls: ['./select-platform.component.scss'],
  viewProviders: [
    { provide: ControlContainer, useExisting: FormGroupDirective },
  ],
})
export class SelectPlatformComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;
  Step3: FormGroup | null;
  flag: boolean = true;
  isTeamSize: boolean = false;
  teamFormat = [
    {
      id: '2',
      name: '2v',
      icon: 'assets/icons/matchmaking/team-format/duo.svg',
      teamSize: 2,
    },
    {
      id: '4',
      name: '4v',
      icon: 'assets/icons/matchmaking/team-format/squad.svg',
      teamSize: 4,
    },
    {
      id: 'n',
      name: 'x vs x',
      icon: 'assets/icons/matchmaking/team-format/x_vs_x.svg',
      teamSize: 5,
    },
  ];
  tournamentFormat = [
    {
      name: 'Single Player',
      type: 'single',
      icon: 'assets/icons/matchmaking/platforms/single-player.svg',
      title: 'Single',
      createdBy: 'admin',
      createdOn: '2020-08-19T12:56:15.185Z',
      status: '1',
      updatedBy: 'admin',
      updatedOn: '2020-08-19T12:56:15.185Z',
      _id: 'individual',
      is_selected: false,
    },
    {
      name: 'Team',
      type: 'team',
      icon: 'assets/icons/matchmaking/platforms/team.svg',
      title: 'team',
      createdBy: 'admin1',
      createdOn: '2020-08-19T12:56:15.185Z',
      status: '2',
      updatedBy: 'admin',
      updatedOn: '2020-08-19T12:56:15.185Z',
      _id: 'team',
      is_selected: false,
    },
  ];
  constructor(
    public eSportsTournamentService: EsportsTournamentService,
    private tournament: FormGroupDirective,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.Step3 = this.tournament.form;
  }

  onSelectPlatform(id: string) {
    this.Step3?.get('platform')?.setValue(id);
  }
  onSelectFormate(id: string) {
    this.Step3?.get('participantType')?.setValue(id);
  }
  onSelectTeamFormate(id: string) {
    this.Step3?.get('teamFormat')?.setValue(id);
    if (this.Step3.get('teamFormat').value == this.teamFormat[2]['id'])
      this.isTeamSize = true;
    else {
      this.isTeamSize = false;
      this.setTeamSize(id);
    }
  }

  setTeamSize(id: string) {
    if (this.teamFormat[0]['id'] == id) {
      this.Step3?.get('teamSize')?.setValue(2);
    }
    if (this.teamFormat[1]['id'] == id) {
      this.Step3?.get('teamSize')?.setValue(4);
    }
  }

  onStepChange(step: number) {
    if (this.Step3.get('participantType').value == 'team') {
      this.flag = false;
      this.Step3.get('teamFormat').clearValidators();
      this.Step3.get('teamFormat').setValidators(
        Validators.compose([Validators.required])
      );
      this.Step3.get('teamFormat').updateValueAndValidity();
      this.Step3.get('teamSize').clearValidators();
      this.Step3.get('teamSize').updateValueAndValidity();
      if (!this.isTeamSize) {
        if (
          !this.Step3?.get('participantType')?.invalid &&
          !this.Step3?.get('platform')?.invalid &&
          !this.Step3?.get('teamFormat')?.invalid
        ) {
          this.Step3?.get('step')?.setValue(step);
        }
      } else {
        this.Step3.get('teamSize').clearValidators();
        this.Step3.get('teamSize').setValidators(
          Validators.compose([Validators.required])
        );
        this.Step3.get('teamSize').updateValueAndValidity();
        if (
          !this.Step3?.get('participantType')?.invalid &&
          !this.Step3?.get('platform')?.invalid &&
          !this.Step3?.get('teamFormat')?.invalid &&
          !this.Step3?.get('teamSize')?.invalid
        ) {
          this.Step3?.get('step')?.setValue(step);
        }
      }
    } else if (this.Step3.get('participantType').value == 'individual') {
      this.Step3.get('teamFormat').clearValidators();
      this.Step3.get('teamFormat').updateValueAndValidity();
      if (
        !this.Step3?.get('participantType')?.invalid &&
        !this.Step3?.get('platform')?.invalid
      ) {
        this.Step3?.get('step')?.setValue(step);
      }
    }
  }

  handleBackClick(step) {
    let prevStep = this.tournament.form?.value?.step - 1;
    if(prevStep == 0) {
      this.router.navigate([AppHtmlRoutes.createTournament]);
    }
      this.tournament.form.get('step').setValue(prevStep);
  }
}
