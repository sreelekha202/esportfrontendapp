import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { timeFormatAMPM } from '../../../../shared/comon';
import {
  ControlContainer,
  FormGroup,
  FormGroupDirective,
  FormControl,
  Validators,
} from '@angular/forms';
import { EsportsToastService } from 'esports';
import { TournamentService } from '../../../../core/service';
import { Router } from '@angular/router';
import { AppHtmlRoutes } from '../../../../app-routing.model';
@Component({
  selector: 'app-select-date',
  templateUrl: './select-date.component.html',
  styleUrls: ['./select-date.component.scss'],
  viewProviders: [
    { provide: ControlContainer, useExisting: FormGroupDirective },
  ],
})
export class SelectDateComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;
  Step5: FormGroup | null;
  minDate: Date | null;
  createTournament: any;
  date: any;
  constructor(
    private tournament: FormGroupDirective,
    private tournamentService: TournamentService,
    private toastService: EsportsToastService,
    private router: Router
  ) {}
  time;

  ngOnInit(): void {
    this.Step5 = this.tournament?.form;
    this.setMinimumDate();
    if(!this.Step5?.get('noOfParticipants'))
    this.Step5.addControl('noOfParticipants', new FormControl(0,[]));


    if(!this.Step5?.get('startTime'))
    this.Step5.addControl('startTime', new FormControl('', Validators.required));

    if(!this.Step5?.get('endDate'))
    this.Step5.addControl('endDate', new FormControl('', Validators.required));


    // if (this.Step5?.get('startDate')?.value) {
    //   this.time = timeFormatAMPM(this.Step5?.get('startDate')?.value);
    // }

    if(this.Step5?.value.startTime){
      this.time = this.Step5?.get('startTime').value;
    }
  }

  setMinimumDate() {
    const date = new Date();
    date.setMinutes(date.getMinutes() + 5);
    date.setSeconds(0);
    this.minDate = date;
  }

  // onEnter(event: any): void {
  //   if (this.time) {
  //     this.date = this.Step5?.get('startDate')?.value;
  //     const myArr = this.time.split(" ");
  //     let AMPM = myArr[1];
  //     let time = myArr[0].split(":")
  //     let hour: number = Number(time[0])
  //     let minute = Number(time[1]);
  //     (AMPM == 'PM' || AMPM == 'pm' || AMPM == 'Pm' || AMPM == 'pM') ? hour = hour + 12 : '';
  //     const date = new Date(this.date);
  //     date.setHours(hour);
  //     date.setMinutes(minute);
  //     const diff = Date.now() - date.getTime();
  //     this.date = date;

  //     this.Step5?.get('startDate').setValue(this.date);
  //     if (this.Step5?.get('startDate')?.invalid) return;
  //     this.Step5.get('step').setValue(this.Step5?.get('step')?.value + 1);
  //   }

  // }
  onTimeChange(t) {
    this.time = t;
    // if (!this.Step5.get('startTime')) {
    //   this.Step5.addControl('startTime', new FormControl(''));
    // }
    // this.Step5.get('startTime').setValue(t);
  }
  onStepChange(step: number): void {
    // if (this.Step5?.get('startDate')?.invalid && this.Step5?.get('step')?.value < step) return;
    if (this.time) {
      this.date = this.Step5?.get('startDate')?.value;
      const myArr = this.time.split(' ');
      let AMPM = myArr[1];
      let time = myArr[0].split(':');
      let hour: number = Number(time[0]);
      let minute = Number(time[1]);

      if (hour == 12 && AMPM.toUpperCase() == 'AM') {
        hour = 0;
      }

      if (hour != 12 && hour != 0) {
        AMPM == 'PM' || AMPM == 'pm' || AMPM == 'Pm' || AMPM == 'pM'
          ? (hour = hour + 12)
          : '';
      }

      const date = new Date(this.date);
      date.setHours(hour);
      date.setMinutes(minute);
      const diff = Date.now() - date.getTime();
      this.date = date;
      this.Step5?.get('startDate').setValue(this.date);
      if (this.Step5?.get('startDate')?.invalid) return;
      if (diff < 300000) {
        this.date = this.Step5?.get('startDate')?.value;
        this.Step5?.get('step')?.setValue(step);
      } else {
        this.toastService.showError(
          'Start time should be 5 minute more than current time'
        );
      }
      this.Step5.get('step').setValue(this.Step5?.get('step')?.value);
    } else {
      this.toastService.showError('Please select the Date-time.');
    }
  }
  handleBackClick(step) {
    let prevStep = this.tournament.form?.value?.step - 1;
    if(prevStep == 0) {
      this.router.navigate([AppHtmlRoutes.createTournament]);
    }
      this.tournament.form.get('step').setValue(prevStep);

  }
}
