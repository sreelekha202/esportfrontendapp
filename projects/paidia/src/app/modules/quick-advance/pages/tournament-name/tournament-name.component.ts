import { Component, Input, OnInit } from '@angular/core';
import {
  EsportsConstantsService, EsportsToastService,
  EsportsTournamentService
} from 'esports';
import {
  ControlContainer,
  FormBuilder,
  FormGroup,
  FormGroupDirective,
} from '@angular/forms';
import { Router } from '@angular/router';
import { AppHtmlRoutes } from '../../../../app-routing.model';
@Component({
  selector: 'app-tournament-name',
  templateUrl: './tournament-name.component.html',
  styleUrls: ['./tournament-name.component.scss'],
  viewProviders: [
    { provide: ControlContainer, useExisting: FormGroupDirective },
  ],
})

export class TournamentNameComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;
  Step1: FormGroup | null;

  constructor(private tournament: FormGroupDirective,
    private eSportsToastService: EsportsToastService,
    private fb: FormBuilder,
    private router: Router
    ) {}

  ngOnInit(): void {
    this.Step1 = this.tournament.form

  }

  onEnter(event: any): void {
    const { value, invalid } = this.Step1;
    if (this.Step1.get('name').valid) {
      this.Step1.get('step').setValue(2);
    }
  }

  onStepChange(step: number): void {
    const { value, invalid } = this.Step1;
    if (this.Step1.get('name').valid) {
      this.Step1.get('step').setValue(step);
    }
    else{

      if (invalid) {
        this.eSportsToastService.showError('please fill required details')
        return;
      }
    }
  }


  handleBackClick(step) {
    let prevStep = this.tournament.form?.value?.step - 1;
    if(prevStep == 0) {
      this.router.navigate([AppHtmlRoutes.createTournament]);
    }
      this.tournament.form.get('step').setValue(prevStep);

  }
}
