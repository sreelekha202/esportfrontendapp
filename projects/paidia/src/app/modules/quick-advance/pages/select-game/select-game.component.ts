import { Component, OnInit } from '@angular/core';

import {
  ControlContainer,
  FormControl,
  FormGroup,
  FormGroupDirective,
} from '@angular/forms';
import { EsportsTournamentService } from 'esports';
import { environment } from '../../../../../environments/environment';
import { Router } from '@angular/router';
import { AppHtmlRoutes } from '../../../../app-routing.model';
const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;
@Component({
  selector: 'app-select-game',
  templateUrl: './select-game.component.html',
  styleUrls: ['./select-game.component.scss'],
  viewProviders: [
    { provide: ControlContainer, useExisting: FormGroupDirective },
  ],
})
export class SelectGameComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;
  Step2: FormGroup | null;
  text: string = '';
  games = [];
  showLoader: boolean = true;

  constructor(
    public eSportsTournamentService: EsportsTournamentService,
    private tournament: FormGroupDirective,
    private router: Router,
  ) {}

  async ngOnInit(): Promise<any> {
    this.Step2 = this.tournament.form;
    let game = this.eSportsTournamentService?.getGameList;
    if(!game || !game.length){
      const response = await this.eSportsTournamentService.getGames();
      this.eSportsTournamentService.setGameList = response?.data;
      game = this.eSportsTournamentService?.getGameList;
    }

    game.map((data) => {
      this.games.push({ ...data, sname: data.name.toLowerCase().trim() });
    });
  }

  onSelectGame(game: any) {
    this.eSportsTournamentService.setPlatformList = game?.platform;
    if (game?.isQuickFormatAllow && this.Step2?.value?.type == 'quick') {
      this.Step2.addControl('noOfTeamInGroup', new FormControl(''));
      this.Step2.addControl('noOfWinningTeamInGroup', new FormControl(''));
      this.Step2.addControl('noOfRoundPerGroup', new FormControl(''));
      this.Step2.addControl('stageBracketType', new FormControl(''));
      this.Step2.patchValue({
        ...game.quickFormatConfig,
      });
    } else {
      this.eSportsTournamentService.setBracketTypes = game?.bracketTypes;
      this.Step2.addControl('teamSize', new FormControl(''));
      this.Step2.patchValue({
        ...game.quickFormatConfig,
      });
    }

    this.Step2.get('gameDetail').setValue(game?._id);
  }

  onStepChange(step: number): void {
    if (
      this.Step2?.get('gameDetail')?.invalid &&
      this.Step2?.get('step')?.value < step
    )
      return;
    this.Step2?.get('step')?.setValue(step);
  }

  handleBackClick(steps) {
    let prevStep = this.tournament.form?.value?.step - 1;
    if(prevStep == 0) {
      this.router.navigate([AppHtmlRoutes.createTournament]);
    }

      this.tournament.form.get('step').setValue(prevStep);

  }

  onTextChange = (data) => {
    this.text = data;
    // search on API
    //  this.getGames();
  };

  // search on Array
  filterGame = () => {
    if (this.Step2?.value.type == 'quick') {
      return this.games?.filter((game) => game?.isQuickFormatAllow && game.sname.includes(this.text.toLowerCase().trim())) || [];
    }
    return this.games?.filter((game) =>
      game.sname.includes(this.text.toLowerCase().trim())
    );
  };
}
