import { Component, Input, OnInit } from '@angular/core';
import {
  ControlContainer,
  FormControl,
  FormGroupDirective,
  Validators,
  FormBuilder,
  ValidatorFn,
  FormArray,
  FormGroup
} from '@angular/forms';
import { EsportsTournamentService } from 'esports';
import { Router } from '@angular/router';
import { AppHtmlRoutes } from '../../../../app-routing.model';
@Component({
  selector: 'app-prize-money',
  templateUrl: './prize-money.component.html',
  styleUrls: ['./prize-money.component.scss'],
  viewProviders: [
    { provide: ControlContainer, useExisting: FormGroupDirective },
  ],
})
export class PrizeMoneyComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;
  Step5;
  isPrizeFlag =false;
  list = [
    { name: 'Yes', value: true },
    { name: 'No', value: false },
  ];

  constructor(
    private tournament: FormGroupDirective,
    private eSportsTournamentService: EsportsTournamentService,
    private fb: FormBuilder,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.Step5 = this.tournament.form;
    this.Step5.addControl('isPrize', new FormControl(false));
    if (this.Step5.get('isPrize').value) this.isPrizeFlag = true;
  }

  onSelect(value) {
    this.Step5.get('isPrize').setValue(value);
    this.prizeClickHandler(value, 1);
  }

  prizeClickHandler(checked, prizeLen = 1) {
    if (checked) {
      const formControl = this.Step5.get('prizeList');
      if (formControl) return;

      this.Step5.addControl(
        'prizeList',
        this.fb.array([], this.customPrizeValidator())
      );
      for (let i = 0; i < prizeLen; i++) this.addPrize(i);
      this.Step5.addControl(
        'prizeCurrency',
        new FormControl('SAR', Validators.compose([Validators.required]))
      );
    } else {
      this.Step5.removeControl('prizeList');
      this.Step5.removeControl('prizeCurrency');
    }
  }

  customPrizeValidator(): ValidatorFn {
    return (formArray: FormArray): { [key: string]: any } | null => {
      let total = 0;
      for (let item of formArray.controls) {
        total = total + item.value.value;
      }
      const maxParticipants =
        formArray['_parent']?.controls?.maxParticipants?.value;
      const prizeLimit = maxParticipants;
      if (formArray.controls.length > prizeLimit) {
        return { prizeLimit: true };
      }
      const isPrizeValid = formArray.controls.length ? total > 0 : true;
      return isPrizeValid ? null : { prizeMoneyRequired: true };
    };
  }

  addPrize(i): void {
    const prizeList = this.Step5.get('prizeList') as FormArray;
    const { maxParticipants } = this.Step5.value;
    let name = '';
    if (prizeList.controls.length < maxParticipants) {
      switch (true) {
        case prizeList.controls.length == 0:
          name = '1st';
          break;
        case prizeList.controls.length == 1:
          name = '2nd';
          break;
        case prizeList.controls.length == 2:
          name = '3rd';
          break;
        default:
          name = `${prizeList.controls.length + 1}th`;
          break;
      }
      const createPrizeForm = (): FormGroup => {
        const validator = [Validators.required, Validators.pattern('^[0-9]*$')];
        if (i == 0) {
          validator.push(Validators.min(1));
        }
        return this.fb.group({
          name: [name],
          value: ['', Validators.compose(validator)],
        });
      };
      prizeList.push(createPrizeForm());
    } else {
    }
  }

  onStepChange(step) {
    if (
      this.Step5.get('isPrize').value &&
      step > this.Step5.get('step').value && !this.isPrizeFlag
    ) {
      this.eSportsTournamentService.totalPages.next(
        this.eSportsTournamentService.totalPages.value + 1
      );
    }
    this.Step5.get('step').setValue(step);
  }

  handleBackClick(step) {
    let prevStep = this.tournament.form?.value?.step - 1;
    if(prevStep == 0) {
      this.router.navigate([AppHtmlRoutes.createTournament]);
    }
      this.tournament.form.get('step').setValue(prevStep);
  }
}
