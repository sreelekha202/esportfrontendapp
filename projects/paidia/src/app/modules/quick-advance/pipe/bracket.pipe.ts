import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'bracketPipe',
})
export class BracketPipe implements PipeTransform {
  transform(value) {
    const bracketTypes = {
      single: 'assets/icons/tournament/bracket-types/select.svg',
      double: 'assets/icons/tournament/bracket-types/double.svg',
      round_robin: 'assets/icons/tournament/bracket-types/round-robin.svg',
      ladder:'assets/icons/tournament/bracket-types/ladder.svg'
    };

    return (
      bracketTypes[value] ||
      'assets/icons/tournament/bracket-types/round-robin.svg'
    );
  }
}
