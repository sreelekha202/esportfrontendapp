import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-input-search',
  templateUrl: './input-search.component.html',
  styleUrls: ['./input-search.component.scss'],
})
export class InputSearchComponent implements OnInit {
  @Input() placeholder: string = '';
  @Output() emitValue = new EventEmitter();
  @Input() text: string = '';
  @Output() onTextChange = new EventEmitter<string>();
  timeoutId = null;

  constructor() {}
  ngOnInit(): void {}
  onTextChang = () => this.onTextChange.emit(this.text)
  onSearch(value) {
    const emitValue = () => {
      // if (!value) return;
      this.emitValue.emit(value?.trim() || '');
    };
    clearTimeout(this.timeoutId);
    this.timeoutId = setTimeout(emitValue, 500);
  }
  
}
