import { HttpClient } from '@angular/common/http';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ControlContainer, FormBuilder, FormGroup, FormGroupDirective, Validators } from '@angular/forms';
import { EsportsUserService } from 'esports';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-tournament-type',
  templateUrl: './tournament-type.component.html',
  styleUrls: ['./tournament-type.component.scss'],
  viewProviders: [
    { provide: ControlContainer, useExisting: FormGroupDirective },
  ],
})
export class TournamentTypeComponent implements OnInit {
  @Input() customFormControlName: string;
  countryList: any = [];
  addForm: FormGroup;
  isOffline = false
  types = [
    {
      type: 'online',
      description:
        'You are hosting an online tournament',
    },
    {
      type: 'offline',
      description:
        'You are hosting an offline tournament',
    },
    {
      type: 'hybrid',
      description:
        'Your tournament is being hosted both online and offline',
    },
  ];

  constructor( private http: HttpClient,  private fb: FormBuilder,   private userService: EsportsUserService,  ) {
    this.addForm = this.fb.group({
      fullName: [null, Validators.compose([Validators.required])],
      country: [null, Validators.compose([Validators.required])],
    });
    this.userService.getAllCountries().subscribe((res) => {
      this.countryList = res.countries;
    });

  }

  ngOnInit(): void {}


offline(type){
  if(type=='offline'){
    this.isOffline = !this.isOffline
  }
  if(type=='online'){
    this.isOffline = false
  }
  if(type=='hybrid'){
    this.isOffline = false
  }
}


}
