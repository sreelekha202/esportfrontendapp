import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-input-text-default',
  templateUrl: './input-text-default.component.html',
  styleUrls: ['./input-text-default.component.scss'],
})
export class InputTextDefaultComponent implements OnInit {
  @Input() customFormGroup: FormGroup;
  @Input() customFormControlName: string;
  @Input() title: string = '';
  @Input() placeholder: string = '';
  @Input() value: string | number = '';
  @Input() isBasic: boolean = false;
  @Input() isDisabled: boolean = false;
  @Input() type: string = 'text';

  @Output() submit = new EventEmitter();
  constructor() {}

  ngOnInit(): void {}
  // to handle Enter event
  onSubmit(event) {
    if (event?.keyCode == 13) {
      this.submit.next(true);
    }
  }
}
