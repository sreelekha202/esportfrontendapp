import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-input-date-only',
  templateUrl: './input-date-only.component.html',
  styleUrls: ['./input-date-only.component.scss'],
})
export class InputDateOnlyComponent implements OnInit {
  @Input() customFormGroup: FormGroup;
  @Input() customFormControlName: string;
  @Input() title: string = '';
  @Input() placeholder: string = '';
  @Input() value: string | number = '';
  @Input() isBasic: boolean = false;
  @Input() isDisabled: boolean = false;
  @Input() minDate;
  constructor() {}

  ngOnInit(): void {}
}
