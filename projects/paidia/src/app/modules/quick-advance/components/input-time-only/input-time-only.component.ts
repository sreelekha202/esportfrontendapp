import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-input-time-only',
  templateUrl: './input-time-only.component.html',
  styleUrls: ['./input-time-only.component.scss'],
})
export class InputTimeOnlyComponent implements OnInit {
  @Input() customFormGroup: FormGroup;
  @Input() customFormControlName: string;
  @Input() title: string = '';
  @Input() placeholder: string = '';
  @Input() value: string | number = '';
  @Input() isBasic: boolean = false;
  @Input() isDisabled: boolean = false;
  @Input() minDate;
  constructor() {}

  ngOnInit(): void {}
}


// import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
// import { FormGroup } from '@angular/forms';
// import { timeFormatAMPM } from 'dist/esports/lib/shared/comon';

// @Component({
//   selector: 'app-input-time-only',
//   templateUrl: './input-time-only.component.html',
//   styleUrls: ['./input-time-only.component.scss'],
// })
// export class InputTimeOnlyComponent implements OnInit {
//   @Input() time: any = timeFormatAMPM(new Date())
//   @Output() timeChange = new EventEmitter();
//   constructor() { }
//   ngOnInit(): void { }
//   onTimeChange = () => {
//     this.timeChange.emit(this.time)
//   }
// }
