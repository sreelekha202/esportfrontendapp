import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {
  ControlContainer,
  FormGroup,
  FormGroupDirective,
} from '@angular/forms';

@Component({
  selector: 'app-input-number',
  templateUrl: './input-number.component.html',
  styleUrls: ['./input-number.component.scss'],
  viewProviders: [
    { provide: ControlContainer, useExisting: FormGroupDirective },
  ],
})
export class InputNumberComponent implements OnInit {
  @Input() customFormGroup: FormGroup;
  @Input() customFormControlName: string;
  @Input() title: string = '';
  @Input() isDisabled: boolean = false;

  @Output() submit = new EventEmitter();
  @Output() valChange = new EventEmitter();

  constructor() {}

  ngOnInit(): void {}

  // to handle Enter event
  onSubmit(event) {
    if (event?.keyCode == 13) {
      this.submit.next(true);
    }
  }

  onChange(val) {
    this.valChange.next(val);
  }
}
