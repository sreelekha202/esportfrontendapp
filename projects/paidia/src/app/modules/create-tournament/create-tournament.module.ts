import { RouterBackModule } from './../../shared/directives/router-back.module';
import { LoadingModule } from './../../core/loading/loading.module';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SendInviteComponent } from './pages/send-invite/send-invite.component';
import { MatExpansionModule } from '@angular/material/expansion';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { FormComponentModule } from '../../shared/components/form-component/form-component.module';
import { HeaderPaidiaModule } from '../../shared/components/header-paidia/header-paidia.module';
import { CreateTournamentComponent } from './create-tournament.component';
import { CreateTournamentRoutingModule } from './create-tournament-routing.module';
import { SelectDateComponent } from './pages/select-date/select-date.component';
import { SelectGameComponent } from './pages/select-game/select-game.component';
import { SelectPlatformComponent } from './pages/select-platform/select-platform.component';
import { SelectTypeComponent } from './pages/select-type/select-type.component';
import { TournamentDetailsComponent } from './pages/tournament-details/tournament-details.component';
import { TournamentNameComponent } from './pages/tournament-name/tournament-name.component';
import { InputDateComponent } from './components/input-date/input-date.component';
import { InputInviteComponent } from './components/input-invite/input-invite.component';
import { InputSearchComponent } from './components/input-search/input-search.component';
import { InputTimeComponent } from './components/input-time/input-time.component';
import { PopupCreatedComponent } from './components/popup-created/popup-created.component';
import { TournamentDateComponent } from './components/tournament-date/tournament-date.component';
import { TournamentImageComponent } from './components/tournament-image/tournament-image.component';
import { TournamentNameInputComponent } from './components/tournament-name-input/tournament-name-input.component';
import { TournamentTimeComponent } from './components/tournament-time/tournament-time.component';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/modules/shared.module';
import { BracketTreeModule } from '../../shared/components/bracket-tree/bracket-tree.module';
import { ParticipantComponent } from './components/participant/participant.component';
import { WYSIWYGEditorModule } from 'esports';
import { environment } from '../../../environments/environment';
const components = [
  CreateTournamentComponent,
  InputDateComponent,
  InputInviteComponent,
  InputSearchComponent,
  InputTimeComponent,
  PopupCreatedComponent,
  SendInviteComponent,
  ParticipantComponent,
  SelectDateComponent,
  SelectGameComponent,
  SelectPlatformComponent,
  SelectTypeComponent,
  TournamentDateComponent,
  TournamentDetailsComponent,
  TournamentImageComponent,
  TournamentNameComponent,
  TournamentNameInputComponent,
  TournamentTimeComponent,
];

const modules = [
  WYSIWYGEditorModule.forRoot(environment),
  CommonModule,
  CreateTournamentRoutingModule,
  FormComponentModule,
  HeaderPaidiaModule,
  MatExpansionModule,
  NgxMaterialTimepickerModule,
  SharedModule,
  ReactiveFormsModule,
  RouterBackModule,
  BracketTreeModule
];

@NgModule({
  declarations: components,
  imports: modules,
})
export class CreateTournamentModule {}
