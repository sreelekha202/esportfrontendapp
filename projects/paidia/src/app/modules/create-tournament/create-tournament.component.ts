import { Component, OnInit } from '@angular/core';
import { AppHtmlRoutes } from '../../app-routing.model';

@Component({
  selector: 'app-create-tournament',
  templateUrl: './create-tournament.component.html',
  styleUrls: ['./create-tournament.component.scss'],
})
export class CreateTournamentComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;

  constructor() {}

  ngOnInit(): void {this.clearStorage()}
  clearStorage() {
    localStorage.removeItem("date");
    localStorage.removeItem("time");
    localStorage.removeItem("selectedPlatformIndex1");
    localStorage.removeItem("selectedPlatformIndex");
    localStorage.removeItem("name");
    localStorage.removeItem("selectedgame");
  }
}
