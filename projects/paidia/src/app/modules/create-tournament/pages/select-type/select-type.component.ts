import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { TournamentService } from 'projects/paidia/src/app/core/service';
import { AppHtmlRoutes } from '../../../../app-routing.model';

@Component({
  selector: 'app-select-type',
  templateUrl: './select-type.component.html',
  styleUrls: ['./select-type.component.scss'],
})
export class SelectTypeComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;
  tournamentDetails: Subscription;
  createTournament: any;
  type = [{ label: "Quick tournament", value: 1 }, { label: " Advanced Tournament ", value: 2 }];
  constructor(
    private tournamentService: TournamentService,
    private router: Router) { }

  ngOnInit(): void {
    this.tournamentDetails = this.tournamentService.createTournament.subscribe((data) => {
      if (data) {
        this.createTournament = data;
      }
    });
  }

  selectType(value: any) {
    if (localStorage.getItem("DUID")) {
      this.tournamentService.createTournamentSubject.next({ ...this.createTournament, "tournament_type": this.type[value] })
      if (value == 0) {
        this.router.navigateByUrl("create-tournament/game");
      } else if (value == 1) {
        this.router.navigateByUrl("/advance-tournament");
      }
    } else {
      this.router.navigateByUrl("user/email-login");
    }
  }
  ngOnDestroy(): void {
    if (this.tournamentDetails)
      this.tournamentDetails.unsubscribe();
  }
}
