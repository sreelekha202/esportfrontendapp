import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SendInvitiesComponent } from './send-invities.component';

describe('SendInvitiesComponent', () => {
  let component: SendInvitiesComponent;
  let fixture: ComponentFixture<SendInvitiesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SendInvitiesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SendInvitiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
