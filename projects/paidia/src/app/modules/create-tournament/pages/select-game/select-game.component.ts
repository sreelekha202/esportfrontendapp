import { TournamentService } from 'projects/paidia/src/app/core/service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Subscription } from 'rxjs';
import { AppHtmlRoutes } from '../../../../app-routing.model';
import { EsportsToastService, EsportsGameService } from 'esports';
import { environment } from '../../../../../environments/environment';
const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;
@Component({
  selector: 'app-select-game',
  templateUrl: './select-game.component.html',
  styleUrls: ['./select-game.component.scss'],
})
export class SelectGameComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;
  tournamentDetails: Subscription;
  createTournament: any;
  games = [];
  text: string = '';
  selectedGame = null;
  showLoader: boolean = true;


  constructor(
    private tournamentService: TournamentService,
    private router: Router,
    private toastService: EsportsToastService,
    private gameService: EsportsGameService) { }

  ngOnInit(): void {

    this.tournamentDetails = this.tournamentService.createTournament.subscribe((data) => {
      if (data) {
        this.createTournament = data;
      }
    });
    this.getGames();
  }

  onTextChange = (data) => {
    this.text = data;
    // search on API
    //  this.getGames();
  }


  // search on Array
  filterGame = () => {
    return this.games.filter((game) => game.sname.includes(this.text.toLowerCase().trim()));
  }

  getGames() {
    this.showLoader = true;
    const param: any = {};
    this.text ? param.search = this.text : ''
    // this.gameService.getAllGames(API,`?search=${this.text}`).subscribe((res) => {
    this.gameService.getGames(API).subscribe((res) => {
      this.showLoader = false;
      if (res && res.data && res.data.length) {
        res.data.map((obj) => {
          this.games.push({ ...obj, sname: String(obj.name.toLowerCase().trim()) })
          this.setGame(JSON.parse(localStorage.getItem('selectedgame')));
        })
      }
    }, (err) => { this.showLoader = false; })
  }

  next() {
    if (this.selectedGame) {
      localStorage.setItem("selectedgame", JSON.stringify(this.selectedGame))
      this.tournamentService.createTournamentSubject.next({ ...this.createTournament, "selectedGame": this.selectedGame })
      this.router.navigateByUrl("create-tournament/name")
    } else {
      this.toastService.showError("Please select the game.")
    }
  }

  setGame(game) {
    this.selectedGame = game;
  }
  ngOnDestroy(): void {
    if (this.tournamentDetails)
      this.tournamentDetails.unsubscribe();
  }
}
