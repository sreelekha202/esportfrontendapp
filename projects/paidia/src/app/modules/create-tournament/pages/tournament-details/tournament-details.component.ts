import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import {
  EsportsToastService,
  EsportsTournamentService,
  EsportsGtmService,
  EventProperties,
} from 'esports';
import { environment } from '../../../../../environments/environment';
import { TournamentService } from '../../../../core/service';
import { EsportsUserService, EsportsGameService } from 'esports';
import { PopupCreatedComponent } from '../../components/popup-created/popup-created.component';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
// import { ConfirmComponent } from '../../popups/confirm/confirm.component';
import moment from 'moment';
import { ConfirmComponent } from '../../../quick-advance/popups/confirm/confirm.component';
const API = environment.apiEndPoint;
@Component({
  selector: 'app-tournament-details',
  templateUrl: './tournament-details.component.html',
  styleUrls: ['./tournament-details.component.scss'],
})
export class TournamentDetailsComponent implements OnInit {
  text: string;
  ckConfig: any;
  games = [];

  date = '';
  name = ' ';
  selectedPlatformIndex = '';
  game: any;
  time = '';
  gameid = '';
  platformId = '';
  TournamentID = '';
  currentUser: any;
  Tournmenttype = [];
  platforms = [];
  rules: any;
  isScreenshotRequired: boolean = false;
  isParticipantsLimit: boolean = true;
  isShowCountryFlag: boolean = false;
  scoreReporting: number = 2;
  visibility: number = 1;
  bra_date: any;
  participants: any = [];
  form: FormGroup;
  imgurl = '';
  platformId1: string;
  TournamentID1: string;
  selectedPlatformIndex1: any;
  emailsforinvite: any = [];
  date1: any;
  time1: any;
  par_limit: Number = 2;
  isSpecifyAllowedRegions: boolean = false;
  games1: any = [];
  selectedcontries: any = [];
  createTournament: any;
  start_date: any;
  showLoader: boolean = false;
  minDate = new Date();

  todayDate: Date = new Date(
    new Date().setFullYear(new Date().getFullYear() - 10)
  );
  parentalDate: Date = new Date(
    new Date().setFullYear(new Date().getFullYear() - 18)
  );
  parental = false;
  isBracketLoaded = false;
  bracketType: any;
  structure: any;
  description = ``;
  editorConfig = {};
  constructor(
    public matDialog: MatDialog,
    private gameService: EsportsGameService,
    private userService: EsportsUserService,
    public tournamentService: TournamentService,
    private router: Router,
    private toastService: EsportsToastService,
    private eSportsTournamentService: EsportsTournamentService,
    private gtmService: EsportsGtmService,
    private http: HttpClient
  ) {}

  ngOnInit(): void {
    this.showLoader = true;
    let countries = [];
    this.games1 = [];
    this.http.get('assets/json/countries.json').subscribe(
      (data: any) => {
        countries = data.countries;
        for (let d of countries) {
          d.checked = false;
          //  this.games1.push(d)
        }
        this.games1 = countries;
        this.showLoader = false;
        //this.products = data;
      },
      (err) => {
        this.showLoader = false;
      }
    );
    this.getGames();
    this.date = localStorage.getItem('date');
    this.TournamentID = localStorage.getItem('Tournmenttype');
    this.name = localStorage.getItem('name');
    this.game = localStorage.getItem('selectedgame');
    this.time = localStorage.getItem('time');
    this.game = JSON.parse(this.game);
    this.gameid = this.game?._id;
    this.tournamentService.createTournament.subscribe(async (data) => {
      if (data) {
        this.createTournament = data;
        if (
          this.createTournament.selectedGame &&
          this.createTournament.selectedGame.quickFormatConfig
        ) {
          this.isParticipantsLimit =
            this.createTournament.selectedGame.quickFormatConfig.isParticipantsLimit;
          this.start_date = data.start_date;
          this.par_limit =
            this.createTournament.selectedGame.quickFormatConfig.maxParticipants;
          this.bracketType =
            this.createTournament.selectedGame.quickFormatConfig.bracketType;
          this.platformId = this.createTournament.selected_platform._id;
          let b = {
            bracketType: this.bracketType,
            maximumParticipants: this.par_limit,
            noOfSet: 1,
          };

          const response = await this.eSportsTournamentService.generateBracket(
            b
          );
          this.structure = response.data;
          this.isBracketLoaded = true;
        }
      } else {
        this.router.navigateByUrl('/play');
      }
    });
    // this.ckConfig = {
    //   toolbar: {
    //     items: [
    //       'heading',
    //       '|',
    //       'fontfamily',
    //       'fontsize',
    //       '|',
    //       'alignment',
    //       '|',
    //       'fontColor',
    //       'fontBackgroundColor',
    //       '|',
    //       'bold',
    //       'italic',
    //       'strikethrough',
    //       'underline',
    //       'subscript',
    //       'superscript',
    //       '|',
    //       'link',
    //       '|',
    //       'outdent',
    //       'indent',
    //       '|',
    //       'bulletedList',
    //       'numberedList',
    //       'todoList',
    //       '|',
    //       'code',
    //       'codeBlock',
    //       '|',
    //       'insertTable',
    //       '|',
    //       'uploadImage',
    //       'blockQuote',
    //       '|',
    //       'undo',
    //       'redo',
    //       'mediaEmbed',
    //     ],
    //     shouldNotGroupWhenFull: true,
    //   },
    //   heading: {
    //     options: [
    //       {
    //         model: 'paragraph',
    //         title: 'Paragraph',
    //         class: 'ck-heading_paragraph',
    //       },
    //       {
    //         model: 'heading1',
    //         view: 'h1',
    //         title: 'Heading 1',
    //         class: 'ck-heading_heading1',
    //       },
    //       {
    //         model: 'heading2',
    //         view: 'h2',
    //         title: 'Heading 2',
    //         class: 'ck-heading_heading2',
    //       },
    //     ],
    //   },
    //   mediaEmbed: {
    //     previewsInData: true,
    //   },
    // };
    this.editorConfig = {
      editable: true,
      spellcheck: true,
      height: '180px',
      width: 'auto',
      minWidth: '0',
      translate: 'yes',
      enableToolbar: true,
      showToolbar: true,
      defaultParagraphSeparator: '',
      defaultFontName: '',
      defaultFontSize: '',
      fonts: [
        { class: 'Arial', name: 'Arial' },
        { class: 'redhat-regular', name: 'redhat-regular' },
        { class: 'redhat-bold', name: 'redhat-bold' },
        { class: 'comic-sans-ms', name: 'Comic Sans MS' },
      ],

      customClasses: [
        {
          name: 'quote',
          class: 'quote',
        },
        {
          name: 'redText',
          class: 'redText',
        },
        {
          name: 'titleText',
          class: 'titleText',
          tag: 'h1',
        },
      ],
      sanitize: true,
      toolbarPosition: 'top',
      toolbarHiddenButtons: [['bold', 'italic'], ['fontSize']],
    };
  }

  parentalCheckBox(date: any) {
    const selectedDate = new Date(date.value);
    if (selectedDate > this.parentalDate && selectedDate < this.todayDate) {
      this.parental = true;
      this.form.get('dobPolicy').setValidators(Validators.pattern('true'));
      this.form.get('dobPolicy').setValue(true);
    } else {
      this.parental = false;
      this.form.get('dobPolicy').setValidators(Validators.pattern('false'));
      this.form.get('dobPolicy').setValue(false);
    }

    this.form.get('dobPolicy').updateValueAndValidity();
  }

  getNotification(event) {
    this.isScreenshotRequired = event.checked;
  }

  getNotification5(event) {
    this.isSpecifyAllowedRegions = event.checked;
  }

  getNotification6(event) {
    this.selectedcontries = event;
  }

  getNotification1(event) {
    this.isParticipantsLimit = event.checked;
  }

  getNotification2(event) {
    this.isShowCountryFlag = event.checked;
  }
  getNotification3(event) {
    this.scoreReporting = event;
  }
  getNotification4(event) {
    this.visibility = event;
  }

  bracket(data) {
    this.bra_date = data;
  }

  bracket1(data) {
    if (data) {
      this.participants = data;
    }
  }

  datatoimg(data) {
    this.imgurl = data;
  }

  emitValuefor(data) {
    this.emailsforinvite.push(data);
  }

  onDateChange() {}

  datefun1() {}

  getGames() {
    this.showLoader = true;
    const param: any = {};
    this.text ? (param.search = this.text) : '';
    this.gameService.getGames(API).subscribe(
      (res) => {
        this.games = res.data;
        this.showLoader = false;
        for (let data of this.games) {
          if (data._id == this.gameid) {
            this.platforms = data.platform;
            this.Tournmenttype = data.Tournmenttype;
          }
        }
        let i = 0;

        for (let data1 of this.platforms) {
          this.selectedPlatformIndex = localStorage.getItem(
            'selectedPlatformIndex'
          );
          if (i == Number(this.selectedPlatformIndex)) {
            this.platformId = data1._id;
          }
        }

        for (let data1 of this.Tournmenttype) {
          this.selectedPlatformIndex = localStorage.getItem(
            'selectedPlatformIndex'
          );
          if (i == Number(this.selectedPlatformIndex)) {
            this.TournamentID = data1._id;
          }
        }
      },
      (err) => {
        this.showLoader = false;
      }
    );

    this.selectedPlatformIndex1 = localStorage.getItem(
      'selectedPlatformIndex1'
    );
    if (0 == Number(this.selectedPlatformIndex1)) {
      this.platformId1 = 'individual';
    }
    if (1 == Number(this.selectedPlatformIndex1)) {
      this.platformId1 = 'team';
    }
  }

  onDateChange2() {}

  popUpTitleAndText = async (data) => {
    if (data?._id) {
      return {
        title: 'TOURNAMENT.UPDATE',
        text: 'TOURNAMENT.UPDATE_TXT',
      };
    } else {
      return {
        title: 'TOURNAMENT.SAVE_TOURNAMENT',
        text: 'TOURNAMENT.STATUS_2',
      };
    }
  };

  showCreatedPopup = async () => {
    this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.currentUser = data;
      }
    });
    let countries = [];
    this.selectedcontries.map((obj) => {
      countries.push(obj.name);
    });

    if (this.name) {
      if (
        this.time1 != undefined &&
        this.date1 != undefined &&
        this.time1 != '' &&
        this.date1 != ''
      ) {
        const myArr = this.time1.split(' ');
        let AMPM = myArr[1];
        let time = myArr[0].split(':');
        let hour: number = Number(time[0]);
        let minute = Number(time[1]);
        if (hour == 12 && AMPM.toUpperCase() == 'AM') {
          hour = 0;
        }

        if (hour != 12 && hour != 0) {
          AMPM == 'PM' || AMPM == 'pm' || AMPM == 'Pm' || AMPM == 'pM'
            ? (hour = hour + 12)
            : '';
        }
        const date = new Date(this.date1);
        date.setHours(hour);
        date.setMinutes(minute);
        this.date1 = date;
        let currentDate = new Date();
        let datatosend = {
          name: this.name,
          url: 'https://dev.stcplay.gg/tournament/t22_7_21One',
          participantType: this.platformId1,
          Tournmenttype: this.TournamentID1,
          startDate: moment.utc(this.start_date),
          // "startTime": this.time,
          endDate: moment.utc(this.date1),
          // "endTime": this.time1,
          isPaid: false,
          isRegStartDate: false,
          isRegEndDate: false,
          description: this.description,
          rules: this.rules,
          criticalRules: '',
          isPrize: false,
          faqs: '',
          schedule: '',
          isIncludeSponsor: false,
          tournamentType: 'online',
          type: 'quick',
          isScreenshotRequired: this.isScreenshotRequired,
          isShowCountryFlag: this.isShowCountryFlag,
          isManualApproverParticipant: false,
          isSpecifyAllowedRegions: this.isSpecifyAllowedRegions,
          isParticipantsLimit: this.isParticipantsLimit,
          scoreReporting: this.scoreReporting,
          disableManualScoreUpdate: this.scoreReporting == 2 ? false : true,
          SpecifyAllowedRegions: this.selectedcontries,
          invitationLink: '',
          youtubeVideoLink: '',
          participants: this.participants,
          facebookVideoLink: '',
          contactDetails: '',
          twitchVideoLink: '',
          visibility: this.visibility,
          checkInEndDate: '',
          regionsAllowed: countries,
          sponsors: [],
          banner: this.imgurl,
          maxParticipants: this.par_limit,
          bracketType: this.bracketType,
          noOfSet: '1',
          bracket_data: this.bra_date,
          stageMatch: '',
          stageMatchNoOfSet: '',
          isAllowMultipleRounds: false,
          venueAddress: [],
          contactOn: '',
          gameDetail: this.gameid,
          teamSize:
            this.createTournament.selectedGame.quickFormatConfig.teamSize,
          whatsApp: '',
          prizeList: [],
          substituteMemberSize: 0,
          platform: this.platformId,
          //"Tournmenttype":this.Tournmenttype,
          allowSubstituteMember: false,
          allowAdvanceStage: false,
          step: 6,
          // "timezone": "IST",
          isCheckInRequired: false,
          checkInStartDate: '',
          isAllowThirdPlaceMatch: false,
          createdBy: this.currentUser._id,
          updatedBy: this.currentUser._id,
          tournamentStatus: 'submitted_for_approval',
          organizerDetail: this.currentUser._id,
          noOfTeamInGroup: 0,
          noOfWinningTeamInGroup: 0,
          noOfRoundPerGroup: 0,
          emailInvite: this.emailsforinvite,
        };
        if (this.date1 > this.start_date && this.start_date > currentDate) {
          const confirmed = await this.matDialog
          .open(ConfirmComponent)
          .afterClosed()
          .toPromise();
          let temp: boolean = false;
          if (this.isParticipantsLimit) {
            if (this.par_limit >= 2) {
              this.showLoader = true;
              this.tournamentService.saveTournament1(datatosend).subscribe(
                (data: any) => {
                  this.pushPublishTag();
                  this.showLoader = false;
                  this.router.navigateByUrl(`/tournament/${data.data.slug}`);
                  this.matDialog.open(PopupCreatedComponent);
                  this.clearStorage();
                },
                (err) => {
                  this.showLoader = false;
                  this.toastService.showError(err.error.message);
                }
              );
            } else {
              this.toastService.showError('Minimum allowed limit is 2');
            }
          } else {
            this.showLoader = true;
            this.tournamentService.saveTournament1(datatosend).subscribe(
              (data: any) => {
                this.showLoader = false;
                this.router.navigateByUrl(`/tournament/${data.data.slug}`);
                this.matDialog.open(PopupCreatedComponent);
                this.clearStorage();
              },
              (err) => {
                this.showLoader = false;
                this.toastService.showError(err.error.message);
              }
            );
          }
        } else {
          this.toastService.showError(
            'End time should be greater than start time'
          );
        }
      } else {
        this.toastService.showError('Please select End Date and Time.');
      }
    } else {
      this.toastService.showError('pls enter tournment name ');
    }
  };

  clearStorage() {
    localStorage.removeItem('date');
    localStorage.removeItem('time');
    localStorage.removeItem('selectedPlatformIndex1');
    localStorage.removeItem('selectedPlatformIndex');
    localStorage.removeItem('name');
    localStorage.removeItem('selectedgame');
    // localStorage.removeItem('')
  }

  onReady(eventData) {
    eventData.plugins.get('FileRepository').createUploadAdapter = function (
      loader
    ) {
      return new UploadAdapter(loader);
    };
  }

  pushPublishTag() {
    this.pushGTMTags('Publish_Complete', {
      gameTitle: this?.games?.find((ele) => ele?._id == this?.gameid)?.name,
      platform: this?.platforms?.find((ele) => ele?._id == this?.platformId)?.name,
    });
  }

  pushGTMTags(eventName: string, eventData: EventProperties = null) {
    this.gtmService.pushGTMTags(eventName, eventData);
  }
}
class UploadAdapter {
  loader: any;
  constructor(loader) {
    this.loader = loader;
  }

  upload() {
    return this.loader.file.then(
      (file) =>
        new Promise((resolve, reject) => {
          var myReader = new FileReader();
          myReader.onloadend = (e) => {
            const imageData: any = {
              path: environment.articleS3BucketName,
              files: [myReader.result],
            };

            // resolve({ default: myReader.result });
            const accessToken = localStorage.getItem(environment.currentToken);
            fetch(`${environment.apiEndPoint}file-upload`, {
              method: 'POST',
              headers: {
                authorization: `Bearer ${accessToken}`,
                'Access-Control-Allow-Origin': '*',
              },
              body: JSON.stringify(imageData), // This is your file object
            })
              .then(
                (response) => response.json() // if the response is a JSON object
              )
              .then(
                (success) => {
                  resolve({ default: success.data[0]['Location'] });
                } // Handle the success response object
              )
              .catch(
                (error) => {} // Handle the error response object
              );
          };

          myReader.readAsDataURL(file);
        })
    );
  }
}
