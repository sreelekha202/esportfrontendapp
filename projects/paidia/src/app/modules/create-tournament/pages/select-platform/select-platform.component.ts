import { EsportsToastService } from 'esports';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TournamentService } from 'projects/paidia/src/app/core/service';
import { Subscription } from 'rxjs';
import { AppHtmlRoutes } from '../../../../app-routing.model';

@Component({
  selector: 'app-select-platform',
  templateUrl: './select-platform.component.html',
  styleUrls: ['./select-platform.component.scss'],
})
export class SelectPlatformComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;
  selectedPlatformIndex = null;
  selectedPlatformIndex1 = null;
  tournamentDetails: Subscription;
  platforms = [];
  gametype = [];
  createTournament: any;
  bracketType: any;
  isplatform = true
  isplatformselected:boolean
  istournamenttype:boolean
  constructor(
    private tournamentService: TournamentService,
    private toastService: EsportsToastService,
    private router: Router) { }

  ngOnInit(): void {
    this.gametype = [{
      name: "Single Player",
      type: 'single',
      icon: 'assets/icons/matchmaking/platforms/single-player.svg',
      title: 'Single',
      createdBy: "admin",
      createdOn: "2020-08-19T12:56:15.185Z",
      status: "1",
      updatedBy: "admin",
      updatedOn: "2020-08-19T12:56:15.185Z",
      _id: "aaa",
      is_selected: false
    }, {
      name: "Team",

      type: 'team',
      icon: 'assets/icons/matchmaking/platforms/team.svg',
      title: 'team',
      createdBy: "admin1",
      createdOn: "2020-08-19T12:56:15.185Z",
      status: "2",
      updatedBy: "admin",
      updatedOn: "2020-08-19T12:56:15.185Z",
      _id: "bbb",
      is_selected: false
    }

    ]
    this.tournamentDetails = this.tournamentService.createTournament.subscribe((data) => {
      if (data) {
        this.createTournament = data;

        if (this.createTournament.selectedGame && this.createTournament.selectedGame.platform && this.createTournament.selectedGame.platform.length > 0)
          this.createTournament.selectedGame.platform.map((obj) => {
            if (obj.name == 'pc') {
              this.platforms.push({
                ...obj,
                type: 'pc',
                icon: 'assets/icons/matchmaking/platforms/pc.svg',
                title: 'MATCHMAKING.SELECT_PLATFORM.PLATFORMS.PC',
              })
            }
            else if (obj.name == 'Mobile') {
              this.platforms.push({
                ...obj,
                type: 'mobile',
                icon: 'assets/icons/matchmaking/platforms/mobile.svg',
                title: 'MATCHMAKING.SELECT_PLATFORM.PLATFORMS.MOBILE',
              })
            }
            else if (obj.name == 'Other') {
              this.platforms.push({
                ...obj,
                type: 'console',
                icon: 'assets/icons/matchmaking/platforms/console.svg',
                title: 'MATCHMAKING.SELECT_PLATFORM.PLATFORMS.CONSOLE',
              })
            }
            else if (obj.name == 'Xbox') {
              this.platforms.push({
                ...obj,
                type: 'xbox',
                icon: 'assets/icons/matchmaking/platforms/xbox.svg',
                title: 'MATCHMAKING.SELECT_PLATFORM.PLATFORMS.XBOX',
              })
            }
            else if (obj.name == 'PS4') {
              this.platforms.push({
                ...obj,
                type: 'ps4',
                icon: 'assets/icons/matchmaking/platforms/ps4.svg',
                title: 'MATCHMAKING.SELECT_PLATFORM.PLATFORMS.PS4',
              })
            }
            else if (obj.name == 'PS5') {
              this.platforms.push({
                ...obj,
                type: 'ps5',
                icon: 'assets/icons/matchmaking/platforms/ps5.svg',
                title: 'MATCHMAKING.SELECT_PLATFORM.PLATFORMS.PS5',
              })
            }
            else if (obj.name == 'Xbox Series X') {
              this.platforms.push({
                ...obj,
                type: 'Xbox Series X',
                icon: 'assets/icons/matchmaking/platforms/series.svg',
                title: 'MATCHMAKING.SELECT_PLATFORM.PLATFORMS.PS5',
              })
            }
            else if (obj.name == 'Nintendo Switch') {
              this.platforms.push({
                ...obj,
                type: 'Nintendo Switch',
                icon: 'assets/icons/matchmaking/platforms/vector2.svg',
                title: 'MATCHMAKING.SELECT_PLATFORM.PLATFORMS.PS5',
              })
            }
            else if (obj.name == 'Nintendo Wii') {
              this.platforms.push({
                ...obj,
                type: 'Nintendo Wii',
                icon: 'assets/icons/matchmaking/platforms/nintendo.svg',
                title: 'MATCHMAKING.SELECT_PLATFORM.PLATFORMS.PS5',
              })
            }
            else {
              this.platforms.push({
                ...obj,
                type: 'pc',
                icon: 'assets/icons/matchmaking/platforms/pc.svg',
                title: 'MATCHMAKING.SELECT_PLATFORM.PLATFORMS.PC',
              })
            }
          })
        // if (this.createTournament.selectedGame.quickFormatConfig && this.createTournament.selectedGame.quickFormatConfig.bracketType) {
        //   this.bracketType = this.createTournament.selectedGame.quickFormatConfig.bracketType;
        //   if (this.bracketType == "single") {
        //     this.gametype[0].is_selected = true;
        //     this.selectedPlatformIndex1 = true;
        //   } else if (this.bracketType == "team") {
        //     this.gametype[1].is_selected = true;
        //     this.selectedPlatformIndex1 = true;
        //   } else {
        //     this.selectedPlatformIndex1 = false;
        //   }
        // }
        if (localStorage.hasOwnProperty("selectedPlatformIndex")) {
          this.setPlatform(Number(localStorage.getItem("selectedPlatformIndex")));
        }
        if (localStorage.hasOwnProperty("selectedPlatformIndex1")) {
          this.setPlatform2(Number(localStorage.getItem("selectedPlatformIndex1")));
        }
      }
    });
  }

  setPlatform(i) {
    this.isplatformselected = true
    this.selectedPlatformIndex = i
    if(this.isplatformselected && this.istournamenttype){
      this.isplatform = false
    }
  }
  setPlatform2(i) {
    this.istournamenttype = true
    this.selectedPlatformIndex1 = i;
    // this.gametype.map((obj)=>{obj.is_selected = false})
    // this.gametype[i].is_selected = true;
    if(this.isplatformselected && this.istournamenttype){
      this.isplatform = false
    }
  }
  next = () => {
    if (this.selectedPlatformIndex != null && this.selectedPlatformIndex != undefined && this.createTournament.selectedGame.platform[this.selectedPlatformIndex]) {
      if (this.selectedPlatformIndex1 != null) {
        localStorage.setItem("selectedPlatformIndex", this.selectedPlatformIndex)
        localStorage.setItem("selectedPlatformIndex1", this.selectedPlatformIndex1)
        this.router.navigateByUrl("create-tournament/date");
        this.tournamentService.createTournamentSubject.next({ ...this.createTournament, "selected_platform": this.createTournament.selectedGame.platform[this.selectedPlatformIndex] })
      } else {
        this.toastService.showError("Please select the tournament format.")
      }
    } else {
      this.toastService.showError("Please select the platform.")
    }
  }

  ngOnDestroy(): void {
    if (this.tournamentDetails)
      this.tournamentDetails.unsubscribe();
  }
}



