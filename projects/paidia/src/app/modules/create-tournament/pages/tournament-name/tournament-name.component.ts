import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TournamentService } from 'projects/paidia/src/app/core/service/tournament.service';
import { Subscription } from 'rxjs';
import { AppHtmlRoutes } from '../../../../app-routing.model';
import { EsportsToastService } from 'esports';

@Component({
  selector: 'app-tournament-name',
  templateUrl: './tournament-name.component.html',
  styleUrls: ['./tournament-name.component.scss'],
})
export class TournamentNameComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;
  tournamentDetails: Subscription;
  createTournament: any;
  text: string='';
  constructor(
    private tournamentService: TournamentService,
    private router: Router,
    private toastService: EsportsToastService
  ) { }

  ngOnInit(): void {
    this.text=localStorage.getItem('name')
    this.tournamentDetails = this.tournamentService.createTournament.subscribe((data) => {
      if (data) {
        this.createTournament = data;
      }
    });
  }
  onTextChange = (text) => {
    this.text = text;
    localStorage.setItem('name', text)
  }
  ngOnDestroy(): void {
    if (this.tournamentDetails)
      this.tournamentDetails.unsubscribe();
  }

  next() {
    this.text = this.text ? this.text.trim() : '';
    if (this.text) {
      this.tournamentService.createTournamentSubject.next({ ...this.createTournament, "tournament_name": this.text,"tournament_name_skip": false })
      this.router.navigateByUrl("create-tournament/platform");
    } else {
      this.toastService.showError("Tournament name is Required.")
    }
  }
  skip() {
    this.tournamentService.createTournamentSubject.next({ ...this.createTournament, "tournament_name_skip": true })
    this.router.navigateByUrl("create-tournament/platform");
  }

}
