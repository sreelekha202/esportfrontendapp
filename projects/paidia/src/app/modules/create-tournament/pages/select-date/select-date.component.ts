
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TournamentService } from 'projects/paidia/src/app/core/service';
import { Subscription } from 'rxjs-compat';
import { AppHtmlRoutes } from '../../../../app-routing.model';
import { EsportsToastService } from 'esports';

@Component({
  selector: 'app-select-date',
  templateUrl: './select-date.component.html',
  styleUrls: ['./select-date.component.scss'],
})
export class SelectDateComponent implements OnInit {

  AppHtmlRoutes = AppHtmlRoutes;
  // date: Date = new Date();
  // time: any = timeFormatAMPM(new Date())
  date: Date;
  time: any
  tournamentDetails: Subscription;
  platforms = [];
  createTournament: any;
  isTimeDate = true
  constructor(
    private tournamentService: TournamentService,
    private toastService: EsportsToastService,
    private router: Router) { }

  ngOnInit(): void {
    this.tournamentDetails = this.tournamentService.createTournament.subscribe((data) => {
      if (data) {
        this.createTournament = data;

      }
    });

    if (localStorage.hasOwnProperty("date")) {
      this.date = new Date(localStorage.getItem('date'))
    }
    if (localStorage.hasOwnProperty("date")) {
      this.time = localStorage.getItem('time')
    }


  }

  onDateChange(data) {
    this.date = data
    localStorage.setItem('date', data)
    if(this.date && this.time){
      this.isTimeDate = false
    }
  }
  onTimeChange(data) {
    this.time = data;
    localStorage.setItem('time', data)
    if(this.date && this.time){
      this.isTimeDate = false
    }
  }
  next = () => {
    if (this.date && this.time) {
      const myArr = this.time.split(" ");
      let AMPM = myArr[1];
      let time = myArr[0].split(":")
      let hour: number = Number(time[0])
      let minute = Number(time[1]);
      if (hour == 12 && AMPM.toUpperCase() == 'AM') {
        hour = 0;
      }

      if (hour != 12 && hour != 0) {
        AMPM == 'PM' || AMPM == 'pm' || AMPM == 'Pm' || AMPM == 'pM'
          ? (hour = hour + 12)
          : '';
      }
      const date = new Date(this.date);
      date.setHours(hour);
      date.setMinutes(minute);
      const diff = Date.now() - date.getTime();
      this.date = date;
      if (diff < 300000) {
        localStorage.setItem('date', String(this.date))
        this.tournamentService.createTournamentSubject.next({ ...this.createTournament, "start_date": this.date, "start_time": this.time })
        this.router.navigateByUrl("create-tournament/details");
      } else {
        this.toastService.showError("Start time should be 5 minute more than current time")
      }
    } else {
      this.toastService.showError("Please select the Date-time.")
    }
  }

  checkDateTime(): Boolean {
    return true
  }

  ngOnDestroy(): void {
    if (this.tournamentDetails)
      this.tournamentDetails.unsubscribe();
  }
}
