import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { CreateTournamentComponent } from './create-tournament.component';
import { SelectTypeComponent } from './pages/select-type/select-type.component';
import { TournamentNameComponent } from './pages/tournament-name/tournament-name.component';
import { SelectGameComponent } from './pages/select-game/select-game.component';
import { SelectPlatformComponent } from './pages/select-platform/select-platform.component';
import { SelectDateComponent } from './pages/select-date/select-date.component';
import { TournamentDetailsComponent } from './pages/tournament-details/tournament-details.component';

const routes: Routes = [
  {
    path: '',
    component: CreateTournamentComponent,
    children: [
      {
        path: '',
        component: SelectTypeComponent,
      },
      {
        path: 'game',
        component: SelectGameComponent,
      },
      {
        path: 'name',
        component: TournamentNameComponent,
      },
      {
        path: 'platform',
        component: SelectPlatformComponent,
      },
      {
        path: 'date',
        component: SelectDateComponent,
      },
      {
        path: 'details',
        component: TournamentDetailsComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CreateTournamentRoutingModule {}
