import { EsportsToastService } from 'esports';
import { Component, OnInit } from '@angular/core';
import { TournamentService } from 'projects/paidia/src/app/core/service';

@Component({
  selector: 'app-input-date',
  templateUrl: './input-date.component.html',
  styleUrls: ['./input-date.component.scss'],
})
export class InputDateComponent implements OnInit {
  createTournament: any;
  constructor(
    private tournamentService: TournamentService,
    private toastService: EsportsToastService) {
    this.tournamentService.createTournament.subscribe((data) => {
      if (data) {
        this.createTournament = data;
      }
    });
  }

  date: Date;
  time: any;
  minDate = new Date();

  ngOnInit(): void {
    if (localStorage.hasOwnProperty('date') && localStorage.getItem('date')) {
      this.date = new Date(localStorage.getItem('date'));
    }
    if (localStorage.hasOwnProperty('time') && localStorage.getItem('time')) {
      this.time = localStorage.getItem('time')
    }
  }

  onDateChange(date) {
    this.date = new Date(date);
    if (localStorage.hasOwnProperty('time') && localStorage.getItem('time')) {
      this.time = localStorage.getItem('time')
    }
    localStorage.setItem('date', String(this.date))
    if (this.date) {
      if (this.time) {
        const myArr = this.time ? this.time.split(" ") : '';
        let AMPM = myArr[1];
        let time = myArr[0].split(":")
        let hour: number = Number(time[0])
        let minute = Number(time[1]);
        (AMPM == 'PM' || AMPM == 'pm' || AMPM == 'Pm' || AMPM == 'pM') ? hour = hour + 12 : '';
        const date = new Date(this.date);
        date.setHours(hour);
        date.setMinutes(minute);
      }
      const diff = Date.now() - date.getTime();
      this.date = date;
      if (diff < 300000) {
        this.tournamentService.createTournamentSubject.next({ ...this.createTournament, "start_date": this.date, "start_time": this.time })
      } else {
        this.toastService.showError("Start time should be 5 minute more than current time")
      }
    } else {
      // this.toastService.showError("Please select the Date-Time")
    }
  }


}
