import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { timeFormatAMPM } from 'projects/paidia/src/app/shared/comon';

@Component({
  selector: 'app-tournament-time',
  templateUrl: './tournament-time.component.html',
  styleUrls: ['./tournament-time.component.scss'],
})
export class TournamentTimeComponent implements OnInit {
  @Input() time:any=timeFormatAMPM(new Date())
  @Output() timeChange = new EventEmitter();
  constructor() { }
  ngOnInit(): void { }
  onTimeChange = () => { this.timeChange.emit(this.time)
  }
}
