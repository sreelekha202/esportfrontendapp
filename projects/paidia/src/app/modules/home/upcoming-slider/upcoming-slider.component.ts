import {Component, OnInit, ViewChild } from '@angular/core';
import {AppHtmlRoutes} from '../../../app-routing.model';
import {HomeService} from '../../../core/service';
import{EsportsLanguageService} from 'esports';
import {TranslateService,LangChangeEvent} from '@ngx-translate/core';
import {NgbCarousel, NgbCarouselConfig} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-upcoming-slider',
  templateUrl: './upcoming-slider.component.html',
  styleUrls: ['./upcoming-slider.component.scss'],
  providers: [NgbCarouselConfig],
})
export class UpcomingSliderComponent implements OnInit {
  @ViewChild('carousel') carousel: NgbCarousel;

  AppHtmlRoutes = AppHtmlRoutes;

  tournamentsSlides = [];

  currentLang: string = 'english';
  currentSlide = null;

  constructor(
    private config: NgbCarouselConfig,
    private homeService: HomeService,
    private translate: TranslateService,
    public language: EsportsLanguageService
  ) {
    this.config.interval = 0;
    this.config.animation = false;
    this.currentLang = translate.currentLang == 'ms' ? 'malay' : 'english';

    translate.onLangChange.subscribe((event: LangChangeEvent) => {
      this.currentLang = event.lang == 'ms' ? 'malay' : 'english';
    });
  }

  ngOnInit(): void {
    this.getTournamentsSlides();
  }

  getTournamentsSlides() {
    this.homeService.getUpcomingSliderTournament().subscribe((res) => {
      this.tournamentsSlides = res?.data;
      this.currentSlide = this.tournamentsSlides[0];
    }, (err) => { })

  }

  setCurrentSlide(event): void {
    const index = this.getSlideIndex(event.current);
    this.currentSlide = this.tournamentsSlides[index];
  }

  getSlideIndex(str: string): number {
    return +str.replace(/[^0-9]/g, '');
  }
}
