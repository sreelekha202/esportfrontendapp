import { Component, OnInit, ViewChild } from '@angular/core';
import { AppHtmlRoutes } from '../../../app-routing.model';
import { TranslateService } from '@ngx-translate/core';
import { NgbCarousel, NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';
import { HomeService } from '../../../core/service';

@Component({
  selector: 'app-preview-slider',
  templateUrl: './preview-slider.component.html',
  styleUrls: ['./preview-slider.component.scss'],
  providers: [NgbCarouselConfig],
})
export class PreviewSliderComponent implements OnInit {
  @ViewChild('carousel') carousel: NgbCarousel;

  AppHtmlRoutes = AppHtmlRoutes;

  slides = [];

  constructor(
    private translate: TranslateService,
    private homeService: HomeService,
    private config: NgbCarouselConfig
  ) {
    this.config.interval = 0;
    this.config.animation = true;
    this.config.showNavigationArrows = false;
  }

  ngOnInit(): void {
    this.homeService._getBanner().subscribe(
      (res) => {
        this.slides = res?.data;
      },
      (err) => {}
    );
  }
}
