import { Router } from '@angular/router';
import { Component, Inject, OnInit, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { TranslateService } from '@ngx-translate/core';
import { environment } from '../../../environments/environment';
import {
  EsportsLanguageService,
  EsportsUserService,
  EsportsSocialService,
  EsportsGtmService,
  IPOST,
  IUser,
  EsportsToastService,
} from 'esports';

const LIMIT_POST = 10;
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  public isBrowser: boolean;
  public isPostDetail: boolean = false;
  public tokenUser: string = localStorage.getItem('refreshToken');
  public currentUser: IUser;
  public currLanguage: string = 'english';
  public enableHeader: boolean = true;
  public enablePrivatePost: boolean = false;
  public posts: Array<IPOST> = [];
  public showMorePostLoader: boolean = false;
  public page: number = 0;
  public pageTotal: number = 0;

  constructor(
    @Inject(PLATFORM_ID) private platformId,
    private _router: Router,
    private _userService: EsportsUserService,
    public _language: EsportsLanguageService,
    private _toastService: EsportsToastService,
    private _gtmService: EsportsGtmService,
    private _socialService: EsportsSocialService,
    public _translateService: TranslateService
  ) {
    this.isBrowser = isPlatformBrowser(platformId);
  }
  ngOnInit(): void {
    this.initLanguage();
    this.getCurrentUser();
    this.initPosts();
    this.pushGTMTags('View_Main_Menu');
  }

  public showMorePost(): void {
    this.showMorePostLoader = true;
    const skip = this.page * LIMIT_POST;
    localStorage.getItem(environment.currentToken)
      ? this.getPostAfterLogin(skip, LIMIT_POST)
      : this.getPostPreLogin(skip, LIMIT_POST);
    this.showMorePostLoader = false;
  }

  private initLanguage() {
    if (this.isBrowser) {
      this._language.language.subscribe((lang) => {
        if (lang) {
          this.currLanguage = lang === 'ms' ? 'malay' : 'english';
        }
      });
    }
  }

  private getCurrentUser(): void {
    this._userService.currentUser.subscribe(
      (data) => (this.currentUser = data),
      (err) => {
        throw err;
      }
    );
  }

  private initPosts(): void {
    const regex_URL = new RegExp('^/post/');
    if (this._router.url.match(regex_URL)) {
      this.isPostDetail = true;
      const regex_postId = new RegExp('[0-9a-fA-F]{24}');
      const postIdMatch = this._router.url.match(regex_postId);
      const postId = postIdMatch ? postIdMatch[0] : '';
      this.getPostById(postId);
    } else {
      localStorage?.getItem(environment.currentToken)
        ? this.getPostAfterLogin(0, LIMIT_POST)
        : this.getPostPreLogin(0, LIMIT_POST);
    }
  }

  private getPostAfterLogin(skip: number, limit: number) {
    this._socialService.getPostAfterLogin(skip, limit).subscribe(
      (res: any) => {
        const posts: Array<IPOST> = res?.data?.posts;
        if (!posts) throw Error();
        this.posts = [...this.posts, ...posts];
        this.page = Math.ceil(this.posts.length / 10);
        this.pageTotal = this.pageTotal = Math.ceil(res?.data?.total / 10);
      },
      (err) => {
        this._toastService.showError(
          this._translateService.instant('SOCIAL_FEED.MESSAGES.TRY_AGAIN')
        );
        throw err;
      }
    );
  }

  private getPostPreLogin(skip: number, limit: number) {
    this._socialService.getPostPreLogin(skip, limit).subscribe(
      (res: any) => {
        const posts: Array<IPOST> = res?.data?.posts;
        if (!posts) throw Error();
        this.posts = [...this.posts, ...posts];
        this.page = Math.ceil(this.posts.length / 10);
        this.pageTotal = this.pageTotal = Math.ceil(res?.data?.total / 10);
      },
      (err) => {
        throw err;
      }
    );
  }

  private getPostById(postId: string) {
    this._socialService.getPostById(postId).subscribe(
      (res) => {
        const post = res?.data;
        if (!post) throw Error();
        this.posts.push(post);
      },
      (err) => {
        this._router.navigate(['/404']);
        throw err;
      }
    );
  }

  private pushGTMTags(eventName: string, eventData = null) {
    this._gtmService.pushGTMTags(eventName, eventData);
  }
}
