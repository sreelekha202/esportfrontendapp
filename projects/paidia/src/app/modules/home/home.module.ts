import { NgModule } from '@angular/core';
import { HomeComponent } from './home.component';
import { HomeRoutingModule } from './home-routing.module';
import { PreviewSliderComponent } from './preview-slider/preview-slider.component';
import { UpcomingSliderComponent } from './upcoming-slider/upcoming-slider.component';
import { UserAsideComponent } from './user-aside/user-aside.component';
import { UserLevelComponent } from './user-level/user-level.component';
import { SharedModule } from '../../shared/modules/shared.module';
import { SliderComponent } from './slider/slider.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatDialogModule } from '@angular/material/dialog';
import { MatMenuModule } from '@angular/material/menu';
import { MatTooltipModule } from '@angular/material/tooltip';
const components = [
  HomeComponent,
  PreviewSliderComponent,
  UpcomingSliderComponent,
  UserAsideComponent,
  UserLevelComponent,
  SliderComponent,
];

const modules = [
  SharedModule,
  HomeRoutingModule,
  MatFormFieldModule,
  MatInputModule,
  MatDialogModule,
  MatMenuModule,
  MatTooltipModule
];

@NgModule({
  declarations: [...components],
  imports: [...modules],
})
export class HomeModule { }
