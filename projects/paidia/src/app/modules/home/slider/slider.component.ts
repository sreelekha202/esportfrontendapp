import { Component, OnInit } from '@angular/core';
import { HomeService } from '../../../core/service';
import { EsportsGtmService, EventProperties } from 'esports';
@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.scss'],
})
export class SliderComponent implements OnInit {
  public dataSlider = [];
  public slideItem: any = {};
  constructor(
    private _homeService: HomeService,
    private _gtmService: EsportsGtmService
  ) {}

  ngOnInit(): void {
    this.initBanner();
  }

  private initBanner() {
    this._homeService._getBanner().subscribe(
      (res) => {
        this.dataSlider = res?.data;
        this.slideItem = res?.data[0];
      },
      (err) => {}
    );
  }

  public onSilde(event) {
    const slideIndex = event.current.split('-').pop();
    this.slideItem = this.dataSlider[slideIndex];
  }

  public pushViewBannerTag(_bannerData) {
    if (_bannerData && typeof _bannerData == 'object') {
      const bannerData = JSON.parse(JSON.stringify(_bannerData));
      delete bannerData?.sub_title;
      delete bannerData?.button_text;
      delete bannerData?.__v;
      this.pushGtmTags('Banner_Click', { bannerDetails: bannerData });
    }
  }

  public pushGtmTags(eventName, eventData: EventProperties = null) {
    this._gtmService.pushGTMTags(eventName, eventData);
  }
}
