import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { IUser } from 'esports';
import { AppHtmlRoutes } from '../../../app-routing.model';
import { SettingsLightboxComponent } from '../../settings-lightbox/settings-lightbox.component';
import { EsportsGameService, EsportsUserService } from 'esports';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { UserPreferenceService, UserService } from '../../../core/service';
import { ElementAst } from '@angular/compiler';

@Component({
  selector: 'app-user-aside',
  templateUrl: './user-aside.component.html',
  styleUrls: ['./user-aside.component.scss'],
})
export class UserAsideComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;
  date: Date = new Date();

  currentYear = new Date().getFullYear();
    slectedGames:any;
  //currentYear = new Date().getFullYear();
  game: any = [];
  currentUser: IUser;
  userSubscription: Subscription;
  showLoader: boolean = true;
  games: any = [];
  gameslist: any = [];
  gameslist1: any = [];

  shortcuts = [
    {
      icon: 'videogame_asset',
      text: 'HOME.ASIDE.SHORTCUTS.NEW_TOURNAMENT',
      link: '/create-tournament',
      modifier: 'gamepad',
    },
    {
      icon: 'group',
      text: 'HOME.ASIDE.SHORTCUTS.NEW_TEAM',
      link: '/create-team',
      modifier: 'group',
    },
    {
      icon: 'edit',
      text: 'HOME.ASIDE.SHORTCUTS.REPORT',
      link: '/profile/matches',
      modifier: 'edit',
    },
    // {
    //   icon: 'person_add',
    //   text: 'HOME.ASIDE.SHORTCUTS.REFER',
    //   link: '/profile/dashboard',
    //   modifier: 'plus-user',
    // },
  ];

  constructor(
    private userPreferenceService: UserPreferenceService,
    public matDialog: MatDialog,
    private userService: EsportsUserService,
    private routes: Router,
    private gameService: EsportsGameService
  ) { }

  ngOnInit(): void {
    //this.games = this.getGames();
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
       if (data) {
        this.currentUser = data;
        this.game = data?.preference?.game;
        let i;
        this.getSelectedGames(this.game)
        for (i = 1; i < this.game.length; i++) {
          this.games.forEach(game => {
            if (game[i] == game._id) {
              this.gameslist.push(game)
            }
          });
        }
      }
    });

    // for(let i=0;i<this.game.length;i++){
    //  this.userService.getUserGames(this.game[i]).subscribe((res) => {
    //   this.games.push(res)
    //  })
    // }


  }

  getGames(api) {
    this.showLoader = true;
    this.gameService.getGames(api).subscribe((res) => {
      this.games = res?.data;
      this.showLoader = false;
    }, (err) => { this.showLoader = false; })
    return this.games
  }

  openSettingsLightbox(selectedTab = 0) {
    if (this.currentUser) {
      const data = { selectedTab, };
      this.matDialog.open(SettingsLightboxComponent, {
        data, position: { top: '34px' },
      });
    } else {
      this.routes.navigateByUrl('/user/email-login');
    }
  }

  getSelectedGames(selectedgame) {
    this.userPreferenceService.getAllSettingPageData().subscribe((res) => {
     var games = res?.data?.game;
    games.map((obj) => {
      let is_selected: boolean = false;
      this.currentUser.preference?.game.map((userGame) => {
        if (userGame == obj?._id) { is_selected = true; }
      });
      this.games.push({ ...obj, is_selected: is_selected, name: String(obj?.name).toLowerCase() })
    })
     let g=[];
     for(let i=0;i<selectedgame.length;i++){
      for(let j=0;j<this.games.length;j++){
        if(selectedgame[i]==this.games[j]?._id){
               g.push(this.games[j])
        }
      }
     }
     this.gameslist1= Array.from( g.reduce((m, t) => m.set(t.name, t), new Map()).values())
    }, (err) => { })
  }

}
