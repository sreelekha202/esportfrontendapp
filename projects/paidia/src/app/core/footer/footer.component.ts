import { Component, OnInit } from '@angular/core';
import { AppHtmlRoutes } from '../../app-routing.model';
import { environment } from '../../../environments/environment';
import { Router } from '@angular/router';
import { EsportsGtmService, EventProperties } from 'esports';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
})
export class FooterComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;
  windowScrolled: boolean;
  buildConfig = environment.buildConfig || 'N/A';

  socials = [
    {
      link: 'https://www.twitch.tv/paidiagaming',
      icon: 'assets/icons/socials/twitch.svg',
    },
    {
      link: 'https://discord.gg/KG9qBzs96d',
      icon: 'assets/icons/socials/discord.svg',
    },
    {
      link: 'https://vm.tiktok.com/ZM8nPYwNh/',
      icon: 'assets/icons/socials/tiktok.svg',
    },
    {
      link: 'https://twitter.com/PaidiaGaming',
      icon: 'assets/icons/socials/twitter.svg',
    },
    {
      link: 'https://www.youtube.com/channel/UCZYwRQ-bcixXa9OSk4tJA6A',
      icon: 'assets/icons/socials/youtube.svg',
    },
    // {
    //   link: 'https://www.facebook.com/paidiagaming',
    //   icon: 'assets/icons/socials/facebook.svg',
    // },
    {
      link: 'https://www.instagram.com/paidiagaming/',
      icon: 'assets/icons/socials/instagram.svg',
    },
    {
      link: 'https://www.linkedin.com/company/paidia-gaming/',
      icon: 'assets/icons/socials/linkedin.svg',
    },
  ];

  constructor(private router: Router, private gtmService: EsportsGtmService) {}

  ngOnInit(): void {}

  gotoPage(link) {
    document.body.style.height = '1300px';
    window.scrollTo(0, 1300);
    // window.scrollTo(0,2000);
    // window.scrollTo(0, 0)
    this.router.navigateByUrl(link);
  }

  pushFooterTags(link) {
    switch (link) {
      case AppHtmlRoutes.privacyPolicy:
        this.pushGTMTags('View_Privacy_Policy');
        break;
      case AppHtmlRoutes.terms:
        this.pushGTMTags('View_Terms');
        break;
      default:
        break;
    }
  }

  pushGTMTags(eventName, eventData: EventProperties = null) {
    this.gtmService.pushGTMTags(eventName, eventData);
  }
}
