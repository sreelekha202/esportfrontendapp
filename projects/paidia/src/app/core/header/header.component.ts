import {
  Component,
  ElementRef,
  Inject,
  OnChanges,
  OnDestroy,
  OnInit,
  PLATFORM_ID,
  Input,
} from '@angular/core';

import { DOCUMENT, isPlatformBrowser } from '@angular/common';
import { Router } from '@angular/router';

import { fromEvent, Subscription } from 'rxjs';
import { environment } from '../../../environments/environment';
import { AppHtmlProfileRoutes, AppHtmlRoutes } from '../../app-routing.model';
import { AppHtmlRoutesLoginType } from '../../app-routing.model';
import { IUser } from '../../shared/models';
import { TranslateService } from '@ngx-translate/core';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';
import {
  faBars,
  faTimes,
  faSearch,
  faBell,
} from '@fortawesome/free-solid-svg-icons';
import { MatDialog } from '@angular/material/dialog';
import { SettingsLightboxComponent } from '../../modules/settings-lightbox/settings-lightbox.component';
import {
  EsportsChatService,
  EsportsUserService,
  EsportsLanguageService,
  EsportsConstantsService,
  EsportsToastService,
  EsportsChatSidenavService,
  EsportsSocialService,
  GlobalUtils,
  EsportsGtmService,
  EventProperties,
} from 'esports';
const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;
enum AppLanguage {
  fr = 'fr',
  en = 'en',
}
@AutoUnsubscribe()
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit, OnChanges, OnDestroy {
  @Input() dataNotifi;
  activeLang = this.constantsService?.defaultLangCode;
  AppHtmlProfileRoutes = AppHtmlProfileRoutes;
  AppHtmlRoutes = AppHtmlRoutes;
  AppHtmlRoutesLoginType = AppHtmlRoutesLoginType;
  collapsed: boolean = true;
  isAdmin: boolean = false;
  isBrowser: boolean;
  isUserLoggedIn: boolean = false;
  mobMenuOpened: boolean = false;
  mobMenuOpeneds: boolean = false;
  showLoader: boolean = false;
  currentUser: IUser;
  tournaments: any;
  matchlist: any;
  matchcount: number;
  chatWins = [];
  currentUserId: any;
  AppLanguage = [];

  dataNotification;

  faBell = faBell;
  faSearch = faSearch;
  faBars = faBars;
  faTimes = faTimes;
  notifiSeen = [];
  navigation = [
    {
      title: 'HEADER.CONTENT',
      url: AppHtmlRoutes.content,
    },
    {
      title: 'HEADER.PLAY',
      url: AppHtmlRoutes.play,
    },
    {
      title: 'HEADER.MATCHMAKING',
      url: AppHtmlRoutes.matchmaking,
    },
    {
      title: 'HEADER.LEADERBOARD',
      url: AppHtmlRoutes.leaderBoard,
    },
    {
      title: 'HEADER.ABOUT',
      url: AppHtmlRoutes.aboutUs,
    },
  ];

  userLinks = [
    {
      title: 'My Account',
      icon: 'person_outline',
      url: AppHtmlProfileRoutes.dashboard,
    },
    {
      title: 'My Profile',
      icon: 'person_outline',
      url: AppHtmlProfileRoutes.privateWall,
    },
    {
      title: 'My Teams',
      icon: 'people_outline',
      url: AppHtmlProfileRoutes.myTeams,
    },
    {
      title: 'My Stats',
      icon: 'show_chart',
      url: AppHtmlProfileRoutes.myStats,
    },
    // {
    //   title: 'My Connections',
    //   icon: 'person_pin',
    //   url: AppHtmlProfileRoutes.connections,
    // },
  ];

  private sub1: Subscription;
  userSubscription: Subscription;
  constructor(
    @Inject(PLATFORM_ID) private platformId,
    @Inject(DOCUMENT) private document: Document,
    private chatService: EsportsChatService,
    private chatSidenavService: EsportsChatSidenavService,
    private elementRef: ElementRef,
    private languageService: EsportsLanguageService,
    private router: Router,
    private userService: EsportsUserService,
    public toastService: EsportsToastService,
    public constantsService: EsportsConstantsService,
    public translate: TranslateService,
    public matDialog: MatDialog,
    private gtmService: EsportsGtmService,
    public socialService: EsportsSocialService
  ) {
    this.isBrowser = isPlatformBrowser(platformId);
  }
  ngOnInit(): void {
    this.matchlist = [];
    this.activeLang = this.translate.currentLang;
    this.AppLanguage = this.constantsService?.language;
    this.matchcount = 0;

    if (
      this.isBrowser &&
      GlobalUtils.isBrowser() &&
      localStorage.getItem(environment.currentToken)
    ) {
      this.showLoader = true;

      this.userSubscription = this.userService.currentUser.subscribe(
        (data) => {
          if (data) {
            this.currentUser = data;
            this.isUserLoggedIn = !!this.currentUser;
            this.showLoader = false;
            this.currentUserId = this.currentUser._id;
            this.isAdmin =
              data?.accountType === EsportsConstantsService.AccountType.Admin;
            const accessToken = localStorage.getItem(environment.currentToken);

            this.chatService?.login.subscribe((data) => {
              // if (data.accessToken != 'undefined' && data.accessToken != null) {
              this.chatService?.connectUser();

              this.chatService.unreadCount.subscribe((unreadCount) => {
                this.matchcount = unreadCount;
              });

              this.chatService?.getUnReadCount().subscribe((res: any) => { });
              // }
            });
          } else {
            this.userService.getAuthenticatedUser(API, TOKEN);
          }
        },
        (error) => {
          this.showLoader = false;
          this.toastService.showError(error.message);
        }
      );
    }
    this.onHeaderScroll();

    this.getAllNotification();
  }

  ready() { }
  ngOnChanges() {
    if (this.dataNotifi) {
      this.getAllNotification();
    }
  }
  ngOnDestroy() {
    this.userSubscription?.unsubscribe();
    if (this.currentUserId) {
      this.chatService?.disconnectUser();
    }
  }
  getAllNotification(): void {
    if (localStorage.getItem(environment.currentToken)) {
      this.socialService.getNofitication().subscribe(
        (res: any) => {
          if (res) {
            this.dataNotification = res.data.notifications;
            this.notifiSeen = res.data.notifications.filter((item) => item.status == 1);
          }
        },
        (err) => {
          throw err;
        }
      );
    } else {
      this.dataNotification = [];
    }
  }

  detailNotification(item: any): void {
    if (item.status == 1) {
      this.seenNotifi(item)
    }
    if (item.commentId) {
      this.socialService.getCommentById(item.commentId).subscribe(
        (res: any) => {
          if (res) {
          }
        },
        (err) => {
          throw err;
        }
      );
    }
    if (item.likeId) {
      this.socialService.getLikeById(item.likeId).subscribe(
        (res: any) => {
          if (res) {
          }
        },
        (err) => {
          throw err;
        }
      );
    }
    if (item.reportId) {
      this.socialService.getReportById(item.reportId).subscribe(
        (res: any) => {
          if (res) {
          }
        },
        (err) => {
          throw err;
        }
      );
    }
  }

  onChatSidenavToggle(): void {
    this.pushGTMTags('View_User_Chats');
    this.chatSidenavService.toggle();
    if (this.currentUserId) {
      this.chatService?.getAllMatches(this.currentUserId, 1, 30);
    }

    if (this.chatSidenavService.checkOpen()) {
      if (this.chatService.getChatStatus()) {
        this.chatService.setChatStatus(false);
      }
    } else {
      if (this.chatService.getChatStatus()) {
        this.chatService.setChatStatus(false);
      }
    }
  }

  onLogOut(): void {
    if (GlobalUtils.isBrowser()) {
      this.chatService.setChatStatus(false);
      this.userService.logout(API, TOKEN);
      this.chatSidenavService.close();
      this.chatService.doLogout();
      this.chatService?.disconnectUser();
      this.pushGTMTags('Logout');
    }

    this.router.navigate([AppHtmlRoutes.landing]);
  }

  onLanguageChange(lang): void {
    if (GlobalUtils.isBrowser()) {
      localStorage.setItem('currentLanguage', lang);
      this.activeLang = lang;
      this.translate.use(lang);
      this.languageService.setLanguage(lang);
    }
  }

  private onHeaderScroll(): void {
    if (GlobalUtils.isBrowser()) {
      const scrollingContent = document.querySelector('mat-sidenav-content');

      if (scrollingContent) {
        const header = this.elementRef.nativeElement;

        this.sub1 = fromEvent(scrollingContent, 'scroll').subscribe(
          (e: Event) => {
            if ((e.target as Element).scrollTop > header.clientHeight) {
              header.classList.add('is-scrolling');
            } else {
              header.classList.remove('is-scrolling');
            }
          }
        );
      }
    }
  }

  onScrollBody() {
    const hiddenOrVisible = this.mobMenuOpened ? 'hidden' : 'visible';
    this.document.body.style.overflow = hiddenOrVisible;
  }

  openSettingsLightbox(selectedTab = 0) {

    switch (selectedTab) {
      case 0:
        this.pushGTMTags('Account_Tab_Click');
        break;

      default:
        break;
    }

    const data = { selectedTab };
    this.matDialog.open(SettingsLightboxComponent, {
      data,
      panelClass:'Account_settings'
    });
  }

  Administration() { }
  seenNotifi(item: any): void {
    this.socialService.changeStatusNotifi(item._id).subscribe(res => {
      if (res?.data) {
        item.status = 2;
        this.notifiSeen.splice(0, 1);
      }
    })
  }

  pushProfileGTMtags(data) {
    switch (data.url) {
      case AppHtmlProfileRoutes.privateWall:
        this.pushGTMTags('View_Profile', { fromPage: this.router.url });
        break;
      case AppHtmlProfileRoutes.myStats:
        this.pushGTMTags('View_User_Statistics');
        break;
      case AppHtmlProfileRoutes.myTeams:
        this.pushGTMTags('View_User_Teams');
        break;

      default:
        break;
    }
  }

  pushNavGTMTags(data) {
    switch (data.url) {
      case AppHtmlRoutes.content:
        this.pushGTMTags('View_Discover_Tab');
        break;
      case AppHtmlRoutes.matchmaking:
        this.pushGTMTags('View_Matchmaking_Tab');
        break;
      case AppHtmlRoutes.play:
        this.pushGTMTags('View_Tournaments_Tab');
        break;
      case AppHtmlRoutes.leaderBoard:
        this.pushGTMTags('View_Leaderboard_Tab');
        break;
      case AppHtmlRoutes.aboutUs:
        this.pushGTMTags('View_About_Us_Tab');
        break;
      default:
        break;
    }
  }

  pushGTMTags(eventName: string, eventData: EventProperties = null) {
    this.gtmService.pushGTMTags(eventName, eventData);
  }
}
