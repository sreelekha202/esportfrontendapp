import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';

import { IPagination } from 'esports';
import { PageChangedEvent } from 'ngx-bootstrap/pagination';
import { GlobalUtils, EsportsPaginationService } from 'esports';

@Component({
  selector: 'app-pagination',
  templateUrl: './custom-pagination.component.html',
  styleUrls: ['./custom-pagination.component.scss'],
})
export class CustomPaginationComponent implements OnInit, OnChanges {
  @Input() activePage: Number = 1;
  @Input() page: IPagination;
  @Output() currentPage = new EventEmitter<Number>();

  constructor(
    private paginationService: EsportsPaginationService,
    private globalUtils: GlobalUtils
  ) { }

  ngOnInit(): void { }

  pageChanged(event: PageChangedEvent) {
    this.currentPage.emit(event.page);
    this.paginationService.setPageChanged(true);
  }

  ngOnChanges(simpleChanges: SimpleChanges) { }

  onScrollTop() {
    if (GlobalUtils.isBrowser()) {
      window.scroll(0, 0);
    }
  }
}
