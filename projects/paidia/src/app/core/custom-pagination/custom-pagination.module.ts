import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CustomPaginationComponent } from './custom-pagination.component';
import { PaginationModule, PaginationConfig } from 'ngx-bootstrap/pagination';

@NgModule({
  declarations: [CustomPaginationComponent],
  imports: [CommonModule, PaginationModule.forRoot(), FormsModule],
  exports: [CustomPaginationComponent],
  providers: [{ provide: PaginationConfig }],
})
export class CustomPaginationModule {}
