import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Component, Inject, OnInit } from '@angular/core';
import { AppHtmlProfileRoutes } from '../../app-routing.model';

@Component({
  selector: 'app-stc-popup',
  templateUrl: './stc-popup.component.html',
  styleUrls: ['./stc-popup.component.scss'],
})
export class StcPopupComponent implements OnInit {
  AppHtmlProfileRoutes = AppHtmlProfileRoutes;
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<any>
  ) {}

  ngOnInit(): void {}

  onClose(): void {
    this.dialogRef.close(false);
  }
}
