import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";

import { Observable, SubscribableOrPromise, BehaviorSubject } from "rxjs";
import { environment } from "../../../environments/environment";
import { catchError, map } from 'rxjs/operators';

@Injectable({
  providedIn: "root",
})
export class TeamService {
  public socialFiledSubject$ = new BehaviorSubject('');
  constructor(private http: HttpClient) {
  }

  // geturserdetails(data1){
  //   const httpHeaders = new HttpHeaders();
  //   httpHeaders.set("Content-Type", "application/json");
  //   let urlString = data1+ `&select=fullName,username,phoneNumber,email,profilePicture&pagination=${encodeURIComponent(
  //       1
  //     )}limit${encodeURIComponent(
  //       10
  //     )}NextPage${encodeURIComponent(true)}`;
  //   return this.http.get(environment.apiEndPoint + urlString);
  // }

  geturserdetails(data1) {
    let params = {
      status: "2",
      page: 1,
      limit: 10,
      sort: "Oldest",
    };

    const encodedPagination = encodeURIComponent(params["pagination"]);
    const url = `${environment.apiEndPoint}/user/search_users?query=${data1}&select=fullName,username,phoneNumber,email,profilePicture,country,state`;
    return this.http.get(url)
    // const httpHeaders = new HttpHeaders();
    // httpHeaders.set("Content-Type", "application/json");
    // let urlString = data1+ `&select=fullName,username,phoneNumber,email,profilePicture&pagination=${encodeURIComponent(
    //     1
    //   )}limit${encodeURIComponent(
    //     10
    //   )}NextPage${encodeURIComponent(true)}`;
    // return this.http.get(`${environment.apiEndPoint}` + urlString);
  }

  saveteam(data1) {
    return this.http.post(`${environment.apiEndPoint}user/create_team`, data1);
  }

  searchUsers(searchKeyword, specificProjection = undefined) {
    // const query = JSON.stringify({
    //   text: { $regex: searchKeyword, $options: 'i' },
    // });
    // const query = JSON.stringify({
    //   text: searchKeyword,
    // });
    const projection =
      specificProjection && specificProjection.state == true
        ? specificProjection.read
        : 'fullName,username,phoneNumber,email,profilePicture';
    // const encodedUrl = encodeURIComponent(query);
    return this.http
      .get(environment.apiEndPoint + `user/search_users?text=${searchKeyword}&select=${projection}`)
      .pipe(
        map((value: any) => {
          return value.data;
        }),
        catchError((err) => err)
      );
  }

  getTeamMemberList(team_id) {
    return this.http.get(environment.apiEndPoint + 'user/team_member/' + team_id);
  }

  deleteTeam(data) {
    return this.http
      .patch(`${environment.apiEndPoint}user/update_team_admin`, data)
      .toPromise();
  }

  getViewTeam(team_id) {
    return this.http.get(environment.apiEndPoint + `home/team/${team_id}`);
  }

  getEditTeamAutoFill(team_id) {
    const query = { _id: team_id };
    const encodedUrl = encodeURIComponent(JSON.stringify(query));
    return this.http.get(environment.apiEndPoint + 'user/teamsDetail?query=' + encodedUrl);
  }

  team_update(team) {
    return this.http.patch(environment.apiEndPoint + 'team_update', team);
  }
}
