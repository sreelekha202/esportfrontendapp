import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class GameService {
  constructor(private http: HttpClient) {}

  public createMatchMakingSubject = new BehaviorSubject({});
  public createMatchMaking = this.createMatchMakingSubject.asObservable();

  fetchSeasonDetails(seasonId): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    return this.http.get(environment.apiEndPoint + `seasons/` + seasonId, {
      headers: httpHeaders,
    });
  }
  checkSeasonJoinStatus(seasonId: string): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    return this.http.get(
      environment.apiEndPoint +
        `participant/season-join-status?seasonId=` +
        seasonId,
      {
        headers: httpHeaders,
      }
    );
  }
  fetchRecentSeasonMatches(params): Observable<any> {
    const encodedUrl = encodeURIComponent(params.query);
    let url = `${environment.apiEndPoint}match/season-matches?query=${encodedUrl}`;
    return this.http.get(url);
  }
  getGames(param: any = null): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    return this.http.get(environment.apiEndPoint + `game`, {
      headers: httpHeaders,
      params: param,
    });
  }
  checkPlatform(id, data: any): Observable<any> {
    return this.http.get(
      environment.apiEndPoint + `userpreference/matchmaking/` + id,
      { params: data }
    );
  }
  saveGame(id, formData): Observable<any> {
    return this.http.post(
      environment.apiEndPoint + `game/upload/` + id,
      formData
    );
  }

  disableGame(gameId): Observable<any> {
    return this.http.delete(environment.apiEndPoint + `game/${gameId}`);
  }
}
