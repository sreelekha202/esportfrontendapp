import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { BehaviorSubject, Observable, SubscribableOrPromise } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class HomeService {
  public searchedArticle: Observable<[]>;
  public searchedArticleSubject: BehaviorSubject<[]>;
  public searchedTournament: Observable<[]>;
  public searchedTournamentSubject: BehaviorSubject<[]>;
  public searchedVideo: Observable<[]>;
  public searchedVideoSubject: BehaviorSubject<[]>;
  public searchedUser: Observable<[]>;
  public searchedUserSubject: BehaviorSubject<[]>;

  private tournamentStatusFilter = 3;

  private category = '';
  private headerSearchText = '';
  private sort = '';
  private page = '';
  private limit = '';

  constructor(private http: HttpClient) {
    this.searchedTournamentSubject = new BehaviorSubject([]);
    this.searchedTournament = this.searchedTournamentSubject.asObservable();
    this.searchedArticleSubject = new BehaviorSubject([]);
    this.searchedArticle = this.searchedArticleSubject.asObservable();
    this.searchedVideoSubject = new BehaviorSubject([]);
    this.searchedVideo = this.searchedVideoSubject.asObservable();
    this.searchedUserSubject = new BehaviorSubject([]);
    this.searchedUser = this.searchedUserSubject.asObservable();
  }

  ongoing_tournaments(): Observable<any> {
    return this.http.get(environment.apiEndPoint + `home/ongoing_tournaments`);
  }
  getTournament(params): Observable<any> {
    return this.http.get(environment.apiEndPoint + `home/tournament`, {
      params: params,
    });
  }

  getongoing(params): Observable<any> {
    return this.http.get(
      environment.apiEndPoint +
        `participant/tournament?status=1&pagination=%7B%22page%22%3A1%2C%22limit%22%3A5%7D`,
      { params: params }
    );
  }
  getUpcoming(params): Observable<any> {
    return this.http.get(
      environment.apiEndPoint +
        `participant/tournament?status=0&pagination=%7B%22page%22%3A1%2C%22limit%22%3A5%7D`,
      { params: params }
    );
  }
  getPastList(params): Observable<any> {
    return this.http.get(
      environment.apiEndPoint +
        `participant/tournament?status=2&pagination=%7B%22page%22%3A1%2C%22limit%22%3A5%7D`,
      { params: params }
    );
  }

  hottest_post(): Observable<any> {
    return this.http.get(environment.apiEndPoint + `home/hottest_post`);
  }

  trending_posts(): Observable<any> {
    return this.http.get(environment.apiEndPoint + `home/trending_posts`);
  }

  /**
   * Tournament search private helper function
   */
  private _searchTournament(): Observable<any> {
    return this.http.get(
      environment.apiEndPoint +
        `home/tournament?text=${this.headerSearchText}&status=${this.tournamentStatusFilter}&category=${this.category}&sort=${this.sort}&page=${this.page}&limit=${this.limit}`
    );
  }

  /**
   * Article search private helper function
   */
  private _searchArticle(): Observable<any> {
    return this.http.get(
      environment.apiEndPoint +
        `home/article?text=${this.headerSearchText}&page=${this.page}&limit=${this.limit}`
    );
  }

  /**
   * User search private helper function
   */
  public _searchUser(): Observable<any> {
    return this.http.get(environment.apiEndPoint + `user/search_users?text=${this.headerSearchText}`
    );
  }

  /**
   * method to get an announcement object
   */
  getAnnouncements() {
    return this.http.get(environment.apiEndPoint + `general/announcement`);
  }

  /**
   * banner list private helper function
   */
  _getBanner(): Observable<any> {
    return this.http.get(environment.apiEndPoint + `banner`);
  }

  /**
   * top players List
   */
  _topPlayers(): Observable<any> {
    return this.http.get(environment.apiEndPoint + `home/top-player`);
  }

  /**
   * leader board List
   */
  _leaderBoard(): Observable<any> {
    return this.http.get(environment.apiEndPoint + `home/fetchleaderboard`);
  }

  /**
   * Article search private helper function
   */
  private _searchVideo(): Observable<any> {
    return this.http.get(
      environment.apiEndPoint +
        `home/video?text=${this.headerSearchText}&page=${this.page}&limit=${this.limit}`
    );
  }

  /**
   * Tournament search service
   * can be called from anywhere
   */
  searchTournament() {
    //this.headerSearchText = query || '';
    this._searchTournament().subscribe((res) => {
      this.searchedTournamentSubject.next(res.data || []);
      return this.searchedTournamentSubject.value;
    });
  }
  /**
   * Article search service
   * can be called from anywhere
   */
  searchArticle() {
    this._searchArticle().subscribe((res) => {
      this.searchedArticleSubject.next(res.data);
      return this.searchedArticleSubject.value;
    });
  }

  /**
   * Video search service
   * can be called from anywhere
   */
  searchVideo() {
    this._searchVideo().subscribe((res) => {
      this.searchedVideoSubject.next(res.data);
      return this.searchedVideoSubject.value;
    });
  }

  /**
   * User search service
   * can be called from anywhere
   */
  searchUser() {
    this._searchUser().subscribe((res) => {
      this.searchedUserSubject.next(res.data);
      return this.searchedUserSubject.value;
    });
  }

  /**
   * Update tournament status, this is global function.
   * Header search will take filter value from here only.
   * to add filter value call this function.
   * @param status TournamentStatus
   */
  updateTournamentStatusFilter(status) {
    this.tournamentStatusFilter = status || 0;
    //this.tournamentStatusFilterSubject.next(status || 0);
    this._searchTournament().subscribe((res) => {
      this.searchedTournamentSubject.next(res.data);
      return this.searchedTournamentSubject.value;
    });
  }

  updateSearchParams(param, pagination) {
    this.headerSearchText = param;
    this.sort = pagination.sort;
    this.page = pagination.page;
    this.limit = pagination.limit;
  }

  updateView(body): SubscribableOrPromise<any> {
    return this.http
      .patch(`${environment.apiEndPoint}home/updateView`, body)
      .toPromise();
  }

  fetchScore(): SubscribableOrPromise<any> {
    return this.http
      .get(`${environment.apiEndPoint}home/fetch_score`)
      .toPromise();
  }

  // upcoming slider after Login
  getUpcomingSliderTournament(): Observable<any> {
    return this.http.get(
      `${environment.apiEndPoint}tournament/my_tournaments?status=6`
    );
  }

 /**
   * Play Now Ladder Keys
   */
  _playNowKeys(): Observable<any> {
    return this.http.get(environment.apiEndPoint + `home/play-now-keys`);
  }

}
