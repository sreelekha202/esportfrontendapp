import { Injectable } from "@angular/core";
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { map, catchError } from "rxjs/operators";
import { Observable, throwError } from "rxjs";
import { environment } from "../../../environments/environment";
import { GlobalUtils } from "esports";
const API = environment.apiEndPoint + "user/";
@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor(
    private http: HttpClient,
  ) { }

  getRecentlyUpdatedUsers(params): Observable<any> {
    const encodedUrl = encodeURIComponent(params['query']);
    const encodedPagination = encodeURIComponent(params['pagination']);
    return this.http.get(
      API + `usersPaginated?query=${encodedUrl}&pagination=${encodedPagination}`
    );
  }
  getAvatars(): Observable<any> {
     return this.http.get<any>(`${environment.apiEndPoint}admin/profile/avatar`);
  }

  updateProfile(data): Observable<any> {
    return this.http.post(`${environment.apiEndPoint}user/change_password`,data).pipe(
      map((value) => {
        return value;
      }),
      catchError(this.handleError)
    );
  }

  handleError(error: HttpErrorResponse, tokenName) {
    if (GlobalUtils.isBrowser() && error?.error?.message == 'Unauthorized') {
      localStorage.removeItem(tokenName);
      window.location.reload();
    }
    let msg = '';
    if (error?.error instanceof ErrorEvent) {
      // client-side error
      msg = error?.error?.message;
    } else {
      // server-side error
      msg = error?.error?.message;
    }
    return throwError(msg);
  }

}
