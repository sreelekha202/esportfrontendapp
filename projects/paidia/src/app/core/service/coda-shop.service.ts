import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";

import { Observable } from "rxjs";
// import { environment } from "../../../environments/environment";

@Injectable({
  providedIn: "root",
})
export class CodaShopService {
  constructor(private http: HttpClient) {}

  // getProductDetail(productName): Observable<any> {
  //   const httpHeaders = new HttpHeaders();
  //   httpHeaders.set("Content-Type", "application/json");
  //   let url = environment.eCommerceAPIEndPoint + `product/${productName}`;
  //   return this.http.get(url, { headers: httpHeaders });
  // }

  // validateUserDetail(dataToValidate) {
  //   const httpHeaders = new HttpHeaders();
  //   httpHeaders.set("Content-Type", "application/json");
  //   let url = environment.eCommerceAPIEndPoint + `validate`;
  //   return this.http.post(url, dataToValidate, { headers: httpHeaders });
  // }

  // processTopup(validatedUserData) {
  //   const httpHeaders = new HttpHeaders();
  //   httpHeaders.set("Content-Type", "application/json");
  //   let url = environment.eCommerceAPIEndPoint + `topup`;
  //   return this.http.post(url, validatedUserData, { headers: httpHeaders });
  // }

  // getCodaTransactionCode(userValidateResult) {
  //   const httpHeaders = new HttpHeaders();
  //   httpHeaders.set("Content-Type", "application/json");
  //   let url = environment.eCommerceAPIEndPoint + `getTransactionId`;
  //   return this.http.post(url, userValidateResult, { headers: httpHeaders });
  // }

  // /**
  //  * Service to fetch product information
  //  * @param productName
  //  */
  // getProductInfo(productName): Observable<any> {
  //   const httpHeaders = new HttpHeaders();
  //   httpHeaders.set("Content-Type", "application/json");
  //   let url = environment.apiEndPoint + `transaction/getProduct/${productName}`;
  //   return this.http.get(url, { headers: httpHeaders });
  // }
}
