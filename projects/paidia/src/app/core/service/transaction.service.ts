import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";

import { Observable, SubscribableOrPromise } from "rxjs";
import { environment } from "../../../environments/environment";

@Injectable({
  providedIn: "root",
})
export class TransactionService {
  constructor(private http: HttpClient) {}

  getRegFeeRefundStatus(query): SubscribableOrPromise<any> {
    const url = `${environment.apiEndPoint}transaction/reg_fee_refund_status`;
    return this.http.get(url, { params: query }).toPromise();
  }
  refundRegFee(payload): SubscribableOrPromise<any> {
    const url = `${environment.apiEndPoint}transaction/refund_reg_fee`;
    return this.http.post(url, payload).toPromise();
  }



}
