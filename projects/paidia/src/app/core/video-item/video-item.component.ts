import { Component, Input, OnInit, EventEmitter, Output } from '@angular/core';
import { AppHtmlRoutes } from '../../app-routing.model';
import { EsportsLanguageService } from 'esports';

export interface VideoItemComponentData {
  date: string;
  id: number;
  img: string;
  text: string;
  title: string;
}

@Component({
  selector: 'app-video-item',
  templateUrl: './video-item.component.html',
  styleUrls: ['./video-item.component.scss'],
})
export class VideoItemComponent implements OnInit {
  @Input() data;
  @Output() viewVideo = new EventEmitter<Boolean>();
  AppHtmlRoutes = AppHtmlRoutes;

  constructor(public languageService: EsportsLanguageService) {}

  ngOnInit(): void {}
  videoClicked() {
    this.viewVideo.emit(true);
  }
}
