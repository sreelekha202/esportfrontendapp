import { TranslateService } from '@ngx-translate/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import { AppHtmlRoutes } from '../../app-routing.model';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';
import { EsportsToastService } from 'esports';
import { HomeService } from '../../core/service/home.service';

@Component({
  selector: 'app-search-input',
  templateUrl: './search-input.component.html',
  styleUrls: ['./search-input.component.scss'],
})
export class SearchInputComponent implements AfterViewInit, OnInit, OnDestroy {
  AppHtmlRoutes = AppHtmlRoutes;
  text = '';
  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    public toastService: EsportsToastService,
    public translate: TranslateService,
    private homeService: HomeService
  ) { }

  ngOnInit(): void {

  }
  ngAfterViewInit(): void { }

  ngOnDestroy(): void {
  }

  onSearch(): void {
    if (!this.text) {
      this.toastService.showInfo(
        this.translate.instant('SEARCH.INPUT_TEXT_REQUIRED')
      );
    }
    if (this.text) {
      this.router.navigate([AppHtmlRoutes.search], {
        relativeTo: this.activatedRoute,
        queryParams: { text: this.text },
        queryParamsHandling: 'merge', // remove to replace all query params by provided
      });
    }
  }

  onSearchReset() {
    this.text = ' ';
    this.router.navigate([AppHtmlRoutes.search], {
      relativeTo: this.activatedRoute,
      queryParams: { text: this.text },
      queryParamsHandling: 'merge', // remove to replace all query params by provided
    });
  }
}
