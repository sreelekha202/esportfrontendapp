import { Component, OnInit, Input, Inject } from '@angular/core';
import {
  EsportsLanguageService,
  EsportsToastService,
  EsportsSocialService,
  EsportsLeaderboardService,
} from 'esports';
import { Router } from '@angular/router';
@Component({
  selector: 'app-user-item',
  templateUrl: './user-item.component.html',
  styleUrls: ['./user-item.component.scss'],
})
export class UserItemComponent implements OnInit {
  @Input() data;
  idUser: string = '';
  showUser: string = '';
  public isFollow: boolean;
  constructor(
    @Inject('env') private env,
    public languageService: EsportsLanguageService,
    public router: Router,
    private toastService: EsportsToastService,
    private socialService: EsportsSocialService,
    private leaderboardService: EsportsLeaderboardService
  ) {}

  ngOnInit(): void {
    this.leaderboardService
      .checkFollowStatus(this.env.apiEndPoint, this.data._id)
      .subscribe((res) => {
        if (res && res.data) {
          this.isFollow = res.data.isFollowing;
        }
      });
  }
  chooseUser(userId): void {
    if (this.showUser != '') {
      this.showUser = '';
    } else {
      this.showUser = userId;
    }
  }
  goToProfile(): void {
    this.router.navigateByUrl('/public-profile/' + this.data._id);
  }
  followUser() {
    if (this.isFollow == false) {
      this.leaderboardService
        .followUser(this.env.apiEndPoint, this.data._id)
        .subscribe((res) => {
          if (res && res.data) {
            this.toastService.showSuccess(`Follow successfully !`);
          }
        });
      this.isFollow = true;
    }
  }
  unfollowUser() {
    if (this.isFollow == true) {
      this.leaderboardService
        .unfollowUser(this.env.apiEndPoint, this.data._id)
        .subscribe((res) => {
          if (res.success) {
            this.toastService.showSuccess(`Unfollow successfully !`);
          }
        });
      this.isFollow = false;
    }
  }
}
