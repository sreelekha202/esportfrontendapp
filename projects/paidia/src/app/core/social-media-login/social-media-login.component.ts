import { Component, ElementRef, OnDestroy, OnInit, TemplateRef, ViewChild} from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { EsportsToastService, EsportsUserService } from 'esports';
import { environment } from "../../../environments/environment";
import {
  SocialAuthService,
  FacebookLoginProvider,
  GoogleLoginProvider,
  SocialUser,
} from 'angularx-social-login';
import { faPhone } from "@fortawesome/free-solid-svg-icons";
import { NgbModalConfig, NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { TranslateService } from "@ngx-translate/core";
import { Subscription } from "rxjs";
import {
  AuthServices,
} from '../service';
const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;

@Component({
  selector: 'app-social-media-login',
  templateUrl: './social-media-login.component.html',
  styleUrls: ['./social-media-login.component.scss'],
  providers: [NgbModalConfig, NgbModal],
})
export class SocialMediaLoginComponent implements OnInit, OnDestroy {
  @ViewChild('content')
  private content: TemplateRef<any>;
  @ViewChild('formRow') rows: ElementRef;

  codeEntered: boolean;
  didFail: boolean = false;
  loading: boolean = false;
  separateDialCode: boolean = true;
  stepOne = true;
  stepTwo: boolean = false;
  userSubscription: Subscription;

  confirmForm: FormGroup;
  location: Location;
  MobileVerificationForm: FormGroup;
  user: SocialUser;

  timeLeft: number = 240;

  code: string;
  id = '';
  message = '';
  returnUrl: string;

  interval;

  faPhone = faPhone;

  constructor(
    config: NgbModalConfig,
    private authService: SocialAuthService,
    private el: ElementRef,
    private formBuilder: FormBuilder,
    private loginService: AuthServices,
    private modalService: NgbModal,
    private router: Router,
    private userService: EsportsUserService,
    public translate: TranslateService,
    public toastService: EsportsToastService
  ) {
    config.backdrop = 'static';
    config.keyboard = false;
    config.centered = true;
    config.backdropClass = 'dark-modal';
  }

  ngOnInit() {
    this.MobileVerificationForm = this.formBuilder.group({
      phoneNo: ['', Validators.compose([Validators.required])],
    });

    this.confirmForm = this.formBuilder.group({
      code1: ['', Validators.compose([Validators.required])],
      code2: ['', Validators.compose([Validators.required])],
      code3: ['', Validators.compose([Validators.required])],
      code4: ['', Validators.compose([Validators.required])],
    });

    this.returnUrl = this.loginService.redirectUrl || '/';
  }

  ngOnDestroy() {
    this.modalService.dismissAll();
    this.userSubscription?.unsubscribe();
  }

  public socialSignIn(socialProvider: string) {
    let socialPlatformProvider;

    if (socialProvider === 'facebook') {
      socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
    } else if (socialProvider === 'google') {
      socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
    }

    this.authService
      .signIn(socialPlatformProvider)
      .then((socialusers) => {
        if (socialusers) {
          this.loading = true;
          this.Savesresponse(socialusers.authToken, socialusers.provider);
        }
      })
      .catch((err) => { });
  }

  Savesresponse(authToken, provider) {
    this.loginService.social(authToken, provider).subscribe(
      async (data) => {
        this.loading = false;
        this.userService.startRefreshTokenTimer(API, TOKEN);
        await this.userService.getAuthenticatedUser(API, TOKEN);
        this.userSubscription = this.userService.currentUser.subscribe(
          (sdata) => {
            if (sdata) {
              if (data.data.type === 'admin') {
                this.router.navigate(['/admin']);
                this.loading = false;
              } else {
                if (data.data.firstLogin === 1) {
                  this.router.navigate(['/profile']);
                  this.loading = false;
                } else {
                  this.router.navigate([this.returnUrl]);
                  this.loading = false;
                }
              }
            }
          }
        );
      },
      (error) => {
        this.loading = false;
        this.toastService.showError(error?.error?.message);
      }
    );
  }

  enableStepTwo(value) {
    const controls = this.MobileVerificationForm.controls;

    if (this.MobileVerificationForm.invalid) {
      Object.keys(controls).forEach((controlName) =>
        controls[controlName].markAsTouched()
      );
      return;
    }

    const data = {
      type: 'update_mobile',
      phoneNumber: value.phoneNo.e164Number,
      id: this.id,
    };

    this.didFail = false;
    this.message = '';
    this.loginService.social_login(data).subscribe(
      (data) => {
        this.didFail = false;
        this.message = '';
        this.stepOne = false;
        this.stepTwo = true;

        this.interval = setInterval(() => {
          if (this.timeLeft > 0) {
            this.timeLeft--;
          }
        }, 1000);
      },
      (error) => {
        this.didFail = true;
        this.message = error.error.message;
      }
    );
  }

  resendOtp() {
    let data = {
      id: this.id,
      type: 'resend_otp',
    };

    this.loginService.social_login(data).subscribe(
      (data) => {
        this.didFail = false;
        this.message = '';

        this.timeLeft = 240;
      },
      (error) => {
        this.didFail = true;
        this.message = error.error.message;
      }
    );
  }

  keyUpEvent(event, index) {
    let pos = index;

    if (event.keyCode === 8 && event.which === 8) {
      pos = index - 1;
    } else {
      pos = index + 1;
    }
  }

  onOtpChange(code: string): void {
    this.codeEntered = code.length === 4;

    if (this.codeEntered) {
      this.code = code;
    }
  }

  onConfirm(value) {
    const code = this.code;
    this.didFail = false;
    this.message = '';

    const data = {
      type: 'verify_mobile',
      otp: code,
      id: this.id,
    };

    this.loginService.social_login(data).subscribe(
      (data) => {
        this.loginService.setCookie(data.data.token, 'accessToken');
        localStorage.setItem(environment.currentToken, data.data.token);
        this.userService.startRefreshTokenTimer(API, TOKEN);
        this.modalService.dismissAll();
        if (data.data.type === 'admin') {
          this.router.navigate(['/admin']);
        } else {
          if (data.data.firstLogin == 1) {
            this.router.navigate(['/profile']);
          } else {
            this.router.navigate([this.returnUrl]);
          }
        }
      },
      (error) => {
        this.didFail = true;
        this.message = error.error.message;
      }
    );
  }

  open(content) {
    this.modalService.open(content);
  }

  cancel() {
    this.modalService.dismissAll();
    this.router
      .navigateByUrl('/home', { skipLocationChange: true })
      .then(() => {
        this.router.navigate(['/user', 'phone-login']);
      });
  }

  isControlHasErrorphone(controlName: string): boolean {
    const control = this.MobileVerificationForm.controls[controlName];
    if (control.touched) {
      if (control.invalid) {
        this.didFail = false;
        return true;
      }
    } else {
      return false;
    }
  }
}
