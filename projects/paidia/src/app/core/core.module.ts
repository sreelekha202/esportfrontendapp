import { NgModule } from '@angular/core';
import { environment } from '../../environments/environment';
import { AnnouncementComponent } from './../modules/home/announcement/announcement.component';
import { ArticleItemComponent } from './article-item/article-item.component';
import { DropDownSelectComponent } from './drop-down-select/drop-down-select.component';
import { JumbotronComponent } from './jumbotron/jumbotron.component';
import { ScrollTopComponent } from './scroll-top/scroll-top.component';
import { SearchInputComponent } from './search-input/search-input.component';
import { SnackComponent } from './snack/snack.component';
import { StepperComponent } from './stepper/stepper.component';
import { TournamentsCardComponent } from '../modules/home/tournaments-card/tournaments-card.component';
import { VideoItemComponent } from './video-item/video-item.component';
import { StcPopupComponent } from './stc-popup/stc-popup.component';
import {
  SocialAuthServiceConfig,
  GoogleLoginProvider,
  FacebookLoginProvider,
} from 'angularx-social-login';
import { SharedModule } from '../shared/modules/shared.module';
import { UserItemComponent } from './user-item/user-item.component';

const components = [
  AnnouncementComponent,
  ArticleItemComponent,
  DropDownSelectComponent,
  JumbotronComponent,
  ScrollTopComponent,
  SearchInputComponent,
  SnackComponent,
  StepperComponent,
  TournamentsCardComponent,
  VideoItemComponent,
  StcPopupComponent,
  UserItemComponent,
];

@NgModule({
  declarations: [...components],
  imports: [SharedModule],
  exports: [...components],
  providers: [
    {
      provide: 'SocialAuthServiceConfig',
      useValue: {
        autoLogin: false,
        providers: [
          {
            id: GoogleLoginProvider.PROVIDER_ID,
            provider: new GoogleLoginProvider(environment.googleAPPID),
          },
          {
            id: FacebookLoginProvider.PROVIDER_ID,
            provider: new FacebookLoginProvider(environment.facebookAPPID),
          },
        ],
      } as SocialAuthServiceConfig,
    },
  ],
})
export class CoreModule {}
