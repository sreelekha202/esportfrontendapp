import { JoinSeasonParticipantComponent } from './modules/matchmaking/join-participant/join-participant.component';
import { Title } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, Router, Route, RouterModule } from '@angular/router';
import { AuthGuard } from './shared/guard/auth.guard';
import { GamesComponent } from './modules/games/games.component';
import { MatchmakingComponent } from './modules/matchmaking/matchmaking.component';
import { NotfoundComponent } from './modules/not-found/not-found.component';
import { PrivacyPolicyComponent } from './modules/privacy-policy/privacy-policy.component';
import { TermsOfUsComponent } from './modules/terms-of-us/terms-of-us.component';
import { MatchSeasonComponent } from './modules/games/match-season/match-season.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PersonalProfileComponent } from './modules/personal-profile/personal-profile.component';
import { PublicProfileComponent } from './modules/public-profile/public-profile.component';
import { HomeComponent } from './modules/home/home.component';

const routes: Routes = [
  {
    path: 'content/create',
    loadChildren: () =>
      import('./modules/article/create-article/create-article.module').then(
        (m) => m.CreateArticleModule
      ),
    canActivate: [AuthGuard],
    data: {
      tags: [
        {
          name: 'title',
          content: 'Paidia',
        },
      ],
      title: 'Paidia',
    },
  },
  {
    path: 'content/edit/:id',
    loadChildren: () =>
      import('./modules/article/create-article/create-article.module').then(
        (m) => m.CreateArticleModule
      ),
    canActivate: [AuthGuard],
  },
  {
    path: 'content',
    loadChildren: () =>
      import('./modules/article/article.module').then((m) => m.ArticleModule),
    data: {
      tags: [
        {
          name: 'title',
          content: 'Watch Gaming Content',
        },
        {
          name: 'description',
          content:
            'Watch video on demand content from inspiring women in the gaming industry',
        },
        {
          name: 'og:description',
          content:
            'Watch video on demand content from inspiring women in the gaming industry',
        },
        {
          name: 'twitter:description',
          content:
            'Watch video on demand content from inspiring women in the gaming industry',
        },
        { name: 'keywords ', content: 'esports, news, esportsnews' },
      ],
      title: 'Watch Gaming Content',
    },
  },
  {
    path: 'play',
    loadChildren: () =>
      import('./modules/play/play.module').then((m) => m.PlayModule),
    data: {
      title: 'Browse Tournaments',
      tags: [
        {
          name: 'title',
          content: 'Browse Tournaments',
        },
        {
          name: 'description',
          content: 'Host or play in tournaments for casual to advanced players',
        },
        {
          name: 'og:description',
          content: 'Host or play in tournaments for casual to advanced players',
        },
        {
          name: 'twitter:description',
          content: 'Host or play in tournaments for casual to advanced players',
        },
      ],
    },
  },
  {
    path: 'leaderboard',
    loadChildren: () =>
      import('./modules/leaderboard/leaderboard.module').then(
        (m) => m.LeaderboardModule
      ),
    canActivate: [AuthGuard],
    data: { title: 'Paidia - Leaderboard' },
  },
  // {
  //   path: 'store',
  //   loadChildren: () =>
  //     import('./modules/store/store.module').then((m) => m.StoreModule),
  //   data: { title: 'Paidia - Store' },
  // },

  { path: 'home', component: HomeComponent, data: { skipGuard: true } },
  { path: 'post/:id', component: HomeComponent, data: { skipGuard: true } },
  { path: 'private-profile', component: PersonalProfileComponent },
  { path: 'public-profile/:id', component: PublicProfileComponent },

  {
    path: 'get-match',
    loadChildren: () =>
      import('./modules/get-match/get-match.module').then(
        (m) => m.GetMatchModule
      ),
    canActivate: [AuthGuard],
    data: {
      isRootPage: true,
      title: 'Paidia Get-Match',
    },
  },
  {
    path: 'matchmaking',
    component: MatchmakingComponent,
    data: {
      title: 'Matchmaking',
      tags: [
        {
          name: 'title',
          content: 'Matchmaking',
        },
        {
          name: 'description',
          content:
            'Find other players online ready to play in Paidia’s matchmaking section',
        },
        {
          name: 'og:description',
          content:
            'Find other players online ready to play in Paidia’s matchmaking section',
        },
        {
          name: 'twitter:description',
          content:
            'Find other players online ready to play in Paidia’s matchmaking section',
        },
      ],
    },
  },
  {
    path: 'seasons',
    loadChildren: () =>
      import('./modules/seasons/seasons.module').then((m) => m.SeasonsModule),
  },
  {
    path: 'all-tournaments',
    loadChildren: () =>
      import('./modules/all-tournaments/all-tournaments.module').then(
        (m) => m.AllTournamentsModule
      ),
  },
  {
    path: 'matchmaking/:id',
    component: JoinSeasonParticipantComponent,
    canActivate: [AuthGuard],
    data: {
      isRootPage: true,
    },
  },
  {
    path: 'about-us',
    loadChildren: () =>
      import('./modules/about-us/about-us.module').then((m) => m.AboutUsModule),
    data: {
      title: 'About us',
      tags: [
        {
          name: 'title',
          content: 'About us',
        },
        {
          name: 'description',
          content:
            'An inclusive gaming community for women and allies of all genders to safely connect, learn and play.',
        },
        {
          name: 'og:description',
          content:
            'An inclusive gaming community for women and allies of all genders to safely connect, learn and play.',
        },
        {
          name: 'twitter:description',
          content:
            'An inclusive gaming community for women and allies of all genders to safely connect, learn and play.',
        },
      ],
    },
  },
  { path: 'games', component: GamesComponent, canActivate: [AuthGuard] },
  {
    path: 'games/match-season/:id',
    component: MatchSeasonComponent,
  },
  {
    path: 'games/:id',
    component: MatchSeasonComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'home',
    loadChildren: () =>
      import('./modules/home/home.module').then((m) => m.HomeModule),
  },
  // {
  //   path: 'about-us',
  //   loadChildren: () =>
  //     import('./../app/modules/about-us/about-us.module').then(
  //       (m) => m.AboutUsModule
  //     ),
  // },
  {
    path: 'tournament',
    loadChildren: () =>
      import('./modules/tournament/tournament.module').then(
        (m) => m.TournamentModule
      ),
    data: {
      tags: [
        {
          name: 'description',
          content:
            'Paidia.gg supports and covers more than [99] esports games. Create/Organize, join and follow your favourite esports tournaments today!',
        },
        {
          name: 'og:description',
          content:
            'Paidia.gg supports and covers more than [99] esports games. Create/Organize, join and follow your favourite esports tournaments today!',
        },
        {
          name: 'twitter:description',
          content:
            'Paidia.gg supports and covers more than [99] esports games. Create/Organize, join and follow your favourite esports tournaments today!',
        },
        {
          name: 'title',
          content:
            'Paidia.gg supports and covers more than [99] esports games. Create/Organize, join and follow your favourite esports tournaments today!',
        },
        { name: 'keywords ', content: 'esports, tournament' },
      ],
      title: 'Paidia TOURNAMENTS',
    },
  },
  {
    path: 'create-tournament',
    loadChildren: () =>
      import('./modules/create-tournament/create-tournament.module').then(
        (m) => m.CreateTournamentModule
      ),
    canActivate: [AuthGuard],
    data: {
      isRootPage: true,
      Title: 'Paidia Create-Tournament',
    },
  },
  {
    path: 'advance-tournament',
    loadChildren: () =>
      import('./modules/quick-advance/quick-advance.module').then(
        (m) => m.QuickAdvanceTournamentModule
      ),
    canActivate: [AuthGuard],
    data: {
      isRootPage: true,
      Title: 'Paidia Create-Tournament',
    },
  },

  {
    path: 'join-tournament',
    loadChildren: () =>
      import('./modules/join-tournament/join-tournament.module').then(
        (m) => m.JoinTournamentModule
      ),
    canActivate: [AuthGuard],
    data: {
      isRootPage: true,
      Title: 'Paidia Join-Tournament',
    },
  },
  {
    path: 'user/:pageType',
    loadChildren: () =>
      import('./modules/log-reg/log-reg.module').then((m) => m.LogRegModule),
    data: {
      isRootPage: true,
      skipGuard: true,
      tags: [
        {
          name: 'description',
          content:
            'Start your esports journey with  Paidia.gg. A complete esports tournament management platform',
        },
        {
          name: 'og:description',
          content:
            'Start your esports journey with  Paidia.gg. A complete esports tournament management platform',
        },
        {
          name: 'twitter:description',
          content:
            'Start your esports journey with  Paidia.gg. A complete esports tournament management platform',
        },
        {
          name: 'title',
          content:
            'Start your esports journey with  Paidia.gg. A complete esports tournament management platform',
        },
      ],
      title: 'Paidia',
    },
  },
  {
    path: 'privacy-policy',
    component: PrivacyPolicyComponent,
    data: { Title: 'Paidia Privacy-Policy' },
  },
  {
    path: 'terms-of-use',
    component: TermsOfUsComponent,
    data: { Title: 'Paidia Terms-Of-Use' },
  },
  {
    path: 'create-team',
    loadChildren: () =>
      import('./modules/team/create-team/create-team.module').then(
        (m) => m.CreateTeamModule
      ),
    canActivate: [AuthGuard],
    data: {
      isRootPage: true,
      title: 'Paidia Create-team',
    },
  },
  //   {path: 'profile/:my-teams',
  //   loadChildren: () => import('./modules/profile/profile.module').then((m) => m.ProfileModule),
  //   canActivate: [AuthGuard],
  //   data: {
  //     isRootPage: true,
  //     title: "Paidia profile"
  //   },
  // },

  {
    path: 'view-team/:id',
    loadChildren: () =>
      import('./modules/team/view-team/view-team.module').then(
        (m) => m.ViewTeamModule
      ),
    canActivate: [AuthGuard],
    data: {
      title: 'Paidia Update-team',
    },
  },
  {
    path: 'manage-team',
    loadChildren: () =>
      import('./modules/manage-teams/manage-teams.module').then(
        (m) => m.ManageTeamsModule
      ),
    canActivate: [AuthGuard],
    data: {
      isRootPage: true,
      title: 'Paidia Manage-team',
    },
  },
  {
    path: 'manage-tournament/:slug',
    loadChildren: () =>
      import('./modules/manage-tournamens/manage-tournamens.module').then(
        (m) => m.ManageTournamensModule
      ),
    canActivate: [AuthGuard],
    data: {
      isRootPage: true,
      title: 'Paidia Manage-Tournaments',
    },
  },
  {
    path: 'manage-match/:id',
    loadChildren: () =>
      import('./modules/manage-match/manage-match.module').then(
        (m) => m.ManageMatchModule
      ),
    canActivate: [AuthGuard],
    data: {
      isRootPage: true,
      title: 'Paidia Manage-Tournaments',
    },
  },
  {
    path: 'team-registration/:id',
    loadChildren: () =>
      import('./modules/team-registration/team-registration.module').then(
        (m) => m.TeamRegistrationModule
      ),
    canActivate: [AuthGuard],
    data: {
      isRootPage: true,
      title: 'Paidia Team-Registration',
    },
  },
  {
    path: 'videos',
    canActivate: [AuthGuard],
    loadChildren: () =>
      import('./modules/videos/videos.module').then((m) => m.VideosModule),
  },
  {
    path: 'search',
    loadChildren: () =>
      import('./modules/search/search.module').then((m) => m.SearchModule),
  },
  {
    path: 'bracket',
    canActivate: [AuthGuard],
    loadChildren: () =>
      import('./modules/bracket/bracket.module').then((m) => m.BracketModule),
    data: { title: 'Paidia Bracket' },
  },
  {
    path: 'profile',
    canActivate: [AuthGuard],
    loadChildren: () =>
      import('./modules/profile/profile.module').then((m) => m.ProfileModule),
    data: { title: 'Paidia Profile' },
  },
  {
    path: 'admin',
    loadChildren: () =>
      import('./modules/admin/admin.module').then((m) => m.AdminModule),
    canActivate: [AuthGuard],
    data: { title: 'Paidia Admin' },
  },
  {
    path: 'web-view',
    loadChildren: () =>
      import('./modules/web-view/web-view.module').then((m) => m.WebViewModule),
  },
  {
    path: 'landing-page',
    loadChildren: () =>
      import('./modules/landing/landing.module').then((m) => m.LandingModule),
    data: { skipGuard: true },
  },
  {
    path: 'report-score',
    loadChildren: () =>
      import('./modules/report-score/report-score.module').then(
        (m) => m.ReportScoreModule
      ),
    canActivate: [AuthGuard],
    data: {
      isRootPage: true,
      title: 'Paidia Report score',
    },
  },
  {
    path: 'report-issue',
    loadChildren: () =>
      import('./modules/report-issue/report-issue.module').then(
        (m) => m.ReportIssueModule
      ),
    canActivate: [AuthGuard],
    data: {
      isRootPage: false,
      title: 'Paidia Report issue',
    },
  },
  {
    path: '',
    redirectTo: '/landing-page',
    pathMatch: 'full',
    data: { skipGuard: true },
  },
  { path: '404', component: NotfoundComponent, data: { skipGuard: true } },
  {
    path: '**',
    redirectTo: '/404',
    pathMatch: 'full',
    data: { skipGuard: true },
  },
];

@NgModule({
  imports: [
    BrowserAnimationsModule,
    RouterModule.forRoot(routes, {
      scrollPositionRestoration: 'enabled',
      initialNavigation: 'enabled',
    }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
