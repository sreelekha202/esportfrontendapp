import { AngularFireMessagingModule } from '@angular/fire/messaging';
import { AngularFireModule } from '@angular/fire';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DatePipe } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { JwtInterceptor } from './core/helpers/interceptors/token-interceptors.service';
import { S3UploadService } from './core/service';
import { AppRoutingModule } from './app-routing.module';
import { CoreModule } from './core/core.module';
import { SharedModule } from './shared/modules/shared.module';
import { AppComponent } from './app.component';
import { FooterComponent } from './core/footer/footer.component';
import { GamesComponent } from './modules/games/games.component';
import { HeaderComponent } from './core/header/header.component';
import { MatchmakingComponent } from './modules/matchmaking/matchmaking.component';
import { MatchmakerDialogComponent } from './modules/games/matchmaker-dialog/matchmaker-dialog.component';
import { NotfoundComponent } from './modules/not-found/not-found.component';
import { PrivacyPolicyComponent } from './modules/privacy-policy/privacy-policy.component';
import { TermsOfUsComponent } from './modules/terms-of-us/terms-of-us.component';
import { ToastsContainer } from './shared/toast/toast-container.component';
import { appInitializer } from './core/helpers/app.initializer';
import { environment } from '../environments/environment';
import {
  SocialAuthServiceConfig,
  SocialLoginModule,
  GoogleLoginProvider,
  FacebookLoginProvider,
} from 'angularx-social-login';
import { CookieService } from 'ngx-cookie-service';
import { InlineSVGModule } from 'ng-inline-svg';
import { MatchSeasonComponent } from './modules/games/match-season/match-season.component';
import { CountdownModule } from 'ngx-countdown';
import {
  BrowserModule,
  BrowserTransferStateModule,
} from '@angular/platform-browser';
import { JoinSeasonParticipantComponent } from './modules/matchmaking/join-participant/join-participant.component';
import { FormComponentModule } from './shared/components/form-component/form-component.module';
import { ScrollingModule } from '@angular/cdk/scrolling';
import {
  EsportsUserService,
  EsportsLoaderModule,
  EsportsPaginationService,
  EsportsModule,
  I18nModule,
  EsportsToastService,
  EsportsSnackBarModule,
  PipeModule,
} from 'esports';
import { EsportsChatService } from 'esports';

// PERSONAL PROFLE
import { PersonalProfileComponent } from './modules/personal-profile/personal-profile.component';
import { BannerPersonComponent } from './modules/personal-profile/banner-person/banner-person.component';
import { AboutProfileComponent } from './modules/personal-profile/about-profile/about-profile.component';
import { FeedOptionComponent } from './modules/personal-profile/feed-option/feed-option.component';

//Public Profile
import { PublicProfileComponent } from './modules/public-profile/public-profile.component';
import { PublicBannerProfileComponent } from './modules/public-profile/public-banner-profile/public-banner-profile.component';
import { PublicAboutProfileComponent } from './modules/public-profile/public-about-profile/public-about-profile.component';
import { PublicFeedProfileComponent } from './modules/public-profile/public-feed-profile/public-feed-profile.component';

// import {
//   SocialAuthServiceConfig,
//   SocialLoginModule,
//   GoogleLoginProvider,
//   FacebookLoginProvider,
// } from 'angularx-social-login';
// import { CookieService } from 'ngx-cookie-service';
// import { InlineSVGModule } from 'ng-inline-svg';

import { MatExpansionModule } from '@angular/material/expansion';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatRadioModule } from '@angular/material/radio';
import { MobileRestrictPopupComponent } from './shared/popups/mobile-restrict-popup/mobile-restrict-popup.component';
import { PickerModule } from '@ctrl/ngx-emoji-mart';
@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    GamesComponent,
    HeaderComponent,
    MatchmakerDialogComponent,
    MatchmakingComponent,
    NotfoundComponent,
    PrivacyPolicyComponent,
    TermsOfUsComponent,
    ToastsContainer,
    MatchSeasonComponent,
    JoinSeasonParticipantComponent,
    PersonalProfileComponent,
    BannerPersonComponent,
    AboutProfileComponent,
    FeedOptionComponent,
    PublicProfileComponent,
    PublicBannerProfileComponent,
    PublicAboutProfileComponent,
    PublicFeedProfileComponent,
    MobileRestrictPopupComponent,
  ],
  imports: [
    PickerModule,
    AngularFireMessagingModule,
    AngularFireModule.initializeApp(environment.firebase),
    AppRoutingModule,
    BrowserAnimationsModule,
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    BrowserTransferStateModule,
    CoreModule,
    I18nModule.forRoot(environment),
    InlineSVGModule.forRoot(),
    PipeModule,
    SharedModule,
    SocialLoginModule,
    FormComponentModule,
    CountdownModule,
    ScrollingModule,
    EsportsModule.forRoot(environment),
    EsportsSnackBarModule.setPlatform('ESPORTSCORE'),
    EsportsLoaderModule.setColor('#1d252d'),
    MatExpansionModule,
    MatSlideToggleModule,
    MatDialogModule,
    MatInputModule,
    MatFormFieldModule,
    MatMenuModule,
    MatRadioModule,
  ],
  exports: [HeaderComponent, FooterComponent, ToastsContainer],

  providers: [
    CookieService,
    EsportsChatService,
    DatePipe,
    EsportsPaginationService,
    S3UploadService,
    EsportsToastService,
    {
      provide: APP_INITIALIZER,
      useFactory: appInitializer,
      multi: true,
      deps: [EsportsUserService],
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptor,
      multi: true,
    },
    {
      provide: 'SocialAuthServiceConfig',
      useValue: {
        autoLogin: false,
        providers: [
          {
            id: GoogleLoginProvider.PROVIDER_ID,
            provider: new GoogleLoginProvider(environment.googleAPPID),
          },
          {
            id: FacebookLoginProvider.PROVIDER_ID,
            provider: new FacebookLoginProvider(environment.facebookAPPID),
          },
        ],
      } as SocialAuthServiceConfig,
    },
    ...(environment.gtmId
      ? [{ provide: 'googleTagManagerId', useValue: environment.gtmId }]
      : []),
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
