import {
  AfterViewInit,
  Component,
  ElementRef,
  Inject,
  OnInit,
  PLATFORM_ID,
} from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { Direction } from '@angular/cdk/bidi';
import { isPlatformBrowser, DOCUMENT } from '@angular/common';
import { Meta, Title } from '@angular/platform-browser';
import { AuthServices } from './core/service';
import {
  EsportsUserService,
  EsportsChatService,
  EsportsPaginationService,
  EsportsConstantsService,
  EsportsToastService,
  EsportsScriptLoadingService,
  IUser,
  GlobalUtils,
} from 'esports';
import { filter, map, take } from 'rxjs/operators';
import { AppHtmlRoutes } from './app-routing.model';
import { AppRoutesData } from './app-routing.model';
import { CookieService } from 'ngx-cookie-service';
import { environment } from '../environments/environment';
import * as firebase from 'firebase/app';
import { MatDialog } from '@angular/material/dialog';
import { MobileRestrictPopupComponent } from './shared/popups/mobile-restrict-popup/mobile-restrict-popup.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit, AfterViewInit {
  AppHtmlRoutes = AppHtmlRoutes;

  isAdminPage: boolean = false;
  isBrowser: boolean;
  isRootPage: boolean = false;
  isUserLoggedIn: boolean = false;

  direction: Direction = 'ltr';
  currentUserId: String = '';

  currentUser: IUser;
  public scroll: ElementRef;
  cookiePolicy;
  dataNotifi: any;
  screenWidth: number;
  screenHeight: number;

  constructor(
    @Inject(DOCUMENT) private document: Document,
    @Inject(PLATFORM_ID) private platformId,
    private activatedRoute: ActivatedRoute,
    private authService: AuthServices,
    private cookie: CookieService,
    private meta: Meta,
    private paginationService: EsportsPaginationService,
    private titleService: Title,
    private userService: EsportsUserService,
    public router: Router,
    private chatService: EsportsChatService,
    private toastService: EsportsToastService,
    private esportsScriptLoadingService: EsportsScriptLoadingService,
    public dialog: MatDialog
  ) {
    this.isBrowser = isPlatformBrowser(platformId);

    if (this.isBrowser && GlobalUtils.isBrowser()) {
      this.addSiteTrackers();
      this.checkAdmin();
      this.globalRouterEvents();

      window.addEventListener('storage', (event) => {
        if (event.newValue && event.key === environment.currentToken) {
          window.location.reload();
        }

        if (event.oldValue && event.key === environment.currentToken) {
          window.location.reload();
        }
      });
    }
  }

  ngOnInit(): void {
    if (this.isBrowser) {
      this.screenWidth = window.innerWidth;
      this.screenHeight = window.innerHeight;
      if (this.screenWidth < 1024) {
        const dialogRef = this.dialog.open(MobileRestrictPopupComponent, {
          disableClose: true,
        });
        dialogRef.afterClosed().subscribe(() => {});
      }
      this.cookiePolicy = this.cookie.get('enable_Cookie');
      this.paginationService.pageChanged.subscribe(
        (res) => {
          if (res) {
            this.scroll.nativeElement.scrollTop = 0;
          }
        },
        (err) => {}
      );

      this.userService.currentUser.subscribe((data) => {
        if (data) {
          this.currentUser = data;
          this.isUserLoggedIn = !!this.currentUser;
          this.currentUserId = this.currentUser._id;
          this.chatService.initialiseSocket();
          this.chatService.listenLikePost().subscribe((payload) => {
            this.dataNotifi = payload;
            this.toastService.showInfo(`Someone ${payload}`);
          });
          this.chatService.listenReplyComment().subscribe((payload) => {
            this.dataNotifi = payload;
            this.toastService.showInfo(`Someone ${payload}`);
          });
          this.chatService.listenReplyPost().subscribe((payload) => {
            this.dataNotifi = payload;
            this.toastService.showInfo(`Someone ${payload}`);
          });
          this.chatService.listenCommentPost().subscribe((payload) => {
            this.dataNotifi = payload;
            this.toastService.showInfo(`Someone ${payload}`);
          });
          this.chatService.listenReplyPost().subscribe((payload) => {
            this.dataNotifi = payload;
            this.toastService.showInfo(`Someone ${payload}`);
          });
          this.chatService.listenReportPost().subscribe((payload) => {
            this.dataNotifi = payload;
            this.toastService.showInfo(`Someone ${payload}`);
          });
        } else {
          //this.userService.getAuthenticatedUser();
        }
      });
      firebase.default.initializeApp(environment.firebase);
    }
  }

  ngAfterViewInit(): void {
    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
        if (this.scroll?.nativeElement?.scrollTop) {
          this.scroll.nativeElement.scrollTop = 0;
        }
        return;
      }

      if (this.scroll?.nativeElement?.scrollTop) {
        this.scroll.nativeElement.scrollTop = 0;
      }
    });
  }

  onActivate(): void {}

  checkAdmin(): void {
    this.userService.currentUser
      .pipe(
        filter((data) => !!data),
        take(1)
      )
      .subscribe((data) => {
        if (data) {
          if (data.accountType === EsportsConstantsService.AccountType.Admin) {
            this.isAdminPage = true;
          }
        } else {
          this.isAdminPage = false;
        }
      });
  }

  private globalRouterEvents(): void {
    this.router.events
      .pipe(
        filter((event) => event instanceof NavigationEnd),
        map(() => this.activatedRoute.root.firstChild.snapshot.data)
      )
      .subscribe((data: AppRoutesData) => {
        this.isRootPage = data && data.isRootPage;

        const title = data && data.title;
        const tags = data && data.tags;
        if (title) {
          this.titleService.setTitle(title);
        }
        if (tags) {
          tags.forEach((tag) => {
            this.meta.updateTag(tag);
          });
        }
      });
  }

  clickAccept() {
    if (GlobalUtils.isBrowser()) {
      this.authService.setCookie('true', 'enable_Cookie');
      this.cookiePolicy = this.cookie.get('enable_Cookie');
    }
  }

  addSiteTrackers() {
    this.esportsScriptLoadingService.scriptLoader({ name: 'GTag' });
    this.esportsScriptLoadingService.scriptLoader({ name: 'GTMTag' });
    this.esportsScriptLoadingService.scriptLoader({ name: 'facebookPixel' });
    this.esportsScriptLoadingService.scriptLoader({
      name: 'twitterUniversalPageView',
    });
  }
}
