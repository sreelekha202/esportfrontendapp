import { IUser } from 'esports';

export const timeFormatAMPM = (date) => {
  var hours = date.getHours();
  var minutes = date.getMinutes();
  var ampm = hours >= 12 ? 'pm' : 'am';
  hours = hours % 12;
  hours = hours ? hours : 12; // the hour '0' should be '12'
  minutes = minutes < 10 ? '0' + minutes : minutes;
  var strTime = hours + ':' + minutes + ' ' + ampm;
  return strTime;
};

export enum TYPE_IMG_LOAD {
  AVATAR = 'avatar',
  ANY = 'any',
}

export const onLoadImgError = (event: any, type?: TYPE_IMG_LOAD) => {
  event.target.src =
    type == TYPE_IMG_LOAD.AVATAR
      ? 'assets/images/Home/avatar-prelogin.png'
      : 'assets/images/games/video-post-ab-game.png';
};

export const setCreatedBy = (user: IUser): IUser => {
  const { _id, profilePicture, username, gender, customPronoun } = user;
  return { _id, profilePicture, username, gender, customPronoun };
};
