import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CommentService, FormService } from '../../../../core/service';
import {
  EsportsUserService,
  EsportsToastService,
  EsportsGtmService,
} from 'esports';
import { IUser } from 'esports';
@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.scss'],
})
export class MessageComponent implements OnInit {
  @Input() _id: string;
  @Input() DOM_ID: string;
  @Input() enableCancelEvent: boolean = true;
  @Input() enableInput: boolean;
  @Input() objectId: string;
  @Input() objectType: string;
  @Input() placeholder: string;
  @Input() text: string;

  @Output() valueEmitOnSubmit = new EventEmitter<any>();
  @Output() valueEmitOnCancel = new EventEmitter<boolean>();

  currentUser: IUser;

  messageForm: FormGroup;
  isProcessing: boolean = false;

  constructor(
    private commentService: CommentService,
    private fb: FormBuilder,
    private formService: FormService,
    private toastService: EsportsToastService,
    private gtmService: EsportsGtmService,
    private userService: EsportsUserService
  ) {}

  ngOnInit(): void {
    this.messageForm = this.fb.group({
      text: [
        '',
        Validators.compose([Validators.required, this.formService.emptyCheck]),
      ],
    });

    this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.currentUser = data;
      }
    });
  }

  ngOnChanges() {
    if (this.text) {
      this.messageForm?.get('text')?.setValue(this.text);
    }
  }

  onSubmit = async () => {
    try {
      const payload = {
        comment: this.messageForm.value.text,
        objectId: this.objectId,
        objectType: this.objectType,
        ...(this._id && { _id: this._id }),
      };
      this.pushDiscussionTags();
      this.isProcessing = true;
      const message = await this.commentService.upsertComment(payload);
      this.isProcessing = false;
      this.messageForm.reset();
      this.valueEmitOnSubmit.emit({ ...message?.data, isCommentedUser: true });
    } catch (error) {
      this.isProcessing = false;
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  onCancel = async () => {
    this.valueEmitOnCancel.emit(true);
  };

  pushDiscussionTags() {
    switch (this.objectType) {
      case 'tournament':
        this.gtmService.pushGTMTags('Tournament_Discussion_Message');
        break;
      case 'comment':
        this.gtmService.pushGTMTags('Reply_To_User');
        break;
      case 'article':
        this.gtmService.pushGTMTags('Add_Post_Comment', { messageLength: this?.messageForm?.value?.text?.length });
        break;
      default:
        break;
    }
  }
}
