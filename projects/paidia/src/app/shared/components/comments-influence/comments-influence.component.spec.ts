import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CommentsInfluenceComponent } from './comments-influence.component';

describe('CommentsInfluenceComponent', () => {
  let component: CommentsInfluenceComponent;
  let fixture: ComponentFixture<CommentsInfluenceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CommentsInfluenceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CommentsInfluenceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
