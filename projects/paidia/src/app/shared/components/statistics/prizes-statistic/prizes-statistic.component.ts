import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-prizes-statistic',
  templateUrl: './prizes-statistic.component.html',
  styleUrls: ['./prizes-statistic.component.scss'],
})
export class PrizesStatisticComponent implements OnInit {
  @Input() statisticsDetails: any;
  constructor() {}

  ngOnInit(): void {}
}
