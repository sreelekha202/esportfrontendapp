import { Component, Input, OnInit } from '@angular/core';
import { ChartType } from 'chart.js';
import { MultiDataSet, Label } from 'ng2-charts';
import { EsportsGtmService, EventProperties } from "esports";

@Component({
  selector: 'app-months-statistic',
  templateUrl: './months-statistic.component.html',
  styleUrls: ['./months-statistic.component.scss'],
})
export class MonthsStatisticComponent implements OnInit {
  @Input() statisticsDetails: any;
    // Doughnut
    public doughnutChartLabels: Label[] = ['Win', 'Draw', 'Lose'];
    public doughnutChartData: MultiDataSet = [
      [350, 450, 100],
      // [50, 150, 120],
      // [250, 130, 70],

    ];
    public doughnutChartType: ChartType = 'doughnut';


  selectedMonth: any = 'Select month';

  months = [
    'JANUARY',
    'FEBRUARY',
    'MARCH',
    'APRIL',
    'MAY',
    'JUNE',
    'JULY',
    'AUGUST',
    'SEPTEMBER',
    'OCTOBER',
    'NOVEMBER',
    'DECEMBER',
  ];
  threemonth: any;

  constructor(private gtmService: EsportsGtmService) {
    this.threemonth = "THIS_MONTH";
    this.selectedMonth = this.months[new Date().getMonth()]
  }

  ngOnInit(): void {

  }
  selecteMonths(type) {
    this.threemonth = type;
    this.pushGTMTags('Statistics_Date_Filter_Trigger');
  }

  selectMonth(month){
    this.selectMonth = month;
    this.pushGTMTags('Statistics_Date_Filter_Trigger');
  }

  pushGTMTags(eventName, eventData: EventProperties = null){
    this.gtmService.pushGTMTags(eventName, eventData);
  }

}
