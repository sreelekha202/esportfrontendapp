import { Component, OnInit, Input } from '@angular/core';
import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';
import { EsportsSocialService } from 'esports';

@Component({
  selector: 'app-preview-link',
  templateUrl: './preview-link.component.html',
  styleUrls: ['./preview-link.component.scss'],
  providers: [NgbCarouselConfig],
})
export class PreviewLinkComponent implements OnInit {
  @Input() link: string = '';
  public imageSrc = '';
  public title = '';
  public description = '';

  constructor(private socialService: EsportsSocialService) {}

  ngOnInit(): void {
    if (this.link) {
      this.getPreview();
    }
  }

  getPreview() {
    this.socialService.getPreviewURL(this.link).subscribe((res) => {
      if (res && res.data) {
        this.title = res.data.title;
        this.imageSrc = res.data.image;
        this.description = this.subString(res.data.description);
      }
    });
  }

  subString(text) {
    if (text.length > 100) {
      return (text = text.substr(0, 100) + '...');
    }
    return text;
  }

  onLoadImgError(event: any) {
    event.target.src = '../../../../assets/images/games/video-post-ab-game.png';
  }
}
