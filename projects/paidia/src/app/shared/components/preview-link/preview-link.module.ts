import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PreviewLinkComponent } from './preview-link.component';

@NgModule({
  declarations: [PreviewLinkComponent],
  imports: [CommonModule],
  exports: [PreviewLinkComponent],
})
export class PreviewLinkModule {}
