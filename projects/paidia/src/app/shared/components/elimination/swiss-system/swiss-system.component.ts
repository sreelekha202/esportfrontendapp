import { EsportsChatService, EsportsToastService,EsportsUtilsService, EsportsBracketService } from 'esports';
import { Component, Input, OnInit } from "@angular/core";
import { ITournament } from "esports";

@Component({
  selector: "app-swiss-system",
  templateUrl: "./swiss-system.component.html",
  styleUrls: ["./swiss-system.component.scss"],
})
export class SwissSystemComponent implements OnInit {
  @Input() matchList: Array<any> = [];
  @Input() isSeeded: boolean;
  @Input() isAdmin: boolean;
  @Input() enableWebview: boolean;
  @Input() participantId: string;
  @Input() tournament: ITournament;
  @Input() disableStanding: boolean;
  @Input() hideMockStructure: boolean;

  isProccessing = false;
  isUpdateMatchScore = false;
  currentMatch;
  roundList = [1];
  activeRound = 1;
  tempList = [];
  hideDropdown = true;
  selectedNumber;
  roundStartDate: Date;

  constructor(
    private bracketService: EsportsBracketService,
    private utilsService: EsportsUtilsService,
    private toastService: EsportsToastService,
    private chatService: EsportsChatService
  ) {}

  ngOnInit(): void {}

  ngOnChanges() {
    if (this.tournament?.isSeeded && !this.hideMockStructure) {
      this.roundList = Array(this.tournament?.noOfRound || 0)
        .fill(1)
        .map((x, i) => x + i);
      this.tempList = this.matchList || [];
      this.fetchAllMatches();
    } else if (
      typeof this.tournament?.isSeeded == "boolean" &&
      !this.hideMockStructure
    ) {
      this.mockStructure();
    }

    if (this.tournament?.startDate) {
      this.setRoundDate();
    }
  }

  /** Get all saved matches of the tournamnet */
  fetchAllMatches = async () => {
    try {
      if (this.isProccessing) return;
      this.isProccessing = true;

/**
      const filterQuery = {
        tournamentId: this.tournament?._id,
        bye: false,
        "currentMatch.round": this.activeRound,
      };
      const optionQuery = {
        sort: {
          matchNo: 1,
        },
      };
      const option = `${this.utilsService.encodeQuery(optionQuery)}`;
      const queryParam = `?query=${this.utilsService.encodeQuery(filterQuery)}&select=${select}&option=${option}`;
 */
      const select = `_id,bye,currentMatch,matchStatus,teamA,teamB,disQualifiedTeam,teamAWinSet,teamBWinSet,matchNo,loserTeam2,tournamentId`;
      const queryParams = {
        tournamentId: this.tournament?._id,
        bye: false,
        "round": this.activeRound,
        select,
        sortBy: 'matchNo',
        sortOrder: 1
      }
      const response = await this.bracketService.fetchAllMatchesV3(queryParams);
      this.matchList = response.data;
      let matchset = [];
      for (let t = 0; t < this.matchList.length; t++) {
        matchset.push(this.matchList[t]._id);
      }
      this.chatService
        ?.startTournament(JSON.stringify(matchset))
        .subscribe((res) => {
        });

      this.tempList = response?.data;
      this.isProccessing = false;
    } catch (error) {
      this.isProccessing = false;
      this.toastService.showError(error.message);
    }
  };

  mockStructure = async () => {
    try {
      this.isProccessing = true;
      const payload = {
        bracketType: this.tournament?.bracketType,
        maximumParticipants: this.tournament?.maxParticipants,
        noOfSet: this.tournament?.noOfSet,
      };
      const response = await this.bracketService.generateBracket(payload);
      this.matchList = response.data;
      this.isProccessing = false;
    } catch (error) {
      this.isProccessing = false;
      this.toastService.showError(error.message);
    }
  };

  selectMatch(m) {
    this.currentMatch = m;
    this.isUpdateMatchScore = true;
  }

  selectedRound(r) {
    this.activeRound = r;
    this.setRoundDate();
    this.fetchAllMatches();
  }

  selectDropdownRound(r) {
    this.activeRound = r;
    this.selectedNumber = r;
    this.hideDropdown = true;
    this.setRoundDate();
    this.fetchAllMatches();
  }

  exitFromScoreCard(data) {
    this.isUpdateMatchScore = data.isOpenScoreCard;
    this.fetchAllMatches();
  }

  onSearch(text) {
    const value = text.toLowerCase().trim();
    this.matchList = this.tempList.filter((item: any) => {
      if (this.tournament?.participantType == "individual") {
        return (
          item?.teamA?.name.toLowerCase().indexOf(value) !== -1 ||
          item?.teamB?.name.toLowerCase().indexOf(value) !== -1
        );
      } else {
        return (
          item?.teamA?.teamName.toLowerCase().indexOf(value) !== -1 ||
          item?.teamB?.teamName.toLowerCase().indexOf(value) !== -1
        );
      }
    });
  }

  onClearSearch(event) {
    setTimeout(() => {
      if (!event?.target?.value) {
        this.onSearch(""); // reset search
      }
    }, 100);
  }

  setRoundDate = () => {
    const duration = this.tournament?.setDuration;
    const date = new Date(this.tournament?.startDate);
    date.setMinutes(date.getMinutes() + duration.mins * (this.activeRound - 1));
    date.setHours(date.getHours() + duration.hours * (this.activeRound - 1));
    date.setDate(date.getDate() + duration.days * (this.activeRound - 1));
    this.roundStartDate = date;
  };
}
