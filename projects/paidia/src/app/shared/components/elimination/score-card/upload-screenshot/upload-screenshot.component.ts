import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { S3UploadService } from '../../../../../core/service';
import { EsportsToastService } from 'esports';

@Component({
  selector: 'upload-screenshot',
  templateUrl: './upload-screenshot.component.html',
  styleUrls: ['./upload-screenshot.component.scss'],
})
export class UploadScreenshotComponent implements OnInit {
  constructor(
    private s3UploadService: S3UploadService,
    private eSportsToastService: EsportsToastService,
    public dialogRef: MatDialogRef<UploadScreenshotComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  isProcessing: boolean = false;
  screenShotDimension = { width: 180, height: 180 };
  screenShotSize = 1024 * 1024 * 2;
  screenshotUrl: string | null = '';

  ngOnInit(): void {
    this.dialogRef.beforeClosed().subscribe(data => {
      this.onClose();
    });
  }

  onClose() {
    this.dialogRef.close({
      screenshotUrl: this.screenshotUrl || this.data?.screenshotUrl,
      hasScreenshotUpdateAccess: this.data?.hasScreenshotUpdateAccess
    });
  }

  onCancel() {
    this.onClose();
  }

  screenShotUpload = async (event) => {
    try {
      this.isProcessing = true;

      const upload: any = await this.s3UploadService.validateAndUploadImage(
        event,
        this.screenShotDimension,
        this.screenShotSize,
        'SCREENSHOT'
      );
      this.screenshotUrl = upload?.data[0]?.Location;
      this.isProcessing = false;
      this.eSportsToastService.showSuccess(upload.message || "Screenshot uploaded successfully.")
    } catch (error) {
      this.isProcessing = false;
      this.eSportsToastService.showError(
        error?.error?.message || error?.message
      );
    }
  };
}
