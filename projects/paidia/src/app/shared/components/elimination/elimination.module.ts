import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { NgModule } from "@angular/core";

import { LoadingModule } from "../../../core/loading/loading.module";
import {
  EsportsModule,
  EsportsLoaderModule,
  PipeModule,
  EsportsEliminationModule,
  I18nModule
} from 'esports';

import { AwardsComponent } from "./awards/awards.component";
import { BattleRoyaleMultiComponent } from "./battle-royale-multi/battle-royale-multi.component";
import { CarouselComponent } from "./carousel/carousel.component";
import { chatComponent } from "./chatwindow/chat/chat.component";
import { ChatwindowComponent } from "./chatwindow/chatwindow.component";
import { DoubleEliminationComponent } from "./double-elimination/double-elimination.component";
import { EliminationComponent } from "./elimination.component";
import { MatchComponent } from "./match/match.component";
import { RoundRobinMultiComponent } from "./round-robin-multi/round-robin-multi.component";
import { ScoreCardComponent } from "./score-card/score-card.component";
import { SingleEliminationComponent } from "./single-elimination/single-elimination.component";
import { TeamComponent } from "./match/team/team.component";

import { CarouselModule } from "ngx-bootstrap/carousel";
import { LazyLoadImageModule } from "ng-lazyload-image";

import { MatTooltipModule } from "@angular/material/tooltip";
import { MatIconModule } from "@angular/material/icon";
import { MatProgressSpinnerModule } from "@angular/material/progress-spinner";
import { MatSidenavModule } from "@angular/material/sidenav";

import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { NgxFlagIconCssModule } from "ngx-flag-icon-css";
import { ProgressbarModule } from "ngx-bootstrap/progressbar";

import { TabsModule } from "ngx-bootstrap/tabs";
import { MatExpansionModule } from '@angular/material/expansion';
import { MaterialModule } from '../../modules/material.module';

import {
  FontAwesomeModule,
  FaIconLibrary,
} from "@fortawesome/angular-fontawesome";
import {
  faTrophy,
  faPencilAlt,
  faSortUp,
  faSortDown,
  faCommentAlt,
  faCaretSquareUp,
  faCheckCircle,
} from "@fortawesome/free-solid-svg-icons";
import { ChatsidebarComponent } from "./chatwindow/chatsidebar/chatsidebar.component";
import { TableHeaderCellComponent } from "./battle-royale-multi/table-header-cell/table-header-cell.component";
import { TableSliderCellComponent } from "./battle-royale-multi/table-slider-cell/table-slider-cell.component";
import { TableCounterCellComponent } from "./battle-royale-multi/table-counter-cell/table-counter-cell.component";
import { ScoreConfirmPopupComponent } from "./battle-royale-multi/score-confirm-popup/score-confirm-popup.component";
import { SwissSystemComponent } from "../../components/elimination/swiss-system/swiss-system.component";
import { SwissStandingComponent } from "../../components/elimination/swiss-system/swiss-standing/swiss-standing.component";
import { environment } from "../../../../environments/environment";
import { ParticipantTypePipe } from './pipe/participant-type.pipe';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { ViewTeamComponent } from './score-card/view-team/view-team.component';
import { ViewSetComponent } from './score-card/view-set/view-set.component';
import { FinalResultComponent } from "./score-card/final-result/final-result.component";
import { ScoreCounterComponent } from './score-card/score-counter/score-counter.component';
import { UploadScreenshotComponent } from './score-card/upload-screenshot/upload-screenshot.component';

@NgModule({
  declarations: [
    SwissSystemComponent,
    SwissStandingComponent,
    AwardsComponent,
    BattleRoyaleMultiComponent,
    CarouselComponent,
    chatComponent,
    ChatsidebarComponent,
    ChatwindowComponent,
    TableCounterCellComponent,
    DoubleEliminationComponent,
    EliminationComponent,
    MatchComponent,
    RoundRobinMultiComponent,
    ScoreCardComponent,
    SingleEliminationComponent,
    TeamComponent,
    ChatsidebarComponent,
    TableHeaderCellComponent,
    TableSliderCellComponent,
    ScoreConfirmPopupComponent,
    ParticipantTypePipe,
    ViewTeamComponent,
    ViewSetComponent,
    FinalResultComponent,
    ScoreCounterComponent,
    UploadScreenshotComponent,
  ],
  imports: [
    CarouselModule.forRoot(),
    CommonModule,
    FontAwesomeModule,
    FormsModule,
    LazyLoadImageModule,
    LoadingModule,
    MatProgressSpinnerModule,
    MatSidenavModule,
    NgbModule,
    NgxFlagIconCssModule,
    EsportsModule,
    EsportsLoaderModule,
    PipeModule,
    EsportsEliminationModule,
    MatSidenavModule,
    ProgressbarModule.forRoot(),
    TabsModule.forRoot(),
    I18nModule.forRoot(environment),
    MatTooltipModule,
    MatExpansionModule,
    MaterialModule,
    NgxDatatableModule,
  ],
  exports: [
    chatComponent,
    ChatsidebarComponent,
    ChatwindowComponent,
    EliminationComponent,
    ScoreCardComponent,
    MatSidenavModule
  ],
})
export class EliminationModule {
  constructor(private library: FaIconLibrary) {
    library.addIcons(
      faCaretSquareUp,
      faCheckCircle,
      faCommentAlt,
      faPencilAlt,
      faSortDown,
      faSortUp,
      faTrophy
    );
  }
}
