import { Component, OnInit, Input } from '@angular/core';
import { EsportsUserService } from 'esports';
import { IUser } from 'esports';

@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.scss'],
})
export class CommentComponent implements OnInit {
  @Input() placeholder: string = '';
  @Input() isReplying: boolean = false;

  currentUser: IUser;

  constructor(private userService: EsportsUserService) {}

  ngOnInit(): void {
    this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.currentUser = data;
      }
    });
  }
} 
