import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MessagePostComponent } from './message-post.component';
import { PreviewLinkModule } from '../preview-link/preview-link.module';

@NgModule({
  declarations: [MessagePostComponent],
  imports: [CommonModule, PreviewLinkModule],
  exports: [MessagePostComponent],
})
export class MessagePostModule {}
