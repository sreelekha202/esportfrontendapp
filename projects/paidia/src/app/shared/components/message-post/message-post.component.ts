import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-message-post',
  templateUrl: './message-post.component.html',
  styleUrls: ['./message-post.component.scss'],
})
export class MessagePostComponent implements OnInit {
  @Input() message: string = '';
  @Input() class = '';
  @Input() link: string = '';
  public innerHtml: string = '';
  constructor() {}

  ngOnInit(): void {
    if (this.message) {
      this.innerHtml = this.linkify(this.message);
    }
  }

  linkify(text: string) {
    const urlRegex =
      /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/gi;
    return text.replace(urlRegex, (url) => {
      return `<a href="${url}" target="_blank">${url}</a>`;
    });
  }

  getLink(text: string): any[] {
    const urlRegex =
      /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/gi;
    return text.match(urlRegex);
  }
}
