import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { EsportsUserService,EsportsToastService,} from 'esports';

@Component({
  selector: 'app-search-players',
  templateUrl: './search-players.component.html',
  styleUrls: ['./search-players.component.scss'],
})
export class SearchPlayersComponent implements OnInit {
  @Input() heading: string = "";
  @Input() listTitle: string = "";
  @Input() members
  @Output() list = new EventEmitter<any>();
  @Output() submitData = new EventEmitter<any>();;

  players: any = [];
  text: string = '';
  constructor(
       private userService: EsportsUserService,
      private eSportsToastService: EsportsToastService,) { }

  ngOnInit(): void {
    // MOCK PLAYERS DATA

    // this.players = [
    //   {
    //     image:
    //       'https://www.advertisingweek360.com/wp-content/uploads/2018/10/169girlgamers-1170x600.jpg',
    //     nickname: 'killmonger',
    //     isSelected: false,
    //   },
    //   {
    //     image: 'https://cdn6.dissolve.com/p/D538_291_180/D538_291_180_1200.jpg',
    //     nickname: 'xxinfinity01',
    //     isSelected: true,
    //   },
    //   {
    //     image:
    //       'https://media.suara.com/pictures/970x544/2020/05/10/60545-main-game.jpg',
    //     nickname: 'supergirl004',
    //     isSelected: false,
    //   },
    //   {
    //     image:
    //       'https://upload.wikimedia.org/wikipedia/commons/6/6a/GORDOx-BGS-2018.jpg',
    //     nickname: 'tinythor',
    //     isSelected: true,
    //   },
    // ];
    this.text = "";
    this.getvalues('');
    setInterval(() => {
      this.senddata();
    }, 1000);
  }

  senddata() {
    let data1 = [];
    for (let data of this.players) {
      if (data.isSelected == true) {
        data1.push(data)
      }
    }
    this.list.emit(data1);
  }

  addPlayer(index: number): void {

let exists=this.playerExists(this.players[index]._id)

   if(exists){
    this.eSportsToastService.showError("already exists");
   } else{
    this.players[index].isSelected = !this.players[index].isSelected
    this.submitData.emit(this.players[index]);
   }
  }
  playerExists(id) {
    return this.members.some((el)=>{
     return el._id == id;
   });
  }

  filteredPlayer() {
    return this.players.filter((obj) => obj.nickname.includes(this.text.toLowerCase().trim()));
  }

  getvalues(e) {
    let pre_player = this.players
    this.players = []
    this.userService.searchUsers(e).subscribe(
      (res: any) => {
        this.players = res;
        for (let d of this.players) {
          d.isSelected = false;
        }
        for (let d2 of pre_player) {
          if (d2.isSelected == true) {
            let f = 1;
            for (let d3 of this.players) {
              if (d3.fullName == d2.fullName) {
                f = 2;
                d3.isSelected = true
              }
            }
            if (f == 1) {
              this.players.push(d2)
            }
          }
        }
      },
      (err) => { }
    );
  }

}
