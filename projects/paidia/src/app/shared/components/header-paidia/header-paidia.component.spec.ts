import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderPaidiaComponent } from './header-paidia.component';

describe('PaidiaHeaderComponent', () => {
  let component: HeaderPaidiaComponent;
  let fixture: ComponentFixture<HeaderPaidiaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HeaderPaidiaComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderPaidiaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
