import { Component, Inject, Input, OnInit } from '@angular/core';
import {
  EsportsSocialService,
  EsportsArticleService,
  IUser,
  IPOST,
  POST_TYPE,
} from 'esports';
import { environment } from '../../../../environments/environment';
@Component({
  selector: 'app-main-right',
  templateUrl: './main-right.component.html',
  styleUrls: ['./main-right.component.scss'],
})
export class MainRightComponent implements OnInit {
  @Input() currentUser: IUser;

  public postType = POST_TYPE;
  public trendingPosts: Array<IPOST> = [];
  public articles: any[] = [];
  public iconSrc = {
    trendingPosts: 'assets/icons/Home/trending.svg',
    latestArticles: 'assets/icons/Home/Group.svg',
    link: 'assets/icons/Home/link.svg',
    gallery: 'assets/icons/Home/gallery.svg',
    star: 'assets/icons/Home/star.svg',
    guide: 'assets/icons/Home/guide- icon.svg',
  };

  constructor(
    private _socialService: EsportsSocialService,
    private _articleService: EsportsArticleService
  ) {}

  ngOnInit(): void {
    this.getTrendingPosts();
    this.getArticles();
  }

  private getTrendingPosts() {
    this._socialService
      .getTrendingPosts()
      .subscribe((res: any) => (this.trendingPosts = res?.data?.posts));
  }

  private getArticles() {
    this._articleService
      .getTrendingPosts(environment.apiEndPoint)
      .subscribe((res: any) => (this.articles = res?.data.slice(0, 5)));
  }

  public getTrendingMessage(message: string): string {
    return message ? message : 'SOCIAL_FEED.LABELS.IMAGE_VIDEO';
  }
}
