import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainRightComponent } from './main-right.component';
import { MomentModule } from 'ngx-moment';
import { environment } from '../../../../environments/environment';
import { I18nModule } from 'esports';
import { HeadingComponent } from './heading/heading.component';

@NgModule({
  declarations: [MainRightComponent, HeadingComponent],
  imports: [CommonModule, MomentModule, I18nModule.forRoot(environment)],
  exports: [MainRightComponent],
})
export class MainRightModule {}
