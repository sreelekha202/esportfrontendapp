import { Component, Input, OnInit } from '@angular/core';
import { IMODAL_POST, IUser, POST_EVENT_TYPE, POST_TYPE } from 'esports';
import { AppHtmlBroadcastRoutes } from '../../../../app-routing.model';
import { onLoadImgError, TYPE_IMG_LOAD } from '../../../comon';

@Component({
  selector: 'app-header-wall',
  templateUrl: './header-wall.component.html',
  styleUrls: ['./header-wall.component.scss'],
})
export class HeaderWallComponent implements OnInit {
  @Input() enable: boolean = false;
  @Input() enableGoLive: boolean = true;
  @Input() currentUser: IUser;
  @Input() openModalPost: Function;
  public postType = POST_TYPE;
  public typeImgLoad = TYPE_IMG_LOAD;
  public onLoadImgError: Function = onLoadImgError;
  public AppHtmlBroadcastRoutes = AppHtmlBroadcastRoutes;

  constructor() {}

  ngOnInit(): void {}

  public onCreatePost(showLinkContainer?: boolean) {
    const payLoad: IMODAL_POST = {
      type: POST_EVENT_TYPE.ADD,
      showLinkContainer,
      username: this.currentUser.username,
    };
    this.openModalPost(payLoad);
  }
}
