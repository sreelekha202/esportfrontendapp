import { Component, OnInit, Input } from '@angular/core';
import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';
import { EsportsSocialService, IPREVIEW_LINK } from 'esports';
import { onLoadImgError } from '../../../comon';

@Component({
  selector: 'app-preview-link',
  templateUrl: './preview-link.component.html',
  styleUrls: ['./preview-link.component.scss'],
  providers: [NgbCarouselConfig],
})
export class PreviewLinkComponent implements OnInit {
  @Input() link: string = '';
  public metadata: IPREVIEW_LINK = { image: '', title: '', description: '' };
  public onLoadImgError: Function = onLoadImgError;

  constructor(private socialService: EsportsSocialService) {}

  ngOnInit(): void {
    if (this.link) {
      this.getPreview();
    }
  }

  private getPreview() {
    this.socialService.getPreviewURL(this.link).subscribe(
      (res) => {
        const data = res?.data;
        if (!data) throw Error();
        this.metadata.image = data.image;
        this.metadata.title = this.subString(data?.title);
        this.metadata.description = this.subString(data?.description);
      },
      (err) => {
        throw err;
      }
    );
  }

  private subString(text: string) {
    return text.length > 100 ? text.substring(0, 100) + '...' : text;
  }
}
