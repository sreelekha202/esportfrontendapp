import {
  Component,
  OnInit,
  Inject,
  Optional,
  ViewChild,
  ElementRef,
  AfterViewInit,
} from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import {
  EsportsToastService,
  EsportsSocialService,
  IPOST,
  POST_TYPE,
  FILE_TYPE,
  IMODAL_POST,
  POST_EVENT_TYPE,
} from 'esports';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

const _MAXFILEAMMOUNT = 6;

@Component({
  selector: 'app-modal-post',
  templateUrl: './modal-post.component.html',
  styleUrls: ['./modal-post.component.scss'],
})
export class ModalPostComponent implements OnInit, AfterViewInit {
  @ViewChild('txtMessage') txtMessage: ElementRef;
  @ViewChild('txtUrl') txtUrl: ElementRef;
  @ViewChild('selectPostType') selectPostType: ElementRef;
  @ViewChild('selectText') selectText: ElementRef;
  @ViewChild('btnSubmit') btnSubmit: ElementRef;

  public modalPostTitle = 'CREATE';
  public username: string = '';
  public post: IPOST = {
    message: '',
    url: '',
    imagesLink: [],
    videoLink: [],
    postType: POST_TYPE.MEDIA,
  };
  public showLinkContainer: boolean = false;
  public modalpostType: POST_EVENT_TYPE = POST_EVENT_TYPE.ADD;
  public loading: boolean = false;
  public selectedFileCount = 0;
  public isLoadingFile: boolean = false;
  public priviewFileUploads: any[] = [];
  public showEmojiPicker: boolean = false;
  public enableSubmit: boolean = false;
  public isOpenSelect: boolean = false;
  public bitSize: number = 1024;
  public videoMaxSize: number = 100;

  constructor(
    @Optional() @Inject(MAT_DIALOG_DATA) public data: any,
    private _dialogRef: MatDialogRef<ModalPostComponent>,
    private _toastService: EsportsToastService,
    private _socialService: EsportsSocialService,
    private _translateService: TranslateService
  ) {}

  ngOnInit(): void {
    const data: IMODAL_POST = this._dialogRef?.componentInstance?.data;
    this.username = data?.username;
    this.modalPostTitle = data?.type;
    this.showLinkContainer = data?.showLinkContainer || false;
    if (data?.post) {
      this.modalPostTitle = POST_EVENT_TYPE.EDIT;
      this.post = data.post;
      const image = this.getPreviewMediaFile(
        this.post.imagesLink,
        FILE_TYPE.IMAGE
      );
      const video = this.getPreviewMediaFile(
        this.post.videoLink,
        FILE_TYPE.VIDEO
      );
      this.priviewFileUploads = [...image, ...video];
    }
  }
  ngAfterViewInit(): void {
    this.txtMessage.nativeElement.focus();
  }

  public onToggleSelect() {
    this.isOpenSelect = !this.isOpenSelect;
  }

  public onCustomSelect(event) {
    this.selectText.nativeElement.innerText = event.target.innerText;
    this.selectPostType.nativeElement.setAttribute(
      'value',
      event.target.getAttribute('value')
    );
    this.isOpenSelect = !this.isOpenSelect;
  }

  public onInputMessage(value) {
    this.showLinkContainer
      ? (this.enableSubmit = !!this.post?.url)
      : (this.enableSubmit = !!value);
  }

  public onInputLink(value: string) {
    const urlRegex =
      /[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)?/gi;
    const regex = new RegExp(urlRegex);
    const isMatchUrl = !!value.match(regex);
    this.post.postType = isMatchUrl ? POST_TYPE.LINK : POST_TYPE.MEDIA;
    this.enableSubmit = isMatchUrl;
    this.post.imagesLink = [];
    this.post.videoLink = [];
    this.priviewFileUploads = [];
  }

  public onSelectedFiles(files: any[]) {
    const fileSize = files[0].size / this.bitSize / this.bitSize;
    const selectedFilesAmount = files?.length;
    if (fileSize > this.videoMaxSize) {
      this._toastService.showError(
        this._translateService.instant('SOCIAL_FEED.MESSAGES.VIDEO_LIMITED')
      );
      return;
    }
    if (this.selectedFileCount + selectedFilesAmount > _MAXFILEAMMOUNT) {
      this._toastService.showError(
        this._translateService.instant('SOCIAL_FEED.MESSAGES.FILES_LIMITED')
      );
      return;
    }
    this.selectedFileCount += selectedFilesAmount;
    this.showLinkContainer = false;
    for (let file of files) {
      this.isLoadingFile = true;
      this.handlerFile(file);
    }
  }

  public removeFileUpload(index: number, url: string, type: string) {
    this.removePreviewFile(index);
    this.removeLinkUpload(url, type);
    this.selectedFileCount--;
  }

  public showLink() {
    this.showLinkContainer = true;
  }

  public goBack(): void {
    this.showLinkContainer = false;
  }

  public onHandlePost() {
    this.post.isPrivate =
      this.selectPostType.nativeElement.getAttribute('value') || false;
    this.modalpostType == POST_EVENT_TYPE.ADD
      ? this.addPost()
      : this.editPost();
  }

  private handlerFile(file: any) {
    const fileName = file.name;
    const fileType = file.type.split('/')[0];
    if (!this.validFile(fileName, fileType)) {
      return;
    }
    this._socialService.signedFile(fileName, fileType).subscribe((res: any) => {
      const url = res?.data?.imageURL;
      const signedRequest = res?.data?.uploadURL;
      this.putFile(signedRequest, file, url);
      const fileReader = new FileReader();
      fileReader.readAsDataURL(file);
      fileReader.onload = () => {
        const data = fileReader.result;
        this.priviewFileUploads.push({ url, data, type: fileType });
        this.post.postType = POST_TYPE.MEDIA;
        this.isLoadingFile = false;
        switch (fileType) {
          case FILE_TYPE.IMAGE:
            this.post.imagesLink.push(url);
            break;
          case FILE_TYPE.VIDEO:
            this.post.videoLink.push(url);
            break;
        }
        this.enableSubmit =
          this.priviewFileUploads.length == this.selectedFileCount;
      };
    });
  }

  private editPost() {
    const data = { postType: POST_TYPE.MEDIA, ...this.post };
    delete data?._id;
    this._socialService.updatePost(this.post._id, data).subscribe(
      (res) => {
        if (!res?.data) throw Error();
        this._dialogRef.close({ data: this.post });
        this._toastService.showSuccess(
          this._translateService.instant(
            'SOCIAL_FEED.MESSAGES.UPLOAD_POST_SUCCESS'
          )
        );
      },
      (err) =>
        this._toastService.showError(
          this._translateService.instant('SOCIAL_FEED.MESSAGES.TRY_AGAIN')
        )
    );
  }

  private addPost() {
    this._socialService.addNewPost(this.post).subscribe(
      (res: any) => {
        if (!res?.data) throw Error();
        this._dialogRef.close({ data: res?.data });
        this._toastService.showSuccess(
          this._translateService.instant(
            'SOCIAL_FEED.MESSAGES.ADD_POST_SUCCESS'
          )
        );
      },
      (err) =>
        this._toastService.showError(
          this._translateService.instant('SOCIAL_FEED.MESSAGES.TRY_AGAIN')
        )
    );
  }

  private putFile(signedURL: string, file: any, url: string) {
    const xhr = new XMLHttpRequest();
    const fileType = file.type.split('/')[0];
    xhr.open('PUT', signedURL);
    xhr.onreadystatechange = () => {
      if (xhr?.readyState === 4) {
        if (xhr?.status !== 200)
          this._toastService.showError(
            this._translateService.instant('SOCIAL_FEED.MESSAGES.TRY_AGAIN')
          );
      }
    };
    xhr.send(file);
  }

  private validFile(name: string, type: string): boolean {
    if (!(type == FILE_TYPE.IMAGE || type == FILE_TYPE.VIDEO)) {
      this._toastService.showError(
        `${name} ${this._translateService.instant(
          'SOCIAL_FEED.MESSAGES.INVALID'
        )}!`
      );
      return false;
    }
    return true;
  }

  private removePreviewFile(index: number) {
    this.priviewFileUploads.splice(index, 1);
  }

  private removeLinkUpload(url: string, type: string) {
    switch (type) {
      case FILE_TYPE.IMAGE:
        this.post.imagesLink = this.post.imagesLink.filter(
          (imgItem) => imgItem != url
        );
        break;
      case FILE_TYPE.VIDEO:
        this.post.videoLink = this.post.videoLink.filter(
          (videoItem) => videoItem != url
        );
        break;
    }
  }

  private getPreviewMediaFile(mediaUrls: any[], type: FILE_TYPE) {
    return mediaUrls.map((itemUrl) => {
      return { url: itemUrl, data: itemUrl, type };
    });
  }
}
