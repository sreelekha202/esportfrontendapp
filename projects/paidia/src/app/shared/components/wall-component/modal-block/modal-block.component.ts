import { Component, OnInit, Inject, Optional } from '@angular/core';
import {
  EsportsChatService,
  EsportsSocialService,
  EsportsToastService,
  IO_REPORT,
} from 'esports';
import {
  MatDialog,
  MAT_DIALOG_DATA,
  MatDialogRef,
} from '@angular/material/dialog';
import { TranslateService } from '@ngx-translate/core';
@Component({
  selector: 'app-modal-block',
  templateUrl: './modal-block.component.html',
  styleUrls: ['./modal-block.component.scss'],
})
export class ModalBlockComponent implements OnInit {
  public stepBlock: number = 1;
  public idOption_2: string = '';
  public postId: string = '';
  public postAuthor: any;
  public optionsReport = [
    {
      id: 1,
      issue: 'SOCIAL_FEED.MESSAGES.OPTION_REPORT.ISS.ISS_1',
      description: 'SOCIAL_FEED.MESSAGES.OPTION_REPORT.DESC.DESC_1',
    },
    {
      id: 2,
      issue: 'SOCIAL_FEED.MESSAGES.OPTION_REPORT.ISS.ISS_2',
      description: 'SOCIAL_FEED.MESSAGES.OPTION_REPORT.DESC.DESC_2',
    },
    {
      id: 3,
      issue: 'SOCIAL_FEED.MESSAGES.OPTION_REPORT.ISS.ISS_3',
      description: 'SOCIAL_FEED.MESSAGES.OPTION_REPORT.DESC.DESC_3',
    },
    {
      id: 4,
      issue: 'SOCIAL_FEED.MESSAGES.OPTION_REPORT.ISS.ISS_4',
      description: 'SOCIAL_FEED.MESSAGES.OPTION_REPORT.DESC.DESC_4',
    },
    {
      id: 5,
      issue: 'SOCIAL_FEED.MESSAGES.OPTION_REPORT.ISS.ISS_5',
      description: 'SOCIAL_FEED.MESSAGES.OPTION_REPORT.DESC.DESC_5',
    },
  ];

  constructor(
    public _dialog: MatDialog,
    @Optional() @Inject(MAT_DIALOG_DATA) public data: any,
    public _dialogRef: MatDialogRef<ModalBlockComponent>,
    private _socialService: EsportsSocialService,
    private _chatService: EsportsChatService,
    private _toastService: EsportsToastService,
    private _translateService: TranslateService
  ) {}
  ngOnInit(): void {
    this.postId = this._dialogRef.componentInstance.data.postId;
    this.postAuthor = this._dialogRef.componentInstance.data.postAuthor;
  }

  nextOption(type): void {
    switch (type) {
      case 2:
        if (this.idOption_2 !== '') {
          this.stepBlock++;
        }
        break;
      case 3:
        this.stepBlock++;
        const data = { status: 'open', reportText: this.idOption_2 };
        this.reportPost(type, data);
        break;
      case 5:
        break;
      default:
        this.stepBlock++;
        break;
    }
  }

  chooseOptionsReport2(issue: string): void {
    this.idOption_2 = issue;
  }

  backStep(type): void {
    if (type != 1) {
      this.stepBlock--;
    }
  }

  private reportPost(type, data) {
    this._socialService.reportPost(this.postId, type, data).subscribe(
      (res: any) => {
        const resData = res?.data;
        if (!resData) throw Error();
        const payload: IO_REPORT = {
          message: this._translateService.instant(
            'SOCIAL_FEED.MESSAGES.REPORT_PORT'
          ),
          reportId: resData?._id,
          postAuthor: this.postAuthor,
        };
        this._chatService.emitReportPost(payload);
        this._toastService.showSuccess(
          this._translateService.instant('SOCIAL_FEED.MESSAGES.REPORT_SUCCESS')
        );
      },
      (err) => {
        this._toastService.showError(
          this._translateService.instant('SOCIAL_FEED.MESSAGES.REPORT_FAILED')
        );
        throw err;
      }
    );
  }
}
