import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { TranslateService } from '@ngx-translate/core';
import {
  EsportsSocialService,
  EsportsToastService,
  GENDER,
  IMODAL_POST,
  IPOST,
  IUser,
} from 'esports';
import { setCreatedBy } from '../../comon';
import { ModalPostComponent } from './modal-post/modal-post.component';

@Component({
  selector: 'app-wall-component',
  templateUrl: './wall-component.component.html',
  styleUrls: ['./wall-component.component.scss'],
})
export class WallComponentComponent implements OnInit {
  @Input() enableHeader: boolean = false;
  @Input() enableGoLive: boolean = true;
  @Input() enablePrivatePost: boolean = false;
  @Input() currentUser: IUser = {
    _id: '',
    username: '',
    profilePicture: '',
    gender: '',
    customPronoun: '',
  };
  @Input() posts: Array<IPOST> = [];

  public isLogin: boolean = Boolean(localStorage.getItem('DUID'));
  public onHandlerModalPostEvent: Function;
  public onHandlerGender: Function;
  public onHandlerDeletePostById: Function;

  constructor(
    private _dialog: MatDialog,
    private _socialService: EsportsSocialService,
    private _toastService: EsportsToastService,
    private _translateService: TranslateService
  ) {}

  ngOnInit(): void {
    this.onHandlerModalPostEvent = this.modalPostEvent.bind(this);
    this.onHandlerGender = this.getGender.bind(this);
    this.onHandlerDeletePostById = this.deletePostById.bind(this);
  }

  private modalPostEvent(payload: IMODAL_POST) {
    const modalPostDialog = this._dialog.open(ModalPostComponent, {
      data: payload,
    });
    modalPostDialog.afterClosed().subscribe((rs) => {
      const post: IPOST = rs?.data;
      if (post && (this.enablePrivatePost || post?.isPrivate == false)) {
        post.createdBy = setCreatedBy(this.currentUser);
        const postIndex = this.posts.findIndex(
          (postItem) => postItem._id == post._id
        );
        postIndex != -1
          ? (this.posts[postIndex] = post)
          : this.posts.unshift(post);
      }
    });
  }

  private deletePostById(postId: string) {
    const confirmMessage = this._translateService.instant(
      'SOCIAL_FEED.MESSAGES.CONFIRM_DELETE_POST'
    );
    if (confirm(confirmMessage)) {
      this._socialService.deletePostById(postId).subscribe(
        (res: any) => {
          const resData = res?.data;
          if (!resData) throw Error();
          this.posts = this.posts.filter((post) => post._id != postId);
          this._toastService.showSuccess(
            this._translateService.instant(
              'SOCIAL_FEED.MESSAGES.DELETE_POST_SUCCESS'
            )
          );
        },
        (err) => {
          this._toastService.showError(
            this._translateService.instant('SOCIAL_FEED.MESSAGES.TRY_AGAIN')
          );
          throw err;
        }
      );
    }
  }

  private getGender(gender: GENDER, customPronoun) {
    let pronoun: string;
    switch (gender) {
      case GENDER.HE:
        pronoun = GENDER.HE.toUpperCase();
        break;
      case GENDER.SHE:
        pronoun = GENDER.SHE.toUpperCase();
        break;
      case GENDER.THEY:
        pronoun = GENDER.THEY.toUpperCase();
        break;
      default:
        return customPronoun;
    }
    return this._translateService.instant(`SOCIAL_FEED.USER.GENDER.${pronoun}`);
  }
}
