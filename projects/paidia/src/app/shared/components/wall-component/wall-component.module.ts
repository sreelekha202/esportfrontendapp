import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WallComponentComponent } from './wall-component.component';
import { ModalPostComponent } from './modal-post/modal-post.component';
import { MatDialogModule } from '@angular/material/dialog';
import { FormsModule } from '@angular/forms';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { I18nModule } from 'esports';
import { environment } from '../../../../environments/environment';
import { PreviewLinkComponent } from './preview-link/preview-link.component';
import { HeaderWallComponent } from './header-wall/header-wall.component';
import { PostComponentComponent } from './post-component/post-component.component';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { NgbCarouselModule } from '@ng-bootstrap/ng-bootstrap';
import { MomentModule } from 'ngx-moment';
import { MatTooltipModule } from '@angular/material/tooltip';
import { ReactionInfoComponent } from './post-component/action-component/reaction-info/reaction-info.component';
import { MatTabsModule } from '@angular/material/tabs';
import { ReactionContainerComponent } from './post-component/action-component/reaction-info/reaction-container/reaction-container.component';
import { ModalBlockComponent } from './modal-block/modal-block.component';
import { MatRadioModule } from '@angular/material/radio';
import { CommentPostComponent } from './post-component/comment-post/comment-post.component';
import { ActionComponentComponent } from './post-component/action-component/action-component.component';
import { AddCommnentComponent } from './post-component/add-commnent/add-commnent.component';
import { PickerModule } from '@ctrl/ngx-emoji-mart';
const components = [
  WallComponentComponent,
  ModalPostComponent,
  PreviewLinkComponent,
  HeaderWallComponent,
  PostComponentComponent,
  ReactionInfoComponent,
  ReactionContainerComponent,
  ModalBlockComponent,
  AddCommnentComponent,
  CommentPostComponent,
  ActionComponentComponent,
];

const modules = [
  PickerModule,
  CommonModule,
  I18nModule.forRoot(environment),
  MatDialogModule,
  FormsModule,
  MatProgressSpinnerModule,
  MatMenuModule,
  MatIconModule,
  NgbCarouselModule,
  MomentModule,
  MatTooltipModule,
  MatTabsModule,
  MatRadioModule,
];

@NgModule({
  declarations: [...components],
  imports: [...modules],
  exports: [WallComponentComponent],
})
export class WallComponentModule {}
