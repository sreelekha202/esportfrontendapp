import {
  Component,
  DoCheck,
  Input,
  OnInit,
  ViewChild,
  ElementRef,
  AfterViewInit,
} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { TranslateService } from '@ngx-translate/core';
import {
  EsportsSocialService,
  EsportsToastService,
  IUser,
  IPOST,
  POST_TYPE,
  POST_EVENT_TYPE,
  IMODAL_POST,
  ICOMMENT,
  EsportsChatService,
  IO_COMMENT,
} from 'esports';
import { AppHtmlBroadcastRoutes } from '../../../../app-routing.model';
import { onLoadImgError, setCreatedBy, TYPE_IMG_LOAD } from '../../../comon';
import { ModalBlockComponent } from '../modal-block/modal-block.component';
const LIMIT_COMMENT = 5;
@Component({
  selector: 'app-post-component',
  templateUrl: './post-component.component.html',
  styleUrls: ['./post-component.component.scss'],
})
export class PostComponentComponent implements OnInit, AfterViewInit {
  @Input() currentUser: IUser;
  @Input() post: IPOST;
  @Input() isLogin: boolean = false;
  @Input() openModalPost: Function;
  @Input() getGender: Function;
  @Input() deletePostById: Function;
  @ViewChild('message') elementView: ElementRef;

  public AppHtmlBroadcastRoutes = AppHtmlBroadcastRoutes;
  public typeImgLoad = TYPE_IMG_LOAD;
  public onLoadImgError: Function = onLoadImgError;
  public postType = POST_TYPE;
  public postTypeIcon: string = 'assets/icons/text-field.svg';
  public EVENT_TYPES = {
    SHARE_NEW_FEED: 'SHARE_NEW_FEED',
    SHARE_VIA: 'SHARE_VIA',
    BLOCK_USER: 'BLOCK_USER',
    EDIT_POST: 'EDIT_POST',
    DELETE_POST: 'DELETE_POST',
  };
  public commentAmmount: number = 0;
  public showComment: boolean = false;
  public onHandlerDeleteComment: Function;
  public onHandlerUpdateComment: Function;
  public pageTotal: number = 0;
  public pageComment: number = 0;
  public showMoreCommentLoader: boolean = false;
  public isAddCommentLoader: boolean = false;
  public isShowMorePost: boolean = false;
  public postSize: number = 180;
  public shareFacebook: string =
    'https://www.facebook.com/sharer/sharer.php?u=';
  public shareTwitter: string = 'https://twitter.com/intent/tweet?&url=';
  constructor(
    public dialog: MatDialog,
    private _translateService: TranslateService,
    private _socialService: EsportsSocialService,
    private _chatService: EsportsChatService,
    private _toastService: EsportsToastService
  ) {}

  ngOnInit(): void {
    this.onHandlerDeleteComment = this.deleteComment.bind(this);
    this.onHandlerUpdateComment = this.updateComment.bind(this);
    this.commentAmmount = this.post?.comments?.length || 0;
    this.post.comments = [];
  }

  ngAfterViewInit() {
    this.viewInitShowMore();
  }

  public viewInitShowMore() {
    if (this.elementView) {
      if (this.elementView.nativeElement.offsetHeight > this.postSize) {
        this.isShowMorePost = true;
      }
    }
  }

  public linkShare(id: any, type: any) {
    let link = '';
    switch (type) {
      case 'facebook':
        link =
          this.shareFacebook +
          window.location.hostname +
          `/post/${id}` +
          ';src=sdkpreparse';
        break;
      case 'twitter':
        link = this.shareTwitter + window.location.hostname + `/post/${id}`;
        break;
      default:
        break;
    }
    return link;
  }

  public handleShowMorePost() {
    this.isShowMorePost = false;
  }

  public popupEvent(eventType: string, post: any) {
    switch (eventType) {
      case this.EVENT_TYPES.BLOCK_USER:
        this.openBlockUser(post);
        return;
      case this.EVENT_TYPES.EDIT_POST:
        const payload: IMODAL_POST = {
          type: POST_EVENT_TYPE.EDIT,
          username: this.currentUser.username,
          post: this.post,
        };
        if (this.post.postType == POST_TYPE.MEDIA) {
          this.openModalPost(payload);
        }
        return;
      case this.EVENT_TYPES.DELETE_POST:
        this.deletePostById(this.post._id);
        return;
      case this.EVENT_TYPES.SHARE_NEW_FEED:
        //isPrivate = false
        return;
      case this.EVENT_TYPES.SHARE_VIA:
        //isPrivate = false
        return;
    }
  }

  public openBlockUser(post: any) {
    const data = { postId: post?._id, postAuthor: post?.createdBy?._id };
    const dialogRef = this.dialog.open(ModalBlockComponent, { data: data });
    dialogRef.afterClosed().subscribe((result) => {});
  }

  public onGetComment(event) {
    this.addComment(event);
  }

  public onToggleComment() {
    !this.showComment;
  }

  public getPostType() {
    let postType = 'SHARED_A_TEST_POST';
    this.postTypeIcon = 'assets/icons/text-field.svg';
    switch (this.post.postType) {
      case POST_TYPE.MEDIA:
        if (this.post?.imagesLink[0] || this.post?.videoLink[0]) {
          postType = 'SHARED_A_PHOTO_VIDEO';
          this.postTypeIcon = 'assets/icons/gallery.svg';
        }
        break;
      case POST_TYPE.LINK:
        postType = 'SHARED_A_LINK';
        this.postTypeIcon = '';
        break;
      case POST_TYPE.SHARE:
        postType = 'SHARED_A_POST';
        this.postTypeIcon = '';
        break;
      case POST_TYPE.BROAD_CAST:
        postType = 'IS_LIVE_NOW';
        this.postTypeIcon = 'assets/icons/broadcast_type.svg';
        break;
    }
    return this._translateService.instant(`SOCIAL_FEED.LABELS.${postType}`);
  }

  public onGetToggleComment(value) {
    this.showComment = value;
    if (value && this.post?.comments?.length == 0) {
      this.isAddCommentLoader = value;
      this.getComment(0, LIMIT_COMMENT);
    }
  }

  public showMoreComment(page: number) {
    this.showMoreCommentLoader = true;
    this.pageComment = page + 1;
    this.getComment(0, (this.pageComment + 1) * LIMIT_COMMENT);
  }

  private getComment(skip: number, limit: number) {
    this._socialService.getCommentByPost(this.post?._id, skip, limit).subscribe(
      (res) => {
        const comments: Array<ICOMMENT> = res?.data?.comments || [];
        this.post.comments = comments;
        this.isAddCommentLoader = false;
        this.showMoreCommentLoader = false;
        this.pageTotal = Math.floor(res?.data?.total / LIMIT_COMMENT);
      },
      (err) => {
        this.isAddCommentLoader = false;
        this._toastService.showError(
          this._translateService.instant('SOCIAL_FEED.MESSAGES.TRY_AGAIN')
        );
        throw err;
      }
    );
  }

  private addComment(payload: ICOMMENT) {
    this.isAddCommentLoader = true;
    this._socialService.addComment(this.post._id, payload).subscribe(
      (res) => {
        const data: ICOMMENT = res?.data;
        if (!data) throw Error();
        const comment = { ...data, createdBy: setCreatedBy(this.currentUser) };
        this.post.comments.unshift(comment);
        this.commentAmmount++;
        this._toastService.showSuccess(
          this._translateService.instant(
            'SOCIAL_FEED.MESSAGES.ADD_COMMENT_SUCCESS'
          )
        );
        this.isAddCommentLoader = false;
        const postAuthor = this.post?.createdBy?._id;
        if (postAuthor != this.currentUser._id) {
          const payload: IO_COMMENT = {
            postAuthor,
            commentId: comment?._id,
            message: this._translateService.instant(
              'SOCIAL_FEED.MESSAGES.COMMENT_POST'
            ),
          };
          this._chatService.emitCommentPost(payload);
        }
      },
      (err) => {
        this._toastService.showError(
          this._translateService.instant('SOCIAL_FEED.MESSAGES.TRY_AGAIN')
        );
        throw err;
      }
    );
  }

  private updateComment(payload: ICOMMENT) {
    const comment: ICOMMENT = {
      message: payload?.message,
      imagesLink: payload?.imagesLink,
    };
    this._socialService.updateComment(payload?._id, comment).subscribe(
      (res) => {
        if (!res?.data) throw Error;
        this.editComment(payload._id, payload?.message, payload?.imagesLink);
      },
      (err) => {
        this._toastService.showError(
          this._translateService.instant('SOCIAL_FEED.MESSAGES.TRY_AGAIN')
        );
        throw err;
      }
    );
  }

  private editComment(commentId: string, message: string, imagesLink: string) {
    const index = this.post.comments.findIndex(
      (comment) => comment._id == commentId
    );
    this.post.comments[index] = {
      ...this.post.comments[index],
      message,
      imagesLink,
    };
  }

  private deleteComment(commentId: string) {
    const confirmMessage = this._translateService.instant(
      'SOCIAL_FEED.MESSAGES.CONFIRM_DELETE_COMMENT'
    );
    if (confirm(confirmMessage)) {
      this._socialService.deleteComment(commentId).subscribe(
        (res) => {
          if (!res?.data) throw Error;
          this.post.comments = this.post.comments.filter(
            (comment) => comment._id != commentId
          );
          this.commentAmmount--;
          this._toastService.showSuccess(
            this._translateService.instant(
              'SOCIAL_FEED.MESSAGES.DELETE_COMMENT_SUCCESS'
            )
          );
        },
        (err) => {
          this._toastService.showError(
            this._translateService.instant('SOCIAL_FEED.MESSAGES.TRY_AGAIN')
          );
          throw err;
        }
      );
    }
  }
}
