import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import {
  EsportsChatService,
  EsportsSocialService,
  EsportsToastService,
  ILIKE,
  IO_LIKE,
  IUser,
  LKIE_REACT,
} from 'esports';
import { setCreatedBy } from '../../../../comon';
import { ReactionInfoComponent } from './reaction-info/reaction-info.component';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-action-component',
  templateUrl: './action-component.component.html',
  styleUrls: ['./action-component.component.scss'],
})
export class ActionComponentComponent implements OnInit {
  @Input() postId: string = '';
  @Input() reactions: Array<ILIKE> = [];
  @Input() commentsCount: number = 0;
  @Input() user: IUser;
  @Input() postAuthor: string = '';
  @Input() getGender: Function;
  @Output() onHandlerToggleComment = new EventEmitter<boolean>();

  public togglecomment: boolean = false;
  public loadingReaction: boolean = true;
  public likeReact: ILIKE = { _id: '', likeReact: LKIE_REACT.NONE };
  public feels: Array<ILIKE> = [];
  public safeguarded: Array<ILIKE> = [];
  public winning: Array<ILIKE> = [];
  public agreed: Array<ILIKE> = [];
  public hoverReact: boolean = false;
  public reactionType = LKIE_REACT;
  public reactionIcon: any;
  public reactInclude = [];
  public reactionProp = [
    {
      src: 'assets/icons/reaction/Feels.svg',
      label: 'SOCIAL_FEED.LABELS.FEELS',
      alt: 'Feels',
    },
    {
      src: 'assets/icons/reaction/Safeguarded.svg',
      label: 'SOCIAL_FEED.LABELS.SAFEGUARDED',
      alt: 'Safeguarded',
    },
    {
      src: 'assets/icons/reaction/Winning.svg',
      label: 'SOCIAL_FEED.LABELS.WINNING',
      alt: 'Winning',
    },
    {
      src: 'assets/icons/reaction/Agreed.svg',
      label: 'SOCIAL_FEED.LABELS.AGREED',
      alt: 'Agreed',
    },
    {
      src: 'assets/icons/reaction/smile.svg',
      label: 'SOCIAL_FEED.LABELS.REACT',
      alt: 'Smile',
    },
  ];

  constructor(
    private _dialog: MatDialog,
    private _socialService: EsportsSocialService,
    private _translateService: TranslateService,
    private _chatService: EsportsChatService,
    private _toastService: EsportsToastService
  ) {}

  ngOnInit(): void {
    this.initReactions();
  }

  public onToggleComment() {
    this.togglecomment = !this.togglecomment;
    this.onHandlerToggleComment.emit(this.togglecomment);
  }

  public openDialogReaction() {
    const data = {
      reactions: this.reactions,
      feels: this.feels,
      safeguarded: this.safeguarded,
      winning: this.winning,
      agreed: this.agreed,
    };
    this._dialog.open(ReactionInfoComponent, { data });
  }

  public onReactionPost(react: LKIE_REACT) {
    if (this.likeReact.likeReact == this.reactionType.NONE) {
      this.addReact(react);
    } else if (this.likeReact.likeReact == react) {
      this.removeReactById();
    } else {
      this.editReact(react);
    }
  }

  public onHoverReaction(status: boolean) {
    this.hoverReact = status;
  }

  private initReactions() {
    for (let like of this.reactions) {
      this.pushReactionInit(like);
      if (like?.createdBy?._id == this.user?._id) {
        this.likeReact.likeReact = like.likeReact;
        this.likeReact._id = like._id;
      }
    }
    this.sortReactInclude();
  }

  private pushReactionInit(reaction: ILIKE) {
    switch (reaction.likeReact) {
      case LKIE_REACT.FEELS:
        this.feels.push(reaction);
        this.addReactInclude(LKIE_REACT.FEELS);
        break;
      case LKIE_REACT.SAFEGUARDED:
        this.safeguarded.push(reaction);
        this.addReactInclude(LKIE_REACT.SAFEGUARDED);
        break;
      case LKIE_REACT.WINNING:
        this.winning.push(reaction);
        this.addReactInclude(LKIE_REACT.WINNING);
        break;
      case LKIE_REACT.AGREED:
        this.agreed.push(reaction);
        this.addReactInclude(LKIE_REACT.AGREED);
        break;
    }
  }

  private addReact(react: LKIE_REACT) {
    this._socialService
      .addLikePost({ likeReact: react }, this.postId)
      .subscribe(
        (res) => {
          const data = res?.data;
          if (!data) throw Error();
          const like: ILIKE = { ...data, createdBy: setCreatedBy(this.user) };
          this.likeReact = {
            likeReact: react,
            _id: data?._id,
            createdBy: like.createdBy,
          };
          this.addReactInclude(react);
          this.reactions.unshift(like);
          this.handleAddReactions(like);
          if (this.postAuthor != this.user._id) {
            const payload: IO_LIKE = {
              postAuthor: this.postAuthor,
              likeId: like?._id,
              message: this._translateService.instant(
                'SOCIAL_FEED.MESSAGES.LIKE_POST'
              ),
            };
            this._chatService.emitLikePost(payload);
          }
        },
        (err) => {
          this._toastService.showError(
            this._translateService.instant('SOCIAL_FEED.MESSAGES.TRY_AGAIN')
          );
          throw err;
        }
      );
  }

  private editReact(react: LKIE_REACT) {
    this._socialService
      .updateLikePost({ likeReact: react }, this.likeReact?._id)
      .subscribe(
        (res) => {
          const data: ILIKE = res?.data;
          if (!data) throw Error();
          this.handleRemoveReactions(this.likeReact);
          this.addReactInclude(react);
          const like = { ...data, createdBy: setCreatedBy(this.user) };
          this.likeReact = {
            likeReact: react,
            _id: data?._id,
            createdBy: like.createdBy,
          };
          this.handleAddReactions(this.likeReact);
          const likeIndexOfReaction = this.reactions.findIndex(
            (reaction) => reaction._id == like._id
          );
          this.reactions[likeIndexOfReaction] = like;
        },
        (err) => {
          this._toastService.showError(
            this._translateService.instant('SOCIAL_FEED.MESSAGES.TRY_AGAIN')
          );
          throw err;
        }
      );
  }

  private removeReactById() {
    this._socialService.removeLikePost(this.likeReact?._id).subscribe(
      (res) => {
        const data: ILIKE = res?.data;
        if (!data) throw Error();
        this.reactions = this.filterReaction(
          this.reactions,
          this.likeReact?._id
        );
        this.handleRemoveReactions(this.likeReact);
        this.likeReact = { _id: '', likeReact: LKIE_REACT.NONE };
      },
      (err) => {
        this._toastService.showError(
          this._translateService.instant('SOCIAL_FEED.MESSAGES.TRY_AGAIN')
        );
        throw err;
      }
    );
  }

  private handleAddReactions(reaction: ILIKE) {
    switch (reaction.likeReact) {
      case LKIE_REACT.FEELS:
        this.feels.unshift(reaction);
        this.addReactInclude(LKIE_REACT.FEELS);
        break;
      case LKIE_REACT.SAFEGUARDED:
        this.safeguarded.unshift(reaction);
        this.addReactInclude(LKIE_REACT.SAFEGUARDED);
        break;
      case LKIE_REACT.WINNING:
        this.winning.unshift(reaction);
        this.addReactInclude(LKIE_REACT.WINNING);
        break;
      case LKIE_REACT.AGREED:
        this.agreed.unshift(reaction);
        this.addReactInclude(LKIE_REACT.AGREED);
        break;
    }
  }

  private handleRemoveReactions(reaction: ILIKE) {
    switch (reaction.likeReact) {
      case LKIE_REACT.FEELS:
        this.feels = this.filterReaction(this.feels, reaction?._id);
        this.removeReactInclude(this.feels, LKIE_REACT.FEELS);
        break;
      case LKIE_REACT.SAFEGUARDED:
        this.safeguarded = this.filterReaction(this.safeguarded, reaction?._id);
        this.removeReactInclude(this.safeguarded, LKIE_REACT.SAFEGUARDED);
        break;
      case LKIE_REACT.WINNING:
        this.winning = this.filterReaction(this.winning, reaction?._id);
        this.removeReactInclude(this.winning, LKIE_REACT.WINNING);
        break;
      case LKIE_REACT.AGREED:
        this.agreed = this.filterReaction(this.agreed, reaction?._id);
        this.removeReactInclude(this.agreed, LKIE_REACT.AGREED);
        break;
    }
  }

  private addReactInclude(react: LKIE_REACT) {
    if (!this.reactInclude.includes(react)) {
      this.reactInclude.push(react);
      this.sortReactInclude();
    }
  }

  private removeReactInclude(reactions: Array<ILIKE>, react: LKIE_REACT) {
    if (reactions.length == 0) {
      this.reactInclude = this.reactInclude.filter(
        (reactInclude) => reactInclude != react
      );
    }
  }

  private sortReactInclude() {
    this.reactInclude.sort((pre, aft) => pre - aft);
  }

  private filterReaction(
    reactions: Array<ILIKE>,
    reactId: string
  ): Array<ILIKE> {
    reactions = reactions.filter((reaction) => reaction?._id !== reactId);
    return reactions;
  }
}
