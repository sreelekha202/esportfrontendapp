import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddCommnentComponent } from './add-commnent.component';

describe('AddCommnentComponent', () => {
  let component: AddCommnentComponent;
  let fixture: ComponentFixture<AddCommnentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddCommnentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddCommnentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
