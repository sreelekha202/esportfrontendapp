import {
  onLoadImgError,
  setCreatedBy,
  TYPE_IMG_LOAD,
} from './../../../../comon';
import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import {
  EsportsSocialService,
  EsportsToastService,
  ICOMMENT,
  EsportsChatService,
  IUser,
  IO_REPLY,
} from 'esports';
import { TranslateService } from '@ngx-translate/core';
const LIMIT_REPLIES = 5;
@Component({
  selector: 'app-comment-post',
  templateUrl: './comment-post.component.html',
  styleUrls: ['./comment-post.component.scss'],
})
export class CommentPostComponent implements OnInit {
  @ViewChild('txtEditComment') txtEditComment: ElementRef<HTMLElement>;
  @ViewChild('replyText') replyText: ElementRef;
  @Input() comment: ICOMMENT;
  @Input() currentUser: IUser;
  @Input() postAuthor: string = '';
  @Input() getGender: Function;
  @Input() updateComment: Function;
  @Input() deleteComment: Function;

  @Output() onHandleRemoveComment = new EventEmitter<string>();
  @Output() onHandleRemoveReply = new EventEmitter<string>();

  public isEditComment: boolean = false;
  public isEditImage: boolean = false;
  public showReply: boolean = false;
  public onHandlerDeleteReply: Function;
  public onHandlerUpdateReply: Function;
  public replies: Array<ICOMMENT> = [];
  public repliesAmount: number = 0;
  public priviewFileUploads: any;
  public onLoadImgError: Function = onLoadImgError;
  public typeImgLoad = TYPE_IMG_LOAD;
  public pageComment: number = 0;
  public pageTotal: number = 0;
  public showMoreReplyLoader: boolean = false;
  public isLoaderReply: boolean = false;

  constructor(
    private _socialService: EsportsSocialService,
    public _translateService: TranslateService,
    private _toastService: EsportsToastService,
    private _chatService: EsportsChatService
  ) {}

  ngOnInit(): void {
    if (!this?.comment?.commentParent) {
      this.onHandlerDeleteReply = this.deleteReply.bind(this);
      this.onHandlerUpdateReply = this.updateReply.bind(this);
      this.repliesAmount = this.comment?.commentChilds?.length;
      this.comment.commentChilds = [];
    }
  }

  public onPopUpEvent(eventType) {
    switch (eventType) {
      case 'EDIT_COMMENT':
        this.priviewFileUploads = {
          url: this.comment.imagesLink,
          data: this.comment.imagesLink,
          type: 'image',
        };
        this.isEditComment = true;
        break;
      case 'DELETE_COMMENT':
        this.deleteComment(this.comment._id);
        break;
    }
  }

  public onKeyDownEvent(event) {
    const message = event.target?.value;
    this.comment = { ...this.comment, message };
    const keyCode = event.which || event.keyCode;
    if (keyCode === 13 && !event.shiftKey) {
      if (event.type == 'keydown') {
        this.updateComment(this.comment);
      }
      this.isEditComment = false;
      event.preventDefault();
    }
  }

  public onSelectedFile(file) {
    const fileName = file.name;
    const fileType = file.type.split('/')[0];
    if (fileType != 'image') {
      this._toastService.showError(
        this._translateService.instant('SOCIAL_FEED.MESSAGES.INVALID_FILE')
      );
      return;
    }
    this.isEditImage = true;
    this._socialService.signedFile(fileName, fileType).subscribe((res: any) => {
      const url = res?.data?.imageURL;
      const signedRequest = res?.data?.uploadURL;
      this.putFile(signedRequest, file, url);
      const fileReader = new FileReader();
      fileReader.readAsDataURL(file);
      fileReader.onload = () => {
        const data = fileReader.result;
        this.priviewFileUploads = { url, data, type: fileType };
      };
      this.isEditImage = false;
    });
  }

  public onToggleReply() {
    this.showReply = !this.showReply;
    if (
      this.showReply &&
      !this.comment?.commentParent &&
      this.comment?.commentChilds.length == 0
    ) {
      this.isLoaderReply = true;
      this.getReplies(0, LIMIT_REPLIES);
    }
  }

  public showMoreReply(page: number) {
    this.showMoreReplyLoader = true;
    this.pageComment = page + 1;
    this.getReplies(0, (this.pageComment + 1) * LIMIT_REPLIES);
  }

  public removeImage() {
    this.comment.imagesLink = '';
  }

  public onGetReply(value) {
    const { commentParent, postId } = value;
    delete value.commentParent;
    delete value.postId;
    this.isLoaderReply = true;
    this.addReply(postId, commentParent, value);
  }

  private getReplies(skip: number, limit: number) {
    if (this.comment.commentParent) {
      return;
    }
    this._socialService
      .getReplyByComment(this.comment._id, skip, limit)
      .subscribe(
        (res) => {
          const replies: Array<ICOMMENT> = res?.data?.comments;
          if (!replies) throw Error();
          this.comment.commentChilds = replies;
          this.isLoaderReply = false;
          this.pageTotal = Math.floor(res?.data?.total / LIMIT_REPLIES);
          this.showMoreReplyLoader = false;
        },
        (err) => {
          this._toastService.showError(
            this._translateService.instant('SOCIAL_FEED.MESSAGES.TRY_AGAIN')
          );
          throw err;
        }
      );
  }

  private addReply(postId: string, commentParent: string, payload: ICOMMENT) {
    this._socialService.addReply(postId, commentParent, payload).subscribe(
      (res) => {
        const resData: ICOMMENT = res?.data;
        if (!resData) throw Error();
        this.isLoaderReply = false;
        const reply = { ...resData, createdBy: setCreatedBy(this.currentUser) };
        this.comment.commentChilds.unshift(reply);
        const payload: IO_REPLY = { commentId: commentParent };
        if (this.comment.createdBy._id != this.currentUser._id) {
          payload.commentAuthor = this.comment.createdBy._id;
          payload.message.toCommentAuthor = this._translateService.instant(
            'SOCIAL_FEED.MESSAGES.REPLY_COMMENT'
          );
        }
        if (this.postAuthor != this.currentUser._id) {
          payload.postAuthor = this.postAuthor;
          payload.message.toPostAuthor = this._translateService.instant(
            'SOCIAL_FEED.MESSAGES.REPLY_POST'
          );
        }
        if (payload?.commentAuthor || payload?.postAuthor)
          this._chatService.emitReplyComment(payload);
      },
      (err) => {
        this._toastService.showError(
          this._translateService.instant('SOCIAL_FEED.MESSAGES.TRY_AGAIN')
        );
        throw err;
      }
    );
  }

  private deleteReply(commentId: string) {
    const confirmMessage = this._translateService.instant(
      'SOCIAL_FEED.MESSAGES.CONFIRM_DELETE_REPLY'
    );
    if (confirm(confirmMessage)) {
      this._socialService.deleteComment(commentId).subscribe(
        (res) => {
          if (!res?.data) throw Error;
          this.comment.commentChilds = this.comment.commentChilds.filter(
            (comment) => comment._id != commentId
          );
          this.repliesAmount--;
          this._toastService.showSuccess(
            this._translateService.instant(
              'SOCIAL_FEED.MESSAGES.DELETE_COMMENT_SUCCESS'
            )
          );
        },
        (err) => {
          this._toastService.showError(
            this._translateService.instant('SOCIAL_FEED.MESSAGES.TRY_AGAIN')
          );
          throw err;
        }
      );
    }
  }

  private updateReply(payload: ICOMMENT) {
    const comment: ICOMMENT = {
      message: payload?.message,
      imagesLink: payload?.imagesLink,
    };
    this._socialService.updateComment(payload?._id, comment).subscribe(
      (res) => {
        if (!res?.data) throw Error;
        this.editComment(payload._id, payload?.message, payload?.imagesLink);
      },
      (err) => {
        this._toastService.showError(
          this._translateService.instant('SOCIAL_FEED.MESSAGES.TRY_AGAIN')
        );
        throw err;
      }
    );
  }

  private editComment(commentId: string, message: string, imagesLink: string) {
    const index = this.comment.commentChilds.findIndex(
      (comment) => comment._id == commentId
    );
    this.comment.commentChilds[index] = {
      ...this.comment.commentChilds[index],
      message,
      imagesLink,
    };
  }

  private putFile(signedURL: string, file: any, url: string) {
    const xhr = new XMLHttpRequest();
    xhr.open('PUT', signedURL);
    xhr.onreadystatechange = () => {
      if (xhr?.readyState === 4) {
        if (xhr?.status === 200) {
          this.comment.imagesLink = url;
        } else {
          this._toastService.showError(
            this._translateService.instant('SOCIAL_FEED.MESSAGES.TRY_AGAIN')
          );
        }
      }
    };
    xhr.send(file);
  }
}
