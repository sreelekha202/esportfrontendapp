import {
  Component,
  Input,
  OnInit,
  ViewChild,
  Output,
  EventEmitter,
} from '@angular/core';
import {
  EsportsToastService,
  ICOMMENT,
  EsportsSocialService,
  IUser,
} from 'esports';
import { onLoadImgError, setCreatedBy, TYPE_IMG_LOAD } from '../../../../comon';
import { TranslateService } from '@ngx-translate/core';
@Component({
  selector: 'app-add-commnent',
  templateUrl: './add-commnent.component.html',
  styleUrls: ['./add-commnent.component.scss'],
})
export class AddCommnentComponent implements OnInit {
  @ViewChild('commentText') commentText: any;
  @Input() currentUser: IUser;
  @Input() postId: string = '';
  @Input() commentParent: string = '';
  @Output() onHandlerComment = new EventEmitter<ICOMMENT>();

  public comment: ICOMMENT = { imagesLink: '' };
  public typeImgLoad = TYPE_IMG_LOAD;
  public onLoadImgError: Function = onLoadImgError;
  public priviewFileUploads: any;
  public showLoaderImage: boolean = false;
  public showEmojiPicker: boolean = false;
  public message: string = '';

  constructor(
    private _socialService: EsportsSocialService,
    private _toastService: EsportsToastService,
    public _translateService: TranslateService
  ) {}

  ngOnInit(): void {}

  public onKeyDownEvent(event) {
    event.target.style.height = '0px';
    event.target.style.height = event.target.scrollHeight + 0 + 'px';
    this.comment = {
      ...this.comment,
      message: this.message,
      imagesLink: this.priviewFileUploads?.url,
      commentParent: this.commentParent,
      postId: this.postId,
    };
    const keyCode = event.which || event.keyCode;
    if (keyCode === 13 && !event.shiftKey) {
      if (event.type == 'keydown') {
        this.addComment(event);
        event.target.value = '';
        this.removeImageComment();
      }
      event.preventDefault();
    }
  }

  public toggleEmojiPicker() {
    this.showEmojiPicker = !this.showEmojiPicker;
  }

  public addEmoji(event) {
    this.message = `${this.message}${event.emoji.native}`;
  }

  private addComment(event:any) {
    const { message, imagesLink, commentParent, postId } = this.comment;
    if (message?.trim().length == 0 && imagesLink?.trim().length == 0) {
      this._toastService.showError(
        this._translateService.instant('SOCIAL_FEED.MESSAGES.EMPTY_COMMENT')
      );
    }
    this.commentText.nativeElement.value = '';
    this.onHandlerComment.emit({ message, imagesLink, commentParent, postId });
    this.message = '';
    this.showEmojiPicker = false;
    event.target.style.height =  40 + 'px';
  }

  public removeImageComment() {
    this.priviewFileUploads = '';
    this.comment.imagesLink = '';
  }

  public onSeletedFile(file) {
    const fileName = file.name;
    const fileType = file.type.split('/')[0];
    if (fileType != 'image') {
      this._toastService.showError(
        this._translateService.instant('SOCIAL_MESSAGE.INVALID_FILE')
      );
      return;
    }
    this.showLoaderImage = true;
    this._socialService.signedFile(fileName, fileType).subscribe((res) => {
      const url = res?.data?.imageURL;
      const signedRequest = res?.data?.uploadURL;
      this.putFile(signedRequest, file, url);
      const fileReader = new FileReader();
      fileReader.readAsDataURL(file);
      fileReader.onload = () => {
        const data = fileReader.result;
        this.priviewFileUploads = { url, data, type: fileType };
        this.showLoaderImage = false;
      };
    });
  }

  private putFile(signedURL: string, file: any, url: string) {
    const xhr = new XMLHttpRequest();
    xhr.open('PUT', signedURL);
    xhr.onreadystatechange = () => {
      if (xhr?.readyState === 4) {
        if (xhr?.status === 200) {
          this.comment.imagesLink = url;
        } else {
          this._toastService.showError(
            this._translateService.instant('SOCIAL_FEED.MESSAGES.TRY_AGAIN')
          );
        }
      }
    };
    xhr.send(file);
  }
}
