import { Component, OnInit, Inject, Optional } from '@angular/core';
import { Observable, Observer } from 'rxjs';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { GENDER, ILIKE, LKIE_REACT } from 'esports';
import { AppHtmlProfileRoutes } from '../../../../../../app-routing.model';

export interface ExampleTab {
  label: string;
  content: string;
}

@Component({
  selector: 'app-reaction-info',
  templateUrl: './reaction-info.component.html',
  styleUrls: ['./reaction-info.component.scss'],
})
export class ReactionInfoComponent implements OnInit {
  public AppHtmlProfileRoutes = AppHtmlProfileRoutes;
  public asyncTabs: Observable<ExampleTab[]>;
  public reactions: Array<ILIKE> = [];
  public feels: Array<ILIKE> = [];
  public safeguarded: Array<ILIKE> = [];
  public winning: Array<ILIKE> = [];
  public agreed: Array<ILIKE> = [];
  public reactionsIcon: any[] = [
    { src: 'assets/images/Icon/Like & heart.png', alt: 'Like & Heart' },
    { src: 'assets/images/Icon/Protected & safe.png', alt: 'Protected & Safe' },
    { src: 'assets/images/Icon/Win.png', alt: 'Winning' },
    { src: 'assets/images/Icon/100.png', alt: 'Agreed' },
  ];

  constructor(
    public _dialogRef: MatDialogRef<ReactionInfoComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.asyncTabs = new Observable((observer: Observer<ExampleTab[]>) => {
      setTimeout(() => {
        observer.next([
          { label: 'First', content: 'Content 1' },
          { label: 'Second', content: 'Content 2' },
          { label: 'Third', content: 'Content 3' },
        ]);
      }, 1000);
    });
  }

  ngOnInit(): void {
    this.initReact();
  }

  private initReact() {
    const data = this._dialogRef.componentInstance?.data;
    this.reactions = data?.reactions || [];
    this.feels = data?.feels || [];
    this.safeguarded = data?.safeguarded || [];
    this.winning = data?.winning || [];
    this.agreed = data?.agreed || [];
  }
}
