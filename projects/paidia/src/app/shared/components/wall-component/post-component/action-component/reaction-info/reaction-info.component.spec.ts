import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReactionInfoComponent } from './reaction-info.component';

describe('ReactionInfoComponent', () => {
  let component: ReactionInfoComponent;
  let fixture: ComponentFixture<ReactionInfoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReactionInfoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReactionInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
