import { Component, Input, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { GENDER, ILIKE } from 'esports';
import { AppHtmlProfileRoutes } from '../../../../../../../app-routing.model';

@Component({
  selector: 'app-reaction-container',
  templateUrl: './reaction-container.component.html',
  styleUrls: ['./reaction-container.component.scss'],
})
export class ReactionContainerComponent implements OnInit {
  @Input() reactions: Array<ILIKE> = [];
  @Input() reactionIcon: any[] = [];

  public AppHtmlProfileRoutes = AppHtmlProfileRoutes;

  constructor(private _translateService: TranslateService) {}

  ngOnInit(): void {}

  public navPublicProfile(userId: string): string {
    return `${AppHtmlProfileRoutes.publicWall}/${userId}`;
  }

  public getGender(gender: GENDER, customPronoun) {
    let pronoun: string;
    switch (gender) {
      case GENDER.HE:
        pronoun = GENDER.HE.toUpperCase();
        break;
      case GENDER.SHE:
        pronoun = GENDER.SHE.toUpperCase();
        break;
      case GENDER.THEY:
        pronoun = GENDER.THEY.toUpperCase();
        break;
      default:
        return customPronoun;
    }
    return this._translateService.instant(`SOCIAL_FEED.USER.GENDER.${pronoun}`);
  }
}
