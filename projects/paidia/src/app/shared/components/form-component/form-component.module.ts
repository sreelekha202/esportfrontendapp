import { PhoneNoComponent } from './phone-no/phone-no.component';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';

import { CollapseModule } from 'ngx-bootstrap/collapse';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatIconModule } from '@angular/material/icon';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { I18nModule, PipeModule } from 'esports';
import { LoadingModule } from '../../../core/loading/loading.module';
import { CounterComponent } from './counter/counter.component';
import { DropDownComponent } from './drop-down/drop-down.component';
import { DurationCounterComponent } from './duration-counter/duration-counter.component';
import { InputComponent } from './input/input.component';
import { MultipleListSelectComponent } from './multiple-list-select/multiple-list-select.component';
import { MultipleSelectComponent } from './multiple-select/multiple-select.component';
import { SelectToggleComponent } from './select-toggle/select-toggle.component';
import { ToggleSwitchComponent } from './toggle-switch/toggle-switch.component';
import { UploadComponent } from './upload/upload.component';
import { NgxIntlTelInputModule } from 'ngx-intl-tel-input';
import { environment } from '../../../../environments/environment';
import { InputDateTwoComponent } from './input-date-two/input-date-two.component';
import { InputTextNormalComponent } from './input-text-normal/input-text-normal.component';
import { InputNumberComponent } from './input-number/input-number.component';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import {
  NgxMatDatetimePickerModule,
  NgxMatNativeDateModule,
  NgxMatTimepickerModule,
} from '@angular-material-components/datetime-picker';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import  { TournamentImageComponent } from './tournament-image/tournament-image.component';
import { EsportsLoaderModule } from 'esports';

const components = [
  CounterComponent,
  DropDownComponent,
  DurationCounterComponent,
  InputComponent,
  MultipleListSelectComponent,
  MultipleSelectComponent,
  SelectToggleComponent,
  ToggleSwitchComponent,
  UploadComponent,
  PhoneNoComponent,
  InputDateTwoComponent,
  InputTextNormalComponent,
  InputNumberComponent,
  TournamentImageComponent,
];

const modules = [
  CollapseModule,
  CommonModule,
  FontAwesomeModule,
  FormsModule,
  I18nModule.forRoot(environment),
  LoadingModule,
  NgxIntlTelInputModule,
  MatCheckboxModule,
  MatExpansionModule,
  MatIconModule,
  MatSlideToggleModule,
  NgbModule,
  PipeModule,
  ReactiveFormsModule,
  NgxMaterialTimepickerModule,
  NgxMatDatetimePickerModule,
  NgxMatNativeDateModule,
  NgxMatTimepickerModule,
  MatDatepickerModule,
  MatFormFieldModule,
  MatInputModule,
  EsportsLoaderModule.setColor('#1d252d'),
];

@NgModule({
  declarations: components,
  imports: modules,
  exports: components,
})
export class FormComponentModule {}
