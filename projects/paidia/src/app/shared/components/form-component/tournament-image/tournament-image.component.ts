import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { EsportsToastService, S3UploadService } from 'esports';

@Component({
  selector: 'app-shared-tournament-image',
  templateUrl: './tournament-image.component.html',
  styleUrls: ['./tournament-image.component.scss'],
})
export class TournamentImageComponent implements OnInit {
  @Input() customFormControlName: string;
  @Input() customFormGroup: FormGroup;
  @Input() title: string;
  @Input() subtitle: string;
  @Input() dimension;
  @Input() size;
  @Input() type;
  @Input() id;
  @Input() required: boolean = false;
  @Output() imgurltosave = new EventEmitter();
  @Input() data?;

  isProcessing: boolean = false;
  imgurl: any = '';

  constructor(
    private s3UploadService: S3UploadService,
    private toastService: EsportsToastService
  ) {}

  ngOnInit(): void {
    if(this.data?.imgurl){
      this.imgurl = this.data?.imgurl;
    }
  }

  eventChangee = async (event) => {
    try {
      this.isProcessing = true;
      const upload: any = await this.s3UploadService.validateAndUploadImage(
        event,
        this.dimension,
        this.size,
        this.type
      );
      this.imgurl = upload.data[0].Location;
      this.imgurltosave.emit(this.imgurl);
      this.customFormGroup
        .get(this.customFormControlName)
        .setValue(this.imgurl);
      this.toastService.showSuccess(upload?.message);
      this.isProcessing = false;
    } catch (error) {
      this.isProcessing = false;
      const errorMessage = error?.error?.message || error?.message;
      this.toastService.showError(errorMessage);
    }
  };
}
