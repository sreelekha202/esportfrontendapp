import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AbstractControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-toggle-switch',
  templateUrl: './toggle-switch.component.html',
  styleUrls: ['./toggle-switch.component.scss'],
})
export class ToggleSwitchComponent implements OnInit {
  @Input() title: string;
  @Input() customFormControlName: string;
  @Input() customFormGroup: FormGroup;
  @Input() readOnly: boolean;
  @Input() toolTipMessage: string;
  @Input() select: boolean;
  @Output() valueEmit = new EventEmitter<AbstractControl>();

  constructor() {}

  ngOnInit(): void {}

  emitChangedValue(val) {
    if(this.customFormGroup) {
      this.valueEmit.emit(this.customFormGroup?.controls[this?.customFormControlName]);
    } else {
      this.valueEmit.emit(val?.target);
    }
  }
}
