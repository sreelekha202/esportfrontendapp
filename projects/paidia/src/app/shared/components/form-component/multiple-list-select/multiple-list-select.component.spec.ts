import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MultipleListSelectComponent } from './multiple-list-select.component';

describe('MultipleListSelectComponent', () => {
  let component: MultipleListSelectComponent;
  let fixture: ComponentFixture<MultipleListSelectComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MultipleListSelectComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MultipleListSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
