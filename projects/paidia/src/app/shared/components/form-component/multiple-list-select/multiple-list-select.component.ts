import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-multiple-list-select',
  templateUrl: './multiple-list-select.component.html',
  styleUrls: ['./multiple-list-select.component.scss'],
})
export class MultipleListSelectComponent implements OnInit {
  @Input() label: string = 'Allowed regions';
  @Input() placehoder: string = 'Selected';
  @Input() menuButton: string = 'Add regions';
  @Input() fields = [
    {
      title: 'Canada',
      value: 'canada',
      subfields: [
        {
          title: 'Calgary',
          value: 'calgary',
        },
        {
          title: 'Toronto',
          value: 'toronto',
        },
        {
          title: 'Winnipeg',
          value: 'winnipeg',
        },
      ],
    },
    {
      title: 'USA',
      value: 'usa',
      subfields: [
        {
          title: 'California',
          value: 'california',
        },
        {
          title: 'Los Angeles',
          value: 'los-angeles',
        },
        {
          title: 'New York',
          value: 'new-yory',
        },
        {
          title: 'Colorado',
          value: 'colorado',
        },
        {
          title: 'California',
          value: 'california',
        },
        {
          title: 'Los Angeles',
          value: 'los-angeles',
        },
        {
          title: 'New York',
          value: 'new-yory',
        },
        {
          title: 'Colorado',
          value: 'colorado',
        },
      ],
    },
    {
      title: 'Mexico',
      value: 'mexico',
      subfields: [
        {
          title: 'Baja California',
          value: 'baja-california',
        },
        {
          title: 'Coahuila',
          value: 'coahuila',
        },
        {
          title: 'Durango',
          value: 'durango',
        },
      ],
    },
  ];

  isMenuOpened: boolean = false;

  constructor() {}

  ngOnInit(): void {}

  onToggleMenu() {
    this.isMenuOpened = !this.isMenuOpened;
  }
}
