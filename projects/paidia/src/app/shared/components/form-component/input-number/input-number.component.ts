import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {
  ControlContainer,
  FormGroup,
  FormGroupDirective,
} from '@angular/forms';

@Component({
  selector: 'app-input-number-shared',
  templateUrl: './input-number.component.html',
  styleUrls: ['./input-number.component.scss'],
  viewProviders: [
    { provide: ControlContainer, useExisting: FormGroupDirective },
  ],
})
export class InputNumberComponent implements OnInit {
  @Input() customFormGroup: FormGroup;
  @Input() customFormControlName: string;
  @Input() title: string = '';
  @Input() isDisabled: boolean = false;

  @Output() submit = new EventEmitter();
  @Output() valChange = new EventEmitter();
  @Output() valueEmit = new EventEmitter<number>();


  constructor() {}

  ngOnInit(): void {}

  // to handle Enter event
  onSubmit(event) {
    if (event?.keyCode == 13) {
      this.submit.next(true);
    }
  }

  onChange(val) {
    this.valChange.next(val);
  }

  increment() {
    const control = this.customFormGroup.get(this.customFormControlName);
    control.markAsTouched({ onlySelf: true });
    let val = this.customFormGroup.get(this.customFormControlName).value;
    val++;
    this.customFormGroup.controls[this.customFormControlName].setValue(val);
    this.counter(val);
  }

  decrement() {
    const control = this.customFormGroup.get(this.customFormControlName);
    control.markAsTouched({ onlySelf: true });
    let val = this.customFormGroup.get(this.customFormControlName).value;
    if (val > 0) {
      val--;
    }
    this.customFormGroup.controls[this.customFormControlName].setValue(val);
    this.counter(val);
  }

  counter(value) {
    this.valueEmit.emit(value || 0);
  }
}
