import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { LazyLoadImageModule } from 'ng-lazyload-image';
import { MomentModule } from 'ngx-moment';

import { MaterialModule } from '../../modules/material.module';
import { I18nModule } from 'esports';
import { NeedLogingModule } from '../need-login/need-login.module';
import { CommentModule } from '../comment/comment.module';

import { CommentsComponent } from './comments.component';
import { environment } from '../../../../environments/environment';

@NgModule({
  declarations: [CommentsComponent],
  imports: [
    CommonModule,
    FormsModule,
    I18nModule.forRoot(environment),
    LazyLoadImageModule,
    MaterialModule,
    MomentModule,
    CommentModule,
    NeedLogingModule,
    ReactiveFormsModule,
    RouterModule,
  ],
  exports: [CommentsComponent],
})
export class CommentsModule {}
