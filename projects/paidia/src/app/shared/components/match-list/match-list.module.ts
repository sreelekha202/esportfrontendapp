import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";

import { MatchListComponent } from "./match-list.component";
import { LoadingModule } from "../../../core/loading/loading.module";

import { faSortDown } from "@fortawesome/free-solid-svg-icons";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { FormsModule } from '@angular/forms';
import {
  FontAwesomeModule,
  FaIconLibrary,
} from "@fortawesome/angular-fontawesome";
import {
  I18nModule,
  EsportsLoaderModule,
  PipeModule,
  EsportsCustomPaginationModule,
} from 'esports';
import { EliminationModule } from "../elimination/elimination.module";
import { OpenTeamDetailsComponent } from "./open-team-details/open-team-details.component";
import { environment } from "../../../../environments/environment";
import { MatExpansionModule } from '@angular/material/expansion';
import { MaterialModule } from '../../modules/material.module';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { LadderMatchComponent } from './ladder-match/ladder-match.component';
@NgModule({
  declarations: [MatchListComponent, OpenTeamDetailsComponent,LadderMatchComponent],
  imports: [
    CommonModule,
    FontAwesomeModule,
    LoadingModule,
    NgbModule,
    I18nModule.forRoot(environment),
    EsportsLoaderModule.setColor('#501090'),
    EliminationModule,
    NgxDatatableModule,
    MatExpansionModule,
    MaterialModule,
    EsportsCustomPaginationModule,
    PipeModule,
    FormsModule
  ],
  exports: [MatchListComponent],
})
export class MatchListModule {
  constructor(private library: FaIconLibrary) {
    library.addIcons(faSortDown);
  }
}
