import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import {
  SWIPER_CONFIG,
  SwiperConfigInterface,
  SwiperModule,
} from 'ngx-swiper-wrapper';

import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { CarouselModule } from 'primeng/carousel';
import { ClipboardModule } from '@angular/cdk/clipboard';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { InlineSVGModule } from 'ng-inline-svg';
import { LazyLoadImageModule } from 'ng-lazyload-image';
// import { MatPaginatorModule } from '@angular/material/paginator';
import { MomentModule } from 'ngx-moment';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgOtpInputModule } from 'ng-otp-input';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { ShareButtonModule } from 'ngx-sharebuttons/button';
import { ShareButtonsModule } from 'ngx-sharebuttons/buttons';
import { ShareIconsModule } from 'ngx-sharebuttons/icons';
import { TranslateService } from '@ngx-translate/core';
import { MatPaginatorModule } from '@angular/material/paginator';
import { CardsStatisticModule } from '../components/statistics/cards-statistic/cards-statistic.module';
import { CommentsInfluenceModule } from '../../shared/components/comments-influence/comments-influence.module';
import { CommentsModule } from '../../shared/components/comments/comments.module';
import { CustomPaginationModule } from '../../core/custom-pagination/custom-pagination.module';
import { DirectivesModule } from '../../shared/directives/directive.module';
import { DiscussionModule } from '../../shared/components/discussion/discussion.module';
import { EarnPointsModule } from '../../shared/components/earn-points/earn-points.module';
import { EliminationModule } from '../components/elimination/elimination.module';
import { GameStatisticModule } from '../components/statistics/game-statistic/game-statistic.module';
import { I18nModule, PipeModule } from 'esports';
import { LoadingModule } from '../../core/loading/loading.module';
import { MatchListModule } from '../../shared/components/match-list/match-list.module';
import { MaterialModule } from './material.module';
import { MonthsStatisticModule } from '../components/statistics/months-statistic/months-statistic.module';
import { PrizesStatisticModule } from '../components/statistics/prizes-statistic/prizes-statistic.module';
import { RatingModule } from '../../shared/components/rating/rating.module';
import { SearchFilterModule } from '../../shared/components/search-filter/search-filter.module';
import { SearchPlayersModule } from '../components/search-players/search-players.module';
import { SettingsLightboxModule } from '../../modules/settings-lightbox/settings-lightbox.module';
import { AvatarUpdateComponent } from '../popups/avatar-update/avatar-update.component';
import { FeaturedContentComponent } from '../../modules/featured-content/featured-content.component';
import { FullscreenLoadingComponent } from '../../core/fullscreen-loading/fullscreen-loading.component';
import { InfoPopupComponent } from '../popups/info-popup/info-popup.component';
import { RecentVideosComponent } from '../../modules/recent-videos/recent-videos.component';
import { SocialShareComponent } from '../popups/social-share/social-share.component';
import { LadderPopupComponent } from '../popups/ladder-popup/ladder-popup.component';
import { TournamentItemComponent } from '../../core/tournament-item/tournament-item.component';
import {
  EsportsLanguageService,
  EsportsChatSidenavService,
  EsportsLoaderModule,
} from 'esports';
import { environment } from '../../../environments/environment';
import { VimeoViewerModule } from '../directives/vimeo-viewer.module';
import { PreviewLinkModule } from '../components/preview-link/preview-link.module';
import { MessagePostModule } from '../components/message-post/message-post.module';
import { MainRightModule } from '../components/main-right/main-right.module';
import { WallComponentModule } from '../components/wall-component/wall-component.module';

const DEFAULT_SWIPER_CONFIG: SwiperConfigInterface = {
  direction: 'horizontal',
  slidesPerView: 'auto',
};

const components = [
  FeaturedContentComponent,
  FullscreenLoadingComponent,
  RecentVideosComponent,
  TournamentItemComponent,
];

const directives = [];

const popups = [
  AvatarUpdateComponent,
  InfoPopupComponent,
  SocialShareComponent,
  LadderPopupComponent,
];

const sheets = [];

const modules = [
  BsDropdownModule,
  CardsStatisticModule,
  CarouselModule,
  ClipboardModule,
  CollapseModule.forRoot(),
  CommentsInfluenceModule,
  CommentsModule,
  CommonModule,
  CustomPaginationModule,
  DirectivesModule,
  DiscussionModule,
  EarnPointsModule,
  EliminationModule,
  FontAwesomeModule,
  FormsModule,
  GameStatisticModule,
  I18nModule.forRoot(environment),
  InlineSVGModule.forRoot(),
  LazyLoadImageModule,
  LoadingModule,
  MatchListModule,
  MaterialModule,
  MatPaginatorModule,
  MomentModule,
  MonthsStatisticModule,
  NgbModule,
  NgOtpInputModule,
  NgxDatatableModule,
  PipeModule,
  PrizesStatisticModule,
  RatingModule,
  ReactiveFormsModule,
  RouterModule,
  SearchFilterModule,
  SearchPlayersModule,
  SettingsLightboxModule,
  ShareButtonModule,
  ShareButtonsModule,
  ShareIconsModule,
  SwiperModule,
  VimeoViewerModule,
  EsportsLoaderModule.setColor('#bbf55a'),
  PreviewLinkModule,
  MessagePostModule,
  MainRightModule,
  WallComponentModule,
];

const providers = [
  EsportsChatSidenavService,
  {
    provide: SWIPER_CONFIG,
    useValue: DEFAULT_SWIPER_CONFIG,
  },
];

@NgModule({
  imports: [...modules],
  declarations: [...components, ...directives, ...sheets, ...popups],
  exports: [...modules, ...components, ...directives, ...sheets, ...popups],
  providers: [...providers],
})
export class SharedModule {
  constructor(
    private languageService: EsportsLanguageService,
    private translateService: TranslateService
  ) {
    this.languageService.language.subscribe((lang) => {
      this.translateService.use(lang);
    })
  }
}
