export interface IAdvertisementManagement {
  advertisementType: string;
  bidAmount: string;
  budgetAmount: string;
  budgetLimitType: string;
  campaignName: string;
  campaignNumber: string;
  checkIconShow: boolean;
  gamingPreference: any[];
  gender: string;
  payPreference: string;
  suggestedBidAmount: string;
  targetDevice: any[];
}
