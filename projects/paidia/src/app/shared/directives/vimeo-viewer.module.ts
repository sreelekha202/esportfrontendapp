import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VimeoViewerDirective } from './vimeo-viewer.directive';



@NgModule({
  declarations: [
    VimeoViewerDirective
  ],
  imports: [
    CommonModule
  ],
  exports: [VimeoViewerDirective],
})
export class VimeoViewerModule {

 }

 