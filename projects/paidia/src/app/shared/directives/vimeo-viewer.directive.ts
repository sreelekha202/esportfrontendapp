import { Directive, OnInit,ViewChild,ElementRef , Input} from '@angular/core';
import Player from "@vimeo/player";

@Directive({
  selector: '[appVimeoViewer]'
})
export class VimeoViewerDirective {

  id:any 
  viewer:any
  @Input() url: string;
  @Input() width: string;
  @Input() height: string;
  @Input() autoplay: boolean;
  @Input() loop: boolean;
  @Input() controls: boolean;
  @Input() background: boolean;


  constructor(private elementRef: ElementRef) { 
    
    this.viewer = this.elementRef.nativeElement
    
  }

  @ViewChild('vimeoVideoContainer') vimeoVideoContainer;
  public player: Player;

  ngAfterViewInit() { 
    
    this.player = new Player(this.viewer, {
        url: this.url, // Generic Id
        width: this.width,
        height:this.height,
        autoplay:this.autoplay || false,
        loop:this.loop || false,
        controls: true,
        background: false
    });
  }

}
