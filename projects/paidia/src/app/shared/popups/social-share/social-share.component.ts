import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-social-share',
  templateUrl: './social-share.component.html',
  styleUrls: ['./social-share.component.scss'],
})
export class SocialShareComponent implements OnInit {
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<SocialShareComponent>
  ) {}

  sharedLink: string = 'https://www.paidia.gg/article/Is-Valorant-Fortnine';

  ngOnInit(): void {}

  onClose(): void {
    this.dialogRef.close();
  }
}
