import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MobileRestrictPopupComponent } from './mobile-restrict-popup.component';

describe('MobileRestrictPopupComponent', () => {
  let component: MobileRestrictPopupComponent;
  let fixture: ComponentFixture<MobileRestrictPopupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MobileRestrictPopupComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MobileRestrictPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
