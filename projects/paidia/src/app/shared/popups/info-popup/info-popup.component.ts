import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Component, Inject, OnInit } from '@angular/core';

export enum InfoPopupComponentType {
  confirm,
  info,
  rating,
  socialSharing,
}

export interface InfoPopupComponentData {
  btnText?: string;
  cancelBtnText?: string;
  rate?: number;
  text: string;
  title: string;
  type: InfoPopupComponentType;
  
}

@Component({
  selector: 'app-info-popup',
  templateUrl: './info-popup.component.html',
  styleUrls: ['./info-popup.component.scss'],
})
export class InfoPopupComponent implements OnInit {
  InfoPopupComponentType = InfoPopupComponentType;
  currenturl:any;
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: InfoPopupComponentData,
    public dialogRef: MatDialogRef<InfoPopupComponent>
  ) {}

  ngOnInit(): void {
    this.currenturl=window.location.href
  }

  onClose(): void {
    this.dialogRef.close();
  }

  onConfirm(): void {
    this.dialogRef.close(this.data?.rate || true);
  }

  message(value) {
    this.data.rate = value;
  }
  copyInputMessage(inputElement){
    inputElement.select();
    document.execCommand('copy');
    inputElement.setSelectionRange(0, 0);
  }

}
