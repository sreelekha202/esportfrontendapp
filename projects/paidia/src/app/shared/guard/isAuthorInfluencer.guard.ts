import { Injectable } from "@angular/core";
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
} from "@angular/router";


import { EsportsUserService, GlobalUtils } from 'esports';
import { Observable } from "rxjs";
import { environment } from '../../../environments/environment';
const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;
@Injectable({
  providedIn: "root",
})
export class AuthorAccessGuard implements CanActivate {
  constructor(private userService: EsportsUserService, private router: Router) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | boolean {
    let url: string = state.url;

    let isValid = false;

    if (
      GlobalUtils.isBrowser() &&
      localStorage.getItem(environment.currentToken)
    ) {
      return this.checkAdmin();
    } else {
      this.router.navigate(["/login"]);
      return false;
    }

    if (isValid) {
      return true;
    } else {
      this.router.navigate(["/profile/basic-info"]);
      return false;
    }
  }

  checkAdmin(): any {
    return new Promise((resolve, reject) => {
      this.userService.getProfile(API,TOKEN).subscribe(
        (response: any) => {
          if (
            response.data["isAuthor"] == 0 ||
            response.data["isInfluencer"] === 0 ||
            response.data.accountType == "admin"
          ) {
            resolve(true);
          } else {
            this.router.navigate(["/profile/basic-info"]);
            resolve(false);
          }
        },
        (fail: any) => {
          this.router.navigate(["/user/phone-login"]);
          resolve(false);
        }
      );
    });
  }
}
