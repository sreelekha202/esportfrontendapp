import { Injectable } from "@angular/core";
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
} from "@angular/router";
import { Observable } from "rxjs";
import "rxjs/Rx";
import { EsportsUserService } from 'esports';
import { EsportsToastService } from 'esports';
import { environment } from '../../../environments/environment';
const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;

import { GlobalUtils } from "esports";
import { UserAccessType } from "../../modules/admin/components/access-management/access-management.model";

@Injectable({
  providedIn: "root",
})
export class AdminControlGuard implements CanActivate {
  constructor(
    private router: Router,
    private toastService: EsportsToastService,
    private userService: EsportsUserService
  ) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    let url: string = state.url;
    if (GlobalUtils.isBrowser()) {
      return this.checkAdmin(url);
    } else {
      this.toastService.showInfo("Please login to access that page.");
      this.router.navigate(["/user/phone-login"]);
      return false;
    }
  }

  checkAdmin(url: string): any {
    return new Promise((resolve, reject) => {
      this.userService.getProfile(API, TOKEN).subscribe(
        (response: any) => {
          if (
            response.data &&
            response.data.accountType &&
            response.data.accountType === "admin"
          ) {
            let _moduleType;
            switch (url && url.split("?")[0]) {
              case "/admin": {
                response.data.accessLevel.push("admin");
                _moduleType = "admin";
                break;
              }
              case "/admin/site-configuration": {
                _moduleType = UserAccessType.sc;
                break;
              }
              case "/admin/access-management": {
                _moduleType = UserAccessType.acm;
                break;
              }
              case "/admin/esports-management": {
                _moduleType = UserAccessType.em;
                break;
              }
              case "/admin/content-management": {
                _moduleType = UserAccessType.cm;
                break;
              }
              case "/admin/user-notifications": {
                _moduleType = UserAccessType.un;
                break;
              }
              case "/admin/user-management": {
                _moduleType = UserAccessType.um;
                break;
              }
              case "/admin/team-management": {
                _moduleType = UserAccessType.tm;
                break;
              }
              case '/admin/season-management': {
                _moduleType = UserAccessType.sem;
                break;
              }
              default: {
                if (url.startsWith("/admin/access-management")) {
                  _moduleType = UserAccessType.acm;
                } else if (url.startsWith("/admin/user-management")) {
                  _moduleType = UserAccessType.um;
                } else if (url.startsWith("/admin/dashboard")) {
                  response.data.accessLevel.push("dashboard");
                  _moduleType = "dashboard";
                } else if (url.startsWith("/admin/esports-management")) {
                  _moduleType = UserAccessType.em;
                } else if (url.startsWith("/admin/team-management/edit")) {
                  _moduleType = UserAccessType.tm;
                } else if (url.startsWith('/admin/season-management/create')) {
                  _moduleType = UserAccessType.sem;
                }
                else {
                  _moduleType = null;
                }
                break;
              }
            }
            if (
              _moduleType &&
              response.data.accessLevel.includes(_moduleType)
            ) {
              resolve(true);
            } else {
              this.router.navigate(["/404"]);
              resolve(false);
            }
          } else {
            this.toastService.showInfo("Please login to access that page.");
            this.router.navigate(["/user/phone-login"]);
            resolve(false);
          }
        },
        (fail: any) => {
          this.toastService.showInfo("Please login to access that page.");
          this.router.navigate(["/user/phone-login"]);
          resolve(false);
        }
      );
    });
  }
}
