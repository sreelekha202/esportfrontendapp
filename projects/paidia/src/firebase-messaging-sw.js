importScripts("https://www.gstatic.com/firebasejs/7.16.1/firebase-app.js");
importScripts(
  "https://www.gstatic.com/firebasejs/7.16.1/firebase-messaging.js"
);

firebase.initializeApp({
  apiKey: "AIzaSyATZFDppCblUstkDrf7HZjMktacH2zkKPU",
  authDomain: "paida-test.firebaseapp.com",
  projectId: "paida-test",
  storageBucket: "paida-test.appspot.com",
  messagingSenderId: "78844726296",
  appId: "1:78844726296:web:607036be8f64ce7f79476d",
  measurementId: "G-YNC2LN04G9"
});

const messaging = firebase.messaging();

messaging.setBackgroundMessageHandler(function (payload) {
  const notificationTitle = payload.data.title;
  const notificationOptions = {
    body: payload.data.body,
  };
  return self.registration.showNotification(
    notificationTitle,
    notificationOptions
  );
});
