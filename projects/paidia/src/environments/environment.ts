// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json` .

export const environment = {
  production: false,
  buildConfig: 'dev',
  apiEndPoint:
    'https://096m9vdoi1.execute-api.ca-central-1.amazonaws.com/stage/',
  currentToken: 'DUID',
  facebookAPPID: '1417432915264232',
  googleAPPID:
    '346336302247-93msahs9cjrh82dfefgb824sske1g1uo.apps.googleusercontent.com',
  socketEndPoint: 'https://paidia-chat.dynasty-staging.com',
  cookieDomain: 'localhost',
  eCommerceAPIEndPoint:
    'https://tuub60cp6h.execute-api.ap-southeast-1.amazonaws.com/dev/',
  firebase: {
    apiKey: 'AIzaSyATZFDppCblUstkDrf7HZjMktacH2zkKPU',
    authDomain: 'paida-test.firebaseapp.com',
    projectId: 'paida-test',
    storageBucket: 'paida-test.appspot.com',
    messagingSenderId: '78844726296',
    appId: '1:78844726296:web:607036be8f64ce7f79476d',
    measurementId: 'G-YNC2LN04G9',
  },
  articleS3BucketName: 'article',
  tournamentS3BucketName: 'tournament',
  iOSBundelName: '',
  iOSAppstoreURL: '',
  paypal_client_id:
    'ATimH35UqH6DnmNd9zqb_nEOffr6gwIOYyyeLuhA7wxT5-inZf6ndLb5FvmnXzqYiO1IJRQjt1ts3-hx&currency=CAD',
  enableFirebase: false,

  defaultLangCode: 'en',
  rtl: ['en'],
  language: [{ code: 'en', key: 'english', value: 'English' }],
  pageSizeOptions: [5, 10, 15, 20],
  maxparticipant: {
    single: 4096,
    double: 2048,
    roundRobin: 128,
    battleRoyale: 4096,
  },
  gTagId: '',
  gTagUrl: 'https://www.googletagmanager.com/gtag/js?id=',
  gtmId: '',
  googleAdsId: '',
  twitterUniversalTagId: '',
  facebookPixelId: '',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
