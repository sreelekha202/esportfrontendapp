import { Component, Input, OnInit } from '@angular/core';
import { IMatch } from 'esports';

@Component({
  selector: 'match-final-result',
  templateUrl: './final-result.component.html',
  styleUrls: ['./final-result.component.scss'],
})
export class FinalResultComponent implements OnInit {
  @Input() match: IMatch;
  @Input() pType: string;

  constructor() {}

  ngOnInit(): void {}
}
