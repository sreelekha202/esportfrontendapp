import {
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { EsportsLanguageService,EsportsToastService,EsportsBracketService} from 'esports';


@Component({
  selector: 'app-awards',
  templateUrl: './awards.component.html',
  styleUrls: ['./awards.component.scss'],
})
export class AwardsComponent implements OnInit, OnChanges {
  @Input() tournamentId;
  @Input() type: string;

  goldPrize = 'assets/images/Tournament/1-prize.png';
  silverPrize = 'assets/images/Tournament/2-prize.png';
  bronzedPrize = 'assets/images/Tournament/3-prize.png';

  championList = [];

  constructor(
    private bracketService: EsportsBracketService,
    private languageService: EsportsLanguageService,
    private toastService: EsportsToastService,
    private translateService: TranslateService
  ) {}

  ngOnInit(): void {
    this.languageService.language.subscribe((lang) =>
      this.translateService.use(lang || 'en')
    );
  }

  fetchWinnerList = async () => {
    try {
      const { data } = await this.bracketService.fetchWinnerList(
        this.tournamentId
      );
      this.championList = data;
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  ngOnChanges(changes: SimpleChanges): void {
    if (
      changes.hasOwnProperty('tournamentId') &&
      changes.tournamentId.currentValue &&
      changes.hasOwnProperty('type') &&
      changes.type.currentValue
    ) {
      this.fetchWinnerList();
    }
  }
}
