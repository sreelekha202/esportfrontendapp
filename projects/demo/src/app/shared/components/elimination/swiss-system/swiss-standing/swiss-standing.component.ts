import { Component, Input, OnInit } from "@angular/core";
import { EsportsToastService, EsportsBracketService } from "esports";

@Component({
  selector: "swiss-standing",
  templateUrl: "./swiss-standing.component.html",
  styleUrls: ["./swiss-standing.component.scss"],
})
export class SwissStandingComponent implements OnInit {
  @Input() tournamentId;
  @Input() tournamentType;
  @Input() enableTiebreaker: boolean = false;

  isProccessing = false;
  standing = [];

  constructor(
    private bracketService: EsportsBracketService,
    private toastService: EsportsToastService
  ) {}

  ngOnInit(): void {}

  ngOnChanges() {
    if (this.tournamentId) this.fetchStanding();
  }

  fetchStanding = async () => {
    try {
      this.standing = [];
      this.isProccessing = true;
      const queryParam = `?tournamentId=${this.tournamentId}`;
      const response = await this.bracketService.fetchStanding(queryParam);
      this.standing = response.data;
      this.isProccessing = false;
    } catch (error) {
      this.isProccessing = false;
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };
}
