import {
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
} from "@angular/core";

@Component({
  selector: "app-team",
  templateUrl: "./team.component.html",
  styleUrls: ["./team.component.scss"],
})
export class TeamComponent implements OnInit, OnChanges {
  @Input() bye: boolean = false;
  @Input() country: string;
  @Input() countryList: Array<any> = [];
  @Input() disqualified: boolean;
  @Input() isShowCountryFlag: boolean = false;
  @Input() logo: string;
  @Input() placeholder: string;
  @Input() setScore: number;
  @Input() teamName: string;
  @Input() match: any;

  countryCode = null;

  constructor() {}

  ngOnInit(): void {

    if(this.setScore == null || this.setScore == undefined){
      this.setScore = 0;
    }
  }

  ngOnChanges(simpleChanges: SimpleChanges) {
    if (this.country && this.countryList) {
      this.setCountryCode(this.country, this.countryList);
    }
  }

  setCountryCode(country, countryList) {
    const selectedCountry = countryList.find((c) => c?.name == country);
    this.countryCode = selectedCountry
      ? selectedCountry?.sortname.toLowerCase()
      : null;
  }
}
