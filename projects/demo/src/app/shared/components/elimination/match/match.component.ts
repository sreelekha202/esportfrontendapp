import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";

@Component({
  selector: "app-match",
  templateUrl: "./match.component.html",
  styleUrls: ["./match.component.scss"],
})
export class MatchComponent implements OnInit {

  @Input() isAdmin: boolean;
  @Input() openActionTab: boolean;
  @Input() isShowCountryFlag: boolean = false;
  @Input() participantType: string;
  @Input() countryList: Array<any> = [];
  @Input() totalSet: number = 1;
  @Input() participantId;
  @Output() openScoreCard = new EventEmitter<any>();
  @Output() promoteTeam = new EventEmitter<boolean>();
  @Input() match: any;
  @Input() isSeeded: boolean;


  isActionEnable: boolean = false;

  constructor() {}

  ngOnInit(): void {}

  scoreCard() {
    this.openScoreCard.emit({
      isOpenScoreCard: true,
      match: this.match,
    });
  }

  /**
   * Allow Admin and related participant to view score screen
   * @param isActionEnable check
   */
  onHover(isActionEnable: boolean) {
    this.isActionEnable =
      this.isSeeded &&
      isActionEnable &&
      this.match.matchStatus !== "inactive" &&
      !this.match.bye &&
      (this.isAdmin ||
        (this.participantId &&
          [this.match?.teamA?._id, this.match?.teamB?._id].includes(
            this.participantId
          )));
  }
}
