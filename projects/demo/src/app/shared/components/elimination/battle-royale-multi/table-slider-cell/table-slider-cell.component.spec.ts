import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { TableSliderCellComponent } from "./table-slider-cell.component";

describe("TableSliderCellComponent", () => {
  let component: TableSliderCellComponent;
  let fixture: ComponentFixture<TableSliderCellComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TableSliderCellComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableSliderCellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
