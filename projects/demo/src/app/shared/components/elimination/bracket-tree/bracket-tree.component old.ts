import { Component, OnInit, SimpleChanges } from '@angular/core';
import { Input, Output, EventEmitter } from '@angular/core';
import { GlobalUtils, EsportsBracketService } from 'esports';

export interface NgttTournament {
  rounds: NgttRound[];
}
export interface NgttRound {
  type: 'Winnerbracket' | 'Loserbracket' | 'Final';
  matches: any[];
}
@Component({
  selector: 'app-bracket-tree',
  templateUrl: './bracket-tree.component.html',
  styleUrls: ['./bracket-tree.component.scss'],
})
export class BracketTreeComponent implements OnInit {
  @Input() from: any; // form ='create' means generate demo or Tournament Details get on LocalStorage
  @Input() structure: any; //pass structure to set bracket
  @Output() valueEmit = new EventEmitter<any>();
  singleEliminationTournament: NgttTournament = null;
  dataforbracket1: any = [];
  tournamentDetails: any;

  constructor(private bracketService: EsportsBracketService) {}

  ngOnInit(): void {
    if (GlobalUtils.isBrowser()) {
      if (this.from != 'create') {
        this.tournamentDetails = localStorage.getItem('tournamentDetails');
        this.tournamentDetails = JSON.parse(this.tournamentDetails);
        let datatosend = {
          bracketType: this.tournamentDetails.bracketType,
          maximumParticipants: this.tournamentDetails.maxParticipants,
          noOfSet: this.tournamentDetails.noOfSet,
        };
        this.bracketService
          .generateBracket1(datatosend)
          .subscribe((data: any) => {
            this.setStructure(data);
          });
      } else {
        this.dataforbracket1 = [];
        let total = 10;
        let round = [];
        let i = 0;
        let dataforbracket = [];
        //this.dataforbracket[0].type = 'Winnerbracket'
        let j = 1;
        let k = 1;
        for (let data = 0; data < 16; data++) {
          let number = j;
          let players: any = [];
          let dt = {
            name: 'player' + k,
            score: 0,
            number: j,
            previousMatch: '',
          };
          players.push(dt);
          k = k + 1;
          let dt1 = {
            name: 'player' + k,
            score: 0,
            number: j,
            previousMatch: '',
          };
          k = k + 1;
          players.push(dt1);
          let data3 = {
            number: j,
            players: players,
          };
          dataforbracket.push(data3);
          j = j + 1;
        }
        let data4 = {
          type: 'Winnerbracket',
          matches: dataforbracket,
        };
        this.dataforbracket1.push(data4);
        let s = 1;
        let a = 2;
        // for(let m = 0 ; m < 2 ; m++){
        dataforbracket = [];
        for (let m = 0; m < 8; m++) {
          let number = j;
          let players: any = [];
          let dt = {
            name: 'player',
            score: 0,
            number: 0,
            previousMatch: s,
          };
          s = s + 1;
          let dt1 = {
            name: 'player',
            score: 0,
            number: 0,
            previousMatch: s,
          };
          s = s + 1;
          players.push(dt);
          players.push(dt1);
          let data3 = {
            number: j,
            players: players,
          };
          dataforbracket.push(data3);
          j = j + 1;
        }
        let data5 = {
          type: 'Winnerbracket',
          matches: dataforbracket,
        };

        this.dataforbracket1.push(data5);

        dataforbracket = [];
        for (let m = 0; m < 4; m++) {
          let number = j;
          let players: any = [];
          let dt = {
            name: 'player',
            score: 0,
            number: 0,
            previousMatch: s,
          };
          s = s + 1;
          let dt1 = {
            name: 'player',
            score: 0,
            number: 0,
            previousMatch: s,
          };
          s = s + 1;
          players.push(dt);
          players.push(dt1);
          let data3 = {
            number: j,
            players: players,
          };
          dataforbracket.push(data3);
          j = j + 1;
        }
        let data6 = {
          type: 'Winnerbracket',
          matches: dataforbracket,
        };

        this.dataforbracket1.push(data6);
        //}
        dataforbracket = [];
        // for(let data of round[total.toString()]){
        let number = j;
        let players: any = [];
        let dt = {
          name: 'player',
          score: 0,
          number: j,
          previousMatch: s,
        };
        s = s + 1;
        let dt1 = {
          name: 'player',
          score: 0,
          number: j,
          previousMatch: s,
        };
        s = s + 1;
        players.push(dt);
        players.push(dt1);
        let data3 = {
          number: j,
          players: players,
        };
        dataforbracket.push(data3);
        j = j + 1;
        //  }
        let data7 = {
          type: 'Final',
          matches: dataforbracket,
        };

        this.dataforbracket1.push(data7);
        this.singleEliminationTournament = {
          rounds: this.dataforbracket1,
        };
      }
      this.valueEmit.emit(this.singleEliminationTournament);
      // if (this.structure && this.structure.round) {
      //   this.setStructure(this.structure)
      // }

      // this.singleEliminationTournament = {
      //   rounds: [
      //     {
      //       type: 'Winnerbracket',
      //       matches: [
      //         {
      //           number: 1,
      //           players: [
      //             { name: 'markeloff', score: 0, number: 1, previousMatch: '' },
      //             { name: 'f0rest', score: 0, number: 8, previousMatch: '' },
      //           ],
      //         },
      //         {
      //           number: 2,
      //           players: [
      //             { name: 'Edward', score: 0, number: 2, previousMatch: '' },
      //             { name: 'Gux', score: 0, number: 7, previousMatch: '' },
      //           ],
      //         },
      //         {
      //           number: 3,
      //           players: [
      //             { name: 'ceh9', score: 0, number: 4, previousMatch: '' },
      //             { name: 'DanucD', score: 0, number: 6, previousMatch: '' },
      //           ],
      //         },
      //         {
      //           number: 4,
      //           players: [
      //             { name: 'POKAMOLODOY', score: 0, number: 3, previousMatch: '' },
      //             { name: 'trace', score: 0, number: 5, previousMatch: '' },
      //           ],
      //         },
      //       ],
      //     },
      //     {
      //       type: 'Winnerbracket',
      //       matches: [
      //         {
      //           number: 5,
      //           players: [
      //             { name: '', score: 0, previousMatch: 1 },
      //             { name: '', score: 0, previousMatch: 2 },
      //           ],
      //         },
      //         {
      //           number: 6,
      //           players: [
      //             { name: '', score: 0, previousMatch: 3 },
      //             { name: '', score: 0, previousMatch: 4 },
      //           ],
      //         },
      //       ],
      //     },
      //     {
      //       type: 'Final',
      //       matches: [
      //         {
      //           number: 7,
      //           players: [
      //             {
      //               name: '',
      //               score: 0,
      //               previousMatch: 5,
      //             },
      //             {
      //               name: '',
      //               score: 0,
      //               previousMatch: 6,
      //             },
      //           ],
      //         },
      //       ],
      //     },
      //   ],
      // };
    }
  }
  setStructure(data: any) {
    this.dataforbracket1 = [];
    let total = data.data.noOfRound;
    let round = data.data.round;
    let i = 0;
    let dataforbracket = [];
    //this.dataforbracket[0].type = 'Winnerbracket'
    let j = 1;
    let k = 0;
    for (let data of round['1']) {
      let number = j;
      let players: any = [];
      let dt = {
        name: data.teamA.teamName,
        score: data.sets[0].teamAScore,
        number: j,
        previousMatch: '',
      };
      let dt1 = {
        name: data.teamB.teamName,
        score: data.sets[0].teamBScore,
        number: j,
        previousMatch: '',
      };
      players.push(dt);
      players.push(dt1);
      let data3 = {
        number: j,
        players: players,
      };
      dataforbracket.push(data3);
      j = j + 1;
    }
    let data4 = {
      type: 'Winnerbracket',
      matches: dataforbracket,
    };
    this.dataforbracket1.push(data4);
    let s = 1;
    let a = 2;
    for (let m = 2; m < total; m++) {
      dataforbracket = [];
      for (let data of round[m.toString()]) {
        let number = j;
        let players: any = [];
        let dt = {
          name: data.placeHolderA,
          score: 0,
          number: j,
          previousMatch: s,
        };
        s = s + 1;
        let dt1 = {
          name: data.placeHolderB,
          score: 0,
          number: j,
          previousMatch: s,
        };
        s = s + 1;
        players.push(dt);
        players.push(dt1);
        let data3 = {
          number: j,
          players: players,
        };
        dataforbracket.push(data3);
        j = j + 1;
      }
      let data5 = {
        type: 'Winnerbracket',
        matches: dataforbracket,
      };

      this.dataforbracket1.push(data5);
    }
    dataforbracket = [];
    for (let data of round[total.toString()]) {
      let number = j;
      let players: any = [];
      let dt = {
        name: data.placeHolderA,
        score: 0,
        number: j,
        previousMatch: s,
      };
      s = s + 1;
      let dt1 = {
        name: data.placeHolderB,
        score: 0,
        number: j,
        previousMatch: s,
      };
      s = s + 1;
      players.push(dt);
      players.push(dt1);
      let data3 = {
        number: j,
        players: players,
      };
      dataforbracket.push(data3);
      j = j + 1;
    }
    let data6 = {
      type: 'Final',
      matches: dataforbracket,
    };

    this.dataforbracket1.push(data6);
    this.singleEliminationTournament = {
      rounds: this.dataforbracket1,
    };
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.hasOwnProperty('structure') && changes.structure.currentValue) {
      // this.structure = changes.structure.currentValue
      if (this.structure) {
        this.setStructure(this.structure);
      }
    }
  }
}
