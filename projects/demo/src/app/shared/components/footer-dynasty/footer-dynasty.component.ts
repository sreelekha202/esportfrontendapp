import { Component, OnInit } from '@angular/core';
import { AppHtmlRoutes } from '../../../app-routing.model';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'app-footer-dynasty',
  templateUrl: './footer-dynasty.component.html',
  styleUrls: ['./footer-dynasty.component.scss']
})
export class FooterDynastyComponent implements OnInit {

  AppHtmlRoutes = AppHtmlRoutes;

  buildConfig = environment.buildConfig || 'N/A';

  socials = [
    {
      link: 'https://twitter.com/PaidiaGaming',
      icon: 'assets/icons/socials/icon-twitter.svg',
    },
    {
      link: 'https://www.facebook.com/paidiagaming',
      icon: 'assets/icons/socials/icon-fb.svg',
    },
    {
      link: 'https://www.instagram.com/paidiagaming/',
      icon: 'assets/icons/socials/icon-insta.svg',
    },
    {
      link: 'https://www.linkedin.com/company/paidia-gaming/',
      icon: 'assets/icons/socials/icon-linke.svg',
    },
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
