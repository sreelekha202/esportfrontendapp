import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FooterDynastyComponent } from './footer-dynasty.component';

describe('FooterDynastyComponent', () => {
  let component: FooterDynastyComponent;
  let fixture: ComponentFixture<FooterDynastyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [FooterDynastyComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FooterDynastyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
