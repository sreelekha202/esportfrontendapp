import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FooterDynastyComponent } from './footer-dynasty.component';
import { RouterModule } from '@angular/router';

import { MatIconModule } from '@angular/material/icon';
import { I18nModule } from 'esports';
import { environment } from '../../../../environments/environment';

@NgModule({
  declarations: [FooterDynastyComponent],
  imports: [CommonModule, RouterModule, MatIconModule, I18nModule.forRoot(environment),],
  exports: [FooterDynastyComponent],
})
export class FooterDynastyModule { }
