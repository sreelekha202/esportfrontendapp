import { Component, OnInit,Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-months-statistic',
  templateUrl: './months-statistic.component.html',
  styleUrls: ['./months-statistic.component.scss'],
})
export class MonthsStatisticComponent implements OnInit {
  selectedMonth: any = 'Select month';
  zero=0;
  @Input() statisticsDetails: any;
  @Output() monthData=new EventEmitter
  months = [
    'JANUARY',
    'FEBRUARY',
    'MARCH',
    'APRIL',
    'MAY',
    'JUNE',
    'JULY',
    'AUGUST',
    'SEPTEMBER',
    'OCTOBER',
    'NOVEMBER',
    'DECEMBER',
  ];
  threemonth: any;

  constructor() {
    this.threemonth = "THIS_MONTH";
    this.selectedMonth = this.months[new Date().getMonth()]
  }

  ngOnInit(): void {

  }

selecteMonths(type) {
  if(type=="THIS_MONTH"){
    this.threemonth = type;
    this.monthData.emit(1*30)
  }
  if(type=="THREE_MONTH"){
    this.threemonth = type;
    this.monthData.emit(3*30)
  }
  if(type=="SIX_MONTH"){
    this.threemonth = type;
    this.monthData.emit(6*30)
  }

    }

}
