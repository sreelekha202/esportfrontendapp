import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-cards-statistic',
  templateUrl: './cards-statistic.component.html',
  styleUrls: ['./cards-statistic.component.scss'],
})
export class CardsStatisticComponent implements OnInit {
  @Input() Data:any;
  constructor() {}

  ngOnInit(): void {}
}
