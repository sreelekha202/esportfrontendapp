import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FooterMenuMobileComponent } from './footer-menu-mobile.component';

describe('FooterMenuMobileComponent', () => {
  let component: FooterMenuMobileComponent;
  let fixture: ComponentFixture<FooterMenuMobileComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FooterMenuMobileComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FooterMenuMobileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
