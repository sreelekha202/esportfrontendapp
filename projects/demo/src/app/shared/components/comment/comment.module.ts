import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { LazyLoadImageModule } from 'ng-lazyload-image';

import { I18nModule } from 'esports';

import { CommentComponent } from './comment.component';
import { environment } from '../../../../environments/environment';

@NgModule({
  declarations: [CommentComponent],
  imports: [CommonModule,  I18nModule.forRoot(environment), LazyLoadImageModule, RouterModule],
  exports: [CommentComponent],
})
export class CommentModule {}
