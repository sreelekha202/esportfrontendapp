import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderDynastyComponent } from './header-dynasty.component';

describe('PaidiaHeaderComponent', () => {
  let component: HeaderDynastyComponent;
  let fixture: ComponentFixture<HeaderDynastyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HeaderDynastyComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderDynastyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
