import { Component, OnInit, Input, Output, EventEmitter,ViewChild,ElementRef,Renderer2 } from '@angular/core';
import { EsportsUserService } from 'esports';
import { Router } from '@angular/router';

@Component({
  selector: 'app-search-players',
  templateUrl: './search-players.component.html',
  styleUrls: ['./search-players.component.scss'],
})
export class SearchPlayersComponent implements OnInit {
  @Input() heading: string = "";
  @Input() listTitle: string = "";
  @Output() submitData = new EventEmitter<any>();
  @ViewChild('searchSuggetions') searchSuggetion: ElementRef;
  @ViewChild('toggleInput') togInput: ElementRef;
  checkCreate8 = false;
  players: any = [];
  selectedPlayers: any = [];
  selectedInvite = 0;
  text: string = '';
  isSuggestionsOpen:boolean = false;
  constructor(
    private userService: EsportsUserService,
    public router: Router,
    private renderer: Renderer2
    ) { 
      this.renderer.listen('window', 'click',(e:Event)=>{
        if(this.togInput && !this.togInput.nativeElement.contains(e.target) && this.searchSuggetion && !this.searchSuggetion.nativeElement.contains(e.target)){
            this.isSuggestionsOpen=false;
        }
     });
     }

  ngOnInit(): void {
    
    this.checkRoute();
    this.text = "";
    
  }
  checkWidthScreen() {
    return window.screen.width <= 414;
  }
  sendData() {
    let data1 = [];
    for (let data of this.selectedPlayers) {
      data1.push(data)
    }
    this.submitData.emit(data1);
  }
  checkRoute() {
    if (this.router.url == "/create-tournament/CreateQuick") {
      this.checkCreate8 = true
    } else {
      this.checkCreate8 = false
    }
  }

  addPlayer(index: number, item:any): void {
    let currentPlayer;
    this.players.forEach((item1, i) => {
      if (item1._id === item._id) {
        currentPlayer = item;
        this.players[i].isSelected = !this.players[i].isSelected;
        if(this.players[i].isSelected) {
          this.selectedInvite++;
        }
        else {
          this.selectedInvite--;
        }
      }
    });

    var existed = false;
    this.selectedPlayers.forEach((item2, i) => {
      if (item2._id === item._id) {
        existed = true;
        this.selectedPlayers.splice(i, 1);
      }
    });
    if(existed == false) {
      this.selectedPlayers.push(currentPlayer);
    }   

    let data1 = [];
    for (let data of this.selectedPlayers) {
      data1.push(data)
    }
    this.submitData.emit(data1);
  }

  filteredPlayer() {
    return this.players.filter((obj) => obj.nickname.includes(this.text.toLowerCase().trim()));
  }

  openFocus(){
    this.isSuggestionsOpen = true;
  }

  clearSelected(){
    this.selectedPlayers = [];
  }

  getvalues(e) {
    let pre_player = this.selectedPlayers;
    this.players = [];
    if(e){
    this.userService.searchUsers(e).subscribe(
      (res: any) => {
        this.isSuggestionsOpen = true;
        this.players = res;
        for (let d of this.players) {
          d.isSelected = false;
        }
        for (let d2 of pre_player) {
          if (d2.isSelected == true) {
            let f = 1;
            for (let d3 of this.players) {
              if (d3.fullName == d2.fullName) {
                f = 2;
                d3.isSelected = true
              }
            }
            if (f == 1) {
              this.players.push(d2)
            }
          }
        }
      },
      (err) => {
       }
    );
      }

  }

}
