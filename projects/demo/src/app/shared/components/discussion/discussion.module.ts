import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MaterialModule } from '../../modules/material.module';
import { I18nModule, EsportsPopupModule, EsportsModule } from 'esports';

import { DiscussionComponent } from './discussion.component';
import { MessageComponent } from './message/message.component';
import { NeedLoginComponent } from './need-login/need-login.component';

import { MomentModule } from 'ngx-moment';
import { LazyLoadImageModule } from 'ng-lazyload-image';
import { environment } from '../../../../environments/environment';
import { PopupModule } from '../popup-member/popup/popup.module';
import { EsportsChatService } from 'esports'
@NgModule({

  declarations: [DiscussionComponent, MessageComponent, NeedLoginComponent],
  providers : [
    EsportsChatService
  ],
  imports: [
    CommonModule,
    FormsModule,
    I18nModule.forRoot(environment),
    LazyLoadImageModule,
    MaterialModule,
    MomentModule,
    ReactiveFormsModule,
    RouterModule,
    PopupModule,
    EsportsPopupModule,
    EsportsModule.forRoot(environment),
  ],
  exports: [DiscussionComponent, MessageComponent, NeedLoginComponent],
})
export class DiscussionModule { }
