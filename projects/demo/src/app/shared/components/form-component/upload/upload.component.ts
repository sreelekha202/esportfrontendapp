import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { FormGroup } from "@angular/forms";
import { EsportsToastService, S3UploadService } from "esports";

@Component({
  selector: "app-upload",
  templateUrl: "./upload.component.html",
  styleUrls: ["./upload.component.scss"],
})
export class UploadComponent implements OnInit {
  @Input() btnTitle: string;
  @Input() customFormControlName: string;
  @Input() customFormGroup: FormGroup;
  @Input() dimension;
  @Input() size;
  @Input() title: string;
  @Input() type;
  @Output() valueEmit = new EventEmitter<string>();

  // Tool tip
  @Input() addMessage;
  @Input() changeMessage;
  @Input() customClass: string;
  @Input() id;
  @Input() required: boolean = false;

  isProccessing: boolean = false;
  isProcessing: boolean = false;

  constructor(
    private s3UploadService: S3UploadService,
    private toastService: EsportsToastService
  ) {}

  ngOnInit(): void {}

  eventChange = async (event) => {
    try {
      this.isProcessing = true;

      const upload: any = await this.s3UploadService.validateAndUploadImage(
        event,
        this.dimension,
        this.size,
        this.type
      );

      this.customFormGroup
        .get(this.customFormControlName)
        .setValue(upload?.data[0]?.Location);
      this.valueEmit.emit(upload?.data[0]?.Location);
      this.toastService.showSuccess(upload?.message);
      this.isProcessing = false;
    } catch (error) {
      this.isProcessing = false;
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };
}
