import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { OpenTeamDetailsComponent } from './open-team-details/open-team-details.component';
import {
  EsportsToastService,
  EsportsBracketService,
  EsportsUtilsService,
} from 'esports';
import * as _ from 'lodash';
import { Router } from '@angular/router';

@Component({
  selector: 'app-match-list',
  templateUrl: './match-list.component.html',
  styleUrls: [
    './match-list.component.scss',
  ],
})
export class MatchListComponent implements OnInit, OnChanges {
  @Input() tournament;
  @Input() participantId: string | null;
  @Input() isAdmin: boolean;
  @Input() matchAndTournamentID;
  isLoaded = false;
  round: Array<any> = [];
  stage: Array<any> = [];
  standingRound: Array<any> = [];
  matchList = [];
  tHeader: Array<any> = [];
  setHeader: Array<any> = [];
  selectedRound: any;
  teamLogo: string = '../../../../assets/icons/users.svg';
  teamLogo1: string = '../../../../assets/icons/users.svg';
  currentRoute: string;
  constructor(
    private bracketService: EsportsBracketService,
    private toastService: EsportsToastService,
    private matDialog: MatDialog,
    private router: Router,
    private utilsService: EsportsUtilsService
  ) {}

  ngOnInit(): void {
    this.currentRoute = this.router.url;
  }

  ngOnChanges() {
    if (this.tournament?._id && this.tournament?.bracketType) {
      if (['single', 'double'].includes(this.tournament?.bracketType)) {
        this.fetchDistinctRound();
      } else if (
        ['round_robin', 'battle_royale'].includes(this.tournament?.bracketType)
      ) {
        this.fetchDistinctStage();
      } else if (this.tournament?.bracketType == 'swiss_safeis') {
        this.isLoaded = true;
      }
    }
  }
  fetchDistinctRound = async () => {
    try {
      const queryParam = `?query=${this.utilsService.encodeQuery({
        tournamentId: this.tournament?._id,
      })}`;

      const field = this.getFieldType(this.tournament?.bracketType);
      const response = await this.bracketService.fetchDistinctValue(
        queryParam,
        field
      );
      this.round = response.data.map((r) => {
        return {
          id: r,
          isCollapsed: false,
          isLoaded: false,
        };
      });
      this.selectedRound = this.round[0];
      this.fetchMatches(this.round[0], 0);
      this.isLoaded = true;
    } catch (error) {
      this.isLoaded = true;
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  fetchDistinctStage = async () => {
    try {
      const queryParam = `?query=${this.utilsService.encodeQuery({
        tournamentId: this.tournament?._id,
      })}`;

      const field = this.getFieldType(this.tournament?.bracketType);
      const response = await this.bracketService.fetchDistinctValue(
        queryParam,
        field
      );

      this.stage = response.data.map((r) => {
        return {
          id: r,
          isCollapsed: false,
          isLoaded: false,
        };
      });
      this.isLoaded = true;
    } catch (error) {
      this.isLoaded = true;
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  fetchDistictStageGroup = async (stage, i) => {
    try {
      if (stage.isCollapsed && !stage.isLoaded) {
        const queryParam = `?query=${encodeURIComponent(
          JSON.stringify({
            tournamentId: this.tournament?._id,
            'currentMatch.stage': stage.id,
          })
        )}`;

        const response = await this.bracketService.fetchDistinctValue(
          queryParam,
          'currentMatch.group'
        );

        this.stage[i].isLoaded = true;
        this.stage[i].group = response.data.map((g) => {
          return {
            id: g,
            isCollapsed: false,
            isLoaded: false,
          };
        });
      }
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  fetchDistictStageGroupRound = async (stage, group, i, j) => {
    try {
      if (group.isCollapsed && !group.isLoaded) {
        if (this.tournament?.bracketType == 'battle_royale') {
          const queryParam = `?stage=${stage.id}&group=${group.id}&tournamentId=${this.tournament?._id}`;
          const response = await this.bracketService.fetchStanding(queryParam);

          this.stage[i].group[j].standings = response.data;
          this.standingRound = response.data.length
            ? response.data[0].round
            : [];
        } else {
          const queryParam = `?query=${encodeURIComponent(
            JSON.stringify({
              tournamentId: this.tournament?._id,
              'currentMatch.stage': stage.id,
              'currentMatch.group': group.id,
            })
          )}`;

          const response = await this.bracketService.fetchDistinctValue(
            queryParam,
            'currentMatch.round'
          );

          this.stage[i].group[j].round = response.data.map((r) => {
            return {
              id: r,
              isCollapsed: false,
              isLoaded: false,
            };
          });
        }
        this.stage[i].group[j].isLoaded = true;
      }
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  fetchDistictStageGroupRoundMatch = async (stage, group, round, i, j, k) => {
    try {
      if (round.isCollapsed && !round.isLoaded) {
        const queryParam = `?query=${encodeURIComponent(
          JSON.stringify({
            tournamentId: this.tournament?._id,
            'currentMatch.stage': stage.id,
            'currentMatch.group': group.id,
            'currentMatch.round': round.id,
            matchStatus: { $ne: 'inactive' },
            bye: false,
          })
        )}&select=sets,totalSet,matchNo,winnerTeam,loserTeam,disQualifiedTeam,battleRoyalTeams`;
        const response = await this.bracketService.fetchAllMatches(queryParam);
        this.stage[i].group[j].round[k].match = response.data;
        this.stage[i].group[j].round[k].isLoaded = true;

        const totalSet = response.data.reduce((accumulator, currentValue) => {
          const max =
            currentValue.sets.length > currentValue.totalSet
              ? currentValue.sets.length
              : currentValue.totalSet;
          accumulator = max > accumulator ? max : accumulator;
          return accumulator;
        }, 0);
        response.data.length
          // ? this.createSetHeader(totalSet)
          // : this.createSetHeader(0);
      }
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  fetchMatches = async (round, i) => {
    this.isLoaded = false;
    this.matchList = [];
    try {
      //   const queryParam = `?tournamentId=${this.tournament?._id}&page=1&round=1`;
      // const response = await this.bracketService.fetchAllMatches(queryParam);
      const queryParam = `?query=${encodeURIComponent(
        JSON.stringify({
          tournamentId: this.tournament?._id,
          'currentMatch.round': round.id,
          matchStatus: { $ne: 'inactive' },
          bye: false,
        })
      )}&select=sets,totalSet,matchNo,winnerTeam,loserTeam,disQualifiedTeam,battleRoyalTeams`;
      const response = await this.bracketService.fetchAllMatches(queryParam);

      const highestActiveSetInMatch = await this.fetchHighestActiveSetInMatch(
        response?.data
      );
      this.createSetHeader(this.tournament?.noOfSet, highestActiveSetInMatch);

      if (this.tournament?.bracketType == "battle_royale") {
      }

      else {

        this.matchList = response?.data;
        this.matchList.sort((a, b) => {
          return a.matchNo- b.matchNo;
      });
        this.selectedRound = this.round[i];
        const totalSet = _.reduce(response?.data, function (accumulator, currentValue) {
          const max =
            currentValue.sets.length > currentValue.totalSet
              ? currentValue.sets.length
              : currentValue.totalSet;
          accumulator = max > accumulator ? max : accumulator;
          return accumulator;
        }, 0);
        this.isLoaded = true;
        // this.round[i].match = response.data;
        this.matchList = response.data;
        // if (response.data) {
        //   this.createSetHeader(totalSet);
        // } else {
        //   this.createSetHeader(0);
        // }
      }

      this.round[i].isLoaded = true;
      this.round[i].match = response.data;
      this.selectedRound = this.round[i];
    } catch (error) {
      this.isLoaded = true;
    }
  };


  fetchHighestActiveSetInMatch = async (
    matchList: Array<any>
  ): Promise<number> => {
    let highestActiveSetInMatch = 0;
    for (let match of matchList) {
      if (highestActiveSetInMatch < match?.sets?.length) {
        highestActiveSetInMatch = match?.sets?.length;
      }
    }
    return highestActiveSetInMatch;
  };

  createSetHeader(noOfSetInMatch: number, highestActiveSetInMatch: number) {
    const limit =
      noOfSetInMatch < highestActiveSetInMatch
        ? highestActiveSetInMatch
        : noOfSetInMatch;
    this.setHeader = [];
    for (let i = 0; i < limit; i++) {
      this.setHeader.push({ name: `Set ${i + 1}` });
    }
  }



  // createSetHeader(noOfSet) {
  //   if (this.tHeader.length < noOfSet) {
  //     this.tHeader = [];
  //     for (let i = 0; i < noOfSet; i++) {
  //       this.tHeader.push({ name: `Set ${i + 1}` });
  //     }
  //   }
  // }

  remainingtHeader(th, mh) {
    const rHeader = [];
    for (let i = 0; i < th.length - mh.length; i++) {
      rHeader.push({});
    }

    return rHeader;
  }

  getFieldType = (type) => {
    if (['single', 'double'].includes(type)) {
      return 'currentMatch.round';
    } else if (['round_robin', 'battle_royale'].includes(type)) {
      return 'currentMatch.stage';
    } else {
      return 'currentMatch.round';
    }
  };

  openTeamDetails = async (player) => {
    const confirmed = await this.matDialog
      .open(OpenTeamDetailsComponent, { data: player })
      .afterClosed()
      .toPromise();
  };

  navigateToReportScore(round,index) {
/*
    let roundWithIndex:any={
      round:round,
      matchIndex:index
    }
    this.bracketService.selectedTournament = this.tournament;
    this.bracketService.macthRoundData = roundWithIndex;
    this.currentRoute = this.currentRoute.replace('scoring', 'update-score');
    this.router.navigateByUrl(this.currentRoute,{ state: this.matchAndTournamentID});
*/
    this.router.navigate([`report-score/${round._id}`])
  }

  updateScore(){
    // this.router.navigateByUrl('/manage-tournament/update-score');
  }

  set_1 = ['Round 1'];
  set_2 = ['Round 2'];
  set_3 = ['Round 3'];
  expandedIndex = 0;

  onTextChange(data) {}
  data: any = [
    {
      description: {
        english:
          'How are my Fortnite replays stored? Replays are stored locally on your console or PC. On console your last 10...',
        french: '',
      },
      slug: 'testt--600ecd33089c510008c6e57b',
      thumbnailUrl: 'assets/icons/video-player/image 59.png',
      title: { english: 'fortnite battle royale - replay system', french: '' },
      youtubeUrl: 'https://www.youtube.com/watch?v=VPe6-LoYXyQ',
      _id: '600ecd33089c510008c6e57b',
    },
    {
      description: {
        english:
          'ESRB Rating: Teen with violence sign up for the fortnite defending the Fort- Fortnite gameplay...',
        french: '',
      },
      slug: 'testt--600ecd33089c510008c6e57b',
      thumbnailUrl: 'assets/icons/video-player/image 60.png',
      title: { english: 'defending the fort - fortnite gameplay', french: '' },
      youtubeUrl: 'https://www.youtube.com/watch?v=VPe6-LoYXyQ',
      _id: '600ecd33089c510008c6e57b',
    },
    {
      description: {
        english:
          'Checkout some of the new features in Fortnite chapter 2, and we even got a Victory Royale to celebrate...',
        french: '',
      },
      slug: 'testt--600ecd33089c510008c6e57b',
      thumbnailUrl: 'assets/icons/video-player/image 61.png',
      title: {
        english: 'fortnite chapter 2 - victory royale gamepaly',
        french: '',
      },
      youtubeUrl: 'https://www.youtube.com/watch?v=VPe6-LoYXyQ',
      _id: '600ecd33089c510008c6e57b',
    },
  ];


}
