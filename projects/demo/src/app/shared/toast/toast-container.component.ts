import { Component, TemplateRef } from "@angular/core";
import { EsportsToastService } from "esports";
@Component({
  selector: "app-toasts",
  template: `
    <ngb-toast
      *ngFor="let toast of toastService.toasts"
      (hide)="toastService.remove(toast)"
      [class]="toast.classname"
      [delay]="toast.delay || 5000"
    >
      <ng-template [ngIf]="isTemplate(toast)" [ngIfElse]="text">
        <ng-template [ngTemplateOutlet]="toast.textOrTpl"></ng-template>
      </ng-template>

      <ng-template #text></ng-template>
      <div [innerHTML]="toast.textOrTpl"></div>
    </ngb-toast>
  `,
  host: { "[class.ngb-toasts]": "false" },
  styles: [
    `
      :host {
        position: fixed;
        top: 10%;
        left: 60%;
        margin: 0.5em;
        z-index: 1200;
      }
      .invalidFields ol {
        list-style-type: decimal !important;
      }
    `,
  ],
})
export class ToastsContainer {
  constructor(public toastService: EsportsToastService) {}

  isTemplate(toast) {
    return toast.textOrTpl instanceof TemplateRef;
  }
}
