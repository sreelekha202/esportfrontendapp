import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { EsportsTournamentService } from 'esports';
import {
  FormBuilder,
  FormGroup,
  AbstractControl,
  Validators,
} from '@angular/forms';

export interface LadderPopupComponentData {
  isShowLoading?: boolean;
  isShowNotFound?: boolean;
  isShowScheduler?: boolean;
  isShowReportScoreCard?: boolean;
  payload?: any;
  isShowNotFoundSeason?: boolean;
}

export enum closeAction {
  'scheduleMatch' = 0,
  'searchAgain' = 1,
}


@Component({
  selector: 'app-ladder-popup',
  templateUrl: './ladder-popup.component.html',
  styleUrls: ['./ladder-popup.component.scss']
})
export class LadderPopupComponent implements OnInit {
  timeForm: FormGroup;
  minStartDateValue = this.setMinDate();
  maxDateValue = this.setMaxDate();
  leftTime: Number;
  timeOut: Number;
  constructor(
    // @Inject('color') public color,
    public tournamentService: EsportsTournamentService,
    private fb: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: LadderPopupComponentData,
    public dialogRef: MatDialogRef<LadderPopupComponent>
  ) { }

  ngOnInit(): void {
    this.leftTime = this.data?.payload?.leftTime
      ? this.data?.payload?.leftTime
      : 180;
    this.timeOut=0;
    this.timeForm = this.fb.group({
      startDate: [
        '',
        Validators.compose([
          Validators.required,
          this.ValidateStartDate.bind(this),
        ]),
      ],
      startTime: [
        '',
        Validators.compose([
          Validators.required,
          this.ValidateStartTime.bind(this),
          this.ValidateRescheduleTime.bind(this),
        ]),
      ],
    });

    this.timeForm = this.fb.group({
      startDate: [
        '',
        Validators.compose([
          Validators.required,
          this.ValidateStartDate.bind(this),
        ]),
      ],
      startTime: [
        '',
        Validators.compose([
          Validators.required,
          this.ValidateStartTime.bind(this),
          this.ValidateRescheduleTime.bind(this),
        ]),
      ],
    });
  }

  onSubmit() {
    if (this.timeForm.valid) {
      this.dialogRef.close(this.timeForm.value);
    }
  }

  closePopup(event) {
    this.dialogRef.close();
  }

  closePopupOnTimeout(event) {
    if (event) {
      if (event.action == 'done') {
        this.dialogRef.close('timeOut');
      }
    }
  }

  ValidateStartTime(control: AbstractControl): { [key: string]: any } | null {
    return !control.value
      ? null
      : this.ValidateBothStartDateAndTime(control['_parent'], 'pastStartTime');
  }

  ValidateRescheduleTime(control: AbstractControl): { [key: string]: any } | null {
    return !control.value
      ? null
      : this.ValidateReschduleTimeWithStartDate(control['_parent'], 'ladderRescheduleEndTime');
  }

  ValidateStartDate(control: AbstractControl): { [key: string]: any } | null {
    return !control.value
      ? null
      : this.ValidateBothStartDateAndTime(control['_parent'], 'pastStartDate');
  }

  ValidateBothStartDateAndTime(
    formGroup: FormGroup,
    ekey: string
  ): { [key: string]: boolean } {
    const { startDate, startTime } = formGroup.controls;
    const time = startTime.value || {};
    const { hour, minute } = time;

    if (startTime.value && startDate.value) {
      const date = new Date(startDate.value);
      date.setHours(hour);
      date.setMinutes(minute);
      const diff = date.getTime() - Date.now();
      if (diff < 300000) {
        return { [ekey]: true };
      }

      this.timeForm.get('startTime').setErrors(null);
      this.timeForm.get('startDate').setErrors(null);
    }
    return null;
  }

  ValidateReschduleTimeWithStartDate(
    formGroup: FormGroup,
    ekey: string
  ): { [key: string]: boolean } {
    const { startDate, startTime } = formGroup.controls;
    const time = startTime.value || {};
    const { hour, minute } = time;

    if (startTime.value && startDate.value) {
      const date = new Date(startDate.value);
      date.setHours(hour);
      date.setMinutes(minute);

      if (this.data?.payload?.endDate) {
        let endDate = new Date(this.data?.payload?.endDate);
        if (endDate <= date) {
          return { [ekey]: true };
        }
      }

      this.timeForm.get('startTime').setErrors(null);
    }
    return null;
  }

  setMinDate() {
    let date = new Date();
    let year: any = date.getFullYear();
    let month: any = date.getMonth() + 1;
    month = month < 10 ? '0' + month : month;
    let day: any = date.getDate();
    day = day < 10 ? '0' + day : day;
    let d = `${year}-${month}-${day}`;
    return d;
  }

  setMaxDate() {
    if (this.data?.payload?.endDate) {
      let date = new Date(this.data?.payload?.endDate);

      let year: any = date.getFullYear();
      let month: any = date.getMonth() + 1;
      month = month < 10 ? '0' + month : month;
      let day: any = date.getDate();
      day = day < 10 ? '0' + day : day;
      let d = `${year}-${month}-${day}`;
      return d;
    } else {
      return null;
    }
  }

  closeNotFound(action: closeAction) {
    this.dialogRef.close(closeAction[action]);
  }

}


