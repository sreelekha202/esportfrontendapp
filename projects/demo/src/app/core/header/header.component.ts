import {
  Component,
  ElementRef,
  Inject,
  OnDestroy,
  OnInit,
  PLATFORM_ID,
} from '@angular/core';
import { DOCUMENT, isPlatformBrowser } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { fromEvent, Subscription } from 'rxjs';
import { environment } from '../../../environments/environment';
import {
  AppHtmlProfileRoutes,
  AppHtmlRoutes,
  AppHtmlRoutesLoginType,
  AppHtmlAccountSettingRoutes,
  AppHtmlOverviewRoutes,
  AppHtmlGetMatchRoutes,
} from '../../app-routing.model';
import { TranslateService } from '@ngx-translate/core';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';
import {
  faBars,
  faTimes,
  faSearch,
  faBell,
} from '@fortawesome/free-solid-svg-icons';
import { MatDialog } from '@angular/material/dialog';
import { SettingsLightboxComponent } from '../../modules/settings-lightbox/settings-lightbox.component';
import {
  EsportsUserService,
  EsportsLanguageService,
  EsportsConstantsService,
  EsportsToastService,
  EsportsGameService,
  GlobalUtils,
  EsportsChatService,
  IUser,
  EsportsChatSidenavService,
} from 'esports';
import { AccountSettingComponent } from '../../modules/account-setting/account-setting/account-setting.component';

const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;

enum AppLanguage {
  fr = 'fr',
  en = 'en',
}

@AutoUnsubscribe()
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit, OnDestroy {
  activeLang = this.constantsService?.defaultLangCode;
  AppHtmlProfileRoutes = AppHtmlProfileRoutes;
  AppHtmlRoutes = AppHtmlRoutes;
  AppHtmlRoutesLoginType = AppHtmlRoutesLoginType;
  AppHtmlOverviewRoutes = AppHtmlOverviewRoutes;
  collapsed: boolean = true;
  isAdmin: boolean = false;
  isBrowser: boolean;
  isUserLoggedIn: boolean = false;
  mobMenuOpened: boolean = false;
  mobMenuOpeneds: boolean = false;
  showLoader: boolean = false;
  gameListShow: boolean = false;
  base64textString: any;
  currentUser: IUser;
  gameList: any;
  tournaments: any;
  matchlist: any;
  matchcount: number;
  chatWins = [];
  currentUserId: any;

  AppLanguage = [];

  faBell = faBell;
  faSearch = faSearch;
  faBars = faBars;
  faTimes = faTimes;

  navigation = [
    // {
    //   title: 'HEADER.HOME',
    //   url: AppHtmlRoutes,
    // },
    {
      title: 'HEADER.DISCOVER',
      url: AppHtmlRoutes.content,
      icon: 'assets/icons/nav/discover-icon.png',
    },
    {
      title: 'HEADER.PLAY',
      url: AppHtmlRoutes.play,
      icon: 'assets/icons/nav/play-icon.png',
    },
    {
      title: 'HEADER.STORE',
      url: AppHtmlRoutes.store,
      icon: 'assets/icons/nav/shop-icon.png',
    },
    {
      title: 'HEADER.MATCHMAKING',
      url: AppHtmlGetMatchRoutes.matchMaking,
      icon: 'assets/icons/nav/matchmaking-icon.png',
    },
    {
      title: 'HEADER.LEADERBOARD',
      url: AppHtmlRoutes.leaderBoard,
      icon: 'assets/icons/nav/leaderboard-icon.png',
    },
    // {
    //   title: 'HEADER.ABOUT',
    //   url: AppHtmlRoutes.aboutUs,
    // },
  ];

  socials = [
    {
      link: 'https://twitter.com/SparkNZ',
      icon: 'assets/icons/socials/twitter.svg',
    },
    {
      link: 'https://www.facebook.com/spark4nz/',
      icon: 'assets/icons/socials/facebook.svg',
    },
    {
      link: 'https://www.instagram.com/sparknz/',
      icon: 'assets/icons/socials/instagram.svg',
    },
    {
      link: 'https://www.linkedin.com/company/spark-new-zealand/',
      icon: 'assets/icons/socials/linkedin.svg',
    },
  ];
  userLinks = [
    {
      title: 'My profile',
      icon: 'person_outline',
      url: AppHtmlProfileRoutes.dashboard,
    },
    {
      title: 'My Teams',
      icon: 'people_outline',
      url: AppHtmlProfileRoutes.myTeams,
    },
    {
      title: 'My Tournament',
      icon: 'people_outline',
      url: AppHtmlProfileRoutes.tournamentsCreated,
    },
    {
      title: 'My Stats',
      icon: 'show_chart',
      url: AppHtmlProfileRoutes.myStats,
    },
    {
      title: 'My bookmarks',
      icon: 'bookmark_border',
      url: AppHtmlProfileRoutes.bookmarks,
    },
  ];

  private sub1: Subscription;
  userSubscription: Subscription;
  selectedFile: any;

  constructor(
    @Inject(PLATFORM_ID) private platformId,
    @Inject(DOCUMENT) private document: Document,
    private chatService: EsportsChatService,
    private chatSidenavService: EsportsChatSidenavService,
    private elementRef: ElementRef,
    private languageService: EsportsLanguageService,
    private router: Router,
    private userService: EsportsUserService,
    public toastService: EsportsToastService,
    public constantsService: EsportsConstantsService,
    public translate: TranslateService,
    public matDialog: MatDialog,
    public gameService: EsportsGameService
  ) {
    this.isBrowser = isPlatformBrowser(platformId);
  }

  ngOnInit(): void {
    this.matchlist = [];
    this.activeLang = this.translate.currentLang;
    this.AppLanguage = this.constantsService?.language;
    this.matchcount = 0;
    this.getGames();

    if (
      this.isBrowser &&
      GlobalUtils.isBrowser() &&
      localStorage.getItem(environment.currentToken)
    ) {
      this.showLoader = true;

      this.userSubscription = this.userService.currentUser.subscribe(
        (data) => {
          if (data) {
            this.currentUser = data;
            this.isUserLoggedIn = !!this.currentUser;
            this.showLoader = false;

            this.currentUserId = this.currentUser._id;
            this.isAdmin =
              data?.accountType === EsportsConstantsService.AccountType.Admin;
            const accessToken = localStorage.getItem(environment.currentToken);

            this.chatService?.login.subscribe((data) => {
              // if (data.accessToken != 'undefined' && data.accessToken != null) {
              this.chatService?.connectUser();

              this.chatService.unreadCount.subscribe((unreadCount) => {
                this.matchcount = unreadCount;
              });

              this.chatService?.getUnReadCount().subscribe((res: any) => { });
              // }
            });
          } else {
            this.userService.getAuthenticatedUser(API, TOKEN);
          }
        },
        (error) => {
          this.showLoader = false;
          this.toastService.showError(error.message);
        }
      );
    }
    this.onHeaderScroll();
  }

  ready() { }

  ngOnDestroy() {
    this.userSubscription?.unsubscribe();
    if (this.currentUserId) {
      this.chatService?.disconnectUser();
    }
  }

  onselectedGame(gameId) {
    localStorage.setItem('gameId', gameId);
    this.router
      .navigateByUrl('/RefreshComponent', { skipLocationChange: true })
      .then(() => {
        this.router.navigate(['/overview']);
        this.gameListShow = false;
      });
  }

  onChatSidenavToggle(): void {
    this.chatSidenavService.toggle();

    if (this.currentUserId) {
      this.chatService?.getAllMatches(this.currentUserId, 1, 30);
    }

    if (this.chatSidenavService.checkOpen()) {
      if (this.chatService.getChatStatus()) {
        this.chatService.setChatStatus(false);
      }
    } else {
      if (this.chatService.getChatStatus()) {
        this.chatService.setChatStatus(false);
      }
    }
  }

  onLogOut(): void {
    if (GlobalUtils.isBrowser()) {
      this.chatService.setChatStatus(false);
      this.userService.logout(API, TOKEN);
      this.chatSidenavService.close();
      this.chatService.doLogout();
      this.chatService?.disconnectUser();
    }

    this.router.navigate([
      AppHtmlRoutes.userPageType,
      AppHtmlRoutesLoginType.emailLogin,
    ]);
  }

  onLanguageChange(lang): void {
    if (GlobalUtils.isBrowser()) {
      localStorage.setItem('currentLanguage', lang);
      this.activeLang = lang;
      this.translate.use(lang);
      this.languageService.setLanguage(lang);
    }
  }

  private onHeaderScroll(): void {
    if (GlobalUtils.isBrowser()) {
      const scrollingContent = document.querySelector('mat-sidenav-content');

      if (scrollingContent) {
        const header = this.elementRef.nativeElement;

        this.sub1 = fromEvent(scrollingContent, 'scroll').subscribe(
          (e: Event) => {
            if ((e.target as Element).scrollTop > header.clientHeight) {
              header.classList.add('is-scrolling');
            } else {
              header.classList.remove('is-scrolling');
            }
          }
        );
      }
    }
  }

  onScrollBody() {
    const hiddenOrVisible = this.mobMenuOpened ? 'hidden' : 'visible';
    this.document.body.style.overflow = hiddenOrVisible;
  }

  openSettingsLightbox(selectedTab = 0) {
    this.router.navigate([AppHtmlAccountSettingRoutes.accountSetting]);
  }

  getGames() {
    this.gameService.getGames(API).subscribe(
      (res) => {
        this.gameList = res.data;
      },
      (err) => { }
    );
  }

  fileChangeEvent(fileInput: any) {
    const reg = /(.*?)\.(jpg|jpeg|png|gif|giff)$/;
    if (!fileInput.target.files[0].name.match(reg)) {
      this.toastService.showError(this.translate.instant('ADMIN.SELECT'));

      return false;
    } else {
      this.selectedFile = fileInput.target.files[0];
      var reader = new FileReader();
      reader.onload = this.handleReaderLoaded.bind(this);
      reader.readAsBinaryString(this.selectedFile);
    }
  }
  handleReaderLoaded(readerEvt) {
    var binaryString = readerEvt.target.result;
    this.base64textString = 'data:image/png;base64,' + btoa(binaryString);
    this.setImage(this.base64textString);
  }

  setImage(data) {
    this.showLoader = true;

    this.userService.updateProfile(API, { profilePicture: data }).subscribe(
      (res) => {
        this.showLoader = false;
        this.toastService.showSuccess(res.message);
        this.userService.refreshCurrentUser(API, TOKEN);
      },
      (err) => {
        this.showLoader = false;
        this.toastService.showError(err.message);
      }
    );
  }
}
