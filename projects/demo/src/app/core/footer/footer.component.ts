import { Component, OnInit } from '@angular/core';

import { AppHtmlRoutes } from '../../app-routing.model';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
})
export class FooterComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;

  buildConfig = environment.buildConfig || 'N/A';

  socials = [
    {
      link: 'https://twitter.com/SparkNZ',
      icon: 'assets/icons/socials/twitter.svg',
    },
    {
      link: 'https://www.facebook.com/spark4nz/',
      icon: 'assets/icons/socials/facebook.svg',
    },
    {
      link: 'https://www.instagram.com/sparknz/',
      icon: 'assets/icons/socials/instagram.svg',
    },
    {
      link: 'https://www.linkedin.com/company/spark-new-zealand/',
      icon: 'assets/icons/socials/linkedin.svg',
    },
  ];

  constructor() {}

  ngOnInit(): void {}
}
