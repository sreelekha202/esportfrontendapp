import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './shared/guard/auth.guard';
import { NotfoundComponent } from './modules/not-found/not-found.component';
import { PrivacyPolicyComponent } from './modules/privacy-policy/privacy-policy.component';
import { TermsOfUsComponent } from './modules/terms-of-us/terms-of-us.component';

const routes: Routes = [
  {
    path: 'content/create',
    loadChildren: () =>
      import('./modules/article/create-article/create-article.module').then(
        (m) => m.CreateArticleModule
      ),
    data: {
      tags: [
        {
          name: 'title',
          content: 'Dynasty',
        },
      ],
      title: 'Dynasty',
    },
  },
  {
    path: 'content/edit/:id',
    loadChildren: () =>
      import('./modules/article/create-article/create-article.module').then(
        (m) => m.CreateArticleModule
      ),
  },
  {
    path: 'content',
    loadChildren: () =>
      import('./modules/article/article.module').then((m) => m.ArticleModule),
    data: {
      tags: [
        {
          name: 'description',
          content:
            'Get the latest news and updates on everything about esports on Dynasty.gg.',
        },
        {
          name: 'og:description',
          content:
            'Get the latest news and updates on everything about esports on Dynasty.gg.',
        },
        {
          name: 'twitter:description',
          content:
            'Get the latest news and updates on everything about esports on Dynasty.gg.',
        },
        {
          name: 'title',
          content:
            'Get the latest news and updates on everything about esports on Dynasty.gg.',
        },
        { name: 'keywords ', content: 'esports, news, esportsnews' },
      ],
      title: 'GAMING NEWS & ARTICLES | Dynasty',
    },
  },
  {
    path: 'play',
    loadChildren: () =>
      import('./modules/play/play.module').then((m) => m.PlayModule),
    data: { title: 'Dynasty - Play' },
  },
  {
    path: 'leaderboard',
    loadChildren: () =>
      import('./modules/leaderboard/leaderboard.module').then(
        (m) => m.LeaderboardModule
      ),
    data: { title: 'Dynasty - Leaderboard' },
  },
  {
    path: 'store',
    loadChildren: () =>
      import('./modules/store/store.module').then((m) => m.StoreModule),
    data: { title: 'Dynasty - Store' },
  },

  {
    path: 'get-match',
    loadChildren: () =>
      import('./modules/get-match/get-match.module').then(
        (m) => m.GetMatchModule
      ),
    data: {
      isRootPage: false,
      title: 'Dynasty Get-Match',
    },
  },
  {
    path: 'notification',
    loadChildren: () =>
      import('./modules/notifications/notifications.module').then(
        (m) => m.NotificationsModule
      ),
  },
  {
    path: 'about-us',
    loadChildren: () =>
      import('./modules/about-us/about-us.module').then((m) => m.AboutUsModule),
  },
  {
    path: 'games',
    loadChildren: () =>
      import('./modules/games/games.module').then((m) => m.GamesModule),
  },
  {
    path: 'home',
    loadChildren: () =>
      import('./modules/home/home.module').then((m) => m.HomeModule),
  },
  {
    path: 'about-us',
    loadChildren: () =>
      import('./../app/modules/about-us/about-us.module').then(
        (m) => m.AboutUsModule
      ),
    data: { title: 'Dynasty - about-us' },
  },
  {
    path: 'tournament',
    loadChildren: () =>
      import('./modules/tournament/tournament.module').then(
        (m) => m.TournamentModule
      ),
    data: {
      tags: [
        {
          name: 'description',
          content:
            'Dynasty.gg supports and covers more than [99] esports games. Create/Organize, join and follow your favourite esports tournaments today!',
        },
        {
          name: 'og:description',
          content:
            'Dynasty.gg supports and covers more than [99] esports games. Create/Organize, join and follow your favourite esports tournaments today!',
        },
        {
          name: 'twitter:description',
          content:
            'Dynasty.gg supports and covers more than [99] esports games. Create/Organize, join and follow your favourite esports tournaments today!',
        },
        {
          name: 'title',
          content:
            'Dynasty.gg supports and covers more than [99] esports games. Create/Organize, join and follow your favourite esports tournaments today!',
        },
        { name: 'keywords ', content: 'esports, tournament' },
      ],
      title: 'Dynasty TOURNAMENTS',
    },
  },
  {
    path: 'create-tournament',
    loadChildren: () =>
      import('./modules/create-tournament/create-tournament.module').then(
        (m) => m.CreateTournamentModule
      ),
    data: {
      isRootPage: true,
      Title: 'Dynasty Create-Tournament',
    },
  },
  {
    path: 'advance-tournament',
    loadChildren: () =>
      import('./modules/quick-advance/quick-advance.module').then(
        (m) => m.QuickAdvanceTournamentModule
      ),
    canActivate: [AuthGuard],
    data: {
      isRootPage: true,
      Title: 'Dynasty Create-Tournament',
    },
  },  
  {
    path: 'join-tournament',
    loadChildren: () =>
      import('./modules/join-tournament/join-tournament.module').then(
        (m) => m.JoinTournamentModule
      ),
    data: {
      isRootPage: true,
      Title: 'Dynasty Join-Tournament',
    },
  },
  {
    path: 'user/:pageType',
    loadChildren: () =>
      import('./modules/log-reg/log-reg.module').then((m) => m.LogRegModule),
    data: {
      isRootPage: true,
      tags: [
        {
          name: 'description',
          content:
            'Start your esports journey with  Dynasty.gg. A complete esports tournament management platform',
        },
        {
          name: 'og:description',
          content:
            'Start your esports journey with  Dynasty.gg. A complete esports tournament management platform',
        },
        {
          name: 'twitter:description',
          content:
            'Start your esports journey with  Dynasty.gg. A complete esports tournament management platform',
        },
        {
          name: 'title',
          content:
            'Start your esports journey with  Dynasty.gg. A complete esports tournament management platform',
        },
      ],
      title: 'Dynasty',
    },
  },
  {
    path: 'privacy-policy',
    component: PrivacyPolicyComponent,
    data: { Title: 'Dynasty Privacy-Policy' },
  },
  {
    path: 'terms-of-use',
    component: TermsOfUsComponent,
    data: { Title: 'Dynasty Terms-Of-Use' },
  },
  {
    path: 'create-team',
    loadChildren: () =>
      import('./modules/team/create-team/create-team.module').then(
        (m) => m.CreateTeamModule
      ),
    canActivate: [AuthGuard],
    data: {
      isRootPage: true,
      title: 'Dynasty Create-team',
    },
  },
  //   {path: 'profile/:my-teams',
  //   loadChildren: () => import('./modules/profile/profile.module').then((m) => m.ProfileModule),
  //   canActivate: [AuthGuard],
  //   data: {
  //     isRootPage: true,
  //     title: "Dynasty profile"
  //   },
  // },

  {
    path: 'view-team/:id',
    loadChildren: () =>
      import('./modules/team/view-team/view-team.module').then(
        (m) => m.ViewTeamModule
      ),
    canActivate: [AuthGuard],
    data: {
      title: 'Dynasty Update-team',
    },
  },
  {
    path: 'manage-team',
    loadChildren: () =>
      import('./modules/manage-teams/manage-teams.module').then(
        (m) => m.ManageTeamsModule
      ),
    canActivate: [AuthGuard],
    data: {
      isRootPage: true,
      title: 'Dynasty Manage-team',
    },
  },

  {
    path: 'manage-tournament/:slug',
    loadChildren: () =>
      import('./modules/manage-tournamens/manage-tournamens.module').then(
        (m) => m.ManageTournamensModule
      ),
    canActivate: [AuthGuard],
    data: {
      isRootPage: true,
      title: 'Dynasty Manage-Tournaments',
    },
  },

  {
    path: 'team-registration/:id',
    loadChildren: () =>
      import('./modules/team-registration/team-registration.module').then(
        (m) => m.TeamRegistrationModule
      ),
    canActivate: [AuthGuard],
    data: {
      isRootPage: true,
      title: 'Dynasty Team-Registration',
    },
  },
  {
    path: 'videos',
    loadChildren: () =>
      import('./modules/videos/videos.module').then((m) => m.VideosModule),
  },

  {
    path: 'search',
    loadChildren: () =>
      import('./modules/search/search.module').then((m) => m.SearchModule),
    data: { title: 'Dynasty Search' },
  },
  {
    path: 'bracket',
    canActivate: [AuthGuard],
    loadChildren: () =>
      import('./modules/bracket/bracket.module').then((m) => m.BracketModule),
    data: { title: 'Dynasty Bracket' },
  },
  {
    path: 'profile',
    canActivate: [AuthGuard],
    loadChildren: () =>
      import('./modules/profile/profile.module').then((m) => m.ProfileModule),
    data: { title: 'Dynasty Profile' },
  },
  {
    path: 'account_settings',
    loadChildren: () =>
      import('./modules/account-setting/account-setting.module').then(
        (m) => m.AccountSettingModule
      ),
    data: { title: 'Dynasty account settings' },
  },
  {
    path: 'admin',
    loadChildren: () =>
      import('./modules/admin/admin.module').then((m) => m.AdminModule),
    data: { title: 'Dynasty Admin' },
  },
  {
    path: 'web-view',
    loadChildren: () =>
      import('./modules/web-view/web-view.module').then((m) => m.WebViewModule),
  },
  {
    path: 'landing-page',
    loadChildren: () =>
      import('./modules/landing/landing.module').then((m) => m.LandingModule),
  },
  {
    path: 'report-score',
    canActivate: [AuthGuard],
    loadChildren: () =>
      import('./modules/report-score/report-score.module').then(
        (m) => m.ReportScoreModule
      ),
    data: {
      isRootPage: true,
      title: 'Dynasty Report score',
    },
  },
  {
    path: 'overview',
    canActivate: [AuthGuard],
    loadChildren: () =>
      import('./modules/overview/overview.module').then(
        (m) => m.OverviewModule
      ),
  },
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: '404', component: NotfoundComponent },
  { path: '**', redirectTo: '/404', pathMatch: 'full' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      scrollPositionRestoration: 'enabled',
      initialNavigation: 'enabled',
    }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule { }
