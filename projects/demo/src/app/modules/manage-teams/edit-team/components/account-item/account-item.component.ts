import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

interface AccountItem {
  icon: string;
  label: string;
  placeHolder: string;
  dataText:string;
}

@Component({
  selector: 'app-account-item',
  templateUrl: './account-item.component.html',
  styleUrls: ['./account-item.component.scss'],
})
export class AccountItemComponent implements OnInit {
  @Input() data: AccountItem;
  @Output() removesocialmedia = new EventEmitter();
  @Output() modalChange = new EventEmitter();
  value= "";
  constructor() {}

  ngOnInit(): void {
    this.value = this.data.dataText;
  }

  removeSocial(a){
    this.value='';
    this.removesocialmedia.emit(this.data.dataText)
  }

  updateSocial(a){
    this.modalChange.emit(a)
  }

}
