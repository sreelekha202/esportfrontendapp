import { Component, OnInit } from '@angular/core';
import { ActivatedRoute,Router } from '@angular/router';
import { AppHtmlManageTeams, AppHtmlProfileRoutes } from '../../../app-routing.model';
import { AppHtmlRoutes } from '../../../app-routing.model';
import { EsportsToastService,
  EsportsGtmService,SuperProperties,IUser,EsportsUserService } from 'esports';
import { TranslateService } from '@ngx-translate/core';
import { environment } from '../../../../environments/environment';
const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;
@Component({
  selector: 'app-team-members',
  templateUrl: './team-members.component.html',
  styleUrls: ['./team-members.component.scss']
})
export class TeamMembersComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;
  AppHtmlProfileRoutes=AppHtmlProfileRoutes;
  AppHtmlManageTeams = AppHtmlManageTeams;
  active = 1;
  nextId: number = 1;
  myTeams = [];
  teamRequests = [];
  showLoader: boolean ;
  show = false;
  isRemove = false;
  id : any;
  mData:any;
  currentUser: IUser;
  isRemovePerson:any = 'assign_as_captain';
  constructor(
    private _activateRoute : ActivatedRoute,
    private router: Router,
    private userService: EsportsUserService,
    private toastService: EsportsToastService,
    public translate: TranslateService,
    private gtmService: EsportsGtmService,
    ) { }

  async ngOnInit() {
    // let id = this._activateRoute.snapshot.paramMap.get('id');
    this._activateRoute.paramMap.subscribe((params)=>{
        this.id = params.get('id');
    })
    const teamMembersData: any = await this.userService.getTeamMember(
      API,
      this.id
    );
      // this.myTeams = res;
      this.myTeams = [];
       for (let d of teamMembersData.data) {
        let d1 = {
          image: d.userId.profilePicture,
          name: d.userId.fullName,
          role : d.role,
          userId : d.userId._id,
          userVo : d.userId,
          status : d.status
        }

        this.myTeams.push(d1)
      }

    // MOCK DATA
    this.teamRequests = [
      {
        image:
          'https://i115.fastpic.ru/big/2021/0711/f1/8bd6a86ccd571aa928cc13958c4173f1.png',
        name: 'Paradox',
        teamsLength: 5,
        createtdAt: 1,
      },
      {
        image:
          'https://i115.fastpic.ru/big/2021/0711/bc/3c1e95f844d101e292be66deca3b09bc.png',
        name: 'Dark Ninja',
        teamsLength: 4,
        createtdAt: 2,
      },
    ];
  }
  showPopup(param,value=null) {
    this.show = !this.show
    if(param && !this.isRemove &&  value && value=='assign_as_captain'){
      this.changeRole(this.mData, 'captain');
    }else if(param && this.isRemove && !this.show){
      this.deleteTeamMember(this.mData, 'deleted');
    }
    this.isRemove = param
  }

  getMData(data){
    this.mData = data;
  }

  async changeRole(player, role) {

    if (role == 'captain') {
      this.pushGTMTags('Make_Captain_Clicked');
    }

    await this.userService
      .update_member(API, {
        teamId: this.id,
        userId: player.userId,
        role: role,
        name: player.name,
      })
      .subscribe(
        (data) => {
          this.toastService.showSuccess(data?.message);
          this.router
            .navigateByUrl('/profile/teams', { skipLocationChange: true })
            .then(() => {
              this.router.navigate(['/manage-team/team-members', this.id]);
            });
        },
        (error) => {}
      );
  }

  pushGTMTags(eventName: string, articleOrVideo = null) {
    let superProperties: SuperProperties = {};
    if (this.currentUser) {
      superProperties = this.gtmService.assignLoggedInUsedData(this.currentUser);
    }

    this.gtmService.gtmEventWithSuperProp({
      eventName: eventName,
      superProps: superProperties,
    });
  }

  /* remove memebr from the team action here  */
  deleteTeamMember(player, status) {
    let data = {
      teamId: this.id,
      userId: player.userId,
      name: player.name,
      status: status,
    };

    this.userService.update_member(API, data).subscribe(
      (data) => {
        this.toastService.showSuccess(data?.message);
        this.router
          .navigateByUrl('/profile/teams', { skipLocationChange: true })
          .then(() => {
            this.router.navigate(['/manage-team/team-members', this.id]);
            //  this.router.navigate(['profile/my-teams']);
          });
      },
      (error) => {}
    );
  }

}
