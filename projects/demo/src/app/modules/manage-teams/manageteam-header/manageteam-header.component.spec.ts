import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageteamHeaderComponent } from './manageteam-header.component';

describe('ManageteamHeaderComponent', () => {
  let component: ManageteamHeaderComponent;
  let fixture: ComponentFixture<ManageteamHeaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManageteamHeaderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageteamHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
