import { Component, OnInit,Input } from '@angular/core';

@Component({
  selector: 'app-manageteam-header',
  templateUrl: './manageteam-header.component.html',
  styleUrls: ['./manageteam-header.component.scss']
})
export class ManageteamHeaderComponent implements OnInit {
  @Input() currentPageLink: string;
  @Input() title: string;
  constructor() { }

  ngOnInit(): void {
  }

}
