import {
  Component,
  OnInit,
  Input
} from '@angular/core';
import {
  EsportsUserService,
  EsportsToastService,
  EsportsLeaderboardService,
} from 'esports';
import { AppHtmlProfileRoutes,AppHtmlRoutes } from '../../../app-routing.model';
import { Router,ActivatedRoute } from '@angular/router';
import { environment } from '../../../../environments/environment';
const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;

@Component({
  selector: 'app-invites-sent',
  templateUrl: './invites-sent.component.html',
  styleUrls: ['./invites-sent.component.scss']
})
export class InvitesSentComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;
  constructor(
    private leaderboardService: EsportsLeaderboardService,
    private router: Router,
    private userService: EsportsUserService,
    private toastService: EsportsToastService,
    private _activateRoute: ActivatedRoute,
  ) { }
  show = false;
  isRemove = false;
  isSelectedData:any;
  pendingInvite = [
    {
      src : '../../../../assets/images/create-tournament/image.png',
      name: 'sitikus96'
    },
    {
      src : '../../../../assets/images/create-tournament/image-2.png',
      name: 'kerisss99'
    },
    {
      src : '../../../../assets/images/Profile/Ellipse 592.png',
      name: 'x0x0youlose'
    },
  ];
  AppHtmlProfileRoutes=AppHtmlProfileRoutes
  followStatus:String = 'follow';
  showLoader: boolean = false;
  teamManageData: any;
  InvitesSent = [];
  totalInvitesSent = [];
  teamId:any;
  isRemovePerson:any = 'invited_person';

  @Input() data: any;


  ngOnInit(): void {
    if (this.data && this.data.image) {
      this.teamManageData = this.data;
    }

    this._activateRoute.paramMap.subscribe((params) => {
      this.teamId = params.get('id');
    });
    this.getMyInvite();
  }

  getMyInvite() {
    this.showLoader = true;
    const query =  {_id: this.teamId };
    const encodedUrl = '?query='+encodeURIComponent(JSON.stringify(query));

    this.userService.getTeamList(API,encodedUrl).subscribe((res: any) => {
      if (res && res.data && res.data[0] && res.data[0].member) {
        this.showLoader = false;
        let b = [];
        res.data[0].member.map((invite) => {
          invite.status == 'invite_pending' ? b.push(invite) : '';
        });
        this.totalInvitesSent = b;
        this.InvitesSent = res.data[0].member;
      }
    },(res: any) => {
      this.showLoader = false;
   });
  }
  
  followUser(data) {
    if (data) {
      if (this.followStatus == 'follow') {
        this.leaderboardService.followUser(API, data.teamMemberId).subscribe(
          (res: any) => {
            this.followStatus = 'unfollow';
            this.checkFollowStatus(data);
            this.toastService.showSuccess(res.message);
            this.userService.refreshCurrentUser(API, TOKEN);
          },
          (err) => {
             this.showLoader = false;
          }
        );
      } else if (this.followStatus == 'unfollow') {
        this.leaderboardService.unfollowUser(API, data.teamMemberId).subscribe(
          (res: any) => {
            this.followStatus = 'follow';
            this.checkFollowStatus(data);
            this.toastService.showSuccess(res.message);
            this.userService.refreshCurrentUser(API, TOKEN);
          },
          (res: any) => {
             this.showLoader = false;
          }
        );
      }
    } else {
      // Navigate to the login page with extras
      //this.router.navigate(['/user/email-login']);
    }
  }

  checkFollowStatus(dat) {
    this.leaderboardService.checkFollowStatus(API, dat.teamMemberId).subscribe(
      (res: any) => {
        if (res.data[0].status == 1) {
          this.followStatus = 'unfollow';
        } else if (res.data[0].status == 0) {
          this.followStatus = 'follow';
        }
      },
      (err) => {
         this.showLoader = false;
      }
    );
  }

  deleteTeam(player, status) {
    let data = {
      teamId: this.teamId,
      userId: player.teamMemberId,
      name: player.name,
      status: status,
    };

    this.userService.update_member(API, data).subscribe(
      (data) => {
        this.toastService.showSuccess(data?.message);
        this.router
          .navigateByUrl('/profile/teams', { skipLocationChange: true })
          .then(() => {
            this.router.navigate(['/manage-team/team-members', this.teamId]);
            //  this.router.navigate(['profile/my-teams']);
          });
      },
      (error) => {}
    );
  }

  // MOCK DATA
  showPopup(event){
    this.show = !this.show
    this.isRemove = event;

    if(event){
      this.deleteTeam(this.isSelectedData, 'deleted')
    }
  }
  isShowPopup(){
    this.show = true;
  }

  isSelectedRow(data){
    this.isSelectedData = data;
  }
}
