import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import {
  AppHtmlManageTeams,
  AppHtmlProfileRoutes,
  AppHtmlRoutes
} from '../../app-routing.model';
import { environment } from '../../../environments/environment';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';
const API = environment.apiEndPoint;
import {
  EsportsConstantsService,
  EsportsToastService,
  EsportsTournamentService,
  EsportsUserService,
  GlobalUtils,
  S3UploadService,
} from 'esports';

@Component({
  selector: 'app-manage-teams',
  templateUrl: './manage-teams.component.html',
  styleUrls: ['./manage-teams.component.scss'],
})
export class ManageTeamsComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;
  teamId: any;
  checkAddMember = false;
  linkTeamMember = '';
  actionName = '';
  src = '';
  teamData: any;
  teamLogoSRCFile: any;
  showLoader: boolean = false;
  selectedTeamData: any;
  currentUser: any;
  userSubscription: Subscription;
  show = false;
  isRemove = true;
  isRemoveTeam: any = 'remove_team';
  selectTeam : any =''
  listSelectTeam=[
    {
      label:"MANAGE_TEAM.NAV.TEAM_MEMBERS",
      link:"/manage-team/team-members"
    },
    {
      label: "MANAGE_TEAM.NAV.INVITES_SENT" ,
      link:AppHtmlManageTeams.invitesSent
    },   {
      label:"MANAGE_TEAM.NAV.EDIT_TEAM" ,
      link:AppHtmlManageTeams.editTeam
    }
  ]
  constructor(
    private activeRoute: ActivatedRoute,
    private router: Router,
    public s3Service: S3UploadService,
    private eSportsToastService: EsportsToastService,
    private eSportsTournamentService: EsportsTournamentService,
    private userService: EsportsUserService,
    private translateService: TranslateService
  ) { }
  AppHtmlManageTeams = AppHtmlManageTeams;
  AppHtmlProfileRoutes = AppHtmlProfileRoutes;
  ngOnInit(): void {
    this.activeRoute.queryParamMap.subscribe((params) => {
      this.teamId = params.get('id');
    });

    if (!this.teamId) { this.teamId = String(window.location.href).split('/').reverse()[0]; }

    this.linkTeamMember = AppHtmlManageTeams.teamMembers + '/' + this.teamId;
    this.checkRoute();
    this.getUserData();
    this.router.events.subscribe((val) => {
      this.checkRoute();
    });

  }

  ngOnDestroy() {
    if (this.userSubscription)
      this.userSubscription.unsubscribe();
  }

  /* get user data here */
  async getUserData() {
    const team: any = await this.userService.getTeamById(API, this.teamId);
    this.selectedTeamData = team?.data;

    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.currentUser = data;
      }
    });

    /* get team refeshment */
    this.refreshTeam();

  }

  /* check routes here */
  checkRoute() {
    if (this.router.url == '/manage-team/add-members') {
      this.checkAddMember = true;
    } else {
      this.checkAddMember = false;
    }
  }
  uploadFile() {
    document.getElementById('upImage').click();
  }
  showPopupHide(parm) {
    if (parm === 'remove_team' || parm === 'switch_team') {
      this.isRemoveTeam = parm;
    } else if (parm && this.isRemoveTeam == 'switch_team') {
      /* redirect here for switch team */
    }
    this.show = true;
  }
  async handleFileInput(files) {
    /*this.src = URL.createObjectURL(files.item(0));
    if (files.item(0)) {
      const file = files.item(0);
      const reader = new FileReader();
      reader.onload = (e: any) => {
        this.src = e.target.result;
      };
      reader.readAsDataURL(file);
    }*/
    //this.isValidImage(files, { width: 1920, height: 300 },'logo');
    let fileDiamention: any = { width: 1600, height: 900 };
    const upload: any = await this.s3Service.isValidImage(files, fileDiamention, 'banner');
    if (upload && upload == 'invalid_size') {
      this.eSportsToastService.showError(
        this.translateService.instant('TOURNAMENT.ERROR_IMG_2')
      );
      return;
    }
    else if (upload && upload == 'valid_image') {
      this.eSportsToastService.showError(
        this.translateService.instant('TOURNAMENT.ERROR_IMG_1')
      );

      const file = files[0];
      const reader = new FileReader();
      reader.readAsDataURL(file);
      let th = this;
      reader.onload = (e) => {
        let image: any = new Image();
        image.src = e.target.result;
        image.onload = function () {
          let height = this.height;
          let width = this.width;
          if (height != fileDiamention['height'] && width != fileDiamention['width']) {
            th.eSportsToastService.showInfo(
              th.translateService.instant('TOURNAMENT.ERROR_IMG')
            );
          }
        };
      }

      this.upload(files, 'logo');
    }
  }

  async upload(files, type: string = 'logo') {
    let promises = [];
    for (let obj of files) {
      promises.push(this.toBase64(obj));
    }
    let Base64String = await Promise.all(promises);
    let imageData = {
      path: environment.tournamentS3BucketName,
      files: Base64String,
    };
    this.teamLogoSRCFile = null;
    this.s3Service.fileUpload(environment.apiEndPoint, imageData).subscribe(
      (res) => {
        if (type == 'logo') {
          this.teamLogoSRCFile = res['data'][0]['Location'];
          this.src = URL.createObjectURL(files.item(0));
          if (files.item(0)) {
            const file = files.item(0);
            const reader = new FileReader();
            reader.onload = (e: any) => {
              this.src = e.target.result;
            };
            reader.readAsDataURL(file);
          }
          this.updateTeam();
        }

        this.eSportsToastService.showSuccess(
          this.translateService.instant('API.BANNER.POST.SUCCESS')
        );
      },
      (err) => {
        this.eSportsToastService.showError(
          this.translateService.instant('API.BANNER.POST.ERROR')
        );
      }
    );
  }

  toBase64(file) {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = (error) => reject(error);
    });
  }

  /* delete team action here */
  deleteTeam(team_id) {

    const data = {
      "id": team_id,
      "admin": true,
      "query": {
        "condition": { "_id": team_id, "status": "active" },
        "update": { "status": "inactive" }
      }
    };
    this.showLoader = true;
    this.userService.update_team_admin(API, data).subscribe((data: any) => {
      let res = JSON.stringify(data);
      this.showLoader = false;
      this.eSportsToastService.showSuccess(
        this.translateService.instant('TEAMS.ACTION.SUCCESS_DELETE')
      );
      this.router.navigate(['profile/my-teams']);
    }, (error) => {
      this.showLoader = false;
      this.eSportsToastService.showError(
        this.translateService.instant('TEAMS.ACTION.ERROR_DELETE')
      );
    });
  }

  showPopup(event) {
    this.show = false
    this.isRemove = ((this.isRemoveTeam == 'remove_team' || this.isRemoveTeam == 'switch_team') ? this.isRemove : event);
    if (event && this.isRemoveTeam == 'remove_team') {
      this.deleteTeam(this.teamId);
    }
  }
  checkInvite(item) {
    this.selectTeam=item
    // if (this.router.url == '/manage-team/invites-sent') {
    //   return (this.actionName = 'Invites sent');
    // } else if (this.router.url == '/manage-team/edit-team') {
    //   return (this.actionName = 'Edit team');
    // } else {
    //   return (this.actionName = 'Team members');
    // }
  }

  updateTeam() {
    let teamObj: any = {
      admin: true,
      id: this.teamId,
      email: [],
      member: this.teamData.members,
      query: {
        condition: { _id: this.teamId },
        update: {
          logo: (this.teamLogoSRCFile ? this.teamLogoSRCFile : ''),
          shortDescription: this.teamData.shortDescription,
          social: (this.teamData.socialMedia ? this.teamData.socialMedia : {}),
          teamName: this.teamData.teamName,
          teamBanner: this.teamData.teamBanner,
        },
      },
      userName: (this.currentUser ? this.currentUser.username : ''),
    };
    this.userService.team_update(API, teamObj).subscribe(
      (res: any) => {
        if (res.data) {
          this.eSportsToastService.showSuccess(res.message);
        } else {
          this.eSportsToastService.showError(res.message);
        }
      },
      (err) => {
        this.eSportsToastService.showError(err.error.message);
      }
    );

  }
  isCheckActive() {
    const currentUrl = this.router.url
    if (currentUrl.search('/manage-team/team-members/') != -1 || currentUrl.search('/manage-team/add-members/') != -1)
      return true
    return false
  }

  refreshTeam(){
    const query = { _id: this.teamId };
    const encodedUrl = '?query=' + encodeURIComponent(JSON.stringify(query));

    this.userService.getTeamList(API, encodedUrl).subscribe((res: any) => {
      this.teamData = {
        teamLogo: res.data[0].logo,
        socialMedia: res.data[0]?.social,
        shortDescription: res.data[0].shortDescription,
        teamName: res.data[0].name,
        members: res.data[0].member,
        teamBanner: res.data[0].teamBanner,
        status: res.data[0].status,
      };
    });
  }
}