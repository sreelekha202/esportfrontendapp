import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ParticipantsRequestComponent } from './participants-request.component';

describe('ParticipantsRequestComponent', () => {
  let component: ParticipantsRequestComponent;
  let fixture: ComponentFixture<ParticipantsRequestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ParticipantsRequestComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ParticipantsRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
