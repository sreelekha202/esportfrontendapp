import { Component, OnInit } from '@angular/core';
import { AppHtmlRoutes } from '../../../../app-routing.model';

import { MatDialog } from '@angular/material/dialog';

import { PopupCreatedComponent } from '../../components/popup-created/popup-created.component';
import { WYSIWYGEditorConfig } from 'esports';

@Component({
  selector: 'app-tournament-details',
  templateUrl: './tournament-details.component.html',
  styleUrls: ['./tournament-details.component.scss'],
})
export class TournamentDetailsComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;
  editorConfig: WYSIWYGEditorConfig = {};
  games = [
    {
      title: 'Valorant',
      value: 'valorant',
    },
    {
      title: 'Overwatch',
      value: 'overwatch',
    },
  ];

  platforms = [
    {
      title: 'PC',
      value: 'pc',
    },
    {
      title: 'Mobile',
      value: 'mobile',
    },
  ];

  constructor(public matDialog: MatDialog) {}

  ngOnInit(): void {
    this.editorConfig = {
      editable: true,
      spellcheck: true,
      height: '200px',
      width: 'auto',
      minWidth: '0',
      translate: 'false',
      enableToolbar: true,
      showToolbar: true,
      defaultParagraphSeparator: '',
      defaultFontName: '',
      defaultFontSize: '',
      fonts: [
        { class: 'arial', name: 'Arial' },
        { class: 'times-new-roman', name: 'Times New Roman' },
        { class: 'calibri', name: 'Calibri' },
        { class: 'comic-sans-ms', name: 'Comic Sans MS' },
      ],
      customClasses: [
        {
          name: 'quote',
          class: 'quote',
        },
        {
          name: 'redText',
          class: 'redText',
        },
        {
          name: 'titleText',
          class: 'titleText',
          tag: 'h1',
        },
      ],
      sanitize: true,
      toolbarPosition: 'top',
      toolbarHiddenButtons: [['bold', 'italic'], ['fontSize']],
    };
  }

  showCreatedPopup(): void {
    this.matDialog.open(PopupCreatedComponent);
  }
}
