import {
  GlobalUtils,
  IUser,
  EsportsToastService,
  EsportsUserService,
  EsportsProfileService
} from 'esports';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppHtmlRoutes } from '../../../../app-routing.model';
import { ErrorStateMatcher } from '@angular/material/core';
import {
  FormControl,
  FormGroupDirective,
  NgForm,
  Validators,
} from '@angular/forms';
import { environment } from '../../../../../environments/environment';

const API = environment.apiEndPoint;

/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(
    control: FormControl | null,
    form: FormGroupDirective | NgForm | null
  ): boolean {
    const isSubmitted = form && form.submitted;
    return !!(
      control &&
      control.invalid &&
      (control.dirty || control.touched || isSubmitted)
    );
  }
}
@Component({
  selector: 'app-select-teamdetails',
  templateUrl: './select-teamdetails.component.html',
  styleUrls: ['./select-teamdetails.component.scss'],
})
export class SelectTeamdetailsComponent implements OnInit {
  d = ['a', 'b'];
  AppHtmlRoutes = AppHtmlRoutes;
  currentUser: IUser;
  constructor(
    private profileService: EsportsProfileService,
    private router: Router,
    private userService: EsportsUserService,
    private toastService: EsportsToastService
  ) {}
  selected: any;
  selectedteam: any;
  tournamentDetails: any;
  showLoader = false;
  submitbut = false;
  ngOnInit(): void {
    if (GlobalUtils.isBrowser()) {
      this.userService.currentUser.subscribe((data) => {
        this.currentUser = data;
      });
      this.tournamentDetails = localStorage.getItem('tournamentDetails');
      this.tournamentDetails = JSON.parse(this.tournamentDetails);
      this.selected = JSON.parse(sessionStorage.getItem('selectedusers'));
      this.selectedteam = JSON.parse(sessionStorage.getItem('selectedteam'));
      this.submitbut = true;
      for (let d of this.selectedteam) {
        d.details.error = false;
        if (d.details.username == undefined || d.details.username == '') {
          this.submitbut = false;
          d.details.error = true;
        }
      }
    }
  }

  validate(data, i) {
    // this.showLoader = true;
    this.profileService
      .ckeckuserid(data, this.tournamentDetails._id)
      .subscribe((data3: any) => {
        // this.showLoader = false;
        if (data3.data.invalidId == false && data3.data.isExist == false) {
          this.submitbut = true;
          for (let d of this.selectedteam) {
            d.details.error = false;
            if (d.details.username == undefined || d.details.username == '') {
              this.submitbut = false;
              d.details.error = true;
            }
          }
          this.selected[i].details.error = false;
        } else {
          this.selected[i].details.error = true;
        }
      });
  }

  submitdata() {
    // this.showLoader = true;
    // let selectuser = [];
    // for (let d of this.selected) {
    //   let d1 = {
    //     name: d.details.fullName,
    //     email: d.details.email,
    //     phoneNumber: d.details.phoneNumber,
    //     inGamerUserId: d.details.username,
    //   };
    //   selectuser.push(d1);
    // }
    // let inGamerUserId = '';
    // this.tournamentDetails.gameDetail._id;
    // this.currentUser.preference.gameDetails.map((game) => {
    //   if (game._id == this.tournamentDetails.gameDetail._id) {
    //     inGamerUserId = game.userGameId;
    //   }
    // });
    // let datatosend = {
    //   teamId: '',
    //   logo: 'https://stc-img.s3.me-south-1.amazonaws.com/dev/profile/1624437521923.png',
    //   teamName: this.selectedteam.name,
    //   name: this.currentUser.fullName,
    //   phoneNumber: this.currentUser.phoneNumber,
    //   email: this.currentUser.email,
    //   inGamerUserId: inGamerUserId,
    //   teamMembers: selectuser,
    //   substituteMembers: [],
    //   participantType: this.tournamentDetails.participantType,
    //   tournamentId: this.tournamentDetails._id,
    // };
    // this.profileService.jointournament(datatosend).subscribe(
    //   (data: any) => {
    //     this.showLoader = false;
    //     this.toastService.showSuccess(data.message);
    //     this.router.navigateByUrl('/play');
    //   },
    //   (err) => {
    //     this.showLoader = false;
    //     this.toastService.showError(err.error.message);
    //     // this.router.navigateByUrl('/play')
    //   }
    // );
    this.router.navigateByUrl('/join-tournament/teamdetails');
  }
  set_1 = ['Player 1 details'];
  set_2 = ['Player 2 details'];
  set_3 = ['Player 3 details'];
  expandedIndex = 0;

  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);

  matcher = new MyErrorStateMatcher();
}
