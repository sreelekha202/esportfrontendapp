import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TournamentNameInputComponent } from './tournament-name-input.component';

describe('TournamentInputComponent', () => {
  let component: TournamentNameInputComponent;
  let fixture: ComponentFixture<TournamentNameInputComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TournamentNameInputComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TournamentNameInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
