import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { GlobalUtils } from 'esports';

@Component({
  selector: 'app-edit-manage-tournament',
  templateUrl: './edit-manage-tournament.component.html',
  styleUrls: ['./edit-manage-tournament.component.scss']
})
export class EditManageTournamentComponent implements OnInit {
  @Output() HiddenPopup = new EventEmitter();
  date: Date;
  time: any;
  constructor() { }

  ngOnInit(): void {
  }
  onDateChange(data) {
    if (GlobalUtils.isBrowser()) {
      this.date = data;
      localStorage.setItem('date', data);
    }

  }
  onTimeChange(data) {
    this.time = data;
   

  }
}
