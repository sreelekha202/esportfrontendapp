import { Component, OnInit, Input } from '@angular/core';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
import { MatDialog } from '@angular/material/dialog';

interface BookmarkCard {
  image: string;
  thumbnailUrl:any,
  title: any;
  shortDescription: any;
  description:any,
  createdDate: any;
  author?: string;
  views?: string;
}

@Component({
  selector: 'app-bookmark-card',
  templateUrl: './bookmark-card.component.html',
  styleUrls: ['./bookmark-card.component.scss'],
})
export class BookmarkCardComponent implements OnInit {
  @Input() data: BookmarkCard;
  @Input() isVideo: boolean = false;
  
  currentLang: string = 'english';
  constructor(
    private translateService: TranslateService,
    public matDialog: MatDialog,
  ) {
    this.currentLang =
      translateService.currentLang == 'ms' ? 'malay' : 'english';

    translateService.onLangChange.subscribe((event: LangChangeEvent) => {
      this.currentLang = event.lang == 'ms' ? 'malay' : 'english';
    });
  }
  ngOnInit(): void {}
  
}
