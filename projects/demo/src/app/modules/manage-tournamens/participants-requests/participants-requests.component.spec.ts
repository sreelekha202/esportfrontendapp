import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ParticipantsRequestsComponent } from './participants-requests.component';

describe('ParticipantsRequestsComponent', () => {
  let component: ParticipantsRequestsComponent;
  let fixture: ComponentFixture<ParticipantsRequestsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ParticipantsRequestsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ParticipantsRequestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
