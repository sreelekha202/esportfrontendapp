import { Component, OnInit,ElementRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import {MatChipInputEvent} from '@angular/material/chips';
import {COMMA, ENTER} from '@angular/cdk/keycodes';


interface status {
  value: string;
  viewValue: string;
}

export interface Attachment {
  name: string;
}


@Component({
  selector: 'app-single-item',
  templateUrl: './single-item.component.html',
  styleUrls: ['./single-item.component.scss']
})
export class SingleItemComponent implements OnInit {

  show = false;

  ngOnInit(): void {
  }

  constructor(
    private router : Router,
  ){}

  previousPage(){
    this.router.navigateByUrl("manage-tournament/reports")
  }

  statuss: status[] = [
    {value: 'open', viewValue: 'Open'},
    {value: 'close', viewValue: 'Close'}
  ];

  selectable = true;
  removable = true;
  addOnBlur = true;
  readonly separatorKeysCodes = [ENTER, COMMA] as const;
  attachments: Attachment[] = [
    {name: 'Image_0012'},
    {name: 'Image_101'},
    {name: 'Image_645'},
    {name: 'Image_1e22'},
  ];

  add(event: MatChipInputEvent): void {
    const value = (event.value || '').trim();

    // Add our fruit
    if (value) {
      this.attachments.push({name: value});
    }

    // Clear the input value
    // event.chipInput!.clear();
  }

  remove(fruit: Attachment): void {
    const index = this.attachments.indexOf(fruit);

    if (index >= 0) {
      this.attachments.splice(index, 1);
    }
  }

  

}
