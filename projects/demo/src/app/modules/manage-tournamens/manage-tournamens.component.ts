import { Component,Inject, OnInit ,PLATFORM_ID } from '@angular/core';;
import { AppHtmlManageTeams, AppHtmlProfileRoutes } from '../../app-routing.model';
import { AppHtmlRoutes } from '../../app-routing.model';
import { Router } from '@angular/router';
import { AppHtmlGetMatchRoutes, } from './../../app-routing.model';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { isPlatformBrowser } from '@angular/common';
import { EsportsChatService, EsportsTimezone,EsportsEditPopupComponent, EsportsToastService, EsportsTournamentService, EsportsUserService, GlobalUtils, IUser } from 'esports';
import { TranslateService } from '@ngx-translate/core';
import { MatDialog } from '@angular/material/dialog';
import { environment } from '../../../environments/environment';
import { I } from '@angular/cdk/keycodes';
const API = environment.apiEndPoint;


@Component({
  selector: 'app-manage-tournamens',
  templateUrl: './manage-tournamens.component.html',
  styleUrls: ['./manage-tournamens.component.scss']
})
export class ManageTournamensComponent implements OnInit {
  userSubscription: Subscription;
  AppHtmlRoutes = AppHtmlRoutes;
  timezone;
  tournament: any = {};
  user: IUser;
  apiLoaded: Array<boolean> = [];
  hideMessage = true;
  hasRequiredAccess: boolean = true;
  tournamentStartTime = '';
  startDateIntervalId = null;
  endDateIntervalId = null;
  noStart = false;
  isLoaded = false;
  slug:any;
  tournamentDetails: any;
  AppHtmlManageTeams = AppHtmlManageTeams;
  AppHtmlProfileRoutes = AppHtmlProfileRoutes;
  AppHtmlGetMatchRoutes = AppHtmlGetMatchRoutes;
  actionName = "";
  showPopup = false;
  tournamentId;
  btnType: string = '';
  participantId: string | null;
   registrationEndedTxt;
  isRegistrationClosed = true;
  isProcessing: boolean = false;
  restrationStartDate: Date;
  remainingTime;
  remainingTimeForTournamentStart;
  remainigCheckInTime: number = 0;
  notifyConfig;
  showRegTime;
  showTournamentTime;

  registrationStatus = {
    isOpen: false,
    isClosed: false,
    notYetStarted: false,
  };

  date = new Date();
  tournament_endDate: any;
  tournament_startdate: any;
  isclosed: any;
  isyettoopen: boolean = false;
  editInfoVo:any;
  Step7;
  gameList = [];
  constructor(private router: Router,
    private _activatedRoute: ActivatedRoute,
    @Inject(PLATFORM_ID) private platformId,
    private esportsTimezone: EsportsTimezone,
    private eSportsChatService: EsportsChatService,
    private userService: EsportsUserService,
    private translateService: TranslateService,
    private eSportsToastService: EsportsToastService,
    private esportsTournamentService: EsportsTournamentService,
    private matDialog: MatDialog
    ) { }

  ngOnInit(): void {

    const isBrowser = isPlatformBrowser(this.platformId);
    if (isBrowser && GlobalUtils.isBrowser()) {
      this.timezone = this.esportsTimezone.getTimeZoneName();
      this.slug = this._activatedRoute.snapshot.params.slug; // get slug from route
      if (this.slug) {
        this.getCurrentUserDetails();
        this.eSportsChatService.connectTournamentEvents(this.slug);



      }
    }

/* set edit info VO object here */


  }


  getCurrentUserDetails = async () => {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.user = data;
        if(this.slug){
          this.fetchTournamentDetails();
        }
      }
    });
  }

  fetchTournamentDetails = async (isTournamentStart: boolean = false, isTournamentFinished: boolean = false) => {
    try {
      this.apiLoaded.push(false);
      const tournament = await this.esportsTournamentService.getTournamentBySlug(this.slug)
      this.tournament = tournament?.data;
      this.tournamentDetails=tournament?.data;
       this.editInfoVo = {};
       this.getSetEditInfo()
      this.setRegistrationOpenStatus()
      this.esportsTournamentService.manageTournamentSubject.next(tournament?.data);
      if (!this.tournament) {
        this.hideMessage = false;
        throw new Error(this.translateService.instant('VIEW_TOURNAMENT.ERROR'));
      }

      const isEventOrganizer = this.user?._id === this.tournament?.organizerDetail?._id;
      const hasAdminAccess = this.user?.accountType == 'admin' && this.user?.accessLevel?.includes('em');

      this.hasRequiredAccess = isEventOrganizer || hasAdminAccess;
      if (!this.hasRequiredAccess) {
        throw new Error(
          this.translateService.instant('VIEW_TOURNAMENT.ACCESS_DENIED')
        )
      }

      if (this.tournament?.isFinished) {
        if (isTournamentFinished) {
          this.eSportsToastService.showInfo(
            this.translateService.instant('ELIMINATION.TOURNAMENT_FINISHED'),
            false
          );
        }
        this.tournamentStartTime = this.translateService.instant(
          'MANAGE_TOURNAMENT.TOURNAMENT_FINISHED'
        );
      } else if (this.tournament?.isSeeded) {
        if (isTournamentStart) {
          this.eSportsToastService.showInfo(
            this.translateService.instant('ELIMINATION.TOURNAMENT_STARTED'),
            false
          );
        }

        this.tournamentStartTime = this.translateService.instant(
          'MANAGE_TOURNAMENT.TOURNAMENT_STARTED'
        );
        this.endDateTimer();
      } else {
        this.startDateTimer();
        this.noStart = true;
      }

      this.apiLoaded.push(true);
    } catch (error) {
      this.hasRequiredAccess = false;
      this.apiLoaded.push(true);
      this.eSportsToastService.showError(error?.error?.message || error?.message);
    } finally {
      this.checkAllApiDataLoaded();
    }
  };

  startDateTimer = () => {
    const startDate = new Date(this.tournament.startDate);
    const currentDate = new Date();
    if (startDate.getTime() - currentDate.getTime() > 0) {
      const millisecond = startDate.getTime() - currentDate.getTime();
      const dd = (millisecond / (1000 * 60 * 60 * 24)) | 0;
      if (dd) {
        const currentLanguage = this.translateService.currentLang;

        if(currentLanguage == 'en') {
          this.tournamentStartTime = `${dd} ${
            dd === 1
              ? this.translateService.instant('MANAGE_TOURNAMENT.DAYS_LEFT.01')
              : this.translateService.instant('MANAGE_TOURNAMENT.DAYS_LEFT.02')
          }`;
        } else {
          if(dd === 1) {
            this.tournamentStartTime = `${dd} ${ this.translateService.instant('MANAGE_TOURNAMENT.DAYS_LEFT.01')}`
          }
          else if (dd === 2){
            this.tournamentStartTime = `${ this.translateService.instant('MANAGE_TOURNAMENT.DAYS_LEFT.02')}`
          }
          else if (dd === 3){
            this.tournamentStartTime = `${dd} ${ this.translateService.instant('MANAGE_TOURNAMENT.DAYS_LEFT.03')}`
          }
          else if (dd === 11){
            this.tournamentStartTime = `${dd} ${ this.translateService.instant('MANAGE_TOURNAMENT.DAYS_LEFT.04')}`
          }
          else {
            this.tournamentStartTime = `${dd} ${ this.translateService.instant('MANAGE_TOURNAMENT.DAYS_LEFT.04')}`
          }
        }
        return;
      }

      const hh = (millisecond / (1000 * 60 * 60)) | 0;
      const mm = ((millisecond - hh * 1000 * 60 * 60) / (1000 * 60)) | 0;
      const ss = ((millisecond - hh * 1000 * 60 * 60) / 1000) % 60 | 0;
      this.tournamentStartTime = `${hh > 9 ? hh : `0${hh}`}: ${ mm > 9 ? mm : `0${mm}`}: ${ss > 9 ? ss : `0${ss}`}`;

      if (!this.startDateIntervalId) {
        this.startDateIntervalId = setInterval(() => this.startDateTimer(), 1000);
      }
    } else {
      if (this.startDateIntervalId) {
        this.fetchTournamentDetails(true);
      } else {
        this.tournamentStartTime = this.translateService.instant(
          'MANAGE_TOURNAMENT.TOURNAMENT_EXPIRED'
        );
      }
      clearInterval(this.startDateIntervalId);
      this.startDateIntervalId = null;
    }
  }

  endDateTimer = () => {
    const endDate = new Date(this.tournament?.endDate);
    const currentDate = new Date();
    const timeDiff = endDate.getTime() - currentDate.getTime();
    if (timeDiff > 0) {
      this.endDateIntervalId = setInterval(() => this.endDateTimer(), 1000);
    } else if (['swiss_safeis', 'ladder'].includes(this.tournament?.bracketType)) {
      if (this.endDateIntervalId) {
        this.fetchTournamentDetails(false, true);
        clearInterval(this.endDateIntervalId);
        this.endDateIntervalId = null;
      }
    }
  }

  /**
   * Fatch Register Or CheckedIn Participant
   * @param field registeredParticipantCount, checkedInParticipantCount
   * @param obj for checkedIn users
   */
   fetchRegisteredParticipant = async (field, obj = {}) => {
    try {
      this.apiLoaded.push(false);
      const encodeUrl = `?query=${encodeURIComponent(
        JSON.stringify({ tournamentId: this.tournament?._id, ...obj })
      )}&select=''&option=${encodeURIComponent(
        JSON.stringify({ count: true })
      )}`;
      const response = await this.esportsTournamentService
        .getParticipants(API,encodeUrl)
        .toPromise();
      this[field] = response?.totals?.count;
      this.apiLoaded.push(true);
    } catch (error) {
      this.apiLoaded.push(true);
      this.eSportsToastService.showError(error?.error?.message || error?.message);
    } finally {
      this.checkAllApiDataLoaded();
    }
  };


  checkAllApiDataLoaded() {
    const startedApiCount = this.apiLoaded.filter((el) => !el).length;
    const finishedApiCount = this.apiLoaded.filter((el) => el).length;
    this.isLoaded = startedApiCount === finishedApiCount;
  }



  haveHeaderWhite(){
    return (
      this.router.url === '/manage-tournament/update-score' ||
      this.router.url === '/manage-tournament/scoring-success');
  }
  noHeading() {
    return (
      this.router.url === '/manage-tournament/update-score' ||
      this.router.url === '/manage-tournament/scoring-success');
  }

  isUpdateScoreRoute() {
    return (
      this.router.url === '/manage-tournament/update-score');
  }

  isScoringSuccessRoute() {
    return (
      this.router.url === '/manage-tournament/scoring-success');
  }

  isScoringRoute() {
    return (
      this.router.url === '/manage-tournament/scoring');
  }
  isShowPopup() {
    this.showPopup = true;
  }
  HiddenPopup() {
    this.showPopup = false;
  }

  checkInvite() {
    if (this.router.url == "/manage-tournament/participants-requests") {
      return this.actionName = "Participant requests"
    } else if (this.router.url == "/manage-tournament/brackets") {
      return this.actionName = "Brackets"
    } else if (this.router.url == "/manage-tournament/scoring") {
      return this.actionName = "Scoring"
    } else if (this.router.url == "/manage-tournament/reports") {
      return this.actionName = "User reports"
    } else if (this.router.url == "/manage-tournament/discussion") {
      return this.actionName = "Discussion"
    } else if (this.router.url == "/manage-tournament/clips") {
      return this.actionName = "Clips"
    } else if (this.router.url == "/manage-tournament/notification") {
      return this.actionName = "Notifications"
    } else {
      return this.actionName = "Participant requests"
    }
  }

  fetchParticipantRegisterationStatus = async (id) => {
    try {
      const { data } =
        await this.esportsTournamentService.fetchParticipantRegistrationStatus(
          API,
          id
        );
      this.btnType = data?.type;
      if (data?.isRegistrationClosed !== undefined) {
        this.isRegistrationClosed = data?.isRegistrationClosed;
      }
      this.registrationEndedTxt = data?.regEnded;
      this.participantId = data?.participantId;
      this.remainingTime = data?.remainingTimeForRegStart / 1000;
      this.remainingTimeForTournamentStart =
        data?.remainingTimeForTournamentStart / 1000 || 0;
      this.showRegTime = this.remainingTime > 0 ? true : false;
      this.showTournamentTime =
        this.remainingTimeForTournamentStart > 0 ? true : false;
      if (this.remainingTime < 0) {
        this.showRegTime = false;
      }
      this.remainigCheckInTime = data?.remainingCheckInTime / 1000 || 0;
      if (this.btnType == 'tournament-started' && this.participantId) {
        // this.isAuthorizedUser.emit(true);
      }
    } catch (error) {
      this.eSportsToastService.showError(
        error?.error?.message || error?.message
      );
    }
  };

  setRegistrationOpenStatus() {
    const now = new Date().getTime();
    const tournamentStartTime = new Date(this.tournamentDetails?.startDate).getTime();
    const regStartTime = new Date(this.tournamentDetails?.regStartDate).getTime();
    const regEndTime = new Date(this.tournamentDetails?.regEndDate).getTime();
    if(this.tournamentDetails?.isRegistrationClosed) {
      this.registrationStatus.isClosed = true;
      return;
    }
    if(this.tournamentDetails.isSeeded || now > tournamentStartTime){
      this.registrationStatus.isClosed = true;
      return;
    }

    if (this.tournamentDetails['isRegStartDate'] && now > regEndTime) {
      this.registrationStatus.isClosed = true;
      return;
    }

    if(this.tournamentDetails['isRegStartDate'] && regStartTime > now) {
      this.registrationStatus.notYetStarted = true;
      return;
    }
    this.registrationStatus.isOpen = true;


  }

  getSetEditInfo(){
    let selectedPlatformIndex = 0;
    for (let data1 of this.tournamentDetails?.gameDetail?.platform) {
      if (data1?._id == this.tournamentDetails?.platform) {
        this.editInfoVo.selectedPlatformIndex = selectedPlatformIndex;
      }
      selectedPlatformIndex++;
    }

    let gameData={
      _id:this.tournamentDetails?.gameDetail?._id,
    }

    this.editInfoVo.tournamentName = this.tournamentDetails?.name;
    this.editInfoVo.visibility = this.tournamentDetails?.visibility;
    this.editInfoVo.tournamentType = {};
     this.editInfoVo.tournamentType.type =this.tournamentDetails?.participantType
    this.editInfoVo.tournamentNameTextarea = this.tournamentDetails?.description
  }



  editInfo(){
    const dialogRef = this.matDialog.open(EsportsEditPopupComponent, {
      data: this.editInfoVo,
    });

    dialogRef.afterClosed().subscribe((newData) => {
     if(newData){
      this.submit({
        description: newData.tournamentNameTextarea,
        name: newData.tournamentName,
        visibility: newData.visibility,
      })
     }
    });
  }

  submit(newData) {
    this.esportsTournamentService
      .updateTournament2(newData, this.tournamentDetails._id)
      .subscribe(
        (response) => {
          this.eSportsToastService.showSuccess(response.message);
        },
        (err) => {
          this.eSportsToastService.showError(err.message);

        }
      );
  }


}
