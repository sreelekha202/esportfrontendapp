import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { toggleOpacity, VisibilityState } from '../../../animations';
import { Subscription } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import {
  EsportsUserService,
  GlobalUtils,
  EsportsCommentService,
  EsportsLeaderboardService,
  EsportsGtmService,
  EsportsToastService,
  EsportsChatService,
  SuperProperties,
  EventProperties,
  EsportsTournamentService,
} from 'esports';
import {
  InfoPopupComponentData,
  InfoPopupComponentType,
} from '../../../shared/popups/info-popup/info-popup.component';
import { environment } from '../../../../environments/environment';
const API = environment.apiEndPoint;
const TOKEN = environment.currentToken;
@Component({
  selector: 'app-discussion',
  templateUrl: './discussion.component.html',
  styleUrls: ['./discussion.component.scss'],
  animations: [toggleOpacity],
})
export class DiscussionComponent implements OnInit {
  VisibilityState = VisibilityState;
  collapseOrExpand: VisibilityState = VisibilityState.Visible;
  @Input() isReplying: boolean = false;
  @Input() placeholder: string = '';
  @Input() creatorId;
  comments = [];
  followStatus: any = 'follow';
  allowDelete: boolean = false;
  currentUser: any;
  currentUserId: any;
  userSubscription: Subscription;
  matchdetails: any;
  typeofchat = "user"
  windowposition: String = 'chat_window chat_window_right_drawer';
  isLoadingNextComments: boolean = false;
  hasNextComment: boolean = true;
  tournamentDetails: any;
  showLoader

  constructor(
    private userService: EsportsUserService,
    private commentService: EsportsCommentService,
    private leaderboardService: EsportsLeaderboardService,
    private gtmService: EsportsGtmService,
    private chatService: EsportsChatService,
    private toastService: EsportsToastService,
    private translateService: TranslateService,
    private esportsTournamentService: EsportsTournamentService,
  ) {}

  ngOnInit(): void {
    if (GlobalUtils.isBrowser()) {
      this.esportsTournamentService.manageTournamentSubject.subscribe(res=>{
        if(res){
          this.tournamentDetails=res
          this.getComment();
        }
      })
      this.userSubscription = this.userService.currentUser.subscribe((data) => {
        if (data) {
          this.currentUser = data;
        }
      });
    }
  }

  getComment() {
    this.showLoader=true
    this.commentService
      .getAllComment1('tournament', this.tournamentDetails?._id, 1)
      .subscribe((data) => {
        if(data){
          this.comments = data?.data;
          this.showLoader=false
        }
      });
  }

  onSaveCommentt(msg) {
    let data = {
      comment: msg,
      objectId: this.tournamentDetails?._id,
      objectType: 'tournament',
    };
    this.commentService.saveComment(data).subscribe((res) => {
      this.getComment();
    });
  }

  getCurrentUserDetails() {
    this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.currentUser = data;
        this.currentUserId = data?._id
        this.checkFollowStatus()
        if (data?.accountType == 'admin') {
          this.allowDelete = true;
        }
        if (data?._id == this.creatorId) {
          this.allowDelete = true;
        }
      }
    });
  }

  followUser() {
    if (this.currentUser) {
      if (this.followStatus == 'follow') {
        this.leaderboardService.followUser(API, this.currentUser?._id).subscribe((res: any) => {
          this.followStatus = 'unfollow';
          this.checkFollowStatus();
          this.toastService.showSuccess(res?.message)
          this.userService.refreshCurrentUser(API, TOKEN);
        },
          (err) => {
            //  this.showLoader = false;
          }
        );
      } else if (this.followStatus == 'unfollow') {
        this.leaderboardService.unfollowUser(API, this.currentUser?._id).subscribe(
          (res: any) => {
            this.followStatus = 'follow';
            this.checkFollowStatus();
            this.toastService.showSuccess(res?.message)
            this.userService.refreshCurrentUser(API, TOKEN);
          },
          (res: any) => {
            //  this.showLoader = false;
          }
        );
      }
    } else {

      // Navigate to the login page with extras
      //this.router.navigate(['/user/email-login']);
    }
  }

  checkFollowStatus() {
    this.leaderboardService.checkFollowStatus(API, this.currentUserId).subscribe(
      (res: any) => {
        if (res.data[0].status == 1) {
          this.followStatus = 'unfollow';
        } else if (res.data[0].status == 0) {
          this.followStatus = 'follow';
        }
      },
      (err) => {
        //this.showLoader = false;
      }
    );
  }

  pushGTMTags(eventName: string, eventProps = false) {
    let superProperties: SuperProperties = {};
    if (this.currentUser) {
      superProperties = this.gtmService.assignLoggedInUsedData(this.currentUser);
    }
    let eventProperties: EventProperties = {};
    if (eventProps) {
      eventProperties.fromPage = 'Chats';
    }
    this.gtmService.gtmEventWithSuperProp({
      eventName: eventName,
      superProps: superProperties,
      eventProps: eventProperties,
    });
  }

  public toggleChat(chatwindow) {
    this.pushGTMTags('Chat_Opened');
    this.matchdetails = chatwindow._id + '-' + this.currentUserId;
    // this.showChat = true;
    this.chatService.setWindowPos(this.windowposition);
    this.chatService.setCurrentMatch(this.matchdetails);
    this.chatService.setTypeOfChat(this.typeofchat);
    this.chatService.setChatStatus(true);
    this.chatService.setTextButtonStatus(true);
  }

  deleteCommentOrReply = async (id, i, j = -1) => {
    try {
      const data: InfoPopupComponentData = {
        title: this.translateService.instant('DISCUSSION.DELETE_COMMENT_TITLE'),
        text: this.translateService.instant('DISCUSSION.DELETE_COMMENT_TEXT'),
        type: InfoPopupComponentType.confirm,
        btnText: this.translateService.instant('BUTTON.CONFIRM'),
      };

      if (true) {
        const deleteComment = await this.commentService.deleteComment(id);
        if (deleteComment.data) {
          if (j > -1) {
            this.comments[i].replies.splice(j, 1);
            this.comments[i].totalReplies = this.comments[i].totalReplies - 1;
          } else {
            this.comments.splice(i, 1);
          }
          this.toastService.showSuccess(deleteComment?.message);
        }
      }
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

}
