import { CoreModule } from './../../core/core.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { MatTreeModule } from '@angular/material/tree';
import { CdkAccordionModule } from '@angular/cdk/accordion';
import { RouterBackModule } from './../../shared/directives/router-back.module';
import { HeaderDynastyModule } from "../../shared/components/header-dynasty/header-dynasty.module";
import { ManageTournamensComponent } from './manage-tournamens.component';
import { ScoringComponent } from './scoring/scoring.component';
import { AddNotificationComponent } from './add-notification/add-notification.component';
import { NotificationComponent } from './notification/notification.component';
import { AddClipsComponent } from './add-clips/add-clips.component';
import { ClipsComponent } from './clips/clips.component';
import { BookmarkCardComponent } from './clips/components/bookmark-card/bookmark-card.component';
import { InputtSearchComponent } from './notification/components/input-search/inputt-search.component';
import { EditScoringComponent } from './edit-scoring/edit-scoring.component';
import { ManageScoreComponent } from './manage-score/manage-score.component';
import { TeamCardComponent } from './edit-scoring/components/team-card/team-card.component';
import { TeamBannerComponent } from './manage-score/components/team-banner/team-banner.component';
import { PopupCreatedComponent } from './manage-score/components/popup-created/popup-created.component';
import { ParticipantsRequestsComponent } from './participants-requests/participants-requests.component'
import { DiscussionComponent } from './discussion/discussion.component';
import { UserReportsComponent } from './user-reports/user-reports.component';
import { IsearchComponent } from './user-reports/component/isearch.component';
import { TeamscardComponent } from './participants-requests/teamscard/teamscard/teamscard.component';
import { DiscussionCommentsComponent } from './discussion/discussion-comments/discussion-comments/discussion-comments.component';
import { HeaderComponent } from './header/header.component';
import { BracketsComponent } from './brackets/brackets.component';
import { ParticipantCardComponent } from './brackets/participant-card/participant-card.component';
import { SharedModule } from '../../shared/modules/shared.module';
import { EditManageTournamentComponent } from './edit-manage-tournament/edit-manage-tournament.component';
import { UpdateScoreComponent } from './scoring//update-score/update-score.component';
import { ScoringSuccessComponent } from './scoring/scoring-success/scoring-success.component';
import { SingleItemComponent } from './user-reports/single-item/single-item.component';
import { HeaderDynastyWhiteComponent } from './header-dynasty-white/header-dynasty-white.component';
import { MatChipsModule } from '@angular/material/chips';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { PopupBracketsComponent } from './brackets/popup/popup.component';
import { EsportsCustomPaginationModule, EsportsLoaderModule } from 'esports'

export const routes: Routes = [
  {
    path: '',
    component: ManageTournamensComponent,
    children: [
      {
        path: '',
        redirectTo: 'participants-requests',
        pathMatch: 'full',
      },
      {
        path: 'scoring',
        component: ScoringComponent,
      },
      {
        path: 'clips',
        component: ClipsComponent,
      },
      {
        path: 'add-clips',
        component: AddClipsComponent,
      },
      {
        path: 'notification',
        component: NotificationComponent,
      },
      {
        path: 'add-notification',
        component: AddNotificationComponent,
      },
      {
        path: 'edit-score',
        component: EditScoringComponent,
      },
      {
        path: 'manage-score',
        component: ManageScoreComponent,
      },
      {
        path: 'participants-requests',
        component: ParticipantsRequestsComponent,
      },
      {
        path: 'participants-requests-blank',
        component: ParticipantsRequestsComponent,
      },
      {
        path: 'discussion',
        component: DiscussionComponent,
      },
      {
        path: 'reports',
        component: UserReportsComponent,
      },
      {
        path: 'brackets',
        component: BracketsComponent,
      },
      {
        path: 'brackets/generated',
        component: BracketsComponent,
      },
      {
        path: 'update-score',
        component: UpdateScoreComponent,
      },
      {
        path: 'scoring-success',
        component: ScoringSuccessComponent,
      },
      {
        path: 'reports/single-item',
        component: SingleItemComponent,
      },
      {
        path: 'reports/:tournamentId',
        component: UserReportsComponent,
      },
    ],

  }
]

@NgModule({
  declarations: [
    ManageTournamensComponent,
    ScoringComponent,
    AddNotificationComponent,
    NotificationComponent,
    AddClipsComponent,
    ClipsComponent,
    BookmarkCardComponent,
    InputtSearchComponent,
    EditScoringComponent,
    ManageScoreComponent,
    TeamCardComponent,
    TeamBannerComponent,
    PopupCreatedComponent,
    UserReportsComponent,
    DiscussionComponent,
    ParticipantsRequestsComponent,
    IsearchComponent,
    TeamscardComponent,
    DiscussionCommentsComponent,
    HeaderComponent,
    BracketsComponent,
    ParticipantCardComponent,
    EditManageTournamentComponent,
    UpdateScoreComponent,
    HeaderDynastyWhiteComponent,
    SingleItemComponent,
    ScoringSuccessComponent,
    PopupBracketsComponent,

  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule,
    HeaderDynastyModule,
    CoreModule,
    RouterBackModule,
    MatTreeModule,
    CdkAccordionModule,
    MatChipsModule,
    NgxMaterialTimepickerModule,
    EsportsCustomPaginationModule,
    EsportsLoaderModule.setColor('#1d252d')
  ]
})
export class ManageTournamensModule { }
