import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'app-popup-brackets',
  templateUrl: './popup.component.html',
  styleUrls: ['./popup.component.scss']
})
export class PopupBracketsComponent implements OnInit {

  @Input() tournament;
  @Input() allApprovedParticipants;
  @Output() hidePopup = new EventEmitter();
  @Output() confirmBracketParent = new EventEmitter();
  constructor() { }

  ngOnInit(): void {
  }
  confirmBracket (){
    this.confirmBracketParent.emit();
    this.hidePopup.emit();
  }
}