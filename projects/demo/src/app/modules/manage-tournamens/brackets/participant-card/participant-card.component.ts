import { Component, OnInit, Input, Output, EventEmitter, OnChanges,SimpleChanges } from '@angular/core';
import { Subscription } from 'rxjs';
import { EsportsChatService, EsportsGtmService, EsportsLeaderboardService, EsportsToastService, 
  EsportsTournamentService, EsportsUserService, EventProperties, SuperProperties } from 'esports';
import { TranslateService } from '@ngx-translate/core';
import { environment } from '../../../../../environments/environment';
const API = environment.apiEndPoint;
const TOKEN = environment.currentToken;

@Component({
  selector: 'app-participant-card',
  templateUrl: './participant-card.component.html',
  styleUrls: ['./participant-card.component.scss']
})
export class ParticipantCardComponent implements OnInit, OnChanges {
  @Input() isManage: boolean = false;
  @Input() dataList;
  @Input() allApprovedParticipants_length;
  @Input() tournament;
  @Input() creatorId;
  @Output() manualSeedValidation = new EventEmitter();
  @Output() refresh= new EventEmitter();
  
  followStatus: any = 'follow';
  allowDelete: boolean = false;
  currentUser: any;
  currentUserId: any;
  userSubscription: Subscription;
  matchdetails: any;
  typeofchat = "user"
  windowposition: String = 'chat_window chat_window_right_drawer';
  isLoadingNextComments: boolean = false;
  hasNextComment: boolean = true;
  tournamentDetails: any;
  
  @Input() shuffe : boolean = false;
  @Input() filter : boolean = false;
  constructor(
    private tournamentService: EsportsTournamentService,
    private eSportsToastService: EsportsToastService,private userService: EsportsUserService,
    private leaderboardService: EsportsLeaderboardService,
    private gtmService: EsportsGtmService,
    private chatService: EsportsChatService,
    private toastService: EsportsToastService,
    private translateService: TranslateService
  ) { }

  ngOnInit(): void {
    this.getCurrentUserDetails();
    
  }

  manualSeedValidationn(index, _id, value){
    this.manualSeedValidation.emit(
      {
        index:index,
         _id:_id,
         value:value
      }
    )
  }

  reject(d){
    const data = {
      participantStatus: 'rejected',
    }
    this.tournamentService.updateParticipant(API,d?._id, data).subscribe(res => {
      if (res?.success) {
        this.refresh.emit(d?._id)
        this.eSportsToastService.showSuccess(res?.message);
      } else {
        this.eSportsToastService.showInfo(res?.message);
      }
    })
  }

  getCurrentUserDetails() {
    this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.currentUser = data;
        this.currentUserId = data?._id
        this.checkFollowStatus()
        if (data.accountType == 'admin') {
          this.allowDelete = true;
        }
        if (data._id == this.creatorId) {
          this.allowDelete = true;
        }
      }
    });
  }

  followUser() {
    if (this.currentUser) {
      if (this.followStatus == 'follow') {
        this.leaderboardService.followUser(API, this.currentUser?._id).subscribe((res: any) => {
          this.checkFollowStatus();
          this.toastService.showSuccess(res?.message)
        },
          (err) => {
            //  this.showLoader = false;
          }
        );
      } 
      if (this.followStatus == 'unfollow') {
        this.leaderboardService.unfollowUser(API, this.currentUser?._id).subscribe((res: any) => {
            this.checkFollowStatus();
            this.toastService.showSuccess(res?.message)
          },
          (res: any) => {
            //  this.showLoader = false;
          }
        );
      }
    } else {

      // Navigate to the login page with extras
      //this.router.navigate(['/user/email-login']);
    }
  }



  checkFollowStatus() {
    this.leaderboardService.checkFollowStatus(API, this.currentUserId).subscribe((res: any) => {
        if (res?.data[0]?.status == 1) {
          this.followStatus = 'unfollow';
        } else if (res?.data[0]?.status == 0) {
          this.followStatus = 'follow';
        }
      },
      (err) => {
        //this.showLoader = false;
      }
    );
  }

  pushGTMTags(eventName: string, eventProps = false) {
    let superProperties: SuperProperties = {};
    if (this.currentUser) {
      superProperties = this.gtmService.assignLoggedInUsedData(this.currentUser);
    }
    let eventProperties: EventProperties = {};
    if (eventProps) {
      eventProperties.fromPage = 'Chats';
    }
    this.gtmService.gtmEventWithSuperProp({
      eventName: eventName,
      superProps: superProperties,
      eventProps: eventProperties,
    });
  }

  public toggleChat(chatwindow) {
    this.pushGTMTags('Chat_Opened');
    this.matchdetails = chatwindow._id + '-' + this.currentUserId;
    // this.showChat = true;
    this.chatService.setWindowPos(this.windowposition);
    this.chatService.setCurrentMatch(this.matchdetails);
    this.chatService.setTypeOfChat(this.typeofchat);
    this.chatService.setChatStatus(true);
    this.chatService.setTextButtonStatus(true);
  }

  remove(data){

  }

  shuffeData(){
    const newArr = this.dataList.sort(() => Math.random() - 0.5)
    this.dataList = newArr
  }
  filterData(){
    if(this.filter === false)
      return;
    const newData =  this.dataList.filter(item => item.join == true)
    this.dataList = newData
  }
  ngOnChanges(changes: SimpleChanges){
    this.filterData()
    if(!changes?.shuffe?.firstChange){
      return this.shuffeData()
    }
  }

}
