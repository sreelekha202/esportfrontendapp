import { HttpParams } from '@angular/common/http';
import { Component, OnInit, Input } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import {
  IPagination,
  EsportsLanguageService,
  EsportsToastService,
  GlobalUtils,
  EsportsTournamentService,
  EsportsBracketService,
  EsportsUtilsService,
  IUser,
  EsportsChatService,
} from 'esports';
import { Router } from '@angular/router';
import { environment } from '../../../../environments/environment';

const API = environment.apiEndPoint;
@Component({
  selector: 'app-brackets',
  templateUrl: './brackets.component.html',
  styleUrls: ['./brackets.component.scss'],
})
export class BracketsComponent implements OnInit {
  Step7;
  nextId: number = 1;
  active = 1;
  @Input() isManage: boolean = false;
  @Input() data;
  currentUser: any;
  structure: any;
  isLoading = false;

  arebicText = false;
  isSeeded = false;
  isDisableCreateBracket: boolean = false;
  allApprovedParticipants: Array<any> = [];
  allSelectedParticipants: Array<any> = [];
  participantList: Array<{}> = [];
  activePage = 1;
  pageSize = 10;
  seedingMethodList = [
    { name: 'SEED.BY_REGISTRATION', value: 'regOrder' },
    { name: 'SEED.RANDOM', value: 'random' },
  ];
  filterList = [
    { name: 'SEED.ALL_PLAYERS', value: 'all-players' },
    // { name: 'Not Previously Seeded', value: 'not-previously-seeded' },
  ];
  itemPerPageList = [5, 10, 15, 20, 25];
  selectFilterType = null;
  selectedSeedingMethod = null;
  isChangeInSeedingOrder = false;
  apiLoaded: Array<boolean> = [];
  isLoaded = false;
  page: IPagination;
  searchKey: string | null = '';
  tournament: any;
  hasRequiredAccess: boolean = false;
  startDateIntervalId = null;
  endDateIntervalId = null;
  tournamentStartTime = '';
  user: IUser;
  noStart: boolean = false;
  hideMessage: boolean = true;
  isBracketGenerated: boolean = false;
  tReport: any;
  show = false;
  shuffle: boolean = false;
  filter: boolean = false;
  constructor(
    private tournamentService: EsportsTournamentService,
    private bracketService: EsportsBracketService,
    private eSportsToastService: EsportsToastService,
    private translateService: TranslateService,
    public language: EsportsLanguageService,
    private router: Router,
    private utilsService: EsportsUtilsService,
    private eSportsChatService: EsportsChatService,
    private esportsTournamentService: EsportsTournamentService
  ) {}

  ngOnInit(): void {
    this.tournamens();
  }

  tournamens() {
    this.isLoaded = true;
    this.esportsTournamentService.manageTournament.subscribe((data) => {
      if (data) {
        this.tournament = data;
        this.isLoaded = false;

        this.getParticipantBracket();
        if (this.tournament?.isCheckInRequired) {
          this.filterList[1] = {
            name: 'SEED.CHECK_IN',
            value: 'checked-in-only',
          };
        }
        if (this.tournament?.isSeeded) {
          this.fetchBracket();
        } else {
          this.fetchAllApprovedParticipants();
        }
      }
    });
  }

  /** Get all approved participants of the tournament  */
  fetchAllApprovedParticipants = async () => {
    try {
      this.apiLoaded.push(false);
      let params = new HttpParams();
      params = params.append(
        'query',
        JSON.stringify({
          tournamentId: this.tournament?._id,
          participantStatus: 'approved',
        })
      );
      params = params.set(
        'select',
        'checkedIn,teamName,seed,participantType,teamMembers,matchWin,name'
      );
      const participants = await this.tournamentService
        .getParticipants(API, params)
        .toPromise();
      this.allApprovedParticipants = participants.data.map((obj, index) => {
        const record = obj;
        record.reg = index + 1;
        return record;
      });
      this.allSelectedParticipants = this.allApprovedParticipants.filter(
        (item: any) => {
          if (
            !this.selectFilterType ||
            this.selectFilterType?.value !== 'checked-in-only'
          ) {
            return true;
          }
          return item.checkedIn;
        }
      );
      // this.initializeTable();
      this.apiLoaded.push(true);
    } catch (error) {
      this.apiLoaded.push(true);
      // this.eSportsToastService.showError(error.message);
    } finally {
      this.checkAllApiDataLoaded();
    }
  };

  /** Get all saved matches of the tournamnet */
  fetchAllMatches = async () => {
    if (!this.tournament?.isSeeded) {
      this.generateBracket();
    } else {
      this.active = 2;
      try {
        this.apiLoaded.push(false);
        const queryParam = `?query=${encodeURIComponent(
          JSON.stringify({ tournamentId: this.tournament?._id })
        )}&select=bye,currentMatch,matchStatus,teamA,teamB,disQualifiedTeam,placeHolderA,placeHolderB,teamAWinSet,teamBWinSet,isThirdPlaceMatch,matchNo,battleRoyalTeams.noOfKill,battleRoyalTeams.placement,battleRoyalTeams.score,battleRoyalTeams._id,battleRoyalScoringOption,externalMatchId,isScoreConflict&option=${encodeURIComponent(
          JSON.stringify({ sort: { matchNo: 1 } })
        )}`;
        const response = await this.bracketService.fetchAllMatches(queryParam);
        this.structure = this.bracketService.assembleStructure(response.data);
        this.isLoaded = true;
        this.isSeeded = true;
        this.isBracketGenerated = true;

        this.apiLoaded.push(true);
      } catch (error) {
        this.apiLoaded.push(true);
        this.eSportsToastService.showError(error.message);
      } finally {
        this.checkAllApiDataLoaded();
      }
    }
  };

  shuffleData() {
    this.shuffle = !this.shuffle;
  }
  filterData() {
    this.filter = true;
  }

  navChanges = (e) => {
    this.nextId = e.nextId;
    switch (this.nextId) {
      case 1:
        this.getParticipantBracket();
        break;
    }
  };

  getParticipantBracket() {
    this.isLoaded = false;
    let params = new HttpParams();
    params = params.append(
      'query',
      JSON.stringify({
        tournamentId: this.tournament._id,
        participantStatus: 'approved',
      })
    );
    params = params.set(
      'select',
      'checkedIn,teamName,seed,participantType,teamMembers,matchWin,name'
    );
    this.tournamentService.getParticipantBracket(params).subscribe((res) => {
      if (res) {
        this.allApprovedParticipants = res?.data;
        this.allSelectedParticipants = this.allApprovedParticipants;
        this.isLoaded = true;
      }
    });
  }

  seedRandomly() {
    const tempArray = [];
    this.isChangeInSeedingOrder = true;
    const availableParticipant = this.allApprovedParticipants.filter(
      (item: any) => {
        if (
          (!this.selectFilterType ||
            this.selectFilterType?.value !== 'checked-in-only') &&
          !this.tournament?.isCheckInRequired
        ) {
          return true;
        }
        return item.checkedIn;
      }
    );
    while (tempArray.length < availableParticipant.length) {
      const seed = Math.floor(Math.random() * availableParticipant.length) + 1;
      if (tempArray.indexOf(seed) === -1) {
        tempArray.push(seed);
      }
    }
    availableParticipant.forEach((participant: any, index) => {
      const mainIndex = this.allApprovedParticipants.findIndex(
        (item: any) => item._id === participant._id
      );
      const subIndex = this.allSelectedParticipants.findIndex(
        (item: any) => item._id === participant._id
      );
      if (mainIndex >= 0) {
        this.allApprovedParticipants[mainIndex].seed = tempArray[index];
      }
      if (subIndex >= 0) {
        this.allSelectedParticipants[subIndex].seed = tempArray[index];
      }
    });
  }

  /** Save seeding value of participants */
  saveAllSeeds = async (isShowMessage = true) => {
    try {
      const { isValid, reason } = this.isDisableSaveAll();
      if (!isValid) {
        isShowMessage ? this.eSportsToastService.showError(reason) : null;
        return { isValid: false, reason };
      }
      const data = this.allApprovedParticipants
        .filter((item: any) => {
          if (
            !this.selectFilterType ||
            this.selectFilterType?.value !== 'checked-in-only'
          ) {
            return true;
          }
          return item.checkedIn;
        })
        .map((item) => {
          return {
            _id: item._id,
            seed: item.seed,
          };
        });
      const response = await this.tournamentService
        .updateParticipantBulk(API, { data })
        .toPromise();
      this.isChangeInSeedingOrder = false;
      this.eSportsToastService.showSuccess(response.message);
      return { isValid: true };
    } catch (error) {
      this.eSportsToastService.showError(
        error?.error?.message || error?.message
      );
      return {
        isValid: false,
        reason: error?.error?.message || error?.message,
      };
    }
  };

  /** Validate seeding value */
  isDisableSaveAll() {
    const teamSet = new Set();
    let isInvalid = false;
    let reason = '';
    isInvalid = this.allApprovedParticipants
      .filter((item: any) => {
        if (
          !this.selectFilterType ||
          this.selectFilterType?.value !== 'checked-in-only'
        ) {
          return true;
        }
        return item.checkedIn;
      })
      .some((obj: any, index, array) => {
        if (!obj.seed || obj.seed < 1 || obj.seed > array.length) {
          if (this.tournament?.isCheckInRequired && !obj.checkedIn)
            return false;

          reason = this.translateService.instant(
            'MANAGE_TOURNAMENT.SEED_BRACKET.INCOMPLETE_SEEDING'
          );
          return true;
        } else if (teamSet.size === teamSet.add(obj.seed).size) {
          reason = this.translateService.instant(
            'MANAGE_TOURNAMENT.SEED_BRACKET.DUPLICATE_SEED_ID'
          );
          return true;
        }
      });
    return { isValid: !isInvalid, reason };
  }

  /** Generate mock bracket structure */
  generateBracket = async () => {
    this.active = 2;
    try {
      if (this.tournament?.bracketType == 'swiss_safeis') {
        // await this.modalService.open(this.bracketConfirmRef, {
        //   windowClass: 'confirm-modal-content',
        //   centered: true,
        // });
      } else {
        const data: any = this.isChangeInSeedingOrder
          ? await this.saveAllSeeds(false)
          : this.isDisableSaveAll();

        if (!data.isValid) {
          throw new Error(data.reason);
        }

        const payload = {
          tournamentId: this.tournament?._id,
          ...(this.selectFilterType?.value === 'checked-in-only' && {
            checkedIn: true,
          }),
        };
        const response = await this.bracketService.generateBracket(payload);
        this.structure = response.data;
        this.isLoaded = true;
        this.isSeeded = true;
        this.isBracketGenerated = true;
      }
    } catch (error) {
      this.eSportsToastService.showError(
        error?.error?.error || error?.error?.message || error.message
      );
      this.isLoaded = true;
    }
  };

  manualSeedValidation(data) {
    let index = data.index;
    let _id = data._id;
    let value = data.value;
    const tempArrayLength = this.allApprovedParticipants.filter((item: any) => {
      if (
        !this.selectFilterType ||
        this.selectFilterType?.value !== 'checked-in-only'
      ) {
        return true;
      }
      return item.checkedIn;
    }).length;
    const mainIndex = this.allApprovedParticipants.findIndex(
      (item: any) => item._id === _id
    );
    this.allApprovedParticipants[mainIndex].seed = value && Number(value);
    if (value) {
      this.isChangeInSeedingOrder = true;
      if (value < 1) {
        this.allSelectedParticipants[index].seed = 1;
        this.allApprovedParticipants[mainIndex].seed = 1;
      } else if (value > tempArrayLength) {
        this.allSelectedParticipants[index].seed = tempArrayLength;
        this.allApprovedParticipants[mainIndex].seed = tempArrayLength;
      }
    }
  }

  /** Confirm mock structure and save it in data base */
  confirmBracket = async () => {
    try {
      const payload = {
        tournamentId: this.tournament?._id,
        ...(this.selectFilterType?.value === 'checked-in-only' && {
          checkedIn: true,
        }),
      };
      const response = await this.bracketService.saveGeneratedBracket(payload);
      if (response) {
        this.fetchTournamentDetails();
        this.eSportsToastService.showSuccess(response.message);
        this.tournamens();
      }
    } catch (error) {
      this.eSportsToastService.showError(error.message);
    }
    this.active = 1;
  };

  /**
   * Fetch All Match If Seed Else Fetch Mock Structure Of The Tournament
   */
  fetchBracket = async () => {
    try {
      this.apiLoaded.push(false);
      if (
        this.tournament?.isSeeded &&
        ['single', 'double'].includes(this.tournament?.bracketType)
      ) {
        const queryParam = `?query=${encodeURIComponent(
          JSON.stringify({ tournamentId: this.tournament._id })
        )}&option=${encodeURIComponent(
          JSON.stringify({ sort: { matchNo: 1 } })
        )}&select=bye,currentMatch,matchStatus,teamA,teamB,disQualifiedTeam,placeHolderA,placeHolderB,teamAWinSet,teamBWinSet,isThirdPlaceMatch,matchNo,isScoreConflict`;
        const response = await this.bracketService.fetchAllMatches(queryParam);
        this.structure = this.bracketService.assembleStructure(response.data);
      } else if (
        !this.tournament?.isSeeded &&
        ['single', 'double', 'round_robin', 'battle_royale'].includes(
          this.tournament?.bracketType
        )
      ) {
        const payload = {
          bracketType: this.tournament?.bracketType,
          maximumParticipants: this.tournament?.maxParticipants,
          noOfSet: this.tournament?.noOfSet,
          ...(['round_robin', 'battle_royale'].includes(
            this.tournament?.bracketType
          ) && {
            noOfTeamInGroup: this.tournament?.noOfTeamInGroup,
            noOfWinningTeamInGroup: this.tournament?.noOfWinningTeamInGroup,
            noOfRoundPerGroup: this.tournament?.noOfRoundPerGroup,
            stageBracketType: this.tournament?.stageBracketType,
          }),
        };
        const response = await this.bracketService.generateBracket(payload);
        this.structure = response.data;
        this.isLoaded = true;
      }
      this.apiLoaded.push(true);
      this.isBracketGenerated = true;
    } catch (error) {
      this.isLoaded = true;
      this.apiLoaded.push(true);
      this.eSportsToastService.showError(
        error?.error?.message || error?.message
      );
    } finally {
      this.isLoaded = true;
      // this.checkAllApiDataLoaded();
    }
  };

  getComponentResponse(data) {
    let isRefresh = false;
    if (typeof data === 'object') {
      if (data.active) {
        this.active = data.active;
      }
      isRefresh = data?.refresh || data?.isRefresh;
    } else {
      isRefresh = data;
    }

    if (isRefresh) {
      this.fetchTournamentDetails();
      clearInterval(this.startDateIntervalId);
      this.startDateIntervalId = null;
      if (data.type) {
        this.eSportsChatService.emitTournamentEvents(
          this.tournament?.slug,
          this.user?._id,
          data?.type
        );
      }
    }
  }

  /**
   * Start date Timer
   */
  startDateTimer = () => {
    const startDate = new Date(this.tournament.startDate);
    const currentDate = new Date();
    if (startDate.getTime() - currentDate.getTime() > 0) {
      const millisecond = startDate.getTime() - currentDate.getTime();
      const dd = (millisecond / (1000 * 60 * 60 * 24)) | 0;
      if (dd) {
        const currentLanguage = this.translateService.currentLang;

        if (currentLanguage == 'en') {
          this.tournamentStartTime = `${dd} ${
            dd === 1
              ? this.translateService.instant('MANAGE_TOURNAMENT.DAYS_LEFT.01')
              : this.translateService.instant('MANAGE_TOURNAMENT.DAYS_LEFT.02')
          }`;
        } else {
          if (dd === 1) {
            this.tournamentStartTime = `${dd} ${this.translateService.instant(
              'MANAGE_TOURNAMENT.DAYS_LEFT.01'
            )}`;
          } else if (dd === 2) {
            this.tournamentStartTime = `${this.translateService.instant(
              'MANAGE_TOURNAMENT.DAYS_LEFT.02'
            )}`;
          } else if (dd === 3) {
            this.tournamentStartTime = `${dd} ${this.translateService.instant(
              'MANAGE_TOURNAMENT.DAYS_LEFT.03'
            )}`;
          } else if (dd === 11) {
            this.tournamentStartTime = `${dd} ${this.translateService.instant(
              'MANAGE_TOURNAMENT.DAYS_LEFT.04'
            )}`;
          } else {
            this.tournamentStartTime = `${dd} ${this.translateService.instant(
              'MANAGE_TOURNAMENT.DAYS_LEFT.04'
            )}`;
          }
        }
        return;
      }

      const hh = (millisecond / (1000 * 60 * 60)) | 0;
      const mm = ((millisecond - hh * 1000 * 60 * 60) / (1000 * 60)) | 0;
      const ss = ((millisecond - hh * 1000 * 60 * 60) / 1000) % 60 | 0;
      this.tournamentStartTime = `${hh > 9 ? hh : `0${hh}`}: ${
        mm > 9 ? mm : `0${mm}`
      }: ${ss > 9 ? ss : `0${ss}`}`;

      if (!this.startDateIntervalId) {
        this.startDateIntervalId = setInterval(
          () => this.startDateTimer(),
          1000
        );
      }
    } else {
      if (this.startDateIntervalId) {
        this.fetchTournamentDetails(true);
      } else {
        this.tournamentStartTime = this.translateService.instant(
          'MANAGE_TOURNAMENT.TOURNAMENT_EXPIRED'
        );
      }
      clearInterval(this.startDateIntervalId);
      this.startDateIntervalId = null;
    }
  };

  endDateTimer = () => {
    const endDate = new Date(this.tournament?.endDate);
    const currentDate = new Date();
    const timeDiff = endDate.getTime() - currentDate.getTime();
    if (timeDiff > 0) {
      this.endDateIntervalId = setInterval(() => this.endDateTimer(), 1000);
    } else if (
      ['swiss_safeis', 'ladder'].includes(this.tournament?.bracketType)
    ) {
      if (this.endDateIntervalId) {
        this.fetchTournamentDetails(false, true);
        clearInterval(this.endDateIntervalId);
        this.endDateIntervalId = null;
      }
    }
  };

  /**
   * Fatch Register Or CheckedIn Participant
   * @param field registeredParticipantCount, checkedInParticipantCount
   * @param obj for checkedIn users
   */
  fetchRegisteredParticipant = async (field, obj = {}) => {
    try {
      this.apiLoaded.push(false);
      const encodeUrl = `?query=${encodeURIComponent(
        JSON.stringify({ tournamentId: this.tournament?._id, ...obj })
      )}&select=''&option=${encodeURIComponent(
        JSON.stringify({ count: true })
      )}`;
      const response = await this.tournamentService
        .getParticipants(API, encodeUrl)
        .toPromise();
      this[field] = response?.totals?.count;
      this.apiLoaded.push(true);
    } catch (error) {
      this.apiLoaded.push(true);
      this.eSportsToastService.showError(
        error?.error?.message || error?.message
      );
    } finally {
      // this.checkAllApiDataLoaded();
    }
  };
  /**
   * Fetch TOurnament Details
   */
  fetchTournamentDetails = async (
    isTournamentStart: boolean = false,
    isTournamentFinished: boolean = false
  ) => {
    try {
      this.apiLoaded.push(false);
      const tournament = await this.tournamentService.getTournamentBySlug(
        this.tournament?.slug
      );
      if (tournament) {
        this.tournament = tournament?.data;
        this.esportsTournamentService.manageTournamentSubject.next(
          tournament?.data
        );
      }

      // .toPromise();

      if (!this.tournament) {
        this.hideMessage = false;
        throw new Error(this.translateService.instant('VIEW_TOURNAMENT.ERROR'));
      }

      const isEventOrganizer =
        this.user?._id === this.tournament?.organizerDetail?._id;
      const hasAdminAccess =
        this.user?.accountType == 'admin' &&
        this.user?.accessLevel?.includes('em');

      this.hasRequiredAccess = isEventOrganizer || hasAdminAccess;
      if (!this.hasRequiredAccess) {
        throw new Error(
          this.translateService.instant('VIEW_TOURNAMENT.ACCESS_DENIED')
        );
      }

      if (this.tournament?.isFinished) {
        if (isTournamentFinished) {
          this.eSportsToastService.showInfo(
            this.translateService.instant('ELIMINATION.TOURNAMENT_FINISHED'),
            false
          );
        }
        this.tournamentStartTime = this.translateService.instant(
          'MANAGE_TOURNAMENT.TOURNAMENT_FINISHED'
        );
      } else if (this.tournament?.isSeeded) {
        if (isTournamentStart) {
          this.eSportsToastService.showInfo(
            this.translateService.instant('ELIMINATION.TOURNAMENT_STARTED'),
            false
          );
        }

        this.tournamentStartTime = this.translateService.instant(
          'MANAGE_TOURNAMENT.TOURNAMENT_STARTED'
        );
        this.endDateTimer();
      } else {
        this.startDateTimer();
        this.noStart = true;
      }
      this.fetchRegisteredParticipant('registeredParticipantCount');
      this.fetchRegisteredParticipant('checkedInParticipantCount', {
        checkedIn: true,
      });
      this.apiLoaded.push(true);
    } catch (error) {
      this.hasRequiredAccess = false;
      this.apiLoaded.push(true);
      this.eSportsToastService.showError(
        error?.error?.message || error?.message
      );
    } finally {
      //this.checkAllApiDataLoaded();
    }
  };

  refresh(id) {
    this.allApprovedParticipants.forEach((e, i) => {
      if (e._id == id) {
        this.allApprovedParticipants.splice(i, 1);
      }
    });
  }

  checkAllApiDataLoaded() {
    const startedApiCount = this.apiLoaded.filter((el) => !el).length;
    const finishedApiCount = this.apiLoaded.filter((el) => el).length;
    this.isLoaded = startedApiCount === finishedApiCount;
  }

  showPopup($event) {
    this.show = true;
  }
  hidePopup($event) {
    this.show = false;
  }
  showGenerated() {
    return this.router.url == '/manage-tournament/brackets/generated';
  }
}
