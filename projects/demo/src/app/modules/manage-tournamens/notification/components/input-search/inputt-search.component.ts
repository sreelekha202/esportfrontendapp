import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-inputt-search',
  templateUrl: './inputt-search.component.html',
  styleUrls: ['./inputt-search.component.scss'],
})
export class InputtSearchComponent implements OnInit {
  @Input() placeholder: string = '';
  @Input() text: string = '';
  @Output() onTextChange = new EventEmitter<string>();
  constructor() { }
  ngOnInit(): void { }
  onTextChang = () => this.onTextChange.emit(this.text)
}
