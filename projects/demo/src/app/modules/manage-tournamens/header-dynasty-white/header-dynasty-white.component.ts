import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
@Component({
  selector: 'app-header-dynasty-white',
  templateUrl: './header-dynasty-white.component.html',
  styleUrls: ['./header-dynasty-white.component.scss']
})
export class HeaderDynastyWhiteComponent implements OnInit {

  constructor(private router: Router,
    private _location: Location) { }

  ngOnInit(): void {
  }

  noBtnClose() {
    return (
      this.router.url === '/manage-tournament/scoring-success' ||
      this.router.url === '/manage-tournament/update-score')
  }

  backClicked() {
    this._location.back();
  }

}
