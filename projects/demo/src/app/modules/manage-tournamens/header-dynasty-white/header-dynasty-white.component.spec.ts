import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderDynastyWhiteComponent } from './header-dynasty-white.component';

describe('HeaderDynastyWhiteComponent', () => {
  let component: HeaderDynastyWhiteComponent;
  let fixture: ComponentFixture<HeaderDynastyWhiteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HeaderDynastyWhiteComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderDynastyWhiteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
