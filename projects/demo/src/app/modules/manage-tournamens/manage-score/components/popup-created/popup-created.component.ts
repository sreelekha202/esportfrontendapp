import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialogRef } from '@angular/material/dialog';
import { AppHtmlRoutes } from '../../../../../app-routing.model';

@Component({
  selector: 'app-popup-created',
  templateUrl: './popup-created.component.html',
  styleUrls: ['./popup-created.component.scss'],
})
export class PopupCreatedComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;

  constructor(public dialogRef: MatDialogRef<any>, private router: Router) {}

  ngOnInit(): void {}

  onClose(): void {
    this.dialogRef.close();
  }

  onCloseAndRedirect(): void {
    this.onClose();
    this.router.navigate([AppHtmlRoutes.play]);
  }
}
