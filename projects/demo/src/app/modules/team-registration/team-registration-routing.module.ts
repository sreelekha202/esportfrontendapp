import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { TeamRegistrationComponent } from "./team-registration.component";

import { NewTeamComponent } from "./pages/new-team/new-team.component";
import { SelectTeammatesComponent } from "./pages/select-teammates/select-teammates.component";
import { TournamentRegistrationComponent } from "./pages/tournament-registration/tournament-registration.component";

const routes: Routes = [
  {
    path: "",
    component: TeamRegistrationComponent,
    children: [
      {
        path: "",
        component: NewTeamComponent,
      },
      {
        path: "teammates",
        component: SelectTeammatesComponent,
      },
      {
        path: "registration",
        component: TournamentRegistrationComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TeamRegistrationRoutingModule {}
