import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
@Component({
  selector: "app-tournament-registration",
  templateUrl: "./tournament-registration.component.html",
  styleUrls: ["./tournament-registration.component.scss"],
})
export class TournamentRegistrationComponent implements OnInit {
  form: FormGroup;

  mock_players = [1, 2, 3, 4];

  constructor(private fb: FormBuilder) {}

  ngOnInit(): void {
    this.createTournamentForm();
  }

  createTournamentForm() {
    this.form = this.fb.group({});
  }
}
