import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';
import {
  EsportsUserService,
  EsportsToastService,
  EsportsTournamentService,
  IUser,
} from 'esports';
import { environment } from '../../../environments/environment';
import { AppHtmlRoutes } from '../../app-routing.model';

const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;

@Component({
  selector: 'app-team-registration',
  templateUrl: './team-registration.component.html',
  styleUrls: ['./team-registration.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TeamRegistrationComponent implements OnInit, OnDestroy {
  AppHtmlRoutes = AppHtmlRoutes;
  teamRegistrationActiveStep = 0;
  userSubscription: Subscription;
  user: IUser;
  tournament: any = {};
  teamList: any = [];
  teamSelected: any = {};
  teamMemberList: any = {};
  isLoaded = false;
  participantRS: string | null;
  showParticipantForm = false;
  prizeSum;
  registrationStatus = {
    isOpen: false,
    isClosed: false,
    notYetStarted: false,
  };
  isSeasonTournament = false;
  isMatchMaking = false
  tournamentSlug = '';

  totalPages : number = 4;
  step : number = 3;
  numberStep : number = 4;
  pageTitle : string = 'TOURNAMENT_CARD.JOIN';

  constructor(
    private userService: EsportsUserService,
    private tournamentService: EsportsTournamentService,
    private activatedRoute: ActivatedRoute,
    private translateService: TranslateService,
    private toastService: EsportsToastService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.isSeasonTournament =
      this.activatedRoute.snapshot.queryParams.isSeason == 'true';
    this.isMatchMaking =
      this.activatedRoute.snapshot.queryParams.isMatchmaking == 'true';
    if(this.isMatchMaking){
      this.pageTitle = 'HEADER.MATCHMAKING'
    }
    this.tournamentSlug = this.activatedRoute.snapshot.params.id;
    this.getCurrentUserDetails();
  }

  ngOnDestroy() {
    this.userSubscription?.unsubscribe();
  }

  getCurrentUserDetails() {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.user = data;
        const pId = this.activatedRoute.snapshot.params.pId;
        if (pId && !this.tournamentSlug) {
          this.tournamentSlug = this.router.url.split('/').reverse()[2];
        } else if (!this.tournamentSlug){
          this.tournamentSlug = this.router.url.split('/').reverse()[0];
        }
        this.fetchTournamentDetails(this.tournamentSlug);
      }
    });
  }

  /**
   * Fetch Some tournament Details
   * @param slug
   */
  fetchTournamentDetails = async (slug) => {
    if (this.isSeasonTournament) {
      this.fetchSeasonTournamentDetails(slug);
      return;
    }

    try {
      this.isLoaded = false;
      const tournament = await this.tournamentService.getTournamentBySlug(slug);
      this.tournament = tournament.data;
      this.setRegistrationOpenStatus();
      const totalPrice = this.tournament.prizeList || [];
      this.prizeSum = totalPrice.reduce((acc, el) => acc + el.value, 0);

      if (!this.tournament) {
        throw new Error(
          this.translateService.instant(
            'VIEW_TOURNAMENT.JOIN_PARTICIPANT.INVALID_TOURNAMENT'
          )
        );
      }
      const participantId = this.activatedRoute?.snapshot?.params?.pId;
      const { data } =
        await this.tournamentService.fetchParticipantRegistrationStatus(
          API,
          this.tournament?._id,
          participantId
        );
      this.participantRS = data?.type;
      this.showParticipantForm = [
        'join',
        'already-registered',
        'checked-in',
        'already-checked-in',
      ].includes(this.participantRS);
      this.getMyTeams();
      this.isLoaded = true;
    } catch (error) {
      await this.fetchSeasonTournamentDetails(slug);
    }
  };

  getMyTeams = async () => {
    const data = await this.userService.getJoinMyTeam(
      API,
      this.tournament?._id
    );
    if (data) {
      data?.data.forEach((control, index) => {
        let totalMemberSize = 0;

        if (this.tournament?.allowSubstituteMember) {
          totalMemberSize +=
            this.tournament?.teamSize + this.tournament?.substituteMemberSize;
        } else {
          totalMemberSize = this.tournament?.teamSize;
        }
        if (control.teamSize >= totalMemberSize) {
          this.teamList.push(control);
        }
      });
    }
  };

  async selectedTeam(data) {
    const teamMembersData: any = await this.userService.getTeamMember(
      API,
      data._id
    );
    this.teamRegistrationActiveStep = 1;
    this.step = 4;
    this.numberStep = 4;
    this.teamSelected = data;
    this.teamMemberList = teamMembersData.data;

    // let teamMembers = [];

    // teamMembersData.data.forEach((control) => {
    //  let newData = {
    //    name: control.userId.fullName,
    //    phoneNumber: control.userId.phoneNumber,
    //    email: control.userId.email,
    //  };
    //     teamMembers.push(newData);
    // });

    // this.tournamentService.setSelectedTeam({
    //   logo: data.logo,
    //   teamName: data.teamName,
    //   teamMembers: teamMembers,
    // });

    // this.router.navigate(['tournament', this.tournament?.slug, 'join']);
  }

  /**
   * Fetch Season tournament Details
   * @param slug
   */
  fetchSeasonTournamentDetails = async (slug) => {
    try {
      this.isLoaded = false;
      const query = JSON.stringify({ slug });
      const select = `&select=_id,name,logo,banner,participantType,teamSize,substituteMemberSize,allowSubstituteMember,slug,startDate,tournamentType,isIncludeSponsor,maxParticipants,bracketType,isPaid,regFee,regFeeCurrency,isSeason`;
      const tournament = await this.tournamentService
        .getSeasonTournaments({ query }, select)
        .toPromise();
      this.tournament = tournament?.data?.length ? tournament.data[0] : null;
      if (!this.tournament) {
        throw new Error(
          this.translateService.instant(
            'VIEW_TOURNAMENT.JOIN_PARTICIPANT.INVALID_TOURNAMENT'
          )
        );
      }

      const { data } =
        await this.tournamentService.fetchSeasonParticipantRegistrationStatus(
          this.tournament?._id
        );

      if (data?.isRegistered) {
      } else {
        this.showParticipantForm = true;
      }

      this.getMyTeams();

      this.isLoaded = true;
    } catch (error) {
      this.isLoaded = true;
      this.toastService.showError(error?.statusText || error?.message);
    }
  };
  setRegistrationOpenStatus() {
    const now = new Date().getTime();
    const tournamentStartTime = new Date(this.tournament?.startDate).getTime();
    const regStartTime = new Date(this.tournament?.regStartDate).getTime();
    const regEndTime = new Date(this.tournament?.regEndDate).getTime();

    if (this.tournament?.isRegistrationClosed) {
      this.registrationStatus.isClosed = true;
      return;
    }
    if (this.tournament.isSeeded || now > tournamentStartTime) {
      this.registrationStatus.isClosed = true;
      return;
    }

    if (this.tournament['isRegStartDate'] && now > regEndTime) {
      this.registrationStatus.isClosed = true;
      return;
    }

    if (this.tournament['isRegStartDate'] && regStartTime > now) {
      this.registrationStatus.notYetStarted = true;
      return;
    }
    this.registrationStatus.isOpen = true;
  }
}
