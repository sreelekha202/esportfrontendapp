
import { Component, OnInit, Input } from '@angular/core';
import { AppHtmlRoutes } from '../../../app-routing.model';
import { EsportsLeaderboardService, EsportsUserService, EsportsToastService } from 'esports';
import { MatDialog } from '@angular/material/dialog';
import { PopupCreatedComponent } from '../popup-created/popup-created.component';
import { environment } from '../../../../environments/environment';
const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;

@Component({
  selector: 'app-leaderboard-user-card',
  templateUrl: './leaderboard-user-card.component.html',
  styleUrls: ['./leaderboard-user-card.component.scss'],
})

export class LeaderboardUserCardComponent implements OnInit {
  @Input() data: any;

  AppHtmlRoutes = AppHtmlRoutes;

  constructor(
    public matDialog: MatDialog,
    private leaderboardService: EsportsLeaderboardService,
    private toastService: EsportsToastService,
    private userService: EsportsUserService,

  ) { }

  ngOnInit(): void { }

  followUser(followedUserId) {
    this.leaderboardService.followUser(API,followedUserId).subscribe((res) => {
      if (res.success) {
        this.toastService.showSuccess(res.message)
        this.userService.refreshCurrentUser(API, TOKEN);
      }
    })
  }
  commingSoon(): void {
    this.matDialog.open(PopupCreatedComponent);
  }
}
