import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { EsportsLeaderboardService } from 'esports';

import { environment } from '../../../../environments/environment';
const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;
@Component({
  selector: 'app-leaderboard-player',
  templateUrl: './leaderboard-player.component.html',
  styleUrls: ['./leaderboard-player.component.scss'],
})
export class LeaderboardPlayerComponent implements OnInit {
  showLoader: boolean = true;

  games = [];

  playerData = null;

  constructor(
    private activatedRoute: ActivatedRoute,
    private leaderboardService: EsportsLeaderboardService,
    private location: Location
  ) { }

  ngOnInit(): void {
    this.getPlayerDetails();

    // MOCK DATA
    this.games = [
      { name: 'valorant' },
      { name: 'dota 2' },
      { name: 'fortnine' },
    ];
  }

  goBack() {
    this.location.back();
  }

  getPlayerDetails() {
    const userId = this.activatedRoute.snapshot.params.userId;

    this.leaderboardService.getUserLeaderboard(API, { userId }).subscribe(
      (res: any) => {
        this.playerData = res.data;
        this.showLoader = false;
      },
      (err) => {
        this.showLoader = false;
      }
    );
  }
}
