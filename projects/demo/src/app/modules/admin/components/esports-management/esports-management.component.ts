import { Component, EventEmitter, OnInit } from "@angular/core";
import { MatTabChangeEvent } from "@angular/material/tabs";
import { IPagination, EsportsAdminService, EsportsConstantsService, EsportsTournamentService, EsportsUserService } from 'esports';
import { environment } from '../../../../../environments/environment';
const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;
import {
  InfoPopupComponentData,
  InfoPopupComponentType,
  InfoPopupComponent,
} from "../../../../shared/popups/info-popup/info-popup.component";
import { ActivatedRoute, Router } from "@angular/router";
import { MatDialog } from "@angular/material/dialog";
import { AppHtmlRoutes } from "../../../../app-routing.model";
import { TranslateService } from "@ngx-translate/core";
import { debounceTime } from "rxjs/operators";
import { EsportsNotificationsService } from "esports";

@Component({
  selector: "app-esports-management",
  templateUrl: "./esports-management.component.html",
  styleUrls: ["./esports-management.component.scss"],
})
export class EsportsManagementComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;
  checkIconShow = false;
  viewIconShow = false;
  deleteIconShow = false;
  editIconShow = false;
  manageIconShow = false;
  userId;
  page: IPagination;
  finishedTournament: any = [];
  activeTabIndex;
  paginationData = {
    page: 1,
    limit: 50,
    sort: "createdOn",
  };
  statusText: string = EsportsConstantsService.Status.Live;
  tournamentList = [];
  allTournamentList: any = [];
  isLoading = false;
  selectedTab = "ongoing";
  customMatTabEvent = new EventEmitter<MatTabChangeEvent>();

  constructor(
    private tournamentService: EsportsTournamentService,
    private userService: EsportsUserService,
    private adminService: EsportsAdminService,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private dialog: MatDialog,
    private userNotificationsService: EsportsNotificationsService,
    private translateService: TranslateService
  ) { }

  ngOnInit(): void {
    this.activeRoute.queryParams
      .subscribe((params) => {
        if (params.activeTab) {
          this.activeTabIndex = +params.activeTab;
          this.switchData(this.activeTabIndex);
        } else {
          this.activeTabIndex = 0;
          this.getData("1", "Latest");
          this.setConfiguration(true, true, false, false, true);
        }
      })
      .unsubscribe();

    this.customMatTabEvent
      .pipe(debounceTime(30))
      .subscribe((tabChangeEvent) => {
        this.activeTabIndex = tabChangeEvent.index;
        this.router.navigate(["."], {
          relativeTo: this.activeRoute,
          queryParams: { activeTab: tabChangeEvent.index },
        });
        this.switchData(tabChangeEvent.index);
      });
  }

  pageChanged(page): void {
    this.paginationData.page = page;
    switch (this.activeTabIndex) {
      case 0:
        this.getData("1", "Latest"); //ongoing
        break;
      case 1:
        this.getData("0", "Latest"); // upcoming
        break;
      case 2:
        this.getData("2", "Oldest"); //past
        break;
      case 4:
        this.getTournamentForPendingPayments();
        break;
      default:
        break;
    }
  }

  getTournamentForPendingPayments() {
    this.isLoading = true;
    this.adminService.getTournamentsforPayments(API).subscribe(
      (res: any) => {
        const response = res.data.map((element) => {
          return {
            _id: element._id,
            name: element.name,
            slug: element.slug,
            isPaid: element.isPaid,
            isFeature: element.isFeature,
            game: element.gameDetail?.name,
            region: element.regionsAllowed,
            startDate: element.startDate,
            tournamentType: element.tournamentType,
            createdOn: element.createdOn,
            maxParticipants: element.maxParticipants,
          };
        });
        this.isLoading = false;
        this.finishedTournament = response;
      },
      (err: any) => {
        this.isLoading = false;
      }
    );
  }

  /**
   * method to update the modifications on tournaments
   * @param data
   */
  modifyTournamentHandler(data) {
    if (data.type === "abort") {
      this.switchData(this.activeTabIndex);
    } else if (data.type === "feature") {
      this.tournamentService
        .updateTournament2({ isFeature: data.value }, data.id)
        .subscribe(
          (res: any) => {
            if (data.value) {
              // TODO
              // const inputData = {
              //   message: this.translateService.instant(
              //     'API.TOURNAMENT.FEATURE.PUSH.MESSAGE'
              //   ),
              //   title: this.translateService.instant(
              //     'API.TOURNAMENT.FEATURE.PUSH.TITLE'
              //   ),
              //   userSegment: 'TOURNAMENT_PLAYERS_BY_ID',
              //   tournamentId: data.id,
              // };
              // this.userNotificationsService
              //   .sendPushNotifications(inputData)
              //   .subscribe((resp onse: any) => {});
            }
            this.getData("0", "Latest");
            const afterBlockData: InfoPopupComponentData = {
              title: this.translateService.instant(
                "API.TOURNAMENT.PUT.SUCCESS_HEADER"
              ),
              text: res.message,
              type: InfoPopupComponentType.info,
            };
            this.dialog.open(InfoPopupComponent, { data: afterBlockData });
          },
          (err: any) => {
            const afterBlockData: InfoPopupComponentData = {
              title: this.translateService.instant(
                "API.TOURNAMENT.PUT.ERROR_HEADER"
              ),
              text:
                this.translateService.instant(err.error.messageCode) ||
                err.error.message,
              type: InfoPopupComponentType.info,
            };
            this.dialog.open(InfoPopupComponent, { data: afterBlockData });
          }
        );
    } else if (data.type === "update") {
      this.switchData(data.value);
    } else if (data.type === "updatePaymentTable") {
      this.getTournamentForPendingPayments();
    }
  }

  /**
   * Get the tournament list based on Tab selection
   * @param tabChangeEvent : Event on Change of Tab
   */
  onTabChanged = (tabChangeEvent: MatTabChangeEvent): void => {
    this.customMatTabEvent.emit(tabChangeEvent);
  };

  switchData(index) {
    switch (index) {
      case 0:
        this.resetPage();
        this.setConfiguration(true, true, false, false, true);
        this.getData("1", "Latest"); //ongoing
        break;
      case 1:
        this.resetPage();
        this.setConfiguration(true, true, false, true, true);
        this.getData("0", "Latest"); // upcoming
        break;
      case 2:
        this.resetPage();
        this.setConfiguration(true, true, false, false, true);
        this.getData("2", "Oldest"); //past
        break;
      case 3:
        this.setConfiguration(true, true, true, true, false);
        this.getTournamentDetailsForApprovalPending();
        break;
      case 4:
        this.getTournamentForPendingPayments();
        break;
      default:
        break;
    }
  }

  resetPage() {
    this.paginationData = {
      page: 1,
      limit: 50,
      sort: "createdOn",
    };
    this.tournamentList = [];
  }

  getData = async (status, sortOrder) => {
    try {
      const params = {
        status: status,
        page: this.paginationData.page,
        limit: this.paginationData.limit,
        sort: sortOrder,
      };

      this.isLoading = true;
      const { data } = await this.tournamentService.fetchTournamentByStatus(environment.apiEndPoint, params);

      this.mapTournamentFilterList(data?.docs);
      this.page = {
        totalItems: data?.totalDocs,
        itemsPerPage: data?.limit,
        maxSize: 5,
      };
      this.isLoading = false;
    } catch (error) {
      this.isLoading = false;
    }
  };

  getTournamentDetailsForApprovalPending() {
    this.isLoading = true;
    this.tournamentList = [];
    const query = JSON.stringify({
      tournamentStatus: { $in: ["submitted_for_approval", "un_paid"] },
      // isCharged: true,
    });
    this.tournamentService.getTournaments(environment.apiEndPoint, { query: query }).subscribe(
      (res) => {
        if (res && res.data) {
          let response = res.data || [];
          this.mapTournamentFilterList(response);
        }
        this.isLoading = false;
      },
      (err) => {
        this.isLoading = false;
      }
    );
  }

  /**
   * Map the tournament details to display in the table
   * @param tournamentList : response from method
   */
  mapTournamentFilterList(tournamentList) {
    const response = tournamentList.map((element) => {
      return {
        _id: element._id,
        slug: element.slug,
        name: element.name,
        isPaid: element.isPaid,
        isFeature: element.isFeature,
        game: element.gameDetail?.name,
        region: element.regionsAllowed,
        startDate: element.startDate,
        tournamentType: element.tournamentType,
        createdOn: element.createdOn,
        maxParticipants: element.maxParticipants,
        reason: element.reason,
      };
    });
    this.tournamentList = response;
  }

  /**
   * Configure icon based on boolean value
   * @param viewIcon : show/hide icon
   * @param deleteIcon:show/hide icon
   * @param checkIcon:show/hide icon
   */
  setConfiguration(viewIcon, deleteIcon, checkIcon, editIcon, manageIcon) {
    this.viewIconShow = viewIcon;
    this.deleteIconShow = deleteIcon;
    this.checkIconShow = checkIcon;
    this.editIconShow = editIcon;
    this.manageIconShow = manageIcon;
  }
}
