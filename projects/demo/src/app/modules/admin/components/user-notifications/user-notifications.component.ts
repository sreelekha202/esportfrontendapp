import { Component, OnInit } from "@angular/core";
import { MatTabChangeEvent } from "@angular/material/tabs";
import { EsportsNotificationsService } from "esports";
import {
  InfoPopupComponentData,
  InfoPopupComponentType,
  InfoPopupComponent,
} from "../../../../shared/popups/info-popup/info-popup.component";
import { TranslateService } from "@ngx-translate/core";
import { MatDialog } from "@angular/material/dialog";
import { environment } from '../../../../../environments/environment';
const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;
@Component({
  selector: "app-user-notifications",
  templateUrl: "./user-notifications.component.html",
  styleUrls: ["./user-notifications.component.scss"],
})
export class UserNotificationsComponent implements OnInit {
  constructor(
    private userNotificationsService: EsportsNotificationsService,
    private translateService: TranslateService,
    private dialog: MatDialog
  ) {}

  activeTabIndex = 0;
  isLoading = false;
  UserSegment = [
    "All users",
    "Paying users",
    "Tournament players",
    "Tournament organizers",
    "Recently purchased",
    "No purchase in the last 30 days",
  ];
  type = ["Email", "Push", "Inbox"];
  status = ["In Queue", "Sent"];

  ngOnInit(): void {}
  onTabChange(matTab: MatTabChangeEvent): void {
    this.activeTabIndex = matTab.index;
    if (this.activeTabIndex == 3) {
      this.getSentList();
    }
  }
  sentList = [];
  getSentList() {
    this.isLoading = true;
    this.userNotificationsService.getSentItems(API).subscribe(
      (res: any) => {
        this.isLoading = false;
        this.sentList = res.data.map((item) => {
          return {
            Date: item.createdOn,
            Type: this.type[item.type],
            Title: item.title,
            UserSegment: this.UserSegment[item.userSegment],
            Status: this.status[item.status],
          };
        });
      },
      (err: any) => {
        this.isLoading = false;
        const afterBlockData: InfoPopupComponentData = {
          title: "Fail",
          text:
            this.translateService.instant(err.error.messageCode) ||
            err.error.message,
          type: InfoPopupComponentType.info,
        };
        this.dialog.open(InfoPopupComponent, { data: afterBlockData });
      }
    );
  }
}
