import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { Location } from "@angular/common";
import { PaymentDisbursementDialogComponent } from "./payment-disbursement-dialog/payment-disbursement-dialog.component";
import { AutomatedDialogComponent } from "./automated-dialog/automated-dialog.component";
import { SendCouponDialogComponent } from "./send-coupon-dialog/send-coupon-dialog.component";
import { PaymentSuccessfullDialogComponent } from "./payment-successfull-dialog/payment-successfull-dialog.component";
import { ActivatedRoute } from "@angular/router";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { EsportsAdminService } from 'esports';
import { environment } from '../../../../../environments/environment';
const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;
@Component({
  selector: "app-esports-payment-details",
  templateUrl: "./esports-payment-details.component.html",
  styleUrls: ["./esports-payment-details.component.scss"],
})
export class EsportsPaymentDetailsComponent implements OnInit {
  isPaid: any;
  @Input() tournamentId: any;
  @Output() isToggle = new EventEmitter();
  organizer = {
    organizerName: "joan doe",
    email: "Joan04@gmail.com",
    mobileNo: "915595652",
    prizeAmount: "1800 MYR",
    globalFees: "200 MYR",
    total: "2000",
    paymentDate: "15 sep 20",
    status: "in Escrow",
    //paymentMethod: 'assets/global-images/logo.svg',
    paymentMethod: "assets/images/payment/paypal.png",
    check: "assets/images/Special-offer/right-mark.svg",
    organizerImage: "assets/images/Profile/article01.png",
  };

  winners = [
    {
      winnerImage: "assets/images/Profile/article01.png",
      name: "winner Name",
      rank: "1",
      prize: "100 MYR",
      paymentMethod: "assets/global-images/logo.svg",
      check: "assets/images/Special-offer/right-mark.svg",
    },

    {
      winnerImage: "assets/images/Profile/article01.png",
      name: "winner Name",
      rank: "1",
      prize: "100 MYR",
      paymentMethod: "assets/images/Special-offer/boost.png",
      check: "assets/images/Special-offer/right-mark.svg",
    },
  ];

  sportDetail = [];
  transactionValue: any;
  isLoading = true;

  columnsFirstTab = [
    { name: "Tournament Name" },
    { name: "Registration" },
    { name: "Game" },
    { name: "Region" },
    { name: "Type" },
    { name: "Start Date" },
    { name: "End Date" },
    { name: "Participant(s)" },
    { name: "" },
  ];

  tournamentDetails = [];
  paypalObject: any = {};
  constructor(
    public dialog: MatDialog,
    private modalService: NgbModal,
    private location: Location,
    private activatedRoute: ActivatedRoute,
    private adminService:  EsportsAdminService
  ) {}
  ngOnInit(): void {
    // this.activatedRoute.params.subscribe((params) => {
    //   this.tournamentId = params['id'];
    // });
    this.getWinnersList();
  }

  getWinnersList() {
    const query = {
      filter: {
        tournament: this.tournamentId,
        isAward: true,
        awardAmount: { $gte: 0 },
      },
      option: {
        sort: { awardPosition: 1 },
      },
    };
    this.isLoading = true;
    this.adminService.getTournamentWinners(API,query).subscribe(
      (res: any) => {
        this.isLoading = false;
        if (res.data.length > 0) {
          this.sportDetail = res.data;
          this.tournamentDetails = [res.data[0].tournament];
        }
      },
      (err: any) => {}
    );
  }

  cancel() {
    this.isToggle.emit({ toggle: false });
  }
  disburseAmount(index, type, model) {
    if (type == "stcpay") {
      const formData = {
        tournamentId: this.tournamentId,
        mobileNumber: this.sportDetail[index].winnerUser.accountDetail
          .paymentAccountId,
        playerId: this.sportDetail[index].winnerUser._id,
      };
      this.isLoading = true;
      this.adminService.stcCall(API, formData).subscribe(
        (res: any) => {
          this.isLoading = false;
          this.getWinnersList();
        },
        (err: any) => {
          this.isLoading = false;
          this.getWinnersList();
        }
      );
    } else if (type == "paypal") {
      this.modalService.open(model, {
        size: "lg",
        centered: true,
        scrollable: true,
        windowClass: "custom-modal-content",
      });
      this.paypalObject.tournamentId = this.tournamentId;
      this.paypalObject.playerId = this.sportDetail[index].winnerUser._id;
    }
  }

  update() {
    if (this.transactionValue) {
      this.modalService.dismissAll();
      this.isLoading = true;
      this.paypalObject.transactionId = this.transactionValue;
      this.adminService.paypalCall(API,this.paypalObject).subscribe(
        (res: any) => {
          this.isLoading = false;
          this.paypalObject = {};
          this.transactionValue = "";
          this.getWinnersList();
        },
        (err: any) => {
          this.isLoading = false;
        }
      );
    }
  }

  openDialog = async () => {
    try {
      const pdResponse = await this.dialog
        .open(PaymentDisbursementDialogComponent, {
          data: 5,
          panelClass: "custom-dialog-container",
        })
        .afterClosed()
        .toPromise();
      if (pdResponse == "automated") {
        const AResponse = await this.dialog
          .open(AutomatedDialogComponent, {
            data: 2000,
            panelClass: "custom-dialog-container",
          })
          .afterClosed()
          .toPromise();
        if (AResponse == "cancel") {
          const canResponse = await this.dialog
            .open(PaymentDisbursementDialogComponent, {
              data: 5,
              panelClass: "custom-dialog-container",
            })
            .afterClosed()
            .toPromise();
        }
      } else if (pdResponse == "coupon") {
        const CResponse = await this.dialog
          .open(SendCouponDialogComponent, {
            data: 5,
            panelClass: "custom-dialog-container",
          })
          .afterClosed()
          .toPromise();
        if (CResponse == "successfull") {
          const SResponse = await this.dialog
            .open(PaymentSuccessfullDialogComponent, {
              data: 5,
              panelClass: "custom-dialog-container",
            })
            .afterClosed()
            .toPromise();
        } else if (CResponse == "cancel") {
          const canResponse = await this.dialog
            .open(PaymentDisbursementDialogComponent, {
              data: 5,
              panelClass: "custom-dialog-container",
            })
            .afterClosed()
            .toPromise();
        }
      }
    } catch (error) {}
  };
}
