import { Component, OnInit, EventEmitter, Input, Output } from "@angular/core";
import { EsportsUserService,EsportsTransactionService, EsportsToastService } from 'esports';
import { environment } from '../../../../../../environments/environment';
const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;
@Component({
  selector: "app-reg-fee-refund-details",
  templateUrl: "./reg-fee-refund-details.component.html",
  styleUrls: ["./reg-fee-refund-details.component.scss"],
})
export class RegFeeRefundDetailsComponent implements OnInit {
  @Input() tournamentDetails: any;
  @Output() isToggle = new EventEmitter();
  isLoading = false;
  disbursalStatus;
  participantDetails;
  paymentMethod;
  accountDetail;

  columns = [
    { name: "Tournament Name" },
    { name: "Game" },
    { name: "End Date" },
    { name: "Participant(s)" },
    { name: "Disbursal Status" },
    { name: "Details" },
  ];
  rows: any = [];
  status = "";
  constructor(
    private userService: EsportsUserService,
    private transactionService: EsportsTransactionService,
    public toastService: EsportsToastService
  ) {}

  ngOnInit(): void {
    this.rows = [this.tournamentDetails];
    this.disbursalStatus = this.tournamentDetails.lastDisbursalRecord;
    this.getDisbursalStatus();
    this.getParticipantDetails();
  }

  cancel() {
    this.isToggle.emit({ toggle: false });
  }

  async getParticipantDetails() {
    try {
      this.userService
        .getUserAccountDetails(API,
          this.tournamentDetails.registeredParticipant.userId
        )
        .subscribe((res) => {
          this.participantDetails = res["data"];
          this.accountDetail = this.participantDetails?.accountDetail;
          this.setPaymentMethod();
        });
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
    }
  }

  setPaymentMethod() {
    const isAccountVerified = this.participantDetails.accountDetail
      .isPaymentAccountVerified;
    if (isAccountVerified) {
      const paymentType = this.participantDetails.accountDetail.paymentType;
      this.paymentMethod =
        paymentType == "paypal" ? "assets/images/payment/paypal.png" : "";
    }
  }

  async getDisbursalStatus() {
    try {
      this.status = this.disbursalStatus?.status;
      if (
        this.tournamentDetails.lastDisbursalRecord.status === "NOT DISBURSED"
      ) {
        return;
      }
      const query = {
        tournamentId: this.tournamentDetails._id,
        userId: this.tournamentDetails.registeredParticipant.userId,
      };
      const res = await this.transactionService.getRegFeeRefundStatus(query);
      this.disbursalStatus = res.data;
      this.status = this.disbursalStatus?.status;
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
    }
  }

  async initiateDisbursal() {
    try {
      if (!this.accountDetail?.paymentAccountId) {
        return;
      }

      this.isLoading = true;
      const payload = {
        tournamentId: this.tournamentDetails._id,
        userId: this.tournamentDetails.registeredParticipant.userId,
      };
      const res = await this.transactionService.refundRegFee(payload);
      this.toastService.showSuccess(res.message);
      this.disbursalStatus = res.data;
      this.getDisbursalStatus();
      this.isLoading = false;
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
      this.isLoading = false;
    }
  }
}
