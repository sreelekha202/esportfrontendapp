import { Component, OnInit, EventEmitter, Input, Output } from "@angular/core";
import { EsportsUserService, EsportsToastService, EsportsTransactionService } from 'esports';

import { environment } from '../../../../../../environments/environment';
const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;
@Component({
  selector: "app-prize-money-refund-details",
  templateUrl: "./prize-money-refund-details.component.html",
  styleUrls: ["./prize-money-refund-details.component.scss"],
})
export class PrizeMoneyRefundDetailsComponent implements OnInit {
  @Input() tournamentDetails: any;
  @Output() isToggle = new EventEmitter();
  isLoading = false;
  disbursalStatus;
  organizerDetail;
  paymentMethod;
  accountDetail;

  columns = [
    { name: "Tournament Name" },
    { name: "Game" },
    { name: "End Date" },
    { name: "Participant(s)" },
    { name: "Disbursal Status" },
    { name: "Details" },
  ];
  rows: any = [];
  status = "";
  constructor(
    private userService: EsportsUserService,
    private transactionService: EsportsTransactionService,
    public toastService: EsportsToastService
  ) { }

  ngOnInit(): void {
    this.rows = [this.tournamentDetails];
    this.disbursalStatus = this.tournamentDetails.lastDisbursalRecord;
    this.getDisbursalStatus();
    this.getOrganizerDetails();
  }

  getTotalAmount() {
    const totalPrice = this.tournamentDetails.prizeList || [];
    const prizeSum = totalPrice.reduce((acc, el) => acc + el.value, 0);
    return prizeSum;
  }

  cancel() {
    this.isToggle.emit({ toggle: false });
  }

  async getOrganizerDetails() {
    try {
      this.userService
        .getUserAccountDetails(API, this.tournamentDetails.organizerDetail)
        .subscribe((res) => {
          this.organizerDetail = res["data"];
          this.accountDetail = this.organizerDetail?.accountDetail;
          this.setPaymentMethod();
        });
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
    }
  }

  setPaymentMethod() {
    const isAccountVerified = this.organizerDetail.accountDetail
      .isPaymentAccountVerified;
    if (isAccountVerified) {
      const paymentType = this.organizerDetail.accountDetail.paymentType;
      this.paymentMethod =
        paymentType == "paypal" ? "assets/images/payment/paypal.png" : "";
    }
  }

  async getDisbursalStatus() {
    try {
      this.status = this.disbursalStatus?.status;
      if (
        this.tournamentDetails.lastDisbursalRecord.status === "NOT DISBURSED"
      ) {
        return;
      }
      const res = await this.transactionService.getPrizeMoneyRefundStatus(API,
        this.tournamentDetails._id
      );
      this.disbursalStatus = res.data;
      this.status = this.disbursalStatus?.status;
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
    }
  }

  async initiateDisbursal() {
    try {
      if (!this.accountDetail?.paymentAccountId) {
        return;
      }

      this.isLoading = true;
      const payload = {
        tournamentId: this.tournamentDetails._id,
      };
      const res = await this.transactionService.refundPrizeMoney(API,payload);
      this.toastService.showSuccess(res.message);
      this.disbursalStatus = res.data;
      this.getDisbursalStatus();
      this.isLoading = false;
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
      this.isLoading = false;
    }
  }
}
