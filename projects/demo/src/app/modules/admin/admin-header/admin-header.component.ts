import { AfterViewInit, Component, OnDestroy, OnInit } from "@angular/core";
import { faBell } from "@fortawesome/free-solid-svg-icons";
import { MatSidenav } from "@angular/material/sidenav";
import { Router } from "@angular/router";
import {
  AppHtmlAdminRoutes,
  AppHtmlRoutesLoginType,
  AppHtmlRoutes,
  AppHtmlProfileRoutes,
} from "../../../app-routing.model";
import { IUser, EsportsUserService, EsportsLanguageService, EsportsConstantsService, EsportsChatSidenavService, GlobalUtils } from "esports";
import { TranslateService } from "@ngx-translate/core";
import { Subscription } from "rxjs";
enum AppLanguage {
  ms = "ms",
  en = "en",
}
import { environment } from '../../../../environments/environment';
const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;
@Component({
  selector: "app-admin-header",
  templateUrl: "./admin-header.component.html",
  styleUrls: ["./admin-header.component.scss"],
})
export class AdminHeaderComponent implements OnInit, AfterViewInit, OnDestroy {
  AppHtmlAdminRoutes = AppHtmlAdminRoutes;
  faBell = faBell;
  isMenuOpened = false;
  currentUser: IUser;
  AppLanguage: any = [];
  activeLang: any = this.constantsService?.defaultLangCode;
  AppHtmlRoutesLoginType = AppHtmlRoutesLoginType;
  AppHtmlRoutes = AppHtmlRoutes;
  AppHtmlProfileRoutes = AppHtmlProfileRoutes;

  userSubscription: Subscription;

  constructor(
    private sidenavService: EsportsChatSidenavService,
    private constantsService: EsportsConstantsService,
    private userService: EsportsUserService,
    private router: Router,
    public translate: TranslateService,
    private languageService: EsportsLanguageService
  ) { }

  ngOnInit(): void {
    this.AppLanguage = this.constantsService?.language;
    this.activeLang = this.translate.currentLang;
    this.languageService.setLanguage(this.activeLang);
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.currentUser = data;
      }
    });
  }

  ngOnDestroy() {
    this.userSubscription?.unsubscribe();
  }

  onLanguageChange(lang: AppLanguage): void {
    if (GlobalUtils.isBrowser()) {
      localStorage.setItem("currentLanguage", lang);
      this.activeLang = lang;
      this.translate.use(lang);
      this.languageService.setLanguage(lang);
    }
  }

  async ngAfterViewInit(): Promise<void> {
    const sidenav: MatSidenav = await this.sidenavService.getChatSidenav();

    if (sidenav) {
      sidenav.openedChange.subscribe((isOpened) => {
        this.isMenuOpened = isOpened;
      });
    }
  }

  onLogOut(): void {
    this.userService.logout(API, TOKEN);
    this.router.navigate([
      AppHtmlRoutes.userPageType,
      AppHtmlRoutesLoginType.emailLogin,
    ]);
  }

  onMenuToggle(): void {
    this.sidenavService.toggle();
  }
}
