import { AfterViewInit, Component, OnInit, ViewChild } from "@angular/core";
import { ScreenService } from "../../shared/service/screen/screen.service";
import { MatSidenav } from "@angular/material/sidenav";
import { EsportsChatSidenavService, GlobalUtils } from "esports";

@Component({
  selector: "app-admin",
  templateUrl: "./admin.component.html",
  styleUrls: ["./admin.component.scss"],
})
export class AdminComponent implements OnInit, AfterViewInit {
  @ViewChild("sidenav") public sidenav: MatSidenav;

  isLgScreen: boolean = false;

  constructor(
    private sidenavService: EsportsChatSidenavService,
    private screenService: ScreenService
  ) { }

  ngOnInit(): void { }

  ngAfterViewInit(): void {
    this.sidenavService.setChatSidenav(this.sidenav);
    this.screenService.onAppResizeListener();
    this.initSubscriptions();
  }

  private initSubscriptions(): void {
    if (GlobalUtils.isBrowser()) {
      setTimeout(() => {
        this.screenService.isLgScreen.subscribe((isLgScreen: boolean) => {
          this.isLgScreen = isLgScreen;

          if (isLgScreen) {
            this.sidenavService.close();
          } else {
            this.sidenavService.open();
          }
        });
      });
    }
  }
}
