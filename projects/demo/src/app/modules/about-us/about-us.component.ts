import { Component, OnInit } from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';


@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.scss']
})
export class AboutUsComponent implements OnInit {

  customOptions: OwlOptions = {
    loop: true,
    autoplay: false,
    autoplayTimeout:4000,
    center: true,
    dots: false,
    nav: true,
    navText: ["<", ">"],
    autoHeight: true,
    autoWidth: true,
    responsive: {
      0: {
        items: 1
      },
      600: {
        items: 3,
      },
      900: {
        items: 5,
      }
    }
  }
  constructor() { }

  ngOnInit(): void {
  }



}
