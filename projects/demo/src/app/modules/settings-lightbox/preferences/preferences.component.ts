import { EsportsToastService, EsportsUserService, EsportsUserPreferenceService } from 'esports';
import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { SettingsLightboxComponent } from '../settings-lightbox.component';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-preferences',
  templateUrl: './preferences.component.html',
  styleUrls: ['./preferences.component.scss'],
})
export class PreferencesComponent implements OnInit {
  games = [];
  platforms = [];
  content = [];
  userSubscription: Subscription;
  currentUser: any;
  itemsArray = [];


  constructor(
    private userPreferenceService: EsportsUserPreferenceService,
    private toastService: EsportsToastService,
    private userService: EsportsUserService,
    public dialogRef: MatDialogRef<SettingsLightboxComponent>
  ) { }
  searchGame: string = '';


  ngOnInit(): void {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) { this.currentUser = data; }
    });
    this.getAllSettingPreference()
  }

  getAllSettingPreference() {
    this.userPreferenceService.getAllSettingPageData().subscribe((res) => {
      this.setUserDefaultPreference(res.data);
    }, (err) => { })
  }

  setUserDefaultPreference(data) {
    let games = data.game;
    let platforms = data.platform;
    let content = data.prefer;
    // game
    games.map((obj) => {
      let is_selected: boolean = false;
      this.currentUser.preference.game.map((userGame) => {
        if (userGame == obj._id) { is_selected = true; }
      });
      this.games.push({ ...obj, is_selected: is_selected, name: String(obj.name).toLowerCase() })
    })
    // platform
    platforms.map((obj) => {
      let is_selected: boolean = false;
      this.currentUser.preference.platform.map((userPlatform) => {
        if (userPlatform == obj._id) { is_selected = true; }
      });
      this.platforms.push({ ...obj, is_selected: is_selected, name: String(obj.name).toLowerCase() })
    })
    // content
    content.map((obj) => {
      let is_selected: boolean = false;
      this.currentUser.preference.prefercontent.map((userGame) => {
        if (userGame == obj._id) { is_selected = true; }
      });
      this.content.push({ ...obj, is_selected: is_selected, name: String(obj.name).toLowerCase() })
    })
  }

  onGameItemSelect(data) {
    let game = this.games;
    game.find((obj, i) => { if (obj._id == data.data._id) { game[i] = data.data; } })
    this.games = game;
  }
  onPlatformItemSelect(data) {
    let platform = this.platforms;
    platform[data.index] = data.data;
    this.platforms = platform;
  }
  onContentItemSelect(data) {
    let content = this.content;
    content[data.index] = data.data;
    this.content = content;
  }

  onSubmit() {
    let games = [];
    let platforms = [];
    let content = []
    this.games.map((obj) => { obj.is_selected ? games.push(obj._id) : '' });
    this.platforms.map((obj) => { obj.is_selected ? platforms.push(obj._id) : '' });
    this.content.map((obj) => { obj.is_selected ? content.push(obj._id) : '' });
    if (games.length > 0 || platforms.length > 0 || content.length > 0) {
      this.userPreferenceService.addPrefrence({ genre: [], game: games, platform: platforms, prefer: content }).subscribe((res) => {
        if (res.success) {
          this.toastService.showSuccess(res.message);
          // this.onClose();
        }
      }, (err) => { })
    }
  }

  onClose(): void { this.dialogRef.close(); }

  filteredGames() {
    return this.games.filter((game) => game.name.includes(this.searchGame.toLowerCase().trim()));
  }
  ngOnDestroy(): void { if (this.userSubscription) this.userSubscription.unsubscribe(); }

  // cancelButton() {
  //   let game = this.games
  //   for(var i=0; i<= game.length;i++){
  //     if(game[i].is_selected == true){
  //       game.splice(i,1);
  //     }
  //   }
  // }
  cancelButton() { this.dialogRef.close(); }


}

