import {
  EsportsUserService,
  EsportsToastService,
  EsportsAuthServices,
} from 'esports';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Subscription } from 'rxjs';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { SettingsLightboxComponent } from '../settings-lightbox.component';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { environment } from '../../../../environments/environment';

const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
})
export class ProfileComponent implements OnInit {
  @Output() dateChange: EventEmitter<MatDatepickerInputEvent<any>>;
  selectedOption: string;
  showLoader: boolean;
  currentUser: any;
  userSubscription: Subscription;
  addForm: FormGroup;
  editImageUrl: any = null;
  selectedFile: any;
  isShown: boolean = false;
  url: any;
  editprofile = false;
  normal = true;
  // gender = [
  //   { title: 'Female', value: 'female' },
  //   { title: 'Male', value: 'male' },
  // ]

  pronouns = [
    { title: 'Please Select', value: 'She' },
    { title: 'She/Her', value: 'She' },
    { title: 'He/His', value: 'He' },
    { title: 'They/Them', value: 'They' },
  ];
  gender = [
    { title: 'She/Her', value: 'she' },
    { title: 'He/Him', value: 'he' },
    { title: 'They/Them', value: 'they' },
    { title: 'Custom', value: 'Custom' },
  ];

  stateList: any = [];
  countryList: any = [];
  base64textString: any;
  isExist: any;

  form = this.formBuilder.group({
    dobPolicy: [false],
  });
  todayDate: Date = new Date(
    new Date().setFullYear(new Date().getFullYear() - 10)
  );
  parentalDate: Date = new Date(
    new Date().setFullYear(new Date().getFullYear() - 18)
  );
  parental = false;

  constructor(
    private userService: EsportsUserService,
    private authServices: EsportsAuthServices,
    private fb: FormBuilder,
    private toastService: EsportsToastService,
    public dialogRef: MatDialogRef<SettingsLightboxComponent>,
    private formBuilder: FormBuilder
  ) {
    this.addForm = this.fb.group({
      fullName: [null, Validators.compose([Validators.required])],
      country: [null, Validators.compose([Validators.required])],
      username: [
        null,
        Validators.compose([
          Validators.required,
          Validators.minLength(3),
          Validators.pattern(/^[a-zA-Z0-9_]+$/),
        ]),
      ],
      gender: [null, Validators.compose([Validators.required])],
      //pronouns: [null],
      dob: [null, Validators.compose([Validators.required])],
      state: [null, Validators.compose([Validators.required])],
      // postal_code: ['123456', Validators.compose([Validators.required])],
      shortBio: [null],
      profilePicture: [''],
    });
    this.userService.getAllCountries().subscribe((res) => {
      this.countryList = res.countries;
    });
  }

  ngOnInit(): void {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.currentUser = data;
        this.setUserData();
      }
    });
    this.getavatar();
  }

  getavatar() {
    // this.userService.getAvatarProfile().subscribe((data)=>{
    // })
  }

  parentalCheckBox(date: any) {
    const selectedDate = new Date(date.value);
    if (selectedDate > this.parentalDate && selectedDate < this.todayDate) {
      this.parental = true;
      this.form.get('dobPolicy').setValidators(Validators.pattern('true'));
      this.form.get('dobPolicy').setValue(true);
    } else {
      this.parental = false;
      this.form.get('dobPolicy').setValidators(Validators.pattern('false'));
      this.form.get('dobPolicy').setValue(false);
    }

    this.form.get('dobPolicy').updateValueAndValidity();
  }
  function() {
    this.selectedOption;
  }
  setUserData() {
    this.getCountryWiseState(Number(this.currentUser.country));
    (this.editImageUrl = this.currentUser.profilePicture),
      this.addForm.patchValue({
        fullName: this.currentUser.fullName,
        username: this.currentUser.username,
        gender: this.currentUser.gender,
        //'pronouns': this.currentUser['pronouns'],
        dob: this.currentUser.dob,
        country: Number(this.currentUser.country),
        state: this.currentUser.state,
        profilePicture: this.currentUser.profilePicture,
        shortBio: this.currentUser.shortBio,
      });
  }

  getCountryWiseState(country_id: any) {
    let allState: any = [];
    this.userService.getStates().subscribe((res) => {
      allState = res.states;
      allState.filter((obj) => {
        if (obj['country_id'] == country_id) {
          this.stateList.push(obj);
        }
      });
    });
  }

  openprofile() {
    this.normal = false;
    this.editprofile = true;
  }

  close() {
    this.normal = true;
    this.editprofile = false;
  }
  onCountryChange = (data: any) => this.getCountryWiseState(data.value);

  onSubmit(formData: any) {
    this.showLoader = true;
    if (this.addForm.valid) {
      // for (let key in this.addForm.value) { formData.append(`${key}`, this.addForm.value[key]); }
      this.userService.updateProfile(API, this.addForm.value).subscribe(
        (res) => {
          this.showLoader = false;
          this.setUserData();
          this.toastService.showSuccess(res.message);
          this.onClose();
          this.userService.refreshCurrentUser(API, TOKEN);
        },
        (err) => {
          this.showLoader = false;
          this.toastService.showError(err.message);
        }
      );
    }
  }

  onClose(): void {
    this.dialogRef.close();
  }

  fileChangeEvent(fileInput: any) {
    const reg = /(.*?)\.(jpg|jpeg|png|gif|giff)$/;
    if (!fileInput.target.files[0].name.match(reg)) {
      this.toastService.showError('Please select valid file');
      this.removeFile();
      return false;
    } else {
      this.removeFile();
      this.selectedFile = fileInput.target.files[0];
      var reader = new FileReader();
      reader.onload = this.handleReaderLoaded.bind(this);
      reader.readAsBinaryString(this.selectedFile);
    }
  }

  handleReaderLoaded(readerEvt) {
    var binaryString = readerEvt.target.result;
    this.base64textString = 'data:image/png;base64,' + btoa(binaryString);
    this.addForm.get('profilePicture').setValue(this.base64textString);
  }

  removeFile() {
    this.selectedFile = null;
  }
  onresetForm() {
    this.addForm.reset();
    // this.setUserData();
  }

  isUniqueName = async (name) => {
    this.authServices.searchUsername(name).subscribe((res) => {
      this.isExist = res.data?.isExist;
      if (this.isExist) {
        this.addForm.controls['username'].setErrors({ incorrect: true });
      } else {
        this.addForm.controls['username'].setErrors(null);
        this.addForm.controls['username'].setValidators([
          Validators.required,
          Validators.minLength(3),
          Validators.pattern(/^[a-zA-Z0-9_]+$/),
        ]);
        this.addForm.controls['username'].updateValueAndValidity();
      }
    });
  };

  ngOnDestroy(): void {
    if (this.userSubscription) this.userSubscription.unsubscribe();
  }
}
