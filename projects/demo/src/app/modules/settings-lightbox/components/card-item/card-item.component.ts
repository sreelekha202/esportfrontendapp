import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-card-item',
  templateUrl: './card-item.component.html',
  styleUrls: ['./card-item.component.scss'],
})
export class CardItemComponent implements OnInit {
  @Input() data: any;
  @Input() isImage: boolean;
  @Input() isIcon: boolean;
  @Input() index: number;
  @Output() onItemSelect:any = new EventEmitter();

  isActive: boolean = false;

  gamesIconPath = 'assets/icons/games';
  contentIconPath = 'assets/icons/matchmaking/content';
  platformtsIconPath = 'assets/icons/matchmaking/platforms';

  constructor() {}

  ngOnInit(): void {}

  onSelect() {
    this.onItemSelect.emit({ "index": this.index,"data": { ...this.data, is_selected: !this.data.is_selected } })
  }

  getIcon(name: string): string {
    name=name.toLowerCase();

    switch (name) {
      case 'thesims4': return `${this.gamesIconPath}/sims4.svg`;
      case 'valorant': return `${this.gamesIconPath}/valorant.svg`;
      case 'fortnine': return `${this.gamesIconPath}/fortnine.svg`;
      case 'minecraft': return `${this.gamesIconPath}/minecraft.svg`;
      case 'rust': return `${this.gamesIconPath}/rust.svg`;
      case 'cod': return `${this.gamesIconPath}/cod.svg`;
      case 'overwatch': return `${this.gamesIconPath}/overwatch.svg`;

      case 'pc': return `${this.platformtsIconPath}/pc.svg`;
      case 'other': return `${this.platformtsIconPath}/console.svg`;
      case 'mobile': return `${this.platformtsIconPath}/mobile.svg`;
      case 'ps5': return `${this.platformtsIconPath}/ps5.svg`;
      case 'ps4': return `${this.platformtsIconPath}/ps4.svg`;
      case 'xbox': return `${this.platformtsIconPath}/xbox.svg`;

      case 'games': return `${this.platformtsIconPath}/console.svg`;
      case 'influencer': return `${this.contentIconPath}/influencer.svg`;
      case 'storeoffers': return `${this.contentIconPath}/storeoffers.svg`;
      case 'tournament': return `${this.contentIconPath}/tournament.svg`;
    }
  }
}
