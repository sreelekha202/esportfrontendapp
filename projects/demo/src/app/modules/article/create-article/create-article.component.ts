import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import {
  EsportsToastService,
  S3UploadService,
  WYSIWYGEditorConfig,
  EsportsInfoPopupComponentType,
  EsportsArticleService,
  EsportsOptionService,
  EsportsGameService,
} from 'esports';
import { Location } from '@angular/common';
import { AppHtmlProfileRoutes } from '../../../app-routing.model';
import { AppHtmlRoutes } from '../../../app-routing.model';
import { environment } from '../../../../environments/environment';
import {
  EsportsInfoPopupComponent,
  EsportsInfoPopupComponentData,
  GlobalUtils,
} from 'projects/esports/src/public-api';
import { MatDialog } from '@angular/material/dialog';

const API = environment.apiEndPoint;
@Component({
  selector: 'app-create-article',
  templateUrl: './create-article.component.html',
  styleUrls: ['./create-article.component.scss'],
})
export class CreateArticleComponent implements OnInit {
  AppHtmlProfileRoutes = AppHtmlProfileRoutes;
  createMemberForm: FormGroup;

  fileToUploadBanner: any = '';
  teamBannerSRCFile: any = '';
  teamBannerSRCPost: any = '';

  languageList: any = [{ _id: 1, name: 'English' }];
  selectedLanguage: string = '';

  editorConfig: WYSIWYGEditorConfig = {
    editable: true,
    spellcheck: true,
    height: '200px',
    width: 'auto',
    minWidth: '0',
    translate: 'false',
    enableToolbar: true,
    showToolbar: true,
    defaultParagraphSeparator: '',
    defaultFontName: '',
    defaultFontSize: '',
    fonts: [
      { class: 'arial', name: 'Arial' },
      { class: 'times-new-roman', name: 'Times New Roman' },
      { class: 'calibri', name: 'Calibri' },
      { class: 'comic-sans-ms', name: 'Comic Sans MS' },
    ],
    customClasses: [
      {
        name: 'quote',
        class: 'quote',
      },
      {
        name: 'redText',
        class: 'redText',
      },
      {
        name: 'titleText',
        class: 'titleText',
        tag: 'h1',
      },
    ],
    sanitize: true,
    toolbarPosition: 'top',
    toolbarHiddenButtons: [['bold', 'italic'], ['fontSize']],
  };

  gameList: any = [];
  selectedGame: string = '';
  genreList: any = [];
  selectedGenre: string = '';
  categoryList: any = [];
  selectedCategory: string = '';
  selectedCategoryId: number;
  selectedGenreId: number;
  selectedPlatformId: number;
  selectedTagId: number;
  minReadList: any = [
    { _id: 2, name: '2 Minutes' },
    { _id: 3, name: '3 Minutes' },
    { _id: 4, name: '4 Minutes' },
    { _id: 5, name: '5 Minutes' },
    { _id: 6, name: '6 Minutes' },
    { _id: 7, name: '7 Minutes' },
  ];
  selectedMinRead: string = '';
  selectedMinReadId: number;
  tagList: any = [];
  selectedTag: string = '';
  platformList: any = [];
  selectedPlatform: string = '';

  clicked: boolean = false;
  articleData: any;
  isEdit: boolean = false;
  articleId: string = '';
  ArticleSlugId: string = '';
  articleStatus: string;
  headerTitle: string = 'CREATE_ARTICLE.HEADER_MAIN_TITLE';
  showLoader: boolean = false;

  constructor(
    public toastService: EsportsToastService,
    private translateService: TranslateService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private location: Location,
    public s3Service: S3UploadService,
    private dialog: MatDialog,
    private articlesService: EsportsArticleService,
    private _esportsOptionService: EsportsOptionService,
    private _esportsGameService: EsportsGameService,
    private articleService: EsportsArticleService,
    private fb: FormBuilder,
  ) { }

  ngOnInit(): void {
    const url: string = this.router.url;
    if (url.includes('edit')) {
      this.isEdit = true;
      this.headerTitle = 'CREATE_ARTICLE.HEADER_EDIT_TITLE';
    }
    this.articleId = this.activatedRoute.snapshot.params['id'];
    this.createForm();
    this.getAllGame();
    this.getAllGenres();
    this.getAllCategories();
    this.getAllTags();

  }

  createForm() {
    this.createMemberForm = this.fb.group({
      teamBanner: ['', Validators.compose([Validators.required])],
      shortDescription: [[], Validators.compose([])],
      articleTitle: [[], Validators.compose([])],
      content: ['', Validators.compose([])]
    });
  }

  async handleFileInputBanner(files: FileList) {
    this.fileToUploadBanner = files.item(0);
    let fileDiamention: any = { width: 1600, height: 900 };
    const upload: any = await this.s3Service.isValidImage(
      files,
      fileDiamention,
      'banner'
    );
    if (upload && upload == 'invalid_size') {
      this.toastService.showError(
        this.translateService.instant('TOURNAMENT.ERROR_IMG_2')
      );
      return;
    } else if (upload && upload == 'valid_image') {
      this.toastService.showError(
        this.translateService.instant('TOURNAMENT.ERROR_IMG_1')
      );

      const file = files[0];
      const reader = new FileReader();
      reader.readAsDataURL(file);
      let th = this;
      reader.onload = (e) => {
        let image: any = new Image();
        image.src = e.target.result;
        image.onload = function () {
          let height = this.height;
          let width = this.width;
          if (
            height != fileDiamention['height'] &&
            width != fileDiamention['width']
          ) {
            th.toastService.showInfo(
              th.translateService.instant('TOURNAMENT.ERROR_IMG')
            );
          }
        };
      };

      this.upload(files, 'banner');
    }
  }

  async upload(files, type: string = 'banner') {
    let promises = [];
    for (let obj of files) {
      promises.push(this.toBase64(obj));
    }
    let Base64String = await Promise.all(promises);
    let imageData = {
      path: environment.tournamentS3BucketName,
      files: Base64String,
    };
    this.s3Service.fileUpload(environment.apiEndPoint, imageData).subscribe(
      (res) => {
        this.teamBannerSRCPost = '';
        this.teamBannerSRCPost = res['data'][0]['Location'];
        this.fileToUploadBanner = files.item(0);

        this.toastService.showSuccess(
          this.translateService.instant('FILE_UPLOAD.SUCCESS')
        );
      },
      (err) => {
        this.toastService.showError(
          this.translateService.instant('FILE_UPLOAD.ERROR')
        );
      }
    );
  }

  toBase64(file) {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = (error) => reject(error);
    });
  }

  onLanguageChange(item) {
    this.selectedLanguage = item.name;
  }

  getAllGame() {
    this._esportsGameService.getGames(API).subscribe(
      (res) => {
        this.gameList = res['data'];
        if (this.articleId) {
          this.getArticle(this.articleId);
        }
      },
      (err) => { }
    );
  }

  onGameChange(item) {
    this.selectedGame = item.name;
    this.platformList = item.platform;
  }

  getAllGenres = async () => {
    const option = await this._esportsOptionService.fetchAllGenres(API);
    if (option) {
      this.genreList = option?.data;
    }
  };

  onGenreChange(item) {
    this.selectedGenre = item.name;
    this.selectedGenreId = item._id;
  }

  getAllCategories = async () => {
    try {
      const option = await this._esportsOptionService.fetchAllCategories(API);
      if (option) {
        this.categoryList = option.data;
      }
    } catch (error) { }
  };

  onCategoryChange(item) {
    this.selectedCategory = item.name;
    this.selectedCategoryId = item._id;
  }

  onMinToReadChange(item) {
    this.selectedMinRead = item.name;
    this.selectedMinReadId = item._id;
  }

  getAllTags = async () => {
    const option = await this._esportsOptionService.fetchAllTags(API);
    if (option) {
      this.tagList = option?.data;
    }
  };

  onTagChange(item) {
    this.selectedTag = item.name;
    this.selectedTagId = item._id;
  }

  onPlatformChange(item) {
    this.selectedPlatform = item.name;
    this.selectedPlatformId = item._id;
  }

  getArticle(id) {
    this.articleService.getArticleByID(API, id).subscribe(
      (res) => {
        let data = res['data'][0];
        this.createMemberForm.patchValue({
          teamBanner: [''],
          shortDescription: [data.shortDescription.english],
          articleTitle: [data.title.english],
          content: [data.content.english]
        });
        this.teamBannerSRCFile = data.image;
        this.articleData = data;
        this.ArticleSlugId = data.slug;
        // this.fileToUploadBanner['name'] = data.image;
        this.selectedLanguage = 'English';
        const gameDetails = this.gameList.filter(x => x.name === data.game);
        this.setPlatformList(gameDetails[0]?._id, this.gameList);
        const genre = this.genreList.filter(x => x._id === data.genre[0]);
        const tag = this.tagList.filter(x => x._id === data.tags[0]);
        const category = this.categoryList.filter(x => x._id === data.category);
        const platforms = this.platformList.filter(x => x._id === data.platform[0]);
        const minRd = this.minReadList.filter(x => x._id === data.minRead);
        this.selectedGenre = genre[0]?.name;
        this.selectedGenreId = genre[0]?._id;
        this.selectedTag = tag[0]?.name;
        this.selectedTagId = tag[0]?._id;
        this.selectedCategory = category[0]?.name;
        this.selectedCategoryId = category[0]?._id;
        this.selectedPlatform = platforms[0]?.name;
        this.selectedPlatformId = platforms[0]?._id;
        this.selectedGame = data?.game;
        this.selectedMinRead = minRd[0]?.name;
        this.selectedMinReadId = minRd[0]?._id;
        this.articleStatus = data?.articleStatus;
        // this.articleForm.addControl('id', new FormControl(id));
        // const category = data.category;
        // const tag = data.tags.map((item) => item._id);
        // const platforms = data.platform.map((item) => item._id);
        // const genres = data.genre.map((item) => item._id);
        // const gameDetails = data.gameDetails._id;
        // this.setPlatformList(gameDetails, this.gameList);
        // this.articleForm.patchValue({
        //   ...data,
        //   category,
        //   tag,
        //   platforms,
        //   genres,
        //   gameDetails,
        // });
        // this.bannerSRC = data['image'];
        // this.s3RefKey = data['s3RefKey'];
        // if (data['createdBy'] != this.user?._id) {
        //   if (this.user?.accountType != 'admin') {
        //     this.router.navigate(['/article']);
        //   }
        //   if (this.user?.accountType == 'admin') {
        //     this.display_user = data?.authorDetails?.fullName;
        //     this.userImageSrc = data?.authorDetails?.profilePicture;
        //   }
        // } else {
        //   this.display_user = data?.authorDetails?.fullName;
        //   this.userImageSrc = data?.authorDetails?.profilePicture;
        // }
      },
      (err) => { }
    );
  }

  setPlatformList(game, gameList) {
    if (gameList.length && game) {
      for (const iterator of gameList) {
        if (iterator._id == game) {
          this.platformList = iterator.platform;
        }
      }
    }
  }

  createArticle() {
    const Data: EsportsInfoPopupComponentData = {
      title: this.translateService.instant('HEADER_DYNASTY_ARTICLE.ARTICLE_CREATED'),
      text: this.translateService.instant('HEADER_DYNASTY_ARTICLE.ARTICLE_CREATED_TEXT'),
      type: EsportsInfoPopupComponentType.info,
      btnText: this.translateService.instant('HEADER_DYNASTY_ARTICLE.BACK'),
      isCancelledRequired: false
    };
    const dialogRef = this.dialog.open(EsportsInfoPopupComponent, {
      data: Data,
      maxWidth: 423,
      maxHeight: 296,
      hasBackdrop: true,
    });
    dialogRef.afterClosed().subscribe((confirmed) => {
      if (confirmed) {
        if (this.articleId) {
          const postData: any = {
            image: this.teamBannerSRCPost,
            title: { "english": this.createMemberForm.value.articleTitle, "arabic": "" },
            shortDescription: { "english": this.createMemberForm.value.shortDescription, "arabic": "" },
            content: { "english": this.createMemberForm.value.content, "arabic": "" },
            game: (this.selectedGame) ? this.selectedGame : '',
            genre: (this.selectedGenre) ? [this.selectedGenreId] : [],
            category: (this.selectedCategoryId) ? this.selectedCategoryId : 0,
            minRead: (this.selectedMinReadId) ? this.selectedMinReadId : '',
            tags: (this.selectedTag) ? [this.selectedTagId] : [],
            platform: (this.selectedPlatform) ? [this.selectedPlatformId] : [],
            articleStatus: this.articleStatus,
          }
          this.articlesService.updateArticle(API, this.articleId, postData).subscribe(
            (res: any) => {
              // this.showLoader = false;
              if (status == 'draft') {
                this.toastService.showSuccess(
                  this.translateService.instant('ARTICLE_POST.DRAFT')
                );
              } else {
                this.toastService.showSuccess(
                  this.translateService.instant('ARTICLE_POST.SAVE')
                );
              }

              this.router.navigate(['/profile/content']);
            },
            (err) => {
              // this.showLoader = false;
              this.toastService.showError(
                `${this.translateService.instant('ARTICLE_POST.ERROR')},
                  ${err['error']['error']}`
              );
            }
          );
        } else {
          const postData: any = {
            image: this.teamBannerSRCPost,
            title: { "english": this.createMemberForm.value.articleTitle, "arabic": "" },
            shortDescription: { "english": this.createMemberForm.value.shortDescription, "arabic": "" },
            content: { "english": this.createMemberForm.value.content, "arabic": "" },
            game: (this.selectedGame) ? this.selectedGame : '',
            genre: (this.selectedGenre) ? [this.selectedGenreId] : [],
            category: (this.selectedCategoryId) ? this.selectedCategoryId : 0,
            minRead: (this.selectedMinReadId) ? this.selectedMinReadId : '',
            tags: (this.selectedTag) ? [this.selectedTagId] : [],
            platform: (this.selectedPlatform) ? [this.selectedPlatformId] : [],
            articleStatus: "submitted_for_approval"
          }
          this.articlesService.saveArticle(API, postData).subscribe(
            (res: any) => {
              // this.showLoader = false;
              if (status == 'draft') {
                this.toastService.showSuccess(
                  this.translateService.instant('ARTICLE_POST.DRAFT')
                );
              } else {
                this.toastService.showSuccess(
                  this.translateService.instant('ARTICLE_POST.SAVE')
                );
              }

              this.router.navigate(['/profile/content']);
            },
            (err) => {
              // this.showLoader = false;
              this.toastService.showError(
                `${this.translateService.instant('ARTICLE_POST.ERROR')},
                  ${err['error']['error']}`
              );
            }
          );
        }
      }
    });
  }

  back(data) {
    this.router.navigateByUrl('profile/content');
  }
  close(data) {
    this.router.navigateByUrl('profile/content');
  }

  leftAction(data) {
    if (data && data == 'delete') {
      this.showLoader = true;
      let deleteArticleVo = { articleStatus: "deleted" };
      this.articlesService.updateArticle(API, this.articleId, deleteArticleVo).subscribe(
        (res: any) => {
          this.showLoader = false;
          this.toastService.showError(
            this.translateService.instant('ARTICLE_POST.SUCCESS_DELETE')
          );
          this.router.navigate(['/profile/content']);
        },
        (err) => {
          this.showLoader = false;
          this.toastService.showError(
            this.translateService.instant('ARTICLE_POST.ERROR_DELETE')
          );
        }
      );
    } else {
      this.router.navigateByUrl(AppHtmlRoutes.article + '/' + this.ArticleSlugId);
    }
  }
}
