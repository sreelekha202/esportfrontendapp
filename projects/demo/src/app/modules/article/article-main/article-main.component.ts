import { Component, OnInit } from '@angular/core';
import {
  AppHtmlRoutes,
  AppHtmlRoutesArticleMoreType,
} from '../../../app-routing.model';
import { TranslateService } from '@ngx-translate/core';
import { EsportsUserService, EsportsArticleService, EsportsLanguageService, EsportsOptionService, IUser, EsportsUtilsService, IPagination } from 'esports';
import { environment } from "../../../../environments/environment";
const API = environment.apiEndPoint;

@Component({
  selector: 'app-article-main',
  templateUrl: './article-main.component.html',
  styleUrls: ['./article-main.component.scss'],
})
export class ArticleMainComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;
  AppHtmlRoutesArticleMoreType = AppHtmlRoutesArticleMoreType;
  showLoader: boolean = true;
  user: IUser;
  hottestArticle = null;
  latestArticle = [];
  trendingArticles = [];
  articles: any = [];
  searchFilterForm = [
    {
      title: 'Select game',
      fields: [{ label: 'Select games', value: 'all' }],
    },
    {
      title: 'Article type',
      fields: [{ label: 'All', value: 'all' }],
    },
  ];
  categoryList: any = [];
  paginationData = {
    page: 1,
    limit: 20,
    sort: "-views",
  };
  page: IPagination;
  participantPage: IPagination = {
    activePage: 1,
    totalItems: 1,
    maxSize: 10,
    itemsPerPage: 10,
  };
  gameArray = [];
  selectedCategory: any = 'all';
  constructor(
    private articleService: EsportsArticleService,
    private userService: EsportsUserService,
    public language: EsportsLanguageService,
    public translateService: TranslateService,
    public utilsService: EsportsUtilsService,
    private optionService: EsportsOptionService,
    private articleApiService: EsportsArticleService,
  ) { }

  ngOnInit(): void {
    this.getHottestArticle();
    this.getCurrentUserDetails();
    this.getLatestPost();
    this.getAllCategory();
    this.getRecentPost('');
    this.getAllCategoryList();
  }
  async getAllCategory() {

    const option = await Promise.all([this.optionService.fetchAllCategories(API)]);
    if (option[0]?.data) {
      let categoryList = option[0]?.data;
      categoryList.map((cat: any) => {
        this.categoryList.push({ [cat._id]: cat });
      })
      // this.getRecentPost();
    }
  }

  getAllCategoryList = async () => {
    const res = await this.optionService.fetchAllCategories(API);
    
    this.categoryList = [{ name: 'All Updates', _id: 'all' }, ...res?.data];
    
  };
  getRecentPost(id) {
    if (id) {
      let isChecked = id?.target?.checked ? true : false;
      const gameid = { gameDetails: id?.target?.value };
      if (isChecked && id !== '') {
        this.gameArray.push(gameid);
      } else {
        for (let i = 0; i < this.gameArray.length; i++) {
          if (JSON.stringify(this.gameArray[i]) == JSON.stringify(gameid)) {
            this.gameArray.splice(i, 1);
            if (this.gameArray == []) {
              id = '';
            }
          }
        }
      }
    }
      const pagination = JSON.stringify({
      page: this.participantPage.activePage,
      limit: this.participantPage.itemsPerPage,    
      sort: { createdDate: -1 },
    });
    let query: any = { articleStatus: 'publish' };
    const preference = JSON.stringify({
      prefernce: this.gameArray,
    });
    this.showLoader = true;
    let param: any = {
      pagination: pagination,
      query: JSON.stringify(query),
      prefernce: preference,
    };
    this.selectedCategory != 'all'
      ? (param.category = JSON.stringify({ category: this.selectedCategory }))
      : '';

    param.category ? param.category : delete param.category;
    this.articleApiService
      .getLatestArticleAll(API, param).subscribe(
      (res: any) => {
        this.articles = res['data']['docs'];
        this.showLoader = false;
        this.participantPage.itemsPerPage = res?.data?.limit;
        this.participantPage.totalItems = res?.data?.totalDocs;
      },
      (err) => {
        this.showLoader = false;
      }
    );
  }


  getLatestPost() {
    const pagination = JSON.stringify({
      page: this.paginationData.page,
      limit: 20,
      sort: { createdDate: -1 },
    });
    const query = JSON.stringify({
      articleStatus: "publish",
    });

    const perfernce = JSON.stringify({
      prefernce: this.user?.preference?.game
        ? this.user?.preference?.game.map((item) => {
          return { gameDetails: item };
        })
        : "",
    });
    this.showLoader = true;
    this.articleApiService
      .getLatestArticle(API, {
        pagination: pagination,
        query: query,
        preference: perfernce,
      })
      .subscribe(
        (res: any) => {
          this.trendingArticles = res["data"]["docs"];
          this.showLoader = false;
          this.page = {
            totalItems: res?.data?.totalDocs,
            itemsPerPage: res?.data?.limit,
            maxSize: 5,
          };
        },
        (err) => {
          this.showLoader = false;
        }
      );
  }

  getHottestArticle() {
    this.articleService.getArticleList(API).subscribe(
      (res) => {
        // MOCK HOTTEST ARTICLE TYPE
        this.hottestArticle = { ...res['data'][0], type: 'guide' };
        this.showLoader = false;
      },
      (err) => {
        this.showLoader = false;
      }
    );
  }

  getCurrentUserDetails() {
    this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.user = data;
      }
    });
  }

  getArticleIconByType(type: string): string {
    const path = 'assets/icons/news';
    switch (type) {
      case 'feature': return `${path}/feature.svg`;
      case 'opinion': return `${path}/opinion.svg`;
      case 'guide': return `${path}/guide.svg`;
      case 'listicle': return `${path}/listicle.svg`;
      case 'hot': return `${path}/hot.svg`;
      case 'trending': return `${path}/trending.svg`;
    }
  }
}
