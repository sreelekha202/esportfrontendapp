import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TermsOfUsComponent } from './terms-of-us.component';

describe('TermsOfUsComponent', () => {
  let component: TermsOfUsComponent;
  let fixture: ComponentFixture<TermsOfUsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TermsOfUsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TermsOfUsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
