import { Component, OnInit, ViewChild, HostListener } from '@angular/core';
import { AppHtmlRoutes } from '../../../app-routing.model';
import { TranslateService } from '@ngx-translate/core';
import { NgbCarousel, NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';
import { EsportsHomeService, GlobalUtils } from 'esports';
import { Router } from '@angular/router';

@Component({
  selector: 'app-preview-slider',
  templateUrl: './preview-slider.component.html',
  styleUrls: ['./preview-slider.component.scss'],
  providers: [NgbCarouselConfig],
})
export class PreviewSliderComponent implements OnInit {
  @ViewChild('carousel') carousel: NgbCarousel;

  AppHtmlRoutes = AppHtmlRoutes;

  slides = [];

  constructor(
    private translate: TranslateService,
    private homeService: EsportsHomeService,
    private config: NgbCarouselConfig,
    private route: Router
  ) {
    this.config.interval = 0;
    this.config.animation = true;
    this.config.showNavigationArrows = false;
    this.bannerData();
  }

  ngOnInit(): void {}

  createtournament(destination) {
    this.route.navigate([destination]);
  }
  bannerData() {
    this.homeService._getBanner().subscribe(
      (res) => {
        this.slides = res?.data;
      },
      (err) => {}
    );
  }

  scroll() {
    if (GlobalUtils.isBrowser()) {
      document.getElementById('scroll_btn').scrollIntoView({
        behavior: 'smooth',
      });
    }
  }
}
