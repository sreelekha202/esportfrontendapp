import { Component, OnInit, ViewChild } from '@angular/core';
import { AppHtmlRoutes } from '../../../app-routing.model';
import {
  EsportsLanguageService,
  EsportsUserService,
  EsportsHomeService,
} from 'esports';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
import { NgbCarousel, NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';

@AutoUnsubscribe()
@Component({
  selector: 'app-upcoming-slider',
  templateUrl: './upcoming-slider.component.html',
  styleUrls: ['./upcoming-slider.component.scss'],
  providers: [NgbCarouselConfig],
})
export class UpcomingSliderComponent implements OnInit {
  @ViewChild('carousel') carousel: NgbCarousel;
  AppHtmlRoutes = AppHtmlRoutes;
  tournamentsSlides = [];
  userSubscription: Subscription;
  currentLang: string = 'english';
  currentSlide = null;
  currentUser: any;

  constructor(
    private config: NgbCarouselConfig,
    private homeService: EsportsHomeService,
    private translate: TranslateService,
    public language: EsportsLanguageService,
    private userService: EsportsUserService
  ) {
    this.config.interval = 0;
    this.config.animation = false;
    this.currentLang = translate.currentLang == 'ms' ? 'malay' : 'english';

    translate.onLangChange.subscribe((event: LangChangeEvent) => {
      this.currentLang = event.lang == 'ms' ? 'malay' : 'english';
    });
  }

  ngOnInit(): void {
    this.getTournamentsSlides();
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.currentUser = data;
      }
    });
  }

  getTournamentsSlides() {
    if (this.currentUser) {
      this.homeService.getUpcomingSliderTournament().subscribe(
        (res) => {
          this.tournamentsSlides = res.data;
          this.currentSlide = this.tournamentsSlides[0];
        },
        (err) => {}
      );
    }
  }

  setCurrentSlide(event): void {
    const index = this.getSlideIndex(event.current);
    this.currentSlide = this.tournamentsSlides[index];
  }

  getSlideIndex(str: string): number {
    return +str.replace(/[^0-9]/g, '');
  }
  ngOnDestroy(): void {
    if (this.userSubscription) this.userSubscription.unsubscribe();
  }
}
