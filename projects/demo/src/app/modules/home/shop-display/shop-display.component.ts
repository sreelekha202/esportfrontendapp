import { Component, OnInit } from '@angular/core';

import {
  AppHtmlRoutes,
} from '../../../app-routing.model';
@Component({
  selector: 'shop-display',
  templateUrl: './shop-display.component.html',
  styleUrls: ['./shop-display.component.scss']
})
export class shopDisplayComponent implements OnInit {

  AppHtmlRoutes = AppHtmlRoutes;
  constructor() { }

  ngOnInit(): void {
  }

}