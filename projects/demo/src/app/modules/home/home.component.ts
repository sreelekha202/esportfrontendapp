import { Router } from '@angular/router';
import { Component, Inject, Input, OnInit, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import {
  EsportsHomeService,
  EsportsToastService,
  IUser,
  IPagination,
  EsportsLeaderboardService,
  EsportsUserPreferenceService,
  EsportsTournamentService,
  EsportsOptionService,
  EsportsUserService,
  EsportsUtilsService,
  EsportsArticleService,
  EsportsSeasonService,
  EsportsGameService,
  GlobalUtils,
} from 'esports';

import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';
import { environment } from '../../../environments/environment';
import { tokenName } from '@angular/compiler';
import { AppHtmlGetMatchRoutes, AppHtmlRoutes } from '../../app-routing.model';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';
import { LeaderBoard, LeaderboardTableComponent } from '../leaderboard/leaderboard-table/leaderboard-table.component';
const API = environment.apiEndPoint;
@AutoUnsubscribe()
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;
  AppHtmlGetMatchRoutes = AppHtmlGetMatchRoutes;
  showLoader: boolean = true;
  isBrowser: boolean;
  currentUser: IUser;
  userSubscription: Subscription;
  categoryList;
  categoryId;
  tournaments = [];
  featuredTournaments = [];
  featuredSeasons = [];
  prizeTournaments = [];
  // FEATURED CONTENT
  trendingNews = [];
  selectedCountryName = '';
  spoLightList: any;
  params: any = {};
  seassons = [];
  games = [];
  text = '';
  gameId: any;
  isGameId: any;
  paginationDetails: any;
  participantPage: IPagination = {
    activePage: 1,
    totalItems: 1,
    maxSize: 10,
    itemsPerPage: 10,
  };
  selectedGames = 'all';
  selectedGame: any = { all: true };
  pageSizeOptions = environment.pageSizeOptions;
  prefData = [];
  prefrenceData;
  paginationData = {
    page: 1,
    limit: 10,
    sort: {"startDate":-1},
  };
  @Input() data: LeaderBoard = {
    page: 'leaderboard',
    title: 'All Games',
  };
  teamFlag: boolean = false;
  tournamentType:any = 'tournament';
  ongoingTournaments = [];
  istTournament: any;
  sortedArray;
  gamesShort;
  leaderboardData: any = [];
  constructor(
    @Inject(PLATFORM_ID) private platformId,
    private articleService: EsportsArticleService,
    private optionService: EsportsOptionService,
    private userService: EsportsUserService,
    public translateService: TranslateService,
    public utilsService: EsportsUtilsService,
    private leaderboardService: EsportsLeaderboardService,
    private toastService: EsportsToastService,
    private router: Router,
    private homeService: EsportsHomeService,
    private esportsSeasonService: EsportsSeasonService,
    private gameService: EsportsGameService,
    private esportsTournamentService: EsportsTournamentService
  ) {
    this.isBrowser = isPlatformBrowser(platformId);
  }

  ngOnInit(): void {
    this.getUserData();
    if (GlobalUtils.isBrowser()) {
      this.gameId = localStorage.getItem('gameId');
      if (this.gameId) {
        this.isGameId = true;
      } else {
        this.isGameId = false;
      }
    }
    this.getAllTournaments();
    this.getSpotlightList();
    // this.fetchOptions();
    this.getGames();
    this.getSeasons();
    this.filterLeaderboard();
  }

  getAllTournaments() {
    this.esportsTournamentService.getPaginatedTournaments(API, this.paginationData).subscribe(
      (res: any) => {
        this.tournaments = res?.data?.docs;
        this.istTournament = this.tournaments;
        this.setFeaturedTournaments();
      },
      (err: any) => {
        this.showLoader = false;
      }
    );
  }

  getSeasons() {
    this.esportsSeasonService
      .getSeasons(
        API,
        'isActive=true&fields=isFeatured,name,slug,endDate,startDate,image,duration&sort=-createdOn&page=1&limit=4'
      )
      .subscribe((res) => {
        this.seassons = res?.data?.data?.docs;
      });
  }

  getGames() {
    this.showLoader = true;
    this.gameService.getGames(API).subscribe(
      (res) => {
        this.games = res?.data.splice(0, 4);
        // this.getFeaturedGames(this.games);
        this.showLoader = false;
      },
      (err) => {
        this.showLoader = false;
      }
    );
  }

  // getFeaturedGames(allGamesList) {
  //   const featuredGames = [];
  //   for (let i = 0; i < allGamesList.length; i++) {
  //     if (allGamesList[i]?.name == 'Valorant') {
  //       featuredGames[0] = allGamesList[i];
  //     }

  //     if (allGamesList[i]?.name == 'League of Legends') {
  //       featuredGames[1] = allGamesList[i];
  //     }

  //     if (allGamesList[i]?.name == 'DOTA 2') {
  //       featuredGames[2] = allGamesList[i];
  //     }

  //     if (allGamesList[i]?.name == 'CS:GO') {
  //       featuredGames[3] = allGamesList[i];
  //     }
  //     if (
  //       this.games[i]?.name != 'Valorant' &&
  //       this.games[i]?.name != 'League of Legends' &&
  //       this.games[i]?.name != 'Halo MCC' &&
  //       this.games[i]?.name != 'World of Warcraft'
  //     ) {
  //       b[i] = this.games[i];
  //     }
  //   }
  //   this.gamesShort = featuredGames;
  // }

  async getTournaments() {
    this.showLoader = true;
    const params: any = {
      limit: 4,
      page: this.paginationData?.page,
      status: 1,
    };

    try {
      if (this.currentUser) {
        const tournament =
          await this.esportsTournamentService.fetchMyTournament(params);
        if (tournament) {
          this.ongoingTournaments = tournament?.data?.docs;
          this.paginationDetails = tournament?.data;
          this.paginationData.limit = this.paginationDetails?.limit;
          this.paginationData.page = this.paginationDetails?.page;
          this.showLoader = false;
        }
      }
    } catch (error) {
      this.showLoader = false;
    }
  }

  getUserData(): void {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.currentUser = data;
        this.getTournaments();
        this.currentUser?.preference?.game.forEach((game) => {
          this.prefData.push({ gameDetails: game });
        });
        const preference = JSON.stringify({
          prefernce: this.prefData,
        });

        const pagination = JSON.stringify({
          pagination: this.paginationData,
        });
        let param: any = {
          pagination: pagination,
          prefernce: preference,
        };

        this.homeService.getTournament(param).subscribe(
          (res: any) => {
            this.prefrenceData = res?.data?.docs.slice(0, 4);
          },
          (err: any) => {
            this.showLoader = false;
          }
        );
      }
    });
  }

  // fetchOptions = async () => {
  //   this.showLoader = true;
  //   const option = await Promise.all([
  //     this.optionService.fetchAllCategories(API),
  //   ]);
  //   this.categoryList = option[0]?.data;
  //   for (const iterator of this.categoryList) {
  //     if (iterator?.name == 'News') {
  //       this.categoryId = iterator?._id;
  //     }
  //   }
  //   this.showLoader = false;
  //   // this.getArticleNews(this.categoryId);
  // };

  // getArticleNews(categoryId) {
  //   this.showLoader = true;
  //   const query = JSON.stringify({
  //     articleStatus: 'publish',
  //     category: categoryId,
  //   });
  //   const option = JSON.stringify({ limit: 7, sort: { createdDate: -1 } });
  //   this.articleService.getArticles_PublicAPI(API, { query, option }).subscribe(
  //     (res: any) => {
  //       let trendingNews = res?.data.slice(0, 6);
  //       trendingNews.map((res) => {
  //         if (res?.isFeature) {
  //           this.trendingNews.push(res);
  //         }
  //       });
  //       this.showLoader = false;
  //     },
  //     (err) => {
  //       this.showLoader = false;
  //     }
  //   );
  // }
  getSpotlightList() {
    this.articleService.getTrendingPosts(API).subscribe((res: any) => {
      this.spoLightList = res?.data;
    });
  }

  followUser(apiEndPoint, followUserId) {
    if (followUserId && followUserId?._id && this.currentUser) {
      this.showLoader = true;
      this.leaderboardService
        .followUser(apiEndPoint, followUserId)
        .subscribe((res) => {
          if (res?.success) {
            this.toastService.showSuccess(res?.message);
            this.userService.refreshCurrentUser(API, tokenName);
          }
          this.showLoader = true;
        });
    } else {
      this.router.navigateByUrl('/user/email-login');
    }
  }

  setFeaturedTournaments() {
    this.featuredTournaments = [];
    this.featuredSeasons = [];
    this.prizeTournaments = [];
    this.tournaments.map((item) => {
      if (this.isGameId && item?.isFeature) {
        item?.gameDetail?._id == this.gameId
          ? this.featuredTournaments.push(item)
          : '';
      } else if (item?.isFeature) {
        this.featuredTournaments.push(item);
      }
      item?.isFeature ? this.featuredSeasons.push(item) : '';
      item?.isPrize ? this.prizeTournaments.push(item) : '';
    });
  }
  ngOnDestroy(): void {
    if (this.userSubscription) this.userSubscription.unsubscribe();
  }
  filterLeaderboard(page = 1) {
    this.teamFlag = false;
    this.selectedGame = {};
    this.selectedGame[this.selectedGames] = true;
    this.showLoader = true;

    const params = {
      country: this.selectedCountryName,
      gameId: this.selectedGames,
      limit: this.paginationData.limit,
      page: page,
      state: null,
    };

    if (this.data.page === 'home') {
      this.homeService._leaderBoard().subscribe((data) => {
        this.showLoader = false;
        this.leaderboardData = data.data.leaderboardData;

        if (this.leaderboardData) {
          this.leaderboardData.forEach((obj, index) => {
            obj.rank = index + 1;
          });
        }
      });
    } else {
      this.leaderboardService.getGameLeaderboard(API, params).subscribe(
        (res: any) => {
          this.showLoader = false;
          this.leaderboardData = [];

          if (res.data.docs) {
            let count = 0;
            let userData: any = {};
            let leaderboardDataVo: any = res.data.docs;
            /* sort gold by decending */
            leaderboardDataVo.sort((a, b) => {
              return b.firstPosition - a.firstPosition;
            });

            for (const data of leaderboardDataVo) {
              if (data.user.length > 0) {
                userData.id = data._id.user;
                userData.rank = count + 1;
                count++;
                userData.img =
                  data.user[0].profilePicture == ''
                    ? './assets/images/Profile/formation.svg'
                    : data.user[0].profilePicture;
                userData.name = data.user[0].username;
                userData.region = data.user[0].country
                  ? data.user[0].country
                  : '';
                userData.trophies = {};
                userData.trophies.first = data.firstPosition;
                userData.trophies.second = data.secondPosition;
                userData.trophies.third = data.thirdPosition;
                userData.points = data.points;
                userData.amount = data.amountPrice;
                userData.gamesCount = data.gamesCount;
                this.paginationDetails = res?.data;
                this.participantPage.itemsPerPage =
                  this.paginationDetails?.limit;
                this.participantPage.totalItems =
                  this.paginationDetails?.totalDocs;
                this.leaderboardData.push({ ...userData });
                userData = {};
              } else {
                this.participantPage.itemsPerPage =
                  this.paginationDetails?.limit;
                this.participantPage.totalItems = 0;
              }
            }
          }
        },
        (err) => {
          this.showLoader = false;
        }
      );
    }
  }
}
