import { Component, OnInit } from '@angular/core';
import { AppHtmlRoutes } from '../../../app-routing.model';
import { Input } from '@angular/core';

@Component({
  selector: 'app-tournament-card-home',
  templateUrl: './tournament-card-home.component.html',
  styleUrls: ['./tournament-card-home.component.scss'],
})
export class TournamentCardHomeComponent implements OnInit {
  @Input() params;
  @Input() seasson;
  @Input() ongoingTournament;
  @Input() type;
  AppHtmlRoutes = AppHtmlRoutes;
  constructor() {}

  ngOnInit(): void {}
}
