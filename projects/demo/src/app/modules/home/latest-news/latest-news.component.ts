import { Component, OnInit } from '@angular/core';
import {
  EsportsLanguageService,
  EsportsOptionService,
  EsportsArticleService,
} from 'esports';
import { AppHtmlRoutes } from '../../../app-routing.model';
import { environment } from '../../../../environments/environment';
const API = environment.apiEndPoint;

@Component({
  selector: 'app-latest-news',
  templateUrl: './latest-news.component.html',
  styleUrls: ['./latest-news.component.scss'],
})
export class LatestNewsComponent implements OnInit {
  hottestPost: any = [];
  categoryList: any;
  categoryId: any;
  AppHtmlRoutes = AppHtmlRoutes;
  constructor(
    public languageService: EsportsLanguageService,
    private articleService: EsportsArticleService,
    private optionService: EsportsOptionService
  ) {}

  ngOnInit(): void {
    this.fetchOptions();
  }

  fetchOptions = async () => {
    const option = await Promise.all([
      this.optionService.fetchAllCategories(API),
    ]);
    this.categoryList = option[0]?.data;
    for (const iterator of this.categoryList) {
      if (iterator.name == 'News') {
        this.categoryId = iterator?._id;
      }
    }
    this.getNews(this.categoryId);
  };

  getNews(categoryId) {
    const query = JSON.stringify({
      articleStatus: 'publish',
      category: categoryId,
    });
    const option = JSON.stringify({ limit: 4, sort: { createdDate: -1 } });
    this.articleService.getArticles_PublicAPI(API, { query, option }).subscribe(
      (res: any) => {
        if (res?.data?.length > 3) {
          res.data = res?.data.slice(0, 3);
        }
        this.hottestPost = res?.data;
      },
      (err) => {}
    );
  }
}
