import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { EsportsUserService } from 'esports';
import { environment } from '../../../../../environments/environment';

const API = environment.apiEndPoint;

@Component({
  selector: 'app-team-card',
  templateUrl: './team-card.component.html',
  styleUrls: ['./team-card.component.scss'],
})
export class TeamCardComponent implements OnInit {
  @Input() isManage: boolean = false;
  @Input() data;
  myTeams: '/my-teams'[];

  constructor(private router: Router, private userService: EsportsUserService) { }

  ngOnInit(): void { }

  deleteTeam(team_id) {

    const data = {
      "id": team_id,
      "admin": true,
      "query": {
        "condition": { "_id": team_id, "status": "active" },
        "update": { "status": "inactive" }
      }
    };
    this.userService.update_team_admin(API, data).subscribe((data: any) => {
      let res = JSON.stringify(data)
      this.router.navigate(['profile/my-teams']);
    });
  }

}
