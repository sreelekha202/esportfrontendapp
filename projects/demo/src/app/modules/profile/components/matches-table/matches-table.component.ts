import { Component, OnInit, Input, ChangeDetectorRef, IterableDiffers } from '@angular/core';

@Component({
  selector: 'app-matches-table',
  templateUrl: './matches-table.component.html',
  styleUrls: ['./matches-table.component.scss'],
})
export class MatchesTableComponent implements OnInit {
  @Input() matches: Array<any> = [];
  constructor() { }
  ngOnInit(): void {
  }

}
