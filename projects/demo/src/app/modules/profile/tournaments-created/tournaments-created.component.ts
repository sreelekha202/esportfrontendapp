import { Component, OnInit } from '@angular/core';
import { EsportsTournamentService, IPagination } from 'esports';
import { environment } from '../../../../environments/environment';
@Component({
  selector: 'app-tournaments-created',
  templateUrl: './tournaments-created.component.html',
  styleUrls: ['./tournaments-created.component.scss'],
})
export class TournamentsCreatedComponent implements OnInit {
  tournaments:any;
  showLoader: boolean = true;
  active = 1;
  nextId = 1;
  paginationDetails: any;
  pageSizeOptions = environment.pageSizeOptions;
  paginationData = {
    page: 1,
    limit: 10,
    sort: 'startDate',
  };
  participantPage: IPagination = {
    activePage: 1,
    totalItems: 1,
    maxSize: 10,
    itemsPerPage: 10,
  };


  constructor(private esportsTournamentService: EsportsTournamentService) {}

  ngOnInit(): void {
    this.getTournaments();
  }

  navChanges = (e) => {
    this.nextId = e.nextId;
    this.getTournaments();
  };

  getTournaments = async () => {
    this.showLoader = true;
    const params: any = {
      limit: this.participantPage.itemsPerPage,
      page: this.participantPage.activePage,
      status: this.nextId,
    };
    try {
      const tournament = await this.esportsTournamentService.fetchMyTournament(
        params
      );
      if (tournament) {
        this.tournaments = tournament?.data;
        this.participantPage.itemsPerPage = this.tournaments?.limit;
        this.participantPage.totalItems = this.tournaments?.totalDocs;
        // this.paginationData.page = this.paginationDetails.page;
        this.showLoader = false;
      }
    } catch (error) {}
  };

  pageChanged(page): void {
    if (this.paginationDetails?.totalDocs > 0) {
      this.paginationData.page = page.pageIndex + 1;
      this.paginationData.limit = page.pageSize;
      this.navChanges({ nextId: this.nextId });
    }
  }
  currentPage(page): void {
    this.participantPage.activePage = page;
    this.navChanges({ nextId: this.nextId });
     this.getTournaments();
  }
}
