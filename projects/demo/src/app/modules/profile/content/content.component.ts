import {
  EsportsUserService,
  EsportsVideoLibraryService,
  EsportsToastService,
  EsportsArticleService,
  IPagination
} from 'esports';
import { Component, OnInit } from '@angular/core';
import { LangChangeEvent, TranslateService } from '@ngx-translate/core';
import { HttpParams } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { environment } from '../../../../environments/environment';
const API = environment.apiEndPoint;
@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss'],
})
export class ContentComponent implements OnInit {
  articles = [];
  currentLang: string = 'english';
  pageSizeOptions = environment.pageSizeOptions;
  showLoader: boolean = true;
  slicesArticles = [];
  currentUser: any;
  isAuthor: boolean = false;
  active = 1;
  nextId = 1;
  paginationData = {
    page: 1,
    limit: 10,
    sort: '-updatedOn',
    projection: ['title', 'description', 'updatedOn', 'slug', 'views'],
  };
  participantPage: IPagination = {
    activePage: 1,
    totalItems: 1,
    maxSize: 10,
    itemsPerPage: 10,
  };
  paginationDetails: any;
  videoLibraryDetails: any;
  userId;
  articlesList = [];
  constructor(
    private articleService: EsportsArticleService,
    private videoLibraryService: EsportsVideoLibraryService,
    private userService: EsportsUserService,
    private translateService: TranslateService,
    private eSportsToastService: EsportsToastService,
    private activatedRoute: ActivatedRoute
  ) {
  }

  ngOnInit(): void {
    this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.currentUser = data;
        if (this.activatedRoute?.snapshot?.queryParams['activeTab'] == 2) {
          this.active = 2;
          this.getVideo();
        } else {
          this.active = 1;
          this.getArticles();
        }
        if (this.currentUser.hasOwnProperty('isAuthor')) {
          this.currentUser.isAuthor == 0 ? (this.isAuthor = true) : '';
        }
      }
    });
  }

  /** fetch videos */
  getVideo = async () => {
    try {
      const encodeURI = `?query=${encodeURIComponent(
        JSON.stringify({ createdBy: this.currentUser._id })
      )}&pagination=${encodeURIComponent(JSON.stringify(this.paginationData))}`;

      this.videoLibraryDetails =
        await this.videoLibraryService.fetchVideoLibrary(API, encodeURI);
      this.showLoader = false;

    } catch (error) {
      this.showLoader = false;
      this.eSportsToastService.showError(
        error?.error?.message || error?.message
      );
    }
  };
  pageChanged(page){
    this.participantPage.activePage = page;
    this.getVideo();
  }


    /** fetch user articles */
    getArticles = async () => {
      try {
        const pagination = {
          limit: this.participantPage.itemsPerPage,
          page: this.participantPage.activePage,
        }

        // const pagination = `&limit=${this.participantPage.itemsPerPage}&page=${this.participantPage.activePage}`;
        const query = JSON.stringify({ author: this.userId });
        const option = JSON.stringify({ sort: { createdDate: -1 } });
        const res: any = await this.articleService
          .getArticles(API,{ query, pagination, option})
          .toPromise();
        this.showLoader = false;
        this.articles = res.data;  
        this.articlesList = this.articles.slice(0, this.participantPage.itemsPerPage);    
      } catch (error) {
        this.showLoader = false;
      }
    };

  currentPage(page): void {
    // const startIndex = event.pageIndex * event.pageSize;
    // let endIndex = startIndex + event.pageSize;
    // if (endIndex > this.articlesList.length) {
    //   endIndex = this.articlesList.length;
    // }
    // this.articlesList = this.articles.slice(this.participantPage.activePage);
    this.participantPage.activePage = page;
    this.getArticles();
  }

  videoPageChanged(page): void {
    this.participantPage.activePage = page.pageIndex + 1;
    this.participantPage.itemsPerPage = page.pageSize;
    this.getVideo();
  }

  navChanges = (e) => {
    this.nextId = e.nextId;
    switch (this.nextId) {
      case 1:
        this.getArticles();
        break;
      case 2: {
        this.participantPage.activePage = 1;
        this.participantPage.itemsPerPage = 10;
        this.getVideo();
        break;
      }
      default:
        break;
    }
  };
}
