import { Component, OnInit } from '@angular/core';
import { EsportsTournamentService, IPagination } from 'esports';
import { environment } from '../../../../environments/environment';
@Component({
  selector: 'app-tournaments-joined',
  templateUrl: './tournaments-joined.component.html',
  styleUrls: ['./tournaments-joined.component.scss'],
})
export class TournamentsJoinedComponent implements OnInit {
  tournaments: any;
  showLoader: boolean = true;
  active = 1;
  nextId = 1;
  paginationDetails: any;
  pageSizeOptions = environment.pageSizeOptions;
  paginationData = {
    page: 1,
    limit: 10,
    sort: 'startDate',
  };
  participantPage: IPagination = {
    activePage: 1,
    totalItems: 1,
    maxSize: 10,
    itemsPerPage: 10,
  };

  constructor(private esportsTournamentService: EsportsTournamentService) {}

  ngOnInit(): void {
    this.getTournaments();
  }

  navChanges = (e) => {
    this.nextId = e.nextId;
    this.getTournaments();
  };

  getTournaments = async () => {
    this.showLoader = true;
    const params: any = {
      pagination: JSON.stringify({
        limit: this.participantPage.itemsPerPage,
        page: this.participantPage.activePage,
        sort: 'startDate',
      }),

      type: this.nextId,
    };
    try {
      this.esportsTournamentService
        .getParticipantTournament1(params)
        .subscribe((res) => {
          if (res && res?.data) {
            this.tournaments = res?.data;
            this.participantPage.itemsPerPage = this.tournaments?.limit;
            this.participantPage.totalItems = this.tournaments?.totalDocs;
            // this.paginationData.page = this.paginationDetails.page;
            this.showLoader = false;
          }
        });
    } catch (error) {}
  };

  pageChanged(page): void {
    if (this.paginationDetails?.totalDocs > 0) {
      this.paginationData.page = page.pageIndex + 1;
      this.paginationData.limit = page.pageSize;
      this.navChanges({ nextId: this.nextId });
    }
  }
  currentPage(page): void {
    this.participantPage.activePage = page;
    this.navChanges({ nextId: this.nextId });
    this.getTournaments();
  }
}
