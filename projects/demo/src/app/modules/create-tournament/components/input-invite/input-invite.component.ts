import { EsportsToastService } from 'esports';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-input-invite',
  templateUrl: './input-invite.component.html',
  styleUrls: ['./input-invite.component.scss'],
})
export class InputInviteComponent implements OnInit {
  @Output() emitValue = new EventEmitter();
  data = [];
  timeoutId = null;
  email: any;
  constructor(private toastService: EsportsToastService) { }

  ngOnInit(): void { }

  checkEmail() {
    //remove space of this email
    const newData = this.email.replace(/\s/g, '');
    const data = newData.split(',');
    const email = data.filter((item) => {
      return this.validateEmail(item) === true;
    });
    if (email.length !== data.length) return false;
    this.data = email;
    return true;
  }

  validateEmail(email) {
    const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }
  onChangeValue(): void {
    if (this.checkEmail()) {
      this.emitValue.emit(this.email);
      setTimeout(() => {
        this.email = "";
      }, 200);
    } else {
      this.toastService.showError("Invalid email address.")
    }
    // const valid = event?.target?.validity?.valid;
    // const value = event?.target?.value;
    // const emitValue = () => {
    //   if (!value || !valid) return;
    //   this.emitValue.emit(event?.target?.value.trim());
    //   event.target.value = '';
    // };
    // clearTimeout(this.timeoutId);
    // this.timeoutId = setTimeout(emitValue, 1000);
  }
}
