import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { CreateTournamentComponent } from './create-tournament.component';
import { SelectTypeComponent } from './pages/select-type/select-type.component';
import { CreateQuickComponent } from './pages/create-quick/create-quick.component';
import { AuthGuard } from '../../shared/guard/auth.guard';
import { QuickTournamentComponent } from './pages/quick-tournament/quick-tournament.component';
// import { Detail7Component } from './pages/detail7/detail7.component';

const routes: Routes = [
  {
    path: '',
    component: CreateTournamentComponent,
    children: [
      {
        path: '',
        component: SelectTypeComponent,
      },
      {
        path: 'quicktournament',
        component: QuickTournamentComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'CreateQuick/:id',
        component: CreateQuickComponent,
        canActivate: [AuthGuard],
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CreateTournamentRoutingModule { }
