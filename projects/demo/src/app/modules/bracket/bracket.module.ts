import { NgModule } from "@angular/core";
import { BracketRoutingModule } from "./bracket-routing.module";
import { BracketComponent } from "./bracket.component";
import { BracketPreviewComponent } from "./bracket-preview/bracket-preview.component";
import { UpsertBracketComponent } from "./upsert-bracket/upsert-bracket.component";
import { SharedModule } from "../../shared/modules/shared.module";

@NgModule({
  declarations: [
    BracketComponent,
    BracketPreviewComponent,
    UpsertBracketComponent,
  ],
  imports: [BracketRoutingModule, SharedModule],
})
export class BracketModule {}
