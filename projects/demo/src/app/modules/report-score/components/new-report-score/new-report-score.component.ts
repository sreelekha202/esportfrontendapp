import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import {
  EsportsBracketService,
  EsportsTournamentService,
  EsportsUserService,
  EsportsUtilsService,
  EsportsToastService,
} from 'esports';
import { AppHtmlProfileRoutes } from '../../../../app-routing.model';
import { ITournament, IUser } from 'projects/esports/src/public-api';
import { environment } from '../../../../../environments/environment';
import { UploadScreenshotComponent } from '../../../../shared/components/elimination/score-card/upload-screenshot/upload-screenshot.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-new-report-score',
  templateUrl: './new-report-score.component.html',
  styleUrls: ['./new-report-score.component.scss'],
})
export class NewReportScoreComponent implements OnInit {
  toggle = true;
  sets = <any>[];
  matchId;
  matchDetails;
  setForms = <any>[];
  AppHtmlProfileRoutes = AppHtmlProfileRoutes;
  teamScreenShots = {
    teamAScreenShot: '',
    teamBScreenShot: '',
  };
  registrationStatus = {
    isOpen: false,
    isClosed: false,
    notYetStarted: false,
  };
  myTeam = '';
  isAdmin = false;
  isEO = false;
  currentUser: IUser;
  tournamentData: ITournament;
  scoreSubmitted = false;
  isLoading = false;
  winnerTeam;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private bracketService: EsportsBracketService,
    private tournamentService: EsportsTournamentService,
    private userService: EsportsUserService,
    private utilsService: EsportsUtilsService,
    private toastService: EsportsToastService,
    private matDialog: MatDialog,
    private location: Location
  ) {}

  ngOnInit(): void {
    this.getMatch();
  }

  onBackClick() {
    this.location.back();
  }

  async getMatch() {
    this.isLoading = true;
    this.matchId = this.activatedRoute.snapshot.params.matchId;
    if (!this.matchId) {
      this.router.navigate(['profile/matches']);
    }

    const queryParam = `?query=${encodeURIComponent(
      JSON.stringify({ _id: this.matchId })
    )}&select=winnerTeam,createdOn,bye,currentMatch,matchStatus,teamA,teamB,disQualifiedTeam,placeHolderA,placeHolderB,teamAWinSet,teamBWinSet,isThirdPlaceMatch,matchNo,battleRoyalTeams.noOfKill,battleRoyalTeams.placement,battleRoyalTeams.score,battleRoyalTeams._id,battleRoyalScoringOption,externalMatchId,isScoreConflict&option=${encodeURIComponent(
      JSON.stringify({ sort: { matchNo: 1 } })
    )}`;

    const response = await this.bracketService.fetchAllMatches(queryParam);
    this.matchDetails = response?.data[0];
    await this.getTournamentData();
    this.sets = this.matchDetails.sets;
    this.getMyTeam();
    this.createSetForms();
    this.isLoading = false;
  }

  async getTournamentData() {
    const tournamentApiRes = await this.tournamentService
      .getTournament(
        environment.apiEndPoint,
        this.matchDetails?.tournamentId?._id
      )
      .toPromise();
    this.tournamentData = tournamentApiRes?.data;
    this.setRegistrationOpenStatus();
  }

  getMyTeam() {
    this.currentUser = this.userService.currentUserSubject.value;
    if (this.currentUser?._id === this.matchDetails?.teamA?.userId?._id) {
      this.myTeam = 'teamA';
    } else if (
      this.currentUser?._id === this.matchDetails?.teamB?.userId?._id
    ) {
      this.myTeam = 'teamB';
    }

    if (this.tournamentData?.organizerDetail?._id == this.currentUser?._id) {
      this.isEO = true;
    }

    if (this.currentUser.accountType == 'admin') {
      this.isAdmin = true;
    }

    if (!this.myTeam && !this.isEO && !this.isAdmin) {
      this.router.navigate([this.AppHtmlProfileRoutes.matches]);
    }
  }

  createSetForms() {
    this.sets.forEach((set, idx) => {
      const disabled = set.status == 'completed';

      const setForm = this.fb.group({
        teamAScore: [{ value: set.teamAScore || 0, disabled }],
        teamBScore: [{ value: set.teamBScore || 0, disabled }],
        teamAScreenShot: [null],
        teamBScreenShot: [null],
      });
      const setData = {
        formName: `set ${idx + 1}`,
        setForm,
        toggle: false,
        disabled,
        setId: set.id,
      };
      this.setForms.push(setData);
    });
  }

  toggleSet(setData) {
    setData.toggle = !setData.toggle;
  }

  changeValue(val, formControl) {
    let curVal = formControl.value;
    curVal += val;
    formControl.setValue(curVal);
  }

  async submitScore() {
    const lastSet = this.setForms.at(-1);
    const payload = lastSet?.setForm?.getRawValue();

    const query = {
      _id: this.matchDetails?._id,
      'sets.id': lastSet.setId,
    };
    const queryParam = `?query=${this.utilsService.getEncodedQuery(query)}`;
    if (this.teamScreenShots?.teamBScreenShot) {
      payload.teamBScreenShot = this.teamScreenShots.teamBScreenShot;
    }
    if (this.teamScreenShots.teamAScreenShot) {
      payload.teamAScreenShot = this.teamScreenShots.teamAScreenShot;
    }

    try {
      this.isLoading = true;
      let response = { message: 'Updating score...' };
      if (this.isAdmin || this.isEO) {
        response = await this.bracketService.updateMatch(queryParam, payload);
      } else {
        if (this.myTeam == 'teamA') {
          payload.teamBScore = 0;
        } else {
          payload.teamAScore = 0;
        }

        response = await this.bracketService.updateParticipantScore(
          queryParam,
          payload
        );
      }
      this.toastService.showSuccess(response?.message);
      this.isLoading = false;
      this.showResultUpdateScreen();

    } catch (error) {
      this.isLoading = false
      if(error?.error){
        this.toastService.showError(error?.error?.message);
      } else {
        this.toastService.showError(error?.message);
      }

    }
  }

  openScreenshotUploadPopup = async (screenShotName) => {
    try {
      const data = {
        // screenshotUrl: screenShotName,
        hasScreenshotUpdateAccess: true,
      };
      const response = await this.matDialog
        .open(UploadScreenshotComponent, {
          width: '900px',
          data,
        })
        .afterClosed()
        .toPromise();

      if (response) {
        this.teamScreenShots[screenShotName] = response?.screenshotUrl;
      }
    } catch (error) {}
  };

  setRegistrationOpenStatus() {
    const now = new Date().getTime();
    const tournamentStartTime = new Date(
      this.tournamentData?.startDate
    ).getTime();
    const regStartTime = new Date(this.tournamentData?.regStartDate).getTime();
    const regEndTime = new Date(this.tournamentData?.regEndDate).getTime();

    if (this.tournamentData?.isRegistrationClosed) {
      this.registrationStatus.isClosed = true;
      return;
    }
    if (this.tournamentData.isSeeded || now > tournamentStartTime) {
      this.registrationStatus.isClosed = true;
      return;
    }

    if (this.tournamentData['isRegStartDate'] && now > regEndTime) {
      this.registrationStatus.isClosed = true;
      return;
    }

    if (this.tournamentData['isRegStartDate'] && regStartTime > now) {
      this.registrationStatus.notYetStarted = true;
      return;
    }
    this.registrationStatus.isOpen = true;
  }

  async showResultUpdateScreen() {
    this.scoreSubmitted = true;
    const queryParam = `?query=${encodeURIComponent(
      JSON.stringify({ _id: this.matchId })
    )}&select=winnerTeam,createdOn,bye,currentMatch,matchStatus,teamA,teamB,disQualifiedTeam,placeHolderA,placeHolderB,teamAWinSet,teamBWinSet,isThirdPlaceMatch,matchNo,battleRoyalTeams.noOfKill,battleRoyalTeams.placement,battleRoyalTeams.score,battleRoyalTeams._id,battleRoyalScoringOption,externalMatchId,isScoreConflict&option=${encodeURIComponent(
      JSON.stringify({ sort: { matchNo: 1 } })
    )}`;
    this.isLoading = true;
    const response = await this.bracketService.fetchAllMatches(queryParam);
    const newMatchDetails = response.data[0];

    if (newMatchDetails?.winnerTeam) {
      if (newMatchDetails?.winnerTeam?._id == newMatchDetails?.teamA?._id) {
        this.winnerTeam = newMatchDetails.teamA;
      } else {
        this.winnerTeam = newMatchDetails.teamB;
      }
    }

    this.sets = newMatchDetails.sets.map((setData, idx) => {
      if (setData.winner == newMatchDetails.teamA?._id) {
        setData.winner = newMatchDetails.teamA;
      } else {
        setData.winner = newMatchDetails.teamB;
      }
      setData.name = 'Set ' + (idx + 1);
      return setData;
    });
    this.isLoading = false;
  }
}
