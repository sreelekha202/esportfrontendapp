import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {
  EsportsConstantsService,
  EsportsToastService,
  EsportsTransactionService,
} from 'esports';
import { environment } from '../../../../environments/environment';

const API = environment.apiEndPoint;
@Component({
  selector: 'payment-web-view',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss'],
})
export class PaymentComponent implements OnInit {
  params;
  selectPaymentType = '';
  transaction;
  isProccesing = true;
  paymentMethods = [];
  prizePools = [];
  totalAmounts = [];

  constructor(
    private transactionService: EsportsTransactionService,
    private toastService: EsportsToastService,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private constantService: EsportsConstantsService
  ) {}

  ngOnInit(): void {
    this.activeRoute.queryParams.subscribe((params) => {
      this.params = params;
      this.constantService.setWebViewMetaData(params);
      this.fetchTransaction();
    });
  }

  fetchTransaction = async () => {
    try {
      const payload = {
        tournamentId: this.params?.tournamentId,
        type: 'organizer',
      };

      const { data } = await this.transactionService.createOrUpdateTransaction(
        API,
        payload
      );
      this.totalAmounts.push({
        prizeLabel: 'PRIZE_AMOUNT',
        amount: data.totalAmount - data.platformFee,
      });
      this.totalAmounts.push({
        prizeLabel: 'TOTAL_AMOUNT',
        amount: data.totalAmount,
      });

      this.prizePools = data?.prizeList?.map((item) => {
        let img;
        if (item.name == '1st') {
          img = '../assets/images/payment/Gold.svg';
        } else if (item.name == '2nd') {
          img = '../assets/images/payment/Silver.svg';
        } else if (item.name == '3rd') {
          img = '../assets/images/payment/Bronze.svg';
        } else {
          img = '../assets/images/payment/Bronze.svg';
        }
        return {
          ...item,
          img,
        };
      });
      this.transaction = data;

      if (this.transaction.provider == 'paypal') {
        this.paymentMethods = [
          {
            img: '../assets/images/payment/paypal.png',
            name: 'Paypal',
            value: 'paypal',
          },
        ];
      } else if (this.transaction.provider == 'stcpay') {
        this.paymentMethods = [
          {
            img: '../assets/images/payment/stc_play.png',
            name: 'stc pay',
            value: 'stcpay',
          },
        ];
      }

      this.isProccesing = false;
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
      this.isProccesing = false;
      this.router.navigate(['/web-view/payment-failure']);
    }
  };

  getPaymentResponse = async (order) => {
    try {
      this.isProccesing = true;
      const payload = {
        tournamentId: this.params?.tournamentId,
        transactionId: this.transaction?._id,
        txnId: order?.id,
        provider: 'paypal',
      };
      const response = await this.transactionService.createOrUpdateTransaction(
        API,
        payload
      );
      this.toastService.showSuccess(response?.message);
      this.router.navigate(['/web-view/payment-success']);
      this.isProccesing = false;
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
      this.isProccesing = false;
      this.router.navigate(['/web-view/payment-failure']);
    }
  };

  getStcPaymentResponse = async (order) => {
    try {
      const payload = {
        tournamentId: this.params?.tournamentId,
        type: 'stcpay',
        mobileNo: order.number.replace(/\s+/g, ''),
      };

      const response = await this.transactionService.createOrUpdateTransaction(
        API,
        payload
      );
      this.transaction = response;
      this.toastService.showSuccess(response?.message);
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  getStcOtpVerify = async (params) => {
    try {
      const payload = {
        OtpReference:
          this.transaction['data']['DirectPaymentAuthorizeV4ResponseMessage'][
            'OtpReference'
          ],
        STCPayPmtReference:
          this.transaction['data']['DirectPaymentAuthorizeV4ResponseMessage'][
            'STCPayPmtReference'
          ],
        otp: params,
        id: this.transaction.data.id,
        tournamentId: this.params?.tournamentId,
      };
      const response = await this.transactionService.verifyStcTransaction(
        API,
        payload
      );
      this.toastService.showSuccess(response?.message);
      this.router.navigate(['/web-view/payment-success']);
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
      this.router.navigate(['/web-view/payment-failure']);
    }
  };
}
