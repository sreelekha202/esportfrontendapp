import { Component, ElementRef, OnChanges, OnInit, ViewChild,Output,EventEmitter, SimpleChanges } from '@angular/core';
import { AppHtmlRoutes } from '../../app-routing.model';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { TranslateService } from '@ngx-translate/core';
import { ActivatedRoute, Router } from '@angular/router';
import {
  IPagination,
  EsportsLanguageService,
  EsportsTournamentService,
  EsportsHomeService,
  EsportsGameService,
  GlobalUtils,
} from 'esports';
import { environment } from '../../../environments/environment';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';

const API = environment.apiEndPoint;

@AutoUnsubscribe()
@Component({
  selector: 'app-play',
  templateUrl: './play.component.html',
  styleUrls: ['./play.component.scss'],
})
export class PlayComponent implements OnInit,OnChanges {
  @ViewChild('mainContainer', { read: ElementRef })
  public scroll: ElementRef<any>;
  @Output() getValueData = new EventEmitter();
  AppHtmlRoutes = AppHtmlRoutes;
  showLoader: boolean = false;
  currentTab = 'upcoming';
  page: IPagination;
  allTournamentList: any = [];
  gameId;
  tournamentData: any;
  curentPage = 1;
  activeIndex = 0;
  showFilter = true;
  selectedSortBy: any = '';
  paginationData = {
    page: 1,
    limit: 8,
    sort: 'startDate',
  };
  participantPage: IPagination = {
    activePage: 1,
    totalItems: 1,
    maxSize: 10,
    itemsPerPage: 9,
  };
  checkboxFilter = [];
  selected_platform: any;
  faSearch = faSearch;
  isShown: boolean = false;
  params: any = {};
  gameDetail: any;
  paginationDetails: any;
  pageSizeOptions = environment.pageSizeOptions;
  individualTournaments = [];
  prizeMoneyTournaments = [];
  teamTournaments = [];
  hidePagination = false;
  seasonCheck = [
    {
      id: 0,
      name: 'Fortnite',
      check: false,
      isShowIcon: true,
      type: 'season',
    },
    {
      id: 1,
      name: 'Valorant',
      check: false,
      isShowIcon: true,
      type: 'season',
    },
    {
      id: 2,
      name: 'Apex Legends',
      check: false,
      isShowIcon: true,
      type: 'season',
    },
    {
      id: 3,
      name: 'Overwatch',
      check: false,
      isShowIcon: true,
      type: 'season',
    },
    {
      id: 4,
      name: 'CS:GO',
      check: false,
      isShowIcon: true,
      type: 'season',
    },
    {
      id: 5,
      name: 'PUBG Mobile',
      check: false,
      isShowIcon: true,
      type: 'season',
    },
    {
      id: 6,
      name: 'Mobile Legends',
      check: false,
      isShowIcon: true,
      type: 'season',
    },
    {
      id: 7,
      name: 'Arena of Valor',
      check: false,
      isShowIcon: true,
      type: 'season',
    },
    {
      id: 8,
      name: 'PC',
      check: false,
      isShowIcon: true,
      type: 'platform',
    },
    {
      id: 9,
      name: 'Console',
      check: false,
      isShowIcon: true,
      type: 'platform',
    },
    {
      id: 10,
      name: 'Mobile',
      check: false,
      isShowIcon: true,
      type: 'platform',
    },
    {
      id: 11,
      name: 'Competitive',
      check: false,
      isShowIcon: true,
      type: 'mode',
    },
    {
      id: 12,
      name: 'Casual',
      check: false,
      isShowIcon: true,
      type: 'mode',
    },
    {
      id: 13,
      name: 'Deathmatch',
      check: false,
      isShowIcon: true,
      type: 'mode',
    },
    {
      id: 14,
      name: 'Arms Race',
      check: false,
      isShowIcon: true,
      type: 'mode',
    },
    {
      id: 15,
      name: 'Demolition',
      check: false,
      isShowIcon: true,
      type: 'mode',
    },
    {
      id: 16,
      name: 'Wingman',
      check: false,
      isShowIcon: true,
      type: 'mode',
    },
    {
      id: 17,
      name: 'Retakes',
      check: false,
      isShowIcon: true,
      type: 'mode',
    },
    {
      id: 18,
      name: 'Danger Zone',
      check: false,
      isShowIcon: true,
      type: 'mode',
    },
    {
      id: 19,
      name: 'Flying Scoutsman',
      check: false,
      isShowIcon: true,
      type: 'mode',
    },
  ];
  sortby = [
    { value: '0', viewValue: 'Upcoming tournaments' },
    { value: '1', viewValue: 'Ongoing tournaments' },
    { value: '2', viewValue: 'Completed tournaments' },
    { value: '3', viewValue: 'All tournaments' },
  ];
  selected_sortby_tournament = this.sortby[3].value;
  listFilter :any [];
  sortByList = [
    {
      label: 'Start date',
      value: 'start date',
    },
    {
      label: 'End date',
      value: 'End date',
    },
    {
      label: 'Name',
      value: 'Name',
    },
    {
      label: 'No of participants registered',
      value: 'participants',
    },
  ];
  isAll = true;
  isOngoing = false;
  isUpcoming = false;
  tournamentType:any = '';
  tournamentFlagType = '';
  constructor(
    private languageService: EsportsLanguageService,
    private tournamentService: EsportsTournamentService,
    private translateService: TranslateService,
    private gameService: EsportsGameService,
    private homeService: EsportsHomeService,
    private _activateRoute: ActivatedRoute
  ) { }
  ngOnChanges(changes: SimpleChanges): void {
    throw new Error('Method not implemented.');
  }

  ngOnInit(): void {
    this.isAll = true;
    this.languageService.language.subscribe((lang) =>
      this.translateService.use(lang || 'en')
    );
    this.getAllTournaments();
  }

  clearFilter() {
    this.showFilter = !this.showFilter;
  }
  onChangeSeason(event, id) {
    const index = this.seasonCheck.findIndex((item) => {
      return item.id === id;
    });
    if (index === -1) {
      return;
    }
    this.seasonCheck[index] = {
      ...this.seasonCheck[index],
      check: event.checked,
    };
    this.getValueData.emit(this.seasonCheck);
  }

  isSelected(isactive) {
    this.hidePagination = true;
    this.participantPage.activePage = 1;
    this.curentPage = 1;
    if (isactive == 'isAll') {
      this.isAll = true;
      this.isOngoing = false;
      this.isUpcoming = false;
    }
    if (isactive == 'isOngoing') {
      this.isAll = false;
      this.isOngoing = true;
      this.isUpcoming = false;
    }
    if (isactive == 'isUpcoming') {
      this.isAll = false;
      this.isOngoing = false;
      this.isUpcoming = true;
    }
  }




  getAllTournaments() {
    this.showLoader = true;
    this.tournamentType = '';
    this.tournamentFlagType = 'All';
    this.params = {
      status: '3',
      limit: this.participantPage.itemsPerPage,
      page: this.curentPage,
      // game: this.gameId,
      sort: {"startDate":1},
    };

    this.tournamentService.getPaginatedTournaments(API,this.params).subscribe(
      (res: any) => {
        this.tournamentData = res?.data;
        this.showLoader = false;
        this.participantPage.itemsPerPage = this.tournamentData?.limit;
        this.participantPage.totalItems = this.tournamentData?.totalDocs;
        // this.gameDetail = res?.data?.docs[0].gameDetail;
        this.hidePagination = false;

      },
      (err: any) => {
        this.showLoader = false;
        this.hidePagination = false;
      }
    );
  }


  getOnGoingTournamentsPaginated() {
    this.showLoader = true;
    this.tournamentType = 'onGoing';
    this.tournamentFlagType = 'OnGoing';
    let query: any = {
      $and: [{ tournamentStatus: 'publish' }],
      $or: [{ startDate: { $lt: new Date() } }, { isSeeded: true }],
    };
    if (this.gameId) {
      query.gameDetail = this.gameId;
    }
    query = JSON.stringify(query);
    let params = {
      status: '1',
      limit: this.participantPage.itemsPerPage,
      page: this.curentPage,
      // game: this.gameId,
      sort: {"endDate":1},
    };

    if (this.gameId) {
      params['game'] = this.gameId;
    }

    this.tournamentService.getPaginatedTournaments(API, params).subscribe(
      (res: any) => {
        this.showLoader = false;
        this.tournamentData = res?.data;
        this.participantPage.itemsPerPage = this.tournamentData?.limit;
        this.participantPage.totalItems = this.tournamentData?.totalDocs;
        this.hidePagination = false;
      },
      (err) => {
        this.showLoader = false;
        this.hidePagination = false;
      }
    );
  }



  getUpcomingTournamentsPaginated() {
    this.showLoader = true;
    this.tournamentType = '';
    this.tournamentFlagType = 'UpComing';
    let query: any = {
      $and: [
        { tournamentStatus: 'publish' },
        { startDate: { $gt: new Date() } },
        { isSeeded: false },
        { isFinished: false },
      ],
    };

    if (this.gameId) {
      query.gameDetail = this.gameId;
    }

    query = JSON.stringify(query);
    let params = {
      status: '0',
      limit: this.participantPage.itemsPerPage,
      page: this.curentPage,
      // game: this.gameId,
      sort: {"startDate":1},
    };

    if (this.gameId) {
      params['game'] = this.gameId;
    }

    this.tournamentService.getPaginatedTournaments(API, params).subscribe(
      (res: any) => {
        this.showLoader = false;
        this.tournamentData = res?.data;
        this.participantPage.itemsPerPage = this.tournamentData?.limit;
        this.participantPage.totalItems = this.tournamentData?.totalDocs;
        this.hidePagination = false;
        this.gameDetail = res?.data?.docs[0]?.gameDetail;
        this.gameService.createMatchMakingSubject.next({
          selectedGame: this.gameDetail,
          selectedPlatForm: res?.data?.docs[0]?.platform,
        });
      },
      (err) => {
        this.showLoader = false;
        this.hidePagination = false;
      }
    );
  }

  currentPage(page): void {
    this.participantPage.activePage = Number(page);
    this.curentPage = Number(page);
    if (this.isAll) this.getAllTournaments();
    if (this.isOngoing) this.getOnGoingTournamentsPaginated();
    if (this.isUpcoming) this.getUpcomingTournamentsPaginated();
  }

  // pageChanged(page): void {
  //   this.paginationData.page = page.pageIndex + 1;
  //   this.paginationData.limit = page.pageSize;
  //   this.getAllTournaments();
  // }

  scrollToTop() {
    this.scroll.nativeElement.scrollIntoView();
  }

  showPopup() {
    this.isShown = true;
  }
  hidePopup() {
    this.isShown = false;
  }
  onSortBy(item) {
    const tournament = [];
    this.selectedSortBy = item;
    this.tournamentData.docs.forEach((t) => {
      if (item.value.toLowerCase() == t.participantType.toLowerCase()) {
        tournament.push(t);
      }
      if (item.value.toLowerCase() == 'prize' && t.isPrize) {
        tournament.push(t);
      }
    });
    this.tournamentData.docs = tournament;
    // this.getAllTournaments();
  }
  onSortByoption(event, option) {
    const index = this.listFilter.findIndex((item, i) => {
      return item.value == option.value;
    });
    this.listFilter[index] = {
      ...this.listFilter[index],
      check: event.checked,
    };
    this.selectedSortBy = option;

    this.onSortByLogic(option);
    // this.getAllTournaments();
  }

  onSortByLogic(option) {
    let t = [];
    this.tournamentData.docs.forEach((tournament, i) => {
      this.listFilter.forEach((opValueFromList) => {
        if (opValueFromList.check) {
          if (
            opValueFromList.value.toLowerCase() ==
            tournament.platform.name.toLowerCase()
          ) {
            t.push(tournament);
          }
        }
      });
    });
    this.tournamentData.docs = t;
  }
  removeSortBy(event) {
    const index = this.listFilter.findIndex((item) => {
      return item.value == event.value;
    });
    this.listFilter[index] = {
      ...this.listFilter[index],
      check: false,
    };
    // this.selectedSortBy = '';
    this.onSortByLogic(event);
  }
  clearFilterBtn() {
    this.listFilter = this.listFilter.map((item) => {
      return {
        ...item,
        check: false,
      };
    });

    this.getAllTournaments();
  }
  ngOnDestroy(): void { }
}
