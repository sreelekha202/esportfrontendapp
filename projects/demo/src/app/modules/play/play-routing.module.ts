import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { PlayComponent } from './play.component';

const routes: Routes = [{ path: '', component: PlayComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PlayRoutingModule {}
