import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClipboardModule } from '@angular/cdk/clipboard';
import { CoreModule } from '../../core/core.module';
import { FormComponentModule } from '../../shared/components/form-component/form-component.module';
import { HeaderDynastyModule } from '../../shared/components/header-dynasty/header-dynasty.module';
import { HeaderBottomModule } from '../../shared/components/header-bottom/header-bottom.module';
import { UploadImageModule } from '../../shared/components/upload-image/upload-image.module';
import { GetMatchComponent } from './get-match.component';
import { GetMatchRoutingModule } from './get-match-routing.module';
import { GameLobbyComponent } from './game-lobby/game-lobby.component';
import { SelectMembersComponent } from './select-members/select-members.component';
import { SelectPlatformComponent } from './select-platform/select-platform.component';
import { SelectTeamsComponent } from './select-teams/select-teams.component';
import { SelectGameTypeComponent } from './select-game-type/select-game-type.component';
import { HeaderMatchmakingComponent } from './header-matchmaking/header-matchmaking.component';
import { TeamGameComponent } from './team-game/team-game.component';
import { GameLoadingComponent } from './game-loading/game-loading.component';
import { CasualMatchmakingComponent } from './casual-matchmaking/casual-matchmaking.component';
import { MatchLobbyComponent } from './match-lobby/match-lobby.component';
import { MatchLobbyApiComponent } from './match-lobby-api/match-lobby-api.component';
import { MatchLobbyNonApiComponent } from './match-lobby-non-api/match-lobby-non-api.component';
import { MatchDetailComponent } from './match-detail/match-detail.component';
import { MatchCompletedComponent } from './match-completed/match-completed.component';
import { SeasonHomeComponent } from './season/season-home/season-home.component';
import { ContentLeftComponent } from './season/season-home/components/content-left/content-left.component';
import { ContentRightComponent } from './season/season-home/components/content-right/content-right.component';
import { TournamentScoringComponent } from './tournament-scoring/tournament-scoring.component';
import { TourScoringUpdateScoreComponent } from './tour-scoring-update-score/tour-scoring-update-score.component';
import { FooterDynastyModule } from '../../shared/components/footer-dynasty/footer-dynasty.module';
import { SelectTeamComponent } from './season/select-team/select-team.component';
import { UpdateScoringSuccessComponent } from './update-scoring-success/update-scoring-success.component';
import { PopupComponent } from './season/season-home/components/popup/popup.component';
// GAME LOBBY COMPONENTS
import { MatchroomChatComponent } from './game-lobby/matchroom-chat/matchroom-chat.component';
import { PlayersComponent } from './game-lobby/players/players.component';
import { PostgameStatsComponent } from './game-lobby/postgame-stats/postgame-stats.component';
import { RouterBackModule } from '../../shared/directives/router-back.module';
import { GameheaderComponent } from './game-lobby/gameheader/gameheader.component';
import { SharedModule } from '../../shared/modules/shared.module';
import {MatRadioModule} from '@angular/material/radio';
import { MatIconModule } from "@angular/material/icon";
import {
  EsportsModule,
  EsportsSeasonModule,
  EsportsLoaderModule,
  EsportsCustomPaginationModule,
  EsportsChatService
} from 'esports';
import { FooterMenuMobileModule } from '../../shared/components/footer-menu-mobile/footer-menu-mobile.module';
import { environment } from '../../../environments/environment';
import { ManageScoreComponent } from './manage-score/manage-score.component';
import { TeamBannerComponent } from './manage-score/components/team-banner/team-banner.component';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { JwtInterceptor } from '../../core/helpers/interceptors/token-interceptors.service';

const components = [
  GameLobbyComponent,
  GetMatchComponent,
  MatchroomChatComponent,
  PlayersComponent,
  PostgameStatsComponent,
  SelectMembersComponent,
  SelectPlatformComponent,
  SelectTeamsComponent,
  ManageScoreComponent,
  GameheaderComponent,
  SelectGameTypeComponent,
  HeaderMatchmakingComponent,
  TeamGameComponent,
  GameLoadingComponent,
  CasualMatchmakingComponent,
  MatchLobbyComponent,
  MatchLobbyApiComponent,
  MatchLobbyNonApiComponent,
  MatchDetailComponent,
  MatchCompletedComponent,
  TournamentScoringComponent,
  TourScoringUpdateScoreComponent,
  SeasonHomeComponent,
  ContentLeftComponent,
  ContentRightComponent,
  TournamentScoringComponent,
  SelectTeamComponent,
  UpdateScoringSuccessComponent,
  PopupComponent,
  TeamBannerComponent,
];

const modules = [
  ClipboardModule,
  CommonModule,
  CoreModule,
  FormComponentModule,
  GetMatchRoutingModule,
  HeaderDynastyModule,
  MatIconModule,
  SharedModule,
  UploadImageModule,
  MatRadioModule,
  RouterBackModule,
  HeaderBottomModule,
  FooterDynastyModule,
  FooterMenuMobileModule,
  EsportsLoaderModule.setColor('#1d252d'),
  EsportsModule.forRoot(environment),
  EsportsSeasonModule.forRoot(environment),
  EsportsCustomPaginationModule
];

@NgModule({
  declarations: components,
  imports: modules,
  providers: [
    EsportsChatService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptor,
      multi: true,
    },
  ]
})
export class GetMatchModule { }
