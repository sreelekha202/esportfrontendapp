import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TourScoringUpdateScoreComponent } from './tour-scoring-update-score.component';

describe('TourScoringUpdateScoreComponent', () => {
  let component: TourScoringUpdateScoreComponent;
  let fixture: ComponentFixture<TourScoringUpdateScoreComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TourScoringUpdateScoreComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TourScoringUpdateScoreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
