import { AuthGuard } from './../../shared/guard/auth.guard';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GetMatchComponent } from './get-match.component';
import { GameLobbyComponent } from './game-lobby/game-lobby.component';
import { SelectMembersComponent } from './select-members/select-members.component';
import { SelectPlatformComponent } from './select-platform/select-platform.component';
import { SelectTeamsComponent } from './select-teams/select-teams.component';
import { SelectGameTypeComponent } from './select-game-type/select-game-type.component';
import { TeamGameComponent } from './team-game/team-game.component';
import { GameLoadingComponent } from './game-loading/game-loading.component';
import { CasualMatchmakingComponent } from './casual-matchmaking/casual-matchmaking.component';
import { MatchLobbyComponent } from './match-lobby/match-lobby.component';
import { MatchLobbyApiComponent } from './match-lobby-api/match-lobby-api.component';
import { MatchLobbyNonApiComponent } from './match-lobby-non-api/match-lobby-non-api.component';
import { MatchDetailComponent } from './match-detail/match-detail.component';
import { MatchCompletedComponent } from './match-completed/match-completed.component';
import { SeasonHomeComponent } from './season/season-home/season-home.component';
import { TournamentScoringComponent } from './tournament-scoring/tournament-scoring.component';
import { TourScoringUpdateScoreComponent } from './tour-scoring-update-score/tour-scoring-update-score.component';
import { SelectTeamComponent } from './season/select-team/select-team.component';
import { UpdateScoringSuccessComponent } from './update-scoring-success/update-scoring-success.component';
import { ManageScoreComponent } from './manage-score/manage-score.component';

const routes: Routes = [
  {
    path: '',
    component: GetMatchComponent,
    canActivate: [AuthGuard],
    children: [
      // {
      //   path: '',
      //   component: SelectGameTypeComponent,
      // },
      {
        path: 'matchmaking',
        component: SelectGameTypeComponent,
      },
      {
        path: 'platforms-single',
        component: SelectPlatformComponent,
      },
      {
        path: 'platforms',
        component: SelectPlatformComponent,
      },
      {
        path: 'teams',
        component: SelectTeamsComponent,
      },
      {
        path: 'members',
        component: SelectMembersComponent,
      },
      {
        path: 'game-lobby',
        component: GameLobbyComponent,
      },
      {
        path: 'manage-score',
        component: ManageScoreComponent,
      },
      { path: 'season', component: SeasonHomeComponent },
      // unused (design team please remove ,Don't create new components)
      {
        path: 'game-loading',
        component: GameLoadingComponent,
      },
      {
        path: 'casual-matchmaking',
        component: CasualMatchmakingComponent,
      },
      {
        path: 'match-lobby',
        component: MatchLobbyComponent,
      },
      {
        path: 'match-lobby-api',
        component: MatchLobbyApiComponent,
      },
      {
        path: 'match-lobby-non-api',
        component: MatchLobbyNonApiComponent,
      },
      {
        path: 'match-completed',
        component: MatchCompletedComponent,
      },
      {
        path: 'match-detail',
        component: MatchDetailComponent,
      },
      {
        path: 'tournament-scoring',
        component: TournamentScoringComponent,
      },
      {
        path: 'update-score',
        component: TourScoringUpdateScoreComponent,
      },
      {
        path: 'scoring-success',
        component: UpdateScoringSuccessComponent,
      },
      {
        path: 'team-game',
        component: TeamGameComponent,
      },
      { path: 'season/select-team', component: SelectTeamComponent },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GetMatchRoutingModule {}
