import { Component, ElementRef, HostListener, OnInit, ViewChild } from '@angular/core';
import { AppHtmlGetMatchRoutes, AppHtmlRoutes } from '../../../app-routing.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-match-lobby-non-api',
  templateUrl: './match-lobby-non-api.component.html',
  styleUrls: ['./match-lobby-non-api.component.scss']
})
export class MatchLobbyNonApiComponent implements OnInit {
  @ViewChild('popup') popup: ElementRef;
  @ViewChild('btnShowPopup') btnShowPopup: ElementRef;
  @ViewChild('btnShowPopupfoot') btnShowPopupfoot: ElementRef;
  AppHtmlRoutes = AppHtmlRoutes
  AppHtmlGetMatchRoutes = AppHtmlGetMatchRoutes;
  show = false;
  contentChat = false;
  showEmojiPicker = false;
  constructor(private router: Router) { }
  ngOnInit(): void {
  }

  nextPage(){
      this.router.navigateByUrl("/get-match/tournament-scoring")
  }
  
  @HostListener('document:click',['$event'])
  clickOutside(event){
    if( !(this.popup.nativeElement.contains(event.target)) && this.show === true && !(this.btnShowPopup.nativeElement.contains(event.target)) && !(this.btnShowPopupfoot.nativeElement.contains(event.target))){
        this.show = false;
    }
  }
  toggleEmojiPicker(){
    this.showEmojiPicker = !this.showEmojiPicker;
  }

}
