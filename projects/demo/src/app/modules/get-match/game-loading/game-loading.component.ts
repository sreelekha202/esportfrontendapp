import { Subscription } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { AppHtmlGetMatchRoutes } from '../../../app-routing.model';
import { Router } from '@angular/router';
import { EsportsGameService } from 'esports';

@Component({
  selector: 'app-game-loading',
  templateUrl: './game-loading.component.html',
  styleUrls: ['./game-loading.component.scss'],
})
export class GameLoadingComponent implements OnInit {
  AppHtmlGetMatchRoutes = AppHtmlGetMatchRoutes;
  showPopup = true;
  matchmakingDetails: Subscription;
  createMatchMaking: any;
  selectedGame: any = null;
  selectedPlatformDetails: any = null;

  constructor(
    private gameService: EsportsGameService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.matchmakingDetails = this.gameService.createMatchMaking.subscribe(
      (data: any) => {
        if (data) {
          this.createMatchMaking = data;
          this.selectedGame = data.selectedGame;
        }
      }
    );
  }

  toMatchLobby() {
    this.router.navigateByUrl('/get-match/match-lobby');
  }
}
