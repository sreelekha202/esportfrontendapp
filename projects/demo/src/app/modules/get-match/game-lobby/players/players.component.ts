import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-players',
  templateUrl: './players.component.html',
  styleUrls: ['./players.component.scss'],
})
export class PlayersComponent implements OnInit {
  @Input() teamsData:any = [];
  @Input() isSingle:boolean;
  @Input() playersData:any = [];
  @Input() matchData:any = [];
  show = false;
  showPopup = false;

  constructor() {}

  ngOnInit(): void {
    // MOCK TEAMS DATA

    // this.teams = [
    //   {
    //     isMyTeam: true,
    //     logo: 'https://i.pinimg.com/736x/f9/a8/c0/f9a8c0734c8e13adaa0d39fdf81274c2.jpg',
    //     name: 'Dark Ninja',
    //     players: [
    //       {
    //         number: '0921001010',
    //         image:
    //           'https://cdn6.dissolve.com/p/D538_291_180/D538_291_180_1200.jpg',
    //         nickname: 'xxinfinity01',
    //         role: 'Captain',
    //         location: 'Mexico',
    //       },
    //       {
    //         number: 'Aaime01010',
    //         image:
    //           'https://media.suara.com/pictures/970x544/2020/05/10/60545-main-game.jpg',
    //         nickname: 'Aaime',
    //         role: 'Captain',
    //         location: 'Mexico',
    //       },
    //     ],
    //   },
    //   {
    //     logo: 'https://i.pinimg.com/originals/60/95/51/6095510eaff84568bf69dc0217f6e8b8.png',
    //     name: 'Brightforces',
    //     players: [
    //       {
    //         number: 'Supergirl410',
    //         image:
    //           'https://cdn6.dissolve.com/p/D538_291_180/D538_291_180_1200.jpg',
    //         nickname: 'Supergirl',
    //         role: 'Captain',
    //         location: 'Mexico',
    //       },
    //       {
    //         number: '01wxdmaox',
    //         image:
    //           'https://media.suara.com/pictures/970x544/2020/05/10/60545-main-game.jpg',
    //         nickname: 'wxdmaox',
    //         role: 'Captain',
    //         location: 'Dominican Republic',
    //       },
    //     ],
    //   },
    // ];
  }
}
