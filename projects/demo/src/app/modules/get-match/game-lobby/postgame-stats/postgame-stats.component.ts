import { Component, OnInit, ViewChild } from '@angular/core';
import { SwiperConfigInterface, SwiperComponent } from 'ngx-swiper-wrapper';

@Component({
  selector: 'app-postgame-stats',
  templateUrl: './postgame-stats.component.html',
  styleUrls: ['./postgame-stats.component.scss'],
})
export class PostgameStatsComponent implements OnInit {
  @ViewChild(SwiperComponent, { static: false }) compRef?: SwiperComponent;

  currentGame = null;

  players = [];

  public config: SwiperConfigInterface = {
    breakpoints: {
      320: {
        slidesPerView: 6,
        spaceBetween: 16,
        autoHeight: false,
      },
    },
  };

  constructor() {}

  ngOnInit(): void {
    // MOCK GAME DATA
    this.currentGame = {
      logo: 'https://i114.fastpic.ru/big/2021/0617/67/40feb4fcee1ec84b2863bfdff9728c67.png',
      name: 'Radiant victory',
      time: '35:12',
      match: {
        id: '58825603',
        type: 'Ranked',
        mode: 'All Picked',
      },
    };

    // MOCK PLAYERS DATA
    this.players = [
      {
        image:
          'https://i114.fastpic.ru/big/2021/0617/3f/247c37812c59ddf8bb984482d296be3f.png',
        nickname: 'supergirl',
        role: 'captain',
        location: 'Mexico',
        hero: {
          image:
            'https://i114.fastpic.ru/big/2021/0617/82/6347c3c2b657708a52901d6672acbf82.jpg',
          name: 'Bruno',
          kda: '4/8/11',
          rate: 25,
          net: '22,107',
          power: 'Underlord',
        },
        items: [
          {
            logo: 'https://i114.fastpic.ru/big/2021/0617/3a/b94406c422f2be79c4b34a67d792c43a.jpg',
            type: 'crm',
            damage: '352 / 4',
          },
          {
            logo: 'https://i114.fastpic.ru/big/2021/0617/7a/eb6c57b46dfd6eebb551dbae2ad7ef7a.jpg',
            type: 'crm',
            damage: 457,
          },
          {
            logo: 'https://i114.fastpic.ru/big/2021/0617/57/3acbc32ceca24f0a4ea018ce4aad6057.jpg',
            type: 'xpm',
            damage: 564,
          },
          {
            logo: 'https://i114.fastpic.ru/big/2021/0617/2d/86466861c2fc12b115fa4c36f1fa362d.jpg',
            type: 'dmg',
            damage: '10,107',
          },
        ],
      },
      {
        image:
          'https://i114.fastpic.ru/big/2021/0617/df/633fc0f0f4b1c52931d109cbc42cc6df.png',
        nickname: 'xxinfinity01',
        role: 'captain',
        location: 'Mexico',
        hero: {
          image:
            'https://i114.fastpic.ru/big/2021/0617/f6/0fb463ac6ec362fefc58c0874e3d29f6.jpg',
          name: 'Bruno',
          kda: '4/8/11',
          rate: 25,
          net: '22,107',
          crm1: '352 / 4',
          crm2: 457,
          xpm: 546,
          dmg: '10,107',
          power: 'Underlord',
        },
        items: [
          {
            logo: 'https://i114.fastpic.ru/big/2021/0617/6d/49142aced9aba7c5e44524386bb2116d.jpg',
            type: 'crm',
            damage: '352 / 4',
          },
          {
            logo: 'https://i114.fastpic.ru/big/2021/0617/53/e82d5e08574f4c334fde63ed03f8b953.jpg',
            type: 'crm',
            damage: 457,
          },
          {
            logo: 'https://i114.fastpic.ru/big/2021/0617/b7/4e0f5d65fd2f1ecdac7b3439149c04b7.jpg',
            type: 'xpm',
            damage: 564,
          },
          {
            logo: 'https://i114.fastpic.ru/big/2021/0617/f9/1f85be7eb270b209169fca50006eb4f9.jpg',
            type: 'dmg',
            damage: '10,107',
          },
        ],
      },
      {
        image:
          'https://i114.fastpic.ru/big/2021/0617/69/4e7300bb13893e0834b83d1a8c6dc069.png',
        nickname: 'supergirl',
        role: 'captain',
        location: 'Mexico',
        hero: {
          image:
            'https://i114.fastpic.ru/big/2021/0617/86/6a52f852b237d924ba911480c16eb886.jpg',
          name: 'Bruno',
          kda: '4/8/11',
          rate: 25,
          net: '22,107',
          crm1: '352 / 4',
          crm2: 457,
          xpm: 546,
          dmg: '10,107',
          power: 'Underlord',
        },
        items: [
          {
            logo: 'https://i114.fastpic.ru/big/2021/0617/3a/b94406c422f2be79c4b34a67d792c43a.jpg',
            type: 'crm',
            damage: '352 / 4',
          },
          {
            logo: 'https://i114.fastpic.ru/big/2021/0617/7a/eb6c57b46dfd6eebb551dbae2ad7ef7a.jpg',
            type: 'crm',
            damage: 457,
          },
          {
            logo: 'https://i114.fastpic.ru/big/2021/0617/57/3acbc32ceca24f0a4ea018ce4aad6057.jpg',
            type: 'xpm',
            damage: 564,
          },
          {
            logo: 'https://i114.fastpic.ru/big/2021/0617/2d/86466861c2fc12b115fa4c36f1fa362d.jpg',
            type: 'dmg',
            damage: '10,107',
          },
        ],
      },
      {
        image:
          'https://i114.fastpic.ru/big/2021/0617/5d/9889d86c1ec78fb39bc74bb1a61c1b5d.png',
        nickname: 'wxmaoe',
        role: 'captain',
        location: 'Mexico',
        hero: {
          image:
            'https://i114.fastpic.ru/big/2021/0617/87/517ecb3f1e87def524536a2cfd6d5c87.jpg',
          name: 'Bruno',
          kda: '4/8/11',
          rate: 25,
          net: '22,107',
          crm1: '352 / 4',
          crm2: 457,
          xpm: 546,
          dmg: '10,107',
          power: 'Underlord',
        },
        items: [
          {
            logo: 'https://i114.fastpic.ru/big/2021/0617/6d/49142aced9aba7c5e44524386bb2116d.jpg',
            type: 'crm',
            damage: '352 / 4',
          },
          {
            logo: 'https://i114.fastpic.ru/big/2021/0617/53/e82d5e08574f4c334fde63ed03f8b953.jpg',
            type: 'crm',
            damage: 457,
          },
          {
            logo: 'https://i114.fastpic.ru/big/2021/0617/b7/4e0f5d65fd2f1ecdac7b3439149c04b7.jpg',
            type: 'xpm',
            damage: 564,
          },
          {
            logo: 'https://i114.fastpic.ru/big/2021/0617/2d/86466861c2fc12b115fa4c36f1fa362d.jpg',
            type: 'dmg',
            damage: '10,107',
          },
        ],
      },
    ];
  }

  nextSlide() {
    this.compRef.directiveRef.nextSlide();
  }
}
