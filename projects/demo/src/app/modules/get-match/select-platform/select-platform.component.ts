import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { AppHtmlGetMatchRoutes } from '../../../app-routing.model';
import { Router } from '@angular/router';
import {
  EsportsToastService,
  GlobalUtils,
  EsportsGameService,
  EsportsSeasonService,
} from 'esports';
import { environment } from '../../../../environments/environment';
import { TranslateService } from '@ngx-translate/core';

const API = environment.apiEndPoint;

@Component({
  selector: 'app-select-platform',
  templateUrl: './select-platform.component.html',
  styleUrls: ['./select-platform.component.scss'],
  host: { class: 'full-height' },
})
export class SelectPlatformComponent implements OnInit {
  AppHtmlGetMatchRoutes = AppHtmlGetMatchRoutes;
  @ViewChild('team') team: ElementRef;
  // isLoaded=true;
  selectedPlatform = null;
  selectedPlatformName = null;
  selectedTeam = null;
  subModule = null;
  platforms = [];
  selectedGame: any = null;
  selectedPlatformDetails: any = null;
  showLoader: boolean;
  popupCreateTeam = false;

  teams = [];
  gameList: any;
  gameID: any;
  gameTypes: any = [];
  selectedGameType : any;
  valuePlatform2 = null;
  teamFormatSelected : any;
  totalPages : number = 2;
  step : number = 2;
  numberStep : number = 2;

  constructor(
    private gameService: EsportsGameService,
    private esportsSeasonService: EsportsSeasonService,
    private router: Router,
    private toastService: EsportsToastService,
    private translate: TranslateService
  ) {
    this.gameService.createMatchMakingSubject.subscribe(data=>{
      if(data && data['_id']){
        this.gameID =  data['_id'];
      }else{
        this.router.navigate(['/get-match/matchmaking'])
      }
    })   
  }

  ngOnInit(): void {
    // this.teams = [
    // {
    //   image:
    //     'https://i.pinimg.com/736x/f9/a8/c0/f9a8c0734c8e13adaa0d39fdf81274c2.jpg',
    //   name: 'Dark Ninja',
    // },
    // ];
    this.getGames();
    this.setGameType();
  }

  setGameType() {
    this.gameTypes.push({
      type: 'individual',
      icon: 'assets/icons/matchmaking/platforms/single-player-2.svg',
      title: 'MATCHMAKING.SELECT_PLATFORM.GAME_TYPES.SINGLE',
    });

    this.gameTypes.push({
      type: 'team',
      icon: 'assets/icons/matchmaking/platforms/team-2.svg',
      title: 'MATCHMAKING.SELECT_PLATFORM.GAME_TYPES.TEAMS',
    });
  }

  nextPage(game) {
    this.selectedGameType = game.type;
    if(this.selectedGameType == 'team'){
      this.totalPages = 4;        
    }
  }

  reset(){
    this.dataTeamDropdownItem.map(data=>{ data.selected = false});
  }

  selectedPlatformIndex : any;
  selectedPlatformIndex1 : any;
  setPlatform(i) {
    this.selectedPlatformIndex = i;
  }
  setPlatform2(i) {
    this.selectedPlatformIndex1 = i;
    this.valuePlatform2 = i;    
    this.reset();
    // if (this.valuePlatform2 == 1 && !this.Step3.value.teamTournamentType)
    //   this.clickChange(this.dataTeamDropdownItem[0]);
    // else{
    //   this.getSelectedFormat();
    // }
  }

  getGames() {
    this.showLoader = true;
    this.gameService.getGames(API).subscribe(
      (res) => {
        this.showLoader = false;
        this.gameList = res.data;
        this.gameList.map((game) => {
          if (game._id == this.gameID) {
            this.selectedGame = game;
            this.selectedGame.platform && this.selectedGame.platform.length > 0
              ? this.setPlatForm()
              : '';
          }
        });
      },
      (err) => {
        this.showLoader = false;
      }
    );
    // this.teamFormatSelected = this.dataTeamDropdownItem.find((ele) => {
    //   if(ele.id == this.selectedGame.teamTournamentType){
    //     return ele.teamSize;
    //   }
    //   return 0;
    // });
  }

  checkRouter() {
    return this.router.url === '/get-match/platforms-single';
  }
  setPlatForm() {
    this.selectedGame.platform.map((obj) => {
      if (obj.name == 'pc') {
        this.platforms.push({
          ...obj,
          type: 'pc',
          icon: 'assets/icons/matchmaking/platforms/pc.svg',
          title: 'MATCHMAKING.SELECT_PLATFORM.PLATFORMS.PC',
        });
      } else if (obj.name == 'Mobile') {
        this.platforms.push({
          ...obj,
          type: 'mobile',
          icon: 'assets/icons/matchmaking/platforms/mobile.svg',
          title: 'MATCHMAKING.SELECT_PLATFORM.PLATFORMS.MOBILE',
        });
      } else if (obj.name == 'Other') {
        this.platforms.push({
          ...obj,
          type: 'console',
          icon: 'assets/icons/matchmaking/platforms/console.svg',
          title: 'MATCHMAKING.SELECT_PLATFORM.PLATFORMS.CONSOLE',
        });
      } else if (obj.name == 'Xbox') {
        this.platforms.push({
          ...obj,
          type: 'xbox',
          icon: 'assets/icons/matchmaking/platforms/xbox.svg',
          title: 'MATCHMAKING.SELECT_PLATFORM.PLATFORMS.XBOX',
        });
      } else if (obj.name == 'PS4') {
        this.platforms.push({
          ...obj,
          type: 'ps4',
          icon: 'assets/icons/matchmaking/platforms/ps4.svg',
          title: 'MATCHMAKING.SELECT_PLATFORM.PLATFORMS.PS4',
        });
      } else if (obj.name == 'PS5') {
        this.platforms.push({
          ...obj,
          type: 'ps5',
          icon: 'assets/icons/matchmaking/platforms/ps5.svg',
          title: 'MATCHMAKING.SELECT_PLATFORM.PLATFORMS.PS5',
        });
      } else {
        this.platforms.push({
          ...obj,
          type: 'pc',
          icon: 'assets/icons/matchmaking/platforms/pc.svg',
          title: 'MATCHMAKING.SELECT_PLATFORM.PLATFORMS.PC',
        });
      }
    });
  }

  selectPlatformType(platform) {
    this.selectedPlatform = platform.type;
    this.selectedPlatformName = platform.name;
    this.selectedPlatformDetails = platform;
  }

  // Notes : for Html/css Desigener
  // don't create new component all ready logical code compnent is avilable
  // redesign the exisiting compnents.

  // nextPage() {
  //   if (this.selectedPlatformDetails) {
  //     if (this.checkRouter()) {
  //       this.gameService.createMatchMakingSubject.next({
  //         selectedPlatform: this.selectedPlatformDetails,
  //         selectedGame: this.selectedGame,
  //         selectedGameType: 'individual',
  //       });
  //       this.router.navigateByUrl('/get-match/game-loading');
  //     } else {
  //       this.gameService.createMatchMakingSubject.next({
  //         selectedPlatform: this.selectedPlatformDetails,
  //         selectedGame: this.selectedGame,
  //         selectedGameType: 'team',
  //       });
  //       this.router.navigateByUrl('/get-match/team-game');
  //     }
  //   } else {
  //     this.toastService.showError('Select platforms.');
  //   }
  // }

  next() {
    if (this.selectedGame && this.selectedPlatformDetails && this.selectedGameType) {
      let type = this.selectedGameType;     
      this.gameService.createMatchMakingSubject.next({
        selectedPlatform: this.selectedPlatformDetails,
        selectedGame: this.selectedGame,
        selectedGameType: type,
      });
      const params = {
        game: this.selectedGame._id,
        platform: this.selectedPlatformDetails._id,
        type: type,
      };
      // version 2.0 (Matchmaking)
      this.showLoader = true;
      this.esportsSeasonService.checkSeasons(API, params).subscribe(
        (res) => {
          this.showLoader = false;
          if (res?.data?.data?.season.length > 0) {
            this.toastService.showSuccess(res?.message);
            if (res?.data?.data?.participant) {
              this.router.navigate(
                ['/games/match-season/' + res?.data?.data?.season[0]?._id],
                { queryParams: { playnow: 'true' } }
              );
            } else {
              if (res?.data?.data?.season[0]?.participantType === 'team') {
                this.router.navigate(
                  [
                    `/team-registration/${res?.data?.data?.season[0]?.slug}-${res?.data?.data?.season[0]?._id}`,
                  ],
                  {
                    queryParams: { isSeason: 'true', isMatchmaking: 'true' },
                  }
                );
              } else {
                this.router.navigate([
                  `/tournament/${res?.data?.data?.season[0]?.slug}-${res?.data?.data?.season[0]?._id}/join`,
                ]);
              }
            }
          } else {
            this.toastService.showError(res?.message);
          }
        },
        (err) => {
          this.showLoader = false;
          this.toastService.showError(err.error.message);
        }
      );
    }else{
      if(!this.selectedGameType){
        this.toastService.showError(
          this.translate.instant('SELECT_PLATFORM.ERR_CHOOSE_TOURNAMENT_TYPE')
        );
      }
      if(!this.selectedPlatformDetails){
        this.toastService.showError(
          this.translate.instant('SELECT_PLATFORM.ERR_CHOOSE_PLATFORM')
        );
      }
    }
  }

  noOfTeamSize:number = 5;
  dataFilterTeamDropdownItem = '';
  dataTeamDropdownItem = [
    {
      id: 'Duo',
      name: 'Duo',
      description : this.translate.instant('SELECT_PLATFORM.EACH_PARTICIPATING_COMPRISE_TWO'),
      teamSize: 2,
      selected : true
    },
    {
      id: 'Team',
      name: 'Team',
      description : this.translate.instant('SELECT_PLATFORM.EACH_PARTICIPATING_COMPRISE_FOUR'),
      teamSize: 4,
      selected : false
    },
    {
      id: 'X vs X',
      name: 'X vs X',
      description : this.translate.instant('SELECT_PLATFORM.SET_THE_NUMBER_OF_PLAYERS_ALLOWED'),
      teamSize: 5,
      selected : false
    },
  ];

  clickChange(value) {    
    this.dataTeamDropdownItem.map(menu =>{
      if(menu.id == value.id){ menu.selected = true}
    })
    this.dataFilterTeamDropdownItem = (value.name ? value.name : this.teamFormatSelected.name);
   
    // if (this.valuePlatform2 == 1) {
    //   this.Step3?.get('teamTournamentType')?.setValue(
    //     (value.id ? value.id : this.teamFormatSelected.id)
    //   );
    // }else{
    //   this.dataFilterTeamDropdownItem = ''
    //   this.Step3?.get('teamTournamentType')?.setValue(
    //     this.dataFilterTeamDropdownItem
    //   );
    // }
  }

  // onSelectTeamFormate(id: string) {
  //   this.Step3?.get('teamFormat')?.setValue(id);
  //   if (this.Step3.get('teamFormat').value == this.dataTeamDropdownItem[2]['id'])
  //     this.isTeamSize = true;
  //   else {
  //     this.isTeamSize = false;
  //     this.setTeamSize(id);
  //   }
  // }

  // setTeamSize(id: string) {
  //   if (this.dataTeamDropdownItem[0]['id'] == id) {
  //     this.Step3?.get('teamTournamentType')?.setValue(2);
  //   }
  //   if (this.dataTeamDropdownItem[1]['id'] == id) {
  //     this.Step3?.get('teamTournamentType')?.setValue(4);
  //   }
  // }

  activateClass(team) {
    team.active = !team.active;
  }

  updateTeamChange(val){
    this.noOfTeamSize = (val.target.value ? parseInt(val.target.value) : 1);
  }

  operateTeam(data){
    this.noOfTeamSize += data;    
  }
}
