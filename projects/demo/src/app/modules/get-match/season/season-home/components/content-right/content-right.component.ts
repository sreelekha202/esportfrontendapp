import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  OnChanges,
} from '@angular/core';
import { EsportsSeasonService, EsportsGameService } from 'esports';
import { environment } from '../../../../../../../environments/environment';

const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;
@Component({
  selector: 'app-content-right',
  templateUrl: './content-right.component.html',
  styleUrls: ['./content-right.component.scss'],
})
export class ContentRightComponent implements OnInit {
  @Output() getValueData = new EventEmitter();
  @Input() seasonCheck = [];
  seasonTypeSingle = false;
  seasonTypeTeam = false;
  gameList: any;
  platformList: any;
  data = [];
  seasonFilter = {
    "sessionType": [],
    "duration": [],
    "platform": [],
    "game": [],
  };
  seasonCkeck = [
    {
      id: 'individual',
      name: 'Single player',
      check: false,
      isShowIcon: true,
      season: 'season',
    },
    {
      id: 'team',
      name: 'Team',
      check: false,
      isShowIcon: true,
      season: 'season',
    },
    {
      id: '31',
      name: '1 month',
      check: false,
      isShowIcon: true,
      season: 'length',
    },
    {
      id: '91',
      name: '3 months',
      check: false,
      isShowIcon: true,
      season: 'length',
    }
  ];
  showLoader: boolean = true;
  seasonsList = [];
  constructor(private esportsSeasonService: EsportsSeasonService,public gameService: EsportsGameService) {}

  ngOnInit(): void {
    this.getGamesList();
    this.getPlatformsList();
    this.getAllSeasons();
  }

  getGamesList() {
    this.gameService.getGames(API).subscribe(
      (res) => {
        this.gameList = res.data;
      },
      (err) => {}
    );
  }

  getPlatformsList() {
    this.gameService.fetchPlatforms(API).subscribe(
      (res) => {
        this.platformList = res.data;
      },
      (err) => {}
    );
  }

  getAllSeasons() {
    let query: any = `isActive=true&fields=isFeatured,name,slug,endDate,startDate,image,duration&sort=-createdOn`;
    this.esportsSeasonService.getSeasons(API, query).subscribe(
      (res) => {
        this.showLoader = false;
        this.seasonsList = res.data.data.docs;
      (err) => {
        this.showLoader = false;
      }
      });
  }


  checkSeasonType(type) {
    if (type == 'single') {
      if (!this.seasonTypeSingle) {
        this.seasonTypeTeam = false;
      }
    } else {
      if (!this.seasonTypeTeam) {
        this.seasonTypeSingle = false;
      }
    }
  }
  onChangeSeason(type,event, id) {
    switch (type) {
      case "seasonType":
        if(event.checked)
         this.seasonFilter.sessionType.push(id);
         else {
          this.removeArrayElement(this.seasonFilter.sessionType,id);
         }
        break;
      case "duration":
        if(event.checked)
        this.seasonFilter.duration.push(id);
        else {
          this.removeArrayElement(this.seasonFilter.duration,id);
         }
        break;
      case "platform":
        if(event.checked)
        this.seasonFilter.platform.push(id);
        else {
          this.removeArrayElement(this.seasonFilter.platform,id);
         }
        break;
      case "game":
        if(event.checked)
          this.seasonFilter.game.push(id);
          else {
            this.removeArrayElement(this.seasonFilter.game,id);
           }
          break;
      default:
        break;
    }
     
    this.getValueData.emit(this.seasonFilter);
    
    // const index = this.seasonCkeck.findIndex((item) => {
    //   return item.id === id;
    // });
    // if (index === -1) {
    //   return;
    // }
    // this.seasonCkeck[index] = {
    //   ...this.seasonCkeck[index],
    //   check: event.checked,
    // };

    // this.getValueData.emit(this.seasonCkeck);
  }

  removeArrayElement(inputArray,removeItem){
    inputArray.forEach((element,index)=>{
      if(element==removeItem) inputArray.splice(index,1);
   });
  }

  // ngOnChanges(): void {
  //   if (this.seasonCheck.length === 0) return;
  //   this.seasonCkeck = this.seasonCheck;
  // }
}
