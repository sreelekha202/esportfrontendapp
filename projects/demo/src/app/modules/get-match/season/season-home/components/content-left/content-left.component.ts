import { Component, Input, OnInit, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';
import { EsportsGameService, EsportsSeasonService, GlobalUtils, IPagination } from 'esports';
import { environment } from '../../../../../../../environments/environment';
const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;
@Component({
  selector: 'app-content-left',
  templateUrl: './content-left.component.html',
  styleUrls: ['./content-left.component.scss'],
})
export class ContentLeftComponent implements OnInit, OnChanges {
  @Input() seasonData : any;
  isShown: boolean = false;
  @Output() getValueData = new EventEmitter();
  showLoader: boolean = true;
  selectedGame: any;
  params: any = {};
  gameId: any;
  game: any;
  text: string = '';
  selected_game: any = '';
  sortType: any = '';
  seasonFilter: any;
  progress_width: any = '70%';
  seasonsList = [];
  showFilter = true;
  paginationData = {
    page: 1,
    limit: 10,
    sort: 'startDate',
  };
  totalDocs:any;
  paginationDetails: any;
  pageSizeOptions = environment.pageSizeOptions;
  listFilter = [
    {
      id: 1,
      content: 'mobile',
      isShowIcon: true,
    },
    {
      id: 2,
      content: 'console',
      isShowIcon: true,
    },
    {
      id: 3,
      content: 'Team',
      isShowIcon: true,
    },
  ];
  filterGame = [
    {
      fieldGame: 'name',
      filterName: 'A - Z',
    },
    {
      fieldGame: '-name',
      filterName: 'Z - A',
    },
    {
      fieldGame: '-createdOn',
      filterName: 'Start Date',
    }
  ];
  itemGame = [
    {
      src: '../../../../../../../assets/images/season/image 59.png',
      title: 'CS:GO Chapter 2 - Monthly Ladder',
      date: 'End Monday, December 11, 15:00 • Elimination',
      srcPlayer: '../../../../../../../assets/images/Profile/img2.png',
      name: 'By jagong_gong',
      player: 231,
    }
  ];
  page: IPagination;
  participantPage: IPagination = {
    activePage: 1,
    totalItems: 1,
    maxSize: 10,
    itemsPerPage: 9,
  };
  constructor(    private gameService: EsportsGameService,
    private esportsSeasonService: EsportsSeasonService,
    public router: Router,) {}

  ngOnInit(): void {
    if (GlobalUtils.isBrowser()) {
        this.getAllSeasons();
    }
    
  }

  ngOnChanges(changes: SimpleChanges): void {
    if(changes.seasonData.currentValue)
    {
      this.seasonFilter = changes.seasonData.currentValue;
      this.getAllSeasons();
    }
   
  }

  formAPIUrl(){

  }

  // showFilterFC() {
  //   this.seasonData = this.seasonData.map((item) => {
  //     return {
  //       ...item,
  //       check: false,
  //     };
  //   });
  //   this.getValueData.emit(this.seasonData);
  // }
  // removeSeason(item) {
  //   const index = this.seasonData.findIndex((each) => {
  //     return item.name === each.name;
  //   });
  //   if (index === -1) return;
  //   this.seasonData[index] = {
  //     ...this.seasonData[index],
  //     check: !this.seasonData[index].check,
  //   };
  //   this.getValueData.emit(this.seasonData);
  // }


  getAllSeasons() {
    let gameFilter, durationFilter, platformFilter, sessionTypeFilter  = '';
    let query: any;
    if(this.sortType == '')
    {
      this.sortType = '-createdOn';
    } 
    if(this.seasonFilter?.game.length > 0)
    {
      gameFilter =  this.seasonFilter.game.toString();
      gameFilter =  gameFilter.replace(/,/g, '||');
    }
    if(this.seasonFilter?.duration.length > 0)
    {
      durationFilter =  this.seasonFilter.duration.toString();
      durationFilter =  durationFilter.replace(/,/g, '||');
    }
    if(this.seasonFilter?.platform.length > 0)
    {
      platformFilter =  this.seasonFilter.platform.toString();
      platformFilter =  platformFilter.replace(/,/g, '||');
    }
    if(this.seasonFilter?.sessionType.length > 0)
    {
      sessionTypeFilter =  this.seasonFilter.sessionType.toString();
      sessionTypeFilter =  sessionTypeFilter.replace(/,/g, '||');
    }
   
    query = `isActive=true&fields=isFeatured,name,slug,endDate,startDate,image,duration&sort=${this.sortType}${(gameFilter == '' || gameFilter == undefined)? '' : `&game=${gameFilter}`}${(durationFilter == '' || durationFilter == undefined)? '' : `&duration=${durationFilter}`}${(platformFilter == '' || platformFilter == undefined)? '' : `&platform=${platformFilter}`}${(sessionTypeFilter == '' || sessionTypeFilter == undefined)? '' : `&participantType=${sessionTypeFilter}`}&limit=${this.participantPage.itemsPerPage}&page=${this.participantPage.activePage}`
    // query = `isActive=true&fields=isFeatured,name,slug,endDate,startDate,image,duration&sort=${this.sortType}&limit=${this.participantPage.itemsPerPage}&page=${this.participantPage.activePage}`;
    this.esportsSeasonService
      .getSeasons(
        API,
        query
      )
      .subscribe((res) => {
        this.seasonsList = res?.data?.data?.docs;
        this.findSessonPercentage();
        this.totalDocs = res?.data?.data?.totalDocs;
        this.participantPage.itemsPerPage = res?.data?.data?.limit;
        this.participantPage.totalItems = res?.data?.data?.totalDocs;
      });
  }
  findSessonPercentage() {
    let endDate: any;
    let currentDate: any;
    let startDate: any;
    let duration: any;
    let progressPercentage: any;
    this.seasonsList.forEach((element, index) => {
      endDate = new Date(element.endDate);
      startDate = new Date(element.startDate);
      currentDate = new Date();
      duration = element.duration;
      if (startDate > currentDate) {
        progressPercentage = "0%";
      } else if (endDate > currentDate) {
        progressPercentage = (100 - Math.round(100 *  Math.round((endDate-currentDate)/(1000*60*60*24)) / duration)) + "%";
      } else {
        progressPercentage = "100%";
      }
      element.progressPercentage = progressPercentage;
    });
  
  
   
  }
  currentPage(page): void {
    this.participantPage.activePage = page;
    this.getAllSeasons();
  }
  showPopup() {
    this.isShown = true;
  }
  hidePopup() {
    this.isShown = false;
  }
  sort(item) {
      this.sortType = item?.fieldGame;
     this.getAllSeasons();
    // const newPlayers = this.itemGame.sort((a, b) => {
    //   return (
    //     a[item.fieldGame].codePointAt(0) - b[item.fieldGame].codePointAt(0)
    //   );
    // });
    // this.itemGame = newPlayers;
  }
  // sortByPlayer() {
  //   const newPlayers = this.itemGame.sort((a, b) => {
  //     return a.player - b.player;
  //   });
  //   this.itemGame = newPlayers;
  // }
}
