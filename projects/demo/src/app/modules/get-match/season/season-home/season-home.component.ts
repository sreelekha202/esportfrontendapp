import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { EsportsSeasonService, GlobalUtils } from 'esports';
import { environment } from '../../../../../environments/environment';
import { NgbCarousel } from '@ng-bootstrap/ng-bootstrap';
const API = environment.apiEndPoint;
@Component({
  selector: 'app-season-home',
  templateUrl: './season-home.component.html',
  styleUrls: ['./season-home.component.scss'],
})
export class SeasonHomeComponent implements OnInit {
  @ViewChild('ngcarousel', {
    static: true,
  })
  ngCarousel: NgbCarousel;
  @Input() data: any;
  gameId: any;
  //seasonCheck: any;
  seasonCheck : Array<{}>;
  isClear = false;
  totalDocs: any;
  isShowSlider = false;

  constructor(
    private router: Router,
    private esportsSeasonService: EsportsSeasonService
  ) { }
  seassons = [];

  ngOnInit(): void {
    if (GlobalUtils.isBrowser()) {
      this.gameId = localStorage.getItem('gameId');
    }
    this.getSeasons();
  }

  getSeasons() {
    this.esportsSeasonService
      .getSeasons(
        API,
        'isActive=true&fields=isFeatured,name,slug,endDate,startDate,image,duration&sort=-createdOn&page=1&limit=8'
      )
      .subscribe((res) => {
        this.seassons = res?.data?.data?.docs;
      });
  }

  getToPrev() {
    this.ngCarousel.prev();
  }

  goToNext() {
    this.ngCarousel.next();
  }

  viewDtail() {
    this.router.navigateByUrl('/games/match-season/');
  }
  getValueData(event) {
    this.seasonCheck = {...event};;
    console.log('this.seasonCheck', this.seasonCheck);
  }
  isClearFilter(event) {
    this.seasonCheck = event;
  }
}
