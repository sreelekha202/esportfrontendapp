import { Component, OnInit } from '@angular/core';
import { AppHtmlGetMatchRoutes } from '../../../../app-routing.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-select-team',
  templateUrl: './select-team.component.html',
  styleUrls: ['./select-team.component.scss'],
})
export class SelectTeamComponent implements OnInit {
  AppHtmlGetMatchRoutes = AppHtmlGetMatchRoutes;
  constructor(private router: Router) {}
  teams = [];
  ready = false;
  ngOnInit(): void {
    this.teams = [
      {
        image:
          'https://i.pinimg.com/736x/f9/a8/c0/f9a8c0734c8e13adaa0d39fdf81274c2.jpg',
        name: 'Dark Ninja',
      },
    ];
  }

  activateClass(team){
    team.active = !team.active;
    this.ready = !this.ready;
  }
  toTeamGame(){
    this.router.navigateByUrl("/get-match/team-game");
  }
}
