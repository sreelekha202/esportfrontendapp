import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TournamentScoringComponent } from './tournament-scoring.component';

describe('TournamentScoringComponent', () => {
  let component: TournamentScoringComponent;
  let fixture: ComponentFixture<TournamentScoringComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TournamentScoringComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TournamentScoringComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
