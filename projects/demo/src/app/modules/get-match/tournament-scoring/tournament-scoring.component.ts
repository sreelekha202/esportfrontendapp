import { Component, OnInit } from '@angular/core';
import { AppHtmlGetMatchRoutes, AppHtmlRoutes } from '../../../app-routing.model';
import { Router } from '@angular/router';
import {MatRadioModule} from '@angular/material/radio';

@Component({
  selector: 'app-tournament-scoring',
  templateUrl: './tournament-scoring.component.html',
  styleUrls: ['./tournament-scoring.component.scss']
})
export class TournamentScoringComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;
  AppHtmlGetMatchRoutes = AppHtmlGetMatchRoutes;
  fileName = ''

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  getFile(event){
    this.fileName = event.target.files[0].name
  }
}
