import { Subscription } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { AppHtmlGetMatchRoutes } from '../../../app-routing.model';
import { Router } from '@angular/router';
import { EsportsToastService, EsportsGameService, GlobalUtils } from 'esports';
import { TranslateService } from '@ngx-translate/core';
import { environment } from '../../../../environments/environment';

const API = environment.apiEndPoint;
import {
  FormGroup,
  FormBuilder,
  FormControl,
  Validators,
  ControlContainer, 
  FormGroupDirective,
} from '@angular/forms';

import {
  EsportsTournamentService,
} from 'esports';

@Component({
  selector: 'app-select-game-type',
  templateUrl: './select-game-type.component.html',
  styleUrls: ['./select-game-type.component.scss'],
  viewProviders: [FormGroupDirective
  ],
})
export class SelectGameTypeComponent implements OnInit {
  Step2: FormGroup | null;
  AppHtmlGetMatchRoutes = AppHtmlGetMatchRoutes;
  gameTypes: any = [];
  selectedGameType: any;
  selectedGame: any;
  gameList: any;
  gameID: string;
  showLoader: boolean;
  games = [];
  text : string = '';

  totalPages : number = 2;
  step : number = 1;
  numberStep : number = 2;

  constructor(
    private translate: TranslateService,
    private router: Router,
    private gameService: EsportsGameService,
    private toastService: EsportsToastService,
    public eSportsTournamentService: EsportsTournamentService,
    private fb: FormBuilder,
  ) {
    if (GlobalUtils.isBrowser()) {
      this.gameID = localStorage.getItem('gameId');
    }
  }
  setGameType() {
    this.gameTypes.push({
      type: 'individual',
      icon: 'assets/icons/matchmaking/game-type/single-type.svg',
      title: 'MATCHMAKING.SELECT_PLATFORM.GAME_TYPES.SINGLE',
    });

    this.gameTypes.push({
      type: 'team',
      icon: 'assets/icons/matchmaking/game-type/team-type.svg',
      title: 'MATCHMAKING.SELECT_PLATFORM.GAME_TYPES.TEAMS',
    });
  }

  ngOnInit(): void {
    this.Step2 = this.fb.group({
      gameDetail: ['', Validators.required],
    })
    if (!this.Step2?.get('gameDetailsObj'))
      this.Step2.addControl('gameDetailsObj', new FormControl('', []));
   
    this.getGames();
  }

  onTextChange = (data) => {
    this.text = data;
    // search on API
    //  this.getGames();
  };

  // search on Array
  filterGame = () => {
    return this.games.filter((game) =>
      game.name.includes(this.text.toLowerCase().trim())
    );
  };

  getGames() {
    this.showLoader = true;
    const param: any = {};
    this.text ? (param.search = this.text) : '';
    this.eSportsTournamentService.getAllGames(param).subscribe(
      (res) => {
        this.showLoader = false;
        if (res && res.data && res.data.length) {
          res.data.map((obj) => {
            this.games.push({ ...obj, name: String(obj.name).toLowerCase() });
          });
        }
      },
      (err) => {
        this.showLoader = false;
      }
    );
  }

  onSelectGame(game: any) {
    const param: any = {};
    this.text ? (param.search = this.text) : '';
    this.eSportsTournamentService.setPlatformList = game?.platform;
    this.gameService.createMatchMakingSubject.next(game);
    this.selectedGame = game;   
  }
  

  next(){
    if(this.selectedGame){
      this.router.navigate(['/get-match/platforms']);
    }else{
      this.toastService.showError(
        this.translate.instant('SELECT_PLATFORM.ERR_CHOOSE_GAME')
      ); 
    }    
  }
}
