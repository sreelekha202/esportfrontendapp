import { Component, OnInit } from '@angular/core';
import { AppHtmlRoutes } from '../../../app-routing.model';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from '../../../../environments/environment';
import {
  EsportsChatService,
  EsportsToastService,
  EsportsTournamentService,
  EsportsGameService,
} from 'esports';
import { MatDialog } from '@angular/material/dialog';
import { TranslateService } from '@ngx-translate/core';
import {
  LadderPopupComponent,
  LadderPopupComponentData,
} from '../../../shared/popups/ladder-popup/ladder-popup.component';
import { Observable, Subscription } from 'rxjs';
import { formatDistanceToNowStrict } from 'date-fns';
import { Location, formatDate } from '@angular/common';
const API = environment.apiEndPoint;
@Component({
  selector: 'app-match-detail',
  templateUrl: './match-detail.component.html',
  styleUrls: ['./match-detail.component.scss'],
})
export class MatchDetailComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;
  seasonId: string = null;
  showLoader: boolean;
  gameInfo = null;
  seasonJoinStatus = null;
  matchSeason = null;
  tableData = [];
  page = 1;

  constructor(
    private gameService: EsportsGameService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private toastService: EsportsToastService,
    private chatService: EsportsChatService,
    private matDialog: MatDialog,
    private translateService: TranslateService,
    private esportsTournamentService: EsportsTournamentService,
    private location: Location
  ) {}

  ngOnInit(): void {
    this.showLoader = true;
    if (this.activatedRoute.snapshot.params.id) {
      this.seasonId = this.activatedRoute.snapshot.params.id;
      this.fetchSeasonDetails();
      this.loadLadder();
    }

    this.matchSeason = {};
    this.gameInfo = {};
    this.seasonJoinStatus = {};

    if (this.activatedRoute.snapshot.queryParamMap.get('playnow')) {
      this.openPlayNowPopup();
      this.router
        .navigateByUrl('/games', { skipLocationChange: true })
        .then(() => {
          this.router.navigate(['/games/match-season/', this.seasonId]);
        });
    }

    this.getSeasonJoinStatus();
    this.getRecentMatches();
  }

  playNow() {
    if (this.seasonJoinStatus?.isRegistered) {
      this.openPlayNowPopup();
    } else {
      if (this.matchSeason.participantType === 'team') {
        this.router.navigate([
          `/team-registration/${this.matchSeason?.slug}-${this.matchSeason?.id}`,
        ]);
      } else {
        this.router.navigate([
          `tournament/${this.matchSeason?.slug}-${this.matchSeason?.id}/join`,
        ]);
      }
    }
  }

  async getSeasonJoinStatus() {
    this.gameService
      .checkSeasonJoinStatus(API, this.seasonId)
      .subscribe((res) => {
        this.seasonJoinStatus = res.data;
      });
  }

  openPlayNowPopup() {
    this.chatService.connectSocket();
    let popupData: LadderPopupComponentData = {
      isShowLoading: true,
      payload: { leftTime: 180 },
    };

    const loadDialogRef = this.matDialog.open(LadderPopupComponent, {
      data: popupData,
      disableClose: true,
    });

    this.chatService.findSeasonMatch({
      seasonId: this.seasonId,
    });

    const successObservable = new Observable((subscriber) => {
      this.chatService.listenSeasonMatchFound(subscriber);
    });

    let matchFoundSubscription = successObservable.subscribe((data) => {
      if (data) {
        loadDialogRef.close(true);
        this.toastService.showSuccess(
          this.translateService.instant('Match Found')
        );
        this.router.navigate(['get-match/game-lobby'], {
          queryParams: { matchId: data['data']['newMatch']['_id'] },
        });
      }
    });

    const errorObservable = new Observable((subscriber) => {
      this.chatService.listenFindSeasonMatchError(subscriber);
    });

    let findSeasonMatchErrorSubscription = errorObservable.subscribe(
      (error) => {
        loadDialogRef.close(true);
        if (error['errorCode']) {
          this.toastService.showError(
            'You already have 1 active match in this tournament'
          );
        } else {
          this.toastService.showError(
            this.translateService.instant('LADDER.FIND_MATCH_ERROR')
          );
        }
      }
    );

    loadDialogRef.afterClosed().subscribe((loadCloseData) => {
      this.chatService.removeFromQueue('');
      matchFoundSubscription.unsubscribe();
      findSeasonMatchErrorSubscription.unsubscribe();
      if (loadCloseData == 'timeOut') {
        const notFoundPopupRef = this.matDialog.open(LadderPopupComponent, {
          data: { isShowNotFoundSeason: true },
        });
        notFoundPopupRef.afterClosed().subscribe((action) => {
          if (action == 'searchAgain') {
            this.openPlayNowPopup();
          }
        });
      }
    });
  }

  async loadLadder() {
    const parms = {
      id: this.seasonId,
      page: this.page,
    };
    // /6102497780f8e50008ebb587
    this.esportsTournamentService
      .getLaderStanding(API, parms)
      .subscribe((res) => {
        this.tableData = res.data.standing.docs;
        this.showLoader = false;
      });
  }

  getRecentMatches() {
    const params = {
      query: JSON.stringify({
        seasonId: this.seasonId,
        limit: 1,
        page: 1,
      }),
    };
    this.gameService.fetchRecentSeasonMatches(API, params).subscribe(
      (res) => {
        this.showLoader = false;
        this.gameInfo.matches = res.data.docs;
      },
      (err) => {
        this.showLoader = false;
      }
    );
  }
  fetchSeasonDetails = async () => {
    this.gameService.fetchSeasonDetails(API, this.seasonId).subscribe((res) => {
      const data = res['data']['data'];
      let text;
      const endDate = new Date(data.endDate);
      const startDate = new Date(data.startDate);
      const currentDate = new Date();
      if (startDate > currentDate) {
        text = `Starts ${formatDistanceToNowStrict(startDate, {
          addSuffix: true,
          unit: 'day',
        })}`;
      } else if (endDate > currentDate) {
        text = `Ends ${formatDistanceToNowStrict(endDate, {
          addSuffix: true,
          unit: 'day',
        })}`;
      } else {
        text = 'Season ended';
      }
      //  this.matchSeason = data;
      Object.assign(this.matchSeason, {
        title: data?.name,
        text: text,
        image: data?.image,
        id: data?._id,
        participantType: data?.participantType,
        slug: data?.slug,
      });

      Object.assign(this.gameInfo, {
        image: data?.game?.image,
        icon: data?.game?.icon,
        details: {
          platform: data?.platform?.map((ele) => ele?.name).join(', '),
          duration: `${data?.duration} days`,
          endDate: formatDate(endDate, 'dd MMM yyyy', 'en'),
          totalPlayers: data?.participantCount,
        },
      });
      this.showLoader = false;
    });
  };
}
