import { Component, OnInit } from '@angular/core';
import { AppHtmlRoutes } from '../../../app-routing.model';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'app-footer-matchmaking',
  templateUrl: './footer-matchmaking.component.html',
  styleUrls: ['./footer-matchmaking.component.scss']
})
export class FooterMatchmakingComponent implements OnInit {

  AppHtmlRoutes = AppHtmlRoutes;

  buildConfig = environment.buildConfig || 'N/A';

  socials = [
    {
      link: 'https://twitter.com/SparkGaming',
      icon: 'assets/icons/socials/icon-twitter.svg',
    },
    {
      link: 'https://www.facebook.com/Sparkgaming',
      icon: 'assets/icons/socials/icon-fb.svg',
    },
    {
      link: 'https://www.instagram.com/Sparkgaming/',
      icon: 'assets/icons/socials/icon-insta.svg',
    },
    {
      link: 'https://www.linkedin.com/company/Spark-gaming/',
      icon: 'assets/icons/socials/icon-linke.svg',
    },
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
