import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FooterMatchmakingComponent } from './footer-matchmaking.component';

describe('FooterMatchmakingComponent', () => {
  let component: FooterMatchmakingComponent;
  let fixture: ComponentFixture<FooterMatchmakingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FooterMatchmakingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FooterMatchmakingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
