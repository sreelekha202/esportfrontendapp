import { Component, ElementRef, HostListener, OnInit, ViewChild } from '@angular/core';
import { AppHtmlGetMatchRoutes, AppHtmlRoutes } from '../../../app-routing.model';
import { Subscription } from 'rxjs';
import { EsportsGameService } from 'esports';

@Component({
  selector: 'app-match-lobby-api',
  templateUrl: './match-lobby-api.component.html',
  styleUrls: ['./match-lobby-api.component.scss']
})
export class MatchLobbyApiComponent implements OnInit {
  @ViewChild('popup') popup: ElementRef;
  @ViewChild('btnShowPopup') btnShowPopup: ElementRef;
  @ViewChild('btnShowPopupfoot') btnShowPopupfoot: ElementRef;
  AppHtmlRoutes = AppHtmlRoutes
  AppHtmlGetMatchRoutes = AppHtmlGetMatchRoutes;
  platforms = [];
  gameTypes = [];
  matchmakingDetails: Subscription;
  createMatchMaking: any;
  selectedGame: any = null;
  selectedPlatformDetails: any = null;
  show = false;
  contentChat = false;
  ready = false;
  constructor(
    private gameService: EsportsGameService) {
    this.matchmakingDetails = this.gameService.createMatchMaking.subscribe((data: any) => {
      if (data) {
        this.createMatchMaking = data;
        this.selectedGame = data.selectedGame;
        this.setPlatForm(this.selectedGame['platform'])
        this.setGameType(this.selectedGame['bracketTypes'])
      }
    });
  }

  setGameType(arg: any) {
    // arg && arg.single
    if (true) {
      this.gameTypes.push(
        {
          type: 'individual',
          icon: 'assets/icons/matchmaking/game-type/single-type.svg',
          title: 'MATCHMAKING.SELECT_PLATFORM.GAME_TYPES.SINGLE',
        },
      )
    }
    // arg && arg.double
    if (true) {
      this.gameTypes.push(
        {
          type: 'team',
          icon: 'assets/icons/matchmaking/game-type/team-type.svg',
          title: 'MATCHMAKING.SELECT_PLATFORM.GAME_TYPES.TEAMS',
        },
      )
    }
  }
  setPlatForm(data) {
    data.map((obj) => {
      if (obj.name == 'pc') {
        this.platforms.push({
          ...obj,
          type: 'pc',
          icon: 'assets/icons/matchmaking/platforms/pc.svg',
          title: 'MATCHMAKING.SELECT_PLATFORM.PLATFORMS.PC',
        })
      }
      else if (obj.name == 'Mobile') {
        this.platforms.push({
          ...obj,
          type: 'mobile',
          icon: 'assets/icons/matchmaking/platforms/mobile.svg',
          title: 'MATCHMAKING.SELECT_PLATFORM.PLATFORMS.MOBILE',
        })
      }
      else if (obj.name == 'Other') {
        this.platforms.push({
          ...obj,
          type: 'console',
          icon: 'assets/icons/matchmaking/platforms/console.svg',
          title: 'MATCHMAKING.SELECT_PLATFORM.PLATFORMS.CONSOLE',
        })
      }
      else if (obj.name == 'Xbox') {
        this.platforms.push({
          ...obj,
          type: 'xbox',
          icon: 'assets/icons/matchmaking/platforms/xbox.svg',
          title: 'MATCHMAKING.SELECT_PLATFORM.PLATFORMS.XBOX',
        })
      }
      else if (obj.name == 'PS4') {
        this.platforms.push({
          ...obj,
          type: 'ps4',
          icon: 'assets/icons/matchmaking/platforms/ps4.svg',
          title: 'MATCHMAKING.SELECT_PLATFORM.PLATFORMS.PS4',
        })
      }
      else if (obj.name == 'PS5') {
        this.platforms.push({
          ...obj,
          type: 'ps5',
          icon: 'assets/icons/matchmaking/platforms/ps5.svg',
          title: 'MATCHMAKING.SELECT_PLATFORM.PLATFORMS.PS5',
        })
      }
      else {
        this.platforms.push({
          ...obj,
          type: 'pc',
          icon: 'assets/icons/matchmaking/platforms/pc.svg',
          title: 'MATCHMAKING.SELECT_PLATFORM.PLATFORMS.PC',
        })
      }
    })
  }

  ngOnInit(): void {
    this.matchmakingDetails = this.gameService.createMatchMaking.subscribe(
      (data: any) => {
        if (data) {
          this.selectedGame = data.selectedGame;
        }
      }
    );
  }

  @HostListener('document:click', ['$event'])
  clickOutside(event) {
    if (!(this.popup.nativeElement.contains(event.target)) && this.show === true && !(this.btnShowPopup.nativeElement.contains(event.target)) && !(this.btnShowPopupfoot.nativeElement.contains(event.target))) {
      this.show = false;
    }
  }

}
