import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TournamentCardOverviewComponent } from './tournament-card-overview.component';

describe('TournamentCardOverviewComponent', () => {
  let component: TournamentCardOverviewComponent;
  let fixture: ComponentFixture<TournamentCardOverviewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TournamentCardOverviewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TournamentCardOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
