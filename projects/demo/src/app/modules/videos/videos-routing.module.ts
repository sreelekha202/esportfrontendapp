import { Routes, RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";

import { VideoViewComponent } from "./video-view/video-view.component";
import { VideosComponent } from "./videos.component";
import { CreateVideoLibraryComponent } from "./create-video-library/create-video-library.component";

const routes: Routes = [
  { path: "", component: VideosComponent },
  { path: "create-video-library", component: CreateVideoLibraryComponent },
  { path: "edit-video-library/:id", component: CreateVideoLibraryComponent },
  { path: "view/:id", component: VideoViewComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VideosRoutingModule {}
