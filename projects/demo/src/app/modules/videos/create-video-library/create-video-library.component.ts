import { Component, OnInit } from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl,
} from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
// import { FormService } from '../../../core/service';
import {
  EsportsGameService,
  EsportsConstantsService,
  EsportsToastService,
  EsportsLanguageService,
  EsportsUtilsService,
  EsportsVideoLibraryService,
  EsportsOptionService,
  EsportsFormService,
  EsportsInfoPopupComponent,
  EsportsInfoPopupComponentData,
  EsportsInfoPopupComponentType,
} from 'esports';
import { AppHtmlProfileRoutes } from '../../../app-routing.model';
import { TranslateService } from '@ngx-translate/core';
import { environment } from '../../../../environments/environment';
import { MatDialog } from '@angular/material/dialog';

const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;
@Component({
  selector: 'app-create-video-library',
  templateUrl: './create-video-library.component.html',
  styleUrls: [
    './create-video-library.component.scss',
    '../../article/create-article/create-article.component.scss',
  ],
})
export class CreateVideoLibraryComponent implements OnInit {
  AppHtmlProfileRoutes = AppHtmlProfileRoutes;
  videoLibraryForm: FormGroup;
  tagsList = [];
  categoryList = [];
  gameList = [];
  platformList = [];
  genreList = [];
  language = [];
  currLangauge = '';
  currLanguageDisplay = '';
  navUrl = '../../';
  apiLoaded: Array<boolean> = [];
  isLoaded = false;
  isProcessing = false;
  slug;
  showLoader: boolean = true;
  check_url = false;
  constructor(
    private fb: FormBuilder,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private videoLibraryService: EsportsVideoLibraryService,
    private toastService: EsportsToastService,
    private utilsService: EsportsUtilsService,
    private gameService: EsportsGameService,
    private translateService: TranslateService,
    private languageService: EsportsLanguageService,
    private optionService: EsportsOptionService,
    private constantsService: EsportsConstantsService,
    private formService: EsportsFormService,
    public dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.language = this.constantsService?.language;
    this.currLangauge = this.language[0]?.key;
    this.currLanguageDisplay = this.language[0]?.value;
    this.languageService.language.subscribe((lang) =>
      this.translateService.use(lang || this.constantsService?.defaultLangCode)
    );
    this.fetchOptions();
    this.videoLibraryForm = this.fb.group({
      title: this.fb.group(this.constantsService.languageFC(), {
        validator: this.formService.groupFieldValidator(),
      }),
      // ,{ validator: this.groupFieldValidator }
      youtubeUrl: ['', Validators.compose([Validators.required])],
      description: this.fb.group(this.constantsService.languageFC(), {
        validator: this.formService.groupFieldValidator(),
      }),
      tags: [
        [],
        Validators.compose([]),
      ],
      platforms: [
        [],
        Validators.compose([]),
      ],
      genres: [
        [],
        Validators.compose([]),
      ],
      game: ['', Validators.compose([Validators.required])],
      category: ['', Validators.compose([Validators.required])],
    });
    this.slug = this.activeRoute.snapshot.params.id;
    this.checkRouter();
    this.router.events.subscribe((val) => {
      this.checkRouter();
    });
  }
  checkRouter() {
    if (this.router.url === '/profile/add-video') {
      this.check_url = true;
    }
  }
  /**
   * Get Options
   */
  fetchOptions = async () => {
    try {
      this.apiLoaded.push(false);
      const option = await Promise.all([
        this.gameService.getAllGames(API, '').toPromise(),
        this.optionService.fetchAllCategories(API),
        this.optionService.fetchAllGenres(API),
        this.optionService.fetchAllTags(API),
      ]);
      this.gameList = option[0]?.data;
      this.categoryList = option[1]?.data;
      this.genreList = option[2]?.data;
      this.tagsList = option[3]?.data;

      if (this.slug) {
        this.fetchVideoLibraryById(this.slug);
      }
      this.apiLoaded.push(true);
    } catch (error) {
      this.apiLoaded.push(true);
      this.toastService.showError(error?.error?.message || error.message);
    } finally {
      this.allApiDataLoaded();
    }
  };

  /**
   * fetch video details by Id
   */
  fetchVideoLibraryById = async (id) => {
    try {
      this.apiLoaded.push(false);
      this.navUrl = this.navUrl + '../';
      const response = await this.videoLibraryService.fetchVideoLibraryById(
        API,
        id
      );
      const data = response?.data || null;
      data.title = !data.title
        ? ''
        : typeof data.title === 'object'
          ? data.title
          : { english: data.title, arabic: data.title };
      data.description = !data.description
        ? ''
        : typeof data.description === 'object'
          ? data.description
          : { english: data.description, arabic: data.description };
      if (data) {
        this.videoLibraryForm.addControl('id', new FormControl(id));

        const category = data.category._id;
        const tags = data.tags.map((item) => item._id);
        const platforms = data.platforms.map((item) => item._id);
        const genres = data.genres.map((item) => item._id);
        const game = data.game._id;

        this.videoLibraryForm.patchValue({
          ...data,
          category,
          tags,
          platforms,
          genres,
          game,
        });
        this.setPlatformList(data.game._id, this.gameList);
      } else {
        this.toastService.showError(
          this.translateService.instant('VIDEO_CREATE.DELETE')
        );
      }
      this.apiLoaded.push(true);
    } catch (error) {
      this.apiLoaded.push(true);
      this.toastService.showError(error?.error?.message || error?.message);
    } finally {
      this.allApiDataLoaded();
    }
  };

  /**
   * Check All API's Are Finished.
   * If All APi's Finished Then Remove Full Page Loader
   */
  allApiDataLoaded() {
    const startedApiCount = this.apiLoaded.filter((el) => !el).length;
    const finishedApiCount = this.apiLoaded.filter((el) => el).length;
    this.isLoaded = startedApiCount === finishedApiCount;
  }

  /**
   * Get Platform List For Selected Game
   * @param gameList gameList
   * @param game game
   */
  setPlatformList(_id, gameList) {
    if (_id && gameList.length) {
      this.platformList = gameList.find((item) => item._id == _id)?.platform;
    }
  }

  onChangePlatform() {
    this.videoLibraryForm.get('platforms').setValue([]);
    this.setPlatformList(this.videoLibraryForm.value.game, this.gameList);
  }
  /**
   * Array validation
   * @param control ArrayFormControl
   */
  arrayValidation(control: FormControl) {
    if (!control.value.length) {
      return { required: true };
    }
    return null;
  }

  /**
   * Validate Youtube Url
   * @param control
   */
  // validateUrl(control: FormControl) {
  //   if (control.value) {
  //     return control.value.match(
  //       `^(https?\:\/\/)?(www\.youtube\.com|youtu\.?be)\/.+$`
  //     )
  //       ? null
  //       : { invalidUrl: true };
  //   } else {
  //     return null;
  //   }
  // }

  /**
   * Set Values In Form
   * @param formControl formControl
   * @param value value
   * @param isAllowMultiple isAllowMultiple
   * @param platform platform
   */
  addItem(formControl, value, isAllowMultiple = true, platform = null) {
    if (isAllowMultiple) {
      const array = this.videoLibraryForm.get(formControl).value;
      const index = array.indexOf(value);
      if (index < 0) {
        array.push(value);
        this.videoLibraryForm.controls[formControl].setValue(array);
      }
    } else {
      this.videoLibraryForm.get(formControl).setValue(value);
      if (platform) {
        this.platformList = platform?.data;
        this.videoLibraryForm.controls.platforms.setValue([]);
        this.videoLibraryForm.get('platforms').updateValueAndValidity();
      }
    }
    this.videoLibraryForm.get(formControl).updateValueAndValidity();
  }

  /**
   * Remove Value from the Form
   * @param formControl formControl
   * @param value value
   */
  removeItem(formControl, value) {
    const index = this.videoLibraryForm.value[formControl].indexOf(value);
    this.videoLibraryForm.value[formControl].splice(index, 1);
    this.videoLibraryForm.get(formControl).updateValueAndValidity();
  }

  /**
   * Set Marked As touch For Form Fields
   * @param formControl formControl
   */
  setFomControlTouched(formControl) {
    this.videoLibraryForm.controls[formControl].markAsTouched({
      onlySelf: true,
    });
  }

  /**
   * Save Video Data
   */
  submit = async () => {
    try {
      if (this.videoLibraryForm.invalid) {
        this.videoLibraryForm.markAllAsTouched();
        return;
      }

      this.isProcessing = true;
      const payload = await this.addDefaultContent(this.videoLibraryForm.value);
      const videoId = await this.utilsService.getVideoId(payload.youtubeUrl);
      payload.thumbnailUrl = `https://img.youtube.com/vi/${videoId}/hqdefault.jpg`;
      // payload.thumbnailUrl = ' ';
      const response = payload.id
        ? await this.videoLibraryService.updateVideoLibrary(API, payload)
        : await this.videoLibraryService.createVideoLibrary(API, payload);

      this.toastService.showSuccess(response.message);
      this.router.navigateByUrl('/profile/content?activeTab=2');

      this.isProcessing = false;
    } catch (error) {
      this.isProcessing = false;
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  addDefaultContent = async (payload) => {
    const fillTitle: Array<any> = this.language.filter(
      (el: string) => payload.title[el]
    );
    const emptyTitle: Array<any> = this.language.filter(
      (el: string) => !payload.title[el]
    );

    if (fillTitle.length !== 2) {
      payload.title[emptyTitle[0]] = payload.title[fillTitle[0]];
    }

    const fillDescription: Array<any> = this.language.filter(
      (el: string) => payload.description[el]
    );
    const emptyDescription: Array<any> = this.language.filter(
      (el: string) => !payload.description[el]
    );

    if (fillDescription.length !== 2) {
      payload.description[emptyDescription[0]] =
        payload.description[fillDescription[0]];
    }

    return payload;
  };

  changeLocalLanguage(lang) {
    this.currLangauge = lang?.key;
    this.currLanguageDisplay = lang?.value;
  }
  isAddVideo() {
    return this.router.url === '/profile/add-video'
  }
  close(data) {
    const Data: EsportsInfoPopupComponentData = {
      title: this.translateService.instant('VIDEO.POPUP.TITLE'),
      text: this.translateService.instant('VIDEO.POPUP.DESCRIPTION'),
      type: EsportsInfoPopupComponentType.info,
      btnText: this.translateService.instant('BUTTON.BACK_TO_PROFILE'),
      cancelBtnText: this.translateService.instant('HEADER_DYNASTY.CANCEL'),
    };

    const dialogRef = this.dialog.open(EsportsInfoPopupComponent, {
      data: Data,
    });
    dialogRef.afterClosed().subscribe((confirmed) => {
      if (confirmed) {
        this.router.navigateByUrl('profile');
      }
    });
  }
}
