import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { CreateVideoLibraryComponent } from "./create-video-library.component";

describe("CreateVideoLibraryComponent", () => {
  let component: CreateVideoLibraryComponent;
  let fixture: ComponentFixture<CreateVideoLibraryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CreateVideoLibraryComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateVideoLibraryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
