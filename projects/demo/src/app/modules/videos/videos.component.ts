import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import {
  IPagination,
  EsportsConstantsService,
  EsportsVideoLibraryService,
  EsportsLanguageService,
  EsportsToastService,
} from 'esports';
import { environment } from '../../../environments/environment';

const API = environment.apiEndPoint;

enum VideosFilter {
  allVideos,
  recent,
}

@Component({
  selector: 'app-videos',
  templateUrl: './videos.component.html',
  styleUrls: ['./videos.component.scss'],
})
export class VideosComponent implements OnInit {
  VideosFilter = VideosFilter;
  videosFilter: VideosFilter = VideosFilter.allVideos;
  videos: Array<any> = [];
  pagination = {
    page: 1,
    limit: 20,
    sort: '-updatedOn',
    projection: [
      '_id',
      'slug',
      'title',
      'description',
      'youtubeUrl',
      'thumbnailUrl',
      'updatedOn',
    ],
  };
  videoDetails;
  isLoaded = false;
  page: IPagination;

  constructor(
    private videoLibraryService: EsportsVideoLibraryService,
    private constantsService: EsportsConstantsService,
    private toastService: EsportsToastService,
    private translateService: TranslateService,
    private languageService: EsportsLanguageService
  ) {}

  ngOnInit(): void {
    this.languageService.language.subscribe((lang) =>
      this.translateService.use(lang || this.constantsService?.defaultLangCode)
    );
    this.fetchAllVideoLibrary();
  }

  fetchAllVideoLibrary = async () => {
    try {
      this.isLoaded = false;
      const encodeUrl = `?pagination=${encodeURIComponent(
        JSON.stringify(this.pagination)
      )}`;
      this.videoDetails = await this.videoLibraryService.fetchVideoLibrary(
        API,
        encodeUrl
      );
      this.page = {
        totalItems: this.videoDetails?.data?.totalDocs,
        itemsPerPage: this.videoDetails?.data?.limit,
        maxSize: 5,
      };
      this.isLoaded = true;
    } catch (error) {
      this.isLoaded = true;
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  pageChanged(page) {
    this.pagination.page = page;
    this.fetchAllVideoLibrary();
  }
}
