import {
  Component,
  OnDestroy,
  OnInit,
  PLATFORM_ID,
  Inject,
} from '@angular/core';
import { Location, isPlatformBrowser } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import {
  EsportsUserService,
  EsportsToastService,
  EsportsLanguageService,
  GlobalUtils,
  EsportsUtilsService,
  EsportsVideoLibraryService,
  CustomTranslatePipe,
  EsportsHomeService,
  EsportsUserPreferenceService
} from 'esports';
import { TranslateService } from '@ngx-translate/core';
import {
  InfoPopupComponent,
  InfoPopupComponentData,
  InfoPopupComponentType,
} from '../../../shared/popups/info-popup/info-popup.component';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { DOCUMENT } from '@angular/common';
import { Title } from '@angular/platform-browser';
import { environment } from '../../../../environments/environment';

const API = environment.apiEndPoint;
@Component({
  selector: 'app-video-view',
  templateUrl: './video-view.component.html',
  styleUrls: ['./video-view.component.scss'],
  providers: [CustomTranslatePipe],
})
export class VideoViewComponent implements OnInit, OnDestroy {
  videoDetails;
  userId;
  isBookmarked = false;
  videoId;
  isLoaded = false;
  isBrowser: boolean;

  userSubscription: Subscription;

  constructor(
    public location: Location,
    public utilsService: EsportsUtilsService,
    private activeRoute: ActivatedRoute,
    private videoLibraryService: EsportsVideoLibraryService,
    private toastService: EsportsToastService,
    private userPreferenceService: EsportsUserPreferenceService,
    private userService: EsportsUserService,
    private translateService: TranslateService,
    public languageService: EsportsLanguageService,
    private homeService: EsportsHomeService,
    public matDialog: MatDialog,
    private router: Router,
    @Inject(PLATFORM_ID) platformId: Object,
    private titleService: Title,
    private customTranslatePipe: CustomTranslatePipe,
    @Inject(DOCUMENT) private document: Document,
    private globalUtils: GlobalUtils
  ) {
    this.isBrowser = isPlatformBrowser(platformId);
  }

  ngOnInit(): void {
    const slug = this.activeRoute.snapshot.params.id;
    this.getCurrentUserDetails();
    this.fetchVideoDetails(slug);
  }

  ngOnDestroy() {
    this.userSubscription?.unsubscribe();
  }

  fetchVideoDetails = async (id) => {
    try {
      this.isLoaded = false;
      const video = await this.videoLibraryService.fetchVideoLibraryBySlug(
        API,
        id
      );
      video.data.youtubeUrl = await this.utilsService.getEmbbedUrl(
        video?.data?.youtubeUrl
      );
      this.videoDetails = video.data;
      this.videoId = this.videoDetails?._id;
      this.setMetaTags(this.videoDetails);
      this.isLoaded = true;
      if (this.userId) {
        this.getBookmarks();
      }
      if (this.isBrowser) {
        this.updateView();
      }
    } catch (error) {
      this.isLoaded = true;
      this.toastService.showError(error?.error?.message || error?.message);
      this.router.navigate(['/404']);
    }
  };

  setMetaTags(videoDetails) {
    this.titleService.setTitle(
      this.customTranslatePipe.transform(videoDetails?.title)
    );
    if (videoDetails?.thumbnailUrl) {
      this.globalUtils.setMetaTags([
        {
          property: 'twitter:image',
          content: videoDetails?.thumbnailUrl,
        },
        {
          property: 'og:image',
          content: videoDetails?.thumbnailUrl,
        },
        {
          property: 'og:image:secure_url',
          content: videoDetails?.thumbnailUrl,
        },
        {
          property: 'og:image:url',
          content: videoDetails?.thumbnailUrl,
        },
        {
          property: 'og:image:width',
          content: '1200',
        },
        {
          property: 'og:image:height',
          content: '630',
        },
        {
          name: 'description',
          content: this.customTranslatePipe.transform(
            videoDetails?.description
          ),
        },
        {
          name: 'title',
          content: this.customTranslatePipe.transform(videoDetails?.title),
        },
        {
          property: 'og:description',
          content: this.customTranslatePipe.transform(
            videoDetails?.description
          ),
        },
        {
          property: 'twitter:description',
          content: this.customTranslatePipe.transform(
            videoDetails?.description
          ),
        },
        {
          property: 'og:title',
          content: this.customTranslatePipe.transform(videoDetails?.title),
        },
        {
          property: 'twitter:title',
          content: this.customTranslatePipe.transform(videoDetails?.title),
        },
        {
          property: 'og:url',
          content:
            this.document.location.protocol +
            '//' +
            this.document.location.hostname +
            this.router.url,
        },
      ]);
    }
  }

  addBookmark = async (id) => {
    try {
      const body = {
        videoLibraryId: id,
      };
      const bookmark = await this.userPreferenceService
        .addBookmark(body)
        .toPromise();
      this.toastService.showSuccess(bookmark?.message);
      this.getBookmarks();
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  removeBookmark = async (id) => {
    try {
      const queryParam = `videoLibraryId=${id}`;
      const bookmark = await this.userPreferenceService
        .removeBookmark(queryParam)
        .toPromise();
      this.toastService.showSuccess(bookmark?.message);
      this.getBookmarks();
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  getBookmarks = async () => {
    try {
      const filter = JSON.stringify({
        userId: this.userId,
        videoLibrary: this.videoId,
      });
      const projection = 'videoLibrary';
      const userPreference = await this.userPreferenceService
        .getPreferences(filter, projection)
        .toPromise();
      this.isBookmarked = userPreference?.data?.length ? true : false;
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  getCurrentUserDetails() {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.userId = data._id;
      }
    });
  }

  updateView = async () => {
    const payload = {
      _id: this.videoId,
      modalName: 'videoLibrary',
    };
    await this.homeService.updateView(payload);
  };

  openSocialShareComponent() {
    const data: InfoPopupComponentData = {
      title: this.translateService.instant('ARTICLE_POST.SHARE_LINK'),
      text: ``,
      type: InfoPopupComponentType.socialSharing,
      cancelBtnText: 'Close',
    };
    this.matDialog.open(InfoPopupComponent, {
      data,
    });
  }

  back() {
    this.location.back();
  }
}
