import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { TournamentComponent } from './tournament.component';
import { AuthGuard } from '../../shared/guard/auth.guard';

const routes: Routes = [

  {
    path: ':id',
    loadChildren: () =>
      import(
        '../../modules/tournament/view-tournament/view-tournament.module'
      ).then((m) => m.ViewTournamentModule),
  },
  { path: '', component: TournamentComponent },
  
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TournamentRoutingModule {}
