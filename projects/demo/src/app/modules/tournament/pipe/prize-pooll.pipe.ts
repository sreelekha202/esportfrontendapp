import { Pipe, PipeTransform } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
@Pipe({
  name: "prizePooll",
})
export class PrizePoollPipe implements PipeTransform {
  constructor(private translateService: TranslateService) { }

  transform(value, lang) {
    const position = (value || 0) + 1;
    const remainder = position % 10;
    if (lang == "fr") {
      switch (remainder) {
        case 1:
          return `${this.translateService.instant(
            "TOURNAMENT.CREATE.PRIZE_TITLE"
          )} ${position}er ${this.translateService.instant(
            "TOURNAMENT.CREATE.PRIZE_PLACE"
          )}`;
        case 2:
          return `${this.translateService.instant(
            "TOURNAMENT.CREATE.PRIZE_TITLE"
          )} ${position}ème ${this.translateService.instant(
            "TOURNAMENT.CREATE.PRIZE_PLACE"
          )}`;
        case 3:
          return `${this.translateService.instant(
            "TOURNAMENT.CREATE.PRIZE_TITLE"
          )} ${position}ème ${this.translateService.instant(
            "TOURNAMENT.CREATE.PRIZE_PLACE"
          )}`;
        default:
          return `${this.translateService.instant(
            "TOURNAMENT.CREATE.PRIZE_TITLE"
          )} ${position}ème ${this.translateService.instant(
            "TOURNAMENT.CREATE.PRIZE_PLACE"
          )}`;
      }
    } else {
      switch (remainder) {
        case 1:
          return `${position}st ${this.translateService.instant(
            "TOURNAMENT.CREATE.PRIZE_PLACE"
          )} ${this.translateService.instant("TOURNAMENT.CREATE.PRIZE_TITLE")}`;
        case 2:
          return `${position}nd ${this.translateService.instant(
            "TOURNAMENT.CREATE.PRIZE_PLACE"
          )} ${this.translateService.instant("TOURNAMENT.CREATE.PRIZE_TITLE")}`;
        case 3:
          return `${position}rd ${this.translateService.instant(
            "TOURNAMENT.CREATE.PRIZE_PLACE"
          )} ${this.translateService.instant("TOURNAMENT.CREATE.PRIZE_TITLE")}`;
        default:
          return `${position}th ${this.translateService.instant(
            "TOURNAMENT.CREATE.PRIZE_PLACE"
          )} ${this.translateService.instant("TOURNAMENT.CREATE.PRIZE_TITLE")}`;
      }
    }
  }
}
