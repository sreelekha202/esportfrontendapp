import { Component, OnInit, Input } from '@angular/core';
import { EsportsGameService, EsportsLanguageService, EsportsUserService, GlobalUtils, IUser } from 'esports';
import { environment } from '../../../../../environments/environment';
const API = environment.apiEndPoint;

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.scss', '../view-tournament.component.scss'],
})
export class OverviewComponent implements OnInit {
  @Input() tournamentDetails;
  registrationStatus = {
    isOpen: false,
    isClosed: false,
    notYetStarted: false,
  };
  platformList: any = [];
  date=new Date() ;
  tournament_endDate:any;
  isEnd: boolean = false;
  isNotEnded: boolean = false;


  constructor(
    public languageService: EsportsLanguageService,
    private esportsGameService: EsportsGameService,
    private userService: EsportsUserService,
    ) {}

    user: IUser;

  ngOnInit(): void {
    // MOCK DATA
    this.tournamentDetails = localStorage.getItem("tournamentDetails")
    this.tournamentDetails = JSON.parse(this.tournamentDetails)
    this.tournament_endDate=new Date(this.tournamentDetails.endDate);
    if (this.tournament_endDate<this.date){
      this.isEnd = true;
    }else if (this.tournament_endDate>=this.date){
      this.isNotEnded = true;
    }
    this.setRegistrationOpenStatus();
    this.getPlatformList();
  }

  getCurrentUserDetails() {
    this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.user = data;
      }
    });
  }

  getPlatformList() {
    this.esportsGameService.fetchPlatforms(API).subscribe((res) => {
      this.platformList = res.data;
    });
  }

  getPlatformName(p_id) {
    let name = '';
    this.platformList.map((res) => {
      if (p_id == res?._id) {
        name = res.name;
      }
    });
    return name;
  }

  setRegistrationOpenStatus() {

    const now = new Date().getTime();
    const tournamentStartTime = new Date(this.tournamentDetails?.startDate).getTime();
    const regStartTime = new Date(this.tournamentDetails?.regStartDate).getTime();
    const regEndTime = new Date(this.tournamentDetails?.regEndDate).getTime();

    if(this.tournamentDetails?.isRegistrationClosed) {
      this.registrationStatus.isClosed = true;
      return;
    }
    if(this.tournamentDetails.isSeeded || now > tournamentStartTime){
      this.registrationStatus.isClosed = true;
      return;
    }

    if (this.tournamentDetails['isRegStartDate'] && now > regEndTime) {
      this.registrationStatus.isClosed = true;
      return;
    }

    if(this.tournamentDetails['isRegStartDate'] && regStartTime > now) {
      this.registrationStatus.notYetStarted = true;
      return;
    }
    this.registrationStatus.isOpen = true;
  }
}
