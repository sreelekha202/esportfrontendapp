import { Component, OnInit, Input } from '@angular/core';
import { EsportsConstantsService, EsportsLeaderboardService, EsportsToastService, EsportsUserService, GlobalUtils, IPagination, IUser } from 'esports';
import { environment } from '../../../../../../../environments/environment';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { AppHtmlRoutes, AppHtmlRoutesLoginType } from '../../../../../../app-routing.model';

const API = environment.apiEndPoint;
const TOKEN = environment.currentToken;

@Component({
  selector: 'app-cardteam',
  templateUrl: './cardteam.component.html',
  styleUrls: ['./cardteam.component.scss'],
})
export class CardteamComponent implements OnInit {
  AppHtmlRoutesLoginType = AppHtmlRoutesLoginType;
  @Input() isManage: boolean = false;
  @Input() data;
  isBrowser: boolean;
  show = false;
  isRemove = false;
  user: IUser;
  followStatus: any = 'follow';
  currentUserId: any;
  showLoader: boolean = false;
  currentUser: IUser;
  matchcount: number;
  isUserLoggedIn: boolean = false;
  isAdmin: boolean = false;

  AppHtmlRoutes = AppHtmlRoutes;
  paginationDetails: any;
  pageSizeOptions = environment.pageSizeOptions;
  userSubscription: Subscription;
  showTeam = false;

  constructor(
    private leaderboardService :EsportsLeaderboardService,
    private router: Router,
    private toastService: EsportsToastService,
    private userService: EsportsUserService,
    public constantsService: EsportsConstantsService,
  ) {}
  isShowPopup() {
    this.show = !this.show;
  }
  showPopup(event) {
    this.show = !this.show;
    this.isRemove = event;
  }

  ngOnInit(): void {
    this.getCurrentUserDetails();
  }


  followUser() {
    if (this.user) {
      if (this.followStatus == 'follow') {
        this.leaderboardService.followUser(API, this.user?._id).subscribe((res: any) => {
          if(res){
          this.followStatus = 'unfollow';
          this.checkFollowStatus();
          this.toastService.showSuccess(res?.message)
          this.userService.refreshCurrentUser(API, TOKEN);
          }
        },
          (err) => {
            //  this.showLoader = false;
          }
        );
      } else if (this.followStatus == 'unfollow') {
        this.leaderboardService.unfollowUser(API, this.user?._id).subscribe(
          (res: any) => {
            this.followStatus = 'follow';
            this.checkFollowStatus();
            this.toastService.showSuccess(res?.message)
            this.userService.refreshCurrentUser(API, TOKEN);
          },
          (res: any) => {
            //  this.showLoader = false;
          }
        );
      }
    } else {

      // Navigate to the login page with extras
      //this.router.navigate(['/user/email-login']);
    }
  }

  checkFollowStatus() {
    this.leaderboardService.checkFollowStatus(API,  this.user?._id).subscribe(
      (res: any) => {
        if (res.data[0].status == 1) {
          this.followStatus = 'unfollow';
        } else if (res.data[0].status == 0) {
          this.followStatus = 'follow';
        }
      },
      (err) => {
        //this.showLoader = false;
      }
    );
  }
  getCurrentUserDetails() {
    this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.user = data;
      }
    });
  }



  ngOnDestroy() {
    this.userSubscription?.unsubscribe();
  }

  showOrHideTeam(){
    if(this.data?.participantType != 'team'){
      return;
    }
    this.showTeam = !this.showTeam;
  }

}





