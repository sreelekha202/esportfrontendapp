import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailSponsorComponent } from './detail-sponsor.component';

describe('DetailSponsorComponent', () => {
  let component: DetailSponsorComponent;
  let fixture: ComponentFixture<DetailSponsorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetailSponsorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailSponsorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
