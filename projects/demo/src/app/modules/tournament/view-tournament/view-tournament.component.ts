import { Title } from '@angular/platform-browser';
import { TranslateService } from '@ngx-translate/core';
import { MatDialog } from '@angular/material/dialog';
import {
  Component,
  OnInit,
  Inject,
  PLATFORM_ID,
  ViewChild,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import 'rxjs/add/operator/filter';
import {
  InfoPopupComponent,
  InfoPopupComponentData,
  InfoPopupComponentType,
} from '../../../shared/popups/info-popup/info-popup.component';
import { DOCUMENT } from '@angular/common';
import { isPlatformBrowser, Location } from '@angular/common';
import {
  EsportsTimezone,
  EsportsLanguageService,
  EsportsUtilsService,
  EsportsToastService,
  IUser,
  GlobalUtils,
  EsportsUserService,
  EsportsTournamentService,
  EsportsHomeService,
  EsportsRatingService,
  EsportsLeaderboardService,
  EsportsBracketService,
  EsportsGameService,
} from 'esports';
import { AppHtmlRoutes } from '../../../app-routing.model';
import { NgbCarousel } from '@ng-bootstrap/ng-bootstrap/carousel/carousel';
import { HttpParams } from '@angular/common/http';
import { CountdownFormatFn } from 'ngx-countdown';
import { environment } from '../../../../environments/environment';

const API = environment.apiEndPoint;
const TOKEN = environment.currentToken;

@Component({
  selector: 'app-view-tournament',
  templateUrl: './view-tournament.component.html',
  styleUrls: ['./view-tournament.component.scss'],
})
export class ViewTournamentComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;
  params: any = {};
  gameId: any;
  gameDetail: any;
  tournamentDetails;
  active = 1;
  isLoaded = false;
  hideMessage = true;
  domain;
  user: IUser;
  isBrowser: boolean;
  enableComment: boolean = false;
  text: string;
  participantId: string | null;
  isOrganizer: boolean = false;
  isParticipant: boolean = false;
  timezone;
  showregbut: boolean;
  show = false;
  showIssue = false;
  followStatus: any = 'follow';
  selectedRound: any;
  round: Array<any> = [];

  @ViewChild('carousel') carousel: NgbCarousel;

  tournamentsSlides = [];

  currentLang: string = 'english';
  currentSlide = null;
  startDate: any;
  // timer: any;
  // timem: string;
  remainingTime;
  remainingTimeForTournamentStart;
  showRegTime;
  showTournamentTime;
  remainigCheckInTime: number = 0;
  matchList: any;

  CountdownTimeUnits: Array<[string, number]> = [
    ['Y', 1000 * 60 * 60 * 24 * 365], // years
    ['M', 1000 * 60 * 60 * 24 * 30], // months
    ['D', 1000 * 60 * 60 * 24], // days
    ['H', 1000 * 60 * 60], // hours
    ['m', 1000 * 60], // minutes
    ['s', 1000], // seconds
  ];

  constructor(
    private tournamentService: EsportsTournamentService,
    private activatedRoute: ActivatedRoute,
    private toastService: EsportsToastService,
    private utilsService: EsportsUtilsService,
    private matDialog: MatDialog,
    private ratingService: EsportsRatingService,
    private translateService: TranslateService,
    private globalUtils: GlobalUtils,
    private languageService: EsportsLanguageService,
    private router: Router,
    private titleService: Title,
    @Inject(DOCUMENT) private document: Document,
    private userService: EsportsUserService,
    private esportsTimezone: EsportsTimezone,
    @Inject(PLATFORM_ID) private platformId,
    private location: Location,
    private homeService: EsportsHomeService,
    private leaderboardService: EsportsLeaderboardService,
    private bracketService: EsportsBracketService,
    private gameService: EsportsGameService
  ) {
    this.isBrowser = isPlatformBrowser(platformId);
  }

  ngOnInit(): void {
    if (GlobalUtils.isBrowser()) {
      this.gameId = localStorage.getItem('gameId');
    }
    this.remainingTime = 0;
    this.remainingTimeForTournamentStart = 0;
    this.showRegTime = false;
    this.showTournamentTime = false;
    this.timezone = this.esportsTimezone.getTimeZoneName();
    this.languageService.language.subscribe((lang) =>
      this.translateService.use(lang || 'en')
    );
    this.getCurrentUserDetails();
    const slug = this.activatedRoute.snapshot.params.id; // get slug from route
    if (slug) {
      // this.tournamentId = this.activatedRoute.snapshot.queryParams.id;
      // if (!this.tournamentId) {
      //   this.router.navigate(["/404"]);
      // } else {
      //   this.fetchTournamentDetails(this.tournamentId);
      // }
      this.fetchTournamentDetails(slug);
      this.domain =
        this.document.location.protocol +
        '//' +
        this.document.location.hostname;
      // this.activatedRoute.queryParams
      //   .filter((params) => params.id)
      //   .subscribe((params) => {
      //     this.tournamentId = params.id;
      //     this.fetchTournamentDetails(this.tournamentId);
      //   });
      // if (GlobalUtils.isBrowser()) {
      //   if (this.tournamentId) {
      //     this.deeplinkService.deeplink({
      //       objectType: "tournament",
      //       objectId: this.tournamentId,
      //     });
      //   }
      // }
      this.getTournamentsSlides();
      this.getAllTournaments();
    }
  }

  fetchMatches = async (round, i) => {
    this.isLoaded = false;
    this.matchList = [];
    try {
      //   const queryParam = `?tournamentId=${this.tournament?._id}&page=1&round=1`;
      // const response = await this.bracketService.fetchAllMatches(queryParam);
      const queryParam = `?query=${encodeURIComponent(
        JSON.stringify({
          tournamentId: this.tournamentDetails?._id,
          'currentMatch.round': round.id,
          matchStatus: { $ne: 'inactive' },
          bye: false,
        })
      )}&select=sets,totalSet,matchNo,winnerTeam,loserTeam,disQualifiedTeam,battleRoyalTeams`;
      const response = await this.bracketService.fetchAllMatches(queryParam);
      if (this.tournamentDetails?.bracketType == 'battle_royale') {
      } else {
        this.matchList =
          response.data && response.data.length > 0 ? response.data[0] : '';

        this.selectedRound = this.round[i];
        // const totalSet = _.reduce(response.data, function (accumulator, currentValue) {
        //   const max =
        //     currentValue.sets.length > currentValue.totalSet
        //       ? currentValue.sets.length
        //       : currentValue.totalSet;
        //   accumulator = max > accumulator ? max : accumulator;
        //   return accumulator;
        // }, 0);
        this.isLoaded = true;
        // this.round[i].match = response.data;
        // this.matchList = response.data;
        // if (response.data) {
        //   this.createSetHeader(totalSet);
        // } else {
        //   this.createSetHeader(0);
        // }
      }

      this.round[i].isLoaded = true;
      this.round[i].match = response.data;
      this.selectedRound = this.round[i];
    } catch (error) {
      this.isLoaded = true;
    }
  };

  fetchDistinctRound = async () => {
    try {
      const queryParam = `?query=${this.utilsService.encodeQuery({
        tournamentId: this.tournamentDetails?._id,
      })}`;

      const field = this.getFieldType(this.tournamentDetails?.bracketType);
      const response = await this.bracketService.fetchDistinctValue(
        queryParam,
        field
      );
      this.round = response.data.map((r) => {
        return {
          id: r,
          isCollapsed: false,
          isLoaded: false,
        };
      });
      this.selectedRound = this.round[0];
      this.fetchMatches(this.round[0], 0);
      this.isLoaded = true;
    } catch (error) {
      this.isLoaded = true;
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  getFieldType = (type) => {
    if (['single', 'double'].includes(type)) {
      return 'currentMatch.round';
    } else if (['round_robin', 'battle_royale'].includes(type)) {
      return 'currentMatch.stage';
    } else {
      return 'currentMatch.round';
    }
  };

  followUser() {
    if (this.user) {
      if (this.followStatus == 'follow') {
        this.leaderboardService.followUser(API, this.user?._id).subscribe(
          (res: any) => {
            this.followStatus = 'unfollow';
            this.checkFollowStatus();
            this.toastService.showSuccess(res?.message);
            this.userService.refreshCurrentUser(API, TOKEN);
          },
          (err) => {
            //  this.showLoader = false;
          }
        );
      } else if (this.followStatus == 'unfollow') {
        this.leaderboardService.unfollowUser(API, this.user?._id).subscribe(
          (res: any) => {
            this.followStatus = 'follow';
            this.checkFollowStatus();
            this.toastService.showSuccess(res?.message);
            this.userService.refreshCurrentUser(API, TOKEN);
          },
          (res: any) => {
            //  this.showLoader = false;
          }
        );
      }
    } else {
      // Navigate to the login page with extras
      //this.router.navigate(['/user/email-login']);
    }
  }

  checkFollowStatus() {
    this.leaderboardService.checkFollowStatus(API, this.user?._id).subscribe(
      (res: any) => {
        if (res.data[0].status == 1) {
          this.followStatus = 'unfollow';
        } else if (res.data[0].status == 0) {
          this.followStatus = 'follow';
        }
      },
      (err) => {
        //this.showLoader = false;
      }
    );
  }

  shareTournament = async () => {
    try {
      const data: InfoPopupComponentData = {
        title: this.translateService.instant('ARTICLE_POST.SHARE_TOURNAMENT'),
        text: ``,
        type: InfoPopupComponentType.socialSharing,
        cancelBtnText: 'Close',
      };

      const dialogRef = this.matDialog.open(InfoPopupComponent, {
        data,
      });

      await dialogRef.afterClosed().toPromise();
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };
  getTournamentsSlides() {
    // MOCK_DATA
    this.tournamentsSlides = [
      {
        bannerFileUrl:
          'https://i115.fastpic.ru/big/2021/0706/e5/c625bd1ac866cfba9340ebc800b241e5.jpg',
        title: 'Fortnite League 2021 - Alpha phase',
        date: 'Tomorrow at 16:50 GMT+8 • Fortnite',
        points: 30,
      },
      {
        bannerFileUrl:
          'https://i115.fastpic.ru/big/2021/0706/15/f534b3311fd642a13dc57103b92b5a15.jpg',
        title: 'Overwatch individual tournament august 2021',
        date: 'Tomorrow at 18:20 GMT+8 • Fortnite',
        points: 25,
      },
    ];

    this.homeService._getBanner().subscribe(
      (res) => {
        // this.tournamentsSlides = res.data;
        this.currentSlide = this.tournamentsSlides[0];
      },
      (err) => {}
    );
  }

  fetchTournamentDetails = async (slug) => {
    if (GlobalUtils.isBrowser()) {
      try {
        this.isLoaded = false;
        let paramsd = new HttpParams();
        paramsd = paramsd.append(
          'query',
          JSON.stringify({ slug: slug, tournamentStatus: 'publish' })
        );
        const tournament = await this.tournamentService
          .getTournamentDetails(paramsd)
          .toPromise();
        this.tournamentDetails = tournament.data.length
          ? tournament.data[0]
          : null;
        // this.tournamentDetails = tournament.data;
        localStorage.setItem(
          'tournamentDetails',
          JSON.stringify(this.tournamentDetails)
        );
        this.tournamentService
          .fetchParticipantRegistrationStatus1(this.tournamentDetails._id)
          .subscribe((data: any) => {
            if (data.data.participantStatus == null) {
              this.showregbut = true;
            } else {
              this.showregbut = false;
            }
          });
        if (this.tournamentDetails?.organizerDetail?._id == this.user?._id) {
          this.isOrganizer = true;
        }

        if (!this.tournamentDetails) {
          this.hideMessage = false;
          this.toastService.showError(
            this.translateService.instant('VIEW_TOURNAMENT.ERROR')
          );
          this.location.back();
        } else {
          if (
            (this.tournamentDetails && this.tournamentDetails.banner) ||
            this.tournamentDetails.gameDetail.logo
          ) {
            this.fetchDistinctRound();
            this.titleService.setTitle(this.tournamentDetails.name);
            this.globalUtils.setMetaTags([
              {
                property: 'twitter:image',
                content:
                  this.tournamentDetails.banner ||
                  this.tournamentDetails.gameDetail.logo,
              },
              {
                property: 'og:image',
                content:
                  this.tournamentDetails.banner ||
                  this.tournamentDetails.gameDetail.logo,
              },
              {
                property: 'og:image:secure_url',
                content:
                  this.tournamentDetails.banner ||
                  this.tournamentDetails.gameDetail.logo,
              },
              {
                property: 'og:image:url',
                content:
                  this.tournamentDetails.banner ||
                  this.tournamentDetails.gameDetail.logo,
              },
              {
                property: 'og:image:width',
                content: '1200',
              },
              {
                property: 'og:image:height',
                content: '630',
              },
              {
                name: 'description',
                content: this.tournamentDetails.description,
              },
              {
                property: 'og:description',
                content: this.tournamentDetails.description,
              },
              {
                property: 'twitter:description',
                content: this.tournamentDetails.description,
              },
              {
                name: 'title',
                content: this.tournamentDetails.name,
              },
              {
                name: 'title',
                content: this.tournamentDetails.name,
              },
              {
                property: 'og:title',
                content: this.tournamentDetails.name,
              },
              {
                property: 'twitter:title',
                content: this.tournamentDetails.name,
              },
              {
                property: 'og:url',
                content: this.domain + this.router.url,
              },
            ]);
          }

          const data = await this.fetchParticipantRegisterationStatus(
            this.tournamentDetails?._id
          );
          if (data.type == 'already-registered') {
            this.isParticipant = true;
          }
        }
        this.isLoaded = true;
      } catch (error) {
        this.isLoaded = true;
        // this.toastService.showError(error?.error?.message || error?.message);
        // this.router.navigate(['/404']);
      }
    }

    this.gameService.createMatchMakingSubject.next({
      selectedTournament: this.tournamentDetails,
    });
  };

  joint() {
    if (GlobalUtils.isBrowser()) {
      localStorage.setItem(
        'tournamentDetails',
        JSON.stringify(this.tournamentDetails)
      );
      this.router.navigateByUrl('/join-tournament');
    }
  }

  getCheckInDuration() {
    if (
      this.tournamentDetails?.checkInStartDate &&
      this.tournamentDetails?.checkInEndDate
    ) {
      const startDate = new Date(this.tournamentDetails.checkInStartDate);
      const endDate = new Date(this.tournamentDetails.checkInEndDate);
      if (
        this.tournamentDetails?.checkInStartDate ===
        this.tournamentDetails?.checkInEndDate
      ) {
        return `${startDate.getDate()} ${startDate.toLocaleString('default', {
          month: 'long',
        })} ${startDate.getFullYear()}`;
      } else if (startDate.getFullYear() < endDate.getFullYear()) {
        return `${startDate.getDate()} ${startDate.toLocaleString('default', {
          month: 'long',
        })} ${startDate.getFullYear()} to ${endDate.getDate()} ${endDate.toLocaleString(
          'default',
          { month: 'long' }
        )} ${endDate.getFullYear()}`;
      } else if (startDate.getMonth() < endDate.getMonth()) {
        return `${startDate.getDate()} ${startDate.toLocaleString('default', {
          month: 'long',
        })} - ${endDate.getDate()} ${endDate.toLocaleString('default', {
          month: 'long',
        })} ${endDate.getFullYear()}`;
      } else if (startDate.getDate() < endDate.getDate()) {
        return `${startDate.getDate()} - ${endDate.getDate()} ${endDate.toLocaleString(
          'default',
          { month: 'long' }
        )} ${endDate.getFullYear()}`;
      } else {
        return 'N/A';
      }
    }
  }

  isNumber(val): boolean {
    const str: string = val.toString();
    return str.startsWith('+');
  }

  getStartTime() {
    return this.utilsService.convertStringIntoTime(
      this.tournamentDetails?.startDate,
      this.tournamentDetails?.startTime
    );
  }

  buttonResponse(data) {
    if (data) {
      this.fetchTournamentDetails(this.tournamentDetails?.slug);
    }
  }

  enableRating(data) {
    if (data?.isAuthorized) {
      // add field for ratings
    }
  }

  rateUs = async () => {
    try {
      const query = `?query=${encodeURIComponent(
        JSON.stringify({
          posterId: this.tournamentDetails._id,
          type: 'Tournament',
        })
      )}`;
      const rating = await this.ratingService.getRating(query);
      const rate = rating.data || {
        raterId: this.user?._id,
        posterId: this.tournamentDetails._id,
        value: 3,
        type: 'Tournament',
      };
      const blockUserData: InfoPopupComponentData = {
        title: 'Rate US',
        text: `Your feedback is valuable for us.`,
        type: InfoPopupComponentType.rating,
        btnText: 'Confirm',
        rate: rate.value,
      };

      const dialogRef = this.matDialog.open(InfoPopupComponent, {
        data: blockUserData,
      });

      const confirmed = await dialogRef.afterClosed().toPromise();

      if (confirmed) {
        rate.value = confirmed;
        const addOrUpdateRating = rate._id
          ? await this.ratingService.updateRating(rate._id, rate)
          : await this.ratingService.addRating(rate);
        this.toastService.showSuccess(addOrUpdateRating?.message);
      }
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  getCurrentUserDetails() {
    this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.user = data;
      }
    });
  }

  fetchParticipantRegisterationStatus = async (id: string) => {
    try {
      const { data } =
        await this.tournamentService.fetchParticipantRegistrationStatus(
          API,
          id
        );
      this.enableComment =
        this.tournamentDetails?.organizerDetail?._id == this.user._id ||
        !!data?.participantId;
      this.participantId = data?.participantId;
      this.remainingTime = data?.remainingTimeForRegStart / 1000;
      this.remainingTimeForTournamentStart =
        data?.remainingTimeForTournamentStart / 1000 || 0;
      this.showRegTime = this.remainingTime > 0 ? true : false;
      this.showTournamentTime =
        this.remainingTimeForTournamentStart > 0 ? true : false;
      if (this.remainingTime < 0) {
        this.showRegTime = false;
      }
      this.remainigCheckInTime = data?.remainingCheckInTime / 1000 || 0;
      if (!this.enableComment) {
        if (
          !['tournament-finished', 'tournament-started', 'no-action'].includes(
            data.type
          )
        ) {
          this.text = 'DISCUSSION.PARTICIPANT_NA';
        }
      }
      return data;
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  joinindivialtour() {
    if (GlobalUtils.isBrowser()) {
      this.isLoaded = false;
      this.userService.currentUser.subscribe((data) => {
        localStorage.setItem('userDetails', JSON.stringify(data));
        localStorage.setItem(
          'tournamentDetails',
          JSON.stringify(this.tournamentDetails)
        );
        this.router.navigateByUrl(
          `tournament/${this.tournamentDetails?.slug}/join`
        );
        // this.router.navigateByUrl('/join-tournament')
      });

      //   let data1 = Math.floor(Math.random() * 100)
      //   let id = 'SSS' + data1;
      //   this.profileService.ckeckuserid(id,this.tournamentDetails._id).subscribe((data3:any)=>{
      //     if(data3.data.invalidId == false && data3.data.isExist == false){
      //       let datatosend = {
      //         "teamId": "",
      //         "logo": "https://stc-img.s3.me-south-1.amazonaws.com/dev/profile/1624437521923.png",
      //         "teamName": "",
      //         "name": data.fullName,
      //         "phoneNumber": data.phoneNumber,
      //         "email": data.email,
      //         "inGamerUserId": id,
      //         "teamMembers": [],
      //         "substituteMembers": [],
      //         "participantType": this.tournamentDetails.participantType,
      //         "tournamentId": this.tournamentDetails._id
      //     }
      //     this.profileService.jointournament(datatosend).subscribe((data:any)=>{
      //       this.isLoaded = true;
      //       this.toastService.showSuccess(data.message)
      //       //this.router.navigateByUrl('/play')
      //     }, (err) => {
      //       this.isLoaded = true;
      //       this.toastService.showError(err.error.message) })
      //     }
      //   })
      // });
    }
  }

  // callTime(date) {
  //   this.startDate = date;
  //   this.timer = setInterval(this.startTime, 1000);
  // }

  // startTime = () => {
  //   let startDate = new Date(this.startDate).getTime()
  //   let curentdate = new Date().getTime()
  //   let newDate: Date = new Date(curentdate - startDate);
  //   this.timem = (`${newDate.getHours()} H : ${newDate.getMinutes()} M : ${newDate.getSeconds()} S`)
  // }

  // ngOnDestroy(): void {
  //   clearInterval(this.timer);
  // }

  formatDate?: CountdownFormatFn = ({ date, formatStr }) => {
    let duration = Number(date || 0);
    return this.CountdownTimeUnits.reduce((current, [name, unit]) => {
      if (current.indexOf(name) !== -1) {
        const v = Math.floor(duration / unit);
        duration -= v * unit;
        return current.replace(new RegExp(`${name}+`, 'g'), (match: string) => {
          return v.toString().padStart(match.length, '0');
        });
      }
      return current;
    }, formatStr);
  };

  onTournamnetTimerFinished(e: Event) {
    if (e['action'] == 'done') {
      Promise.resolve(() => {
        this.showTournamentTime = false;
      });
      this.fetchParticipantRegisterationStatus(this.tournamentDetails?._id);
      // this.ngOnChanges();
    }
  }
  onTimerFinished(e: Event) {
    if (e['action'] == 'done') {
      Promise.resolve(() => {
        this.showRegTime = false;
      });
      this.fetchParticipantRegisterationStatus(this.tournamentDetails?._id);
      // this.ngOnChanges();
    }
  }
  showPopup($event) {
    this.show = true;
  }
  hidePopup($event) {
    this.show = false;
  }
  showPopupIssue($event) {
    this.showIssue = true;
  }
  // genrateReport() {
  //   if (this.data?.tournament && this.data?.matchId) {
  //     localStorage.setItem('t_report', JSON.stringify(this.data));
  //     this.router.navigate([`/report-issue`],
  //      {queryParams: { matchId:this.data?.matchId,tournamentId:this.data?.tournament }
  //     });
  //   }
  // }
  hidePopupIssue($event) {
    this.showIssue = false;
  }
  closePopup(event) {}
  getAllTournaments() {
    this.params['game'] = this.gameId;
    this.homeService.getTournament(this.params).subscribe(
      (res: any) => {
        this.gameDetail = res?.data?.docs[0].gameDetail;
      },
      (err: any) => {
        // this.showLoader = false;
      }
    );
  }
  // get device(): any {
  //   return this.deviceService.getDeviceInfo();
  // }

  // isMobileRes() {
  //   return this.deviceService.isMobile();
  // }

  // get isTablet(): boolean {
  //   return this.deviceService.isTablet();
  // }

  // get isDesktop(): boolean {
  //   return this.deviceService.isDesktop();
  // }
}
