import { Component, OnInit, Input } from '@angular/core';
import { toggleOpacity, VisibilityState } from '../../../../animations';
import { Subscription } from 'rxjs';
import { EsportsUserService, EsportsCommentService } from 'esports';
@Component({
  selector: 'app-discussion-tournament',
  templateUrl: './discussion.component.html',
  styleUrls: ['./discussion.component.scss'],
  animations: [toggleOpacity],
})
export class DiscussionComponent implements OnInit {
  VisibilityState = VisibilityState;
  collapseOrExpand: VisibilityState = VisibilityState.Visible;
  @Input() placeholder: string = '';
  @Input() isReplying: boolean = false;
  comments = [];
  isRemove = false;
  show = false;

  currentUser: any;
  userSubscription: Subscription;

  tournamentDetails: any;
  show1 = false;
  show2 = false;
  show3 = false;
  constructor(
    private userService: EsportsUserService,
    private commentService: EsportsCommentService
  ) {}

  ngOnInit(): void {
    // MOCK COMMENTS DATA

    this.tournamentDetails = localStorage.getItem('tournamentDetails');
    this.tournamentDetails = JSON.parse(this.tournamentDetails);
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.currentUser = data;
      }
    });

    this.getComment();
  }
  showReply1() {
    this.show1 = !this.show1;
  }
  showReply2() {
    this.show2 = !this.show2;
  }
  showReply3() {
    this.show3 = !this.show3;
  }
  getComment() {
    this.commentService
      .getAllComment1('tournament', this.tournamentDetails._id, 1)
      .subscribe((data) => {
        this.comments = data.data;
      });
  }

  onSaveCommentt(msg) {
    let data = {
      comment: msg,
      objectId: this.tournamentDetails._id,
      objectType: 'tournament',
    };
    this.commentService.saveComment(data).subscribe((res) => {
      this.getComment();
    });
  }
  isShowPopup() {
    this.show = !this.show;
  }
  showPopup(event) {
    this.show = !this.show;
    this.isRemove = event;
  }
}
