import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import {
  EsportsToastService,
  UserReportsService,
  S3UploadService,
  EsportsUserService,
} from 'esports';
import { TranslateService } from '@ngx-translate/core';
import { environment } from '../../../../../environments/environment';
import { Location } from '@angular/common';
import { Subscription } from 'rxjs';
import { FormGroup } from '@angular/forms';
const API = environment.apiEndPoint;

@Component({
  selector: 'app-report-issue',
  templateUrl: './report-issue.component.html',
  styleUrls: ['./report-issue.component.scss'],
})
export class ReportIssueComponent implements OnInit {
  @Input() matchId;
  @Input() tournamentId;

  @Output() hidePopupIssue = new EventEmitter();

  activeButton = '';
  type;
  form: FormGroup;
  addForm: FormGroup;
  base64textString;
  briefDescription;
  descrip1;
  user;
  isLoaded = false;
  userSubscription: Subscription;
  showLoader: boolean;
  id;
  imgurl = '';
  tReport: any;
  tournament: any;
  selectedFile: any;

  dimension = { width: 1000, height: 1000 };
  size = '1000  1000  5';

  constructor(
    public toastService: EsportsToastService,
    private translateService: TranslateService,
    private eSportsToastService: EsportsToastService,
    private location: Location,
    private userService: EsportsUserService,
    private s3UploadService: S3UploadService,
    private userReportsService: UserReportsService
  ) {}

  ngOnInit(): void {
    this.getCurrentUserDetails();
    this.tReport = JSON.parse(localStorage.getItem('t_report'));
    //   this.activeRoute.queryParamMap.subscribe(params => {
    //   this.matchId=params.get('matchId')
    //   this.tournamentId=params.get('tournamentId')
    //  });
  }

  getCurrentUserDetails() {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.isLoaded = true;
        this.user = data;
      }
    });
  }
  goBack() {
    this.location.back();
  }

  selectType(reportType) {
    this.type = reportType;
  }

  eventChange = async (event) => {
    try {
      // this.isProcessing = true;

      const upload: any = await this.s3UploadService.validateAndUploadImage(
        event,
        this.dimension,
        this.size,
        this.type
      );
      this.imgurl = upload.data[0].Location;
      this.selectedFile = event.target.files[0];
      var reader = new FileReader();
      reader.onload = this.handleReaderLoaded.bind(this);
      reader.readAsBinaryString(this.selectedFile);
    } catch (error) {
      // this.isProcessing = false;
      const errorMessage = error?.error?.message || error?.message;
      this.toastService.showError(errorMessage);
    }
  };
  handleReaderLoaded(readerEvt) {
    var binaryString = readerEvt.target.result;
    this.base64textString = 'data:image/png;base64,' + btoa(binaryString);
    // this.imgurltosave.emit(this.base64textString)
    this.datatoimg(this.base64textString);
  }

  datatoimg(data) {
    this.imgurl = data;
  }

  reportsubmit(descrip) {
    this.briefDescription = descrip;
    if (
      this.briefDescription &&
      this.type &&
      this.matchId &&
      this.user &&
      this.tournamentId
    ) {
      this.isLoaded = false;
      const data = {
        createrId: this.user?._id, // userId
        tournamentId: this.tournamentId,
        matchId: this.matchId?._id,
        reportType: this.type,
        reportText: this.briefDescription,
        reportImage: this.imgurl,
      };
      this.userReportsService.createReport(API, data).subscribe(
        (res: any) => {
          if (res && res?.data) {
            this.eSportsToastService.showSuccess(res?.message);
            this.location.back();
          }
          this.isLoaded = true;
        },
        (err) => {
          this.isLoaded = true;
          this.eSportsToastService.showError(err?.error?.message);
        }
      );
    } else if (!this.type) {
      this.eSportsToastService.showError(
        this.translateService.instant('VIEW_TOURNAMENT.USERREPORT_REQUIRED_TYPE')
      );
    } else {
      this.eSportsToastService.showError(
        this.translateService.instant('VIEW_TOURNAMENT.USERREPORT_REQUIRED_COMMENT')
      );
    }
  }
  // sendIssue() {

  //   this.hidePopupIssue.emit();

  //   this.toastService.showSuccess(this.translateService.instant('VIEW_TOURNAMENT.POPUP_ISSUE.SHOWSUCCESS'))
  // }
  dataTeamDropdownItem = [
    {
      id: 0,
      text: 'My opponent has an incorrect game account',
      type: 'incorrectOpponent',
    },
    {
      id: 1,
      text: 'My opponent was cheating',
      type: 'cheating',
    },
    {
      id: 2,
      text: 'Other grievances',
      type: 'grievances',
    },
  ];

  clickChange(reportType) {
    // this.activeButton = value;
    this.type = reportType;
  }
}
