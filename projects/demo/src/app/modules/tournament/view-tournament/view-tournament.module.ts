import { NgModule } from "@angular/core";
import { FaIconLibrary } from "@fortawesome/angular-fontawesome";
import { faTrash, faShareAlt } from "@fortawesome/free-solid-svg-icons";
import { ViewTournamenttRoutingModule } from "./view-tournament-routing.module";
import { ViewTournamentComponent } from "./view-tournament.component";
import { JoinParticipantComponent } from "./join-participant/join-participant.component";
import { OverviewComponent } from "./overview/overview.component";
import { ParticipantsComponent } from "./participants/participants.component";
import { BracketsComponent } from "./brackets/brackets.component";
import { JoinButtonComponent } from "./join-button/join-button.component";
import { PaymentComponent } from "./join-participant/payment/payment.component";
import { InfoComponent } from "./info/info.component";
import { FormComponentModule } from "../../../shared/components/form-component/form-component.module";
import { HeaderBottomModule } from "../../../shared/components/header-bottom/header-bottom.module";
import { HeaderDynastyModule } from "../../../shared/components/header-dynasty/header-dynasty.module";
import { PaymentModule } from "../../payment/payment.module";
import { CountdownModule } from "ngx-countdown";
import { ClipsViewComponent } from './clips-view/clips-view.component';
import { RulesViewComponent } from './rules-view/rules-view.component';
import { MatchesComponent } from './matches/matches.component';
import { CardteamComponent } from './participants/compponent/cardteam/cardteam.component';
import { DiscussionComponent } from './discussion/discussion.component';
import { DiscussionCommentsComponent } from './discussion/discussion-comments/discussion-comments/discussion-comments.component';
import { SearchFormComponent } from './matches/components/search-form/search-form.component';
import { DetailSponsorComponent } from './detail-sponsor/detail-sponsor.component';
import { FaqComponent } from "./faq/faq.component";
import { ReportIssueComponent } from "./report-issue/report-issue.component";
import { RouterBackModule } from "../../../shared/directives/router-back.module";
import { SharedModule } from "../../../shared/modules/shared.module";
import { PrizePoollModule } from "../pipe/prize-pooll.module";
import { CdkAccordionModule } from '@angular/cdk/accordion';
import { FooterDynastyModule } from "../../../shared/components/footer-dynasty/footer-dynasty.module";
import { MatIconModule } from '@angular/material/icon';
import { EsportsLoaderModule, EsportsCustomPaginationModule, WYSIWYGEditorModule } from 'esports';
import { FooterMenuMobileModule } from "../../../shared/components/footer-menu-mobile/footer-menu-mobile.module";
import { BracketTreeModule } from '../../../shared/components/elimination/bracket-tree/bracket-tree.module';
@NgModule({
  declarations: [
    ViewTournamentComponent,
    JoinParticipantComponent,
    OverviewComponent,
    ParticipantsComponent,
    BracketsComponent,
    JoinButtonComponent,
    InfoComponent,
    PaymentComponent,
    ClipsViewComponent,
    RulesViewComponent,
    CardteamComponent,
    DiscussionComponent,
    DiscussionCommentsComponent,
    MatchesComponent,
    SearchFormComponent,
    DetailSponsorComponent,
    FaqComponent,
    ReportIssueComponent,
  ],
  imports: [
    ViewTournamenttRoutingModule,
    SharedModule,
    FormComponentModule,
    PrizePoollModule,
    PaymentModule,
    CountdownModule,
    RouterBackModule,
    HeaderBottomModule,
    CdkAccordionModule,
    FooterDynastyModule,
    MatIconModule,
    FooterMenuMobileModule,
    EsportsCustomPaginationModule,
    EsportsLoaderModule.setColor('#1d252d'),
    BracketTreeModule,
    HeaderDynastyModule
  ],

  providers: [],
})
export class ViewTournamentModule {
  constructor(private library: FaIconLibrary) {
    library.addIcons(faTrash, faShareAlt);
  }

}
