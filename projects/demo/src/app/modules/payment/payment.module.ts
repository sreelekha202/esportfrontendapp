import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { SharedModule } from "../../shared/modules/shared.module";
import { PaymentComponent } from "./payment.component";
import { PaypalComponent } from "./paypal/paypal.component";

@NgModule({
  declarations: [PaymentComponent, PaypalComponent],
  imports: [CommonModule, SharedModule],
  exports: [PaymentComponent],
})
export class PaymentModule {}
