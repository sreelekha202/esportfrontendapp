import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-input-search',
  templateUrl: './input-search.component.html',
  styleUrls: ['./input-search.component.scss'],
})
export class InputSearchComponent implements OnInit {
  @Input() placeholder: string = '';
 

  @Input() heading: string = "";
  @Input() listTitle: string = "";

  @Output() onTextChange = new EventEmitter<string>();

  players = [];
  text: string = '';

  constructor() { }
  ngOnInit(): void { 
      // MOCK PLAYERS DATA

      // this.players = [
      //   {
      //     image:
      //       'https://www.advertisingweek360.com/wp-content/uploads/2018/10/169girlgamers-1170x600.jpg',
      //     nickname: 'killmonger',
      //     isSelected: false,
      //   },
      //   {
      //     image: 'https://cdn6.dissolve.com/p/D538_291_180/D538_291_180_1200.jpg',
      //     nickname: 'xxinfinity01',
      //     isSelected: true,
      //   },
      //   {
      //     image:
      //       'https://media.suara.com/pictures/970x544/2020/05/10/60545-main-game.jpg',
      //     nickname: 'supergirl004',
      //     isSelected: false,
      //   },
      //   {
      //     image:
      //       'https://upload.wikimedia.org/wikipedia/commons/6/6a/GORDOx-BGS-2018.jpg',
      //     nickname: 'tinythor',
      //     isSelected: true,
      //   },
      // ];
      this.text="";
  }

  addPlayer(index: number): void {
    this.players.forEach((_, i) => {
      if (index === i) {
        this.players[i].isSelected = !this.players[i].isSelected;
      }
    });
  }
  filteredPlayer() {
    return this.players.filter((obj) => obj.nickname.includes(this.text.toLowerCase().trim()));
  }
  onTextChang = () => this.onTextChange.emit(this.text)
}
