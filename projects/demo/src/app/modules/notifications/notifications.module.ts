import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { environment } from '../../../environments/environment';
import { I18nModule, PipeModule } from 'esports';
import { NotificationsRoutingModule } from './notifications-routing.module';
import { NotificationsComponent } from './notifications/notifications.component';
import { MaterialModule } from '../../shared/modules/material.module';

@NgModule({
  declarations: [NotificationsComponent],
  imports: [
    CommonModule,
    NotificationsRoutingModule,
    MaterialModule,
    I18nModule.forRoot(environment),
    PipeModule,
  ],
})
export class NotificationsModule {}
