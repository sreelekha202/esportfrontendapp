import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'prizePool',
})
export class PrizePoolPipe implements PipeTransform {
  transform(value) {
    return typeof value == 'number'
      ? `TOURNAMENT.PRIZE_${value + 1}`
      : 'TOURNAMENT.N_A_1';
  }
}
