import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'bracketPipe',
})
export class BracketPipe implements PipeTransform {
  transform(value) {
    const bracketTypes = {
      single: 'assets/icons/matchmaking/format/Single-elimination.svg',
      double: 'assets/icons/matchmaking/format/Double-elimination.svg',
      round_robin: 'assets/icons/matchmaking/format/Round-robin.svg',
      battle_royale: 'assets/icons/matchmaking/format/Battle-royale.svg',
      swiss_safeis: 'assets/icons/matchmaking/format/Round-robin.svg',
      ladder: 'assets/icons/matchmaking/format/Ladder.svg',
    };

    return (
      bracketTypes[value] || 'assets/icons/matchmaking/format/Round-robin.svg'
    );
  }
}
