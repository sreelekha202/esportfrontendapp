import { Component, OnInit, Input } from '@angular/core';
@Component({
  selector: 'app-type-card',
  templateUrl: './type-card.component.html',
  styleUrls: ['./type-card.component.scss'],
})
export class TypeCardComponent implements OnInit {
  @Input() image: string = '';
  @Input() title: string = '';
  @Input() text: string = '';

  constructor() {}

  ngOnInit(): void {}
}
