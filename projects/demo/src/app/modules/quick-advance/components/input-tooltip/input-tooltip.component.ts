import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
@Component({
  selector: 'app-input-tooltip',
  templateUrl: './input-tooltip.component.html',
  styleUrls: ['./input-tooltip.component.scss'],
})
export class InputTooltipComponent implements OnInit {
  @Input() buttonText: string = 'OK';
  @Input() title: string = 'Press enter';
  @Input() showIcon: boolean = true;
  @Input() callBack: () => void;

  @Output() onEnter = new EventEmitter();

  constructor() {}

  ngOnInit(): void {}

  ok() {
    this.onEnter.emit(true);
  }
}
