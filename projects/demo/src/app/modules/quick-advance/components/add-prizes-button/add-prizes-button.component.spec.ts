import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddPrizesButtonComponent } from './add-prizes-button.component';

describe('AddPrizesButtonComponent', () => {
  let component: AddPrizesButtonComponent;
  let fixture: ComponentFixture<AddPrizesButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AddPrizesButtonComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddPrizesButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
