import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-input-date',
  templateUrl: './input-date.component.html',
  styleUrls: ['./input-date.component.scss'],
})
export class InputDateComponent implements OnInit {
  @Input() title: string = '';
  @Input() placeholder: string = '';
  @Input() value: string | number = '';
  @Input() isBasic: boolean = false;
  @Input() isDisabled: boolean = false;
  @Input() customFormControlName: string;
  @Input() customFormGroup: FormGroup;
  @Input() minDate: Date | null;

  @Output() submit = new EventEmitter();

  stepSecond: number = 0;

  constructor() {}

  ngOnInit(): void {}

  // to handle Enter event
  onSubmit() {
      this.submit.next(true);
  }
}
