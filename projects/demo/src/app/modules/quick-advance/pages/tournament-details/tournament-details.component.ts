import { Component, EventEmitter, OnInit, Output,ViewChild,ElementRef, ViewEncapsulation } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {
  AbstractControl,
  ControlContainer,
  FormControl,
  FormGroupDirective,
  ValidatorFn,
  Validators,
  FormBuilder,
  FormArray,
  FormGroup,
} from '@angular/forms';
import { Subscription } from 'rxjs-compat';
import { AppHtmlRoutes } from '../../../../app-routing.model';
import { ConfirmComponent } from '../../popups/confirm/confirm.component';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { environment } from '../../../../../environments/environment';
import {
  EsportsConstantsService,
  EsportsToastService,
  EsportsTournamentService,
  EsportsUserService,
  GlobalUtils,
  S3UploadService,
  EsportsEditPopupComponent
} from 'esports';
const API = environment.apiEndPoint;
import { TranslateService } from '@ngx-translate/core';
@Component({
  selector: 'app-tournament-details',
  templateUrl: './tournament-details.component.html',
  styleUrls: [
    '../send-invite/send-invite.component.scss',
    './tournament-details.component.scss',
  ],
  viewProviders: [
    { provide: ControlContainer, useExisting: FormGroupDirective },
  ],
  encapsulation: ViewEncapsulation.None
})
export class TournamentDetailsComponent implements OnInit {
  regionList = [];
  showPopup = false;
  editInfoVo:any;
  Step7;
  structure;
  isBracketLoaded: boolean = false;
  userList = [];
  gameList = [];
  currentGame = '';
  currentPlatform = '';
  currentBracket = '';
  tournamentDetails;
  platformList = [];
  showAdvanceStage = false;
  showCustomRegistrationEndDate = false;
  showCustomTournamentEndDate = false;
  brackettypeList = [];
  isProcessing: boolean = false;
  isFormLoaded: boolean = false;
  screenshotRequired: boolean = false;
  showCountryFlag: boolean = false;
  minDate: Date | null;
  minDateSummary: Date | null;
  currentDate = new Date();
  currentUser: any;
  showLoader: boolean = false;
  ruleInfoForm: any = new FormGroup({});
  battleRoyaleForm: any = new FormGroup({});
  poolSettingPrizeForm: any = new FormGroup({});
  SponsorForm: any = new FormGroup({});
  @ViewChild('element1') toruEnd: ElementRef;
  @ViewChild('element2') custReg: ElementRef;
  @ViewChild('element3') advOpt: ElementRef;

  editorConfig = {};
  @Output() tournamentEvent = new EventEmitter();
  selectedGameDetails: any;

  AppHtmlRoutes = AppHtmlRoutes;
  summarySponsorVo: any = [];
  sponsorsVO: any = [];
  bannerSRCFile: any = null;
  // date: Date = new Date();
  // time: any = timeFormatAMPM(new Date())
  advSummaryRuleSet: any;
  advFbURLRuleSet: any;
  advVideoStreemRuleSet: any;
  advTwitchStreemRuleSet: any;
  advContactURLRuleSet: any;
  advGameid: any;
  advPlatformId: any;
  date: Date;
  time: any;
  custTime: any;
  tournamentDetails2: Subscription;
  platforms = [];
  createTournament: any;
  text: string;
  ckConfig: any;
  dataTournamentTypeValue = 0;
  payouts: any = [0];
  fileToUploadLogo: File | null = null;
  fileToUploadBanner: File | null = null;
  checkFileNameLogo: boolean = false;
  checkFileNameBanner: boolean = false;
  toggleInputFileLogo: boolean = false;
  toggleInputFileBanner: boolean = false;
  toggleChangePrizePayout: boolean = false;
  toggleChangeTournamentSettings: boolean = false;
  toggleChangeRulesAndInfo: boolean = false;
  toggleChangeSponsors: boolean = false;

  imgurl = '';
  selectCurrency: string = '';
  selectPay: string = '';
  selectContact: string = '';
  selectMatchFormat: string = '';
  selectMatchFormatSet:string = '';
  selectRound: string = '';
  selectPlaceMatch: string = 'Disabled';
  selectPlaceMatchKey: string = 'TOURNAMENT.TIE_DISABLE';
  selectStageBracket: string = '';
  selectTieBreakerKey: string = 'TOURNAMENT.TIE_DISABLE';
  selectTieBreaker: string = 'Disabled';
  selectRegion: any;
  selectStages: string = '';
  selectStagesId:any;
  selectStageMatchFormats: string = '';
  selectStageMatchFormatsId:any;
  numberSubstitutes: number = 1;
  numberWinningParticipants: number = 1;
  numberNoOfParticipants: number = 2;
  numberOfStageAutoGen:number = 1;
  pointsUntilPlace:number = 2;
  inputPointsPlaceVo:any = [];
  numberNoOfKills:number = 0;
  numberOfMatch:number = 0;

  participants: boolean = true;
  myselfOnly: boolean = false;
  noLimit: boolean = true;
  addLimit: boolean = false;
  privateVisiblity: boolean = true;
  publicVisiblity: boolean = false;
  noCheckIn: boolean = true;
  addCheckIn: boolean = false;
  noSubstitute: boolean = true;
  addSubstitute: boolean = false;
  offlineVenues = [
    {
      name: '',
      region: '',
      id:null
    },
  ];

  waggerPool:any;
  games1:any = [];
  dataTournamentType = [
    {
      id: 0,
      textTitle: 'Online',
      textDes: 'Your tournament will be held as an online event',
    },
    {
      id: 1,
      textTitle: 'Offline',
      textDes: 'Your tournament will be held as an offline event',
    },
    {
      id: 2,
      textTitle: 'Hybrid',
      textDes:
        'Your tournament will be held as a hybrid (online and offline) event',
    },
  ];
  totalSponsor = [1];
  currencies = ['USD', 'VND', 'DKK'];
  payoutsMethod = [
    'Select payout method 1',
    'Select payout method 2',
    'Select payout method 3',
  ];
  contactOptions = [
    'Select contact option 1 ',
    'Select contact option 2 ',
    'Select contact option 3',
  ];
  placeMatchs = [{key:'TOURNAMENT.TIE_ENABLE',value:'Enabled'}, {key:'Disabled',value:'TOURNAMENT.TIE_DISABLE'}];
  matchFormat = [
    { _id: 1, name: 'TOURNAMENT.BEST_OF_1' },
    { _id: 3, name: 'TOURNAMENT.BEST_OF_3' },
    { _id: 5, name: 'TOURNAMENT.BEST_OF_5' },
    { _id: 7, name: 'TOURNAMENT.BEST_OF_7' },
  ];
  stageMatchFormat = [
    { _id: 'quarterFinal', name: 'TOURNAMENT.QUARTER_FINAL' },
    { _id: 'semiFinal', name: 'TOURNAMENT.SEMI_FINAL' },
    { _id: 'final', name: 'TOURNAMENT.FINAL' },
  ];

  participantType = [
    { _id: 'individual', name: 'Solo' },
    { _id: 'team', name: 'Team' },
  ];

  teamFormat = [
    {
      id: '2',
      name: '2v',
      teamSize: 2,
    },
    {
      id: '4',
      name: '4v',
      teamSize: 4,
    },
    {
      id: 'n',
      name: 'x vs x',
      teamSize: 5,
    },
  ];

  rrFormat = [
    { _id: 1, name: 'Once' },
    { _id: 2, name: 'Twice' },
  ];
  bracketTypes = [
    'single',
    'double'
  ];

  stageMatchFormats = ['Match Format 1', 'Match Format 2', 'Match Format 3'];

  constructor(
    public s3Service: S3UploadService,
    private tournament: FormGroupDirective,
    private fb: FormBuilder,
    private eSportsToastService: EsportsToastService,
    private eSportsTournamentService: EsportsTournamentService,
    private matDialog: MatDialog,
    private router: Router,
    private userService: EsportsUserService,
    private translateService: TranslateService,
    private http: HttpClient,
  ) { }

  ngOnInit(): void {
    if (!this.summarySponsorVo.length) {
      this.summarySponsorVo = [{}];
    }

    if (this.sponsorsVO && !this.sponsorsVO.length) {
      this.sponsorsVO = [];
    }

    if (!this.waggerPool) {
      this.waggerPool = {};
    }
    this.Step7 = this.tournament.form;

    if (this.Step7.value.organizerDetails) {
      this.waggerPool = this.Step7.value.organizerDetails;
      this.waggerPool.amounts = this.Step7.value.organizerDetails.prize;
      /*this.Step7.value.organizerDetails.prizeAmount =
        this.Step7.value.organizerDetails.entranceFee &&
          this.Step7.value.organizerDetails.entranceFee
          ? parseInt(this.Step7.value.organizerDetails.entranceFee) *
          parseInt(this.Step7.value.organizerDetails.participants)
          : '0';*/
      this.selectPay = this.Step7.value.organizerDetails.payout;
      this.selectCurrency = this.Step7.value.organizerDetails.currency;
    }

    if (!this.Step7?.get('teamLogo'))
      this.Step7.addControl('teamLogo', new FormControl('', []));

    if (!this.Step7?.get('custEndDate'))
      this.Step7.addControl('custEndDate', new FormControl('', []));

    if (!this.Step7?.get('endDate'))
      this.Step7.addControl('endDate', new FormControl('', []));

    if (!this.Step7?.get('custEndTime'))
      this.Step7.addControl('custEndTime', new FormControl('', []));

    if (!this.Step7?.get('endTime'))
      this.Step7.addControl('endTime', new FormControl('', []));

    this.gameList = this.eSportsTournamentService.getGameList;
    this.imgurl = this.Step7?.value?.bannerSRC;
    this.privateVisiblity = ((this.Step7.value.visibility && this.Step7.value.visibility==1) ? false : true );
    this.publicVisiblity = ((this.Step7.value.visibility && this.Step7.value.visibility==1) ? true : false );
    this.getGameName();
    this.platformList = this.eSportsTournamentService.getPlatformList;
    this.getPlatformName();
    this.fetchBracketMockStructure();
    this.createTournamentForm();
    this.setMinimumDate();
    this.battleRoyaleForm.addControl(
      'numberOfSubstitute',
      this.fb.control(null, [])
    );
    this.battleRoyaleForm.addControl(
      'numberOfwinning',
      this.fb.control(null, (this.Step7?.value?.bracketType=='round_robin' ? [Validators.required] : []))
    );
    this.battleRoyaleForm.addControl(
      'numberOfparticipants',
      this.fb.control(null, (this.Step7?.value?.bracketType=='battle_royale' ? [Validators.required] : []))
    );
    this.battleRoyaleForm.addControl(
      'numberOfKills',
      this.fb.control(null, [])
    );
    this.battleRoyaleForm.addControl(
      'numberOfMatch',
      this.fb.control(null, [])
    );
    this.battleRoyaleForm.addControl(
      'numberOfStageAutoGen',
      this.fb.control(null, [])
    );
    this.battleRoyaleForm.addControl(
      'pointsUntilPlace',
      this.fb.control(null, (this.Step7?.value?.bracketType=='battle_royale' ? [Validators.required] : []))
    );

    if(this.Step7?.value?.bracketType=='battle_royale'){
      this.inputPointsPlaceVo = [{inputPointsValue:null},{inputPointsValue:null}];
      for(let i=0;i<this.inputPointsPlaceVo.length;i++){
        this.battleRoyaleForm.addControl(
          'inputPointsPlace'+i,
          this.fb.control(null, (this.Step7?.value?.bracketType=='battle_royale' ? [Validators.required] : []))
        );
      }
    }
    this.ruleInfoForm.addControl('ruleInfoText', this.fb.control('', []));
    this.ruleInfoForm.addControl('contactOption', this.fb.control('', []));
    this.ruleInfoForm.addControl(
      'socialFacebookUrl',
      this.fb.control(null, [
        Validators.pattern(EsportsConstantsService.facebookRegex),
      ])
    );
    this.ruleInfoForm.addControl(
      'twitchStreemingUrl',
      this.fb.control(null, [
        Validators.pattern(EsportsConstantsService.twitchRegex),
      ])
    );
    this.ruleInfoForm.addControl(
      'videoStreemingUrl',
      this.fb.control(null, [
        Validators.pattern(EsportsConstantsService.youtubeRegex),
      ])
    );
    this.ruleInfoForm.addControl(
      'contactStreemingUrl',
      this.fb.control(null, [
        Validators.pattern(EsportsConstantsService.webUrlRegex),
      ])
    );
    this.poolSettingPrizeForm.addControl(
      'enterenceFee',
      this.fb.control('', [])
    );
    if(this.waggerPool && this.waggerPool.amounts){
    for (let i = 0; i < this.waggerPool.amounts.length; i++) {
      this.poolSettingPrizeForm.addControl(
        'enterenceInput1' + i,
        this.fb.control(this.waggerPool.amounts[i].from, [])
      );
      this.poolSettingPrizeForm.addControl(
        'enterenceInput2' + i,
        this.fb.control(this.waggerPool.amounts[i].to, [])
      );
      this.poolSettingPrizeForm.addControl(
        'enterenceInput3' + i,
        this.fb.control(this.waggerPool.amounts[i].weightage, [])
      );
    }


  }

    for (let i = 0; i < this.summarySponsorVo.length; i++) {
      this.SponsorForm.addControl(
        'SponsorName' + i,
        this.fb.control(this.summarySponsorVo[i].SponsorName, [
          Validators.required,
        ])
      );
      this.SponsorForm.addControl(
        'SponsorURL' + i,
        this.fb.control(this.summarySponsorVo[i].SponsorURL, [
          Validators.required,
          Validators.pattern(EsportsConstantsService.webUrlRegex),
        ])
      );
      this.SponsorForm.addControl(
        'SponsorAppleURL' + i,
        this.fb.control(this.summarySponsorVo[i].SponsorAppleURL, [
          Validators.pattern(EsportsConstantsService.appStoreUrlRegex),
        ])
      );
      this.SponsorForm.addControl(
        'SponsorGoogleURL' + i,
        this.fb.control(this.summarySponsorVo[i].SponsorGoogleURL, [
          Validators.pattern(EsportsConstantsService.playStoreUrlRegex),
        ])
      );
      this.SponsorForm.addControl(
        'TournamentNameFile' + i,
        this.fb.control('', [])
      );
      this.SponsorForm.addControl(
        'TournamentNameFile1' + i,
        this.fb.control('', [])
      );
    }

    this.editorConfig = {
      editable: true,
      spellcheck: true,
      height: '180px',
      width: 'auto',
      minWidth: '0',
      translate: 'yes',
      enableToolbar: true,
      showToolbar: true,
      defaultParagraphSeparator: '',
      defaultFontName: '',
      defaultFontSize: '',
      fonts: [
        { class: 'arial', name: 'Arial' },
        { class: 'times-new-roman', name: 'Times New Roman' },
        { class: 'calibri', name: 'Calibri' },
        { class: 'comic-sans-ms', name: 'Comic Sans MS' },
      ],
      customClasses: [
        {
          name: 'quote',
          class: 'quote',
        },
        {
          name: 'redText',
          class: 'redText',
        },
        {
          name: 'titleText',
          class: 'titleText',
          tag: 'h1',
        },
      ],
      sanitize: true,
      toolbarPosition: 'top',
      toolbarHiddenButtons: [['bold', 'italic'], ['fontSize']],
    };
    this.setGameImage();

    /* set edit info VO object here */
    this.editInfoVo = {};
    this.getSetEditInfo()

    /* countries dat ahere */
    let countries = [];
    let regions = [];

    this.games1 = [];
    // this.userService.getStates().subscribe((res) => {
    //   allState = res.states;

    //   allState.filter((obj) => {
    //     if (obj['country_id'] == country_id) {
    //       this.stateList.push(obj);
    //     }
    //   });
    //   isEdit ? this.onStateChange() : '';
    // });
    this.userService.getStates().subscribe(
      (data: any) => {
        regions = data.states;
        regions.filter((obj) => {
        if (obj['country_id'] == 157) {
          this.regionList.push(obj);
        }
      });
        // for (let d of countries) {
        //   d.checked = false;
        //   //  this.games1.push(d)
        // }
        // this.games1 = countries;
        this.showLoader = false;
        //this.products = data;
      },
      (err) => {
        this.showLoader = false;
      }
    );
  }

  datatoimg(data) {
    this.imgurl = data;
  }

  setGameImage() {
    this.gameList.map((game) => {
      if (game._id == this.Step7.get('gameDetail').value) {
        this.selectedGameDetails = game;
      }
    });
  }

  getGameName() {
    this.gameList.map((game) => {
      if (game._id == this.Step7.get('gameDetail').value) {
        this.currentGame = game.name;
        this.advGameid = game._id;
      }
    });
  }

  getPlatformName() {
    this.platformList.map((platform) => {
      if (platform._id == this.Step7.get('platform').value) {
        this.currentPlatform = platform.name;
        this.advPlatformId = platform._id;
      }
    });
  }

  getBracketTypes() {
    this.brackettypeList.map((b) => {
      if (b.key == this.Step7.get('bracketType').value) {
        this.currentBracket = b.name;
      }
    });
  }
  setMinimumDate() {
    const date = new Date();
    date.setMinutes(date.getMinutes() + 5);
    date.setSeconds(0);
    this.minDate = date;
    this.minDateSummary = new Date();
  }

  addAnotherVeture(event) {
    event.preventDefault();
    this.offlineVenues.push({ name: '', region: '',id:null });
  }

  onVentureRegionChange(value, i) {
    this.offlineVenues[i].region = value.name;
    this.offlineVenues[i].id = value.id;
  }

  onVentureNameTextChange(value, i) {
    this.offlineVenues[i].name = value;
  }

  fetchBracketMockStructure = async () => {
    try {
      this.isBracketLoaded = false;
      const { value } = this.Step7;
      const payload = {
        bracketType: value?.bracketType,
        ...(value?.bracketType != 'swiss_safeis' && {
          maximumParticipants: value?.maxParticipants,
          noOfSet: value?.noOfSet,
        }),
        ...(['round_robin', 'battle_royale'].includes(value?.bracketType) && {
          noOfTeamInGroup: value?.noOfTeamInGroup,
          noOfWinningTeamInGroup: value?.noOfWinningTeamInGroup,
          noOfRoundPerGroup: value?.noOfRoundPerGroup,
          stageBracketType: (value?.stageBracketType ? value?.stageBracketType : this.selectStageBracket),
        }),
      };
      const response = await this.eSportsTournamentService.generateBracket(
        payload
      );
      this.structure = response.data;
      this.isBracketLoaded = true;
    } catch (error) {
      this.isBracketLoaded = true;
      const errorMessage = error?.error?.message || error?.message;
      this.eSportsToastService.showError(errorMessage);
    }
  };

  // custom field controller
  addNewControl = (field, value, customValidation?) => {
    try {
      const formControl = this.Step7.get(field);
      if (!formControl) {
        const control = customValidation
          ? new FormControl(value, customValidation)
          : new FormControl(value);

        this.Step7.addControl(field, control);
      }
    } catch (error) { }
  };

  prizeClickHandler(checked, prizeLen = 1) {
    if (checked) {
      const formControl = this.Step7.get('prizeList');
      if (formControl) return;

      this.Step7.addControl(
        'prizeList',
        this.fb.array([], this.customPrizeValidator())
      );
      for (let i = 0; i < prizeLen; i++) this.addPrize(i);
      this.Step7.addControl(
        'prizeCurrency',
        new FormControl('KWD', Validators.compose([Validators.required]))
      );
    } else {
      this.Step7.removeControl('prizeList');
      this.Step7.removeControl('prizeCurrency');
    }
  }

  sponsorClickHandler(checked, len = 1) {
    if (checked) {
      const formControl = this.Step7.get('sponsors');
      if (formControl) return;
      this.Step7.addControl('sponsors', this.fb.array([]));
      this.addSponsor(len);
    } else {
      this.Step7.removeControl('sponsors');
    }
  }
  addNewSponsor() {
    this.totalSponsor.push(1);
  }

  createSponsor(): FormGroup {
    return this.fb.group({
      sponsorName: ['', Validators.compose([])],
      website: [
        '',
        Validators.compose([
          Validators.pattern(EsportsConstantsService?.webUrlRegex),
        ]),
      ],
      playStoreUrl: [
        '',
        Validators.compose([
          Validators.pattern(EsportsConstantsService?.playStoreUrlRegex),
        ]),
      ],
      appStoreUrl: [
        '',
        Validators.compose([
          Validators.pattern(EsportsConstantsService?.appStoreUrlRegex),
        ]),
      ],
      sponsorLogo: ['', Validators.compose([])],
      sponsorBanner: ['', Validators.compose([])],
    });
  }

  addSponsor(len = 1): void {
    const sponsor = this.Step7.get('sponsors') as FormArray;
    for (let i = 0; i < len; i++) {
      sponsor.push(this.createSponsor());
    }
  }

  paidRegistrationHandler(check) {
    if (check) {
      const regCurrencyValidators = Validators.compose([Validators.required]);
      const regFeeValidators = Validators.compose([
        Validators.required,
        Validators.min(1),
      ]);
      this.addNewControl('regFeeCurrency', 'SAR', regCurrencyValidators);
      this.addNewControl('regFee', 1, regFeeValidators);
    } else {
      this.Step7.removeControl('regFeeCurrency');
      this.Step7.removeControl('regFee');
    }
  }

  createTournamentForm() {
    this.isFormLoaded = false;
    // for advance
    if (this.Step7.value?.type == 'advance') {
      const regStartDtValidation = Validators.compose([
        Validators.required,
        this.ValidateRegStartDate.bind(this),
      ]);

      const regEndDtValidation = Validators.compose([
        Validators.required,
        this.ValidateRegEndDate.bind(this),
      ]);

      const tournamentEndDtValidation = Validators.compose([
        Validators.required,
        this.ValidateEndDate.bind(this),
      ]);

      const tournamentTypeValidators = Validators.compose([
        Validators.required,
      ]);

      const youtubeVideoLinkValidators = Validators.compose([
        Validators.pattern(EsportsConstantsService.youtubeRegex),
      ]);

      const facebookVideoLinkValidators = Validators.compose([
        Validators.pattern(EsportsConstantsService.facebookRegex),
      ]);

      const twitchVideoLinkValidators = Validators.compose([
        Validators.pattern(EsportsConstantsService.twitchRegex),
      ]);

      this.addNewControl('regStartDate', '', regStartDtValidation);
      this.addNewControl('regEndDate', '', regEndDtValidation);
      this.addNewControl('endDate', '', tournamentEndDtValidation);
      this.addNewControl('tournamentType', '', tournamentTypeValidators);

      this.addNewControl('youtubeVideoLink', '', youtubeVideoLinkValidators);
      this.addNewControl('facebookVideoLink', '', facebookVideoLinkValidators);
      this.addNewControl('twitchVideoLink', '', twitchVideoLinkValidators);
      this.paidRegistrationHandler(this.Step7?.value?.isPaid);
      this.sponsorClickHandler(true, 1);
      this.prizeClickHandler(this.Step7.value?.isPrize, 1);
    }

    this.isFormLoaded = true;
  }

  onStepChange(step) {
    this.Step7.get('step').setValue(step);
  }

  popUpTitleAndText = async (data) => {
    if (data?._id) {
      return {
        title: 'TOURNAMENT.UPDATE',
        text: 'TOURNAMENT.UPDATE_TXT',
      };
    } else {
      return {
        title: 'TOURNAMENT.SAVE_TOURNAMENT',
        text: 'TOURNAMENT.STATUS_2',
      };
    }
  };

  setShowAdvanceStage(): void {
    this.showAdvanceStage = true;

    setTimeout(() => {
      this.toruEnd.nativeElement.scrollIntoView();
    }, 200);
  }

  setShowCustomRegistrationEndDate(): void {
    this.showCustomRegistrationEndDate = true;
    setTimeout(() => {
      if(this.showAdvanceStage)
        this.advOpt.nativeElement.scrollIntoView();
      else
        this.custReg.nativeElement.scrollIntoView();
    }, 200);


  }

  setShowCustomTournamentEndDate(): void {
    this.showCustomTournamentEndDate = true;
    setTimeout(() => {
      this.custReg.nativeElement.scrollIntoView();
    }, 200);
  }

  onGameChange = async (value: any) => {
    const games = this.eSportsTournamentService.getGameList;
    const selectedGame = games.find((elem) => {
      return elem?._id == value;
    });
    this.eSportsTournamentService.setPlatformList = selectedGame?.platform;
    this.platformList = this.eSportsTournamentService.getPlatformList;
  };

  Submit = async () => {
    try {
      const { value, invalid } = this.Step7;
      const invalidKeys = Object.keys(this.Step7.controls).filter((element) => {
        return this.Step7.controls[element].status != 'VALID';
      });
      if (invalid) {
        this.eSportsToastService.showError('please fill required details');
        return;
      }
      const data = await this.popUpTitleAndText(value);
      const confirmed = await this.matDialog
        .open(ConfirmComponent, { data })
        .afterClosed()
        .toPromise();

      if (!confirmed) return;

      this.isProcessing = true;
      const response = value?._id
        ? await this.eSportsTournamentService.updateTournament(
          value,
          value?._id
        )
        : await this.eSportsTournamentService.saveTournament(value);

      this.eSportsToastService.showSuccess(response?.message);
      if (response?.data?.enablePayment) {
        this.tournamentDetails = response?.data;
        this.tournamentEvent.emit(this.tournamentDetails);
        this.Step7.get('step').setValue(11);
      } else {
        this.router.navigate(['/profile/tournaments-created']);
      }

      this.isProcessing = false;
    } catch (error) {
      this.isProcessing = false;
      const errorMessage = error?.error?.message || error?.message;
      this.eSportsToastService.showError(errorMessage);
    }
  };

  addPrize(i): void {
    const prizeList = this.Step7.get('prizeList') as FormArray;
    const { maxParticipants } = this.Step7.value;
    let name = '';
    if (prizeList.controls.length < maxParticipants) {
      switch (true) {
        case prizeList.controls.length == 0:
          name = '1st';
          break;
        case prizeList.controls.length == 1:
          name = '2nd';
          break;
        case prizeList.controls.length == 2:
          name = '3rd';
          break;
        default:
          name = `${prizeList.controls.length + 1}th`;
          break;
      }
      const createPrizeForm = (): FormGroup => {
        const validator = [Validators.required, Validators.pattern('^[0-9]*$')];
        if (i == 0) {
          validator.push(Validators.min(1));
        }
        return this.fb.group({
          name: [name],
          value: ['', Validators.compose(validator)],
        });
      };
      prizeList.push(createPrizeForm());
    } else {
    }
  }

  // Custom validations

  ValidateRegStartDate(
    control: AbstractControl
  ): { [key: string]: any } | null {
    return !control.value
      ? null
      : this.validateRegStartDate(control['_parent'], 'pastRegStartDate');
  }

  ValidateRegEndDate(control: AbstractControl): { [key: string]: any } | null {
    return !control.value
      ? null
      : this.validateRegEndDate(control['_parent'], 'pastRegEndDate');
  }

  ValidateEndDate(control: AbstractControl): { [key: string]: any } | null {
    return !control.value
      ? null
      : this.validateBothEndDate(control['_parent'], 'pastEndDate');
  }

  validateRegStartDate(
    formGroup: FormGroup,
    ekey: string
  ): { [key: string]: boolean } {
    const { regStartDate, startDate } = formGroup.controls;

    if (ekey == 'pastRegStartDate' && !regStartDate?.value) {
      return { required: true };
    } else if (startDate?.value && regStartDate?.value) {
      const currentDate = new Date();
      const startDt = new Date(startDate.value);
      const regstartDt = new Date(regStartDate.value);

      if (startDt > regstartDt && regstartDt > currentDate) {
        return null;
      } else {
        return {
          [ekey]: true,
        };
      }
    }
    return null;
  }

  validateRegEndDate(
    formGroup: FormGroup,
    ekey: string
  ): { [key: string]: boolean } {
    const { regStartDate, regEndDate, startDate } = formGroup.controls;
    if (ekey == 'pastRegEndDate' && !regEndDate?.value) {
      return { required: true };
    } else if (regEndDate?.value && regStartDate?.value && startDate?.value) {
      const regEndDt = new Date(regEndDate.value);
      const regstartDt = new Date(regStartDate.value);
      let tournamentStartDt, tournamentStartDate;
      tournamentStartDt = new Date(startDate.value);

      if (regEndDt > regstartDt && regEndDt <= tournamentStartDt) {
        return null;
      } else {
        return {
          [ekey]: true,
        };
      }
    }
    return null;
  }

  validateBothEndDate(
    formGroup: FormGroup,
    ekey: string
  ): { [key: string]: boolean } {
    const { startDate, startTime, endDate, endTime } = formGroup.controls;
    if (ekey == 'pastEndDate' && !endDate?.value) {
      return { required: true };
    } else if (startDate?.value && endDate?.value) {
      const startDt = new Date(startDate.value);
      const endDt = new Date(endDate.value);
      if (endDt > startDt) {
        return null;
      } else {
        return {
          [ekey]: true,
        };
      }
    }
    return null;
  }

  customPrizeValidator(): ValidatorFn {
    return (formArray: FormArray): { [key: string]: any } | null => {
      let total = 0;
      for (let item of formArray.controls) {
        total = total + item.value.value;
      }
      const maxParticipants =
        formArray['_parent']?.controls?.maxParticipants?.value;
      const prizeLimit = maxParticipants;
      if (formArray.controls.length > prizeLimit) {
        return { prizeLimit: true };
      }
      const isPrizeValid = formArray.controls.length ? total > 0 : true;
      return isPrizeValid ? null : { prizeMoneyRequired: true };
    };
  }

  emptyCheck = (control: FormControl) => {
    return control.value && control.value.trim() ? null : { required: true };
  };

  onEdit() {
    this.Step7.get('step').setValue(1);
  }

  onDateChange(data) {
    if (GlobalUtils.isBrowser()) {
      this.date = data;
      localStorage.setItem('date', data);
    }
  }
  onTimeChange(data) {
    if (GlobalUtils.isBrowser()) {
      this.time = data;
      localStorage.setItem('time', data);
    }
  }

  checkDateTime(): Boolean {
    return true;
  }

  ngOnDestroy(): void {
    if (this.tournamentDetails2) this.tournamentDetails2.unsubscribe();
  }
  clickChecked(checked) {
    this.dataTournamentTypeValue = checked.id;
    checked.ichecked = true;
    if (this.dataTournamentTypeValue == 2) {
      this.showCustomTournamentEndDate = true;
    }
    // this.Step6.get('step').setValue(step);
  }

  handleFileInputLogo(files: FileList, indx) {
    /*this.toggleInputFileLogo = !this.toggleInputFileLogo;
    this.checkFileNameLogo = true;
    this.fileToUploadLogo = files.item(0);*/
    this.isValidImage(files, { width: 800, height: 800 }, 'sponcerLogo', indx);
  }

  handleFileInputBanner(files: FileList, indx) {
    /*this.toggleInputFileBanner = !this.toggleInputFileBanner;
    this.checkFileNameBanner = true;
    this.fileToUploadBanner = files.item(0);*/
    this.isValidImage(files, { width: 1920, height: 300 }, 'banner', indx);
  }

  // Handlechange currency
  onToggleChangeCurrency(currency): void {
    this.selectCurrency = currency;
  }

  // Handlechange payloadMethod Component 2
  onToggleChangePayouts(pay): void {
    this.selectPay = pay;
  }
  onToggleChangeContact(contact): void {
    this.selectContact = contact;
  }

  addPayout(): void {
    if (this.waggerPool === 'none') {
      let idP = this.payouts[this.payouts.length - 1];
      this.payouts.push({ from: '', to: '', weightage: '' });
    } else {
      let idP = this.waggerPool.amounts[this.waggerPool.amounts.length - 1];
      this.waggerPool.amounts.push({ from: '', to: '', weightage: '' });
    }
    this.waggerPool.entranceFee =
      this.Step7.value.waggerPoolDetails.entranceFee;
    this.poolSettingPrizeForm.addControl(
      'enterenceInput1' + (this.waggerPool.amounts.length - 1),
      this.fb.control(null, [])
    );
    this.poolSettingPrizeForm.addControl(
      'enterenceInput2' + (this.waggerPool.amounts.length - 1),
      this.fb.control(null, [])
    );
    this.poolSettingPrizeForm.addControl(
      'enterenceInput3' + (this.waggerPool.amounts.length - 1),
      this.fb.control(null, [])
    );
  }

  onToggleDelete(id): void {
    if (this.waggerPool === 'none') {
      const indexPayout = this.payouts.findIndex((p) => p === id);
      if (indexPayout < 0) return;

      const newPayout = this.payouts;
      if (newPayout.length > 1) {
        newPayout.splice(indexPayout, 1);
      }
      this.payouts = newPayout;
    } else {
      const indexPayout = this.waggerPool.amounts.findIndex((p) => p === id);
      if (indexPayout < 0) return;

      const newPayout = this.waggerPool.amounts;
      if (newPayout.length > 1) {
        newPayout.splice(indexPayout, 1);
      }
      this.waggerPool.amounts = newPayout;
    }
    this.poolSettingPrizeForm = new FormGroup({});
    for (let i = 0; i < this.waggerPool.amounts.length; i++) {
      this.poolSettingPrizeForm.addControl(
        'enterenceInput1' + i,
        this.fb.control(this.waggerPool.amounts[i].from, [])
      );
      this.poolSettingPrizeForm.addControl(
        'enterenceInput2' + i,
        this.fb.control(this.waggerPool.amounts[i].to, [])
      );
      this.poolSettingPrizeForm.addControl(
        'enterenceInput3' + i,
        this.fb.control(this.waggerPool.amounts[i].weightage, [])
      );
    }
  }

  onToggleChangePrizePayout(): void {
    this.toggleChangePrizePayout = !this.toggleChangePrizePayout;
  }

  onToggleChangeTournamentSettings(): void {
    this.toggleChangeTournamentSettings = !this.toggleChangeTournamentSettings;
  }

  onToggleChangeRulesAndInfo(): void {
    this.toggleChangeRulesAndInfo = !this.toggleChangeRulesAndInfo;
  }

  onToggleChangeSponsors(): void {
    this.toggleChangeSponsors = !this.toggleChangeSponsors;
  }
  onToggleChangeNumberOfRounds(numberOfRound): void {
    this.selectRound = numberOfRound;
  }

  onToggleChangePlaceMatchs(placeMatch): void {
    this.selectPlaceMatch = placeMatch.value;
    this.selectPlaceMatchKey = placeMatch.key;
  }

  onToggleChangeMatchFormats(matchFormat): void {
    this.selectMatchFormat = matchFormat.name;
    this.selectMatchFormatSet = matchFormat._id;
  }

  onToggleChangeStageBrackets(stageBracket): void {
    this.selectStageBracket = stageBracket;
  }

  onToggleChangeTieBreaker(tie): void {
    this.selectTieBreaker = tie.value;
    this.selectTieBreakerKey = tie.key;
  }

  getValueInputNumberOfKills(kills){
    this.numberNoOfKills = (kills.target.value ? parseInt(kills.target.value) : 0);
  }

  onToggleChangeregion(region): void {
    this.selectRegion = region;
  }

  onCheckscreenshotRequired(): void {
    this.screenshotRequired = !this.screenshotRequired;
  }

  onCheckShowCountryFlag(): void {
    this.showCountryFlag = !this.showCountryFlag;
  }

  onCheckScoreReporting(): void {
    this.participants = !this.participants;
    this.myselfOnly = !this.myselfOnly;
  }

  onCheckParticipantLimit(): void {
    this.noLimit = !this.noLimit;
    this.addLimit = !this.addLimit;
  }

  onCheckVisibility(): void {
    this.privateVisiblity = !this.privateVisiblity;
    this.publicVisiblity = !this.publicVisiblity;
    this.Step7.value.visibility = (this.privateVisiblity ? 0 : 1);
    this.getSetEditInfo();
  }

  onCheckCheckInRequired(): void {
    this.noCheckIn = !this.noCheckIn;
    this.addCheckIn = !this.addCheckIn;
  }

  onCheckMandatorySubstitute(): void {
    this.noSubstitute = !this.noSubstitute;
    this.addSubstitute = !this.addSubstitute;
  }

  getValueInputNumberSubstitutes(value: any): void {
    this.numberSubstitutes = parseInt(value.target.value);
  }

  onChangeNumberSubstitutes(value: number): void {
    let checkNumber = this.numberSubstitutes + value;
    if (checkNumber < 0) {
      this.numberSubstitutes = 0;
    } else {
      this.numberSubstitutes += value;
    }
  }

  getValueInputWinningParticipants(value: any): void {
    this.numberWinningParticipants = parseInt(value.target.value);
  }

  onChangeWinningParticipants(value: number): void {
    let checkNumber = this.numberWinningParticipants + value;
    if (checkNumber < 0) {
      this.numberWinningParticipants = 0;
    } else {
      this.numberWinningParticipants += value;
    }
  }

  getValueInputNoOfParticipants(value: any): void {
    this.numberNoOfParticipants = (value.target.value ? parseInt(value.target.value) : 0);
  }

  onChangeNoOfParticipants(value: number): void {
    this.numberNoOfParticipants += value;
  }

  getValueInputStageAutoGen(value: any){
    this.numberOfStageAutoGen = (value.target.value ? parseInt(value.target.value) : 0);
  }

  onChangeStageAutoGen(value: number){
    this.numberOfStageAutoGen += value;
  }

  getValueInputNoOfMatch(value: any){
    this.numberOfMatch = (value.target.value ? parseInt(value.target.value) : 0);
  }

  onChangeNoOfMatch(value: number){
    this.numberOfMatch += value;
  }

  getValueInputPointsUntilPlace(value: any): void {
    this.pointsUntilPlace = (value.target.value ? parseInt(value.target.value) : 0);
  }

  onChangePointsUntilPlace(value: number): void {

    if(this.inputPointsPlaceVo.length>=2 && value==1){
      this.eSportsToastService.showError(this.translateService.instant('CREATE_TOURNMENT.MAX_POINTSLIMIT_REACHED'));
      return;
    }

    if(this.pointsUntilPlace || this.pointsUntilPlace==0)
      this.pointsUntilPlace += value;
    else
      this.pointsUntilPlace = 0;

    if(value == 1){
      this.inputPointsPlaceVo.push({inputPointsValue:null});
      let placeIndx = this.inputPointsPlaceVo.length-1;
      this.battleRoyaleForm.addControl(
        'inputPointsPlace'+placeIndx,
        this.fb.control(null, (this.Step7?.value?.bracketType=='battle_royale' ? [Validators.required] : []))
      );
    }else{
      this.inputPointsPlaceVo.splice(this.inputPointsPlaceVo.length-1);
      let placeIndx = this.inputPointsPlaceVo.length+1;
      this.battleRoyaleForm.removeControl('inputPointsPlace'+placeIndx);
    }

  }

  getValueInputPointsPlace(value: any,index:any){
    this.inputPointsPlaceVo[index].inputPointsValue = ((!value.target.value) ? null : parseInt(value.target.value));
  }

  getValueSelectStages(value: any): void {
    this.selectStages = value.name;
    this.selectStagesId = value._id;
  }

  getValueNextStageMatchFormats(value: any) {
    this.selectStageMatchFormats = value.name;
    this.selectStageMatchFormatsId = value._id;
  }

  /* function publish advance tournament here */
  tournamentPublish() {
    this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.currentUser = data;
      }
    });

    /* check for form valid here */
    /*if(this.SponsorForm.invalid){
      this.eSportsToastService.showError(this.translateService.instant('CREATE_TOURNMENT.SPONSOR_REQUIRED'));
      return;
    }*/

    if (
      this.ruleInfoForm.invalid ||
      ((this.Step7?.value?.bracketType=='round_robin' || this.Step7?.value?.bracketType=='battle_royale') && this.battleRoyaleForm.invalid) ||
      (this.Step7?.value?.bracketType=='round_robin' && !this.selectRound) ||
      ((this.Step7?.value?.bracketType!='swiss_safeis' && this.Step7?.value?.bracketType!='battle_royale') && !this.selectMatchFormatSet) ||
      (this.Step7?.value?.bracketType=='battle_royale' ? (!this.pointsUntilPlace || this.pointsUntilPlace==0 ) : false)
    ) {
      this.eSportsToastService.showError(
        this.translateService.instant('CREATE_TOURNMENT.MANDATORY_REQUIRED')
      );
      return;
    }

    /* automatically add 7 days from start tournament date */
    let autocalculatedEndDate = new Date(this.Step7?.value?.startDate);
    let receivedCalculatedEndDate = null;
    if (!this.Step7?.value?.endDate) {
      receivedCalculatedEndDate = autocalculatedEndDate.setDate(
        autocalculatedEndDate.getDate() + 7
      );
    } else {
      receivedCalculatedEndDate = this.Step7?.value?.endDate;
    }

    /* include sponsor details while tournament publish */
    if(this.sponsorsVO && this.summarySponsorVo){
      for(let i=0;i<this.summarySponsorVo.length;i++){
        let temVO:any = {'sponsorName':(this.SponsorForm.value['SponsorName'+i]),'website':(this.SponsorForm.value['SponsorURL'+i]),'appStoreUrl':(this.SponsorForm.value['SponsorAppleURL'+i]),'playStoreUrl':(this.SponsorForm.value['SponsorGoogleURL'+i]),'sponsorLogo':(this.summarySponsorVo[i].TournamentNameFile),'sponsorBanner':(this.summarySponsorVo[i].TournamentNameFile1)};
        this.sponsorsVO.push(temVO);
      }
    }
    const allFormData = this.Step7.value;
    let autocalculatedRegEndDate:any = null;
    if(this.checkStartTime(allFormData.startTime)){
      const getRegEndDate:any = new Date(allFormData.startDate);
      autocalculatedRegEndDate = new Date(getRegEndDate.setDate(getRegEndDate.getDate() - 1));
    }
    const teamFormatSelected = this.teamFormat.find((ele) => {
      if(ele.id == allFormData?.teamTournamentType){
        return ele;
      }
    });
    const getPlacementPoints = (inputPointsPlaceVal) => {
      inputPointsPlaceVal = inputPointsPlaceVal.sort((a, b)=> {
        return a.inputPointsValue - b.inputPointsValue
      })
      const newPlacementPoints = []
      for (let index = 0; index < inputPointsPlaceVal.length; index++) {
        newPlacementPoints.push({ position: index+1, value: inputPointsPlaceVal[index]  })
      }
      return newPlacementPoints;
    }
    /* combine tournament start date & time here */
    var updateStartDateTime:any = new Date(this.Step7.value.startDate).setDate(this.Step7.value.startDate.getDate()+1);
    updateStartDateTime = new Date(updateStartDateTime).toISOString().substring(0, 10);
    updateStartDateTime = updateStartDateTime+' '+this.Step7.value.startTime;
    let updatedStartDateTime = new Date(updateStartDateTime);

    let advTournamentPayloadData = {
      name: this.Step7?.value?.name,
      participantType:
        this.Step7?.value?.tournamentType &&
          this.Step7?.value?.tournamentType == 'Single'
          ? 'individual'
          : 'team',
      Tournmenttype: this.dataTournamentTypeValue,
      startDate: (updatedStartDateTime ? updatedStartDateTime : this.Step7?.value?.startDate ),
      startTime: this.Step7?.value?.startTime,
      endDate: new Date(receivedCalculatedEndDate),
      endTime: (allFormData.endTime
        ? allFormData.endTime
        : '12:00 AM'),
      regStartDate: new Date(),
      regEndDate: (allFormData.custEndDate
        ? allFormData.custEndDate
        : ( this.checkStartTime(allFormData.startTime) ? autocalculatedRegEndDate : allFormData.startDate)),
      regEndTime: (allFormData.custEndTime
        ? allFormData.custEndTime
        : this.calculateRegEndTime(this.Step7?.value?.startTime)),
      regStartTime: this.timeFormatAMPM(new Date()),
      isPaid: false,
      isRegStartDate: true,
      isRegEndDate: true,
      description: (allFormData.tournamentAddOnDescription
        ? allFormData.tournamentAddOnDescription
        : ''), // optional description here
      rules: this.advSummaryRuleSet,
      criticalRules: '',
      isPrize: ((allFormData.isPaid.id != 2) ? true : false),
      faqs: '',
      schedule: '',
      isIncludeSponsor: true,
      noOfStage:(this.numberOfStageAutoGen ? this.numberOfStageAutoGen : 0),
      isKillPointRequired: ((allFormData.bracketType=='battle_royale' && this.selectTieBreaker=='Enabled') ? true : false ),
      enableTiebreaker:false,
      pointsKills: (((allFormData.bracketType=='battle_royale') && this.selectTieBreaker=='Enabled') ? this.pointsUntilPlace : 0),
      tournamentType:
        ((this.dataTournamentTypeValue == 0 || this.dataTournamentTypeValue == 1)
          ? (this.dataTournamentTypeValue == 1
            ? 'offline'
            : 'online')
          : 'hybrid'),
      type: 'advance',
      isScreenshotRequired: this.screenshotRequired,
      isShowCountryFlag: this.showCountryFlag,
      isManualApproverParticipant: false,
      isSpecifyAllowedRegions: this.Step7?.value?.isSpecifyAllowedRegions,
      isParticipantsLimit: this.Step7?.value?.isParticipantsLimit,
      scoreReporting: this.participants ? 1 : 2,
      SpecifyAllowedRegions: this.selectRegion
        ? [this.selectRegion]
        : [],
      invitationLink: '',
      visibility: (this.Step7?.value?.visibility),
      checkInEndDate: '',
      sponsors: this.sponsorsVO,
      banner: allFormData.bannerSRC ? allFormData.bannerSRC : '',
      logo: this.imgurl
        ? this.imgurl
        : this.Step7?.value?.bannerSRCSummaryLogo
          ? this.Step7?.value?.bannerSRCSummaryLogo
          : '',
      maxParticipants: this.Step7?.value?.noOfParticipants,
      bracketType: this.Step7?.value?.bracketType,
      noOfSet: (this.selectMatchFormatSet),
      bracket_data: null,
      stageMatch: (this.selectStagesId ? this.selectStagesId : ''),
      stageMatchNoOfSet: (this.selectStageMatchFormatsId ? this.selectStageMatchFormatsId : 0),
      isAllowMultipleRounds: false,
      venueAddress: ((this.dataTournamentTypeValue == 2 && this.offlineVenues.length) ? this.offlineVenues : []),
      contactOn: '',
      gameDetail: this.advGameid,
      teamSize: (allFormData.tournamentType != 'Single' ? (teamFormatSelected.id!='n' ? teamFormatSelected?.teamSize : (allFormData.noOfTeamSize ? allFormData.noOfTeamSize : 5)) : 0),
      whatsApp: '',
      prizeList:
        this.waggerPool && this.waggerPool.prize && this.waggerPool.prize.length
          ? this.waggerPool.prize
          : [],
      substituteMemberSize: (allFormData.tournamentType != 'Single' ? this.numberSubstitutes : 0),
      stageBracketType: (allFormData.bracketType=='round_robin' ? this.selectStageBracket : ''),
      platform: this.advPlatformId,
      //"Tournmenttype":this.Tournmenttype,
      allowSubstituteMember: (allFormData.tournamentType == 'Single' ? false : true),
      allowAdvanceStage: false,
      step: 7,
      isCheckInRequired: (this.addCheckIn),
      checkInStartDate: '',
      isAllowThirdPlaceMatch: (this.selectPlaceMatch == 'Enabled' ? true : false),
      createdBy: this.currentUser._id,
      updatedBy: this.currentUser._id,
      /*tournamentStatus: 'submitted_for_approval',*/
      organizerDetail: this.currentUser._id,
      noOfTeamInGroup: this.Step7?.value?.noOfTeamInGroup,
      noOfWinningTeamInGroup: this.Step7?.value?.noOfWinningTeamInGroup,
      noOfRoundPerGroup: (allFormData.bracketType=='battle_royale' ? this.numberOfMatch : allFormData.noOfRoundPerGroup),
      emailInvite: [],
      placementPoints: (allFormData.bracketType=='battle_royale' ? getPlacementPoints(this.inputPointsPlaceVo) : [])
    };


    if (
      this.showCustomTournamentEndDate &&
      (!receivedCalculatedEndDate || !this.Step7?.value?.startDate)
    ) {
      this.eSportsToastService.showError(this.translateService.instant('CREATE_TOURNAMENT.REG_DATE_INVALID'));
      return;
    }

    if(this.checkRegistationEndDateTime(advTournamentPayloadData)){
      this.eSportsToastService.showError(this.translateService.instant('CREATE_TOURNAMENT.REG_TIME_INVALID'));
      return;
    }


    if (this.Step7.value.startDate < new Date(receivedCalculatedEndDate)) {
      if (this.Step7?.value?.noOfParticipants >= 2) {
        this.showLoader = true;
        this.eSportsTournamentService
          .saveTournament1(advTournamentPayloadData)
          .subscribe(
            (response: any) => {
              this.showLoader = false;
              this.eSportsToastService.showSuccess(response.message);
              this.router.navigateByUrl(
                '/create-tournament/CreateQuick/' +
                (response.data[0]
                  ? response.data[0].slug
                  : response.data.slug)
              );

            },
            (err) => {
              this.showLoader = false;
              this.eSportsToastService.showError(err.message);
            }
          );
      } else {
        this.eSportsToastService.showError('Minimum allowed limit is 2');
      }
    }
  }

  isValidImage(files: any, values, type: string = 'banner', indx): Boolean {
    if (files && files[0]) {
      if (files[0]['size'] / (1000 * 1000) > 2) {
        this.eSportsToastService.showError(
          this.translateService.instant('TOURNAMENT.ERROR_IMG_2')
        );
        return;
      }

      if (
        files[0].type != 'image/jpeg' &&
        files[0].type != 'image/jpg' &&
        files[0].type != 'image/png'
      ) {
        this.eSportsToastService.showError(
          this.translateService.instant('TOURNAMENT.ERROR_IMG_1')
        );
        return;
      }

      try {
        const file = files[0];

        const reader = new FileReader();
        reader.readAsDataURL(file);
        let th = this;
        reader.onload = (e) => {
          let image: any = new Image();
          image.src = e.target.result;
          image.onload = function () {
            let height = this.height;
            let width = this.width;
            if (height != values['height'] && width != values['width']) {
              th.eSportsToastService.showInfo(
                th.translateService.instant('TOURNAMENT.ERROR_IMG')
              );
            }
            th.upload(files, type, indx);
          };
        };
      } catch (err) {
        return false;
      }
    } else {
      return false;
    }
  }

  async upload(files, type: string = 'banner', indx) {
    let promises = [];
    for (let obj of files) {
      promises.push(this.toBase64(obj));
    }
    let Base64String = await Promise.all(promises);
    let imageData = {
      path: environment.tournamentS3BucketName,
      files: Base64String,
    };
    this.bannerSRCFile = null;
    this.s3Service.fileUpload(environment.apiEndPoint, imageData).subscribe(
      (res) => {
        if (type == 'banner') {
          this.bannerSRCFile = res['data'][0]['Location'];
          this.Step7?.get('TournamentNameFile1' + indx)?.setValue(
            this.bannerSRCFile
          );
          this.summarySponsorVo[indx].TournamentNameFile1 = this.bannerSRCFile;
          this.toggleInputFileBanner = !this.toggleInputFileBanner;
          this.checkFileNameBanner = true;
          this.fileToUploadBanner = files.item(0);
          this.summarySponsorVo[indx].bannerFileName = this.fileToUploadBanner.name;
        } else {
          this.bannerSRCFile = res['data'][0]['Location'];
          this.Step7?.get('TournamentNameFile' + indx)?.setValue(
            this.bannerSRCFile
          );
          this.summarySponsorVo[indx].TournamentNameFile = this.bannerSRCFile;
          this.toggleInputFileLogo = !this.toggleInputFileLogo;
          this.checkFileNameLogo = true;
          this.fileToUploadLogo = files.item(0);
          this.summarySponsorVo[indx].LogoFileName = this.fileToUploadLogo.name;
        }

        this.eSportsToastService.showSuccess(
          this.translateService.instant('TOURNAMENT.BANNER_UPLOADED')
        );
      },
      (err) => {
        this.eSportsToastService.showError(
          this.translateService.instant('TOURNAMENT.ERROR_LOGO')
        );
      }
    );
  }

  toBase64(file) {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = (error) => reject(error);
    });
  }

  goTostepFirst() {
    this.Step7?.get('step')?.setValue(1);
  }

  /* add multiple sponsor here */
  addMultipleSponsor() {
    this.summarySponsorVo.push({});

    this.SponsorForm.addControl(
      'SponsorName' + (this.summarySponsorVo.length - 1),
      this.fb.control('', [Validators.required])
    );
    this.SponsorForm.addControl(
      'SponsorURL' + (this.summarySponsorVo.length - 1),
      this.fb.control('', [
        Validators.required,
        Validators.pattern(EsportsConstantsService.webUrlRegex),
      ])
    );
    this.SponsorForm.addControl(
      'SponsorAppleURL' + (this.summarySponsorVo.length - 1),
      this.fb.control('', [
        Validators.pattern(EsportsConstantsService.appStoreUrlRegex),
      ])
    );
    this.SponsorForm.addControl(
      'SponsorGoogleURL' + (this.summarySponsorVo.length - 1),
      this.fb.control('', [
        Validators.pattern(EsportsConstantsService.playStoreUrlRegex),
      ])
    );
    this.SponsorForm.addControl(
      'TournamentNameFile' + (this.summarySponsorVo.length - 1),
      this.fb.control('', [])
    );
    this.SponsorForm.addControl(
      'TournamentNameFile1' + (this.summarySponsorVo.length - 1),
      this.fb.control('', [])
    );
  }

  /* delete sponsor VO object */
  deleteMultipleSponsor(ind) {
    if (this.summarySponsorVo.length > 1) {
      this.summarySponsorVo.splice(ind, 1);
      this.SponsorForm = new FormGroup({});
      for (let i = 0; i < this.summarySponsorVo.length; i++) {
        this.SponsorForm.addControl(
          'SponsorName' + i,
          this.fb.control(this.summarySponsorVo[i].SponsorName, [
            Validators.required,
          ])
        );
        this.SponsorForm.addControl(
          'SponsorURL' + i,
          this.fb.control(this.summarySponsorVo[i].SponsorURL, [
            Validators.required,
            Validators.pattern(EsportsConstantsService.webUrlRegex),
          ])
        );
        this.SponsorForm.addControl(
          'SponsorAppleURL' + i,
          this.fb.control(this.summarySponsorVo[i].SponsorAppleURL, [
            Validators.pattern(EsportsConstantsService.appStoreUrlRegex),
          ])
        );
        this.SponsorForm.addControl(
          'SponsorGoogleURL' + i,
          this.fb.control(this.summarySponsorVo[i].SponsorGoogleURL, [
            Validators.pattern(EsportsConstantsService.playStoreUrlRegex),
          ])
        );
        this.SponsorForm.addControl(
          'TournamentNameFile' + i,
          this.fb.control('', [])
        );
        this.SponsorForm.addControl(
          'TournamentNameFile1' + i,
          this.fb.control('', [])
        );
      }
    }
  }

  editInfo(){
    const dialogRef = this.matDialog.open(EsportsEditPopupComponent, {
      data: this.editInfoVo,
    });

    dialogRef.afterClosed().subscribe((newData) => {
      if (newData) {
        let selectedGame = newData?.selectedGame;
        if(this.Step7.value.gameDetail != newData?.selectedGame?._id){
          let currentBracket:any = [];
          var keys = Object.keys(selectedGame?.bracketTypes);
          keys.map((b) => {
            if(selectedGame?.bracketTypes[b])
            currentBracket.push(b);
          });
          this.Step7.value.bracketType = currentBracket[0] ? currentBracket[0] : '';
        }
        this.Step7.value.gameDetail = newData?.selectedGame?._id;
        this.Step7.value.platform = newData?.selectedGame?.platform[newData?.selectedPlatformIndex]._id;
        this.Step7.value.tournamentAddOnDescription = newData?.tournamentNameTextarea;
        this.Step7.value.selected_platform = newData?.selected_platform;
        this.Step7.value.tournamentNameTextarea = newData?.tournamentNameTextarea;
        this.Step7.value.tournamentType = (newData?.tournamentType ? (newData?.tournamentType?.type == 'individual' ? 'Single' : 'Team' ) : newData?.tournamentType?.type);
        this.Step7.value.visibility = newData?.visibility;
        this.Step7.value.name = newData?.tournamentName;
        this.currentGame = newData?.selectedGame?.name;
        this.currentPlatform = newData?.selectedGame?.platform[newData?.selectedPlatformIndex].name;
        this.privateVisiblity = ((this.Step7.value.visibility && this.Step7.value.visibility==1) ? false : true );
        this.publicVisiblity = ((this.Step7.value.visibility && this.Step7.value.visibility==1) ? true : false );

        /* reset validation here */
        this.battleRoyaleForm.removeControl('numberOfwinning');
        this.battleRoyaleForm.addControl(
          'numberOfwinning',
          this.fb.control(null, (this.Step7?.value?.bracketType=='round_robin' ? [Validators.required] : []))
        );
        this.battleRoyaleForm.removeControl('numberOfparticipants');
        this.battleRoyaleForm.addControl(
          'numberOfparticipants',
          this.fb.control(null, (this.Step7?.value?.bracketType=='battle_royale' ? [Validators.required] : []))
        );
        this.battleRoyaleForm.removeControl('pointsUntilPlace');
        this.battleRoyaleForm.addControl(
          'pointsUntilPlace',
          this.fb.control(null, (this.Step7?.value?.bracketType=='battle_royale' ? [Validators.required] : []))
        );

        if(this.Step7?.value?.bracketType=='battle_royale'){
          for(let i=0;i<this.inputPointsPlaceVo.length;i++){
            this.battleRoyaleForm.removeControl('inputPointsPlace'+i);
          }
          this.inputPointsPlaceVo = [{inputPointsValue:null},{inputPointsValue:null}];
          for(let i=0;i<this.inputPointsPlaceVo.length;i++){
            this.battleRoyaleForm.addControl(
              'inputPointsPlace'+i,
              this.fb.control(null, (this.Step7?.value?.bracketType=='battle_royale' ? [Validators.required] : []))
            );
          }
        }

        this.getSetEditInfo();
      }
    });
  }

  getSetEditInfo(){
    for (let data of this.gameList) {
      if (data._id == this.Step7?.value?.gameDetail) {
        //this.platforms = data.platform;
        //this.Tournmenttype = data.Tournmenttype;
        this.editInfoVo.selectedGame = data;
      }
    }
    let selectedPlatformIndex = 0;
    for (let data1 of this.editInfoVo.selectedGame.platform) {
      if (data1?._id == this.Step7?.value?.platform) {
        this.editInfoVo.selectedPlatformIndex = selectedPlatformIndex;
      }
      selectedPlatformIndex++;
    }
    this.editInfoVo.tournamentName = this.Step7?.value?.name;
    this.editInfoVo.visibility = this.Step7?.value?.visibility;
    this.editInfoVo.tournamentType = {};
    this.editInfoVo.tournamentType.type = ((this.Step7?.value?.tournamentType && this.Step7?.value?.tournamentType == 'Single') ? 'individual' : 'team');
    this.editInfoVo.tournamentNameTextarea = (this.Step7?.value?.tournamentAddOnDescription ? this.Step7?.value?.tournamentAddOnDescription : '');
  }

  /* 12 hrs time format */
  timeFormatAMPM(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12;
    minutes = minutes < 10 ? '0'+minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
  }

  /* calculate registation end time here */
  calculateRegEndTime(time){
    if(time){
      var getSelectedTime:any = new Date(this.Step7.value.startDate).setDate(this.Step7.value.startDate.getDate()+1);
      getSelectedTime = new Date(getSelectedTime).toISOString().substring(0, 10)
      getSelectedTime = getSelectedTime+' '+time;
      var getSelectedDTime = new Date(getSelectedTime);

      let tempStartTime = getSelectedDTime.setMinutes(getSelectedDTime.getMinutes() - 10);
      return this.timeFormatAMPM(new Date(tempStartTime));
    }
    return (this.timeFormatAMPM(new Date()));
  }

  /* check for registation end date time here */
  checkRegistationEndDateTime(data){
    var getRegDTTime:any = new Date(data.regEndDate).setDate(data.regEndDate.getDate()+1);
    getRegDTTime = new Date(getRegDTTime).toISOString().substring(0, 10);
    getRegDTTime = getRegDTTime+' '+data.regEndTime;
    var getREGDTime = new Date(getRegDTTime);
    let tempRegEnd = getREGDTime.setMinutes(getREGDTime.getMinutes());

    var getGameDTTime:any = new Date(this.Step7.value.startDate).setDate(this.Step7.value.startDate.getDate()+1);
    getGameDTTime = new Date(getGameDTTime).toISOString().substring(0, 10);
    getGameDTTime = getGameDTTime+' '+data.startTime;
    var getSelectedDTime = new Date(getGameDTTime);
    let tempGameStart = getSelectedDTime.setMinutes(getSelectedDTime.getMinutes() - 10); //tournament start time 10 minutes before registration close

    if(tempRegEnd > tempGameStart){
      return true;
    }
    return false;
  }

  checkStartTime(time){
    if(time.includes('12') && parseInt(time.substr(3,4))<10){
      return true;
    }
    return false;
  }

}
