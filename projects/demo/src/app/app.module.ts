import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { JwtInterceptor } from './core/helpers/interceptors/token-interceptors.service';
import { AppRoutingModule } from './app-routing.module';
import { CoreModule } from './core/core.module';
import { SharedModule } from './shared/modules/shared.module';
import { AppComponent } from './app.component';
import { FooterComponent } from './core/footer/footer.component';
import { HeaderComponent } from './core/header/header.component';
import { NotfoundComponent } from './modules/not-found/not-found.component';
import { PrivacyPolicyComponent } from './modules/privacy-policy/privacy-policy.component';
import { TermsOfUsComponent } from './modules/terms-of-us/terms-of-us.component';
import { ToastsContainer } from './shared/toast/toast-container.component';
import { appInitializer } from './core/helpers/app.initializer';
import { environment } from '../environments/environment';
import { InlineSVGModule } from 'ng-inline-svg';

import {
  BrowserModule,
  BrowserTransferStateModule,
} from '@angular/platform-browser';
import { ScrollingModule } from '@angular/cdk/scrolling';
import {
  EsportsUserService,
  EsportsLoaderModule,
  EsportsPaginationService,
  EsportsModule,
  I18nModule,
  EsportsToastService,
  EsportsSnackBarModule,
  EsportsChatService
} from 'esports';

@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    HeaderComponent,
    NotfoundComponent,
    PrivacyPolicyComponent,
    TermsOfUsComponent,
    ToastsContainer,
  ],
  imports: [
    AppRoutingModule,
    BrowserAnimationsModule,
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    BrowserTransferStateModule,
    CoreModule,
    I18nModule.forRoot(environment),
    InlineSVGModule.forRoot(),
    SharedModule,
    ScrollingModule,
    EsportsModule.forRoot(environment),
    EsportsSnackBarModule.setPlatform('ESPORTSCORE'),
    EsportsLoaderModule.setColor('#1d252d'),
  ],
  exports: [HeaderComponent, FooterComponent, ToastsContainer],

  providers: [
    EsportsChatService,
    EsportsPaginationService,
    EsportsToastService,
    {
      provide: APP_INITIALIZER,
      useFactory: appInitializer,
      multi: true,
      deps: [EsportsUserService],
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptor,
      multi: true,
    },
    { provide: 'googleTagManagerId', useValue: 'G-FB4CHLFXC0' },
  ],
  bootstrap: [AppComponent],
})
export class AppModule { }
