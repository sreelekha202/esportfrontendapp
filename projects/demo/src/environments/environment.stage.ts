export const environment = {
  production: false,
  buildConfig: 'stage',
  // apiEndPoint: 'https://w5chwyhsr3.execute-api.ap-southeast-1.amazonaws.com/dev/',
  apiEndPoint:
    'https://aznf6hl673.execute-api.ap-southeast-1.amazonaws.com/core/',
  currentToken: 'DUID',
  facebookAPPID: '1578462858982905',
  googleAPPID:
    '183623623972-6aerbro3aj790tj37f23kfcohu2k3jb7.apps.googleusercontent.com',
  socketEndPoint: 'https://chat-core.dynasty-dev.com',
  cookieDomain: '.stcplay.gg',
  eCommerceAPIEndPoint:
    'https://tuub60cp6h.execute-api.ap-southeast-1.amazonaws.com/dev/',
  firebase: {
    apiKey: 'AIzaSyB1mEgrCbtK1vvE4zR7VcOzXtcWs4ysvLw',
    authDomain: 'dynasty-test.firebaseapp.com',
    databaseURL: 'https://dynasty-test.firebaseio.com',
    projectId: 'dynasty-test',
    storageBucket: 'dynasty-test.appspot.com',
    messagingSenderId: '415594613035',
    appId: '1:415594613035:web:c0a1d274208530618add6e',
    measurementId: 'G-YBTGYLZ7RN',
  },
  articleS3BucketName: 'article',
  tournamentS3BucketName: 'tournament',
  iOSBundelName: 'stcmobileapp',
  iOSAppstoreURL: 'https://apps.apple.com/us/app/mesf/id1529620569',
  payfort: {
    access_code: '2sXOSK8ogBoMmRr8sUWB',
    merchant_identifier: '4527f1a6',
    url: 'https://sbcheckout.payfort.com/FortAPI/paymentPage',
  },
  paypalScopes:
    'profile email address https://uri.paypal.com/services/paypalattributes',
  paypalAccountMode: 'sandbox',
  paypal_client_id:
    'AQkjxFtDLlgreeQKXUq4RM9c0HIjSvV8oJ5Ck1DwPrrYYUvBacg4yfarBWwl64tzRRGcXh-HSky7RnT5',
  RECAPTCHA_V2_SITE_KEY: '6LejgGQaAAAAAKu6EMSQW2514m8gmt-NHdyE4nxO',
  RECAPTCHA_SCRIPT_URL: 'https://www.google.com/recaptcha/api.js',
  defaultLangCode: 'en',
  rtl: ['en'],
  language: [
    { code: 'en', key: 'english', value: 'English' },
  ],
  gameStatsEndPoint:
    'https://1faiw2v9ca.execute-api.eu-west-2.amazonaws.com/api/',
  gameAPIEndPoint:
    'https://1faiw2v9ca.execute-api.eu-west-2.amazonaws.com/api/',
  gameApiKey: '92bb28e18dc67c3c08462f770d318a75',
  enableFirebase: false,
  gTagId: '',
  gTagUrl: 'https://www.googletagmanager.com/gtag/js?id=',
  gtmId: '',
  pageSizeOptions: [5, 10, 15, 20],
  maxparticipant: {
    single: 4096,
    double: 2048,
    roundRobin: 128,
    battleRoyale: 4096,
  },
  cardLink:"",
};
