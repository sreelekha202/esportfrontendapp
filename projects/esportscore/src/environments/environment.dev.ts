// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  buildConfig: 'dev',
  apiEndPoint:
    'https://aznf6hl673.execute-api.ap-southeast-1.amazonaws.com/core/',
  currentToken: 'DUID',
  facebookAPPID: '1578462858982905',
  googleAPPID:
    '183623623972-6aerbro3aj790tj37f23kfcohu2k3jb7.apps.googleusercontent.com',
  socketEndPoint: 'https://chat-core.dynasty-dev.com',
  cookieDomain: '.exaltaretech.com',
  eCommerceAPIEndPoint:
    'https://tuub60cp6h.execute-api.ap-southeast-1.amazonaws.com/dev/',
  firebase: {
    apiKey: 'AIzaSyB1mEgrCbtK1vvE4zR7VcOzXtcWs4ysvLw',
    authDomain: 'dynasty-test.firebaseapp.com',
    databaseURL: 'https://dynasty-test.firebaseio.com',
    projectId: 'dynasty-test',
    storageBucket: 'dynasty-test.appspot.com',
    messagingSenderId: '415594613035',
    appId: '1:415594613035:web:c0a1d274208530618add6e',
    measurementId: 'G-YBTGYLZ7RN',
  },
  articleS3BucketName: 'article',
  tournamentS3BucketName: 'tournament',
  iOSBundelName: 'stcmobileapp',
  iOSAppstoreURL: 'https://apps.apple.com/us/app/mesf/id1529620569',
  payfort: {
    access_code: '2sXOSK8ogBoMmRr8sUWB',
    merchant_identifier: '4527f1a6',
    url: 'https://sbcheckout.payfort.com/FortAPI/paymentPage',
  },
  paypalScopes:
    'profile email address https://uri.paypal.com/services/paypalattributes',
  paypalAccountMode: 'sandbox',
  paypal_client_id:
    'AQkjxFtDLlgreeQKXUq4RM9c0HIjSvV8oJ5Ck1DwPrrYYUvBacg4yfarBWwl64tzRRGcXh-HSky7RnT5',
  RECAPTCHA_V2_SITE_KEY: '6LejgGQaAAAAAKu6EMSQW2514m8gmt-NHdyE4nxO',
  RECAPTCHA_SCRIPT_URL: 'https://www.google.com/recaptcha/api.js',
  defaultLangCode: 'ar',
  rtl: ['ar'],
  language: [
    { code: 'en', key: 'english', value: 'English' },
    { code: 'ar', key: 'arabic', value: 'عربى' },
  ],
  gameStatsEndPoint:
    'https://1faiw2v9ca.execute-api.eu-west-2.amazonaws.com/api/',
  gameAPIEndPoint:
    'https://1faiw2v9ca.execute-api.eu-west-2.amazonaws.com/api/',
  gameApiKey: '92bb28e18dc67c3c08462f770d318a75',
  enableFirebase: false,
  gTagId: '',
  gTagUrl: 'https://www.googletagmanager.com/gtag/js?id=',
  gtmId: '',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
