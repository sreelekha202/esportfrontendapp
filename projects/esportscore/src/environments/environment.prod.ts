export const environment = {
  production: true,
  buildConfig: 'prod',
  // apiEndPoint: 'https://w5chwyhsr3.execute-api.ap-southeast-1.amazonaws.com/dev/',
  apiEndPoint:
    'https://aznf6hl673.execute-api.ap-southeast-1.amazonaws.com/core/',
  currentToken: 'DUID',
  facebookAPPID: '1578462858982905',
  googleAPPID:
    '183623623972-6aerbro3aj790tj37f23kfcohu2k3jb7.apps.googleusercontent.com',
  socketEndPoint: 'https://chat-core.dynasty-dev.com',
  cookieDomain: '.stcplay.gg',
  eCommerceAPIEndPoint:
    'https://tuub60cp6h.execute-api.ap-southeast-1.amazonaws.com/dev/',
  firebase: {
    apiKey: 'AIzaSyDHcxNG7GGrNLw1DXpocp4VQ6C5CdKG7Vo',
    authDomain: 'dynasty-prod.firebaseapp.com',
    projectId: 'dynasty-prod',
    storageBucket: 'dynasty-prod.appspot.com',
    messagingSenderId: '8205006144',
    appId: '1:8205006144:web:dd5ab3a9cdf982afa676a5',
    measurementId: 'G-34KJVGCSDP',
  },
  articleS3BucketName: 'article',
  tournamentS3BucketName: 'tournament',
  payfort: {
    access_code: '1uKKdrI3Hmb0vCyig9yM',
    merchant_identifier: 'ahRCaPtM',
    url: 'https://checkout.payfort.com/FortAPI/paymentPage',
  },
  iOSBundelName: 'stcmobileapp',
  iOSAppstoreURL: 'https://apps.apple.com/us/app/mesf/id1529620569',
  paypalScopes: 'profile',
  paypalAccountMode: 'live',
  paypal_client_id:
    'AQkjxFtDLlgreeQKXUq4RM9c0HIjSvV8oJ5Ck1DwPrrYYUvBacg4yfarBWwl64tzRRGcXh-HSky7RnT5',
  RECAPTCHA_V2_SITE_KEY: '6LejgGQaAAAAAKu6EMSQW2514m8gmt-NHdyE4nxO',
  RECAPTCHA_SCRIPT_URL: 'https://www.google.com/recaptcha/api.js',
  defaultLangCode: 'ar',
  rtl: ['ar'],
  language: [
    { code: 'en', key: 'english', value: 'English' },
    { code: 'ar', key: 'arabic', value: 'عربى' },
  ],
  gameStatsEndPoint:
    'https://1faiw2v9ca.execute-api.eu-west-2.amazonaws.com/api/',
  gameAPIEndPoint:
    'https://1faiw2v9ca.execute-api.eu-west-2.amazonaws.com/api/',
  gameApiKey: '92bb28e18dc67c3c08462f770d318a75',
  enableFirebase: false,
  gTagId: '',
  gTagUrl: 'https://www.googletagmanager.com/gtag/js?id=',
  gtmId: 'xyz',
};
