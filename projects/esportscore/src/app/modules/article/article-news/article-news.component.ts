import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';

import { ArticleApiService, OptionService } from '../../../core/service';
import { IPagination } from '../../../shared/models';
import { IUser, EsportsUserService } from 'esports';

@Component({
  selector: 'app-article-news',
  templateUrl: './article-news.component.html',
  styleUrls: ['./article-news.component.scss'],
})
export class ArticleNewsComponent implements OnInit, OnDestroy {
  user: IUser;
  articles: any = [];
  paginationData = {
    page: 1,
    limit: 20,
    sort: { createdDate: -1 },
  };
  page: IPagination;

  categoryList = [];
  categoryId;
  userSubscription: Subscription;

  constructor(
    private articleService: ArticleApiService,
    private userService: EsportsUserService,
    private optionService: OptionService
  ) {}

  ngOnInit(): void {
    this.getCurrentUserDetails();
    this.fetchOptions();
  }

  ngOnDestroy() {
    if (this.userSubscription) {
      this.userSubscription?.unsubscribe();
    }
  }

  pageChanged(page): void {
    this.paginationData.page = page;
    this.getArticleNews(this.categoryId);
  }

  fetchOptions = async () => {
    const option = await Promise.all([this.optionService.fetchAllCategories()]);
    this.categoryList = option[0]?.data;

    for (const iterator of this.categoryList) {
      if (iterator.name == 'News') {
        this.categoryId = iterator._id;
      }
    }
    this.getArticleNews(this.categoryId);
  };

  getArticleNews = async (categoryId) => {
    try {
      const query = JSON.stringify({
        articleStatus: 'publish',
        category: categoryId,
      });
      const pagination = JSON.stringify(this.paginationData);
      const prefernce = JSON.stringify({
        prefernce: this.user?.preference?.game
          ? this.user?.preference?.game.map((item) => {
              return { gameDetails: item };
            })
          : '',
      });
      this.articleService
        .getLatestArticle({ query, pagination, preference: prefernce })
        .subscribe(
          (res: any) => {
            this.articles = res['data']['docs'];
          },
          (err) => {}
        );
    } catch (error) {}
  };

  getCurrentUserDetails() {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.user = data;
      }
    });
  }
}
