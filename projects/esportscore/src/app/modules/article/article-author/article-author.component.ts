import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

import { JumbotronComponentData } from '../../../core/jumbotron/jumbotron.component';
import { ActivatedRoute } from '@angular/router';
import { ArticleApiService } from '../../../core/service';
import { IPagination } from '../../../shared/models';

@Component({
  selector: 'app-article-author',
  templateUrl: './article-author.component.html',
  styleUrls: ['./article-author.component.scss'],
})
export class ArticleAuthorComponent implements OnInit {
  authorId;
  articles = [];
  hottestPost: any = {};
  articleData;
  jumbotron: JumbotronComponentData = {
    _id: '',
    ribbon: '',
    slug: '',
    dateTitle: '',
    title: '',
    isBtnHidden: true,
    btn: '',
    user: {
      img: '',
      name: '',
    },
  };
  paginationData = {
    page: 1,
    limit: 20,
    sort: '-views',
  };
  page: IPagination;
  showLoader = true;
  authorName = '';
  constructor(
    private location: Location,
    private activatedRoute: ActivatedRoute,
    private articleService: ArticleApiService
  ) {}

  ngOnInit(): void {
    this.authorId = this.activatedRoute.snapshot.params['id'];
    this.fetchArticles();
  }

  goBack(): void {
    this.location.back();
  }

  getHottestPost() {
    this.articleService.getHottestPost().subscribe(
      (res) => {
        this.hottestPost = res['data'][0];
        const createdDate = new Date(this.hottestPost['createdDate']);
        const date = createdDate.getDate();
        const month = createdDate.toLocaleString('default', { month: 'short' });
        this.jumbotron = {
          _id: this.hottestPost._id,
          slug: this.hottestPost.slug,
          ribbon: 'Hottest post',
          dateTitle: `${this.hottestPost.game} | ${date} ${month}`,
          title: this.hottestPost.title,
          isBtnHidden: true,
          btn: 'read post',
          user: {
            img: this.hottestPost['authorDetails']['profilePicture']
              ? this.hottestPost['authorDetails']['profilePicture']
              : 'assets/images/Profile/stc_avatar.png',
            name: this.hottestPost['authorDetails']['fullName'],
          },
        };
      },
      (err) => {}
    );
  }

  pageChanged(page): void {
    this.paginationData.page = page;
    this.fetchArticles();
  }

  fetchArticles = async () => {
    try {
      const pagination = JSON.stringify(this.paginationData);
      const query = JSON.stringify({
        author: this.authorId,
        articleStatus: 'publish',
      });
      this.articleService.getPaginatedArticles({ pagination, query }).subscribe(
        (res: any) => {
          if (res && res['authorDetails'] && res['authorDetails']['fullName']) {
            this.authorName = res['authorDetails']['fullName'];
          }
          this.showLoader = false;
          this.articleData = res['data'];
          this.articles = res['data']['docs'];
          this.page = {
            totalItems: res?.data?.totalDocs,
            itemsPerPage: res?.data?.limit,
            maxSize: 5,
          };
          this.getHottestPost();
        },
        (err) => {
          this.showLoader = false;
          console.error(err);
        }
      );
    } catch (error) {
      this.showLoader = false;
      console.error(error);
    }
  };
}
