import { SwiperConfigInterface } from 'ngx-swiper-wrapper';
import { TranslateService } from '@ngx-translate/core';
import { Component, OnDestroy, OnInit } from '@angular/core';

import {
  ArticleApiService,
  HomeService,
  UtilsService,
  VideoLibraryService,
  GameService,
  OptionService,
} from '../../../core/service';
import { JumbotronComponentData } from '../../../core/jumbotron/jumbotron.component';
import { AppHtmlRoutes } from '../../../app-routing.model';
import { IUser, EsportsUserService, EsportsLanguageService, EsportsToastService } from 'esports';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-article-main',
  templateUrl: './article-main.component.html',
  styleUrls: ['./article-main.component.scss'],
})
export class ArticleMainComponent implements OnInit, OnDestroy {
  user: IUser;
  AppHtmlRoutes = AppHtmlRoutes;
  jumbotron: JumbotronComponentData = {
    _id: '',
    slug: '',
    ribbon: '',
    dateTitle: '',
    title: '',
    isBtnHidden: true,
    btn: 'read post',
    user: {
      img: '',
      name: '',
    },
  };

  videoLibrary: Array<{}> = [];
  activeVideo = null;
  trendingAuthors = [];
  gameList = [];
  latestArticle = [];
  showLoader = true;
  trendingNews = [];
  swiperConfig: SwiperConfigInterface = {
    spaceBetween: 30,
    navigation: true,
  };
  latestNews = [];
  latestArticleData = [];
  trendingData = [];
  hottestData = [];
  categoryList = [];
  categoryId;

  userSubscription: Subscription;

  constructor(
    private videoLibraryService: VideoLibraryService,
    private eSportsToastService: EsportsToastService,
    public utilsService: UtilsService,
    private articleService: ArticleApiService,
    public language: EsportsLanguageService,
    public translateService: TranslateService,
    private homeService: HomeService,
    private gameService: GameService,
    private userService: EsportsUserService,
    private optionService: OptionService,
    public languageService: EsportsLanguageService
  ) {}

  ngOnInit(): void {
    this.getCurrentUserDetails();
    this.getArticleList();
    // this.getHottestPost();
    this.fetchVideoLibrary();
    // this.loadArticleMainPageData();
    this.fetchOptions();
    // this.getTrendingPost();
    // this.getRecentPost();
    this.getGames();
  }

  ngOnDestroy() {
    if (this.userSubscription) {
      this.userSubscription?.unsubscribe();
    }
  }

  fetchVideoLibrary = async () => {
    try {
      const pagination = {
        page: 1,
        limit: 5,
        sort: '-updatedOn',
        projection: [
          '_id',
          'slug',
          'title',
          'description',
          'youtubeUrl',
          'thumbnailUrl',
          'createdBy',
        ],
      };
      const query = `?pagination=${encodeURIComponent(
        JSON.stringify(pagination)
      )}`;

      const response = await this.videoLibraryService.fetchVideoLibrary(query);
      this.videoLibrary = await Promise.all(
        response?.data?.docs.map(async (item) => {
          return {
            ...item,
            youtubeUrl: await this.utilsService.getEmbbedUrl(item.youtubeUrl),
          };
        })
      );
      this.activeVideo = this.videoLibrary.length ? this.videoLibrary[0] : null;
    } catch (error) {
      this.eSportsToastService.showError(error?.error?.message || error?.message);
    }
  };

  fetchOptions = async () => {
    const option = await Promise.all([this.optionService.fetchAllCategories()]);
    this.categoryList = option[0]?.data;

    for (const iterator of this.categoryList) {
      if (iterator.name == 'News') {
        this.categoryId = iterator._id;
      }
    }
    this.getArticleNews(this.categoryId);
  };

  getArticleNews(categoryId) {
    const query = JSON.stringify({
      articleStatus: 'publish',
      category: categoryId,
    });
    const pagination = JSON.stringify({ limit: 7, sort: { createdDate: -1 } });
    const prefernce = JSON.stringify({
      prefernce: this.user?.preference?.game
        ? this.user?.preference?.game.map((item) => {
            return { gameDetails: item };
          })
        : '',
    });
    this.articleService
      .getLatestArticle({ query, pagination, preference: prefernce })
      .subscribe(
        (res: any) => {
          this.latestNews = res.data.docs;
        },
        (err) => {}
      );
  }

  getGames = async () => {
    try {
      const { data } = await this.gameService.getGames().toPromise();
      this.gameList = data || [];
    } catch (error) {
      this.showLoader = false;
      this.eSportsToastService.showError(error?.error?.message || error?.message);
    }
  };

  getRecentPost() {
    const pagination = JSON.stringify({ limit: 8, sort: { createdDate: -1 } });
    const query = JSON.stringify({
      articleStatus: 'publish',
    });
    const perfernce = JSON.stringify({
      prefernce: this.user?.preference?.game
        ? this.user?.preference?.game.map((item) => {
            return { gameDetails: item };
          })
        : '',
    });
    this.articleService
      .getLatestArticle({
        pagination,
        query,
        preference: perfernce,
      })
      .subscribe(
        (res: any) => {
          this.latestArticle = res.data.docs;
          this.showLoader = false;
        },
        (err) => {
          this.showLoader = false;
        }
      );
  }

  getArticleList() {
    this.articleService.getArticleList().subscribe(
      (res: any) => {
        this.latestArticleData = res.data || [];
        for (let index = 0; index < this.latestArticleData.length; index++) {
          const element = this.latestArticleData[index];
          if (index == 1) {
            this.hottestData.push(element);
          } else {
            this.trendingData.push(element);
          }
        }
        this.showLoader = false;
      },
      (err) => {
        this.showLoader = false;
      }
    );
  }
  getCurrentUserDetails() {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.user = data;
      }
      this.getRecentPost();
    });
  }
}
