// import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { ShareModule } from 'ngx-sharebuttons';
import { NgModule } from '@angular/core';

import { ArticleAuthorComponent } from './article-author/article-author.component';
import { PaginationModule, PaginationConfig } from 'ngx-bootstrap/pagination';
import { ArticleMoreComponent } from './article-more/article-more.component';
import { ArticlePostComponent } from './article-post/article-post.component';
import { ArticleMainComponent } from './article-main/article-main.component';
import { ArticleGameComponent } from './article-game/article-game.component';
import { ArticleNewsComponent } from './article-news/article-news.component';
import { SharedModule } from '../../shared/modules/shared.module';
import { ArticleRoutingModule } from './article-routing.module';
import { CoreModule } from '../../core/core.module';
import { RouterBackModule } from '../../shared/directives/router-back.module';

@NgModule({
  declarations: [
    // CreateArticleComponent,
    ArticlePostComponent,
    ArticleMoreComponent,
    ArticleAuthorComponent,
    ArticleMainComponent,
    ArticleGameComponent,
    ArticleNewsComponent,
  ],
  imports: [
    SharedModule,
    // CKEditorModule,
    ArticleRoutingModule,
    CoreModule,
    ShareModule,
    PaginationModule.forRoot(),
    RouterBackModule,
  ],
  providers: [{ provide: PaginationConfig }],
})
export class ArticleModule {}
