import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { ComingSoonComponent } from './coming-soon.component';

const routes: Routes = [{ path: '', component: ComingSoonComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ComingSoonRoutingModule {}
