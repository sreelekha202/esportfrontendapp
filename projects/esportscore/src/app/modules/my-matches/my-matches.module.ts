import { NgModule } from '@angular/core';

import { SharedModule } from '../../shared/modules/shared.module';
import { CoreModule } from '../../core/core.module';

import { MyMatchesRoutingModule } from './my-matches-routing.module';
import { MyMatchesComponent } from './my-matches.component';
import { MatchCardComponent } from './components/match-card/match-card.component';
import { ChallengesCardComponent } from './components/challenges-card/challenges-card.component';
import { ScorePopupComponent } from "./components/score-popup/score-popup.component";

@NgModule({
  declarations: [MyMatchesComponent, MatchCardComponent, ChallengesCardComponent, ScorePopupComponent],
  imports: [SharedModule, CoreModule, MyMatchesRoutingModule],
  exports: [],
})
export class MyMatchesModule {}
