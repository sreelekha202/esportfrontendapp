import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'score-popup',
  templateUrl: './score-popup.component.html',
  styleUrls: ['./score-popup.component.scss'],
})
export class ScorePopupComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<ScorePopupComponent>
  ) {}

  onClose(): void {
    this.dialogRef.close();
  }

  exitFromScoreCard(data) {
    if (!data?.isOpenScoreCard) {
      this.dialogRef.close();
    }
  }

  ngOnInit(): void {}
}
