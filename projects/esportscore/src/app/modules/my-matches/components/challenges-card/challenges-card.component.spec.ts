import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChallengesCardComponent } from './challenges-card.component';

describe('ChallengesCardComponent', () => {
  let component: ChallengesCardComponent;
  let fixture: ComponentFixture<ChallengesCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChallengesCardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChallengesCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
