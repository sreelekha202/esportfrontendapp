import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { AppHtmlRoutes } from '../../../../app-routing.model';
import { MatDialog } from '@angular/material/dialog';
import { ScorePopupComponent } from '../score-popup/score-popup.component';
interface MatchCard {
  title: string;
  opponentName: string;
  gameAccount: string;
  gameName: string;
  image: string;
  tournamentId: string;
  tournamentSlug: string;
  tournamentName: string;
  matchId: string;
  match: object;
  participantId: string;
  isAdmin: boolean;
  ip: string;
  password: string;
}

import {
  EsportsChatService
} from 'esports';

@Component({
  selector: 'app-match-card',
  templateUrl: './match-card.component.html',
  styleUrls: ['./match-card.component.scss'],
})
export class MatchCardComponent implements OnInit {
  @Input() data: MatchCard;
  windowposition: String = 'chat_window chat_window_right';

  constructor(
    private router: Router,
    private matDialog: MatDialog,
    public chatService: EsportsChatService
  ) { }

  ngOnInit(): void { }

  genrateReport() {
    if (this.data?.tournamentId && this.data?.matchId) {
      this.router.navigate([
        `${AppHtmlRoutes.tournament}/report/${this.data?.matchId}/${this.data?.tournamentId}`,
      ]);
    }
  }
  viewMatch() {
    if (this.data?.matchId) {
      this.router.navigate([`tournament/${this.data?.matchId}/match-view`], {
        queryParams: { matchId: this.data?.matchId },
      });
    }
  }
  openScoreCard = async () => {
    try {
      const data = this.data;
      const response = await this.matDialog
        .open(ScorePopupComponent, {
          width: '600px',
          data,
        })
        .afterClosed()
        .toPromise();
    } catch (error) { }
  };

  toggleChat(matchid) {
    this.chatService.getMatchInformation(matchid).subscribe((result: any) => {
      const mtchdetails = {
        _id: result.matchdetails._id,
        picture: result.matchdetails.tournamentId.gameDetail.logo,
        name: result.matchdetails.tournamentId.name,
        matchno: result.matchdetails.matchNo,
        participants:
          result.matchdetails.teamA.inGamerUserId +
          ',' +
          result.matchdetails.teamB.inGamerUserId,
        tournamentId: result.matchdetails.tournamentId._id,
        teamAId: result.matchdetails.teamA.userId,
        teamBId: result.matchdetails.teamB.userId,
        teamAGameId: result.matchdetails.teamA.inGamerUserId,
        teamBGameId: result.matchdetails.teamB.inGamerUserId,
        type: 'tournament',
      };
      this.chatService.setWindowPos(this.windowposition);
      let firstChat = this.chatService.getChatStatus();
      if (firstChat == true) {
        this.chatService.setChatStatus(false);
        this.chatService?.disconnectMatch(mtchdetails._id, 'tournament');
        this.chatService.setTextButtonStatus(false);
      } else {
        this.chatService.setCurrentMatch(mtchdetails);
        this.chatService.setTypeOfChat('tournament');
        this.chatService.setChatStatus(true);
        this.chatService.setTextButtonStatus(true);
      }
    });
  }

}
