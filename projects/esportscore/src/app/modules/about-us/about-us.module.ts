import { NgModule } from '@angular/core';

import { AboutUsRoutingModule } from './about-us-routing.module';
import { CoreModule } from '../../core/core.module';
import { SharedModule } from '../../shared/modules/shared.module';
import { AboutUsComponent } from './about-us.component';

@NgModule({
  declarations: [AboutUsComponent],
  imports: [AboutUsRoutingModule, CoreModule, SharedModule],
  exports: [],
})
export class AboutUsModule {}
