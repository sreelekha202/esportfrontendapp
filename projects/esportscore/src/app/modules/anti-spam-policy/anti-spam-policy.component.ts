import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { EsportsLanguageService } from 'esports';

@Component({
  selector: 'app-anti-spam-policy',
  templateUrl: './anti-spam-policy.component.html',
  styleUrls: ['./anti-spam-policy.component.scss'],
})
export class AntiSpamPolicyComponent implements OnInit {
  constructor(
    public translate: TranslateService,
    public language: EsportsLanguageService
  ) {}

  ngOnInit(): void {}
}
