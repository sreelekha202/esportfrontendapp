import { NgModule } from '@angular/core';

import { SharedModule } from '../../shared/modules/shared.module';
import { CoreModule } from '../../core/core.module';

import { AntiSpamPolicyRoutingModule } from './anti-spam-policy-routing.module';
import { AntiSpamPolicyComponent } from './anti-spam-policy.component';

@NgModule({
  declarations: [AntiSpamPolicyComponent],
  imports: [SharedModule, CoreModule, AntiSpamPolicyRoutingModule],
  exports: [],
})
export class AntiSpamPolicyModule {}
