import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { CodeOfConductComponent } from './code-of-conduct.component';

const routes: Routes = [{ path: '', component: CodeOfConductComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CodeOfConductRoutingModule {}
