import { SwiperConfigInterface } from 'ngx-swiper-wrapper';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {
  GameService,
} from '../../core/service';
import { EsportsLeaderboardService, EsportsUserService, EsportsLanguageService, EsportsConstantsService } from 'esports';
import { AppHtmlRoutes } from '../../app-routing.model';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-leader-board',
  templateUrl: './leader-board.component.html',
  styleUrls: ['./leader-board.component.scss'],
})
export class LeaderBoardComponent implements OnInit {
  public config: SwiperConfigInterface = {
    navigation: true,
    pagination: false,
    autoHeight: true,
  };
  apiEndPoint = environment.apiEndPoint;
  gameList = [];
  stateList = [];
  boardCardsData = [
    {
      image: 'assets/images/Leaderboard/leader-board-card-image.png',
      place: 1,
      name: 'Killermonk',
      myr: '3,000',
      games: 102,
    },
    {
      image: 'assets/images/Leaderboard/leader-board-card-image.png',
      place: 2,
      name: 'Cormac Sabastian',
      myr: '3,000',
      games: 102,
    },
    {
      image: 'assets/images/Leaderboard/leader-board-card-image.png',
      place: 3,
      name: 'Weston Dawn',
      myr: '3,000',
      games: 102,
    },
  ];
  leaderBoardRows = [
    {
      rank: 1,
      img: '/assets/images/AboutUs/company.png',
      name: 'Tony Matt',
      region: 'perlis',
      game: 'Rocket League',
      trophies: {
        first: 8,
        second: 8,
        third: 6,
      },
      points: 1993,
    },
    {
      rank: 2,
      img: '/assets/images/AboutUs/company.png',
      name: 'weston DAWN',
      region: 'kayang',
      game: 'Rocket League',
      trophies: {
        first: 5,
        second: 3,
        third: 6,
      },
      points: 2312,
    },
    {
      rank: 3,
      img: '/assets/images/AboutUs/company.png',
      name: 'Cormac SABISTIAN',
      region: 'hitam',
      game: 'Rocket League',
      trophies: {
        first: 9,
        second: 8,
        third: 3,
      },
      points: 254,
    },
    {
      rank: 4,
      img: '/assets/images/AboutUs/company.png',
      name: 'Tony Matt',
      region: 'pendang',
      game: 'Rocket League',
      trophies: {
        first: 4,
        second: 6,
        third: 2,
      },
      points: 2153,
    },
    {
      rank: 5,
      img: '/assets/images/AboutUs/company.png',
      name: 'weston DAWN',
      region: 'marang',
      game: 'Rocket League',
      trophies: {
        first: 1,
        second: 5,
        third: 8,
      },
      points: 2205,
    },
    {
      rank: 6,
      img: '/assets/images/AboutUs/company.png',
      name: 'Cormac SABISTIAN',
      region: 'pekan',
      game: 'Rocket League',
      trophies: {
        first: 5,
        second: 2,
        third: 2,
      },
      points: 938,
    },
    {
      rank: 7,
      img: '/assets/images/AboutUs/company.png',
      name: 'Tony Matt',
      region: 'mersing',
      game: 'Rocket League',
      trophies: {
        first: 8,
        second: 4,
        third: 6,
      },
      points: 1690,
    },
    {
      rank: 8,
      img: '/assets/images/AboutUs/company.png',
      name: 'weston DAWN',
      region: 'bandar',
      game: 'Rocket League',
      trophies: {
        first: 8,
        second: 2,
        third: 1,
      },
      points: 2013,
    },
    {
      rank: 9,
      img: '/assets/images/AboutUs/company.png',
      name: 'Cormac SABISTIAN',
      region: 'skudai',
      game: 'Rocket League',
      trophies: {
        first: 4,
        second: 1,
        third: 2,
      },
      points: 843,
    },
    {
      rank: 10,
      img: '/assets/images/AboutUs/company.png',
      name: 'Tony Matt',
      region: 'batu',
      game: 'Rocket League',
      trophies: {
        first: 6,
        second: 1,
        third: 4,
      },
      points: 1039,
    },
    {
      rank: 11,
      img: '/assets/images/AboutUs/company.png',
      name: 'Tony Matt',
      region: 'perlis',
      game: 'Rocket League',
      trophies: {
        first: 8,
        second: 8,
        third: 6,
      },
      points: 1993,
    },
    {
      rank: 12,
      img: '/assets/images/AboutUs/company.png',
      name: 'weston DAWN',
      region: 'kayang',
      game: 'Rocket League',
      trophies: {
        first: 5,
        second: 3,
        third: 6,
      },
      points: 2312,
    },
    {
      rank: 13,
      img: '/assets/images/AboutUs/company.png',
      name: 'Cormac SABISTIAN',
      region: 'hitam',
      game: 'Rocket League',
      trophies: {
        first: 9,
        second: 8,
        third: 3,
      },
      points: 254,
    },
    {
      rank: 14,
      img: '/assets/images/AboutUs/company.png',
      name: 'Tony Matt',
      region: 'pendang',
      game: 'Rocket League',
      trophies: {
        first: 4,
        second: 6,
        third: 2,
      },
      points: 2153,
    },
    {
      rank: 15,
      img: '/assets/images/AboutUs/company.png',
      name: 'weston DAWN',
      region: 'marang',
      game: 'Rocket League',
      trophies: {
        first: 1,
        second: 5,
        third: 8,
      },
      points: 2205,
    },
    {
      rank: 16,
      img: '/assets/images/AboutUs/company.png',
      name: 'Cormac SABISTIAN',
      region: 'pekan',
      game: 'Rocket League',
      trophies: {
        first: 5,
        second: 2,
        third: 2,
      },
      points: 938,
    },
    {
      rank: 17,
      img: '/assets/images/AboutUs/company.png',
      name: 'Tony Matt',
      region: 'mersing',
      game: 'Rocket League',
      trophies: {
        first: 8,
        second: 4,
        third: 6,
      },
      points: 1690,
    },
    {
      rank: 18,
      img: '/assets/images/AboutUs/company.png',
      name: 'weston DAWN',
      region: 'bandar',
      game: 'Rocket League',
      trophies: {
        first: 8,
        second: 2,
        third: 1,
      },
      points: 2013,
    },
    {
      rank: 19,
      img: '/assets/images/AboutUs/company.png',
      name: 'Cormac SABISTIAN',
      region: 'skudai',
      game: 'Rocket League',
      trophies: {
        first: 4,
        second: 1,
        third: 2,
      },
      points: 843,
    },
    {
      rank: 20,
      img: '/assets/images/AboutUs/company.png',
      name: 'Tony Matt',
      region: 'batu',
      game: 'Rocket League',
      trophies: {
        first: 6,
        second: 1,
        third: 4,
      },
      points: 1039,
    },
    {
      rank: 21,
      img: '/assets/images/AboutUs/company.png',
      name: 'Tony Matt',
      region: 'perlis',
      game: 'Rocket League',
      trophies: {
        first: 8,
        second: 8,
        third: 6,
      },
      points: 1993,
    },
    {
      rank: 22,
      img: '/assets/images/AboutUs/company.png',
      name: 'weston DAWN',
      region: 'kayang',
      game: 'Rocket League',
      trophies: {
        first: 5,
        second: 3,
        third: 6,
      },
      points: 2312,
    },
    {
      rank: 23,
      img: '/assets/images/AboutUs/company.png',
      name: 'Cormac SABISTIAN',
      region: 'hitam',
      game: 'Rocket League',
      trophies: {
        first: 9,
        second: 8,
        third: 3,
      },
      points: 254,
    },
    {
      rank: 24,
      img: '/assets/images/AboutUs/company.png',
      name: 'Tony Matt',
      region: 'pendang',
      game: 'Rocket League',
      trophies: {
        first: 4,
        second: 6,
        third: 2,
      },
      points: 2153,
    },
    {
      rank: 25,
      img: '/assets/images/AboutUs/company.png',
      name: 'weston DAWN',
      region: 'marang',
      game: 'Rocket League',
      trophies: {
        first: 1,
        second: 5,
        third: 8,
      },
      points: 2205,
    },
    {
      rank: 26,
      img: '/assets/images/AboutUs/company.png',
      name: 'Cormac SABISTIAN',
      region: 'pekan',
      game: 'Rocket League',
      trophies: {
        first: 5,
        second: 2,
        third: 2,
      },
      points: 938,
    },
    {
      rank: 27,
      img: '/assets/images/AboutUs/company.png',
      name: 'Tony Matt',
      region: 'mersing',
      game: 'Rocket League',
      trophies: {
        first: 8,
        second: 4,
        third: 6,
      },
      points: 1690,
    },
  ];
  leaderBoardColumns = [
    { name: 'RANK' },
    { name: 'Player Name' },
    { name: 'REGION' },
    { name: 'GAME' },
    { name: 'TROPHIES' },
    { name: 'POINTS' },
  ];

  selectedRegions = '';
  selectedGames = 'all';

  selectedGame: any = { all: true };
  paginationData = {
    page: 1,
    limit: 100,
    sort: 'startDate',
  };
  gameId = 'all';
  showLoader = true;
  leaderboardData: any = [];
  currentLang = this.ConstantsService?.defaultLangCode;

  constructor(
    private leaderboardService: EsportsLeaderboardService,
    private gameService: GameService,
    private userService: EsportsUserService,
    private router: Router,
    public language: EsportsLanguageService,
    public ConstantsService: EsportsConstantsService
  ) {}

  ngOnInit(): void {
    this.getGames();
    // 17: Bahrain, 117: kuwait,
    this.getStates([191]);
    // this.filterLeaderboard();
    this.language.language.subscribe((lang) => {
      this.currentLang = lang;
    });
  }

  getGames() {
    const encodeUrl = `?query=${encodeURIComponent(
      JSON.stringify({ isTournamentAllowed: true })
    )}`;
    this.gameService.getAllGames(encodeUrl).subscribe(
      (res) => (this.gameList = res.data),
      (err) => {}
    );
  }

  getStates(countryIds) {
    this.stateList = [];
    this.userService.getStates().subscribe((data) => {
      this.stateList = data.states.filter((el) =>
        countryIds.includes(parseInt(el.country_id))
      );

      this.stateList.unshift({
        id: 117,
        sortname: 'KW',
        name: 'Kuwait',
        phoneCode: 965,
        en: 'Kuwait',
        ar: 'دولة الكويت',
      });
      this.stateList.unshift({
        id: 17,
        sortname: 'BH',
        name: 'Bahrain',
        phoneCode: 973,
        en: 'Bahrain',
        ar: 'مملكة البحرين',
      });
      this.filterLeaderboard();
    });
  }

  filterLeaderboard() {
    this.paginationData.page = 1;

    this.showLoader = true;
    const params = {
      page: this.paginationData.page,
      limit: this.paginationData.limit,
      gameId: this.selectedGames,
      state: null,
      country: null,
    };
    if (this.selectedRegions == 'Kuwait' || this.selectedRegions == 'Bahrain') {
      params.country = this.selectedRegions;
    } else {
      params.state = this.selectedRegions;
    }
    this.leaderboardService
      .getGameLeaderboard(this.apiEndPoint, params)
      .subscribe(
        (res: any) => {
          this.showLoader = false;
          this.leaderboardData = [];
          // this.page = {
          //   totalItems: res?.data?.totalDocs,
          //   itemsPerPage: res?.data?.limit,
          //   maxSize: 5,
          // };
          if (res.data.docs) {
            let count = (params.page - 1) * params.limit;
            let userData: any = {};
            for (const data of res.data.docs) {
              if (data.user.length > 0) {
                userData.id = data._id.user;
                userData.rank = count + 1;
                count++;
                userData.img =
                  data.user[0].profilePicture == ''
                    ? './assets/images/Profile/stc_avatar.png'
                    : data.user[0].profilePicture;
                userData.name = data.user[0].username
                  ? data.user[0].username
                  : data.user[0].fullName;
                userData.region = data.user[0].state ? data.user[0].state : '';
                userData.trophies = {};
                userData.trophies.first = data.firstPosition;
                userData.trophies.second = data.secondPosition;
                userData.trophies.third = data.thirdPosition;
                userData.points = data.points;
                userData.amount = data.amountPrice;
                userData.gamesCount = data.gamesCount;
                this.leaderboardData.push({ ...userData });
                userData = {};
              }
            }
          }
        },
        (err) => {
          this.showLoader = false;
        }
      );
  }

  changeTab(value, type) {
    switch (type) {
      case 'regions': {
        this.selectedRegions = value;
        this.filterLeaderboard();
        break;
      }
      case 'games': {
        this.selectedGames = value;
        this.filterLeaderboard();
        break;
      }
    }
  }

  onRowClick(event) {
    if (event.type === 'click') {
      this.router.navigate([AppHtmlRoutes.leaderboard, event.row.id]);
    }
  }
}
