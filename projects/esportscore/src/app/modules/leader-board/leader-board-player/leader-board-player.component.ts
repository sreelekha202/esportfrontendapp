import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Chart } from 'chart.js';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthServices } from '../../../core/service';
import { IUser, EsportsUserService, EsportsLeaderboardService, EsportsConstantsService, EsportsLanguageService, EsportsToastService } from 'esports';
import { Subscription } from 'rxjs';

import { environment } from '../../../../environments/environment';

const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;


@Component({
  selector: 'app-leader-board-player',
  templateUrl: './leader-board-player.component.html',
  styleUrls: ['./leader-board-player.component.scss'],
})
export class LeaderBoardPlayerComponent implements OnInit, OnDestroy {
  @ViewChild('lineChart', { static: true }) private chartRef;
  @ViewChild('doughnutRefA', { static: true }) private doughnutRefA;
  @ViewChild('doughnutRefB', { static: true }) private doughnutRefB;

  apiEndPoint = environment.apiEndPoint;
  gameStreak = 'allTournaments';
  days: any = [];
  wins: any = [];
  userId: any = null;
  showLoader = true;
  leaderboardData: any = {};
  streakGraphData: any = {};
  selectedDate: any = 'month';
  monthMapping = {
    0: 'LEADERBOARD.PLAYER_DETAILS.MONTHS.JANUARY',
    1: 'LEADERBOARD.PLAYER_DETAILS.MONTHS.FEBRUARY',
    2: 'LEADERBOARD.PLAYER_DETAILS.MONTHS.MARCH',
    3: 'LEADERBOARD.PLAYER_DETAILS.MONTHS.APRIL',
    4: 'LEADERBOARD.PLAYER_DETAILS.MONTHS.MAY',
    5: 'LEADERBOARD.PLAYER_DETAILS.MONTHS.JUNE',
    6: 'LEADERBOARD.PLAYER_DETAILS.MONTHS.JULY',
    7: 'LEADERBOARD.PLAYER_DETAILS.MONTHS.AUGUST',
    8: 'LEADERBOARD.PLAYER_DETAILS.MONTHS.SEPTEMBER',
    9: 'LEADERBOARD.PLAYER_DETAILS.MONTHS.OCTOBER',
    10: 'LEADERBOARD.PLAYER_DETAILS.MONTHS.NOVEMBER',
    11: 'LEADERBOARD.PLAYER_DETAILS.MONTHS.DECEMBER',
  };
  user: IUser;
  followStatus: any = 'follow';
  chart: any;
  chartFlag = false;
  isMyStats = false;
  userSubscription: Subscription;
  stateList = [];
  currentLang = this.ConstantsService?.defaultLangCode;
  constructor(
    private activatedRoute: ActivatedRoute,
    private leaderboardService: EsportsLeaderboardService,
    private translateService: TranslateService,
    public languageService: EsportsLanguageService,
    public ConstantsService: EsportsConstantsService,
    private userService: EsportsUserService,
    private authService: AuthServices,
    private router: Router,
    public eSportsToastService: EsportsToastService
  ) {}

  ngOnInit(): void {
    this.getStates();
    this.languageService.language.subscribe((lang) => {
      this.currentLang = lang;
    });
    if (this.activatedRoute.snapshot.params.id) {
      this.userId = this.activatedRoute.snapshot.params.id;
      this.isMyStats = false;
    }
    this.getCurrentUserDetails();
  }

  ngOnDestroy() {
    this.userSubscription?.unsubscribe();
  }

  // Get user details if user is logged in
  getCurrentUserDetails() {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.user = data;
        if (!this.userId) {
          this.userId = this.user._id;
          this.isMyStats = true;
        }
        this.checkFollowStatus();
      }
      if (this.userId) {
        this.filterByUser();
      }
    });
  }

  // check if the logged in user is following the users of stats
  checkFollowStatus() {
    if (this.user) {
      if (this.user._id == this.userId) {
        this.followStatus = 'hide';
      } else {
        this.leaderboardService
          .checkFollowStatus(this.apiEndPoint, this.userId)
          .subscribe(
            (res: any) => {
              if (res.data[0]?.status) {
                this.followStatus = 'unfollow';
              } else {
                this.followStatus = 'follow';
              }  
            },
            (err) => {
              this.showLoader = false;
            }
          );
      }
    } else {
      this.followStatus = 'follow';
    }
  }

  // Follow and Unfollow a user
  followUser() {
    if (this.user) {
      if (this.followStatus == 'follow') {
        this.leaderboardService
          .followUser(this.apiEndPoint, this.userId)
          .subscribe(
            (res: any) => {
              this.followStatus = 'unfollow';
              this.userService.refreshCurrentUser(API, TOKEN);
            },
            (err) => {
              this.showLoader = false;
            }
        );
      } else if (this.followStatus == 'unfollow') {
        this.leaderboardService
          .unfollowUser(this.apiEndPoint, this.userId)
          .subscribe(
            (res: any) => {
              this.followStatus = 'follow';
              this.userService.refreshCurrentUser(API, TOKEN);
            },
            (err) => {
              this.showLoader = false;
            }
          );
      }
    } else {
      this.authService.redirectUrl = `/leaderboard/${this.userId}`;
      this.eSportsToastService.showError(
        this.translateService.instant('TOURNAMENT.TOURNAMENT.PLEASE_LOGIN')
      );
      // Navigate to the login page with extras
      this.router.navigate(['/user/phone-login']);
    }
  }

  filterByUser(
    month = new Date().getMonth() + 1,
    year = new Date().getFullYear()
  ) {
    this.showLoader = true;
    const params = {
      userId: this.userId,
      month,
      year,
    };
    this.leaderboardService
      .getUserLeaderboard(this.apiEndPoint, params)
      .subscribe(
        (res: any) => {
          this.showLoader = false;
          this.leaderboardData = { ...res.data };
          const today: any = new Date();
          if (res.data) {
            this.leaderboardData.img =
              res.data.user[0].profilePicture == ''
                ? './assets/images/Profile/stc_avatar.png'
                : res.data.user[0].profilePicture;
            this.leaderboardData.name = res.data.user[0].username
              ? res.data.user[0].username
              : res.data.user[0].fullName;
            this.leaderboardData.region = res.data.user[0].state
              ? res.data.user[0].state
              : '';
            this.leaderboardData.selectedMonth = this.monthMapping[month - 1];
            this.leaderboardData.month = month;
            this.leaderboardData.selectedYear = today.getFullYear();
            this.leaderboardData.rank =
              res.data.rank != null ? res.data.rank : '-';
            this.leaderboardData.previousData = [];
            for (let i = 0; i < 6; i++) {
              const myDate = new Date(
                new Date().getFullYear(),
                new Date().getMonth() - i
              );
              this.leaderboardData.previousData.push({
                textMonth: this.monthMapping[myDate.getMonth()],
                textYear: myDate.getFullYear().toString(),
                year: myDate.getFullYear(),
                month: myDate.getMonth() + 1,
              });
            }

            //Graph Data
            const lastDay = new Date(year, month, 0).getDate();
            this.wins = [];
            this.days = [];
            let isCurrentMonth = false;

            if (today.getMonth() + 1 == month && today.getFullYear() == year) {
              isCurrentMonth = true;
            }
            for (let i = 0; i < lastDay; i++) {
              this.days.push(i + 1);
              if (!isCurrentMonth) {
                this.wins.push(0);
              } else {
                if (today.getDate() > i) {
                  this.wins.push(0);
                }
              }
            }
            for (const gameWins of res.data.gameStreakData) {
              this.wins[gameWins._id.day - 1] = gameWins.winCount;
            }
          } else {
            this.router.navigate(['/404']);
          }
          if (this.chartFlag) {
            if (this.chart) {
              this.chart.destroy();
            }
            this.initCharts(this.leaderboardData, this.translateService);
          } else {
            this.initCharts(this.leaderboardData, this.translateService);
            this.chartFlag = true;
          }
        },
        (err) => {
          this.router.navigate(['/404']);
          this.showLoader = false;
        }
      );
  }

  initCharts(leaderData, translateService): void {
    this.chart = new Chart(this.chartRef.nativeElement, {
      type: 'line',
      data: {
        labels: this.days,
        datasets: [
          {
            label: 'Game Streak: ',
            data: this.wins,
            borderColor: '#1ec48c',
            pointRadius: 5,
            fill: false,
          },
        ],
      },
      options: {
        tooltips: {
          enabled: true,
          mode: 'single',
          callbacks: {
            title: function (tooltipItems, data) {
              //Return value for title
              return (
                tooltipItems[0].xLabel +
                ' ' +
                translateService.instant(leaderData.selectedMonth) +
                ' ' +
                leaderData.selectedYear
              );
            },
            label: function (tooltipItems, data) {
              return 'Game Streak ' + tooltipItems.yLabel.toString();
            },
          },
        },
        maintainAspectRatio: false,
        responsive: true,
        legend: { display: false },
        scales: {
          xAxes: [
            {
              gridLines: { display: false },
            },
          ],
          yAxes: [
            {
              gridLines: { color: 'transparent' },
              ticks: {
                beginAtZero: true,
                display: false,
              },
            },
          ],
        },
      },
    });

    // new Chart(this.doughnutRefA.nativeElement, {
    //   type: 'doughnut',
    //   data: {
    //     datasets: [
    //       {
    //         data: [70, 10],
    //         backgroundColor: ['#22ced8', '#f8f8f8'],
    //       },
    //     ],
    //   },
    //   options: {
    //     title: {
    //       display: true,
    //       text: '70% stc average',
    //       position: 'bottom',
    //     },
    //     responsive: true,
    //     aspectRatio: 1,
    //     maintainAspectRatio: false,
    //     cutoutPercentage: 80,
    //     tooltips: { enabled: false },
    //     hover: { mode: null },
    //   },
    // });

    // new Chart(this.doughnutRefB.nativeElement, {
    //   type: 'doughnut',
    //   data: {
    //     datasets: [
    //       {
    //         data: [20, 80],
    //         backgroundColor: ['#fd375e', '#f8f8f8'],
    //       },
    //     ],
    //   },
    //   options: {
    //     title: {
    //       display: true,
    //       text: '70% stc average',
    //       position: 'bottom',
    //     },
    //     responsive: true,
    //     aspectRatio: 1,
    //     maintainAspectRatio: false,
    //     cutoutPercentage: 80,
    //     tooltips: { enabled: false },
    //     hover: { mode: null },
    //   },
    // });
  }

  changeTab(value, type) {
    switch (type) {
      case 'date': {
        this.selectedDate = value;
        if (value != 'month') {
          const temp = value.split(',');
          this.filterByUser(temp[0], temp[1]);
        } else {
          this.filterByUser();
        }
        break;
      }
    }
  }
  getStates() {
    this.stateList = [];
    this.userService.getStates().subscribe((data) => {
      this.stateList = data.states;
    });
  }
}
