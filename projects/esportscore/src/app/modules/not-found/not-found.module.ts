import { NgModule } from '@angular/core';

import { SharedModule } from '../../shared/modules/shared.module';
import { CoreModule } from '../../core/core.module';

import { NotfoundRoutingModule } from './not-found-routing.module';
import { NotfoundComponent } from './not-found.component';

@NgModule({
  declarations: [NotfoundComponent],
  imports: [SharedModule, CoreModule, NotfoundRoutingModule],
  exports: [],
})
export class NotfoundModule {}
