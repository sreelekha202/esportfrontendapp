import {
  Component,
  OnDestroy,
  OnInit,
  Inject,
  PLATFORM_ID,
} from '@angular/core';

import {
  VideoLibraryService,
  UtilsService,
  HomeService,
  TournamentService,
  ArticleApiService,
} from '../../core/service';
import {
  InfoPopupComponent,
  InfoPopupComponentData,
  InfoPopupComponentType,
} from '../../shared/popups/info-popup/info-popup.component';
import { MatDialog } from '@angular/material/dialog';
import { AppHtmlRoutes } from '../../app-routing.model';
import { TranslateService } from '@ngx-translate/core';
import {
  IUser,
  EsportsUserService,
  EsportsLanguageService,
  EsportsToastService,
  EsportsTimezone,
} from 'esports';
import { Subscription } from 'rxjs';
import { ScriptLoadingService } from '../../core/service/script-loading.service';
import { isPlatformBrowser } from '@angular/common';
import { GlobalUtils } from '../../shared/service/global-utils/global-utils';
import { GameService } from '../../core/service';
declare const zE;

@Component({
  selector: 'app-home2',
  templateUrl: './home2.component.html',
  styleUrls: ['./home2.component.scss'],
})
export class Home2Component implements OnInit, OnDestroy {
  user: IUser;
  isBrowser: boolean;
  AppHtmlRoutes = AppHtmlRoutes;
  postData;
  trendingPosts;
  isAnnouncementAvailabale = false;
  announcementData: any;
  videoLibrary: Array<{}> = [];
  activeVideo = null;

  tournamentsData = [];
  gwbTouranmentData = [];
  saudi_E_League = [];

  currentData = [];
  data: any = [];
  tournamentsfor3wb = [];
  featureTournamentsData = [];

  ongoingTournamentsData = [];

  tournamentsSlides = [];
  latestArticle = [];
  gameName = ['عيشها بلا حدود'];
  games = [
    {
      image: 'assets/images/Gwb/all-tournaments.png',
      name: 'GWB.COMPONENTS.TOURNAMENTS.ALL_TOURNAMENTS',
      isSelected: false,
    },
  ];
  gamesCards = [
    {
      img: 'assets/images/GamesCards/free-fire.png',
      name: 'free fire',
      date: 'August 30, 2020',
      description:
        'The Ups and downs being an assasian in sPEED DRIFTERS that you may relate to and also will be inspire to play also will be inspire to play...',
    },
    {
      img: 'assets/images/GamesCards/free-fire.png',
      name: 'VALORANT',
      date: 'August 30, 2020',
      description:
        'The Ups and downs being an assasian in Valorant that you may relate to and also will be inspire to play...',
    },
    {
      img: 'assets/images/GamesCards/speed-drifters.png',
      name: 'SPEED DRIFTERS',
      date: 'August 30, 2020',
      description:
        'The Ups and downs being an assasian in sPEED DRIFTERS that you may relate to and also will be inspire to play…',
    },
    {
      img: 'assets/images/GamesCards/heartstone.png',
      name: 'HEARTSTONE',
      date: 'August 30, 2020',
      description:
        'The Ups and downs being an assasian in sPEED DRIFTERS that you may relate to and also will be inspire to play…',
    },
    {
      img: 'assets/images/GamesCards/drift-masters.png',
      name: 'VALORANT',
      date: 'August 30, 2020',
      description:
        'The Ups and downs being an assasian in sPEED DRIFTERS that you may relate to and also will be inspire to play…',
    },
    {
      img: 'assets/images/GamesCards/drift-masters.png',
      name: 'HEARTSTONE',
      date: 'August 30, 2020',
      description:
        'The Ups and downs being an assasian in sPEED DRIFTERS that you may relate to and also will be inspire to play…',
    },
    {
      img: 'assets/images/GamesCards/drift-masters.png',
      name: 'VALORANT',
      date: 'August 30, 2020',
      description:
        'The Ups and downs being an assasian in sPEED DRIFTERS that you may relate to and also will be inspire to play…',
    },
    {
      img: 'assets/images/GamesCards/drift-masters.png',
      name: 'ARTICLE TITLE',
      date: 'August 30, 2020',
      description:
        'The Ups and downs being an assasian in sPEED DRIFTERS that you may relate to and also will be inspire to play…',
    },
    {
      img: 'assets/images/GamesCards/drift-masters.png',
      name: 'ARTICLE TITLE',
      date: 'August 30, 2020',
      description:
        'The Ups and downs being an assasian in sPEED DRIFTERS that you may relate to and also will be inspire to play…',
    },
    {
      img: 'assets/images/GamesCards/drift-masters.png',
      name: 'ARTICLE TITLE',
      date: 'August 30, 2020',
      description:
        'The Ups and downs being an assasian in sPEED DRIFTERS that you may relate to and also will be inspire to play…',
    },
    {
      img: 'assets/images/GamesCards/drift-masters.png',
      name: 'ARTICLE TITLE',
      date: 'August 30, 2020',
      description:
        'The Ups and downs being an assasian in sPEED DRIFTERS that you may relate to and also will be inspire to play…',
    },
  ];

  topPlayers = [];

  highlights = [];

  news = [
    {
      title: 'CLASH ROYALES BEST MO..',
      text: 'Team ultimately won the championship with highest score...',
      date: 'August 20, 2020',
    },
    {
      title: 'CLASH ROYALES BEST MO..',
      text: 'Team ultimately won the championship with highest score...',
      date: 'August 20, 2020',
    },
    {
      title: 'CLASH ROYALES BEST MO..',
      text: 'Team ultimately won the championship with highest score...',
      date: 'August 20, 2020',
    },
    {
      title: 'CLASH ROYALES BEST MO..',
      text: 'Team ultimately won the championship with highest score...',
      date: 'August 20, 2020',
    },
    {
      title: 'CLASH ROYALES BEST MO..',
      text: 'Team ultimately won the championship with highest score...',
      date: 'August 20, 2020',
    },
    {
      title: 'CLASH ROYALES BEST MO..',
      text: 'Team ultimately won the championship with highest score...',
      date: 'August 20, 2020',
    },
    {
      title: 'CLASH ROYALES BEST MO..',
      text: 'Team ultimately won the championship with highest score...',
      date: 'August 20, 2020',
    },
  ];

  leaderBoardRows = [];
  leaderBoardColumns = [
    { name: 'RANK' },
    { name: 'Player Name' },
    // { name: 'GAME' },
    { name: 'REGION' },
    { name: 'TROPHIES' },
    { name: 'POINTS' },
  ];
  allTournamentsData = [
    { gameName: 'PUBG' },
    { gameName: 'CS:GO' },
    { gameName: 'Fortnite' },
    { gameName: 'Call Of Duty' },
  ];

  userSubscription: Subscription;
  timezone;

  constructor(
    private eSportsToastService: EsportsToastService,
    private videoLibraryService: VideoLibraryService,
    public utilsService: UtilsService,
    private homeService: HomeService,
    private tournamentService: TournamentService,
    public language: EsportsLanguageService,
    public matDialog: MatDialog,
    public translateService: TranslateService,
    public userService: EsportsUserService,
    private articleService: ArticleApiService,
    public languageService: EsportsLanguageService,
    private scriptLoadingService: ScriptLoadingService,
    private esportsTimezone: EsportsTimezone,
    private gameService: GameService,
    @Inject(PLATFORM_ID) private platformId
  ) {
    this.isBrowser = isPlatformBrowser(platformId);
  }

  ngOnInit(): void {
    this.timezone = this.esportsTimezone.getTimeZoneName();
    this.getCurrentUserDetails();
    this.getGames();
  }

  getGames = async () => {
    try {
      const encodeUrl = `?query=${encodeURIComponent(
        JSON.stringify({
          isTournamentAllowed: true,
          name: this.gameName,
        })
      )}`;
      this.data = await this.gameService.getAllGames(encodeUrl).toPromise();
      if (this.data?.data[0]?._id) {
        this.getTournaments({
          status: '11',
          limit: 8,
          page: 1,
          game: this.data.data[0]._id,
        });
      }
      else { 
        this.tournamentsfor3wb = [];
      }
    } catch (error) {
      this.eSportsToastService.showError(
        this.translateService.instant(
          '3WB_TOURNAMENTS.ERRORS.ERROR_WHILE_FETCHING_GAMES'
        )
      );
    }
  }
  async getTournaments(payload) {
    try {
      this.tournamentService
        .getPaginatedTournaments(payload)
        .subscribe((res: any) => {
          this.tournamentsfor3wb = res.data.docs;
          });
    } catch (error) {
      this.eSportsToastService.showError(
        this.translateService.instant(
          '3WB_TOURNAMENTS.ERRORS.ERROR_WHILE_FETCH_TOURNAMENTS'
        )
      );
    }
  }
  isNumber(val): boolean {
    const str: string = val.toString();
    return str.startsWith('+');
  }

  ngOnDestroy() {
    this.userSubscription?.unsubscribe();
  }

  getHottestPost() {
    this.homeService.hottest_post().subscribe((data) => {});
  }
  getRecentPost() {
    const pagination = JSON.stringify({ limit: 8, sort: { createdDate: -1 } });
    const query = JSON.stringify({
      articleStatus: 'publish',
    });
    const perfernce = JSON.stringify({
      prefernce: this.user?.preference?.game
        ? this.user?.preference?.game.map((item) => {
            return { gameDetails: item };
          })
        : '',
    });
    this.articleService
      .getLatestArticle({
        pagination,
        query,
        preference: perfernce,
      })
      .subscribe(
        (res: any) => {
          this.latestArticle = res.data.docs;
        },
        (err) => {}
      );
  }

  /**
   * method to call the announcement data
   */
  getAnnouncement() {
    this.homeService.getAnnouncements().subscribe(
      (res: any) => {
        if (res.data && res.data.length > 0) {
          const data = res.data.map((ele) => {
            return {
              uniqueId: ele._id,
              image: ele.announcementFileUrl,
              header: ele.header,
              description: ele.description,
              destination: ele.destination,
            };
          });
          this.announcementData = data[0];
          if (this.announcementData.uniqueId) {
            if (GlobalUtils.isBrowser()) {
              const itemId = localStorage.getItem('announcement');
              if (itemId != this.announcementData.uniqueId) {
                this.isAnnouncementAvailabale = true;
              } else {
                this.isAnnouncementAvailabale = false;
              }
            }
          }
        }
      },
      (err: any) => {}
    );
  }
  getLeaderBoard() {
    this.homeService._leaderBoard().subscribe((data) => {
      this.leaderBoardRows = data.data.leaderboardData;
      if (this.leaderBoardRows) {
        this.leaderBoardRows.forEach((obj, index) => {
          obj.rank = index + 1;
        });
      }
    });
  }

  fetchVideoLibrary = async () => {
    try {
      const pagination = {
        page: 1,
        limit: 3,
        sort: '-updatedOn',
        projection: [
          '_id',
          'title',
          'description',
          'youtubeUrl',
          'thumbnailUrl',
          'createdBy',
        ],
      };
      const query = `?pagination=${encodeURIComponent(
        JSON.stringify(pagination)
      )}`;

      const response = await this.videoLibraryService.fetchVideoLibrary(query);
      this.videoLibrary = await Promise.all(
        response?.data?.docs.map(async (item) => {
          return {
            ...item,
            youtubeUrl: await this.utilsService.getEmbbedUrl(item.youtubeUrl),
          };
        })
      );
      this.activeVideo = this.videoLibrary.length ? this.videoLibrary[0] : null;
    } catch (error) {
      this.eSportsToastService.showError(
        error?.error?.message && error?.message
      );
    }
  };

  getBanner() {
    this.homeService._getBanner().subscribe(
      (res: any) => {
        this.tournamentsSlides = res.data;
      },
      (err: any) => {}
    );
  }

  getTopPlayers() {
    this.homeService._topPlayers().subscribe(
      (res: any) => {
        this.topPlayers = res.data.map((ele) => {
          return {
            fullName: ele?.fullName,
            photo: ele?.profilePicture
              ? ele?.profilePicture
              : './assets/images/Profile/stc_avatar.png',
          };
        });
      },
      (err: any) => {}
    );
  }
  getTournament() {
    // let query: any = {
    //   $and: [{ tournamentStatus: 'publish' }],
    //   $or: [{ startDate: { $lt: new Date() } }, { isSeeded: true }],
    // };
    // query = JSON.stringify(query);

    this.tournamentService
      .getPaginatedTournaments({
        status: '0',
        // need to status change
        limit: 6,
        sort: 'participantJoined',
        prefernce: '',
      })
      .subscribe(
        (res: any) => {
          // this.showLoader = false;
          this.tournamentsData = res.data.docs;
        },
        (err) => {
          // this.showLoader = false;
        }
      );
  }

  getSaudi_ELeague() {
    this.tournamentService
      .getPaginatedTournaments({
        status: '7',
        // need to status change
        limit: 6,
        sort: 'participantJoined',
        prefernce: '',
      })
      .subscribe(
        (res: any) => {
          // this.showLoader = false;
          this.saudi_E_League = res.data.docs;
        },
        (err) => {
          // this.showLoader = false;
        }
      );
  }

  getCurrentTournament() {
    this.tournamentService
      .getPaginatedTournaments({
        status: '1',
        limit: 6,
        sort: 'participantJoined',
        prefernce: this.user?.preference?.game
          ? this.user?.preference?.game.map((item) => {
              return { gameDetail: item };
            })
          : '',
      })
      .subscribe(
        (res: any) => {
          // this.showLoader = false;
          this.currentData = res.data.docs;
        },
        (err) => {
          // this.showLoader = false;
        }
      );
  }

  getOnGoingTournaments() {
    this.tournamentService
      .getPaginatedTournaments({
        status: '6',
        limit: 5,
        sort: 'participantJoined',
      })
      .subscribe(
        (res: any) => {
          this.featureTournamentsData = res.data.docs;
        },
        (err) => {
          // this.showLoader = false;
        }
      );
  }

  getRegistrationStatus(
    startDate,
    startTime,
    isSeeded,
    isFinished,
    maxParticipants,
    participantsJoined
  ) {
    if (!startDate || !startTime) {
      return;
    }
    const timeObj = this.convert12HrsTo24HrsFormat(startTime);
    const sDate = new Date(startDate);
    sDate.setHours(timeObj.hour);
    sDate.setMinutes(timeObj.minute);
    if (isSeeded == true || isFinished == true) {
      return 'Closed';
    } else if (participantsJoined >= maxParticipants) {
      return 'Closed';
    } else if (sDate.getTime() < Date.now()) {
      return 'Closed';
    } else {
      return 'Open';
    }
  }

  convert12HrsTo24HrsFormat(time) {
    if (!time) {
      return;
    }
    let hours = Number(time.match(/^(\d+)/)[1]);
    const minutes = Number(time.match(/:(\d+)/)[1]);
    let AP = time.match(/\s(.*)$/);
    if (!AP) {
      AP = time.slice(-2);
    } else {
      AP = AP[1];
    }
    if (AP == 'PM' && hours < 12) {
      hours = hours + 12;
    }
    if (AP == 'AM' && hours == 12) {
      hours = hours - 12;
    }
    let Hours24 = hours.toString();
    let Minutes24 = minutes.toString();
    if (hours < 10) {
      Hours24 = '0' + Hours24;
    }
    if (minutes < 10) {
      Minutes24 = '0' + Minutes24;
    }

    return { hour: parseInt(Hours24), minute: parseInt(Minutes24) };
  }

  openSocialShareComponent() {
    const data: InfoPopupComponentData = {
      title: this.translateService.instant('ARTICLE_POST.SHARE_TOURNAMENT'),
      text: ``,
      type: InfoPopupComponentType.socialSharing,
      cancelBtnText: 'Close',
    };
    this.matDialog.open(InfoPopupComponent, {
      data,
    });
  }

  getCurrentUserDetails() {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.user = data;
        this.getAnnouncement();
        this.fetchVideoLibrary();
        this.getTournament();
        this.getCurrentTournament();
        this.getOnGoingTournaments();
        this.getGWBTournaments();
        // this.getHottestPost();
        this.getSaudi_ELeague();
        this.getRecentPost();
        this.getBanner();
        this.getTopPlayers();
        this.getLeaderBoard();
      } else {
        this.getAnnouncement();
        this.getGWBTournaments();
        this.fetchVideoLibrary();
        this.getTournament();
        this.getCurrentTournament();
        this.getOnGoingTournaments();
        // this.getHottestPost();
        this.getSaudi_ELeague();
        this.getRecentPost();
        this.getBanner();
        this.getTopPlayers();
        this.getLeaderBoard();
      }
      this.loadZendesk();
    });
  }

  loadZendesk() {
    let zenDeskUrl =
    'https://static.zdassets.com/ekr/snippet.js?key=073f03b8-0c38-4a51-beaf-dab96dc8177c';
    this.scriptLoadingService.registerScript(zenDeskUrl, 'zE', (res) => {
      zE('webWidget', 'setLocale', 'en');
      zE('webWidget', 'updateSettings', {
        webWidget: {
          chat: {
            title: {
              '*': this.translateService.instant(
                'CHAT.ZENDESK_CHAT_WINDOW_TITLE'
              ),
            },
          },
          offset: {
            vertical: '53px',
            mobile: {
              horizontal: '2px',
              vertical: '40px',
            },
          },
        },
      });
    });
  }
  getGWBTournaments() {
    this.tournamentService
      .getPaginatedTournaments({
        status: '0',
        limit: 6,
        level2: true,
      })
      .subscribe(
        (res: any) => {
          this.gwbTouranmentData = res?.data?.docs;
        },
        (err) => {
          // this.showLoader = false;
        }
      );
  }
}
