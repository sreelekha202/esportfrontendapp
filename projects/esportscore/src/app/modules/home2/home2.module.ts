import { NgModule } from '@angular/core';

import { SharedModule } from '../../shared/modules/shared.module';
import { CoreModule } from '../../core/core.module';

import { Home2RoutingModule } from './home2-routing.module';
import { Home2Component } from './home2.component';
import { TournamentsCardModule } from './tournaments-card/tournaments-card.module';

@NgModule({
  declarations: [Home2Component],
  imports: [
    SharedModule,
    CoreModule,
    Home2RoutingModule,
    TournamentsCardModule,
  ],
  exports: [],
})
export class Home2Module {}
