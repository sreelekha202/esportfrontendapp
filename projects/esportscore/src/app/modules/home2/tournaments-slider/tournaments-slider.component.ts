import { Component, OnInit, Input } from '@angular/core';
import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';
import { EsportsLanguageService } from 'esports';
import { TranslateService } from '@ngx-translate/core';
import { GlobalUtils } from 'esports';

export interface Slide {
  bannerFileUrl: string;
  title: any;
  sub_title: any;
  button_text: any;
  destination: string;
}

@Component({
  selector: 'app-tournaments-slider',
  templateUrl: './tournaments-slider.component.html',
  styleUrls: ['./tournaments-slider.component.scss'],
  providers: [NgbCarouselConfig],
})
export class TournamentsSliderComponent implements OnInit {
  showNavigationArrows = false;
  showNavigationIndicators = true;
  bannerList: any = [];
  @Input() slides: any[];

  constructor(
    private config: NgbCarouselConfig,
    private translate: TranslateService,
    public language: EsportsLanguageService
  ) {
    config.showNavigationArrows = true;
    config.showNavigationIndicators = true;
  }

  ngOnChanges() {
    this.bannerList = this.slides;
  }

  /**
   * destination url
   * @param obj
   */
  redirectLink(obj) {
    if (GlobalUtils.isBrowser()) {
      window.location.href = obj;
    }
  }

  ngOnInit(): void {}
}
