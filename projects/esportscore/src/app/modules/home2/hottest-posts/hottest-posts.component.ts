import { EsportsLanguageService } from 'esports';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-hottest-posts',
  templateUrl: './hottest-posts.component.html',
  styleUrls: ['./hottest-posts.component.scss'],
})
export class HottestPostsComponent implements OnInit {
  @Input() params: any;

  constructor(public languageService: EsportsLanguageService) {}

  ngOnInit() {}
}
