import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TournamentsCardComponent } from './tournaments-card.component';
import { I18nModule, PipeModule } from 'esports';
import { RouterModule } from '@angular/router';
import { environment } from '../../../../environments/environment'

@NgModule({
  declarations: [TournamentsCardComponent],
  imports: [
    CommonModule,
    PipeModule,
    I18nModule.forRoot(environment),
    RouterModule,
  ],
  exports: [TournamentsCardComponent],
})
export class TournamentsCardModule {}
