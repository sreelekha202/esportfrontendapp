import { Component, OnInit, Input } from '@angular/core';

interface TopPlayer {
  fullName: string;
  team: string;
  photo: string;
}

@Component({
  selector: 'app-top-players',
  templateUrl: './top-players.component.html',
  styleUrls: ['./top-players.component.scss'],
})
export class TopPlayersComponent implements OnInit {
  @Input() players: any[];

  constructor() {}

  ngOnInit(): void {}
}
