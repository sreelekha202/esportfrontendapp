import { Component, OnInit, Input } from '@angular/core';

import { GlobalUtils } from '../../../shared/service/global-utils/global-utils';
import { AppHtmlRoutes } from '../../../app-routing.model';

@Component({
  selector: 'app-sports-federation',
  templateUrl: './sports-federation.component.html',
  styleUrls: ['./sports-federation.component.scss'],
})
export class SportsFederationComponent implements OnInit {
  @Input() params;

  AppHtmlRoutes = AppHtmlRoutes;

  constructor() {}

  ngOnInit(): void {}

  goTop(): void {
    if (GlobalUtils.isBrowser()) {
      document.querySelector('mat-sidenav-content').scroll({
        top: 0,
        left: 0,
        behavior: 'smooth',
      });
    }
  }
}
