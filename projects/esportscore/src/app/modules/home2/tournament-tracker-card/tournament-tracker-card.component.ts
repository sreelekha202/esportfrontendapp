import { Component, OnInit, Input } from '@angular/core';

interface Card {
  tournamentName: string;
  gamer1Name: string;
  gamer2Name: string;
  score1: number | string;
  score2: number | string;
  city: string;
}

@Component({
  selector: 'app-tournament-tracker-card',
  templateUrl: './tournament-tracker-card.component.html',
  styleUrls: ['./tournament-tracker-card.component.scss'],
})
export class TournamentTrackerCardComponent implements OnInit {
  @Input() params: any;

  constructor() {}

  ngOnInit(): void {}
}
