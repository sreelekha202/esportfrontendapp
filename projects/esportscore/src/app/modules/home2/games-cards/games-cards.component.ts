import { Component, OnInit, Input } from '@angular/core';
import { EsportsLanguageService } from 'esports'

@Component({
  selector: 'app-games-cards',
  templateUrl: './games-cards.component.html',
  styleUrls: ['./games-cards.component.scss'],
})
export class GamesCardsComponent implements OnInit {
  @Input() cards = [];

  constructor(public languageService: EsportsLanguageService) {}

  ngOnInit(): void {}
}
