import { ChartsModule } from 'ng2-charts';
import { NgModule } from '@angular/core';

import { MonitoringRoutingModule } from './monitoring-routing.module';
import { SharedModule } from '../../shared/modules/shared.module';
import { CoreModule } from '../../core/core.module';
import { MonitoringComponent } from './monitoring.component';

@NgModule({
  declarations: [
    MonitoringComponent
  ],
  imports: [SharedModule, CoreModule, MonitoringRoutingModule, ChartsModule],
  exports: [],
})
export class MonitoringModule {}
