import { SwiperConfigInterface } from 'ngx-swiper-wrapper';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {
  GameService,
} from '../../core/service';
import { EsportsUserService, EsportsLanguageService, EsportsConstantsService } from 'esports';
import { AppHtmlRoutes } from '../../app-routing.model';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-monitoring',
  templateUrl: './monitoring.component.html',
  styleUrls: ['./monitoring.component.scss'],
})
export class MonitoringComponent implements OnInit {
  public config: SwiperConfigInterface = {
    navigation: true,
    pagination: false,
    autoHeight: true,
  };
  apiEndPoint = environment.apiEndPoint;
  showLoader = false;
  currentLang = this.ConstantsService?.defaultLangCode;

  availableTokensCount = 27;
  gameServersCount = 3;

  serversColumns = [
    // { name: 'liveMatch', width: 64 },
    { name: 'id', width: 750 },
    { name: 'reachability' },
    { name: 'state' },
    { name: 'get5GameState' },
    { name: 'serverInfo' },
    { name: 'lastSyncTime' },
    // { name: 'controls', width: 50 }
  ];

  serversData: any = [
    {
      instanceId: 'instance_id_1234',
      csgoImageAmiId: 'csgo_image_ami_id_1234',
      platformMatchId: 'some_id_12345',
      matchId: 123456789,
      instanceState: 'running',
      csgoServerState: 'running',
      csgoExpressApiState: 'running',
      awsSystemReachability: 'passed',
      awsInstanceReachability: 'passed',
      get5GameState: 7,
      publicIp: '123.456.78.90',
      serverPassword: 'password123',
      csgoServerVersion: '9.999',
      lastSyncTime: '2022-01-10',
    },
    {
      instanceId: 'instance_id_6789',
      csgoImageAmiId: 'csgo_image_ami_id_6789',
      platformMatchId: 'some_id_67890',
      matchId: 987654321,
      instanceState: 'running',
      csgoServerState: 'unavailable',
      csgoExpressApiState: 'running',
      awsSystemReachability: 'failed',
      awsInstanceReachability: 'passed',
      get5GameState: 5,
      publicIp: '123.456.78.90',
      serverPassword: 'password321',
      csgoServerVersion: '8.999',
      lastSyncTime: '2022-01-09',
    },
    {
      instanceId: 'instance_id_1234',
      csgoImageAmiId: 'csgo_image_ami_id_1234',
      platformMatchId: 'some_id_12345',
      matchId: 0,
      instanceState: 'running',
      csgoServerState: 'running',
      csgoExpressApiState: 'running',
      awsSystemReachability: 'passed',
      awsInstanceReachability: 'passed',
      get5GameState: 2,
      publicIp: '123.456.78.90',
      serverPassword: 'password_011',
      csgoServerVersion: '7.999',
      lastSyncTime: '2022-01-10',
    }
  ];


  constructor(
    private gameService: GameService,
    private userService: EsportsUserService,
    private router: Router,
    public language: EsportsLanguageService,
    public ConstantsService: EsportsConstantsService
  ) {}

  ngOnInit(): void {
    this.language.language.subscribe((lang) => {
      this.currentLang = lang;
    });
  }

  toggleRow(row) {
    console.log('azaza1', row);
  }
}
