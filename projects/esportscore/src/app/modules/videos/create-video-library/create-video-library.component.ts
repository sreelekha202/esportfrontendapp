import { Component, OnInit } from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl,
  ValidatorFn,
  ValidationErrors,
} from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import {
  GameService,
  UtilsService,
  VideoLibraryService,
  OptionService,
  FormService,
} from '../../../core/service';
import { TranslateService } from '@ngx-translate/core';
import { EsportsLanguageService, EsportsConstantsService, EsportsToastService, CustomTranslatePipe } from 'esports'

@Component({
  selector: 'app-create-video-library',
  templateUrl: './create-video-library.component.html',
  styleUrls: ['./create-video-library.component.scss'],
  providers: [CustomTranslatePipe],
})
export class CreateVideoLibraryComponent implements OnInit {
  videoLibraryForm: FormGroup;

  tagsList = [];
  categoryList = [];
  gameList = [];
  platformList = [];
  genreList = [];

  language = [];
  currLangauge = '';
  navUrl = '../../';

  apiLoaded: Array<boolean> = [];
  isLoaded = false;
  isProcessing = false;
  slug;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private videoLibraryService: VideoLibraryService,
    private eSportsToastService: EsportsToastService,
    private utilsService: UtilsService,
    private gameService: GameService,
    private translateService: TranslateService,
    private languageService: EsportsLanguageService,
    private optionService: OptionService,
    private ConstantsService: EsportsConstantsService,
    private formService: FormService,
    private customTranslatePipe: CustomTranslatePipe
  ) {}

  ngOnInit(): void {
    this.language = this.ConstantsService?.language;
    this.currLangauge = this.language[0]?.key;
    this.languageService.language.subscribe((lang) =>
      this.translateService.use(lang || this.ConstantsService?.defaultLangCode)
    );
    this.fetchOptions();
    this.videoLibraryForm = this.fb.group({
      title: this.fb.group(this.ConstantsService.languageFC(), {
        validator: this.formService.groupFieldValidator(),
      }),
      youtubeUrl: [
        '',
        Validators.compose([Validators.required, this.validateUrl]),
      ],
      description: this.fb.group(this.ConstantsService.languageFC(), {
        validator: this.formService.groupFieldValidator(),
      }),
      tags: [
        [],
        Validators.compose([Validators.required, this.arrayValidation]),
      ],
      platforms: [
        [],
        Validators.compose([Validators.required, this.arrayValidation]),
      ],
      genres: [
        [],
        Validators.compose([Validators.required, this.arrayValidation]),
      ],
      game: ['', Validators.required],
      category: ['', Validators.required],
    });
    this.slug = this.activeRoute.snapshot.params.id;
  }
  /**
   * Get Options
   */
  fetchOptions = async () => {
    const encodeUrl = `?query=${encodeURIComponent(
      JSON.stringify({ isTournamentAllowed: true })
    )}`;
    try {
      this.apiLoaded.push(false);

      const option = await Promise.all([
        this.gameService.getAllGames(encodeUrl).toPromise(),
        this.optionService.fetchAllCategories(),
        this.optionService.fetchAllGenres(),
        this.optionService.fetchAllTags(),
      ]);
      this.gameList = option[0]?.data;
      this.categoryList = option[1]?.data;
      this.genreList = option[2]?.data;
      this.tagsList = option[3]?.data;

      if (this.slug) {
        this.fetchVideoLibraryById(this.slug);
      }
      this.apiLoaded.push(true);
    } catch (error) {
      this.apiLoaded.push(true);
      this.eSportsToastService.showError(error?.error?.message || error.message);
    } finally {
      this.allApiDataLoaded();
    }
  };

  /**
   * fetch video details by Id
   */
  fetchVideoLibraryById = async (id) => {
    try {
      this.apiLoaded.push(false);
      this.navUrl = this.navUrl + '../';
      const response = await this.videoLibraryService.fetchVideoLibraryById(id);
      const data = response?.data || null;
      data.title[this.currLangauge] = this.customTranslatePipe.transform(
        data.title
      );
      data.description[this.currLangauge] = this.customTranslatePipe.transform(
        data.description
      );
      if (data) {
        this.videoLibraryForm.addControl('id', new FormControl(id));

        const category = data.category._id;
        const tags = data.tags.map((item) => item._id);
        const platforms = data.platforms.map((item) => item._id);
        const genres = data.genres.map((item) => item._id);
        const game = data.game._id;

        this.videoLibraryForm.patchValue({
          ...data,
          category,
          tags,
          platforms,
          genres,
          game,
        });
        this.setPlatformList(data.game._id, this.gameList);
      } else {
        this.eSportsToastService.showError(
          this.translateService.instant('VIDEO_CREATE.DELETE')
        );
      }
      this.apiLoaded.push(true);
    } catch (error) {
      this.apiLoaded.push(true);
      this.eSportsToastService.showError(error?.error?.message || error?.message);
    } finally {
      this.allApiDataLoaded();
    }
  };

  /**
   * Check All API's Are Finished.
   * If All APi's Finished Then Remove Full Page Loader
   */
  allApiDataLoaded() {
    const startedApiCount = this.apiLoaded.filter((el) => !el).length;
    const finishedApiCount = this.apiLoaded.filter((el) => el).length;
    this.isLoaded = startedApiCount === finishedApiCount;
  }

  /**
   * Get Platform List For Selected Game
   * @param gameList gameList
   * @param game game
   */
  setPlatformList(_id, gameList) {
    if (_id && gameList.length) {
      this.platformList = gameList.find((item) => item._id == _id)?.platform;
    }
  }

  onChangePlatform(id, gameList) {
    this.videoLibraryForm.get('platforms').setValue([]);
    this.setPlatformList(id, gameList);
  }

  /**
   * Array validation
   * @param control ArrayFormControl
   */
  arrayValidation(control: FormControl) {
    if (!control.value.length) {
      return { required: true };
    }
    return null;
  }

  /**
   * Validate Youtube Url
   * @param control
   */
  validateUrl(control: FormControl) {
    if (control.value) {
      return control.value.match(
        `^(https?\:\/\/)?(www\.youtube\.com|youtu\.?be)\/.+$`
      )
        ? null
        : { invalidUrl: true };
    } else {
      return null;
    }
  }

  /**
   * Set Values In Form
   * @param formControl formControl
   * @param value value
   * @param isAllowMultiple isAllowMultiple
   * @param platform platform
   */
  addItem(formControl, value, isAllowMultiple = true, platform = null) {
    if (isAllowMultiple) {
      const array = this.videoLibraryForm.get(formControl).value;
      const index = array.indexOf(value);
      if (index < 0) {
        array.push(value);
        this.videoLibraryForm.controls[formControl].setValue(array);
      }
    } else {
      this.videoLibraryForm.get(formControl).setValue(value);
      if (platform) {
        this.platformList = platform?.data;
        this.videoLibraryForm.controls.platforms.setValue([]);
        this.videoLibraryForm.get('platforms').updateValueAndValidity();
      }
    }
    this.videoLibraryForm.get(formControl).updateValueAndValidity();
  }

  /**
   * Remove Value from the Form
   * @param formControl formControl
   * @param value value
   */
  removeItem(formControl, value) {
    const index = this.videoLibraryForm.value[formControl].indexOf(value);
    this.videoLibraryForm.value[formControl].splice(index, 1);
    this.videoLibraryForm.get(formControl).updateValueAndValidity();
  }

  /**
   * Set Marked As touch For Form Fields
   * @param formControl formControl
   */
  setFomControlTouched(formControl) {
    this.videoLibraryForm.controls[formControl].markAsTouched({
      onlySelf: true,
    });
  }

  /**
   * Save Video Data
   */
  submit = async () => {
    try {
      if (this.videoLibraryForm.invalid) {
        this.videoLibraryForm.markAllAsTouched();
        return;
      }
      this.isProcessing = true;
      const payload = await this.addDefaultContent(this.videoLibraryForm.value);
      const videoId = await this.utilsService.getVideoId(payload.youtubeUrl);
      payload.thumbnailUrl = `https://img.youtube.com/vi/${videoId}/hqdefault.jpg`;
      const response = payload.id
        ? await this.videoLibraryService.updateVideoLibrary(payload)
        : await this.videoLibraryService.createVideoLibrary(payload);

      this.eSportsToastService.showSuccess(response.message);

      this.router.navigateByUrl(`/profile/my-content`, {
        state: { tab: 'video-library' },
      });
      this.isProcessing = false;
    } catch (error) {
      this.isProcessing = false;
      this.eSportsToastService.showError(error?.error?.message || error?.message);
    }
  };
  cancel = async () => {
    this.router.navigateByUrl(`/profile/my-content`, {
      state: { tab: 'video-library' },
    });
  };

  addDefaultContent = async (payload) => {
    const fillTitle: Array<any> = this.language.filter(
      (el: string) => payload.title[el]
    );
    const emptyTitle: Array<any> = this.language.filter(
      (el: string) => !payload.title[el]
    );

    if (fillTitle.length !== 2) {
      payload.title[emptyTitle[0]] = payload.title[fillTitle[0]];
    }

    const fillDescription: Array<any> = this.language.filter(
      (el: string) => payload.description[el]
    );
    const emptyDescription: Array<any> = this.language.filter(
      (el: string) => !payload.description[el]
    );

    if (fillDescription.length !== 2) {
      payload.description[emptyDescription[0]] =
        payload.description[fillDescription[0]];
    }

    return payload;
  };
}
