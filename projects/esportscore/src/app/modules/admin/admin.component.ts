import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';

import { GlobalUtils } from '../../shared/service/global-utils/global-utils';
import { ScreenService } from '../../shared/service/screen/screen.service';
import { SidenavService } from '../../shared/service/sidenav/sidenav.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss'],
})
export class AdminComponent implements OnInit, AfterViewInit {
  @ViewChild('sidenav') public sidenav: MatSidenav;
  isLgScreen = false;

  constructor(
    private screenService: ScreenService,
    private sidenavService: SidenavService
  ) {}

  ngOnInit(): void {}

  ngAfterViewInit(): void {
    this.sidenavService.setSidenav(this.sidenav);
    this.screenService.onAppResizeListener();
    this.initSubscriptions();
  }

  private initSubscriptions(): void {
    if (GlobalUtils.isBrowser()) {
      setTimeout(() => {
        this.screenService.isLgScreen.subscribe((isLgScreen: boolean) => {
          this.isLgScreen = isLgScreen;

          if (isLgScreen) {
            this.sidenavService.close();
          } else {
            this.sidenavService.open();
          }
        });
      });
    }
  }
}
