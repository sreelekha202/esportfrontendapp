import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentSuccessfullDialogComponent } from './payment-successfull-dialog.component';

describe('PaymentSuccessfullDialogComponent', () => {
  let component: PaymentSuccessfullDialogComponent;
  let fixture: ComponentFixture<PaymentSuccessfullDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PaymentSuccessfullDialogComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentSuccessfullDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
