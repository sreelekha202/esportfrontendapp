import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-send-coupon-dialog',
  templateUrl: './send-coupon-dialog.component.html',
  styleUrls: [
    './send-coupon-dialog.component.scss',
    '../esports-payment-details.component.scss',
  ],
})
export class SendCouponDialogComponent implements OnInit {
  couponMailDetails = [
    {
      name: 'First winner prize coupon',
      prize: '100 MYR',
      email: 'Joandoe@gmail.com',
      value: 'add coupon',
    },

    {
      name: 'First winner prize coupon',
      prize: '100 MYR',
      email: 'Joandoe@gmail.com',
      value: 'add coupon',
    },

    {
      name: 'First winner prize coupon',
      prize: '100 MYR',
      email: 'Joandoe@gmail.com',
      value: 'add coupon',
    },
  ];

  constructor(
    @Inject(MAT_DIALOG_DATA) public data,
    public dialogRef: MatDialogRef<SendCouponDialogComponent>
  ) {}

  ngOnInit(): void {}

  onClose(text): void {
    this.dialogRef.close(text);
  }
}
