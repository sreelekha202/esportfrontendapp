import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QitafPointsComponent } from './qitaf-points.component';

describe('QitafPointsComponent', () => {
  let component: QitafPointsComponent;
  let fixture: ComponentFixture<QitafPointsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QitafPointsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(QitafPointsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
