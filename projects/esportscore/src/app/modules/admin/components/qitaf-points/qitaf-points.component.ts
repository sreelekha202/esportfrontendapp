import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EsportsAdminService } from 'esports';
import { environment } from '../../../../../environments/environment';

const API = environment.apiEndPoint;
@Component({
  selector: 'app-qitaf-points',
  templateUrl: './qitaf-points.component.html',
  styleUrls: ['./qitaf-points.component.scss'],
})
export class QitafPointsComponent implements OnInit {
  form: FormGroup;
  isLoading = false;
  tempData: any = null;

  constructor(
    private fb: FormBuilder,
    private adminService: EsportsAdminService
  ) {
    this.form = this.fb.group({
      registrationPoints: [0, Validators.required],
      referralPoints: [0 , Validators.required],
      IsRegistrationPointsActive: [true],
      IsReferralPointsActive: [true]
    });
  }

  ngOnInit(): void {
    this.getQitafConfigPoints();
  }

  cancel() {
    if(this.tempData) {
      this.form.setValue({
        registrationPoints: this.tempData.registrationPoints || 0,
        referralPoints: this.tempData.referralPoints || 0,
        IsRegistrationPointsActive: this.tempData?.IsRegistrationPointsActive || false,
        IsReferralPointsActive: this.tempData?.IsReferralPointsActive || false,
      });
    }
  }

  async getQitafConfigPoints() {
    this.isLoading = true;
    try {
      const qitafPoints = await this.adminService.getQitafConfigPoints(API);
      if(qitafPoints && qitafPoints.data && qitafPoints.data.length) {
        this.tempData = qitafPoints.data[0];
        this.form.setValue({
          registrationPoints: 0, // set fetched intial value temprary set as 0
          IsRegistrationPointsActive: false,
          referralPoints: qitafPoints.data[0].referralPoints || 0,
          IsReferralPointsActive: qitafPoints.data[0]?.IsReferralPointsActive || false,
        });
      }
      this.isLoading = false;
    } catch (error) {
      this.isLoading = false;
      console.error(error);
    }
  }

  increment(type) {
    this.form.get(type.toString()).setValue(this.form.get(type.toString()).value + 1);
  }

  decrement(type) {
    if(this.form.get(type.toString()).value > 0) {
      this.form.get(type.toString()).setValue(this.form.get(type.toString()).value - 1);
    }
  }

  async saveRefrral() {
    if (
      this.form.value &&
      this.form.value.hasOwnProperty('referralPoints') &&
      this.form.value.referralPoints >= 0
    ) {
      this.isLoading = true;
      try {
        await this.adminService.updateQitafConfigPoints(API, {
          // registrationPoints: parseInt(this.form.value.registrationPoints),
          referralPoints: parseInt(this.form.value.referralPoints),
          IsReferralPointsActive: this.form?.value?.IsReferralPointsActive,
        });
        this.isLoading = false;
      } catch (error) {
        this.isLoading = false;
        console.error(error);
      }
    }
  }

  saveNewRegistration() {
    // In form registrationPoints will be New Registrtion Point
    // IsRegistrationPointsActive is for switch toggel for on of
  }
}
