import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { EsportsAdminService } from 'esports';
import { environment } from '../../../../../../environments/environment';

const API = environment.apiEndPoint;

@Component({
  selector: 'app-language-configuration',
  templateUrl: './language-configuration.component.html',
  styleUrls: [
    './language-configuration.component.scss',
    '../../site-configuration/site-configuration.component.scss',
  ],
})
export class LanguageConfigurationComponent implements OnInit {
  public ownerForm: FormGroup;

  isLoading = true;
  showAdList = true;

  addedRegions = [];
  isEditable = {};
  languageList: any = [];

  bannerUri: any = '';
  _id = 0;

  constructor(private adminService: EsportsAdminService) {}

  clickHandler(type, rowData) {
    if (type == 'view') {
      this.ownerForm.setValue({
        displayKeyName: rowData.display_key,
        keyName: rowData.key,
        languageName: rowData.name,
      });

      this._id = rowData._id;
      this.showAdList = false;
    } else if (type == 'delete') {
      this.isLoading = true;
      this.adminService
        .updateLanguageDetails(API, rowData._id, { status: 0 })
        .subscribe(
          (res: any) => {
            this.isLoading = false;
            this.getLanguageDetails();
          },
          (err: any) => {
            this.isLoading = false;
          }
        );
    }
  }

  ngOnInit(): void {
    this.ownerForm = new FormGroup({
      languageName: new FormControl('', [Validators.required]),
      keyName: new FormControl('', [Validators.required]),
      displayKeyName: new FormControl('', [Validators.required]),
    });

    this.getLanguageDetails();
  }

  /**
   * method to get list of all the regions with active status
  //  */
  getLanguageDetails() {
    this.isLoading = true;

    this.adminService.getLanguageDetails(API).subscribe((res: any) => {
      this.languageList = [...res.data];
      this.isLoading = false;
      this.showAdList = true;
    });
  }

  /**
   * method to toggle the components on the page i.e. the table and the form
   */
  toggleLanguageList() {
    this.resetLanguageControls();
    this.showAdList = !this.showAdList;
  }

  removeRegion(index) {
    if (index > -1) {
      this.addedRegions.splice(index, 1);
    }
  }

  /**
   * method to be called before to reset the form controls
   */
  resetLanguageControls() {
    this.ownerForm.reset();
    this._id = 0;
  }

  /**
   * method to check the form controls the validations
   * @param controlName
   * @param errorName
   */
  public hasError = (controlName: string, errorName: string) => {
    return this.ownerForm.controls[controlName].hasError(errorName);
  };

  addData() {
    if (this.ownerForm.invalid) {
      return;
    }

    this.isLoading = true;

    const formData = {
      display_key: this.ownerForm.value.displayKeyName,
      key: this.ownerForm.value.keyName,
      name: this.ownerForm.value.languageName,
    };

    let subscriber;
    subscriber =
      this._id != 0
        ? this.adminService.updateLanguageDetails(API, this._id, formData)
        : this.adminService.addLanguageDetails(API, formData);

    subscriber.subscribe(
      (res: any) => {
        this.showAdList = true;
        this.isLoading = false;
        this.resetLanguageControls();
        this.getLanguageDetails();
      },

      (err: any) => {
        this.isLoading = false;
      }
    );
  }
}
