import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from '../../../../../environments/environment';

@Component({
  selector: 'app-season-management',
  templateUrl: './season-management.component.html',
  styleUrls: ['./season-management.component.scss'],
})
export class SeasonManagementComponent implements OnInit {
  createComponent: boolean = false;
  API = environment.apiEndPoint;
  TOKEN = environment.currentToken;
  seasonId;
  constructor(private activatedRoute: ActivatedRoute, private router: Router) {}

  ngOnInit(): void {
    this.seasonId = this.activatedRoute.snapshot.params['id'];
    if (this.router.url.match('create')) {
      this.createComponent = true;
    } else {
      this.createComponent = false;
    }
  }
}
