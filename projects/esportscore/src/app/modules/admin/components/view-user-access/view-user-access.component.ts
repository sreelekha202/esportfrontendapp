import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { Location } from '@angular/common';

import { EsportsUserService } from 'esports';

import { AccessType } from '../access-management/access-management.model';
import { Country } from 'ngx-intl-tel-input/lib/model/country.model';

import {
  InfoPopupComponent,
  InfoPopupComponentData,
  InfoPopupComponentType,
} from '../../../../shared/popups/info-popup/info-popup.component';

import { TranslateService } from '@ngx-translate/core';
import { MatDialog } from '@angular/material/dialog';

import { CountryISO, SearchCountryField } from 'ngx-intl-tel-input';

import { environment } from '../../../../../environments/environment';

const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;

@Component({
  selector: 'app-view-user-access',
  templateUrl: './view-user-access.component.html',
  styleUrls: ['./view-user-access.component.scss'],
})
export class ViewUserAccessComponent implements OnInit {
  AccessType = AccessType;
  CountryISO = CountryISO;
  mySelectedCountryISO: CountryISO;
  SearchCountryField = SearchCountryField;
  selectedCountryISO = CountryISO.Malaysia;

  isControlDisabled = false;
  isLoading = false;

  data: any;
  profilePicture: any = '';
  userId: any;

  ownerForm: FormGroup;

  preferredCountries: CountryISO[] = [
    CountryISO.SaudiArabia,
    CountryISO.Bahrain,
    CountryISO.Kuwait,
  ];

  userObject: any = {
    _id: '',
    accessLevel: [],
    email: '',
    fullName: '',
    phoneNumber: '',
  };

  constructor(
    private activatedRoute: ActivatedRoute,
    private location: Location,
    private translateService: TranslateService,
    private userService: EsportsUserService,
    public dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.getIdFromRoute();

    this.ownerForm = new FormGroup({
      name: new FormControl(
        { value: this.userObject.name, disabled: this.isControlDisabled },
        [Validators.required]
      ),
      email: new FormControl(
        { value: this.userObject.email, disabled: this.isControlDisabled },
        [Validators.required, Validators.email]
      ),
      phoneNumber: new FormControl(
        {
          value: this.userObject.phoneNumber,
          disabled: this.isControlDisabled,
        },
        [Validators.required]
      ),
    });
  }

  public hasError = (controlName: string, errorName: string) => {
    return this.ownerForm.controls[controlName].hasError(errorName);
  };

  getIdFromRoute() {
    this.activatedRoute.params.subscribe((params) => {
      this.userId = params['id'];
      if (this.userId !== '0123456789ab') {
        this.isControlDisabled = true;
        this.getUserListRecenltyUpdated(this.userId);
      }
    });
  }

  onCountryChange(e: Country) {
    this.mySelectedCountryISO = e.iso2 as CountryISO;
  }

  includeAccessType(key, isAdded) {
    if (isAdded) {
      this.userObject.accessLevel.push(key);
    } else {
      const index = this.userObject.accessLevel.indexOf(key);

      if (index > -1) {
        this.userObject.accessLevel.splice(index, 1);
      }
    }
  }

  getUserListRecenltyUpdated(id) {
    const query = `?query=${encodeURIComponent(JSON.stringify({ _id: id }))}`;

    this.userService.getAdminUsers(API, query).subscribe((res) => {
      if (res.data[0] && !res.data[0].accessLevel) {
        res.data[0]['accessLevel'] = [];
      }

      this.userObject = res.data.map((element) => ({
        accessLevel: element.accessLevel,
        email: element.email,
        fullName: element.fullName,
        phoneNumber: element.phoneNumber,
        profilePicture: element.profilePicture,
      }));

      this.profilePicture = this.userObject[0].profilePicture;
      this.userObject = this.userObject[0];
      this.ownerForm.setValue({
        name: this.userObject.fullName,
        email: this.userObject.email,
        phoneNumber: this.userObject.phoneNumber,
      });
      this.ownerForm.setErrors(null);
    });
  }

  goBack() {
    this.location.back();
  }

  checkValueIncludes(value) {
    return this.userObject.accessLevel
      ? this.userObject.accessLevel.includes(value)
      : false;
  }

  onGrantAccess() {
    if (this.userObject.accessLevel.length == 0) {
      const afterBlockData: InfoPopupComponentData = {
        title: this.translateService.instant('ADMIN_NEW.GRANT_ACCESS_ALERT'),
        text: this.translateService.instant('ADMIN_NEW.GRANT_ACCESS'),
        type: InfoPopupComponentType.info,
      };
      this.dialog.open(InfoPopupComponent, { data: afterBlockData });
      return;
    }
    this.isLoading = true;
    this.userService
      .updateUser(
        API,
        { accessLevel: this.userObject.accessLevel },
        this.userId
      )
      .subscribe(
        (res: any) => {
          this.isLoading = false;
          const afterBlockData: InfoPopupComponentData = {
            title: this.translateService.instant('API.AM.GET.SUCCESS_HEADER'),
            text: res?.message,
            type: InfoPopupComponentType.info,
          };
          this.dialog.open(InfoPopupComponent, { data: afterBlockData });
          this.goBack();
        },
        (err: any) => {
          this.isLoading = false;
          const afterBlockData: InfoPopupComponentData = {
            title: this.translateService.instant('API.AM.GET.ERROR_HEADER'),
            text: err?.error?.message || err?.message,
            type: InfoPopupComponentType.info,
          };
          this.dialog.open(InfoPopupComponent, { data: afterBlockData });
        }
      );
  }
}
