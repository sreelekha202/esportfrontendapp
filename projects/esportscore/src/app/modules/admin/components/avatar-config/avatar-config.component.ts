import { Component, OnInit } from '@angular/core';
import { MatDialog } from "@angular/material/dialog";
import { TranslateService } from "@ngx-translate/core";
import {EsportsAdminService } from 'esports';
import {
  InfoPopupComponent,
  InfoPopupComponentData,
  InfoPopupComponentType,
} from '../../../../shared/popups/info-popup/info-popup.component';
import { environment } from '../../../../../environments/environment';
const API = environment.apiEndPoint;
@Component({
  selector: 'app-avatar-config',
  templateUrl: './avatar-config.component.html',
  styleUrls: ['./avatar-config.component.scss']
})
export class AvatarConfigComponent implements OnInit {
  base64textString;
  avatars = [];
  isLoading;
  constructor(
    private adminService: EsportsAdminService,
    private translateService: TranslateService,
    private dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.getAvatars();
  }

  //get default avatars
  getAvatars() {
    this.isLoading = true;
    this.adminService.getAvatars(API).subscribe(
      (res: any) => {
        if (res?.data[0]) {
          this.avatars = res?.data[0].avatarUrls;
          this.isLoading = false;
        }else{
          this.isLoading = false;
        }
      },
      (err) => {
      }
    );
  }

  //convert image to binary
  handleFileSelect(evt) {
    var files = evt.target.files;
    var file = files[0];
    if (file) {
      var reader = new FileReader();
      reader.onload = this._handleReaderLoaded.bind(this);
      reader.readAsBinaryString(file);
    }
  }
  _handleReaderLoaded(readerEvt) {
    var binaryString = readerEvt.target.result;
    this.base64textString = "data:image/png;base64," + btoa(binaryString);
    this.adminService
      .uploadAvatar(API,{ avatarImg: this.base64textString })
      .subscribe(
        (res: any) => {
          if (res) {
            this.avatars.push({ avatarImg: res.imgUrl });
          }
        },
        (err) => {
        }
      );
  }
  //save avatars to database
  uploadAvatar() {
    this.adminService.saveAvatar(API,{ avatars: this.avatars }).subscribe(
      (res: any) => {
        if (res && res.data) {
          this.avatars = res.data.avatarUrls;
          const afterBlockData: InfoPopupComponentData = {
            title: this.translateService.instant(
              "API.PRODUCT.POST.SUCCESS_HEADER"
            ),
            text: this.translateService.instant("AVATAR SAVED") || res.message,
            type: InfoPopupComponentType.info,
          };
          this.dialog.open(InfoPopupComponent, { data: afterBlockData });
        }
      },
      (err) => {
      }
    );
  }
  // remove avatar from list
  deleteAvatar(index) {
    this.avatars.splice(index, 1);
  }
}