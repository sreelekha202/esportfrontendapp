import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';

import { faTrash } from '@fortawesome/free-solid-svg-icons';
import { MatTabChangeEvent, MatTabGroup } from '@angular/material/tabs';
import { debounceTime } from 'rxjs/operators';

enum SiteConfigurationTabs {
  siteCustomize,
  rewards,
  Alerts,
  banner,
}

@Component({
  selector: 'app-site-configuration',
  templateUrl: './site-configuration.component.html',
  styleUrls: ['./site-configuration.component.scss'],
})
export class SiteConfigurationComponent implements OnInit, AfterViewInit {
  @ViewChild('matTabGroup') matTabGroup: MatTabGroup;
  siteConfigurationTabs = SiteConfigurationTabs;

  checkIconShow = false;
  deleteIconShow = true;
  viewIconShow = true;

  activeTabIndex = 0;

  activeButtonColor = '#0C0E0F';
  backgroundColour = '#FC4500';
  nonActiveButtonColor = '#FC4500';

  faTrash = faTrash;

  constructor() {}

  ngOnInit(): void {}

  ngAfterViewInit() {
    this.matTabGroup.selectedTabChange
      .pipe(debounceTime(400))
      .subscribe((tabChangeEvent: MatTabChangeEvent) => {
        this.onTabChange(tabChangeEvent);
      });
  }

  onTabChange(matTab: MatTabChangeEvent): void {
    this.activeTabIndex = matTab.index;
  }
}
