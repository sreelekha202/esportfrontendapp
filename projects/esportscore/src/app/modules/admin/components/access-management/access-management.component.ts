import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { MatDialog } from '@angular/material/dialog';

import {
  ViewUserAccessPopupComponent,
  ViewUserAccessPopupComponentData,
} from '../../popups/view-user-access/view-user-access-popup.component';
import {
  InfoPopupComponent,
  InfoPopupComponentData,
  InfoPopupComponentType,
} from '../../../../shared/popups/info-popup/info-popup.component';
import { AccessManagementUser } from './access-management.model';
import { UserAccessType } from './access-management.model';
import {
  EsportsUserService,
  EsportsAdminService,
  EsportsTimezone,
  IPagination,
  EsportsLanguageService,
} from 'esports';
import { TranslateService } from '@ngx-translate/core';
import { AppHtmlRoutes } from '../../../../app-routing.model';
import { Router } from '@angular/router';
import { environment } from '../../../../../environments/environment';
import { MatTabChangeEvent } from '@angular/material/tabs';
import { forkJoin, Observable, of, fromEvent } from 'rxjs';
import { FormBuilder } from '@angular/forms';
import {
  debounceTime,
  map,
  distinctUntilChanged,
  filter,
  finalize,
  catchError,
} from 'rxjs/operators';

const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;

@Component({
  selector: 'app-access-management',
  templateUrl: './access-management.component.html',
  styleUrls: ['./access-management.component.scss'],
})
export class AccessManagementComponent implements OnInit {
  @ViewChild(DatatableComponent) table: DatatableComponent;
  searchInput: any;

  @ViewChild('searchInput', { static: false }) set setSearchInput(element) {
    if (element && this.showFilter) {
      this.searchInput = element;
      this.initializeSearchtracker();
      this.checkSearchStatus();
    }
  }
  tempFirstTab = [];
  rowsFirstTab = [];
  logData = [];
  page: IPagination;
  activeTabIndex = 0;
  currentlang;
  templog = [];
  timezone;
  eventTypes = [];
  isLoading = false;
  paginationData = {
    page: 1,
    limit: 50,
  };
  eventsArray = {};

  customEventArray = {};

  showFilter = false;
  columnsFirstTab = [
    { name: 'User ID' },
    { name: 'Name' },
    { name: 'Access Level' },
    { name: 'Phone' },
    { name: '' },
  ];
  columnsSecondTab = [
    { name: 'Time' },
    { name: 'Orginator' },
    { name: 'Type' },
    { name: 'Event description' },
  ];

  eventsControl = this.fb.group({
    events: [''],
  });
  constructor(
    public dialog: MatDialog,
    private router: Router,
    private fb: FormBuilder,
    private userService: EsportsUserService,
    private adminService: EsportsAdminService,
    private translateService: TranslateService,
    private esportsTimezone: EsportsTimezone,
    public languageService: EsportsLanguageService
  ) {}

  ngOnInit(): void {
    this.languageService.language.subscribe((lang: string) => {
      this.currentlang = lang;
    });
    // By default - To get the list of user for recently Updated
    this.timezone = this.esportsTimezone.getTimeZoneName();

    this.getUserListRecenltyUpdated();
  }
  isNumber(val): boolean {
    const str: string = val.toString();
    return str.startsWith('+');
  }

  initializeSearchtracker() {
    fromEvent(this.searchInput.nativeElement, 'keyup')
      .pipe(
        map((event: any) => {
          return event.target.value;
        }),
        filter((res) => res.length > 3),
        debounceTime(1000),
        distinctUntilChanged()
      )
      .subscribe((text: string) => {
        this.searchgetCall(text).subscribe((res: any) => {
          if (res[0] && res[0].data && res[0].data.docs) {
            this.logData = this.bindLogData(res[0].data);
          }
          if (res[1] && res[1].data) {
            this.eventsArray = res[1].data;
            this.customEventArray = { ...this.eventsArray };
            for (var key in this.customEventArray) {
              if (this.customEventArray.hasOwnProperty(key)) {
                this.customEventArray[key] = this.customEventArray[key][
                  this.currentlang
                ]['text'];
              }
            }
          }
        });
      });
  }

  searchgetCall(term) {
    if (term === '') {
      return of([]);
    }
    this.paginationData.page = 1;
    this.paginationData.limit = 50;
    return forkJoin(this.observablesForLogPage(term));
  }

  checkSearchStatus() {
    fromEvent(this.searchInput.nativeElement, 'keyup').subscribe(
      (res: any) => {
        if (res.target.value == '') {
          this.paginationData.page = 1;
          this.getLogData();
        }
      },
      (err) => {}
    );
  }

  selectType(e) {
    // TODO this.emailNotificationForm.controls['userSegment'].setValue(e);
  }

  onTabChanged = (tabChangeEvent: MatTabChangeEvent): void => {
    this.activeTabIndex = tabChangeEvent.index;
    this.paginationData.page = 1;
    switch (tabChangeEvent.index) {
      case 0: {
        this.getUserListRecenltyUpdated();
        break;
      }
      case 1: {
        this.getLogData();
        break;
      }
      default: {
        break;
      }
    }
  };

  observablesForLogPage(isSearchText) {
    const apiArray: Observable<any>[] = [];
    const params = {
      limit: this.paginationData.limit,
      page: this.paginationData.page,
      searchtext: isSearchText,
    };
    apiArray.push(
      this.adminService.getLogs(API, params).pipe(
        catchError((err) => {
          return of(undefined);
        })
      )
    );
    apiArray.push(
      this.adminService.getEvents(API).pipe(
        catchError((err) => {
          return of(undefined);
        })
      )
    );
    return apiArray;
  }

  pageChanged(page): void {
    this.paginationData.page = page;
    this.getLogData();
  }

  getLogData(isSearchText = null) {
    this.paginationData.limit = 50;
    this.isLoading = true;
    forkJoin(this.observablesForLogPage(isSearchText))
      .pipe(
        finalize(() => {
          this.isLoading = false;
        })
      )
      .subscribe((res: any) => {
        if (res[0] && res[0].data && res[0].data.docs) {
          this.templog = this.bindLogData(res[0].data);
          this.logData = [...this.templog];
        }
        if (res[1] && res[1].data) {
          this.eventsArray = res[1].data;

          this.customEventArray = { ...this.eventsArray };

          for (var key in this.customEventArray) {
            if (this.customEventArray.hasOwnProperty(key)) {
              this.customEventArray[key] = this.customEventArray[key][
                this.currentlang
              ]['text'];
            }
          }
          // this.eventTypes.forEach()
        }
      });
  }

  bindLogData(data) {
    this.page = {
      totalItems: data.totalDocs,
      itemsPerPage: data.limit,
      maxSize: 5,
    };

    let modifiedData = data.docs.map((ele) => {
      return {
        time: ele.createdAt,
        originator: ele.userId?.fullName || '',
        type: ele.eventCode,
        codes: ele.codes,
        remark: ele.remark,
      };
    });

    return modifiedData;
  }

  clearSearch() {
    // if (this.searchInput.nativeElement.value) {
    if (this.searchInput.nativeElement.value) {
      //   this.searchInput.nativeElement.value = '';
      this.searchInput.nativeElement.value = '';
      //   this.rowsFirstTab = this.tempFirstTab;
      this.paginationData.page = 1;
      this.getLogData();
      // }
    }
  }

  // Call an API to get the user details for Recently Updated Tab
  getUserListRecenltyUpdated() {
    this.isLoading = true;
    const query = `?query=${encodeURIComponent(
      JSON.stringify({ accountType: 'admin' })
    )}`;
    this.userService.getAdminUsers(API, query).subscribe(
      (res: any) => {
        this.isLoading = false;

        const response = res.data.map((element) => {
          return {
            id: element._id,
            name: element.fullName,
            username: element.username,
            email: element.email,
            phone: element.phoneNumber,
            profilePicture: element.profilePicture,
            accessLevel: element.accessLevel.filter((value) =>
              Object.keys(UserAccessType).includes(value)
            ),
            createdOn: element.createdOn,
          };
        });
        // cache our list
        this.tempFirstTab = [...response];
        // push our inital complete list
        this.rowsFirstTab = response;
      },

      (err: any) => {
        if (err.error?.messageCode == 'U_E014') {
          this.dialog.closeAll();
          this.userService.refreshCurrentUser(API, TOKEN);
          this.router.navigate([AppHtmlRoutes.home]);
        }
      }
    );
  }

  onViewUserAccess(user: AccessManagementUser): void {
    const blockUserData: ViewUserAccessPopupComponentData = { user };

    const dialogRef = this.dialog.open(ViewUserAccessPopupComponent, {
      data: blockUserData,
      panelClass: 'custom-dialog-container',
    });

    dialogRef.afterClosed().subscribe(() => {});
  }

  onRemoveUser(_id) {
    const blockUserData: InfoPopupComponentData = {
      title: this.translateService.instant('API.AM.MODAL.REMOVE_MODAL.TITLE'),
      text: this.translateService.instant('API.AM.MODAL.REMOVE_MODAL.TEXT'),
      type: InfoPopupComponentType.confirm,
      btnText: this.translateService.instant(
        'API.AM.MODAL.REMOVE_MODAL.BUTTONTEXT'
      ),
    };
    const dialogRef = this.dialog.open(InfoPopupComponent, {
      data: blockUserData,
    });

    dialogRef.afterClosed().subscribe((confirmed) => {
      if (confirmed) {
        this.userService
          .updateUser(API, { accountType: 'user' }, _id)
          .subscribe(
            (res: any) => {
              this.getUserListRecenltyUpdated();
              const afterBlockData: InfoPopupComponentData = {
                title: this.translateService.instant(
                  'API.AM.DELETE.SUCCESS_HEADER'
                ),
                text: res.message,
                type: InfoPopupComponentType.info,
              };
              this.dialog.open(InfoPopupComponent, { data: afterBlockData });
            },
            (err: any) => {
              const afterBlockData: InfoPopupComponentData = {
                title: this.translateService.instant(
                  'API.AM.DELETE.ERROR_HEADER'
                ),
                text: err?.error?.message || err?.message,
                type: InfoPopupComponentType.info,
              };
              this.dialog.open(InfoPopupComponent, { data: afterBlockData });
            }
          );
      }
    });
  }
}
