import {
  Component,
  ElementRef,
  OnInit,
  ViewChild,
  OnDestroy,
} from '@angular/core';

import { AppHtmlAdminRoutes } from '../../../../app-routing.model';
import {
  EsportsUserService,
  EsportsToastService,
  UserReportsService,
  EsportsConstantsService,
} from 'esports';
import { TranslateService } from '@ngx-translate/core';
import { MatDialog } from '@angular/material/dialog';
import { Observable, interval, Subscription, fromEvent, of } from 'rxjs';
import { MatTabChangeEvent, MatTabGroup } from '@angular/material/tabs';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { environment } from '../../../../../environments/environment';
import {
  InfoPopupComponent,
  InfoPopupComponentData,
  InfoPopupComponentType,
} from '../../../../shared/popups/info-popup/info-popup.component';
import { debounceTime, distinctUntilChanged, filter, map } from 'rxjs/operators';

const API = environment.apiEndPoint;

@Component({
  selector: 'app-user-management',
  templateUrl: './user-management.component.html',
  styleUrls: ['./user-management.component.scss'],
})
export class UserManagementComponent implements OnInit, OnDestroy {
  @ViewChild(DatatableComponent) table: DatatableComponent;

  AppHtmlAdminRoutes = AppHtmlAdminRoutes;
  isLoading: boolean = false;
  replyText;
  base64textString;
  isLoaded = true;
  userReports;
  userReportDetail;
  subs;
  userSubscription: Subscription;
  isAdmin = true;
  status: number;

  userid: string;

  tempFirstTab = [];
  rowsFirstTab = [];
  modelDesc: string;
  modelHeader: string;
  modelTitle: string;
  columnsFirstTab = [
    { name: 'Id' },
    { name: 'Name' },
    { name: 'Email' },
    { name: 'Phone' },
    { name: 'Sign Up From' },
    { name: 'Member Since' },
    { name: '' },
  ];

  tempSecondTab = [];
  rowsSecondTab = [];
  columnsSecondTab = [
    { name: 'Id' },
    { name: 'Name' },
    { name: 'Email' },
    { name: 'Phone' },
    { name: 'Sign Up From' },
    { name: 'Member Since' },
    { name: 'Amount Spend' },
    { name: '' },
  ];

  @ViewChild('searchInput', { static: true }) searchInput: ElementRef;

  constructor(
    private userService: EsportsUserService,
    private UserReportsService: UserReportsService,
    private eSportsToastService: EsportsToastService,
    private translateService: TranslateService,
    private modalService: NgbModal,
    public dialog: MatDialog
  ) {}

  ngOnInit(): void {
    // By default - To get the list of user for recently Updated
    this.getUserListRecenltyUpdated();
    this.initializeSearchtracker();
    this.checkSearchStatus();
  }

  // Call an API to get the user details for Recently Updated Tab
  getUserListRecenltyUpdated() {
    this.isLoading = true;
    this.userService.getRecentlyUpdatedUserList(API).subscribe(
      (res) => {
        const response = res.data.map((element) => ({
          createdOn: element.createdOn,
          email: element.email,
          id: element._id,
          name: element.fullName,
          username: element.username,
          phoneNumber: element.phoneNumber,
          status: element.Status,
        }));

        // cache our list
        this.tempFirstTab = [...response];

        // push our inital complete list
        this.rowsFirstTab = response;

        this.isLoading = false;
      },
      (err) => {
        this.isLoading = false;
      }
    );
  }

  initializeSearchtracker() {
    fromEvent(this.searchInput.nativeElement, 'keyup')
      .pipe(
        map((event: any) => {
          return event.target.value;
        }),
        filter((res) => res?.length > 1),
        debounceTime(1000),
        distinctUntilChanged()
      )
      .subscribe((text: string) => {
        this.searchgetCall(text).subscribe(
          (res) => {
            const response = res.map((element) => ({
              createdOn: element.createdOn,
              email: element.email,
              id: element._id,
              name: element.fullName,
              username: element.username,
              phoneNumber: element.phoneNumber,
            }));
            this.rowsFirstTab = response;
          },
          (err) => {}
        );
      });
  }

  checkSearchStatus() {
    fromEvent(this.searchInput.nativeElement, 'keyup').subscribe(
      (res: any) => {
        if (res.target.value == '') {
          this.rowsFirstTab = this.tempFirstTab;
        }
      },
      (err) => {}
    );
  }

  clearSearch() {
    if (this.searchInput.nativeElement.value) {
      this.searchInput.nativeElement.value = '';
      this.rowsFirstTab = this.tempFirstTab;
    }
  }

  // Call an API to get the user details for Recently Updated Tab
  getUserListTopSpending() {
    this.isLoading = true;
    this.userService.getTopSpendingUserList(API).subscribe(
      (res) => {
        this.tempSecondTab = [...res.data];

        // push our inital complete list
        this.rowsSecondTab = res.data;
        this.isLoading = false;
      },
      (err) => {
        this.isLoading = false;
      }
    );
  }

  /**
   * Get the user list based on Tab selection
   * @param tabChangeEvent : Event on Change of Tab
   */
  onTabChanged = (tabChangeEvent: MatTabChangeEvent): void => {
    this.clearSearch();
    if (tabChangeEvent.index === 0) {
      // Call an API to get the user details for Recently Updated Tab
      this.getUserListRecenltyUpdated();
    } else if (tabChangeEvent.index === 1) {
      // Call an API to get the user details for Top Spending Tab
      this.getUserListTopSpending();
    } else if (tabChangeEvent.index === 2) {
      // Call an API to get the user reports
      this.getUserReport();
      this.subsribeUserReport();
    }
  };

  searchgetCall(term) {
    if (term === '') {
      return of([]);
    }
    return this.userService.searchUsersAdminPanel(term, {
      state: true,
      read: 'createdOn,email,_id,fullName,username,phoneNumber',
    });
  }

  updateSecondTabFilter(event) {
    const value = event.target.value.toLowerCase().trim();
    // get the amount of columns in the table
    const count = this.columnsSecondTab.length;
    // get the key names of each column in the dataset
    const keys = Object.keys(this.tempSecondTab[0]);
    // assign filtered matches to the active datatable
    this.rowsSecondTab = this.tempSecondTab.filter((item) => {
      // iterate through each row's column data
      for (let i = 0; i < count; i++) {
        // check for a match
        if (
          (item[keys[i]] &&
            item[keys[i]].toString().toLowerCase().indexOf(value) !== -1) ||
          !value
        ) {
          // found match, return true to add to result set
          return true;
        }
      }
    });
    // Whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }

  ngOnDestroy() {
    if (this.subs) {
      this.subs.unsubscribe();
    }
    this.userSubscription?.unsubscribe();
  }

  subsribeUserReport() {
    this.subs = Observable.interval(10000).subscribe((val) => {
      this.getUserReport();
    });
  }

  getUserReport() {
    this.isLoading = true;
    this.UserReportsService.getUserReports(environment.apiEndPoint).subscribe(
      (res: any) => {
        this.userReports = res?.data?.docs;
        this.isLoading = false;
      },
      (err) => {
        this.isLoading = false;
      }
    );
  }

  getReportDetail(id) {
    this.isLoading = true;
    this.UserReportsService.getUserReportById(
      environment.apiEndPoint,
      id
    ).subscribe(
      (res: any) => {
        this.userReportDetail = res.data;
        if (res?.data?.discussion?.length) {
          this.userReportDetail['adminUser'] = res.data.discussion.find(
            (item) => {
              if (item?.updatedBy?.accountType === 'admin') {
                return item;
              }
            }
          );
        }
        this.isLoading = false;
      },
      (err) => {
        this.isLoading = false;
      }
    );
  }

  backBtnClicked() {
    this.userReportDetail = false;
    this.replyText = '';
    this.base64textString = '';
  }

  downloadUrl(url: string) {
    const a: any = document.createElement('a');
    a.href = url;
    a.download = 'commentImg';
    document.body.appendChild(a);
    a.style = 'display: none';
    a.click();
    a.remove();
  }

  onReply(isClose) {
    if (
      this.userReportDetail.report._id &&
      (this.replyText || this.base64textString)
    ) {
      this.isLoading = true;
      const data = {
        reportId: this.userReportDetail.report._id,
        reportText: this.replyText,
        reportImage: this.base64textString || '',
      };
      this.isLoaded = false;
      this.UserReportsService.replyOnUserReport(
        environment.apiEndPoint,
        data
      ).subscribe(
        (res) => {
          if (isClose) {
            this.UserReportsService.closeReport(
              environment.apiEndPoint,
              this.userReportDetail.report._id
            ).subscribe(
              (res) => {
                this.isLoading = false;
                this.userReportDetail = '';
                this.getUserReport();
              },
              (err) => {
                this.isLoading = false;
              }
            );
            this.replyText = '';
            this.base64textString = '';
          } else {
            this.userReportDetail = '';
            this.getUserReport();
            this.replyText = '';
            this.base64textString = '';
            this.isLoading = false;
          }
        },
        (err) => {
          this.isLoading = false;
        }
      );
    } else {
      // show error required field msg
      this.eSportsToastService.showError(
        this.translateService.instant(
          'USER_REPORT.REPORT_REQUIRED_COMMENT_FIELD'
        )
      );
    }
  }

  //convert image to binary
  handleFileSelect(evt) {
    var files = evt.target.files;
    var file = files[0];

    if (files && file) {
      var reader = new FileReader();
      reader.onload = this._handleReaderLoaded.bind(this);
      reader.readAsBinaryString(file);
    }
  }

  _handleReaderLoaded(readerEvt) {
    var binaryString = readerEvt.target.result;
    this.base64textString = 'data:image/png;base64,' + btoa(binaryString);
  }

  removeUser(status, id) {
    if (status == 3) {
      const afterBlockData: InfoPopupComponentData = {
        title: this.translateService.instant('ADMIN.ERROR.FAIL'),
        text: this.translateService.instant(
          'ADMIN.USER_MANAGEMENT.REMOVE_USER.UNABLE_TO_DELETE'
        ),
        type: InfoPopupComponentType.info,
      };
      this.dialog.open(InfoPopupComponent, { data: afterBlockData });
    } else {
      this.userService.removeUser(environment.apiEndPoint, id).subscribe(
        (res) => {
          const afterBlockData: InfoPopupComponentData = {
            title: this.translateService.instant(
              'ADMIN.USER_NOTIFICATION.RESPONSE.SUCCESS'
            ),
            text: res.message,
            type: InfoPopupComponentType.info,
          };
          this.dialog.open(InfoPopupComponent, { data: afterBlockData });
          this.rowsFirstTab = [];
          this.getUserListRecenltyUpdated();
        },
        (err) => {
          const afterBlockData: InfoPopupComponentData = {
            title: this.translateService.instant(
              'ADMIN.USER_NOTIFICATION.RESPONSE.ERROR'
            ),
            text: err.error.message,
            type: InfoPopupComponentType.info,
          };
          this.dialog.open(InfoPopupComponent, { data: afterBlockData });
        }
      );
    }
  }


  /**
   * Open model pop on button click
   * @param content -content
   */
  open(content, status, id, modelType) {
    this.status = status;
    this.userid = id;
    // Check to open model and configure inner content
    if (modelType === 'DeleteParticiepants') {
      this.setModelConfiguration(
        EsportsConstantsService.Model.DeleteParticiepants.header,
        EsportsConstantsService.Model.DeleteParticiepants.title,
        EsportsConstantsService.Model.DeleteParticiepants.description
      );
    }
    this.modalService.open(content, {
      size: 'lg',
      centered: true,
      scrollable: true,
      windowClass: 'custom-modal-content',
    });
  }


  Confirmatio() {
    this.removeUser(this.status, this.userid);
    this.modalService.dismissAll();
  }

  setModelConfiguration(header, title, description) {
    this.modelTitle = this.translateService.instant(title);
    this.modelDesc = this.translateService.instant(description);
    this.modelHeader = this.translateService.instant(header);
  }

  openResult(content) {
    this.modalService.open(content, {
      centered: true,
      size: 'lg',
      scrollable: false,
      windowClass: 'custom-modal-content',
    });
  }

  exitFromScoreCard(data) {
    this.modalService.dismissAll();
  }
}
