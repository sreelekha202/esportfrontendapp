import { Component, OnInit, Input } from '@angular/core';

import { ArticleApiService } from '../../../../core/service';
import { IArticle } from '../../../../shared/models/articles';

import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import {
  faEye,
  faTrashAlt,
  faCheckSquare,
} from '@fortawesome/free-solid-svg-icons';
import { EsportsConstantsService } from 'esports'

@Component({
  selector: 'app-articles-details',
  templateUrl: './articles-details.component.html',
  styleUrls: ['./articles-details.component.scss'],
})
export class ArticlesDetailsComponent implements OnInit {
  @Input() articlesList: IArticle[] = [];
  @Input() checkIconShow;
  @Input() deleteIconShow;
  @Input() viewIconShow;

  hideBtn = false;
  showRequestEditBtn = false;
  viewModelBtnDisable = false;

  modelDesc: string;
  modelHeader: string;
  modelTitle: string;
  selectedId: string;

  articleModel: IArticle;

  faCheckSquare = faCheckSquare;
  faEye = faEye;
  faTrash = faTrashAlt;

  constructor(
    private articleService: ArticleApiService,
    private modalService: NgbModal,
    private translateService: TranslateService
  ) {}

  ngOnInit(): void {}

  /**
   * Open model pop on button click
   * @param content -content
   */
  open(content, Id, modelType) {
    this.selectedId = Id;
    this.hideBtn = true;
    this.showRequestEditBtn = false;

    // Check to open model and configure inner content
    if (modelType === 'requestEdit') {
      this.showRequestEditBtn = true;
      this.setModelConfiguration(
        EsportsConstantsService.Model.RequestEdit.header,
        EsportsConstantsService.Model.RequestEdit.title,
        EsportsConstantsService.Model.RequestEdit.description
      );
    } else {
      this.setModelConfiguration(
        EsportsConstantsService.Model.Delete.header,
        EsportsConstantsService.Model.Delete.title,
        EsportsConstantsService.Model.Delete.description
      );
    }

    this.modalService.open(content, {
      size: 'lg',
      centered: true,
      scrollable: true,
      windowClass: 'custom-modal-content',
    });
  }

  /**
   * Open model to view the articles
   * @param model : model
   * @param Id : Set Selected Id
   * @param Type : Check type of model to hide/show the button
   */
  viewArticle(model, Id, Type) {
    this.viewModelBtnDisable = Type === 'view';

    this.modalService.open(model, {
      size: 'lg',
      centered: true,
      scrollable: true,
      windowClass: 'custom-modal-content',
    });
    this.getArticleById(Id);
  }

  // Abort Articles based on Selected Id
  abort() {
    this.articleService.delArticle(this.selectedId).subscribe((res) => {
      if (res) {
        this.setModelConfiguration(
          EsportsConstantsService.Model.Abort.header,
          EsportsConstantsService.Model.Abort.title,
          EsportsConstantsService.Model.Abort.description
        );

        // Remove Object from list based on Index
        const currIndex = this.articlesList.findIndex(
          (resp) => resp.id === this.selectedId
        );

        this.articlesList.splice(currIndex, 1);
        this.hideBtn = false;
      }
    });
  }

  /**
   * Confiure model
   * @param header : header value
   * @param title : Title value
   * @param description : Description value
   */
  setModelConfiguration(header, title, description) {
    this.modelTitle = this.translateService.instant(title);
    this.modelDesc = this.translateService.instant(description);
    this.modelHeader = this.translateService.instant(header);
  }

  // get articles details based on Id
  getArticleById(Id) {
    this.articleService.getArticleByID(Id).subscribe(
      (data: any) => {
        this.articleModel = data.data[0];
      },
      (err) => {}
    );
  }
}
