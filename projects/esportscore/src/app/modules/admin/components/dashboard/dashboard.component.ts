import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';


import { EsportsAdminService, EsportsUserService } from 'esports';
import { GameService } from '../../../../core/service';
import { environment } from '../../../../../environments/environment';

const API = environment.apiEndPoint;

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {
  public ownerForm: FormGroup;

  currentDate: Date = new Date();

  countryList = [];
  gameList = [];
  stateList = [];

  dashboardData: any;
  tornamentDetails: any;
  onGoingTournamentPer = 0;

  filter = { country: '', region: '', gameId: '' };

  constructor(
    private adminService: EsportsAdminService,
    private fb: FormBuilder,
    private gameService: GameService,
    private userService: EsportsUserService
  ) {}

  ngOnInit(): void {
    this.ownerForm = this.fb.group({
      game: [''],
      country: [''],
      region: [''],
    });
    this.loadData();
    this.getGames();
    this.fetchCountries();
  }

  loadData() {
    this.adminService.getCountsOnfilter(API, this.filter).subscribe((res: any) => {
      this.dashboardData = res.data;
      if (
        this.dashboardData &&
        this.dashboardData.ongoingTournamentsCount &&
        this.dashboardData.upComingTournamentsCount
      ) {
        const total =
          this.dashboardData?.ongoingTournamentsCount +
          this.dashboardData?.upComingTournamentsCount;
        this.onGoingTournamentPer =
          (this.dashboardData?.ongoingTournamentsCount / total) * 100;
      } else {
        this.onGoingTournamentPer = 0;
      }
    });
  }

  fetchCountries = async () => {
    try {
      const data = await this.userService.getAllCountries().toPromise();
      this.countryList.push({
        ar: 'كل',
        en: 'ALL',
        id: 0,
        name: 'ALL',
        phoneCode: 973,
        sortname: 'ALL',
      });
      const countryList = data.countries.filter((item) =>
        [191, 117, 17].includes(item.id)
      );
      this.countryList = [].concat(this.countryList, countryList);
    } catch (error) {}
  };

  selectType(e) {
    if (e == 'ALL') {
      this.filter.gameId = '';
      this.loadData();
    } else {
      this.filter.gameId = e;
      this.loadData();
    }
  }

  getGames() {
    this.gameService.getAdminAllGames().subscribe(
      (res) => {
        if (res && res.data) {
          this.gameList = res.data;
        }
      },
      (err) => {}
    );
  }

  onCountryChange = async (countryname) => {
    this.stateList.length = 0;
    if (countryname == 'ALL') {
      this.filter.country = '';
      this.stateList = [];
      this.ownerForm.patchValue({
        region: '',
      });
      this.loadData();
    } else {
      this.filter.country = countryname;
      this.loadData();
      const country = this.countryList.find((item) => item.name == countryname);
      const data = await this.userService.getStates().toPromise();
      this.stateList = data.states.filter(
        (item) => item.country_id == country.id
      );
    }
  };

  onRegionChange = async (regionName) => {
    if (regionName == 'ALL') {
      this.filter.region = '';
      this.loadData();
    } else {
      this.filter.region = regionName;
      this.loadData();
    }
  };
}
