import { Component, OnInit } from '@angular/core';
import { MatTabChangeEvent } from '@angular/material/tabs';

import { AdvertisementService } from '../../../../core/service';
import { IAdvertisementManagement } from '../../../../shared/models/advertisement';
import { EsportsConstantsService } from 'esports'

@Component({
  selector: 'app-advertisement-management',
  templateUrl: './advertisement-management.component.html',
  styleUrls: ['./advertisement-management.component.scss'],
})
export class AdvertisementManagementComponent implements OnInit {
  checkIconShow: boolean = false;
  deleteIconShow: boolean = false;
  viewIconShow: boolean = false;

  advertisementList: IAdvertisementManagement[] = [];

  constructor(private advertisementService: AdvertisementService) {}

  ngOnInit(): void {
    this.advertisementService.getPendingAd().subscribe((res: any) => {
      this.advertisementList = res.data;
    });
    this.setIconConfiguration(true, true, false);
  }

  /**
   * Get the tournament list based on Tab selection
   * @param tabChangeEvent : Event on Change of Tab
   */
  onTabChanged = (tabChangeEvent: MatTabChangeEvent): void => {
    switch (tabChangeEvent.tab.textLabel) {
      case EsportsConstantsService.AdvertisementTab.Pending:
        this.setIconConfiguration(true, true, false);
        this.advertisementService.getPendingAd().subscribe((res: any) => {
          this.advertisementList = res.data;
        });
        break;

      case EsportsConstantsService.AdvertisementTab.Running:
        this.setIconConfiguration(true, false, true);
        this.advertisementService.getRunningAd().subscribe((res: any) => {
          this.advertisementList = res.data;
        });
        break;

      case EsportsConstantsService.AdvertisementTab.Paused:
        this.setIconConfiguration(true, false, true);
        this.advertisementService.getPausedAd().subscribe((res: any) => {
          this.advertisementList = res.data;
        });
        break;

      case EsportsConstantsService.AdvertisementTab.Finished:
        this.setIconConfiguration(true, false, false);
        this.advertisementService.getFinishedAd().subscribe((res: any) => {
          this.advertisementList = res.data;
        });
        break;

        defaul: break;
    }
  };

  /**
   * Configure icon based on boolean value
   * @param viewIcon : show/hide icon
   * @param deleteIcon:show/hide icon
   * @param checkIcon:show/hide icon
   */
  setIconConfiguration(viewIcon, checkIcon, deleteIcon) {
    this.viewIconShow = viewIcon;
    this.deleteIconShow = deleteIcon;
    this.checkIconShow = checkIcon;
  }
}
