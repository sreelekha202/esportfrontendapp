import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { MatTabChangeEvent, MatTabGroup } from '@angular/material/tabs';
import {
  InfoPopupComponentData,
  InfoPopupComponentType,
  InfoPopupComponent,
} from '../../../../shared/popups/info-popup/info-popup.component';
import { MatDialog } from '@angular/material/dialog';
import { AppHtmlRoutes } from '../../../../app-routing.model';
import { TranslateService } from '@ngx-translate/core';
import { IPagination } from '../../../../shared/models';
import { TournamentService } from '../../../../core/service';
import {
  EsportsConstantsService,
  EsportsAdminService,
  EsportsToastService,
} from 'esports';
import { ActivatedRoute, Router } from '@angular/router';
import { debounceTime } from 'rxjs/operators';
import { environment } from '../../../../../environments/environment';
const API = environment.apiEndPoint;
@Component({
  selector: 'app-esports-management',
  templateUrl: './esports-management.component.html',
  styleUrls: ['./esports-management.component.scss'],
})
export class EsportsManagementComponent implements OnInit, AfterViewInit {
  @ViewChild('matTabGroup') matTabGroup: MatTabGroup;
  IAMAFFTournament: boolean = false;
  AppHtmlRoutes = AppHtmlRoutes;

  checkIconShow: boolean = false;
  deleteIconShow: boolean = false;
  editIconShow: boolean = false;
  isLoading: boolean = false;
  manageIconShow: boolean = false;
  viewIconShow: boolean = false;

  activeTabIndex;
  page: IPagination;
  userId;

  selectedTab: string = 'ongoing';
  statusText: string = EsportsConstantsService.Status.Live;

  allTournamentList: any = [];
  finishedTournament: any = [];
  tournamentList = [];
  text: string;
  paginationData = {
    page: 1,
    limit: 50,
    sort: 'startDate',
  };
  timeoutId = null;
  constructor(
    private activeRoute: ActivatedRoute,
    private dialog: MatDialog,
    private router: Router,
    private tournamentService: TournamentService,
    private translateService: TranslateService,
    private esportsAdminService: EsportsAdminService,
    private eSportsToastService: EsportsToastService
  ) {}

  ngOnInit(): void {
    this.activeRoute.queryParams
      .subscribe((params) => {
        if (params.activeTab) {
          this.activeTabIndex = +params.activeTab;
          this.switchData(this.activeTabIndex);
        } else {
          this.activeTabIndex = 0;
          this.getData('1', 'Latest');
          this.setConfiguration(true, true, false, false, true);
        }
      })
      .unsubscribe();
  }

  ngAfterViewInit() {
    this.matTabGroup.selectedTabChange
      .pipe(debounceTime(400))
      .subscribe((tabChangeEvent: MatTabChangeEvent) => {
        this.onTabChanged(tabChangeEvent);
      });
  }

  pageChanged(page): void {
    this.paginationData.page = page;
    switch (this.activeTabIndex) {
      case 0:
        this.getData('1', 'Latest'); //ongoing
        break;
      case 1:
        this.getData('0', 'Latest'); // upcoming
        break;
      case 2:
        this.getData('2', 'Latest'); //past
        break;
      case 3:
        this.getData('5', 'Latest'); // Approval pending
        break;
      case 4:
        this.getData('3', 'Oldest'); // wager tournament
        break;
      case 5:
        this.getData('10', 'Oldest'); // payament  pending
      default:
        break;
    }
  }

  // getTournamentForPendingPayments() {
  //   this.isLoading = true;

  //   this.adminService.getTournamentsforPayments().subscribe(
  //     (res: any) => {
  //       const response = res.data.map((element) => ({
  //         _id: element._id,
  //         game: element.gameDetail?.name,
  //         isFeature: element.isFeature,
  //         isPaid: element.isPaid,
  //         maxParticipants: element.maxParticipants,
  //         name: element.name,
  //         region: element.regionsAllowed,
  //         startDate: element.startDate,
  //         tournamentType: element.tournamentType,
  //       }));

  //       this.isLoading = false;
  //       this.finishedTournament = response;
  //     },
  //     (err: any) => {
  //       this.isLoading = false;
  //     }
  //   );
  // }
  getModifiedOptions = async (body) => {
    try {
      const optionUpdate = async () => {
        try {
          const { data, message } = await this.esportsAdminService.patchOptions(
            API,
            body
          );

          this.IAMAFFTournament = data?.IAMAFFTournament;

          this.eSportsToastService.showSuccess(message);
        } catch (error) {
          clearTimeout(this.timeoutId);
          this.eSportsToastService.showError(
            error?.error?.message || error?.message
          );
        }
      };

      if (this.timeoutId) clearTimeout(this.timeoutId);

      this.timeoutId = setTimeout(optionUpdate, 1000);
    } catch (error) {
      this.eSportsToastService.showError(
        error?.error?.message || error?.message
      );
    }
  };
  /**
   * method to update the modifications on tournaments
   * @param data
   */
  modifyTournamentHandler = async (data: any) => {
    if (data.type === 'abort') {
      this.switchData(this.activeTabIndex);
    } else if (data.type === 'feature') {
      this.tournamentService
        .updateTournament({ isFeature: data.value }, data.id)
        .subscribe(
          (res: any) => {
            this.getData('0', 'Latest');

            const afterBlockData: InfoPopupComponentData = {
              title: this.translateService.instant(
                'API.TOURNAMENT.PUT.SUCCESS_HEADER'
              ),
              text: res.message,
              type: InfoPopupComponentType.info,
            };
            this.dialog.open(InfoPopupComponent, { data: afterBlockData });
          },
          (err: any) => {
            const afterBlockData: InfoPopupComponentData = {
              title: this.translateService.instant(
                'API.TOURNAMENT.PUT.ERROR_HEADER'
              ),
              text:
                this.translateService.instant(err.error.messageCode) ||
                err.error.message,
              type: InfoPopupComponentType.info,
            };
            this.dialog.open(InfoPopupComponent, { data: afterBlockData });
          }
        );
    } else if (data.type === 'update') {
      this.switchData(data.value);
      if (data.value == 3) {
        this.getData('5', 'Latest');
      } else {
        this.switchData(data.value);
      }
    } else if (data.type === 'updatePaymentTable') {
      //this.getTournamentForPendingPayments();
      this.getData('10', 'Oldest');
    } else if (data.type === 'GWB') {
      this.tournamentService
        .updateGWBTournament({ level: data.value }, data.id)
        .subscribe(
          (res: any) => {
            if (data.tabIndex == 0) {
              this.getData(1, 'Latest');
            } else if (data.tabIndex == 1) {
              this.getData(0, 'Latest');
            } else {
              this.getData(data.tabIndex, 'Latest');
            }
            const afterBlockData: InfoPopupComponentData = {
              title: this.translateService.instant(
                'API.TOURNAMENT.PUT.SUCCESS_HEADER'
              ),
              text: res.message,
              type: InfoPopupComponentType.info,
            };
            this.dialog.open(InfoPopupComponent, { data: afterBlockData });
          },
          (err: any) => {
            const afterBlockData: InfoPopupComponentData = {
              title: this.translateService.instant(
                'API.TOURNAMENT.PUT.ERROR_HEADER'
              ),
              text:
                this.translateService.instant(err.error.messageCode) ||
                err.error.message,
              type: InfoPopupComponentType.info,
            };
            this.dialog.open(InfoPopupComponent, { data: afterBlockData });
          }
        );
    } else if (data.text) {
      this.text = data.text;
      this.paginationData.page = data.page || 1;
      this.paginationData.limit = 50;
      this.getData(data.status, 'Latest');
    }
  };
  /**
   * Get the tournament list based on Tab selection
   * @param tabChangeEvent : Event on Change of Tab
   */
  onTabChanged = (tabChangeEvent: MatTabChangeEvent): void => {
    this.activeTabIndex = tabChangeEvent.index;
    this.router.navigate(['.'], {
      relativeTo: this.activeRoute,
      queryParams: { activeTab: tabChangeEvent.index },
    });
    this.switchData(tabChangeEvent.index);
  };

  switchData(index) {
    switch (index) {
      case 0:
        this.resetPage();
        this.setConfiguration(true, true, false, false, true);
        this.getData('1', 'Latest'); //ongoing
        break;
      case 1:
        this.resetPage();
        this.setConfiguration(true, true, false, true, true);
        this.getData('0', 'Latest'); // upcoming
        break;
      case 2:
        this.resetPage();
        this.setConfiguration(true, true, false, false, true);
        this.getData('2', 'Latest'); //past
        break;
      case 3:
        this.resetPage();
        this.setConfiguration(true, true, true, true, false);
        this.getData('5', 'Latest');
        break;
      case 4:
        this.resetPage();
        this.setConfiguration(true, true, true, true, false);
        this.getData('3', 'Latest');
        break;
      case 5:
        this.resetPage1();
        this.getData('10', 'Oldest');
        // this.getTournamentForPendingPayments();
        break;
      default:
        break;
    }
  }
  resetPage1() {
    this.paginationData = {
      page: 1,
      limit: 50,
      sort: 'Oldest',
    };
    this.tournamentList = [];
  }

  resetPage() {
    this.paginationData = {
      page: 1,
      limit: 50,
      sort: 'startDate',
    };
    this.tournamentList = [];
  }
  getData = async (status, sortOrder) => {
    try {
      const params = {
        limit: this.paginationData.limit,
        page: this.paginationData.page,
        status: status,
        sort: sortOrder,
        ...(status == 5 && {
          tournamentStatus: 'submitted_for_approval',
        }),
        ...(this.text && {
          text: this.text,
        }),
      };
      let response;

      this.isLoading = true;

      if (status == 5 || status == 10) {
        this.tournamentList = [];
        response = await this.tournamentService
          .getPaginatedTournaments(params)
          .toPromise();
      } else {
        if (params.text) {
          response = await this.tournamentService.fetchTournamentByStatus(
            params
          );
        } else {
          this.tournamentList = [];
          response = await this.tournamentService.fetchTournamentByStatus(
            params
          );
        }
        this.text = '';
      }

      this.mapTournamentFilterList(response?.data?.docs);
      this.page = {
        totalItems: response?.data?.totalDocs,
        itemsPerPage: response?.data?.limit,
        maxSize: 5,
      };
      this.isLoading = false;
    } catch (error) {
      this.isLoading = false;
    }
  };
  /**
   * Map the tournament details to display in the table
   * @param tournamentList : response from method
   */
  mapTournamentFilterList(tournamentList) {
    const response = tournamentList.map((element) => ({
      _id: element._id,
      game: element.gameDetail?.name,
      isFeature: element.isFeature,
      isPaid: element.isPaid,
      maxParticipants: element.maxParticipants,
      name: element.name,
      region: element.regionsAllowed,
      slug: element.slug,
      startDate: element.startDate,
      tournamentType: element.tournamentType,
      reason: element.reason,
      level: element.level,
      isWager: element.isWager || false,
    }));

    this.tournamentList = response;
  }

  /**
   * Configure icon based on boolean value
   * @param viewIcon : show/hide icon
   * @param deleteIcon:show/hide icon
   * @param checkIcon:show/hide icon
   */
  setConfiguration(viewIcon, deleteIcon, checkIcon, editIcon, manageIcon) {
    this.viewIconShow = viewIcon;
    this.deleteIconShow = deleteIcon;
    this.checkIconShow = checkIcon;
    this.editIconShow = editIcon;
    this.manageIconShow = manageIcon;
  }
}
