import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EsportsUserService, EsportsToastService } from 'esports';
import { DatePipe } from '@angular/common';
import {
  AppHtmlProfileRoutes,
  AppHtmlAdminRoutes,
  AppHtmlRoutes,
} from '../../../../app-routing.model';

import { environment } from '../../../../../environments/environment';

const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;

@Component({
  selector: 'app-team-management-edit',
  templateUrl: './team-management-edit.component.html',
  styleUrls: ['./team-management-edit.component.scss'],
})
export class TeamManagementEditComponent implements OnInit {
  mock_members = [];
  paginationData = {
    page: 1,
    limit: 1,
    sort: { createdOn: -1 },
    _id: '',
  };
  mock_team: {
    id: string;
    title: string;
    image: string;
    teamId: {};
  };
  showMe: boolean = false;
  isTeamDeleted: boolean = false;
  constructor(
    private activatedRoute: ActivatedRoute,
    private userService: EsportsUserService,
    public datePipe: DatePipe,
    public eSportsToastService: EsportsToastService
  ) {}
  AppHtmlRoutes = AppHtmlRoutes;
  AppHtmlProfileRoutes = AppHtmlProfileRoutes;
  AppHtmlAdminRoutes = AppHtmlAdminRoutes;
  ngOnInit(): void {
    const logId = this.activatedRoute.snapshot.params['id'];
    if (logId) {
      this.getTeamData(logId);
      this.getTeamLogs(logId);
    }
  }
  getTeamLogs = (id) => {
    this.userService.getTeamLogs(API, id).subscribe(
      (res) => {
        this.mock_members = res.data;
        this.mock_members.forEach((obj) => {
          obj.createdOn = this.datePipe.transform(obj.createdOn, 'd MMMM yyyy');
        });
      },
      (err) => {}
    );
  };
  deleteTeam() {
    const teamObj = {
      id: this.mock_team.id,
      admin: true,
      query: {
        condition: { _id: this.mock_team.id, status: 'active' },
        update: { status: 'inactive' },
      },
    };
    this.userService.update_team_admin(API, teamObj).subscribe(
      (res) => {
        if (res.data) {
          const teamLog = {
            description:
              'Team ' +
              "'" +
              res.data.teamName +
              "'" +
              'deleted' +
              ' successfully',
            createdOn: this.datePipe.transform(new Date(), 'd MMMM yyyy'),
          };
          this.mock_members.push(teamLog);
          this.mock_members = [...this.mock_members];
          this.isTeamDeleted = true;
          this.eSportsToastService.showSuccess(res.message);
        } else {
          this.eSportsToastService.showError(res.message);
        }
      },
      (err) => {
        this.eSportsToastService.showError(err.error.message);
      }
    );
  }
  getTeamData(id) {
    this.paginationData._id = id;
    const pagination = JSON.stringify(this.paginationData);
    this.userService.teamsDetail_Admin(API, { pagination }).subscribe(
      (res) => {
        this.mock_team = {
          teamId: {
            teamName: res.data[0].name,
            logo: res.data[0].logo,
          },
          id: id,
          title: res.data[0].name,
          image: res.data[0].logo,
        };
        this.showMe = true;
      },
      (err) => {}
    );
  }
}
