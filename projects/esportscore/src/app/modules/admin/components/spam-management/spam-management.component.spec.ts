import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpamManagementComponent } from './spam-management.component';

describe('SpamManagementComponent', () => {
  let component: SpamManagementComponent;
  let fixture: ComponentFixture<SpamManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpamManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpamManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
