import { isPlatformBrowser } from '@angular/common';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {
  Component,
  OnInit,
  Inject,
  PLATFORM_ID,
  OnDestroy,
} from '@angular/core';
import { EsportsChatService } from 'esports';
import { IUser, EsportsUserService } from 'esports';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-spam-management',
  templateUrl: './spam-management.component.html',
  styleUrls: ['./spam-management.component.scss'],
})
export class SpamManagementComponent implements OnInit, OnDestroy {
  spamuserlist: any = [];
  cblockidtoblok;
  isBrowser: boolean;
  matchdetails: any;
  showChat: boolean = false;
  matchdetailsReporter: any;
  showChatReporter: boolean = false;
  matchdetailsAbuser: any;
  showChatAbuser: boolean = false;
  typeofchatHistory: string = 'individual';
  typeofchatReporter: string = 'individual';
  typeofchatAbuser: string = 'individual';
  isUserLoggedIn: boolean = false;
  currentUserId: String = '';
  currenUser: IUser;
  windowPositionReporter: String = 'chat_window chat_window_reporter_drawer';
  windowPositionAbuser: String = 'chat_window chat_window_abuser_drawer';
  windowPositionHistory: String = 'chat_window chat_window_history_drawer';
  userSubscription: Subscription;

  columnsFirstTab = [
    { name: 'Id' },
    { name: 'Reported For' },
    { name: 'Reported By' },
    { name: 'Comment' },
    { name: 'Updated At' },
    { name: '' },
  ];

  constructor(
    @Inject(PLATFORM_ID) private platformId,
    public chatService: EsportsChatService,
    private modalService: NgbModal,
    private userService: EsportsUserService
  ) {
    this.isBrowser = isPlatformBrowser(platformId);
  }

  ngOnInit(): void {
    this.chatService.getAllSpamList().subscribe(
      (spamlist: any) => {
        for (let i = 0; i < spamlist.length; i++) {
          if (spamlist[i].comment == 'undefined') {
            spamlist[i].comment = 'No comments given';
          }
        }
        this.spamuserlist = spamlist;
      },
      (err) => {}
    );

    if (this.isBrowser) {
      this.userSubscription = this.userService.currentUser.subscribe((data) => {
        if (data) {
          this.currenUser = data;
          this.isUserLoggedIn = !!this.currenUser;
          this.currentUserId = this.currenUser._id;

          this.chatService.chatStatus.subscribe((cstatus) => {
            this.showChat = cstatus;
          });
        } else {
        }
      });
    }
  }

  ngOnDestroy() {
    this.userSubscription?.unsubscribe();
  }
  blockUser(content, cblokid) {
    this.cblockidtoblok = cblokid;

    this.modalService.open(content, {
      centered: true,
      windowClass: 'custom-modal-content',
    });
  }

  onConfirmSpam() {
    this.chatService
      ?.changeBlockStatus(this.cblockidtoblok, 'spm')
      .subscribe((resp: any) => {
        this.cblockidtoblok = '';
        this.modalService.dismissAll();
        this.chatService.getAllSpamList().subscribe(
          (spamlist) => {
            this.spamuserlist = spamlist;
          },
          (err) => {}
        );
      });
  }

  messageReporter(row) {
    let matchid = row.userid._id + '-' + this.currentUserId;
    this.matchdetails = matchid;
    this.showChat = true;
    this.chatService.setWindowPos(this.windowPositionAbuser);
    this.chatService.setCurrentMatch(matchid);
    this.chatService.setTypeOfChat('individual');
    this.chatService.setChatStatus(true);
    this.chatService.setTextButtonStatus(true);
    this.chatService.setAdminMessageStatus(true);
    if (this.chatService.getChatStatus() == false) {
      this.chatService?.disconnectMatch(matchid, 'individual');
    }
  }

  messageAbuser(row) {
    let matchid = row.blockedUserId._id + '-' + this.currentUserId;
    this.matchdetails = matchid;
    this.showChat = true;
    this.chatService.setWindowPos(this.windowPositionAbuser);
    this.chatService.setCurrentMatch(matchid);
    this.chatService.setTypeOfChat('individual');
    this.chatService.setChatStatus(true);
    this.chatService.setTextButtonStatus(true);
    this.chatService.setAdminMessageStatus(true);
    if (this.chatService.getChatStatus() == false) {
      this.chatService?.disconnectMatch(matchid, 'individual');
    }
  }

  showLog(row) {
    let matchid = row.blockedUserId._id + '-' + row.userid._id;
    this.matchdetails = matchid;
    this.showChat = true;
    this.chatService.setWindowPos(this.windowPositionAbuser);
    this.chatService.setCurrentMatch(matchid);
    this.chatService.setChatHistory(row.chathistory);
    this.chatService.setTypeOfChat('chathistory');
    this.chatService.setTextButtonStatus(false);
    this.chatService.setAdminMessageStatus(true);
    this.chatService.setChatStatus(true);
    if (this.chatService.getChatStatus() == false) {
      this.chatService?.disconnectMatch(matchid, 'chathistory');
    }
  }
}
