import {
  AfterViewChecked,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import {
  AbstractControl,
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  ValidatorFn,
  Validators,
} from '@angular/forms';

import {
  AdminService,
  GameService,
  S3UploadService,
  FormService,
} from '../../../../../core/service';

import {
  EsportsAdminService,
  EsportsConstantsService,
  EsportsLanguageService,
  EsportsToastService,
  GlobalUtils,
} from 'esports';
import {
  InfoPopupComponentData,
  InfoPopupComponentType,
  InfoPopupComponent,
} from '../../../../../shared/popups/info-popup/info-popup.component';

import { MatDialog } from '@angular/material/dialog';
import { TranslateService } from '@ngx-translate/core';
import { environment } from '../../../../../../environments/environment';

const API = environment.apiEndPoint;

@Component({
  selector: 'upsert-game',
  templateUrl: './upsert-game.component.html',
  styleUrls: [
    './upsert-game.component.scss',
    '../../site-configuration/site-configuration.component.scss',
  ],
})
export class UpsertGameComponent implements OnInit, AfterViewChecked {
  @Input() platformList: Array<any> = [];
  @Input() gameDetails: any = null;

  @Output() valueEmit = new EventEmitter<any>();

  gameForm: FormGroup;
  isLoading: boolean = true;
  dropdownSettings = {};

  bracketList: any;

  @ViewChild('logoUpload', { static: false }) set logoUpload(element) {
    if (GlobalUtils.isBrowser() && element && this.gameForm?.value?.logo) {
      document.getElementById('logoUpload').style.background =
        'url(' + this.gameForm?.value?.logo + ')';
    }
  }

  @ViewChild('imageUpload', { static: false }) set imageUpload(element) {
    if (GlobalUtils.isBrowser() && element && this.gameForm?.value?.image) {
      document.getElementById('imageUpload').style.background =
        'url(' + this.gameForm?.value?.image + ')';
    }
  }

  constructor(
    private gameService: GameService,
    private translateService: TranslateService,
    public dialog: MatDialog,
    public globalUtils: GlobalUtils,
    public eSportsToastService: EsportsToastService,
    private s3UploadService: S3UploadService,
    private formService: FormService,
    private cdr: ChangeDetectorRef,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.bracketList = EsportsConstantsService?.bracketType || [];
    this.gameForm = this.fb.group({
      _id: ['0123456789ab'],
      image: ['', Validators.required],
      logo: ['', Validators.required],
      bracketTypes: this.fb.group({
        single: [false],
        double: [false],
        round_robin: [false],
        battle_royale: [false],
        swiss_safeis: [false],
        ladder: [false],
      }),
      isTournamentAllowed: [false],
      name: [
        '',
        Validators.compose([Validators.required, this.formService.emptyCheck]),
      ],
      platform: [[], this.formService.minLengthArray(1)],
      isQuickFormatAllow: [false],
    });
    this.quickformatHandler(this.gameDetails?.isQuickFormatAllow);

    this.gameForm.patchValue({
      ...this.gameDetails,
      platform: this.gameDetails?.platform?.map((item) => item?._id) || [],
    });

    this.dropdownSettings = {
      classes: 'myclass multiselect-class',
      enableSearchFilter: true,
      searchPlaceholderText: this.translateService.instant('GAME.SEARCH'),
      selectAllText: this.translateService.instant('GAME.SELECT_ALL'),
      singleSelection: false,
      text: '',
      unSelectAllText: this.translateService.instant('GAME.UNSELECT_ALL'),
    };
  }

  ngAfterViewChecked(){
    this.cdr.detectChanges();
 }

  /**
   * method to load the log image on the page
   */
  logoUploader() {
    if (GlobalUtils.isBrowser()) {
      const logo: any = document.getElementById('logofileUpload');
      logo.click();
      logo.onchange = function () {
        const reader = new FileReader();
        reader.onload = function (event: any) {
          const img = new Image();
          img.onload = function () {
            this.uploadImages(event.target.result, 'logo');
            document.getElementById('logoUpload').style['background'] =
              'url(' + event.target.result + ')';
          }.bind(this);
          img.onerror = function () {};
          img.src = window.URL.createObjectURL(logo.files[0]);
        }.bind(this);
        reader.readAsDataURL(logo.files[0]);
      }.bind(this);
    }
  }

  /**
   * method to load image on the page
   */
  imageUploader() {
    if (GlobalUtils.isBrowser()) {
      const image: any = document.getElementById('imagefileUpload');
      image.click();
      image.onchange = function () {
        const reader = new FileReader();
        reader.onload = function (event: any) {
          const img = new Image();
          img.onload = function () {
            this.uploadImages(event.target.result, 'image');
            document.getElementById('imageUpload').style['background'] =
              'url(' + event.target.result + ')';
          }.bind(this);
          img.onerror = function () {};
          img.src = window.URL.createObjectURL(image.files[0]);
        }.bind(this);
        reader.readAsDataURL(image.files[0]);
      }.bind(this);
    }
  }

  uploadImages = async (base64: string, field: string) => {
    try {
      const payload = {
        path: 'game',
        file: base64,
        type: 'image',
      };
      const imageUpload: any = await this.s3UploadService
        .fileUpload(payload)
        .toPromise();
      this.gameForm.get(field).setValue(imageUpload?.data?.Location || '');
      this.eSportsToastService.showSuccess(imageUpload?.message);
    } catch (error) {
      const errorMessage = error?.error?.message || error?.message;
      this.eSportsToastService.showError(errorMessage);
    }
  };

  /**
   * method to toggle the edit panel
   */
  backToGameList(isRefresh: boolean = false) {
    this.gameForm.reset();
    this.valueEmit.emit(isRefresh);
  }

  /**
   * method to call to save the game
   */
  saveGame = async () => {
    try {
      const { value, valid } = this.gameForm;
      debugger;
      if (!valid) return;
      value.slug = value.name.replace(/\s/g, '');
      this.isLoading = true;
      const saveOrUpdateResponse = await this.gameService
        .saveGame(value._id, value)
        .toPromise();
      const data: InfoPopupComponentData = {
        title: this.translateService.instant('API.GAME.POST.SUCCESS_HEADER'),
        text: saveOrUpdateResponse?.message,
        type: InfoPopupComponentType.info,
      };
      this.dialog.open(InfoPopupComponent, { data });
      this.isLoading = false;
      this.backToGameList(true);
    } catch (error) {
      this.isLoading = false;
      const errorMessage = error?.error?.message || error?.message;
      const data: InfoPopupComponentData = {
        title: this.translateService.instant('API.GAME.POST.ERROR_HEADER'),
        text: errorMessage,
        type: InfoPopupComponentType.info,
      };
      this.dialog.open(InfoPopupComponent, { data });
    }
  };

  quickformatHandler(enableQuickFormat: boolean) {
    if (enableQuickFormat) {
      this.gameForm.addControl(
        'quickFormatConfig',
        this.fb.group({
          bracketType: ['', Validators.required],
          maxParticipants: [2],
          isParticipantsLimit: [true]
        })
      );
    }
  }
}
