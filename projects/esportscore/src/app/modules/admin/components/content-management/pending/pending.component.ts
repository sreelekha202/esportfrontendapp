import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { EsportsLanguageService } from 'esports';

@Component({
  selector: 'app-pending',
  templateUrl: './pending.component.html',
  styleUrls: [ '../content-management.component.scss','./pending.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PendingComponent implements OnInit {

  constructor(public languageService: EsportsLanguageService) { }
  @Input() articlesList;
  @Input() page;
  @Input() activeTabIndex;
  @Output() callBack = new EventEmitter();

  ngOnInit(): void {
  }

  open(id, type) {
    this.callBack.emit({
      type: 'open',
      data: {
        id, type
      }
    });
  }

  viewArticle(id, type ) {
    this.callBack.emit({
      type: type,
      data: {
        id, type
      }
    });
  }

  pageChanged(e) {
    this.callBack.emit({
      type: 'pageChange', 
      data: {
        event: e
      }
    })
  }

}

