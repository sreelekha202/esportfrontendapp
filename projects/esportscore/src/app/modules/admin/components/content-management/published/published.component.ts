import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { EsportsLanguageService } from 'esports';
import { AppHtmlRoutes } from '../../../../../app-routing.model';
@Component({
  selector: 'app-published',
  templateUrl: './published.component.html',
  styleUrls: ['../content-management.component.scss', './published.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PublishedComponent implements OnInit {

  constructor(public languageService: EsportsLanguageService) { }
  AppHtmlRoutes = AppHtmlRoutes;
  @Input() articlesList;
  @Input() page;
  @Input() activeTabIndex;
  @Output() callBack = new EventEmitter();

  ngOnInit(): void {
  }

  open(id, type) {
    this.callBack.emit({
      type: 'open',
      data: {
        id, type
      }
    });
  }

  viewArticle(id, type) {
    this.callBack.emit({
      type: 'view',
      data: {
        id, type
      }
    });
  }

  pageChanged(e) {
    this.callBack.emit({
      type: 'pageChange',
      data: {
        event: e
      }
    });
  }

  updateFeature(e, _id) {
    this.callBack.emit({
      type: 'feature',
      data: {
        event: e,
        id: _id
      }
    });
  }

}

