import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  AfterViewInit,
} from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {
  OptionService,
} from '../../../../core/service';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { MatTabChangeEvent } from '@angular/material/tabs';
import { ActivatedRoute, Router } from '@angular/router';
import {
  InfoPopupComponent,
  InfoPopupComponentData,
  InfoPopupComponentType,
} from '../../../../shared/popups/info-popup/info-popup.component';
import { MatDialog } from '@angular/material/dialog';
import { TranslateService } from '@ngx-translate/core';
import { AppHtmlRoutes } from '../../../../app-routing.model';
import {
  EsportsArticleService,
  IArticle,
  EsportsLanguageService,
  EsportsConstantsService,
  EsportsToastService,
  IPagination,
} from 'esports';
import { fromEvent } from 'rxjs';
import {
  debounceTime,
  distinctUntilChanged,
  filter,
  map,
} from 'rxjs/operators';
import { environment } from '../../../../../environments/environment';

const API = environment.apiEndPoint;

@Component({
  selector: 'app-content-management',
  templateUrl: './content-management.component.html',
  styleUrls: ['./content-management.component.scss'],
})
export class ContentManagementComponent implements OnInit {
  @ViewChild(DatatableComponent) table: DatatableComponent;

  AppHtmlRoutes = AppHtmlRoutes;
  @ViewChild('searchInput') inputName;
  @ViewChild('openModel') openModel;
  @ViewChild('viewModel') viewModel
  @ViewChild('searchInput', { static: false }) set searchInput(element) {
    if (element) {
      this.initializeSearchtracker(element);
      this.checkSearchStatus(element);
    }
  }
  tempArticleList = [];
  activeTabIndex = 0;
  columnsFirstTab = [
    { name: 'Article Name' },
    { name: 'Game' },
    { name: 'Author Name' },
    { name: 'Publish Date' },
    { name: 'Likes' },
    { name: 'Views' },
    { name: '' },
  ];
  columnsSecondTab = [
    { name: 'Article Name' },
    { name: 'Game' },
    { name: 'Author Name' },
    { name: 'Publish Date' },
    { name: 'Likes' },
    { name: 'Views' },
    { name: '' },
  ];
  articleModel: any;
  modelTitle: string;
  modelDesc: string;
  modelHeader: string;
  selectedId: string;
  query: any = {};
  hideBtn = false;
  viewModelBtnDisable = false;
  showRequestEditBtn = false;
  articlesList: IArticle[] = [];
  changeRequestModel: string;
  isLoading = false;
  isFullLoading = false;
  page: IPagination;
  paginationData = {
    page: 1,
    limit: 50,
    sort: { createdDate: -1 },
  };
  tagsList: any = [];

  constructor(
    private articlesService: EsportsArticleService,
    private modalService: NgbModal,
    private activeRoute: ActivatedRoute,
    private eSportsToastService: EsportsToastService,
    private optionService: OptionService,
    private router: Router,
    public dialog: MatDialog,
    private translateService: TranslateService,
    public languageService: EsportsLanguageService
  ) { }

  ngOnInit(): void {
    this.activeRoute.queryParams.subscribe((params) => {
      if (params.activeTab) {
        this.activeTabIndex = +params.activeTab;
        const e = new MatTabChangeEvent();
        e.index = this.activeTabIndex;
        this.onTabChanged(e);
      } else {
        this.activeTabIndex = 0;
      }
    }).unsubscribe();
    this.switchData(0);
    this.fetchData();
  }

  initializeSearchtracker(element) {
    fromEvent(element.nativeElement, 'keyup')
      .pipe(
        map((event: any) => {
          return event.target.value;
        }),
        filter((res) => res.length > 1),
        debounceTime(1000),
        distinctUntilChanged()
      )
      .subscribe((text: string) => {
        const query = JSON.parse(this.query);
        query['searchtext'] = text;
        this.resetPagination();
        this.getArticlesDetailsByStatus(JSON.stringify(query));
      });
  }

  checkSearchStatus(element) {
    fromEvent(element.nativeElement, 'keyup').subscribe(
      (res: any) => {
        if (res.target.value == '') {
          this.resetPagination();
          this.getArticlesDetailsByStatus(this.query);
        }
      },
      (err) => { }
    );
  }

  clearSearch() {
    if (this.inputName.nativeElement.value) {
      this.inputName.nativeElement.value = '';
    }
    this.resetPagination();
    this.getArticlesDetailsByStatus(this.query);
  }

  fetchData = async () => {
    const option = await Promise.all([this.optionService.fetchAllTags()]);
    this.tagsList = option[0]?.data;
  };

  updateFeature(e, id) {
    this.isLoading = true;
    this.articlesService
      .updateArticle(API, id, { isFeature: e.checked })
      .subscribe(
        (res: any) => {
          this.isLoading = false;
          this.query = JSON.stringify({
            articleStatus: EsportsConstantsService.Status.Publish,
          });
          this.getArticlesDetailsByStatus(this.query);
          const afterBlockData: InfoPopupComponentData = {
            title: this.translateService.instant(
              'API.TOURNAMENT.PUT.SUCCESS_HEADER'
            ),
            text: this.translateService.instant(`${res.message}`),
            type: InfoPopupComponentType.info,
          };
          this.dialog.open(InfoPopupComponent, { data: afterBlockData });
        },
        (err: any) => {
          this.isLoading = false;
          const afterBlockData: InfoPopupComponentData = {
            title: this.translateService.instant(
              'API.TOURNAMENT.PUT.ERROR_HEADER'
            ),
            text: err?.error?.message || err?.message,
            type: InfoPopupComponentType.info,
          };
          this.dialog.open(InfoPopupComponent, { data: afterBlockData });
        }
      );
  }

  pageChanged(page): void {
    this.paginationData.page = page;
    this.switchData(this.activeTabIndex);
  }
  switchData(index) {
    switch (index) {
      case 0:
        this.getHighlightedArticles(); // Highlighted article
        break;
      case 1:
        this.query = JSON.stringify({
          articleStatus: EsportsConstantsService.Status.SubmitForApprove,
        });
        this.getArticlesDetailsByStatus(this.query);
        break;
      case 2:
        this.query = JSON.stringify({
          articleStatus: EsportsConstantsService.Status.Publish,
        });
        this.getArticlesDetailsByStatus(this.query);
        break;
      default:
        break;
    }
  }
  resetPage() {
    this.page = {
      totalItems: 0,
      itemsPerPage: 0,
      maxSize: 0,
    };
    this.resetPagination();
  }
  resetPagination() {
    this.paginationData = {
      page: 1,
      limit: 50,
      sort: { createdDate: -1 },
    };
  }
  // Get Articles details
  getArticlesDetailsByStatus(query) {
    this.isLoading = true;
    const pagination = JSON.stringify(this.paginationData);
    this.articlesService
      .getPaginatedArticles(API, { pagination: pagination, query: query })
      .subscribe(
        (res: any) => {
          if (res && res.data) {
            this.page = {
              totalItems: res?.data?.totalDocs,
              itemsPerPage: res?.data?.limit,
              maxSize: 5,
            };
            const response = res.data.docs.map((element) => {
              return {
                id: element?._id,
                title: element?.title,
                fullText: element?.fullText,
                game: element?.gameDetails?.name,
                authorName: element?.authorDetails?.fullName,
                createdDate: element?.createdDate,
                likes: element?.likes,
                isFeature: element?.isFeature,
                views: element?.views,
              };
            });
            this.tempArticleList = [...response];
            this.articlesList = response || [];
          }
          this.isLoading = false;
        },
        (err) => {
          this.isLoading = false;
        }
      );
  }
  // get Trending Articles
  getHighlightedArticles() {
    this.isLoading = true;
    this.articlesService.getTrendingPosts(API).subscribe(
      (res: any) => {
        if (res && res.data) {
          const response = res.data.map((element) => {
            return {
              id: element._id,
              title: element.title,
              game: element.gameDetails?.name,
              fullText: element.fullText,
              authorName: element.authorDetails.fullName,
              createdDate: element.createdDate,
              likes: element.likes,
              views: element.views,
            };
          });
          this.tempArticleList = [...response];
          this.articlesList = response || [];
        }
        this.isLoading = false;
      },
      (err) => {
        this.isLoading = false;
      }
    );
  }

  /**
   * Get the article list based on Tab selection
   * @param tabChangeEvent : Event on Change of Tab
   */
  onTabChanged = (tabChangeEvent: MatTabChangeEvent): void => {
    this.activeTabIndex = tabChangeEvent.index;
    this.router.navigate(['.'], {
      relativeTo: this.activeRoute,
      queryParams: { activeTab: tabChangeEvent.index },
    });
    this.articlesList = [];
    this.resetPage();
    this.switchData(tabChangeEvent.index);
  };

  /**
   * Open model to view the articles
   * @param model : model
   * @param Id : Set Selected Id
   * @param Type : Check type of model to hide/show the button
   */
  viewArticle(model, Id, Type) {
    this.selectedId = Id;
    this.viewModelBtnDisable = Type === 'view' ? true : false;
    this.modalService.open(model, {
      size: 'lg',
      centered: true,
      scrollable: true,
      windowClass: 'custom-modal-content',
    });
    this.getArticleById(Id);
  }

  // get articles details based on Id
  getArticleById(Id) {
    this.articlesService.getArticleByID(API, Id).subscribe(
      (data: any) => {
        this.articleModel = data.data[0];
      },
      (err) => { }
    );
  }

  /**
   * Open model pop on button click
   * @param content -content
   */
  open(content, Id, modelType) {
    this.selectedId = Id;
    this.hideBtn = true;
    this.showRequestEditBtn = false;
    // Check to open model and configure inner content
    if (modelType === 'requestEdit') {
      this.showRequestEditBtn = true;
      this.setModelConfiguration(
        EsportsConstantsService.Model.RequestEdit.header,
        EsportsConstantsService.Model.RequestEdit.title,
        EsportsConstantsService.Model.RequestEdit.description
      );
    } else {
      this.setModelConfiguration(
        EsportsConstantsService.Model.Delete.header,
        EsportsConstantsService.Model.Delete.title,
        EsportsConstantsService.Model.Delete.description
      );
    }
    this.modalService.open(content, {
      size: 'lg',
      centered: true,
      scrollable: true,
      windowClass: 'custom-modal-content',
    });
  }

  /**
   * Confiure model
   * @param header : header value
   * @param title : Title value
   * @param description : Description value
   */
  setModelConfiguration(header, title, description) {
    this.modelTitle = this.translateService.instant(title);
    this.modelDesc = this.translateService.instant(description);
    this.modelHeader = this.translateService.instant(header);
  }

  // Abort Articles based on Selected Id
  trash() {
    this.modalService.dismissAll();
    this.isFullLoading = true;
    const formValues = {
      articleStatus: EsportsConstantsService.Status.Delete,
    };
    this.articlesService
      .updateArticle(API, this.selectedId, formValues)
      .subscribe(
        (res) => {
          if (res) {
            this.SuccessModel(
              EsportsConstantsService.Model.Article_Deleted.header,
              EsportsConstantsService.Model.Article_Deleted.description
            );
            // Remove Object from list based on Index
            const articleObject = this.articlesList;
            this.articlesList = articleObject.filter(
              (resp) => resp.id !== this.selectedId
            );
            this.hideBtn = false;
          }
          this.isFullLoading = false;
        },
        (err) => {
          this.isFullLoading = false;
        }
      );
  }

  submitRequestEdit() {
    const formValues = {
      articleStatus: EsportsConstantsService.Status.RequestEdit,
      changeRequest: this.changeRequestModel,
    };
    this.updateArticles(formValues);
  }

  approvePublish() {
    const formValues = {
      articleStatus: EsportsConstantsService.Status.Publish,
    };
    this.updateArticles(formValues);
  }

  // Update Articles
  updateArticles(formValues) {
    if (this.selectedId) {
      this.modalService.dismissAll();
      this.isFullLoading = true;
      this.articlesService
        .updateArticle(API, this.selectedId, formValues)
        .subscribe(
          (res) => {
            this.SuccessModel(
              EsportsConstantsService.Success.Title,
              EsportsConstantsService.Success.ArticlesUpdateSuccess
            );
            // Remove Object from list based on Index
            const articleObject = this.articlesList;
            this.articlesList = articleObject.filter(
              (resp) => resp.id !== this.selectedId
            );
            this.hideBtn = false;
            this.isFullLoading = false;
          },
          (error) => {
            this.eSportsToastService.showError(
              this.translateService.instant(
                EsportsConstantsService.APIError.ArticlesUpdateError
              )
            );
            this.isFullLoading = false;
          }
        );
    }
  }
  // Display the model pop up for success message
  SuccessModel(title, text) {
    const data: InfoPopupComponentData = {
      title: this.translateService.instant(title),
      text: this.translateService.instant(text),
      type: InfoPopupComponentType.info,
    };
    this.dialog.open(InfoPopupComponent, { data });
  }
  callBack(e) {
    switch (e.type) {
      case 'open': {
        this.open(this.openModel, e.data.id, e.data.type);
        break;
      }
      case 'view': {
        this.viewArticle(this.viewModel, e.data.id, e.data.type);
        break;
      }
      case 'check': {
        this.viewArticle(this.viewModel, e.data.id, e.data.type);
        break;
      }
      case 'delete': {
        this.open(this.openModel, e.data.id, e.data.type);
        break;
      }
      case 'pageChange': {
        this.pageChanged(e.data.event);
        break;
      }
      case 'feature': {
        this.updateFeature(e.data.event, e.data.id)
      }
    }
  }
}
