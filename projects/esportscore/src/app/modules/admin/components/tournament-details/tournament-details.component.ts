import {
  Component,
  EventEmitter,
  Inject,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  ViewChild,
  ElementRef,
} from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { fromEvent } from 'rxjs';
import {
  AdminService,
  TournamentService,
  GameService,
} from '../../../../core/service';
import { EsportsConstantsService, EsportsToastService } from 'esports'
import {
  debounceTime,
  distinctUntilChanged,
  filter,
  map,
} from 'rxjs/operators';
import { AppHtmlRoutes } from '../../../../app-routing.model';
import { IPagination } from '../../../../shared/models';
import { FormGroup, FormBuilder } from '@angular/forms';
import {
  InfoPopupComponent,
  InfoPopupComponentData,
  InfoPopupComponentType,
} from '../../../../shared/popups/info-popup/info-popup.component';

import { DatatableComponent } from '@swimlane/ngx-datatable';
import { MatDialog } from '@angular/material/dialog';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-tournament-details',
  templateUrl: './tournament-details.component.html',
  styleUrls: ['./tournament-details.component.scss'],
})
export class TournamentDetailsComponent implements OnInit, OnChanges {
  platformList: any = [];
  @Input() activePage: any;
  @Input() checkIconShow: boolean;
  @Input() deleteIconShow: boolean;
  @Input() editIconShow: boolean;
  @Input() isFeatureEditable: boolean;
  @Input() isGWB: boolean;
  @Input() manageIconShow: boolean;
  @Input() page: IPagination;
  @Input() tournamentList = [];
  @Input() type: any;
  @Input() viewIconShow: boolean;
  @Input() activeTab: number;
  @Input() isExportEnable:boolean;
  @Output() currentPage = new EventEmitter();
  @Output() isModified = new EventEmitter();
  @Input() IAMAFFTournament: boolean = false;
  @Output() modifiedOptions = new EventEmitter();
  statusForPaidPrize = '';
  @ViewChild('searchInput') inputName;
  @ViewChild(DatatableComponent) table: DatatableComponent;
  @ViewChild('searchInput', { static: true }) searchInput: ElementRef;
  AppHtmlRoutes = AppHtmlRoutes;
  abortModelType = 'delete';
  hideBtn = false;
  showRequestEditBtn = false;
  showRequestTrashBtn = false;
  viewModelBtnDisable = false;
  tournamentToDelete: '';
  changeRequestModel: string;
  modelDesc: string;
  modelHeader: string;
  modelTitle: string;
  status: number;
  articleModel: any;
  tournamentDetails: any;
  tournamentId: any;

  optionForm: FormGroup;
  rows: any = [];
  tempTournamentList = [];

  columnsFirstTab = [
    { name: 'Tournament Name' },
    { name: 'Registration' },
    { name: 'Game' },
    { name: 'Region' },
    { name: 'Type' },
    { name: 'Start Date' },
    { name: 'End Date' },
    { name: 'Participant(s)' },
    { name: '' },
  ];

  constructor(
    @Inject(DOCUMENT) private document: Document,
    private adminService: AdminService,
    private modalService: NgbModal,
    private tournamentService: TournamentService,
    private translateService: TranslateService,
    public dialog: MatDialog,
    private gameService: GameService,
    public eSportsToastService: EsportsToastService,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.fetchData();
    this.optionForm = this.fb.group({
      IAMAFFTournament: [false],
    });
    this.initializeSearchtracker();
    this.checkSearchStatus();
  }

  ngOnChanges(simpleChanges: SimpleChanges): void {
    this.rows = [];
    this.tempTournamentList = [];

    if (this.tournamentList?.length) {
      this.rows = [...this.tournamentList];
      this.tempTournamentList = [...this.tournamentList];
    }
  }
  IAMAFFTournamentHandler = async (check) => {
    this.modifiedOptions.emit(this.optionForm.value);
  };
  initializeSearchtracker() {
    if (this.activeTab == 0) {
      this.status = 1;
    } else if (this.activeTab == 1) {
      this.status = 0;
    } else if (this.activeTab == 3) {
      this.status = 5;
    } else if (this.activeTab == 4) {
      this.status = 3;
    } else {
      this.status = this.activeTab;
    }
    fromEvent(this.searchInput.nativeElement, 'keyup')
      .pipe(
        map((event: any) => {
          return event.target.value;
        }),
        filter((res) => res.length > 1),
        debounceTime(1000),
        distinctUntilChanged()
      )
      .subscribe((text: string) => {
        this.isModified.emit({
          text: text,
          page: 1,
          status: this.status,
        });
      });
  }
  /**
   * method to emit the feature value change to parent
   * @param e
   * @param _id
   */
  updateValue(e, _id) {
    this.isModified.emit({ type: 'feature', value: e.checked, id: _id });
    this.rows = [];
  }

  fetchData = async () => {
    const option = await this.gameService.fetchPlatforms().toPromise();
    this.platformList = option.data;
  };
  getTournament(Id) {
    const tournament = this.rows.find((el) => el._id == Id);
    this.tournamentToDelete = tournament.name;
  }
  pageChanged($event) {
    this.currentPage.emit($event);
  }

  checkSearchStatus() {
    fromEvent(this.searchInput.nativeElement, 'keyup').subscribe(
      (res: any) => {
        if (res.target.value == '') {
          this.isModified.emit({ type: 'update', value: this.activeTab });
        }
      },
      (err) => {}
    );
  }

  clearSearch() {
    if (this.inputName.nativeElement.value) {
      this.inputName.nativeElement.value = '';
    }
    this.isModified.emit({ type: 'update', value: this.activeTab });
  }
  // search the tournament details
  searchByValueFilter(event) {
    const value = event.target.value.toLowerCase().trim();
    // get the amount of columns in the table
    const count = this.columnsFirstTab.length;
    const keys = Object.keys(this.tempTournamentList[0]);
    // assign filtered matches to the active datatable
    const filterData = this.tempTournamentList.filter((item) => {
      // iterate through each row's column data
      for (let i = 0; i < count; i++) {
        // check for a match
        if (
          (item[keys[i]] &&
            item[keys[i]].toString().toLowerCase().indexOf(value) !== -1) ||
          !value
        ) {
          // found match, return true to add to result set
          return true;
        }
      }
    });
    this.rows = [...filterData];
    // Whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }
  updateValueGWB(e, _id) {
    if (e.checked == true) {
      this.isModified.emit({
        type: 'GWB',
        value: 2,
        id: _id,
        tabIndex: this.activeTab,
      });
      this.rows = [];
    } else if (e.checked == false) {
      this.isModified.emit({
        type: 'GWB',
        value: 0,
        id: _id,
        tabIndex: this.activeTab,
      });
      this.rows = [];
    }
  }
  /**
   * Open model pop on button click
   * @param content -content
   */
  open(content, Id, modelType) {
    this.getTournament(Id);
    this.tournamentId = Id;
    this.hideBtn = true;
    this.showRequestEditBtn = false;
    // Check to open model and configure inner content
    if (modelType === 'requestEdit') {
      this.showRequestEditBtn = true;
      this.showRequestTrashBtn = false;
      this.setModelConfiguration(
        EsportsConstantsService.Model.RequestEdit.header,
        EsportsConstantsService.Model.RequestEdit.title,
        EsportsConstantsService.Model.RequestEdit.description,
        modelType
      );
    } else if (modelType === 'trash') {
      this.showRequestEditBtn = true;
      this.showRequestTrashBtn = true;
      this.setModelConfiguration(
        EsportsConstantsService.Model.TournamentTrash.header,
        EsportsConstantsService.Model.TournamentTrash.title,
        EsportsConstantsService.Model.TournamentTrash.description,
        modelType
      );
    } else {
      this.setModelConfiguration(
        EsportsConstantsService.Model.TournamentAbort.header,
        EsportsConstantsService.Model.TournamentAbort.title1,
        EsportsConstantsService.Model.TournamentAbort.description,
        modelType
      );
    }
    this.modalService.dismissAll();
    this.modalService.open(content, {
      size: 'lg',
      centered: true,
      scrollable: true,
      windowClass: 'custom-modal-content',
    });
  }

  /**
   * Open model to view the Tournament
   * @param model : model
   * @param Id : Set Selected Id
   * @param Type : Check type of model to hide/show the button
   */
  viewTournament(model, Id, Type) {
    this.tournamentId = Id;
    this.viewModelBtnDisable = Type === 'view' ? true : false;
    this.modalService.open(model, {
      size: 'lg',
      centered: true,
      scrollable: true,
      windowClass: 'custom-modal-content',
    });
    this.getTournamentById(Id);
  }
  // get tournament details based on Id
  getTournamentById(Id) {
    this.tournamentService.getTournament(Id).subscribe(
      (res: any) => {
        this.tournamentDetails = res.data;
        if (this.tournamentDetails?.isCharged == true) {
          this.statusForPaidPrize = 'Paid';
        } else {
          this.statusForPaidPrize = 'Unpaid';
        }
      },
      (err) => {}
    );
  }

  // Abort Tournament based on Selected Id
  abort() {
    const formValues = {
      tournamentId: this.tournamentId,
    };

    this.tournamentService.abortTournament(formValues).subscribe(
      (res) => {
        if (res) {
          this.setModelConfiguration(
            EsportsConstantsService.Model.TournamentAbortSuccess.header,
            EsportsConstantsService.Model.TournamentAbortSuccess.title,
            EsportsConstantsService.Model.TournamentAbortSuccess.description,
            this.abortModelType
          );
          // Remove Object from list based on ID
          this.isModified.emit({
            type: 'abort',
            value: true,
            id: this.tournamentId,
          });
          const tournamentObject = this.tournamentList;
          this.tournamentList = tournamentObject.filter(
            (resp) => resp._id !== this.tournamentId
          );
          this.hideBtn = false;
        } else {
          this.eSportsToastService.showError(
            this.translateService.instant(
              EsportsConstantsService.APIError.TournamentUpdateError
            )
          );
        }
      },
      (err: any) => {
        const afterBlockData: InfoPopupComponentData = {
          title: this.translateService.instant(
            'API.TOURNAMENT.PUT.SUCCESS_HEADER'
          ),
          text:
            this.translateService.instant(err.error.messageCode) ||
            err.error.message,
          type: InfoPopupComponentType.info,
        };
        this.dialog.open(InfoPopupComponent, { data: afterBlockData });
      }
    );
  }

  // Trash the tournament
  trash() {
    const formValues = {
      tournamentStatus: EsportsConstantsService.Status.Delete,
      changeRequest: this.changeRequestModel,
    };
    this.updateTournamentById(formValues);
  }

  // Approve/Publish tournament
  approveTournament() {
    const formValues = {
      tournamentStatus: EsportsConstantsService.Status.Publish,
    };
    this.updateTournamentById(formValues);
  }

  // Change request
  requestEdit() {
    const formValues = {
      tournamentStatus: EsportsConstantsService.Status.RequestEdit,
      changeRequest: this.changeRequestModel,
    };
    this.updateTournamentById(formValues);
  }

  // Update tournament by Id
  updateTournamentById(formValues) {
    if (this.tournamentId) {
      this.tournamentService
        .updateTournament(formValues, this.tournamentId)
        .subscribe(
          (res: any) => {
            this.modalService.dismissAll();
            this.changeRequestModel = '';
            this.isModified.emit({ type: 'update', value: this.activeTab });
            this.SuccessModel(
              this.translateService.instant(
                'API.TOURNAMENT.PUT.SUCCESS_HEADER'
              ),
              res.message
            );
            const tournamentObject = this.tournamentList.filter(
              (resp) => resp._id !== this.tournamentId
            );
            this.tournamentList = [...tournamentObject];
          },
          (error) => {
            this.eSportsToastService.showError(
              this.translateService.instant(
                EsportsConstantsService.APIError.TournamentUpdateError
              )
            );
          }
        );
    }
  }

  /**
   * Confiure model
   * @param header : header value
   * @param title : Title value
   * @param description : Description value
   */
  setModelConfiguration(header, title, description, modelType) {
    if (modelType == 'delete') {
      this.modelTitle =
        this.translateService.instant(title) +
        '"' +
        `${this.tournamentToDelete}` +
        '"';
      this.modelDesc = this.translateService.instant(description);
      this.modelHeader = this.translateService.instant(header);
    } else {
      this.modelTitle = this.translateService.instant(title);
      this.modelDesc = this.translateService.instant(description);
      this.modelHeader = this.translateService.instant(header);
    }
  }

  // Display the model pop up for success message
  SuccessModel(title, text) {
    const data: InfoPopupComponentData = {
      title: this.translateService.instant(title),
      text: this.translateService.instant(text),
      type: InfoPopupComponentType.info,
    };
    this.dialog.open(InfoPopupComponent, { data });
  }

  export(row) {
    this.fetchParticipants(row._id, row);
  }

  getWinnersList = async () => {};

  exportToCSV(data, row, winnerList) {
    const csvData = [
      {
        'Number of players joined': data.length,
        'Match Results': '',
        'Winners list': winnerList
          .map((item) => {
            return item.winnerUser.fullName;
          })
          .toString(),
        Game: row.game,
        'Start Date': row.startDate,
        'End Date': row.endDate ? row.endDate : '',
      },
    ];
    this.JSONToCSVConvertor(csvData, '', true, {});
  }

  JSONToCSVConvertor(JSONData, ReportTitle, ShowLabel, tournamentDetail) {
    const arrData =
      typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;
    let row = '';
    let CSV = '';

    if (ShowLabel) {
      for (const i in tournamentDetail) {
        row += i + ',';
      }
      //row = row.slice(0, -1);
      CSV += row;
      row = '';
      for (const i in tournamentDetail) {
        row += '"' + tournamentDetail[i] + '",';
      }
      row.slice(0, row.length - 1);
      CSV += row + '\r\n';

      if (ReportTitle) {
        CSV += ReportTitle + '\r\n\n';
      }
      row = '';
      for (const index in arrData[0]) {
        row += index + ',';
      }
      row = row.slice(0, -1);
      CSV += row + '\r\n';
    }

    for (let i = 0; i < arrData.length; i++) {
      let row = '';
      for (const index in arrData[i]) {
        if (index === 'Mobile Number') {
          row += '"=""' + arrData[i][index] + '""",';
        } else {
          row += '"' + arrData[i][index] + '",';
        }
      }
      row.slice(0, row.length - 1);
      CSV += row + '\r\n';
    }

    if (CSV == '') {
      this.eSportsToastService.showError('Invalid data');
      return;
    }
    const universalBOM = '\uFEFF';
    let fileName = 'STC_PARTICIPANTS';
    fileName += ReportTitle.replace(/ /g, '_');
    const uri =
      'data:text/csv;charset=utf-8,' + encodeURIComponent(universalBOM + CSV);
    const link = this.document.createElement('a');
    link.href = uri;
    link.download = fileName + '.csv';
    this.document.body.appendChild(link);
    link.click();
    this.document.body.removeChild(link);
  }

  fetchParticipants = async (id, row) => {
    try {
      // this.isLoaded = false;
      const encodeUrl = `?query=${encodeURIComponent(
        JSON.stringify({ tournamentId: id })
      )}`;
      const participants = await this.tournamentService
        .getParticipants(encodeUrl)
        .toPromise();

      const query = {
        filter: {
          tournament: id,
          isAward: true,
          awardPosition: 3,
        },
        option: {
          sort: { awardPosition: 1 },
        },
      };
      const winnerList: any = await this.adminService
        .getTournamentWinners(query)
        .toPromise();
      this.exportToCSV(participants?.data, row, winnerList.data);
      // this.participarntDa = participants?.data;
      // this.participantsListPending = participants?.data.filter(
      //   (item) => !item?.participantStatus
      // );

      // this.participantsListApproved = participants?.data.filter(
      //   (item) => item?.participantStatus === 'approved'
      // );

      // this.participantsListRejected = participants?.data.filter(
      //   (item) => item?.participantStatus === 'rejected'
      // );
      // this.setParticipant();
      // this.isLoaded = true;
    } catch (error) {
      // this.isLoaded = true;
      // this.eSportsToastService.showError(error?.error?.message || error?.message);
    }
  };
}
