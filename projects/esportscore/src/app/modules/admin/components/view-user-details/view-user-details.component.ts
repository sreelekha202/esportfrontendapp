import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

import { TournamentService } from '../../../../core/service';
import { EsportsUserService, EsportsConstantsService } from 'esports';

import {
  InfoPopupComponent,
  InfoPopupComponentData,
  InfoPopupComponentType,
} from '../../../../shared/popups/info-popup/info-popup.component';

import { AppHtmlRoutes } from '../../../../app-routing.model';
import { IPagination } from '../../../../shared/models';
import { IReward } from '../../../../shared/models/reward';

import { MatDialog } from '@angular/material/dialog';
import { MatTabChangeEvent } from '@angular/material/tabs';
import { TranslateService } from '@ngx-translate/core';

import { environment } from '../../../../../environments/environment';

const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;

@Component({
  selector: 'app-view-user-details',
  templateUrl: './view-user-details.component.html',
  styleUrls: ['./view-user-details.component.scss'],
})
export class ViewUserDetailsComponent implements OnInit {
  constructor(
    private activatedRoute: ActivatedRoute,
    private location: Location,
    private tournamentService: TournamentService,
    private translateService: TranslateService,
    private userService: EsportsUserService,
    public dialog: MatDialog
  ) {}
  AppHtmlRoutes = AppHtmlRoutes;

  isEO = true;
  isLoading = false;
  isUserActive = false;
  isUserAuthor = false;
  isUserInfluencer = false;
  isUserMarkEO = false;
  isUserVerified = false;
  userStatus: number;
  page: IPagination;
  userId: string;
  userObject: any;

  createdTournamentList: any[] = [];
  joinedTournamentList: any[] = [];
  rewardList: IReward[] = [];

  columnsFirstTab = [
    { name: 'Id' },
    { name: 'Full Name' },
    { name: 'Email' },
    { name: 'Phone Number' },
    { name: 'Sign Up From' },
    { name: 'Member Since' },
  ];

  rewardTransColumnTab = [
    { name: 'Date' },
    { name: 'Balance Reward' },
    { name: 'Reward Type' },
    { name: 'Description' },
    { name: 'Status' },
    { name: 'Expiry Date' },
  ];

  tournamentColumnTab = [
    { name: 'Tournament Name' },
    { name: 'Registration' },
    { name: 'Game' },
    { name: 'Region' },
    { name: 'Type' },
    { name: 'Start Date' },
    { name: '' },
  ];

  pagination = {
    page: 1,
    limit: 50,
    sort: 'startDate',
    projection: [
      '_id',
      'name',
      'description',
      'startDate',
      'regionsAllowed',
      'tournamentType',
      'slug',
    ],
  };

  ngOnInit(): void {
    this.getUserIdFromActiveRoute();
  }

  // Get the user details with all other tabs
  getUserIdFromActiveRoute() {
    this.activatedRoute.params.subscribe((params) => {
      this.userId = params['id'];

      if (this.userId) {
        this.getUserDetailsById();
      }
    });
  }

  getJoinedTournamentQuery() {
    return {
      pagination: JSON.stringify(this.pagination),
      query: JSON.stringify({
        userId: this.userId,
      }),
    };
  }

  pageChanged(page): void {
    this.pagination.page = page;
    this.getJoinedTournamentDetails();
  }

  // Get Joined tournament details
  async getJoinedTournamentDetails() {
    this.isLoading = true;

    const payload = await this.getJoinedTournamentQuery();

    const tournament = await this.tournamentService
      .getJoinedTournamentsByUser(payload)
      .toPromise();

    this.page = {
      itemsPerPage: tournament?.data?.limit,
      maxSize: 5,
      totalItems: tournament?.data?.totalDocs,
    };

    this.joinedTournamentList =
      tournament?.data?.docs.map(
        (element) => ({
          game: element?.game?.name,
          id: element.tournament._id,
          region: element?.tournament?.regionsAllowed,
          registration: element.tournament.isPaid,
          slug: element.tournament.slug,
          startDate: element.tournament.startDate,
          tournamentName: element.tournament.name,
          type: element?.tournament?.tournamentType,
        }),
        (err) => {
          this.isLoading = false;
        }
      ) || [];
    this.isLoading = false;
  }

  // Get Created tournament details
  getCreatedTournamentDetails() {
    this.isLoading = true;

    const query = JSON.stringify({
      createdBy: this.userId,
      isFinished: false,
      tournamentStatus: 'publish',
    });

    this.tournamentService.getTournaments({ query }).subscribe(
      (data) => {
        this.isLoading = false;
        const tournamentDetails = data['data'].slice();

        const response = tournamentDetails.map((element) => ({
          game: element.gameDetail?.name,
          id: element._id,
          region: element.regionsAllowed,
          registration: element.isPaid,
          startDate: element.startDate,
          tournamentName: element.name,
          type: element.tournamentType,
          slug: element?.slug,
        }));
        this.createdTournamentList = response || [];
      },
      (err) => {
        this.isLoading = false;
      }
    );
  }

  getRewardTransaction() {
    this.isLoading = true;
    const query = JSON.stringify({ user: this.userId });

    this.userService.getRewardTransactionByUserId(API, { query }).subscribe(
      (res) => {
        this.isLoading = false;
        const response = res.data.map((element) => {
          if (element.status == 0) {
            status = this.translateService.instant(
              'PROFILE.TRANSACTIONS.REDEEMED'
            );
          } else if (element.status == 1) {
            status = this.translateService.instant(
              'PROFILE.TRANSACTIONS.NOT_REDEEMED'
            );
          } else if (element.status == 2) {
            status = this.translateService.instant(
              'PROFILE.TRANSACTIONS.PARTIAL_REDEEMED'
            );
          } else {
            status = this.translateService.instant(
              'PROFILE.TRANSACTIONS.EXPIRED'
            );
          }
          return {
            balanceReward: element.reward_value,
            date: element.createdOn,
            description: element.description,
            expiryDate: element.expiredOn,
            rewardId: element._id,
            rewardType:
              element.type.charAt(0).toUpperCase() +
              element.type.slice(1).toLowerCase(),
            status,
          };
        });
        this.rewardList = response || [];
      },
      (err) => {
        this.isLoading = false;
      }
    );
  }

  getUserDetailsById() {
    this.isLoading = true;
    this.userService.getUserDetails(API, this.userId).subscribe(
      (res) => {
        this.isLoading = false;
        this.userObject = res.data[0];
        this.userStatus = res?.data[0]?.Status;
        if (this.userObject && this.userObject.Status) {
          this.isUserActive = this.userObject.Status === 1;
          this.isUserVerified = this.userObject.isVerified === 1;
          this.isUserInfluencer = this.userObject.isInfluencer === 1;
          this.isUserAuthor = this.userObject.isAuthor === 1;
          this.isUserMarkEO = this.userObject.isEO === 1;
        }
        this.getUserPrefenrece();
      },
      (err) => {
        this.isLoading = false;
      }
    );
  }

  getUserPrefenrece() {
    this.isLoading = true;
    // Get User Preferences
    this.userService.getUserPrefernces(API, this.userId).subscribe(
      (res) => {
        if (res && res.data) {
          this.getRewardTransaction();
          Object.assign(this.userObject, res.data);
        }
        this.isLoading = false;
      },
      (err) => {
        this.isLoading = false;
      }
    );
  }

  goBack() {
    this.location.back();
  }

  onBlockUser() {
    const blockUserData: InfoPopupComponentData = {
      title: this.translateService.instant(
        EsportsConstantsService.Model.Block_Confirmation.header
      ),
      text: this.translateService.instant(
        EsportsConstantsService.Model.Block_Confirmation.title
      ),
      type: InfoPopupComponentType.confirm,
      btnText: this.translateService.instant(
        EsportsConstantsService.Model.Block_Confirmation.button
      ),
    };

    const dialogRef = this.dialog.open(InfoPopupComponent, {
      data: blockUserData,
    });

    dialogRef.afterClosed().subscribe((confirmed) => {
      if (confirmed) {
        this.blockUser();
      }
    });
  }

  blockUser() {
    this.isLoading = true;
    const formValue = {
      Status: 2,
    };

    this.userService.updateUser(API, formValue, this.userId).subscribe(
      (res) => {
        this.isLoading = false;
        this.isUserActive = !this.isUserActive;
        const userName = this.userObject?.fullName;
        const afterBlockData: InfoPopupComponentData = {
          title: this.translateService.instant(
            EsportsConstantsService.Model.Block.title
          ),
          text: this.translateService.instant(
            EsportsConstantsService.Model.Block.description
          ),
          type: InfoPopupComponentType.info,
        };
        this.dialog.open(InfoPopupComponent, { data: afterBlockData });
      },
      (err) => {
        this.isLoading = false;
      }
    );
  }

  onUnBlockUser() {
    this.isLoading = true;

    const formValue = {
      Status: 1,
    };

    this.userService.updateUser(API, formValue, this.userId).subscribe(
      (res) => {
        this.isLoading = false;
        this.isUserActive = !this.isUserActive;
        const afterBlockData: InfoPopupComponentData = {
          title: this.translateService.instant(
            EsportsConstantsService.Model.Unblock.title
          ),
          text: this.translateService.instant(
            EsportsConstantsService.Model.Unblock.description
          ),
          type: InfoPopupComponentType.info,
        };
        this.dialog.open(InfoPopupComponent, { data: afterBlockData });
      },
      (err) => {
        this.isLoading = false;
      }
    );
  }

  /**
   * Method to mark as Influencer/remove influencer
   * @param boolVal : Mark user account as Influencer/UnInfluencer
   */
  onInfluencer(boolVal) {
    this.isLoading = true;
    const formValue = {
      isInfluencer: boolVal ? 0 : 1,
    };
    this.userService.updateUser(API, formValue, this.userId).subscribe(
      (res) => {
        this.isLoading = false;
        this.isUserInfluencer = !this.isUserInfluencer;
        if (boolVal) {
          this.SuccessModel(
            EsportsConstantsService.Model.UserInfluencer.title,
            EsportsConstantsService.Model.UserInfluencer.description
          );
        } else {
          this.SuccessModel(
            EsportsConstantsService.Model.UserUnInfluencer.title,
            EsportsConstantsService.Model.UserUnInfluencer.description
          );
        }
      },
      (err) => {
        this.isLoading = false;
      }
    );
  }

  onTabChanged = (tabChangeEvent: MatTabChangeEvent): void => {
    switch (tabChangeEvent.index) {
      case 0: {
        this.getRewardTransaction();
        break;
      }

      case 1: {
        this.getJoinedTournamentDetails();
        break;
      }

      case 2: {
        this.getCreatedTournamentDetails();
        break;
      }
    }
  };

  /**
   * Method to verified/Unverified the account
   * @param boolVal : Mark user account ad verfied or unverified
   */
  onVerify(boolVal) {
    this.isLoading = true;

    const formValue = {
      isVerified: boolVal ? 0 : 1,
    };

    this.userService.updateUser(API, formValue, this.userId).subscribe(
      (res) => {
        this.isLoading = false;
        this.isUserVerified = !this.isUserVerified;
        this.userService.refreshCurrentUser(API, TOKEN);
        if (boolVal) {
          this.SuccessModel(
            EsportsConstantsService.Model.UserVerified.title,
            EsportsConstantsService.Model.UserVerified.description
          );
        } else {
          this.SuccessModel(
            EsportsConstantsService.Model.UserUnVerified.title,
            EsportsConstantsService.Model.UserUnVerified.description
          );
        }
      },
      (err) => {
        this.isLoading = false;
      }
    );
  }

  onEO(boolVal) {
    this.isLoading = true;

    const formValue = {
      isEO: boolVal ? 0 : 1,
    };

    this.userService.updateUser(API, formValue, this.userId).subscribe(
      (res) => {
        this.isLoading = false;
        this.isUserMarkEO = !this.isUserMarkEO;
        if (boolVal) {
          this.SuccessModel(
            EsportsConstantsService.Model.UserMarkEO.title,
            EsportsConstantsService.Model.UserMarkEO.description
          );
        } else {
          this.SuccessModel(
            EsportsConstantsService.Model.UserUnMarkEO.title,
            EsportsConstantsService.Model.UserUnMarkEO.description
          );
        }
      },
      (err) => {
        this.isLoading = false;
      }
    );
  }

  /**
   * Method to mark as Author/UnAuthor
   * @param boolVal : Mark user account ad Author or Unauthor
   */
  onAuthor(boolVal) {
    this.isLoading = true;

    const formValue = {
      isAuthor: boolVal ? 0 : 1,
    };

    this.userService.updateUser(API, formValue, this.userId).subscribe(
      (res) => {
        this.isLoading = false;
        this.isUserAuthor = !this.isUserAuthor;
        if (boolVal) {
          this.SuccessModel(
            EsportsConstantsService.Model.UserAuthor.title,
            EsportsConstantsService.Model.UserAuthor.description
          );
        } else {
          this.SuccessModel(
            EsportsConstantsService.Model.RevokeAuthor.title,
            EsportsConstantsService.Model.RevokeAuthor.description
          );
        }
      },
      (err) => {
        this.isLoading = false;
      }
    );
  }

  /**
   * Model pop up
   * @param title : Display as header
   * @param text : Display as body text
   */
  SuccessModel(title, text) {
    const data: InfoPopupComponentData = {
      text: this.translateService.instant(text),
      title: this.translateService.instant(title),
      type: InfoPopupComponentType.info,
    };

    this.dialog.open(InfoPopupComponent, { data });
  }
}
