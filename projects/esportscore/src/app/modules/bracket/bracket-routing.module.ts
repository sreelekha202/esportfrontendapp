import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UpsertBracketComponent } from './upsert-bracket/upsert-bracket.component';
import { BracketPreviewComponent } from './bracket-preview/bracket-preview.component';
import { BracketComponent } from './bracket.component';

const routes: Routes = [
  { path: '', component: BracketComponent },
  { path: 'create', component: UpsertBracketComponent },
  { path: 'preview', component: BracketPreviewComponent },
  { path: 'edit/:id', component: UpsertBracketComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BracketRoutingModule {}
