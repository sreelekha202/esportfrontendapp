import { Component, OnInit } from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl,
  ValidatorFn,
  AbstractControl,
} from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

import { TranslateService } from '@ngx-translate/core';
import { BracketService } from '../../../core/service';
import { EsportsLanguageService, EsportsToastService } from 'esports';
@Component({
  selector: 'app-upsert-bracket',
  templateUrl: './upsert-bracket.component.html',
  styleUrls: ['./upsert-bracket.component.scss'],
})
export class UpsertBracketComponent implements OnInit {
  bracket: FormGroup;
  navUrl = '../preview';
  maximumParticipants = 1024;
  isProcessing = false;
  bracketList = ['single', 'double', 'round_robin', 'battle_royale'];
  constructor(
    private bracketService: BracketService,
    private fb: FormBuilder,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private eSportsToastService: EsportsToastService,
    private translateService: TranslateService,
    private languageService: EsportsLanguageService
  ) {}

  ngOnInit(): void {
    this.languageService.language.subscribe((lang) =>
      this.translateService.use(lang || 'en')
    );
    this.bracket = this.fb.group({
      id: [''],
      name: ['', Validators.required],
      bracketType: ['single', Validators.required],
      maximumParticipants: [
        2,
        Validators.compose([
          Validators.required,
          Validators.min(2),
          Validators.max(1024),
          this.teamInGroupValidation('maximumParticipants').bind(this),
        ]),
      ],
      noOfSet: [3],
    });
    const bracket = this.bracketService.getBracketData();
    const id = this.activeRoute.snapshot.params.id;
    this.navUrl = bracket?.id || id ? '../' + this.navUrl : this.navUrl;
    if (bracket) {
      this.updateParticipantLimit(bracket?.bracketType);
      this.bracket.patchValue({
        ...bracket,
      });
    } else if (id) {
      this.fetchBracket(id);
    }
  }

  /**
   * Fatch bracket details By Id
   * @param id  Bracket ID
   */
  fetchBracket = async (id) => {
    try {
      this.isProcessing = true;
      const bracket = await this.bracketService.getBracketByID(id);
      this.updateParticipantLimit(bracket.data[0]?.bracketType);
      this.bracket.patchValue({
        ...bracket.data[0],
        id: bracket.data[0]._id,
      });
      this.isProcessing = false;
    } catch (error) {
      this.isProcessing = false;
      this.eSportsToastService.showError(error?.error?.message || error?.message);
    }
  };

  /**
   * Move to preview Screen
   */
  preview() {
    if (this.bracket.invalid) {
      this.bracket.markAllAsTouched();
      return;
    }
    const value = this.bracket.value;
    this.bracketService.setBracketData(value);
    this.router.navigate([this.navUrl], { relativeTo: this.activeRoute });
  }

  updateParticipantLimit(type) {
    let maximumParticipants = 1024;

    if (['single', 'double'].includes(type)) {
      this.multiStageTournamentHandler(false);
    } else {
      this.multiStageTournamentHandler(true, type);
    }

    switch (type) {
      case 'single':
        maximumParticipants = 1024;
        break;
      case 'double':
        maximumParticipants = 512;
        break;
      case 'round_robin':
        maximumParticipants = 1024;
        break;
      case 'battle_royale':
        maximumParticipants = 1024;
        break;
    }
    this.bracket
      .get('maximumParticipants')
      .setValidators([
        Validators.required,
        Validators.min(2),
        Validators.max(maximumParticipants),
        this.teamInGroupValidation('maximumParticipants').bind(this),
      ]);

    this.bracket.get('maximumParticipants').updateValueAndValidity();
  }

  multiStageTournamentHandler = (isEnableFields: boolean, type?): void => {
    if (isEnableFields) {
      const noOfRound = type == 'round_robin' ? 2 : 10;
      const formControl = (field, minV, maxV = null) => {
        const validatorArray = [
          Validators.required,
          Validators.min(minV),
          this.teamInGroupValidation(field).bind(this),
        ];
        if (maxV) {
          validatorArray.splice(2, 1, Validators.max(maxV));
        }

        const formFieldExist = this.bracket.get(field);
        if (formFieldExist) {
          formFieldExist.setValidators(validatorArray);
          formFieldExist.updateValueAndValidity();
        }

        return new FormControl(minV, Validators.compose(validatorArray));
      };

      this.bracket.addControl(
        'noOfTeamInGroup',
        formControl('noOfTeamInGroup', 2)
      );
      this.bracket.addControl(
        'noOfWinningTeamInGroup',
        formControl('noOfWinningTeamInGroup', 1)
      );
      this.bracket.addControl(
        'noOfRoundPerGroup',
        formControl('noOfRoundPerGroup', 1, noOfRound)
      );
      this.bracket.addControl('noOfStage', new FormControl(1));
    } else {
      this.bracket.removeControl('noOfTeamInGroup');
      this.bracket.removeControl('noOfWinningTeamInGroup');
      this.bracket.removeControl('noOfRoundPerGroup');
      this.bracket.removeControl('noOfStage');
    }
  };

  teamInGroupValidation = (field?: string): ValidatorFn => {
    return (control: AbstractControl): { [key: string]: any } | null => {
      if (control['_parent']) {
        const {
          noOfTeamInGroup,
          noOfWinningTeamInGroup,
          maximumParticipants,
          noOfStage,
        } = control?.['_parent']?.controls;
        if (
          maximumParticipants?.valid &&
          noOfTeamInGroup?.value &&
          !noOfTeamInGroup?.errors?.max &&
          noOfWinningTeamInGroup?.value &&
          maximumParticipants?.value &&
          noOfStage &&
          maximumParticipants?.value > noOfTeamInGroup?.value
        ) {
          const fieldValidation = (noOfTeams, noOfWinningTeam) => {
            if (noOfTeams > noOfWinningTeam) {
              return noOfTeams % noOfWinningTeam != 0;
            } else {
              return true;
            }
          };
          const errorConfig = fieldValidation(
            noOfTeamInGroup?.value,
            noOfWinningTeamInGroup?.value
          )
            ? { multiStageConfigError: true }
            : null;
          if (!errorConfig) {
            const totalStage = (
              totalPlayer,
              noOfPlayerPerGroup,
              noOfWinning,
              noOfStage,
              previousStagePlayer
            ) => {
              let nextStagePlayer;
              const createStage = () => {
                nextStagePlayer =
                  Math.ceil(totalPlayer / noOfPlayerPerGroup) * noOfWinning;
                if (nextStagePlayer < previousStagePlayer) {
                  // noOfPlayerPerGroup = noOfPlayerPerGroup + 1;
                  nextStagePlayer =
                    Math.ceil(totalPlayer / noOfPlayerPerGroup) * noOfWinning;
                } else {
                  createStage();
                }
              };

              if (totalPlayer !== noOfPlayerPerGroup) {
                createStage();
              }
              return totalPlayer == noOfPlayerPerGroup
                ? noOfStage
                : !(Math.ceil(nextStagePlayer / noOfPlayerPerGroup) > 1)
                ? noOfStage + 1
                : Math.ceil(nextStagePlayer / noOfPlayerPerGroup) == 2
                ? noOfStage + 2
                : totalStage(
                    nextStagePlayer,
                    noOfPlayerPerGroup,
                    noOfWinning,
                    noOfStage + 1,
                    previousStagePlayer
                  );
            };

            const noOfStage = totalStage(
              maximumParticipants?.value,
              noOfTeamInGroup?.value,
              noOfWinningTeamInGroup?.value,
              1,
              maximumParticipants?.value
            );
            this.bracket.get('noOfStage').setValue(noOfStage);
          }

          if (field == 'noOfTeamInGroup') {
            return errorConfig;
          } else {
            this.bracket.get('noOfTeamInGroup').setErrors(errorConfig);
            return null;
          }
        }
      } else {
        return null;
      }
    };
  };
}
