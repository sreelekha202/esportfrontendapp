import { Component, OnDestroy, OnInit, EventEmitter } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TournamentService } from '../../../core/service';
import { TranslateService } from '@ngx-translate/core';
import { IPagination } from '../../../shared/models';
import { Subscription } from 'rxjs';
import { MatTabChangeEvent } from '@angular/material/tabs';
import { debounceTime } from 'rxjs/operators';
import { EsportsUserService, IUser, EsportsToastService } from 'esports';
import { AppHtmlRoutes } from '../../../app-routing.model';

@Component({
  selector: 'app-my-tournament',
  templateUrl: './my-tournament.component.html',
  styleUrls: ['./my-tournament.component.scss'],
})
export class MyTournamentComponent implements OnInit, OnDestroy {
  AppHtmlRoutes = AppHtmlRoutes;
  tournamentList = [];
  type;
  tab = 0; //upcoming
  activeTabIndex = 0;
  pagination = {
    page: 1,
    limit: 5,
  };
  cPagination;
  page: IPagination;
  user: IUser;
  isDraft = false;
  isHidden = false;

  userSubscription: Subscription;
  tournamentSubscription: Subscription;
  customTabEvent = new EventEmitter<MatTabChangeEvent>();

  constructor(
    private tournamentService: TournamentService,
    private router: Router,
    private userService: EsportsUserService,
    private eSportsToastService: EsportsToastService,
    private activatedRoute: ActivatedRoute,
    private translateService: TranslateService
  ) {}

  ngOnInit(): void {
    this.type = this.activatedRoute.snapshot.routeConfig.path
      .split('/')
      .reverse()[0];
    this.getCurrentUserDetails();
    this.customTabEvent
      .pipe(debounceTime(30))
      .subscribe((tabChangeEvent: MatTabChangeEvent) => {
        this.attachTabChangeListener(tabChangeEvent);
      });

    this.activeTabIndex =
      +this.activatedRoute.snapshot.queryParams?.activeTab || 0;
    this.cPagination = Object.assign({}, this.pagination);
    this.switchData(this.activeTabIndex);
  }

  ngOnDestroy() {
    this.userSubscription?.unsubscribe();
    this.tournamentSubscription?.unsubscribe();
  }

  showNotification() {
    if (this.activatedRoute.snapshot.queryParams.transaction) {
      if (this.activatedRoute.snapshot.queryParams.transaction == 'success') {
        this.eSportsToastService.showSuccess(
          this.translateService.instant(
            'PROFILE.TOURNAMENTS.PAYMENT.PAYMENT_SUCCESS'
          )
        );
      }
      if (this.activatedRoute.snapshot.queryParams.transaction == 'failed') {
        this.eSportsToastService.showError(
          this.translateService.instant(
            'PROFILE.TOURNAMENTS.PAYMENT.PAYMENT_FAILED'
          )
        );
      }
    }
  }
  getCurrentUserDetails() {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.user = data;
        // this.cPagination = Object.assign({}, this.pagination);
        // this.fetchTournaments(this.type, this.tab);
        this.showNotification();
      }
    });
  }

  /**
   * Get tournament list
   */

  fetchTournaments = async (type, tab) => {
    try {
      this.type = type;
      this.tab = tab;
      this.isDraft = tab == 4; //draft
      this.tournamentList = [];

      if (this.tournamentSubscription) {
        this.tournamentSubscription.unsubscribe();
      }

      let payload;
      if (type === 'joined') {
        payload = await this.getJoinedTournamentQuery(tab);
        this.tournamentSubscription = this.tournamentService
          .getParticipantTournament(payload)
          .subscribe((tournament) => {
            if (tournament) {
              this.page = {
                totalItems: tournament?.data?.totalDocs,
                itemsPerPage: tournament?.data?.limit,
                maxSize: 5,
              };
              this.tournamentList = tournament.data.docs || [];
            }
          });
      } else if (type === 'created') {
        payload = await this.getCreatedTournamentQuery(tab);
        const tournament = await this.tournamentService.fetchMyTournament(payload);
        this.page = {
          totalItems: tournament?.data?.totalDocs,
          itemsPerPage: tournament?.data?.limit,
          maxSize: 5,
        };
        this.tournamentList = tournament.data.docs || [];
      }
    } catch (error) {
      this.eSportsToastService.showError(error?.error?.message || error?.message);
    }
  };

  // beforeTabChange(event) {
  //   const navtab = event.nextId.split('-');
  //   this.cPagination = Object.assign({}, this.pagination);

  //   if (navtab[0] == 'created' && navtab[1] == 'ongoing') {
  //     this.isHidden = true;
  //   } else if (navtab[0] == 'created' && navtab[1] == 'past') {
  //     this.isHidden = true;
  //   } else if (navtab[0] == 'joined' && navtab[1] == 'ongoing') {
  //     this.isHidden = true;
  //   } else if (navtab[0] == 'joined' && navtab[1] == 'past') {
  //     this.isHidden = true;
  //   } else {
  //     this.isHidden = false;
  //   }
  //   this.fetchTournaments(navtab[0], navtab[1]);
  // }

  onTabChanged = (tabChangeEvent: MatTabChangeEvent): void => {
    this.customTabEvent.emit(tabChangeEvent);
  };

  attachTabChangeListener(tabChangeEvent: MatTabChangeEvent) {
    this.activeTabIndex = tabChangeEvent.index;
    this.router.navigate(['.'], {
      relativeTo: this.activatedRoute,
      queryParams: { activeTab: tabChangeEvent.index },
    });
    this.switchData(tabChangeEvent.index);
  }

  switchData(index: number) {
    switch (index) {
      case 0:
        this.fetchTournaments(this.type, 0); //upcoming
        break;
      case 1:
        this.fetchTournaments(this.type, 1); //ongoing
        break;
      case 2:
        this.fetchTournaments(this.type, 2); //past
        break;
      case 3:
        this.fetchTournaments(this.type, 5); //past
        break;  
      default:
        break;
    }
  }

  async getTournamentData(tabName) {
    this.resetPage();
    const payload = this.getCreatedTournamentQuery(tabName);
    const res = await this.tournamentService
      .getPaginatedTournaments(payload)
      .toPromise();
    this.tournamentList = res['data'].docs || [];
  }

  resetPage() {
    this.tournamentList = [];
  }

  pageChanged(page): void {
    this.cPagination.page = page;
    this.fetchTournaments(this.type, this.tab);
  }

  getCreatedTournamentQuery(tab) {
    switch (true) {
      case tab === 0: //upcoming
        this.isHidden = false;
        return {
          page: this.cPagination.page,
          status: '0',
          limit: 5,
        };
      case tab === 1: //ongoing
        this.isHidden = true;
        return {
          page: this.cPagination.page,
          status: '1',
          limit: 5,
        };
      case tab === 2: //past
        this.isHidden = true;
        return {
          page: this.cPagination.page,
          status: '2',
          limit: 5,
        };
      case tab === 4: //draft
        return {
          page: this.cPagination.page,
          status: '4',
          limit: 5,
        };
      case tab === 5: //unpublished
        return {
          page: this.cPagination.page,
          status: '5',
          limit: 5,
        };
    }
  }

  getJoinedTournamentQuery(tab) {
    switch (true) {
      case tab === 0: //upcoming
         this.isHidden = false;
        return {
          pagination: JSON.stringify(this.cPagination),
          type: 0,
        };
      case tab === 1: //ongoing
        this.isHidden = true;
        return {
          pagination: JSON.stringify(this.cPagination),
          type: 1,
          isEdit: true,
        };
      case tab === 2: //past
        this.isHidden = true;
        return {
          pagination: JSON.stringify(this.cPagination),
          type: 2,
          isEdit: true,
        };
    }
  }
}
