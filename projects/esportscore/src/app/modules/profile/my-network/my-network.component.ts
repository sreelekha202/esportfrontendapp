import {
  Component,
  ElementRef,
  OnInit,
  ViewChild,
  EventEmitter,
} from '@angular/core';
import {
  IUser,
  EsportsUserService,
  EsportsLanguageService,
  EsportsToastService,
} from 'esports';
import { Subscription, fromEvent, of } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { EsportsConstantsService } from 'esports';
import { TranslateService } from '@ngx-translate/core';
import { MatTabChangeEvent, MatTabGroup } from '@angular/material/tabs';
import { ActivatedRoute, Router } from '@angular/router';
import { IPagination } from '../../../shared/models/pagination';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {
  debounceTime,
  map,
  distinctUntilChanged,
  filter,
} from 'rxjs/operators';
const API = environment.apiEndPoint;
const TOKEN = environment.currentToken;
@Component({
  selector: 'app-my-network',
  templateUrl: './my-network.component.html',
  styleUrls: ['./my-network.component.scss'],
})
export class MyNetworkComponent implements OnInit {
  page: IPagination;
  user: IUser;
  userSubscription: Subscription;
  followeUserList: any;
  followingList: any;
  activeTabIndex = 0;
  showLoader = true;
  countFollowingUser: Number;
  countFollowerUser: Number;
  modelDesc: string;
  modelHeader: string;
  modelTitle: string;
  customTabEvent = new EventEmitter<MatTabChangeEvent>();
  @ViewChild('tabs') tabGroup: MatTabGroup;
  @ViewChild('searchInput') inputName;
  @ViewChild('searchInput1') inputName1;
  followerList: any;
  paginationData1 = {
    page: 1,
    limit: 5,
    sort: '-createdOn',
  };
  paginationData2 = {
    page: 1,
    limit: 5,
    sort: '-createdOn',
  };
  followerListLength: Number;
  followingListLength: Number;
  userId: any;
  modelType: any;
  @ViewChild('searchInput', { static: true }) searchInput: ElementRef;
  constructor(
    private userService: EsportsUserService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    public eSportsToastService: EsportsToastService,
    private translateService: TranslateService,
    private modalService: NgbModal
  ) {}

  ngOnInit(): void {
    this.callContentData();
    this.customTabEvent
      .pipe(debounceTime(30))
      .subscribe((tabChangeEvent: MatTabChangeEvent) => {
        this.attachTabChangeListener(tabChangeEvent);
      });
    this.getFollowerUser();
    this.getFollowingUser();
  }
  callContentData() {
    this.activatedRoute.queryParams
      .subscribe((params) => {
        if (params.activeTabIndex) {
          this.activeTabIndex = +params.activeTabIndex;
        } else {
          this.activeTabIndex = 0;
        }
        this.switchData(this.activeTabIndex);
      })
      .unsubscribe();
  }
  ngAfterViewInit() {
    this.tabGroup.selectedIndex = this.activeTabIndex;
  }
  attachTabChangeListener(tabChangeEvent: MatTabChangeEvent) {
    this.switchData(tabChangeEvent.index);
  }
  switchData(index: number) {
    switch (index) {
      case 0:
        this.router.navigate(['.'], {
          relativeTo: this.activatedRoute,
          queryParams: { activeTabIndex: index },
        });
        this.paginationData1 = {
          page: 1,
          limit: 5,
          sort: '-createdOn',
        };
        this.getFollowingUser();
        break;
      case 1:
        this.router.navigate(['.'], {
          relativeTo: this.activatedRoute,
          queryParams: { activeTabIndex: index },
        });
        this.paginationData2 = {
          page: 1,
          limit: 5,
          sort: '-createdOn',
        };
        this.getFollowerUser();
        break;
      default:
        break;
    }
  }
  onTabChanged = (tabChangeEvent: MatTabChangeEvent): void => {
    this.customTabEvent.emit(tabChangeEvent);
  };
  getFollowingUser() {
    try {
      const pagination = JSON.stringify(this.paginationData1);
      //Get following users
      this.userService
        .getUserFollower({ API, pagination: pagination })
        .subscribe(
          (res) => {
            this.showLoader = false;
            this.followerList = res.data.docs;
            this.countFollowingUser = res.data.totalDocs;
            this.page = {
              totalItems: res?.data?.totalDocs,
              itemsPerPage: res?.data?.limit,
              maxSize: 5,
            };

            this.followingListLength = this.followerList.length;
          },

          (err) => {
            this.showLoader = false;
          }
        );
    } catch {
      this.eSportsToastService.showError(
        this.translateService.instant('PROFILE.MY_NETWORK.ERRORS.ERROR1')
      );
    }
  }
  getFollowerUser() {
    try {
      const pagination = JSON.stringify(this.paginationData2);
      this.userService
        .getFollowerList({ API, pagination: pagination })
        .subscribe(
          (res) => {
            this.showLoader = false;
            this.page = {
              totalItems: res?.data?.totalDocs,
              itemsPerPage: res?.data?.limit,
              maxSize: 5,
            };
            this.followingList = res.data.docs;
            this.followerListLength = this.followingList.length;
            this.countFollowerUser = res.data.totalDocs;
          },
          (err) => {
            this.showLoader = false;
          }
        );
    } catch {
      this.eSportsToastService.showError(
        this.translateService.instant('PROFILE.MY_NETWORK.ERRORS.ERROR2')
      );
    }
  }

  pageChanged1(page): void {
    this.paginationData1.page = page;
    this.getFollowingUser();
  }
  pageChanged2(page): void {
    this.paginationData2.page = page;
    this.getFollowerUser();
  }

  removeFollowerUser(id) {
    try {
      this.userService.removeFollowerUser(API, id).subscribe(
        (res) => {
          this.modalService.dismissAll();
          this.getFollowerUser();
          this.userService.refreshCurrentUser(API, TOKEN);
        },
        (err) => {
          this.showLoader = false;
        }
      );
    } catch {
      this.eSportsToastService.showError(
        this.translateService.instant('PROFILE.MY_NETWORK.ERRORS.ERROR4')
      );
    }
  }

  removeFollowingUser(id) {  
    try {
       this.userService.removeFollowingUser(API, id).subscribe(
        (res) => {
          this.modalService.dismissAll();
          this.getFollowingUser();
          this.userService.refreshCurrentUser(API, TOKEN);
        },
        (err) => {
          this.showLoader = false;
        }
      );
    } catch {
      this.eSportsToastService.showError(
        this.translateService.instant('PROFILE.MY_NETWORK.ERRORS.ERROR3')
      );
    }
  }

  onFOllowingUserSearch(text) {
    const pagination = JSON.stringify(this.paginationData1);
    const value = text.toLowerCase().trim();
    if (text == '') {
      this.getFollowingUser();
    } else {
      setTimeout(() => {
        this.userService
          .searchForFollowingUser({ API, value, pagination: pagination })
          .subscribe((res) => {
            if (res) {
              this.page = {
                totalItems: res?.data?.totalDocs,
                itemsPerPage: res?.data?.limit,
                maxSize: 5,
              };
              this.showLoader = false;
              this.followerList = res.data.docs;
              this.followingListLength = res.data.totalDocs;
            }
          });
      }, 1000);
    }
  }
  clearSearchForFollowingUser() {
    if (this.inputName.nativeElement.value) {
      this.inputName.nativeElement.value = '';
    }
    this.getFollowingUser();
  }
  clearSearchForFollowerUser() {
    if (this.inputName1.nativeElement.value) {
      this.inputName1.nativeElement.value = '';
    }
    this.getFollowerUser();
  }
  onFollowerUserSearch(text) {
    const pagination = JSON.stringify(this.paginationData2);
    const value = text.toLowerCase().trim();
    if (text == '') {
      this.getFollowerUser();
    }

    setTimeout(() => {
      this.userService
        .searchForFollowerUser({ API, value, pagination: pagination })
        .subscribe((res) => {
          if (res) {
            this.page = {
              totalItems: res?.data?.totalDocs,
              itemsPerPage: res?.data?.limit,
              maxSize: 5,
            };
            this.showLoader = false;
            this.followingList = res.data.docs;
            this.followerListLength = res.data.totalDocs;
          }
        });
    }, 1000);
  }
  open(content, type, id) { 
    this.modelType = type;    
    this.userId = id;
    this.modalService.dismissAll();
    this.setModelConfiguration(
      EsportsConstantsService.Model.RemoveUser.title,
      EsportsConstantsService.Model.RemoveUser.description,
      EsportsConstantsService.Model.RemoveUser.header
    );
    this.modalService.open(content, {
      size: 'md',
      centered: true,
      scrollable: true,
      windowClass: 'custom-modal-content',
    });
  }
  setModelConfiguration(title, description, header) {
    this.modelTitle = this.translateService.instant(title);
    this.modelDesc = this.translateService.instant(description);
    this.modelHeader = this.translateService.instant(header);
  }

  removeUser() { 
    if (this.modelType == 'removeFollowingUser') {
      this.removeFollowingUser(this.userId);
    } else if (this.modelType == 'removeFollowerUser') {
      this.removeFollowerUser(this.userId);
    }
    
  }
}
