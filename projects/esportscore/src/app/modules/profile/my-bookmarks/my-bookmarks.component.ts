import { Component, OnDestroy, OnInit } from '@angular/core';
import {
  UserPreferenceService,
  UtilsService,
} from '../../../core/service';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';

import { EsportsUserService, EsportsLanguageService, EsportsToastService } from 'esports';

enum MyBookmarksComponentTab {
  article = 'PROFILE.BOOKMARKS.TAB1',
  video = 'PROFILE.BOOKMARKS.TAB2',
}

@Component({
  selector: 'app-my-bookmarks',
  templateUrl: './my-bookmarks.component.html',
  styleUrls: ['./my-bookmarks.component.scss'],
})
export class MyBookmarksComponent implements OnInit, OnDestroy {
  MyBookmarksComponentTab = MyBookmarksComponentTab;
  userId;
  articleBookmarkList: Array<{}> = [];
  videoBookmarkList: Array<{}> = [];

  userSubscription: Subscription;

  constructor(
    private userPreferenceService: UserPreferenceService,
    private userService: EsportsUserService,
    private eSportsToastService: EsportsToastService,
    public translate: TranslateService,
    public language: EsportsLanguageService,
    public utilsService: UtilsService,
    public languageService: EsportsLanguageService
  ) {}

  ngOnInit(): void {
    this.getCurrentUserDetails();
  }

  ngOnDestroy() {
    this.userSubscription?.unsubscribe();
  }

  getBookmarks = async () => {
    try {
      const filter = JSON.stringify({ userId: this.userId });
      const projection = 'articleBookmarks,videoLibrary';
      const userPreference = await this.userPreferenceService
        .getPreferences(filter, projection)
        .toPromise();
      this.articleBookmarkList =
        userPreference?.data[0]?.articleBookmarks || [];
      this.videoBookmarkList = userPreference?.data[0]?.videoLibrary || [];
    } catch (error) {
      this.eSportsToastService.showError(error?.error?.message || error?.message);
    }
  };

  getCurrentUserDetails() {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.userId = data._id;
        this.getBookmarks();
      }
    });
  }

  deleteBookmark = async (id, idType) => {
    try {
      if (!this.userId) {
        this.eSportsToastService.showError('Session Expired Please Login');
        return;
      }
      const queryParam = `${idType}=${id}`;
      const userPreference = await this.userPreferenceService
        .removeBookmark(queryParam)
        .toPromise();
      this.eSportsToastService.showSuccess(userPreference?.message);
      this.getBookmarks();
    } catch (error) {
      this.eSportsToastService.showError(error?.error?.message || error?.message);
    }
  };

  beforeTabChange(event) {
    const navtab = event.nextId;
  }
}
