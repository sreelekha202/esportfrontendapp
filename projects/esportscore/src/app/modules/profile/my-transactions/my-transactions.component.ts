import { Component, OnInit, EventEmitter } from '@angular/core';
import { DatePipe } from '@angular/common';

import { TransactionService } from '../../../core/service';
import { TranslateService } from '@ngx-translate/core';
import { debounceTime } from 'rxjs/operators';
import { MatTabChangeEvent } from '@angular/material/tabs';
import { EsportsUserService } from 'esports';

import { environment } from '../../../../environments/environment';

const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;

enum MyTransactionsComponentTab {
  purchase = 'PROFILE.TRANSACTIONS.TAB1',
  rewards = 'PROFILE.TRANSACTIONS.TAB2',
}

@Component({
  selector: 'app-my-transactions',
  templateUrl: './my-transactions.component.html',
  styleUrls: ['./my-transactions.component.scss'],
})
export class MyTransactionsComponent implements OnInit {
  MyTransactionsComponentTab = MyTransactionsComponentTab;
  customTabEvent = new EventEmitter<MatTabChangeEvent>();
  transactionDetail;
  rewardDetails;
  showLoader = false;
  paginationData = {
    page: 1,
    limit: 5,
    sort: { createdOn: -1 },
  };
  rows = [];
  activetab = 'rewards';

  purchase = {
    rows: [],
    columns: [
      { name: 'Bill Number' },
      { name: 'TransactionId' },
      { name: 'Amount' },
      { name: 'Currency code' },
      { name: 'OrderId' },
      { name: 'Created Date' },
      { name: 'Status' },
    ],
  };
  rewards = {
    rows: [
      // {
      //   rewardId: 'ID12x12x12',
      //   rewardType: 'Transactional',
      //   description: 'Description about reward here',
      //   status: 'Credit',
      //   date: '16/6/2020',
      // },
      // {
      //   rewardId: 'ID12x12x12',
      //   rewardType: 'Transactional',
      //   description: 'Description about reward here',
      //   status: 'Credit',
      //   date: '16/6/2020',
      // },
      // {
      //   rewardId: 'ID12x12x12',
      //   rewardType: 'Transactional',
      //   description: 'Description about reward here',
      //   status: 'Credit',
      //   date: '16/6/2020',
      // },
      // {
      //   rewardId: 'ID12x12x12',
      //   rewardType: 'Transactional',
      //   description: 'Description about reward here',
      //   status: 'Credit',
      //   date: '16/6/2020',
      // },
      // {
      //   rewardId: 'ID12x12x12',
      //   rewardType: 'Transactional',
      //   description: 'Description about reward here',
      //   status: 'Credit',
      //   date: '16/6/2020',
      // },
      // {
      //   rewardId: 'ID12x12x12',
      //   rewardType: 'Transactional',
      //   description: 'Description about reward here',
      //   status: 'Credit',
      //   date: '16/6/2020',
      // },
      // {
      //   rewardId: 'ID12x12x12',
      //   rewardType: 'Transactional',
      //   description: 'Description about reward here',
      //   status: 'Credit',
      //   date: '16/6/2020',
      // },
      // {
      //   rewardId: 'ID12x12x12',
      //   rewardType: 'Transactional',
      //   description: 'Description about reward here',
      //   status: 'Credit',
      //   date: '16/6/2020',
      // },
      // {
      //   rewardId: 'ID12x12x12',
      //   rewardType: 'Transactional',
      //   description: 'Description about reward here',
      //   status: 'Credit',
      //   date: '16/6/2020',
      // },
    ],
    columns: [
      { name: 'REWARD' },
      { name: 'DESCRIPTION' },
      { name: 'BALANCE' },
      { name: 'STATUS' },
      { name: 'DATE' },
      { name: 'EXPIRY' },
    ],
  };

  constructor(
    private userService: EsportsUserService,
    public datePipe: DatePipe,
    private transactionServices: TransactionService,
    private translateService: TranslateService
  ) {}

  ngOnInit(): void {
    this.getRewardsDetails();
    this.customTabEvent
      .pipe(debounceTime(30))
      .subscribe((matTabEvent: MatTabChangeEvent) => {
        this.fetchDataOnTabChange(matTabEvent);
      });
  }

  onTabChangeEvent(matTabEvent: MatTabChangeEvent) {
    this.customTabEvent.emit(matTabEvent);
  }

  fetchDataOnTabChange(matTabChangeEvent: MatTabChangeEvent) {
    if (matTabChangeEvent.index == 0) {
      this.showLoader = true;
      this.rows.length = 0;
      this.paginationData = {
        page: 1,
        limit: 5,
        sort: { createdOn: -1 },
      };
      this.getRewardsDetails();
    } else {
      this.showLoader = true;
      this.rows.length = 0;
      this.paginationData = {
        page: 1,
        limit: 5,
        sort: { createdOn: -1 },
      };
      this.getPaymentDetail();
    }
  }

  // beforeTabChange(event) {
  //   if (event.nextId === 'rewards') {
  //     this.showLoader = true;
  //     this.rows.length = 0;
  //     this.getRewardsDetails();
  //   } else if (event.nextId === 'payment') {
  //     this.showLoader = true;
  //     this.rows.length = 0;
  //     this.getPaymentDetail();
  //   }
  // }

  getRewardsDetails = async () => {
    const that = this;
    const pagination = JSON.stringify(this.paginationData);
    const data = await this.userService.getRewardTransaction(API, {
      pagination,
    });
    this.rewardDetails = data.data;
    this.rows = data.data['docs'];
    this.rows.forEach((obj, index) => {
      let status;
      if (obj.status == 0) {
        status = this.translateService.instant('PROFILE.TRANSACTIONS.REDEEMED');
      } else if (obj.status == 1) {
        status = this.translateService.instant(
          'PROFILE.TRANSACTIONS.NOT_REDEEMED'
        );
      } else if (obj.status == 2) {
        status = this.translateService.instant(
          'PROFILE.TRANSACTIONS.PARTIAL_REDEEMED'
        );
      } else {
        status = this.translateService.instant('PROFILE.TRANSACTIONS.EXPIRED');
      }
      obj.type =
        obj.type.charAt(0).toUpperCase() + obj.type.slice(1).toLowerCase();
      obj.status = status;
      obj.reward_type = obj.reward_type + ' +' + obj.reward_value;
      obj.date = that.datePipe.transform(obj.createdOn, 'MMM d,yyyy');
      obj.expiryDate = that.datePipe.transform(obj.expiredOn, 'MMM d,yyyy');
    });
    this.showLoader = false;
  };
  getPaymentDetail = async () => {
    const pagination = JSON.stringify(this.paginationData);
    const data = await this.transactionServices.paymentDetail({
      pagination,
    });
    this.transactionDetail = data.data;
    this.rows = data.data['docs'];
    this.rows.forEach((obj, index) => {
      if (obj.txnId === 'na') {
        obj.txnId = 'NA';
      }
    });
    this.showLoader = false;
  };

  setPage1(event) {
    this.paginationData.page = event.offset + 1;
    this.showLoader = true;
    this.rows.length = 0;
    this.getPaymentDetail();
  }
  setPage2(event) {
    this.paginationData.page = event.offset + 1;
    this.showLoader = true;
    this.rows.length = 0;
    this.getRewardsDetails();
  }
}
