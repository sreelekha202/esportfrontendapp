import { Component, OnInit, Inject, PLATFORM_ID } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { AppHtmlRoutes } from '../../../app-routing.model';
import { EsportsUserService, IPagination } from 'esports';
import { environment } from '../../../../environments/environment';
import { isPlatformBrowser } from '@angular/common';
const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;

@Component({
  selector: 'app-my-teams',
  templateUrl: './my-teams.component.html',
  styleUrls: ['./my-teams.component.scss'],
})
export class MyTeamsComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;
  paginationData = {
    page: 1,
    limit: 5,
    sort: { _id: -1 },
    text: '',
  };
  paginationData2 = {
    page: 1,
    limit: 5,
    sort: { _id: -1 },
  };
  page: IPagination;
  page2: IPagination;
  isBrowser: boolean;
  public form: FormGroup;

  mock_select = [
    {
      name: 'react',
    },
    {
      name: 'angular',
    },
    {
      name: 'vue',
    },
  ];

  mock_teams_control = [];

  mock_teams_invites = [];

  constructor(
    private fb: FormBuilder,
    private userService: EsportsUserService,
    @Inject(PLATFORM_ID) private platformId
  ) {
    this.isBrowser = isPlatformBrowser(platformId);
  }

  ngOnInit(): void {
    if (this.isBrowser) {
      this.form = this.fb.group({
        view: [''],
      });

      this.getMyTeams();
      this.getMyInvites();
    }
  }

  selectType(event): void {}

  getMyTeams = async () => {
    const pagination = JSON.stringify(this.paginationData);
    const data = await this.userService.getMyTeam(API, {
      pagination: pagination,
    });
    if (data) {
      this.page = {
        totalItems: data?.data?.totalDocs,
        itemsPerPage: data?.data?.limit,
        maxSize: 5,
      };
    }
    this.mock_teams_control = data.data.docs;
  };
  searchByValueFilter(event) {
    if (event.key == 'Enter' || event.target.value == '') {
      this.paginationData.text = event.target.value.trim();
      this.getMyTeams();
    }
  }

  getMyInvites = async () => {
    const pagination = JSON.stringify(this.paginationData2);
    const data = await this.userService.getMyInvite(API, {
      pagination: pagination,
    });
    if (data) {
      this.page2 = {
        totalItems: data?.data?.totalDocs,
        itemsPerPage: data?.data?.limit,
        maxSize: 5,
      };
    }
    this.mock_teams_invites = data.data.docs;
  };

  pageChanged(page): void {
    this.paginationData.page = page;
    this.getMyTeams();
  }

  pageChanged2(page): void {
    this.paginationData2.page = page;
    this.getMyInvites();
  }
}
