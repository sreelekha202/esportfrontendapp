import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-recent-match-card',
  templateUrl: './recent-match-card.component.html',
  styleUrls: ['./recent-match-card.component.scss'],
})
export class RecentMatchCardComponent implements OnInit {
  @Input() data;

  constructor() {}

  ngOnInit(): void {}
}
