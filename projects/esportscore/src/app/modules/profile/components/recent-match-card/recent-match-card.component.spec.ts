import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RecentMatchCardComponent } from './recent-match-card.component';

describe('RecentMatchCardComponent', () => {
  let component: RecentMatchCardComponent;
  let fixture: ComponentFixture<RecentMatchCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RecentMatchCardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RecentMatchCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
