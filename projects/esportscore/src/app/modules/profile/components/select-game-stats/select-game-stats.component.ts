import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-select-game-stats',
  templateUrl: './select-game-stats.component.html',
  styleUrls: ['./select-game-stats.component.scss'],
})
export class SelectGameStatsComponent implements OnInit {
  games = [];

  responsiveOptions = [
    {
      breakpoint: '1199px',
      numVisible: 4,
      numScroll: 1,
    },
    {
      breakpoint: '767px',
      numVisible: 3,
      numScroll: 1,
    },
    {
      breakpoint: '575px',
      numVisible: 2,
      numScroll: 1,
    },
  ];

  @Output() selectedGame = new EventEmitter()


  constructor() { }

  ngOnInit(): void {
    this.games = [
      {
        id: 1,
        image: 'assets/icons/games/dota2.svg',
        name: 'DOTA 2',
        isSelected: true,
        gameKey: 'DOTA2',
      },
      {
        id: 2,
        image: 'assets/icons/games/valorant.svg',
        name: 'Valorant',
        isSelected: false,
        gameKey: 'VALORANT'
      },
      {
        id: 3,
        image: 'assets/icons/games/fortnine.svg',
        name: 'Fortnite',
        isSelected: false,
        gameKey: 'FORTNITE',
      },
      {
        id: 4,
        image: 'assets/icons/games/fifa21.svg',
        name: 'FIFA 21',
        isSelected: false,
        gameKey: 'FIFA21'
      },
      {
        id: 5,
        image: 'assets/icons/games/csgo.svg',
        name: 'CS:GO',
        isSelected: false,
        gameKey: 'CSGO'
      }
    ];
  }

  onSelectGame(id: number, gameKey: string): void {
    this.games.forEach((game) => {
      game.isSelected = game.id === id;
    });

    // this.selectedGame.emit({ id })
    this.selectedGame.emit({ gameKey })

  }
}
