import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectGameStatsComponent } from './select-game-stats.component';

describe('SelectGameStatsComponent', () => {
  let component: SelectGameStatsComponent;
  let fixture: ComponentFixture<SelectGameStatsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SelectGameStatsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectGameStatsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
