import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { EsportsUserService, EsportsToastService } from 'esports';

import { environment } from '../../../../../environments/environment';
import {
  AppHtmlRoutes,
  AppHtmlProfileRoutes,
  AppHtmlAdminRoutes,
} from '../../../../app-routing.model';
const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;

interface TeamCardInvite {
  _id: number | string;
  teamName: string;
  logo: string;
  teamId: {
    teamName: string;
    logo: string;
  };
}

@Component({
  selector: 'app-team-card-invite',
  templateUrl: './team-card-invite.component.html',
  styleUrls: ['./team-card-invite.component.scss'],
})
export class TeamCardInviteComponent implements OnInit {
  @Input() data: TeamCardInvite;

  AppHtmlRoutes = AppHtmlRoutes;
  AppHtmlProfileRoutes = AppHtmlProfileRoutes;
  AppHtmlAdminRoutes = AppHtmlAdminRoutes;

  constructor(
    private userService: EsportsUserService,
    private router: Router,
    public eSportsToastService: EsportsToastService
  ) {}

  ngOnInit(): void {}

  async updateInvitees(teamId, status) {
    await this.userService.update_invitees(API, { teamId, status }).subscribe(
      (data) => {
        this.eSportsToastService.showSuccess(data?.message);
        this.router
          .navigateByUrl('/profile/basic-info', { skipLocationChange: true })
          .then(() => {
            this.router.navigate(['/profile/teams']);
          });
      },
      (error) => {}
    );
  }

  view(data) {
    this.router.navigate([AppHtmlProfileRoutes.teamsView, data._id]);
  }
}
