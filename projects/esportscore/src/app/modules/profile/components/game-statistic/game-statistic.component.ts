import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { EsportsGameApiService, EsportsUserService, EsportsToastService } from 'esports';
import { environment } from 'projects/esportscore/src/environments/environment';


@Component({
  selector: 'app-game-statistic',
  templateUrl: './game-statistic.component.html',
  styleUrls: ['./game-statistic.component.scss'],
})
export class GameStatisticComponent implements OnInit {
  @Input() data;
  @Input() steamId;
  @Output() onSelected = new EventEmitter()

  isFirstSelected: boolean = false;



  constructor(
    private gameApiService: EsportsGameApiService,
    private eSportsToastService: EsportsToastService,

  ) {

  }

  ngOnInit(): void {


  }

  onLifeTimestats() {

    if (this.isFirstSelected) {
      this.isFirstSelected = false
      this.onSelected.emit({ selected: this.isFirstSelected })

    }

  }


  getStcStats() {

    if (!this.isFirstSelected) {

      this.isFirstSelected = true
      this.onSelected.emit({ selected: this.isFirstSelected })

    }
  }



  getLifetimeStats(params) {
    this.gameApiService.getUsergetMedal( params).subscribe(
      (res) => {

        if (res.data) {

          let gold_per_min = res.data.find((ele) => {
            return ele.field == 'gold_per_min';
          });
          let duration = res.data.find((ele) => {
            return ele.field == 'duration';
          });
          let hero_healing = res.data.find((ele) => {
            return ele.field == 'hero_healing';
          });
          let assists = res.data.find((ele) => {
            return ele.field == 'assists';
          });
          let tower_damage = res.data.find((ele) => {
            return ele.field == 'tower_damage';
          });
          let hero_damage = res.data.find((ele) => {
            return ele.field == 'hero_damage';
          });


          this.data['details']['goldPerMinute'] =
            gold_per_min != undefined ? gold_per_min.sum : 0;
          this.data['details']['duration'] =
            duration != undefined
              ? this.SecondsTohhmmss(duration.sum)
              : '0';

          this.data['details']['healing'] =
            hero_healing != undefined ? hero_healing.sum : 0;

          this.data['details']['heroDmg'] =
            hero_damage != undefined ? hero_damage.sum : 0;

          this.data['details']['assist'] =
            assists != undefined && assists != 0 ? assists.sum : 0;

          this.data['details']['towerDmg'] =
            tower_damage != undefined ? tower_damage.sum : 0;
        }
      
      },
      (err) => {
        this.eSportsToastService.showError(err?.error?.message || err?.message);
      }
    );
  }

  getWinLoss(params) {
    this.gameApiService.getUserWinsLose( params).subscribe(
      (res) => {
        if (res.data) {
          this.data["details"]['wins'] = res.data['win'];
          this.data['details']["losses"] = res.data['lose'];
          if (res.data['win'] && res.data['lose']) {
            this.data["details"]['matches'] = res.data['win'] + res.data['lose']
          }

         
        } else {
        }

      },
      (err) => {
        this.eSportsToastService.showError(err?.error?.message || err?.message);
      }
    );
  }

  getMMR(params) {
    this.gameApiService.getMMR( params).subscribe(
      (res) => {
        if (res.data) {
          this.data['details']['mmr'] =
            res.data['mmr_estimate']['estimate'] != undefined
              ? res.data['mmr_estimate']['estimate']
              : 0;
        } else {
        }

      },
      (err) => {
        this.eSportsToastService.showError(err?.error?.message || err?.message);
      }
    );
  }





  SecondsTohhmmss(n) {
    const day = Math.floor(n / (24 * 3600));
    n = n % (24 * 3600);
    const hour = Math.floor(n / 3600);
    n %= 3600;
    const minutes = Math.floor(n / 60);
    n %= 60;
    const seconds = Math.floor(n);
    return (
      day + ' days ' + hour + ' hr ' + minutes + ' min ' + seconds + ' sec'
    );
  }
}
