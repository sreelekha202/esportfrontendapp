import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeamCardInfoComponent } from './team-card-info.component';

describe('TeamCardInfoComponent', () => {
  let component: TeamCardInfoComponent;
  let fixture: ComponentFixture<TeamCardInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TeamCardInfoComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamCardInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
