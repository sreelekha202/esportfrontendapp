import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-tournament-matches',
  templateUrl: './tournament-matches.component.html',
  styleUrls: ['./tournament-matches.component.scss'],
})
export class TournamentMatchesComponent implements OnInit {
  @Input() data: any;

  constructor() {}

  ngOnInit(): void {}
}
