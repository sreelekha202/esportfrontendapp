import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IUser, EsportsUserService, EsportsToastService } from 'esports';
import { Location } from '@angular/common';

import { environment } from '../../../../environments/environment';

const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;
@Component({
  selector: 'app-my-teams-view',
  templateUrl: './my-teams-view.component.html',
  styleUrls: ['./my-teams-view.component.scss'],
})
export class MyTeamsViewComponent implements OnInit {
  user: IUser;
  showLoader = false;
  mock_team;

  mock_members;

  mock_matches = {
    title: 'Pubg global tournament',
    matches: [
      {
        result: 'win',
        team: 'Brightforce',
        info: {
          day: 'Monday',
          date: ' 08/02/21',
          time: '04:15',
        },
      },
      {
        result: 'loss',
        team: 'Peanutbutter',
        info: {
          day: 'Monday',
          date: ' 08/02/21',
          time: '04:15',
        },
      },
      {
        result: 'win',
        team: 'Fuelpower',
        info: {
          day: 'Monday',
          date: ' 08/02/21',
          time: '04:15',
        },
      },
      {
        result: 'win',
        team: 'Cheesezy',
        info: {
          day: 'Monday',
          date: ' 08/02/21',
          time: '04:15',
        },
      },
    ],
  };

  constructor(
    private activatedRoute: ActivatedRoute,
    private userService: EsportsUserService,
    private eSportsToastService: EsportsToastService,
    private location: Location
  ) {}

  ngOnInit() {
    this.activatedRoute.params.subscribe((params) => {
      if (params?.id) {
        this.getTeamById(params?.id);
      }
    });
  }

  getTeamById = async (id) => {
    try {
      this.showLoader = true;

      const team: any = await this.userService.getTeamById(API, id);
      this.mock_team = team?.data?.team;
      this.mock_members = team?.data?.members;
      this.showLoader = false;
    } catch (error) {
      this.showLoader = false;
      this.eSportsToastService.showError(error?.error?.message || error?.message);
    }
  };
  back() {
    this.location.back();
  }
}
