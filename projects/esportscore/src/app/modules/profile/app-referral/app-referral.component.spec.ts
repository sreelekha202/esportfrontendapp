import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppReferralComponent } from './app-referral.component';

describe('AppReferralComponent', () => {
  let component: AppReferralComponent;
  let fixture: ComponentFixture<AppReferralComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AppReferralComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppReferralComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
