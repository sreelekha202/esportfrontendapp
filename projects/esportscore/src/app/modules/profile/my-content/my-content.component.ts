import {
  Component,
  OnDestroy,
  OnInit,
  EventEmitter,
  ViewChild,
  AfterViewInit,
} from '@angular/core';
import {
  ArticleApiService,
  VideoLibraryService,
} from '../../../core/service';
import { EsportsUserService, EsportsLanguageService, EsportsToastService, CustomTranslatePipe } from 'esports';
import { TranslateService } from '@ngx-translate/core';
import { ActivatedRoute, Router } from '@angular/router';
import {
  InfoPopupComponent,
  InfoPopupComponentData,
  InfoPopupComponentType,
} from '../../../shared/popups/info-popup/info-popup.component';
import { MatDialog } from '@angular/material/dialog';
import { GlobalUtils } from '../../../shared/service/global-utils/global-utils';
import { Subscription } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { MatTabChangeEvent, MatTabGroup } from '@angular/material/tabs';

@Component({
  selector: 'app-my-transactions',
  templateUrl: './my-content.component.html',
  styleUrls: ['./my-content.component.scss'],
  providers: [CustomTranslatePipe],
})
export class MyContentComponent implements OnInit, OnDestroy, AfterViewInit {
  rows = [];
  columns = [
    { name: 'DATE' },
    { name: 'TITLE' },
    { name: 'VIEWS' },
    { name: 'TOTAL LIKES' },
    { name: 'ARTICLE STATUS' },
  ];
  userId;
  videoLibraryColumns = [
    { name: 'TITLE' },
    { name: 'DESCRIPTION' },
    { name: 'LAST UPDATE' },
    { name: 'ACTION' },
  ];
  videoLibraryDetails;
  videoLibraryPagination = {
    page: 1,
    limit: 4,
    sort: '-updatedOn',
    projection: ['title', 'description', 'updatedOn', 'slug'],
  };
  selectedVideo;
  activetab = 'article';
  showLoader = true;
  activeTabIndex = 0;
  customTabEvent = new EventEmitter<MatTabChangeEvent>();
  @ViewChild('tabs') tabGroup: MatTabGroup;

  userSubscription: Subscription;

  constructor(
    private videoLibraryService: VideoLibraryService,
    private eSportsToastService: EsportsToastService,
    private userService: EsportsUserService,
    public articleService: ArticleApiService,
    public translate: TranslateService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private matDialog: MatDialog,
    private customTranslatePipe: CustomTranslatePipe,
    public languageService: EsportsLanguageService
  ) {}

  ngOnInit(): void {
    this.getCurrentUserDetails();
    this.customTabEvent
      .pipe(debounceTime(30))
      .subscribe((tabChangeEvent: MatTabChangeEvent) => {
        this.attachTabChangeListener(tabChangeEvent);
      });
  }

  callContentData() {
    this.activatedRoute.queryParams
      .subscribe((params) => {
        if (params.activeTabIndex) {
          this.activeTabIndex = +params.activeTabIndex;
        } else {
          this.activeTabIndex = 0;
        }
        this.switchData(this.activeTabIndex);
      })
      .unsubscribe();
  }

  ngOnDestroy() {
    this.userSubscription?.unsubscribe();
  }

  ngAfterViewInit() {
    this.tabGroup.selectedIndex = this.activeTabIndex;
  }

  onTabChanged = (tabChangeEvent: MatTabChangeEvent): void => {
    this.customTabEvent.emit(tabChangeEvent);
  };

  attachTabChangeListener(tabChangeEvent: MatTabChangeEvent) {
    this.switchData(tabChangeEvent.index);
  }
  switchData(index: number) {
    switch (index) {
      case 0:
        this.router.navigate(['.'], {
          relativeTo: this.activatedRoute,
          queryParams: { activeTabIndex: index },
        });
        this.getArticles();
        this.showLoader = false;
        break;
      case 1:
        this.router.navigate(['.'], {
          relativeTo: this.activatedRoute,
          queryParams: { activeTabIndex: index },
        });
        this.fetchVideoLibraryList();
        this.showLoader = false;
        break;
      default:
        break;
    }
  }

  onRowCliCk(event): void {
    if (event.type === 'click') {
    }
  }

  // beforeTabChange(event) {
  //   this.router.navigate(['.'], {
  //     relativeTo: this.activatedRoute,
  //     queryParams: { activeTab: event.nextId },
  //   });
  //   if (event.nextId === 'article') {
  //     this.getArticles();
  //   } else if (event.nextId === 'video-library') {
  //     this.fetchVideoLibraryList();
  //   }
  // }

  /** fetch videos */
  fetchVideoLibraryList = async () => {
    try {
      const encodeURI = `?query=${encodeURIComponent(
        JSON.stringify({ createdBy: this.userId })
      )}&pagination=${encodeURIComponent(
        JSON.stringify(this.videoLibraryPagination)
      )}`;

      this.videoLibraryDetails = await this.videoLibraryService.fetchVideoLibrary(
        encodeURI
      );
      this.showLoader = false;
    } catch (error) {
      this.showLoader = false;
      this.eSportsToastService.showError(error?.error?.message || error?.message);
    }
  };

  /** change page */
  onPageChange(event) {
    this.videoLibraryPagination.page = event.offset + 1;
    this.fetchVideoLibraryList();
  }

  /** remove video */
  deleteVideo = async (video) => {
    try {
      const data: InfoPopupComponentData = {
        title: this.translate.instant('PROFILE.CONTENTS.MODAL.TITLE'),
        text: `${this.translate.instant(
          'PROFILE.CONTENTS.MODAL.TEXT'
        )} ${this.customTranslatePipe.transform(video?.title)}.`,
        type: InfoPopupComponentType.confirm,
        btnText: this.translate.instant('BUTTON.CONFIRM'),
      };

      const result = await this.matDialog
        .open(InfoPopupComponent, { data })
        .afterClosed()
        .toPromise();
      if (result) {
        const deleteVideo = await this.videoLibraryService.deleteVideoLibrary(
          video?._id
        );
        this.eSportsToastService.showSuccess(deleteVideo?.message);
        this.fetchVideoLibraryList();
      }
    } catch (error) {
      this.eSportsToastService.showError(error?.error?.message || error?.message);
    }
  };

  /** Get current user */
  getCurrentUserDetails() {
    if (GlobalUtils.isBrowser()) {
      this.userSubscription = this.userService.currentUser.subscribe((data) => {
        if (data) {
          this.userId = data._id;
          this.callContentData();
        }
      });
    }
  }

  /** fetch user articles */
  getArticles = async () => {
    try {
      const query = JSON.stringify({ author: this.userId });
      const option = JSON.stringify({ sort: { createdDate: -1 } });
      const res: any = await this.articleService
        .getArticles({ query, option })
        .toPromise();
      this.showLoader = false;
      this.rows = res.data;
    } catch (error) {
      this.showLoader = false;
      this.eSportsToastService.showError(error?.error?.message || error?.message);
    }
  };
}
