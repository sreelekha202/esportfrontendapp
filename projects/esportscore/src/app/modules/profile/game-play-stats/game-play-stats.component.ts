import { Component, OnInit } from '@angular/core';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import { Label } from 'ng2-charts';
import { LeaderboardService } from '../../../../app/core/service/leaderboard.service';
import { EsportsGameApiService, EsportsUserService, EsportsToastService } from 'esports';
import { environment } from 'projects/esportscore/src/environments/environment';
import { DomSanitizer } from '@angular/platform-browser';
import { GAMES } from '../../../../app/shared/constants';
const TOKEN = environment.currentToken;
@Component({
  selector: 'app-game-play-stats',
  templateUrl: './game-play-stats.component.html',
  styleUrls: ['./game-play-stats.component.scss'],
})
export class GamePlayStatsComponent implements OnInit {

  steamId: string;

  currentGame: string = 'DOTA2';
  playerID: string = '76561198177128005';
  // params = {
  //   game: 'DOTA2',
  //   playerId: '76561198177128005'
  // };
  mock_game_statistic = {
    gameKey: 'DOTA2',
    gameTitle: 'DOTA 2',
    image: 'assets/images/articles/pic3.jpg',
    details: {
      matches: '2895',
      wins: '2761',
      losses: '2891',
      duration: '57.7%',
      goldPerMinute: '58.7%',
      assist: '67.7%',
      heroDmg: '85.7%',
      towerDmg: '45.6%',
      healing: '2891',
      mmr: '3721',
    },
  };

  mock_matches = [
    {
      image: 'assets/images/articles/pic1.jpg',
      name: 'Bruno',
      quantity: 25,
      isWinner: true,
      details: {
        kda: '4/8/11',
        net: '22,107',
        lhdh: '352 / 4',
        xpm: 546,
        dmg: '10,107',
        heal: 0,
      },
    },
    {
      image: 'assets/images/articles/pic2.jpg',
      name: 'Pio',
      quantity: 5,
      isWinner: true,
      details: {
        kda: '2/3/14',
        net: '5,107',
        lhdh: '122 / 75',
        xpm: 234,
        dmg: '5,107',
        heal: 5,
      },
    },
    {
      image: 'assets/images/articles/pic3.jpg',
      name: 'Loki',
      quantity: 64,
      isWinner: false,
      details: {
        kda: '53/31/14',
        net: '132,107',
        lhdh: '1432 / 755',
        xpm: 2534,
        dmg: '25,107',
        heal: 93,
      },
    },
  ];

  mock_barChartOptions: ChartOptions = {
    responsive: true,
    scales: { xAxes: [{}], yAxes: [{}] },
    plugins: {
      datalabels: {
        anchor: 'end',
        align: 'end',
      },
    },
  };
  mock_barChartLabels: Label[] = [
    '2006',
    '2007',
    '2008',
    '2009',
    '2010',
    '2011',
    '2012',
  ];
  mock_barChartType: ChartType = 'bar';
  mock_barChartLegend = true;
  mock_barChartPlugins = [];
  mock_barChartData: ChartDataSets[] = [
    { data: [65, 59, 80, 81, 56, 55, 40], label: 'Matches' },
    { data: [28, 48, 40, 19, 86, 27, 90], label: 'Wins' },
  ];

  constructor(
    private leaderboardService: LeaderboardService,
    private gameApiService: EsportsGameApiService,
    private sanitizer: DomSanitizer,
    private eSportsToastService: EsportsToastService,
    private userService: EsportsUserService
  ) { }
  recentMatches;
  totalRecentMatches;

  joinDotaLink;
  showLoader = false;

  ngOnInit(): void {

    this.userService.getProfile(environment.apiEndPoint, TOKEN).subscribe(res => {

      this.onLifeTimestats(this.currentGame,this.playerID)

      if (res.data.preference) {
        const gameDetails = res.data.preference.gameDetails;
        const dota2 = gameDetails.find(data => {
          return data.name == 'DOTA 2'
        })

        this.steamId = dota2.userGameId;
        this.playerID = dota2.userGameId;
        if (dota2.userGameId) {
          // const params = {
          //   gameType: 'DOTA2',
          //   gamerId: dota2.userGameId || '76561198177128005',
          // };

          this.onLifeTimestats(this.currentGame,this.playerID)
          this.getRecentMatches(this.currentGame,this.playerID)
          this.getPlayerHistogram(this.currentGame,this.playerID)
        }
      }

    }, err => {
      this.eSportsToastService.showError('Could not find steamid')
    })

  }

  gameSelected(data) {
    this.currentGame = data.gameKey;
    console.log('this.currentGame', this.currentGame);
    console.log('this.steamId', this.steamId);

    this.mock_game_statistic.gameKey = this.currentGame;
    this.mock_game_statistic.gameTitle = GAMES[this.currentGame];

    this.getLifetimeStats(this.currentGame,this.playerID);

  }

  onTabSelected(data) {

    console.log('onTabSelected', data);
    console.log('this.currentGame', this.currentGame);

    if (this.steamId) {
      // const params = {
      //   gameType: 'DOTA2',
      //   gamerId: this.steamId,
      // };

      if (data.selected) {

        this.getStcStats(this.currentGame,this.playerID)

      }

      if (!data.selected) {
        this.onLifeTimestats(this.currentGame,this.playerID)
      }
    }

  }

  getLifetimeStats(game,player) {
    this.showLoader = true;

    if (game === 'CSGO') {
      this.gameApiService.getCSGOPlayerStats({
        gameType: game,
        gamerId: player,
      }).subscribe(
        (res) => {

          console.log('res', res);

          this.showLoader = false;
        },
        (err) => {
          console.log('err', err);
          this.showLoader = false;
          this.eSportsToastService.showError(err?.error?.message || err?.message);
        }
      );
    }

    else if (game === 'DOTA2') {
      this.gameApiService.getUsergetMedal({
        gameType: game,
        gamerId: player,
      }).subscribe(
        (res) => {

          if (res.data) {

            let gold_per_min = res.data.find((ele) => {
              return ele.field == 'gold_per_min';
            });
            let duration = res.data.find((ele) => {
              return ele.field == 'duration';
            });
            let hero_healing = res.data.find((ele) => {
              return ele.field == 'hero_healing';
            });
            let assists = res.data.find((ele) => {
              return ele.field == 'assists';
            });
            let tower_damage = res.data.find((ele) => {
              return ele.field == 'tower_damage';
            });
            let hero_damage = res.data.find((ele) => {
              return ele.field == 'hero_damage';
            });


            this.mock_game_statistic['details']['goldPerMinute'] =
              gold_per_min != undefined ? Math.floor(gold_per_min.sum).toLocaleString() : '0';
            this.mock_game_statistic['details']['duration'] =
              duration != undefined
                ? this.SecondsTohhmmss(duration.sum)
                : '0';

            this.mock_game_statistic['details']['healing'] =
              hero_healing != undefined ? this.abbreviateNumber(hero_healing.sum) : '0';

            this.mock_game_statistic['details']['heroDmg'] =
              hero_damage != undefined ? this.abbreviateNumber(hero_damage.sum) : '0';

            this.mock_game_statistic['details']['assist'] =
              assists != undefined && assists != 0 ? Math.floor(assists.sum).toLocaleString() : '0';

            this.mock_game_statistic['details']['towerDmg'] =
              tower_damage != undefined ? this.abbreviateNumber(tower_damage.sum) : '0';
          }

          this.showLoader = false;
        },
        (err) => {
          this.showLoader = false;
          this.eSportsToastService.showError(err?.error?.message || err?.message);
        }
      );
    }
    else {
      this.showLoader = false;
    }
  }


  getWinLoss(game,player) {
    this.showLoader = true;
    this.gameApiService.getUserWinsLose({
      gameType: game,
      gamerId: player
    }).subscribe(
      (res) => {
        if (res.data) {
          this.mock_game_statistic['details']['wins'] = res.data['win'] ? Math.floor(res.data['win']).toLocaleString() : '0';
          this.mock_game_statistic['details']['losses'] = res.data['lose'] ? Math.floor(res.data['lose']).toLocaleString() : '0';
          if (res.data['win'] && res.data['lose']) {
            this.mock_game_statistic['details']['matches'] = Math.floor(res.data['win'] + res.data['lose']).toLocaleString()
          } else {
            this.mock_game_statistic['details']['matches'] = "0"
          }

        } else {
        }

        this.showLoader = false;
      },
      (err) => {
        this.showLoader = false;
        this.eSportsToastService.showError(err?.error?.message || err?.message);
      }
    );
  }

  getDotaJoinLink() {
    this.showLoader = true;
    this.gameApiService.getJoinDotaLink(this.steamId).subscribe(
      (res) => {
        this.joinDotaLink = res['joinUrl'];
        this.showLoader = false;
        // this.eSportsToastService.showSuccess('Invitation will be sent on your game.');
        this.eSportsToastService.showSuccess(res.data || res.message);
      },
      (err) => {
        this.showLoader = false;
        this.eSportsToastService.showError(err?.error?.message || err?.message);
      }
    );
  }

  onLifeTimestats(game,player) {
    // const params = {
    //   gameType: 'DOTA2',
    //   gamerId: this.steamId || '76561198177128005',
    // }

    this.getLifetimeStats(game,player)
    this.getWinLoss(game,player)
    this.getMMR(game,player)

  }


  getMMR(game,player) {
    this.showLoader = true;
    this.gameApiService.getMMR({
      gameType: game,
      gamerId: player
    }).subscribe(
      (res) => {
        if (res.data) {
          this.mock_game_statistic['details']['mmr'] =
            res.data['mmr_estimate']['estimate'] != undefined
              ? Math.floor(res.data['mmr_estimate']['estimate']).toLocaleString()
              : '0';
        } else {
        }

        this.showLoader = false;
      },
      (err) => {
        this.showLoader = false;
        this.eSportsToastService.showError(err?.error?.message || err?.message);
      }
    );
  }


  getRecentMatches(game,player) {
    this.showLoader = true;

    this.gameApiService.getUsersMatchHistory({
      gameType: game,
      gamerId: player
    }).subscribe (
      (data) => {

        if (data) {
          this.showLoader = false;

          this.recentMatches = data['data'];
          this.totalRecentMatches = this.recentMatches.length;
        } else {
        }

        this.showLoader = false;
      },
      (err) => {
        this.showLoader = false;
        this.eSportsToastService.showError(err?.error?.message || err?.message);
      }
    );
  }

  getPlayerHistogram(game,player) {
    this.showLoader = true;
    this.gameApiService.getPlayerHistogram({
      gameType: game,
      gamerId: player
    }).subscribe(
      (res) => {
        if (res.data) {
          this.showLoader = false;


          let matches = [];
          let wins = [];
          let x = [];

          for (let e of res.data) {
            matches.push(e.games);
            wins.push(e.win);
            x.push(e.x.toString());
          }
          this.mock_barChartLabels = x;
          this.mock_barChartData[0]['data'] = matches;
          this.mock_barChartData[1]['data'] = wins;


        } else {
        }
      },
      (err) => {
        this.showLoader = false;
        this.eSportsToastService.showError(err?.error?.message || err?.message);
      }
    );
  }


  getStcStats(game,player) {
    this.showLoader = true;
    this.gameApiService.getStcStats({
      gameType: game,
      gamerId: player
    }).subscribe(
      (res) => {
        if (res.data) {
          this.showLoader = false;

          this.mock_game_statistic['details']['wins'] = res.data['totalWins'] ? Math.floor(res.data['totalWins']).toLocaleString() : '0';
          this.mock_game_statistic['details']['losses'] = res.data['totalLoses'] ? Math.floor(res.data['totalLoses']).toLocaleString() : '0';
          this.mock_game_statistic['details']['matches'] = res.data['totalmatches'] ? Math.floor(res.data['totalmatches']).toLocaleString() : '0';

          this.mock_game_statistic['details']['goldPerMinute'] = res.data['avgGpm'] ? Math.floor(res.data['avgGpm']).toLocaleString() : '0';
          this.mock_game_statistic['details']['duration'] = res.data['totalDuration'] ? this.formatSeconds(res.data['totalDuration']) : '0';


          this.mock_game_statistic['details']['healing'] = res.data['totalHeroHealing'] ? Math.floor(res.data['totalHeroHealing']).toLocaleString() : '0';

          this.mock_game_statistic['details']['heroDmg'] = res.data['totalHeroDmg'] ? Math.floor(res.data['totalHeroDmg']).toLocaleString() : '0';

          this.mock_game_statistic['details']['assist'] = res.data['totalAssits'] ? Math.floor(res.data['totalAssits']).toLocaleString() : '0';

          this.mock_game_statistic['details']['towerDmg'] = res.data['totalTowDmg'] ? Math.floor(res.data['totalTowDmg']).toLocaleString() : '0';
          this.mock_game_statistic['details']['mmr'] = res.data['totalKills'] ? Math.floor(res.data['totalKills']).toLocaleString() : '0';
        }
      },
      (err) => {
        this.showLoader = false;
        this.eSportsToastService.showError(err?.error?.message || err?.message);
      }
    );
  }



  SecondsTohhmmss(n) {
    const day = Math.floor(n / (24 * 3600));
    n = n % (24 * 3600);
    const hour = Math.floor(n / 3600);
    n %= 3600;
    const minutes = Math.floor(n / 60);
    n %= 60;
    const seconds = Math.floor(n);
    return (
      day + ' days ' + hour + ' hr ' + minutes + ' min ' + seconds + ' sec'
    );
  }

  startGame() {
  }

  sanitize(url: string) {
    return this.sanitizer.bypassSecurityTrustUrl(url);
  }

  formatSeconds(input) {
    if (!Number.isNaN(parseFloat(input)) && Number.isFinite(Number(input))) {
      const absTime = Math.abs(input);
      const minutes = Math.floor(absTime / 60);
      const seconds = this.pad(Math.floor(absTime % 60), 2);

      let time = ((input < 0) ? '-' : '');
      time += `${minutes}:${seconds}`;

      return time;
    }

    return null;
  }

  pad(n, width, z = '0') {
    const str = `${n}`;
    return str.length >= width ? str : new Array((width - str.length) + 1).join(z) + n;
  }

  abbreviateNumber(num) {
    if (!num) {
      return '-';
    } else if (num >= 1000 && num < 1000000) {
      return `${Number((num / 1000).toFixed(1))}k`;
    } else if (num >= 1000000 && num < 1000000000) {
      return `${Number((num / 1000000).toFixed(1))}m`;
    } else if (num >= 1000000000 && num < 1000000000000) {
      return `${Number((num / 1000000000).toFixed(1))}b`;
    } else if (num >= 1000000000000) {
      return `${Number((num / 1000000000000).toFixed(1))}t`;
    }

    return num.toFixed(0);
  }
}
