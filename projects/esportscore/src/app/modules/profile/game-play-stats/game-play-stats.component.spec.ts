import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GamePlayStatsComponent } from './game-play-stats.component';

describe('GamePlayStatsComponent', () => {
  let component: GamePlayStatsComponent;
  let fixture: ComponentFixture<GamePlayStatsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GamePlayStatsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GamePlayStatsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
