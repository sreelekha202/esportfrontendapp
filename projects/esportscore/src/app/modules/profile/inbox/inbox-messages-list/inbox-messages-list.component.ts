import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { faEye, faTrash } from '@fortawesome/free-solid-svg-icons';

import { MessageService } from '../../../../core/service';
import { IMessage } from '../../../../shared/models';

@Component({
  selector: 'app-inbox-messages-list',
  templateUrl: './inbox-messages-list.component.html',
  styleUrls: ['./inbox-messages-list.component.scss'],
})
export class InboxMessagesListComponent implements OnInit {
  faEye = faEye;
  faDelete = faTrash;
  modalRef: BsModalRef;
  inboxMessages: Array<IMessage> = [];
  messageCheckboxes: any[] = [];
  selectedMessageIds: Array<string> = [];
  deleteConfirmationMessage: string =
    'Are you sure you want to delete the message?';

  constructor(
    private messageService: MessageService,
    private modalService: BsModalService
  ) {}

  ngOnInit(): void {
    this.getInboxMessages();
  }

  getInboxMessages(): void {
    this.messageService.getInboxMessages().subscribe(
      (response) => {
        this.inboxMessages = response.data;
        this.messageCheckboxes = this.inboxMessages.map((inboxMessage) => {
          return { value: inboxMessage._id, checked: false };
        });
      },
      (error) => {
        console.error(error);
      }
    );
  }

  deleteMessage(
    messageId: string,
    delete_confirmation_template: TemplateRef<any>
  ): void {
    this.selectedMessageIds = [messageId];
    this.deleteConfirmationMessage =
      'Are you sure you want to delete the message?';
    this.modalRef = this.modalService.show(delete_confirmation_template, {
      class: 'modal-md',
    });
  }

  deleteSelectedMessage(delete_confirmation_template: TemplateRef<any>): void {
    const selectedCheckbox = this.messageCheckboxes.filter(
      (checkbox) => checkbox.checked
    );
    this.selectedMessageIds = selectedCheckbox.map(
      (checkbox) => checkbox.value
    );
    this.deleteConfirmationMessage =
      'Are you sure you want to delete the selected message?';
    this.modalRef = this.modalService.show(delete_confirmation_template, {
      class: 'modal-sm',
    });
  }

  confirmDelete(): void {
    this.modalRef.hide();
    this.messageService
      .deleteMultipleMessage(this.selectedMessageIds)
      .subscribe((response) => {
        this.getInboxMessages();
      });
  }

  declineDelete(): void {
    this.modalRef.hide();
  }

  selectAllMessages() {
    if (this.messageCheckboxes.every((val) => val.checked == true))
      this.messageCheckboxes.forEach((val) => {
        val.checked = false;
      });
    else
      this.messageCheckboxes.forEach((val) => {
        val.checked = true;
      });
  }
}
