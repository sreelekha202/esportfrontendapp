import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InboxMessagesListComponent } from './inbox-messages-list.component';

describe('InboxMessagesListComponent', () => {
  let component: InboxMessagesListComponent;
  let fixture: ComponentFixture<InboxMessagesListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [InboxMessagesListComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InboxMessagesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
