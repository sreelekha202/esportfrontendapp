import { faAngleUp } from '@fortawesome/free-solid-svg-icons';
import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';

import { MessageService } from '../../../../core/service';
import { IMessage } from '../../../../shared/models';
import { IUser, EsportsUserService } from 'esports';
import { InboxMessageItemComponentType } from '../inbox-message-item/inbox-message-item.component';

import { environment } from '../../../../../environments/environment';

const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;

@Component({
  selector: 'app-inbox-message',
  templateUrl: './inbox-message.component.html',
  styleUrls: ['./inbox-message.component.scss'],
})
export class InboxMessageComponent implements OnInit {
  InboxMessageItemComponentType = InboxMessageItemComponentType;
  faAngleUp = faAngleUp;
  messagesArray: Array<IMessage> = [];
  replyMessages: Array<IMessage> = [];
  parentMessage: IMessage;
  loggedInUser: IUser;
  replyToUserId: string;
  replyMessage = '';

  constructor(
    private location: Location,
    private route: ActivatedRoute,
    private messageService: MessageService,
    private userService: EsportsUserService
  ) {}

  ngOnInit(): void {
    this.loggedInUser = this.userService.getAuthenticatedUser(API, TOKEN);
    this.getMessage(this.route.snapshot.params.id);
  }

  getMessage(messageId: string): void {
    this.messageService.getMessage(messageId).subscribe(
      (response) => {
        this.messagesArray = response.data;
        if (this.messagesArray.length > 0) {
          this.parentMessage = this.messagesArray[0];
          this.replyToUserId =
            this.parentMessage.toUser === this.loggedInUser._id
              ? this.parentMessage.senderDetails._id
              : this.parentMessage.toUser;
          if (this.parentMessage.seen === false) {
            this.messageService
              .seenMessage(messageId, { seen: true })
              .subscribe((response) => {});
          }
          if (this.messagesArray.length > 1) {
            this.replyMessages = this.messagesArray.slice(1);
          }
        }
      },
      (error) => {
        console.error(error);
      }
    );
  }

  sendReply() {
    const inputData = {
      toUser: this.replyToUserId,
      subject: 'Reply Message',
      message: this.replyMessage,
      parentId: this.parentMessage._id,
    };
    this.messageService.sendMessage(inputData).subscribe(
      (response) => {
        this.replyMessage = '';
        this.replyMessages.push(response.data);
      },
      (error) => {
        console.error(error);
      }
    );
  }

  saveReplyMessage(message: string) {
    this.replyMessage = message;
  }
  goBack() {
    this.location.back();
  }
}
