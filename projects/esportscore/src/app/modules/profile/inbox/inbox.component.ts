import { Component, OnInit } from '@angular/core';
import { faTrash } from '@fortawesome/free-solid-svg-icons';

import { MessageService } from '../../../core/service';
import { IMessage } from '../../../shared/models';

@Component({
  selector: 'app-inbox',
  templateUrl: './inbox.component.html',
  styleUrls: ['./inbox.component.scss'],
})
export class InboxComponent implements OnInit {
  faTrash = faTrash;
  inboxMessages: Array<IMessage> = [];

  constructor(private messageService: MessageService) {}

  ngOnInit(): void {
    this.getInboxMessages();
  }

  onSelectAll(): void {
    if (this.inboxMessages.every((item) => item.isChecked)) {
      this.inboxMessages.forEach((item) => (item.isChecked = false));
    } else {
      this.inboxMessages.forEach((item) => (item.isChecked = true));
    }
  }

  onDeleteChecked(): void {
    const selectedMessages = this.inboxMessages.filter(
      (item) => item.isChecked
    );
    const selectedMessagesId = selectedMessages.map((item) => item._id);
    this.messageService
      .deleteMultipleMessage(selectedMessagesId)
      .subscribe((response) => {
        this.getInboxMessages();
      });
    this.inboxMessages = this.inboxMessages.filter((item) => !item.isChecked);
  }

  onDeleteOne(index: number, messageId: string): void {
    this.inboxMessages.splice(index, 1);
    this.messageService
      .deleteMultipleMessage([messageId])
      .subscribe((response) => {
        this.getInboxMessages();
      });
  }

  getInboxMessages(): void {
    this.messageService.getInboxMessages().subscribe(
      (response) => {
        this.inboxMessages = response.data;
      },
      (error) => {
        console.error(error);
      }
    );
  }
}
