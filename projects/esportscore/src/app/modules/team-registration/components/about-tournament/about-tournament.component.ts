import { Component, Input, OnInit } from '@angular/core';
import { EsportsTimezone } from 'esports';
@Component({
  selector: 'app-about-tournament',
  templateUrl: './about-tournament.component.html',
  styleUrls: ['./about-tournament.component.scss'],
})
export class AboutTournamentComponent implements OnInit {
  @Input() tournament;
  timezone;

  constructor(private esportsTimezone: EsportsTimezone) {}

  ngOnInit(): void {
    this.timezone = this.esportsTimezone.getTimeZoneName();
  }

  isNumber(val): boolean { const str: string = val.toString(); return str.startsWith("+"); }

  ngOnChanges() {}
}
