import { NgModule } from '@angular/core';

import { SearchTournamentsComponent } from './search-tournaments/search-tournaments.component';
import { SearchArticleComponent } from './search-article/search-article.component';
import { SearchVideoComponent } from './search-video/search-video.component';
import { SearchAllComponent } from './search-all/search-all.component';
import { SharedModule } from '../../shared/modules/shared.module';
import { SearchRoutingModule } from './search-routing.module';
import { SearchComponent } from './search.component';
import { CoreModule } from '../../core/core.module';
import { TournamentsCardModule } from '../home2/tournaments-card/tournaments-card.module';

@NgModule({
  declarations: [
    SearchComponent,
    SearchAllComponent,
    SearchTournamentsComponent,
    SearchArticleComponent,
    SearchVideoComponent,
  ],
  imports: [
    SharedModule,
    CoreModule,
    SearchRoutingModule,
    TournamentsCardModule,
  ],
})
export class SearchModule {}
