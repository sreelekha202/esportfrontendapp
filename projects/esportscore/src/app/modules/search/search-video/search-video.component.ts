import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IPagination } from 'projects/esports/src/lib/models/paginations';
import { HomeService } from '../../../core/service';

@Component({
  selector: 'app-search-video',
  templateUrl: './search-video.component.html',
  styleUrls: ['./search-video.component.scss'],
})
export class SearchVideoComponent implements OnInit {
  isVideoFlag = false;
  constructor(public homeService: HomeService,public activatedRoute: ActivatedRoute,) {}
  page: IPagination;
  paginationData = {
    page: 1,
    limit: 6,
    sort: "-startDate",
  };
  text;

  ngOnInit(): void {
    this.activatedRoute.queryParamMap.subscribe((e) => {
      if (e.get('text')) {
        this.text = e.get('text');
      }
    });
    this.getSearchVideo();
  }

  getSearchVideo(){
    this.homeService.searchedVideo.subscribe((res:any) => {
      if (res?.docs?.length > 0) this.isVideoFlag = true;
      this.page = {
        totalItems: res?.totalDocs,
        itemsPerPage: res?.limit,
        maxSize: 5,
      };
    });
  }

  pageChanged(page): void {
    this.paginationData.page = page;
    this.homeService.searchVideo(this.text, this.paginationData);
  }
}
