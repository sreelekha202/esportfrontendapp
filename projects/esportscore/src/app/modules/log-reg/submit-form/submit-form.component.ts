import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import {
  CountryISO,
  NgxIntlTelInputComponent,
  SearchCountryField,
  PhoneNumberFormat,
} from 'ngx-intl-tel-input';
import { faFacebookF, faTwitter } from '@fortawesome/free-brands-svg-icons';
import { Country } from 'ngx-intl-tel-input/lib/model/country.model';
import { faEnvelope } from '@fortawesome/free-solid-svg-icons';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import {
  AppHtmlRoutes,
  AppHtmlRoutesLoginType,
} from '../../../app-routing.model';
import { AuthServices } from '../../../core/service';
import { EsportsLanguageService, EsportsToastService } from 'esports';
import { GlobalUtils } from '../../../shared/service/global-utils/global-utils';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { TranslateService } from '@ngx-translate/core';
import { take } from 'rxjs/operators';
import { environment } from '../../../../environments/environment';
import { ReCaptcha2Component } from 'ngx-captcha';
import { LowerCasePipe } from '@angular/common';

export interface SubmitFormComponentOutputData {
  [key: string]: string;
}

export interface SubmitFormComponentDataItem {
  title: string;
  subtitle: string;
  checkboxLabel: string;
  checkboxLabelpp: string;
  submitBtn: string;
  or: string;
  orLabel: string;
  footerText: string;
  footerLink: string;
  footerLinkUrl: string[];
  firstInputName: string;
  firstInputType: string;
  firstInputPlaceholder: string;
  firstInputIcon: string;
  fifthInputName: string;
  fifthInputType: string;
  fifthInputPlaceholder: string;
  fifthInputIcon: string;
  secondInputName: string;
  secondInputType: string;
  secondInputPlaceholder: string;
  secondInputIcon: string;
  thirdInputName: string;
  thirdInputType: string;
  thirdInputPlaceholder: string;
  thirdInputIcon: string;
  fourthInputName: string;
  fourthInputType: string;
  fourthInputPlaceholder: string;
  fourthInputIcon: string;
  sixInputName: string;
  sixInputType: string;
  sixInputPlaceholder: string;
  sixInputIcon: string;
  forgotPass?: string;
  error?: string;
  isRegistrationForGamer: boolean;
  data: any;
}

declare var $: any;
@Component({
  selector: 'app-submit-form',
  templateUrl: './submit-form.component.html',
  styleUrls: ['./submit-form.component.scss'],
})
export class SubmitFormComponent implements OnInit {
  @Output() dateChange: EventEmitter<MatDatepickerInputEvent<any>>;
  @ViewChild('ngxTel') ngxTel: NgxIntlTelInputComponent;
  @ViewChild('captchaElem') captchaElem: ReCaptcha2Component;
  @Output() submit = new EventEmitter();
  @Input() data: SubmitFormComponentDataItem;
  @Input() registrationStep: Number;
  timeoutId = null;
  test: Number;
  LANGUAGE = 'en';
  RECAPTCHA_V2_SITE_KEY = environment.RECAPTCHA_V2_SITE_KEY;
  showToast: boolean;
  @Input() set type(type: AppHtmlRoutesLoginType) {
    this.activeTypeValue = type;

    if (this.activeTypeValue === AppHtmlRoutesLoginType.registration) {
      this.form
        .get('firstInput')
        .setValidators([
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(100),
          Validators.pattern(
            /^[a-zA-Z0-9\u0600-\u06FF,!@#$&()\\-`.+,/\"][\sa-zA-Z0-9\u0600-\u06FF,!@#$*%&()\\-`.+,/\"]*$/
          ),
        ]);
      this.form
        .get('fifthInput')
        .setValidators([
          Validators.required,
          Validators.minLength(3),
          Validators.pattern(/^[a-zA-Z0-9_]+$/),
        ]);
      this.form
        .get('thirdInput')
        .setValidators([Validators.required, Validators.email]);
      this.form.get('fourthInput').setValidators([Validators.required]);
      this.form.get('sixInput').setValidators([Validators.required]);
      this.form.get('privacyPolicy').setValidators(Validators.pattern('true'));
      this.form.get('dobPolicy').setValidators(Validators.pattern('false'));
      this.form.get('dobPolicy').updateValueAndValidity();
    }
    if (this.activeTypeValue === AppHtmlRoutesLoginType.emailLogin) {
      this.form
        .get('firstInput')
        .setValidators([Validators.required, Validators.email]);
      this.form.get('dobPolicy').updateValueAndValidity();
    }
  }

  AppHtmlRoutes = AppHtmlRoutes;
  AppHtmlRoutesLoginType = AppHtmlRoutesLoginType;
  activeTypeValue: AppHtmlRoutesLoginType;

  form = this.formBuilder.group({
    firstInput: ['', Validators.required],
    secondInput: ['', Validators.required],
    thirdInput: [''],
    fourthInput: [''],
    fifthInput: [''],
    keepMeLoggedIn: [false],
    privacyPolicy: [false],
    dobPolicy: [false],
    sixInput: [''],
    // captcha: ['', Validators.required],
  });

  todayDate: Date = new Date(
    new Date().setFullYear(new Date().getFullYear() - 10)
  );
  parentalDate: Date = new Date(
    new Date().setFullYear(new Date().getFullYear() - 18)
  );
  parental = false;
  isExist = false;

  faFacebookF = faFacebookF;
  faTwitter = faTwitter;
  faEnvelope = faEnvelope;
  isLoading = false;
  SearchCountryField = SearchCountryField;
  PhoneNumberFormat = PhoneNumberFormat;
  CountryISO = CountryISO;
  preferredCountries: CountryISO[] = [
    CountryISO.SaudiArabia,
    CountryISO.Bahrain,
    CountryISO.Kuwait,
    CountryISO.India,
  ];
  selectedCountryISO = CountryISO.India;
  mySelectedCountryISO: CountryISO;

  constructor(
    private formBuilder: FormBuilder,
    private translateService: TranslateService,
    private router: Router,
    public eSportsToastService: EsportsToastService,
    private authServices: AuthServices,
    private languageService: EsportsLanguageService,
    private lowercase: LowerCasePipe
  ) {
    this.checkForKeepMeLoggedIn();
  }

  ngOnInit() {
    this.showToast = true;
    this.getCurrentLang();

    // let data = {
    //   firstInput: 'dynamo',
    // };
  }

  isUniqueName = async (name) => {
    try {
      const isAvailable = async () => {
        try {
          const response = await this.authServices
            .searchUsername(name)
            .toPromise();
          this.isExist = response.data?.isExist;
          if (this.isExist) {
            this.form.controls['fifthInput'].setErrors({
              incorrect: true,
            });
          } else {
            this.form.controls['fifthInput'].setErrors(null);
            this.form.controls['fifthInput'].setValidators([
              Validators.required,
              Validators.minLength(3),
              Validators.pattern(/^[a-zA-Z0-9_]+$/),
            ]);

            this.form.controls['fifthInput'].updateValueAndValidity();
          }
        } catch (error) {
          throw error;
        } finally {
          clearTimeout(this.timeoutId);
        }
      };
      if (!this.timeoutId) {
        this.timeoutId = setTimeout(isAvailable, 500);
      } else {
        clearTimeout(this.timeoutId);
        this.timeoutId = setTimeout(isAvailable, 500);
      }
    } catch (error) {
      this.eSportsToastService.showError(
        error?.error?.message || error?.message
      );
    }
  };

  ngOnChanges() {
    if (this.data?.data) {
      if (this.activeTypeValue === AppHtmlRoutesLoginType.registration) {
        this.form.patchValue({
          ...this.data.data,
        });

        const selectedDate = new Date(this.data.data.fourthInput);
        if (selectedDate > this.parentalDate && selectedDate < this.todayDate) {
          this.parental = true;
          this.form.get('dobPolicy').setValidators(Validators.pattern('true'));
          this.form.get('dobPolicy').setValue(true);
        } else {
          this.parental = false;
          this.form.get('dobPolicy').setValidators(Validators.pattern('false'));
          this.form.get('dobPolicy').setValue(false);
        }
        this.form.get('dobPolicy').updateValueAndValidity();
      }
    }
  }

  resolved(token: string) {
    if (this.data.isRegistrationForGamer) {
      this.isLoading = true;
      this.authServices.validateCaptcha({ captcha: token }).subscribe(
        (data) => {
          this.form.get('captcha').setValue(token);
          this.isLoading = false;
        },
        (err) => {
          this.isLoading = false;
          if (err?.error?.message) {
            this.eSportsToastService.showError(err?.error?.message);
          }
        }
      );
    } else {
      this.form.get('captcha').setValue(token);
    }
  }

  handleReset() {
    this.form.get('captcha').setValue('');
  }

  getCurrentLang() {
    this.languageService.language.subscribe((lang) => {
      this.LANGUAGE = lang || 'en';
    });
  }

  toast() {
    if (this.showToast == true) {
      if (this.activeTypeValue === this.AppHtmlRoutesLoginType.registration) {
        this.showToast = false;
        this.test = this.registrationStep;
        this.translateService
          .get('LOG_REG.ALERTS.TEN_YEARS')
          .pipe(take(1))
          .subscribe((text: string) => {
            // this.eSportsToastService.showError(text);
          });
      }
    }
  }

  onCountryChange(e: Country) {
    this.mySelectedCountryISO = e.iso2 as CountryISO;
    this.onKeepLoggedIn();
  }

  parentalCheckBox(date: any) {
    const selectedDate = new Date(date.value);
    if (selectedDate > this.parentalDate && selectedDate < this.todayDate) {
      this.parental = true;
      this.form.get('dobPolicy').setValidators(Validators.pattern('true'));
      this.form.get('dobPolicy').setValue(true);
    } else {
      this.parental = false;
      this.form.get('dobPolicy').setValidators(Validators.pattern('false'));
      this.form.get('dobPolicy').setValue(false);
    }
    this.form.get('dobPolicy').updateValueAndValidity();
  }

  onKeepLoggedIn() {
    if (GlobalUtils.isBrowser()) {
      if (this.form.get('privacyPolicy').value) {
        const keepMeLoggedIn = {
          keepMeLoggedIn: true,
          mySelectedCountryISO:
            this.mySelectedCountryISO || this.preferredCountries[0],
        };
        localStorage.setItem('keepMeLoggedIn', JSON.stringify(keepMeLoggedIn));
      } else {
        localStorage.removeItem('keepMeLoggedIn');
      }
    }
  }

  onSubmitEvent(): void {
    this.submit.emit({
      [this.data.firstInputName]: this.form.value.firstInput,
      [this.data.secondInputName]: this.form.value.secondInput,
      [this.data.thirdInputName]: this.form.value.thirdInput,
      [this.data.fourthInputName]: this.form.value.fourthInput,
      [this.data.fifthInputName]: this.form.value.fifthInput,
      [this.data.sixInputName]: this.form.value.sixInput,
      firstInputType: this.data.firstInputName,
      secondInputType: this.data.secondInputName,
      thirdInputType: this.data.thirdInputName,
      fourthInputType: this.data.fourthInputName,
      fifthInputType: this.data.fifthInputName,
      sixInputType: this.data.sixInputName,
      data: this.form.value,
    });
    if (this.captchaElem) {
      this.captchaElem.resetCaptcha();
    }
  }

  changeInput(input): void {
    if (input === 'phone') {
      this.data.secondInputName = 'email';
      this.data.secondInputType = 'email';
      this.data.secondInputPlaceholder = 'LOG_REG.LOGIN.firstInputPlaceholder1';
      this.data.secondInputIcon = 'email';
      this.form
        .get('secondInput')
        .setValidators([Validators.required, Validators.email]);
      this.form.get('secondInput').reset();
    } else {
      this.data.secondInputName = 'phone';
      this.data.secondInputType = 'phone';
      this.data.secondInputPlaceholder = 'LOG_REG.LOGIN.firstInputPlaceholder';
      this.data.secondInputIcon = 'phone';
      this.form.get('secondInput').reset();
    }
  }

  changeLoginType(type) {
    if (type === 'email') {
      this.router
        .navigateByUrl('/home', { skipLocationChange: true })
        .then(() => {
          this.router.navigate(['/user', AppHtmlRoutesLoginType.emailLogin]);
        });
    }
    if (type === 'phone') {
      this.router
        .navigateByUrl('/home', { skipLocationChange: true })
        .then(() => {
          this.router.navigate(['/user', AppHtmlRoutesLoginType.phoneLogin]);
        });
    }
  }

  forgot() {
    this.router
      .navigateByUrl('/home', { skipLocationChange: true })
      .then(() => {
        this.router.navigate(['/user', AppHtmlRoutesLoginType.forgotPass]);
      });
  }

  private checkForKeepMeLoggedIn(): void {
    if (GlobalUtils.isBrowser()) {
      const keepMeLoggedIn = localStorage.getItem('keepMeLoggedIn');
      if (keepMeLoggedIn) {
        const parsedKeepMeLoggedIn = JSON.parse(keepMeLoggedIn);
        if (parsedKeepMeLoggedIn) {
          this.form
            .get('privacyPolicy')
            .setValue(parsedKeepMeLoggedIn.keepMeLoggedIn);
          this.selectedCountryISO = parsedKeepMeLoggedIn.mySelectedCountryISO;
        }
      }
    }
  }
}
