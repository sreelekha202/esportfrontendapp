import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { LogRegComponent } from './log-reg.component';

const routes: Routes = [{ path: '', component: LogRegComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LogRegRoutingModule {}
