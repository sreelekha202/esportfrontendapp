import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import {
  AppHtmlRoutes,
  AppHtmlRoutesLoginType,
} from '../../../app-routing.model';
import { AuthServices } from '../../../core/service';
import { EsportsLanguageService } from 'esports';

export interface SelectProfileComponentDataItem {
  footerLabel: string;
  footerLinkText: string;
  footerLinkUrl: string[];
  submitBtn: string;
  subtitle: string;
  title: string;
}

const userLinks = new Map([
  [0, 'registration'],
  [1, 'registrationAsGameCenter'],
  [2, 'registrationAsSponser'],
  [3, 'registrationAsOrganizer'],
]);

const whiteimageArray = new Map([
  [0, 'assets/images/Register/active-gamer.svg'],
  [1, 'assets/images/Register/active-gamecenter.svg'],
  [2, 'assets/images/Register/icon-sponsor.svg'],
  [3, 'assets/images/Register/icon-organizer.svg'],
]);

@Component({
  selector: 'app-select-profile',
  templateUrl: './select-profile.component.html',
  styleUrls: ['./select-profile.component.scss'],
})
export class SelectProfileComponent implements OnInit {
  @Output() submit = new EventEmitter();

  data: SelectProfileComponentDataItem = {
    title: 'LOG_REG.SELECT_PROFILE.TITLE',
    subtitle: 'LOG_REG.SELECT_PROFILE.SUBTITLE',
    submitBtn: 'LOG_REG.SELECT_PROFILE.SUBMIT_BTN',
    footerLabel: 'LOG_REG.REGISTER.footerText',
    footerLinkText: 'LOG_REG.REGISTER.footerLink',
    footerLinkUrl: [
      AppHtmlRoutes.userPageType,
      AppHtmlRoutesLoginType.phoneLogin,
    ],
  };

  AppHtmlRoutesLoginType = AppHtmlRoutesLoginType;

  selectedProfile = 0;

  profiles = [];

  constructor(
    private router: Router,
    private authService: AuthServices,
    public languageService: EsportsLanguageService
  ) {}

  ngOnInit(): void {
    this.authService.getRegistrationType().subscribe((res) => {
      if (res && res.data.length) {
        this.profiles = res.data.map((item, index) => {
          return {
            name: item.name,
            image: item.imageUrl,
            url: AppHtmlRoutesLoginType[userLinks.get(item.statusId) || 0],
            whiteimage: whiteimageArray.get(index) || '',
          };
        });
      }
    });
  }

  onSelectProfile(index: number): void {
    this.selectedProfile = index;
  }

  onSubmitEvent(): void {
    this.router.navigate(['/user', this.profiles[this.selectedProfile].url]);
    // this.submit.emit({
    //   type: this.profiles[this.selectedProfile].type,
    // });
  }
}
