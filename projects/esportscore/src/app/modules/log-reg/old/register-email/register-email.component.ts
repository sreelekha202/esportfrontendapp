import { Component, OnInit, ViewChildren } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import {
  faUser,
  faInbox,
  faLock,
  faEnvelope,
  faEye,
  faEyeSlash,
  faCheck,
  faDotCircle,
} from '@fortawesome/free-solid-svg-icons';
import { Router } from '@angular/router';
import { AuthServices } from '../../../../core/service';
import { environment } from '../../../../../environments/environment';
import { GlobalUtils } from '../../../../shared/service/global-utils/global-utils';

@Component({
  selector: 'app-register-email',
  templateUrl: './register-email.component.html',
  styleUrls: ['./register-email.component.scss'],
})
export class RegisterEmailComponent implements OnInit {
  faUser = faUser;
  faPhone = faInbox;
  faLock = faLock;
  faMail = faEnvelope;
  faEye = faEye;
  faEyeSlash = faEyeSlash;
  faCheck = faCheck;
  faDot = faDotCircle;
  fieldTextType = false;
  rfieldTextType = false;
  didFail = false;
  agree = '';
  message = '';
  public loading = false;
  token = '';
  @ViewChildren('formRow') rows: any;

  firstStepForm: FormGroup;
  OTPForm: FormGroup;
  MobileVerificationForm: FormGroup;

  submitted = false;
  misMatchPassword = false;
  submittedOTP = false;
  submittedMobile = false;

  stepZero = true;
  stepOne = false;
  stepTwo = false;
  stepThree = false;
  stepFour = false;
  stepFive = false;

  timeLeft = 60;
  interval;

  constructor(
    private fb: FormBuilder,
    private route: Router,
    private authService: AuthServices
  ) {
    if (
      GlobalUtils.isBrowser() &&
      localStorage.getItem(environment.currentToken)
    ) {
      this.route.navigate(['/home']);
    }
  }

  async ngOnInit() {
    // create form
    this.createForm();
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.firstStepForm.controls;
  }
  get otp() {
    return this.OTPForm.controls;
  }
  get m() {
    return this.MobileVerificationForm.controls;
  }

  // initialize form
  createForm() {
    this.firstStepForm = this.fb.group({
      name: [
        '',
        Validators.compose([
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(100),
        ]),
      ],
      emailId: [
        '',
        Validators.compose([Validators.required, Validators.email]),
      ],
      password: [
        '',
        Validators.compose([
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(100),
        ]),
      ],
      rpassword: [
        '',
        Validators.compose([
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(100),
        ]),
      ],
      privacy_policy: [false, Validators.compose([Validators.required])],
    });

    this.OTPForm = this.fb.group({
      code1: ['', Validators.compose([Validators.required])],
      code2: ['', Validators.compose([Validators.required])],
      code3: ['', Validators.compose([Validators.required])],
      code4: ['', Validators.compose([Validators.required])],
    });

    this.MobileVerificationForm = this.fb.group({
      phoneNo: ['', Validators.required],
    });
  }

  enableNext() {
    const controls = this.firstStepForm.controls;

    if (controls.name.status == 'INVALID') {
      controls.name.markAsTouched();

      this.stepOne = false;
    } else if (controls.emailId.status == 'INVALID') {
      controls.emailId.markAsTouched();
      this.stepZero = true;
      this.stepOne = false;
    } else if (!controls.privacy_policy.value) {
      controls.privacy_policy.markAsTouched();

      this.agree =
        'Please indicate that you have read and agree to the Terms and Conditions and Privacy Policy';

      this.stepZero = true;
      this.stepOne = false;
    } else {
      this.message = '';
      this.agree = '';
      this.stepZero = false;
      this.stepOne = true;
    }

    return;
  }

  enableStepTwo() {
    const controls = this.firstStepForm.controls;
    /** check form */
    if (this.firstStepForm.invalid) {
      Object.keys(controls).forEach((controlName) =>
        controls[controlName].markAsTouched()
      );
      this.didFail = true;

      return;
    } else if (controls.password.value !== controls.rpassword.value) {
      this.didFail = true;
      this.message = "password doesn't match";
      return;
    } else {
      this.loading = true;
      this.didFail = false;
      this.message = '';
      const register = {
        fullName: this.firstStepForm.value.name,
        password: this.firstStepForm.value.password,
        email: this.firstStepForm.value.emailId,
        type: 'email',
      };
      this.authService.register(register).subscribe(
        (data) => {
          this.didFail = false;
          this.message = '';
          this.token = data.data.token;

          this.stepOne = false;
          this.stepTwo = true;
          this.loading = false;

          this.interval = setInterval(() => {
            if (this.timeLeft > 0) {
              this.timeLeft--;
            }
            // else {
            //   this.timeLeft = 60;
            // }
          }, 1000);
        },
        (error) => {
          this.loading = false;
          this.didFail = true;
          this.message = error.error.message;
          this.stepZero = true;
          this.stepOne = false;
          this.stepTwo = false;
        }
      );
    }
  }

  keyUpEvent(event, index) {
    let pos = index;
    if (event.keyCode === 8 && event.which === 8) {
      pos = index - 1;
    } else {
      pos = index + 1;
    }
    if (pos > -1 && pos < 4) {
      this.rows._results[pos].nativeElement.focus();
    }
  }

  enableStepThree() {
    const controls = this.OTPForm.controls;
    /** check form */
    if (this.OTPForm.invalid) {
      Object.keys(controls).forEach((controlName) =>
        controls[controlName].markAsTouched()
      );
      this.didFail = true;
      this.message = 'Insert valid OTP';
      return;
    }

    this.didFail = false;
    this.message = '';
    this.loading = true;

    const obj = this.OTPForm.value;
    const code = obj.code1 + obj.code2 + obj.code3 + obj.code4;

    this.authService.confirmUser(this.token, code, 'email').subscribe(
      (data) => {
        this.didFail = false;
        this.message = '';
        this.loading = false;
        this.stepTwo = false;
        this.stepFive = true;
      },
      (error) => {
        this.loading = false;
        this.didFail = true;
        this.message = error.error.message;
        this.stepTwo = true;
        this.stepFive = false;
      }
    );
  }

  resendOtp() {
    let data = {
      email: this.firstStepForm.value.emailId,
      type: 'email',
    };
    this.authService.resendOTP(data).subscribe(
      (data) => {
        this.didFail = false;
        this.message = '';
        this.loading = false;
        this.timeLeft = 60;
      },
      (error) => {
        this.loading = false;
        this.didFail = true;
        this.message = error.error.message;
      }
    );
  }

  enableStepFour() {
    this.submittedMobile = true;
    if (this.MobileVerificationForm.invalid) {
      return;
    }
    this.stepThree = false;
    this.stepFour = true;
  }

  enableStepFive() {
    this.submittedOTP = true;
    if (this.OTPForm.invalid) {
      return;
    }
    this.stepFour = false;
    this.stepFive = true;
  }
  toggleFieldTextType() {
    this.fieldTextType = !this.fieldTextType;
  }
  rtoggleFieldTextType() {
    this.rfieldTextType = !this.rfieldTextType;
  }

  isControlHasError(controlName: string, validationType: string): boolean {
    const control = this.firstStepForm.controls[controlName];
    if (!control) {
      return false;
    }

    const result =
      control.hasError(validationType) && (control.dirty || control.touched);
    return result;
  }
}
