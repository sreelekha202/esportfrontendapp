import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PayfortComponent } from './payfort.component';

describe('PayfortComponent', () => {
  let component: PayfortComponent;
  let fixture: ComponentFixture<PayfortComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PayfortComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PayfortComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
