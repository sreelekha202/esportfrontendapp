import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StcPayComponent } from './stc-pay.component';

describe('StcPayComponent', () => {
  let component: StcPayComponent;
  let fixture: ComponentFixture<StcPayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [StcPayComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StcPayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
