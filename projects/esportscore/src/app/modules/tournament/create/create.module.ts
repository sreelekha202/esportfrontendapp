import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreateComponent } from './create.component';
// Module
import { SharedModule } from '../../../shared/modules/shared.module';
import { CoreModule } from '../../../core/core.module';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

// Routing
import { CreateRoutingModule } from './create-routing.module';
import { FormComponentModule } from '../../../shared/components/form-component/form-component.module';
import { VenueAddressComponent } from './venue-address/venue-address.component';

import { FaIconLibrary } from '@fortawesome/angular-fontawesome';
import { faTrash } from '@fortawesome/free-solid-svg-icons';
import { PaymentComponent } from './preview/payment/payment.component';
import { PreviewComponent } from './preview/preview.component';
import { OverviewComponent } from './preview/overview/overview.component';
import { PaymentModule } from '../../payment/payment.module';
import { WYSIWYGEditorModule } from 'esports';
import { environment } from '../../../../environments/environment';

@NgModule({
  declarations: [
    CreateComponent,
    VenueAddressComponent,
    PaymentComponent,
    PreviewComponent,
    OverviewComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    CoreModule,
    WYSIWYGEditorModule.forRoot(environment),
    MatProgressSpinnerModule,
    CreateRoutingModule,
    FormComponentModule,
    PaymentModule,
  ],
})
export class CreateModule {
  constructor(private library: FaIconLibrary) {
    library.addIcons(faTrash);
  }
}
