import {
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';
import { EsportsConstantsService } from 'esports'

@Component({
  selector: 'app-preview',
  templateUrl: './preview.component.html',
  styleUrls: ['./preview.component.scss'],
})
export class PreviewComponent implements OnInit, OnChanges {
  imageUrl = '';
  @Input() tournament;
  @Input() gameList;
  @Input() userFullName;

  checkInDt: Date;
  startDt: Date;

  constructor() {}

  ngOnInit(): void {}

  ngOnChanges(simpleChanges: SimpleChanges) {
    if (simpleChanges.hasOwnProperty('tournament') && this.tournament) {
      this.getCheckInAndStartDt(this.tournament);
      this.imageUrl = this.gameList.find(
        (item) => item._id == this.tournament.gameDetail
      ).logo;
    }
  }

  getCheckInAndStartDt(data) {
    this.startDt = new Date(data.startDate);
    this.startDt.setHours(data.startTime.hour || 0, data.startTime.minute || 0);

    if (this.tournament?.isCheckInRequired) {
      const duration = EsportsConstantsService.checkInTimeOptions.find(
        (item) => item.name == data.checkInStartDate
      ).value;
      this.checkInDt = new Date(this.startDt);
      this.checkInDt.setHours(
        this.checkInDt.getHours() - duration.hour,
        this.checkInDt.getMinutes() - duration.minute
      );
    }
  }
}
