import { Component, OnInit, OnDestroy } from '@angular/core';
import {
  UserReportsService,
  EsportsUserService,
  EsportsToastService,
  EsportImageService,
  S3UploadService,
} from 'esports';
import { TranslateService } from '@ngx-translate/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { environment } from '../../../../environments/environment';
import { Subscription } from 'rxjs';
import { take } from 'rxjs/operators';
@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.scss'],
})
export class ReportComponent implements OnInit, OnDestroy {
  base64textString;
  briefDescription;
  type;
  user;
  isLoaded = false;
  matchId;
  tournamentId;
  userSubscription: Subscription;

  constructor(
    private userService: EsportsUserService,
    private userReportsService: UserReportsService,
    private eSportsToastService: EsportsToastService,
    private translateService: TranslateService,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private location: Location,
    private s3UploadService: S3UploadService,
    private ImageService: EsportImageService
  ) {}

  ngOnInit(): void {
    this.getCurrentUserDetails();
    const { matchId, tournamentId } = this.activeRoute.snapshot.params;
    this.matchId = matchId;
    this.tournamentId = tournamentId;
  }

  ngOnDestroy() {
    this.userSubscription?.unsubscribe();
  }

  submitReport() {
    if (
      this.briefDescription &&
      this.type &&
      this.matchId &&
      this.user &&
      this.tournamentId
    ) {
      this.isLoaded = false;
      const data = {
        createrId: this.user._id, // userId
        tournamentId: this.tournamentId,
        matchId: this.matchId,
        reportType: this.type,
        reportText: this.briefDescription,
        reportImage: this.base64textString || '',
      };
      this.userReportsService
        .createReport(environment.apiEndPoint, data)
        .subscribe(
          (res: any) => {
            if (res && res.data) {
              this.eSportsToastService.showInfo(
                this.translateService.instant('USER_REPORT.REPORT_CREATED')
              );

              this.location.back();
            }
            this.isLoaded = true;
          },
          (err) => {
            this.isLoaded = true;
            const errorMessage = err?.error?.message || err?.message;
            this.eSportsToastService.showError(errorMessage);
          }
        );
    } else if (!this.type) {
      this.eSportsToastService.showError(
        this.translateService.instant('USER_REPORT.REPORT_REQUIRED_TYPE_FIELD')
      );
    } else {
      this.eSportsToastService.showError(
        this.translateService.instant(
          'USER_REPORT.REPORT_REQUIRED_COMMENT_FIELD'
        )
      );
    }
  }

  getCurrentUserDetails() {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.isLoaded = true;
        this.user = data;
      }
    });
  }

  selectType(reportType) {
    this.type = reportType;
  }

  async upload(event) {
    const files = event.target.files;
    let promises = [];
    for (let obj of files) {
      await new Promise((resolve, reject) => {
        this.ImageService.compress(obj)
          .pipe(take(1))
          .subscribe(async (compressedImage) => {
            promises.push(this.toBase64(compressedImage));
            resolve(true);
          });
      }
      );
    }
    let Base64String = await Promise.all(promises);
    let imageData = {
      path: 'report',
      files: Base64String,
    };

    await this.s3UploadService
      .fileUpload(environment.apiEndPoint,imageData)
      .subscribe(
        (res) => {
          if (
            res &&
            res['data'] &&
            res['data'][0] &&
            res['data'][0]['Location']
          ) {
            this.isLoaded = true;
            this.base64textString = res['data'][0]['Location'];
            this.eSportsToastService.showSuccess(
              this.translateService.instant('ARTICLE_POST.BANNER_UPLOAD')
            );
          }
        },
        (err) => {
          if (
            err &&
            err.error &&
            err.error.statusCode &&
            err.error.statusCode == 401
          ) {
            this.eSportsToastService.showError(
              this.translateService.instant('ARTICLE_POST.SESSION_EXPIRED')
            );
          } else {
            this.eSportsToastService.showError(
              this.translateService.instant('ARTICLE_POST.ERROR_BANNER')
            );
          }
        }
      );
  }

  async selectFile(event) {
    this.isValidImage(event, { height: 560, width: 1088 });
  }

  isValidImage(event: any, values): Boolean {
    if (event.target.files && event.target.files[0]) {
      if (event.target.files[0]['size'] / (1000 * 1000) > 2) {
        this.eSportsToastService.showError(
          this.translateService.instant('ARTICLE_POST.IMG_LESS')
        );
        return;
      }

      if (
        event.target.files[0].type != 'image/jpeg' &&
        event.target.files[0].type != 'image/jpg' &&
        event.target.files[0].type != 'image/png'
      ) {
        this.eSportsToastService.showError(
          this.translateService.instant('ARTICLE_POST.UPLOAD_CORRECT')
        );
        return;
      }

      try {
        const file = event.target.files[0];

        const reader = new FileReader();
        reader.readAsDataURL(file);
        let th = this;
        reader.onload = (e) => {
          let image: any = new Image();
          image.src = e.target.result;
          image.onload = function () {
            let height = this.height;
            let width = this.width;
            if (height == values['height'] && width == values['width']) {
              th.eSportsToastService.showInfo(
                this.translateService.instant('ARTICLE_POST.PROPER')
              );
            }
            th.upload(event);
            th.readURL(event);
          };
        };
      } catch (err) {
        return false;
      }
    } else {
      return false;
    }
  }

  toBase64(file) {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = (error) => reject(error);
    });
  }

  readURL(event: any): void {
    if (event.target.files && event.target.files[0]) {
      const file = event.target.files[0];

      const reader = new FileReader();
      reader.onload = (e) => (this.base64textString = reader.result);

      reader.readAsDataURL(file);
    }
  }

  back() {
    this.location.back();
  }
}
