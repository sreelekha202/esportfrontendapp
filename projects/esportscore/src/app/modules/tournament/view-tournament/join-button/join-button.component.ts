import {
  Component,
  OnInit,
  Input,
  OnChanges,
  Output,
  EventEmitter,
} from '@angular/core';
import { TournamentService } from '../../../../core/service';
import { ITournament } from '../../../../shared/models';
import { IUser, EsportsToastService } from 'esports';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AppHtmlTeamRegRoutes } from '../../../../app-routing.model';
import {
  InfoPopupComponent,
  InfoPopupComponentData,
  InfoPopupComponentType,
} from '../../../../shared/popups/info-popup/info-popup.component';
import { MatDialog } from '@angular/material/dialog';
import { CountdownFormatFn } from 'ngx-countdown';

@Component({
  selector: 'app-join-button',
  templateUrl: './join-button.component.html',
  styleUrls: [
    './join-button.component.scss',
    '../view-tournament.component.scss',
  ],
})
export class JoinButtonComponent implements OnInit, OnChanges {
  @Input() tournament: ITournament;
  @Input() user: IUser;

  @Output() isRefreshTournament = new EventEmitter<boolean>(false);
  @Output() isAuthorizedUser = new EventEmitter<any>();

  AppHtmlTeamRegRoutes = AppHtmlTeamRegRoutes;

  btnType: string = '';
  participantId: string | null;
  isProcessing: boolean = false;
  restrationStartDate: Date;
  remainingTime;
  remainingTimeForTournamentStart;
  remainigCheckInTime: number = 0;
  notifyConfig;
  showRegTime;
  showTournamentTime;
  registrationEndedTxt;
  isRegistrationClosed = true;

  CountdownTimeUnits: Array<[string, number]> = [
    ['Y', 1000 * 60 * 60 * 24 * 365], // years
    ['M', 1000 * 60 * 60 * 24 * 30], // months
    ['D', 1000 * 60 * 60 * 24], // days
    ['H', 1000 * 60 * 60], // hours
    ['m', 1000 * 60], // minutes
    ['s', 1000], // seconds
  ];

  constructor(
    private tournamentService: TournamentService,
    private translateService: TranslateService,
    private eSportsToastService: EsportsToastService,
    private router: Router,
    public dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.remainingTime = 0;
    this.remainingTimeForTournamentStart = 0;
    this.showRegTime = false;
    this.showTournamentTime = false;
  }

  ngOnChanges() {
    if (this.tournament) {
      this.fetchParticipantRegisterationStatus(this.tournament?._id);
    }
  }

  fetchParticipantRegisterationStatus = async (id) => {
    try {
      const {
        data,
      } = await this.tournamentService.fetchParticipantRegistrationStatus(id);
      this.btnType = data?.type;
      if (data?.isRegistrationClosed !== undefined) {
        this.isRegistrationClosed = data?.isRegistrationClosed;
      }
      this.registrationEndedTxt = data?.regEnded;
      this.participantId = data?.participantId;
      this.remainingTime = data?.remainingTimeForRegStart / 1000;
      this.remainingTimeForTournamentStart =
        data?.remainingTimeForTournamentStart / 1000 || 0;
      this.showRegTime = this.remainingTime > 0 ? true : false;
      this.showTournamentTime =
        this.remainingTimeForTournamentStart > 0 ? true : false;
      if (this.remainingTime < 0) {
        this.showRegTime = false;
      }
      this.remainigCheckInTime = data?.remainingCheckInTime / 1000 || 0;
      if (this.btnType == 'tournament-started' && this.participantId) {
        this.isAuthorizedUser.emit(true);
      }
    } catch (error) {
      this.eSportsToastService.showError(
        error?.error?.message || error?.message
      );
    }
  };

  onTimerFinished(e: Event) {
    if (e['action'] == 'done') {
      Promise.resolve(() => {
        this.showRegTime = false;
      });
      this.fetchParticipantRegisterationStatus(this.tournament?._id);
      // this.ngOnChanges();
    }
  }

  onTournamnetTimerFinished(e: Event) {
    if (e['action'] == 'done') {
      Promise.resolve(() => {
        this.showTournamentTime = false;
      });
      this.fetchParticipantRegisterationStatus(this.tournament?._id);
      // this.ngOnChanges();
    }
  }

  participantCheckIn = async () => {
    try {
      const participant = await this.tournamentService
        .updateParticipant(this.participantId, { checkedIn: true })
        .toPromise();
      this.eSportsToastService.showSuccess(participant.message);
      this.isRefreshTournament.emit(true);
    } catch (error) {
      this.eSportsToastService.showError(
        error?.error?.message || error?.message
      );
    }
  };

  joinParticipant() {
    if (this.tournament?.isPrize) {
      if (this.user && !this.user?.accountDetail?.isPaymentAccountVerified) {
        this.eSportsToastService.showInfo(
          this.translateService.instant(
            'VIEW_TOURNAMENT.JOIN_PARTICIPANT.ACCOUNT'
          )
        );
        return;
      }
    }
    this.router.navigate([`/tournament/${this.tournament?.slug}/join`]);
  }

  showUpdatePaymentPopup() {
    const afterShowPopup: InfoPopupComponentData = {
      title: this.translateService.instant(
        'VIEW_TOURNAMENT.MODAL.PAYMENT_METHOD.TITLE'
      ),
      text: this.translateService.instant(
        'VIEW_TOURNAMENT.MODAL.PAYMENT_METHOD.MESSAGE'
      ),
      type: InfoPopupComponentType.confirm,
      btnText: this.translateService.instant(
        'VIEW_TOURNAMENT.MODAL.PAYMENT_METHOD.BUTTON'
      ),
    };
    const dialogRef = this.dialog.open(InfoPopupComponent, {
      data: afterShowPopup,
    });
    dialogRef.afterClosed().subscribe((confirmed) => {
      if (confirmed) {
        this.router.navigate([`/profile/profile-setting`], {
          queryParams: { activeTab: '2' },
        });
      }
    });
  }

  shouldShowPaymentPopup() {
    if (this.tournament?.isPrize) {
      if (this.user && !this.user?.accountDetail?.isPaymentAccountVerified) {
        return true;
      }
    }
    return false;
  }

  saveParticipantDetails = async () => {
    try {
      if (this.shouldShowPaymentPopup()) {
        this.showUpdatePaymentPopup();

        return;
      }

      this.isProcessing = true;

      // populate user game id only for matching game

      const userGames = this.user?.preference?.gameDetails || [];

      const game = userGames.find((game) => {
        return game._id === this.tournament?.gameDetail?._id;
      });

      const payload = {
        name: this.user?.fullName,

        phoneNumber: this.user?.phoneisVerified ? this.user?.phoneNumber : '',

        email: this.user?.emailisVerified ? this.user?.email : '',

        logo: this.user?.profilePicture || '',

        inGamerUserId: game?.userGameId || '',

        teamName: game?.userGameId || '',

        tournamentId: this.tournament?._id,

        participantType: this.tournament?.participantType,

        teamMembers: [],

        substituteMembers: [],
      };

      const {
        isProcessing,

        message,
      } = await this.tournamentService.saveParticipantDetails(
        payload,

        this.tournament
      );

      if (!isProcessing) {
        this.eSportsToastService.showSuccess(message);
        this.isProcessing = false;
        this.tournamentService.tournamentActiveTab = 3;
        this.isRefreshTournament.emit(true);
      }
    } catch (error) {
      this.isProcessing = false;
      this.eSportsToastService.showError(
        error?.error?.message || error?.message
      );
    }
  };

  formatDate?: CountdownFormatFn = ({ date, formatStr }) => {
    let duration = Number(date || 0);
    return this.CountdownTimeUnits.reduce((current, [name, unit]) => {
      if (current.indexOf(name) !== -1) {
        const v = Math.floor(duration / unit);
        duration -= v * unit;
        return current.replace(new RegExp(`${name}+`, 'g'), (match: string) => {
          return v.toString().padStart(match.length, '0');
        });
      }
      return current;
    }, formatStr);
  };
}
