import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.scss', '../view-tournament.component.scss'],
})
export class InfoComponent implements OnInit {
  @Input() tournamentDetails;

  constructor() {}

  ngOnInit(): void {}
}
