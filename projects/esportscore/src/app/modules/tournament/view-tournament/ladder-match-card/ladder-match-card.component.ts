import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Location } from '@angular/common';
import { MatDialog } from '@angular/material/dialog';

import { LadderPopupComponent, LadderPopupComponentData } from '../../../../shared/popups/ladder-popup/ladder-popup.component';
import { TournamentService } from '../../../../../app/core/service/tournament.service';
import { ActivatedRoute, Router } from '@angular/router';
import { IUser, EsportsUserService, EsportsParticipantService, EsportsToastService } from 'esports';
import { environment } from "../../../../../environments/environment";
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';

const API = environment.apiEndPoint;
@Component({
  selector: 'app-ladder-match-card',
  templateUrl: './ladder-match-card.component.html',
  styleUrls: ['./ladder-match-card.component.scss'],
})
export class LadderMatchCardComponent implements OnInit, OnDestroy {
  @Input() isSingle: boolean = true;
  isImReady: boolean = false;

  challenge;
  gameInfo = null;
  playersData = [];
  teamsData = [];

  isLoaded = false;
  user: IUser;
  tournamentDetails;
  challengeId: string = '';
  matchDetail = null;
  isLoading = false;
  participantId: string | null;

  steps = [
    {
      isDone: false,
      text: 'LADDER.STEPS.FIRST',
      icon: 'assets/icons/ladder/FIRST.svg',
    },
    {
      isDone: false,
      text: 'LADDER.STEPS.SECOND',
      icon: 'assets/icons/ladder/SECOND.svg',
    },
    {
      isDone: false,
      text: 'LADDER.STEPS.THIRD',
      icon: 'assets/icons/ladder/THIRD.svg',
    },
    {
      isDone: false,
      text: 'LADDER.STEPS.FOURTH',
      icon: 'assets/icons/ladder/FOURTH.svg',
    },
  ];
  userSubscription: Subscription;
  showAcceptButton: boolean = false;
  showSuggestTimeButton: boolean = true;

  constructor(
    private location: Location,
    private fb: FormBuilder,
    public matDialog: MatDialog,
    public tournamentService: TournamentService,
    private userService: EsportsUserService,
    private activatedRoute: ActivatedRoute,
    private eSportsToastService: EsportsToastService,
    private translateService: TranslateService,
    private esportsParticipantService: EsportsParticipantService,
    private router: Router,
  ) { }


  ngOnInit(): void {
    this.getCurrentUserDetails();
    this.challengeId = this.activatedRoute.snapshot.queryParams.chid;
    this.getChallengeDetails();
  }

  getCurrentUserDetails() {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.user = data;
      }
    });
  }

  getChallengeDetails() {
    this.isLoading = true;
    this.esportsParticipantService.getChallengeDetails(API, this.challengeId).subscribe(res => {
      const data = res['data'];
      this.challenge = data.challenge;

      if (this.user && this.user._id == this.challenge.challengeFromUserId) {
        if (this.challenge.challengeFromStatus == 'pending') {
          this.showAcceptButton = true;
        }
      } else {
        if (this.challenge.challengeToStatus == 'pending') {
          this.showAcceptButton = true;
        }
      }

      if (this.challenge.challengeFromStatus == 'rejected' ||  this.challenge.challengeToStatus == 'rejected') {
        this.showSuggestTimeButton = false;
      }

      this.tournamentDetails = data.tournamentDetails;
      this.fetchParticipantRegisterationStatus(this.tournamentDetails?._id);

      this.playersData = [];
      this.playersData.push(this.challenge.challengeFrom);
      this.playersData.push(this.challenge.challengeTo);
      this.gameInfo = data.tournamentDetails.gameDetail;

      if (this.tournamentDetails.participantType == 'team') {
        this.teamsData = [];
        let team1Players = [{
          id: this.challenge.challengeFrom._id,
          name: this.challenge.challengeFrom.name,
          inGamerUserId: this.challenge.challengeFrom.inGamerUserId,
          image: this.challenge.challengeFrom.logo
        }]

        const mapImage = (challenge, challenger, arrayName) => {
          challenge[challenger][arrayName] = challenge[challenger][arrayName].map(ele => {
            ele.image = challenge[challenger].logo
            return ele;
          })
        }

        mapImage(this.challenge, 'challengeFrom', 'teamMembers');
        mapImage(this.challenge, 'challengeFrom', 'substituteMembers');
        mapImage(this.challenge, 'challengeTo', 'teamMembers');
        mapImage(this.challenge, 'challengeTo', 'substituteMembers');

        const team1 = {
          name: this.challenge.challengeFrom.teamName,
          players: [
            ...team1Players,
            ...this.challenge.challengeFrom.teamMembers,
            ...this.challenge.challengeFrom.substituteMembers
          ]
        };
        let team2Players = [{
          id: this.challenge.challengeTo._id,
          name: this.challenge.challengeTo.name,
          inGamerUserId: this.challenge.challengeTo.inGamerUserId,
          image: this.challenge.challengeTo.logo
        }]
        const team2 = {
          name: this.challenge.challengeTo.teamName,
          players: [
            ...team2Players,
            ...this.challenge.challengeTo.teamMembers,
            ...this.challenge.challengeTo.substituteMembers
          ]
        }
        this.teamsData = [team1, team2];
        this.isSingle = false;
      }

    }, err => {
    }, () => {
      this.isLoading = false;
    })
  }

  goBack() {
    this.location.back();
  }

  async suggestTime() {
    const data: LadderPopupComponentData = {
      isShowScheduler: true,
      isShowLoading: false,
      isShowNotFound: false,
    }
    const dialogRef = this.matDialog.open(LadderPopupComponent, { data });

    dialogRef.afterClosed().subscribe(res => {
      if (res) {
        const { startDate, startTime } = res;
        const newTime = new Date(startDate.toString());
        newTime.setHours(startTime.hour);
        newTime.setMinutes(startTime.minute);
        const payload = {
          challengeId: this.challengeId,
          newTime
        }

        this.isLoading = true;
        this.esportsParticipantService.suggestNewTime(API, payload).subscribe(response => {
          this.eSportsToastService.showSuccess(this.translateService.instant('LADDER.CHALLENGE_RESCHEDULED'));
        }, err => {
          this.eSportsToastService.showError(this.translateService.instant('LADDER.CHALLENGE_RESCHEDULED_ERROR'));
        }, () => {
          this.getChallengeDetails();
          this.isLoading = false;
        })
      }
    })
  }

  acceptMatch() {
    this.isImReady = true;
    const payload = {
      "tournamentId": this.tournamentDetails._id,
      "challengeId": this.challengeId,
      "accept": true
    };
    this.isLoading = true;
    this.esportsParticipantService.acceptOrRejectChallenge(API, payload).subscribe(res => {
      const successMessage = this.translateService.instant('LADDER.ACCEPT_SUCCESS');
      this.eSportsToastService.showSuccess(successMessage);
      this.router.navigate(['my-matches'], { queryParams: { activeTabIndex: 1 } });
    }, error => {
      this.eSportsToastService.showError(error?.error?.message || error?.message);
    }, () => {
      this.isLoading = false;
    })
  }

  reportScore() {
    const data = {
      isShowReportScoreCard: true,
      payload: {
        tournament: this.tournamentDetails,
        match: {
          _id: this.challenge.matchId
        },
        isLadderTournament: true,
        participantId: this.participantId
      }
    };
    this.matDialog.open(LadderPopupComponent, { data });
  }

  rejectMatch() {
    this.isImReady = true;
    const payload = {
      "tournamentId": this.tournamentDetails._id,
      "challengeId": this.challengeId,
      "accept": false
    };
    this.isLoading = true;
    this.esportsParticipantService.acceptOrRejectChallenge(API, payload).subscribe(res => {
      const successMessage = this.translateService.instant('LADDER.REJECT_SUCCESS');
      this.eSportsToastService.showSuccess(successMessage);
      this.router.navigate([`tournament/${this.tournamentDetails.slug}`]);
    }, error => {
      this.eSportsToastService.showError(error?.error?.message || error?.message);
    }, () => {
      this.isLoading = false;
    })
  }

  showCopySnackbar() {
    this.eSportsToastService.showInfo(this.translateService.instant('LADDER.GAME_ID_COPIED'));
  }

  ngOnDestroy() {
    this.userSubscription?.unsubscribe();
  }

  fetchParticipantRegisterationStatus = async (id) => {
    try {
      if (!id) return;
      const {
        data,
      } = await this.tournamentService.fetchParticipantRegistrationStatus(id);

      this.participantId = data?.participantId;
    } catch (error) {
      this.eSportsToastService.showError(error?.error?.message || error?.message);
    }
  }

}
