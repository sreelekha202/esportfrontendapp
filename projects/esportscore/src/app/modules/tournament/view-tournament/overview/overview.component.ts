import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.scss', '../view-tournament.component.scss'],
})
export class OverviewComponent implements OnInit {
  @Input() tournamentDetails;

  constructor() {}

  ngOnInit(): void {}
}
