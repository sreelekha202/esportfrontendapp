import { Component, OnInit, Input, Inject, PLATFORM_ID  } from '@angular/core';
import { UserReportsService, IPagination } from 'esports';
import { environment } from '../../../../../environments/environment';
import { isPlatformBrowser } from '@angular/common';
@Component({
  selector: 'app-user-report',
  templateUrl: './user-report.component.html',
  styleUrls: ['./user-report.component.scss'],
})
export class UserReportComponent implements OnInit {
  @Input() tournamentId;
  userReports = [];
  userReportDetail;
  isLoaded = false;
  screenshot = '../assets/images/articles/banner.png';
  paginationData = {
    page: 1,
    limit: 10,
  };
  page: IPagination;
  isBrowser: boolean;
  
  constructor(
    private UserReportsService: UserReportsService,
    @Inject(PLATFORM_ID) private platformId
  ) {
    this.isBrowser = isPlatformBrowser(platformId);
  }

  ngOnInit(): void {
    if (this.isBrowser) {
      this.getUserReport();
    }
  }

  pageChanged(page): void {
    this.paginationData.page = page;
    this.getUserReport();
  }

  getUserReport() {
    this.isLoaded = true;
    this.UserReportsService.getTournamentsUserReport(
      environment.apiEndPoint,
      this.tournamentId,
      this.paginationData?.limit,
      this.paginationData?.page
    ).subscribe(
      (res: any) => {
        this.userReports = res?.data?.docs;
        this.isLoaded = false;
        this.page = {
              totalItems: res?.data?.totalDocs,
              itemsPerPage: res?.data?.limit,
              maxSize: 5,
            };
      },
      (err) => {
        this.isLoaded = false;
      }
    );
  }

  getReportDetail(event) {
    if (event.type == 'click') {
      let id = event.row._id;
      this.isLoaded = true;
      this.UserReportsService.getUserReportById(
        environment.apiEndPoint,
        id
      ).subscribe(
        (res: any) => {
          this.userReportDetail = res.data;
          if (res?.data?.discussion?.length) {
            this.userReportDetail['adminUser'] = res.data.discussion.find(
              (item) => {
                if (item?.updatedBy?.accountType === 'admin') {
                  return item;
                }
              }
            );
          }
          this.isLoaded = false;
        },
        (err) => {
          this.isLoaded = false;
        }
      );
    }
  }

  backBtn() {
    this.userReportDetail = false;
  }
}
