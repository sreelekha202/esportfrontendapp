import {
  Component,
  OnInit,
  Input,
  OnChanges,
} from '@angular/core';
import { ParticipantService } from '../../../../core/service';
import { EsportsToastService, IPagination } from 'esports';
@Component({
  selector: 'app-participants',
  templateUrl: './participants.component.html',
  styleUrls: [
    './participants.component.scss',
    '../view-tournament.component.scss',
  ],
})
export class ParticipantsComponent implements OnInit, OnChanges {
  @Input() tournamentId: string;
  @Input() tournament;
  isProccessing: boolean = false;
  selectedTeam = null;
  pagination: IPagination = {
    activePage: 1,
    totalItems: 1,
    maxSize: 10,
    itemsPerPage: 100
  };
  participantData: any;

  constructor(
    private participantService: ParticipantService,
    private eSportsToastService: EsportsToastService
  ) {}

  ngOnInit(): void {}

  ngOnChanges() {
    if (this.tournament?._id) {
      this.fetchParticipantByStatus()
    }
  }

  fetchParticipantByStatus = async(page = 1) => {
   try {
    if (this.isProccessing) return;
    this.isProccessing = true;
    const { data } = await this.participantService.fetchParticipantByStatus(1, this.tournament._id, page);
    this.pagination.itemsPerPage = data?.limit;
    this.pagination.totalItems = data?.totalDocs;
    this.participantData = data;
    this.isProccessing = false;
   } catch(error) {
    this.isProccessing = false;
    this.eSportsToastService.showError(
      error?.error?.message || error?.message
    );
   }
  }

  currentPage(page: number) {
   this.fetchParticipantByStatus(page);
  }

  goBack() {
    this.selectedTeam = false;
  }
}
