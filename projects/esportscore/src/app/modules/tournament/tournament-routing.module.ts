import { ReportComponent } from './report/report.component';

import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { TournamentInfoComponent } from './tournament-info/tournament-info.component';
import { TournamentComponent } from './tournament.component';
import { AuthGuard } from '../../shared/guard/auth.guard';

const routes: Routes = [
  { path: '', component: TournamentComponent },
  {
    path: 'report/:matchId/:tournamentId',
    canActivate: [AuthGuard],
    component: ReportComponent,
  },
  {
    path: 'info',
    component: TournamentInfoComponent,
    data: {
      tags: [
        {
          name: 'title',
          content:
            'Join and create tournaments of different game titles on stcplay.gg. And choose all the details related to it.',
        },
      ],
      title: 'Create Tournament in 3 easy Steps',
    },
  },
  {
    path: 'create',
    canActivate: [AuthGuard],
    loadChildren: () =>
      import('../../modules/tournament/create/create.module').then(
        (m) => m.CreateModule
      ),
    data: {
      tags: [
        {
          name: 'title',
          content:
            'add your information and create a tournament | create your tournament and choose your game rules | add your final details to create your tournament | complete your info and create your tournament',
        },
      ],
      title:
        'Add your Basic Information and create Tournament | Choose your game rules | Add your Final Details | Complete your info',
    },
  },
  {
    path: 'quick-advance',
    canActivate: [AuthGuard],
    loadChildren: () =>
      import('../../modules/tournament/quick-advance/quick-advance.module').then(
        (m) => m.QuickAdvanceTournamentModule
      ),
    data: {
      isRootPage: true,
      tags: [
        {
          name: 'title',
          content:
            'add your information and create a tournament | create your tournament and choose your game rules | add your final details to create your tournament | complete your info and create your tournament',
        },
      ],
      title:
        'Add your Basic Information and create Tournament | Choose your game rules | Add your Final Details | Complete your info',
    },
  },
  {
    path: 'quick-advance/:id',
    canActivate: [AuthGuard],
    loadChildren: () =>
      import('../../modules/tournament/quick-advance/quick-advance.module').then(
        (m) => m.QuickAdvanceTournamentModule
      ),
    data: {
      isRootPage: true,
      tags: [
        {
          name: 'title',
          content:
            'add your information and create a tournament | create your tournament and choose your game rules | add your final details to create your tournament | complete your info and create your tournament',
        },
      ],
      title:
        'Add your Basic Information and create Tournament | Choose your game rules | Add your Final Details | Complete your info',
    },
  },
  {
    path: 'edit/:id',
    canActivate: [AuthGuard],
    loadChildren: () =>
      import('../../modules/tournament/create/create.module').then(
        (m) => m.CreateModule
      ),
  },
  {
    path: 'manage/:id',
    canActivate: [AuthGuard],
    loadChildren: () =>
      import(
        '../../modules/tournament/tournament-management/tournament-management.module'
      ).then((m) => m.TournamentManagementModule),
  },
  {
    path: ':id',
    loadChildren: () =>
      import(
        '../../modules/tournament/view-tournament/view-tournament.module'
      ).then((m) => m.ViewTournamentModule),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TournamentRoutingModule {}
