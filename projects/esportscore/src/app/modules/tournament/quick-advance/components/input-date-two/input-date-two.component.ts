import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-input-date-two',
  templateUrl: './input-date-two.component.html',
  styleUrls: ['./input-date-two.component.scss'],
})
export class InputDateTwoComponent implements OnInit {
  @Input() customFormGroup: FormGroup;
  @Input() customFormControlName: string;
  @Input() title: string = '';
  @Input() placeholder: string = '';
  @Input() value: string | number = '';
  @Input() isBasic: boolean = false;
  @Input() isDisabled: boolean = false;
  @Input() minDate;
  constructor() {}

  ngOnInit(): void {}
}
