import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
} from '@angular/core';
import { EsportsTournamentService } from 'esports';

@Component({
  selector: 'app-page-switcher',
  templateUrl: './page-switcher.component.html',
  styleUrls: ['./page-switcher.component.scss'],
})
export class PageSwitcherComponent implements OnInit {
  @Input() step: number = 0;
  @Output() onStepChange = new EventEmitter<number>();

  totalPages: number = 0;
  activePage: number = 0;

  constructor(
    private eSportsTournamentService: EsportsTournamentService,
  ) {}

  ngOnInit(): void {
    this.eSportsTournamentService.totalPages.subscribe(totalPages => {
      this.totalPages = totalPages;
    });

    this.eSportsTournamentService.activePage.subscribe(activePage => {
      this.activePage = activePage;
    });
  }

  onStepChangeEvent(step) {
    this.onStepChange.emit(step);
  }
}
