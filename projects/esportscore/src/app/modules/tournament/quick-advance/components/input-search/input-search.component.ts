import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-input-search',
  templateUrl: './input-search.component.html',
  styleUrls: ['./input-search.component.scss'],
})
export class InputSearchComponent implements OnInit {
  @Input() placeholder: string = '';
  @Output() emitValue = new EventEmitter();

  timeoutId = null;

  constructor() {}

  ngOnInit(): void {}

  onSearch(value) {
    const emitValue = () => {
      // if (!value) return;
      this.emitValue.emit(value?.trim() || '');
    };
    clearTimeout(this.timeoutId);
    this.timeoutId = setTimeout(emitValue, 500);
  }
}
