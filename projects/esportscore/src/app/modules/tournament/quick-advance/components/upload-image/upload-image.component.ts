import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { S3UploadService } from '../../../../../core/service';
import { EsportsToastService } from 'esports';

@Component({
  selector: 'app-upload-image',
  templateUrl: './upload-image.component.html',
  styleUrls: ['./upload-image.component.scss'],
})
export class UploadImageComponent implements OnInit {
  @Input() customFormControlName: string;
  @Input() customFormGroup: FormGroup;

  @Input() title: string;
  @Input() subtitle: string;

  @Input() dimension;
  @Input() size;
  @Input() type;

  @Input() id;
  @Input() required: boolean = false;

  isProcessing: boolean = false;

  constructor(
    private s3UploadService: S3UploadService,
    private eSportsToastService: EsportsToastService
  ) {}

  ngOnInit(): void {}

  eventChange = async (event) => {
    try {
      this.isProcessing = true;

      const upload: any = await this.s3UploadService.validateAndUploadImage(
        event,
        this.dimension,
        this.size,
        this.type
      );

      this.customFormGroup
        .get(this.customFormControlName)
        .setValue(upload?.data?.Location);
      this.eSportsToastService.showSuccess(upload?.message);
      this.isProcessing = false;
    } catch (error) {
      this.isProcessing = false;
      const errorMessage = error?.error?.message || error?.message;
      this.eSportsToastService.showError(errorMessage);
    }
  };
}
