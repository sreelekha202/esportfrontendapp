import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ControlContainer, FormGroupDirective } from '@angular/forms';

@Component({
  selector: 'app-tournament-type',
  templateUrl: './tournament-type.component.html',
  styleUrls: ['./tournament-type.component.scss'],
  viewProviders: [
    { provide: ControlContainer, useExisting: FormGroupDirective },
  ],
})
export class TournamentTypeComponent implements OnInit {
  @Input() customFormControlName: string;

  types = [
    {
      type: 'online',
      description:
        'Your Tournament will be held on only as online event - online',
    },
    {
      type: 'offline',
      description:
        'Your Tournament will be held on only as online event - offline',
    },
    {
      type: 'hybrid',
      description:
        'Your Tournament will be held on only as online event - hybrid',
    },
  ];

  constructor() {}

  ngOnInit(): void {}
}
