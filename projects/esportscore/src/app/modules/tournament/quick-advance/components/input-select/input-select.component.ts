import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-input-select',
  templateUrl: './input-select.component.html',
  styleUrls: ['./input-select.component.scss'],
})
export class InputSelectComponent implements OnInit {
  @Input() customFormGroup: FormGroup;
  @Input() customFormControlName: string;
  @Input() title: string = 'title';
  @Input() placeholder: string = 'Placeholder';
  @Input() data: any[];
  @Input() readOnly: boolean = false;
  @Output() submit = new EventEmitter();

  constructor() {}

  ngOnInit(): void {}

  onSelect(value: string): void {
    this.submit.emit(value);
  }
}
