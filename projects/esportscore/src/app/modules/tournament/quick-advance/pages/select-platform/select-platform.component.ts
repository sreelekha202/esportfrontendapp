import { Component, OnInit } from '@angular/core';
import {
  ControlContainer,
  FormGroup,
  FormGroupDirective,
} from '@angular/forms';
import { EsportsTournamentService } from 'esports';

@Component({
  selector: 'app-select-platform',
  templateUrl: './select-platform.component.html',
  styleUrls: ['./select-platform.component.scss'],
  viewProviders: [
    { provide: ControlContainer, useExisting: FormGroupDirective },
  ],
})
export class SelectPlatformComponent implements OnInit {
  Step3: FormGroup | null;
  constructor(
    public eSportsTournamentService: EsportsTournamentService,
    private tournament: FormGroupDirective
  ) {}

  ngOnInit(): void {
    this.Step3 = this.tournament.form;
  }

  onSelectPlatform(id: string) {
    this.Step3?.get('platform')?.setValue(id);
  }

  onStepChange(step: number) {
    if (this.Step3?.get('platform')?.invalid && this.Step3?.get('step')?.value < step) return;
    this.Step3?.get('step')?.setValue(step);
  }
}
