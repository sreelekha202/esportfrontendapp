import { Component, Input, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { EsportsToastService, EsportsUserService, ITournament } from 'esports';
import {
  TournamentService,
  TransactionService,
  AuthServices,
} from '../../../../../core/service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss'],
})
export class PaymentComponent implements OnInit {
  transaction;
  @Input() tournament:ITournament;
  totalAmounts = [];
  prizePools = [];
  paymentMethods = [];
  paymentMethodSelected = false;
  isProccesing = true;

  constructor(private location: Location, private transactionService : TransactionService,
    private eSportsToastService : EsportsToastService,
    private tournamentService : TournamentService,
    private router: Router) {}

  ngOnInit(): void {
    this.fetchtransaction();
  }

  goBack() {
    this.location.back();
  }

  addEndingToPlace(place: number) {
    switch (place) {
      case 1:
        return `${place}st`;
      case 2:
        return `${place}nd`;
      case 3:
        return `${place}rd`;
      default:
        return `${place}th`;
    }
  }

  selectPayementMethod(value){
    this.paymentMethodSelected = value;
  }

  getPaymentResponse = async (order) => {
    try {
      this.isProccesing = true;
      const payload = {
        tournamentId: this.tournament?._id,
        transactionId: this.transaction?._id,
        txnId: order?.id,
        provider: 'paypal',
      };
      await this.tournamentService
        .updateTournament({ paymentType: 'paypal' }, this.tournament._id)
        .toPromise();
      const response = await this.transactionService.createOrUpdateTransaction(
        payload
      );
      this.eSportsToastService.showSuccess(response?.message);
      this.router.navigate(['/profile/my-tournament/created']);
      this.isProccesing = false;
    } catch (error) {
      this.eSportsToastService.showError(error?.error?.message || error?.message);
      this.isProccesing = false;
    }
  };

  fetchtransaction = async () => {
    try {
      // this.isPaymentLoaded = true;
      const payload = {
        tournamentId: this.tournament?._id,
        type: 'organizer',
      };
      const { data } = await this.transactionService.createOrUpdateTransaction(
        payload
      );
      this.totalAmounts.push({
        prizeLabel: 'PROFILE.TOURNAMENTS.PAYMENT.PRIZE_AMOUNT',
        amount: data.totalAmount - data.platformFee,
      });
      this.totalAmounts.push({
        prizeLabel: 'PROFILE.TOURNAMENTS.PAYMENT.TOTAL_AMOUNT',
        amount: data.totalAmount,
      });
      
      this.prizePools = data?.prizeList?.map((item) => {
        let img;
        if (item.name == '1st') {
          img = '../assets/images/payment/Gold.svg';
        } else if (item.name == '2nd') {
          img = '../assets/images/payment/Silver.svg';
        } else if (item.name == '3rd') {
          img = '../assets/images/payment/Bronze.svg';
        } else {
          img = '../assets/images/payment/Bronze.svg';
        }
        return {
          ...item,
          img,
        };
      });

      this.transaction = data;
      if (this.transaction.provider == 'paypal') {
        this.paymentMethods = [
          {
            img: 'assets/images/payment/paypal.png',
            name: 'Paypal',
            value: 'paypal',
          },
        ];
      }
      this.isProccesing = false;
    } catch (error) {
      this.eSportsToastService.showError(error?.error?.message || error?.message);
      this.isProccesing = false;
    }
  };
}
