import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import {
  AbstractControl,
  ControlContainer,
  FormControl,
  FormGroupDirective,
  ValidatorFn,
  Validators,
  FormBuilder,
  FormArray,
  FormGroup,
} from '@angular/forms';

import { ConfirmComponent } from '../../popups/confirm/confirm.component';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { EsportsToastService, EsportsTournamentService, EsportsConstantsService } from 'esports';

@Component({
  selector: 'app-tournament-details',
  templateUrl: './tournament-details.component.html',
  styleUrls: [
    '../send-invite/send-invite.component.scss',
    './tournament-details.component.scss',
  ],
  viewProviders: [
    { provide: ControlContainer, useExisting: FormGroupDirective },
  ],
})
export class TournamentDetailsComponent implements OnInit {
  Step7;
  structure;
  isBracketLoaded: boolean = false;
  userList = [];
  gameList = [];
  tournamentDetails;
  platformList = [];
  isProcessing: boolean = false;
  isFormLoaded: boolean = false;
  minDate: Date | null;
  currentDate = new Date();
  matchFormat = [
    { _id: 1, name: 'TOURNAMENT.BEST_OF_1' },
    { _id: 3, name: 'TOURNAMENT.BEST_OF_3' },
    { _id: 5, name: 'TOURNAMENT.BEST_OF_5' },
    { _id: 7, name: 'TOURNAMENT.BEST_OF_7' },
  ];
  editorConfig = {};
  @Output() tournamentEvent = new EventEmitter();
  constructor(
    private tournament: FormGroupDirective,
    private fb: FormBuilder,
    private eSportsToastService: EsportsToastService,
    private eSportsTournamentService: EsportsTournamentService,
    private matDialog: MatDialog,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.Step7 = this.tournament.form;
    this.gameList = this.eSportsTournamentService.getGameList;
    this.platformList = this.eSportsTournamentService.getPlatformList;
    this.fetchBracketMockStructure();
    this.createTournamentForm();
    this.setMinimumDate();
    this.editorConfig = {
      editable: true,
      spellcheck: true,
      height: '180px',
      width: 'auto',
      minWidth: '0',
      translate: 'yes',
      enableToolbar: true,
      showToolbar: true,
      defaultParagraphSeparator: '',
      defaultFontName: '',
      defaultFontSize: '',
      fonts: [
        { class: 'arial', name: 'Arial' },
        { class: 'times-new-roman', name: 'Times New Roman' },
        { class: 'calibri', name: 'Calibri' },
        { class: 'comic-sans-ms', name: 'Comic Sans MS' },
      ],
      customClasses: [
        {
          name: 'quote',
          class: 'quote',
        },
        {
          name: 'redText',
          class: 'redText',
        },
        {
          name: 'titleText',
          class: 'titleText',
          tag: 'h1',
        },
      ],
      sanitize: true,
      toolbarPosition: 'top',
      toolbarHiddenButtons: [['bold', 'italic'], ['fontSize']],
    };
  }

  setMinimumDate() {
    const date = new Date();
    date.setMinutes(date.getMinutes() + 5);
    date.setSeconds(0);
    this.minDate = date;
  }

  fetchBracketMockStructure = async () => {
    try {
      this.isBracketLoaded = false;
      const { value } = this.Step7;
      const payload = {
        bracketType: value?.bracketType,
        ...(value?.bracketType != 'swiss_safeis' && {
          maximumParticipants: value?.maxParticipants,
          noOfSet: value?.noOfSet,
        }),
        ...(['round_robin', 'battle_royale'].includes(value?.bracketType) && {
          noOfTeamInGroup: value?.noOfTeamInGroup,
          noOfWinningTeamInGroup: value?.noOfWinningTeamInGroup,
          noOfRoundPerGroup: value?.noOfRoundPerGroup,
          stageBracketType: value?.stageBracketType,
        }),
      };
      const response = await this.eSportsTournamentService.generateBracket(payload);
      this.structure = response.data;
      this.isBracketLoaded = true;
    } catch (error) {
      this.isBracketLoaded = true;
      const errorMessage = error?.error?.message || error?.message;
      this.eSportsToastService.showError(errorMessage);
    }
  };

  // custom field controller
  addNewControl = (field, value, customValidation?) => {
    try {
      const formControl = this.Step7.get(field);
      if (!formControl) {
        const control = customValidation
          ? new FormControl(value, customValidation)
          : new FormControl(value);

        this.Step7.addControl(field, control);
      }
    } catch (error) {
    }
  };

  prizeClickHandler(checked, prizeLen = 1) {
    if (checked) {
      const formControl = this.Step7.get('prizeList');
      if (formControl) return;

      this.Step7.addControl(
        'prizeList',
        this.fb.array([], this.customPrizeValidator())
      );
      for (let i = 0; i < prizeLen; i++) this.addPrize(i);
      this.Step7.addControl(
        'prizeCurrency',
        new FormControl('KWD', Validators.compose([Validators.required]))
      );
    } else {
      this.Step7.removeControl('prizeList');
      this.Step7.removeControl('prizeCurrency');
    }
  }

  sponsorClickHandler(checked, len = 1) {
    if (checked) {
      const formControl = this.Step7.get('sponsors');
      if (formControl) return;
      this.Step7.addControl('sponsors', this.fb.array([]));
      this.addSponsor(len);
    } else {
      this.Step7.removeControl('sponsors');
    }
  }

  createSponsor(): FormGroup {
    return this.fb.group({
      sponsorName: [
        '',
        Validators.compose([]),
      ],
      website: [
        '',
        Validators.compose([
          Validators.pattern(EsportsConstantsService?.webUrlRegex),
        ]),
      ],
      playStoreUrl: [
        '',
        Validators.compose([
          Validators.pattern(EsportsConstantsService?.playStoreUrlRegex),
        ]),
      ],
      appStoreUrl: [
        '',
        Validators.compose([
          Validators.pattern(EsportsConstantsService?.appStoreUrlRegex),
        ]),
      ],
      sponsorLogo: ['', Validators.compose([])],
      sponsorBanner: ['', Validators.compose([])],
    });
  }

  addSponsor(len = 1): void {
    const sponsor = this.Step7.get('sponsors') as FormArray;
    for (let i = 0; i < len; i++) {
      sponsor.push(this.createSponsor());
    }
  }

  paidRegistrationHandler(check) {
    if (check) {
      const regCurrencyValidators = Validators.compose([Validators.required]);
      const regFeeValidators = Validators.compose([
        Validators.required,
        Validators.min(1),
      ]);
      this.addNewControl('regFeeCurrency', 'SAR', regCurrencyValidators);
      this.addNewControl('regFee', 1, regFeeValidators);
    } else {
      this.Step7.removeControl('regFeeCurrency');
      this.Step7.removeControl('regFee');
    }
  }

  createTournamentForm() {
    this.isFormLoaded = false;
    // for advance
    if (this.Step7.value?.type == 'advance') {
      const regStartDtValidation = Validators.compose([
        Validators.required,
        this.ValidateRegStartDate.bind(this),
      ]);

      const regEndDtValidation = Validators.compose([
        Validators.required,
        this.ValidateRegEndDate.bind(this),
      ]);

      const tournamentEndDtValidation = Validators.compose([
        Validators.required,
        this.ValidateEndDate.bind(this),
      ]);

      const tournamentTypeValidators = Validators.compose([
        Validators.required,
      ]);

      const youtubeVideoLinkValidators = Validators.compose([
        Validators.pattern(EsportsConstantsService.youtubeRegex),
      ]);

      const facebookVideoLinkValidators = Validators.compose([
        Validators.pattern(EsportsConstantsService.facebookRegex),
      ]);

      const twitchVideoLinkValidators = Validators.compose([
        Validators.pattern(EsportsConstantsService.twitchRegex),
      ]);

      this.addNewControl('regStartDate', '', regStartDtValidation);
      this.addNewControl('regEndDate', '', regEndDtValidation);
      this.addNewControl('endDate', '', tournamentEndDtValidation);
      this.addNewControl('tournamentType', '', tournamentTypeValidators);

      this.addNewControl('youtubeVideoLink', '', youtubeVideoLinkValidators);
      this.addNewControl('facebookVideoLink', '', facebookVideoLinkValidators);
      this.addNewControl('twitchVideoLink', '', twitchVideoLinkValidators);
      this.paidRegistrationHandler(this.Step7?.value?.isPaid);
      this.sponsorClickHandler(true, 1);
      this.prizeClickHandler(this.Step7.value?.isPrize, 1);
    }
    
    this.isFormLoaded = true;
  }

  onStepChange(step) {
    this.Step7.get('step').setValue(step);
  }

  popUpTitleAndText = async (data) => {
    if (data?._id) {
      return {
        title: 'TOURNAMENT.UPDATE',
        text: 'TOURNAMENT.UPDATE_TXT',
      };
    } else {
      return {
        title: 'TOURNAMENT.SAVE_TOURNAMENT',
        text: 'TOURNAMENT.STATUS_2',
      };
    }
  };

  onGameChange = async (value:any) => {
    const games = this.eSportsTournamentService.getGameList;
    const selectedGame = games.find(elem => {
      return elem?._id == value;
    });
    this.eSportsTournamentService.setPlatformList = selectedGame?.platform;
    this.platformList = this.eSportsTournamentService.getPlatformList;
  }
  
  Submit = async () => {
    try {
      const { value,invalid } = this.Step7;
      const invalidKeys = Object.keys(this.Step7.controls).filter(element => {
        return this.Step7.controls[element].status != 'VALID';
      });
      if(invalid) {
        this.eSportsToastService.showError('please fill required details')
        return;
      }
      const data = await this.popUpTitleAndText(value);
      const confirmed = await this.matDialog
        .open(ConfirmComponent, { data })
        .afterClosed()
        .toPromise();

      if (!confirmed) return;

      this.isProcessing = true;
      const response = value?._id
        ? await this.eSportsTournamentService.updateTournament(value, value?._id)
        : await this.eSportsTournamentService.saveTournament(value);

      this.eSportsToastService.showSuccess(response?.message);
      if (response?.data?.enablePayment) {
        this.tournamentDetails = response?.data;
        this.tournamentEvent.emit(this.tournamentDetails);
        this.Step7.get('step').setValue(11);
      } else {
        this.router.navigate(['/profile/my-tournament/created']);
      }

      this.isProcessing = false;
    } catch (error) {
      this.isProcessing = false;
      const errorMessage = error?.error?.message || error?.message;
      this.eSportsToastService.showError(errorMessage);
    }
  };

  addPrize(i): void {
    const prizeList = this.Step7.get('prizeList') as FormArray;
    const { maxParticipants } = this.Step7.value;
    let name = '';
    if (prizeList.controls.length < maxParticipants) {
      switch (true) {
        case prizeList.controls.length == 0:
          name = '1st';
          break;
        case prizeList.controls.length == 1:
          name = '2nd';
          break;
        case prizeList.controls.length == 2:
          name = '3rd';
          break;
        default:
          name = `${prizeList.controls.length + 1}th`;
          break;
      }
      const createPrizeForm = (): FormGroup => {
        const validator = [Validators.required, Validators.pattern('^[0-9]*$')];
        if (i == 0) {
          validator.push(Validators.min(1));
        }
        return this.fb.group({
          name: [name],
          value: ['', Validators.compose(validator)],
        });
      };
      prizeList.push(createPrizeForm());
    } else {
    }
  }

  // Custom validations

  ValidateRegStartDate(
    control: AbstractControl
  ): { [key: string]: any } | null {
    return !control.value
      ? null
      : this.validateRegStartDate(control['_parent'], 'pastRegStartDate');
  }

  ValidateRegEndDate(control: AbstractControl): { [key: string]: any } | null {
    return !control.value
      ? null
      : this.validateRegEndDate(control['_parent'], 'pastRegEndDate');
  }

  ValidateEndDate(control: AbstractControl): { [key: string]: any } | null {
    return !control.value
      ? null
      : this.validateBothEndDate(control['_parent'], 'pastEndDate');
  }

  validateRegStartDate(
    formGroup: FormGroup,
    ekey: string
  ): { [key: string]: boolean } {
    const { regStartDate, startDate } = formGroup.controls;

    if (ekey == 'pastRegStartDate' && !regStartDate?.value) {
      return { required: true };
    } else if (startDate?.value && regStartDate?.value) {
      const currentDate = new Date();
      const startDt = new Date(startDate.value);
      const regstartDt = new Date(regStartDate.value);

      if (startDt > regstartDt && regstartDt > currentDate) {
        return null;
      } else {
        return {
          [ekey]: true,
        };
      }
    }
    return null;
  }

  validateRegEndDate(
    formGroup: FormGroup,
    ekey: string
  ): { [key: string]: boolean } {
    const { regStartDate, regEndDate, startDate } = formGroup.controls;
    if (ekey == 'pastRegEndDate' && !regEndDate?.value) {
      return { required: true };
    } else if (regEndDate?.value && regStartDate?.value && startDate?.value) {
      const regEndDt = new Date(regEndDate.value);
      const regstartDt = new Date(regStartDate.value);
      let tournamentStartDt, tournamentStartDate;
      tournamentStartDt = new Date(startDate.value);

      if (regEndDt > regstartDt && regEndDt <= tournamentStartDt) {
        return null;
      } else {
        return {
          [ekey]: true,
        };
      }
    }
    return null;
  }

  validateBothEndDate(
    formGroup: FormGroup,
    ekey: string
  ): { [key: string]: boolean } {
    const { startDate, startTime, endDate, endTime } = formGroup.controls;
    if (ekey == 'pastEndDate' && !endDate?.value) {
      return { required: true };
    } else if (startDate?.value && endDate?.value) {
      const startDt = new Date(startDate.value);
      const endDt = new Date(endDate.value);
      if (endDt > startDt) {
        return null;
      } else {
        return {
          [ekey]: true,
        };
      }
    }
    return null;
  }

  customPrizeValidator(): ValidatorFn {
    return (formArray: FormArray): { [key: string]: any } | null => {
      let total = 0;
      for (let item of formArray.controls) {
        total = total + item.value.value;
      }
      const maxParticipants =
        formArray['_parent']?.controls?.maxParticipants?.value;
      const prizeLimit = maxParticipants;
      if (formArray.controls.length > prizeLimit) {
        return { prizeLimit: true };
      }
      const isPrizeValid = formArray.controls.length ? total > 0 : true;
      return isPrizeValid ? null : { prizeMoneyRequired: true };
    };
  }

  emptyCheck = (control: FormControl) => {
    return control.value && control.value.trim() ? null : { required: true };
  };

  onEdit() {
    this.Step7.get('step').setValue(1);
  }
}
