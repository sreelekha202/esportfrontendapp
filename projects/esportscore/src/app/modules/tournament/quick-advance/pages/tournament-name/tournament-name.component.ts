import { Component, Input, OnInit } from '@angular/core';
import {
  ControlContainer,
  FormGroup,
  FormGroupDirective,
} from '@angular/forms';
@Component({
  selector: 'app-tournament-name',
  templateUrl: './tournament-name.component.html',
  styleUrls: ['./tournament-name.component.scss'],
  viewProviders: [
    { provide: ControlContainer, useExisting: FormGroupDirective },
  ],
})

export class TournamentNameComponent implements OnInit {
  Step1: FormGroup | null;

  constructor(private tournament: FormGroupDirective) {}

  ngOnInit(): void {
    this.Step1 = this.tournament.form;
  }

  onEnter(event: any): void {
    if (this.Step1.get('name').valid) {
      this.Step1.get('step').setValue(2);
    }
  }

  onStepChange(step: number): void {
    if (this.Step1.get('name').invalid) return;
    this.Step1.get('step').setValue(step);
  }
}
