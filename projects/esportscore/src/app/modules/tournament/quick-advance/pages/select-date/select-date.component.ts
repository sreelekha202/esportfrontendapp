import { Component, Input, OnChanges, OnInit } from '@angular/core';
import {
  ControlContainer,
  FormGroup,
  FormGroupDirective,
} from '@angular/forms';
@Component({
  selector: 'app-select-date',
  templateUrl: './select-date.component.html',
  styleUrls: ['./select-date.component.scss'],
  viewProviders: [
    { provide: ControlContainer, useExisting: FormGroupDirective },
  ],
})
export class SelectDateComponent implements OnInit {
  Step5: FormGroup | null;
  minDate: Date | null;
  constructor(private tournament: FormGroupDirective) {}

  ngOnInit(): void {
    this.Step5 = this.tournament?.form;
    this.setMinimumDate();
  }

  setMinimumDate() {
    const date = new Date();
    date.setMinutes(date.getMinutes() + 5);
    date.setSeconds(0);
    this.minDate = date;
  }

  onEnter(event: any): void {
    if (this.Step5?.get('startDate')?.invalid) return;
    this.Step5.get('step').setValue(this.Step5?.get('step')?.value + 1);
  }

  onStepChange(step: number): void {
    if (this.Step5?.get('startDate')?.invalid && this.Step5?.get('step')?.value < step) return;
    this.Step5?.get('step')?.setValue(step);
  }
}
