import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import {
  ControlContainer,
  FormControl,
  FormGroupDirective,
  Validators,
} from '@angular/forms';
import { EsportsTournamentService } from 'esports';
@Component({
  selector: 'app-select-type',
  templateUrl: './select-type.component.html',
  styleUrls: ['./select-type.component.scss'],
  viewProviders: [
    { provide: ControlContainer, useExisting: FormGroupDirective },
  ],
})
export class SelectTypeComponent implements OnInit {
  @Output() emitTournamentType = new EventEmitter();
  Step0;

  constructor(
    private tournament: FormGroupDirective,
    private eSportsTournamentService: EsportsTournamentService
  ) {}

  ngOnInit(): void {
    this.Step0 = this.tournament.form;
    this.Step0.addControl('type', new FormControl('', Validators.required));
  }

  onSelectTournament(type: string): void {
    this.emitTournamentType.emit(type);
    this.Step0.get('step').setValue(1);
  }
}
