import { Component, OnInit } from '@angular/core';
import { EsportsTournamentService } from 'esports';
import {
  ControlContainer,
  FormControl,
  FormGroup,
  FormGroupDirective,
} from '@angular/forms';

@Component({
  selector: 'app-select-game',
  templateUrl: './select-game.component.html',
  styleUrls: ['./select-game.component.scss'],
  viewProviders: [
    { provide: ControlContainer, useExisting: FormGroupDirective },
  ],
})
export class SelectGameComponent implements OnInit {
  Step2: FormGroup | null;
  constructor(
    public eSportsTournamentService: EsportsTournamentService,
    private tournament: FormGroupDirective
  ) {}

  ngOnInit(): void {
    this.Step2 = this.tournament.form;
  }

  onSelectGame(game: any) {
    this.eSportsTournamentService.setPlatformList = game?.platform;
    if (game?.isQuickFormatAllow && this.Step2?.value?.type == 'quick') {
      this.Step2.addControl('noOfTeamInGroup', new FormControl(''));
      this.Step2.addControl('noOfWinningTeamInGroup', new FormControl(''));
      this.Step2.addControl('noOfRoundPerGroup', new FormControl(''));
      this.Step2.addControl('stageBracketType', new FormControl(''));
      this.Step2.patchValue({
        ...game.quickFormatConfig
      })
    } else {
      this.eSportsTournamentService.setBracketTypes = game?.bracketTypes;
    }

    this.Step2.get('gameDetail').setValue(game?._id);
  }

  onStepChange(step: number): void {
    if (this.Step2?.get('gameDetail')?.invalid && this.Step2?.get('step')?.value < step) return;
    this.Step2?.get('step')?.setValue(step);
  }
}
