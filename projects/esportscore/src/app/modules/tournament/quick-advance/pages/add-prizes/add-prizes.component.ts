import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, FormGroupDirective, Validators } from '@angular/forms';

@Component({
  selector: 'app-add-prizes',
  templateUrl: './add-prizes.component.html',
  styleUrls: ['./add-prizes.component.scss'],
})
export class AddPrizesComponent implements OnInit {
  Step9;

  constructor(private fb: FormBuilder,private tournament: FormGroupDirective) {}

  ngOnInit(): void {
    this.Step9 = this.tournament.form;
  }

  addPrize(i): void {
    const prizeList = this.Step9.get('prizeList') as FormArray;
    const { maxParticipants } = this.Step9.value;
    let name = '';
    if (prizeList?.controls?.length < maxParticipants) {
      switch (true) {
        case prizeList?.controls?.length == 0:
          name = '1st';
          break;
        case prizeList?.controls?.length == 1:
          name = '2nd';
          break;
        case prizeList?.controls?.length == 2:
          name = '3rd';
          break;
        default:
          name = `${prizeList?.controls?.length + 1}th`;
          break;
      }
      const createPrizeForm = (): FormGroup => {
        const validator = [Validators.required, Validators.pattern('^[0-9]*$')];
        if (i == 0) {
          validator.push(Validators.min(1));
        }
        return this.fb.group({
          name: [name],
          value: ['', Validators.compose(validator)],
        });
      };
      prizeList.push(createPrizeForm());
    } else {
    }
  }
  
  onEnter(event: any): void {
    if (this.Step9.get('prizeList').valid) {
      this.Step9.get('step').setValue(this.Step9.get('step').value+1);
    }
  }

  onStepChange(step: number): void {
    if (this.Step9.get('prizeList').invalid) return;
    this.Step9.get('step').setValue(step);
  }
}
