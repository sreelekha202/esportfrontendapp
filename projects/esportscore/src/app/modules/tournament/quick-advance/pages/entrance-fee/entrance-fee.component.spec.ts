import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EntranceFeeComponent } from './entrance-fee.component';

describe('EntranceFeeComponent', () => {
  let component: EntranceFeeComponent;
  let fixture: ComponentFixture<EntranceFeeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EntranceFeeComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EntranceFeeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
