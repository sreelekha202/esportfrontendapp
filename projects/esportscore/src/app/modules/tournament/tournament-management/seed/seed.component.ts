import {
  Component,
  OnInit,
  Input,
  SimpleChanges,
  Output,
  EventEmitter,
  OnChanges,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { BracketService, TournamentService } from '../../../../core/service';
import {
  EsportsLanguageService,
  EsportsToastService,
  IPagination,
} from 'esports';
import { ITournament } from '../../../../shared/models';
import { InfoPopupComponent, InfoPopupComponentData, InfoPopupComponentType } from 'projects/esportscore/src/app/shared/popups/info-popup/info-popup.component';
import { MatDialog } from "@angular/material/dialog";

@Component({
  selector: 'app-seed',
  templateUrl: './seed.component.html',
  styleUrls: ['./seed.component.scss'],
})
export class SeedComponent implements OnInit, OnChanges {
  @ViewChild('generateBracketConfirm') bracketConfirmRef: TemplateRef<any>;
  @Input() tournament: ITournament;
  @Output() message = new EventEmitter<any>();

  arebicText = false;
  isSeeded = false;
  isDisableCreateBracket: boolean = false;
  allApprovedParticipants: Array<any> = [];
  allSelectedParticipants: Array<any> = [];
  participantList: Array<{}> = [];
  structure;
  activePage = 1;
  pageSize = 10;
  seedingMethodList = [
    { name: 'SEED.BY_REGISTRATION', value: 'regOrder' },
    { name: 'SEED.RANDOM', value: 'random' },
  ];
  filterList = [
    { name: 'SEED.ALL_PLAYERS', value: 'all-players' },
    // { name: 'Not Previously Seeded', value: 'not-previously-seeded' },
  ];
  itemPerPageList = [5, 10, 15, 20, 25];
  selectFilterType = null;
  selectedSeedingMethod = null;
  isChangeInSeedingOrder = false;
  apiLoaded: Array<boolean> = [];
  isLoaded = false;
  page: IPagination;
  searchKey: string | null = '';

  constructor(
    private tournamentService: TournamentService,
    private bracketService: BracketService,
    private eSportsToastService: EsportsToastService,
    private modalService: NgbModal,
    private translateService: TranslateService,
    public language: EsportsLanguageService,
    private dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.language.language.subscribe((lang) => {
      if (lang) {
        this.arebicText = lang === 'ar' ? true : false;
      }
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.tournament?.isCheckInRequired) {
      this.filterList[1] = { name: 'SEED.CHECK_IN', value: 'checked-in-only' };
    }

    if (
      changes.hasOwnProperty('tournament') &&
      changes.tournament.currentValue
    ) {
      if (this.tournament?.isSeeded) {
        if (['single', 'double'].includes(this.tournament?.bracketType)) {
          this.fetchAllMatches();
        } else {
          this.isLoaded = true;
        }
      } else {
        this.fetchAllApprovedParticipants();
      }
    }
  }

  /** Initialize table after filter or search */
  initializeTable() {
    this.activePage = 1;
    this.page = {
      totalItems: this.allSelectedParticipants.length,
      itemsPerPage: this.pageSize,
      maxSize: 5,
    };
    this.participantList = this.allSelectedParticipants.slice(
      this.pageSize * (this.activePage - 1),
      this.pageSize * this.activePage
    );
  }
  /** Get all approved participants of the tournament  */
  fetchAllApprovedParticipants = async () => {
    try {
      this.apiLoaded.push(false);
      const encodeUrl = `?query=${encodeURIComponent(
        JSON.stringify({
          tournamentId: this.tournament?._id,
          participantStatus: 'approved',
        })
      )}&select=checkedIn,teamName,seed,participantType,teamMembers,matchWin,name`;
      const participants = await this.tournamentService
        .getParticipants(encodeUrl)
        .toPromise();
      this.allApprovedParticipants = participants.data.map((obj, index) => {
        const record = obj;
        record.reg = index + 1;
        return record;
      });
      this.allSelectedParticipants = this.allApprovedParticipants.filter(
        (item: any) => {
          if (
            !this.selectFilterType ||
            this.selectFilterType?.value !== 'checked-in-only'
          ) {
            return true;
          }
          return item.checkedIn;
        }
      );
      this.initializeTable();
      this.apiLoaded.push(true);
    } catch (error) {
      this.apiLoaded.push(true);
      this.eSportsToastService.showError(error.message);
    } finally {
      this.checkAllApiDataLoaded();
    }
  };

  /** Validate seeding value */
  isDisableSaveAll() {
    const teamSet = new Set();
    let isInvalid = false;
    let reason = '';
    isInvalid = this.allApprovedParticipants
      .filter((item: any) => {
        if (
          !this.selectFilterType ||
          this.selectFilterType?.value !== 'checked-in-only'
        ) {
          return true;
        }
        return item.checkedIn;
      })
      .some((obj: any, index, array) => {
        if (!obj.seed || obj.seed < 1 || obj.seed > array.length) {
          if (this.tournament?.isCheckInRequired && !obj.checkedIn) return false;
          
          reason = this.translateService.instant(
            'MANAGE_TOURNAMENT.SEED_BRACKET.INCOMPLETE_SEEDING'
          );
          return true;
        } else if (teamSet.size === teamSet.add(obj.seed).size) {
          reason = this.translateService.instant(
            'MANAGE_TOURNAMENT.SEED_BRACKET.DUPLICATE_SEED_ID'
          );
          return true;
        }
      });
    return { isValid: !isInvalid, reason };
  }

  /** Save seeding value of participants */
  saveAllSeeds = async (isShowMessage = true) => {
    try {
      const { isValid, reason } = this.isDisableSaveAll();
      if (!isValid) {
        isShowMessage ? this.eSportsToastService.showError(reason) : null;
        return { isValid: false, reason };
      }
      const data = this.allApprovedParticipants.filter((item: any) => {
        if (
          !this.selectFilterType ||
          this.selectFilterType?.value !== 'checked-in-only'
        ) {
          return true;
        }
        return item.checkedIn;
      }).map(item => {
        return {
          _id: item._id,
          seed: item.seed
        }
      });
      const response = await this.tournamentService
        .updateParticipantBulk({ data })
        .toPromise();
      this.isChangeInSeedingOrder = false;
      this.eSportsToastService.showSuccess(response.message);
      return { isValid: true };
    } catch (error) {
      this.eSportsToastService.showError(
        error?.error?.message || error?.message
      );
      return {
        isValid: false,
        reason: error?.error?.message || error?.message,
      };
    }
  };

  /** filter participant by checked-in, not-previously-seed */
  filterByType(data) {
    const isExist = (item) => {
      return this.tournament?.participantType == 'individual'
        ? item?.name.toLowerCase().indexOf(this.searchKey.trim()) !== -1
        : item?.teamName.toLowerCase().indexOf(this.searchKey.trim()) !== -1;
    };

    const value = this.searchKey.toLowerCase().trim();

    switch (data.value) {
      case 'all-players':
        this.allSelectedParticipants = this.allApprovedParticipants.filter(
          (item: any) => {
            if (!value) {
              return true;
            }
            return isExist(item);
          }
        );
        break;
      case 'not-previously-seeded':
        this.allSelectedParticipants = this.allApprovedParticipants
          .filter((item: any) => item.seed)
          .filter((item: any) => {
            if (!value) {
              return true;
            }
            return isExist(item);
          });
        break;
      case 'checked-in-only':
        this.allSelectedParticipants = this.allApprovedParticipants
          .filter((item: any) => item.checkedIn)
          .filter((item: any) => {
            if (!value) {
              return true;
            }
            return isExist(item);
          });
        break;
      default:
        this.allSelectedParticipants = [];
        break;
    }
    this.initializeTable();
  }
  /** Client-side pagination for client by change page no or page size */
  pageSizeChanged(pageSize) {
    this.participantList = this.allSelectedParticipants.slice(
      pageSize * 0,
      pageSize * 1
    );
    this.initializeTable();
  }

  pageChanged(page) {
    this.participantList = this.allSelectedParticipants.slice(
      this.pageSize * (page - 1),
      this.pageSize * page
    );
  }

  /** Generate mock bracket structure */
  generateBracket = async () => {
    try {
      if (this.tournament?.bracketType == 'swiss_safeis') {
        await this.modalService.open(this.bracketConfirmRef, {
          windowClass: 'confirm-modal-content',
          centered: true,
        });
      } else {
        const data: any = this.isChangeInSeedingOrder
          ? await this.saveAllSeeds(false)
          : this.isDisableSaveAll();

        if (!data.isValid) {
          throw new Error(data.reason);
        }

        const payload = {
          tournamentId: this.tournament?._id,
          ...(this.selectFilterType?.value === 'checked-in-only' && {
            checkedIn: true,
          }),
        };
        const response = await this.bracketService.generateBracket(payload);
        this.structure = response.data;
        this.isSeeded = true;
      }
    } catch (error) {
      this.eSportsToastService.showError(
        error?.error?.error || error?.error?.message || error.message
      );
    }
  };

  /** Confirmation modal for bracket generation */
  openModal(content) {
    this.modalService.open(content, {
      windowClass: 'confirm-modal-content',
      centered: true,
    });
  }

  /** Get all saved matches of the tournamnet */
  fetchAllMatches = async () => {
    try {
      this.apiLoaded.push(false);
      const queryParam = `?query=${encodeURIComponent(
        JSON.stringify({ tournamentId: this.tournament?._id })
      )}&select=bye,currentMatch,matchStatus,teamA,teamB,disQualifiedTeam,placeHolderA,placeHolderB,teamAWinSet,teamBWinSet,isThirdPlaceMatch,matchNo,battleRoyalTeams.noOfKill,battleRoyalTeams.placement,battleRoyalTeams.score,battleRoyalTeams._id,battleRoyalScoringOption,externalMatchId,isScoreConflict&option=${encodeURIComponent(
        JSON.stringify({ sort: { matchNo: 1 } })
      )}`;
      const response = await this.bracketService.fetchAllMatches(queryParam);
      this.structure = this.bracketService.assembleStructure(response.data);
      this.apiLoaded.push(true);
    } catch (error) {
      this.apiLoaded.push(true);
      this.eSportsToastService.showError(error.message);
    } finally {
      this.checkAllApiDataLoaded();
    }
  };

  /** Confirm mock structure and save it in data base */
  confirmBracket = async () => {
    try {
      const payload = {
        tournamentId: this.tournament?._id,
        ...(this.selectFilterType?.value === 'checked-in-only' && {
          checkedIn: true,
        }),
      };
      const response = await this.bracketService.saveGeneratedBracket(payload);
      this.eSportsToastService.showSuccess(response.message);
      this.message.emit({
        type: 'tournament-started',
        isRefresh: true
      });
      if(response?.createLobbyResp?.status == 200 || response?.createLobbyResp?.status == 400){
        const respHeader =
          response?.createLobbyResp?.status == 200
            ? 'API.PRODUCT.POST.SUCCESS_HEADER'
            : 'API.PRODUCT.POST.ERROR_HEADER';
          const afterBlockData: InfoPopupComponentData = {
            title: this.translateService.instant(respHeader),
            text: response?.createLobbyResp?.message,
            type: InfoPopupComponentType.info,
          };
          this.dialog.open(InfoPopupComponent, { data: afterBlockData });
      }
    } catch (error) {
      this.eSportsToastService.showError(error.message);
    }
  };

  /** Get response from child component */
  getResponseMessage(data) {
    if (data) {
      this.message.emit({
        type: 'tournament-finished',
        isRefresh: true
      });
    }
  }

  /** Assign seed value via Registration order, random order or manually. */
  assignSeedValue(method) {
    switch (method.value) {
      case 'regOrder':
        this.seedByRegNo();
        break;
      case 'random':
        this.seedRandomly();
        break;
      default:
        break;
    }
  }

  seedByRegNo() {
    this.isChangeInSeedingOrder = true;
    this.allApprovedParticipants
      .filter((item: any) => {
        if (
          (!this.selectFilterType ||
            this.selectFilterType?.value !== 'checked-in-only') &&
          !this.tournament?.isCheckInRequired
        ) {
          return true;
        }
        return item.checkedIn;
      })
      .forEach((participant: any, index) => {
        participant.seed = index + 1;
        const main_index = this.allApprovedParticipants.findIndex(
          (item: any) => item._id === participant._id
        );
        this.allApprovedParticipants[main_index].seed = index + 1;
      });
  }

  seedRandomly() {
    const tempArray = [];
    this.isChangeInSeedingOrder = true;
    const availableParticipant = this.allApprovedParticipants.filter(
      (item: any) => {
        if (
          (!this.selectFilterType ||
            this.selectFilterType?.value !== 'checked-in-only') &&
          !this.tournament?.isCheckInRequired
        ) {
          return true;
        }
        return item.checkedIn;
      }
    );
    while (tempArray.length < availableParticipant.length) {
      const seed = Math.floor(Math.random() * availableParticipant.length) + 1;
      if (tempArray.indexOf(seed) === -1) {
        tempArray.push(seed);
      }
    }
    availableParticipant.forEach((participant: any, index) => {
      const mainIndex = this.allApprovedParticipants.findIndex(
        (item: any) => item._id === participant._id
      );
      const subIndex = this.allSelectedParticipants.findIndex(
        (item: any) => item._id === participant._id
      );
      if (mainIndex >= 0) {
        this.allApprovedParticipants[mainIndex].seed = tempArray[index];
      }
      if (subIndex >= 0) {
        this.allSelectedParticipants[subIndex].seed = tempArray[index];
      }
    });
  }

  manualSeedValidation(index, _id, value) {
    const tempArrayLength = this.allApprovedParticipants.filter((item: any) => {
      if (
        !this.selectFilterType ||
        this.selectFilterType?.value !== 'checked-in-only'
      ) {
        return true;
      }
      return item.checkedIn;
    }).length;
    const mainIndex = this.allApprovedParticipants.findIndex(
      (item: any) => item._id === _id
    );
    this.allApprovedParticipants[mainIndex].seed = value && Number(value);
    if (value) {
      this.isChangeInSeedingOrder = true;
      if (value < 1) {
        this.allSelectedParticipants[index].seed = 1;
        this.allApprovedParticipants[mainIndex].seed = 1;
      } else if (value > tempArrayLength) {
        this.allSelectedParticipants[index].seed = tempArrayLength;
        this.allApprovedParticipants[mainIndex].seed = tempArrayLength;
      }
    }
  }

  /**
   * Check All API's Are Finished.
   * If All APi's Finished Then Remove Full Page Loader
   */
  checkAllApiDataLoaded() {
    const startedApiCount = this.apiLoaded.filter((el) => !el).length;
    const finishedApiCount = this.apiLoaded.filter((el) => el).length;
    this.isLoaded = startedApiCount === finishedApiCount;
  }

  showInfo(type) {
    switch (type) {
      case 'reset':
        this.eSportsToastService.showInfo(
          this.translateService.instant(
            'MANAGE_TOURNAMENT.SEED_BRACKET.RESET_INFO'
          )
        );
        break;
      case 'shuffle':
        this.eSportsToastService.showInfo(
          this.translateService.instant(
            'MANAGE_TOURNAMENT.SEED_BRACKET.SHUFFLE_INFO'
          )
        );
        break;
      default:
        break;
    }
  }

  onSearch(text) {
    const value = text.toLowerCase().trim();
    this.searchKey = text;
    this.allSelectedParticipants = this.allApprovedParticipants
      .filter((item: any) => {
        if (
          !this.selectFilterType ||
          this.selectFilterType?.value !== 'checked-in-only'
        ) {
          return true;
        }
        return item.checkedIn;
      })
      .filter(
        (item: any) => item?.teamName.toLowerCase().indexOf(value) !== -1
      );

    this.initializeTable();
  }

  onClearSearch(event) {
    setTimeout(() => {
      if (!event?.target?.value) {
        this.onSearch(''); // reset search
      }
    }, 100);
  }
}
