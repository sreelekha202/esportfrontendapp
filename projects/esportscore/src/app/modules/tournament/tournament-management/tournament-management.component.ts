import { Component, OnInit, OnDestroy, Inject, PLATFORM_ID } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { FormControl } from '@angular/forms';
import { debounceTime, switchMap } from 'rxjs/operators';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { TranslateService } from '@ngx-translate/core';
import { TournamentService } from '../../../core/service';
import { IUser, EsportsUserService, EsportsToastService, EsportsTimezone, EsportsChatService, GlobalUtils } from 'esports';
import 'rxjs/add/operator/filter';
import { isPlatformBrowser } from '@angular/common';
import {RouterExtService } from '../../../core/service/routing.service'
@Component({
  selector: 'app-tournament-management',
  templateUrl: './tournament-management.component.html',
  styleUrls: ['./tournament-management.component.scss'],
})
export class TournamentManagementComponent implements OnInit, OnDestroy {
  active = 1;
  tournament: any = {};
  currentDropdownTab = 'pendingParticipants';
  tournamentStartTime = '';
  registeredParticipantCount = 0;
  checkedInParticipantCount = 0;
  searchKey = '';
  isLoaded = false;
  noStart = false;
  tournamentDetails;
  apiLoaded: Array<boolean> = [];
  user: IUser;
  hideMessage = true;
  userSubscription: Subscription;
  timezone;
  hasRequiredAccess: boolean = true; 

  startDateIntervalId = null;
  endDateIntervalId = null;

  constructor(
    private activatedRoute: ActivatedRoute,
    private eSportsToastService: EsportsToastService,
    private modalService: NgbModal,
    private tournamentService: TournamentService,
    private translateService: TranslateService,
    private userService: EsportsUserService,
    private esportsTimezone: EsportsTimezone,
    private eSportsChatService: EsportsChatService,
    private routerExtService: RouterExtService,
    @Inject(PLATFORM_ID) private platformId
  ) {}

  ngOnInit(): void {
    // should be here for SSR
    const isBrowser = isPlatformBrowser(this.platformId);
    if (isBrowser && GlobalUtils.isBrowser()) {
      if(this.routerExtService.getPreviousUrl().includes("/match-view?matchId")){
        this.active=3;
      }
      this.timezone = this.esportsTimezone.getTimeZoneName();
      this.tournament.slug = this.activatedRoute.snapshot.params.id; // get slug from route
      this.eSportsChatService.connectTournamentEvents(this.tournament.slug);
  
      if (this.tournament.slug) {
        this.getCurrentUserDetails();
      }
    }
  }

  isNumber(val): boolean { const str: string = val.toString(); return str.startsWith("+"); }
  
  /**
   * Fetch TOurnament Details
   */
  fetchTournamentDetails = async (isTournamentStart: boolean = false, isTournamentFinished: boolean = false) => {
    try {
      this.apiLoaded.push(false);
      const tournament = await this.tournamentService.getTournamentBySlug(this.tournament?.slug).toPromise();
      this.tournament = tournament?.data;
      
      if (!this.tournament) {
        this.hideMessage = false;
        throw new Error(this.translateService.instant('VIEW_TOURNAMENT.ERROR'));
      }

      const isEventOrganizer = this.user?._id === this.tournament?.organizerDetail?._id;
      const hasAdminAccess = this.user?.accountType == 'admin' && this.user?.accessLevel?.includes('em');

      this.hasRequiredAccess = isEventOrganizer || hasAdminAccess;
      if (!this.hasRequiredAccess) {
        throw new Error(
          this.translateService.instant('VIEW_TOURNAMENT.ACCESS_DENIED')
        )
      }

      if (this.tournament?.isFinished) {
        if (isTournamentFinished) {
          this.eSportsToastService.showInfo(
            this.translateService.instant('ELIMINATION.TOURNAMENT_FINISHED'),
            false
          );
        }
        this.tournamentStartTime = this.translateService.instant(
          'MANAGE_TOURNAMENT.TOURNAMENT_FINISHED'
        );
      } else if (this.tournament?.isSeeded) {
        if (isTournamentStart) {
          this.eSportsToastService.showInfo(
            this.translateService.instant('ELIMINATION.TOURNAMENT_STARTED'),
            false
          );
        }

        this.tournamentStartTime = this.translateService.instant(
          'MANAGE_TOURNAMENT.TOURNAMENT_STARTED'
        );
        this.endDateTimer();
      } else {
        this.startDateTimer();
        this.noStart = true;
      }
      this.fetchRegisteredParticipant('registeredParticipantCount');
      this.fetchRegisteredParticipant('checkedInParticipantCount', {
        checkedIn: true,
      });
      this.apiLoaded.push(true);
    } catch (error) {
      this.hasRequiredAccess = false;
      this.apiLoaded.push(true);
      this.eSportsToastService.showError(error?.error?.message || error?.message);
    } finally {
      this.checkAllApiDataLoaded();
    }
  };

  /**
   * Fatch Register Or CheckedIn Participant
   * @param field registeredParticipantCount, checkedInParticipantCount
   * @param obj for checkedIn users
   */
  fetchRegisteredParticipant = async (field, obj = {}) => {
    try {
      this.apiLoaded.push(false);
      const encodeUrl = `?query=${encodeURIComponent(
        JSON.stringify({ tournamentId: this.tournament?._id, ...obj })
      )}&select=''&option=${encodeURIComponent(
        JSON.stringify({ count: true })
      )}`;
      const response = await this.tournamentService
        .getParticipants(encodeUrl)
        .toPromise();
      this[field] = response?.totals?.count;
      this.apiLoaded.push(true);
    } catch (error) {
      this.apiLoaded.push(true);
      this.eSportsToastService.showError(error?.error?.message || error?.message);
    } finally {
      this.checkAllApiDataLoaded();
    }
  };

  /**
   * Open Popup
   * @param content model template
   */
  openModal(content) {
    this.modalService.open(content, {
      windowClass: 'preference-modal-content',
      centered: true,
    });
  }

  /**
   * Start date Timer
   */
  startDateTimer = () => {
    const startDate = new Date(this.tournament.startDate);
    const currentDate = new Date();
    if (startDate.getTime() - currentDate.getTime() > 0) {
      const millisecond = startDate.getTime() - currentDate.getTime();
      const dd = (millisecond / (1000 * 60 * 60 * 24)) | 0;
      if (dd) {
        const currentLanguage = this.translateService.currentLang;

        if(currentLanguage == 'en') {
          this.tournamentStartTime = `${dd} ${
            dd === 1
              ? this.translateService.instant('MANAGE_TOURNAMENT.DAYS_LEFT.01')
              : this.translateService.instant('MANAGE_TOURNAMENT.DAYS_LEFT.02')
          }`;
        } else {
          if(dd === 1) {
            this.tournamentStartTime = `${dd} ${ this.translateService.instant('MANAGE_TOURNAMENT.DAYS_LEFT.01')}`
          }
          else if (dd === 2){
            this.tournamentStartTime = `${ this.translateService.instant('MANAGE_TOURNAMENT.DAYS_LEFT.02')}`
          }
          else if (dd === 3){
            this.tournamentStartTime = `${dd} ${ this.translateService.instant('MANAGE_TOURNAMENT.DAYS_LEFT.03')}`
          }
          else if (dd === 11){
            this.tournamentStartTime = `${dd} ${ this.translateService.instant('MANAGE_TOURNAMENT.DAYS_LEFT.04')}`
          }
          else {
            this.tournamentStartTime = `${dd} ${ this.translateService.instant('MANAGE_TOURNAMENT.DAYS_LEFT.04')}`
          }
        }
        return;
      }
    
      const hh = (millisecond / (1000 * 60 * 60)) | 0;
      const mm = ((millisecond - hh * 1000 * 60 * 60) / (1000 * 60)) | 0;
      const ss = ((millisecond - hh * 1000 * 60 * 60) / 1000) % 60 | 0;
      this.tournamentStartTime = `${hh > 9 ? hh : `0${hh}`}: ${ mm > 9 ? mm : `0${mm}`}: ${ss > 9 ? ss : `0${ss}`}`;

      if (!this.startDateIntervalId) {
        this.startDateIntervalId = setInterval(() => this.startDateTimer(), 1000);
      }
    } else {
      if (this.startDateIntervalId) {
        this.fetchTournamentDetails(true);
      } else {
        this.tournamentStartTime = this.translateService.instant(
          'MANAGE_TOURNAMENT.TOURNAMENT_EXPIRED'
        );
      }
      clearInterval(this.startDateIntervalId);
      this.startDateIntervalId = null;
    }
  } 
  
  /**
   * End date timer
   */
  endDateTimer = () => {
    const endDate = new Date(this.tournament?.endDate);
    const currentDate = new Date();
    const timeDiff = endDate.getTime() - currentDate.getTime();
    if (timeDiff > 0) {
      this.endDateIntervalId = setInterval(() => this.endDateTimer(), 1000);
    } else if (['swiss_safeis', 'ladder'].includes(this.tournament?.bracketType)) {
      if (this.endDateIntervalId) {
        this.fetchTournamentDetails(false, true);
        clearInterval(this.endDateIntervalId);
        this.endDateIntervalId = null;
      }
    }
  }

  /**
   * Get Child Component Response
   * @param data child component response
   */
  getComponentResponse(data) {
    let isRefresh = false;
    if (typeof data === 'object') {
      if (data.active) {
        this.active = data.active;
      }
      isRefresh = data?.refresh || data?.isRefresh;
    } else {
      isRefresh = data;
    }

    if (isRefresh) {
      this.fetchTournamentDetails();
      clearInterval(this.startDateIntervalId);
      this.startDateIntervalId = null;
      if (data.type) {
        this.eSportsChatService.emitTournamentEvents(this.tournament?.slug, this.user?._id, data?.type);
      }
    }
  }

  ngOnDestroy() {
    clearInterval(this.startDateIntervalId);
    clearInterval(this.endDateIntervalId);
    this.userSubscription?.unsubscribe();
    this.eSportsChatService.disconnectTournamentEvents(this.tournament.slug);
  }

  /**
   * Check All API's Are Finished.
   * If All APi's Finished Then Remove Full Page Loader
   */
  checkAllApiDataLoaded() {
    const startedApiCount = this.apiLoaded.filter((el) => !el).length;
    const finishedApiCount = this.apiLoaded.filter((el) => el).length;
    this.isLoaded = startedApiCount === finishedApiCount;
  }

  /**
   * Get Current User
   */
  getCurrentUserDetails = async () => {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.user = data;
        this.fetchTournamentDetails();
      }
    });
  }
}
