import { NgModule } from '@angular/core';
import { FaIconLibrary } from '@fortawesome/angular-fontawesome';
import {
  faSortDown,
  faSortUp,
  faTrash,
  faEdit,
  faCheck,
  faCheckCircle,
  faTimesCircle,
  faCaretDown,
  faThumbsUp,
  faThumbsDown,
} from '@fortawesome/free-solid-svg-icons';

import { SharedModule } from '../../../shared/modules/shared.module';

import { TournamentManagementComponent } from './tournament-management.component';
import { SeedComponent } from './seed/seed.component';
import { PaginationModule, PaginationConfig } from 'ngx-bootstrap/pagination';
import { TournamentManagementRoutingModule } from './tournament-management-routing.module';
import { ParticipantsComponent } from './participants/participants.component';
import { EditComponent } from './edit/edit.component';
import { FormComponentModule } from '../../../shared/components/form-component/form-component.module';
// import { PrizeDisbursalComponent } from '../../admin/components/prize-disbursal/prize-disbursal.component';

// Pipe
import { ParticipantTypePipe } from './pipe/participant-type.pipe';
import { ParticipantStatusPipe } from './pipe/participant-status.pipe';
import { WinnersComponent } from './winners/winners.component';
import { CoreModule } from '../../../core/core.module';
import { PrizeDisbursalModule } from '../../admin/components/prize-disbursal/prize-disbursal.module';
import { ShowUserReportComponent } from './show-user-report/show-user-report.component';
import { NotificationComponent } from './notification/notification.component';
import { EsportsCustomPaginationModule, WYSIWYGEditorModule } from 'esports';
import { EmailComponent } from './notification/email/email.component';
import { PushNotificationComponent } from './notification/push-notification/push-notification.component';
import { environment } from '../../../../environments/environment';
@NgModule({
  declarations: [
    TournamentManagementComponent,
    SeedComponent,
    ParticipantsComponent,
    EditComponent,
    ParticipantTypePipe,
    ParticipantStatusPipe,
    WinnersComponent,
    ShowUserReportComponent,
    NotificationComponent,
    EmailComponent,
    PushNotificationComponent,
    // PrizeDisbursalComponent
  ],
  imports: [
    TournamentManagementRoutingModule,
    PaginationModule.forRoot(),
    SharedModule,
    WYSIWYGEditorModule.forRoot(environment),
    CoreModule,
    FormComponentModule,
    PrizeDisbursalModule,
    EsportsCustomPaginationModule
  ],
  exports: [TournamentManagementComponent],
  providers: [{ provide: PaginationConfig }],
})
export class TournamentManagementModule {
  constructor(private library: FaIconLibrary) {
    library.addIcons(
      faSortDown,
      faSortUp,
      faTrash,
      faEdit,
      faCheck,
      faCheckCircle,
      faTimesCircle,
      faCaretDown,
      faThumbsUp,
      faThumbsDown
    );
  }
}
