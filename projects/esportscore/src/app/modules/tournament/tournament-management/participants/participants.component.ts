import {
  Component,
  OnInit,
  Input,
  OnChanges,
  SimpleChanges,
  Inject,
} from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { Router } from '@angular/router';
import {
  TournamentService,
  BracketService,
  ParticipantService,
} from '../../../../core/service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import {
  IUser,
  ITournament,
  IPagination,
  EsportsToastService,
  BracketPipe,
} from 'esports';
import { MatDialog } from '@angular/material/dialog';
import {
  InfoPopupComponent,
  InfoPopupComponentData,
  InfoPopupComponentType,
} from '../../../../shared/popups/info-popup/info-popup.component';
import {
  IFieldType,
  IParticipantStatus,
} from '../../../../shared/models/participant';
@Component({
  selector: 'app-participants',
  templateUrl: './participants.component.html',
  styleUrls: [
    './participants.component.scss',
    '../tournament-management.component.scss',
  ],
  providers: [BracketPipe],
})
export class ParticipantsComponent implements OnInit, OnChanges {
  @Input() tournament: ITournament;
  @Input() currentTab: string;
  @Input() user: IUser;

  selectedTeam = null;
  selectedParticipantStatus = null;
  exportData: Array<{}> = [];
  csvData: Array<{}> = [];
  buttonDisabled = false;

  showRegCloseBtn = false;
  isRegistrationClosed = false;

  topThreeWinners = [];
  tournamentMatcheCount = 0;
  participantPage: IPagination = {
    activePage: 1,
    totalItems: 1,
    maxSize: 10,
    itemsPerPage: 100,
  };
  participantData: any;
  isProccessing: boolean = false;

  constructor(
    private tournamentService: TournamentService,
    private eSportsToastService: EsportsToastService,
    private participantService: ParticipantService,
    private modalService: NgbModal,
    private translateService: TranslateService,
    private bracketService: BracketService,
    private router: Router,
    private matDialog: MatDialog,
    @Inject(DOCUMENT) private document: Document,
    private bracketPipe: BracketPipe
  ) {}

  ngOnInit(): void {}

  ngOnChanges() {
    if (this.currentTab && this.tournament?._id) {
      this.selectedTeam = null;
      this.setParticipant(this.participantPage?.activePage);
    }

    if (this.tournament?._id) {
      this.isRegistrationClosed = this.tournament?.isRegistrationClosed;

      if (this.tournament?.regStartDate && this.tournament?.regEndDate) {
        const currentDate = new Date();
        const registrationStartDt = new Date(this.tournament?.regStartDate);
        const registrationEndDt = new Date(this.tournament?.regEndDate);

        /** Check current date is lie between registartion start date and registration end date */
        const withInRange =
          registrationEndDt > currentDate && currentDate > registrationStartDt;
        this.showRegCloseBtn = withInRange;
      }
    }
  }

  /**
   * Open Confirmation Modal To Approve Or Reject Participant
   * @param content model template
   * @param participant participant details
   * @param isIndividaul check
   */
  openModelOrUpdateTeam = async (content, participant, isIndividaul = true) => {
    this.selectedParticipantStatus = participant?.participantStatus;
    const result = isIndividaul
      ? await this.modalService.open(content, {
          windowClass: 'confirm-modal-content',
          centered: true,
        }).result
      : content;
    if (result === 'approved') {
      const data = {
        participantStatus: 'approved',
      };
      this.updateParticipant(participant._id, data);
    } else if (result === 'rejected') {
      const data = {
        participantStatus: 'rejected',
      };

      this.updateParticipant(participant._id, data);
    } else if (result === 'edit') {
      if (participant?.status == 0) {
        const afterBlockData: InfoPopupComponentData = {
          title: this.translateService.instant('TOURNAMENT.ERROR'),
          text: this.translateService.instant('TOURNAMENT.UNABLE_TO_EDIT'),
          type: InfoPopupComponentType.info,
        };
        this.matDialog.open(InfoPopupComponent, { data: afterBlockData });
      } else {
        this.router.navigate([
          '/tournament/' +
            this.tournament?.slug +
            '/join' +
            '/' +
            participant._id,
        ]);
      }
    } else {
      this.selectedTeam = null;
    }
    this.selectedParticipantStatus = null;
  };

  /**
   * Update Participant Status
   * @param id participantID
   * @param data participant status
   */
  updateParticipant = async (id, data) => {
    try {
      this.selectedTeam = null;
      const response = await this.tournamentService
        .updateParticipant(id, data)
        .toPromise();
      if (response?.success) {
        this.eSportsToastService.showSuccess(response?.message);
        this.setParticipant(this.participantPage?.activePage);
      } else {
        this.eSportsToastService.showInfo(response?.message);
      }
    } catch (error) {
      this.eSportsToastService.showError(
        error?.error?.message || error?.message
      );
    }
  };

  /**
   * Set Participant List Based On Current Tab
   */
  setParticipant(page: number = 1) {
    switch (this.currentTab) {
      case 'pendingParticipants':
        this.fetchParticipantByStatus(0, page);
        break;
      case 'approvedParticipants':
        this.fetchParticipantByStatus(1, page);
        break;
      case 'rejectedParticipants':
        this.fetchParticipantByStatus(2, page);
        break;
      default:
        this.fetchParticipantByStatus(0, page);
        break;
    }
  }

  goBack() {
    this.selectedTeam = false;
  }

  fetchAllParticipant = async (isTeam: boolean = false) => {
    const allParticipant = [];
    try {
      const exportCSV = (
        data: any,
        teamName: string | null,
        captainName: string | null,
        status: string | null,
        pNameSubstr: string,
        isTeam: boolean
      ) => {
        return {
          ...(isTeam && {
            'Team Name': teamName || '',
            'Captain Name': captainName || '',
          }),
          'Player Name': data?.name ? `${pNameSubstr}${data?.name}` : 'N/A',
          'Email Id': data?.email || 'N/A',
          'Mobile Number': data?.phoneNumber || 'N/A',
          'Game Id': data?.inGamerUserId || 'N/A',
          ...(status && { Status: status }),
        };
      };

      const getParticipantsRecursively = async (page = 1) => {
        try {
          const {
            data,
          } = await this.participantService.fetchParticipantByStatus(
            4,
            this.tournament?._id,
            page
          );
          for (let item of data?.docs || []) {
            allParticipant.push(
              exportCSV(
                item,
                item?.teamName,
                item?.name,
                item?.participantStatus || 'pending',
                '',
                isTeam
              )
            );
            for (let teamMember of item?.teamMembers || []) {
              allParticipant.push(
                exportCSV(teamMember, '', '', '', '', isTeam)
              );
            }
            for (let substituteMember of item?.substituteMembers || []) {
              allParticipant.push(
                exportCSV(substituteMember, '', '', '', '(SUB) ', isTeam)
              );
            }
          }

          if (page == data?.totalPages) {
            return allParticipant;
          } else {
            return await getParticipantsRecursively(page + 1);
          }
        } catch (error) {
          return allParticipant;
        }
      };

      return await getParticipantsRecursively();
    } catch (error) {
      const errormessage = error?.error?.message || error?.message;
      this.eSportsToastService.showError(errormessage);
      return [];
    }
  };

  fetchWinnersAndMatches = async () => {
    try {
      const {
        data,
        matchCount,
      } = await this.bracketService.fetchTournamentWinnersAndMatchCount(
        this.tournament?._id
      );

      // Winner string
      const firstWinner = data && data[0]?.teamName;
      const secondWinner = data && data[1]?.teamName;
      const thirdWinner = data && data[2]?.teamName;
      let winners = '';
      if (firstWinner) winners = winners + `1. ${firstWinner}`;
      if (secondWinner) winners = winners + `2. ${secondWinner}`;
      if (thirdWinner) winners = winners + `3. ${thirdWinner}`;
      return { matchCount, winners };
    } catch (error) {
      const errorMessage = error?.error?.message || error?.message;
      this.eSportsToastService.showError(errorMessage);
      return { matchCount: 0, winners: '' };
    }
  };

  exportToCSV = async () => {
    const isTeam = this.tournament?.participantType == 'team';
    const allParticipant = await this.fetchAllParticipant(isTeam);
    const { matchCount, winners } = await this.fetchWinnersAndMatches();
    const bracketType = this.translateService.instant(
      this.bracketPipe.transform(this.tournament?.bracketType)
    );

    const getFormatedDate = (date) => {
      return new Date(date).toLocaleDateString('en-US', {
        weekday: 'short',
        year: 'numeric',
        month: 'short',
        day: 'numeric',
      });
    };

    const tournament = {
      'Tournament Name': this.tournament?.name,
      'Tournament Type': bracketType.toUpperCase(),
      Participants: allParticipant.length,
      'Start Date': getFormatedDate(this.tournament?.startDate),
      'End Date': getFormatedDate(this.tournament.endDate),
      'Games Played': matchCount,
      Winners: winners,
      Creator: this.tournament.organizerDetail?.fullName,
      Game: this.tournament.gameDetail?.name,
      'Team Size': this.tournament.teamSize,
    };

    this.JSONToCSVConvertor(
      allParticipant,
      this.tournament.name,
      true,
      tournament
    );
  };

  JSONToCSVConvertor(
    participantArray: Array<any>,
    ReportTitle: string,
    ShowLabel: boolean,
    tournament: any
  ) {
    var row = '';
    var CSV = '';

    if (ShowLabel) {
      for (let i in tournament) {
        row += i + ',';
      }
      row = row.slice(0, -1);
      CSV += row + '\r\n';
      row = '';
      for (let i in tournament) {
        row += '"' + tournament[i] + '",';
      }
      row.slice(0, row.length - 1);
      CSV += row + '\r\n\n';

      row = '';
      for (var index in participantArray[0]) {
        row += index + ',';
      }
      row = row.slice(0, -1);
      CSV += row + '\r\n';
    }

    for (var i = 0; i < participantArray.length; i++) {
      var row = '';
      for (var index in participantArray[i]) {
        if (index === 'Mobile Number') {
          row += '"=""' + participantArray[i][index] + '""",';
        } else {
          row += '"' + participantArray[i][index] + '",';
        }
      }
      row.slice(0, row.length - 1);
      CSV += row + '\r\n';
    }

    if (CSV == '') {
      this.eSportsToastService.showError('Invalid data');
      return;
    }
    var universalBOM = '\uFEFF';
    var fileName = 'STC_';
    fileName += ReportTitle?.replace(/ /g, '_').toUpperCase();
    var uri =
      'data:text/csv;charset=utf-8,' + encodeURIComponent(universalBOM + CSV);
    var link = this.document.createElement('a');
    link.href = uri;
    link.download = fileName + '.csv';
    this.document.body.appendChild(link);
    link.click();
    this.document.body.removeChild(link);
  }

  async closeRegistration(value) {
    const currDate = new Date();
    const data: InfoPopupComponentData = {
      ...this.popUpTitleAndText(this.tournament),
      type: InfoPopupComponentType.confirm,
      btnText: this.translateService.instant('TOURNAMENT.CONFIRM'),
    };

    const confirmed = await this.matDialog
      .open(InfoPopupComponent, { data })
      .afterClosed()
      .toPromise();
    if (confirmed) {
      this.tournamentService
        .updateTournament(
          {
            isRegistrationClosed: value,
            regEndDate: currDate,
          },
          this.tournament._id
        )
        .subscribe(
          (res) => {
            if (res) {
              this.eSportsToastService.showSuccess('Registration closed');
              this.isRegistrationClosed = res.data.isRegistrationClosed;
              this.buttonDisabled = true;
              let currentUrl = this.router.url;
              this.router
                .navigateByUrl('/', { skipLocationChange: true })
                .then(() => {
                  this.router.navigate([currentUrl]);
                });
            } else {
            }
          },
          (err: any) => {
            this.eSportsToastService.showError('Error');
            let currentUrl = this.router.url;
            this.router
              .navigateByUrl('/', { skipLocationChange: true })
              .then(() => {
                this.router.navigate([currentUrl]);
              });
          }
        );
    } else {
      this.isRegistrationClosed = !value;
      let currentUrl = this.router.url;
      this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
        this.router.navigate([currentUrl]);
      });
    }
  }
  popUpTitleAndText = (data) => {
    if (data?._id) {
      return {
        title: this.translateService.instant(
          'TOURNAMENT.CLOSE_REGISTRATION_HEADER'
        ),
        text: this.translateService.instant('TOURNAMENT.CLOSE_REGISTRATION'),
      };
    }
  };

  rejectAllPendingParticipants = async () => {
    try {
      const payload = {
        id: this.tournament?._id,
        idType: IFieldType.tournament,
        newStatus: IParticipantStatus.rejected,
        previousStatus: IParticipantStatus.pending,
      };
      const {
        success,
        message,
      } = await this.participantService.updateParticipantByStatus(payload);
      const key = success ? 'showSuccess' : 'showInfo';
      this.eSportsToastService[key](message);
      this.fetchParticipantByStatus(0);
    } catch (error) {
      const errorMessage = error?.error?.message || error?.message;
      this.eSportsToastService.showError(errorMessage);
    }
  };

  fetchParticipantByStatus = async (type: number, page = 1) => {
    try {
      if (this.isProccessing) return;
      this.isProccessing = true;
      const { data } = await this.participantService.fetchParticipantByStatus(
        type,
        this.tournament._id,
        page
      );
      this.participantPage.itemsPerPage = data?.limit;
      this.participantPage.totalItems = data?.totalDocs;
      this.participantData = data;
      this.isProccessing = false;
    } catch (error) {
      this.isProccessing = false;
      this.eSportsToastService.showError(
        error?.error?.message || error?.message
      );
    }
  };

  currentPage(page: number) {
    this.setParticipant(page);
  }
}
