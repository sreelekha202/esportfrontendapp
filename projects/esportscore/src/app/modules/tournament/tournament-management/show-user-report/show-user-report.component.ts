import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { UserReportsService, EsportsUserService, IPagination } from 'esports';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { environment } from '../../../../../environments/environment';

@Component({
  selector: 'app-show-user-report',
  templateUrl: './show-user-report.component.html',
  styleUrls: ['./show-user-report.component.scss'],
})
export class ShowUserReportComponent implements OnInit, OnDestroy {
  @Input() tournamentId;
  userReports = [];
  userReportDetail;
  replyText;
  base64textString;
  isLoaded = false;
  screenshot = '../assets/images/articles/banner.png';
  userSubscription: Subscription;
  isAdmin = false;
  paginationData = {
    page: 1,
    limit: 10,
  };
  page: IPagination;

  constructor(
    private UserReportsService: UserReportsService,
    private modalService: NgbModal,
    private userService: EsportsUserService
  ) {}

  ngOnInit(): void {
    this.getCurrentUserDetails();
    this.getUserReport();
  }

  ngOnDestroy() {
    this.userSubscription?.unsubscribe();
  }

  pageChanged(page): void {
    this.paginationData.page = page;
    this.getUserReport();
  }

  getCurrentUserDetails() {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        if (data?.accountType === 'admin') {
          this.isAdmin = true;
        }
      }
    });
  }

  getUserReport() {
    this.isLoaded = true;
    this.UserReportsService.getTournamentsUserReport(
      environment.apiEndPoint,
      this.tournamentId,
      this.paginationData?.limit,
      this.paginationData?.page
    ).subscribe(
      (res: any) => {
        this.userReports = res?.data?.docs;
        this.isLoaded = false;
        this.page = {
              totalItems: res?.data?.totalDocs,
              itemsPerPage: res?.data?.limit,
              maxSize: 5,
            };
      },
      (err) => {
        this.isLoaded = false;
      }
    );
  }

  getReportDetail(event) {
    if (event.type == 'click') {
      let id = event.row._id;
      this.isLoaded = true;
      this.UserReportsService.getUserReportById(
        environment.apiEndPoint,
        id
      ).subscribe(
        (res: any) => {
          this.userReportDetail = res.data;
          if (res?.data?.discussion?.length) {
            this.userReportDetail['adminUser'] = res.data.discussion.find(
              (item) => {
                if (item?.updatedBy?.accountType === 'admin') {
                  return item;
                }
              }
            );
          }
          this.isLoaded = false;
        },
        (err) => {
          this.isLoaded = false;
        }
      );
    }
  }

  backBtn() {
    this.userReportDetail = false;
  }

  onReply(isClose) {
    if (
      this.userReportDetail.report._id &&
      (this.replyText || this.base64textString)
    ) {
      const data = {
        reportId: this.userReportDetail.report._id,
        reportText: this.replyText,
        reportImage: this.base64textString || '',
      };
      this.isLoaded = true;
      this.UserReportsService.replyOnUserReport(
        environment.apiEndPoint,
        data
      ).subscribe(
        (res) => {
          if (isClose) {
            this.UserReportsService.closeReport(
              environment.apiEndPoint,
              this.userReportDetail.report._id
            ).subscribe(
              (res) => {
                this.isLoaded = false;
                this.userReportDetail = false;
                this.getUserReport();
              },
              (err) => {
                this.isLoaded = false;
                this.userReportDetail = false;
              }
            );
            this.replyText = '';
            this.base64textString = '';
          } else {
            this.replyText = '';
            this.base64textString = '';
            this.isLoaded = false;
            this.userReportDetail = false;
            this.getUserReport();
          }
        },
        (err) => {
          this.isLoaded = false;
          this.userReportDetail = false;
        }
      );
    }
  }

  //convert image to binary
  handleFileSelect(evt) {
    const files = evt.target.files;
    const file = files[0];

    if (files && file) {
      const reader = new FileReader();
      reader.onload = this._handleReaderLoaded.bind(this);
      reader.readAsBinaryString(file);
    }
  }
  _handleReaderLoaded(readerEvt) {
    const binaryString = readerEvt.target.result;
    this.base64textString = 'data:image/png;base64,' + btoa(binaryString);
  }

  downloadUrl(url: string) {
    const a: any = document.createElement('a');
    a.href = url;
    a.download = 'commentImg';
    document.body.appendChild(a);
    a.style = 'display: none';
    a.click();
    a.remove();
  }

  openResult(content) {
    this.modalService.open(content, {
      centered: true,
      size: 'lg',
      scrollable: false,
      windowClass: 'custom-modal-content',
    });
  }

  exitFromScoreCard(data) {
    this.modalService.dismissAll();
  }
}
