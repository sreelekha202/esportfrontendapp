import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {
  TournamentService,
  UtilsService,
} from '../../../../core/service';
import { TranslateService } from '@ngx-translate/core';

import {
  Validators,
  FormGroup,
  FormBuilder,
  AbstractControl,
  FormControl,
} from '@angular/forms';
import { EsportsConstantsService, EsportsToastService } from 'esports'

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: [
    './edit.component.scss',
    '../tournament-management.component.scss',
  ],
})
export class EditComponent implements OnInit {
  @Input() tournament;
  @Input() minParticipant;
  @Output() response = new EventEmitter<any>();

  checkInTimeOptions = EsportsConstantsService.checkInTimeOptions;
  tournamentForm: FormGroup;
  minStartDateValue = this.setMinDate();
  maximumParticipants = 16384;
  timeoutId = null;
  condition_active = 'description';
  isProcessing: boolean = false;
  editorConfig = {};

  constructor(
    private fb: FormBuilder,
    private tournamentService: TournamentService,
    private eSportsToastService: EsportsToastService,
    private utilsService: UtilsService,
    private translateService: TranslateService
  ) {}

  ngOnInit(): void {
    this.setMaxParticipantLimit();
    this.tournamentForm = this.fb.group(
      {
        name: ['', Validators.required],
        organiserName: [],
        maxParticipants: [
          '',
          Validators.compose([
            Validators.required,
            Validators.min(2),
            Validators.max(this.maximumParticipants),
          ]),
        ],
        startDate: ['', Validators.required],
        startTime: ['', Validators.required],
        participantType: ['', Validators.required],
        isCheckInRequired: [false],
        description: [''],
        rules: [''],
      },
      { validator: this.formValidator.bind(this) }
    );
    this.setEditForm();

    this.editorConfig = {
      editable: true,
      spellcheck: true,
      height: '100px',
      width: 'auto',
      minWidth: '0',
      translate: 'yes',
      enableToolbar: true,
      showToolbar: true,
      defaultParagraphSeparator: '',
      defaultFontName: '',
      defaultFontSize: '',
      fonts: [
        { class: 'arial', name: 'Arial' },
        { class: 'times-new-roman', name: 'Times New Roman' },
        { class: 'calibri', name: 'Calibri' },
        { class: 'comic-sans-ms', name: 'Comic Sans MS' },
      ],
      customClasses: [
        {
          name: 'quote',
          class: 'quote',
        },
        {
          name: 'redText',
          class: 'redText',
        },
        {
          name: 'titleText',
          class: 'titleText',
          tag: 'h1',
        },
      ],
      sanitize: true,
      toolbarPosition: 'top',
      toolbarHiddenButtons: [['bold', 'italic'], ['fontSize']],
    };

  }

  setMinDate() {
    const date = new Date();
    const year: any = date.getFullYear();
    let month: any = date.getMonth() + 1;
    month = month < 10 ? '0' + month : month;
    let day: any = date.getDate();
    day = day < 10 ? '0' + day : day;
    const d = `${year}-${month}-${day}`;
    return d;
  }

  setEditForm() {
    const date = new Date(this.tournament.startDate);
    const formatToTwoDigit = (number) => {
      if (number < 10) {
        return '0' + number;
      }
      return number;
    };
    const setStartCheckedInDate = (startDate, checkInStartDate) => {
      const d1 = new Date(startDate).getTime();
      const d2 = new Date(checkInStartDate).getTime();
      const hour = Math.floor((d1 - d2) / (3600 * 1000));
      const minute = Math.floor((d1 - d2) / (60 * 1000)) - hour * 60;
      const value = EsportsConstantsService.checkInTimeOptions.find((item) => {
        return item.value.minute == minute && item.value.hour == hour;
      });
      this.tournamentForm.addControl('checkInStartDate', new FormControl(''));
      this.tournament.checkInStartDate = value.name;
    };

    if (this.tournament.isCheckInRequired)
      setStartCheckedInDate(
        this.tournament.startDate,
        this.tournament.checkInStartDate
      );

    const removeParticipantLimit = ['swiss_safeis', 'ladder'].includes(this.tournament?.bracketType);
    if (removeParticipantLimit) 
      this.tournamentForm.get("maxParticipants").clearValidators();
      
    this.tournamentForm.patchValue({
      ...this.tournament,
      organiserName: this.tournament.organizerDetail.fullName,
      startDate: `${date.getFullYear()}-${formatToTwoDigit(
        date.getMonth() + 1
      )}-${formatToTwoDigit(date.getDate())}`,
      startTime: {
        hour: date.getHours(),
        minute: date.getMinutes(),
        second: 0,
      },
    });

    if (this.tournament?.isSeeded) {
      this.tournamentForm.clearValidators();
    }
    if(date<new Date()){
      this.tournamentForm.controls['startDate'].disable();
      this.tournamentForm.controls['startTime'].disable();
      this.tournamentForm.clearValidators();
     }
  }

  setMaxParticipantLimit() {
    switch (this.tournament.bracketType) {
      case 'single':
        this.maximumParticipants = 16384;
        break;
      case 'double':
        this.maximumParticipants = 16384;
        break;
      case 'round_robin':
        this.maximumParticipants = 128;
        break;
      case 'battle_royale':
        this.maximumParticipants = 16384;
        break;
    }
  }

  formValidator(c: AbstractControl): { [key: string]: boolean } {
    const d1 = new Date();
    const d2 = new Date(c.get('startDate').value);
    const d4 = new Date(this?.tournament?.endDate);

    d2.setHours(
      c.get('startTime').value.hour,
      c.get('startTime').value.minute,
      0
    );
    if (d1 > d2) {
      return { invalidTime: true };
    }

    if (d2.getTime() > d4.getTime()) {
      return { hasStartDtGtEndDt : true }
    }

    if ((d2.getTime() - d1.getTime())/(1000 * 60) < 5) {
      return {
        hasStartDtError : true 
      }
    }

    if (c.get('isCheckInRequired').value) {
      const optionValue = EsportsConstantsService.checkInTimeOptions.find((item) => {
        return item.name == c.get('checkInStartDate').value;
      });
      const d3 = new Date(d2);
      d3.setHours(
        d2.getHours() - optionValue?.value?.hour,
        d2.getMinutes() - optionValue?.value?.minute
      );
      if (d3.getTime() < d1.getTime()) {
        return { invalidCheckedIn: true };
      }
    }
  }

  submit = async () => {
    try {
      const value = this.tournamentForm.value;
      const startDate = new Date(this.tournament.startDate);

      if (this.tournament?.isSeeded || startDate < new Date()) {
         delete value.name;
         delete value.organiserName;
         delete value.maxParticipants
         delete value.startDate;
         delete value.startTime;
         delete value.participantType;
         delete value.isCheckInRequired;
         delete value.checkInStartDate;
      } else {
        if (this.tournamentForm.invalid ) {
          this.tournamentForm.markAllAsTouched();
          return;
        }

        if (this.tournament?.isFinished) {
          this.eSportsToastService.showInfo(
            this.translateService.instant(
              'MANAGE_TOURNAMENT.EDIT.EDIT_FINISHED_TOURNAMENT'
            )
          );
          return;
        }

        const d1 = new Date();
        const d2 = new Date(value.startDate);
        d2.setHours(value.startTime.hour, value.startTime.minute, 0);
        if (d1 > d2) {
          this.tournamentForm.setErrors({ invalid: true });
          return;
        }
        const { hour, minute } = value.startTime;
        value.startTime = this.utilsService.convertTimeIntoString(
          (hour < 10 ? '0' + hour : hour) +
            ':' +
            (minute < 10 ? '0' + minute : minute) +
            ''
        );
        value.startDate = d2;
        if (value.isCheckInRequired) {
          const optionValue = EsportsConstantsService.checkInTimeOptions.find((item) => {
            return item.name == value.checkInStartDate;
          });
          const d3 = new Date(d2);
          d3.setHours(
            d2.getHours() - optionValue?.value?.hour,
            d2.getMinutes() - optionValue?.value?.minute
          );
          value.checkInStartDate = d3;
        }
      }

      this.isProcessing = true;
      const response = await this.tournamentService
        .updateTournament(value, this.tournament._id)
        .toPromise();
      this.isProcessing = false;
      this.eSportsToastService.showSuccess(response?.message);
      this.response.emit({ refresh: true, active: 1 });
    } catch (error) {
      this.isProcessing = false;
      this.eSportsToastService.showError(error?.error?.message || error?.message);
    }
  };

  cancel() {
    this.response.emit({ refresh: false, active: 1 });
  }
}
