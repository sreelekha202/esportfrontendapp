import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { EsportsNotificationsService, EsportsToastService } from 'esports';
import { finalize } from 'rxjs/operators';
import { environment } from '../../../../../environments/environment';

const API = environment.apiEndPoint;
@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss'],
})
export class NotificationComponent implements OnInit {
 
  notificationsTypeArray = [
    {
      title: 'Email Notification',
      value: 0,
    },
    {
      title: 'Push Notification',
      value: 1,
    },
  ];
  selectedNotification: any;
  @Input() tournamentId: any;
  @Input() tournamentName: any;
  @Input() tournamentUrl: any;
  isLoading: boolean = false;
  constructor() {}

  ngOnInit(): void {}
  
  selectNotificatonType(item) {
    this.selectedNotification = item;
  }
}