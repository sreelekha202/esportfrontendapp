import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import {
  EsportsNotificationsService,
  EsportsToastService,
} from 'esports';
import { finalize } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';
import { environment } from '../../../../../../environments/environment';
const API = environment.apiEndPoint;

@Component({
  selector: 'app-push-notification',
  templateUrl: './push-notification.component.html',
  styleUrls: ['./push-notification.component.scss']
})
export class PushNotificationComponent implements OnInit {
  @Input() tournamentId: any;
  @Input() tournamentName: any;
  @Input() tournamentUrl: any;

  participantTypeArray = [
    {
      text: this.translateService.instant(
        'ADMIN.USER_NOTIFICATION.LABEL.APPROVED'
      ),
      value: 'approved',
    },
    {
      text: this.translateService.instant(
        'ADMIN.USER_NOTIFICATION.LABEL.PENDING'
      ),
      value: 'pending',
    },
    {
      text: this.translateService.instant(
        'ADMIN.USER_NOTIFICATION.LABEL.REJECTED'
      ),
      value: 'rejected',
    },
  ];
  selectedParticipantType;
  pushNotificationForm = this.fb.group({
    message: ['', Validators.required],
    title: ['', Validators.required],
    type: ['', Validators.required],
  });
  isLoading: boolean = false;

  constructor(
    private fb: FormBuilder,
    private userNotificationsService: EsportsNotificationsService,
    private eSportsToastService: EsportsToastService,
    private translateService: TranslateService
  ) { }

  ngOnInit(): void {}
  updatePush(form) {
    if (!this.pushNotificationForm.valid) {
      return;
    }
    const inputData = {
      message: this.pushNotificationForm.value.message,
      title: this.pushNotificationForm.value.title,
      tournamentId: this.tournamentId,
      participantType: this.pushNotificationForm.value.type,
      userSegment: 'TOURNAMENT_PLAYERS_BY_ID',
    };
    this.isLoading = true;
    this.userNotificationsService
      .sendPushNotifications(API, inputData)
      .pipe(
        finalize(() => {
          this.isLoading = false;
        })
      )
      .subscribe(
        (res: any) => {
          if (res) {
            this.eSportsToastService.showSuccess(res?.message);
          }
        },
        (err: any) => {
          this.eSportsToastService.showError(
            err?.error?.message || err?.message
          );
        }
      );
  }

  selectParticipantType(type) {
    this.selectedParticipantType = type;
    this.pushNotificationForm.controls['type'].setValue(type.value);
  }

}
