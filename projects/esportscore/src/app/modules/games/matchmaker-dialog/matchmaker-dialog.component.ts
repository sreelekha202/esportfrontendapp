import { EsportsChatService } from 'esports';
import { Optional, Component, OnInit, OnDestroy } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { UserPreferenceService } from '../../../core/service';
import { Router } from '@angular/router';
import { AuthServices } from '../../../core/service';
import { IUser, EsportsUserService, EsportsLanguageService, EsportsToastService } from 'esports';
import { Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-matchmaker-dialog',
  templateUrl: './matchmaker-dialog.component.html',
  styleUrls: ['./matchmaker-dialog.component.scss'],
})
export class MatchmakerDialogComponent implements OnInit, OnDestroy {
  profile = false;
  progressValue = 0;
  progressBar = false;
  continue = true;
  notfound = false;
  matchMakingBanner = 'assets/images/GamesCards/matchmaking.png';

  matchMakerProfile = {
    id: null,
    image: 'assets/images/GamesCards/matchmaking.png',
    name: 'Ikhwan Mohammed',
    location: 'Riyadh, Saudi Arabia',
    matchName: 'FIFA21',
  };

  gameId: any = null;
  gameName: any = null;
  gamePlatforms: any;
  user: IUser;
  showLoader = true;
  isPlatform = true;
  selectedPlatform: any = null;

  private currenUser: IUser;
  private isUserLoggedIn = false;
  private currentUserId: String = '';
  matchdetails: any;
  windowposition: String = 'chat_window chat_window_right_game_drawer';

  userSubscription: Subscription;

  constructor(
    public dialogRef: MatDialogRef<MatchmakerDialogComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) public data: any,
    private translateService: TranslateService,
    public language: EsportsLanguageService,
    private userService: EsportsUserService,
    private authService: AuthServices,
    private router: Router,
    public eSportsToastService: EsportsToastService,
    private userpreferenceService: UserPreferenceService,
    private chatService: EsportsChatService
  ) {}

  ngOnInit(): void {
    this.gameId = this.data._id;
    this.gameName = this.data.name;
    this.gamePlatforms = [];
    if (this.data.platform.length == 0) {
      this.isPlatform = false;
    }
    this.getCurrentUserDetails();
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.currenUser = data;
        this.isUserLoggedIn = !!this.currenUser;
        this.currentUserId = this.currenUser._id;
      } else {
      }
    });
  }

  ngOnDestroy() {
    this.userSubscription?.unsubscribe();
  }

  getCurrentUserDetails() {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.user = data;
      } else {
        this.authService.redirectUrl = '/games';
        this.eSportsToastService.showError(
          this.translateService.instant('TOURNAMENT.TOURNAMENT.PLEASE_LOGIN')
        );
        // Navigate to the login page with extras
        this.router.navigate(['/user/phone-login']);
        this.closeDialog();
      }
    });
  }

  matchMaking() {
    if (this.user) {
      let counter = 0;
      const timer = setInterval(() => {
        this.progressValue = counter;
        if (counter < 90) {
          counter += 10;
        }
      }, 100);
      this.userpreferenceService
        .matchMaking(this.gameId, this.selectedPlatform)
        .subscribe(
          (res: any) => {
            if (res.data != null) {
              this.matchMakerProfile = {
                id: res.data._id,
                image:
                  res.data.profilePicture == ''
                    ? './assets/images/Leaderboard/leader-board-card-image.png'
                    : res.data.profilePicture,
                name: res.data.fullName,
                location: '',
                matchName: this.gameName,
              };
              if (res.data.state != null) {
                this.matchMakerProfile.location += res.data.state;
              }
              if (res.data.country != null) {
                if (res.data.state != null) {
                  this.matchMakerProfile.location += ', ' + res.data.country;
                } else {
                  this.matchMakerProfile.location += res.data.country;
                }
              }

              if (timer) {
                clearInterval(timer);
              }
              this.progressValue = 100;
              this.progressBar = false;
              this.profile = true;
            } else {
              if (timer) {
                clearInterval(timer);
              }
              this.progressBar = false;
              this.notfound = true;
            }
          },
          (err) => {
            this.showLoader = false;
          }
        );
      // call follow API
    }
  }

  closeDialog(data = null) {
    this.dialogRef.close(data);

    if (!(this.matchMakerProfile == null)) {
      let matchid = this.matchMakerProfile.id + '-' + this.currentUserId;
      this.matchdetails = {
        matchid: matchid,
        image: this.matchMakerProfile.image,
        matchName: this.matchMakerProfile.matchName,
        name: this.matchMakerProfile.name,
        gameid: this.gameId,
      };
      this.chatService.setWindowPos(this.windowposition);
      this.chatService.setCurrentMatch(this.matchdetails);
      this.chatService.setTypeOfChat('game');
      //this.chatService?.getAllMatches(this.currentUserId, 1, 30);
      this.chatService.setChatStatus(true);
      this.chatService.setTextButtonStatus(true);
      if (this.chatService.getChatStatus() == false) {
        this.chatService?.disconnectMatch(matchid, 'game');
      }
    }
  }

  selectPlatform(id) {
    this.selectedPlatform = id;
  }

  onContinue(selectedPlatform = null) {
    this.selectedPlatform = selectedPlatform;
    this.progressBar = true;
    this.continue = false;
    this.matchMaking();
  }
}
