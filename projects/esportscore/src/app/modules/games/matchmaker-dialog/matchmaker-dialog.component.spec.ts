import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MatchmakerDialogComponent } from './matchmaker-dialog.component';

describe('MatchmakerDialogComponent', () => {
  let component: MatchmakerDialogComponent;
  let fixture: ComponentFixture<MatchmakerDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MatchmakerDialogComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MatchmakerDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
