import { Component, OnDestroy, OnInit } from '@angular/core';
import { GameService } from '../../core/service';
import { MatDialog } from '@angular/material/dialog';
import { MatchmakerDialogComponent } from './matchmaker-dialog/matchmaker-dialog.component';
import { IUser, EsportsUserService, EsportsChatService } from 'esports';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import {
  AppHtmlMatchmakingRoutes,
  AppHtmlRoutes,
} from '../../app-routing.model';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-games',
  templateUrl: './games.component.html',
  styleUrls: ['./games.component.scss'],
})
export class GamesComponent implements OnInit, OnDestroy {
  AppHtmlRoutes = AppHtmlRoutes;
  AppHtmlMatchmakingRoutes = AppHtmlMatchmakingRoutes;

  API = environment.apiEndPoint;
  TOKEN = environment.currentToken;

  gameList = [];

  windowposition = 'chat_window chat_window_right_drawer';
  currenUser: IUser;
  isUserLoggedIn = false;
  currentUserId = '';
  currentUserName = '';
  matchdetails: any;

  userSubscription: Subscription;
  gameSearchText = '';

  constructor(
    private gameService: GameService,
    public gamesMatchmaking: MatDialog,
    private chatService: EsportsChatService,
    private userService: EsportsUserService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.getGames();
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.currenUser = data;
        this.isUserLoggedIn = !!this.currenUser;
        this.currentUserId = this.currenUser._id;
      } else {
      }
    });

  }

  ngOnDestroy() {
    this.userSubscription?.unsubscribe();
  }

  getGames() {
    const encodeUrl = `?query=${encodeURIComponent(
      JSON.stringify({
        isTournamentAllowed: true,
        searchBy: this.gameSearchText,
      })
    )}`;
    this.gameService.getAllGames(encodeUrl).subscribe(
      (res) => {
        this.gameList = res.data;
      },
      (err) => {}
    );
  }
  searchByValueFilter(event) {
    if (
      (this.gameSearchText != '' && event.target.value.trim() == '') ||
      (event.target.value.trim() != '' &&
        this.gameSearchText != event.target.value.trim())
    ) {
      this.gameSearchText = event.target.value.trim();
      this.getGames();
    }
  }
  openDialog(game) {
    const dialogRef = this.gamesMatchmaking.open(MatchmakerDialogComponent, {
      data: game,
      panelClass: 'matchMaking',
      disableClose: true,
      hasBackdrop: false,
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (this.currenUser) {
        this.router.navigateByUrl('/tournament/info');
      }
    });
  }
}
