import { Component, OnInit } from '@angular/core';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'all-seasons',
  templateUrl: './all-season.component.html',
  styleUrls: ['./all-season.component.scss'],
})
export class AllSeasonComponent implements OnInit {
  API = environment.apiEndPoint;
  TOKEN = environment.currentToken;

  constructor() {}

  ngOnInit(): void {}
}
