import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { GamesComponent } from './games.component';
import { MatchSeasonComponent } from './match-season/match-season.component';
import { GameLobbyComponent } from './game-lobby/game-lobby.component';
import { AllSeasonComponent } from './all-season/all-season.component';

const routes: Routes = [
  {
    path: '',
    component: GamesComponent,
  },
  {
    path: 'match-season/:seasonId',
    component: MatchSeasonComponent,
  },
  {
    path: 'all-season',
    component: AllSeasonComponent,
  },
  {
    path: 'lobby',
    component: GameLobbyComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GamesRoutingModule {}
