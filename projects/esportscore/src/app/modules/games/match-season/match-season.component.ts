import { Component, OnInit } from '@angular/core';
import { Location, formatDate } from '@angular/common';
import {
  EsportsParticipantService,
  EsportsChatService,
  EsportsToastService,
  EsportsGameService,
  EsportsTournamentService,
} from 'esports';
import { environment } from '../../../../environments/environment';
import { MatDialog } from '@angular/material/dialog';
import {
  LadderPopupComponent,
  LadderPopupComponentData,
} from '../../../shared/popups/ladder-popup/ladder-popup.component';
import { Observable, Subscription } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { formatDistanceToNowStrict } from 'date-fns';

@Component({
  selector: 'app-match-season',
  templateUrl: './match-season.component.html',
  styleUrls: ['./match-season.component.scss'],
})
export class MatchSeasonComponent implements OnInit {
  matchSeason = null;
  gameInfo = null;
  tableData = [];
  API = environment.apiEndPoint;
  page = 1;
  seasonJoinStatus = null;
  matchFoundListener: Subscription;
  seasonId: string = null;
  constructor(
    private location: Location,
    private gameService: EsportsGameService,
    private matDialog: MatDialog,
    private router: Router,
    private participantService: EsportsParticipantService,
    private chatService: EsportsChatService,
    private eSportsToastService: EsportsToastService,
    private activatedRoute: ActivatedRoute,
    private translateService: TranslateService,
    private tournamentService: EsportsTournamentService
  ) {}
  ngOnInit(): void {
    // MOCK MATCH SEASON
    if (this.activatedRoute.snapshot.params.seasonId) {
      this.seasonId = this.activatedRoute.snapshot.params.seasonId;
      this.fetchSeasonDetails();
      this.fetchLeaderboardData();
    }

    if (this.activatedRoute.snapshot.queryParamMap.get('playnow')) {
      this.openPlayNowPopup();
      this.router
        .navigateByUrl('/games', { skipLocationChange: true })
        .then(() => {
          this.router.navigate(['/games/match-season/', this.seasonId]);
        });
    }

    this.matchSeason = {};
    this.getSeasonJoinStatus();
    this.getRecentMatches();

    // MOCK GAME INFO
    this.gameInfo = {};

    // MOCK TABLE DATA
    this.tableData = [
      {
        sn: 1,
        image:
          'https://i114.fastpic.ru/big/2021/0331/15/a8494278c27be0b22a3ff182fdbc5115.png',
        name: 'Aazim',
        games: '2,345',
        wins: '2,045',
        points: '231',
      },
      {
        sn: 2,
        image:
          'https://i114.fastpic.ru/big/2021/0331/15/a8494278c27be0b22a3ff182fdbc5115.png',
        name: 'Wali-ud-din',
        games: '3,345',
        wins: '2,345',
        points: '1,345',
      },
      {
        sn: 3,
        image:
          'https://i114.fastpic.ru/big/2021/0331/45/086727468fa05ed20f7fb6a337dbe545.png',
        name: 'Zahir',
        games: '2,145',
        wins: '1,345',
        points: '534',
      },
      {
        sn: 4,
        image:
          'https://i114.fastpic.ru/big/2021/0331/15/a8494278c27be0b22a3ff182fdbc5115.png',
        name: 'Mahad',
        games: '2,005',
        wins: '1,545',
        points: '432',
      },
      {
        sn: 5,
        image:
          'https://i114.fastpic.ru/big/2021/0331/15/a8494278c27be0b22a3ff182fdbc5115.png',
        name: 'Aazim',
        games: '2,345',
        wins: '2,045',
        points: '231',
      },
      {
        sn: 6,
        image:
          'https://i114.fastpic.ru/big/2021/0331/15/a8494278c27be0b22a3ff182fdbc5115.png',
        name: 'Wali-ud-din',
        games: '3,345',
        wins: '2,345',
        points: '1,345',
      },
      {
        sn: 7,
        image:
          'https://i114.fastpic.ru/big/2021/0331/45/086727468fa05ed20f7fb6a337dbe545.png',
        name: 'Zahir',
        games: '2,145',
        wins: '1,345',
        points: '534',
      },
      {
        sn: 8,
        image:
          'https://i114.fastpic.ru/big/2021/0331/15/a8494278c27be0b22a3ff182fdbc5115.png',
        name: 'Mahad',
        games: '2,005',
        wins: '1,545',
        points: '432',
      },
      {
        sn: 9,
        image:
          'https://i114.fastpic.ru/big/2021/0331/15/a8494278c27be0b22a3ff182fdbc5115.png',
        name: 'Aazim',
        games: '2,345',
        wins: '2,045',
        points: '231',
      },
      {
        sn: 10,
        image:
          'https://i114.fastpic.ru/big/2021/0331/15/a8494278c27be0b22a3ff182fdbc5115.png',
        name: 'Wali-ud-din',
        games: '3,345',
        wins: '2,345',
        points: '1,345',
      },
    ];
    this.tableData = [];
  }

  fetchLeaderboardData = async () => {
    try {
      const parms = {
        id: this.seasonId,
        page: this.page,
      };
      const response = await this.tournamentService
        .getLaderStanding(this.API, parms)
        .toPromise();
      const standings = response.data.standing.docs.map((standing) => {
        return {
          sn: standing.rank,
          image: standing.logo,
          name: standing.teamName,
          games: standing.matchLoss + standing.matchTie + standing.matchWin,
          wins: standing.matchWin,
          points: standing.points,
        };
      });
      this.tableData = standings;
    } catch (error) {}
  };

  getRecentMatches() {
    const params = {
      query: JSON.stringify({
        seasonId: this.seasonId,
        limit: 5,
        page: 1,
      }),
    };
    this.gameService.fetchRecentSeasonMatches(this.API, params).subscribe(
      (res) => {
        this.gameInfo.matches = res.data.docs;
      },
      (err) => {}
    );
  }

  goBack() {
    this.location.back();
  }

  async getSeasonJoinStatus() {
    const response: any = await this.participantService
      .getSeasonJoinStatus(environment.apiEndPoint, {
        seasonId: this.seasonId,
      })
      .toPromise();
    this.seasonJoinStatus = response?.data;
  }

  playNow() {
    if (this.seasonJoinStatus?.isRegistered) {
      this.openPlayNowPopup();
    } else {
      if (this.matchSeason.participantType === 'team') {
        this.router.navigate([
          `/match-making/team-registration/${this.matchSeason?.slug}-${this.matchSeason?.id}`,
        ]);
      } else {
        this.router.navigate([
          `/match-making/${this.matchSeason?.slug}-${this.matchSeason?.id}`,
        ]);
      }
    }
  }

  openPlayNowPopup() {
    this.chatService.connectSocket();
    let popupData: LadderPopupComponentData = {
      isShowLoading: true,
      payload: { leftTime: 180 },
    };

    const loadDialogRef = this.matDialog.open(LadderPopupComponent, {
      data: popupData,
      disableClose: true,
    });

    this.chatService.findSeasonMatch({
      seasonId: this.seasonId,
    });

    const successObservable = new Observable((subscriber) => {
      this.chatService.listenSeasonMatchFound(subscriber);
    });

    let matchFoundSubscription = successObservable.subscribe((data) => {
      if (data) {
        loadDialogRef.close(true);
        this.eSportsToastService.showSuccess(
          this.translateService.instant('LADDER.MATCH_FOUND')
        );
        this.router.navigate(['games/lobby'], {
          queryParams: { matchId: data['data']['newMatch']['_id'] },
        });
      }
    });

    const errorObservable = new Observable((subscriber) => {
      this.chatService.listenFindSeasonMatchError(subscriber);
    });

    let findSeasonMatchErrorSubscription = errorObservable.subscribe(
      (error) => {
        loadDialogRef.close(true);
        if (error['errorCode']) {
          this.showError(error['errorCode']);
        } else {
          this.eSportsToastService.showError(
            this.translateService.instant('LADDER.FIND_MATCH_ERROR')
          );
        }
      }
    );

    loadDialogRef.afterClosed().subscribe((loadCloseData) => {
      this.chatService.removeFromQueue('');
      matchFoundSubscription.unsubscribe();
      findSeasonMatchErrorSubscription.unsubscribe();
      if (loadCloseData == 'timeOut') {
        const notFoundPopupRef = this.matDialog.open(LadderPopupComponent, {
          data: { isShowNotFoundSeason: true },
        });
        notFoundPopupRef.afterClosed().subscribe((action) => {
          if (action == 'searchAgain') {
            this.openPlayNowPopup();
          }
        });
      }
    });
  }

  fetchSeasonDetails() {
    this.gameService
      .fetchSeasonDetails(this.API, this.seasonId)
      .subscribe((res) => {
        const data = res['data']['data'];
        let text;
        const endDate = new Date(data.endDate);
        const startDate = new Date(data.startDate);
        const currentDate = new Date();
        if (startDate > currentDate) {
          text = `Starts ${formatDistanceToNowStrict(startDate, {
            addSuffix: true,
            unit: 'day',
          })}`;
        } else if (endDate > currentDate) {
          text = `Ends ${formatDistanceToNowStrict(endDate, {
            addSuffix: true,
            unit: 'day',
          })}`;
        } else {
          text = 'Season ended';
        }

        Object.assign(this.matchSeason, {
          title: data?.name,
          text: text,
          image: data?.image,
          id: data?._id,
          participantType: data?.participantType,
          slug: data?.slug,
        });

        Object.assign(this.gameInfo, {
          image: data?.game?.image,
          icon: data?.game?.icon,
          details: {
            platform: data?.platform?.map((ele) => ele?.name).join(', '),
            duration: `${data?.duration} days`,
            endDate: formatDate(endDate, 'dd MMM yyyy', 'en'),
            totalPlayers: data?.participantCount,
          },
        });
      });
  }

  showError(errorCode) {
    let errorMessage;

    switch (errorCode) {
      case 'ACTIVE_MATCH_ERROR':
        errorMessage =
          this.translateService.instant('LADDER.MATCH_NOT_FOUND_PART1') +
          1 +
          this.translateService.instant('LADDER.MATCH_NOT_FOUND_PART2');
        break;
      case 'ON_COOLDOWN_ERROR':
        errorMessage = this.translateService.instant(
          'LADDER.MATCH_JUST_FINISHED'
        );
        break;
      default:
        errorMessage = this.translateService.instant('LADDER.FIND_MATCH_ERROR');
        break;
    }

    this.eSportsToastService.showError(errorMessage);
  }
}
