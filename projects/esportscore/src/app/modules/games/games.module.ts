import { NgModule } from '@angular/core';

import { SharedModule } from '../../shared/modules/shared.module';
import { CoreModule } from '../../core/core.module';

import { GamesComponent } from './games.component';
import { GamesRoutingModule } from './games-routing.module';

import { GameLobbyComponent } from './game-lobby/game-lobby.component';
import { MatchmakerDialogComponent } from './matchmaker-dialog/matchmaker-dialog.component';
import { MatchSeasonComponent } from './match-season/match-season.component';
import { UploadImageModule } from '../../shared/components/upload-image/upload-image.module';
import { EsportsSeasonModule } from 'esports';
import { environment } from '../../../environments/environment';
import { AllSeasonComponent } from './all-season/all-season.component';

@NgModule({
  declarations: [
    GameLobbyComponent,
    GamesComponent,
    MatchmakerDialogComponent,
    MatchSeasonComponent,
    AllSeasonComponent
  ],
  imports: [
    SharedModule,
    CoreModule,
    GamesRoutingModule,
    UploadImageModule,
    EsportsSeasonModule.forRoot(environment),
  ],
  exports: [],
})
export class GamesModule {}
