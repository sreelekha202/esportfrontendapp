import { TranslateService } from '@ngx-translate/core';
import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

import { EsportsLanguageService } from 'esports';

@Component({
  selector: 'app-mob-privacy-policy',
  templateUrl: './mob-privacy-policy.component.html',
  styleUrls: ['./mob-privacy-policy.component.scss'],
})
export class MobPrivacyPolicyComponent implements OnInit {
  arebicText = false;
  public currentLang: string;
  currLanguage = 'english';
  constructor(
    private location: Location,
    public translate: TranslateService,
    public language: EsportsLanguageService
  ) {}

  ngOnInit(): void {
    this.language.language.subscribe((lang) => {
      if (lang) {
        this.arebicText = lang === 'ms' ? true : false;
      }
    });
  }

  goBack() {
    this.location.back();
  }
}
