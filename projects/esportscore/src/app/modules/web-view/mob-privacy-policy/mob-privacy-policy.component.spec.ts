import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MobPrivacyPolicyComponent } from './mob-privacy-policy.component';

describe('MobPrivacyPolicyComponent', () => {
  let component: MobPrivacyPolicyComponent;
  let fixture: ComponentFixture<MobPrivacyPolicyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MobPrivacyPolicyComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MobPrivacyPolicyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
