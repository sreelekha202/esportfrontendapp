import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MobTermsOfUseComponent } from './mob-terms-of-use.component';

describe('MobTermsOfUseComponent', () => {
  let component: MobTermsOfUseComponent;
  let fixture: ComponentFixture<MobTermsOfUseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MobTermsOfUseComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MobTermsOfUseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
