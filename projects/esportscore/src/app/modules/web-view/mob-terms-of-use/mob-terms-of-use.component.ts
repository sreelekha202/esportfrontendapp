import { TranslateService } from '@ngx-translate/core';
import { Component, OnInit } from '@angular/core';
import { EsportsLanguageService } from 'esports'

@Component({
  selector: 'app-mob-terms-of-use',
  templateUrl: './mob-terms-of-use.component.html',
  styleUrls: ['./mob-terms-of-use.component.scss'],
})
export class MobTermsOfUseComponent implements OnInit {
  arebicText = false;
  public currentLang: string;
  currLanguage = 'english';
  constructor(
    public translate: TranslateService,
    public language: EsportsLanguageService
  ) {}

  ngOnInit(): void {
    this.language.language.subscribe((lang) => {
      if (lang) {
        this.arebicText = lang === 'ms' ? true : false;
      }
    });
  }
}
