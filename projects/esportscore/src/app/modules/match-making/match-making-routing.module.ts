import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MatchmakingComponent } from './match-making.component';

import { GetMatchComponent } from './pages/get-match/get-match.component';
import { JoinSeasonParticipantComponent } from './pages/join-participant/join-participant.component';
import { TeamGameComponent } from './pages/team-game/team-game.component';
import { TeamMembersComponent } from './pages/team-members/team-members.component';

import { AuthGuard } from '../../shared/guard/auth.guard';
import { TeamSeasonRegistrationComponent } from './pages/team-registration/team-registration/team-registration.component';

const routes: Routes = [
  {
    path: '',
    component: MatchmakingComponent,
    children: [
      {
        path: '',
        component: GetMatchComponent,
      },
      {
        path: 'team-game',
        component: TeamGameComponent,
      },
      {
        path: 'team-members',
        component: TeamMembersComponent,
      },
      {
        path: 'team-registration/:id',
        component: TeamSeasonRegistrationComponent,
        canActivate: [AuthGuard],
        data: {
          isRootPage: true,
        },
      },
      {
        path: ':id',
        component: JoinSeasonParticipantComponent,
        canActivate: [AuthGuard],
        data: {
          isRootPage: true,
        },
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MatchmakingRoutingModule {}
