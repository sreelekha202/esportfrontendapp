import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';
import { TournamentService } from '../../../../../core/service';
import { IUser, EsportsUserService, EsportsToastService } from 'esports';

import { environment } from '../../../../../../environments/environment';

const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;

@Component({
  selector: 'app-team-registration',
  templateUrl: './team-registration.component.html',
  styleUrls: ['./team-registration.component.scss'],
})
export class TeamSeasonRegistrationComponent implements OnInit, OnDestroy {
  teamRegistrationActiveStep = 0;
  userSubscription: Subscription;
  user: IUser;
  tournament: any = {};
  teamList = [];
  teamSelected: any = {};
  teamMemberList: any = {};
  isLoaded = false;
  participantRS: string | null;
  showParticipantForm = false;

  constructor(
    private userService: EsportsUserService,
    private tournamentService: TournamentService,
    private activatedRoute: ActivatedRoute,
    private translateService: TranslateService,
    private eSportsToastService: EsportsToastService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.getCurrentUserDetails();
  }

  ngOnDestroy() {
    this.userSubscription?.unsubscribe();
  }

  getCurrentUserDetails() {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.user = data;
        const pId = this.activatedRoute.snapshot.params.pId;

        if (pId) {
          this.tournament.slug = this.router.url.split('/').reverse()[2];
          this.fetchTournamentDetails(this.tournament?.slug);
        } else {
          this.tournament.slug = this.router.url.split('/').reverse()[0];
          this.fetchTournamentDetails(this.tournament?.slug);
        }
      }
    });
  }

  /**
   * Fetch Some tournament Details
   * @param slug
   */
  fetchTournamentDetails = async (slug) => {
    try {
      this.isLoaded = false;
      const query = JSON.stringify({ slug });
      const select = `&select=_id,name,logo,banner,participantType,teamSize,substituteMemberSize,allowSubstituteMember,slug,startDate,tournamentType,isIncludeSponsor,maxParticipants,bracketType,isPaid,regFee,regFeeCurrency`;
      const tournament = await this.tournamentService
        .getSeasonTournaments({ query }, select)
        .toPromise();
      this.tournament = tournament?.data?.length ? tournament.data[0] : null;


      if (!this.tournament) {
        throw new Error(
          this.translateService.instant(
            'VIEW_TOURNAMENT.JOIN_PARTICIPANT.INVALID_TOURNAMENT'
          )
        );
      }
      // const participantId = this.activatedRoute?.snapshot?.params?.pId;
      // const {
      //   data,
      // } = await this.tournamentService.fetchParticipantRegistrationStatus(
      //   this.tournament?._id,
      //   participantId
      // );
      // this.participantRS = data?.type;
      // this.showParticipantForm = [
      //   'join',
      //   'already-registered',
      //   'checked-in',
      //   'already-checked-in',
      // ].includes(this.participantRS);

      const {
        data,
      } = await this.tournamentService.fetchSeasonParticipantRegistrationStatus(
        this.tournament?._id
      );

      if (data?.isRegistered) {
      } else {
        this.showParticipantForm = true;
      }

      this.getMyTeams();

      this.isLoaded = true;
    } catch (error) {
      this.isLoaded = true;
      this.eSportsToastService.showError(error?.statusText || error?.message);
    }
  };

  getMyTeams = async () => {
    const data = await this.userService.getJoinMyTeam(
      API,
      this.tournament?._id
    );
    if (data) {
      data?.data.forEach((control, index) => {
        let totalMemberSize = 0;

        if (this.tournament?.substituteMemberSize > 0) {
          totalMemberSize +=
            this.tournament?.teamSize + this.tournament?.substituteMemberSize;
        } else {
          totalMemberSize = this.tournament?.teamSize;
        }
        if (control.teamSize >= totalMemberSize) {
          this.teamList.push(control);
        }
      });
    }
  };

  async selectedTeam(data) {
    const teamMembersData: any = await this.userService.getTeamMember(
      API,
      data._id
    );

    this.teamSelected = data;
    this.teamMemberList = teamMembersData.data;

    if (teamMembersData.data) {
      this.teamRegistrationActiveStep = 1;
    }

    // let teamMembers = [];

    // teamMembersData.data.forEach((control) => {
    //  let newData = {
    //    name: control.userId.fullName,
    //    phoneNumber: control.userId.phoneNumber,
    //    email: control.userId.email,
    //  };
    //     teamMembers.push(newData);
    // });

    // this.tournamentService.setSelectedTeam({
    //   logo: data.logo,
    //   teamName: data.teamName,
    //   teamMembers: teamMembers,
    // });

    // this.router.navigate(['tournament', this.tournament?.slug, 'join']);
  }
}
