import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectTeammatesComponent } from './select-teammates.component';

describe('SelectTeammatesComponent', () => {
  let component: SelectTeammatesComponent;
  let fixture: ComponentFixture<SelectTeammatesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SelectTeammatesComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectTeammatesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
