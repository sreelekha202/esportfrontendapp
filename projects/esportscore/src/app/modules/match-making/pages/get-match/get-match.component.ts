import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { AppHtmlMatchmakingRoutes } from '../../../../../app/app-routing.model';
import { Location } from '@angular/common';
import { GameService } from '../../../../core/service';

import { environment } from '../../../../../environments/environment';
import { EsportsSeasonService, EsportsToastService } from 'esports';
import { Router } from '@angular/router';

const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;
@Component({
  selector: 'app-get-match',
  templateUrl: './get-match.component.html',
  styleUrls: ['./get-match.component.scss'],
})
export class GetMatchComponent implements OnInit {
  AppHtmlMatchmakingRoutes = AppHtmlMatchmakingRoutes;
  ownerForm: FormGroup;

  gameList = [];
  platforms = [];
  gameTypes = [];
  sendData = {
    game: '',
    platform: '',
    type: '',
  };

  constructor(
    private fb: FormBuilder,
    private location: Location,
    private gameService: GameService,
    private seasonService: EsportsSeasonService,
    public eSportsToastService: EsportsToastService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.getGames();
    this.getPlatform();
    this.ownerForm = this.fb.group({
      game: [''],
      platform: [''],
      type: [''],
    });

    // MOCK GAME TYPES
    this.gameTypes = [
      {
        name: 'MATCHMAKING.GET_MATCH.GAME_TYPE.SINGLE',
        icon: 'assets/icons/match-making/platform/pc.svg',
        type: 'individual',
        isSelected: false,
      },
      {
        name: 'MATCHMAKING.GET_MATCH.GAME_TYPE.TEAM',
        icon: 'assets/icons/match-making/platform/console.svg',
        type: 'team',
        isSelected: false,
      },
    ];
  }

  getGames() {
    const encodeUrl = `?query=${encodeURIComponent(
      JSON.stringify({
        isTournamentAllowed: true,
      })
    )}`;
    this.gameService.getAllGames(encodeUrl).subscribe(
      (res) => {
        this.gameList = res.data;
      },
      (err) => {}
    );
  }

  getPlatform() {
    this.gameService.fetchPlatforms().subscribe(
      (res) => {
        this.platforms = res.data;
      },
      (err) => {}
    );
  }

  onSelectGame(event: any) {
    this.gameList.forEach((game) => {
      game.isSelected = game._id === event;
      if (game.isSelected) {
        this.sendData.game = game._id;
      }
    });
  }

  onSelectPlatform(_id: string) {
    this.platforms.forEach((platform) => {
      platform.isSelected = platform._id === _id;
      if (platform.isSelected) {
        this.sendData.platform = platform._id;
      }
    });
  }

  onSelectType(type: string) {
    this.gameTypes.forEach((gameType) => {
      gameType.isSelected = gameType.type === type;
      if (gameType.isSelected) {
        this.sendData.type = gameType.type;
      }
    });
  }
  close() {
    this.location.back();
  }

  checkSeason() {
    if (this.sendData.game && this.sendData.platform && this.sendData.type) {
      this.seasonService.checkSeasons(API, this.sendData).subscribe(
        (res) => {
          if (res?.data?.data?.season.length > 0) {
            this.eSportsToastService.showSuccess(res?.message);
            if (res?.data?.data?.participant) {
              this.router.navigate(
                ['/games/match-season/' + res?.data?.data?.season[0]?._id],
                { queryParams: { playnow: 'true' } }
              );
            } else {
              if (res?.data?.data?.season[0]?.participantType === 'team') {
                this.router.navigate([
                  `/match-making/team-registration/${res?.data?.data?.season[0]?.slug}-${res?.data?.data?.season[0]?._id}`,
                ]);
              } else {
                this.router.navigate([
                  `/match-making/${res?.data?.data?.season[0]?.slug}-${res?.data?.data?.season[0]?._id}`,
                ]);
              }
            }
          } else {
            this.eSportsToastService.showError(res?.message);
          }
        },
        (err) => {}
      );
    } else {
      this.eSportsToastService.showError('Select game, platform, type.');
    }
  }
}
