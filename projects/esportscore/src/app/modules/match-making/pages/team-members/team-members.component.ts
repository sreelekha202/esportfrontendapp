import { Component, OnInit } from '@angular/core';
import { AppHtmlRoutes } from '../../../../../app/app-routing.model';
import { Location } from '@angular/common'

@Component({
  selector: 'app-team-members',
  templateUrl: './team-members.component.html',
  styleUrls: ['./team-members.component.scss'],
})
export class TeamMembersComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;

  members = [];

  constructor(private location:Location) {}

  ngOnInit(): void {
    // MOCK MEMBERS
    this.members = [
      {
        name: 'Khalid Muhammad',
        photo: 'assets/images/TopPlayers/top-player-01.png',
        isSelected: true,
      },
      {
        name: 'Daniel Montaner',
        photo: 'assets/images/TopPlayers/top-player-02.png',
        isSelected: false,
      },
      {
        name: 'Patrik Lindberg',
        photo: 'assets/images/TopPlayers/top-player-03.png',
        isSelected: false,
      },
      {
        name: 'Christopher Alesund',
        photo: 'assets/images/TopPlayers/top-player-02.png',
        isSelected: false,
      },
      {
        name: 'Jordan Gilbert ',
        photo: 'assets/images/TopPlayers/top-player-03.png',
        isSelected: false,
      },
      {
        name: 'Oscar Torgersen',
        photo: 'assets/images/TopPlayers/top-player-01.png',
        isSelected: false,
      },
      {
        name: 'Fredrik Andersson	',
        photo: 'assets/images/TopPlayers/top-player-03.png',
        isSelected: false,
      },
      {
        name: 'Emil Christensen',
        photo: 'assets/images/TopPlayers/top-player-02.png',
        isSelected: false,
      },
    ];
  }

  onSelectMember(idx: number) {
    this.members = this.members.map((member, i) =>
      idx === i ? { ...member, isSelected: !member.isSelected } : member
    );
  }

  previous()
  {
    this.location.back()
  }
}
