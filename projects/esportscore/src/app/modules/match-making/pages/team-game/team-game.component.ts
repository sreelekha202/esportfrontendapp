import { Component, OnInit } from '@angular/core';
import { AppHtmlMatchmakingRoutes } from '../../../../../app/app-routing.model';
import { Location } from '@angular/common';

@Component({
  selector: 'app-team-game',
  templateUrl: './team-game.component.html',
  styleUrls: ['./team-game.component.scss'],
})
export class TeamGameComponent implements OnInit {
  AppHtmlMatchmakingRoutes = AppHtmlMatchmakingRoutes;

  myTeams = [];

  constructor(private location:Location) {}

  ngOnInit(): void {
    // MOCK MY TEAMS
    this.myTeams = [
      {
        image:
          'https://i114.fastpic.ru/big/2021/0331/9b/ada1e8d938bd705eea267a8f833f5d9b.png',
        name: 'Dark Ninja',
      },
      {
        image:
          'https://i114.fastpic.ru/big/2021/0331/ec/3c0faeb267e60153565ef1804e8931ec.png',
        name: 'Kitsune',
      },
      {
        image:
          'https://i114.fastpic.ru/big/2021/0331/be/1a283f3803ff5888941f89e9aa9094be.png',
        name: 'emuLate',
      },
      {
        image:
          'https://i114.fastpic.ru/big/2021/0331/99/4ef8afe35c9b6dd1a1646df558bca899.png',
        name: 'mTw',
      },
      {
        image:
          'https://i114.fastpic.ru/big/2021/0331/4b/c86e08b34318507541619b88204b944b.png',
        name: 'x6tence',
      },
    ];
  }
  previous()
  {
    this.location.back()
  }
}
