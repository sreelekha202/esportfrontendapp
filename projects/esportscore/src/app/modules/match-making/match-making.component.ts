import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-match-making',
  templateUrl: './match-making.component.html',
  styleUrls: ['./match-making.component.scss'],
})
export class MatchmakingComponent implements OnInit {
  teamRegistrationActiveStep = 1;

  constructor() {}

  ngOnInit(): void {}

  onChangePage(page: number): void {
    this.teamRegistrationActiveStep = page;
  }
}
