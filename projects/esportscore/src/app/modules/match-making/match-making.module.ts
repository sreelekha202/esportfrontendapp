import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CoreModule } from '../../core/core.module';
import { FormComponentModule } from '../../shared/components/form-component/form-component.module';
import { SharedModule } from '../../shared/modules/shared.module';
import { UploadImageModule } from '../../shared/components/upload-image/upload-image.module';

import { HeaderInfoModule } from '../../shared/components/header-info/header-info.module';
import { MatchmakingComponent } from './match-making.component';
import { MatchmakingRoutingModule } from './match-making-routing.module';

import { GetMatchComponent } from './pages/get-match/get-match.component';
import { TeamGameComponent } from './pages/team-game/team-game.component';
import { TeamMembersComponent } from './pages/team-members/team-members.component';
import { JoinSeasonParticipantComponent } from './pages/join-participant/join-participant.component';
import { TeamSeasonRegistrationComponent } from './pages/team-registration/team-registration/team-registration.component';
import { NewSeasonTeamComponent } from './pages/team-registration/new-team/new-team.component';
import { SelectSeasonTeammatesComponent } from './pages/team-registration/select-teammates/select-teammates.component';

@NgModule({
  declarations: [
    GetMatchComponent,
    JoinSeasonParticipantComponent,
    MatchmakingComponent,
    TeamGameComponent,
    TeamMembersComponent,
    TeamSeasonRegistrationComponent,
    NewSeasonTeamComponent,
    SelectSeasonTeammatesComponent,
  ],
  imports: [
    CommonModule,
    CoreModule,
    FormComponentModule,
    HeaderInfoModule,
    MatchmakingRoutingModule,
    SharedModule,
    UploadImageModule,
  ],
})
export class MatchmakingModule {}
