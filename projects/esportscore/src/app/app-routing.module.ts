import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
// import { MyTeamsViewComponent } from './modules/profile/my-teams-view/my-teams-view.component';

import { AuthGuard } from './shared/guard/auth.guard';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  {
    path: 'home',
    loadChildren: () =>
      import('./modules/home2/home2.module').then((m) => m.Home2Module),
    data: {
      tags: [
        {
          name: 'description',
          content:
            'A gaming platform for all game lovers offering he opportunity to compete in esports tournaments and consume gaming related content to ultimately bring gamers closer to one another.',
        },
        {
          property: 'og:description',
          content:
            'A gaming platform for all game lovers offering he opportunity to compete in esports tournaments and consume gaming related content to ultimately bring gamers closer to one another.',
        },
        {
          property: 'twitter:description',
          content:
            'A gaming platform for all game lovers offering he opportunity to compete in esports tournaments and consume gaming related content to ultimately bring gamers closer to one another.',
        },
        {
          name: 'keywords ',
          content: 'esports , stc play',
        },
        {
          name: 'title',
          content:
            'A gaming platform for all game lovers offering he opportunity to compete in esports tournaments and consume gaming related content to ultimately bring gamers closer to one another.',
        },
      ],
      title: 'stcplay.gg - stc play',
    },
  },
  {
    path: 'monitoring',
    loadChildren: () =>
      import('./modules/monitoring/monitoring.module').then(
        (m) => m.MonitoringModule
      ),
  },
  {
    path: 'leaderboard',
    loadChildren: () =>
      import('./modules/leader-board/leader-board.module').then(
        (m) => m.LeaderBoardModule
      ),
  },
  {
    path: 'my-matches',
    loadChildren: () =>
      import('./modules/my-matches/my-matches.module').then(
        (m) => m.MyMatchesModule
      ),
  },
  {
    path: 'games',
    loadChildren: () =>
      import('./modules/games/games.module').then((m) => m.GamesModule),
  },
  {
    path: 'terms-of-use',
    loadChildren: () =>
      import('./modules/terms-of-use/terms-of-use.module').then(
        (m) => m.TermsOfUseModule
      ),
  },
  {
    path: 'code-of-conduct',
    loadChildren: () =>
      import('./modules/code-of-conduct/code-of-conduct.module').then(
        (m) => m.CodeOfConductModule
      ),
  },
  {
    path: 'create-team',
    loadChildren: () =>
      import('./modules/create-team/create-team.module').then(
        (m) => m.CreateTeamModule
      ),
    canActivate: [AuthGuard],
    data: {
      isRootPage: true,
    },
  },
  // Need to know is it needed, this route is lazy loaded
  // {
  //   path: 'team/:id',
  //   component: MyTeamsViewComponent,
  // },
  {
    path: 'edit-team/:id',
    loadChildren: () =>
      import('./modules/create-team/create-team.module').then(
        (m) => m.CreateTeamModule
      ),
    canActivate: [AuthGuard],
    data: {
      isRootPage: true,
    },
  },
  {
    path: 'manage-team',
    loadChildren: () =>
      import('./modules/manage-team/manage-team.module').then(
        (m) => m.ManageTeamModule
      ),
    canActivate: [AuthGuard],
    data: {
      isRootPage: true,
    },
  },
  {
    path: 'team-registration/:id',
    loadChildren: () =>
      import('./modules/team-registration/team-registration.module').then(
        (m) => m.TeamRegistrationModule
      ),
    canActivate: [AuthGuard],
    data: {
      isRootPage: true,
    },
  },
  {
    path: 'anti-spam-policy',
    loadChildren: () =>
      import('./modules/anti-spam-policy/anti-spam-policy.module').then(
        (m) => m.AntiSpamPolicyModule
      ),
  },
  {
    path: 'about-us',
    loadChildren: () =>
      import('./modules/about-us/about-us.module').then((m) => m.AboutUsModule),
    data: {
      tags: [
        {
          name: 'description',
          content:
            'Stc play is stc’s gaming arm, it is an online hub with different offerings for the gaming community from all levels of gamers and game lovers providing innovative services and platforms to better service their needs stc play aims to be a powerhouse that drives the gaming ecosystem in the MENA region, bringing in a greater dimension and richness to the gaming experience.',
        },
        {
          property: 'og:description',
          content:
            'Stc play is stc’s gaming arm, it is an online hub with different offerings for the gaming community from all levels of gamers and game lovers providing innovative services and platforms to better service their needs stc play aims to be a powerhouse that drives the gaming ecosystem in the MENA region, bringing in a greater dimension and richness to the gaming experience.',
        },
        {
          property: 'twitter:description',
          content:
            'Stc play is stc’s gaming arm, it is an online hub with different offerings for the gaming community from all levels of gamers and game lovers providing innovative services and platforms to better service their needs stc play aims to be a powerhouse that drives the gaming ecosystem in the MENA region, bringing in a greater dimension and richness to the gaming experience.',
        },
        {
          name: 'keywords ',
          content: 'stc play',
        },
        {
          name: 'title',
          content:
            'Stc play is stc’s gaming arm, it is an online hub with different offerings for the gaming community from all levels of gamers and game lovers providing innovative services and platforms to better service their needs stc play aims to be a powerhouse that drives the gaming ecosystem in the MENA region, bringing in a greater dimension and richness to the gaming experience.',
        },
      ],
      title: 'About stc play',
    },
  },
  {
    path: 'tournament',
    loadChildren: () =>
      import('./modules/tournament/tournament.module').then(
        (m) => m.TournamentModule
      ),
    data: {
      tags: [
        {
          name: 'description',
          content: 'Create/Organize, join and check out tournament highlights',
        },
        {
          property: 'og:description',
          content: 'Create/Organize, join and check out tournament highlights',
        },
        {
          property: 'twitter:description',
          content: 'Create/Organize, join and check out tournament highlights',
        },
        {
          name: 'keywords ',
          content: 'esports, tournament',
        },
        {
          name: 'title',
          content: 'Create/Organize, join and check out tournament highlights',
        },
      ],
      title: 'stc play Tournaments',
    },
  },
  {
    path: 'article/create',
    loadChildren: () =>
      import('./modules/article/create-article/create-article.module').then(
        (m) => m.CreateArticleModule
      ),
    data: {
      tags: [
        {
          name: 'title',
          content: 'stc play',
        },
      ],
      title: 'stc play',
    },
  },
  {
    path: 'article/edit/:id',
    loadChildren: () =>
      import('./modules/article/create-article/create-article.module').then(
        (m) => m.CreateArticleModule
      ),
  },
  {
    path: 'article',
    loadChildren: () =>
      import('./modules/article/article.module').then((m) => m.ArticleModule),
    data: {
      tags: [
        {
          name: 'description',
          content:
            'Get the latest news and updates on gaming and esport on stcplay.gg.',
        },
        {
          property: 'og:description',
          content:
            'Get the latest news and updates on gaming and esport on stcplay.gg.',
        },
        {
          property: 'twitter:description',
          content:
            'Get the latest news and updates on gaming and esport on stcplay.gg.',
        },
        {
          name: 'keywords ',
          content: 'esports, news, esportsnews',
        },
        {
          name: 'title',
          content:
            'Get the latest news and updates on gaming and esport on stcplay.gg.',
        },
      ],
      title: 'Gaming News & Articles',
    },
  },
  {
    path: 'user/:pageType',
    loadChildren: () =>
      import('./modules/log-reg/log-reg.module').then((m) => m.LogRegModule),
    data: {
      isRootPage: true,
      tags: [
        {
          name: 'description',
          content:
            'Start your esports journey with stc play. A complete esports tournament management platform',
        },
        {
          property: 'og:description',
          content:
            'Start your esports journey with stc play. A complete esports tournament management platform',
        },
        {
          property: 'twitter:description',
          content:
            'Start your esports journey with stc play. A complete esports tournament management platform',
        },
      ],
    },
  },

  {
    path: 'match-making',
    loadChildren: () =>
      import('./modules/match-making/match-making.module').then(
        (m) => m.MatchmakingModule
      ),
    canActivate: [AuthGuard],
    data: {
      isRootPage: true,
    },
  },
  {
    path: 'privacy-policy',
    loadChildren: () =>
      import('./modules/privacy-policy/privacy-policy.module').then(
        (m) => m.PrivacyPolicyModule
      ),
  },
  {
    path: 'videos',
    loadChildren: () =>
      import('./modules/videos/videos.module').then((m) => m.VideosModule),
  },
  {
    path: 'search',
    loadChildren: () =>
      import('./modules/search/search.module').then((m) => m.SearchModule),
  },
  {
    path: 'bracket',
    canActivate: [AuthGuard],
    loadChildren: () =>
      import('./modules/bracket/bracket.module').then((m) => m.BracketModule),
  },
  {
    path: 'profile',
    canActivate: [AuthGuard],
    loadChildren: () =>
      import('./modules/profile/profile.module').then((m) => m.ProfileModule),
  },
  {
    path: 'admin',
    loadChildren: () =>
      import('./modules/admin/admin.module').then((m) => m.AdminModule),
  },
  {
    path: 'web-view',
    loadChildren: () =>
      import('./modules/web-view/web-view.module').then((m) => m.WebViewModule),
  },
  {
    path: 'comingsoon',
    loadChildren: () =>
      import('./modules/coming-soon/coming-soon.module').then(
        (m) => m.ComingSoonModule
      ),
  },
  {
    path: '404',
    loadChildren: () =>
      import('./modules/not-found/not-found.module').then(
        (m) => m.NotfoundModule
      ),
  },
  {
    path: 'shop',
    loadChildren: () => import('esports').then((m) => m.ShopModule),
  },
  { path: '**', redirectTo: '/404', pathMatch: 'full' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      scrollPositionRestoration: 'enabled',
      initialNavigation: 'enabled',
    }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
