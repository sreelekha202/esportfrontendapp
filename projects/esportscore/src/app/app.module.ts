import {
  BrowserModule,
  BrowserTransferStateModule,
} from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  SocialAuthServiceConfig,
  SocialLoginModule,
  GoogleLoginProvider,
  FacebookLoginProvider,
} from 'angularx-social-login';
import { AngularFireMessagingModule } from '@angular/fire/messaging';
import { NgxIntlTelInputModule } from 'ngx-intl-tel-input';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';
import { AngularFireModule } from '@angular/fire';
import { DatePipe } from '@angular/common';
import { ChartsModule } from 'ng2-charts';

import { JwtInterceptor } from './core/helpers/interceptors/token-interceptors.service';
import { ComingSoonComponent } from './modules/coming-soon/coming-soon.component';
import { PaginationService } from './core/service/pagination.service';
import { HeaderComponent } from './core/header/header.component';
import {deepLinkHeaderComponent} from  './core/header/linkHeader/deepLink-header.component';
import {stickyFooterComponent} from  './core/footer/stickyFooter/sticky-footer.component';
import { FooterComponent } from './core/footer/footer.component';
import { appInitializer } from './core/helpers/app.initializer';
import { SharedModule } from './shared/modules/shared.module';
import { environment } from '../environments/environment';
import { AppRoutingModule } from './app-routing.module';
import { EsportsUserService, EsportsPaginationService, EsportsModule, I18nModule, EsportsToastService, EsportsSnackBarModule, PipeModule } from 'esports';
import { CoreModule } from './core/core.module';
import { AppComponent } from './app.component';
import { InlineSVGModule } from 'ng-inline-svg';
import {RouterExtService } from '../../src/app/core/service/routing.service'

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    ComingSoonComponent,
    deepLinkHeaderComponent,
    stickyFooterComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    BrowserAnimationsModule,
    BrowserTransferStateModule,
    AppRoutingModule,
    CoreModule,
    SharedModule,
    I18nModule.forRoot(environment),
    SocialLoginModule,
    NgxIntlTelInputModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireMessagingModule,
    PipeModule,
    InlineSVGModule.forRoot(),
    ChartsModule,
    EsportsModule.forRoot(environment),
    EsportsSnackBarModule.setPlatform('ESPORTSCORE')
  ],
  providers: [
    CookieService,
    DatePipe,
    EsportsToastService,
    RouterExtService,
    PaginationService,
    EsportsPaginationService,
    {
      provide: APP_INITIALIZER,
      useFactory: appInitializer,
      multi: true,
      deps: [EsportsUserService],
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptor,
      multi: true,
    },
    {
      provide: 'SocialAuthServiceConfig',
      useValue: {
        autoLogin: false,
        providers: [
          {
            id: GoogleLoginProvider.PROVIDER_ID,
            provider: new GoogleLoginProvider(environment.googleAPPID),
          },
          {
            id: FacebookLoginProvider.PROVIDER_ID,
            provider: new FacebookLoginProvider(environment.facebookAPPID),
          },
        ],
      } as SocialAuthServiceConfig,
    },
    ...(environment.gtmId ? [{ provide: 'googleTagManagerId', useValue: environment.gtmId }] : [])
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
