import {
  Component,
  ElementRef,
  OnDestroy,
  OnInit,
  TemplateRef,
  ViewChild,
} from '@angular/core';

import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import {
  AuthServices,
} from '../service';
import { EsportsUserService, EsportsLanguageService, EsportsConstantsService, EsportsToastService } from 'esports';

import { faPhone } from '@fortawesome/free-solid-svg-icons';
import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';

import {
  SocialAuthService,
  FacebookLoginProvider,
  GoogleLoginProvider,
  SocialUser,
} from 'angularx-social-login';

import {
  CountryISO,
  SearchCountryField,
} from 'ngx-intl-tel-input';

import { environment } from '../../../environments/environment';
import { GlobalUtils } from '../../shared/service/global-utils/global-utils';

const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;

@Component({
  selector: 'app-social-media-login',
  templateUrl: './social-media-login.component.html',
  styleUrls: ['./social-media-login.component.scss'],
  providers: [NgbModalConfig, NgbModal],
})
export class SocialMediaLoginComponent implements OnInit, OnDestroy {
  @ViewChild('content') private content: TemplateRef<any>;
  @ViewChild('formRow') rows: ElementRef;

  SearchCountryField = SearchCountryField;
  CountryISO = CountryISO;

  didFail = false;
  loading = false;
  separateDialCode = true;
  stepOne = true;
  stepTwo = false;
  codeEntered: boolean;

  confirmForm: FormGroup;
  location: Location;
  MobileVerificationForm: FormGroup;
  user: SocialUser;

  id = '';
  message = '';

  code: string;
  returnUrl: string;

  timeLeft = 240;
  interval;

  preferredCountries: CountryISO[] = [
    CountryISO.SaudiArabia,
    CountryISO.Bahrain,
    CountryISO.Kuwait,
  ];

  faPhone = faPhone;

  constructor(
    config: NgbModalConfig,
    private authService: SocialAuthService,
    private formBuilder: FormBuilder,
    private languageService: EsportsLanguageService,
    private ConstantsService: EsportsConstantsService,
    private loginService: AuthServices,
    private modalService: NgbModal,
    private router: Router,
    private translateService: TranslateService,
    private userService: EsportsUserService,
    public eSportsToastService: EsportsToastService,
    public translate: TranslateService
  ) {
    config.backdrop = 'static';
    config.keyboard = false;
    config.centered = true;
    config.backdropClass = 'dark-modal';
  }

  ngOnInit() {
    this.languageService.language.subscribe((lang) =>
      this.translateService.use(lang || this.ConstantsService?.defaultLangCode)
    );

    this.MobileVerificationForm = this.formBuilder.group({
      phoneNo: ['', Validators.compose([Validators.required])],
    });

    this.confirmForm = this.formBuilder.group({
      code1: ['', Validators.compose([Validators.required])],
      code2: ['', Validators.compose([Validators.required])],
      code3: ['', Validators.compose([Validators.required])],
      code4: ['', Validators.compose([Validators.required])],
    });

    this.returnUrl = this.loginService.redirectUrl || '/';
  }

  ngOnDestroy() {
    this.modalService.dismissAll();
  }

  public socialSignIn(socialProvider: string) {
    let socialPlatformProvider;
    if (socialProvider === 'facebook') {
      socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
    } else if (socialProvider === 'google') {
      socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
    }

    this.authService.signIn(socialPlatformProvider).then((socialusers) => {
      if (socialusers) {
        this.loading = true;
        this.Savesresponse(socialusers.authToken, socialusers.provider);
      }
    });
  }

  Savesresponse(authToken, provider) {
    this.loginService.social(authToken, provider).subscribe(
      (data) => {
        if (data.code == 'ER1001') {
          this.loading = false;
          this.id = data.data.id;

          this.modalService.open(this.content, {
            size: 'md',
            centered: true,
            scrollable: false,
            windowClass: 'login-modal-content',
          });
        } else {
          this.loading = false;
          this.userService.startRefreshTokenTimer(API, TOKEN);

          if (data.data.type === 'admin') {
            this.router.navigate(['/admin']);
          } else {
            if (data.data.firstLogin == 1) {
              this.router.navigate(['/profile']);
            } else {
              this.router.navigate([this.returnUrl]);
            }
          }
        }
      },
      (error) => {
        this.loading = false;
        this.eSportsToastService.showError(
          (error?.error?.messageCode &&
            this.translateService.instant(
              `SOCIAL_RESPONSE.${error?.error?.messageCode}`
            )) ||
            error?.error?.message
        );
      }
    );
  }

  enableStepTwo(value) {
    const controls = this.MobileVerificationForm.controls;

    if (this.MobileVerificationForm.invalid) {
      Object.keys(controls).forEach((controlName) =>
        controls[controlName].markAsTouched()
      );
      return;
    }

    const data = {
      type: 'update_mobile',
      phoneNumber: value.phoneNo.e164Number,
      id: this.id,
    };
    this.didFail = false;
    this.message = '';

    this.loginService.social_login(data).subscribe(
      (data) => {
        this.didFail = false;
        this.message = '';
        this.stepOne = false;
        this.stepTwo = true;

        this.interval = setInterval(() => {
          if (this.timeLeft > 0) {
            this.timeLeft--;
          }
        }, 1000);
      },
      (error) => {
        this.didFail = true;
        this.message =
          (error?.error?.messageCode &&
            this.translateService.instant(
              `SOCIAL_RESPONSE.${error?.error?.messageCode}`
            )) ||
          error?.error?.message;
      }
    );
  }

  resendOtp() {
    let data = {
      id: this.id,
      type: 'resend_otp',
    };

    this.loginService.social_login(data).subscribe(
      (data) => {
        this.didFail = false;
        this.message = '';

        this.timeLeft = 240;
      },
      (error) => {
        this.didFail = true;
        this.message =
          (error?.error?.messageCode &&
            this.translateService.instant(
              `SOCIAL_RESPONSE.${error?.error?.messageCode}`
            )) ||
          error?.error?.message;
      }
    );
  }

  keyUpEvent(event, index) {
    let pos = index;

    if (event.keyCode === 8 && event.which === 8) {
      pos = index - 1;
    } else {
      pos = index + 1;
    }
  }

  onOtpChange(code: string): void {
    this.codeEntered = code.length === 4;

    if (this.codeEntered) {
      this.code = code;
    }
  }

  onConfirm(value) {
    const code = this.code;
    this.didFail = false;
    this.message = '';

    const data = {
      id: this.id,
      otp: code,
      type: 'verify_mobile',
    };

    this.loginService.social_login(data).subscribe(
      (data) => {
        if (GlobalUtils.isBrowser()) {
          this.loginService.setCookie(data.data.token);
          localStorage.setItem(environment.currentToken, data.data.token);
          this.userService.startRefreshTokenTimer(API, TOKEN);
          this.modalService.dismissAll();
        }

        if (data.data.type === 'admin') {
          this.router.navigate(['/admin']);
        } else {
          if (data.data.firstLogin == 1) {
            this.router.navigate(['/profile']);
          } else {
            this.router.navigate([this.returnUrl]);
          }
        }
      },
      (error) => {
        this.didFail = true;
        this.message =
          (error?.error?.messageCode &&
            this.translateService.instant(
              `SOCIAL_RESPONSE.${error?.error?.messageCode}`
            )) ||
          error?.error?.message;
      }
    );
  }

  open(content) {
    this.modalService.open(content);
  }

  cancel() {
    this.modalService.dismissAll();
    this.router
      .navigateByUrl('/home', { skipLocationChange: true })
      .then(() => {
        this.router.navigate(['/user', 'phone-login']);
      });
  }

  isControlHasErrorphone(controlName: string): boolean {
    const control = this.MobileVerificationForm.controls[controlName];
    if (control.touched) {
      if (control.invalid) {
        this.didFail = false;
        return true;
      }
    } else {
      return false;
    }
  }
}
