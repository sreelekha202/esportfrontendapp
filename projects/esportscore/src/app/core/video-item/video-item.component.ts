import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit,
} from '@angular/core';
import { EsportsLanguageService } from 'esports';
import { AppHtmlRoutes } from '../../app-routing.model';

@Component({
  selector: 'app-video-item',
  templateUrl: './video-item.component.html',
  styleUrls: ['./video-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class VideoItemComponent implements OnInit {
  @Input() data;

  AppHtmlRoutes = AppHtmlRoutes;

  constructor(public languageService: EsportsLanguageService) {}

  ngOnInit(): void {}
}
