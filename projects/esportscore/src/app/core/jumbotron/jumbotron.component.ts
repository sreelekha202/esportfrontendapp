import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit,
} from '@angular/core';
import { EsportsLanguageService } from 'esports';
import { AppHtmlRoutes } from '../../app-routing.model';

export interface JumbotronComponentData {
  _id: string;
  btn: string;
  dateTitle: string;
  ribbon: string;
  slug: string;
  title?: any;
  user: {
    img: string;
    name: string;
  };

  isBtnHidden?: boolean;
}

@Component({
  selector: 'app-jumbotron',
  templateUrl: './jumbotron.component.html',
  styleUrls: ['./jumbotron.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class JumbotronComponent implements OnInit {
  @Input() backgroundUrl: any;
  @Input() data: JumbotronComponentData;

  AppHtmlRoutes = AppHtmlRoutes;

  constructor(public languageService: EsportsLanguageService) {}

  ngOnChanges() {}

  ngOnInit(): void {}
}
