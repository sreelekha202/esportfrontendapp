import { Component, OnInit } from '@angular/core';

import { AppHtmlRoutes } from '../../app-routing.model';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
})
export class FooterComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;

  socials = [
    {
      link: 'https://www.twitch.tv/stcplay',
      icon: 'assets/icons/twitch-blue.svg',
    },
    {
      link: 'https://twitter.com/stcplay',
      icon: 'assets/icons/twitter-blue.svg',
    },
    {
      link: 'https://www.facebook.com/stcplay.hub',
      icon: 'assets/icons/facebook-blue.svg',
    },
    {
      link: 'https://www.instagram.com/stcplay',
      icon: 'assets/icons/instagram-blue.svg',
    },
    {
      link: 'https://discord.me/stcplay',
      icon: 'assets/icons/discord-blue.svg',
    },
    {
      link: 'https://www.snapchat.com/add/stcplay',
      icon: 'assets/icons/snapchat-blue.svg',
    },
    {
      link: 'https://www.youtube.com/channel/UCu9ZQKKPwtCP1ROZVmULfVQ',
      icon: 'assets/icons/youtube-blue.svg',
    },
  ];

  stores = [
    // {
    //   link: 'https://play.google.com/store/apps/details?id=com.stc.play',
    //   image: 'assets/images/play-store/google-store.png',
    //   alt: 'google store',
    // },
    // {
    //   link: 'https://play.google.com/store/apps/details?id=com.stc.play',
    //   image: 'assets/images/play-store/apple-store.png',
    //   alt: 'apple store',
    // },
  ];

  navigation = [
    { link: AppHtmlRoutes.aboutUs, text: 'FOOTER.ABOUT' },
    { link: AppHtmlRoutes.termsOfUse, text: 'FOOTER.TERMS_OF_USE' },
    { link: AppHtmlRoutes.privacyPolicy, text: 'FOOTER.PRIVACY' },
    { link: AppHtmlRoutes.codeOfConduct, text: 'FOOTER.CODE_OF_CONDUCT' },
    { link: AppHtmlRoutes.antiSpamPolicy, text: 'FOOTER.ANTI_SPAM_POLICY' },
  ];

  constructor() {}

  buildConfig = environment.buildConfig || 'N/A';

  ngOnInit(): void {}
}
