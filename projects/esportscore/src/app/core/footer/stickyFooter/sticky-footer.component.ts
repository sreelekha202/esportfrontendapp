import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  PLATFORM_ID,
  Inject,
} from '@angular/core';
import {
  AppHtmlRoutes,
  AppHtmlProfileRoutes,
  AppHtmlRoutesLoginType,
} from '../../../../app/app-routing.model';
import { TranslateService } from '@ngx-translate/core';
import { isPlatformBrowser } from '@angular/common';
@Component({
  selector: 'app-sticky-footer',
  templateUrl: './sticky-footer.component.html',
  styleUrls: ['./sticky-footer.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class stickyFooterComponent implements OnInit {
  AppHtmlProfileRoutes = AppHtmlProfileRoutes;
  AppHtmlRoutes = AppHtmlRoutes;
  AppHtmlRoutesLoginType = AppHtmlRoutesLoginType;
  isBrowser: boolean;
  constructor(
    public translate: TranslateService,
    @Inject(PLATFORM_ID) private platformId
  ) {
    this.isBrowser = isPlatformBrowser(platformId);
  }
  gameCenterDataforRegister = {
    footerLinkUrl: [
      AppHtmlRoutes.userPageType,
      AppHtmlRoutesLoginType.selectProfile,
    ],
  };

  ngOnInit(): void {}
}
