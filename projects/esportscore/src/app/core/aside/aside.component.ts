import {
  Component,
  OnInit,
  Inject,
  HostListener,
  OnDestroy,
} from '@angular/core';
import { Router } from '@angular/router';


import { EsportsChatSidenavService, EsportsConstantsService, EsportsLanguageService } from 'esports';

import { AppHtmlRoutes, AppHtmlProfileRoutes } from '../../app-routing.model';
import { DOCUMENT } from '@angular/common';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';

import { EsportsUserService, EsportsToastService, GlobalUtils } from 'esports';
import { environment } from '../../../environments/environment';

const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;

@Component({
  selector: 'app-aside',
  templateUrl: './aside.component.html',
  styleUrls: ['./aside.component.scss'],
})
export class AsideComponent implements OnInit, OnDestroy {
  activeLang = this.ConstantsService?.defaultLangCode;
  AppHtmlProfileRoutes = AppHtmlProfileRoutes;
  AppHtmlRoutes = AppHtmlRoutes;

  isAdmin = false;

  currentUser;
  clicked = false;
  AppLanguage = [];
  userStatistic = [];
  URL: string;
  closeResult: boolean;
  pageNavigation = [
    {
      name: 'header.HOME',
      url: AppHtmlRoutes.home,
      icon: 'assets/icons/navigations/home.svg',
      test_id: 'tournament-button-home',
    },
    {
      name: 'header.DISCOVER',
      url: AppHtmlRoutes.article,
      icon: 'assets/icons/navigations/discover.svg',
      test_id: 'tournament-button-discover',
    },
    {
      name: 'header.GAMES',
      url: AppHtmlRoutes.games,
      icon: 'assets/icons/navigations/games.svg',
      test_id: 'tournament-button-games',
    },
    {
      name: 'header.TOURNAMENTS',
      url: AppHtmlRoutes.tournament,
      icon: 'assets/icons/navigations/tournaments.svg',
      test_id: 'tournament-button-tournaments',
    },
    {
      name: 'header.LEADERBOARD',
      url: AppHtmlRoutes.leaderboard,
      icon: 'assets/icons/navigations/leaderboard.svg',
      test_id: 'tournament-button-leaderboard',
    },
    {
      name: 'header.MONITORING',
      url: AppHtmlRoutes.monitoring,
      icon: 'assets/icons/navigations/monitoring.svg',
      test_id: 'tournament-button-monitoring',
    }
  ];

  profileNavigation = [
    {
      name: 'header.HELP',
      url: AppHtmlRoutes.help,
      icon: 'assets/icons/navigations/help.svg',
    },
  ];
  public innerWidth: any;
  userSubscription: Subscription;
  AllCountries = [];
  constructor(
    @Inject(DOCUMENT) private document: Document,
    private chatSidenavService: EsportsChatSidenavService,
    private languageService: EsportsLanguageService,
    private ConstantsService: EsportsConstantsService,
    private modalService: NgbModal,
    private router: Router,
    private userService: EsportsUserService,
    public translate: TranslateService,
    public eSportsToastService: EsportsToastService
  ) {}
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    if (GlobalUtils.isBrowser()) {
      this.innerWidth = window.innerWidth;
    }
  }
  ngOnInit(): void {
    if (GlobalUtils.isBrowser()) {
      this.getAllCountries();
      this.innerWidth = window.innerWidth;
      this.activeLang = this.translate.currentLang;
      this.AppLanguage = this.ConstantsService?.language;
      this.getUserData();
    }
  }

  ngOnDestroy() {
    this.userSubscription?.unsubscribe();
  }

  onConfirmLogout(content) {
    this.modalService.open(content, {
      centered: true,
      windowClass: 'custom-modal-content',
    });
  }
  copyToClipboard(event: any) {
    const dummyElement = this.document.createElement('textarea');
    var element = document.getElementById('myDIV');
    element.classList.toggle('disableBtn');
    this.document.body.appendChild(dummyElement);
    dummyElement.value = this.URL;
    event.target.disabled = true;
    dummyElement.select();
    this.document.execCommand('copy');
    this.document.body.removeChild(dummyElement);
  }
  open(content) {
    if (this.currentUser.accountDetail.referralId) {
      this.clicked = false;
      this.modalService
        .open(content, { ariaLabelledBy: 'modal-basic-title', centered: true })
        .result.then(
          (result) => {
            this.copyToClipboard(this.URL);
          },
          (reason) => {
            this.closeResult = false;
          }
        );
    } else {
      this.eSportsToastService.showError(
        this.translate.instant('PROFILE.BASIC_INFO.REFERRAL_ERROR_MESSAGE')
      );
    }
  }
  onLogOut(): void {
    if (GlobalUtils.isBrowser()) {
      this.userService.logout(API, TOKEN);
      this.chatSidenavService.close();
      this.modalService.dismissAll();
      this.router.navigate([AppHtmlRoutes.home]);
    }
  }

  onLanguageChange(lang): void {
    if (GlobalUtils.isBrowser()) {
      localStorage.setItem('currentLanguage', lang);
      this.activeLang = lang;
      this.translate.use(lang);
      this.languageService.setLanguage(lang);
    }
  }

  getUserData() {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (!data) {
        return;
      }
      this.currentUser = data;
      this.URL =
        this.document.location.origin +
        '/user/referral:' +
        data.accountDetail.referralId;
      this.isAdmin =
        data?.accountType === EsportsConstantsService.AccountType.Admin;
      this.userStatistic = [
        {
          count: this.currentUser.accountDetail?.reward || 0,
          title: 'PROFILE.BASIC_INFO.FORM.REWARD.NAME',
        },
        {
          count: this.currentUser.preference?.followingCount || 0,
          title: 'header.following',
        },
        {
          count: this.currentUser.preference?.followersCount || 0,
          title: 'header.followers',
        },
      ];
    });
  }
  getAllCountries() {
    this.AllCountries = [];
    this.userService.getAllCountries().subscribe((data) => {
      this.AllCountries = data.countries;
    });
  }
}
