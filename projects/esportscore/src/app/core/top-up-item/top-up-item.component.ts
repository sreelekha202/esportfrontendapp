import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit,
} from '@angular/core';
import { AppHtmlRoutes } from '../../app-routing.model';

export interface TopUpItemComponentData {
  logo: string;
  name: string;
  productName: string;
}

@Component({
  selector: 'app-top-up-item',
  templateUrl: './top-up-item.component.html',
  styleUrls: ['./top-up-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TopUpItemComponent implements OnInit {
  @Input() value: TopUpItemComponentData;

  AppHtmlRoutes = AppHtmlRoutes;

  constructor() {}

  ngOnInit(): void {}

  /**
   * Workaround for product image
   */
  _getImage(name) {
    const logo = './assets/images/Home/top1.png';
    return logo;
  }
}
