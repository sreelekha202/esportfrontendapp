import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit,
} from '@angular/core';
import { EsportsLanguageService } from 'esports';
import { TranslateService } from '@ngx-translate/core';

import { AppHtmlRoutes } from '../../app-routing.model';

export interface ArticleItemComponentData {
  _id: string;
  author?: string;
  createdDate: string;
  id: number;
  image: string;
  shortDescription: any;
  slug: string;
  title: any;
}

@Component({
  selector: 'app-article-item',
  templateUrl: './article-item.component.html',
  styleUrls: ['./article-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ArticleItemComponent implements OnInit {
  @Input() data: ArticleItemComponentData;

  AppHtmlRoutes = AppHtmlRoutes;

  constructor(
    public translate: TranslateService,
    public languageService: EsportsLanguageService
  ) {}

  ngOnInit(): void {}
}
