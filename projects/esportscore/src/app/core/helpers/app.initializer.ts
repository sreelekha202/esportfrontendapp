import { GlobalUtils } from '../../shared/service/global-utils/global-utils';

import { environment } from '../../../environments/environment';
import { EsportsUserService } from 'esports';

const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;

export function appInitializer(userService: EsportsUserService) {
  return () =>
    new Promise((resolve) => {
      // attempt to refresh token on app start up to auto authenticate
      if (GlobalUtils.isBrowser() && localStorage.getItem(TOKEN)) {
        const jwtToken = JSON.parse(
          atob(localStorage.getItem(TOKEN).split('.')[1])
        );

        const expires: Date = new Date(jwtToken.exp * 1000);

        if (expires > new Date()) {
          userService.startRefreshTokenTimer(API, TOKEN);
          userService.getAllCountries().subscribe().add(resolve);
        } else {
          userService
            .refreshToken(API, TOKEN)
            .subscribe(
              (data) => {},
              (error) => {
                userService.logout(API, TOKEN);
              }
            )
            .add(resolve);
        }
      } else {
        userService.getAllCountries().subscribe().add(resolve);
      }
    });
}
