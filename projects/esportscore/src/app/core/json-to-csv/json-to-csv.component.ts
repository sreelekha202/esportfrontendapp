import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { ngxCsv } from 'ngx-csv/ngx-csv';

@Component({
  selector: 'app-json-to-csv',
  templateUrl: './json-to-csv.component.html',
  styleUrls: ['./json-to-csv.component.scss'],
})
export class JsonToCsvComponent implements OnInit {
  constructor(private http: HttpClient) {}

  ngOnInit(): void {}

  convertJSONToCSV = async () => {
    const data = await this.http.get('./assets/i18n/en.json').toPromise();
    const list = [];

    const flatJSON = async (obj, key1) => {
      Object.keys(obj).forEach(async (key2) => {
        const name = key1 ? `${key1}.${key2}` : `${key2}`;
        if (typeof obj[key2] === 'object') {
          return await flatJSON(obj[key2], name);
        } else {
          list.push({ key: name, value: obj[key2] });
          return true;
        }
      });
    };

    const options = {
      noDownload: false,
      headers: ['Keys', 'Value'],
    };

    const jsonresponse = await flatJSON(data, null);
    const columns = { key: 'Keys', value: 'Value' };
    new ngxCsv(list, 'My Translation', options);
  };
}
