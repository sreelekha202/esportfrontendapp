import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { deepLinkHeaderComponent } from './deepLink-header.component';

describe('deepLinkHeaderComponent', () => {
  let component: deepLinkHeaderComponent;
  let fixture: ComponentFixture<deepLinkHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [deepLinkHeaderComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(deepLinkHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
