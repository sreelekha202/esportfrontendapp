import { Component, OnInit, Input, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

import { EsportsUserService } from 'esports';
@Component({
  selector: 'app-deepLink-header',
  templateUrl: './deepLink-header.component.html',
  styleUrls: ['./deepLink-header.component.scss'],
})
export class deepLinkHeaderComponent implements OnInit {
  stores = [];
  constructor(
    private userService: EsportsUserService,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<deepLinkHeaderComponent>
  ) {}

  ngOnInit(): void {
    this.dialogRef.updatePosition({ top: `0px` });
    this.userService.currentUser.subscribe(
      (data) => {
        if (data) {
          this.onClose();
        }
      },
      (error) => {}
    );
    this.stores = [
      {
        link: 'https://play.google.com/store/apps/details?id=com.stc.xplay',
        image: 'assets/images/play-store/google-store.png',
        alt: 'google store',
        show: this.data.isAndroid,
      },
      {
        link: 'https://apps.apple.com/us/app/stc-play/id1558331986',
        image: 'assets/images/play-store/apple-store.png',
        alt: 'apple store',
        show: this.data.isIphone,
      },
    ];
  }
  onClose(): void {
    this.dialogRef.close(false);
  }
}
