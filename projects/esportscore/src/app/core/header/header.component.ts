import {
  Component,
  ElementRef,
  EventEmitter,
  OnDestroy,
  OnInit,
  Output,
  Inject,
  PLATFORM_ID,
} from '@angular/core';


import {
  IUser,
  EsportsUserService,
  EsportsChatSidenavService,
  EsportsLanguageService,
  EsportsConstantsService,
  EsportsChatService,
  EsportsToastService,
  GlobalUtils,
} from 'esports';
import { AuthServices } from '../../core/service';
import { fromEvent, Subscription } from 'rxjs';

import { AppHtmlRoutes, AppHtmlRoutesLoginType } from '../../app-routing.model';

import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';
import { faBars } from '@fortawesome/free-solid-svg-icons';
import { TranslateService } from '@ngx-translate/core';
import { isPlatformBrowser } from '@angular/common';
import { environment } from '../../../environments/environment'
import { deepLinkHeaderComponent } from './linkHeader/deepLink-header.component';
import { MatDialog } from '@angular/material/dialog';


const API = environment.apiEndPoint;
const token = environment.currentToken;

@AutoUnsubscribe()
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit, OnDestroy {
  @Output() menuClick = new EventEmitter();
  isAndroid:boolean=false;
  isIphone:boolean=false;
  isBrowser: boolean;
  activeLang = this.ConstantsService.defaultLangCode;
  AppHtmlRoutes = AppHtmlRoutes;
  AppHtmlRoutesLoginType = AppHtmlRoutesLoginType;

  isUserLoggedIn = false;

  currentUserId: any;
  currenUser: IUser;
  matchcount: number;
  matchlist: any;
  tournaments: any;
  chatWins = [];
  faBars = faBars;
  currentUser: IUser = null;

  private sub1: Subscription;
  userSubscription: Subscription;

  constructor(
    @Inject(PLATFORM_ID) private platformId,
    private chatService: EsportsChatService,
    private chatSidenavService: EsportsChatSidenavService,
    private elementRef: ElementRef,
    private languageService: EsportsLanguageService,
    private ConstantsService: EsportsConstantsService,
    private userService: EsportsUserService,
    public eSportsToastService: EsportsToastService,
    public translate: TranslateService,
    public authService: AuthServices,
    private matDialog: MatDialog,
  ) {
    this.isBrowser = isPlatformBrowser(platformId);
  }

  ngOnInit(): void {
    if (this.isBrowser) {
      this.matchlist = [];
      this.matchcount = 0;
      this.activeLang = this.translate.currentLang;
      this.languageService.setLanguage(this.translate.currentLang);
      let ua = navigator.userAgent.toLowerCase();
      this.isAndroid = ua.indexOf('android') > -1; // android check
      this.isIphone = ua.indexOf('iphone') > -1; // ios check
      this.userSubscription = this.userService.currentUser.subscribe(
        (data) => {
          if (data) {
            this.currenUser = data;
            this.isUserLoggedIn = !!this.currenUser;
            if (this.currenUser) {
              this.currentUserId = this.currenUser._id;
            }

            this.chatService?.login.subscribe((data) => {
              // if (data.accessToken != 'undefined' && data.accessToken != null) {
              this.chatService?.connectUser();

              this.chatService.unreadCount.subscribe((unreadCount) => {
                this.matchcount = unreadCount;
              });

              this.chatService?.getUnReadCount().subscribe((res: any) => {});
              // }
            });
          } else {
            this.userService.getAuthenticatedUser(API, token);
          }
        },
        (error) => {
          this.eSportsToastService.showError(error.message);
        }
      );
      this.onHeaderScroll();
      // Set registration source to STCPLAY in local storage for registration tracking
      if (GlobalUtils.isBrowser()) {
        setTimeout(() => {
          if (!this.authService.getRegistrationSrc()) {
            this.authService.setRegistrationSrc('STCPLAY');
          }
        }, 2000);
      }
    }
    if (
      location.pathname ==='/'
    ) {
      if (this.isAndroid || this.isIphone) {
        this.openDeepLinkPopUp();
      }
    }
  }
  openDeepLinkPopUp = async () => {
    try {
      const data = {
        "isAndroid":this.isAndroid,
        "isIphone":this.isIphone
      };
      const response = await this.matDialog
        .open(deepLinkHeaderComponent, {
          width: '100%',
          height:'100px',
          data,
        })
        .afterClosed()
        .toPromise();
    } catch (error) { }
  };
  ngOnDestroy() {
    if (this.currentUserId) {
      this.chatService?.disconnectUser();
    }
    this.userSubscription?.unsubscribe();
  }

  onChatSidenavToggle(): void {
    this.chatSidenavService.toggle();

    if (this.currentUserId) {
      this.chatService?.getAllMatches(this.currentUserId, 1, 30);
    }

    if (this.chatSidenavService.checkOpen()) {
      if (this.chatService.getChatStatus()) {
        this.chatService.setChatStatus(false);
      }
    } else {
      if (this.chatService.getChatStatus()) {
        this.chatService.setChatStatus(false);
      }
    }
  }

  onLogOut(): void {
    if (GlobalUtils.isBrowser()) {
      this.chatService.setChatStatus(false);
      this.userService.logout(API, token);
      this.chatSidenavService.close();
      this.chatService.doLogout();
      this.chatService?.disconnectUser();
      window.location.reload();
    }
  }

  onMenuOpen(): void {
    this.menuClick.next();
  }
  private onHeaderScroll(): void {
    if (GlobalUtils.isBrowser()) {
      const scrollingContent = document.querySelector('mat-sidenav-content');
      if (scrollingContent) {
        const header = this.elementRef.nativeElement;
        this.sub1 = fromEvent(scrollingContent, 'scroll').subscribe(
          (e: Event) => {
            if ((e.target as Element).scrollTop > header.clientHeight) {
              header.classList.add('is-scrolling');
            } else {
              header.classList.remove('is-scrolling');
            }
          }
        );
      }
    }
  }
}
