import { NgModule } from '@angular/core';

import {
  SocialAuthServiceConfig,
  GoogleLoginProvider,
  FacebookLoginProvider,
} from 'angularx-social-login';
import { SharedModule } from '../shared/modules/shared.module';

import { AdvertisingComponent } from '../modules/home2/advertising/advertising.component';
import { AllTournamentsComponent } from '../modules/home2/all-tournaments/all-tournaments.component';
import { AnnouncementComponent } from '../modules/home2/announcement/announcement.component';
import { ArticleItemComponent } from './article-item/article-item.component';
import { AsideComponent } from './aside/aside.component';
import { DropDownSelectComponent } from './drop-down-select/drop-down-select.component';
import { GamesCardsComponent } from '../modules/home2/games-cards/games-cards.component';
import { HottestPostsComponent } from '../modules/home2/hottest-posts/hottest-posts.component';
import { JsonToCsvComponent } from './json-to-csv/json-to-csv.component';
import { JumbotronComponent } from './jumbotron/jumbotron.component';
import { MatchHighlightComponent } from '../modules/home2/match-highlight/match-highlight.component';
import { ScrollTopComponent } from './scroll-top/scroll-top.component';
import { SearchInputComponent } from './search-input/search-input.component';
import { SportsFederationComponent } from '../modules/home2/sports-federation/sports-federation.component';
import { StepperComponent } from './stepper/stepper.component';
import { TopPlayersComponent } from '../modules/home2/top-players/top-players.component';
import { TopUpItemComponent } from './top-up-item/top-up-item.component';
import { TournamentsSliderComponent } from '../modules/home2/tournaments-slider/tournaments-slider.component';
import { TournamentsTrackerComponent } from '../modules/home2/tournaments-tracker/tournaments-tracker.component';
import { TournamentTrackerCardComponent } from '../modules/home2/tournament-tracker-card/tournament-tracker-card.component';
import { VideoItemComponent } from './video-item/video-item.component';
import { VouchersSliderComponent } from '../modules/home2/vouchers-slider/vouchers-slider.component';
import { StcPopupComponent } from './stc-popup/stc-popup.component';
import { environment } from '../../environments/environment';

const components = [
  AdvertisingComponent,
  AllTournamentsComponent,
  AnnouncementComponent,
  ArticleItemComponent,
  AsideComponent,
  DropDownSelectComponent,
  GamesCardsComponent,
  HottestPostsComponent,
  JsonToCsvComponent,
  JumbotronComponent,
  MatchHighlightComponent,
  ScrollTopComponent,
  SearchInputComponent,
  SportsFederationComponent,
  StepperComponent,
  TopPlayersComponent,
  TopUpItemComponent,
  TournamentsSliderComponent,
  TournamentsTrackerComponent,
  TournamentTrackerCardComponent,
  VideoItemComponent,
  VouchersSliderComponent,
  StcPopupComponent,
];

@NgModule({
  declarations: [...components],
  imports: [SharedModule],
  exports: [...components],
  providers: [
    {
      provide: 'SocialAuthServiceConfig',
      useValue: {
        autoLogin: false,
        providers: [
          {
            id: GoogleLoginProvider.PROVIDER_ID,
            provider: new GoogleLoginProvider(environment.googleAPPID),
          },
          {
            id: FacebookLoginProvider.PROVIDER_ID,
            provider: new FacebookLoginProvider(environment.facebookAPPID),
          },
        ],
      } as SocialAuthServiceConfig,
    },
  ],
})
export class CoreModule {}
