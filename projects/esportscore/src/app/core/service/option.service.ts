import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { SubscribableOrPromise } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class OptionService {
  constructor(private http: HttpClient) {}

  fetchAllGenres(): SubscribableOrPromise<any> {
    return this.http
      .get(`${environment.apiEndPoint}option/all-genres`)
      .toPromise();
  }

  fetchAllCategories(): SubscribableOrPromise<any> {
    return this.http
      .get(`${environment.apiEndPoint}option/all-categories`)
      .toPromise();
  }

  fetchAllTags(): SubscribableOrPromise<any> {
    return this.http
      .get(`${environment.apiEndPoint}option/all-tags`)
      .toPromise();
  }
}
