import { Injectable } from '@angular/core';
import { GlobalUtils } from 'esports';

@Injectable({
  providedIn: 'root',
})
export class UtilsService {
  constructor() {}

  convertTimeIntoString(time) {
    time = time
      .toString()
      .match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];
    if (time.length > 1) {
      time = time.slice(1);
      time[5] = +time[0] < 12 ? 'AM' : 'PM';
      time[0] = +time[0] % 12 || 12;
    }
    return time.join('');
  }

  convertStringIntoTime(startDate, startTime) {
    if (startDate && startTime) {
      const startDt = new Date(startDate);
      const time = startTime.match(/[a-z]+|\d+/gi);
      const hour =
        time[2] === 'PM'
          ? (parseInt(time[0], 10) || 0) + 12
          : parseInt(time[0], 10) || 0;
      const minute = parseInt(time[1], 10) || 0;
      startDt.setHours(hour, minute);
      return startDt;
    } else {
      return null;
    }
  }

  getEmbbedUrl = async (url) => {
    const channelName = await this.getVideoId(url);
    return this.createEmbedUrl('YOUTUBE', { channelName });
  };

  createEmbedUrl = (type, url) => {
    if (GlobalUtils.isBrowser()) {
      switch (true) {
        case type === 'YOUTUBE':
          return `https://www.youtube.com/embed/${url.channelName}`;
        case type === 'TWITCH':
          return `https://player.twitch.tv/?channel=${url.channelName}&parent=${window.parent.location.hostname}`;
        case type === 'LIVESTREAM':
          return `${url?.providerName}${url?.channelName}`;
        case type === 'FACEBOOK':
          const video_id =
            url?.channelName &&
            url?.channelName.match(/videos\/(\d+)+|v=(\d+)|vb.\d+\/(\d+)/)?.[1];
          return `https://www.facebook.com/video/embed?video_id=${video_id}`;
        default:
          return null;
      }
    }
  };

  getVideoId = async (url) => {
    const regExp = /^.*(?:(?:youtu\.be\/|v\/|vi\/|u\/\w\/|embed\/)|(?:(?:watch)?\?v(?:i)?=|\&v(?:i)?=))([^#\&\?]*).*/;
    const match = url.match(regExp);
    if (match && match[1].length === 11) {
      return match[1];
    } else {
      throw new Error('Invalid Youtube Url');
    }
  };

  getLanguageWiseValue = (obj, currLanguage) => {
    return !obj ? 'N/A' : typeof obj === 'object' ? obj[currLanguage] : obj;
  };

  scrollToTop(scroll: any) {
    if (scroll) {
      scroll.nativeElement.scrollIntoView();
    }
  }

  showNativeAlert(message) {
    alert(message);
  }

  getEncodedQuery = (query) => {
    return `${encodeURIComponent(JSON.stringify(query))}`;
  };
}
