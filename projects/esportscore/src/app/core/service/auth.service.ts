import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpHeaders,
  HttpErrorResponse,
  HttpBackend,
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';

import { Router } from '@angular/router';

import { environment } from '../../../environments/environment';

import { map } from 'rxjs/operators';
import { CookieService } from 'ngx-cookie-service';
import { GlobalUtils } from '../../shared/service/global-utils/global-utils';
import { StcPopupComponent } from '../stc-popup/stc-popup.component';
import { MatDialog } from '@angular/material/dialog';

const API = environment.apiEndPoint + 'auth/';
const APIV2 = environment.apiEndPoint + 'v2/auth/';
const token = environment.currentToken;

@Injectable({
  providedIn: 'root',
})
export class AuthServices {
  // store the URL so we can redirect after logging in
  redirectUrl: string;

  constructor(
    private router: Router,
    private http: HttpClient,
    handler: HttpBackend,
    private cookieService: CookieService,
    private dialog: MatDialog
  ) {
    // this.http = new HttpClient(handler);
  }

  register(data): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    return this.http.post(APIV2 + 'register', data, { headers: httpHeaders });
  }

  confirmUser(utoken: string, code: string, type: string): Observable<any> {
    const data = {
      token: utoken,
      otp: code,
      type,
    };
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    return this.http.post(APIV2 + 'verify', data, {
      headers: httpHeaders,
    });
  }

  fetchPaypalUserInfo(code) {
    return this.http.get(`${API}paypalverification/${code}`).toPromise();
  }

  login(data) {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');

    // return this.http
    //   .post<any>(API + 'login', data, { headers: httpHeaders })
    //   .pipe(
    //     map((user) => {
    //       if (user.code == 'ER1001') {
    //         return user;
    //       } else {
    //         // store user details and jwt token in local storage to keep user logged in between page refreshes
    //         localStorage.setItem(token, user.data.token);
    //         // Get Profile data based on token
    //         // this.getProfile().subscribe(
    //         //   (res) => {
    //         //     this.currentUserSubject.next(res.data);
    //         //   }
    //         // );
    //         return user;
    //       }
    //     })
    //   );
    return this.http.post<any>(APIV2 + 'login', data, {
      headers: httpHeaders,
    });
  }

  update(data): Observable<any> {
    return this.http.put(API + 'login_update', data);
  }

  confirm(uid: string, code: string, utype: string): Observable<any> {
    const data = {
      otp: code,
      type: utype,
      id: uid,
    };

    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    return this.http.post(API + 'login_verify', data, {
      headers: httpHeaders,
    });
  }

  social(authtoken, sprovider) {
    if (GlobalUtils.isBrowser()) {
      const httpHeaders = new HttpHeaders();
      httpHeaders.set('Content-Type', 'application/json');
      const data = {
        type: 'login',
        provider: sprovider,
        token: authtoken,
      };
      return this.http
        .post<any>(API + 'social_login', data, {
          headers: httpHeaders,
        })
        .pipe(
          map((user) => {
            if (user.code == 'ER1001') {
              return user;
            } else {
              this.setCookie(user.data.token);
              // store user details and jwt token in local storage to keep user logged in between page refreshes
              localStorage.setItem(token, user.data.token);

              // Get Profile data based on token
              // this.getProfile().subscribe(
              //   (res) => {
              //     this.currentUserSubject.next(res.data);
              //   }
              // );
              return user;
            }
          })
        );
    }
  }

  social_login(data): Observable<any> {
    return this.http.post(API + 'social_login', data);
  }

  resendOTP(data): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    return this.http.post(APIV2 + 'resend_otp', data);
  }

  forgotPassword(data): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    return this.http.post(APIV2 + 'forgot_password', data);
  }

  isAuthenticated() {}

  initAuth() {}

  verify(username: string) {}

  devicelist() {}

  // Error
  handleError(error: HttpErrorResponse) {
    let msg = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      msg = error.error.message;
    } else {
      // server-side error
      msg = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    return throwError(msg);
  }

  setCookie(value) {
    let date = new Date();

    // Get Unix milliseconds at current time plus 365 days
    date.setTime(+date + 365 * 86400000); // 24 \* 60 \* 60 \* 100
    this.cookieService.set(
      'accessToken',
      value,
      date,
      '/',
      environment.cookieDomain
    );
  }

  getRegistrationType(): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    return this.http.get(API + 'registration_type');
  }

  validateCaptcha(data): Observable<any> {
    return this.http.post(API + 'validate_captcha', data);
  }

  searchUsername(text, Id?): Observable<any> {
    const urlString = Id
      ? `auth/search_username?text=${encodeURIComponent(text)}&Id=${Id}`
      : `auth/search_username?text=${encodeURIComponent(text)}`;
    return this.http.get(environment.apiEndPoint + urlString);
  }
  getPaymentAccountVerfiedStatus = async () => {
    try {
      const accountPaymentStatus: any = await this.http
        .get(`${API}getPaymentAccountStatus`)
        .toPromise();

      if (
        accountPaymentStatus?.data &&
        accountPaymentStatus?.data.hasOwnProperty('stcAccountVerified') &&
        !accountPaymentStatus?.data?.stcAccountVerified
      ) {
        const result = await this.dialog
          .open(StcPopupComponent, {
            panelClass: 'payment-dialog',
            autoFocus: false,
            restoreFocus: false,
          })
          .afterClosed()
          .toPromise();
        return result;
      } else if (!accountPaymentStatus?.data) {
        this.router.navigateByUrl('/user/phone-login');
      } else {
        return true;
      }
    } catch (error) {
      throw error;
    }
  };

  setRegistrationSrc(source) {
    if (GlobalUtils.isBrowser()) {
      localStorage.setItem('registrationSource', source);
    }
  }

  getRegistrationSrc() {
    if (GlobalUtils.isBrowser()) {
      return localStorage.getItem('registrationSource');
    }
  }
}
