import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { SubscribableOrPromise } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ArticleApiService {
  constructor(private httpClient: HttpClient) {}
  BaseURL = environment.apiEndPoint;
  public getallArticle() {
    return this.httpClient.get(`${this.BaseURL}article`);
  }
  public saveArticle(obj) {
    return this.httpClient.post(`${this.BaseURL}article`, obj);
  }
  public getArticleByID(id) {
    return this.httpClient.get(`${this.BaseURL}article/${id}`);
  }
  public getArticleBySlug(slug): SubscribableOrPromise<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json; charset=utf-8');
    return this.httpClient
      .get(`${this.BaseURL}article/slug/${slug}`, { headers: httpHeaders })
      .toPromise();
  }
  public delArticle(id) {
    return this.httpClient.delete(`${this.BaseURL}article/${id}`);
  }
  public updateArticle(id, value) {
    return this.httpClient.put(`${this.BaseURL}article/${id}`, value);
  }
  public saveComment(data) {
    return this.httpClient.post(`${this.BaseURL}comment`, data);
  }
  public addBookmark(data) {
    return this.httpClient.patch(
      `${this.BaseURL}userpreference/add_bookmark`,
      data
    );
  }
  public getTrendingAuthors() {
    return this.httpClient.get(`${this.BaseURL}home/trending-authors`);
  }
  public getArticles_PublicAPI(params) {
    const encodedUrl = encodeURIComponent(params['query']);
    let url = `${this.BaseURL}home/getArticles?query=${encodedUrl}`;
    if (params['projection']) {
      url += `&select=${params['projection']}`;
    }
    if (params['option']) {
      const option = encodeURIComponent(params['option']);
      url += `&option=${option}`;
    }
    if (params['pagination']) {
      let option = encodeURIComponent(params['pagination']);
      url += `&pagination=${option}`;
    }
    return this.httpClient.get(url);
  }
  public getHottestPost() {
    return this.httpClient.get(`${this.BaseURL}home/hottest_post`);
  }

  public getGameList(query) {
    const encodedUrl = encodeURIComponent(query);
    return this.httpClient.get(`${this.BaseURL}home/getGames?${encodedUrl}`);
  }
  public getTrendingPosts() {
    return this.httpClient.get(`${this.BaseURL}home/trending_posts`);
  }
  public getArticles(params) {
    const encodedUrl = encodeURIComponent(params['query']);
    let url = `${this.BaseURL}article?query=${encodedUrl}`;
    if (params['projection']) {
      url += `&select=${params['projection']}`;
    }
    if (params['option']) {
      let option = encodeURIComponent(params['option']);
      url += `&option=${option}`;
    }
    return this.httpClient.get(url);
  }

  public updateViews(id, value) {
    return this.httpClient.put(
      `${this.BaseURL}article/updateViews/${id}`,
      value
    );
  }

  public getArticleList() {
    return this.httpClient.get(`${this.BaseURL}home/article-list`);
  }

  /**
 *  Get paginated Articles (public API)
 * @param {*} params // params = {query: "JSON String", pagination: "JSON String"}
 *
 * query -  Filter
 * pagination - mongoose-paginate-v2 pagination type Object
 *Example: -
 params = {
   query:   "{\"articleStatus\":\"publish\"}",
   pagination: "{\"page\":1,\"limit\":4,\"sort\":\"-views\"}"
}
 */

  public getPaginatedArticles(params) {
    const encodedUrl = encodeURIComponent(params['query']);
    const encodedPagination = encodeURIComponent(params['pagination']);
    let url = `${this.BaseURL}home/getPaginatedArticles?query=${encodedUrl}&pagination=${encodedPagination}`;
    return this.httpClient.get(url);
  }

  public getLatestArticle(params) {
    const encodedUrl = encodeURIComponent(params['query']);
    const encodedPagination = encodeURIComponent(params['pagination']);
    const encodedPrefernce = params['preference']
      ? encodeURIComponent(params['preference'])
      : JSON.stringify({});
    let url = `${this.BaseURL}home/latestarticles?query=${encodedUrl}&pagination=${encodedPagination}&preference=${encodedPrefernce}`;
    return this.httpClient.get(url);
  }
}
