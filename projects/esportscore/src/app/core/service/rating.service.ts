import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { SubscribableOrPromise } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class RatingService {
  constructor(private http: HttpClient) {}

  addRating(payload): SubscribableOrPromise<any> {
    return this.http
      .post(`${environment.apiEndPoint}rating`, payload)
      .toPromise();
  }

  getRating(query): SubscribableOrPromise<any> {
    return this.http
      .get(`${environment.apiEndPoint}rating${query}`)
      .toPromise();
  }

  updateRating(id, payload): SubscribableOrPromise<any> {
    return this.http
      .patch(`${environment.apiEndPoint}rating/${id}`, payload)
      .toPromise();
  }
}
