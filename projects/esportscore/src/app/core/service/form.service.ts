import { Injectable } from '@angular/core';
import {
  AbstractControl,
  FormControl,
  FormGroup,
  ValidationErrors,
  ValidatorFn,
} from '@angular/forms';

@Injectable({
  providedIn: 'root',
})
export class FormService {
  constructor() {}

  groupFieldValidator(): ValidatorFn {
    return (group: FormGroup): ValidationErrors => {
      const value = group.value;
      const isValid = Object.values(value).some((el: any) => el.trim());
      return isValid ? null : { required: true };
    };
  }

  emptyCheck = (control: FormControl) => {
    return control.value && control.value.trim() ? null : { required: true };
  };

  minLengthArray(min: number) {
    return (c: AbstractControl): {[key: string]: any} => {
        if (c?.value?.length >= min)
            return null; 

        return { 'minLengthArray': {valid: false }};
    }
  }
}
