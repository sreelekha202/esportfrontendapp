import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable, SubscribableOrPromise } from 'rxjs';
import { environment } from '../../../environments/environment';
import { Router } from '@angular/router';
@Injectable({
  providedIn: 'root',
})
export class TournamentService {
  public tournamentActiveTab = 1;

  constructor(private http: HttpClient, private router: Router) {
    this.joinDetailSubject = new BehaviorSubject(null);
    this.joinDetail = this.joinDetailSubject.asObservable();
  }

  public joinDetailSubject: BehaviorSubject<any>;
  public joinDetail: Observable<any>;

  public setSelectedTeam(value: any) {
    this.joinDetailSubject.next(value);
  }

  saveTournament(formValues): Observable<any> {
    const data = formValues;
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    return this.http.post(environment.apiEndPoint + 'tournament', data, {
      headers: httpHeaders,
    });
  }

  updateTournament(formValues, id): Observable<any> {
    const data = formValues;
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    return this.http.put(environment.apiEndPoint + `tournament/${id}`, data, {
      headers: httpHeaders,
    });
  }

  abortTournament(formValues): Observable<any> {
    const data = formValues;
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    return this.http.put(environment.apiEndPoint + `tournament/abort`, data, {
      headers: httpHeaders,
    });
  }

  getTournaments(params, selectParams = ``, option = ''): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json; charset=utf-8');
    const encodedUrl = encodeURIComponent(params.query);
    let url = `${environment.apiEndPoint}tournament?query=${encodedUrl}${selectParams}`;
    if (option) {
      const opt = encodeURIComponent(option);
      url += `&option=${opt}`;
    }
    return this.http.get(url, { headers: httpHeaders });
  }

  getSeasonTournaments(
    params,
    selectParams = ``,
    option = ''
  ): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json; charset=utf-8');
    const encodedUrl = encodeURIComponent(params.query);
    let url = `${environment.apiEndPoint}tournament/season_tournament?query=${encodedUrl}${selectParams}`;
    if (option) {
      const opt = encodeURIComponent(option);
      url += `&option=${opt}`;
    }
    return this.http.get(url, { headers: httpHeaders });
  }

  getTournament(id): Observable<any> {
    return this.http.get(environment.apiEndPoint + `tournament/${id}`);
  }

  saveParticipant(data): Observable<any> {
    return this.http.post(environment.apiEndPoint + 'participant', data);
  }

  getParticipants(encodedUrl): Observable<any> {
    return this.http.get(environment.apiEndPoint + `participant${encodedUrl}`);
  }

  getOpponentParticipantForMatch(data): Observable<any> {
    return this.http.post(
      environment.apiEndPoint + `participant/find-match-opponent`,
      data
    );
  }

  searchParticipant(field, text, tournamentId, pId?): Observable<any> {
    const urlString = pId
      ? `participant/search?field=${field}&text=${encodeURIComponent(
          text
        )}&tournamentId=${tournamentId}&pId=${pId}`
      : `participant/search?field=${field}&text=${encodeURIComponent(
          text
        )}&tournamentId=${tournamentId}`;
    return this.http.get(environment.apiEndPoint + urlString);
  }

  updateParticipant(id, data): Observable<any> {
    return this.http.patch(environment.apiEndPoint + `participant/${id}`, data);
  }

  updateParticipantBulk(data): Observable<any> {
    return this.http.put(
      environment.apiEndPoint + `participant/updateAll`,
      data
    );
  }

  getParticipantTournament(params): Observable<any> {
    const encodedUrl = encodeURIComponent(params.query);
    const encodedPagination = encodeURIComponent(params.pagination);
    const url = `${environment.apiEndPoint}participant/tournament?status=${params.type}&pagination=${encodedPagination}`;
    return this.http.get(url);
  }

  getJoinedTournamentsByUser(params): Observable<any> {
    const encodedUrl = encodeURIComponent(params.query);
    const encodedPagination = encodeURIComponent(params.pagination);
    const url = `${environment.apiEndPoint}admin/participant/tournament?query=${encodedUrl}&pagination=${encodedPagination}`;
    return this.http.get(url);
  }

  /**
   * Delete Tournament details By request Id
   * @param id: Request Id
   */
  deleteTournament(id): Observable<any> {
    return this.http.delete(environment.apiEndPoint + `tournament/${id}`);
  }

  getGames(): Observable<any> {
    return this.http.get(environment.apiEndPoint + `game`);
  }

  getStreamToken(streamId): SubscribableOrPromise<any> {
    return this.http
      .get(`${environment.apiEndPoint}video-streaming?streamId=${streamId}`)
      .toPromise();
  }

  getStreamKey(): SubscribableOrPromise<any> {
    return this.http
      .get(`${environment.apiEndPoint}video-streaming/generate-stream-key`)
      .toPromise();
  }
  updateGWBTournament(formValues, id): Observable<any> {
    const data = formValues;

    return this.http.put(environment.apiEndPoint + `tournament/${id}`, data);
  }
  getPaginatedTournaments(params) {
    const { status, limit, sort, page, game, organizer, prefernce, level2 } =
      params;
    let url = `${environment.apiEndPoint}home/tournament?`;
    if (status != null && status != undefined && status != '') {
      url += `&status=${status}`;
    }
    if (params.tournamentStatus) {
      url += `&tournamentStatus=${params.tournamentStatus}`;
    }
    if (limit != null && limit != undefined && limit != '') {
      url += `&limit=${limit}`;
    }
    if (page) {
      url += `&page=${page}`;
    }
    if (sort) {
      url += `&sort=${sort}`;
    }
    if (game) {
      url += `&game=${game}`;
    }
    if (organizer) {
      url += `&organizer=${organizer}`;
    }
    if (prefernce) {
      url += `&preference=${encodeURIComponent(JSON.stringify(prefernce))}`;
    }
    if (level2) {
      url += `&level2=${level2}`;
    }
    return this.http.get(url);
  }

  fetchMyTournament(payload): SubscribableOrPromise<any> {
    const { status, page, limit } = payload;
    const url = `${environment.apiEndPoint}tournament/my_tournaments?status=${status}&page=${page}&limit=${limit}`;
    return this.http.get(url).toPromise();
  }

  fetchTournamentByStatus(payload): SubscribableOrPromise<any> {
    const { status, page, limit, text } = payload;
    const encodedText = encodeURIComponent(text);
    const url = `${environment.apiEndPoint}tournament/get_all_tournament_by_status?status=${status}&page=${page}&limit=${limit}&text=${encodedText}`;
    return this.http.get(url).toPromise();
  }

  fetchParticipantRegistrationStatus(
    id,
    participantId = null
  ): SubscribableOrPromise<any> {
    let url = `${environment.apiEndPoint}participant/join-status?tournamentId=${id}`;
    if (participantId) {
      url += `&guestId=${participantId}`;
    }
    return this.http.get(url).toPromise();
  }

  fetchSeasonParticipantRegistrationStatus(
    id,
    participantId = null
  ): SubscribableOrPromise<any> {
    let url = `${environment.apiEndPoint}participant/season-join-status?seasonId=${id}`;
    if (participantId) {
      url += `&guestId=${participantId}`;
    }
    return this.http.get(url).toPromise();
  }

  fetchParticipantRegistrationStatusMyTeams(
    id,
    participantId
  ): Observable<any> {
    return this.http.get(
      `${environment.apiEndPoint}participant/join-status-my-teams?tournamentId=${id}&participantId=${participantId}`
    );
  }

  getTournamentBySlug(slug): Observable<any> {
    return this.http.get(environment.apiEndPoint + `tournament/slug/${slug}`);
  }

  disableManualScoreUpdate(data): Observable<any> {
    return this.http.post(
      environment.apiEndPoint + 'tournament/disableManualScoreUpdate',
      data
    );
  }

  saveParticipantDetails = async (payload, tournament) => {
    try {
      // const isFieldValid = Object.values(payload).every((el) => el);

      const isSubMemberFieldsValid = async (isPreviousFieldValid, value) => {
        if (isPreviousFieldValid && value?.length) {
          return value.every((item) => {
            return Object.values(item).every((el) => el);
          });
        } else {
          return isPreviousFieldValid;
        }
      };
      let isFieldValid = Object.values(payload).every((el) => el);
      isFieldValid = await isSubMemberFieldsValid(
        isFieldValid,
        payload?.teamMembers
      );
      isFieldValid = await isSubMemberFieldsValid(
        isFieldValid,
        payload?.substituteMembers
      );
      if (!isFieldValid) {
        this.router.navigate([`/tournament/${tournament?.slug}/join`]);
        return {
          isProcessing: true,
          message: null,
        };
      }

      if (tournament?.participantType == 'individual') {
        const response = await this.searchParticipant(
          'inGamerUserId',
          payload.inGamerUserId,
          tournament?._id
        ).toPromise();
        const isExist = Object.values(response.data).some((el) => el);
        if (isExist) {
          this.router.navigate([`/tournament/${tournament?.slug}/join`]);
          return {
            isProcessing: true,
            message: null,
          };
        }
      }
      if (tournament?.isPaid) {
        this.router.navigate([`/tournament/${tournament?.slug}/join`]);
        return {
          isProcessing: true,
          message: null,
        };
      }

      const saveParticipant = await this.saveParticipant(payload).toPromise();
      return {
        isProcessing: false,
        message: saveParticipant?.message,
      };
    } catch (error) {
      throw error;
    }
  };

  saveSeasonParticipantDetails = async (payload, tournament) => {
    try {
      // const isFieldValid = Object.values(payload).every((el) => el);

      const isSubMemberFieldsValid = async (isPreviousFieldValid, value) => {
        if (isPreviousFieldValid && value?.length) {
          return value.every((item) => {
            return Object.values(item).every((el) => el);
          });
        } else {
          return isPreviousFieldValid;
        }
      };
      let isFieldValid = Object.values(payload).every((el) => el);
      isFieldValid = await isSubMemberFieldsValid(
        isFieldValid,
        payload?.teamMembers
      );
      isFieldValid = await isSubMemberFieldsValid(
        isFieldValid,
        payload?.substituteMembers
      );
      if (!isFieldValid) {
        this.router.navigate([`/match-making/${tournament?.slug}`]);
        return {
          isProcessing: true,
          message: null,
        };
      }

      if (tournament?.participantType == 'individual') {
        const response = await this.searchParticipant(
          'inGamerUserId',
          payload.inGamerUserId,
          tournament?._id
        ).toPromise();
        const isExist = Object.values(response.data).some((el) => el);
        if (isExist) {
          this.router.navigate([`/match-making/${tournament?.slug}`]);
          return {
            isProcessing: true,
            message: null,
          };
        }
      }
      if (tournament?.isPaid) {
        this.router.navigate([`/match-making/${tournament?.slug}`]);
        return {
          isProcessing: true,
          message: null,
        };
      }

      const saveParticipant = await this.saveParticipant(payload).toPromise();
      return {
        isProcessing: false,
        message: saveParticipant?.message,
      };
    } catch (error) {
      throw error;
    }
  };

  fetchRoundDetails(id: string, params: string): SubscribableOrPromise<any> {
    const requestedUrl = `${environment.apiEndPoint}tournament/round-scoring/${id}?${params}`;
    return this.http.get(requestedUrl).toPromise();
  }

  updateRoundDetails(
    id: string,
    params: string,
    payload: any
  ): SubscribableOrPromise<any> {
    const requestedUrl = `${environment.apiEndPoint}tournament/round-scoring/${id}?${params}`;
    return this.http.patch(requestedUrl, payload).toPromise();
  }
}
