import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';

import { IPagination } from '../../shared/models';
import { PageChangedEvent } from 'ngx-bootstrap/pagination';
import { PaginationService } from '../service/pagination.service';

@Component({
  selector: 'app-pagination',
  templateUrl: './custom-pagination.component.html',
  styleUrls: ['./custom-pagination.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CustomPaginationComponent implements OnInit, OnChanges {
  @Input() activePage = 1;
  @Input() page: IPagination;
  @Output() currentPage = new EventEmitter<Number>();

  constructor(private paginationService: PaginationService) {}

  ngOnInit(): void {}

  pageChanged(event: PageChangedEvent) {
    this.currentPage.emit(event.page);
    this.paginationService.setPageChanged(true);
  }

  ngOnChanges(simpleChanges: SimpleChanges) {}
}
