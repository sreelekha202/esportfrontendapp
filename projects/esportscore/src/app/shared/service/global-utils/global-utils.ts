import { Meta, MetaDefinition, Title } from '@angular/platform-browser';
import { Injectable } from '@angular/core';

declare const window: any;

@Injectable({
  providedIn: 'root',
})
export class GlobalUtils {
  static isBrowser() {
    return Boolean(typeof window !== 'undefined');
  }

  static isLTR(): boolean {
    if (GlobalUtils.isBrowser()) {
      if (localStorage && document) {
        const lang = localStorage.getItem('currentLanguage');

        if (lang) {
          return lang === 'en'; // is english LTR else is arabic RTL
        }
        return true;
      }
    }
  }

  constructor(private meta: Meta, private title: Title) {}

  setMetaTags(tags: MetaDefinition[]): void {
    if (tags) {
      tags.forEach((tag) => {
        this.meta.updateTag(tag);
      });
    }
  }
}
