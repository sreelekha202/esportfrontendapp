export const GAMES = {
  DOTA2: 'DOTA 2',
  VALORANT: 'Valorant',
  FORTNITE: 'Fortnite',
  FIFA21: 'FIFA 21',
  CSGO: 'CS:GO'
} as const;
