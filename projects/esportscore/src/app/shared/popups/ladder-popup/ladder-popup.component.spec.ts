import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LadderPopupComponent } from './ladder-popup.component';

describe('LadderPopupComponent', () => {
  let component: LadderPopupComponent;
  let fixture: ComponentFixture<LadderPopupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LadderPopupComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LadderPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
