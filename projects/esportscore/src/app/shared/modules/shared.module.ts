import {
  SWIPER_CONFIG,
  SwiperConfigInterface,
  SwiperModule,
} from 'ngx-swiper-wrapper';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ShareButtonsModule } from 'ngx-sharebuttons/buttons';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { ShareButtonModule } from 'ngx-sharebuttons/button';
import { NgxIntlTelInputModule } from 'ngx-intl-tel-input';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ShareIconsModule } from 'ngx-sharebuttons/icons';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { InlineSVGModule } from 'ng-inline-svg';
import { NgOtpInputModule } from 'ng-otp-input';
import { RouterModule } from '@angular/router';
import { CommonModule, LowerCasePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { DateFnsModule } from 'ngx-date-fns';
import { CarouselModule } from 'primeng/carousel';

declare const zE;

import {
  I18nModule,
  EsportsLanguageService,
  EsportsLoaderModule,
  EsportsSharedModule,
} from 'esports';

import { CustomPaginationModule } from '../../core/custom-pagination/custom-pagination.module';
import { DirectivesModule } from '../../shared/directives/directive.module';
import { DiscussionModule } from '../../shared/components/discussion/discussion.module';
import { EliminationModule } from '../components/elimination/elimination.module';
import { MatchListModule } from '../../shared/components/match-list/match-list.module';
import { MaterialModule } from './material.module';
import { RatingModule } from '../../shared/components/rating/rating.module';

import { InfoPopupComponent } from '../popups/info-popup/info-popup.component';
import { SocialShareComponent } from '../popups/social-share/social-share.component';
// import { TeamCardInviteComponent } from '../../modules/profile/components/team-card-invite/team-card-invite.component';
// import { TournamentItemComponent } from '../../core/tournament-item/tournament-item.component';
// import { UploadImageComponent } from '../components/upload-image/upload-image.component';

import { SidenavService } from '../service/sidenav/sidenav.service';

import { LazyImageDirective } from '../directives/lazy-image.directive';
import { NgxIntlTelPlaceholderDirective } from '../directives/ngx-intl-tel-placeholder.directive';
import { AvatarUpdateComponent } from '../popups/avatar-update/avatar-update.component';
import { LadderPopupComponent } from '../popups/ladder-popup/ladder-popup.component';
import { CountdownModule } from 'ngx-countdown';

import { environment } from '../../../environments/environment';

const DEFAULT_SWIPER_CONFIG: SwiperConfigInterface = {
  direction: 'horizontal',
  slidesPerView: 'auto',
};

const components = [];

const sheets = [];

const popups = [
  InfoPopupComponent,
  SocialShareComponent,
  AvatarUpdateComponent,
  LadderPopupComponent,
];

const directives = [NgxIntlTelPlaceholderDirective, LazyImageDirective];

const modules = [
  EsportsSharedModule,
  // CommonModule,
  // RouterModule,
  // FormsModule,
  // ReactiveFormsModule,
  // MaterialModule,
  EliminationModule,
  NgbModule,
  // FontAwesomeModule,
  NgOtpInputModule,
  NgxIntlTelInputModule,
  // NgxDatatableModule,
  BsDropdownModule,
  SwiperModule,
  InlineSVGModule.forRoot(),
  ShareButtonModule,
  ShareButtonsModule,
  ShareIconsModule,
  CarouselModule,
  CollapseModule.forRoot(),
  RatingModule,
  // PipeModule,
  DirectivesModule,
  MatchListModule,
  CustomPaginationModule,
  I18nModule.forRoot(environment),
  DiscussionModule,
  DateFnsModule.forRoot(),
  EsportsLoaderModule.setColor('#1d252d'),
  CountdownModule,
];

const providers = [
  LowerCasePipe,
  SidenavService,
  {
    provide: SWIPER_CONFIG,
    useValue: DEFAULT_SWIPER_CONFIG,
  },
];

@NgModule({
  imports: [...modules],
  declarations: [...components, ...directives, ...sheets, ...popups],
  exports: [...modules, ...components, ...directives, ...sheets, ...popups],
  providers: [...providers],
})
export class SharedModule {
  constructor(
    private languageService: EsportsLanguageService,
    private translateService: TranslateService
  ) {
    this.languageService.language.subscribe((lang: string) => {
      this.handleLanguageChange(lang);
    });
  }

  private handleLanguageChange(lang: string): void {
    this.translateService.use(lang);

    const isEnglish = lang === 'en';
    const language = isEnglish ? 'en' : 'ar';
    try {
      if (zE && typeof zE == 'function') {
        zE('webWidget', 'setLocale', language);
      }
    } catch (error) {}
  }
}
