import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';

import { LadderPopupComponent } from '../../../../shared/popups/ladder-popup/ladder-popup.component';
@Component({
  selector: 'app-ladder-match',
  templateUrl: './ladder-match.component.html',
  styleUrls: ['./ladder-match.component.scss'],
})
export class LadderMatchComponent implements OnInit {
  @Input() mList: Array<{}> = [];
  @Input() tournament;
  @Input() participantId: string | null;
  @Input() isEoOrAdmin: boolean = false;

  @Output() isRefresh = new EventEmitter<boolean>(false);

  constructor(private router: Router, private matDialog: MatDialog) {}

  ngOnInit(): void {}

  reportScore = async (matchId) => {
    this.router.navigate([`tournament/${this.tournament._id}/match-view`], {
      queryParams: { matchId: matchId },
    });
  };

  reportScoreUpdate = async (matchId) => {
    try {
      const data = {
        isShowReportScoreCard: true,

        payload: {
          tournament: this.tournament,

          match: {
            _id: matchId,
          },

          isEventOrganizerOrAdmin: this.isEoOrAdmin,
        },
      };

      const response = await this.matDialog
        .open(LadderPopupComponent, { data })
        .afterClosed()
        .toPromise();

      this.isRefresh.emit(true);
    } catch (error) {
      this.isRefresh.emit(false);
    }
  };
  getRowClass = (row) => {
    return {
      scoreConflict: row.isScoreConflict == true,
    };
  };
}
