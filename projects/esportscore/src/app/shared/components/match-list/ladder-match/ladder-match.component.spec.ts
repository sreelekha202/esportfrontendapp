import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LadderMatchComponent } from './ladder-match.component';

describe('LadderMatchComponent', () => {
  let component: LadderMatchComponent;
  let fixture: ComponentFixture<LadderMatchComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LadderMatchComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LadderMatchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
