import { Component, OnInit, Input, OnChanges, Output, EventEmitter } from '@angular/core';
import { BracketService, UtilsService, TournamentService } from '../../../core/service';
import { EsportsToastService, IPagination } from 'esports';
import { ScorePopupComponent } from '../../../modules/my-matches/components/score-popup/score-popup.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-match-list',
  templateUrl: './match-list.component.html',
  styleUrls: [
    './match-list.component.scss',
    '../../../modules/tournament/tournament-management/tournament-management.component.scss',
  ],
})
export class MatchListComponent implements OnInit, OnChanges {
  @Input() tournament;
  @Input() participantId: string | null;
  @Input() isAdmin: boolean;
  @Input() isScoreEdit: boolean = false;

  @Output() message = new EventEmitter<any>();

  pageNo = 1;
  page: IPagination;

  round: Array<any> = [];
  stage: Array<any> = [];
  tHeader: Array<any> = [];
  isLoaded = false;
  standingRound: Array<any> = [];
  matchList = [];
  ladderMatchList = [];

  constructor(
    private bracketService: BracketService,
    private eSportsToastService: EsportsToastService,
    private utilsService: UtilsService,
    private matDialog: MatDialog
  ) {}

  ngOnInit(): void {}

  ngOnChanges() {
    if (this.tournament?._id) {

      if (['single', 'double'].includes(this.tournament?.bracketType)) {
        this.fetchDistinctRound();
      } else if (
        ['round_robin', 'battle_royale'].includes(this.tournament?.bracketType)
      ) {
        this.fetchDistinctStage();
      } else if (this.tournament?.bracketType == 'swiss_safeis') {
        this.isLoaded = true;
      } else if (this.tournament?.bracketType == 'ladder') {
        this.isLoaded = true;
        this.fetchLadderMatches();
      }
    }
  }

  fetchDistinctRound = async () => {
    try {
      const queryParam = `?query=${this.utilsService.getEncodedQuery({
        tournamentId: this.tournament?._id,
      })}`;
      const field = this.getFieldType(this.tournament?.bracketType);
      const response = await this.bracketService.fetchDistinctValue(
        queryParam,
        field
      );
      this.round = response.data.map((r) => {
        return {
          id: r,
          isCollapsed: false,
          isLoaded: false,
        };
      });
      this.isLoaded = true;
    } catch (error) {
      this.isLoaded = true;
      this.eSportsToastService.showError(
        error?.error?.message || error?.message
      );
    }
  };
  openScoreCard = async (m) => {
    try {
      const data = {};
      data['match'] = {
        tournamentId: this.tournament,
        _id: m._id,
      };
      data['tournamentId'] = this.tournament._id;
      data['isEventOrganizerOrAdmin'] = this.isAdmin;
      const response = await this.matDialog
        .open(ScorePopupComponent, {
          width: '700px',
          data,
        })
        .afterClosed()
        .subscribe(async () => {
          await this.fetchDistinctRound();
        });
    } catch (error) {}
  };
  fetchDistinctStage = async () => {
    try {
      const queryParam = `?query=${this.utilsService.getEncodedQuery({
        tournamentId: this.tournament?._id,
      })}`;
      const field = this.getFieldType(this.tournament?.bracketType);
      const response = await this.bracketService.fetchDistinctValue(
        queryParam,
        field
      );
      this.stage = response.data.map((r) => {
        return {
          id: r,
          isCollapsed: false,
          isLoaded: false,
        };
      });
      this.isLoaded = true;
    } catch (error) {
      this.isLoaded = true;
      this.eSportsToastService.showError(
        error?.error?.message || error?.message
      );
    }
  };

  fetchDistictStageGroup = async (stage, i) => {
    try {
      if (stage.isCollapsed && !stage.isLoaded) {
        const queryParam = `?query=${encodeURIComponent(
          JSON.stringify({
            tournamentId: this.tournament?._id,
            'currentMatch.stage': stage.id,
          })
        )}`;
        const response = await this.bracketService.fetchDistinctValue(
          queryParam,
          'currentMatch.group'
        );
        this.stage[i].isLoaded = true;
        this.stage[i].group = response.data.map((g) => {
          return {
            id: g,
            isCollapsed: false,
            isLoaded: false,
          };
        });
      }
    } catch (error) {
      this.eSportsToastService.showError(
        error?.error?.message || error?.message
      );
    }
  };

  fetchDistictStageGroupRound = async (stage, group, i, j) => {
    try {
      if (group.isCollapsed && !group.isLoaded) {
        if (this.tournament?.bracketType == 'battle_royale') {
          const queryParam = `?stage=${stage.id}&group=${group.id}&tournamentId=${this.tournament?._id}`;
          const response = await this.bracketService.fetchStanding(queryParam);
          this.stage[i].group[j].standings = response.data;
          this.standingRound = response.data.length
            ? response.data[0].round
            : [];
        } else {
          const queryParam = `?query=${encodeURIComponent(
            JSON.stringify({
              tournamentId: this.tournament?._id,
              'currentMatch.stage': stage.id,
              'currentMatch.group': group.id,
            })
          )}`;
          const response = await this.bracketService.fetchDistinctValue(
            queryParam,
            'currentMatch.round'
          );
          this.stage[i].group[j].round = response.data.map((r) => {
            return {
              id: r,
              isCollapsed: false,
              isLoaded: false,
            };
          });
        }
        this.stage[i].group[j].isLoaded = true;
      }
    } catch (error) {
      this.eSportsToastService.showError(
        error?.error?.message || error?.message
      );
    }
  };

  fetchDistictStageGroupRoundMatch = async (stage, group, round, i, j, k) => {
    try {
      if (round.isCollapsed && !round.isLoaded) {
        const queryParam = `?query=${encodeURIComponent(
          JSON.stringify({
            tournamentId: this.tournament?._id,
            'currentMatch.stage': stage.id,
            'currentMatch.group': group.id,
            'currentMatch.round': round.id,
            matchStatus: { $ne: 'inactive' },
            bye: false,
          })
        )}&select=sets,totalSet,matchNo,winnerTeam,loserTeam,disQualifiedTeam,battleRoyalTeams,teamAWinSet,teamBWinSet,isScoreConflict`;
        const response = await this.bracketService.fetchAllMatches(queryParam);
        this.stage[i].group[j].round[k].match = response.data;
        this.stage[i].group[j].round[k].isLoaded = true;
        const totalSet = response.data.reduce((accumulator, currentValue) => {
          const max =
            currentValue.sets.length > currentValue.totalSet
              ? currentValue.sets.length
              : currentValue.totalSet;
          accumulator = max > accumulator ? max : accumulator;
          return accumulator;
        }, 0);
        response.data.length
          ? this.createSetHeader(totalSet)
          : this.createSetHeader(0);
      }
    } catch (error) {
      this.eSportsToastService.showError(
        error?.error?.message || error?.message
      );
    }
  };

  fetchMatches = async (round, i) => {
    try {
      if (round.isCollapsed && !round.isLoaded) {
        const queryParam = `?query=${encodeURIComponent(
          JSON.stringify({
            tournamentId: this.tournament?._id,
            'currentMatch.round': round.id,
            matchStatus: { $ne: 'inactive' },
            bye: false,
          })
        )}&select=sets,totalSet,matchNo,winnerTeam,loserTeam,disQualifiedTeam,battleRoyalTeams,teamAWinSet,teamBWinSet,isScoreConflict`;
        const response = await this.bracketService.fetchAllMatches(queryParam);
        if (this.tournament?.bracketType == 'battle_royale') {
        } else {
          const totalSet = response.data.reduce((accumulator, currentValue) => {
            const max =
              currentValue.sets.length > currentValue.totalSet
                ? currentValue.sets.length
                : currentValue.totalSet;
            accumulator = max > accumulator ? max : accumulator;
            return accumulator;
          }, 0);
          response.data.length
            ? this.createSetHeader(totalSet)
            : this.createSetHeader(0);
        }
        this.round[i].isLoaded = true;
        this.round[i].match = response.data;
      }
    } catch (error) {
      this.eSportsToastService.showError(
        error?.error?.message || error?.message
      );
    }
  };

  createSetHeader(noOfSet) {
    if (this.tHeader.length < noOfSet) {
      this.tHeader = [];
      for (let i = 0; i < noOfSet; i++) {
        this.tHeader.push({ name: `Set ${i + 1}` });
      }
    }
  }

  remainingtHeader(th, mh) {
    const rHeader = [];
    for (let i = 0; i < th.length - mh.length; i++) {
      rHeader.push({});
    }
    return rHeader;
  }

  getFieldType = (type) => {
    if (['single', 'double'].includes(type)) {
      return 'currentMatch.round';
    } else if (['round_robin', 'battle_royale'].includes(type)) {
      return 'currentMatch.stage';
    } else {
      return 'currentMatch.round';
    }
  };

  fetchLadderMatches = async () => {
    try {
      const queryParam = {
        id: this.tournament?._id,
        page: this.pageNo,
      };
      const match = await this.bracketService.fetchAllLaderMatches(queryParam);
      this.ladderMatchList = match?.data?.docs;
      this.page = {
        totalItems: match?.data?.totalDocs,
        itemsPerPage: match?.data?.limit,
        maxSize: 5,
      };
    } catch (error) {
      this.eSportsToastService.showError(
        error?.error?.message || error?.message
      );
    }
  };

  getResponse(data) {
    if (data) {
      this.fetchLadderMatches();
    }
  }

  pageChanged(page): void {
    this.pageNo = page;
    this.fetchLadderMatches();
  }
}
