import { TranslateService } from '@ngx-translate/core';
import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { AppHtmlRoutes, AppHtmlProfileRoutes } from '../../../app-routing.model';

@Component({
  selector: 'app-header-info',
  templateUrl: './header-info.component.html',
  styleUrls: ['./header-info.component.scss'],
})
export class HeaderInfoComponent implements OnInit {
  @Input() title: string = '';

  AppHtmlRoutes = AppHtmlRoutes;
  AppHtmlProfileRoutes = AppHtmlProfileRoutes;

  constructor(
    private router: Router,
    private location: Location,
    private translateService: TranslateService
  ) {}

  ngOnInit(): void {}
  cancel() {
    this.location.back();
  }
}
