import { I18nModule } from 'esports';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderInfoComponent } from './header-info.component';
import { RouterModule } from '@angular/router';
import { environment } from '../../../../environments/environment'

@NgModule({
  declarations: [HeaderInfoComponent],
  imports: [CommonModule, RouterModule, I18nModule.forRoot(environment)],
  exports: [HeaderInfoComponent],
})
export class HeaderInfoModule {}
