import {
  Component,
  OnInit,
  Input,
  OnChanges,
  SimpleChanges,
  Output,
  EventEmitter,
} from '@angular/core';

@Component({
  selector: 'app-rating',
  templateUrl: './rating.component.html',
  styleUrls: ['./rating.component.scss'],
})
export class RatingComponent implements OnInit, OnChanges {
  @Input() maxStar;
  @Input() currentRate;
  @Input() fontSize;
  @Input() readonly = false;
  @Output() rating = new EventEmitter<number>();
  constructor() {}

  ngOnInit(): void {}

  ngOnChanges(simpleChange: SimpleChanges) {}

  selectValue() {
    if (!this.readonly) {
      this.rating.emit(this.currentRate);
    }
  }
}
