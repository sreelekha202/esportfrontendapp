import { EventEmitter, Output } from '@angular/core';
import { Component, Input, OnInit } from '@angular/core';
import { IMatchSet, IParticipant } from '../../../../models';

@Component({
  selector: 'view-set',
  templateUrl: './view-set.component.html',
  styleUrls: ['./view-set.component.scss'],
})
export class ViewSetComponent implements OnInit {
  @Input() matchSet: IMatchSet;
  @Input() teamA: IParticipant;
  @Input() teamB: IParticipant;
  @Input() setIndex: number = 0;
  @Input() hasParticipantUpdateAccess: boolean = false;
  @Input() hasEoORAdminUpdateAccess: boolean = false;
  @Input() enableScreenshot: boolean = false;
  @Input() params: any;
  @Input() hasDisqualified: boolean = false;
  @Input() isDisableManualScoreUpdate;
  @Output() updatedSet = new EventEmitter<IMatchSet>();
  @Output() submitScore = new EventEmitter<boolean>(false);
  @Output() refreshScoreBoard = new EventEmitter<boolean>(false);

  hasParticipantOneTimeAccess: boolean = true;

  constructor() {}

  ngOnInit(): void {}

  ngOnChanges() {
    if (
      this.params?.participantId == this.teamA?._id &&
      this.params?.matchUpdatedByTeamACount > 0
    ) {
      this.hasParticipantOneTimeAccess = false;
    }

    if (
      this.params?.participantId == this.teamB?._id &&
      this.params?.matchUpdatedByTeamBCount > 0
    ) {
      this.hasParticipantOneTimeAccess = false;
    }
  }

  scoreChanges(value, key, teamId) {
    const hasScoringAccess = this.hasRequiredAccess(teamId);
    if (!hasScoringAccess) return;

    const set = {
      ...this.matchSet,
      [key]: value,
      enableResubmitBtn:
        this.matchSet?.enableResubmitBtn ||
        this.matchSet?.status == 'completed',
      status: 'active',
    };
    this.updatedSet.emit(set);
  }

  screenShotUrl(url, key, teamId) {
    const hasScreenshotAccess = this.hasRequiredAccess(teamId);
    if (!hasScreenshotAccess) return;

    const set = {
      ...this.matchSet,
      [key]: url,
      enableResubmitBtn:
        this.matchSet?.enableResubmitBtn ||
        this.matchSet?.status == 'completed',
      status: 'active',
    };
    this.updatedSet.emit(set);
  }

  submitSetScore() {
    this.submitScore.emit(true);
  }

  onTimerFinished(e: Event) {
    if (e['action'] == 'done') {
      this.refreshScoreBoard.emit(true);
    }
  }

  hasRequiredAccess(teamId) {
    if (this.hasDisqualified) return false;
    if (!this.hasEoORAdminUpdateAccess && !this.hasParticipantUpdateAccess)
      return false;
    if (
      !this.hasEoORAdminUpdateAccess &&
      this.hasParticipantUpdateAccess &&
      !this?.hasParticipantOneTimeAccess
    )
      return false;
    return true;
  }
}
