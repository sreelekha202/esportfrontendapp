import { Component, Input, OnInit } from '@angular/core';
import { IParticipant } from '../../../../models';

@Component({
  selector: 'view-team',
  templateUrl: './view-team.component.html',
  styleUrls: ['./view-team.component.scss'],
})
export class ViewTeamComponent implements OnInit {
  @Input() teamA: IParticipant;
  @Input() teamB: IParticipant;
  @Input() hasDisqualified: boolean = false;

  guestImgUrl: string = 'assets/images/Match/gamer-image.png';

  constructor() {}

  ngOnInit(): void {}
}
