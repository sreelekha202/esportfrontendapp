import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { UploadScreenshotComponent } from '../upload-screenshot/upload-screenshot.component';
import { GlobalUtils } from 'esports';

@Component({
  selector: 'score-counter',
  templateUrl: './score-counter.component.html',
  styleUrls: ['./score-counter.component.scss'],
})
export class ScoreCounterComponent implements OnInit {
  @Input() score: number = 0;
  @Input() screenshotUrl: string | null = '';
  @Input() enableScreenshot: boolean = false;
  @Input() hasScreenshotUpdateAccess: boolean = false;

  @Output() valueChanges = new EventEmitter<number>();
  @Output() screenShotUrl = new EventEmitter<string>();

  constructor(
    private matDialog: MatDialog
  ) { }

  ngOnInit(): void { }

  scoreUpdate(value: number, delta: string) {
    value = Number(value || 0);
    if (delta === 'inc') {
      this.valueChanges.emit(value + 1);
    } else if (delta === 'dec') {
      const val = value <= 0 ? 0 : value - 1;
      this.valueChanges.emit(val);
    } else {
      this.valueChanges.emit(0);
    }
  }

  openScreenshotUploadPopup = async () => {
    try {
      if (this.screenshotUrl) {
        return this.goToUrl();
      }
      const data = {
        screenshotUrl: this.screenshotUrl,
        hasScreenshotUpdateAccess: this.hasScreenshotUpdateAccess
      };
      const response = await this.matDialog
        .open(UploadScreenshotComponent, {
          width: '900px',
          data
        })
        .afterClosed()
        .toPromise();
      if (response?.hasScreenshotUpdateAccess)
        this.screenShotUrl.emit(response?.screenshotUrl);
    } catch (error) {
    }
  }

  goToUrl() {
    if (GlobalUtils.isBrowser()) {
      window.open(this.screenshotUrl, '_blank');
    }
  }
}
