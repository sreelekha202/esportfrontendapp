import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.scss'],
})
export class CarouselComponent implements OnInit {
  @Input() list: Array<number> = [];
  @Input() title: string;
  @Input() loading: boolean = false;

  @Output() emitValue = new EventEmitter<boolean>();

  constructor() {}

  ngOnInit(): void {}

  slideChange(event) {
    this.emitValue.emit(event || 0);
  }
}
