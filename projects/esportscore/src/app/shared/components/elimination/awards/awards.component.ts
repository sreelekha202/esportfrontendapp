import {
  Component,
  OnInit,
  Input,
  OnChanges,
  SimpleChanges,
} from '@angular/core';
import { BracketService } from '../../../../core/service';
import { TranslateService } from '@ngx-translate/core';
import { EsportsLanguageService, EsportsToastService } from 'esports'

@Component({
  selector: 'app-awards',
  templateUrl: './awards.component.html',
  styleUrls: ['./awards.component.scss'],
})
export class AwardsComponent implements OnInit, OnChanges {
  @Input() tournamentId;
  @Input() type: string;
  championList = [];

  constructor(
    private bracketService: BracketService,
    private eSportsToastService: EsportsToastService,
    private translateService: TranslateService,
    private languageService: EsportsLanguageService
  ) {}

  ngOnInit(): void {
    this.languageService.language.subscribe((lang) =>
      this.translateService.use(lang || 'en')
    );
  }

  fetchWinnerList = async () => {
    try {
      const { data } = await this.bracketService.fetchWinnerList(
        this.tournamentId
      );
      this.championList = data;
    } catch (error) {
      this.eSportsToastService.showError(error?.error?.message || error?.message);
    }
  };

  ngOnChanges(changes: SimpleChanges): void {
    if (
      changes.hasOwnProperty('tournamentId') &&
      changes.tournamentId.currentValue &&
      changes.hasOwnProperty('type') &&
      changes.type.currentValue
    ) {
      this.fetchWinnerList();
    }
  }
}
