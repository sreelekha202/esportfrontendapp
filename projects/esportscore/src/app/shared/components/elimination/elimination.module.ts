import { MatSidenavModule } from '@angular/material/sidenav';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SingleEliminationComponent } from './single-elimination/single-elimination.component';
import { DoubleEliminationComponent } from './double-elimination/double-elimination.component';
import { MatchComponent } from './match/match.component';
import { ScoreCardComponent } from './score-card/score-card.component';
import { EliminationComponent } from './elimination.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AwardsComponent } from './awards/awards.component';
import { ProgressbarModule } from 'ngx-bootstrap/progressbar';
import {
  FontAwesomeModule,
  FaIconLibrary,
} from '@fortawesome/angular-fontawesome';
import {
  faTrophy,
  faPencilAlt,
  faSortUp,
  faSortDown,
  faCommentAlt,
  faCaretSquareUp,
  faCheckCircle,
} from '@fortawesome/free-solid-svg-icons';

import { CarouselModule } from 'ngx-bootstrap/carousel';
import { TabsModule } from 'ngx-bootstrap/tabs';

import { FormsModule } from '@angular/forms';
import { CounterComponent } from './counter/counter.component';
import { BattleRoyaleMultiComponent } from './battle-royale-multi/battle-royale-multi.component';
import { RoundRobinMultiComponent } from './round-robin-multi/round-robin-multi.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatIconModule } from '@angular/material/icon';
import { SwissSystemComponent } from './swiss-system/swiss-system.component';
import { SwissStandingComponent } from './swiss-system/swiss-standing/swiss-standing.component';
import { CarouselComponent } from './carousel/carousel.component';
import { environment } from '../../../../environments/environment';
import { TableHeaderCellComponent } from './battle-royale-multi/table-header-cell/table-header-cell.component';
import { TableSliderCellComponent } from './battle-royale-multi/table-slider-cell/table-slider-cell.component';
import { TableCounterCellComponent } from './battle-royale-multi/table-counter-cell/table-counter-cell.component';
import { ScoreConfirmPopupComponent } from './battle-royale-multi/score-confirm-popup/score-confirm-popup.component';
import { ViewTeamComponent } from './score-card/view-team/view-team.component';
import { ViewSetComponent } from './score-card/view-set/view-set.component';
import { ScoreCounterComponent } from './score-card/score-counter/score-counter.component';
import { UploadScreenshotComponent } from './score-card/upload-screenshot/upload-screenshot.component';
import { TeamComponent } from './match/team/team.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import {
  EsportsModule, I18nModule,
  EsportsLoaderModule,
  PipeModule,
  EsportsEliminationModule
} from 'esports';
import { ParticipantTypePipe } from './pipe/participant-type.pipe';
import { FinalResultComponent } from './score-card/final-result/final-result.component';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatTabsModule } from '@angular/material/tabs';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { JwtInterceptor } from '../../../core/helpers/interceptors/token-interceptors.service';
import { LazyLoadImageModule } from "ng-lazyload-image";
import { NgxFlagIconCssModule } from "ngx-flag-icon-css";
import { CountdownModule } from 'ngx-countdown';
@NgModule({
  declarations: [
    SingleEliminationComponent,
    DoubleEliminationComponent,
    SwissSystemComponent,
    MatchComponent,
    ScoreCardComponent,
    EliminationComponent,
    AwardsComponent,
    CounterComponent,
    BattleRoyaleMultiComponent,
    RoundRobinMultiComponent,
    SwissStandingComponent,
    CarouselComponent,
    ParticipantTypePipe,
    FinalResultComponent,
    TableHeaderCellComponent,
    TableSliderCellComponent,
    TableCounterCellComponent,
    ScoreConfirmPopupComponent,
    ViewTeamComponent,
    ViewSetComponent,
    ScoreCounterComponent,
    UploadScreenshotComponent,
    TeamComponent
  ],
  imports: [
    CommonModule,
    NgbModule,
    FontAwesomeModule,
    FormsModule,
    MatSidenavModule,
    ProgressbarModule.forRoot(),
    PipeModule,
    CarouselModule.forRoot(),
    TabsModule.forRoot(),
    MatProgressSpinnerModule,
    MatIconModule,
    I18nModule.forRoot(environment),
    MatTooltipModule,
    MatTabsModule,
    EsportsModule.forRoot(environment),
    EsportsEliminationModule.forRoot(environment),
    EsportsLoaderModule.setColor('#501090'),
    LazyLoadImageModule,
    NgxFlagIconCssModule,
    CountdownModule,
    NgxDatatableModule
  ],
  exports: [
    EliminationComponent,
    MatSidenavModule,
    ScoreCardComponent,
    FinalResultComponent,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptor,
      multi: true,
    },
  ],
})
export class EliminationModule {
  constructor(private library: FaIconLibrary) {
    library.addIcons(
      faTrophy,
      faPencilAlt,
      faSortUp,
      faSortDown,
      faCommentAlt,
      faCaretSquareUp,
      faCheckCircle
    );
  }
}
