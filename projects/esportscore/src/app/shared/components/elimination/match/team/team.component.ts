import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-team',
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.scss']
})
export class TeamComponent implements OnInit {
  @Input() bye: boolean = false;
  @Input() country: string;
  @Input() countryList: Array<any> = [];
  @Input() isDisqualify: boolean;
  @Input() isShowCountryFlag: boolean = false;
  @Input() logo: string;
  @Input() placeholder: string;
  @Input() setScore: number;
  @Input() teamName: string;
  
  countryCode = null;

  constructor() { }

  ngOnInit( ) { }

  ngOnChanges(simpleChanges: SimpleChanges) {
    if (this.country && this.countryList?.length) {
      this.setCountryCode(this.country, this.countryList);
    }
  }

  setCountryCode(country, countryList) {
    const selectedCountry = countryList.find((c) => c?.name == country);
    this.countryCode = selectedCountry
      ? selectedCountry?.sortname.toLowerCase()
      : null;
  }

}
