import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BattleRoyaleMultiComponent } from './battle-royale-multi.component';

describe('BattleRoyaleMultiComponent', () => {
  let component: BattleRoyaleMultiComponent;
  let fixture: ComponentFixture<BattleRoyaleMultiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BattleRoyaleMultiComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BattleRoyaleMultiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
