import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'table-header-cell',
  templateUrl: './table-header-cell.component.html',
  styleUrls: ['./table-header-cell.component.scss'],
})
export class TableHeaderCellComponent implements OnInit {
  @Input() title: string;
  @Input() subTitle: string;
  @Input() sortOrder: number = 0;
  @Input() activeField: boolean;
  @Input() isFirstChild: boolean;

  constructor() {}

  ngOnInit(): void {}
}
