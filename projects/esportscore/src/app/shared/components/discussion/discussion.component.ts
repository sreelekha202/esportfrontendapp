import { Component, Input, OnInit, OnChanges } from '@angular/core';
import { CommentService } from '../../../core/service';
import { toggleOpacity, VisibilityState } from '../../../animations';
import {
  InfoPopupComponent,
  InfoPopupComponentData,
  InfoPopupComponentType,
} from '../../popups/info-popup/info-popup.component';
import { MatDialog } from '@angular/material/dialog';
import { TranslateService } from '@ngx-translate/core';
import { formatDistanceToNow } from 'date-fns';
import { EsportsUserService, EsportsToastService } from 'esports';
@Component({
  selector: 'app-discussion',
  templateUrl: './discussion.component.html',
  styleUrls: ['./discussion.component.scss'],
  animations: [toggleOpacity],
})
export class DiscussionComponent implements OnInit, OnChanges {
  @Input() type: string;
  @Input() objectId: string;
  @Input() userId: string;
  @Input() allowComment: boolean = false;
  @Input() text: string;
  @Input() creatorId;

  VisibilityState = VisibilityState;
  collapseOrExpand: VisibilityState = VisibilityState.Visible;

  comments: Array<any> = [];
  page: number = 1;
  allowDelete: boolean = false;

  isLoadingNextComments: boolean = false;
  hasNextComment: boolean = true;

  constructor(
    private userService: EsportsUserService,
    private commentService: CommentService,
    private eSportsToastService: EsportsToastService,
    private matDialog: MatDialog,
    private translateService: TranslateService
  ) {}

  ngOnInit(): void {
    this.getCurrentUserDetails();
  }

  getCurrentUserDetails() {
    this.userService.currentUser.subscribe((data) => {
      if (data) {
        if (data.accountType == 'admin') {
          this.allowDelete = true;
        }
        if (data._id == this.creatorId) {
          this.allowDelete = true;
        }
      }
    });
  }

  ngOnChanges() {
    if (this.type && this.objectId) {
      this.comments = [];
      this.page = 1;
      this.getAllComment();
    }
  }

  getAllComment = async () => {
    try {
      if (this.isLoadingNextComments) return;
      this.isLoadingNextComments = true;
      const { data, hasNext } = await this.commentService.getAllComment(
        this.type,
        this.objectId,
        this.page
      );

      this.comments = [...this.comments, ...data];
      this.page += this.page;
      this.hasNextComment = hasNext;
      this.isLoadingNextComments = false;
    } catch (error) {
      this.isLoadingNextComments = false;
      this.eSportsToastService.showError(error?.error?.message || error?.message);
    }
  };

  getAllReply = async (commentId, index) => {
    try {
      if (this.comments[index].isLoadingNextReplies) return;

      this.comments[index].page = this.comments[index].page || 1;
      this.comments[index].isLoadingNextReplies =
        this.comments[index].isLoadingNextReplies || true;

      const { data, hasNext } = await this.commentService.getAllComment(
        'comment',
        commentId,
        this.comments[index].page
      );

      this.comments[index].replies = [
        ...(this.comments[index].replies || []),
        ...data,
      ];
      this.comments[index].page += this.comments[index].page;
      this.comments[index].hasNextReply = hasNext;

      this.comments[index].isLoadingNextReplies = !this.comments[index]
        .isLoadingNextReplies;

      this.comments[index].showReplies = true;
      this.comments[index].totalReplies = this.comments[index]?.replies?.length < 50 ? this.comments[index]?.replies?.length: this.comments[index].totalReplies;

    } catch (error) {
      this.comments[index].isLoadingNextReplies = !this.comments[index]
        .isLoadingNextReplies;
      this.eSportsToastService.showError(error?.error?.message || error?.message);
    }
  };

  getCommentResponse = (payload) => {
    this.comments.unshift(payload);
    this.comments.sort((a, b) => b.isPinned - a.isPinned)
  };

  getUpdatedCommentResponse = (payload, i) => {
    this.comments[i] = { ...this.comments[i], ...payload };
    this.comments[i].isEdit = false;
  };

  getCommentCancelResponse = (payload, i) => {
    this.comments[i].isEdit = false;
  };

  getReplyResponse = (payload, i) => {
    if (this.comments[i]?.showReplies) {
      if (!this.comments[i]?.replies?.length) {
        this.comments[i].replies = [payload];
      } else {
        this.comments[i].replies.unshift(payload);
      }
    }
    this.comments[i].totalReplies = (this.comments[i].totalReplies || 0) + 1;
    this.comments[i].openReply = false;
  };

  getReplyCancelResponse = (event, i) => {
    this.comments[i].openReply = false;
  };

  getUpdatedReplyResponse = (payload, i, j) => {
    this.comments[i].replies[j] = {
      ...this.comments[i].replies[j],
      ...payload,
    };
    this.comments[i].replies[j].isEdit = false;
  };

  likeOrUnLikeComment = async (
    objectId: string,
    objectType: string,
    type: number,
    i = 0,
    j = 0,
    isReply = false
  ) => {
    try {
      if (!this.userId) {
        this.eSportsToastService.showInfo('Please login to like this comment');
        return;
      }

      // if (!this.allowComment) {
      //   this.eSportsToastService.showInfo(
      //     'Please Join this tournament to like this comment'
      //   );
      //   return;
      // }

      const payload = {
        objectId,
        objectType,
        type: type == -1 ? 1 : Number(!type),
      };

      const like = await this.commentService.upsertLike(payload);
      if (isReply) {
        this.comments[i].replies[j] = {
          ...this.comments[i].replies[j],
          ...like.data,
        };
      } else {
        this.comments[i] = { ...this.comments[i], ...like.data };
      }
    } catch (error) {
      this.eSportsToastService.showError(error?.error?.message || error?.message);
    }
  };

  deleteCommentOrReply = async (id, i, j = -1) => {
    try {
      const data: InfoPopupComponentData = {
        title: this.translateService.instant('DISCUSSION.DELETE_COMMENT_TITLE'),
        text: this.translateService.instant('DISCUSSION.DELETE_COMMENT_TEXT'),
        type: InfoPopupComponentType.confirm,
        btnText: this.translateService.instant('BUTTON.CONFIRM'),
      };

      const confirmed = await this.matDialog
        .open(InfoPopupComponent, { data })
        .afterClosed()
        .toPromise();

      if (confirmed) {
        const deleteComment = await this.commentService.deleteComment(id);
        if (deleteComment.data) {
          if (j > -1) {
            this.comments[i].replies.splice(j, 1);
            this.comments[i].totalReplies -= this.comments[i].totalReplies;
          } else {
            this.comments.splice(i, 1);
          }
          this.eSportsToastService.showSuccess(deleteComment?.message);
        }
      }
    } catch (error) {
      this.eSportsToastService.showError(error?.error?.message || error?.message);
    }
  };

  getDateFromNow(date) {
    return formatDistanceToNow(new Date(date), { addSuffix: true });
  }

  async pinComment(id) {
    await this.commentService.pinToComment(id);
    this.comments = this.comments.map(item => {
      if(item._id === id) {
        return {
          ...item,
          isPinned: !item.isPinned || false
        }
      } else {
        return item;
      }
    }).sort((a, b) => {
      let n = b.isPinned - a.isPinned;
      if (n !== 0) {
          return n;
      }
      return b.modifiedOn - a.modifiedOn;
    })
  }
}
