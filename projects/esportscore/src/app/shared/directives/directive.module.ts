import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OnlyNumberDirective } from './only-number.directive';
import { NgbdSortableHeader } from './sort-table.directive';

@NgModule({
  declarations: [OnlyNumberDirective, NgbdSortableHeader],
  imports: [CommonModule],
  exports: [OnlyNumberDirective, NgbdSortableHeader],
})
export class DirectivesModule {}
