import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterBackDirective } from './router-back.directive';

@NgModule({
  declarations: [RouterBackDirective],
  imports: [CommonModule],
  exports: [RouterBackDirective],
})
export class RouterBackModule {}
