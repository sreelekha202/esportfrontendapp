interface TMatch {
  tournamentID?: string;
  participants?: Array<String>;
  tournamentName?: string;
  LastMessage?: string;
  LastMessageDateAndTime?: string;
  tournamentLogo?: string;
  matchId?: string;
}

export { TMatch };
