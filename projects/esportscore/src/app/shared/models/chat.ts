export interface IChat {
  attachmenturl?: string;
  chatusername?: string;
  filesattached?: Array<any>;
  fullName?: string;
  matchid?: string;
  matchImage?: string;
  matchLocation?: string;
  matchName?: string;
  messagetype?: string;
  msg?: string;
  thumbnailurl?: string;
  tournamentid?: string;
  typeofchat?: string;
  updatedAt?: number;
  userid?: string;
  receiverid?: string;
  _id?: string;
  deleteflag?: string;
}
