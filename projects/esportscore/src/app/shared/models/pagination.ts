export interface IPagination {
  totalItems: number;
  itemsPerPage: number;
  maxSize: number;
}
