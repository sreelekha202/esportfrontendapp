export interface IVideoLibrary {
  id?: string;
  title?: string;
  description?: string;
  youtubeUrl?: string;
  thumbnailUrl?: string;
  createdOn?: Date;
  updatedOn?: Date;
  category?: string;
  game?: string;
  tags?: Tag[];
  platforms?: Platform[];
  genres?: Genre[];
  createdBy?: string;
  updatedBy?: string;
  status?: number;
}

interface Tag {
  name?: string;
  value?: string;
}

interface Platform {
  name?: string;
  value?: string;
}

interface Genre {
  name?: string;
  value?: string;
}
