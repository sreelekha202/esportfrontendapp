import { MetaDefinition } from '@angular/platform-browser';

export interface AppRoutesData {
  isRootPage?: boolean;
  tags?: MetaDefinition[];
  title?: string;
}

// Use this for routerLing and ts version of it like .navigate()
export enum AppHtmlRoutes {
  wallet = '/wallet',
  walletreport = '/walletreport',
  walletcheckout = '/walletcheckout',
  aboutUs = '/about-us',
  admin = '/admin',
  antiSpamPolicy = '/anti-spam-policy',
  article = '/article',
  articleEdit = '/article/edit',
  bracket = '/bracket',
  bracketCreate = '/bracket/create',
  codeOfConduct = '/code-of-conduct',
  comingSoon = '/comingsoon',
  createTeam = '/create-team',
  dec = '/dec',
  games = '/games',
  transactions='/transactions',
  paymethods='/paymentmethods',
  topup='/topup',
  gamesLobby = '/games/lobby',
  gamesMatchSeason = '/games/match-season',
  help = '/help',
  home = '/home',
  leaderboard = '/leaderboard',
  monitoring = '/monitoring',
  login = '/login',
  loginEmail = '/login-email',
  manageTeam = '/manage-team/',
  myMatches = '/my-matches',
  notFound = '/404',
  privacyPolicy = '/privacy-policy',
  register = '/register',
  registerEmail = '/register-email',
  search = '/search',
  searchArticle = '/search/article',
  searchShop = '/search/shop',
  searchTournament = '/search/tournament',
  searchVideo = '/search/video',
  shop = '/shop',
  shopOrder = '/shop/order',
  shopVoucher = '/shop/voucher',
  specialOffer = '/special-offer',
  termsOfUse = '/terms-of-use',
  topUp = '/top-up',
  tournament = '/tournament',
  tournamentCreate = '/tournament/create',
  createQuickAndAdvanceTournament = '/tournament/quick-advance',
  tournamentEdit = '/tournament/edit/',
  tournamentInfo = '/tournament/info',
  tournamentManage = '/tournament/manage/',
  tournamentView = '/tournament',
  userPageType = '/user',
  videos = '/videos',
  videosView = '/videos/view',
}

export enum AppHtmlRoutesLoginType {
  createPass = 'createPass',
  emailLogin = 'email-login',
  forgotPass = 'forgotPass',
  phoneLogin = 'phone-login',
  registration = 'registration',
  registrationAsGameCenter = 'registration-game-center',
  registrationAsSponser = 'registration-sponser',
  registrationAsOrganizer = 'registration-organizer',
  selectProfile = 'selectProfile',
}

export enum AppHtmlProfileRoutes {
  AppReferralComponent = '/profile/app-referral',
  basicInfo = '/profile/basic-info',
  gamePlayStats = '/profile/game-play-stats/',
  inbox = '/profile/inbox',
  myBookmarks = '/profile/my-bookmarks',
  myBracket = '/profile/my-bracket',
  myContent = '/profile/my-content',
  myStats = '/profile/my-statistics',
  myTournamentCreated = '/profile/my-tournament/created',
  myTournamentJoined = '/profile/my-tournament/joined',
  myTransactions = '/profile/my-transactions',
  setting = '/profile/profile-setting',
  teams = '/profile/teams',
  teamsView = '/profile/teams/',
  myNetwork='/profile/my-network/',
}

export enum AppHtmlAdminRoutes {
  accessManagement = '/admin/access-management',
  contentManagement = '/admin/content-management',
  dashboard = '/admin/dashboard',
  esportsManagement = '/admin/esports-management',
  siteConfiguration = '/admin/site-configuration',
  spamManagement = '/admin/spam-management',
  teamManagement = '/admin/team-management',
  teamManagementEdit = '/admin/team-management/edit',
  editTeam = '/edit-team',
  userManagement = '/admin/user-management',
  userManagementView = '/admin/user-management/view',
  userNotifications = '/admin/user-notifications',
  seasonManagement = '/admin/season-management',
  seasonManagementCreate = '/admin/season-management/create',
}

export enum AppHtmlTeamRegRoutes {
  newTeam = '/team-registration',
  teammates = '/team-registration/teammates',
  registration = '/team-registration/registration',
}

export enum AppHtmlMatchmakingRoutes {
  matchMaking = '/match-making',
  teamGame = '/match-making/team-game',
  teamMembers = '/match-making/team-members',
}
