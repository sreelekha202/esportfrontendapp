export const environment = {
  production: false,
  buildConfig: 'stage',
  // apiEndPoint: 'https://w5chwyhsr3.execute-api.ap-southeast-1.amazonaws.com/dev/',
  apiEndPoint:
    'https://t68yc73nu8.execute-api.ap-southeast-1.amazonaws.com/stage/',
  currentToken: 'DUID',
  facebookAPPID: '1578462858982905',
  googleAPPID:
    '183623623972-6aerbro3aj790tj37f23kfcohu2k3jb7.apps.googleusercontent.com',
  socketEndPoint: 'https://chat-mesf.dynasty-staging.com',
  cookieDomain: '.exaltaretech.com',
  eCommerceAPIEndPoint:
    'https://tuub60cp6h.execute-api.ap-southeast-1.amazonaws.com/dev/',
  firebase: {
    apiKey: 'AIzaSyB1mEgrCbtK1vvE4zR7VcOzXtcWs4ysvLw',
    authDomain: 'dynasty-test.firebaseapp.com',
    databaseURL: 'https://dynasty-test.firebaseio.com',
    projectId: 'dynasty-test',
    storageBucket: 'dynasty-test.appspot.com',
    messagingSenderId: '415594613035',
    appId: '1:415594613035:web:c0a1d274208530618add6e',
    measurementId: 'G-YBTGYLZ7RN',
  },
  articleS3BucketName: 'article',
  tournamentS3BucketName: 'tournament',
  iOSBundelName: 'mesfmobileapp',
  iOSAppstoreURL: 'https://apps.apple.com/us/app/mesf/id1529620569',
};
