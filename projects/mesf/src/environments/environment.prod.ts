export const environment = {
  production: true,
  buildConfig: 'prod',
  // apiEndPoint: 'https://w5chwyhsr3.execute-api.ap-southeast-1.amazonaws.com/dev/',
  apiEndPoint:
    'https://i24fvi4e1g.execute-api.ap-southeast-1.amazonaws.com/prod/',
  currentToken: 'DUID',
  facebookAPPID: '1578462858982905',
  googleAPPID:
    '183623623972-6aerbro3aj790tj37f23kfcohu2k3jb7.apps.googleusercontent.com',
  socketEndPoint: 'https://chat.mesf.gg',
  cookieDomain: '.mesf.gg',
  eCommerceAPIEndPoint:
    'https://a67trhqoc8.execute-api.ap-southeast-1.amazonaws.com/prod/',
  firebase: {
    apiKey: 'AIzaSyB1mEgrCbtK1vvE4zR7VcOzXtcWs4ysvLw',
    authDomain: 'dynasty-test.firebaseapp.com',
    databaseURL: 'https://dynasty-test.firebaseio.com',
    projectId: 'dynasty-test',
    storageBucket: 'dynasty-test.appspot.com',
    messagingSenderId: '415594613035',
    appId: '1:415594613035:web:c0a1d274208530618add6e',
    measurementId: 'G-YBTGYLZ7RN',
  },
  articleS3BucketName: 'article',
  tournamentS3BucketName: 'tournament',
  iOSBundelName: 'mesfmobileapp',
  iOSAppstoreURL: 'https://apps.apple.com/us/app/mesf/id1529620569',
};
