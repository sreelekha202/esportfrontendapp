import {
  AfterViewInit,
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  OnDestroy,
} from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { MatSidenav } from '@angular/material/sidenav';
import { filter, map, take } from 'rxjs/operators';
import { Title } from '@angular/platform-browser';

import { ChatSidenavService } from './shared/service/chat/chat-sidenav.service';
import { GlobalUtils } from './shared/service/global-utils/global-utils';
import { PaginationService, LanguageService } from './core/service';
import { ConstantsService, UserService, ChatService } from './core/service';
import { environment } from '../environments/environment';
import { AppRoutesData } from './app-routing.model';
import { IUser } from './shared/models';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';

declare var gtag;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild('chatSidenav') public chatSidenav: MatSidenav;
  @ViewChild('menuSidenav') public menuSidenav: MatSidenav;
  @ViewChild('appMainConatiner', { read: ElementRef })
  public scroll: ElementRef;
  isRootPage = false;
  isAdminPage = false;
  currenUser: IUser;
  isUserLoggedIn = false;
  currentUserId: String = '';
  currentUserName: String = '';
  matchlist: any;
  userlist: any;
  initialUserList = [];
  showChat: boolean = false;
  matchdetails: any;
  typeofchat: any;
  chatWindows = [];
  isSpin = false;
  isSpin2 = false;
  spamcomment: any;
  showSearchListTitle = false;
  windowposition: String = 'chat_window chat_window_right_drawer';
  @ViewChild('searchinput') searchinput: ElementRef;
  useridtoblock: any;

  constructor(
    public router: Router,
    private activatedRoute: ActivatedRoute,
    private userService: UserService,
    private globalUtils: GlobalUtils,
    private chatSidenavService: ChatSidenavService,
    private paginationService: PaginationService,
    private titleService: Title,
    private chatService: ChatService,
    private modalService: NgbModal,
    private translate: TranslateService,
    private languageService: LanguageService
  ) {
    this.globalRouterEvents(); // should be here for SSR

    if (GlobalUtils.isBrowser()) {
      if (localStorage.getItem('currentLanguage')) {
        const lang = localStorage.getItem('currentLanguage');
        this.translate.use(lang);
        this.languageService.setLanguage(lang);
      }
      this.checkAdmin();

      window.addEventListener('storage', (event) => {
        // let token = localStorage.getItem(environment.currentToken);
        // if (token == undefined) {
        //   window.location.reload();
        // }
        if (event.newValue && event.key === environment.currentToken) {
          window.location.reload();
        }
        if (event.oldValue && event.key === environment.currentToken) {
          window.location.reload();
        }
      });

      const naveEndEvents = router.events.pipe(
        filter((event) => event instanceof NavigationEnd)
      );
      naveEndEvents.subscribe((event: NavigationEnd) => {
        gtag('config', 'G-ZBDK4LLBTQ', {
          page_path: event.urlAfterRedirects,
        });
      });
    }
  }

  searchUsers(event) {
    if (event.keyCode == 13) {
      this.isSpin = true;
      this.showSearchListTitle = false;
      this.chatService
        .searchUsers(this.searchinput?.nativeElement.value)
        .subscribe(
          (res: any) => {
            this.isSpin = false;
            this.showSearchListTitle = true;
            this.userlist = res.data;
          },
          (err) => {}
        );
    }
  }

  toggleChat(chatwindow) {
    if (this.currentUserId) {
      this.chatService?.getAllMatches(this.currentUserId);
    }
    this.typeofchat = chatwindow.type;
    if (chatwindow.type == 'tournament') {
      this.matchdetails = chatwindow;
    } else {
      let matchid = chatwindow._id + '-' + this.currentUserId;
      this.matchdetails = matchid;
      this.showChat = true;
    }

    this.chatService.setWindowPos(this.windowposition);
    this.chatService.setCurrentMatch(this.matchdetails);
    this.chatService.setTypeOfChat(this.typeofchat);
    this.chatService.setChatStatus(true);
    if (this.chatService.getChatStatus() == false) {
      this.chatService?.disconnectMatch(
        this.matchdetails?._id,
        this.typeofchat
      );
    }
  }

  ngOnInit(): void {
    this.paginationService.pageChanged.subscribe(
      (res) => {
        if (res) {
          this.scroll.nativeElement.scrollTop = 0;
        }
      },
      (err) => {}
    );

    this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.currenUser = data;
        this.isUserLoggedIn = !!this.currenUser;
        this.currentUserId = this.currenUser._id;

        this.chatService.mtches.subscribe((matcList) => {
          this.isSpin2 = true;
          this.chatWindows = matcList.allchatwindows;
        });

        if (this.chatWindows?.length == 0) {
          this.chatService?.getAllMatches(this.currentUserId);
        }

        this.chatService.chatStatus.subscribe((cstatus) => {
          this.showChat = cstatus;
        });
      } else {
        //this.userService.getAuthenticatedUser();
      }
    });
  }

  ngOnDestroy(): void {
    this.chatService.setChatStatus(false);
    this.chatService?.disconnectMatch(this.matchdetails?._id, this.typeofchat);
  }

  ngAfterViewInit(): void {
    this.chatSidenavService.setChatSidenav(this.chatSidenav);
    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
        if (this.scroll?.nativeElement?.scrollTop) {
          this.scroll.nativeElement.scrollTop = 0;
        }
        return;
      }
      if (this.scroll?.nativeElement?.scrollTop) {
        this.scroll.nativeElement.scrollTop = 0;
      }
    });
  }

  onActivate(): void {
    // window.scroll(0, 0);
  }

  onMenuClick(): void {
    this.menuSidenav.toggle();
  }

  // Get the account type
  checkAdmin(): void {
    this.userService.currentUser
      .pipe(
        filter((data) => !!data),
        take(1)
      )
      .subscribe((data) => {
        if (data) {
          if (data.accountType === ConstantsService.AccountType.Admin) {
            this.isAdminPage = true;
          } else {
            this.isAdminPage = false;
          }
        } else {
          this.isAdminPage = false;
        }
      });
  }

  private globalRouterEvents(): void {
    this.router.events
      .pipe(
        filter((event) => event instanceof NavigationEnd),
        map(() => this.activatedRoute.root.firstChild.snapshot.data)
      )
      .subscribe((data: AppRoutesData) => {
        this.isRootPage = data && data.isRootPage;

        const title = data && data.title;
        const tags = data && data.tags;
        if (title) {
          this.titleService.setTitle(title);
        }
        if (tags) {
          this.globalUtils.setMetaTags(tags);
        }
      });
  }

  blockUser(content, userid) {
    this.useridtoblock = userid;

    this.modalService.open(content, {
      centered: true,
      windowClass: 'custom-modal-content',
    });
  }

  onConfirmSpam() {
    this.chatService
      ?.blockUser(this.useridtoblock, this.spamcomment)
      .subscribe((resp: any) => {
        if (resp.status == 'success') {
          this.chatService?.getAllMatches(this.currentUserId);
          this.useridtoblock = '';
          this.modalService.dismissAll();
          this.spamcomment = '';
        }
      });
  }
}
