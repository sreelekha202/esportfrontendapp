import { XoxUnityComponent } from './modules/tournament/xox-unity/xox-unity.component';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { GamesComponent } from './modules/games/games.component';
import { LeaderBoardPlayerComponent } from './modules/leader-board-player/leader-board-player.component';
import { PrivacyPolicyComponent } from './modules/privacy-policy/privacy-policy.component';
import { TermsAndConditionsComponent } from './modules/terms-and-conditions/terms-and-conditions.component';
import { LeaderBoardComponent } from './modules/leader-board/leader-board.component';
import { NotfoundComponent } from './modules/not-found/not-found.component';
import { AboutUsComponent } from './modules/about-us/about-us.component';
import { TopUpComponent } from './modules/top-up/top-up.component';
import { Home2Component } from './modules/home2/home2.component';
import { AuthGuard } from './shared/guard/auth.guard';
import { GenerasiBijakComponent } from './modules/tournament/generasi-bijak/generasi-bijak.component';

const routes: Routes = [
  { path: 'leaderboard', component: LeaderBoardComponent },
  { path: 'leaderboard/:userId', component: LeaderBoardPlayerComponent },
  { path: 'home2', component: Home2Component },
  {
    path: 'games',
    component: GamesComponent,
  },
  {
    path: 'home',
    component: Home2Component,
    data: {
      tags: [
        {
          name: 'description',
          content:
            'MESF is the complete esports management platform. Create and join tournaments for free, meet like-minded gamers, pitch yourself against the best in Malaysia.',
        },
        {
          property: 'og:description',
          content:
            'MESF is the complete esports management platform. Create and join tournaments for free, meet like-minded gamers, pitch yourself against the best in Malaysia.',
        },
        {
          property: 'twitter:description',
          content:
            'MESF is the complete esports management platform. Create and join tournaments for free, meet like-minded gamers, pitch yourself against the best in Malaysia.',
        },
        {
          name: 'keywords ',
          content: 'esports , MESF, Malasian eSprts Federation, tournament',
        },
        {
          name: 'title',
          content: 'MESF.gg - Malaysia Esports Federation',
        },
      ],
      title: 'MESF.gg - Malaysia Esports Federation',
    },
  },
  {
    path: 'about-us',
    component: AboutUsComponent,
    data: {
      tags: [
        {
          name: 'description',
          content:
            "Malaysia Electronic Sports Federation or MESF is the governing body for all electronic sports in Malaysia. The federation strives to provide continuous support and growth of the entire sports ecosystem nationally and promotes esports for all; gamers, industry, and nation-binding. Find out more about the team and our 'Esports For All' mission.",
        },
        {
          property: 'og:description',
          content:
            "Malaysia Electronic Sports Federation or MESF is the governing body for all electronic sports in Malaysia. The federation strives to provide continuous support and growth of the entire sports ecosystem nationally and promotes esports for all; gamers, industry, and nation-binding. Find out more about the team and our 'Esports For All' mission.",
        },
        {
          property: 'twitter:description',
          content:
            "Malaysia Electronic Sports Federation or MESF is the governing body for all electronic sports in Malaysia. The federation strives to provide continuous support and growth of the entire sports ecosystem nationally and promotes esports for all; gamers, industry, and nation-binding. Find out more about the team and our 'Esports For All' mission.",
        },
        {
          name: 'keywords ',
          content: 'MESF, Malaysia Electronic Sports Federation',
        },
        {
          name: 'title',
          content: 'About Us | Malaysia Esports Federation',
        },
      ],
      title: 'About Us | Malaysia Esports Federation',
    },
  },
  {
    path: 'tournament',
    loadChildren: () =>
      import('./modules/tournament/tournament.module').then(
        (m) => m.TournamentModule
      ),
    data: {
      tags: [
        {
          name: 'description',
          content:
            'Create your own private or public esport tournaments. Join tournaments for free.',
        },
        {
          property: 'og:description',
          content:
            'Create your own private or public esport tournaments. Join tournaments for free.',
        },
        {
          property: 'twitter:description',
          content:
            'Create your own private or public esport tournaments. Join tournaments for free.',
        },
        {
          name: 'keywords ',
          content: 'esports, tournament',
        },
        {
          name: 'title',
          content: 'Tournaments | Malaysia Esports Federation',
        },
      ],
      title: 'Tournaments | Malaysia Esports Federation',
    },
  },
  {
    path: 'article/create',
    loadChildren: () =>
      import('./modules/article/create-article/create-article.module').then(
        (m) => m.CreateArticleModule
      ),
    data: {
      tags: [
        {
          name: 'title',
          content: 'Article | Malaysia Esports Federation',
        },
      ],
      title: 'Article | Malaysia Esports Federation',
    },
  },
  {
    path: 'article/edit/:id',
    loadChildren: () =>
      import('./modules/article/create-article/create-article.module').then(
        (m) => m.CreateArticleModule
      ),
  },

  {
    path: 'XOX-Unity-League',
    component: XoxUnityComponent,
  },
  {
    path: 'GESS2021',
    component: GenerasiBijakComponent,
  },
  {
    path: 'article',
    loadChildren: () =>
      import('./modules/article/article.module').then((m) => m.ArticleModule),
    data: {
      tags: [
        {
          name: 'description',
          content: 'Get latest news and updates from esports.',
        },
        {
          property: 'og:description',
          content: 'Get latest news and updates from esports.',
        },
        {
          property: 'twitter:description',
          content: 'Get latest news and updates from esports.',
        },
        {
          name: 'keywords ',
          content: 'esports, news, esportsnews',
        },
      ],
    },
  },

  {
    path: 'user/:pageType',
    loadChildren: () =>
      import('./modules/log-reg/log-reg.module').then((m) => m.LogRegModule),
    data: {
      isRootPage: true,
      tags: [
        {
          name: 'description',
          content:
            'Start your esports journey with MESF. A complete esports tournament management platform',
        },
        {
          property: 'og:description',
          content:
            'Start your esports journey with MESF. A complete esports tournament management platform',
        },
        {
          property: 'twitter:description',
          content:
            'Start your esports journey with MESF. A complete esports tournament management platform',
        },
      ],
    },
  },
  {
    path: 'top-up',
    component: TopUpComponent,
  },
  {
    path: 'privacy-policy',
    component: PrivacyPolicyComponent,
  },
  {
    path: 'terms-and-conditions',
    component: TermsAndConditionsComponent,
  },
  {
    path: 'top-up/:product',
    loadChildren: () =>
      import('./modules/special-offer/special-offer.module').then(
        (m) => m.SpecialOfferModule
      ),
    data: { isRootPage: true },
  },
  {
    path: 'videos',
    loadChildren: () =>
      import('./modules/videos/videos.module').then((m) => m.VideosModule),
  },
  {
    path: 'search',
    loadChildren: () =>
      import('./modules/search/search.module').then((m) => m.SearchModule),
    data: {
      tags: [
        {
          name: 'title',
          content: 'Search | Malaysia Esports Federation',
        },
      ],
      title: 'Search | Malaysia Esports Federation',
    },
  },
  {
    path: 'bracket',
    canActivate: [AuthGuard],
    loadChildren: () =>
      import('./modules/bracket/bracket.module').then((m) => m.BracketModule),
  },
  {
    path: 'profile',
    canActivate: [AuthGuard],
    loadChildren: () =>
      import('./modules/profile/profile.module').then((m) => m.ProfileModule),
  },
  {
    path: 'admin',
    loadChildren: () =>
      import('./modules/admin/admin.module').then((m) => m.AdminModule),
  },
  {
    path: 'web-view',
    loadChildren: () =>
      import('./modules/web-view/web-view.module').then((m) => m.WebViewModule),
  },
  {
    path: 'shop',
    loadChildren: () =>
      import('./modules/shop/shop.module').then((m) => m.ShopModule),
  },
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: '404', component: NotfoundComponent },
  { path: '**', redirectTo: '/404', pathMatch: 'full' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
    scrollPositionRestoration: 'enabled',
    initialNavigation: 'enabled',
}),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
