import { makeStateKey, TransferState } from '@angular/platform-browser';
import { TranslateLoader } from '@ngx-translate/core';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { readFileSync } from 'fs';
import { join } from 'path';

@Injectable({
  providedIn: 'root',
})
export class TranslateFsLoader implements TranslateLoader {
  private prefix = 'i18n';
  private suffix = '.json';

  constructor(private transferState: TransferState) {}

  public getTranslation(lang: string): Observable<any> {
    const path = join(
      __dirname,
      '../browser/assets/',
      this.prefix,
      `${lang}${this.suffix}`
    );
    const data = JSON.parse(readFileSync(path, 'utf8'));
    // ADDED: store the translations in the transfer state:
    const key = makeStateKey<any>('transfer-translate-' + lang);
    this.transferState.set(key, data);
    return of(data);
  }
}
