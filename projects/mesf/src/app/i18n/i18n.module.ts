import {
  TranslateLoader,
  TranslateModule,
  TranslateService,
} from '@ngx-translate/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TransferState } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { GlobalUtils } from '../shared/service/global-utils/global-utils';
import {
  LanguageService,
  ConstantsService,
  BracketService,
} from '../core/service';
@NgModule({
  imports: [
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: translateLoaderFactory,
        deps: [HttpClient, TransferState],
      },
    }),
  ],
  exports: [TranslateModule],
})
export class I18nModule {
  constructor(
    translateService: TranslateService,
    languageService: LanguageService,
    bracketService: BracketService
  ) {
    translateService.addLangs(ConstantsService?.language.map((el) => el.code));

    const webViewMetaData = bracketService.getWebViewMeta();
    const browserLang = GlobalUtils.isBrowser()
      ? localStorage.getItem('currentLanguage') ||
        translateService.getBrowserLang() ||
        webViewMetaData?.locale ||
        ConstantsService?.defaultLangCode
      : ConstantsService?.defaultLangCode;
    languageService.setLanguage(browserLang);
    translateService.use(browserLang || ConstantsService?.defaultLangCode);
  }
}

export function translateLoaderFactory(httpClient: HttpClient) {
  return new TranslateHttpLoader(
    httpClient,
    '/assets/i18n/',
    '.json?cb=' + new Date().getTime()
  );
}
