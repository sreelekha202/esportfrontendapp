import { Component, Input, OnInit } from '@angular/core';
import { LanguageService } from '../service';

export interface VideoItemComponentData {
  id: number;
  img: string;
  title: any;
  text: string;
  date: string;
}

@Component({
  selector: 'app-video-item',
  templateUrl: './video-item.component.html',
  styleUrls: ['./video-item.component.scss'],
})
export class VideoItemComponent implements OnInit {
  @Input() data;

  constructor(public languageService: LanguageService) {}

  ngOnInit(): void {}
}
