import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TournamentSliderComponent } from './tournament-slider.component';

describe('TournamentSliderComponent', () => {
  let component: TournamentSliderComponent;
  let fixture: ComponentFixture<TournamentSliderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TournamentSliderComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TournamentSliderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
