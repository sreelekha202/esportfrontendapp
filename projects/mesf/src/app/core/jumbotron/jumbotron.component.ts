import { Component, Input, OnInit } from '@angular/core';

export interface JumbotronComponentData {
  ribbon: string;
  title: string;
  _id: string;
  slug: string;
  dateTitle: string;
  btn: string;
  user: {
    img: string;
    name: string;
  };

  isBtnHidden?: boolean;
}

@Component({
  selector: 'app-jumbotron',
  templateUrl: './jumbotron.component.html',
  styleUrls: ['./jumbotron.component.scss'],
})
export class JumbotronComponent implements OnInit {
  @Input() data: JumbotronComponentData;
  @Input() backgroundUrl: any;

  constructor() {}
  ngOnChanges() {
    this.data = this.data;
  }
  ngOnInit(): void {}
}
