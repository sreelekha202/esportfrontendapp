import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, SubscribableOrPromise } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class LeaderboardService {
  constructor(private http: HttpClient) {}

  getGameLeaderboard(params) {
    const { gameId, country, state, page, limit } = params;
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    let url = `${environment.apiEndPoint}home/fetchleaderboard/game/${gameId}?`;
    if (country != null && country != undefined && country != '') {
      url += `&country=${country}`;
    }
    if (state != null && state != undefined && state != '') {
      url += `&state=${state}`;
    }
    if (page != null && page != undefined && page != '') {
      url += `&page=${page}`;
    }
    if (limit != null && limit != undefined && limit != '') {
      url += `&limit=${limit}`;
    }
    return this.http.get(url);
  }

  getGames() {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    return this.http.get(environment.apiEndPoint + `game`, {
      headers: httpHeaders,
    });
  }

  getUserLeaderboard(params) {
    const { userId, month, year } = params;
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    let url = `${environment.apiEndPoint}home/fetchleaderboard/${userId}?`;
    if (month != null && month != undefined && month != '') {
      url += `&month=${month}`;
    }
    if (year != null && year != undefined && year != '') {
      url += `&year=${year}`;
    }
    return this.http.get(url);
  }

  checkFollowStatus(followedUserId): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    let url = `${environment.apiEndPoint}userpreference/checkfollowstatus/${followedUserId}`;
    return this.http.get(url);
  }
  followUser(followedUserId): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    let url = `${environment.apiEndPoint}userpreference/follow/${followedUserId}`;
    return this.http.post(url, null, {
      headers: httpHeaders,
    });
  }
  unfollowUser(followedUserId): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    let url = `${environment.apiEndPoint}userpreference/unfollow/${followedUserId}`;
    return this.http.post(url, null, {
      headers: httpHeaders,
    });
  }
}
