import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class GameService {
  constructor(private http: HttpClient) {}

  getGames(): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    return this.http.get(environment.apiEndPoint + `game`, {
      headers: httpHeaders,
    });
  }

  getAllGenre(): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    return this.http.get(
      environment.apiEndPoint + `userpreference/get_all_genre`,
      {
        headers: httpHeaders,
      }
    );
  }

  getAllGames(query): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    let encodedUrl = encodeURIComponent(query);
    let url = environment.apiEndPoint + `game?query=${encodedUrl}`;
    return this.http.get(url, { headers: httpHeaders });
  }

  getAdminAllGames(): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    return this.http.get(environment.apiEndPoint + `game`, {
      headers: httpHeaders,
    });
  }

  saveGame(id, formData): Observable<any> {
    return this.http.post(
      environment.apiEndPoint + `game/upload/` + id,
      formData
    );
  }

  disableGame(gameId): Observable<any> {
    return this.http.delete(environment.apiEndPoint + `game/${gameId}`);
  }

  fetchPlatforms(): Observable<any> {
    return this.http.get(environment.apiEndPoint + `game/platform`);
  }
}
