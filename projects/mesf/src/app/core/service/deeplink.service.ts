import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { GlobalUtils } from '../../shared/service/global-utils/global-utils';

@Injectable({
  providedIn: 'root',
})
export class DeeplinkService {
  constructor() {}

  deeplink({ objectType, objectId }) {
    if (GlobalUtils.isBrowser()) {
      let deeplinkURL = `${environment.iOSBundelName}://${objectType}?id=${objectId}`;
      let ua = navigator.userAgent.toLowerCase();
      let isAndroid = ua.indexOf('android') > -1; // android check
      let isIphone = ua.indexOf('iphone') > -1; // ios check
      //isIphone = true;
      if (isIphone == true) {
        let app = {
          launchApp: function () {
            setTimeout(function () {
              window.location.href = `${environment.iOSAppstoreURL}`;
            }, 25);
            window.location.href = deeplinkURL; //which page to open(now from mobile, check its authorization)
          },
          openWebApp: function () {
            window.location.href = `${environment.iOSBundelName}`;
          },
        };
        app.launchApp();
      } else if (isAndroid == true) {
        let app = {
          launchApp: function () {
            window.location.replace('bundlename://linkname'); //which page to open(now from mobile, check its authorization)
            setTimeout(this.openWebApp, 500);
          },
          openWebApp: function () {
            window.location.href =
              'https://play.google.com/store/apps/details?id=packagename';
          },
        };
        app.launchApp();
      } else {
        //navigate to website url
      }
    }
  }
}
