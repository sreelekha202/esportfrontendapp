import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, SubscribableOrPromise } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class TransactionService {
  constructor(private http: HttpClient) {}

  getTransactionDetail(tDetail): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    return this.http.post(
      environment.apiEndPoint + 'transaction/getDetail',
      tDetail,
      {
        headers: httpHeaders,
      }
    );
  }

  /**
   * Return product list promise
   */
  getProduct(type): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    const url = `${environment.apiEndPoint}transaction/getProduct?type=${type}`;
    return this.http.get(url);
  }

  /**
   * Return product list promise
   */
  updateTransactionDetail(tDetail): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    return this.http.post(
      environment.apiEndPoint + 'transaction/update_coda_transaction',
      tDetail,
      {
        headers: httpHeaders,
      }
    );
  }

  sendVoucherEmail(tDetail): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    return this.http.post(environment.apiEndPoint + 'home/email', tDetail, {
      headers: httpHeaders,
    });
  }

  createOrUpdateTransaction(payload): SubscribableOrPromise<any> {
    return this.http
      .patch(`${environment.apiEndPoint}transaction/pay`, payload)
      .toPromise();
  }

  paymentDetail(params): SubscribableOrPromise<any> {
    const encodedPagination = encodeURIComponent(params['pagination']);
    const url = `${environment.apiEndPoint}transaction/paymenthitstory?pagination=${encodedPagination}`;
    return this.http.get(url).toPromise();
  }
}
