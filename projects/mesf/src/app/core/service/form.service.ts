import { Injectable } from '@angular/core';
import {
  FormControl,
  FormGroup,
  ValidationErrors,
  ValidatorFn,
} from '@angular/forms';

@Injectable({
  providedIn: 'root',
})
export class FormService {
  constructor() {}

  groupFieldValidator(): ValidatorFn {
    return (group: FormGroup): ValidationErrors => {
      const value = group.value;
      const isValid = Object.values(value).some((el: any) => el.trim());
      return isValid ? null : { required: true };
    };
  }

  emptyCheck = (control: FormControl) => {
    return control.value && control.value.trim() ? null : { required: true };
  };
}
