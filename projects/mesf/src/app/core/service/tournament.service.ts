import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, SubscribableOrPromise } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class TournamentService {
  constructor(private http: HttpClient) {}

  saveTournament(formValues): Observable<any> {
    const data = formValues;
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    return this.http.post(environment.apiEndPoint + 'tournament', data, {
      headers: httpHeaders,
    });
  }

  updateTournament(formValues, id): Observable<any> {
    const data = formValues;
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    return this.http.put(environment.apiEndPoint + `tournament/${id}`, data, {
      headers: httpHeaders,
    });
  }

  abortTournament(formValues): Observable<any> {
    const data = formValues;
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    return this.http.put(environment.apiEndPoint + `tournament/abort`, data, {
      headers: httpHeaders,
    });
  }
  getTournaments(params, selectParams = ``, option = ''): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json; charset=utf-8');
    let encodedUrl = encodeURIComponent(params.query);
    let url =
      environment.apiEndPoint + `tournament?query=${encodedUrl}${selectParams}`;
    if (option) {
      let opt = encodeURIComponent(option);
      url += `&option=${opt}`;
    }
    return this.http.get(url, { headers: httpHeaders });
  }

  getTournament(id): Observable<any> {
    return this.http.get(environment.apiEndPoint + `tournament/${id}`);
  }

  saveParticipant(data): Observable<any> {
    return this.http.post(environment.apiEndPoint + 'participant', data);
  }

  uploadParticipant(data): Observable<any> {
    return this.http.post(environment.apiEndPoint + 'participant/upload', data);
  }

  getParticipants(encodedUrl): Observable<any> {
    return this.http.get(environment.apiEndPoint + `participant${encodedUrl}`);
  }

  searchParticipant(field, text, tournamentId, pId?): Observable<any> {
    let urlString = pId
      ? `participant/search?field=${field}&text=${encodeURIComponent(
          text
        )}&tournamentId=${tournamentId}&pId=${pId}`
      : `participant/search?field=${field}&text=${encodeURIComponent(
          text
        )}&tournamentId=${tournamentId}`;
    return this.http.get(environment.apiEndPoint + urlString);
  }

  updateParticipant(id, data): Observable<any> {
    return this.http.patch(environment.apiEndPoint + `participant/${id}`, data);
  }

  updateParticipantBulk(data): Observable<any> {
    return this.http.put(
      environment.apiEndPoint + `participant/updateAll`,
      data
    );
  }

  getParticipantTournament(params): Observable<any> {
    const encodedUrl = encodeURIComponent(params.query);
    const encodedPagination = encodeURIComponent(params.pagination);
    const url = `${environment.apiEndPoint}participant/tournament?query=${encodedUrl}&pagination=${encodedPagination}`;
    return this.http.get(url);
  }

  getJoinedTournamentsByUser(params): Observable<any> {
    const encodedUrl = encodeURIComponent(params.query);
    const encodedPagination = encodeURIComponent(params.pagination);
    const url = `${environment.apiEndPoint}admin/participant/tournament?query=${encodedUrl}&pagination=${encodedPagination}`;
    return this.http.get(url);
  }

  /**
   * Delete Tournament details By request Id
   * @param id: Request Id
   */
  deleteTournament(id): Observable<any> {
    return this.http.delete(environment.apiEndPoint + `tournament/${id}`);
  }

  getGames(): Observable<any> {
    return this.http.get(environment.apiEndPoint + `game`);
  }

  getStreamToken(streamId): SubscribableOrPromise<any> {
    return this.http
      .get(`${environment.apiEndPoint}video-streaming?streamId=${streamId}`)
      .toPromise();
  }

  getStreamKey(): SubscribableOrPromise<any> {
    return this.http
      .get(`${environment.apiEndPoint}video-streaming/generate-stream-key`)
      .toPromise();
  }

  getPaginatedTournaments(params) {
    let { status, limit, sort, page, game, organizer, prefernce } = params;
    let url = `${environment.apiEndPoint}home/tournament?`;
    if (status != null && status != undefined && status != '') {
      url += `&status=${status}`;
    }
    if (limit != null && limit != undefined && limit != '') {
      url += `&limit=${limit}`;
    }
    if (page) {
      url += `&page=${page}`;
    }
    if (sort) {
      url += `&sort=${sort}`;
    }
    if (game) {
      url += `&game=${game}`;
    }
    if (organizer) {
      url += `&organizer=${organizer}`;
    }
    if (prefernce) {
      url += `&preference=${encodeURIComponent(JSON.stringify(prefernce))}`;
    }
    return this.http.get(url);
  }

  getgenerasi_bijak_tournaments(params) {
    let { status, sort } = params;
    let url = `${environment.apiEndPoint}home/generasi_bijak_tournaments?`;
    if (status != null && status != undefined && status != '') {
      url += `&status=${status}`;
    }

    if (sort) {
      url += `&sort=${sort}`;
    }

    return this.http.get(url);
  }
}
