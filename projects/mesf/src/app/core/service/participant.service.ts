import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { SubscribableOrPromise } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ParticipantService {
  BaseURL = environment.apiEndPoint;
  constructor(private http: HttpClient) {}

  public getParticipantsWithQuery(params) {
    const encodedUrl = encodeURIComponent(params.filter);
    let url = environment.apiEndPoint + `participant?query=${encodedUrl}`;
    if (params.projection) {
      const select = encodeURIComponent(params.select);
      url = `&select=${select}`;
    }
    if (params.option) {
      const opt = encodeURIComponent(params.option);
      url += `&option=${opt}`;
    }
    return this.http.get(`${url}`);
  }

  fetchParticipantRegistrationStatus(
    id,
    participantId = null
  ): SubscribableOrPromise<any> {
    let url = `${environment.apiEndPoint}participant/join-status?tournamentId=${id}`;
    if (participantId) {
      url += `&guestId=${participantId}`;
    }
    return this.http.get(url).toPromise();
  }
}
