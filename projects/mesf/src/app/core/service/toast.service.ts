import { Injectable, TemplateRef } from '@angular/core';
import {
  SnackComponentData,
  SnackComponentType,
  SnackComponent,
} from '../../core/snack/snack.component';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({ providedIn: 'root' })
export class ToastService {
  toasts: any[] = [];
  constructor(public matSnackBar: MatSnackBar) {}

  showSuccess(message) {
    const data: SnackComponentData = {
      text: message,
      type: SnackComponentType.success,
    };
    this.matSnackBar.openFromComponent(SnackComponent, {
      data,
      panelClass: 'mesf-Snack',
      horizontalPosition: 'end',
      verticalPosition: 'top',
    });
  }

  showError(message) {
    const data: SnackComponentData = {
      text: message,
      type: SnackComponentType.error,
    };
    this.matSnackBar.openFromComponent(SnackComponent, {
      data,
      panelClass: 'mesf-Snack',
      horizontalPosition: 'end',
      verticalPosition: 'top',
    });
  }

  showInfo(message) {
    const data: SnackComponentData = {
      text: message,
      type: SnackComponentType.info,
    };
    this.matSnackBar.openFromComponent(SnackComponent, {
      data,
      panelClass: 'mesf-Snack',
      horizontalPosition: 'end',
      verticalPosition: 'top',
    });
  }

  show(textOrTpl: string | TemplateRef<any>, options: any = {}) {
    this.toasts.push({ textOrTpl, ...options });
  }

  remove(toast) {
    this.toasts = this.toasts.filter((t) => t !== toast);
  }
}
