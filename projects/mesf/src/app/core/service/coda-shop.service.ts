import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class CodaShopService {
  constructor(private http: HttpClient) {}

  getProductDetail(productName): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    let url = environment.eCommerceAPIEndPoint + `ecom/product/${productName}`;
    return this.http.get(url, { headers: httpHeaders });
  }

  validateUserDetail(dataToValidate) {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    let url = environment.eCommerceAPIEndPoint + `ecom/validate`;
    //return this.http.get(url, { headers: httpHeaders });
    return this.http.post(url, dataToValidate, { headers: httpHeaders });
  }

  processTopup(validatedUserData) {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    let url = environment.eCommerceAPIEndPoint + `ecom/topup`;
    return this.http.post(url, validatedUserData, { headers: httpHeaders });
  }

  getCodaTransactionCode(userValidateResult) {
    /*
    let reqData = {
      "initRequest": {
        "country": 458,
        "payType": 238,
        "apiKey": "40dfb8a3996f2d3adbc635c2e2d720702",
        "orderId": userValidateResult.orderId,
        "currency": 458,
        "items": [
          {
            "code": "MLMY14",
            "price": 1,
            "name": "14 diamonds",
            "type": 1
          }
        ],
        "profile": {
          "entry": [
            {
              "key": "user_id",
              "value": "4963"
            }
          ]
        }
      }
    }
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    let url = "https://sandbox.codapayments.com/airtime/api/restful/v1.0/Payment/init.json";
    return this.http.post(url, reqData, { headers: httpHeaders });
    */
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    let url = environment.eCommerceAPIEndPoint + `ecom/getTransactionId`;
    return this.http.post(url, userValidateResult, { headers: httpHeaders });
  }

  getInquiryPaymentResult(tQuery) {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    let url = environment.eCommerceAPIEndPoint + `ecom/getInquiryPaymentResult`;
    return this.http.post(url, tQuery, { headers: httpHeaders });
  }

  /**
   * Service to fetch product information
   * @param productName
   */
  getProductInfo(productName): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    let url = environment.apiEndPoint + `transaction/getProduct/${productName}`;
    return this.http.get(url, { headers: httpHeaders });
  }

  voucherPlaceOrder(userValidateResult) {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    let url = environment.eCommerceAPIEndPoint + `ecom/placeOrder`;
    return this.http.post(url, userValidateResult, { headers: httpHeaders });
  }
}
