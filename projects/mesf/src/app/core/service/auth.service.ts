import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpHeaders,
  HttpErrorResponse,
  HttpBackend,
} from '@angular/common/http';
import { BehaviorSubject, Observable, throwError } from 'rxjs';

import { Router } from '@angular/router';

import { environment } from '../../../environments/environment';

import { map, catchError } from 'rxjs/operators';
import { CookieService } from 'ngx-cookie-service';
import { GlobalUtils } from '../../shared/service/global-utils/global-utils';

const API = environment.apiEndPoint + 'auth/';
const token = environment.currentToken;

@Injectable({
  providedIn: 'root',
})
export class AuthServices {
  // store the URL so we can redirect after logging in
  redirectUrl: string;

  constructor(
    private router: Router,
    private http: HttpClient,
    handler: HttpBackend,
    private cookieService: CookieService
  ) {
    //this.http = new HttpClient(handler);
  }

  register(data): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    return this.http.post(API + 'register', data, { headers: httpHeaders });
  }

  confirmUser(utoken: string, code: string, type: string): Observable<any> {
    const data = {
      token: utoken,
      otp: code,
      type: type,
    };
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    return this.http.post(API + 'verify', data, { headers: httpHeaders });
  }

  login(data) {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');

    // return this.http
    //   .post<any>(API + 'login', data, { headers: httpHeaders })
    //   .pipe(
    //     map((user) => {
    //       if (user.code == 'ER1001') {
    //         return user;
    //       } else {
    //         // store user details and jwt token in local storage to keep user logged in between page refreshes
    //         localStorage.setItem(token, user.data.token);
    //         // Get Profile data based on token
    //         // this.getProfile().subscribe(
    //         //   (res) => {
    //         //     this.currentUserSubject.next(res.data);
    //         //   }
    //         // );
    //         return user;
    //       }
    //     })
    //   );
    return this.http.post<any>(API + 'login', data, {
      headers: httpHeaders,
    });
  }

  update(data): Observable<any> {
    return this.http.put(API + 'login_update', data);
  }

  confirm(uid: string, code: string, utype: string): Observable<any> {
    const data = {
      otp: code,
      type: utype,
      id: uid,
    };

    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    return this.http.post(API + 'login_verify', data, {
      headers: httpHeaders,
    });
  }

  social(authtoken, sprovider, referredBy) {
    if (GlobalUtils.isBrowser()) {
      const httpHeaders = new HttpHeaders();
      httpHeaders.set('Content-Type', 'application/json');
      const data = {
        type: 'login',
        provider: sprovider,
        token: authtoken,
        referredBy: referredBy == undefined ? undefined : referredBy,
      };
      return this.http
        .post<any>(API + 'social_login', data, {
          headers: httpHeaders,
        })
        .pipe(
          map((user) => {
            if (user.code == 'ER1001') {
              return user;
            } else {
              this.setCookie(user.data.token);
              // store user details and jwt token in local storage to keep user logged in between page refreshes
              localStorage.setItem(token, user.data.token);
              localStorage.setItem('refreshToken', user.data.refreshToken);

              // Get Profile data based on token
              // this.getProfile().subscribe(
              //   (res) => {
              //     this.currentUserSubject.next(res.data);
              //   }
              // );
              return user;
            }
          })
        );
    }
  }

  social_login(data): Observable<any> {
    return this.http.post(API + 'social_login', data);
  }

  resendOTP(data): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    return this.http.post(API + 'resend_otp', data);
  }

  forgotPassword(data): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    return this.http.post(API + 'forgot_password', data);
  }

  isAuthenticated() {}

  initAuth() {}

  verify(username: string) {}

  devicelist() {}

  // Error
  handleError(error: HttpErrorResponse) {
    let msg = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      msg = error.error.message;
    } else {
      // server-side error
      msg = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    return throwError(msg);
  }

  setCookie(value) {
    var date = new Date();

    // Get Unix milliseconds at current time plus 365 days
    date.setTime(+date + 365 * 86400000); //24 \* 60 \* 60 \* 100
    this.cookieService.set(
      'accessToken',
      value,
      date,
      '/',
      environment.cookieDomain
    );
  }
}
