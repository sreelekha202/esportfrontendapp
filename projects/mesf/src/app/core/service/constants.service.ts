import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class ConstantsService {
  mobileMetaData;

  constructor() {}

  public static defaultLangCode = 'en';

  public static rtl = ['ar'];

  public static language = [
    { code: 'en', key: 'english', value: 'English' },
    { code: 'ms', key: 'malay', value: 'Malay' },
  ];

  public static AccountType = {
    Admin: 'admin',
  };

  public static Status = {
    Live: 'live',
    Pending: 'pending',
    Draft: 'draft',
    RequestEdit: 'request_edit',
    Abort: 'abort',
    SubmitForApprove: 'submitted_for_approval',
    Delete: 'deleted',
    Publish: 'publish',
  };

  public static APIError = {
    FetchDetailsFailed: 'ADMIN_MODAL.API_ERROR.FETCH_FAILED',
    TournamentUpdateError: 'ADMIN_MODAL.API_ERROR.TOURNAMENT_UPDATE_ERROR',
    ArticlesUpdateError: 'ADMIN_MODAL.API_ERROR.ARTICLE_UPDATE_ERROR',
  };

  public static Success = {
    Title: 'ADMIN_MODAL.SUCCESS.TITLE',
    TournamentUpdateSuccess: 'ADMIN_MODAL.SUCCESS.TOURNAMENT_DESCRIPTION',
    ArticlesUpdateSuccess: 'ADMIN_MODAL.SUCCESS.ARTICLE_DESCRIPTION',
  };

  public static AdvertisementTab = {
    Pending: 'Pending for Approval',
    Running: 'Running Campaigns',
    Paused: 'Paused Campaigns',
    Finished: 'Finished Campaigns',
  };

  public static ContentManagementTab = {
    Highlighted: 'HighlightedArticle',
    Approval: 'ApprovalPending',
  };

  public static Model = {
    Delete: {
      header: 'ADMIN_MODAL.DELETE.HEADER',
      title: 'ADMIN_MODAL.DELETE.TITLE',
      description: 'ADMIN_MODAL.DELETE.DESCRIPTION',
    },
    Abort: {
      header: 'ADMIN_MODAL.ABORT.HEADER',
      title: 'ADMIN_MODAL.ABORT.TITLE',
      description: 'ADMIN_MODAL.ABORT.DESCRIPTION',
    },
    Block: {
      header: 'ADMIN_MODAL.BLOCK_USER.HEADER',
      title: 'ADMIN_MODAL.BLOCK_USER.TITLE',
      description: 'ADMIN_MODAL.BLOCK_USER.DESCRIPTION',
    },
    Block_Confirmation: {
      header: 'ADMIN_MODAL.BLOCK_CONFIRM.HEADER',
      title: 'ADMIN_MODAL.BLOCK_CONFIRM.TITLE',
      button: 'ADMIN_MODAL.BLOCK_CONFIRM.BUTTON',
    },
    Unblock: {
      header: 'ADMIN_MODAL.UNBLOCK_USER.HEADER',
      title: 'ADMIN_MODAL.UNBLOCK_USER.TITLE',
      description: 'ADMIN_MODAL.UNBLOCK_USER.DESCRIPTION',
    },
    Article_Deleted: {
      header: 'ADMIN_MODAL.ARTICLE_DELETED.HEADER',
      title: 'ADMIN_MODAL.ARTICLE_DELETED.TITLE',
      description: 'ADMIN_MODAL.ARTICLE_DELETED.DESCRIPTION',
    },
    RequestEdit: {
      header: 'ADMIN_MODAL.REQUEST_EDIT.HEADER',
      title: 'ADMIN_MODAL.REQUEST_EDIT.TITLE',
      description: 'ADMIN_MODAL.REQUEST_EDIT.DESCRIPTION',
    },
    TournamentAbort: {
      header: 'ADMIN_MODAL.TOURNAMENT_ABORT.HEADER',
      title: 'ADMIN_MODAL.TOURNAMENT_ABORT.TITLE',
      description: 'ADMIN_MODAL.TOURNAMENT_ABORT.DESCRIPTION',
    },
    TournamentTrash: {
      header: 'ADMIN_MODAL.TOURNAMENT_TRASH.HEADER',
      title: 'ADMIN_MODAL.TOURNAMENT_TRASH.TITLE',
      description: 'ADMIN_MODAL.TOURNAMENT_TRASH.DESCRIPTION',
    },
    TournamentAbortSuccess: {
      header: 'ADMIN_MODAL.TOURNAMENT_ABORT_SUCCESS.HEADER',
      title: 'ADMIN_MODAL.TOURNAMENT_ABORT_SUCCESS.TITLE',
      description: 'ADMIN_MODAL.TOURNAMENT_ABORT_SUCCESS.DESCRIPTION',
    },
    UserInfluencer: {
      title: 'ADMIN_MODAL.USER_INFLUENCER.TITLE',
      description: 'ADMIN_MODAL.USER_INFLUENCER.DESCRIPTION',
    },
    UserUnInfluencer: {
      title: 'ADMIN_MODAL.USER_UNINFLUENCER.TITLE',
      description: 'ADMIN_MODAL.USER_UNINFLUENCER.DESCRIPTION',
    },
    UserVerified: {
      title: 'ADMIN_MODAL.USER_VERIFIED.TITLE',
      description: 'ADMIN_MODAL.USER_VERIFIED.DESCRIPTION',
    },
    UserUnVerified: {
      title: 'ADMIN_MODAL.USER_UNVERIFIED.TITLE',
      description: 'ADMIN_MODAL.USER_UNVERIFIED.DESCRIPTION',
    },
    UserAuthor: {
      title: 'ADMIN_MODAL.USER_AUTHOR.TITLE',
      description: 'ADMIN_MODAL.USER_AUTHOR.DESCRIPTION',
    },
    RevokeAuthor: {
      title: 'ADMIN_MODAL.REVOKE_AUTHOR.TITLE',
      description: 'ADMIN_MODAL.REVOKE_AUTHOR.DESCRIPTION',
    },
  };

  public static bracketType = [
    { name: 'OPTIONS.BRACKET.SINGLE', value: 'single' },
    { name: 'OPTIONS.BRACKET.DOUBLE', value: 'double' },
    { name: 'OPTIONS.BRACKET.ROUND_ROBIN', value: 'round_robin' },
    { name: 'OPTIONS.BRACKET.BATTLE_ROYALE', value: 'battle_royale' },
  ];

  public static checkInTimeOptions = [
    { name: 'OPTIONS.CHECKIN_OPT.30MIN_PRIOR', value: { hour: 0, minute: 30 } },
    { name: 'OPTIONS.CHECKIN_OPT.1H_PRIOR', value: { hour: 1, minute: 0 } },
    { name: 'OPTIONS.CHECKIN_OPT.2H_PRIOR', value: { hour: 2, minute: 0 } },
    { name: 'OPTIONS.CHECKIN_OPT.3H_PRIOR', value: { hour: 3, minute: 0 } },
    { name: 'OPTIONS.CHECKIN_OPT.4H_PRIOR', value: { hour: 4, minute: 0 } },
    { name: 'OPTIONS.CHECKIN_OPT.5H_PRIOR', value: { hour: 5, minute: 0 } },
  ];

  public static participantType = {
    individual: 'OPTIONS.T_TYPE.ONE',
    team: 'OPTIONS.T_TYPE.TEAM',
  };

  public static teamFormat = {
    2: 'OPTIONS.TEAM_FORMAT.DUO',
    4: 'OPTIONS.TEAM_FORMAT.SQUAD',
    n: 'OPTIONS.TEAM_FORMAT.VARIABLE',
  };

  public static matchFormat = {
    1: 'OPTIONS.SET.BEST_OF_1',
    3: 'OPTIONS.SET.BEST_OF_3',
    5: 'OPTIONS.SET.BEST_OF_5',
    7: 'OPTIONS.SET.BEST_OF_7',
  };

  public static currencyList = [
    { name: 'MYR', flag: 'assets/images/Tournament/Flag_of_Malaysia.png' },
  ];

  public static stageMatchType = {
    quarterFinal: 'OPTIONS.MATCH.QUARTER_FINAL',
    semiFinal: 'OPTIONS.MATCH.SEMI_FINAL',
    final: 'OPTIONS.MATCH.FINAL',
  };
  public static participantState = {
    johor: 'OPTIONS.STATE.JOHOR',
    putrajaya: 'OPTIONS.STATE.PUTRAJAYA',
    perak: 'OPTIONS.STATE.PERAK',
    kedah: 'OPTIONS.STATE.KEDAH',
  };
  public static managerStatus = {
    teacher: 'OPTIONS.MANAGER_STATUS_S.TEACHER',
    parent: 'OPTIONS.MANAGER_STATUS_S.PARENT',
  };
  public static gender = {
    male: 'OPTIONS.GENDER.MALE',
    female: 'OPTIONS.GENDER.FEMALE',
  };
  // Regex
  public static facebookRegex = /(?:(?:http|https):\/\/)?(?:www.|m.)?facebook.com\/(?!home.php)(?:(?:\w)*#!\/)?(?:pages\/)?(?:[?\w\-]*\/)?(?:profile.php\?id=(?=\d.*))?([\w\.-]+)/;
  public static emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  public static phoneRegex = /^\s*(?:\+?(\d{1,3}))?[-. (]*(\d{3})[-. )]*(\d{3})[-. ]*(\d{4})(?: *x(\d+))?\s*$/;
  public static youtubeRegex = /^(https?\:\/\/)?((www\.)?youtube\.com|youtu\.?be)\/.+$/;
  public static twitchRegex = /(?:https:\/\/)?((www\.)?twitch\.tv)\/(\S+)/;
  public static playStoreUrlRegex = /^(https?\:\/\/)?((www\.)?play\.google\.com)\/.+$/;
  public static appStoreUrlRegex = /^(https?\:\/\/)?((www\.)?itunes\.apple\.com)\/.+$/;
  public static webUrlRegex = /(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})/;

  setWebViewMetaData(data) {
    this.mobileMetaData = data;
  }

  getWebViewMetaData() {
    return this.mobileMetaData;
  }

  languageFC = (validation = null) => {
    const obj = {};
    ConstantsService.language.forEach((el) => {
      obj[el.key] = [''];
      if (validation) {
        obj[el.key].push(validation);
      }
    });
    return obj;
  };
}
