import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class UserNotificationsService {
  constructor(private http: HttpClient) {}

  sendInboxMessages(inputData): Observable<any> {
    const data = inputData;
    data.requestType = 'inbox-message';
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    return this.http.post(
      environment.apiEndPoint + `user-notifications`,
      data,
      {
        headers: httpHeaders,
      }
    );
  }

  sendEmails(inputData): Observable<any> {
    const data = inputData;
    data.requestType = 'email';
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    return this.http.post(
      environment.apiEndPoint + `user-notifications`,
      data,
      {
        headers: httpHeaders,
      }
    );
  }

  registerPushToken(token): Observable<any> {
    const data = {
      requestType: 'register-push-notification',
      platform: 'web',
      token,
    };
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    return this.http.post(
      environment.apiEndPoint + `user-notifications`,
      data,
      {
        headers: httpHeaders,
      }
    );
  }

  sendPushNotifications(inputData) {
    const data = inputData;
    data.requestType = 'push-notification';
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    return this.http.post(
      environment.apiEndPoint + `user-notifications`,
      data,
      {
        headers: httpHeaders,
      }
    );
  }

  getSentItems() {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    return this.http.post(
      environment.apiEndPoint + `user-notifications`,
      { requestType: 'sent' },
      {
        headers: httpHeaders,
      }
    );
  }
}
