import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class AdminService {
  constructor(private httpClient: HttpClient) {}

  BaseURL = environment.apiEndPoint;

  /**
   * service method to add new banner data
   * @param formData
   */
  public addbanner(formData) {
    return this.httpClient.post(`${this.BaseURL}banner`, formData);
  }
  getTournamentWinners(formData) {
    const encodedFilter = encodeURIComponent(JSON.stringify(formData.filter));
    const encodedOption = encodeURIComponent(JSON.stringify(formData.option));
    return this.httpClient.get(
      `${this.BaseURL}admin/winners?query=${encodedFilter}&option=${encodedOption}`
    );
  }

  /**
   * service method to get list of banners
   */
  public getBanner() {
    return this.httpClient.get(`${this.BaseURL}banner`);
  }

  /**
   * service method to update the banner
   * @param _id
   * @param formData
   */
  public updateBanner(_id, formData) {
    return this.httpClient.put(`${this.BaseURL}banner/${_id}`, formData);
  }

  /**
   * service method to delete the banner
   * @param _id
   */
  public deletebanner(_id) {
    return this.httpClient.delete(`${this.BaseURL}banner/${_id}`);
  }

  /**
   * service method to get list of products
   */
  public getAllProducts() {
    return this.httpClient.get(`${this.BaseURL}transaction/getProduct`);
  }

  /**
   * get announcement list
   */
  getAnnouncement() {
    return this.httpClient.get(`${this.BaseURL}admin/announcement`);
  }

  /**
   * add announcement data
   * @param formData
   */
  addannouncement(formData) {
    return this.httpClient.post(`${this.BaseURL}admin/announcement`, formData);
  }

  /**
   * update announcement data based on id
   * @param formData
   * @param _id
   */
  updateAnnouncement(formData, _id) {
    return this.httpClient.put(
      `${this.BaseURL}admin/announcement/${_id}`,
      formData
    );
  }
  paypalCall(formData) {
    return this.httpClient.post(
      `${this.BaseURL}transaction/payout/paypal`,
      formData
    );
  }

  checkDisbursal(formData) {
    return this.httpClient.post(
      `${this.BaseURL}transaction/getDisbursalStatus`,
      formData
    );
  }
  /**
   * delete announcement data based on id
   * @param formData
   * @param _id
   */
  deleteAnnouncement(_id) {
    return this.httpClient.delete(`${this.BaseURL}admin/announcement/${_id}`);
  }

  /**
   * service method to get a particular product
   * @param _id
   */
  public getProduct(_id) {
    return this.httpClient.get(`${this.BaseURL}transaction/getProduct/${_id}`);
  }

  /**
   * service method to add the product
   * @param formData
   */
  public addProduct(formData) {
    return this.httpClient.post(
      `${this.BaseURL}transaction/addProduct`,
      formData
    );
  }

  public getTournamentsforPayments() {
    const encodedFilter = encodeURIComponent(
      JSON.stringify({
        isFinished: true,
        $or: [
          { isWinnersPrizeDisbursed: { $exists: false } },
          { isWinnersPrizeDisbursed: { $eq: false } },
        ],
      })
    );
    const encodedOption = encodeURIComponent(
      JSON.stringify({ sort: { startDate: -1 } })
    );
    return this.httpClient.get(
      `${this.BaseURL}admin/tournament?query=${encodedFilter}&option=${encodedOption}`
    );
  }

  /**
   * service method to update a product
   * @param formData
   */
  public updateProduct(_id, formData) {
    return this.httpClient.put(
      `${this.BaseURL}transaction/updateProduct/${_id}`,
      formData
    );
  }

  /**
   * service method to delete a product
   * @param formData
   */
  public deleteProduct(_id) {
    return this.httpClient.delete(
      `${this.BaseURL}transaction/deleteProduct/${_id}`
    );
  }

  /**
   * service method to get all the leaderboard type based config data
   */
  public getLeaderboardConfig() {
    return this.httpClient.get(`${this.BaseURL}admin/leaderboard-config/MESF`);
  }

  public updateleaderboard(type, id, formData) {
    return this.httpClient.put(
      `${this.BaseURL}admin/leaderboard-config/MESF/${type}?typeId=${id}`,
      formData
    );
  }

  public deleteleaderboardItem(type, id) {
    return this.httpClient.delete(
      `${this.BaseURL}admin/leaderboard-config/MESF/${type}?typeId=${id}`
    );
  }

  public addLeaderboard(type, formData) {
    return this.httpClient.put(
      `${this.BaseURL}admin/leaderboard-config/MESF/${type}`,
      formData
    );
  }

  public addleaderboard(type, formData) {
    return this.httpClient.post(
      `${this.BaseURL}admin/leaderboard-config/${type}`,
      formData
    );
  }

  /**
   * service method to create new leaderboard level config data
   */
  public postLeaderboardLevelConfig(formData) {
    return this.httpClient.post(
      `${this.BaseURL}admin/leaderboard/level`,
      formData
    );
  }

  /**
   * service method to create new leaderboard badge config data
   */
  public postLeaderboardBadgeConfig(formData) {
    return this.httpClient.delete(
      `${this.BaseURL}admin/leaderboard/badge`,
      formData
    );
  }

  /**
   * service method to create new leaderboard level config data
   */
  public updateLeaderboardLevelConfig(formData) {
    return this.httpClient.put(
      `${this.BaseURL}admin/leaderboard/level`,
      formData
    );
  }

  /**
   * service method to create new leaderboard badge config data
   */
  public updateLeaderboardBadgeConfig(formData) {
    return this.httpClient.put(
      `${this.BaseURL}admin/leaderboard/badge`,
      formData
    );
  }
}
