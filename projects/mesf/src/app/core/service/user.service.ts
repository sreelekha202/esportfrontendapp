import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpHeaders,
  HttpErrorResponse,
} from '@angular/common/http';
import {
  BehaviorSubject,
  Observable,
  SubscribableOrPromise,
  throwError,
} from 'rxjs';

import { Router } from '@angular/router';

import { environment } from '../../../environments/environment';

import { map, catchError } from 'rxjs/operators';
import { IUser } from '../../shared/models';
import { AuthServices, ToastService } from './index';
import { GlobalUtils } from '../../shared/service/global-utils/global-utils';

const API = environment.apiEndPoint + 'user/';
const token = environment.currentToken;

@Injectable({
  providedIn: 'root',
})
export class UserService {
  public currentUserSubject: BehaviorSubject<IUser>;
  public currentUser: Observable<IUser>;

  constructor(
    private router: Router,
    private http: HttpClient,
    private authService: AuthServices,
    public toastService: ToastService
  ) {
    this.currentUserSubject = new BehaviorSubject(null);
    this.currentUser = this.currentUserSubject.asObservable();
  }

  refreshToken() {
    if (GlobalUtils.isBrowser()) {
      let data = {
        refreshToken: localStorage.getItem('refreshToken'),
      };
      return this.http
        .post<any>(environment.apiEndPoint + 'auth/web_refresh_token', data, {})
        .pipe(
          map((user) => {
            if (GlobalUtils.isBrowser()) {
              localStorage.setItem(environment.currentToken, user.data);
            }

            this.startRefreshTokenTimer();
            return user;
          })
        );
    }
  }

  /**
   * Get Profile details
   * @param token: profile data based on token Id
   */
  getProfile(): Observable<any> {
    let currToken;
    if (GlobalUtils.isBrowser()) {
      currToken = localStorage.getItem(token);
    }

    // let headers = new HttpHeaders();
    // headers = headers.append('content-type', 'application/json');
    // headers = headers.append('Authorization', `Bearer ${currToken}`);
    //, { headers: headers }

    return this.http.get(API + 'profile').pipe(
      map((value) => {
        return value;
      }),
      catchError(this.handleError)
    );
  }
  getRecentlyUpdatedUsers(params): Observable<any> {
    const encodedUrl = encodeURIComponent(params['query']);
    const encodedPagination = encodeURIComponent(params['pagination']);
    return this.http.get(
      API + `usersPaginated?query=${encodedUrl}&pagination=${encodedPagination}`
    );
  }
  updatePaymentInfo(data): Observable<any> {
    return this.http.post(API + 'add_payment_info', data);
  }

  /**
   * check admin status
   * @param token: profile data based on token Id
   */
  checkAdmin() {
    return this.http.get(API + 'admin').pipe(
      map(
        (res: any) => {
          return res;
        },
        (error: any) => {
          return false;
        }
      ),
      catchError(this.handleError)
    );
  }

  // Return the current User details
  public getAuthenticatedUser(): IUser {
    let currToken;
    if (GlobalUtils.isBrowser()) {
      currToken = localStorage.getItem(token);
    }

    if (this.currentUserSubject.value && currToken) {
      // Check if current user details and token are available
      return this.currentUserSubject.value;
    } else if (currToken && !this.currentUserSubject.value) {
      // if token is available but user details is not available in observables
      this.getProfile().subscribe(
        (res) => {
          this.currentUserSubject.next(res.data);
          return this.currentUserSubject.value;
        },
        (error) => {
          this.logout();
          this.router.navigate(['/home']);
          this.toastService.showError(error);
        }
      );
      return this.currentUserSubject.value;
    } else {
      // return null if token and user details not available
      return null;
    }
  }

  updateProfile(data) {
    return this.http.post(API + 'user_update', data).pipe(
      map((value) => {
        return value;
      }),
      catchError(this.handleError)
    );
  }

  refreshCurrentUser() {
    this.getProfile().subscribe((res) => {
      this.currentUserSubject.next(res.data);
      return this.currentUserSubject.value;
    });
  }

  // Error
  handleTokenError(error: HttpErrorResponse) {
    let that = this;
    that.logout();
    let msg = '';
    if (error?.error instanceof ErrorEvent) {
      // client-side error
      msg = error?.error?.message;
    } else {
      // server-side error
      msg = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    return throwError(msg);
  }
  handleError(error: HttpErrorResponse) {
    if (GlobalUtils.isBrowser() && error?.error?.message == 'Unauthorized') {
      localStorage.removeItem(token);
      window.location.reload();
    }
    let msg = '';
    if (error?.error instanceof ErrorEvent) {
      // client-side error
      msg = error?.error?.message;
    } else {
      // server-side error
      msg = error?.error?.message;
    }
    return throwError(msg);
  }

  /**
   * Use to set/update the current user profile
   * @param value : update user requests
   */
  public setCurrentUser(value: IUser) {
    this.currentUserSubject.next(value);
  }

  logout(): any {
    if (GlobalUtils.isBrowser()) {
      // remove user from local storage to log user out
      let data = {
        refreshToken: localStorage.getItem('refreshToken'),
      };
      this.http.post<any>(API + 'logout', data, {}).subscribe(
        () => {
          this.stopRefreshTokenTimer();
          //this.authService.redirectUrl = '/';
          this.currentUserSubject.next(null);

          localStorage.removeItem(token);
          localStorage.removeItem('refreshToken');

          window.location.reload();
        },
        (error) => {
          this.stopRefreshTokenTimer();
          //this.authService.redirectUrl = '/';
          this.currentUserSubject.next(null);

          localStorage.removeItem(token);
          localStorage.removeItem('refreshToken');

          window.location.reload();
        }
      );
    }
  }

  searchUsers(searchKeyword) {
    let query = JSON.stringify({
      text: { $regex: searchKeyword, $options: 'i' },
    });
    let projection = 'fullName,phoneNumber,email,profilePicture';
    let encodedUrl = encodeURIComponent(query);
    return this.http
      .get(API + `search_users?query=${encodedUrl}&select=${projection}`)
      .pipe(
        map((value) => {
          return value['data'];
        }),
        catchError((err) => err)
      );
  }

  add_social(authtoken, sprovider) {
    const data = {
      provider: sprovider,
      token: authtoken,
    };
    return this.http.post<any>(API + 'add_social', data);
  }

  addEmail(data): Observable<any> {
    return this.http.post(API + 'user_update', data);
  }

  createAdminUser(formData): Observable<any> {
    return this.http.post(API + 'create_admin_user', formData);
  }

  verifyEmail(data): Observable<any> {
    return this.http.post(API + 'user_verify', data);
  }

  //#region - Get the user details for Admin Panel
  getRecentlyUpdatedUserList(): Observable<any> {
    return this.http.get(API + `users`);
  }

  getAdminUsers(query): Observable<any> {
    return this.http.get(API + `users${query}`);
  }

  getTopSpendingUserList(): Observable<any> {
    return this.http.get(API + `users-account`);
  }
  //#endregion

  getAllCountries(): Observable<any> {
    return this.http.get('./assets/json/countries.json');
  }

  getStates(): Observable<any> {
    return this.http.get<any>('./assets/json/states.json');
  }

  getRewardTransaction(params): SubscribableOrPromise<any> {
    const encodedPagination = encodeURIComponent(params['pagination']);
    const url = `${API}reward_transaction?pagination=${encodedPagination}`;
    return this.http.get(url).toPromise();
  }

  updateUser(formValues, id): Observable<any> {
    const data = formValues;
    return this.http.patch(API + `admin_user_update/${id}`, data);
  }
  getRewardTransactionByUserId(params): Observable<any> {
    let encodedUrl = encodeURIComponent(params['query']);
    return this.http.get(API + `admin_reward_tran?query=${encodedUrl}`);
  }

  private refreshTokenTimeout;

  public startRefreshTokenTimer() {
    let jwtToken;
    if (GlobalUtils.isBrowser()) {
      // parse json object from base64 encoded jwt token
      jwtToken = JSON.parse(atob(localStorage.getItem(token).split('.')[1]));
    }

    // set a timeout to refresh the token a minute before it expires
    const expires = new Date(jwtToken.exp * 1000);
    const timeout = expires.getTime() - Date.now() - 60 * 1000;
    this.refreshTokenTimeout = setTimeout(
      () =>
        this.refreshToken().subscribe(
          (data) => {},
          (error) => {
            this.logout();
          }
        ),
      timeout
    );
  }

  private stopRefreshTokenTimer() {
    clearTimeout(this.refreshTokenTimeout);
  }

  getUserPrefernces(id): Observable<any> {
    return this.http.get(API + `admin_user_preference/${id}`);
  }

  changePassword(data): Observable<any> {
    return this.http.post(API + 'change_password', data);
  }
}
