import {
  SocialAuthServiceConfig,
  GoogleLoginProvider,
  FacebookLoginProvider,
} from 'angularx-social-login';
import { environment } from '../../../environments/environment';

const config = '';

export function provideConfig() {
  return config;
}
