import { Component, Input, OnInit } from '@angular/core';

export interface TopUpItemComponentData {
  logo: string;
  name: string;
  productName: string;
}

@Component({
  selector: 'app-top-up-item',
  templateUrl: './top-up-item.component.html',
  styleUrls: ['./top-up-item.component.scss'],
})
export class TopUpItemComponent implements OnInit {
  @Input() value: TopUpItemComponentData;

  constructor() {}

  ngOnInit(): void {}

  /**
   * Workaround for product image
   */
  _getImage(name) {
    let logo = './assets/images/Home/top1.png';
    return logo;
  }
}
