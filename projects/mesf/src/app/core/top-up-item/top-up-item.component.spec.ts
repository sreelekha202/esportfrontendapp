import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TopUpItemComponent } from './top-up-item.component';

describe('ArticleItemComponent', () => {
  let component: TopUpItemComponent;
  let fixture: ComponentFixture<TopUpItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TopUpItemComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TopUpItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
