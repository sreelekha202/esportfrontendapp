import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SharedTestingModule } from '../../shared/modules/shared-testing.module';
import { HeaderComponent } from './header.component';
import { By } from '@angular/platform-browser';

fdescribe('HeaderComponent', () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HeaderComponent],
      imports: [SharedTestingModule],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have 4 nav items', () => {
    expect(
      fixture.debugElement.query(By.css('.Header-nav-item[href="/home"]'))
        .nativeNode.pathname
    ).toBe('/home');
    expect(
      fixture.debugElement.query(By.css('.Header-nav-item[href="/article"]'))
        .nativeNode.pathname
    ).toBe('/article');
    expect(
      fixture.debugElement.query(By.css('.Header-nav-item[href="/tournament"]'))
        .nativeNode.pathname
    ).toBe('/tournament');
    expect(
      fixture.debugElement.query(By.css('.Header-nav-item[href="/about-us"]'))
        .nativeNode.pathname
    ).toBe('/about-us');
  });

  it('should have 4 nav items', () => {
    expect(
      fixture.debugElement.query(By.css('.Header-nav-item[href="/home"]'))
        .nativeNode.pathname
    ).toBe('/home');
    expect(
      fixture.debugElement.query(By.css('.Header-nav-item[href="/article"]'))
        .nativeNode.pathname
    ).toBe('/article');
    expect(
      fixture.debugElement.query(By.css('.Header-nav-item[href="/tournament"]'))
        .nativeNode.pathname
    ).toBe('/tournament');
    expect(
      fixture.debugElement.query(By.css('.Header-nav-item[href="/about-us"]'))
        .nativeNode.pathname
    ).toBe('/about-us');
  });
});
