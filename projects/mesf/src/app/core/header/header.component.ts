import {
  Component,
  ElementRef,
  OnDestroy,
  OnInit,
  EventEmitter,
  Output,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';
import { faBars, faTimes, faBell } from '@fortawesome/free-solid-svg-icons';
import { takeUntil } from 'rxjs/operators';
import { fromEvent, Subscription } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { AppHtmlProfileRoutes, AppHtmlRoutes } from '../../app-routing.model';
import { GlobalUtils } from '../../shared/service/global-utils/global-utils';
import { AppHtmlRoutesLoginType } from '../../app-routing.model';
import { IUser } from '../../shared/models';
import { TMatch } from '../../shared/models/tmatch';
import {
  AuthServices,
  LanguageService,
  UserService,
  ToastService,
} from '../../core/service';
import { ChatSidenavService } from '../../shared/service/chat/chat-sidenav.service';
import { ChatService } from '../../core/service/chat.service';
import { ConstantsService } from '../../core/service';

@AutoUnsubscribe()
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit, OnDestroy {
  @Output() menuClick = new EventEmitter();
  faBars = faBars;
  faTimes = faTimes;
  collapsed = true;
  currenUser: IUser;
  currentUserId: any;

  isUserLoggedIn = false;
  mobMenuOpened = false;
  AppHtmlRoutes = AppHtmlRoutes;
  AppHtmlRoutesLoginType = AppHtmlRoutesLoginType;
  AppHtmlProfileRoutes = AppHtmlProfileRoutes;
  AppLanguage = [];
  activeLang = ConstantsService?.defaultLangCode;
  isAdmin = false;
  tournaments: any;
  matchlist: any;
  matchcount: number;
  chatWins = [];

  links = [
    {
      title: 'header.HOME',
      url: AppHtmlRoutes.home,
    },
    {
      title: 'header.ARTICLES',
      url: AppHtmlRoutes.article,
    },
    {
      title: 'header.SHOP',
      url: AppHtmlRoutes.shop,
    },
    {
      title: 'header.LEADERBOARD',
      url: AppHtmlRoutes.leaderBoard,
    },
    {
      title: 'header.ESPORTS',
      url: AppHtmlRoutes.tournament,
    },
    {
      title: 'header.ABOUT_US',
      url: AppHtmlRoutes.aboutUs,
    },
  ];

  private sub1: Subscription;
  userSubscription: Subscription;
  accessLink: string;

  constructor(
    private elementRef: ElementRef,
    private authService: AuthServices,
    private userService: UserService,
    private router: Router,
    private activeRoute: ActivatedRoute,
    public translate: TranslateService,
    private languageService: LanguageService,
    public toastService: ToastService,
    private chatSidenavService: ChatSidenavService,
    private chatService: ChatService,
    private modalService: NgbModal
  ) {}

  ngOnInit(): void {
    this.matchlist = [];
    this.activeLang = this.translate.currentLang;
    this.AppLanguage = ConstantsService?.language;
    this.matchcount = 0;

    this.userSubscription = this.userService.currentUser.subscribe(
      (data) => {
        if (data) {
          this.currenUser = data;
          this.isUserLoggedIn = !!this.currenUser;
          this.currentUserId = this.currenUser._id;
          this.isAdmin =
            data?.accountType === ConstantsService.AccountType.Admin;
          this.chatService?.login.subscribe((data) => {
            if (data.accessToken != 'undefined' && data.accessToken != null) {
              this.chatService?.connectUser();

              this.chatService?.getUnReadCount().subscribe((res: any) => {
                this.matchcount = res[0].length;
              });

              this.chatService?.getAllUnreadCount().subscribe((res: any) => {
                this.matchcount = res[0].length;
              });
            }
          });
        } else {
          this.userService.getAuthenticatedUser();
        }
      },
      (error) => {
        this.toastService.showError(error.message);
      }
    );
    this.onHeaderScroll();
  }

  ngOnDestroy() {
    this.userSubscription?.unsubscribe();
  }

  ngDoCheck(): void {}

  onChatSidenavToggle(): void {
    this.chatSidenavService.toggle();
    if (this.currentUserId) {
      this.chatService?.getAllMatches(this.currentUserId);
    }
    if (this.chatSidenavService.checkOpen()) {
      if (this.chatService.getChatStatus()) {
        this.chatService.setChatStatus(false);
      }
    } else {
      if (this.chatService.getChatStatus()) {
        this.chatService.setChatStatus(false);
      }
    }
  }

  onLogOut(): void {
    if (GlobalUtils.isBrowser()) {
      this.chatService.setChatStatus(false);
      this.userService.logout();
      this.chatSidenavService.close();
      this.chatService.doLogout();
      this.chatService?.disconnectUser();

      this.router.navigate([AppHtmlRoutes.home]);
    }
  }

  onLanguageChange(lang): void {
    if (GlobalUtils.isBrowser()) {
      localStorage.setItem('currentLanguage', lang);
      this.activeLang = lang;
      this.translate.use(lang);
      this.languageService.setLanguage(lang);
    }
  }

  private onHeaderScroll(): void {
    if (GlobalUtils.isBrowser()) {
      const scrollingContent = document.querySelector('mat-sidenav-content');
      if (scrollingContent) {
        const header = this.elementRef.nativeElement;
        this.sub1 = fromEvent(scrollingContent, 'scroll').subscribe(
          (e: Event) => {
            if ((e.target as Element).scrollTop > header.clientHeight) {
              header.classList.add('is-scrolling');
            } else {
              header.classList.remove('is-scrolling');
            }
          }
        );
      }
    }
  }

  checkAccessLevel(index) {
    switch (this.currenUser.accessLevel[index]) {
      case 'sc': {
        this.router.navigate(['/admin/site-configuration']);
        break;
      }
      case 'acm': {
        this.router.navigate(['/admin/access-management']);
        break;
      }
      case 'em': {
        this.router.navigate(['/admin/esports-management']);
        break;
      }
      case 'cm': {
        this.router.navigate(['/admin/content-management']);
        break;
      }
      case 'un': {
        this.router.navigate(['/admin/user-notifications']);
        break;
      }
      case 'um': {
        this.router.navigate(['/admin/user-management']);
        break;
      }
      default: {
        this.checkAccessLevel(index + 1);
        break;
      }
    }
  }
}
