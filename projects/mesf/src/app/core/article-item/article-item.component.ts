import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { LanguageService } from '../service';
import { AppHtmlRoutes } from '../../app-routing.model';
export interface ArticleItemComponentData {
  id: number;
  _id: string;
  image: string;
  slug: string;
  title: any;
  shortDescription: any;
  createdDate: string;
  author?: string;
}

@Component({
  selector: 'app-article-item',
  templateUrl: './article-item.component.html',
  styleUrls: ['./article-item.component.scss'],
})
export class ArticleItemComponent implements OnInit {
  @Output() onTop = new EventEmitter();
  @Input() data: ArticleItemComponentData;

  AppHtmlRoutes = AppHtmlRoutes;

  constructor(public languageService: LanguageService) {}

  ngOnInit(): void {}
}
