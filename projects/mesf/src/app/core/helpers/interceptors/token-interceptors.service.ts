import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpResponse,
  HttpErrorResponse,
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { tap } from 'rxjs/operators';

import { environment } from '../../../../environments/environment';
import { AuthServices, BracketService, ConstantsService } from '../../service';
import { GlobalUtils } from '../../../shared/service/global-utils/global-utils';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
  constructor(
    private authService: AuthServices,
    private router: Router,
    private bracketService: BracketService
  ) {}

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    // add auth header with jwt if user is logged in and request is to api url

    let index = request.url.lastIndexOf('/');
    let url = request.url.substring(index + 1);
    if (GlobalUtils.isBrowser()) {
      if (url == 'file-upload') {
        if (localStorage.getItem(environment.currentToken)) {
          request = request.clone({
            setHeaders: {
              Authorization: `Bearer ${localStorage.getItem(
                environment.currentToken
              )}`,
            },
          });
        }
      } else {
        const webViewMetaData = this.bracketService.getWebViewMeta();

        if (localStorage.getItem(environment.currentToken)) {
          request = request.clone({
            setHeaders: {
              'Content-Type': 'application/json',
              Authorization: `Bearer ${localStorage.getItem(
                environment.currentToken
              )}`,
            },
          });
        } else if (webViewMetaData) {
          request = request.clone({
            setHeaders: {
              'Content-Type': 'application/json',
              Authorization: `Bearer ${webViewMetaData?.token}`,
            },
          });
        }
      }
      // Add current language
      const locale =
        localStorage.getItem('currentLanguage') ||
        ConstantsService?.defaultLangCode;
      request = request.clone({
        setHeaders: { locale },
      });
    }
    // return next.handle(request);
    return next.handle(request).pipe(
      tap(
        (event) => {},
        (error) => {
          if (error instanceof HttpErrorResponse) {
            if (error.status === 0) {
              throw new Error(
                'Please check your internet connectivity or try after sometime.'
              );
            } else if (
              error?.status === 401 &&
              error?.statusText === 'Unauthorized'
            ) {
              throw new Error(
                'Your token has been expired. Please login again.'
              );
            }
          }
        }
      )
    );
  }
}
