import { Component, Input, OnInit } from '@angular/core';

import { AppHtmlRoutes } from '../../app-routing.model';
import { LanguageService } from '../../core/service';

export interface TournamentItemComponentData {
  name: string;
  startDate: string;
  endDate: string;
  participants: string;
  maxParticipants: string;
  regionsAllowed: string;
  registrationIsOpen: string;
  _id: string;
  slug: string;
  banner: string;
  isSeeded: boolean;
  isFinished: boolean;
  startTime: string;
  participantJoined: any;
}

@Component({
  selector: 'app-tournament-item',
  templateUrl: './tournament-item.component.html',
  styleUrls: ['./tournament-item.component.scss'],
})
export class TournamentItemComponent implements OnInit {
  currentLang;
  @Input() data: TournamentItemComponentData;

  AppHtmlRoutes = AppHtmlRoutes;

  constructor(public language: LanguageService) {}

  ngOnInit(): void {
    this.language.language.subscribe((lang) => {
      if (lang) {
        this.currentLang = lang;
      }
    });
  }

  getRegistrationStatus(
    startDate,
    startTime,
    isSeeded,
    isFinished,
    maxParticipants,
    participantsJoined
  ) {
    if (!startDate || !startTime) {
      return;
    }
    let timeObj = this.convert12HrsTo24HrsFormat(startTime);
    let sDate = new Date(startDate);
    sDate.setHours(timeObj['hour']);
    sDate.setMinutes(timeObj['minute']);
    if (isSeeded == true || isFinished == true) {
      return 'Closed';
    } else if (participantsJoined >= maxParticipants) {
      return 'Closed';
    } else if (sDate.getTime() < Date.now()) {
      return 'Closed';
    } else {
      return 'Open';
    }
  }

  convert12HrsTo24HrsFormat(time) {
    if (!time) {
      return;
    }
    var hours = Number(time.match(/^(\d+)/)[1]);
    var minutes = Number(time.match(/:(\d+)/)[1]);
    var AP = time.match(/\s(.*)$/);
    if (!AP) AP = time.slice(-2);
    else AP = AP[1];
    if (AP == 'PM' && hours < 12) hours = hours + 12;
    if (AP == 'AM' && hours == 12) hours = hours - 12;
    var Hours24 = hours.toString();
    var Minutes24 = minutes.toString();
    if (hours < 10) Hours24 = '0' + Hours24;
    if (minutes < 10) Minutes24 = '0' + Minutes24;

    return { hour: parseInt(Hours24), minute: parseInt(Minutes24) };
  }
}
