import { NgModule } from '@angular/core';
import {
  SocialAuthServiceConfig,
  GoogleLoginProvider,
  FacebookLoginProvider,
} from 'angularx-social-login';
import { SharedModule } from '../shared/modules/shared.module';

import { ArticleItemComponent } from './article-item/article-item.component';
import { TopUpItemComponent } from './top-up-item/top-up-item.component';
import { VideoItemComponent } from './video-item/video-item.component';
import { ScrollTopComponent } from './scroll-top/scroll-top.component';
import { JumbotronComponent } from './jumbotron/jumbotron.component';
import { provideConfig } from './service/social-media-auth.service';
import { StepperComponent } from './stepper/stepper.component';
import { SnackComponent } from './snack/snack.component';
import { DropDownSelectComponent } from './drop-down-select/drop-down-select.component';

import { TournamentsSliderComponent } from '../modules/home2/tournaments-slider/tournaments-slider.component';
import { TournamentsCardComponent } from '../modules/home2/tournaments-card/tournaments-card.component';
import { TournamentsTrackerComponent } from '../modules/home2/tournaments-tracker/tournaments-tracker.component';
import { TournamentTrackerCardComponent } from '../modules/home2/tournament-tracker-card/tournament-tracker-card.component';
import { HottestPostsComponent } from '../modules/home2/hottest-posts/hottest-posts.component';
import { AdvertisingComponent } from '../modules/home2/advertising/advertising.component';
import { GamesCardsComponent } from '../modules/home2/games-cards/games-cards.component';
import { VouchersSliderComponent } from '../modules/home2/vouchers-slider/vouchers-slider.component';
import { TopPlayersComponent } from '../modules/home2/top-players/top-players.component';
import { MatchHighlightComponent } from '../modules/home2/match-highlight/match-highlight.component';
import { SportsFederationComponent } from '../modules/home2/sports-federation/sports-federation.component';
import { AllTournamentsComponent } from '../modules/home2/all-tournaments/all-tournaments.component';
import { LeaderBoardCardComponent } from '../modules/leader-board/leader-board-card/leader-board-card.component';
import { LeaderBoardPlayerComponent } from '../modules/leader-board-player/leader-board-player.component';
import { JsonToCsvComponent } from './json-to-csv/json-to-csv.component';
import { AnnouncementComponent } from '../modules/home2/announcement/announcement.component';

import { environment } from '../../environments/environment';

const components = [
  ScrollTopComponent,
  ArticleItemComponent,
  TopUpItemComponent,
  JumbotronComponent,
  VideoItemComponent,
  StepperComponent,
  SnackComponent,
  TournamentsSliderComponent,
  TournamentsCardComponent,
  TournamentsTrackerComponent,
  TournamentTrackerCardComponent,
  HottestPostsComponent,
  AdvertisingComponent,
  GamesCardsComponent,
  VouchersSliderComponent,
  TopPlayersComponent,
  MatchHighlightComponent,
  SportsFederationComponent,
  AllTournamentsComponent,
  DropDownSelectComponent,
  LeaderBoardCardComponent,
  LeaderBoardPlayerComponent,
  JsonToCsvComponent,
  AnnouncementComponent,
];

@NgModule({
  declarations: [...components],
  imports: [SharedModule],
  exports: [...components],
  providers: [
    {
      provide: 'SocialAuthServiceConfig',
      useValue: {
        autoLogin: false,
        providers: [
          {
            id: GoogleLoginProvider.PROVIDER_ID,
            provider: new GoogleLoginProvider(environment.googleAPPID),
          },
          {
            id: FacebookLoginProvider.PROVIDER_ID,
            provider: new FacebookLoginProvider(environment.facebookAPPID),
          },
        ],
      } as SocialAuthServiceConfig,
    },
  ],
})
export class CoreModule {}
