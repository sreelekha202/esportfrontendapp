import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  OnChanges,
  SimpleChanges,
} from '@angular/core';
import { PageChangedEvent } from 'ngx-bootstrap/pagination';
import { IPagination } from '../../shared/models';
import { PaginationService } from '../service/pagination.service';
import { GlobalUtils } from '../../shared/service/global-utils/global-utils';

@Component({
  selector: 'app-pagination',
  templateUrl: './custom-pagination.component.html',
  styleUrls: ['./custom-pagination.component.scss'],
})
export class CustomPaginationComponent implements OnInit, OnChanges {
  @Input() page: IPagination;
  @Input() activePage: Number = 1;
  @Output() currentPage = new EventEmitter<Number>();

  constructor(
    private paginationService: PaginationService,
    private globalUtils: GlobalUtils
  ) {}

  ngOnInit(): void {}

  pageChanged(event: PageChangedEvent) {
    this.currentPage.emit(event.page);
    this.paginationService.setPageChanged(true);
  }

  ngOnChanges(simpleChanges: SimpleChanges) {}

  onScrollTop() {
    if (GlobalUtils.isBrowser()) {
      window.scroll(0, 0);
    }
  }
}
