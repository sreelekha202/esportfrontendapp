import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PaginationModule, PaginationConfig } from 'ngx-bootstrap/pagination';
import { CustomPaginationComponent } from './custom-pagination.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [CustomPaginationComponent],
  imports: [CommonModule, PaginationModule.forRoot(), FormsModule],
  exports: [CustomPaginationComponent],
  providers: [{ provide: PaginationConfig }],
})
export class CustomPaginationModule {}
