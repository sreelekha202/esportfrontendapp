import {
  Component,
  OnInit,
  Input,
  AfterViewInit,
  ViewChildren,
  ViewChild,
  TemplateRef,
  ElementRef,
  OnDestroy,
} from '@angular/core';

import { SocialAuthService } from 'angularx-social-login';
import { SocialUser } from 'angularx-social-login';
import {
  GoogleLoginProvider,
  FacebookLoginProvider,
} from 'angularx-social-login';
import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router, ActivatedRoute } from '@angular/router';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl,
} from '@angular/forms';
import { faPhone } from '@fortawesome/free-solid-svg-icons';
import { environment } from '../../../environments/environment';
import { TranslateService } from '@ngx-translate/core';
import {
  SearchCountryField,
  CountryISO,
} from 'ngx-intl-tel-input';
import {
  AuthServices,
  LanguageService,
  UserService,
  ToastService,
} from '../../core/service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-social-media-login',
  templateUrl: './social-media-login.component.html',
  styleUrls: ['./social-media-login.component.scss'],
  providers: [NgbModalConfig, NgbModal],
})
export class SocialMediaLoginComponent implements OnInit, OnDestroy {
  @Input() referredBy: string;
  loading = false;
  faPhone = faPhone;

  user: SocialUser;
  id = '';
  returnUrl: string;
  didFail = false;
  message = '';

  stepOne = true;
  stepTwo = false;

  MobileVerificationForm: FormGroup;
  confirmForm: FormGroup;

  separateDialCode = true;
  SearchCountryField = SearchCountryField;
  CountryISO = CountryISO;
  preferredCountries: CountryISO[] = [
    CountryISO.Malaysia,
    CountryISO.Indonesia,
    CountryISO.Singapore,
  ];
  location: Location;

  @ViewChild('content')
  private content: TemplateRef<any>;

  // @ViewChildren('formRow') rows: any;

  @ViewChild('formRow') rows: ElementRef;

  timeLeft: number = 240;
  interval;

  code: string;
  codeEntered: boolean;

  userSubscription: Subscription;

  constructor(
    private formBuilder: FormBuilder,
    private authService: SocialAuthService,
    config: NgbModalConfig,
    private modalService: NgbModal,
    private loginService: AuthServices,
    private router: Router,
    private route: ActivatedRoute,
    private el: ElementRef,
    public translate: TranslateService,
    private userService: UserService,
    public toastService: ToastService,
    private translateService: TranslateService,
    private languageService: LanguageService
  ) {
    config.backdrop = 'static';
    config.keyboard = false;
    config.centered = true;
    config.backdropClass = 'dark-modal';
  }

  ngOnInit() {
    this.languageService.language.subscribe((lang) =>
      this.translateService.use(lang || 'en')
    );
    this.MobileVerificationForm = this.formBuilder.group({
      phoneNo: ['', Validators.compose([Validators.required])],
    });

    this.confirmForm = this.formBuilder.group({
      code1: ['', Validators.compose([Validators.required])],
      code2: ['', Validators.compose([Validators.required])],
      code3: ['', Validators.compose([Validators.required])],
      code4: ['', Validators.compose([Validators.required])],
    });

    // get return url from route parameters or default to '/'
    //this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    this.returnUrl = this.loginService.redirectUrl || '/';
  }

  ngOnDestroy() {
    this.modalService.dismissAll();
    this.userSubscription?.unsubscribe();
  }

  public socialSignIn(socialProvider: string) {
    let socialPlatformProvider;
    if (socialProvider === 'facebook') {
      socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
    } else if (socialProvider === 'google') {
      socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
    }
    this.authService.signIn(socialPlatformProvider).then((socialusers) => {
      if (socialusers) {
        this.loading = true;
        // this.socialusers = socialusers;
        this.Savesresponse(socialusers.authToken, socialusers.provider);
      }
    });
  }

  Savesresponse(authToken, provider) {
    this.loginService.social(authToken, provider, this.referredBy).subscribe(
      async (data) => {
        if (data.code == 'ER1001') {
          this.loading = false;
          this.id = data.data.id;
          this.modalService.open(this.content, {
            size: 'md',
            centered: true,
            scrollable: false,
            windowClass: 'login-modal-content',
          });
        } else {
          this.userService.startRefreshTokenTimer();
          await this.userService.getAuthenticatedUser();
          this.userSubscription = this.userService.currentUser.subscribe(
            (sdata) => {
              if (sdata) {
                if (data.data.type === 'admin') {
                  this.router.navigate(['/admin']);
                  this.loading = false;
                } else {
                  if (data.data.firstLogin == 1) {
                    this.router.navigate(['/profile']);
                    this.loading = false;
                  } else {
                    this.router.navigate([this.returnUrl]);
                    this.loading = false;
                  }
                }
              }
            }
          );
        }
      },
      (error) => {
        // this.didFail = true;
        // this.message = error.error.message;
        this.loading = false;
        this.toastService.showError(error?.error?.message || error?.message);
      }
    );
  }

  enableStepTwo(value) {
    const controls = this.MobileVerificationForm.controls;
    // check form
    if (this.MobileVerificationForm.invalid) {
      Object.keys(controls).forEach((controlName) =>
        controls[controlName].markAsTouched()
      );
      return;
    }
    const data = {
      type: 'update_mobile',
      phoneNumber: value.phoneNo.e164Number,
      id: this.id,
    };
    this.didFail = false;
    this.message = '';
    this.loginService.social_login(data).subscribe(
      (data) => {
        this.didFail = false;
        this.message = '';
        this.stepOne = false;
        this.stepTwo = true;

        this.interval = setInterval(() => {
          if (this.timeLeft > 0) {
            this.timeLeft--;
          }
          // else {
          //   this.timeLeft = 60;
          // }
        }, 1000);
      },
      (error) => {
        this.didFail = true;
        this.message = error?.error?.message || error?.message;
      }
    );
  }

  resendOtp() {
    let data = {
      id: this.id,
      type: 'resend_otp',
    };
    this.loginService.social_login(data).subscribe(
      (data) => {
        this.didFail = false;
        this.message = '';

        this.timeLeft = 240;
      },
      (error) => {
        this.didFail = true;
        this.message = error?.error?.message || error?.message;
      }
    );
  }

  keyUpEvent(event, index) {
    let pos = index;
    if (event.keyCode === 8 && event.which === 8) {
      pos = index - 1;
    } else {
      pos = index + 1;
    }
    if (pos > -1 && pos < 4) {
      //this.rows._results[pos].nativeElement.focus();
    }
  }

  onOtpChange(code: string): void {
    this.codeEntered = code.length === 4;

    if (this.codeEntered) {
      this.code = code;
    }
  }

  onConfirm(value) {
    const code = this.code;
    this.didFail = false;
    this.message = '';
    const data = {
      type: 'verify_mobile',
      otp: code,
      id: this.id,
    };
    this.loginService.social_login(data).subscribe(
      async (data) => {
        this.loginService.setCookie(data.data.token);
        localStorage.setItem(environment.currentToken, data.data.token);
        localStorage.setItem('refreshToken', data.data.refreshToken);
        this.userService.startRefreshTokenTimer();
        this.modalService.dismissAll();
        await this.userService.getAuthenticatedUser();
        this.userSubscription = this.userService.currentUser.subscribe(
          (sdata) => {
            if (sdata) {
              if (data.data.type === 'admin') {
                this.router.navigate(['/admin']);
                this.loading = false;
              } else {
                if (data.data.firstLogin == 1) {
                  this.router.navigate(['/profile']);
                  this.loading = false;
                } else {
                  this.router.navigate([this.returnUrl]);
                  this.loading = false;
                }
              }
            }
          }
        );
      },
      (error) => {
        this.didFail = true;
        this.message = error?.error?.message || error?.message;
      }
    );
  }

  open(content) {
    this.modalService.open(content);
  }

  cancel() {
    this.modalService.dismissAll();
    this.router
      .navigateByUrl('/home', { skipLocationChange: true })
      .then(() => {
        this.router.navigate(['/user', 'phone-login']);
      });
  }

  isControlHasErrorphone(controlName: string): boolean {
    const control = this.MobileVerificationForm.controls[controlName];
    if (control.touched) {
      if (control.invalid) {
        this.didFail = false;
        return true;
      }
    } else {
      return false;
    }
  }
}
