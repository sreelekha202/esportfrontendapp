import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FullscreenLoadingComponent } from './fullscreen-loading.component';

describe('FooterComponent', () => {
  let component: FullscreenLoadingComponent;
  let fixture: ComponentFixture<FullscreenLoadingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FullscreenLoadingComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FullscreenLoadingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
