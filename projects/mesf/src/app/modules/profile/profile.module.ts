import {
  FontAwesomeModule,
  FaIconLibrary,
} from '@fortawesome/angular-fontawesome';
import { faTrash, faEdit, faEye } from '@fortawesome/free-solid-svg-icons';
import { NgCircleProgressModule } from 'ng-circle-progress';
import { ModalModule } from 'ngx-bootstrap/modal';
import { NgModule } from '@angular/core';

import { InboxMessageItemComponent } from './inbox/inbox-message-item/inbox-message-item.component';
import { ChangePasswordComponent } from './profile-setting/change-password/change-password.component';
import { InboxMessageComponent } from './inbox/inbox-message/inbox-message.component';
import { MyTransactionsComponent } from './my-transactions/my-transactions.component';
import { ProfileSettingComponent } from './profile-setting/profile-setting.component';
import { MyTournamentComponent } from './my-tournament/my-tournament.component';
import { MyBookmarksComponent } from './my-bookmarks/my-bookmarks.component';
import { MyContentComponent } from './my-content/my-content.component';
import { BasicInfoComponent } from './basic-info/basic-info.component';
import { MyBracketComponent } from './my-bracket/my-bracket.component';
import { SharedModule } from '../../shared/modules/shared.module';
import { ProfileRoutingModule } from './profile-routing.module';
import { InboxComponent } from './inbox/inbox.component';
import { LogRegModule } from '../log-reg/log-reg.module';
import { ProfileComponent } from './profile.component';
import { CoreModule } from '../../core/core.module';

@NgModule({
  declarations: [
    ProfileComponent,
    BasicInfoComponent,
    MyBracketComponent,
    MyTournamentComponent,
    MyTransactionsComponent,
    MyBookmarksComponent,
    ProfileSettingComponent,
    InboxComponent,
    InboxMessageComponent,
    InboxMessageItemComponent,
    MyContentComponent,
    ChangePasswordComponent,
  ],
  imports: [
    SharedModule,
    CoreModule,
    ProfileRoutingModule,
    ModalModule.forRoot(),
    // Specify ng-circle-progress as an import
    NgCircleProgressModule.forRoot({
      // set defaults here
    }),
    LogRegModule,
    FontAwesomeModule,
  ],
})
export class ProfileModule {
  constructor(private library: FaIconLibrary) {
    library.addIcons(faTrash, faEdit, faEye);
  }
}
