import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AppHtmlRoutes } from '../../../app-routing.model';

import {
  TournamentService,
  UserService,
  ToastService,
} from '../../../core/service';
import { IPagination } from '../../../shared/models';

@Component({
  selector: 'app-my-tournament',
  templateUrl: './my-tournament.component.html',
  styleUrls: ['./my-tournament.component.scss'],
})
export class MyTournamentComponent implements OnInit, OnDestroy {
  AppHtmlRoutes = AppHtmlRoutes;
  tournamentList = [];
  type;
  tab = 'upcoming';
  userId;
  pagination = {
    page: 1,
    limit: 5,
    sort: 'startDate',
    projection: ['_id', 'name', 'description', 'startDate', 'slug'],
  };
  cPagination;
  page: IPagination;
  isDraft = false;
  isHidden = false;

  userSubscription: Subscription;

  constructor(
    private tournamentService: TournamentService,
    private router: Router,
    private userService: UserService,
    private toastService: ToastService
  ) {}

  ngOnInit(): void {
    this.type = this.router.url.split('/').reverse()[0];
    this.getCurrentUserDetails();
  }

  ngOnDestroy() {
    this.userSubscription?.unsubscribe();
  }

  getCurrentUserDetails() {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.userId = data._id;
        this.cPagination = Object.assign({}, this.pagination);
        this.fetchTournaments(this.type, this.tab);
      }
    });
  }

  /**
   * Get tournament list
   */

  fetchTournaments = async (type, tab) => {
    try {
      this.type = type;
      this.tab = tab;
      this.isDraft = tab == 'draft';
      this.tournamentList = [];
      let tournament;

      if (type === 'joined') {
        const payload = await this.getJoinedTournamentQuery(tab);
        tournament = await this.tournamentService
          .getParticipantTournament(payload)
          .toPromise();
      } else if (type === 'created') {
        const payload = await this.getCreatedTournamentQuery(tab);
        tournament = await this.tournamentService
          .getPaginatedTournaments(payload)
          .toPromise();
      }
      this.page = {
        totalItems: tournament?.data?.totalDocs,
        itemsPerPage: tournament?.data?.limit,
        maxSize: 5,
      };
      this.tournamentList = tournament.data.docs || [];
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  beforeTabChange(event) {
    const navtab = event.nextId.split('-');
    this.cPagination = Object.assign({}, this.pagination);
    if (navtab[0] == 'created' && navtab[1] == 'ongoing') {
      this.isHidden = true;
    } else if (navtab[0] == 'created' && navtab[1] == 'past') {
      this.isHidden = true;
    } else if (navtab[0] == 'joined' && navtab[1] == 'ongoing') {
      this.isHidden = true;
    } else if (navtab[0] == 'joined' && navtab[1] == 'past') {
      this.isHidden = true;
    } else {
      this.isHidden = false;
    }
    this.fetchTournaments(navtab[0], navtab[1]);
  }

  pageChanged(page): void {
    this.cPagination.page = page;
    this.fetchTournaments(this.type, this.tab);
  }

  getCreatedTournamentQuery(tab) {
    switch (true) {
      case tab === 'upcoming':
        return {
          organizer: this.userId,
          page: this.cPagination.page,
          sort: 'Oldest',
          status: '0',
          limit: 5,
        };
      case tab === 'ongoing':
        return {
          organizer: this.userId,
          page: this.cPagination.page,
          sort: 'Oldest',
          status: '1',
          limit: 5,
        };
      case tab === 'past':
        return {
          organizer: this.userId,
          page: this.cPagination.page,
          sort: 'Oldest',
          status: '2',
          limit: 5,
        };
      case tab === 'draft':
        return {
          organizer: this.userId,
          page: this.cPagination.page,
          sort: 'Oldest',
          status: '4',
          limit: 5,
        };
      case tab === 'unpublished':
        return {
          organizer: this.userId,
          page: this.cPagination.page,
          sort: 'Oldest',
          status: '5',
          limit: 5,
        };
    }
  }

  getJoinedTournamentQuery(tab) {
    switch (true) {
      case tab === 'upcoming':
        return {
          pagination: JSON.stringify(this.cPagination),
          query: JSON.stringify({
            isSeeded: false,
            isFinished: false,
            startDate: {
              $gt: new Date(),
            },
          }),
        };
      case tab === 'ongoing':
        return {
          pagination: JSON.stringify(this.cPagination),
          query: JSON.stringify({
            isSeeded: true,
            isFinished: false,
          }),
        };
      case tab === 'past':
        return {
          pagination: JSON.stringify(this.cPagination),
          query: JSON.stringify({
            $or: [
              {
                isSeeded: true,
                isFinished: true,
              },
              {
                isSeeded: false,
                isFinished: false,
                startDate: {
                  $lt: new Date(),
                },
              },
            ],
          }),
        };
    }
  }
}
