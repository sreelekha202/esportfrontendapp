import { Component, OnDestroy, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import {
  ArticleApiService,
  LanguageService,
  UserService,
  VideoLibraryService,
  ToastService,
} from '../../../core/service';
import { TranslateService } from '@ngx-translate/core';
import { ActivatedRoute } from '@angular/router';
import { map } from 'rxjs/operators';
import {
  InfoPopupComponent,
  InfoPopupComponentData,
  InfoPopupComponentType,
} from '../../../shared/popups/info-popup/info-popup.component';
import { MatDialog } from '@angular/material/dialog';
import { GlobalUtils } from '../../../shared/service/global-utils/global-utils';
import { CustomTranslatePipe } from '../../../shared/pipe/custom-translate.pipe';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-my-transactions',
  templateUrl: './my-content.component.html',
  styleUrls: ['./my-content.component.scss'],
  providers: [CustomTranslatePipe],
})
export class MyContentComponent implements OnInit, OnDestroy {
  rows = [];
  columns = [
    { name: 'DATE' },
    { name: 'TITLE' },
    { name: 'VIEWS' },
    { name: 'TOTAL LIKES' },
    { name: 'ARTICLE STATUS' },
  ];
  userId;
  videoLibraryColumns = [
    { name: 'TITLE' },
    { name: 'DESCRIPTION' },
    { name: 'LAST UPDATE' },
    { name: 'ACTION' },
  ];
  videoLibraryDetails;
  videoLibraryPagination = {
    page: 1,
    limit: 4,
    sort: '-updatedOn',
    projection: ['title', 'description', 'updatedOn', 'slug'],
  };
  selectedVideo;
  activetab = 'article';
  showLoader = true;
  currentLang = 'en';

  userSubscription: Subscription;

  constructor(
    private videoLibraryService: VideoLibraryService,
    private toastService: ToastService,
    private userService: UserService,
    public articleService: ArticleApiService,
    public translate: TranslateService,
    public languageService: LanguageService,
    private activatedRoute: ActivatedRoute,
    private matDialog: MatDialog,
    private customTranslatePipe: CustomTranslatePipe
  ) {}

  ngOnInit(): void {
    this.getCurrentUserDetails();
    this.languageService.language.subscribe((lang) => {
      if (lang) {
        this.currentLang = lang;
      }
    });
  }

  ngOnDestroy() {
    this.userSubscription?.unsubscribe();
  }

  onRowCliCk(event): void {
    if (event.type === 'click') {
    }
  }

  beforeTabChange(event) {
    if (event.nextId === 'article') {
      this.getArticles();
    } else if (event.nextId === 'video-library') {
      this.fetchVideoLibraryList();
    }
  }

  /** fetch videos */
  fetchVideoLibraryList = async () => {
    try {
      const encodeURI = `?query=${encodeURIComponent(
        JSON.stringify({ createdBy: this.userId })
      )}&pagination=${encodeURIComponent(
        JSON.stringify(this.videoLibraryPagination)
      )}`;

      this.videoLibraryDetails = await this.videoLibraryService.fetchVideoLibrary(
        encodeURI
      );
      this.showLoader = false;
    } catch (error) {
      this.showLoader = false;
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  /** change page */
  onPageChange(event) {
    this.videoLibraryPagination.page = event.offset + 1;
    this.fetchVideoLibraryList();
  }

  /** remove video */
  deleteVideo = async (video) => {
    try {
      const data: InfoPopupComponentData = {
        title: this.translate.instant('PROFILE.CONTENTS.MODAL.TITLE'),
        text: `${this.translate.instant(
          'PROFILE.CONTENTS.MODAL.TEXT'
        )} ${this.customTranslatePipe.transform(
          video?.title,
          this.currentLang
        )}.`,
        type: InfoPopupComponentType.confirm,
        btnText: this.translate.instant('BUTTON.CONFIRM'),
      };

      const result = await this.matDialog
        .open(InfoPopupComponent, { data })
        .afterClosed()
        .toPromise();
      if (result) {
        const deleteVideo = await this.videoLibraryService.deleteVideoLibrary(
          video?._id
        );
        this.toastService.showSuccess(deleteVideo?.message);
        this.fetchVideoLibraryList();
      }
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  /** Get current user */
  getCurrentUserDetails() {
    if (GlobalUtils.isBrowser()) {
      this.userSubscription = this.userService.currentUser.subscribe((data) => {
        if (data) {
          this.userId = data._id;
          this.activatedRoute.paramMap
            .pipe(map(() => window.history.state))
            .subscribe((res) => {
              this.activetab = res?.tab || 'article';
              this.beforeTabChange({ nextId: this.activetab });
            });
        }
      });
    }
  }

  /** fetch user articles */
  getArticles = async () => {
    try {
      const query = JSON.stringify({ author: this.userId });
      const option = JSON.stringify({ sort: { createdDate: -1 } });
      const projection =
        'title,createdDate,views,totalLikes,articleStatus,_id,slug';
      const res: any = await this.articleService
        .getArticles({ query, option, projection })
        .toPromise();
      this.showLoader = false;
      this.rows = res.data;
    } catch (error) {
      this.showLoader = false;
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };
}
