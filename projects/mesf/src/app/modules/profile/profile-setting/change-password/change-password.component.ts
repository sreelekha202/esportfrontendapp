import { FormControl, FormGroup, Validators } from '@angular/forms';
import {
  Component,
  EventEmitter,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';

import { MYCustomValidators } from '../../../log-reg/password-form/custom-validators';
import { UserService, ToastService } from '../../../../core/service';
import { IUser } from '../../../../shared/models';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss'],
})
export class ChangePasswordComponent implements OnInit, OnDestroy {
  @Output() changePassword = new EventEmitter();
  currentUser: IUser;
  user: IUser;

  oldPassword = new FormControl('', [
    Validators.required,
    // check whether the entered password has a number
    MYCustomValidators.patternValidator(/\d/, { hasNumber: true }),
    // check whether the entered password has upper case letter
    MYCustomValidators.patternValidator(/[A-Z]/, { hasCapitalCase: true }),
    // check whether the entered password has a lower case letter
    MYCustomValidators.patternValidator(/[a-z]/, { hasSmallCase: true }),
    // check whether the entered password has a special character
    MYCustomValidators.patternValidator(
      /[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/,
      { hasSpecialCharacters: true }
    ),
    Validators.minLength(8),
  ]);
  error = '';
  password = new FormControl('', [
    Validators.required,
    // check whether the entered password has a number
    MYCustomValidators.patternValidator(/\d/, { hasNumber: true }),
    // check whether the entered password has upper case letter
    MYCustomValidators.patternValidator(/[A-Z]/, { hasCapitalCase: true }),
    // check whether the entered password has a lower case letter
    MYCustomValidators.patternValidator(/[a-z]/, { hasSmallCase: true }),
    // check whether the entered password has a special character
    MYCustomValidators.patternValidator(
      /[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/,
      { hasSpecialCharacters: true }
    ),
    Validators.minLength(8),
  ]);
  certainPassword = new FormControl(
    '',
    //CustomValidators.equalTo(this.password)
  );

  form = new FormGroup({
    oldPassword: this.oldPassword,
    password: this.password,
    certainPassword: this.certainPassword,
  }, { validators: MYCustomValidators.passwordMatchValidator });

  userSubscription: Subscription;

  constructor(
    private userService: UserService,
    public toastService: ToastService,
    private router: Router
  ) {}

  ngOnInit() {
    this.getUserData();
  }

  ngOnDestroy() {
    this.userSubscription?.unsubscribe();
  }

  getUserData() {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.currentUser = data;
        this.user = Object.assign({}, this.currentUser);
        if (!data.isPassword) {
          this.form.removeControl('oldPassword');
          this.form.updateValueAndValidity();
        }
      } else {
        // const newVal = this.userService.getAuthenticatedUser();
      }
    });
  }

  cancel() {
    this.router
      .navigateByUrl('/home', { skipLocationChange: true })
      .then(() => {
        this.router.navigate(['/profile/profile-setting']);
      });
  }

  onVisibilityToggle(el: HTMLInputElement): void {
    el.type = el.type === 'text' ? 'password' : 'text';
  }

  onSubmitEvent(): void {
    this.error = '';
    const updatePassword = {
      old_password: this.form.value.oldPassword,
      new_password: this.form.value.password,
    };
    this.userService.changePassword(updatePassword).subscribe(
      (data) => {
        this.error = '';
        this.userService.refreshCurrentUser();
        this.toastService.showSuccess(data.message);
        this.changePassword.emit();
      },
      (error) => {
        this.error = error.error.message;
      }
    );
    // this.changePassword.emit();
  }
}
