import {
  Component,
  OnInit,
  TemplateRef,
  ViewChild,
  OnDestroy,
} from '@angular/core';
import {
  faFacebookF,
  faTwitter,
  faGoogle,
} from '@fortawesome/free-brands-svg-icons';
import {
  faDesktop,
  faMobileAlt,
  faCheck,
} from '@fortawesome/free-solid-svg-icons';
import {
  GoogleLoginProvider,
  FacebookLoginProvider,
} from 'angularx-social-login';
import { NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { SwiperConfigInterface } from 'ngx-swiper-wrapper';
import { SocialAuthService } from 'angularx-social-login';
import { Router } from '@angular/router';

import {
  PhoneNoFormComponentDataItem,
  PhoneNoFormComponentOutputData,
} from '../../log-reg/phone-no/phone-no.component';
import {
  UserService,
  UserPreferenceService,
  ToastService,
} from '../../../core/service';
import { VerificationFormComponentData } from '../../log-reg/verification-form/verification-form.component';
import { GlobalUtils } from '../../../shared/service/global-utils/global-utils';
import { environment } from '../../../../environments/environment';
import { IUser } from '../../../shared/models';
import { FormBuilder, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';

enum ProfileSettingComponentTab {
  account = 'PROFILE.PROFILE_SETTING.TAB1',
  content = 'PROFILE.PROFILE_SETTING.TAB2',
  payment = 'PROFILE.PROFILE_SETTING.TAB3',
}
declare var $: any;

@Component({
  selector: 'app-profile-setting',
  templateUrl: './profile-setting.component.html',
  styleUrls: ['./profile-setting.component.scss'],
  providers: [NgbModalConfig, NgbModal],
})
export class ProfileSettingComponent implements OnInit, OnDestroy {
  currentUser: IUser;
  loading = false;
  user: IUser;
  selectedPrefrence = 'tab1';
  isDisabled = true;
  ProfileSettingComponentTab = ProfileSettingComponentTab;
  faFacebookF = faFacebookF;
  faTwitter = faTwitter;
  faGoogle = faGoogle;
  faDesktop = faDesktop;
  faMobileAlt = faMobileAlt;
  faCheck = faCheck;
  stepTwo = false;
  @ViewChild('EmailContent')
  private EmailContent: TemplateRef<any>;
  swiperConfig: SwiperConfigInterface = {
    width: 150,
    spaceBetween: 30,
    navigation: true,
  };
  isDesktopPlatform = true;
  isChangePassword = false;
  social = {
    facebook: false,
    google: false,
  };

  selected = {
    type: '',
    paymentAccountId: '',
  };

  emailOutputData: PhoneNoFormComponentOutputData;

  emailData: PhoneNoFormComponentDataItem = {
    title: 'ENTER YOUR EMAIL ID',
    subtitle: 'We will send you the 4 digit verification code',
    submitBtn: 'NEXT',
    cancelBtn: 'CANCEL',
    InputName: 'email',
    InputType: 'email',
    InputPlaceholder: 'Email',
    InputIcon: 'email',
    error: '',
  };

  emailVerification: VerificationFormComponentData = {
    title: 'VERIFY EMAIL ID',
    subtitle: 'You have registered using',
    footerTitle: "Didn't receive OTP?",
    footerLinkTitle: 'RESEND',
    timerMs: 0,
    submitBtn: 'NEXT',
    error: '',
  };

  game = [];
  genre = [];
  prefer = [];
  platform = [];
  error = '';

  list = [
    {
      url: './assets/images/Home/top4.png',
      title: 'LEAGUE OF LEGENDS',
      isChecked: false,
    },
    {
      url: './assets/images/Home/top4.png',
      title: 'LEAGUE OF LEGENDS',
      isChecked: false,
    },
    {
      url: './assets/images/Home/top4.png',
      title: 'LEAGUE OF LEGENDS',
      isChecked: false,
    },
    {
      url: './assets/images/Home/top4.png',
      title: 'LEAGUE OF LEGENDS',
      isChecked: false,
    },
    {
      url: './assets/images/Home/top4.png',
      title: 'LEAGUE OF LEGENDS',
      isChecked: false,
    },
    {
      url: './assets/images/Home/top4.png',
      title: 'LEAGUE OF LEGENDS',
      isChecked: false,
    },
    {
      url: './assets/images/Home/top4.png',
      title: 'LEAGUE OF LEGENDS',
      isChecked: false,
    },
    {
      url: './assets/images/Home/top4.png',
      title: 'LEAGUE OF LEGENDS',
      isChecked: false,
    },
    {
      url: './assets/images/Home/top4.png',
      title: 'LEAGUE OF LEGENDS',
      isChecked: false,
    },
    {
      url: './assets/images/Home/top4.png',
      title: 'LEAGUE OF LEGENDS',
      isChecked: false,
    },
    {
      url: './assets/images/Home/top4.png',
      title: 'LEAGUE OF LEGENDS',
      isChecked: false,
    },
    {
      url: './assets/images/Home/top4.png',
      title: 'LEAGUE OF LEGENDS',
      isChecked: false,
    },
    {
      url: './assets/images/Home/top4.png',
      title: 'LEAGUE OF LEGENDS',
      isChecked: false,
    },
    {
      url: './assets/images/Home/top4.png',
      title: 'LEAGUE OF LEGENDS',
      isChecked: false,
    },
    {
      url: './assets/images/Home/top4.png',
      title: 'LEAGUE OF LEGENDS',
      isChecked: false,
    },
    {
      url: './assets/images/Home/top4.png',
      title: 'LEAGUE OF LEGENDS',
      isChecked: false,
    },
    {
      url: './assets/images/Home/top4.png',
      title: 'LEAGUE OF LEGENDS',
      isChecked: false,
    },
  ];

  form = this.formBuilder.group({
    paymentType: ['', Validators.required],
    paymentAccountId: ['', Validators.required],
  });

  userSubscription: Subscription;

  constructor(
    private userService: UserService,
    private router: Router,
    private formBuilder: FormBuilder,
    private authService: SocialAuthService,
    public toastService: ToastService,
    private modalService: NgbModal,
    config: NgbModalConfig,
    public preference: UserPreferenceService
  ) {
    config.backdrop = 'static';
    config.keyboard = false;
    config.centered = true;
    config.backdropClass = 'dark-modal';
  }

  ngOnInit(): void {
    if (
      GlobalUtils.isBrowser() &&
      localStorage.getItem(environment.currentToken)
    ) {
      this.getUserData();
      this.getAllPreferedData();
    } else {
      this.router.navigate(['/']);
    }
  }

  ngOnDestroy() {
    this.modalService.dismissAll();
    this.userSubscription?.unsubscribe();
  }

  getUserData() {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.currentUser = data;
        this.user = Object.assign({}, this.currentUser);
        let that = this;
        this.user.identifiers.forEach(function (value) {
          if (value.idenType === 'GOOGLE') {
            that.social.google = true;
          }
          if (value.idenType === 'FACEBOOK') {
            that.social.facebook = true;
          }
        });
      } else {
        // const newVal = this.userService.getAuthenticatedUser();
      }
    });
  }

  getAllPreferedData() {
    let that = this;
    this.preference.getAllSettingPageData().subscribe(
      (data) => {
        this.game = data.data.game;
        this.genre = data.data.genre;
        this.prefer = data.data.prefer;
        this.platform = data.data.platform;
        this.preference.getLoginPrefrence().subscribe(
          (data) => {
            if (data.data.game) {
              data.data.game.forEach(function (value) {
                let index = that.game.findIndex((x) => x._id === value);

                that.game[index].isChecked = true;
              });
            }
            if (data.data.gamegenre) {
              data.data.gamegenre.forEach(function (value) {
                let index = that.genre.findIndex((x) => x._id === value);

                that.genre[index].isChecked = true;
              });
            }
            if (data.data.prefercontent) {
              data.data.prefercontent.forEach(function (value) {
                let index = that.prefer.findIndex((x) => x._id === value);

                that.prefer[index].isChecked = true;
              });
            }
            if (data.data.platform) {
              data.data.platform.forEach(function (value) {
                let index = that.platform.findIndex((x) => x._id === value);

                that.platform[index].isChecked = true;
              });
            }
          },
          (error) => {
            this.error = error.error.message;
          }
        );
      },
      (error) => {
        this.error = error.error.message;
      }
    );
  }

  onToggleItem(item): void {
    item.isChecked = !item.isChecked;
  }

  changePlatform(platform) {
    let index = this.platform.findIndex((x) => x === platform);
    if (index >= 0) {
      this.platform.splice(index, 1);
    } else {
      this.platform.push(platform);
    }
  }

  savePrefrences() {
    let game = [];
    let genre = [];
    let prefer = [];
    let platform = [];

    this.game.forEach(function (value) {
      if (value.isChecked == true) {
        game.push(value._id);
      }
    });
    this.genre.forEach(function (value) {
      if (value.isChecked == true) {
        genre.push(value._id);
      }
    });
    this.prefer.forEach(function (value) {
      if (value.isChecked == true) {
        prefer.push(value._id);
      }
    });
    this.platform.forEach(function (value) {
      if (value.isChecked == true) {
        platform.push(value._id);
      }
    });
    let updateData = {
      game: game,
      genre: genre,
      prefer: prefer,
      platform: platform,
    };
    this.preference.addPrefrence(updateData).subscribe(
      (data) => {
        this.toastService.showSuccess(data.message);
        this.router
          .navigateByUrl('/home', { skipLocationChange: true })
          .then(() => {
            this.router.navigate(['/profile/profile-setting']);
          });
      },
      (error) => {
        this.error = error.error.message;
      }
    );
  }

  public socialConnect(socialProvider: string) {
    let socialPlatformProvider;
    if (socialProvider === 'facebook') {
      socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
    } else if (socialProvider === 'google') {
      socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
    }
    this.authService.signIn(socialPlatformProvider).then((socialusers) => {
      if (socialusers) {
        // this.socialusers = socialusers;
        this.Savesresponse(socialusers.authToken, socialusers.provider);
      }
    });
  }

  Savesresponse(authToken, provider) {
    this.userService.add_social(authToken, provider).subscribe(
      (data) => {
        this.userService.refreshCurrentUser();
        this.toastService.showSuccess(data.message);
        this.router
          .navigateByUrl('/home', { skipLocationChange: true })
          .then(() => {
            this.router.navigate(['/profile/profile-setting']);
          });
      },
      (error) => {
        this.toastService.showError(error.error.message);
      }
    );
  }

  add_Email() {
    this.modalService.open(this.EmailContent, {
      size: 'md',
      centered: true,
      // scrollable: true,
      windowClass: 'email-modal-content',
    });
  }

  addEmailNext(data) {
    if (data.type === 'email') {
      this.emailOutputData = data;

      const queryData = {
        email: data.email,
      };
      this.emailData.error = '';
      this.userService.addEmail(queryData).subscribe(
        (response) => {
          this.emailData.error = '';
          this.emailVerification.timerMs = 240;
          this.emailOutputData.mailPhone = queryData.email;
          this.emailOutputData.type = 'add_email';
          this.stepTwo = true;
        },
        (error) => {
          this.emailData.error = error.error.message;
          this.stepTwo = false;
        }
      );
    }
  }

  submitEmail(data) {
    if (data.otp) {
      let queryData = {
        type: 'email',
        otp: data.otp,
      };
      this.emailVerification.error = '';
      this.userService.verifyEmail(queryData).subscribe(
        (response) => {
          this.emailVerification.error = '';
          this.modalService.dismissAll();
          this.userService.refreshCurrentUser();
          this.toastService.showSuccess(response.message);
          this.router
            .navigateByUrl('/home', { skipLocationChange: true })
            .then(() => {
              this.router.navigate(['/profile/profile-setting']);
            });
        },
        (error) => {
          this.emailVerification.error = error.error.message;
        }
      );
    }
  }

  onChangePassword(): void {
    this.isChangePassword = true;
  }

  onChangePasswordSubmit(): void {
    this.isChangePassword = false;
  }

  onToggleEdit() {
    this.isDisabled = !this.isDisabled;
    this.form.controls['paymentAccountId'].enable();
    this.updateBootstrapSelect();
  }

  cancel() {
    this.selected.type = this.user.accountDetail.paymentType
      ? this.user.accountDetail.paymentType
      : '';
    this.selected.paymentAccountId = this.user.accountDetail.paymentAccountId
      ? this.user.accountDetail.paymentAccountId
      : '';
    this.isDisabled = true;
    this.form.controls['paymentAccountId'].disable();
    this.updateBootstrapSelect();
  }

  private updateBootstrapSelect(): void {
    setTimeout(() => {
      // update bootstrap select
      $('.selectpicker').selectpicker('refresh');
    });
  }

  onChangePaymentType(type) {
    this.form.get('paymentAccountId').reset();

    if (type === 'paypal') {
      this.form
        .get('paymentAccountId')
        .setValidators([Validators.required, Validators.email]);
      this.form.get('paymentAccountId').updateValueAndValidity();
    }
  }

  savePaymentAccount() {
    this.loading = true;
    try {
      if (this.form.invalid && !this.form.value?.id) {
        this.form.markAllAsTouched();
        this.loading = false;
        return;
      }

      const { value } = this.form;
      const paymentAccountId = value.paymentAccountId;
      const res = this.userService
        .updatePaymentInfo({
          paymentType: value.paymentType,
          paymentAccountId: paymentAccountId,
        })
        .subscribe(
          (data) => {
            this.userService.refreshCurrentUser();
            this.selectedPrefrence = 'tab3';
            this.router
              .navigateByUrl('/home', { skipLocationChange: true })
              .then(() => {
                this.router.navigate(['/profile/profile-setting']);
              });
            this.toastService.showSuccess(data.message);

            // this.form.controls['paymentAccountId'].disable();

            this.loading = false;

            //   this.getUserData();
            //   this.getAllPreferedData();

            //   ;
            //  this.selectedPrefrence="tab1";
          },
          (error) => {
            this.loading = false;
            return false;
          }
        );
    } catch (error) {}
  }

  fetchNews(evt: any) {
    this.selectedPrefrence = evt.nextId;
    this.form.controls['paymentAccountId'].disable();
    this.selected.type = this.user.accountDetail.paymentType;
    this.selected.paymentAccountId = this.user.accountDetail.paymentAccountId;
    this.updateBootstrapSelect();

    // this.form.controls['paymentAccountId'].disable();
    // this.form.controls['paymentType'].disable();

    // this.form.setValue({
    //   paymentType: this.user.accountDetail.paymentType,
    //   paymentAccountId: this.user.accountDetail.paymentAccountId,
    // });
    // this.selected.type = this.user.accountDetail.paymentType;
    // this.form.setErrors(null);

    // // this.selected.type = this.user.accountDetail.paymentType;
    // // this.selected.paymentAccountId = this.user.accountDetail.paymentAccountId;
    //  this.updateBootstrapSelect();
  }
}
