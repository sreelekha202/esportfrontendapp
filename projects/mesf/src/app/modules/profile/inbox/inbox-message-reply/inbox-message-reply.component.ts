import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';

import { MessageService, UserService } from '../../../../core/service';
import { IUser } from '../../../../shared/models';

@Component({
  selector: 'app-inbox-message-reply',
  templateUrl: './inbox-message-reply.component.html',
  styleUrls: ['./inbox-message-reply.component.scss'],
})
export class InboxMessageReplyComponent implements OnInit {
  messageId: string;
  replytouserid: string;
  loggedInUser: IUser;
  now: string;
  replyMessage: string;

  constructor(
    private route: ActivatedRoute,
    private messageService: MessageService,
    private userService: UserService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.messageId = this.route.snapshot.params.id;
    this.replytouserid = this.route.snapshot.params.replytouserid;
    this.loggedInUser = this.userService.getAuthenticatedUser();
    this.now = new Date().toString();
  }

  sendReply() {
    const inputData = {
      toUser: this.replytouserid,
      subject: 'Reply Message',
      message: this.replyMessage,
      parentId: this.messageId,
    };
    this.messageService.sendMessage(inputData).subscribe(
      (response) => {
        this.router.navigate(['/profile/inbox/message', this.messageId]);
      },
      (error) => {
        this.router.navigate(['/profile/inbox/message', this.messageId]);
      }
    );
  }
}
