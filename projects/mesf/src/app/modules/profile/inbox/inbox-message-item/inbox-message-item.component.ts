import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { faAngleUp } from '@fortawesome/free-solid-svg-icons';
import { IUser, IMessage } from '../../../../shared/models';

export enum InboxMessageItemComponentType {
  default,
  full,
  preview,
  create,
}

@Component({
  selector: 'app-inbox-message-item',
  templateUrl: './inbox-message-item.component.html',
  styleUrls: ['./inbox-message-item.component.scss'],
})
export class InboxMessageItemComponent implements OnInit {
  @Input() type = InboxMessageItemComponentType.default;
  @Input() messageData: IMessage;
  @Input() messageSender: IUser;
  @Input() messageText: string;
  @Output() replyMessageEvent = new EventEmitter<string>();

  InboxMessageItemComponentType = InboxMessageItemComponentType;
  faAngleUp = faAngleUp;

  constructor() {}

  ngOnInit(): void {}

  saveMessage(messageText: string) {
    this.replyMessageEvent.emit(messageText);
  }
}
