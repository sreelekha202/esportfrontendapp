import {
  Component,
  OnInit,
  ViewChild,
  TemplateRef,
  OnDestroy,
  AfterViewInit,
} from '@angular/core';
import { NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';

import { GlobalUtils } from '../../../shared/service/global-utils/global-utils';
import { environment } from '../../../../environments/environment';
import { UserService, ToastService } from '../../../core/service';
import { IUser } from '../../../shared/models';
import { Subscription } from 'rxjs';

declare var $: any;

@Component({
  selector: 'app-basic-info',
  templateUrl: './basic-info.component.html',
  styleUrls: ['./basic-info.component.scss'],
})
export class BasicInfoComponent implements OnInit, OnDestroy, AfterViewInit {
  currentUser: IUser;
  user: IUser;
  isDisabled = true;
  countries = [];
  states = [];
  startDate = new Date(1990, 0, 1);
  todayDate: Date = new Date(
    new Date().setFullYear(new Date().getFullYear() - 10)
  );

  @ViewChild('content')
  private content: TemplateRef<any>;
  public base64textString;

  userSubscription: Subscription;

  constructor(
    private userService: UserService,
    private router: Router,
    public toastService: ToastService,
    private modalService: NgbModal,
    config: NgbModalConfig
  ) {
    config.backdrop = 'static';
    config.keyboard = false;
    config.centered = true;
    config.backdropClass = 'dark-modal';
  }

  ngOnInit(): void {
    this.userService.getAllCountries().subscribe((data) => {
      this.countries = data.countries;
      this.updateBootstrapSelect();
    });
    if (
      GlobalUtils.isBrowser() &&
      localStorage.getItem(environment.currentToken)
    ) {
      this.getUserData();
    } else {
      this.router.navigate(['/']);
    }
  }

  ngOnDestroy() {
    if (this.user?.firstLogin == 1) {
      const data = {
        firstLogin: 0,
      };
      const res = this.userService.updateProfile(data).subscribe(
        () => {
          this.userService.refreshCurrentUser();
        },
        (error) => {
          return false;
        }
      );

      this.userSubscription?.unsubscribe();
    }
  }

  ngAfterViewInit() {
    if (this.user && this.user.firstLogin === 1) {
      //this.openModal();
    }
  }

  getUserData() {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.currentUser = data;
        this.user = Object.assign({}, this.currentUser);
        this.updateBootstrapSelect();
        this.onChangeCountry(this.user.country);
      } else {
        // const newVal = this.userService.getAuthenticatedUser();
      }
    });
  }

  openModal() {
    this.modalService.open(this.content, {
      size: 'md',
      centered: true,
      // scrollable: true,
      windowClass: 'preference-modal-content',
    });
  }

  onToggleEdit() {
    this.isDisabled = !this.isDisabled;
    this.updateBootstrapSelect();
  }

  cancel() {
    this.user = Object.assign({}, this.currentUser);
    this.base64textString = '';
    this.isDisabled = true;
    this.onChangeCountry(this.user.country);
    this.updateBootstrapSelect();
  }

  //convert image to binary
  handleFileSelect(evt) {
    var files = evt.target.files;
    var file = files[0];

    if (files && file) {
      var reader = new FileReader();

      reader.onload = this._handleReaderLoaded.bind(this);

      reader.readAsBinaryString(file);
    }
  }
  _handleReaderLoaded(readerEvt) {
    var binaryString = readerEvt.target.result;
    this.base64textString = 'data:image/png;base64,' + btoa(binaryString);
  }
  dateformat(date) {
    return new Date(date);
  }
  saveProfile() {
    const data = {
      fullName: this.user.fullName,
      profilePicture: this.base64textString,
      martialStatus: this.user.martialStatus,
      parentalStatus: this.user.parentalStatus,
      dob: this.user.dob,
      country: this.user.country,
      state: this.user.state,
      shortBio: this.user.shortBio,
      gender: this.user.gender,
      profession: this.user.profession,
      postalCode: this.user.postalCode,
      updatedBy: 'user',
    };
    const res = this.userService.updateProfile(data).subscribe(
      () => {
        this.userService.refreshCurrentUser();
        this.toastService.showSuccess('Profile Updated');
        this.router
          .navigateByUrl('/home', { skipLocationChange: true })
          .then(() => {
            this.router.navigate(['/profile/basic-info']);
          });
      },
      (error) => {
        return false;
      }
    );
  }

  private updateBootstrapSelect(): void {
    setTimeout(() => {
      // update bootstrap select
      $('.selectpicker').selectpicker('refresh');
    });
  }

  onChangeCountry(countryValue) {
    if (countryValue !== this.currentUser.country) {
      this.user.state = '';
    }
    this.states.length = 0;
    let countriesList = [];
    let that = this;
    this.userService.getAllCountries().subscribe((data) => {
      countriesList = data.countries;

      let index = countriesList.findIndex((x) => x.name === countryValue);
      that.userService.getStates().subscribe((data) => {
        data.states.forEach(function (value) {
          if (value.country_id == countriesList[index]?.id) {
            that.states.push(value);
          }
        });
        that.updateBootstrapSelect();
      });
    });
  }
}
