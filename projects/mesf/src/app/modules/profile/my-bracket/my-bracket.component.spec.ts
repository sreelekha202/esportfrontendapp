import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyBracketComponent } from './my-bracket.component';

describe('MyBracketComponent', () => {
  let component: MyBracketComponent;
  let fixture: ComponentFixture<MyBracketComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MyBracketComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyBracketComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
