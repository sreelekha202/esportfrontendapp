import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { InboxMessageComponent } from './inbox/inbox-message/inbox-message.component';
import { ProfileSettingComponent } from './profile-setting/profile-setting.component';
import { MyTransactionsComponent } from './my-transactions/my-transactions.component';
import { MyTournamentComponent } from './my-tournament/my-tournament.component';
import { MyBookmarksComponent } from './my-bookmarks/my-bookmarks.component';
import { BasicInfoComponent } from './basic-info/basic-info.component';
import { MyBracketComponent } from './my-bracket/my-bracket.component';
import { InboxComponent } from './inbox/inbox.component';
import { ProfileComponent } from './profile.component';
import { MyContentComponent } from './my-content/my-content.component';

const routes: Routes = [
  {
    path: '',
    component: ProfileComponent,
    children: [
      {
        path: '',
        redirectTo: 'basic-info',
        pathMatch: 'full',
        data: {
          tags: [
            {
              name: 'title',
              content: 'My Profile | Malaysia Esports Federation',
            },
          ],
          title: 'My Profile | Malaysia Esports Federation',
        },
      },
      {
        path: 'basic-info',
        component: BasicInfoComponent,
        data: {
          tags: [
            {
              name: 'title',
              content: 'My Profile | Malaysia Esports Federation',
            },
          ],
          title: 'My Profile | Malaysia Esports Federation',
        },
      },
      { path: 'inbox', component: InboxComponent },
      {
        path: 'my-content',
        component: MyContentComponent,
      },
      {
        path: 'my-bookmarks',
        component: MyBookmarksComponent,
        data: {
          tags: [
            {
              name: 'title',
              content: 'My Saved Bookmarks| Malaysia Esports Federation',
            },
          ],
          title: 'My Saved Bookmarks| Malaysia Esports Federation',
        },
      },
      {
        path: 'my-bracket',
        component: MyBracketComponent,
        data: {
          tags: [
            {
              name: 'title',
              content: 'Create Brackets | Malaysia Esports Federation',
            },
          ],
          title: 'Create Brackets | Malaysia Esports Federation',
        },
      },
      { path: 'my-tournament/created', component: MyTournamentComponent },
      { path: 'my-tournament/joined', component: MyTournamentComponent },
      {
        path: 'my-transactions',
        component: MyTransactionsComponent,
        data: {
          tags: [
            {
              name: 'title',
              content:
                'View Past Transactions & Rewards|  Malaysia Esports Federation',
            },
          ],
          title:
            'View Past Transactions & Rewards|  Malaysia Esports Federation',
        },
      },
      {
        path: 'profile-setting',
        component: ProfileSettingComponent,
        data: {
          tags: [
            {
              name: 'title',
              content: 'My Account Settings| Malaysia Esports Federation',
            },
          ],
          title: 'My Account Settings| Malaysia Esports Federation',
        },
      },
      {
        path: 'inbox/:id',
        component: InboxMessageComponent,
        data: {
          tags: [
            {
              name: 'title',
              content: 'My Inbox| Malaysia Esports Federation',
            },
          ],
          title: 'My Inbox| Malaysia Esports Federation',
        },
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProfileRoutingModule {}
