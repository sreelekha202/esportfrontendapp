import { faBars, faTimes } from '@fortawesome/free-solid-svg-icons';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { GlobalUtils } from '../../shared/service/global-utils/global-utils';
import { environment } from '../../../environments/environment';
import { AppHtmlProfileRoutes } from '../../app-routing.model';
import { UserService, LanguageService } from '../../core/service';
import { TranslateService } from '@ngx-translate/core';

@AutoUnsubscribe()
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
})
export class ProfileComponent implements OnInit, OnDestroy {
  faBars = faBars;
  faTimes = faTimes;
  mobMenuOpened = false;
  navLinks = [
    {
      title: 'PROFILE.BASIC_INFO.TITLE',
      url: AppHtmlProfileRoutes.basicInfo,
    },
    {
      title: 'PROFILE.BRACKETS.TITLE',
      url: AppHtmlProfileRoutes.myBracket,
    },
    {
      title: 'PROFILE.TOURNAMENTS.TITLE',
      url: '',
      dropdown: [
        {
          title: 'PROFILE.TOURNAMENTS.SUB_HEADER1',
          url: AppHtmlProfileRoutes.myTournamentJoined,
        },
        {
          title: 'PROFILE.TOURNAMENTS.SUB_HEADER2',
          url: AppHtmlProfileRoutes.myTournamentCreated,
        },
      ],
    },
    {
      title: 'PROFILE.TRANSACTIONS.TITLE',
      url: AppHtmlProfileRoutes.myTransactions,
    },
    {
      title: 'PROFILE.BOOKMARKS.TITLE',
      url: AppHtmlProfileRoutes.myBookmarks,
    },
    {
      title: 'PROFILE.PROFILE_SETTING.TITLE',
      url: AppHtmlProfileRoutes.setting,
    },
    {
      title: 'PROFILE.INBOX.TITLE',
      url: AppHtmlProfileRoutes.inbox,
    },
  ];
  isTournamentPage = false;

  private sub1: Subscription;
  userSubscription: Subscription;

  constructor(
    public router: Router,
    public location: Location,
    private userService: UserService,
    private languageService: LanguageService,
    private translateService: TranslateService
  ) {
    if (
      GlobalUtils.isBrowser() &&
      !localStorage.getItem(environment.currentToken)
    ) {
      this.router.navigate(['/login']);
    }
    this.sub1 = this.router.events.subscribe(() => {
      this.checkTournamentPage();
    });
  }

  ngOnInit(): void {
    this.languageService.language.subscribe((lang) =>
      this.translateService.use(lang || 'en')
    );
    this.getCurrentUserDetails();
  }

  getCurrentUserDetails() {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        if (data['isAuthor'] == 0 || data['isInfluencer'] == 0) {
          const isContentExist = this.navLinks.find(
            (nav) => nav.title === 'PROFILE.CONTENTS.TITLE'
          );
          if (!isContentExist) {
            this.navLinks.splice(1, 0, {
              title: 'PROFILE.CONTENTS.TITLE',
              url: AppHtmlProfileRoutes.myContent,
            });
          }
        }
      }
    });
  }

  ngOnDestroy() {
    this.userSubscription?.unsubscribe();
  }

  private checkTournamentPage(): void {
    this.isTournamentPage = !!this.location
      .path()
      .match('/profile/my-tournament/');
  }
}
