import {
  Component,
  OnInit,
  ChangeDetectorRef,
  AfterContentChecked,
} from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators,
  ValidatorFn,
  AbstractControl,
  FormControl,
} from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

import { TranslateService } from '@ngx-translate/core';
import {
  BracketService,
  LanguageService,
  ToastService,
} from '../../../core/service';

@Component({
  selector: 'app-upsert-bracket',
  templateUrl: './upsert-bracket.component.html',
  styleUrls: [
    './upsert-bracket.component.scss',
    '../../tournament/create/create.component.scss',
  ],
})
export class UpsertBracketComponent implements OnInit, AfterContentChecked {
  bracket: FormGroup;
  navUrl = '../preview';
  maximumParticipants = 1024;
  nfMatchBetweenTwoTeam = 3;
  isProcessing = false;
  bracketList = ['single', 'double', 'round_robin', 'battle_royale'];

  constructor(
    private bracketService: BracketService,
    private fb: FormBuilder,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private toastService: ToastService,
    private translateService: TranslateService,
    private languageService: LanguageService,
    private ref: ChangeDetectorRef
  ) {}

  ngOnInit(): void {
    this.languageService.language.subscribe((lang) =>
      this.translateService.use(lang || 'en')
    );
    this.bracket = this.fb.group({
      id: [''],
      name: ['', Validators.required],
      bracketType: ['single', Validators.required],
      maximumParticipants: [
        2,
        Validators.compose([
          Validators.required,
          Validators.min(2),
          Validators.max(1024),
        ]),
      ],
      isKnowParticipantName: [false, Validators.required],
      nfMatchBetweenTwoTeam: [1],
      isRoundRobin: [false],
      noOfSet: [3],
    });
    const bracket = this.bracketService.getBracketData();
    const id = this.activeRoute.snapshot.params.id;
    this.navUrl = bracket?.id || id ? '../' + this.navUrl : this.navUrl;
    if (bracket) {
      this.updateParticipantLimit(bracket);
      this.bracket.patchValue({
        ...bracket,
      });
    } else if (id) {
      this.fetchBracket(id);
    }
  }

  ngAfterContentChecked() {
    this.ref.detectChanges();
  }

  /**
   * Fatch bracket details By Id
   * @param id  Bracket ID
   */
  fetchBracket = async (id) => {
    try {
      this.isProcessing = true;
      const bracket = await this.bracketService.getBracketByID(id);
      this.updateParticipantLimit(bracket.data[0]);
      this.bracket.patchValue({
        ...bracket.data[0],
        id: bracket.data[0]._id,
      });
      this.isProcessing = false;
    } catch (error) {
      this.isProcessing = false;
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  /**
   * Move to preview Screen
   */
  preview() {
    if (this.bracket.invalid) {
      this.bracket.markAllAsTouched();
      return;
    }
    const value = this.bracket.value;
    this.bracketService.setBracketData(value);
    this.router.navigate([this.navUrl], { relativeTo: this.activeRoute });
  }

  addNewControlAndUpdateValidity = (
    field,
    fieldType,
    defaultValue,
    min?,
    max?,
    pattern?,
    customValidation?
  ) => {
    const formControl = this.bracket.get(field);
    const validation = [];

    switch (fieldType) {
      case 'NUMBER':
        validation.push(Validators.required);
        if (min) validation.push(Validators.min(min));
        if (max) validation.push(Validators.max(max));
        if (pattern) validation.push(pattern);
        if (customValidation) validation.push(customValidation);
        break;
      case 'BOOLEAN':
        validation.push(Validators.required);
        break;
      case 'STRING':
        validation.push(Validators.required);
        break;
    }

    if (formControl) {
      formControl.setValidators(validation);
      formControl.updateValueAndValidity();
    } else {
      this.bracket.addControl(
        field,
        new FormControl(defaultValue, Validators.compose(validation))
      );
    }
  };

  removeControlOrValidity = (field) => {
    const formControl = this.bracket.get(field);
    if (formControl) {
      this.bracket.removeControl(field);
    } else {
    }
  };

  updateParticipantLimit(data) {
    let maximumParticipants = 1024;

    switch (data?.bracketType) {
      case 'single':
        maximumParticipants = 1024;
        break;
      case 'double':
        maximumParticipants = 512;
        break;
      case 'round_robin':
        maximumParticipants = 40;
        break;
      case 'battle_royale':
        maximumParticipants = 1024;
        break;
    }

    this.addNewControlAndUpdateValidity(
      'maximumParticipants',
      'NUMBER',
      2,
      2,
      maximumParticipants,
      null,
      this.teamInGroupValidation('maximumParticipants').bind(this)
    );

    if (['single', 'double'].includes(data?.bracketType)) {
      this.multiStageTournamentHandler(false);
    } else {
      this.multiStageTournamentHandler(true, data);
    }
  }

  maxParticipantHandler(value, bracketType) {
    if (['battle_royale', 'round_robin'].includes(bracketType)) {
      const maxLimit =
        bracketType == 'battle_royale' ? value : value >= 24 ? 24 : value;
      this.addNewControlAndUpdateValidity(
        'noOfTeamInGroup',
        'NUMBER',
        2,
        2,
        maxLimit,
        false,
        this.teamInGroupValidation('noOfTeamInGroup').bind(this)
      );
    }
  }

  multiStageTournamentHandler = (isEnableFields: boolean, data?): void => {
    if (isEnableFields) {
      const noOfRound = data?.bracketType == 'round_robin' ? 2 : 10;
      const maxParticipants =
        data?.maximumParticipants || this.bracket.value.maximumParticipants;
      const maxLimit =
        data?.bracketType == 'battle_royale'
          ? maxParticipants
          : maxParticipants >= 24
          ? 24
          : maxParticipants;
      this.addNewControlAndUpdateValidity(
        'noOfTeamInGroup',
        'NUMBER',
        2,
        2,
        maxLimit,
        null,
        this.teamInGroupValidation('noOfTeamInGroup').bind(this)
      );
      this.addNewControlAndUpdateValidity(
        'noOfWinningTeamInGroup',
        'NUMBER',
        1,
        1,
        null,
        null,
        this.teamInGroupValidation('noOfWinningTeamInGroup').bind(this)
      );
      this.addNewControlAndUpdateValidity(
        'noOfRoundPerGroup',
        'NUMBER',
        1,
        1,
        noOfRound
      );
      this.addNewControlAndUpdateValidity('noOfStage', 'NUMBER', 1);
    } else {
      this.bracket.removeControl('noOfTeamInGroup');
      this.bracket.removeControl('noOfWinningTeamInGroup');
      this.bracket.removeControl('noOfRoundPerGroup');
      this.bracket.removeControl('noOfStage');
    }
  };

  teamInGroupValidation = (field?: string): ValidatorFn => {
    return (control: AbstractControl): { [key: string]: any } | null => {
      if (control['_parent']) {
        const {
          noOfTeamInGroup,
          noOfWinningTeamInGroup,
          maximumParticipants,
          noOfStage,
        } = control?.['_parent']?.controls;
        if (
          maximumParticipants?.valid &&
          noOfTeamInGroup?.value &&
          noOfWinningTeamInGroup?.value &&
          maximumParticipants?.value &&
          noOfStage
        ) {
          const fieldValidation = (
            maxParticipants,
            noOfTeamInGroup,
            noOfWinningTeam
          ) => {
            if (maxParticipants < noOfTeamInGroup) {
              return { max: { max: maxParticipants } };
            } else if (noOfTeamInGroup > noOfWinningTeam) {
              const check = noOfTeamInGroup % noOfWinningTeam != 0;
              return check ? { multiStageConfigError: true } : null;
            } else {
              return { multiStageConfigError: true };
            }
          };
          const errorConfig = fieldValidation(
            maximumParticipants?.value,
            noOfTeamInGroup?.value,
            noOfWinningTeamInGroup?.value
          );
          if (!errorConfig) {
            const totalStage = (maxP, teamPerG, winPerG, stage) => {
              const playerForNextRound = (maxP, teamPerG, winPerG) => {
                const diff = Math.ceil(maxP / teamPerG) - maxP / teamPerG;
                return diff < 0.5
                  ? Math.ceil(maxP / teamPerG) * winPerG
                  : Math.floor(maxP / teamPerG) * winPerG;
              };

              const createNoOfTeamInGroup = (
                pLen,
                noOfTeamInGroup,
                noOfWinningTeamInGroup
              ) => {
                const expectedNoOfGroup = Math.ceil(pLen / noOfTeamInGroup);
                const expectedMinPlayerInGroup = Math.floor(
                  pLen / expectedNoOfGroup
                );
                return expectedMinPlayerInGroup > noOfWinningTeamInGroup
                  ? expectedMinPlayerInGroup
                  : createNoOfTeamInGroup(
                      pLen,
                      noOfTeamInGroup + 1,
                      noOfWinningTeamInGroup
                    );
              };

              const teamPerGp = createNoOfTeamInGroup(maxP, teamPerG, winPerG);
              const nextStageP = playerForNextRound(maxP, teamPerGp, winPerG);

              return nextStageP == winPerG
                ? stage
                : nextStageP < teamPerG
                ? stage + 1
                : totalStage(nextStageP, teamPerG, winPerG, stage + 1);
            };

            const noOfStage = totalStage(
              maximumParticipants?.value,
              noOfTeamInGroup?.value,
              noOfWinningTeamInGroup?.value,
              1
            );

            this.bracket.get('noOfStage').setValue(noOfStage);
          }

          if (field == 'noOfTeamInGroup') {
            return errorConfig;
          } else {
            this.bracket.get('noOfTeamInGroup').setErrors(errorConfig);
            return null;
          }
        }
      } else {
        return null;
      }
    };
  };
}
