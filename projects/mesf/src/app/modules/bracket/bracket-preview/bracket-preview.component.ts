import {
  Component,
  OnInit,
  ChangeDetectorRef,
  AfterContentChecked,
} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { IBracket } from '../../../shared/models';
import { Location } from '@angular/common';
import { TranslateService } from '@ngx-translate/core';
import {
  BracketService,
  LanguageService,
  ToastService,
} from '../../../core/service';

@Component({
  selector: 'app-bracket-preview',
  templateUrl: './bracket-preview.component.html',
  styleUrls: [
    './bracket-preview.component.scss',
    '../../tournament/create/create.component.scss',
  ],
})
export class BracketPreviewComponent implements OnInit, AfterContentChecked {
  bracket: IBracket;
  structure: any;
  isProcessing = false;
  isLoaded = false;

  constructor(
    public location: Location,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private bracketService: BracketService,
    private toastService: ToastService,
    private translateService: TranslateService,
    private languageService: LanguageService,
    private ref: ChangeDetectorRef
  ) {}

  ngOnInit(): void {
    this.languageService.language.subscribe((lang) =>
      this.translateService.use(lang || 'en')
    );
    this.bracket = this.bracketService.getBracketData();
    if (!this.bracket) {
      this.router.navigate(['../create'], { relativeTo: this.activeRoute });
    } else {
      this.fetchBracketStructure(this.bracket);
    }
  }

  ngAfterContentChecked() {
    this.ref.detectChanges();
  }

  /**
   * Fetch Mock Bracket or Match Structure
   * @param payload content meta data to generate bracket
   */
  fetchBracketStructure = async (payload) => {
    try {
      this.isLoaded = false;
      const response = await this.bracketService.generateBracket(payload);
      this.structure = response.data;
      this.isLoaded = true;
    } catch (error) {
      this.isLoaded = true;
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  /**
   * Save Or Update Bracket Details
   */
  save = async () => {
    try {
      this.isProcessing = true;
      const response = this.bracket?.id
        ? await this.bracketService.updateBracket(this.bracket.id, this.bracket)
        : await this.bracketService.saveBracket(this.bracket);
      this.toastService.showSuccess(response?.message);
      this.router.navigate(['../../profile/my-bracket'], {
        relativeTo: this.activeRoute,
      });

      this.bracketService.setBracketData(null);
      this.isProcessing = false;
    } catch (error) {
      this.isProcessing = false;
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };
}
