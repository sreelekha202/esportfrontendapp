import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TournamentTrackerCardComponent } from './tournament-tracker-card.component';

describe('TournamentTrackerCardComponent', () => {
  let component: TournamentTrackerCardComponent;
  let fixture: ComponentFixture<TournamentTrackerCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TournamentTrackerCardComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TournamentTrackerCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
