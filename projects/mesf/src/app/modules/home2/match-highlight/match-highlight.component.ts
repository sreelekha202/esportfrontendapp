import { Component, OnInit, Input } from '@angular/core';
import { LanguageService } from '../../../core/service';
import { EsportsTimezone } from 'esports';

@Component({
  selector: 'app-match-highlight',
  templateUrl: './match-highlight.component.html',
  styleUrls: ['./match-highlight.component.scss'],
})
export class MatchHighlightComponent implements OnInit {
  @Input() params;
  timezone;

  constructor(public languageService: LanguageService, private esportsTimezone: EsportsTimezone) {}

  ngOnInit(): void {
    this.timezone = this.esportsTimezone.getTimeZoneName();
  }

  isNumber(val): boolean { const str: string = val.toString(); return str.startsWith("+"); }
}
