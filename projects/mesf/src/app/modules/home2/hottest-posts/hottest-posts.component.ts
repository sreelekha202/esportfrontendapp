import { Component, OnInit, Input } from '@angular/core';
import { LanguageService } from '../../../core/service';
@Component({
  selector: 'app-hottest-posts',
  templateUrl: './hottest-posts.component.html',
  styleUrls: ['./hottest-posts.component.scss'],
})
export class HottestPostsComponent implements OnInit {
  @Input() params: any;

  constructor(public languageService: LanguageService) {}

  ngOnInit() {}
}
