import { Component, OnInit, Input } from '@angular/core';
import { LanguageService } from '../../../core/service';
export interface GameCard {
  img: string;
  name: string;
  date: string | Date;
  description: string;
}

@Component({
  selector: 'app-games-cards',
  templateUrl: './games-cards.component.html',
  styleUrls: ['./games-cards.component.scss'],
})
export class GamesCardsComponent implements OnInit {
  @Input() cards = [];

  constructor(public languageService: LanguageService) {}

  ngOnInit(): void {}
}
