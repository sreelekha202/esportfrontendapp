import { Component, OnInit } from '@angular/core';

import {
  VideoLibraryService,
  UtilsService,
  HomeService,
  TournamentService,
  UserService,
  ToastService,
} from '../../core/service';
import {
  InfoPopupComponent,
  InfoPopupComponentData,
  InfoPopupComponentType,
} from '../../shared/popups/info-popup/info-popup.component';
import { GlobalUtils } from '../../shared/service/global-utils/global-utils';
import { MatDialog } from '@angular/material/dialog';
import { AppHtmlRoutes } from '../../app-routing.model';
import { IUser } from '../../shared/models';

@Component({
  selector: 'app-home2',
  templateUrl: './home2.component.html',
  styleUrls: ['./home2.component.scss'],
})
export class Home2Component implements OnInit {
  user: IUser;
  AppHtmlRoutes = AppHtmlRoutes;
  postData;
  trendingPosts;
  tournamentsData = [];
  isAnnouncementAvailabale = false;
  announcementData;
  featureTournamentsData = [];

  ongoingTournamentsData = [];

  tournamentsSlides = [];

  gamesCards = [
    {
      img: 'assets/images/GamesCards/free-fire.png',
      name: 'free fire',
      date: 'August 30, 2020',
      description:
        'The Ups and downs being an assasian in sPEED DRIFTERS that you may relate to and also will be inspire to play also will be inspire to play...',
    },
    {
      img: 'assets/images/GamesCards/free-fire.png',
      name: 'VALORANT',
      date: 'August 30, 2020',
      description:
        'The Ups and downs being an assasian in Valorant that you may relate to and also will be inspire to play...',
    },
    {
      img: 'assets/images/GamesCards/speed-drifters.png',
      name: 'SPEED DRIFTERS',
      date: 'August 30, 2020',
      description:
        'The Ups and downs being an assasian in sPEED DRIFTERS that you may relate to and also will be inspire to play…',
    },
    {
      img: 'assets/images/GamesCards/heartstone.png',
      name: 'HEARTSTONE',
      date: 'August 30, 2020',
      description:
        'The Ups and downs being an assasian in sPEED DRIFTERS that you may relate to and also will be inspire to play…',
    },
    {
      img: 'assets/images/GamesCards/drift-masters.png',
      name: 'VALORANT',
      date: 'August 30, 2020',
      description:
        'The Ups and downs being an assasian in sPEED DRIFTERS that you may relate to and also will be inspire to play…',
    },
    {
      img: 'assets/images/GamesCards/drift-masters.png',
      name: 'HEARTSTONE',
      date: 'August 30, 2020',
      description:
        'The Ups and downs being an assasian in sPEED DRIFTERS that you may relate to and also will be inspire to play…',
    },
    {
      img: 'assets/images/GamesCards/drift-masters.png',
      name: 'VALORANT',
      date: 'August 30, 2020',
      description:
        'The Ups and downs being an assasian in sPEED DRIFTERS that you may relate to and also will be inspire to play…',
    },
    {
      img: 'assets/images/GamesCards/drift-masters.png',
      name: 'ARTICLE TITLE',
      date: 'August 30, 2020',
      description:
        'The Ups and downs being an assasian in sPEED DRIFTERS that you may relate to and also will be inspire to play…',
    },
    {
      img: 'assets/images/GamesCards/drift-masters.png',
      name: 'ARTICLE TITLE',
      date: 'August 30, 2020',
      description:
        'The Ups and downs being an assasian in sPEED DRIFTERS that you may relate to and also will be inspire to play…',
    },
    {
      img: 'assets/images/GamesCards/drift-masters.png',
      name: 'ARTICLE TITLE',
      date: 'August 30, 2020',
      description:
        'The Ups and downs being an assasian in sPEED DRIFTERS that you may relate to and also will be inspire to play…',
    },
    {
      img: 'assets/images/GamesCards/drift-masters.png',
      name: 'ARTICLE TITLE',
      date: 'August 30, 2020',
      description:
        'The Ups and downs being an assasian in sPEED DRIFTERS that you may relate to and also will be inspire to play…',
    },
  ];

  topPlayers = [];

  highlights = [];

  news = [
    {
      title: 'CLASH ROYALES BEST MO..',
      text: 'Team ultimately won the championship with highest score...',
      date: 'August 20, 2020',
    },
    {
      title: 'CLASH ROYALES BEST MO..',
      text: 'Team ultimately won the championship with highest score...',
      date: 'August 20, 2020',
    },
    {
      title: 'CLASH ROYALES BEST MO..',
      text: 'Team ultimately won the championship with highest score...',
      date: 'August 20, 2020',
    },
    {
      title: 'CLASH ROYALES BEST MO..',
      text: 'Team ultimately won the championship with highest score...',
      date: 'August 20, 2020',
    },
    {
      title: 'CLASH ROYALES BEST MO..',
      text: 'Team ultimately won the championship with highest score...',
      date: 'August 20, 2020',
    },
    {
      title: 'CLASH ROYALES BEST MO..',
      text: 'Team ultimately won the championship with highest score...',
      date: 'August 20, 2020',
    },
    {
      title: 'CLASH ROYALES BEST MO..',
      text: 'Team ultimately won the championship with highest score...',
      date: 'August 20, 2020',
    },
  ];

  leaderBoardRows = [];
  leaderBoardColumns = [
    { name: 'RANK' },
    { name: 'Player Name' },
    // { name: 'GAME' },
    { name: 'REGION' },
    { name: 'TROPHIES' },
    { name: 'POINTS' },
  ];
  allTournamentsData = [
    { gameName: 'PUBG' },
    { gameName: 'CS:GO' },
    { gameName: 'Fortnite' },
    { gameName: 'Call Of Duty' },
  ];

  constructor(
    private toastService: ToastService,
    private videoLibraryService: VideoLibraryService,
    private utilsService: UtilsService,
    private homeService: HomeService,
    private tournamentService: TournamentService,
    public matDialog: MatDialog,
    private userService: UserService
  ) {}

  ngOnInit(): void {
    this.getCurrentUserDetails();
    this.getAnnouncement();
    this.getTournament();
    this.getOnGoingTournaments();
    //this.getHottestPost();
    this.getTrendingPost();
    this.fetchVideos();
    this.getBanner();
    this.getTopPlayers();
    this.getLeaderBoard();
  }

  getHottestPost() {
    this.homeService.hottest_post().subscribe((data) => {});
  }
  getTrendingPost() {
    this.homeService.trending_posts().subscribe((data) => {
      this.postData = data.data[0] || {};
      data.data.splice(0, 1);
      this.trendingPosts = data.data || [];
    });
  }

  /**
   * method to call the announcement data
   */
  getAnnouncement() {
    this.homeService.getAnnouncements().subscribe((res: any) => {
      if (res.data && res.data.length > 0) {
        let data = res.data.map((ele) => {
          return {
            uniqueId: ele._id,
            image: ele.announcementFileUrl,
            header: ele.header,
            description: ele.description,
            destination: ele.destination,
          };
        });
        this.announcementData = data[0];
      }
      if (this.announcementData?.uniqueId && GlobalUtils.isBrowser()) {
        let itemId = localStorage.getItem('announcement');
        if (itemId != this.announcementData?.uniqueId) {
          this.isAnnouncementAvailabale = true;
        } else {
          this.isAnnouncementAvailabale = false;
        }
      }
    });
  }

  /**
   * method to get leader board data
   */
  getLeaderBoard() {
    this.homeService._leaderBoard().subscribe((data) => {
      this.leaderBoardRows = data.data.leaderboardData;
      if (this.leaderBoardRows) {
        this.leaderBoardRows.forEach((obj, index) => {
          obj.rank = index + 1;
        });
      }
    });
  }

  fetchVideos = async () => {
    try {
      const pagination = {
        page: 1,
        limit: 3,
        pagination: true,
        sort: '-updatedOn',
        projection: [
          '_id',
          'title',
          'description',
          'youtubeUrl',
          'updatedOn',
          'thumbnailUrl',
          'slug',
        ],
      };
      const query = `?pagination=${encodeURIComponent(
        JSON.stringify(pagination)
      )}`;
      const response = await this.videoLibraryService.fetchVideoLibrary(query);
      const promiseArray = response?.data?.docs.map(async (item) => {
        return {
          ...item,
          youtubeUrl: await this.utilsService.getEmbbedUrl(item.youtubeUrl),
        };
      });
      this.highlights = await Promise.all(promiseArray);
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  /**
   * method to get the banner data
   */
  getBanner() {
    this.homeService._getBanner().subscribe(
      (res: any) => {
        this.tournamentsSlides = res.data;
      },
      (err: any) => {}
    );
  }

  /**
   * method to get the top players data
   */
  getTopPlayers() {
    this.homeService._topPlayers().subscribe(
      (res: any) => {
        this.topPlayers = res.data.map((ele) => {
          return {
            fullName: ele?.fullName,
            photo: ele?.profilePicture
              ? ele?.profilePicture
              : './assets/images/Profile/article01.png',
          };
        });
      },
      (err: any) => {}
    );
  }
  getTournament() {
    // let query: any = {
    //   $and: [{ tournamentStatus: 'publish' }],
    //   $or: [{ startDate: { $lt: new Date() } }, { isSeeded: true }],
    // };
    // query = JSON.stringify(query);

    this.tournamentService
      .getPaginatedTournaments({
        status: '0',
        limit: 6,
        sort: 'participantJoined',
      })
      .subscribe(
        (res: any) => {
          // this.showLoader = false;
          this.tournamentsData = res.data.docs;
        },
        (err) => {
          // this.showLoader = false;
        }
      );
  }

  getOnGoingTournaments() {
    this.tournamentService
      .getPaginatedTournaments({
        status: '6',
        limit: 5,
        sort: 'participantJoined',
      })
      .subscribe(
        (res: any) => {
          this.featureTournamentsData = res.data.docs;
        },
        (err) => {
          // this.showLoader = false;
        }
      );
  }

  getRegistrationStatus(
    startDate,
    isSeeded,
    isFinished,
    maxParticipants,
    participantsJoined
  ) {
    if (!startDate) {
      return;
    }
    const sDate = new Date(startDate);
    if (isSeeded == true || isFinished == true) {
      return 'Closed';
    } else if (participantsJoined >= maxParticipants) {
      return 'Closed';
    } else if (sDate.getTime() < Date.now()) {
      return 'Closed';
    } else {
      return 'Open';
    }
  }

  convert12HrsTo24HrsFormat(time) {
    if (!time) {
      return;
    }
    var hours = Number(time.match(/^(\d+)/)[1]);
    var minutes = Number(time.match(/:(\d+)/)[1]);
    var AP = time.match(/\s(.*)$/);
    if (!AP) AP = time.slice(-2);
    else AP = AP[1];
    if (AP == 'PM' && hours < 12) hours = hours + 12;
    if (AP == 'AM' && hours == 12) hours = hours - 12;
    var Hours24 = hours.toString();
    var Minutes24 = minutes.toString();
    if (hours < 10) Hours24 = '0' + Hours24;
    if (minutes < 10) Minutes24 = '0' + Minutes24;

    return { hour: parseInt(Hours24), minute: parseInt(Minutes24) };
  }

  openSocialShareComponent() {
    const data: InfoPopupComponentData = {
      title: `Share Tournament via`,
      text: ``,
      type: InfoPopupComponentType.socialSharing,
      cancelBtnText: 'Close',
    };
    this.matDialog.open(InfoPopupComponent, {
      data,
    });
  }
  getCurrentUserDetails() {
    this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.user = data;
      }
    });
  }
}
