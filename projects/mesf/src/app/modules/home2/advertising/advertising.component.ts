import { Component, OnInit, Input } from '@angular/core';

export interface Advertising {
  logoUrl: string;
  backgroundImageUrl: string;
  title: string;
}

@Component({
  selector: 'app-advertising',
  templateUrl: './advertising.component.html',
  styleUrls: ['./advertising.component.scss'],
})
export class AdvertisingComponent implements OnInit {
  @Input() params: Advertising;

  constructor() {}

  ngOnInit(): void {}
}
