import { ElementRef } from '@angular/core';
import { ViewChild } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { LanguageService, TournamentService } from '../../../core/service';
import { AppHtmlRoutes } from '../../../app-routing.model';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { Router, NavigationEnd } from '@angular/router';
import { GlobalUtils } from '../../../shared/service/global-utils/global-utils';
@Component({
  selector: 'app-generasi-bijak',
  templateUrl: './generasi-bijak.component.html',
  styleUrls: ['./generasi-bijak.component.scss'],
})
export class GenerasiBijakComponent implements OnInit {
  @ViewChild('mainContainer', { read: ElementRef }) public scroll: ElementRef<
    any
  >;
  AppHtmlRoutes = AppHtmlRoutes;
  faSearch = faSearch;
  tournamentData;
  currentTab = 'Primary schools';
  showLoader = true;
  primary: any = [];

  secondary: any = [];
  bannerList: any = [
    {
      banner: '/assets/images/Home/tournamentpage-banner-new.jpg',
    },
    // {
    //   banner: '/assets/images/Home/tournamentpage-banner.jpg',
    //   title: 'GenerasiBijak.SECTIONA.XOX_TITLE',
    //   desc: 'GenerasiBijak.SECTIONA.XOX_DESC',
    //   desc1: 'GenerasiBijak.SECTIONA.XOX_DESC1',
    //   desc2: 'GenerasiBijak.SECTIONA.XOX_DESC2',
    //   desc3: 'GenerasiBijak.SECTIONA.XOX_DESC3',
    // },
    // {
    //   banner: '/assets/images/Home/homepage-banner.jpg',
    //   title: 'GenerasiBijak.SECTIONA.XOX_TITLE',
    //   desc: 'GenerasiBijak.SECTIONA.XOX_DESC',
    //   desc1: 'GenerasiBijak.SECTIONA.XOX_DESC1',
    //   desc2: 'GenerasiBijak.SECTIONA.XOX_DESC2',
    //   desc3: 'GenerasiBijak.SECTIONA.XOX_DESC3',
    // },
  ];
  showNavigationArrows = false;
  showNavigationIndicators = true;

  constructor(
    private tournamentService: TournamentService,
    private translateService: TranslateService,
    private languageService: LanguageService,
    private config: NgbCarouselConfig,
    private router: Router
  ) {
    this.config.interval = 6000;
    config.showNavigationArrows = true;
    config.showNavigationIndicators = true;
  }

  ngOnInit(): void {
    if (GlobalUtils.isBrowser()) {
      this.languageService.language.subscribe((lang) =>
        this.translateService.use(lang || 'en')
      );
      let status = 7;
      this.getTournaments(status);
      this.router.events.subscribe((evt) => {
        if (!(evt instanceof NavigationEnd)) {
          return;
        }
        window.scrollTo(0, 0);
      });
    }
  }

  ngAfterViewInit() {
    //this.scrollToTop();
  }

  changeTab(tab) {
    this.currentTab = tab;
    switch (tab) {
      case 'Primary schools':
        this.tournamentData = this.primary;
        break;
      case 'Secondary schools':
        this.tournamentData = this.secondary;
        break;
    }
  }

  getTournaments = async (status) => {
    try {
      this.showLoader = true;
      const params = { status, sort: 'New' };
      const res: any = await this.tournamentService
        .getgenerasi_bijak_tournaments(params)
        .toPromise();
      this.showLoader = false;
      const nonfilteredData = res.data;
      this.primary = nonfilteredData.filter(
        (x) =>
          x.name.toLowerCase().indexOf('primary schools') > -1 ||
          x.name.toLowerCase().indexOf('sek ren') > -1
      );
      this.secondary = nonfilteredData.filter(
        (x) =>
          x.name.toLowerCase().indexOf('secondary schools') > -1 ||
          x.name.toLowerCase().indexOf('sek men') > -1
      );
      this.tournamentData = this.primary;
    } catch (error) {
      this.showLoader = false;
    }
  };

  scrollToTop() {
    this.scroll.nativeElement.scrollIntoView();
  }
}
