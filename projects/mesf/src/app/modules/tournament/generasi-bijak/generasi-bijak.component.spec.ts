import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GenerasiBijakComponent } from './generasi-bijak.component';

describe('GenerasiBijakComponent', () => {
  let component: GenerasiBijakComponent;
  let fixture: ComponentFixture<GenerasiBijakComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GenerasiBijakComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenerasiBijakComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
