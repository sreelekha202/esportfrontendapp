import { XoxUnityComponent } from './xox-unity/xox-unity.component';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { TournamentInfoComponent } from './tournament-info/tournament-info.component';
import { TournamentComponent } from './tournament.component';
import { AuthGuard } from '../../shared/guard/auth.guard';

const routes: Routes = [
  { path: '', component: TournamentComponent },
  {
    path: 'info',
    component: TournamentInfoComponent,
    data: {
      tags: [
        {
          name: 'title',
          content: 'Create Tournament in 3 Steps| Malaysia Esports Federation',
        },
      ],
      title: 'Create Tournament in 3 Steps| Malaysia Esports Federation',
    },
  },
  {
    path: 'create',
    canActivate: [AuthGuard],
    loadChildren: () =>
      import('../../modules/tournament/create/create.module').then(
        (m) => m.CreateModule
      ),
    data: {
      tags: [
        {
          name: 'title',
          content: 'Create Tournament | Malaysia Esports Federation',
        },
      ],
      title: 'Create Tournament | Malaysia Esports Federation',
    },
  },
  {
    path: 'edit/:id',
    canActivate: [AuthGuard],
    loadChildren: () =>
      import('../../modules/tournament/create/create.module').then(
        (m) => m.CreateModule
      ),
  },
  {
    path: 'manage/:id',
    canActivate: [AuthGuard],
    loadChildren: () =>
      import(
        '../../modules/tournament/tournament-management/tournament-management.module'
      ).then((m) => m.TournamentManagementModule),
  },
  {
    path: ':id',
    loadChildren: () =>
      import(
        '../../modules/tournament/view-tournament/view-tournament.module'
      ).then((m) => m.ViewTournamentModule),
    // data: {
    //   tags: [
    //     {
    //       name: 'title',
    //       content: 'Tournaments "game name" | Malaysia Esports Federation',
    //     },
    //   ],
    //   title: 'Tournaments "game name" | Malaysia Esports Federation',
    // }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TournamentRoutingModule {}
