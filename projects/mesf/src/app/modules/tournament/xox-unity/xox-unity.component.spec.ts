import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { XoxUnityComponent } from './xox-unity.component';

describe('XoxUnityComponent', () => {
  let component: XoxUnityComponent;
  let fixture: ComponentFixture<XoxUnityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [XoxUnityComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(XoxUnityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
