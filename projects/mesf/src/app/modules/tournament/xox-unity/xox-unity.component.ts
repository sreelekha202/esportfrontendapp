import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';
import { LanguageService, TournamentService } from '../../../core/service';
import { AppHtmlRoutes } from '../../../app-routing.model';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-xox-unity',
  templateUrl: './xox-unity.component.html',
  styleUrls: ['./xox-unity.component.scss'],
})
export class XoxUnityComponent implements OnInit {
  @ViewChild('mainContainer', { read: ElementRef }) public scroll: ElementRef<
    any
  >;
  AppHtmlRoutes = AppHtmlRoutes;
  faSearch = faSearch;
  tournamentData;
  currentTab = 'regional';
  showLoader = true;
  bannerList: any = [
    {
      banner: '/assets/images/Home/tournamentpage-banner.jpg',
      title: 'ESPORTS.SECTIONA.XOX_TITLE',
      desc: 'ESPORTS.SECTIONA.XOX_DESC',
      desc1: 'ESPORTS.SECTIONA.XOX_DESC1',
      desc2: 'ESPORTS.SECTIONA.XOX_DESC2',
    },
    {
      banner: '/assets/images/Home/homepage-banner.jpg',
      title: 'ESPORTS.SECTIONA.XOX_TITLE',
      desc: 'ESPORTS.SECTIONA.XOX_DESC',
      desc1: 'ESPORTS.SECTIONA.XOX_DESC1',
      desc2: 'ESPORTS.SECTIONA.XOX_DESC2',
    },
  ];
  showNavigationArrows = false;
  showNavigationIndicators = true;

  constructor(
    private tournamentService: TournamentService,
    private translateService: TranslateService,
    private languageService: LanguageService,
    private config: NgbCarouselConfig
  ) {
    this.config.interval = 6000;
    config.showNavigationArrows = true;
    config.showNavigationIndicators = true;
  }

  ngOnInit(): void {
    this.languageService.language.subscribe((lang) =>
      this.translateService.use(lang || 'en')
    );
    this.changeTab('regional');
  }

  ngAfterViewInit() {
    this.scrollToTop();
  }

  changeTab(tab) {
    let status = 7;
    this.currentTab = tab;
    switch (tab) {
      case 'regional':
        break;
      case 'grandMaster':
        status = 8;
        break;
      case 'grandFinal':
        status = 9;
        break;
    }
    this.getTournaments(status);
  }

  getTournaments = async (status) => {
    try {
      this.showLoader = true;
      const params = { status, sort: 'New' };
      const res: any = await this.tournamentService
        .getPaginatedTournaments(params)
        .toPromise();
      this.showLoader = false;
      this.tournamentData = res.data;
    } catch (error) {
      this.showLoader = false;
    }
  };

  scrollToTop() {
    this.scroll.nativeElement.scrollIntoView();
  }
}
