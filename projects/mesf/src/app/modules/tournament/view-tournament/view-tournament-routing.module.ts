import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ViewTournamentComponent } from './view-tournament.component';
import { JoinParticipantComponent } from './join-participant/join-participant.component';
import { AuthGuard } from '../../../shared/guard/auth.guard';

const routes: Routes = [
  { path: '', component: ViewTournamentComponent },
  {
    path: 'join',
    canActivate: [AuthGuard],
    component: JoinParticipantComponent,
  },
  {
    path: 'join/:pId',
    canActivate: [AuthGuard],
    component: JoinParticipantComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ViewTournamenttRoutingModule {}
