import {
  Component,
  OnInit,
  Input,
  OnChanges,
  SimpleChanges,
} from '@angular/core';
import { TournamentService, ToastService } from '../../../../core/service';

@Component({
  selector: 'app-participants',
  templateUrl: './participants.component.html',
  styleUrls: [
    './participants.component.scss',
    '../view-tournament.component.scss',
  ],
})
export class ParticipantsComponent implements OnInit, OnChanges {
  @Input() tournamentId: string;

  approvedParticipants: Array<{}> = [];
  isLoaded = false;

  constructor(
    private tournamentService: TournamentService,
    private toastService: ToastService
  ) {}

  ngOnInit(): void {}

  fetchAllApprovedParticipants = async () => {
    try {
      this.isLoaded = false;
      const encodeUrl = `?query=${encodeURIComponent(
        JSON.stringify({
          tournamentId: this.tournamentId,
          participantStatus: 'approved',
        })
      )}`;
      const participants = await this.tournamentService
        .getParticipants(encodeUrl)
        .toPromise();
      this.approvedParticipants = participants.data;
      this.isLoaded = true;
    } catch (error) {
      this.isLoaded = true;
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  ngOnChanges(simpleChanges: SimpleChanges) {
    if (
      simpleChanges.hasOwnProperty('tournamentId') &&
      simpleChanges.tournamentId.currentValue
    ) {
      this.fetchAllApprovedParticipants();
    }
  }
}
