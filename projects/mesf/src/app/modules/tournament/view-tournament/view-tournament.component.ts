import { TranslateService } from '@ngx-translate/core';
import { MatDialog } from '@angular/material/dialog';
import { Component, OnDestroy, OnInit, Inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import {
  InfoPopupComponent,
  InfoPopupComponentData,
  InfoPopupComponentType,
} from '../../../shared/popups/info-popup/info-popup.component';
import {
  RatingService,
  TournamentService,
  UserService,
  ToastService,
  ParticipantService,
} from '../../../core/service';
import { GlobalUtils } from '../../../shared/service/global-utils/global-utils';
import { ITournament, IUser } from '../../../shared/models';
import { Subscription } from 'rxjs';
import { Title } from '@angular/platform-browser';
import { DOCUMENT } from '@angular/common';
import { EsportsTimezone } from 'esports';
@Component({
  selector: 'app-view-tournament',
  templateUrl: './view-tournament.component.html',
  styleUrls: ['./view-tournament.component.scss'],
})
export class ViewTournamentComponent implements OnInit, OnDestroy {
  tournament: ITournament;
  user: IUser;

  active = 1;
  isLoaded = false;
  hideMessage = true;
  enableRating = false;
  enableComment: boolean = false;
  text: string | null;
  domain: string;
  isOrganizer: boolean = false;
  isParticipant: boolean = false;

  userSubscription: Subscription;
  timezone;

  constructor(
    private tournamentService: TournamentService,
    private activatedRoute: ActivatedRoute,
    private toastService: ToastService,
    private matDialog: MatDialog,
    private ratingService: RatingService,
    private translateService: TranslateService,
    private globalUtils: GlobalUtils,
    public userService: UserService,
    private participantService: ParticipantService,
    private titleService: Title,
    @Inject(DOCUMENT) private document: Document,
    private router: Router,
    private esportsTimezone: EsportsTimezone
  ) {}

  ngOnInit(): void {
    this.timezone = this.esportsTimezone.getTimeZoneName();
    const slug = this.activatedRoute.snapshot.params.id; // get slug from route
    this.getCurrentUserDetails();
    this.fetchTournamentDetails(slug);
    this.domain =
      this.document.location.protocol + '//' + this.document.location.hostname;
  }

  isNumber(val): boolean { const str: string = val.toString(); return str.startsWith("+"); }
  
  ngOnDestroy() {
    this.userSubscription?.unsubscribe();
  }

  /**
   * Fetch Tournament Details
   * @param slug tournamentSlug
   */
  fetchTournamentDetails = async (slug) => {
    try {
      this.isLoaded = false;
      const query = JSON.stringify({ slug, tournamentStatus: 'publish' });
      const tournament = await this.tournamentService
        .getTournaments({ query })
        .toPromise();
      this.tournament = tournament.data.length ? tournament.data[0] : null;
      if (this.tournament?.organizerDetail?._id == this.user?._id) {
        this.isOrganizer = true;
      }

      if (!this.tournament) {
        this.hideMessage = false;
        throw new Error(this.translateService.instant('VIEW_TOURNAMENT.ERROR'));
      }

      if (
        (this.tournament && this.tournament.banner) ||
        this.tournament.gameDetail.logo
      ) {
        this.titleService.setTitle(this.tournament.name);
        this.globalUtils.setMetaTags([
          {
            property: 'twitter:image',
            content:
              this.tournament.banner ||
              this.tournament.gameDetail.logo,
          },
          {
            property: 'og:image',
            content:
              this.tournament.banner ||
              this.tournament.gameDetail.logo,
          },
          {
            property: 'og:image:secure_url',
            content:
              this.tournament.banner ||
              this.tournament.gameDetail.logo,
          },
          {
            property: 'og:image:url',
            content:
              this.tournament.banner ||
              this.tournament.gameDetail.logo,
          },
          {
            property: 'og:image:width',
            content: '1200',
          },
          {
            property: 'og:image:height',
            content: '630',
          },
          {
            name: 'description',
            content: this.tournament.description
          },
          {
            property: 'og:description',
            content:  this.tournament.description
          },
          {
            property: 'twitter:description',
            content:  this.tournament.description
          },
          {
            name: 'title',
            content: this.tournament.name,
          },
          {
            name: 'title',
            content: this.tournament.name,
          },
          {
            property: 'og:title',
            content: this.tournament.name,
          },
          {
            property: 'twitter:title',
            content: this.tournament.name,
          },
          {
            property: 'og:url',
            content: this.domain + this.router.url,
          },
        ]);
      }
      const data = await this.fetchParticipantRegisterationStatus(
        this.tournament?._id
      );
      if (data.type == 'already-registered') {
        this.isParticipant = true;
      }
      this.isLoaded = true;
    } catch (error) {
      this.isLoaded = true;
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  /**
   * Get Child Component Response
   */
  buttonResponse(data) {
    if (data) {
      this.fetchTournamentDetails(this.tournament?.slug);
    }
  }

  /**
   * Get Authorized User For Rating
   */
  getJoinButtonResponse(data) {
    this.enableRating = data;
  }

  /**
   * Authorized User Can Rate Tournament
   */
  rateUs = async () => {
    try {
      const query = `?query=${encodeURIComponent(
        JSON.stringify({
          posterId: this.tournament._id,
          type: 'Tournament',
        })
      )}`;
      const rating = await this.ratingService.getRating(query);
      const rate = rating.data || {
        raterId: this.user._id,
        posterId: this.tournament._id,
        value: 3,
        type: 'Tournament',
      };
      const blockUserData: InfoPopupComponentData = {
        title: 'Rate US',
        text: `Your feedback is valuable for us.`,
        type: InfoPopupComponentType.rating,
        btnText: 'Confirm',
        rate: rate.value,
      };

      const dialogRef = this.matDialog.open(InfoPopupComponent, {
        data: blockUserData,
      });

      const confirmed = await dialogRef.afterClosed().toPromise();

      if (confirmed) {
        rate.value = confirmed;
        const addOrUpdateRating = rate._id
          ? await this.ratingService.updateRating(rate._id, rate)
          : await this.ratingService.addRating(rate);
        this.toastService.showSuccess(addOrUpdateRating?.message);
      }
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  /**
   * Share Tournament On Various Platform
   */
  shareTournament = async () => {
    try {
      const data: InfoPopupComponentData = {
        title: 'Share Tournament via',
        text: ``,
        type: InfoPopupComponentType.socialSharing,
        cancelBtnText: 'Close',
      };

      const dialogRef = this.matDialog.open(InfoPopupComponent, {
        data,
      });

      await dialogRef.afterClosed().toPromise();
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  getCurrentUserDetails() {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.user = data;
      }
    });
  }

  fetchParticipantRegisterationStatus = async (id: string) => {
    try {
      const {
        data,
      } = await this.participantService.fetchParticipantRegistrationStatus(id);
      this.enableComment = this.tournament?.organizerDetail?._id == this.user._id || !!data?.participantId;
      if (!this.enableComment) {
        if (
          !['tournament-finished', 'tournament-started'].includes(data.type)
        ) {
          this.text = 'DISCUSSION.PARTICIPANT_NA';
        }
      }
      return data;
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };
}
