import {
  Component,
  OnInit,
  Input,
  OnChanges,
  Output,
  EventEmitter,
} from '@angular/core';
import {
  TournamentService,
  ParticipantService,
  ToastService,
} from '../../../../core/service';
import { IUser } from '../../../../shared/models';

@Component({
  selector: 'app-join-button',
  templateUrl: './join-button.component.html',
  styleUrls: [
    './join-button.component.scss',
    '../view-tournament.component.scss',
  ],
})
export class JoinButtonComponent implements OnInit, OnChanges {
  @Input() tournamentId: string;
  @Input() user: IUser;

  @Output() isRefreshTournament = new EventEmitter<boolean>(false);
  @Output() isAuthorizedUser = new EventEmitter<any>();

  btnType: string = '';
  participantId: string | null;

  constructor(
    private tournamentService: TournamentService,
    private participantService: ParticipantService,
    private toastService: ToastService
  ) {}

  ngOnInit(): void {}

  ngOnChanges() {
    if (this.tournamentId) {
      this.fetchParticipantRegisterationStatus(this.tournamentId);
    }
  }

  fetchParticipantRegisterationStatus = async (id: string) => {
    try {
      const {
        data,
      } = await this.participantService.fetchParticipantRegistrationStatus(id);
      this.btnType = data?.type;
      this.participantId = data?.participantId;

      if (this.btnType == 'tournament-started' && this.participantId) {
        this.isAuthorizedUser.emit(true);
      }
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  participantCheckIn = async () => {
    try {
      const participant = await this.tournamentService
        .updateParticipant(this.participantId, { checkedIn: true })
        .toPromise();
      this.toastService.showSuccess(participant.message);
      this.isRefreshTournament.emit(true);
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };
}
