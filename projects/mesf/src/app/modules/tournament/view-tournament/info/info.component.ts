import {
  Component,
  OnInit,
  Input,
  OnChanges,
  SimpleChanges,
} from '@angular/core';

@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.scss', '../view-tournament.component.scss'],
})
export class InfoComponent implements OnInit, OnChanges {
  @Input() tournamentDetails;
  constructor() {}

  ngOnInit(): void {}

  ngOnChanges(simpleChanges: SimpleChanges) {}

  buttonResponse(event) {}
}
