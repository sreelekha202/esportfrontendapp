import {
  Component,
  OnInit,
  Input,
  OnChanges,
  SimpleChanges,
  OnDestroy,
} from '@angular/core';
import { Subscription } from 'rxjs';
import {
  BracketService,
  UserService,
  TournamentService,
  ToastService,
} from '../../../../core/service';

@Component({
  selector: 'app-brackets',
  templateUrl: './brackets.component.html',
  styleUrls: ['./brackets.component.scss', '../view-tournament.component.scss'],
})
export class BracketsComponent implements OnInit, OnChanges, OnDestroy {
  @Input() tournamentDetails;
  userId;
  structure;
  participantId = null;
  participantList: Array<{}> = [];
  apiLoaded: Array<boolean> = [];
  isLoaded = false;

  userSubscription: Subscription;

  constructor(
    private bracketService: BracketService,
    private toastService: ToastService,
    private userService: UserService,
    private tournamentService: TournamentService
  ) {}

  ngOnInit(): void {}

  ngOnDestroy() {
    this.userSubscription?.unsubscribe();
  }

  ngOnChanges(simpleChanges: SimpleChanges) {
    if (
      simpleChanges.hasOwnProperty('tournamentDetails') &&
      simpleChanges.tournamentDetails.currentValue
    ) {
      this.fetchBracket();

      if (this.tournamentDetails?.bracketType === 'round_robin') {
        const encodeUrl = `?query=${encodeURIComponent(
          JSON.stringify({
            tournamentId: this.tournamentDetails._id,
            participantStatus: 'approved',
            seed: { $ne: 0 },
          })
        )}`;
        this.fetchPaticipant(encodeUrl, 'participantList');
      }
      this.getCurrentUserDetails();
    }
  }

  /**
   * Get Current User Details and Its ParticipantId
   */
  getCurrentUserDetails() {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.userId = data._id;
        const encodeUrl = `?query=${encodeURIComponent(
          JSON.stringify({
            tournamentId: this.tournamentDetails._id,
            userId: this.userId,
          })
        )}&select=_id`;
        this.fetchPaticipant(encodeUrl, 'participantId');
      }
    });
  }

  /**
   * Fetch Participant List if tournament Type is round_robin
   *       Or
   * Get ParticipantId If Participant Approved And Registered
   * @param encodeUrl encoded query
   * @param field class member fields
   */
  fetchPaticipant = async (encodeUrl, field) => {
    try {
      this.apiLoaded.push(false);
      const participant = await this.tournamentService
        .getParticipants(encodeUrl)
        .toPromise();
      if (participant.data.length) {
        this[field] =
          participant?.data.length === 1
            ? participant?.data[0]._id
            : participant.data;
      }
      this.apiLoaded.push(true);
    } catch (error) {
      this.apiLoaded.push(true);
      this.toastService.showError(error?.error?.message || error?.message);
    } finally {
      this.checkAllApiDataLoaded();
    }
  };

  /**
   * Fetch All Match If Seed Else Fetch Mock Structure Of The Tournament
   */
  fetchBracket = async () => {
    try {
      this.apiLoaded.push(false);
      if (this.tournamentDetails?.isSeeded) {
        const queryParam = `?query=${encodeURIComponent(
          JSON.stringify({ tournamentId: this.tournamentDetails._id })
        )}`;

        if (
          ['single', 'double'].includes(this.tournamentDetails?.bracketType)
        ) {
          const response = await this.bracketService.fetchAllMatches(
            queryParam
          );
          this.structure = this.bracketService.assembleStructure(response.data);
        }
      } else {
        const payload = {
          bracketType: this.tournamentDetails?.bracketType,
          maximumParticipants: this.tournamentDetails?.maxParticipants,
          noOfSet: this.tournamentDetails?.noOfSet,
          ...(['round_robin', 'battle_royale'].includes(
            this.tournamentDetails?.bracketType
          ) && {
            noOfTeamInGroup: this.tournamentDetails?.noOfTeamInGroup,
            noOfWinningTeamInGroup: this.tournamentDetails
              ?.noOfWinningTeamInGroup,
            noOfRoundPerGroup:
              this.tournamentDetails?.noOfRoundPerGroup ||
              this.tournamentDetails?.nfMatchBetweenTwoTeam ||
              2,
            stageBracketType: this.tournamentDetails?.stageBracketType,
          }),
        };
        const response = await this.bracketService.generateBracket(payload);
        this.structure = response.data;
      }
      this.apiLoaded.push(true);
    } catch (error) {
      this.apiLoaded.push(true);
      this.toastService.showError(error?.error?.message || error?.message);
    } finally {
      this.checkAllApiDataLoaded();
    }
  };

  /**
   * Check All API's Are Finished.
   * If All APi's Finished Then Remove Full Page Loader
   */
  checkAllApiDataLoaded() {
    const startedApiCount = this.apiLoaded.filter((el) => !el).length;
    const finishedApiCount = this.apiLoaded.filter((el) => el).length;
    this.isLoaded = startedApiCount === finishedApiCount;
  }
}
