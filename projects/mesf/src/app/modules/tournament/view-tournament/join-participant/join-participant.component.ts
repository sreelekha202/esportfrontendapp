import { Component, OnDestroy, OnInit } from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormArray,
  FormControl,
  ValidatorFn,
} from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

import {
  TournamentService,
  UserService,
  ToastService,
  ParticipantService,
  ConstantsService,
} from '../../../../core/service';
import { TranslateService } from '@ngx-translate/core';
import { IUser } from '../../../../shared/models';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-join-participant',
  templateUrl: './join-participant.component.html',
  styleUrls: [
    './join-participant.component.scss',
    '../view-tournament.component.scss',
  ],
})
export class JoinParticipantComponent implements OnInit, OnDestroy {
  participantForm: FormGroup;
  teamparticipantForm: FormGroup;
  user: IUser;
  tournamentDetails: any = {};
  categoryList = [1, 2, 3];
  isProcessing = false;
  isLoaded = false;
  showParticipantForm = false;
  male: any;
  male1: any;
  participantRS: string | null;
  timeoutId = null;
  tournamentFilter: any;
  isDisabled = true;
  // nric config
  nricDimension = { width: 200, height: 150 };
  nricSize = 1000 * 1000 * 5;

  // logo config
  logoDimension = { width: 180, height: 180 };
  logoSize = 1000 * 1000 * 5;
  participantState;
  userSubscription: Subscription;
  managerStatus;
  gender;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private tournamentService: TournamentService,
    private toastService: ToastService,
    private userService: UserService,
    private translateService: TranslateService,
    private participantService: ParticipantService
  ) {}

  ngOnInit(): void {
    this.managerStatus = ConstantsService?.managerStatus;
    this.participantState = ConstantsService?.participantState;
    this.gender = ConstantsService?.gender;
    this.getCurrentUserDetails();
  }

  ngOnDestroy() {
    this.userSubscription?.unsubscribe();
  }

  /**
   * Fetch Some tournament Details
   * @param id tournamentID
   */
  fetchTournamentDetails = async (slug) => {
    try {
      this.isLoaded = false;
      const query = JSON.stringify({ slug });
      const select =
        '&select=_id,name,logo,banner,participantType,isCaptainRequired,teamSize,substituteMemberSize,allowSubstituteMember,slug,isPrize,isIdProofRequired';
      const tournament = await this.tournamentService
        .getTournaments({ query }, select)
        .toPromise();
      this.tournamentDetails = tournament?.data?.length
        ? tournament.data[0]
        : null;

      // if (this.tournamentDetails?.isPrize) {
      //   if (this.user && !this.user?.accountDetail?.isPaymentAccountVerified) {
      //     this.toastService.showInfo(
      //       this.translateService.instant(
      //         'VIEW_TOURNAMENT.JOIN_PARTICIPANT.ACCOUNT'
      //       )
      //     );
      //     this.navigate();
      //     return;
      //   }
      // }

      if (!this.tournamentDetails) {
        throw new Error(
          this.translateService.instant(
            'VIEW_TOURNAMENT.JOIN_PARTICIPANT.INVALID_TOURNAMENT'
          )
        );
      }
      const participantId = this.activatedRoute?.snapshot?.params?.pId;
      const {
        data,
      } = await this.participantService.fetchParticipantRegistrationStatus(
        this.tournamentDetails?._id,
        participantId
      );
      this.participantRS = data?.type;
      this.showParticipantForm = [
        'join',
        'already-registered',
        'checked-in',
        'already-checked-in',
      ].includes(this.participantRS);
      this.createForm();
      this.teamcreateForm();

      if (
        this.tournamentDetails?.participantType &&
        this.tournamentDetails?.participantType.toLowerCase() === 'team'
      ) {
        if (this.tournamentDetails?.isCaptainRequired === true) {
          for (let i = 0; i < this.tournamentDetails?.teamSize || 0; i++) {
            this.addtemMember('teamMembers');
          }
        } else {
          for (let i = 0; i < this.tournamentDetails?.teamSize - 1 || 0; i++) {
            this.addMember('teamMembers');
          }
        }

        if (this.tournamentDetails?.allowSubstituteMember) {
          for (
            let i = 0;
            i < this.tournamentDetails?.substituteMemberSize || 0;
            i++
          ) {
            this.addMember('substituteMembers');
            this.addtemMember('substituteMembers');
          }
        }
      }
      this.isLoaded = true;
      if (this.showParticipantForm) {
        this.fetchParticipant(data?.participantId);
      }
    } catch (error) {
      this.isLoaded = true;
      this.toastService.showError(error?.statusText || error?.message);
    }
  };

  /**
   * Create Form
   */
  createForm() {
    this.participantForm = this.fb.group({
      logo: ['', Validators.required],
      teamName: [
        '',
        Validators.compose([
          Validators.required,
          this.emptyCheckAndRequestProccess,
        ]),
      ],
      name: ['', Validators.compose([Validators.required, this.isEmptyCheck])],
      phoneNumber: [''],
      email: [''],
      tournamentUsername: [
        '',
        Validators.compose([
          Validators.required,
          this.emptyCheckAndRequestProccess,
        ]),
      ],
      inGamerUserId: [
        '',
        Validators.compose([
          Validators.required,
          this.emptyCheckAndRequestProccess,
        ]),
      ],
      ...(this.tournamentDetails?.isIdProofRequired && {
        idNumber: [
          null,
          Validators.compose([
            Validators.required,
            this.customNRICPatternValid({
              pattern: /^[0-9]{12}$/,
              type: 'NRIC',
            }),
          ]),
        ],
        age: [
          '',
          Validators.compose([Validators.required, Validators.min(12)]),
        ],
        address: [
          '',
          Validators.compose([Validators.required, this.isEmptyCheck]),
        ],
        idProofDoc: ['', Validators.compose([Validators.required])],
        ...(this.tournamentDetails?.participantType.toUpperCase() ===
          'TEAM' && {
          discordChannelName: [
            '',
            Validators.compose([Validators.required, this.isEmptyCheck]),
          ],
        }),
      }),
      teamMembers: this.fb.array([]),
      substituteMembers: this.fb.array([]),
    });
    this.patchFormValues();
  }

  teamcreateForm() {
    this.teamparticipantForm = this.fb.group({
      logo: ['', Validators.required],
      teamName: [
        '',
        Validators.compose([
          Validators.required,
          this.emptyCheckAndRequestProccess,
        ]),
      ],
      name: ['', Validators.compose([Validators.required, this.isEmptyCheck])],
      // phoneNumber: [''],
      participantState: ['', Validators.compose([Validators.required])],
      schoolName: [
        '',
        Validators.compose([Validators.required, Validators.minLength(15)]),
      ],
      email: ['', Validators.compose([Validators.required, Validators.email])],
      managerStatus: ['', Validators.compose([Validators.required])],
      // tournamentUsername8:[''],
      phoneNumber: [''],

      teamMembers: this.fb.array([]),
      substituteMembers: this.fb.array([]),
    });
    this.patchteamFormValues();
  }

  /**
   * Empty validation
   * @param control formControl
   */
  isEmptyCheck = (control: FormControl) => {
    return control.value && control.value.trim() ? null : { required: true };
  };

  emptyCheckAndRequestProccess = (control: FormControl) => {
    return control.value && control.value.trim() ? null : { required: true };
  };

  /** Add Member For Team */
  addMember(field): void {
    const teamMembers = this.participantForm.get(field) as FormArray;
    const createMemberForm = (): FormGroup => {
      return this.fb.group({
        name: [
          '',
          Validators.compose([Validators.required, this.isEmptyCheck]),
        ],
        phoneNumber: [
          '',
          Validators.compose([
            Validators.required,
            // this.isValidPhoneNumber
          ]),
        ],
        email: [
          '',
          Validators.compose([Validators.required, Validators.email]),
        ],
        tournamentUsername: [
          '',
          Validators.compose([
            Validators.required,
            this.emptyCheckAndRequestProccess,
          ]),
        ],
        inGamerUserId: [
          '',
          Validators.compose([
            Validators.required,
            this.emptyCheckAndRequestProccess,
          ]),
        ],
        ...(this.tournamentDetails?.isIdProofRequired && {
          idNumber: [
            '',
            Validators.compose([
              Validators.required,
              this.customNRICPatternValid({
                pattern: /^[0-9]{12}$/,
                type: 'NRIC',
              }),
            ]),
          ],
          age: [
            '',
            Validators.compose([Validators.required, Validators.min(12)]),
          ],
          address: [
            '',
            Validators.compose([Validators.required, this.isEmptyCheck]),
          ],
          idProofDoc: ['', Validators.compose([Validators.required])],
        }),
      });
    };
    teamMembers.push(createMemberForm());
  }
  addtemMember(field): void {
    const teamMembers = this.teamparticipantForm.get(field) as FormArray;
    const createMemberForm = (): FormGroup => {
      return this.fb.group({
        name: [
          '',
          Validators.compose([Validators.required, this.isEmptyCheck]),
        ],
        inGamerUserId: [
          '',
          Validators.compose([
            Validators.required,
            // this.isValidPhoneNumber
          ]),
        ],
        account: [
          '',
          Validators.compose([
            Validators.required,
            // this.isValidPhoneNumber
          ]),
        ],
        idNumber: [
          '',
          Validators.compose([
            Validators.required,
            this.customNRICPatternValid({
              pattern: /^[0-9]{12}$/,
              type: 'NRIC',
            }),
          ]),
        ],
        gender: [
          '',
          Validators.compose([
            Validators.required,
            this.emptyCheckAndRequestProccess,
          ]),
        ],
        motherName: ['', Validators.compose([])],
        motherContactNumber: [''],
        motherEmail: ['', Validators.compose([Validators.email])],
        fatherName: ['', Validators.compose([])],
        fatherContactNumber: [''],
        fatherEmail: ['', Validators.compose([Validators.email])],
        parental: [false, Validators.compose([Validators.required])],
      });
    };
    teamMembers.push(createMemberForm());
  }

  public customNRICPatternValid(config: any): ValidatorFn {
    return (control: FormControl) => {
      let urlRegEx: RegExp = config.pattern;
      if (control.value && !control.value.match(urlRegEx)) {
        return {
          NRIC: config.type,
        };
      } else {
        return null;
      }
    };
  }

  /**
   * Get Unique Team Name, tournamentUsername and GameUserId Before Registeration
   * @param name text name
   * @param field field name
   * @param index member index
   */

  isUniqueName = async (name, field, index = null, arrayField = null) => {
    try {
      const checkUniqueNameInForm = async () => {
        const value = this.participantForm.value;
        const matchArray = [];
        matchArray.push(value[field].trim() === name.trim());
        value.teamMembers.forEach((el) => {
          matchArray.push(el[field] && el[field].trim() === name.trim());
        });
        value.substituteMembers.forEach((el) => {
          matchArray.push(el[field] && el[field].trim() === name.trim());
        });
        return matchArray.filter((el) => el).length >= 2;
      };

      const isAvailable = async () => {
        try {
          const response = await this.tournamentService
            .searchParticipant(
              field,
              name,
              this.tournamentDetails?._id,
              this.participantForm.value?.id
            )
            .toPromise();
          const isNameAvailable =
            response.data.isExist || (await checkUniqueNameInForm());
          if (typeof index === 'number') {
            const array = this.participantForm.get(arrayField) as FormArray;
            const value = array.at(index).get(field).value.trim();
            array
              .at(index)
              .get(field)
              .setErrors(
                !value
                  ? { required: true }
                  : isNameAvailable
                  ? { isNameAvailable }
                  : response.data.invalidId
                  ? { invalidId: true }
                  : null
              );
          } else {
            const value = this.participantForm.get(field).value.trim();
            this.participantForm
              .get(field)
              .setErrors(
                !value
                  ? { required: true }
                  : isNameAvailable
                  ? { isNameAvailable }
                  : response.data.invalidId
                  ? { invalidId: true }
                  : null
              );
            if (
              this.tournamentDetails?.participantType === 'individual' &&
              field === 'tournamentUsername'
            ) {
              this.participantForm.get('teamName').setValue(value);
              this.participantForm.get('teamName').clearValidators();
              this.participantForm.get('teamName').updateValueAndValidity();
            }
          }
          this.participantForm.updateValueAndValidity();
        } catch (error) {
          throw error;
        } finally {
          clearTimeout(this.timeoutId);
        }
      };

      if (typeof index === 'number') {
        const array = this.participantForm.get(arrayField) as FormArray;
        array.at(index).get(field).setErrors({ wait: true });
      } else {
        this.participantForm.get(field).setErrors({ wait: true });
      }

      if (!this.timeoutId) {
        this.timeoutId = setTimeout(isAvailable, 500);
      } else {
        clearTimeout(this.timeoutId);
        this.timeoutId = setTimeout(isAvailable, 500);
      }
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  buildQuery = async (field, text) => {
    switch (field) {
      case 'teamName':
        return {
          teamName: text,
          tournamentId: this.tournamentDetails?._id,
        };
      default:
        return {
          $or: [
            { [field]: text },
            { teamMembers: { $elemMatch: { [field]: text } } },
          ],
          tournamentId: this.tournamentDetails?._id,
        };
    }
  };

  /**
   * Save Participant Details
   */
  submit = async () => {
    try {
      if (this.participantForm.invalid) {
        this.participantForm.markAllAsTouched();
        return;
      }
      if (!this.user) {
        this.toastService.showError('Please login');
        return;
      }
      this.isProcessing = true;
      const { value } = this.participantForm;
      value.participantType = this.tournamentDetails.participantType;
      value.tournamentId = this.tournamentDetails?._id;
      value.phoneNumber = value.phoneNumber.e164Number;
      value.teamMembers = value.teamMembers.map((tM) => {
        tM.phoneNumber = tM.phoneNumber.e164Number;
        return tM;
      });
      value.substituteMembers = value.substituteMembers.map((tM) => {
        tM.phoneNumber = tM.phoneNumber.e164Number;
        return tM;
      });
      const response = value?.id
        ? await this.tournamentService
            .updateParticipant(value?.id, value)
            .toPromise()
        : await this.tournamentService.saveParticipant(value).toPromise();
      this.toastService.showSuccess(response?.message);
      this.isProcessing = false;
      this.navigate();
    } catch (error) {
      this.isProcessing = false;
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };
  submitteam = async () => {
    try {
      if (this.teamparticipantForm.invalid) {
        this.teamparticipantForm.markAllAsTouched();
        return;
      }
      if (!this.user) {
        this.toastService.showError('Please login');
        return;
      }
      this.isProcessing = true;
      const { value } = this.teamparticipantForm;
      value.participantType = this.tournamentDetails.participantType;
      value.tournamentId = this.tournamentDetails?._id;
      value.inGamerUserId = value.email;
      value.phoneNumber = value.phoneNumber.e164Number;
      // value.teamMembers = value.teamMembers.map((tM) => {
      //   tM.motherContactNumber = tM.motherContactNumber.e164Number;
      //   tM.fatherContactNumber = tM.fatherContactNumber.e164Number;
      //   return tM;
      // });
      // value.substituteMembers = value.substituteMembers.map((tM) => {
      //   tM.motherContactNumber = tM.motherContactNumber.e164Number;
      //   tM.fatherContactNumber = tM.fatherContactNumber.e164Number;
      //   return tM;
      // });
      if (value.teamMembers.length > 0) {
        for (let item of value.teamMembers) {
          if (item.motherContactNumber && item.motherContactNumber != null) {
            item.motherContactNumber = item.motherContactNumber.e164Number;
          }
          if (item.fatherContactNumber && item.fatherContactNumber != null) {
            item.fatherContactNumber = item.fatherContactNumber.e164Number;
          }
          for (let i of Object.keys(item)) {
            item[i] = this.getEmptyIfNull(item[i]);
          }
        }
      }

      if (value.substituteMembers.length > 0) {
        for (let substitute of value.substituteMembers) {
          if (
            substitute.motherContactNumber &&
            substitute.motherContactNumber != null
          ) {
            substitute.motherContactNumber =
              substitute.motherContactNumber.e164Number;
          }
          if (
            substitute.fatherContactNumber &&
            substitute.fatherContactNumber != null
          ) {
            substitute.fatherContactNumber =
              substitute.fatherContactNumber.e164Number;
          }
          for (let i of Object.keys(substitute)) {
            substitute[i] = this.getEmptyIfNull(substitute[i]);
          }
        }
      }

      const response = value?.id
        ? await this.tournamentService
            .updateParticipant(value?.id, value)
            .toPromise()
        : await this.tournamentService.saveParticipant(value).toPromise();
      this.toastService.showSuccess(response?.message);
      this.isProcessing = false;
      this.navigate();
    } catch (error) {
      this.isProcessing = false;
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  /**
   * Get Current User
   */
  getCurrentUserDetails() {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.user = data;
        const pId = this.activatedRoute.snapshot.params.pId;
        if (pId) {
          //this.tournamentDetails.slug = this.router.url.split('/').reverse()[2];
          this.tournamentDetails.slug = this.activatedRoute.snapshot.params.id;
          this.fetchTournamentDetails(this.tournamentDetails?.slug);
        } else {
          //this.tournamentDetails.slug = this.router.url.split('/').reverse()[1];
          this.tournamentDetails.slug = this.activatedRoute.snapshot.params.id;
          this.fetchTournamentDetails(this.tournamentDetails?.slug);
        }
      }
    });
  }

  navigate() {
    const pId = this.activatedRoute?.snapshot?.params?.pId;
    const joinEdit = this.activatedRoute?.snapshot?.queryParams?.success;

    if (pId) {
      this.router.navigate([
        '/tournament/manage/' + this.tournamentDetails?.slug,
      ]);
    } else if (joinEdit) {
      this.router.navigate(['profile/my-tournament/joined']);
    } else {
      this.router.navigate(['tournament/' + this.tournamentDetails?.slug]);
    }
  }

  patchFormValues = () => {
    if (this.participantForm && this.user) {
      const formValue = {
        name: this.user?.fullName,
        phoneNumber: this.user?.phoneisVerified ? this.user?.phoneNumber : '',
        email: this.user?.emailisVerified ? this.user?.email : '',
        logo: this.user?.profilePicture || '',
      };

      this.participantForm.patchValue({
        ...formValue,
      });

      if (!this.user.emailisVerified) {
        this.participantForm
          .get('email')
          .setValidators([Validators.required, Validators.email]);
        this.participantForm.get('email').updateValueAndValidity();
      }
    }
  };

  patchteamFormValues = () => {
    if (this.teamparticipantForm && this.user) {
      const formValue = {
        name: this.user?.fullName,
        phoneNumber: this.user?.phoneisVerified ? this.user?.phoneNumber : '',
        email: this.user?.emailisVerified ? this.user?.email : '',
        logo: this.user?.profilePicture || '',
      };

      this.teamparticipantForm.patchValue({
        ...formValue,
      });

      if (!this.user.emailisVerified) {
        this.teamparticipantForm
          .get('email')
          .setValidators([Validators.required, Validators.email]);
        this.participantForm.get('email').updateValueAndValidity();
      }
    }
  };

  fetchParticipant = async (pid) => {
    try {
      if (pid) {
        const query = {
          _id: pid,
        };
        const encodeUrl = `?query=${encodeURIComponent(JSON.stringify(query))}`;
        const participant = await this.tournamentService
          .getParticipants(encodeUrl)
          .toPromise();
        if (participant.data.length) {
          this.participantForm.addControl(
            'id',
            new FormControl(participant.data[0]._id)
          );
          this.participantForm.patchValue({
            ...participant.data[0],
          });
        }
      } else {
        this.patchFormValues();
      }
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  ageHandler(event, f) {
    if (event && event >= 12 && event <= 18) {
      f.addControl(
        'parentalPremission',
        new FormControl(false, Validators.requiredTrue)
      );
    } else {
      f.removeControl('parentalPremission');
    }
  }
  getEmptyIfNull(str: any): string {
    const val =
      (str == null ? 'N/A' : str) ||
      (str == undefined ? 'N/A' : str) ||
      (str == '' ? 'N/A' : str);
    return val;
  }
}
