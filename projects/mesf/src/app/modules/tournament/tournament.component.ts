import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  AfterViewInit,
} from '@angular/core';
import { faSearch } from '@fortawesome/free-solid-svg-icons';

import {
  ArticleApiService,
  GameService,
  LanguageService,
  TournamentService,
} from '../../core/service';
import { ArticleItemComponentData } from '../../core/article-item/article-item.component';
import { AppHtmlRoutes } from '../../app-routing.model';
import { IPagination } from '../../shared/models';
import { TranslateService } from '@ngx-translate/core';
import { SwiperConfigInterface } from 'ngx-swiper-wrapper';

@Component({
  selector: 'app-tournament',
  templateUrl: './tournament.component.html',
  styleUrls: ['./tournament.component.scss'],
})
export class TournamentComponent implements OnInit, AfterViewInit {
  @ViewChild('mainContainer', { read: ElementRef }) public scroll: ElementRef<
    any
  >;
  AppHtmlRoutes = AppHtmlRoutes;
  faSearch = faSearch;
  tournamentData;

  public config: SwiperConfigInterface = {
    width: 150,
    spaceBetween: 30,
    navigation: true,
    pagination: false,
    autoHeight: true,
  };
  gameList = [];
  tournamentList = { ongoing: [], upcoming: [], completed: [] };
  allTournamentList: any = [];
  allTournamentListClone: any = [];
  slectedGame: any = { all: true };
  trendingPosts: ArticleItemComponentData[] = [];
  paginationData = {
    page: 1,
    limit: 6,
    sort: 'Latest',
  };
  currentTab = 'upcoming';
  gameId;
  showLoader = true;
  page: IPagination;

  constructor(
    private tournamentService: TournamentService,
    private gameService: GameService,
    private articleService: ArticleApiService,
    private translateService: TranslateService,
    private languageService: LanguageService
  ) {}

  ngOnInit(): void {
    this.languageService.language.subscribe((lang) =>
      this.translateService.use(lang || 'en')
    );
    this.getUpcomingTournamentsPaginated();
    this.getNews();
    this.getGames();
  }

  ngAfterViewInit() {
    // this.scrollToTop();
  }

  getAllTournaments() {
    const option = JSON.stringify({ sort: { startDate: 1 } });
    const query = JSON.stringify({ tournamentStatus: 'publish' });
    this.tournamentService.getTournaments({ query }, '', option).subscribe(
      (data) => {
        this.allTournamentList = data.data.slice();
        this.allTournamentListClone = data.data.slice();
        this.getGames();
        this.filterTournaments();
      },
      (err) => {}
    );
  }

  filterTournaments() {
    this.tournamentList.completed = this.allTournamentList.filter(
      (obj) => obj.isFinished === true
    );
    this.tournamentList.upcoming = this.allTournamentList.filter((obj) => {
      const startDate = new Date(obj.startDate);
      const timeObj = this.convert12HrsTo24HrsFormat(obj.startTime);
      startDate.setHours(timeObj.hour);
      startDate.setMinutes(timeObj.minute);
      if (
        startDate.getTime() > Date.now() &&
        obj.isSeeded === false &&
        obj.isFinished === false
      ) {
        return obj;
      }
    });

    this.tournamentList.ongoing = this.allTournamentList.filter((obj) => {
      const startDate = new Date(obj.startDate);
      const timeObj = this.convert12HrsTo24HrsFormat(obj.startTime);
      startDate.setHours(timeObj.hour);
      startDate.setMinutes(timeObj.minute);

      if (
        (startDate.getTime() < Date.now() || obj.isSeeded === true) &&
        obj.isFinished === false
      ) {
        return obj;
      }
    });
    this.tournamentData = this.tournamentList.upcoming;
  }

  getGames() {
    const query = JSON.stringify({ isTournamentAllowed: true });
    this.gameService.getAllGames(query).subscribe(
      (res) => {
        this.gameList = res.data;
      },
      (err) => {}
    );
  }

  filterByGame(gameId) {
    this.paginationData.page = 1;
    this.slectedGame = {};
    this.slectedGame[gameId] = true;
    if (gameId === 'all') {
      this.gameId = null;
    } else {
      this.gameId = gameId;
    }

    switch (this.currentTab) {
      case 'ongoing':
        this.getOnGoingTournamentsPaginated();
        break;
      case 'completed':
        this.getCompletedTournamentsPaginated();
        break;
      case 'upcoming':
        this.getUpcomingTournamentsPaginated();
        break;
    }
  }

  convert12HrsTo24HrsFormat(time) {
    let hours = Number(time.match(/^(\d+)/)[1]);
    const minutes = Number(time.match(/:(\d+)/)[1]);
    let AP = time.match(/\s(.*)$/);
    if (!AP) {
      AP = time.slice(-2);
    } else {
      AP = AP[1];
    }
    if (AP === 'PM' && hours < 12) {
      hours = hours + 12;
    }
    if (AP === 'AM' && hours === 12) {
      hours = hours - 12;
    }
    let Hours24 = hours.toString();
    let Minutes24 = minutes.toString();
    if (hours < 10) {
      Hours24 = '0' + Hours24;
    }
    if (minutes < 10) {
      Minutes24 = '0' + Minutes24;
    }

    return { hour: parseInt(Hours24, 10), minute: parseInt(Minutes24, 10) };
  }

  getNews() {
    const query = JSON.stringify({
      articleStatus: 'publish',
      category: 'News',
    });
    const option = JSON.stringify({ limit: 4, sort: { createdDate: -1 } });
    this.articleService.getArticles_PublicAPI({ query, option }).subscribe(
      (res: any) => {
        this.trendingPosts = res.data;
      },
      (err) => {}
    );
  }

  getCompletedTournamentsPaginated() {
    this.showLoader = true;
    const pagination = JSON.stringify(this.paginationData);
    let query: any = { tournamentStatus: 'publish', isFinished: true };
    if (this.gameId) {
      query.gameDetail = this.gameId;
    }
    query = JSON.stringify(query);
    let params = {
      status: '2',
      page: this.paginationData.page,
      limit: this.paginationData.limit,
      sort: 'Oldest',
    };
    if (this.gameId) {
      params['game'] = this.gameId;
    }
    this.tournamentService.getPaginatedTournaments(params).subscribe(
      (res: any) => {
        this.showLoader = false;
        this.tournamentData = res.data;
        this.page = {
          totalItems: res?.data?.totalDocs,
          itemsPerPage: res?.data?.limit,
          maxSize: 5,
        };
      },
      (err) => {
        this.showLoader = false;
      }
    );
  }

  getOnGoingTournamentsPaginated() {
    this.showLoader = true;
    const pagination = JSON.stringify(this.paginationData);
    let query: any = {
      $and: [{ tournamentStatus: 'publish' }],
      $or: [{ startDate: { $lt: new Date() } }, { isSeeded: true }],
    };
    if (this.gameId) {
      query.gameDetail = this.gameId;
    }
    query = JSON.stringify(query);
    let params = {
      status: '1',
      page: this.paginationData.page,
      limit: this.paginationData.limit,
      sort: 'Latest',
    };
    if (this.gameId) {
      params['game'] = this.gameId;
    }
    this.tournamentService.getPaginatedTournaments(params).subscribe(
      (res: any) => {
        this.showLoader = false;
        this.tournamentData = res.data;
        this.page = {
          totalItems: res?.data?.totalDocs,
          itemsPerPage: res?.data?.limit,
          maxSize: 5,
        };
      },
      (err) => {
        this.showLoader = false;
      }
    );
  }

  getUpcomingTournamentsPaginated() {
    this.showLoader = true;
    const pagination = JSON.stringify(this.paginationData);
    let query: any = {
      $and: [
        { tournamentStatus: 'publish' },
        { startDate: { $gt: new Date() } },
        { isSeeded: false },
        { isFinished: false },
      ],
    };

    if (this.gameId) {
      query.gameDetail = this.gameId;
    }
    query = JSON.stringify(query);
    let params = {
      status: '0',
      page: this.paginationData.page,
      limit: this.paginationData.limit,
      sort: 'Latest',
    };
    if (this.gameId) {
      params['game'] = this.gameId;
    }
    this.tournamentService.getPaginatedTournaments(params).subscribe(
      (res: any) => {
        this.showLoader = false;
        this.tournamentData = res.data;
        this.page = {
          totalItems: res?.data?.totalDocs,
          itemsPerPage: res?.data?.limit,
          maxSize: 5,
        };
      },
      (err) => {
        this.showLoader = false;
      }
    );
  }

  pageChanged(page): void {
    this.paginationData.page = page;
    switch (this.currentTab) {
      case 'ongoing':
        this.getOnGoingTournamentsPaginated();
        break;
      case 'completed':
        this.getCompletedTournamentsPaginated();
        break;
      case 'upcoming':
        this.getUpcomingTournamentsPaginated();
        break;
    }
  }
  changeTab(event) {
    this.paginationData.page = 1;
    switch (event.target.value) {
      case 'ongoing':
        this.currentTab = 'ongoing';
        this.getOnGoingTournamentsPaginated();
        break;
      case 'completed':
        this.currentTab = 'completed';
        this.getCompletedTournamentsPaginated();
        break;
      case 'upcoming':
        this.currentTab = 'upcoming';
        this.getUpcomingTournamentsPaginated();
        break;
    }
  }

  scrollToTop() {
    this.scroll.nativeElement.scrollIntoView();
  }
}
