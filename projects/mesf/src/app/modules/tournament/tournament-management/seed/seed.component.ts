import {
  Component,
  OnInit,
  Input,
  SimpleChanges,
  Output,
  EventEmitter,
  OnChanges,
} from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import {
  BracketService,
  LanguageService,
  TournamentService,
  ToastService,
} from '../../../../core/service';
import { IPagination } from '../../../../shared/models';

@Component({
  selector: 'app-seed',
  templateUrl: './seed.component.html',
  styleUrls: ['./seed.component.scss'],
})
export class SeedComponent implements OnInit, OnChanges {
  @Input() tournamentId;
  @Input() searchKey;
  @Output() message = new EventEmitter<boolean>(false);

  tournament;
  isSeeded = false;
  isDisableCreateBracket: boolean = false;
  allApprovedParticipants: Array<any> = [];
  allSelectedParticipants: Array<any> = [];
  participantList: Array<{}> = [];
  structure;
  activePage = 1;
  pageSize = 5;
  seedingMethodList = [
    { name: 'SEED.BY_REGISTRATION', value: 'regOrder' },
    { name: 'SEED.RANDOM', value: 'random' },
  ];
  filterList = [
    { name: 'SEED.ALL_PLAYERS', value: 'all-players' },
    { name: 'SEED.CHECK_IN', value: 'checked-in-only' },
    // { name: 'Not Previously Seeded', value: 'not-previously-seeded' },
  ];
  selectFilterType = null;
  selectedSeedingMethod = null;
  isChangeInSeedingOrder = false;
  apiLoaded: Array<boolean> = [];
  isLoaded = false;
  page: IPagination;

  constructor(
    private tournamentService: TournamentService,
    private bracketService: BracketService,
    private toastService: ToastService,
    private modalService: NgbModal,
    private translateService: TranslateService,
    private languageService: LanguageService
  ) {}

  ngOnInit(): void {
    this.languageService.language.subscribe((lang) =>
      this.translateService.use(lang || 'en')
    );
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (
      changes.hasOwnProperty('tournamentId') &&
      changes.tournamentId.currentValue
    ) {
      this.fetchTournamentData();
    }

    if (changes.hasOwnProperty('searchKey')) {
      if (changes.searchKey.currentValue.trim()) {
        const str = changes.searchKey.currentValue.trim();
        this.allSelectedParticipants = this.allApprovedParticipants
          .filter((item: any) => {
            if (
              !this.selectFilterType ||
              this.selectFilterType?.value !== 'checked-in-only'
            ) {
              return true;
            }
            return item.checkedIn;
          })
          .filter((item: any) => item?.name.indexOf(str) !== -1);
        this.initializeTable();
      }

      if (!changes.searchKey.currentValue && changes.searchKey.previousValue) {
        this.allSelectedParticipants = this.allApprovedParticipants.filter(
          (item: any) => {
            if (
              !this.selectFilterType ||
              this.selectFilterType?.value !== 'checked-in-only'
            ) {
              return true;
            }
            return item.checkedIn;
          }
        );
        this.initializeTable();
      }
    }
  }

  /** Initialize table after filter or search */
  initializeTable() {
    this.activePage = 1;
    this.page = {
      totalItems: this.allSelectedParticipants.length,
      itemsPerPage: this.pageSize,
      maxSize: 5,
    };
    this.participantList = this.allSelectedParticipants.slice(
      this.pageSize * (this.activePage - 1),
      this.pageSize * this.activePage
    );
  }
  /** Get all approved participants of the tournament  */
  fetchAllApprovedParticipants = async () => {
    try {
      this.apiLoaded.push(false);
      const encodeUrl = `?query=${encodeURIComponent(
        JSON.stringify({
          tournamentId: this.tournamentId,
          participantStatus: 'approved',
        })
      )}&select=checkedIn,teamName,seed,participantType,teamMembers,matchWin`;
      const participants = await this.tournamentService
        .getParticipants(encodeUrl)
        .toPromise();
      this.allApprovedParticipants = participants.data.map((obj, index) => {
        const record = obj;
        record.reg = index + 1;
        return record;
      });
      this.allSelectedParticipants = this.allApprovedParticipants.filter(
        (item: any) => {
          if (
            !this.selectFilterType ||
            this.selectFilterType?.value !== 'checked-in-only'
          ) {
            return true;
          }
          return item.checkedIn;
        }
      );
      this.initializeTable();
      this.apiLoaded.push(true);
    } catch (error) {
      this.apiLoaded.push(true);
      this.toastService.showError(error.message);
    } finally {
      this.checkAllApiDataLoaded();
    }
  };

  /** Validate seeding value */
  isDisableSaveAll() {
    const teamSet = new Set();
    let isInvalid = false;
    let reason = '';
    isInvalid = this.allApprovedParticipants
      .filter((item: any) => {
        if (
          !this.selectFilterType ||
          this.selectFilterType?.value !== 'checked-in-only'
        ) {
          return true;
        }
        return item.checkedIn;
      })
      .some((obj: any, index, array) => {
        if (!obj.seed || obj.seed < 1 || obj.seed > array.length) {
          reason = this.translateService.instant(
            'MANAGE_TOURNAMENT.SEED_BRACKET.INCOMPLETE_SEEDING'
          );
          return true;
        } else if (teamSet.size === teamSet.add(obj.seed).size) {
          reason = this.translateService.instant(
            'MANAGE_TOURNAMENT.SEED_BRACKET.DUPLICATE_SEED_ID'
          );
          return true;
        }
      });
    return { isValid: !isInvalid, reason };
  }

  /** Save seeding value of participants */
  saveAllSeeds = async (isShowMessage = true) => {
    try {
      const { isValid, reason } = this.isDisableSaveAll();
      if (!isValid) {
        isShowMessage ? this.toastService.showError(reason) : null;
        return { isValid: false, reason };
      }
      const data = this.allApprovedParticipants.filter((item: any) => {
        if (
          !this.selectFilterType ||
          this.selectFilterType?.value !== 'checked-in-only'
        ) {
          return true;
        }
        return item.checkedIn;
      });
      const response = await this.tournamentService
        .updateParticipantBulk({ data })
        .toPromise();
      this.isChangeInSeedingOrder = false;
      this.toastService.showSuccess(response.message);
      return { isValid: true };
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
      return {
        isValid: false,
        reason: error?.error?.message || error?.message,
      };
    }
  };

  /** filter participant by checked-in, not-previously-seed */
  filterByType(data) {
    switch (data.value) {
      case 'all-players':
        this.allSelectedParticipants = this.allApprovedParticipants.filter(
          (item: any) => {
            if (!this.searchKey || !this.searchKey.trim()) {
              return true;
            }
            return item?.name.indexOf(this.searchKey.trim()) !== -1;
          }
        );
        break;
      case 'not-previously-seeded':
        this.allSelectedParticipants = this.allApprovedParticipants
          .filter((item: any) => item.seed)
          .filter((item: any) => {
            if (!this.searchKey || !this.searchKey.trim()) {
              return true;
            }
            return item?.name.indexOf(this.searchKey.trim()) !== -1;
          });
        break;
      case 'checked-in-only':
        this.allSelectedParticipants = this.allApprovedParticipants
          .filter((item: any) => item.checkedIn)
          .filter((item: any) => {
            if (!this.searchKey || !this.searchKey.trim()) {
              return true;
            }
            return item?.name.indexOf(this.searchKey.trim()) !== -1;
          });
        break;
      default:
        this.allSelectedParticipants = [];
        break;
    }
    this.initializeTable();
  }
  /** Client-side pagination for client by change page no or page size */
  pageSizeChanged(pageSize) {
    this.participantList = this.allSelectedParticipants.slice(
      pageSize * 0,
      pageSize * 1
    );
    this.initializeTable();
  }

  pageChanged(page) {
    this.participantList = this.allSelectedParticipants.slice(
      this.pageSize * (page - 1),
      this.pageSize * page
    );
  }

  /** Generate mock bracket structure */
  generateBracket = async () => {
    try {
      const data: any = this.isChangeInSeedingOrder
        ? await this.saveAllSeeds(false)
        : this.isDisableSaveAll();

      if (!data.isValid) {
        throw new Error(data.reason);
      }

      const payload = {
        tournamentId: this.tournamentId,
        ...(this.selectFilterType?.value === 'checked-in-only' && {
          checkedIn: true,
        }),
      };
      const response = await this.bracketService.generateBracket(payload);
      this.structure = response.data;
      this.isSeeded = true;
    } catch (error) {
      this.toastService.showError(
        error?.error?.error || error?.error?.message || error.message
      );
    }
  };

  /** Confirmation modal for bracket generation */
  openModal(content) {
    this.modalService.open(content, {
      windowClass: 'confirm-modal-content',
      centered: true,
    });
  }

  /** Get all saved matches of the tournamnet */
  fetchAllMatches = async () => {
    try {
      this.apiLoaded.push(false);
      const queryParam = `?query=${encodeURIComponent(
        JSON.stringify({ tournamentId: this.tournamentId })
      )}&select=bye,currentMatch,matchStatus,teamA,teamB,disQualifiedTeam,placeHolderA,placeHolderB,teamAWinSet,teamBWinSet,isThirdPlaceMatch,matchNo,battleRoyalTeams.noOfKill,battleRoyalTeams.placement,battleRoyalTeams.score,battleRoyalTeams._id,battleRoyalScoringOption,externalMatchId`;
      const response = await this.bracketService.fetchAllMatches(queryParam);
      if (
        ['single', 'double', 'round_robin'].includes(
          this.tournament?.bracketType
        ) &&
        !this.tournament?.stageBracketType
      ) {
        this.structure = this.bracketService.assembleStructure(response.data);
      } else {
        this.structure = response.data;
      }
      this.apiLoaded.push(true);
    } catch (error) {
      this.apiLoaded.push(true);
      this.toastService.showError(error.message);
    } finally {
      this.checkAllApiDataLoaded();
    }
  };

  /** Confirm mock structure and save it in data base */
  confirmBracket = async () => {
    try {
      const payload = {
        tournamentId: this.tournamentId,
        ...(this.selectFilterType?.value === 'checked-in-only' && {
          checkedIn: true,
        }),
      };
      const response = await this.bracketService.saveGeneratedBracket(payload);
      this.toastService.showSuccess(response.message);
      this.fetchTournamentData();
    } catch (error) {
      this.toastService.showError(error.message);
    }
  };

  /** Get response from child component */
  getResponseMessage(data) {
    if (data) {
      this.fetchTournamentData();
    }
  }

  /** Get current tournament data */
  fetchTournamentData = async () => {
    try {
      this.apiLoaded.push(false);
      const query = `${JSON.stringify({ _id: this.tournamentId })}`;
      const selectQuery = `&select=_id,isFinished,isSeeded,bracketType,isCheckInRequired,tournamentStageType,tournamentStage,stageBracketType,isScreenshotRequired,scoreReporting,participantType`;
      const tournament = await this.tournamentService
        .getTournaments({ query }, selectQuery)
        .toPromise();
      this.tournament = tournament?.data?.length ? tournament.data[0] : {};
      // if (!this.tournament?.isFinished) {
      if (this.tournament?.isSeeded) {
        this.fetchAllMatches();
        if (this.tournament?.bracketType === 'round_robin') {
          this.fetchAllApprovedParticipants();
        }
      } else {
        this.fetchAllApprovedParticipants();
      }
      // }
      this.apiLoaded.push(true);
    } catch (error) {
      this.apiLoaded.push(true);
      this.toastService.showError(error.message);
    } finally {
      this.checkAllApiDataLoaded();
    }
  };

  /** Assign seed value via Registration order, random order or manually. */
  assignSeedValue(method) {
    switch (method.value) {
      case 'regOrder':
        this.seedByRegNo();
        break;
      case 'random':
        this.seedRandomly();
        break;
      default:
        break;
    }
  }

  seedByRegNo() {
    this.isChangeInSeedingOrder = true;
    this.allApprovedParticipants
      .filter((item: any) => {
        if (
          !this.selectFilterType ||
          this.selectFilterType?.value !== 'checked-in-only'
        ) {
          return true;
        }
        return item.checkedIn;
      })
      .forEach((participant: any, index) => {
        participant.seed = index + 1;
        const main_index = this.allApprovedParticipants.findIndex(
          (item: any) => item._id === participant._id
        );
        this.allApprovedParticipants[main_index].seed = index + 1;
      });
  }

  seedRandomly() {
    const tempArray = [];
    this.isChangeInSeedingOrder = true;
    const availableParticipant = this.allApprovedParticipants.filter(
      (item: any) => {
        if (
          !this.selectFilterType ||
          this.selectFilterType?.value !== 'checked-in-only'
        ) {
          return true;
        }
        return item.checkedIn;
      }
    );
    while (tempArray.length < availableParticipant.length) {
      const seed = Math.floor(Math.random() * availableParticipant.length) + 1;
      if (tempArray.indexOf(seed) === -1) {
        tempArray.push(seed);
      }
    }
    availableParticipant.forEach((participant: any, index) => {
      const mainIndex = this.allApprovedParticipants.findIndex(
        (item: any) => item._id === participant._id
      );
      const subIndex = this.allSelectedParticipants.findIndex(
        (item: any) => item._id === participant._id
      );
      if (mainIndex >= 0) {
        this.allApprovedParticipants[mainIndex].seed = tempArray[index];
      }
      if (subIndex >= 0) {
        this.allSelectedParticipants[subIndex].seed = tempArray[index];
      }
    });
  }

  manualSeedValidation(index, _id, value) {
    const tempArrayLength = this.allApprovedParticipants.filter((item: any) => {
      if (
        !this.selectFilterType ||
        this.selectFilterType?.value !== 'checked-in-only'
      ) {
        return true;
      }
      return item.checkedIn;
    }).length;
    const mainIndex = this.allApprovedParticipants.findIndex(
      (item: any) => item._id === _id
    );
    this.allApprovedParticipants[mainIndex].seed = value && Number(value);
    if (value) {
      this.isChangeInSeedingOrder = true;
      if (value < 1) {
        this.allSelectedParticipants[index].seed = 1;
        this.allApprovedParticipants[mainIndex].seed = 1;
      } else if (value > tempArrayLength) {
        this.allSelectedParticipants[index].seed = tempArrayLength;
        this.allApprovedParticipants[mainIndex].seed = tempArrayLength;
      }
    }
  }

  /**
   * Check All API's Are Finished.
   * If All APi's Finished Then Remove Full Page Loader
   */
  checkAllApiDataLoaded() {
    const startedApiCount = this.apiLoaded.filter((el) => !el).length;
    const finishedApiCount = this.apiLoaded.filter((el) => el).length;
    this.isLoaded = startedApiCount === finishedApiCount;
  }

  showInfo(type) {
    switch (type) {
      case 'reset':
        this.toastService.showInfo(
          this.translateService.instant(
            'MANAGE_TOURNAMENT.SEED_BRACKET.RESET_INFO'
          )
        );
        break;
      case 'shuffle':
        this.toastService.showInfo(
          this.translateService.instant(
            'MANAGE_TOURNAMENT.SEED_BRACKET.SHUFFLE_INFO'
          )
        );
        break;
      default:
        break;
    }
  }
}
