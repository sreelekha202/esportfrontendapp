import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TournamentManagementComponent } from './tournament-management.component';

const routes: Routes = [{ path: '', component: TournamentManagementComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TournamentManagementRoutingModule {}
