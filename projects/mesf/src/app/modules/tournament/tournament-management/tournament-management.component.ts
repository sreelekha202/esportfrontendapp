import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { FormControl } from '@angular/forms';
import { debounceTime, switchMap } from 'rxjs/operators';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { TranslateService } from '@ngx-translate/core';
import {
  LanguageService,
  TournamentService,
  UserService,
  ToastService,
} from '../../../core/service';
import { IUser } from '../../../shared/models';

import { AppHtmlRoutes } from '../../../app-routing.model';

@Component({
  selector: 'app-tournament-management',
  templateUrl: './tournament-management.component.html',
  styleUrls: ['./tournament-management.component.scss'],
})
export class TournamentManagementComponent implements OnInit, OnDestroy {
  AppHtmlRoutes = AppHtmlRoutes;

  active = 1;
  searchControl: Subscription;
  tournament: any = {};
  currentDropdownTab = 'pendingParticipants';
  searchKeyword = new FormControl(null);
  tournamentStartTime = '';
  intervalId = null;
  registeredParticipantCount = 0;
  checkedInParticipantCount = 0;
  searchKey = '';
  isLoaded = false;
  apiLoaded: Array<boolean> = [];
  user: IUser;
  hideMessage = true;

  userSubscription: Subscription;

  constructor(
    private activatedRoute: ActivatedRoute,
    private toastService: ToastService,
    private modalService: NgbModal,
    private tournamentService: TournamentService,
    private translateService: TranslateService,
    private languageService: LanguageService,
    private userService: UserService
  ) {}

  ngOnInit(): void {
    this.languageService.language.subscribe((lang) =>
      this.translateService.use(lang || 'en')
    );
    this.tournament.slug = this.activatedRoute.snapshot.params.id; // get id from route
    if (this.tournament.slug) {
      this.getCurrentUserDetails();
    }
    this.searchControl = this.searchKeyword.valueChanges
      .pipe(
        debounceTime(1000),
        switchMap((keyword) => {
          this.searchKey = keyword.trim();
          return 'test';
        })
      )
      .subscribe();
  }

  /**
   * Fetch TOurnament Details
   */
  fetchTournamentDetails = async () => {
    try {
      this.apiLoaded.push(false);
      const query = JSON.stringify({
        slug: this.tournament.slug,
        ...(this.user.accountType === 'user' && { createdBy: this.user._id }),
      });
      const tournament = await this.tournamentService
        .getTournaments({ query })
        .toPromise();
      this.tournament = tournament?.data?.length ? tournament.data[0] : null;
      if (!this.tournament) {
        this.hideMessage = false;
        throw new Error(this.translateService.instant('VIEW_TOURNAMENT.ERROR'));
      }
      if (this.tournament?.isFinished) {
        this.tournamentStartTime = this.translateService.instant(
          'MANAGE_TOURNAMENT.TOURNAMENT_FINISHED'
        );
      } else if (this.tournament?.isSeeded) {
        this.tournamentStartTime = this.translateService.instant(
          'MANAGE_TOURNAMENT.TOURNAMENT_STARTED'
        );
      } else {
        this.setTimer();
      }
      this.fetchRegisteredParticipant('registeredParticipantCount');
      this.fetchRegisteredParticipant('checkedInParticipantCount', {
        checkedIn: true,
      });
      this.apiLoaded.push(true);
    } catch (error) {
      this.apiLoaded.push(true);
      this.toastService.showError(error?.error?.message || error?.message);
    } finally {
      this.checkAllApiDataLoaded();
    }
  };

  /**
   * Fatch Register Or CheckedIn Participant
   * @param field registeredParticipantCount, checkedInParticipantCount
   * @param obj for checkedIn users
   */
  fetchRegisteredParticipant = async (field, obj = {}) => {
    try {
      this.apiLoaded.push(false);
      const encodeUrl = `?query=${encodeURIComponent(
        JSON.stringify({ tournamentId: this.tournament?._id, ...obj })
      )}&select=''&option=${encodeURIComponent(
        JSON.stringify({ count: true })
      )}`;
      const response = await this.tournamentService
        .getParticipants(encodeUrl)
        .toPromise();
      this[field] = response?.totals?.count;
      this.apiLoaded.push(true);
    } catch (error) {
      this.apiLoaded.push(true);
      this.toastService.showError(error?.error?.message || error?.message);
    } finally {
      this.checkAllApiDataLoaded();
    }
  };

  /**
   * Open Popup
   * @param content model template
   */
  openModal(content) {
    this.modalService.open(content, {
      windowClass: 'preference-modal-content',
      centered: true,
    });
  }

  /**
   * Set Timer Before Tournament Start Date
   */
  setTimer() {
    const startDate = new Date(this.tournament.startDate);
    const currentDate = new Date();
    if (startDate.getTime() - currentDate.getTime() > 0) {
      const millisecond = startDate.getTime() - currentDate.getTime();
      const dd = (millisecond / (1000 * 60 * 60 * 24)) | 0;
      if (dd) {
        this.tournamentStartTime = `${dd} ${
          dd === 1
            ? this.translateService.instant('MANAGE_TOURNAMENT.DAYS_LEFT.01')
            : this.translateService.instant('MANAGE_TOURNAMENT.DAYS_LEFT.02')
        }`;
        return;
      }
      const hh = (millisecond / (1000 * 60 * 60)) | 0;
      const mm = ((millisecond - hh * 1000 * 60 * 60) / (1000 * 60)) | 0;
      const ss = ((millisecond - hh * 1000 * 60 * 60) / 1000) % 60 | 0;
      this.tournamentStartTime = `${hh > 9 ? hh : '0' + hh}: ${
        mm > 9 ? mm : '0' + mm
      }: ${ss > 9 ? ss : '0' + ss}`;
      if (!this.intervalId) {
        this.intervalId = setInterval(() => this.setTimer(), 1000);
      }
    } else {
      this.tournamentStartTime = this.translateService.instant(
        'MANAGE_TOURNAMENT.TOURNAMENT_EXPIRED'
      );
      clearInterval(this.intervalId);
    }
  }

  /**
   * Get Child Component Response
   * @param data child component response
   */
  getComponentResponse(data) {
    if (typeof data === 'object') {
      this.active = data.active;
      data = data.refresh;
    }

    if (data) {
      this.fetchTournamentDetails();
      clearInterval(this.intervalId);
      this.intervalId = null;
    }
  }

  ngOnDestroy() {
    clearInterval(this.intervalId);
    this.userSubscription?.unsubscribe();
  }

  /**
   * Check All API's Are Finished.
   * If All APi's Finished Then Remove Full Page Loader
   */
  checkAllApiDataLoaded() {
    const startedApiCount = this.apiLoaded.filter((el) => !el).length;
    const finishedApiCount = this.apiLoaded.filter((el) => el).length;
    this.isLoaded = startedApiCount === finishedApiCount;
  }

  /**
   * Get Current User
   */
  getCurrentUserDetails() {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.user = data;
        this.fetchTournamentDetails();
      }
    });
  }
}
