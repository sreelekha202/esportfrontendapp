import {
  Component,
  OnInit,
  Input,
  OnChanges,
  SimpleChanges,
  Inject,
} from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { Router } from '@angular/router';
import {
  LanguageService,
  TournamentService,
  ToastService,
} from '../../../../core/service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { ViewChild } from '@angular/core';
// import { NgxCsvParser } from 'ngx-csv-parser';
// import { NgxCSVParserError } from 'ngx-csv-parser';
import { TeamGenerasi } from '../../../../../app/shared/models/team-generasi';
import { TeamPlayerGenerasi } from '../../../../../app/shared/models/team-player-generasi';
import {
  InfoPopupComponent,
  InfoPopupComponentData,
  InfoPopupComponentType,
} from '../../../../shared/popups/info-popup/info-popup.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-participants',
  templateUrl: './participants.component.html',
  styleUrls: [
    './participants.component.scss',
    '../tournament-management.component.scss',
  ],
})
export class ParticipantsComponent implements OnInit, OnChanges {
  @Input() tournamentId: string;
  @Input() tournamentDetails;
  @Input() currentTab: string;

  participantsListPending: Array<{}> = [];
  participantsListApproved: Array<{}> = [];
  participantsListRejected: Array<{}> = [];
  allParticipantsList: Array<any> = [];

  selectedList: Array<{}> = [];
  selectedTeam = null;
  selectedParticipantStatus = null;
  isLoaded = false;
  isPastTournament = false;
  csvData: Array<{}> = [];
  onHover = false;
  idProof;
  isProcessing = false;
  constructor(
    private tournamentService: TournamentService,
    private toastService: ToastService,
    private modalService: NgbModal,
    private languageService: LanguageService,
    private translateService: TranslateService,
    private router: Router,
   // private ngxCsvParser: NgxCsvParser,
    private matDialog: MatDialog,

    @Inject(DOCUMENT) private document: Document
  ) {}

  ngOnInit(): void {
    this.languageService.language.subscribe((lang) =>
      this.translateService.use(lang || 'en')
    );
    this.fetchParticipants();
  }

  /**
   * Fetch All Participants Of the Tournament
   */
  fetchParticipants = async () => {
    try {
      this.isLoaded = false;
      const encodeUrl = `?query=${encodeURIComponent(
        JSON.stringify({ tournamentId: this.tournamentId })
      )}`;
      const participants = await this.tournamentService
        .getParticipants(encodeUrl)
        .toPromise();
      this.allParticipantsList = participants?.data;
      this.participantsListPending = participants?.data.filter(
        (item) => !item?.participantStatus
      );

      this.participantsListApproved = participants?.data.filter(
        (item) => item?.participantStatus === 'approved'
      );

      this.participantsListRejected = participants?.data.filter(
        (item) => item?.participantStatus === 'rejected'
      );
      this.setParticipant();
      this.isLoaded = true;
    } catch (error) {
      this.isLoaded = true;
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  /**
   * Open Confirmation Modal To Approve Or Reject Participant
   * @param content model template
   * @param participant participant details
   * @param isIndividaul check
   */
  openModelOrUpdateTeam = async (content, participant, isIndividaul = true) => {
    this.selectedParticipantStatus = participant?.participantStatus;
    const result = isIndividaul
      ? await this.modalService.open(content, {
          windowClass: 'confirm-modal-content',
          centered: true,
        }).result
      : content;
    if (result === 'approved') {
      const data = {
        participantStatus: 'approved',
      };
      this.updateParticipant(participant._id, data);
    } else if (result === 'rejected') {
      const data = {
        participantStatus: 'rejected',
      };

      this.updateParticipant(participant._id, data);
    } else if (result === 'edit') {
      this.router.navigate([
        '/tournament/' +
          this.tournamentDetails?.slug +
          '/join' +
          '/' +
          participant._id,
      ]);
    } else {
      this.selectedTeam = null;
    }
    this.selectedParticipantStatus = null;
  };

  /**
   * Update Participant Status
   * @param id participantID
   * @param data participant status
   */
  updateParticipant = async (id, data) => {
    try {
      this.selectedTeam = null;
      if (
        this.tournamentDetails?.isSeeded ||
        this.tournamentDetails?.isFinished ||
        new Date() > new Date(this.tournamentDetails?.startDate)
      ) {
        this.toastService.showError(
          this.translateService.instant(
            'MANAGE_TOURNAMENT.PARTICIPANT.DONT_UPDATE_PARTICIPANT_STATUS'
          )
        );
        return;
      }
      const response = await this.tournamentService
        .updateParticipant(id, data)
        .toPromise();
      this.toastService.showSuccess(response?.message);
      this.fetchParticipants();
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  ngOnChanges(simpleChanges: SimpleChanges) {
    if (
      simpleChanges.hasOwnProperty('currentTab') &&
      simpleChanges.currentTab
    ) {
      this.selectedTeam = null;
      this.setParticipant();
    }

    if (
      simpleChanges.hasOwnProperty('tournamentDetails') &&
      simpleChanges.tournamentDetails
    ) {
      this.isPastTournament =
        this.tournamentDetails?.isSeeded ||
        this.tournamentDetails?.isFinished ||
        new Date() > new Date(this.tournamentDetails?.startDate);
    }
  }

  /**
   * Set Participant List Based On Current Tab
   */
  setParticipant() {
    switch (true) {
      case this.currentTab === 'pendingParticipants':
        this.selectedList = this.participantsListPending;
        break;
      case this.currentTab === 'approvedParticipants':
        this.selectedList = this.participantsListApproved;
        break;
      case this.currentTab === 'rejectedParticipants':
        this.selectedList = this.participantsListRejected;
        break;
      default:
        this.selectedList = [];
        break;
    }
  }

  exportToCSV() {
    this.csvData = [];

    const elimantionType = {
      single: 'SINGLE ELIMINATION',
      double: 'DOUBLE ELIMINATION',
      round_robin: 'ROUND ROBIN',
      battle_royale: 'BATTLE ROYAL',
    };

    const exportCSV = (
      data,
      teamName,
      captainName,
      status,
      pNameSubstr,
      isTeam
    ) => {
      return {
        ...(isTeam && {
          'Team Name': teamName || '',
          'Captain Name': captainName || '',
        }),
        'Player Name': data?.name ? `${pNameSubstr}${data?.name}` : 'N/A',
        'Email Id': data?.email || 'N/A',
        'Mobile Number': data?.phoneNumber || 'N/A',
        'Game Id': data?.inGamerUserId || 'N/A',
        'User Name': data?.tournamentUsername || 'N/A',
        ...(this.tournamentDetails?.isIdProofRequired && {
          NRIC: data?.idNumber || 'N/A',
          Address: data?.address || 'N/A',
          Age: data?.age || 'N/A',
          'NRIC DOC': data?.idProofDoc || 'N/A',
          'Discord Channel': data?.discordChannelName || 'N/A',
        }),
        ...(status && { Status: status }),
      };
    };

    const tournamentDetails = {
      'Tournament Name': this.tournamentDetails.name,
      'Tournament Type': elimantionType[this.tournamentDetails.bracketType],
    };

    for (let item of this.allParticipantsList) {
      this.csvData.push(
        exportCSV(
          item,
          item?.teamName,
          item?.name,
          item?.participantStatus || 'pending',
          '',
          true
        )
      );
      for (let teamMember of item?.teamMembers || []) {
        this.csvData.push(exportCSV(teamMember, '', '', '', '', true));
      }
      for (let substituteMember of item?.substituteMembers || []) {
        this.csvData.push(
          exportCSV(substituteMember, '', '', '', '(SUB) ', true)
        );
      }
    }

    this.JSONToCSVConvertor(
      this.csvData,
      this.tournamentDetails.name,
      true,
      tournamentDetails
    );
  }

  JSONToCSVConvertor(JSONData, ReportTitle, ShowLabel, tournamentDetail) {
    var arrData = typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;
    var row = '';
    var CSV = '';

    if (ShowLabel) {
      for (let i in tournamentDetail) {
        row += i + ',';
      }
      row = row.slice(0, -1);
      CSV += row + '\r\n';
      row = '';
      for (let i in tournamentDetail) {
        row += '"' + tournamentDetail[i] + '",';
      }
      row.slice(0, row.length - 1);
      CSV += row + '\r\n';

      CSV += ReportTitle + '\r\n\n';
      row = '';
      for (var index in arrData[0]) {
        row += index + ',';
      }
      row = row.slice(0, -1);
      CSV += row + '\r\n';
    }

    for (var i = 0; i < arrData.length; i++) {
      var row = '';
      for (var index in arrData[i]) {
        if (index === 'Mobile Number') {
          row += '"=""' + arrData[i][index] + '""",';
        } else if (index === 'NRIC') {
          row += '"=""' + arrData[i][index] + '""",';
        } else {
          row += '"' + arrData[i][index] + '",';
        }
      }
      row.slice(0, row.length - 1);
      CSV += row + '\r\n';
    }

    if (CSV == '') {
      this.toastService.showError('Invalid data');
      return;
    }
    var universalBOM = '\uFEFF';
    var fileName = 'MESF_';
    fileName += ReportTitle.replace(/ /g, '_');
    var uri =
      'data:text/csv;charset=utf-8,' + encodeURIComponent(universalBOM + CSV);
    var link = this.document.createElement('a');
    link.href = uri;
    link.download = fileName + '.csv';
    this.document.body.appendChild(link);
    link.click();
    this.document.body.removeChild(link);
  }

  expandImage = async (content, image) => {
    try {
      this.idProof = image;
      const result = await this.modalService.open(content, {
        ariaLabelledBy: 'modal-basic-title',
        windowClass: 'modal-upload-screenshot',
        centered: true,
      }).result;
    } catch (error) {
      // this.toastService.showInfo('Again, This feature is under development');
    }
  };

  @ViewChild('fileImportInput', { static: false }) fileImportInput: any;

  header = false;
  async fileChangeListener($event: any, filesValue: any) {
    if (filesValue.length === 0) {
      this.toastService.showError('No file found');
      return;
    }

    if (!this.validateFile(filesValue[0].name)) {
      this.toastService.showError(
        'Invalid file extension, only .csv is supported'
      );
      return;
    }
    // Select the files from the event
    const files = $event.srcElement.files;

    // Parse the file you want to select for the operation along with the configuration
    // this.ngxCsvParser
    //   .parse(files[0], { header: this.header, delimiter: ',' })
    //   .pipe()
    //   .subscribe(
    //     async (results: Array<any>) => {
    //       if (this.allParticipantsList.length >= 2) {
    //         this.toastService.showError(
    //           this.translateService.instant('TOURNAMENT.STATUS_4')
    //         );
    //         return true;
    //       }

    //       let teams = [];
    //       let team: TeamGenerasi;
    //       let teamIndexes = [];

    //       for (let i = 0; i < results.length; i++) {
    //         if (results[i][0] == 'team') {
    //           teamIndexes.push(i);
    //         }
    //       }

    //       this.isProcessing = true;

    //       for (let i = 0; i < teamIndexes.length; i++) {
    //         const minIndex = teamIndexes[i];
    //         let maxIndex = 0;

    //         if (i === teamIndexes.length - 1) {
    //           maxIndex = results.length - 1;
    //         } else {
    //           maxIndex = teamIndexes[i + 1] - 1;
    //         }

    //         team = new TeamGenerasi();
    //         for (let j = minIndex; j <= maxIndex; j++) {
    //           if (results[j][0] == 'team') {
    //             team.logo = results[j][1];
    //             team.teamName = results[j][2];
    //             team.schoolName = results[j][3];
    //             team.participantState = results[j][4];
    //             team.name = results[j][5];
    //             team.email = results[j][6];
    //             team.inGamerUserId = results[j][6];
    //             team.phoneNumber = results[j][7];
    //             team.managerStatus = results[j][8];
    //             team.participantType = this.tournamentDetails.participantType;
    //             team.tournamentId = this.tournamentDetails?._id;
    //           } else if (results[j][0] == 'player') {
    //             let teamPlayer = new TeamPlayerGenerasi();
    //             teamPlayer.name = results[j][1];
    //             teamPlayer.inGamerUserId = results[j][2];
    //             teamPlayer.account = results[j][3];
    //             teamPlayer.idNumber = results[j][4];
    //             teamPlayer.gender = results[j][5];
    //             teamPlayer.motherName = results[j][6];
    //             teamPlayer.motherContactNumber = results[j][7];
    //             teamPlayer.motherEmail = results[j][8];
    //             teamPlayer.fatherName = results[j][9];
    //             teamPlayer.fatherContactNumber = results[j][10];
    //             teamPlayer.fatherEmail = results[j][11];
    //             teamPlayer.parental = results[j][12];
    //             team.teamMembers.push(teamPlayer);
    //           } else if (results[j][0] == 'substitute') {
    //             let teamPlayer = new TeamPlayerGenerasi();
    //             teamPlayer.name = results[j][1];
    //             teamPlayer.inGamerUserId = results[j][2];
    //             teamPlayer.account = results[j][3];
    //             teamPlayer.idNumber = results[j][4];
    //             teamPlayer.gender = results[j][5];
    //             teamPlayer.motherName = results[j][6];
    //             teamPlayer.motherContactNumber = results[j][7];
    //             teamPlayer.motherEmail = results[j][8];
    //             teamPlayer.fatherName = results[j][9];
    //             teamPlayer.fatherContactNumber = results[j][10];
    //             teamPlayer.fatherEmail = results[j][11];
    //             teamPlayer.parental = results[j][12];
    //             team.substituteMembers.push(teamPlayer);
    //           }
    //           if (j === maxIndex) {
    //             teams.push(team);
    //           }
    //         }
    //       }

    //       if (teams.length > 0) {
    //         const data: InfoPopupComponentData = {
    //           ...this.popUpTitleAndText(),
    //           type: InfoPopupComponentType.confirm,
    //           btnText: this.translateService.instant('TOURNAMENT.CONFIRM'),
    //         };
    //         const confirmed = await this.matDialog
    //           .open(InfoPopupComponent, { data })
    //           .afterClosed()
    //           .toPromise();
    //         if (confirmed) {
    //           this.tournamentService.uploadParticipant(teams).subscribe(
    //             (response) => {
    //               this.isProcessing = false;
    //               this.toastService.showSuccess(response?.message);
    //               window.location.reload();
    //             },
    //             (err) => {
    //               this.isProcessing = false;
    //               this.toastService.showError(
    //                 err?.error?.message || err?.message
    //               );
    //             }
    //           );
    //         }
    //       }
    //     },
    //     (error: NgxCSVParserError) => {
    //       this.toastService.showError(error?.message);
    //     }
    //   );
  }

  //validation csv file
  validateFile(name: String) {
    var ext = name.substring(name.lastIndexOf('.') + 1);
    if (ext.toLowerCase() == 'csv') {
      return true;
    } else {
      return false;
    }
  }
  popUpTitleAndText = () => {
    return {
      title: this.translateService.instant('TOURNAMENT.UPLOAD_TOURNAMENT'),
      text: this.translateService.instant('TOURNAMENT.STATUS_3'),
    };
  };
}
