import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreateComponent } from './create.component';
// Module
import { SharedModule } from '../../../shared/modules/shared.module';
import { CoreModule } from '../../../core/core.module';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
// Routing
import { CreateRoutingModule } from './create-routing.module';
import { FormComponentModule } from '../../../shared/components/form-component/form-component.module';
import { VenueAddressComponent } from './venue-address/venue-address.component';

import { BasicComponent } from './basic/basic.component';
import { ConditionsComponent } from './conditions/conditions.component';
import { FinalComponent } from './final/final.component';

import { PreviewComponent } from './preview/preview.component';
import { OverviewComponent } from './preview/overview/overview.component';
import { PaymentComponent } from './preview/payment/payment.component';

import { FooterComponent } from './footer/footer.component';

import {
  FontAwesomeModule,
  FaIconLibrary,
} from '@fortawesome/angular-fontawesome';
import {
  faTrash,
  faEdit,
  faCopy,
  faPlay,
  faStop,
  faExpand,
  faPlus,
} from '@fortawesome/free-solid-svg-icons';

import { PaymentModule } from '../../payment/payment.module';

@NgModule({
  declarations: [
    CreateComponent,
    PreviewComponent,
    VenueAddressComponent,
    OverviewComponent,
    BasicComponent,
    FooterComponent,
    ConditionsComponent,
    FinalComponent,
    PaymentComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    CoreModule,
    MatProgressSpinnerModule,
    CreateRoutingModule,
    FormComponentModule,
    FontAwesomeModule,
    PaymentModule,
  ],
})
export class CreateModule {
  constructor(private library: FaIconLibrary) {
    library.addIcons(faTrash, faEdit, faCopy, faPlay, faStop, faExpand, faPlus);
  }
}
