import {
  Component,
  Input,
  OnInit,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import {
  FormGroup,
  FormControl,
  ControlContainer,
  FormGroupDirective,
  Validators,
  AbstractControl,
  FormArray,
  ValidatorFn,
  FormBuilder,
  FormArrayName,
} from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import {
  ConstantsService,
  UserService,
  ToastService,
} from '../../../../core/service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ITournament } from '../../../../shared/models';
import { Subject } from 'rxjs';

@Component({
  selector: 'tournament-conditions',
  templateUrl: './conditions.component.html',
  styleUrls: ['./conditions.component.scss'],
  viewProviders: [
    { provide: ControlContainer, useExisting: FormGroupDirective },
  ],
})
export class ConditionsComponent implements OnInit {
  @ViewChild('regionModel') private regionModal: TemplateRef<any>;
  @Input() tConditions: Subject<any>;

  conditions;
  condition_active = 'description';
  currencyList = [];
  isCharged: boolean = false;

  // sponsor config
  logoDimension = { width: 180, height: 180 };
  logoSize = 1024 * 1024;
  bannerDimension = { width: 1200, height: 200 };
  bannerSize = 1024 * 1024;

  plaformType = [
    { name: 'online', description: 'TOURNAMENT.CREATE.OT_DESC' },
    { name: 'offline', description: 'TOURNAMENT.CREATE.OFF_T_DESC' },
    { name: 'hybrid', description: 'TOURNAMENT.CREATE.HT_DESC' },
  ];
  regionSearchKeyword: any;
  regionsArray = [];

  constructor(
    private tournament: FormGroupDirective,
    public fb: FormBuilder,
    private toastService: ToastService,
    private translateService: TranslateService,
    private modalService: NgbModal,
    private userService: UserService
  ) {}

  ngOnInit(): void {
    this.conditions = this.tournament.form;
    this.currencyList = ConstantsService?.currencyList;
    this.addControls();
    this.getRegions();
  }

  // add controls
  addControls() {
    this.conditions.addControl('description', new FormControl(''));
    this.conditions.addControl('rules', new FormControl(''));
    this.conditions.addControl('isPrize', new FormControl(false));
    this.conditions.addControl('isIncludeSponsor', new FormControl(false));
    this.conditions.addControl(
      'tournamentType',
      new FormControl('', Validators.required)
    );
    this.conditions.addControl('isScreenshotRequired', new FormControl(false));
    this.conditions.addControl('isShowCountryFlag', new FormControl(false));
    this.conditions.addControl(
      'isSpecifyAllowedRegions',
      new FormControl(false)
    );
    this.conditions.addControl(
      'isManualApproverParticipant',
      new FormControl(false)
    );
    this.conditions.addControl('isIdProofRequired', new FormControl(false));
    this.conditions.addControl('isCaptainRequired', new FormControl(false));

    this.conditions.addControl('scoreReporting', new FormControl(1));

    this.tConditions.subscribe((condition) => {
      this.updatetConditions(condition);
    });
  }

  // handlers

  prizeClickHandler(checked, prizeLen = 3) {
    if (checked) {
      this.conditions.addControl(
        'prizeList',
        this.fb.array([], this.customPrizeValidator())
      );
      for (let i = 0; i < prizeLen; i++) this.addPrize();
      this.conditions.addControl(
        'prizeCurrency',
        new FormControl('', Validators.compose([Validators.required]))
      );
    } else {
      this.conditions.removeControl('prizeList');
      this.conditions.removeControl('prizeCurrency');
    }
  }

  sponsorClickHandler(checked, len = 1) {
    if (checked) {
      this.conditions.addControl('sponsors', this.fb.array([]));
      this.addSponsor(len);
    } else {
      this.conditions.removeControl('sponsors');
    }
  }

  venueClickHandler(type, venueLength = 1) {
    if (this.conditions?.value?.venueAddress?.length) {
      this.conditions.removeControl('venueAddress');
    }

    if (['offline', 'hybrid'].includes(type)) {
      this.conditions.addControl('venueAddress', this.fb.array([]));
      for (let i = 0; i < venueLength; i++) this.addVenueAddress(type);
    }
  }

  regionHandler = async (check) => {
    try {
      if (check) {
        this.conditions.addControl('regionsAllowed', new FormControl([]));
        const result = await this.modalService.open(this.regionModal, {
          scrollable: true,
          windowClass: 'custom-modal-content modal-regions-tournament',
          centered: true,
        }).result;
        this.updateRegionConfig();
      } else {
        this.conditions.removeControl('regionsAllowed');
      }
    } catch (error) {
      this.updateRegionConfig();
    }
  };

  // custom validators

  customPrizeValidator(): ValidatorFn {
    return (formArray: FormArray): { [key: string]: any } | null => {
      let total = 0;
      for (let item of formArray.controls) {
        total = total + item.value.value;
      }

      const valid = formArray.controls.length ? total > 0 : true;
      return valid ? null : { prizeMoneyRequired: true };
    };
  }

  emptyCheck = (control: FormControl) => {
    return control.value && control.value.trim() ? null : { required: true };
  };

  enablePrize() {
    if (this.conditions.value.isPrize != true) {
      this.conditions.controls.isPrize.setValue(true);
      this.prizeClickHandler(true);
    }
  }

  addPrize(): void {
    const prizeList = this.conditions.get('prizeList') as FormArray;
    let name = '';
    if (prizeList.controls.length <= 5) {
      switch (true) {
        case prizeList.controls.length == 0:
          name = '1st';
          break;
        case prizeList.controls.length == 1:
          name = '2nd';
          break;
        case prizeList.controls.length == 2:
          name = '3rd';
          break;
        default:
          name = `${prizeList.controls.length + 1}th`;
          break;
      }
      const createPrizeForm = (): FormGroup => {
        return this.fb.group({
          name: [name],
          value: [
            '',
            Validators.compose([
              Validators.required,
              Validators.pattern('^[0-9]*$'),
            ]),
          ],
        });
      };
      prizeList.push(createPrizeForm());
    } else {
      this.toastService.showError(
        this.translateService.instant('TOURNAMENT.MAX_6_ALLOWED')
      );
    }
  }

  clearFieldFromArray(field, index): void {
    const array = this.conditions.get(field) as FormArray;
    array.removeAt(index);
    if (array.length < 1 && field == 'sponsors') {
      this.conditions.controls.isIncludeSponsor.setValue(false);
    }
  }

  createSponsor(): FormGroup {
    return this.fb.group({
      sponsorName: [
        '',
        Validators.compose([Validators.required, this.emptyCheck]),
      ],
      website: [
        '',
        Validators.compose([Validators.pattern(ConstantsService?.webUrlRegex)]),
      ],
      playStoreUrl: [
        '',
        Validators.compose([
          Validators.pattern(ConstantsService?.playStoreUrlRegex),
        ]),
      ],
      appStoreUrl: [
        '',
        Validators.compose([
          Validators.pattern(ConstantsService?.appStoreUrlRegex),
        ]),
      ],
      sponsorLogo: ['', Validators.compose([Validators.required])],
      sponsorBanner: ['', Validators.compose([Validators.required])],
    });
  }

  addSponsor(len = 1): void {
    const sponsor = this.conditions.get('sponsors') as FormArray;
    for (let i = 0; i < len; i++) {
      sponsor.push(this.createSponsor());
    }
  }

  createVenueAddress(type): FormGroup {
    if (['offline', 'hybrid'].includes(type)) {
      return this.fb.group({
        country: ['', Validators.compose([Validators.required])],
        region: ['', Validators.compose([Validators.required])],
        venue: ['', Validators.compose([Validators.required])],
        ...(type == 'hybrid' && {
          stage: ['', Validators.compose([Validators.required])],
        }),
      });
    }
  }

  addVenueAddress(type): void {
    const array = this.conditions.get('venueAddress') as FormArray;
    array.push(this.createVenueAddress(type));
  }

  updateRegionConfig = () => {
    if (!this.conditions?.value?.regionsAllowed?.length) {
      this.conditions.controls.isSpecifyAllowedRegions.setValue(false);
      this.conditions.removeControl('regionsAllowed');
    }
  };

  addRegion(event, id) {
    if (event.target.checked) {
      this.conditions.value.regionsAllowed.push(id);
    } else {
      const index = this.conditions.value.regionsAllowed.indexOf(id);
      if (index >= 0) {
        this.conditions.value.regionsAllowed.splice(index, 1);
      }
    }
  }

  removeRegions(index) {
    this.conditions.value.regionsAllowed.splice(index, 1);
    this.updateRegionConfig();
  }

  getRegions = async () => {
    try {
      const data = await this.userService.getStates().toPromise();
      this.regionsArray = data.states.filter((s) => s.country_id == 132);
    } catch (error) {
      this.toastService.showError(error?.message);
    }
  };

  updatetConditions(condition) {
    this.isCharged = condition?.isCharged;

    this.prizeClickHandler(
      condition?.isPrize,
      condition?.prizeList?.length || 0
    );
    this.sponsorClickHandler(
      condition.isIncludeSponsor,
      condition?.sponsors?.length || 0
    );
    this.venueClickHandler(
      condition?.tournamentType,
      condition?.venueAddress?.length || 0
    );
    if (condition?.isSpecifyAllowedRegions) {
      this.conditions.addControl('regionsAllowed', new FormControl([]));
    }

    this.conditions.patchValue({
      ...(condition?.isPrize && {
        prizeList: condition?.prizeList,
        prizeCurrency: condition?.prizeCurrency,
      }),
      ...(condition?.isIncludeSponsor && { sponsors: condition?.sponsors }),
      ...(['offline', 'hybrid'].includes(condition?.tournamentType) && {
        venueAddress: condition?.venueAddress,
      }),
      ...(condition?.isSpecifyAllowedRegions && {
        regionsAllowed: condition?.regionsAllowed,
      }),
    });
  }
}
