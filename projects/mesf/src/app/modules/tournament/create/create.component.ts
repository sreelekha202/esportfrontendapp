import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  AfterViewChecked,
  ChangeDetectorRef,
} from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import {
  UserService,
  TournamentService,
  UtilsService,
  ToastService,
  GameService,
} from '../../../core/service';
import { IUser } from '../../../shared/models';
import { Subject } from 'rxjs';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss'],
})
export class CreateComponent implements OnInit, AfterViewChecked {
  @ViewChild('mainContainer', { read: ElementRef }) public scroll: ElementRef<
    any
  >;
  childNotifier: Subject<any> = new Subject<any>();
  tournamentDetails: Subject<any> = new Subject<any>();

  tournamentForm: FormGroup;

  user: IUser;

  active = 1;
  showLoader = true;
  editTournament = false;
  fallBack: string;
  fallBackIndex: any;

  isAllDataLoaded = false;
  apiLoaded: Array<boolean> = [];
  isParticipantRegistered: boolean = false;
  constructor(
    public toastService: ToastService,
    private fb: FormBuilder,
    private userService: UserService,
    private tournamentService: TournamentService,
    private activatedRoute: ActivatedRoute,
    private utilsService: UtilsService,
    private readonly changeDetectorRef: ChangeDetectorRef,
    private gameService: GameService
  ) {}

  ngOnInit(): void {
    this.activatedRoute.queryParams.subscribe((params) => {
      if (params.fallback) {
        this.fallBack = '/admin/esports-management';
        this.fallBackIndex = params.activeTab ? params.activeTab : 0;
      }
    });
    this.tournamentForm = this.fb.group({
      step: [1],
    });
    const slug = this.activatedRoute?.snapshot?.params?.id;
    this.fetchCurrentUserDetails();
    this.fetchAllData(slug);
  }

  ngAfterViewChecked(): void {
    this.changeDetectorRef.detectChanges();
  }

  fetchCurrentUserDetails() {
    this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.user = data;
      }
    });
  }

  scrollToTop() {
    this.scroll.nativeElement.scrollIntoView();
  }

  getFooterResponse(message) {
    this.toastService.showError(message);
    this.scrollToTop();
  }

  activeHandler(activeId) {
    this.active = activeId;
    this.scrollToTop();
  }

  navChangeHandler(event) {
    this.childNotifier.next(event);
  }

  fetchAllData = async (slug) => {
    try {
      if (slug) {
        const query = JSON.stringify({
          slug,
        });
        const tournament = await this.tournamentService
          .getTournaments({ query })
          .toPromise();
        const tDetails = tournament?.data?.length ? tournament?.data[0] : null;

        this.tournamentForm.addControl('_id', new FormControl(tDetails?._id));
        this.tournamentDetails.next(tDetails);
        this.fetchRegisteredParticipant(tDetails?._id);
      }

      const query = JSON.stringify({ isTournamentAllowed: true });
      const game = await this.gameService.getAllGames(query).toPromise();
      this.utilsService.setGame(game?.data);
      this.isAllDataLoaded = true;
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
      this.isAllDataLoaded = true;
    }
  };
  fetchRegisteredParticipant = async (tournamentId) => {
    try {
      const encodeUrl = `?query=${encodeURIComponent(
        JSON.stringify({ tournamentId })
      )}&select=''&option=${encodeURIComponent(
        JSON.stringify({ count: true })
      )}`;
      const response = await this.tournamentService
        .getParticipants(encodeUrl)
        .toPromise();
      this.isParticipantRegistered = !!response?.totals?.count;
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };
}
