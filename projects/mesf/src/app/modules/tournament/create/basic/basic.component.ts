import { DOCUMENT } from '@angular/common';
import {
  Component,
  Inject,
  Input,
  OnInit,
  OnChanges,
  SimpleChanges,
} from '@angular/core';
import {
  FormGroup,
  FormControl,
  ControlContainer,
  FormGroupDirective,
  Validators,
  AbstractControl,
  FormArray,
  ValidatorFn,
  FormBuilder,
} from '@angular/forms';
import {
  ConstantsService,
  UtilsService,
  ToastService,
} from '../../../../core/service';
import { TranslateService } from '@ngx-translate/core';
import { SwiperConfigInterface } from 'ngx-swiper-wrapper';
import { Subject } from 'rxjs';

@Component({
  selector: 'tournament-basic',
  templateUrl: './basic.component.html',
  styleUrls: ['./basic.component.scss'],
  viewProviders: [
    { provide: ControlContainer, useExisting: FormGroupDirective },
  ],
})
export class BasicComponent implements OnInit, OnChanges {
  @Input() basicDetails: Subject<any>;
  @Input() isAllDataLoaded: boolean;
  @Input() isParticipantRegistered: boolean = false;
  gameArray = [];
  basicInfo;
  selectedGameIndex = 0;
  bracketList = [];
  platform = [];
  config: SwiperConfigInterface = {
    direction: 'horizontal',
    slidesPerView: 'auto',
    keyboard: false,
    mousewheel: false,
    scrollbar: false,
    navigation: true,
    pagination: true,
    centeredSlides: true,
    slideToClickedSlide: true,
    //autoHeight: true,
    breakpoints: {
      320: {
        slidesPerView: 3,
        spaceBetween: 15,
      },
      575: {
        slidesPerView: 4,
        spaceBetween: 15,
        autoHeight: false,
      },

      1199: {
        slidesPerView: 'auto',
        autoHeight: true,
      },
    },
  };
  minStartDateValue = this.setMinDate();
  enableteamCounter = false;
  participantType;
  teamFormat;
  matchFormat;
  currencyList = [];
  checkInTimeOptions = [];
  stageMatchFormat;
  stageMatchType;

  stageBracketType = ['single', 'double'];

  // banner config
  dimension = { width: 1010, height: 586 };
  bannerSize = 1000 * 1000 * 2;

  constructor(
    private tournament: FormGroupDirective,
    private toastService: ToastService,
    private translateService: TranslateService,
    private utilsService: UtilsService,
    public fb: FormBuilder,
    @Inject(DOCUMENT) private document: Document
  ) {}

  ngOnInit(): void {
    this.basicInfo = this.tournament.form;

    this.participantType = ConstantsService?.participantType;
    this.teamFormat = ConstantsService?.teamFormat;
    this.matchFormat = ConstantsService?.matchFormat;
    this.currencyList = ConstantsService?.currencyList;
    this.checkInTimeOptions = ConstantsService.checkInTimeOptions;
    this.stageMatchType = ConstantsService?.stageMatchType;
    this.stageMatchFormat = ConstantsService?.matchFormat;

    this.addControls();
    this.getGames();
  }

  // form custom validators

  emptyCheck = (control: FormControl) => {
    return control.value && control.value.trim() ? null : { required: true };
  };

  ValidateStartTime(control: AbstractControl): { [key: string]: any } | null {
    return !control.value
      ? null
      : this.ValidateBothStartDateAndTime(control['_parent'], 'pastStartTime');
  }

  ValidateStartDate(control: AbstractControl): { [key: string]: any } | null {
    return !control.value
      ? null
      : this.ValidateBothStartDateAndTime(control['_parent'], 'pastStartDate');
  }

  ValidateEndDate(control: AbstractControl): { [key: string]: any } | null {
    return !control.value
      ? null
      : this.validateBothEndDateAndTime(control['_parent'], 'pastEndDate');
  }

  validateBothEndDateAndTime(
    formGroup: FormGroup,
    ekey: string
  ): { [key: string]: boolean } {
    const { startDate, startTime, endDate } = formGroup.controls;
    if (ekey == 'pastEndDate' && !endDate?.value) {
      return { required: true };
    } else if (startDate?.value && startTime?.value && endDate?.value) {
      const startDt = new Date(startDate.value);
      startDt.setHours(startTime.value?.hour);
      startDt.setMinutes(startTime.value?.minute);

      const endDt = new Date(endDate.value);
      if (endDt > startDt) {
        this.basicInfo.get('endDate').setErrors(null);
        return null;
      } else if (
        ekey == 'pastEndDate' &&
        endDt.toDateString() === startDt.toDateString()
      ) {
        return null;
      } else {
        return {
          [ekey]: true,
        };
      }
    }
    return null;
  }

  ValidateBothStartDateAndTime(
    formGroup: FormGroup,
    ekey: string
  ): { [key: string]: boolean } {
    const { startDate, startTime, isCheckInRequired } = formGroup.controls;
    const time = startTime.value || {};
    const { hour, minute } = time;

    if (startTime.value && startDate.value) {
      const date = new Date(startDate.value);
      date.setHours(hour);
      date.setMinutes(minute);
      const diff = date.getTime() - Date.now();
      if (diff < 300000) {
        return { [ekey]: true };
      }
      const isCheckInValid = this.validateCheckedIn(formGroup);
      const endDateValidate = this.validateBothEndDateAndTime(
        formGroup,
        'pastEndDate'
      );
      this.basicInfo.get('endDate').setErrors(endDateValidate);
      if (isCheckInRequired?.value) {
        this.basicInfo.get('checkInStartDate').setErrors(isCheckInValid);
      }
      this.basicInfo.get('startTime').setErrors(null);
      this.basicInfo.get('startDate').setErrors(null);
    }

    return null;
  }

  validateCheckedIn(formGroup: FormGroup): { [key: string]: boolean } {
    const {
      startDate,
      startTime,
      isCheckInRequired,
      checkInStartDate,
    } = formGroup.controls;
    if (
      startDate?.value &&
      startTime?.value &&
      isCheckInRequired?.value &&
      checkInStartDate?.value
    ) {
      const checkInStart = new Date(startDate.value);
      checkInStart.setHours(startTime.value.hour);
      checkInStart.setMinutes(startTime.value.minute);
      const checkInTime = ConstantsService.checkInTimeOptions.find(
        (el) => el.name === checkInStartDate.value
      );

      checkInStart.setHours(checkInStart.getHours() - checkInTime?.value?.hour);
      checkInStart.setMinutes(
        checkInStart.getMinutes() - checkInTime?.value?.minute
      );
      if (checkInStart.getTime() < Date.now()) {
        return { checkInExpired: true };
      }
    }
    return null;
  }

  teamInGroupValidation = (field?: string): ValidatorFn => {
    return (control: AbstractControl): { [key: string]: any } | null => {
      if (control['_parent']) {
        const {
          noOfTeamInGroup,
          noOfWinningTeamInGroup,
          maxParticipants,
          noOfStage,
        } = control?.['_parent']?.controls;

        if (
          maxParticipants?.valid &&
          noOfTeamInGroup?.value &&
          noOfWinningTeamInGroup?.value &&
          maxParticipants?.value &&
          noOfStage
        ) {
          const fieldValidation = (
            maxParticipants,
            noOfTeamInGroup,
            noOfWinningTeam
          ) => {
            if (maxParticipants < noOfTeamInGroup) {
              return { max: { max: maxParticipants } };
            } else if (noOfTeamInGroup > noOfWinningTeam) {
              const check = noOfTeamInGroup % noOfWinningTeam != 0;
              return check ? { multiStageConfigError: true } : null;
            } else {
              return { multiStageConfigError: true };
            }
          };
          const errorConfig = fieldValidation(
            maxParticipants?.value,
            noOfTeamInGroup?.value,
            noOfWinningTeamInGroup?.value
          );
          if (!errorConfig) {
            const totalStage = (maxP, teamPerG, winPerG, stage) => {
              const playerForNextRound = (maxP, teamPerG, winPerG) => {
                const diff = Math.ceil(maxP / teamPerG) - maxP / teamPerG;
                return diff < 0.5
                  ? Math.ceil(maxP / teamPerG) * winPerG
                  : Math.floor(maxP / teamPerG) * winPerG;
              };

              const createNoOfTeamInGroup = (
                pLen,
                noOfTeamInGroup,
                noOfWinningTeamInGroup
              ) => {
                const expectedNoOfGroup = Math.ceil(pLen / noOfTeamInGroup);
                const expectedMinPlayerInGroup = Math.floor(
                  pLen / expectedNoOfGroup
                );
                return expectedMinPlayerInGroup > noOfWinningTeamInGroup
                  ? expectedMinPlayerInGroup
                  : createNoOfTeamInGroup(
                      pLen,
                      noOfTeamInGroup + 1,
                      noOfWinningTeamInGroup
                    );
              };

              const teamPerGp = createNoOfTeamInGroup(maxP, teamPerG, winPerG);
              const nextStageP = playerForNextRound(maxP, teamPerGp, winPerG);

              return nextStageP == winPerG
                ? stage
                : nextStageP < teamPerG
                ? stage + 1
                : totalStage(nextStageP, teamPerG, winPerG, stage + 1);
            };

            const noOfStage = totalStage(
              maxParticipants?.value,
              noOfTeamInGroup?.value,
              noOfWinningTeamInGroup?.value,
              1
            );
            this.basicInfo.get('noOfStage').setValue(noOfStage);
          }

          if (field == 'noOfTeamInGroup') {
            return errorConfig;
          } else {
            this.basicInfo.get('noOfTeamInGroup').setErrors(errorConfig);
            return null;
          }
        }
      } else {
        return null;
      }
    };
  };

  ValidateCheckInDate(control: AbstractControl): { [key: string]: any } | null {
    return control.value ? this.validateCheckedIn(control['_parent']) : null;
  }

  /**
   *
   * @param field
   * @param fieldType 'STRING', 'BOOLEAN', 'NUMBER'
   * @param defaultValue
   * @param min
   * @param max
   */
  addNewControlAndUpdateValidity = (
    field,
    fieldType,
    defaultValue,
    min?,
    max?,
    pattern?,
    customValidation?
  ) => {
    const formControl = this.basicInfo.get(field);
    const validation = [];

    switch (fieldType) {
      case 'NUMBER':
        validation.push(Validators.required);
        if (min) validation.push(Validators.min(min));
        if (max) validation.push(Validators.max(max));
        if (pattern) validation.push(pattern);
        if (customValidation) validation.push(customValidation);
        break;
      case 'BOOLEAN':
        validation.push(Validators.required);
        break;
      case 'STRING':
        validation.push(Validators.required);
        break;
    }

    if (formControl) {
      formControl.setValidators(validation);
      formControl.updateValueAndValidity();
    } else {
      this.basicInfo.addControl(
        field,
        new FormControl(defaultValue, Validators.compose(validation))
      );
    }
  };

  removeControlOrValidity = (field) => {
    const formControl = this.basicInfo.get(field);
    if (formControl) {
      this.basicInfo.removeControl(field);
    } else {
    }
  };

  // Event Handlers ...

  tournamentNameChangeHandler(name) {
    const domain = `${this.document.location.protocol}//${this.document.location.hostname}/tournament/`;
    const url = `${domain}${name.replace(/\s+/g, '_')}`;
    this.basicInfo.get('url').setValue(url);
  }

  participantTypeHandler(val) {
    if (val == 'team') {
      this.basicInfo.controls['teamFormat'].setValidators([
        Validators.required,
      ]);
      this.addNewControlAndUpdateValidity('teamSize', 'NUMBER', 1, 2, 10);
    } else {
      this.removeControlOrValidity('teamSize');
      this.basicInfo.controls['teamFormat'].clearValidators();
    }
    this.basicInfo.controls['teamFormat'].updateValueAndValidity();
  }

  teamFormatHandler = (event, field) => {
    this.enableteamCounter = event == 'n';
    if (!this.enableteamCounter) {
      this.basicInfo.get(field).setValue(event);
    } else {
      this.basicInfo.get(field).setValue(2);
    }
  };

  allowSubstituteMemberHandler = (check) => {
    if (check) {
      this.basicInfo.controls['substituteMemberSize'].setValidators([
        Validators.required,
        Validators.max(10),
        Validators.min(1),
      ]);
    } else {
      this.basicInfo.controls['substituteMemberSize'].clearValidators();
      this.basicInfo.controls['substituteMemberSize'].setValue(0);
    }
    this.basicInfo.get('substituteMemberSize').updateValueAndValidity();
  };

  bracketChangeHandler(
    type,
    noOfPlacement?,
    maxPlacement = 2,
    maxParticipants?
  ) {
    let maximumParticipants = 1024;

    if (['round_robin', 'battle_royale'].includes(type)) {
      const maxLimit = type == 'battle_royale' ? 10 : 3;
      this.addNewControlAndUpdateValidity(
        'noOfRoundPerGroup',
        'NUMBER',
        1,
        1,
        maxLimit
      );

      type == 'battle_royale'
        ? this.removeControlOrValidity('noOfSet')
        : this.addNewControlAndUpdateValidity('noOfSet', 'STRING', '');
      type == 'battle_royale'
        ? this.addNewControlAndUpdateValidity(
            'noOfPlacement',
            'NUMBER',
            2,
            2,
            this.basicInfo?.value?.noOfTeamInGroup || maxPlacement || 2
          )
        : this.removeControlOrValidity('noOfPlacement');
      type == 'battle_royale'
        ? this.placementCounter(noOfPlacement || 2)
        : this.removeControlOrValidity('placementPoints');
      type == 'battle_royale'
        ? this.addNewControlAndUpdateValidity(
            'isKillPointRequired',
            'BOOLEAN',
            false
          )
        : this.removeControlOrValidity('isKillPointRequired');
      type == 'battle_royale'
        ? this.removeControlOrValidity('stageBracketType')
        : this.addNewControlAndUpdateValidity('stageBracketType', 'STRING', '');
      type == 'battle_royale'
        ? this.removeControlOrValidity('allowAdvanceStage')
        : this.addNewControlAndUpdateValidity(
            'allowAdvanceStage',
            'BOOLEAN',
            false
          );

      this.multiStageTournamentHandler(true, type, maxParticipants);
    } else {
      this.multiStageTournamentHandler(false);
      this.removeControlOrValidity('noOfRoundPerGroup');
      this.addNewControlAndUpdateValidity('noOfSet', 'STRING', '');
      this.removeControlOrValidity('noOfPlacement');
      this.removeControlOrValidity('placementPoints');
      this.removeControlOrValidity('isKillPointRequired');
      this.removeControlOrValidity('stageBracketType');
      this.addNewControlAndUpdateValidity(
        'allowAdvanceStage',
        'BOOLEAN',
        false
      );
    }

    switch (type) {
      case 'single':
        maximumParticipants = 1024;
        break;
      case 'double':
        maximumParticipants = 512;
        break;
      case 'round_robin':
        maximumParticipants = 40;
        break;
      case 'battle_royale':
        maximumParticipants = 1024;
        break;
    }
    this.addNewControlAndUpdateValidity(
      'maxParticipants',
      'NUMBER',
      2,
      2,
      maximumParticipants,
      false,
      this.teamInGroupValidation('maxParticipants').bind(this)
    );
  }

  maxParticipantHandler(value, bracketType) {
    if (['battle_royale', 'round_robin'].includes(bracketType)) {
      const maxLimit =
        bracketType == 'battle_royale' ? value : value >= 24 ? 24 : value;
      this.addNewControlAndUpdateValidity(
        'noOfTeamInGroup',
        'NUMBER',
        2,
        2,
        maxLimit,
        false,
        this.teamInGroupValidation('noOfTeamInGroup').bind(this)
      );
    }
  }

  killingPointHandler = (isAllowKillingPoint) => {
    isAllowKillingPoint
      ? this.addNewControlAndUpdateValidity('pointsKills', 'NUMBER', 1, 1)
      : this.removeControlOrValidity('pointsKills');
  };

  paidRegistrationHandler(check) {
    if (check) {
      this.addNewControlAndUpdateValidity('regFeeCurrency', 'STRING', '');
      this.addNewControlAndUpdateValidity('regFee', 'NUMBER', 1, 1);
    } else {
      this.removeControlOrValidity('regFeeCurrency');
      this.removeControlOrValidity('regFee');
    }
  }

  checkInDateHandler(check) {
    if (check) {
      this.basicInfo.addControl(
        'checkInStartDate',
        new FormControl(
          '',
          Validators.compose([
            Validators.required,
            this.ValidateCheckInDate.bind(this),
          ])
        )
      );
    } else {
      this.basicInfo.removeControl('checkInStartDate');
    }
  }

  allowAdvanceStageHandler = (allowAdvanceStage) => {
    if (allowAdvanceStage) {
      this.addNewControlAndUpdateValidity('stageMatch', 'STRING', '');
      this.addNewControlAndUpdateValidity('stageMatchNoOfSet', 'STRING', '');
    } else {
      this.removeControlOrValidity('stageMatch');
      this.removeControlOrValidity('stageMatchNoOfSet');
    }
  };

  placementCounter = async (value) => {
    this.basicInfo.addControl('placementPoints', this.fb.array([]));
    const placementPointsList = this.basicInfo.get(
      'placementPoints'
    ) as FormArray;

    const createPlacementForm = (position): FormGroup => {
      return this.fb.group({
        position: [position],
        value: [
          '',
          Validators.compose([
            Validators.required,
            Validators.pattern('^[0-9]*$'),
          ]),
        ],
      });
    };

    // remove placement node
    const deletePlacementForm = async (count, value) => {
      placementPointsList.removeAt(count);
      return count <= value ? true : await deletePlacementForm(--count, value);
    };

    // add placement node
    const addPlacementForm = async (count, value) => {
      placementPointsList.push(createPlacementForm(count));
      return count >= value ? true : await addPlacementForm(++count, value);
    };

    let count = placementPointsList.length;

    if (count > value) {
      await deletePlacementForm(count - 1, value);
    }

    if (count < value) {
      await addPlacementForm(count + 1, value);
    }
  };

  multiStageTournamentHandler = (
    isEnableFields: boolean,
    bracketType?,
    maxParticipants?
  ): void => {
    if (isEnableFields) {
      const noOfRound = bracketType == 'round_robin' ? 2 : 10;
      this.addNewControlAndUpdateValidity(
        'noOfTeamInGroup',
        'NUMBER',
        2,
        2,
        maxParticipants || this.basicInfo.value?.maxParticipants || 2,
        false,
        this.teamInGroupValidation('noOfTeamInGroup').bind(this)
      );
      this.addNewControlAndUpdateValidity(
        'noOfWinningTeamInGroup',
        'NUMBER',
        1,
        1,
        false,
        false,
        this.teamInGroupValidation('noOfWinningTeamInGroup').bind(this)
      );
      this.addNewControlAndUpdateValidity(
        'noOfRoundPerGroup',
        'NUMBER',
        1,
        1,
        noOfRound
      );
      this.addNewControlAndUpdateValidity('noOfStage', 'NUMBER', 1);
    } else {
      this.removeControlOrValidity('noOfTeamInGroup');
      this.removeControlOrValidity('noOfWinningTeamInGroup');
      this.removeControlOrValidity('noOfRoundPerGroup');
      this.removeControlOrValidity('noOfStage');
    }
  };

  noOfTeamsPerGroupHandler(value, bracketType) {
    if (bracketType === 'battle_royale') {
      this.addNewControlAndUpdateValidity(
        'noOfPlacement',
        'NUMBER',
        1,
        2,
        value || 2
      );
    }
  }

  // add controls
  addControls() {
    this.basicInfo.addControl(
      'name',
      new FormControl(
        '',
        Validators.compose([
          Validators.required,
          Validators.maxLength(100),
          this.emptyCheck,
        ])
      )
    );
    this.basicInfo.addControl('url', new FormControl(''));
    this.basicInfo.addControl(
      'gameDetail',
      new FormControl('', Validators.compose([Validators.required]))
    );

    this.basicInfo.addControl(
      'startDate',
      new FormControl(
        '',
        Validators.compose([
          Validators.required,
          this.ValidateStartDate.bind(this),
        ])
      )
    );
    this.basicInfo.addControl(
      'startTime',
      new FormControl(
        '',
        Validators.compose([
          Validators.required,
          this.ValidateStartTime.bind(this),
        ])
      )
    );
    this.basicInfo.addControl(
      'endDate',
      new FormControl(
        '',
        Validators.compose([
          Validators.required,
          this.ValidateEndDate.bind(this),
        ])
      )
    );

    this.basicInfo.addControl(
      'participantType',
      new FormControl('', Validators.compose([Validators.required]))
    );
    this.basicInfo.addControl(
      'teamFormat',
      new FormControl('', Validators.compose([]))
    );
    this.basicInfo.addControl('allowSubstituteMember', new FormControl(false));
    this.basicInfo.addControl('substituteMemberSize', new FormControl(''));

    this.basicInfo.addControl(
      'platform',
      new FormControl('', Validators.compose([Validators.required]))
    );
    this.basicInfo.addControl(
      'bracketType',
      new FormControl('', Validators.compose([Validators.required]))
    );

    this.basicInfo.addControl(
      'maxParticipants',
      new FormControl(
        2,
        Validators.compose([
          Validators.required,
          Validators.max(1000),
          Validators.min(2),
        ])
      )
    );

    this.basicInfo.addControl('isPaid', new FormControl(false));
    this.basicInfo.addControl('isCheckInRequired', new FormControl(false));
    this.basicInfo.addControl('banner', new FormControl(''));

    this.basicDetails.subscribe((basic) => {
      if (basic) {
        this.updateBasicDetails(basic);
      }
    });
  }

  updateBasicDetails(basic) {
    const getDateTime = (f1, f2, date) => {
      const d = new Date(date);
      const formatToTwoDigit = (number) =>
        number < 10 ? `0${number}` : number;

      return {
        [f1]: `${d.getFullYear()}-${formatToTwoDigit(
          d.getMonth() + 1
        )}-${formatToTwoDigit(d.getDate())}`,
        [f2]: {
          hour: d.getHours(),
          minute: d.getMinutes(),
          second: 0,
        },
      };
    };
    // get check-in time for tournament
    const setStartCheckedInDate = (startDate, checkInStartDate) => {
      const d1 = new Date(startDate).getTime();
      const d2 = new Date(checkInStartDate).getTime();
      const hour = Math.floor((d1 - d2) / (3600 * 1000));
      const minute = Math.floor((d1 - d2) / (60 * 1000)) - hour * 60;
      const value = ConstantsService.checkInTimeOptions.find((el) => {
        return el.value.minute == minute && el.value.hour == hour;
      });
      return value.name;
    };

    const getTeamFormatValues = (data) => {
      const n = data.teamSize.toString();
      if (['2', '4'].includes(n)) {
        this.enableteamCounter = false;
        return {
          teamFormat: n,
          teamSize: n,
        };
      } else {
        this.enableteamCounter = true;
        return {
          teamFormat: 'n',
          teamSize: n,
        };
      }
    };

    this.participantTypeHandler(basic?.participantType);
    this.paidRegistrationHandler(basic?.isPaid);
    this.checkInDateHandler(basic?.isCheckInRequired);
    this.killingPointHandler(basic?.isKillPointRequired);
    this.allowAdvanceStageHandler(basic?.allowAdvanceStage);

    if (basic?.bracketType) {
      if (['single', 'double'].includes(basic?.bracketType)) {
        this.bracketChangeHandler(basic?.bracketType);
      } else {
        this.bracketChangeHandler(
          basic?.bracketType,
          basic?.noOfPlacement,
          basic?.noOfTeamInGroup,
          basic?.maxParticipants
        );
      }
    }

    this.basicInfo.patchValue({
      ...basic,
      ...(basic.startDate &&
        getDateTime('startDate', 'startTime', basic.startDate)),
      ...(basic.endDate && getDateTime('endDate', 'endTime', basic.endDate)),
      ...(basic.isCheckInRequired && {
        checkInStartDate: setStartCheckedInDate(
          basic?.startDate,
          basic?.checkInStartDate
        ),
      }),
      ...(basic?.participantType == 'team' && getTeamFormatValues(basic)),
      gameDetail: basic.gameDetail?._id || basic?.gameDetail,
    });
    this.getGames();
  }

  setMinDate() {
    const date = new Date();
    const year: any = date.getFullYear();
    let month: any = date.getMonth() + 1;
    month = month < 10 ? '0' + month : month;
    let day: any = date.getDate();
    day = day < 10 ? '0' + day : day;
    const d = `${year}-${month}-${day}`;
    return d;
  }

  getGames = async (enableToaster = false) => {
    try {
      if (this.utilsService.getGame().length) {
        this.gameArray = this.utilsService.getGame();
        if (this.basicInfo.value.gameDetail) {
          const gameIndex = this.gameArray.findIndex(
            (item) =>
              item._id ==
              (this.basicInfo?.value?.gameDetail?._id ||
                this.basicInfo?.value?.gameDetail)
          );
          this.setGameOnSlide(gameIndex, true, enableToaster);
        } else {
          this.setGameOnSlide(
            Math.floor(this.gameArray.length / 2),
            false,
            enableToaster
          );
        }
      }
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  setGameOnSlide(index, updateSlider, enableToast = true) {
    this.selectedGameIndex = index;
    this.basicInfo.get('gameDetail').setValue(this.gameArray[index]?._id);
    this.bracketList = ConstantsService.bracketType.filter(
      (item) => this.gameArray[index].bracketTypes[item.value]
    );
    this.platform = this.gameArray[index]?.platform;

    if (!this.basicInfo?.value?._id && updateSlider) {
      this.basicInfo.get('bracketType').setValue('');
      this.basicInfo.get('platform').setValue('');

      this.basicInfo.get('bracketType').updateValueAndValidity();
      this.basicInfo.get('platform').updateValueAndValidity();
    }

    // if (enableToast) {
    //   this.toastService.showSuccess(
    //     `<h6>${this.gameArray[index]['name']} ${this.translateService.instant(
    //       'TOURNAMENT.SELECTED'
    //     )}</h6>`
    //   );
    // }
  }

  ngOnChanges(simpleChanges: SimpleChanges) {
    if (
      simpleChanges.hasOwnProperty('isAllDataLoaded') &&
      this.isAllDataLoaded
    ) {
      this.getGames();
    }
  }
}
