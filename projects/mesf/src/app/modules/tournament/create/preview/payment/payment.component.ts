import {
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';
import { TransactionService, ToastService } from '../../../../../core/service';
import { Router } from '@angular/router';
import { IUser } from '.././../../../../shared/models';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss'],
})
export class PaymentComponent implements OnInit, OnChanges {
  @Input() tournament;
  selectPaymentType = '';
  transaction;
  isProccesing = true;
  user: IUser;
  paymentMethods = [];
  prizePools = [];
  totalAmounts = [];
  isPaymentLoaded = false;

  constructor(
    private transactionService: TransactionService,
    private toastService: ToastService,
    private router: Router
  ) {}

  ngOnInit(): void {}

  ngOnChanges(simpleChanges: SimpleChanges) {
    if (simpleChanges.hasOwnProperty('tournament') && this.tournament) {
      if (!this.isPaymentLoaded) {
        this.fetchtransaction();
      }
    }
  }

  fetchtransaction = async () => {
    try {
      this.isPaymentLoaded = true;
      const payload = {
        tournamentId: this.tournament?._id,
        type: 'organizer',
      };

      const { data } = await this.transactionService.createOrUpdateTransaction(
        payload
      );

      this.totalAmounts.push({
        prizeLabel: 'PAYMENT.PRIZE_AMOUNT',
        amount: data.totalAmount - data.platformFee,
      });
      this.totalAmounts.push({
        prizeLabel: 'PAYMENT.TOTAL_AMOUNT',
        amount: data.totalAmount,
      });

      this.prizePools = data?.prizeList?.map((item) => {
        let img;

        if (item.name == '1st') {
          img = '../assets/images/payment/Gold.svg';
        } else if (item.name == '2nd') {
          img = '../assets/images/payment/Silver.svg';
        } else if (item.name == '3rd') {
          img = '../assets/images/payment/Bronze.svg';
        } else {
          img = '../assets/images/payment/Bronze.svg';
        }

        return {
          ...item,
          img,
        };
      });

      this.transaction = data;
      if (this.transaction.provider == 'paypal') {
        this.paymentMethods = [
          {
            img: '../assets/images/payment/paypal.png',
            name: 'Paypal',
            value: 'paypal',
          },
        ];
      }
      this.isProccesing = false;
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
      this.isProccesing = false;
    }
  };

  getPaymentResponse = async (order) => {
    try {
      this.isProccesing = true;
      const payload = {
        tournamentId: this.tournament?._id,
        transactionId: this.transaction?._id,
        txnId: order?.id,
        provider: 'paypal',
      };

      const response = await this.transactionService.createOrUpdateTransaction(
        payload
      );
      this.toastService.showSuccess(response?.message);
      this.router.navigate(['/profile/my-tournament/created']);
      this.isProccesing = false;
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
      this.isProccesing = false;
    }
  };
}
