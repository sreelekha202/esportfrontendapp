import {
  AfterViewChecked,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';

declare var paypal;

@Component({
  selector: 'app-paypal',
  templateUrl: './paypal.component.html',
  styleUrls: ['./paypal.component.scss'],
})
export class PaypalComponent implements OnInit, AfterViewChecked {
  @ViewChild('paypal', { static: true }) paypalElement: ElementRef;
  @Input() amount;
  @Input() currencyCode;
  @Output() paypalResponse = new EventEmitter();

  constructor() {}

  ngOnInit(): void {
    paypal
      .Buttons({
        createOrder: (data, actions) => {
          return actions.order.create({
            purchase_units: [
              {
                amount: {
                  value: this.amount,
                  currency_code: this.currencyCode,
                },
              },
            ],
          });
        },
        onApprove: async (data, actions) => {
          const order = await actions.order.capture();
          this.paypalResponse.emit(order);
        },
        onError: (error) => {
        },
      })
      .render(this.paypalElement.nativeElement);
  }

  ngAfterViewChecked(): void {}
}
