import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss'],
})
export class PaymentComponent implements OnInit {
  @Input() type;
  @Input() transaction;

  @Output() valueEmitAfterTransaction = new EventEmitter();

  constructor() {}

  ngOnInit(): void {}

  paypalResponse(data) {
    this.valueEmitAfterTransaction.emit(data);
  }
}
