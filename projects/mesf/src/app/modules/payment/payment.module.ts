import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PaymentComponent } from './payment.component';
import { PaypalComponent } from './paypal/paypal.component';
import { SharedModule } from '../../shared/modules/shared.module';

@NgModule({
  declarations: [PaymentComponent, PaypalComponent],
  imports: [CommonModule, SharedModule],
  exports: [PaymentComponent],
})
export class PaymentModule {}
