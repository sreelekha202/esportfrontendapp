import { Component, OnInit } from '@angular/core';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';

@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.scss'],
})
export class AboutUsComponent implements OnInit {
  public currentLang: string;
  team = [
    {
      photo: '/assets/images/AboutUs/Mr. Afiq Fadhli Bin Narawi.png',
      name: 'Mr. Afiq Fadhli Bin Narawi',
      position: 'ABOUTUS.TITLE2',
    },
    {
      photo: '/assets/images/AboutUs/Mr. Muhd. Naim Al Amin Bin Saharuddin.png',
      name: 'Mr. Muhd. Naim Al Amin Bin Saharuddin',
      position: 'ABOUTUS.TITLE3_1',
    },
    {
      photo: '/assets/images/AboutUs/Mr. Jeff Ooi Boon Seang.png',
      name: 'Mr. Jeff Ooi Boon Seang',
      position: 'ABOUTUS.TITLE3_2',
    },
    {
      photo: '/assets/images/AboutUs/Ms. Tiara Natassah Ching.png',
      name: 'Ms. Tiara Natassah Ching',
      position: 'ABOUTUS.TITLE3_3',
    },
    {
      photo: '/assets/images/AboutUs/Mr. Mohamad Khir Bin Md. Noor.png',
      name: 'Mr. Mohamad Khir Bin Md. Noor',
      position: 'ABOUTUS.TITLE3_4',
    },
    {
      photo: '/assets/images/AboutUs/Mr. Muhammad Farouq Bin Abdul Patah.png',
      name: 'Mr. Muhammad Farouq Bin Abdul Patah',
      position: 'ABOUTUS.TITLE4',
    },
    {
      photo: '/assets/images/AboutUs/Mr. Mohammad Nazri Bin Abdul Latiff.png',
      name: 'Mr. Mohammad Nazri Bin Abdul Latiff',
      position: 'ABOUTUS.TITLE5',
    },
    {
      photo: '/assets/images/AboutUs/Mr. Latt Omar Bin Dato Latt Shariman.png',
      name: 'Mr. Latt Omar Bin Dato Latt Shariman',
      position: 'ABOUTUS.TITLE6',
    },
    {
      photo: '/assets/images/AboutUs/Ms. Janice Loo Hui Yi.png',
      name: 'Ms. Janice Loo Hui Yi',
      position: 'ABOUTUS.TITLE7',
    },
  ];
  chairpersons = [
    {
      photo: '/assets/images/AboutUs/Ms. Tiara Natassah Ching.png',
      name: 'Ms. Tiara Natassah Ching',
      position: 'ABOUTUS.TITLE12',
    },
    {
      photo: '/assets/images/AboutUs/Ms. Janice Loo Hui Yi.png',
      name: 'Ms. Janice Loo Hui Yi',
      position: 'ABOUTUS.TITLE13',
    },
    {
      photo: '/assets/images/AboutUs/Mr. Afiq Fadhli Bin Narawi.png',
      name: 'Mr. Afiq Fadhli Bin Narawi',
      position: 'ABOUTUS.TITLE14',
    },
    {
      photo: '/assets/images/AboutUs/userbg1.png',
      name: '',
      position: 'ABOUTUS.TITLE15',
    },
    {
      photo: '/assets/images/AboutUs/userbg2.png',
      name: '',
      position: 'ABOUTUS.TITLE16',
    },
    {
      photo: '/assets/images/AboutUs/Mr Karnan.png',
      name: 'Mr Karnan',
      position: 'ABOUTUS.TITLE17',
    },
    {
      photo: '/assets/images/AboutUs/userbg1.png',
      name: '',
      position: 'ABOUTUS.TITLE18',
    },
    {
      photo: '/assets/images/AboutUs/Ms. Tiara Natassah Ching.png',
      name: 'Ms. Tiara Natassah Ching',
      position: 'ABOUTUS.TITLE19',
    },
  ];

  constructor(public translate: TranslateService) {
    this.currentLang = translate.currentLang;

    translate.onLangChange.subscribe((event: LangChangeEvent) => {
      this.currentLang = event.lang;
    });
  }

  ngOnInit(): void {}
}
