import { SharedModule } from '../../shared/modules/shared.module';
import { VideosRoutingModule } from './videos-routing.module';
import { CoreModule } from '../../core/core.module';
import { NgModule } from '@angular/core';

import { CreateVideoLibraryComponent } from './create-video-library/create-video-library.component';
import { VideoViewComponent } from './video-view/video-view.component';
import { VideosComponent } from './videos.component';

import { FormComponentModule } from '../../shared/components/form-component/form-component.module';

@NgModule({
  declarations: [
    VideosComponent,
    VideoViewComponent,
    CreateVideoLibraryComponent,
  ],
  imports: [CoreModule, VideosRoutingModule, SharedModule, FormComponentModule],
  providers: [],
})
export class VideosModule {}
