import { TranslateService } from '@ngx-translate/core';
import { Component, OnInit } from '@angular/core';
import { HomeService } from '../../../core/service';
import { VideoItemComponentData } from '../../../core/video-item/video-item.component';

@Component({
  selector: 'app-search-video',
  templateUrl: './search-video.component.html',
  styleUrls: ['./search-video.component.scss'],
})
export class SearchVideoComponent implements OnInit {
  videos: VideoItemComponentData[] = [];

  constructor(
    public translate: TranslateService,
    public homeService: HomeService
  ) {}

  ngOnInit(): void {
    this.getVideo();
  }

  getVideo() {
    this.homeService.searchedVideo.subscribe((data) => {
      this.videos = data;
    });
  }
}

/**
 * {
      id: Math.round(Math.random() * 100),
      img: './assets/images/Home/trending1.png',
      title: 'drift masters',
      text:
        'The Ups and Downs of Being An Assassin in Mobile Legends that You May Relate',
      date: '6 june',
    },
    {
      id: Math.round(Math.random() * 100),
      img: './assets/images/Home/trending2.png',
      title: 'second life',
      text:
        'The Ups and Downs of Being An Assassin in Mobile Legends that You May Relate',
      date: '6 june',
    },
    {
      id: Math.round(Math.random() * 100),
      img: './assets/images/Home/trending3.png',
      title: 'second life',
      text:
        'The Ups and Downs of Being An Assassin in Mobile Legends that You May Relate',
      date: 'date',
    },
    {
      id: Math.round(Math.random() * 100),
      img: './assets/images/Home/trending4.png',
      title: 'jdm monsters',
      text:
        'The Ups and Downs of Being An Assassin in Mobile Legends that You May Relate',
      date: '6 june',
    },
 */
