import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AppHtmlRoutes } from '../../app-routing.model';
import { filter, map } from 'rxjs/operators';
import { HomeService } from '../../core/service';

@Component({
  selector: 'app-home',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
})
export class SearchComponent implements OnInit, OnDestroy {
  filter = {
    text: '',
    sort: 'Latest',
    category: 'online',
    game: 'PUBG',
    platform: 'Mobile',
    region: 'Johor',
  };
  // filtersData = {
  //   sort: ['Latest', 'Oldest'],
  //   category: ['online', 'offline', 'hybrid'],
  //   game: ['PUBG', 'FIFA', 'Dota', 'Call of Duty'],
  //   platform: ['Mobile', 'PC'],
  //   region: [
  //     'Johor',
  //     'Kedah',
  //     'Kelantan',
  //     'Melaka',
  //     'Negeri Sembilan',
  //     'Pahang',
  //     'Pulau Pinang',
  //     'Perak',
  //     'Perlis',
  //     'Sabah',
  //     'Sarawak',
  //     'Selangor',
  //     'Terengganu',
  //     'Kuala Lumpur',
  //     'Labuan',
  //     'Putrajaya',
  //   ],
  // };
  headerSearch: FormGroup;
  isSearchPage = true;

  sub: Subscription;

  constructor(
    public translate: TranslateService,
    public router: Router,
    public activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private homeService: HomeService
  ) {
    this.getRouterData();
  }

  ngOnInit(): void {
    this.headerSearch = this.formBuilder.group({
      text: [
        '',
        Validators.compose([
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(100),
        ]),
      ],
    });
    this.homeService.searchTournament();
    this.homeService.searchArticle();
    this.homeService.searchVideo();
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
    this.homeService.searchedTournamentSubject.next([]);
    this.homeService.searchedArticleSubject.next([]);
    this.homeService.searchedVideoSubject.next([]);
  }

  onParamsUpdate(): void {
    this.router.navigate([], {
      relativeTo: this.activatedRoute,
      queryParams: this.filter,
      queryParamsHandling: 'merge', // remove to replace all query params by provided
    });
    this.updateSearchParams();
  }

  private getRouterData(): void {
    this.sub = this.router.events
      .pipe(
        filter((event) => event instanceof NavigationEnd),
        map((event: NavigationEnd) => event.url)
      )
      .subscribe((url: string) => {
        let currentRouteUrl;
        if (url && url.match(/.+?(?=\?)/)) {
          currentRouteUrl = url.match(/.+?(?=\?)/)[0];
          this.isSearchPage = AppHtmlRoutes.search === currentRouteUrl;
        } else {
          this.isSearchPage = true;
        }
      });
  }

  /**
   * Function will fetch new data based on filter.
   * @param value
   */
  onSearchSubmit(value) {
    this.filter.text = value.text;
    this.updateSearchParams();
    this.homeService.searchTournament();
    this.homeService.searchArticle();
    this.homeService.searchVideo();
  }

  updateSearchParams() {
    this.homeService.updateSearchParams(this.filter);
  }
}
