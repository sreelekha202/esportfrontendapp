import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { SearchTournamentsComponent } from './search-tournaments/search-tournaments.component';
import { SearchArticleComponent } from './search-article/search-article.component';
import { SearchVideoComponent } from './search-video/search-video.component';
import { SearchShopComponent } from './search-shop/search-shop.component';
import { SearchAllComponent } from './search-all/search-all.component';
import { SearchComponent } from './search.component';

const routes: Routes = [
  {
    path: '',
    component: SearchComponent,
    children: [
      { path: '', component: SearchAllComponent },
      { path: 'tournament', component: SearchTournamentsComponent },
      { path: 'article', component: SearchArticleComponent },
      { path: 'video', component: SearchVideoComponent },
      // { path: 'shop', component: SearchShopComponent },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SearchRoutingModule {}
