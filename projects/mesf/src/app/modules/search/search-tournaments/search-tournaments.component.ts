import { TranslateService } from '@ngx-translate/core';
import { Component, OnInit } from '@angular/core';

import { TournamentItemComponentData } from '../../../core/tournament-item/tournament-item.component';
import { HomeService } from '../../../core/service';

enum OngoingTournamentFilter {
  upcoming,
  ongoing,
  completed,
  all,
}

@Component({
  selector: 'app-search-tournaments',
  templateUrl: './search-tournaments.component.html',
  styleUrls: ['./search-tournaments.component.scss'],
})
export class SearchTournamentsComponent implements OnInit {
  OngoingTournamentFilter = OngoingTournamentFilter;
  tournamentFilter: OngoingTournamentFilter = OngoingTournamentFilter.all;
  ongoingTournaments: TournamentItemComponentData[] = [];

  constructor(
    public homeService: HomeService,
    public translate: TranslateService
  ) {}

  ngOnInit(): void {
    //this.homeService.searchTournament();
    this.getTournament();
  }

  getTournament() {
    this.homeService.searchedTournament.subscribe((data: any) => {
      this.ongoingTournaments = data.docs || [];
    });
  }

  /**
   * Function will update filter value to search service.
   */
  handleTournamentStatusChange() {
    this.homeService.updateTournamentStatusFilter(this.tournamentFilter);
  }
}
