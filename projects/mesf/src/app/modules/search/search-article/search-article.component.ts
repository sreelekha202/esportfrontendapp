import { TranslateService } from '@ngx-translate/core';
import { Component, OnInit } from '@angular/core';
import { HomeService } from '../../../core/service';

@Component({
  selector: 'app-search-article',
  templateUrl: './search-article.component.html',
  styleUrls: ['./search-article.component.scss'],
})
export class SearchArticleComponent implements OnInit {
  trendingPosts = [];

  constructor(
    public translate: TranslateService,
    public homeService: HomeService
  ) {}

  ngOnInit(): void {
    this.getArticle();
  }

  /**
   *
   */
  getArticle() {
    this.homeService.searchedArticle.subscribe((data) => {
      this.trendingPosts = data;
    });
  }
}
