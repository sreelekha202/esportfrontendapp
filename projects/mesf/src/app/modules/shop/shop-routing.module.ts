import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ShopComponent } from './shop.component';
import { GameVoucherComponent } from './game-voucher/game-voucher.component';
import { GameOrderComponent } from './game-order/game-order.component';

const routes: Routes = [
  { path: '', component: ShopComponent },
  { path: 'product/:product', component: GameVoucherComponent },
  { path: 'order', component: GameOrderComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ShopRoutingModule {}
