import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VoucherPayComponent } from './voucher-pay.component';

describe('VoucherPayComponent', () => {
  let component: VoucherPayComponent;
  let fixture: ComponentFixture<VoucherPayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [VoucherPayComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VoucherPayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
