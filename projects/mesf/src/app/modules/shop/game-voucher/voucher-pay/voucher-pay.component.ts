import { Component, OnInit, Input } from '@angular/core';
import { AppHtmlRoutes } from '../../../../app-routing.model';
import { ActivatedRoute } from '@angular/router';
import {
  CodaShopService,
  ToastService,
} from '../../../../core/service';
import { MatDialog } from '@angular/material/dialog';
import { PaymentPopupComponent } from '../../payment-popup/payment-popup.component';

@Component({
  selector: 'app-voucher-pay',
  templateUrl: './voucher-pay.component.html',
  styleUrls: ['./voucher-pay.component.scss'],
})
export class VoucherPayComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;
  @Input() productInfo: any;
  @Input() productType;
  productName = '';
  productList = [];
  selectedProduct = {
    description: '',
    price: {
      currency: '',
      amount: '',
    },
    sku: '',
  };
  //model value for input 13482_22001
  userAccount = '';
  userAccountZone = '';
  //flag to show loading mask
  validatingUserDetail = false;
  //flag whether user is validated or not
  userValidated = false;
  //flag for transaction initiated
  initiatingTransaction = false;
  //user detail after validation
  userValidateResult = {
    message: {
      username: '',
      userAccount: '',
    },
    orderId: '',
    sku: {
      description: '',
    },
  };

  /**
   *
   * @param product
   * every product selection make user validation invalid.
   */
  onToggleAmountCard(product) {
    this.selectedProduct = product;
    this.userValidated = false;
    this.productList.findIndex((x) => {
      if (product.sku === x.sku) {
        x.selected = true;
      } else {
        x.selected = false;
      }
    });
    // this.TEST_garenaShells.forEach((item, i) => {
    //   item.selected = idx === i;
    // });
  }

  // onTogglePayMethod(idx) {
  //   this.TEST_payment.forEach((item, i) => {
  //     item.selected = idx === i;
  //   });
  // }

  constructor(
    private activatedRoute: ActivatedRoute,
    private codaShopService: CodaShopService,
    public toastService: ToastService,
    public dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.productName = this.activatedRoute.snapshot.params['product'];
    this._getProductDetail();
  }

  _getProductDetail() {
    if (!this.productName || this.productName.length == 0) {
      return;
    }
    this.codaShopService.getProductDetail(this.productName).subscribe(
      (product) => {
        this.productList = product?.data?.result?.skuList || [];
      },
      (err) => {}
    );
  }

  /**
   * triggered from UI, validate use detail against user entered UserId
   * and selected product. this function will fetch user detail for reconfirmation.
   * return if no product is selected.
   */
  validatePaymentDetail() {
    if (!this.userAccount || this.userAccount.length == 0) {
      this.toastService.showError('Please enter user detail');
      return;
    }
    if (
      this.productInfo.zoneId &&
      (!this.userAccountZone || this.userAccountZone.length == 0)
    ) {
      this.toastService.showError('Please enter user zone Id');
      return;
    }
    if (
      !this.selectedProduct ||
      !this.selectedProduct.sku ||
      this.selectedProduct.sku.length == 0
    ) {
      this.toastService.showError(
        'No Product selected. Please select a product fro the list to proceed'
      );
      return;
    }
    let userAccountInput = this.userAccount;
    if (this.productInfo.zoneId) {
      userAccountInput = userAccountInput + '_' + this.userAccountZone;
    }
    this.validatingUserDetail = true;
    let dataToValidate = {
      userAccount: userAccountInput,
      selectedProduct: this.selectedProduct,
      productName: this.productName,
    };
    this.codaShopService.validateUserDetail(dataToValidate).subscribe(
      (response: any) => {
        if (response && response.data && response.data.error) {
          let errorMessage =
            response.data.error.message || 'User ineligible for SKU';
          this.toastService.showError(errorMessage);
        } else {
          this.validatingUserDetail = false;
          this.userValidated = response?.data?.result ? true : false;
          this.userValidateResult = response?.data?.result || {};
        }
      },
      (err) => {
        this.validatingUserDetail = false;
      }
    );
  }

  /**
   * Initiate Transaction
   * this function will get txnId and open iframe for boost login
   */
  initiateTransaction() {
    this._getCodaTransactionCode();
  }

  _getCodaTransactionCode() {
    this.initiatingTransaction = true;
    let transactionDetail = this.userValidateResult;
    transactionDetail['selectedProduct'] = this.selectedProduct;
    transactionDetail['productType'] = 'TOPUP';
    transactionDetail['productName'] = this.productName;

    this.codaShopService.getCodaTransactionCode(transactionDetail).subscribe(
      (response: any) => {
        this.initiatingTransaction = false;
        const dialogRef = this.dialog.open(PaymentPopupComponent, {
          data: response?.data?.initResult?.txnId,
        });
        dialogRef.afterClosed().subscribe(() => {});
      },
      (err) => {
        this.initiatingTransaction = false;
      }
    );
  }

  initiateVoucherTransaction() {
    this._getCodaVoucherTransactionCode();
  }

  _getCodaVoucherTransactionCode() {
    this.initiatingTransaction = true;
    let transactionDetail = this.userValidateResult;
    transactionDetail.message.userAccount = this.userAccount;
    transactionDetail.message.username = this.userAccount;
    transactionDetail.orderId = new Date().getTime().toString();
    transactionDetail['selectedProduct'] = this.selectedProduct;
    transactionDetail['productType'] = 'VCOUCHER';
    transactionDetail['productName'] = this.productName;
    this.codaShopService.getCodaTransactionCode(transactionDetail).subscribe(
      (response: any) => {
        this.initiatingTransaction = false;
        const dialogRef = this.dialog.open(PaymentPopupComponent, {
          data: response?.data?.initResult?.txnId,
        });
        dialogRef.afterClosed().subscribe(() => {});
      },
      (err) => {
        this.initiatingTransaction = false;
      }
    );
  }

  initiatePlaceOrder() {
    this._getCodaPlaceOrder();
  }

  _getCodaPlaceOrder() {
    this.initiatingTransaction = true;
    let transactionDetail = this.userValidateResult;
    this.selectedProduct['quantity'] = 1;
    transactionDetail['selectedProduct'] = this.selectedProduct;
    /**
     * {
    "productName": "steam-voucher",
    "items": [{
                "sku": "STEAMMY0020",
                "quantity": 1,
                "price": {
                    "amount": 25,
                    "currency": "MYR"
                }
            }]
    }
     */
    let placeOrderQuery = {
      productName: this.productName,
      items: [transactionDetail['selectedProduct']],
    };
    this.codaShopService.voucherPlaceOrder(placeOrderQuery).subscribe(
      (response: any) => {
        this.initiatingTransaction = false;
        const dialogRef = this.dialog.open(PaymentPopupComponent, {
          data: response?.data?.initResult?.txnId,
        });
        dialogRef.afterClosed().subscribe(() => {});
      },
      (err) => {
        this.initiatingTransaction = false;
      }
    );
  }
}
