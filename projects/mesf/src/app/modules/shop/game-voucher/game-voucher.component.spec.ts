import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GameVoucherComponent } from './game-voucher.component';

describe('GameVoucherComponent', () => {
  let component: GameVoucherComponent;
  let fixture: ComponentFixture<GameVoucherComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GameVoucherComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GameVoucherComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
