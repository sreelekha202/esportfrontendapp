import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AppHtmlRoutes } from '../../../app-routing.model';
import { CodaShopService } from '../../../core/service';

@Component({
  selector: 'app-game-voucher',
  templateUrl: './game-voucher.component.html',
  styleUrls: ['./game-voucher.component.scss'],
})
export class GameVoucherComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;
  //selected product name
  productName = '';
  productType = '';
  productInfo = {
    name: '',
    productName: '',
    shortDesc: '',
    longDesc: '',
    banner: '',
    serverList: false,
    zoneId: false,
  };

  constructor(
    private activatedRoute: ActivatedRoute,
    private codaShopService: CodaShopService
  ) {}

  ngOnInit(): void {
    this.productName = this.activatedRoute.snapshot.params['product'];
    this._getProductInfo();
  }

  /**
   * Fetch product information
   */
  _getProductInfo() {
    if (!this.productName || this.productName.length == 0) {
      return;
    }
    this.codaShopService.getProductInfo(this.productName).subscribe(
      (product) => {
        this.productInfo = product?.data[0] || [];
        this.productType = product?.data[0]?.type || 'TOPUP';
      },
      (err) => {}
    );
  }
}
