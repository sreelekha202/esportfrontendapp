import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GameVoucherSliderComponent } from './games-voucher-slider.component';

describe('VoucherSliderComponent', () => {
  let component: GameVoucherSliderComponent;
  let fixture: ComponentFixture<GameVoucherSliderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GameVoucherSliderComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GameVoucherSliderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
