import { Router } from '@angular/router';
import { SwiperConfigInterface } from 'ngx-swiper-wrapper';
import { Component, OnInit, Input } from '@angular/core';

export interface GameVoucherSlide {
  isSelected: boolean;
  logo: string;
  img: string;
  title: string;
  list: string[];
}

@Component({
  selector: 'app-games-voucher-slider',
  templateUrl: './games-voucher-slider.component.html',
  styleUrls: ['./games-voucher-slider.component.scss'],
})
export class GamesVoucherSliderComponent implements OnInit {
  swiperConfig: SwiperConfigInterface = {
    width: 187,
  };

  swiperIndex = 0;

  slider: Array<GameVoucherSlide> = [
    {
      isSelected: true,
      logo: '/assets/images/Shop/ff.png',
      img: '/assets/images/Shop/footer.png',
      title:
        'How to convert Garena shells to Free Fire diamonds after purchase?',
      list: [
        'Converting Garena Shells to Free Fire diamonds is easy! Simply follow the steps below, and the diamonds will be added to your account in seconds.',
        'Proceed to https://shop.garena.com/ and login as shown in the image below.',
        'Select “Free Fire” on the game options.',
        'Choose Garena PPC and enter your voucher code’s information.',
        'Click “Confirm” to complete the conversion.',
      ],
    },
    {
      isSelected: false,
      logo: '/assets/images/Shop/la.png',
      img: '/assets/images/Shop/footer.png',
      title: 'LEAGUE LEGENDS',
      list: [
        'Converting Garena Shells to LEAGUE LEGENDS diamonds is easy! Simply follow the steps below',
        'Proceed to https://shop.garena.com/ and login as shown in the image below.',
        'Select “LEAGUE LEGENDS” on the game options.',
        'Choose Garena PPC and enter your voucher code’s information.',
        'Click “Confirm” to complete the conversion.',
      ],
    },
    {
      isSelected: false,
      logo: '/assets/images/Shop/ml.png',
      img: '/assets/images/Shop/footer.png',
      title: 'CALL OF DUTY',
      list: [
        'Converting Garena Shells to CALL OF DUTY diamonds is easy!',
        'Proceed to https://shop.garena.com/ and login as shown in the image below.',
        'Select “CALL OF DUTY” on the game options.',
        'Choose Garena PPC and enter your voucher code’s information.',
        'Click “Confirm” to complete the conversion.',
      ],
    },
    {
      isSelected: false,
      logo: '/assets/images/Shop/pg.png',
      img: '/assets/images/Shop/footer.png',
      title: 'FIFA20',
      list: [
        'Converting Garena Shells to FIFA20 diamonds is easy!',
        'Proceed to https://shop.garena.com/ and login as shown in the image below.',
        'Select “FIFA20” on the game options.',
        'Choose Garena PPC and enter your voucher code’s information.',
        'Click “Confirm” to complete the conversion.',
      ],
    },
    {
      isSelected: false,
      logo: '/assets/images/Shop/Garena.png',
      img: '/assets/images/Shop/footer.png',
      title: 'Pubg',
      list: [
        'Converting Garena Shells to PUBG diamonds is easy! Simply follow the steps below, and the diamonds will be added to your account in seconds.',
        'Proceed to https://shop.garena.com/ and login as shown in the image below.',
        'Select “PUBG” on the game options.',
        'Choose Garena PPC and enter your voucher code’s information.',
        'Click “Confirm” to complete the conversion.',
      ],
    },
    {
      isSelected: false,
      logo: '/assets/images/AboutUs/col3.png',
      img: '/assets/images/Shop/footer.png',
      title: 'BUY STEAM WALLET CODES',
      list: [
        'Converting Garena Shells to WALLET CODES diamonds is easy!',
        'Proceed to https://shop.garena.com/ and login as shown in the image below.',
        'Select “CODES” on the game options.',
        'Choose Garena PPC and enter your voucher code’s information.',
        'Click “Confirm” to complete the conversion.',
      ],
    },
  ];

  activeSlide: GameVoucherSlide;

  constructor(private router: Router) {
    this.activeSlide = this.slider[this.swiperIndex];
  }

  ngOnInit(): void {}

  onSetMainSlide(selectedSlide: GameVoucherSlide, index: number): void {
    this.swiperIndex = index;
    this.router.navigate(['/shop']);

    this.slider.forEach((item) => (item.isSelected = false));

    selectedSlide.isSelected = true;
    this.activeSlide = selectedSlide;
  }
}
