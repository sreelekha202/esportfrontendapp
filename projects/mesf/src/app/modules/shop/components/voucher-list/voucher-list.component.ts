import { Component, OnInit, Input } from '@angular/core';

import { AppHtmlRoutes } from '../../../../app-routing.model';

export interface Voucher {
  logo: string;
  banner: string;
  name: string;
  shortDesc: string;
  longDesc: string;
  productName: string;
}

@Component({
  selector: 'app-voucher-list',
  templateUrl: './voucher-list.component.html',
  styleUrls: ['./voucher-list.component.scss'],
})
export class VoucherListComponent implements OnInit {
  @Input() vouchers: Array<Voucher> = [];

  AppHtmlRoutes = AppHtmlRoutes;

  constructor() {}

  ngOnInit(): void {}
}
