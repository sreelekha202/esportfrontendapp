import { Component, OnInit } from '@angular/core';
import { Voucher } from './components/voucher-list/voucher-list.component';
import { TransactionService } from '../../core/service';

enum TopOffersFilter {
  TOPUP,
  VOUCHER,
}

@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.scss'],
})
export class ShopComponent implements OnInit {
  TopOffersFilter = TopOffersFilter;
  topOffersFilter = TopOffersFilter.TOPUP;
  topupData: Array<Voucher> = [];
  vouchersData: Array<Voucher> = [];
  shopBanner = 'assets/images/Shop/Voucher_Store.jpg';

  constructor(private transactionService: TransactionService) {}

  ngOnInit(): void {
    this._getTopup();
    this._getVoucher();
  }

  /**
   * fetch top-up list
   */
  _getTopup() {
    let productType = TopOffersFilter[this.topOffersFilter] || 'TOPUP';
    this.transactionService.getProduct(productType).subscribe(
      (data) => {
        if (data && data.data) {
          this.topupData = data.data || [];
        }
      },
      (err) => {}
    );
  }

  /**
   * fetch voucher list
   */
  _getVoucher() {
    let productType = 'VOUCHER';
    this.transactionService.getProduct(productType).subscribe(
      (data) => {
        if (data && data.data) {
          this.vouchersData = data.data || [];
        }
      },
      (err) => {}
    );
  }
}
