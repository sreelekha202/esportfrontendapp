import { Component, OnInit, Input } from '@angular/core';
import { AppHtmlRoutes } from '../../../../app-routing.model';

@Component({
  selector: 'app-order-info',
  templateUrl: './order-info.component.html',
  styleUrls: ['./order-info.component.scss'],
})
export class OrderInfoComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;
  @Input() transaction;
  @Input() voucherTxnDetail;

  // TEST_data = {
  //   transactionId: '60022644199320846482',
  //   orderId: '124e-2644ce0-be1es19932a',
  //   quantity: '14 Diamonds',
  //   price: '20',
  // };

  constructor() {}

  ngOnInit(): void {}
}
