import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GameOrderComponent } from './game-order.component';

describe('GameOrderComponent', () => {
  let component: GameOrderComponent;
  let fixture: ComponentFixture<GameOrderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GameOrderComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GameOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
