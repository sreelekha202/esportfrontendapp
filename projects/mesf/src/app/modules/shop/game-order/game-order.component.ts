import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TransactionService } from '../../../core/service';
import { CodaShopService } from '../../../core/service';

@Component({
  selector: 'app-game-order',
  templateUrl: './game-order.component.html',
  styleUrls: ['./game-order.component.scss'],
})
export class GameOrderComponent implements OnInit {
  TxnId = '';
  OrderId = '';
  isTxnFailed = false;
  transactionDetail = {
    items: [],
    orderId: '',
    txnId: '',
  };
  voucherTxnDetail: any = {};
  paymentStatus = -999;

  constructor(
    private activatedRoute: ActivatedRoute,
    private transactionService: TransactionService,
    private codaShopService: CodaShopService
  ) {}

  ngOnInit(): void {
    this.TxnId = this.activatedRoute.snapshot.queryParams['TxnId'];
    this.OrderId = this.activatedRoute.snapshot.queryParams['OrderId'];
    //this._getTransactionDetail();
    this._getTransactionDetail();
  }

  _getTransactionDetail() {
    let tQuery = {
      txnId: this.TxnId,
      orderId: this.OrderId,
    };
    this.transactionService.getTransactionDetail(tQuery).subscribe(
      (data) => {
        if (data && data.data) {
          this.isTxnFailed = true;
          this.transactionDetail = data.data;
          if (data.data.items[0].productType === 'TOPUP') {
            this._getTopupInquiryPaymentResult();
          } else if (data.data.items[0].productType === 'VCOUCHER') {
            this._placeVoucherOrder();
          }
        }
      },
      (err) => {}
    );
  }

  _placeVoucherOrder() {
    //let transactionDetail = this.userValidateResult;
    //this.selectedProduct['quantity'] = 1;
    //transactionDetail['selectedProduct'] = this.selectedProduct;
    /**
     * {
    "productName": "steam-voucher",
    "items": [{
                "sku": "STEAMMY0020",
                "quantity": 1,
                "price": {
                    "amount": 25,
                    "currency": "MYR"
                }
            }]
    }
     */
    let selectedProduct = this.transactionDetail.items[0].selectedProduct;
    let productName = this.transactionDetail.items[0].productName;
    let placeOrderQuery = {
      productName: productName,
      items: [
        {
          sku: selectedProduct.sku,
          quantity: 1,
          price: selectedProduct.price,
        },
      ],
    };
    this.codaShopService.voucherPlaceOrder(placeOrderQuery).subscribe(
      (response: any) => {
        if (
          response.data &&
          response.data.result &&
          response.data.result.items.length > 0
        ) {
          this.paymentStatus = 0;
          let voucherItems = response.data.result.items[0];
          this.voucherTxnDetail = voucherItems;
          this._updateTransactionStatus(this.TxnId, 0);
          this._sendVoucherToClient();
        }
        // this.initiatingTransaction = false;
        // const dialogRef = this.dialog.open(PaymentPopupComponent, {
        //   data: response?.data?.initResult?.txnId,
        // });
        // dialogRef.afterClosed().subscribe(() => {});
      },
      (err) => {
        //this.initiatingTransaction = false;
      }
    );
  }

  _sendVoucherToClient() {
    // this.transactionDetail
    // this.voucherTxnDetail
    let emailId = this.transactionDetail.items[0].message.userAccount;
    let code = this.voucherTxnDetail.codes;
    let emailData = {
      type: 'coda_shop',
      email: emailId,
      voucher: code,
    };
    this.transactionService.sendVoucherEmail(emailData).subscribe(
      (data) => {},
      (err) => {}
    );
  }

  _getTopupInquiryPaymentResult() {
    if (
      !this.TxnId ||
      !this.OrderId ||
      this.TxnId.length == 0 ||
      this.OrderId.length == 0
    ) {
      return;
    }
    let tQuery = {
      txnId: this.TxnId,
    };
    this.codaShopService.getInquiryPaymentResult(tQuery).subscribe(
      (data: any) => {
        if (data && data.data && data.data.paymentResult) {
          this.paymentStatus = data.data.paymentResult.resultCode;
          this._updateTransactionStatus(
            this.TxnId,
            data.data.paymentResult.resultCode
          );
        }
      },
      (err) => {}
    );
  }

  _updateTransactionStatus(txnId, resultCode) {
    let updateTransaction = {
      txnId: txnId,
      resultCode: resultCode,
    };
    this.transactionService
      .updateTransactionDetail(updateTransaction)
      .subscribe(
        (data: any) => {},
        (err) => {}
      );
  }
  /**
   * fetch transaction detail based on TxnId and OrderId
   */
  _getTransactionDetail_un_used() {
    if (
      !this.TxnId ||
      !this.OrderId ||
      this.TxnId.length == 0 ||
      this.OrderId.length == 0
    ) {
      return;
    }
    // let tQuery = {
    //   txnId: this.TxnId,
    //   orderId: this.OrderId,
    // };
    let tQuery = {
      txnId: this.TxnId,
    };
    /*this.transactionService.getTransactionDetail(tQuery).subscribe(
      (data) => {
        if (data && data.data) {
          this.isTxnFailed = true;
          this.failedTransactionDetail = data.data;
        }
      },
      (err) => {}
    );*/
    this.codaShopService.getInquiryPaymentResult(tQuery).subscribe(
      (data: any) => {
        if (data && data.data && data.data.paymentResult) {
          this.paymentStatus = data.data.paymentResult.resultCode;
        }
        if (
          data &&
          data.data &&
          data.data.transactionDetail &&
          data.data.transactionDetail.data
        ) {
          this.transactionDetail = data.data.transactionDetail.data;
        }
      },
      (err) => {}
    );
  }
}
