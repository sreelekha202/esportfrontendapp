import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'payment-popup',
  templateUrl: './payment-popup.component.html',
  styleUrls: ['./payment-popup.component.scss'],
})
export class PaymentPopupComponent implements OnInit {
  //codaUrlUnSafe: SafeResourceUrl = "";
  codaUrl;
  constructor(@Inject(MAT_DIALOG_DATA) public data) {}

  ngOnInit(): void {
    this.codaUrl =
      'https://airtime.codapayments.com/airtime/begin?type=3&txn_id=' +
      this.data;
  }
}
