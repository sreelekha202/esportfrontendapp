import { ShopRoutingModule } from './shop-routing.module';
import { NgModule } from '@angular/core';

import { GamesVoucherSliderComponent } from './components/games-voucher-slider/games-voucher-slider.component';
import { OrderThanksComponent } from './game-order/order-thanks/order-thanks.component';
import { OrderCancelComponent } from './game-order/order-cancel/order-cancel.component';
import { VoucherListComponent } from './components/voucher-list/voucher-list.component';
import { GameVoucherComponent } from './game-voucher/game-voucher.component';
import { OrderInfoComponent } from './game-order/order-info/order-info.component';
import { GameOrderComponent } from './game-order/game-order.component';
import { SharedModule } from '../../shared/modules/shared.module';
import { ShopComponent } from './shop.component';
import { VoucherPayComponent } from './game-voucher/voucher-pay/voucher-pay.component';
import { PaymentPopupComponent } from './payment-popup/payment-popup.component';

@NgModule({
  declarations: [
    ShopComponent,
    GameOrderComponent,
    OrderInfoComponent,
    VoucherPayComponent,
    OrderThanksComponent,
    OrderCancelComponent,
    GameVoucherComponent,
    VoucherListComponent,
    GamesVoucherSliderComponent,
    PaymentPopupComponent,
  ],
  imports: [ShopRoutingModule, SharedModule],
})
export class ShopModule {}
