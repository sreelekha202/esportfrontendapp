import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { SubmitFormComponentOutputData } from '../submit-form/submit-form.component';
import { AppHtmlRoutesLoginType } from '../../../app-routing.model';
import { AuthServices } from '../../../core/service';

export interface VerificationFormComponentData {
  title: string;
  subtitle: string;
  footerTitle: string;
  footerLinkTitle: string;
  timerMs: number;
  submitBtn: string;
  error?: string;
}

@Component({
  selector: 'app-verification-form',
  templateUrl: './verification-form.component.html',
  styleUrls: ['./verification-form.component.scss'],
})
export class VerificationFormComponent implements OnInit {
  @Output() submit = new EventEmitter();
  @Input() formValue: SubmitFormComponentOutputData;
  @Input() data: VerificationFormComponentData;
  @Input() set type(type: AppHtmlRoutesLoginType) {
    this.activeTypeValue = type;
  }

  AppHtmlRoutesLoginType = AppHtmlRoutesLoginType;
  activeTypeValue: AppHtmlRoutesLoginType;

  code: string;
  codeEntered: boolean;

  constructor(private authService: AuthServices) {}

  ngOnInit(): void {
    this.start();
  }

  start(): void {
    const interval = setInterval(() => {
      if (!(this.data.timerMs - 1)) {
        clearInterval(interval);
      }
      this.data.timerMs--;
    }, 1000);
  }

  onOtpChange(code: string): void {
    this.codeEntered = code.length === 4;

    if (this.codeEntered) {
      this.code = code;
    }
  }

  onSubmitEvent(): void {
    this.submit.emit({
      otp: this.code,
      formValue: this.formValue,
    });
  }

  resendOtp() {
    if (this.activeTypeValue === AppHtmlRoutesLoginType.emailLogin) {
      let queryData = {
        phoneNumber: this.formValue.mailPhone,
        type: 'login',
      };
      this.data.error = '';
      this.authService.resendOTP(queryData).subscribe(
        (data) => {
          this.data.timerMs = 240;
          this.start();
        },
        (error) => {
          this.data.error = error?.error?.message || error?.message;
        }
      );
    }
    if (this.activeTypeValue === AppHtmlRoutesLoginType.registration) {
      let data;
      if (this.formValue.secondInputType === 'email') {
        data = {
          email: this.formValue.mailPhone,
          type: 'email',
        };
      }
      if (this.formValue.secondInputType === 'phone') {
        data = {
          phoneNumber: this.formValue.mailPhone,
          type: 'phone',
        };
      }
      // let data = {
      //   email: this.firstStepForm.value.emailId,
      //   type: 'email',
      // };
      this.data.error = '';
      this.authService.resendOTP(data).subscribe(
        (data) => {
          this.data.timerMs = 240;
          this.start();
        },
        (error) => {
          this.data.error = error?.error?.message || error?.message;
        }
      );
    }
    if (this.activeTypeValue === AppHtmlRoutesLoginType.forgotPass) {
      let data;
      if (this.formValue.formValue['type'] === 'email') {
        data = {
          email: this.formValue.mailPhone,
          type: 'forgot_email',
        };
      }
      if (this.formValue.formValue['type'] === 'phone') {
        data = {
          phoneNumber: this.formValue.mailPhone,
          type: 'forgot_phone',
        };
      }
      this.data.error = '';
      this.authService.resendOTP(data).subscribe(
        (data) => {
          this.data.timerMs = 240;
          this.start();
        },
        (error) => {
          this.data.error = error?.error?.message || error?.message;
        }
      );
    }
    if (this.formValue.type === 'add_email') {
      let data;

      data = {
        email: this.formValue.mailPhone,
        type: 'add_email',
      };

      this.data.error = '';
      this.authService.resendOTP(data).subscribe(
        (data) => {
          this.data.timerMs = 240;
          this.start();
        },
        (error) => {
          this.data.error = error?.error?.message || error?.message;
        }
      );
    }
  }
}
