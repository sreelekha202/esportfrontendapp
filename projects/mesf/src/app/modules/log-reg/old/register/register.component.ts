import { Component, OnInit, ViewChildren } from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl,
} from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthServices } from '../../../../core/service';
import {
  faUser,
  faPhone,
  faLock,
  faEnvelope,
  faEye,
  faEyeSlash,
  faCheck,
  faDotCircle,
} from '@fortawesome/free-solid-svg-icons';
import { environment } from '../../../../../environments/environment';
import { GlobalUtils } from '../../../../shared/service/global-utils/global-utils';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit {
  faUser = faUser;
  faPhone = faPhone;
  faLock = faLock;
  faMail = faEnvelope;
  faEye = faEye;
  faEyeSlash = faEyeSlash;
  faCheck = faCheck;
  faDot = faDotCircle;

  @ViewChildren('formRow') rows: any;

  registerForm: FormGroup;
  confirmForm: FormGroup;
  stepOne = false;
  didFail = false;
  confirmUser = false;
  verified = false;
  username = '';
  public loading = false;
  public error = false;
  message = '';
  confirmError = '';
  token = '';
  isLogin = false;
  agree = '';
  fieldTextType: boolean = false;
  rfieldTextType: boolean = false;

  timeLeft: number = 60;
  interval;

  constructor(
    private fb: FormBuilder,
    private cf: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthServices
  ) {
    if (
      GlobalUtils.isBrowser() &&
      localStorage.getItem(environment.currentToken)
    ) {
      this.router.navigate(['/home']);
    }
  }

  async ngOnInit() {
    this.initRegisterForm();
    this.initConfirmForm();
  }

  initRegisterForm() {
    this.registerForm = this.fb.group({
      name: [
        '',
        Validators.compose([
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(100),
        ]),
      ],
      phoneno: [
        '',
        Validators.compose([
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(100),
        ]),
      ],
      password: [
        '',
        Validators.compose([
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(100),
        ]),
      ],
      rpassword: [
        '',
        Validators.compose([
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(100),
        ]),
      ],
      privacy_policy: [false, Validators.compose([Validators.required])],
    });
  }

  initConfirmForm() {
    this.confirmForm = this.cf.group({
      code1: ['', Validators.compose([Validators.required])],
      code2: ['', Validators.compose([Validators.required])],
      code3: ['', Validators.compose([Validators.required])],
      code4: ['', Validators.compose([Validators.required])],
    });
  }
  stepTow(value) {
    const controls = this.registerForm.controls;

    if (controls.name.status == 'INVALID') {
      controls.name.markAsTouched();

      this.stepOne = false;
    } else if (controls.phoneno.status == 'INVALID') {
      controls.phoneno.markAsTouched();

      this.stepOne = false;
    } else if (!controls.privacy_policy.value) {
      controls.privacy_policy.markAsTouched();

      this.agree =
        'Please indicate that you have read and agree to the Terms and Conditions and Privacy Policy';
      this.stepOne = false;
    } else {
      this.message = '';
      this.agree = '';
      this.stepOne = true;
    }

    return;
  }

  onSubmit(value) {
    const controls = this.registerForm.controls;
    /** check form */
    if (this.registerForm.invalid) {
      Object.keys(controls).forEach((controlName) =>
        controls[controlName].markAsTouched()
      );
      this.didFail = true;

      return;
    } else if (controls.password.value !== controls.rpassword.value) {
      this.didFail = true;
      this.message = "password doesn't match";
      return;
    } else {
      this.loading = true;
      this.didFail = false;
      this.message = '';
      const register = {
        fullName: value.name,
        password: value.password,
        phoneNumber: '+' + value.phoneno,
        type: 'phone',
      };
      this.authService.register(register).subscribe(
        (data) => {
          this.didFail = false;
          this.message = '';
          this.token = data.data.token;

          this.confirmUser = true;
          this.loading = false;

          this.interval = setInterval(() => {
            if (this.timeLeft > 0) {
              this.timeLeft--;
            }
            // else {
            //   this.timeLeft = 60;
            // }
          }, 1000);
        },
        (error) => {
          this.loading = false;
          this.didFail = true;
          this.message = error.error.message;
          this.stepOne = false;
        }
      );
    }
  }

  keyUpEvent(event, index) {
    let pos = index;
    if (event.keyCode === 8 && event.which === 8) {
      pos = index - 1;
    } else {
      pos = index + 1;
    }
    if (pos > -1 && pos < 4) {
      this.rows._results[pos].nativeElement.focus();
    }
  }
  onConfirm(value) {
    const controls = this.confirmForm.controls;
    /** check form */
    if (this.confirmForm.invalid) {
      Object.keys(controls).forEach((controlName) =>
        controls[controlName].markAsTouched()
      );
      this.didFail = true;
      this.message = 'Insert valid data';
      return;
    }

    this.didFail = false;
    this.message = '';
    this.loading = true;

    const obj = this.confirmForm.value;
    const code = obj.code1 + obj.code2 + obj.code3 + obj.code4;

    this.authService.confirmUser(this.token, code, 'phone').subscribe(
      (data) => {
        this.didFail = false;
        this.message = '';
        this.loading = false;
        this.verified = true;
      },
      (error) => {
        this.loading = false;
        this.didFail = true;
        this.message = error.error.message;
        this.verified = false;
      }
    );
  }

  resendOtp() {
    let data = {
      phoneNumber: '+' + this.registerForm.value.phoneno,
      type: 'phone',
    };
    this.authService.resendOTP(data).subscribe(
      (data) => {
        this.didFail = false;
        this.message = '';
        this.loading = false;
        this.timeLeft = 60;
      },
      (error) => {
        this.loading = false;
        this.didFail = true;
        this.message = error.error.message;
      }
    );
  }

  toggleFieldTextType() {
    this.fieldTextType = !this.fieldTextType;
  }
  rtoggleFieldTextType() {
    this.rfieldTextType = !this.rfieldTextType;
  }

  isControlHasError(controlName: string, validationType: string): boolean {
    const control = this.registerForm.controls[controlName];
    if (!control) {
      return false;
    }

    const result =
      control.hasError(validationType) && (control.dirty || control.touched);
    return result;
  }
}
