import { Component, OnInit, ViewChildren } from '@angular/core';
import { AuthServices, ToastService } from '../../../../core/service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {
  faLock,
  faEnvelope,
  faPhone,
  faEye,
  faEyeSlash,
} from '@fortawesome/free-solid-svg-icons';
import { environment } from '../../../../../environments/environment';
import { GlobalUtils } from '../../../../shared/service/global-utils/global-utils';

@Component({
  selector: 'app-login-email',
  templateUrl: './login-email.component.html',
  styleUrls: ['./login-email.component.scss'],
})
export class LoginEmailComponent implements OnInit {
  loginForm: FormGroup;
  MobileVerificationForm: FormGroup;
  confirmForm: FormGroup;
  passwordConfirmForm: FormGroup;
  newPasswordForm: FormGroup;

  didFail = false;
  message = '';
  faLock = faLock;
  faMail = faEnvelope;
  faPhone = faPhone;
  faEye = faEye;
  faEyeSlash = faEyeSlash;
  loading = false;
  returnUrl: string;
  id = '';

  stepZero: boolean = true;
  stepOne: boolean = false;
  stepTwo: boolean = false;

  @ViewChildren('formRow') rows: any;

  timeLeft: number = 60;
  interval;

  forgotOTP = false;
  newPassword = false;

  fieldTextType: boolean = false;
  rfieldTextType: boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    private cf: FormBuilder,
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthServices,
    public toastService: ToastService,
    public globalUtils: GlobalUtils
  ) {
    if (
      GlobalUtils.isBrowser() &&
      localStorage.getItem(environment.currentToken)
    ) {
      this.router.navigate(['/home']);
    }
  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      emailId: [
        '',
        Validators.compose([Validators.required, Validators.email]),
      ],
      password: [
        '',
        Validators.compose([Validators.required, Validators.minLength(3)]),
      ],
    });

    this.MobileVerificationForm = this.formBuilder.group({
      phoneNo: ['', Validators.compose([Validators.required])],
    });

    this.confirmForm = this.formBuilder.group({
      code1: ['', Validators.compose([Validators.required])],
      code2: ['', Validators.compose([Validators.required])],
      code3: ['', Validators.compose([Validators.required])],
      code4: ['', Validators.compose([Validators.required])],
    });

    this.passwordConfirmForm = this.cf.group({
      code1: ['', Validators.compose([Validators.required])],
      code2: ['', Validators.compose([Validators.required])],
      code3: ['', Validators.compose([Validators.required])],
      code4: ['', Validators.compose([Validators.required])],
    });

    this.newPasswordForm = this.fb.group({
      password: [
        '',
        Validators.compose([
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(100),
        ]),
      ],
      rpassword: [
        '',
        Validators.compose([
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(100),
        ]),
      ],
    });

    // get return url from route parameters or default to '/'
    //this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    this.returnUrl = this.authService.redirectUrl || '/';
  }

  onSubmit(value) {
    const controls = this.loginForm.controls;
    // check form
    if (this.loginForm.invalid) {
      Object.keys(controls).forEach((controlName) =>
        controls[controlName].markAsTouched()
      );
      return;
    }

    this.loading = true;
    const login = {
      password: value.password,
      email: value.emailId,
      type: 'email',
    };

    if (GlobalUtils.isBrowser()) {
      this.authService.login(login).subscribe(
        (user) => {
          if (user.code == 'ER1001') {
            this.id = user.data.id;
            this.stepZero = false;
            this.stepOne = true;
          } else {
            // store user details and jwt token in local storage to keep user logged in between page refreshes
            localStorage.setItem(environment.currentToken, user.data.token);
            if (user.data.firstLogin == 1) {
              this.router.navigate(['/profile']);
            } else {
              this.router.navigate([this.returnUrl]);
            }
          }
        },
        (error) => {
          this.didFail = true;
          this.message = error.error.message;
          this.loading = false;
        }
      );
    }
  }

  enableStepTwo(value) {
    const controls = this.MobileVerificationForm.controls;
    // check form
    if (this.MobileVerificationForm.invalid) {
      Object.keys(controls).forEach((controlName) =>
        controls[controlName].markAsTouched()
      );
      return;
    }

    this.loading = true;
    const data = {
      phoneNumber: '+' + value.phoneNo,
      id: this.id,
    };

    this.authService.update(data).subscribe(
      (data) => {
        this.loading = false;
        this.didFail = false;
        this.message = '';
        this.stepOne = false;
        this.stepTwo = true;

        this.interval = setInterval(() => {
          if (this.timeLeft > 0) {
            this.timeLeft--;
          }
          // else {
          //   this.timeLeft = 60;
          // }
        }, 1000);
      },
      (error) => {
        this.loading = false;
        this.didFail = true;
        this.message = error.error.message;
      }
    );
  }

  resendOtp() {
    let data = {
      phoneNumber: '+' + this.MobileVerificationForm.value.phoneNo,
      type: 'login',
    };
    this.authService.resendOTP(data).subscribe(
      (data) => {
        this.didFail = false;
        this.message = '';
        this.loading = false;
        this.timeLeft = 60;
      },
      (error) => {
        this.loading = false;
        this.didFail = true;
        this.message = error.error.message;
      }
    );
  }

  keyUpEvent(event, index) {
    let pos = index;
    if (event.keyCode === 8 && event.which === 8) {
      pos = index - 1;
    } else {
      pos = index + 1;
    }
    if (pos > -1 && pos < 4) {
      this.rows._results[pos].nativeElement.focus();
    }
  }

  onConfirm(value) {
    if (GlobalUtils.isBrowser()) {
      const controls = this.confirmForm.controls;
      /** check form */
      if (this.confirmForm.invalid) {
        Object.keys(controls).forEach((controlName) =>
          controls[controlName].markAsTouched()
        );
        this.didFail = true;
        this.message = 'Insert valid OTP';
        return;
      }

      this.didFail = false;
      this.message = '';
      this.loading = true;

      const obj = this.confirmForm.value;
      const code = obj.code1 + obj.code2 + obj.code3 + obj.code4;

      this.authService.confirm(this.id, code, 'phone').subscribe(
        (data) => {
          localStorage.setItem(environment.currentToken, data.data.token);
          if (data.data.firstLogin == 1) {
            this.router.navigate(['/profile']);
          } else {
            this.router.navigate([this.returnUrl]);
          }
        },
        (error) => {
          this.loading = false;
          this.didFail = true;
          this.message = error.error.message;
        }
      );
    }
  }

  forgot_password() {
    const controls = this.loginForm.controls;
    if (controls.emailId.status == 'INVALID') {
      controls.emailId.markAsTouched();
      return;
    }
    let data = {
      type: 'email',
      email: controls.emailId.value,
    };
    this.authService.forgotPassword(data).subscribe(
      (user) => {
        this.stepZero = false;
        this.didFail = false;
        this.message = '';
        this.forgotOTP = true;
        this.interval = setInterval(() => {
          if (this.timeLeft > 0) {
            this.timeLeft--;
          }
          // else {
          //   this.timeLeft = 60;
          // }
        }, 1000);
      },
      (error) => {
        this.didFail = true;
        this.message = error.error.message;
        this.loading = false;
      }
    );
  }

  passwordOnConfirm(value) {
    const controls = this.passwordConfirmForm.controls;
    /** check form */
    if (this.passwordConfirmForm.invalid) {
      Object.keys(controls).forEach((controlName) =>
        controls[controlName].markAsTouched()
      );
      this.didFail = true;
      this.message = 'Insert valid data';
      return;
    }

    this.didFail = false;
    this.message = '';
    this.loading = true;

    const obj = this.passwordConfirmForm.value;
    const code = obj.code1 + obj.code2 + obj.code3 + obj.code4;

    let data = {
      email: this.loginForm.value.emailId,
      type: 'email_otp',
      emailOTP: code,
    };
    this.authService.forgotPassword(data).subscribe(
      (data) => {
        this.didFail = false;
        this.message = '';
        this.forgotOTP = false;
        this.newPassword = true;
        this.loading = false;
      },
      (error) => {
        this.loading = false;
        this.didFail = true;
        this.message = error.error.message;
      }
    );
  }

  onPasswordSubmit(value) {
    const controls = this.newPasswordForm.controls;
    // check form
    if (this.newPasswordForm.invalid) {
      Object.keys(controls).forEach((controlName) =>
        controls[controlName].markAsTouched()
      );
      return;
    } else if (controls.password.value !== controls.rpassword.value) {
      this.didFail = true;
      this.message = "password doesn't match";
      return;
    } else {
      this.didFail = false;
      this.message = '';
      const obj = this.passwordConfirmForm.value;
      const code = obj.code1 + obj.code2 + obj.code3 + obj.code4;
      let data = {
        type: 'update',
        emailOTP: code,
        email: this.loginForm.controls.emailId.value,
        password: value.password,
      };
      this.authService.forgotPassword(data).subscribe(
        (user) => {
          this.stepZero = false;
          this.didFail = false;
          this.message = '';
          this.toastService.showSuccess(user.message);
          this.router
            .navigateByUrl('/register-email', { skipLocationChange: true })
            .then(() => {
              this.router.navigate(['/login-email']);
            });
        },
        (error) => {
          this.didFail = true;
          this.message = error.error.message;
          this.loading = false;
        }
      );
    }
  }

  passwordResendOtp() {
    let data = {
      email: this.loginForm.value.emailId,
      type: 'forgot_email',
    };
    this.authService.resendOTP(data).subscribe(
      (data) => {
        this.didFail = false;
        this.message = '';
        this.loading = false;
        this.timeLeft = 60;
      },
      (error) => {
        this.loading = false;
        this.didFail = true;
        this.message = error.error.message;
      }
    );
  }

  toggleFieldTextType() {
    this.fieldTextType = !this.fieldTextType;
  }
  rtoggleFieldTextType() {
    this.rfieldTextType = !this.rfieldTextType;
  }

  isPasswordControlHasError(
    controlName: string,
    validationType: string
  ): boolean {
    const control = this.newPasswordForm.controls[controlName];
    if (!control) {
      return false;
    }

    const result =
      control.hasError(validationType) && (control.dirty || control.touched);
    return result;
  }
  isControlHasError(controlName: string, validationType: string): boolean {
    const control = this.loginForm.controls[controlName];
    if (!control) {
      return false;
    }

    const result =
      control.hasError(validationType) && (control.dirty || control.touched);
    return result;
  }

  isControlHasErrorphone(controlName: string, validationType: string): boolean {
    const control = this.MobileVerificationForm.controls[controlName];
    if (!control) {
      return false;
    }

    const result =
      control.hasError(validationType) && (control.dirty || control.touched);
    return result;
  }
}
