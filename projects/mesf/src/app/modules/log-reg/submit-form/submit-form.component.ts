import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import {
  CountryISO,
  NgxIntlTelInputComponent,
  SearchCountryField,
} from 'ngx-intl-tel-input';
import { faFacebookF, faTwitter } from '@fortawesome/free-brands-svg-icons';
import { Country } from 'ngx-intl-tel-input/lib/model/country.model';
import { faEnvelope } from '@fortawesome/free-solid-svg-icons';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import {
  AppHtmlRoutes,
  AppHtmlRoutesLoginType,
} from '../../../app-routing.model';
import { GlobalUtils } from '../../../shared/service/global-utils/global-utils';

export interface SubmitFormComponentOutputData {
  [key: string]: string;
}

export interface SubmitFormComponentDataItem {
  title: string;
  subtitle: string;
  checkboxLabel: string;
  checkboxLabelpp: string;
  checkboxLabeltc: string;
  submitBtn: string;
  orLabel: string;
  footerText: string;
  footerLink: string;
  footerLinkUrl: string[];
  firstInputName: string;
  firstInputType: string;
  firstInputPlaceholder: string;
  firstInputIcon: string;
  secondInputName: string;
  secondInputType: string;
  secondInputPlaceholder: string;
  secondInputIcon: string;
  forgotPass?: string;
  error?: string;
}

@Component({
  selector: 'app-submit-form',
  templateUrl: './submit-form.component.html',
  styleUrls: ['./submit-form.component.scss'],
})
export class SubmitFormComponent implements OnInit {
  @ViewChild('ngxTel') ngxTel: NgxIntlTelInputComponent;
  @Output() submit = new EventEmitter();
  @Input() data: SubmitFormComponentDataItem;
  @Input() referredBy: string;
  @Input() set type(type: AppHtmlRoutesLoginType) {
    this.activeTypeValue = type;

    if (this.activeTypeValue === AppHtmlRoutesLoginType.registration) {
      this.form
        .get('firstInput')
        .setValidators([
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(100),
        ]);
      this.form.get('privacyPolicy').setValidators(Validators.pattern('true'));
      this.form.get('firstInput').updateValueAndValidity();
    }
    if (this.activeTypeValue === AppHtmlRoutesLoginType.emailLogin) {
      this.form
        .get('firstInput')
        .setValidators([Validators.required, Validators.email]);
      this.form.get('firstInput').updateValueAndValidity();
    }
  }

  AppHtmlRoutes = AppHtmlRoutes;
  AppHtmlRoutesLoginType = AppHtmlRoutesLoginType;
  activeTypeValue: AppHtmlRoutesLoginType;

  form = this.formBuilder.group({
    firstInput: ['', Validators.required],
    secondInput: ['', Validators.required],
    keepMeLoggedIn: [false],
    privacyPolicy: [false],
  });

  faFacebookF = faFacebookF;
  faTwitter = faTwitter;
  faEnvelope = faEnvelope;
  enableTermsPolicy = false;
  SearchCountryField = SearchCountryField;
  CountryISO = CountryISO;
  preferredCountries: CountryISO[] = [
    CountryISO.Malaysia,
    CountryISO.Indonesia,
    CountryISO.Singapore,
  ];
  selectedCountryISO = CountryISO.Malaysia;
  mySelectedCountryISO: CountryISO;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private modalService: NgbModal
  ) {
    this.checkForKeepMeLoggedIn();
  }

  ngOnInit() {}

  onCountryChange(e: Country) {
    this.mySelectedCountryISO = e.iso2 as CountryISO;
    this.onKeepLoggedIn();
  }

  onKeepLoggedIn() {
    if (GlobalUtils.isBrowser()) {
      if (this.form.get('privacyPolicy').value) {
        const keepMeLoggedIn = {
          keepMeLoggedIn: true,
          mySelectedCountryISO:
            this.mySelectedCountryISO || this.preferredCountries[0],
        };
        localStorage.setItem('keepMeLoggedIn', JSON.stringify(keepMeLoggedIn));
      } else {
        localStorage.removeItem('keepMeLoggedIn');
      }
    }
  }

  onSubmitEvent(): void {
    this.submit.emit({
      [this.data.firstInputName]: this.form.value.firstInput,
      [this.data.secondInputName]: this.form.value.secondInput,
      firstInputType: this.data.firstInputName,
      secondInputType: this.data.secondInputName,
    });
  }

  changeInput(input): void {
    if (input === 'phone') {
      this.data.secondInputName = 'email';
      this.data.secondInputType = 'email';
      this.data.secondInputPlaceholder = 'LOG_REG.LOGIN.firstInputPlaceholder1';
      this.data.secondInputIcon = 'email';
      this.form
        .get('secondInput')
        .setValidators([Validators.required, Validators.email]);
      this.form.get('secondInput').reset();
      this.form.get('secondInput').updateValueAndValidity();
    } else {
      this.data.secondInputName = 'phone';
      this.data.secondInputType = 'phone';
      this.data.secondInputPlaceholder = 'LOG_REG.LOGIN.firstInputPlaceholder';
      this.data.secondInputIcon = 'phone';
      this.form.get('secondInput').setValidators([Validators.required]);
      this.form.get('secondInput').reset();
      this.form.get('secondInput').updateValueAndValidity();
    }
  }

  changeLoginType(type) {
    if (type === 'email') {
      this.router
        .navigateByUrl('/home', { skipLocationChange: true })
        .then(() => {
          this.router.navigate(['/user', AppHtmlRoutesLoginType.emailLogin]);
        });
    }
    if (type === 'phone') {
      this.router
        .navigateByUrl('/home', { skipLocationChange: true })
        .then(() => {
          this.router.navigate(['/user', AppHtmlRoutesLoginType.phoneLogin]);
        });
    }
  }
  forgot() {
    this.router
      .navigateByUrl('/home', { skipLocationChange: true })
      .then(() => {
        this.router.navigate(['/user', AppHtmlRoutesLoginType.forgotPass]);
      });
  }

  private checkForKeepMeLoggedIn(): void {
    if (GlobalUtils.isBrowser()) {
      const keepMeLoggedIn = JSON.parse(localStorage.getItem('keepMeLoggedIn'));
      if (keepMeLoggedIn) {
        this.form.get('privacyPolicy').setValue(keepMeLoggedIn.keepMeLoggedIn);
        this.selectedCountryISO = keepMeLoggedIn.mySelectedCountryISO;
      }
    }
  }
}
