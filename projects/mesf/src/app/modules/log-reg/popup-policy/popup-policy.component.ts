import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import {
  Component,
  Input,
  OnInit,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { UserService } from '../../../core/service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-popup-policy',
  templateUrl: './popup-policy.component.html',
  styleUrls: ['./popup-policy.component.scss'],
})
export class PopupPolicyComponent implements OnInit {
  @ViewChild('pp1', { static: true })
  public pp1: TemplateRef<any>;
  private privacyPolicyRef: NgbModalRef;
  userSubscription: Subscription;
  enable = false;

  constructor(
    public modalService: NgbModal,
    private userService: UserService,
    public translate: TranslateService
  ) {}

  ngOnInit(): void {
    this.getUser();
  }
  getUser() {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (
        typeof data?.isPrivacyPolicyAccepted == 'boolean' &&
        !data?.isPrivacyPolicyAccepted &&
        this.pp1
      ) {
        this.privacyPolicyRef = this.modalService.open(this.pp1, {
          size: 'lg',
          centered: true,
          scrollable: true,
          windowClass: 'custom-modal-content',
          backdrop: 'static',
          keyboard: false,
        });
      } else if (this.privacyPolicyRef && this.pp1) {
        this.privacyPolicyRef?.dismiss();
      }
    });
  }
  onClick1() {
    if (this.enable == false) {
      this.enable = true;
    } else {
      this.enable = false;
    }
  }

  update() {
    const data = {
      isPrivacyPolicyAccepted: true,
    };
    const res = this.userService.updateProfile(data).subscribe(() => {
      this.privacyPolicyRef?.dismiss();
      this.userService.refreshCurrentUser();
    });
  }
}
