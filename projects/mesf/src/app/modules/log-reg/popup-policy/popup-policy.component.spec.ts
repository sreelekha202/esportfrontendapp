import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopupPolicyComponent } from './popup-policy.component';

describe('PopupPolicyComponent', () => {
  let component: PopupPolicyComponent;
  let fixture: ComponentFixture<PopupPolicyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PopupPolicyComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopupPolicyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
