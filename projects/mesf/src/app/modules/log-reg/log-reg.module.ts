import { NgModule } from '@angular/core';

import { SocialMediaLoginComponent } from '../../core/social-media-login/social-media-login.component';
import { VerificationFormComponent } from './verification-form/verification-form.component';
import { CongratulationComponent } from './congratulation-form/congratulation.component';
import { RegisterEmailComponent } from './old/register-email/register-email.component';
import { PasswordFormComponent } from './password-form/password-form.component';
import { LoginEmailComponent } from './old/login-email/login-email.component';
import { SubmitFormComponent } from './submit-form/submit-form.component';
import { RegisterComponent } from './old/register/register.component';
import { PhoneNoFormComponent } from './phone-no/phone-no.component';
import { SharedModule } from '../../shared/modules/shared.module';
import { LogRegRoutingModule } from './log-reg-routing.module';
import { LoginComponent } from './old/login/login.component';
import { LogRegComponent } from './log-reg.component';
import { CoreModule } from '../../core/core.module';

@NgModule({
  declarations: [
    LogRegComponent,
    RegisterComponent,
    LoginComponent,
    RegisterEmailComponent,
    LoginEmailComponent,
    SocialMediaLoginComponent,

    VerificationFormComponent,
    CongratulationComponent,
    SubmitFormComponent,
    PhoneNoFormComponent,
    PasswordFormComponent,
  ],
  imports: [SharedModule, LogRegRoutingModule, CoreModule],
  exports: [PhoneNoFormComponent, VerificationFormComponent],
})
export class LogRegModule {}
