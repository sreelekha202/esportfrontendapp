import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

import { IAdvertisementManagement } from '../../../../shared/models/advertisement';

export interface ViewAdvertisementPopupComponentData {
  advertisement: IAdvertisementManagement;
}
@Component({
  selector: 'app-view-advertisement-model',
  templateUrl: './view-advertisement-model.component.html',
  styleUrls: ['./view-advertisement-model.component.scss'],
})
export class ViewAdvertisementModelComponent implements OnInit {
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: ViewAdvertisementPopupComponentData
  ) {}

  ngOnInit(): void {}
}
