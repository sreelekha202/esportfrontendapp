import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';

import { InboxNotificationsComponent } from './components/user-notifications/inbox-notifications/inbox-notifications.component';
import { EmailNotificationsComponent } from './components/user-notifications/email-notifications/email-notifications.component';
import { PushNotificationsComponent } from './components/user-notifications/push-notifications/push-notifications.component';
import { AdvertisementManagementComponent } from './components/advertisement-management/advertisement-management.component';
import { ViewAdvertisementModelComponent } from './popups/view-advertisement-model/view-advertisement-model.component';
import { EsportsManagementComponent } from './components/esports-management/esports-management.component';
import { TournamentDetailsComponent } from './components/tournament-details/tournament-details.component';
import { ContentManagementComponent } from './components/content-management/content-management.component';
import { UserNotificationsComponent } from './components/user-notifications/user-notifications.component';
import { ViewUserAccessPopupComponent } from './popups/view-user-access/view-user-access-popup.component';
import { ViewAdvertisementComponent } from './components/view-advertisement/view-advertisement.component';
import { SiteConfigurationComponent } from './components/site-configuration/site-configuration.component';
import { AccessManagementComponent } from './components/access-management/access-management.component';
import { ViewUserDetailsComponent } from './components/view-user-details/view-user-details.component';
import { ArticlesDetailsComponent } from './components/articles-details/articles-details.component';
import { ViewUserAccessComponent } from './components/view-user-access/view-user-access.component';
import { UserManagementComponent } from './components/user-management/user-management.component';
import { UserDetailsComponent } from './components/user-details/user-details.component';
import { RegistrationTypePipe } from '../../shared/pipe/registration-type.pipe';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { AdminHeaderComponent } from './admin-header/admin-header.component';
import { ConvertStatusPipe } from '../../shared/pipe/convert-status.pipe';
import { AdminMenuComponent } from './admin-menu/admin-menu.component';
import { FirstWordPipe } from '../../shared/pipe/first-word.pipe';
import { SharedModule } from '../../shared/modules/shared.module';
import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './admin.component';
import { RewardConfigurationComponent } from './components/reward-configuration/reward-configuration.component';
import { ShopConfigurationComponent } from './components/shop-configuration/shop-configuration.component';
import { CoreModule } from '../../core/core.module';
import { BannerComponent } from './components/banner/banner.component';
import { GameComponent } from './components/game/game.component';
import { LeaderboardConfigurationComponent } from './components/leaderboard-configuration/leaderboard-configuration.component';
import { SentNotificationsComponent } from './components/user-notifications/sent-notifications/sent-notifications.component';
import { EsportsPaymentComponent } from './components/esports-payment/esports-payment.component';
import { EsportsPaymentDetailsComponent } from './components/esports-payment-details/esports-payment-details.component';
import { PaymentDisbursementDialogComponent } from './components/esports-payment-details/payment-disbursement-dialog/payment-disbursement-dialog.component';
import { AutomatedDialogComponent } from './components/esports-payment-details/automated-dialog/automated-dialog.component';
import { SendCouponDialogComponent } from './components/esports-payment-details/send-coupon-dialog/send-coupon-dialog.component';
import { PaymentSuccessfullDialogComponent } from './components/esports-payment-details/payment-successfull-dialog/payment-successfull-dialog.component';
import { AnnouncementConfigurationComponent } from './components/announcement-configuration/announcement-configuration.component';
import { PrizeDisbursalComponent } from './components/prize-disbursal/prize-disbursal.component';
@NgModule({
  declarations: [
    AdminComponent,
    DashboardComponent,
    EsportsManagementComponent,
    TournamentDetailsComponent,
    RegistrationTypePipe,
    ContentManagementComponent,
    ArticlesDetailsComponent,
    UserManagementComponent,
    UserDetailsComponent,
    ViewUserDetailsComponent,
    UserNotificationsComponent,
    InboxNotificationsComponent,
    AdminMenuComponent,
    AdminHeaderComponent,
    EmailNotificationsComponent,
    AccessManagementComponent,
    ViewUserAccessPopupComponent,
    ViewUserAccessComponent,
    AdvertisementManagementComponent,
    ViewAdvertisementComponent,
    ViewAdvertisementModelComponent,
    PushNotificationsComponent,
    PrizeDisbursalComponent,
    SiteConfigurationComponent,
    FirstWordPipe,
    ConvertStatusPipe,
    RewardConfigurationComponent,
    ShopConfigurationComponent,
    BannerComponent,
    GameComponent,
    LeaderboardConfigurationComponent,
    SentNotificationsComponent,
    EsportsPaymentComponent,
    EsportsPaymentDetailsComponent,
    PaymentDisbursementDialogComponent,
    AutomatedDialogComponent,
    SendCouponDialogComponent,
    PaymentSuccessfullDialogComponent,
    AnnouncementConfigurationComponent,
  ],
  imports: [
    SharedModule,
    AdminRoutingModule,
    AngularMultiSelectModule,
    FormsModule,
    CoreModule,
  ],
})
export class AdminModule {}
