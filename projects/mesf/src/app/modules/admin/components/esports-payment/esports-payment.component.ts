import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  SimpleChanges,
  ViewChild,
} from '@angular/core';

import {
  ConstantsService,
  TournamentService,
  ToastService,
} from '../../../../core/service';

import { AppHtmlRoutes } from '../../../../app-routing.model';
import {
  InfoPopupComponent,
  InfoPopupComponentData,
  InfoPopupComponentType,
} from '../../../../shared/popups/info-popup/info-popup.component';

import { DatatableComponent } from '@swimlane/ngx-datatable';
import { MatDialog } from '@angular/material/dialog';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-esports-payment',
  templateUrl: './esports-payment.component.html',
  styleUrls: ['./esports-payment.component.scss'],
})
export class EsportsPaymentComponent implements OnInit {
  @Input() rows: any = [];
  @Input() tournamentList = [];
  @Input() isFeatureEditable: boolean;
  @Input() viewIconShow: boolean;
  @Input() deleteIconShow: boolean;
  @Input() checkIconShow: boolean;
  @Output() isModified = new EventEmitter();

  @ViewChild(DatatableComponent) table: DatatableComponent;

  AppHtmlRoutes = AppHtmlRoutes;

  hideBtn: boolean = false;
  showDetails: boolean = false;
  showRequestEditBtn: boolean = false;
  showRequestTrashBtn: boolean = false;
  viewModelBtnDisable: boolean = false;

  changeRequestModel: string;
  modelDesc: string;
  modelHeader: string;
  modelTitle: string;

  articleModel: any;
  tournamentDetails: any;
  tournamentId: any;

  tempTournamentList = [];

  columnsFirstTab = [
    { name: 'Tournament Name' },
    { name: 'Registration' },
    { name: 'Game' },
    { name: 'Region' },
    { name: 'Type' },
    { name: 'Start Date' },
    { name: 'End Date' },
    { name: 'Participant(s)' },
    { name: '' },
  ];

  constructor(
    private modalService: NgbModal,
    private tournamentService: TournamentService,
    private translateService: TranslateService,
    public dialog: MatDialog,
    public toastService: ToastService
  ) {}

  ngOnInit(): void {}

  ngOnChanges(simpleChanges: SimpleChanges): void {
    if (
      simpleChanges.hasOwnProperty('tournamentList') &&
      simpleChanges.tournamentList.currentValue &&
      simpleChanges.tournamentList.currentValue.length
    ) {
      this.rows = [...this.tournamentList];
      this.tempTournamentList =
        this.tournamentList && this.tournamentList.length > 0
          ? [...this.tournamentList]
          : [];
    } else {
      this.tournamentList = [];
    }
  }

  /**
   * method to emit the feature value change to parent
   * @param e
   * @param _id
   */
  updateValue(e, _id) {
    this.isModified.emit({ type: 'feature', value: e.checked, id: _id });
    this.rows = [];
  }

  navigate(id) {
    this.tournamentId = id;
    this.showDetails = true;
  }

  isToggle(e) {
    this.showDetails = e.toggle;
    this.isModified.emit({ type: 'updatePaymentTable' });
  }

  // search the tournament details
  searchByValueFilter(event) {
    const value = event.target.value.toLowerCase().trim();
    // get the amount of columns in the table
    const count = this.columnsFirstTab.length;
    const keys = Object.keys(this.tempTournamentList[0]);
    // assign filtered matches to the active datatable
    const filterData = this.tempTournamentList.filter((item) => {
      // iterate through each row's column data
      for (let i = 0; i < count; i++) {
        // check for a match
        if (
          (item[keys[i]] &&
            item[keys[i]].toString().toLowerCase().indexOf(value) !== -1) ||
          !value
        ) {
          // found match, return true to add to result set
          return true;
        }
      }
    });
    this.rows = [...filterData];
    // Whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }

  /**
   * Open model pop on button click
   * @param content -content
   */
  open(content, Id, modelType) {
    this.tournamentId = Id;
    this.hideBtn = true;
    this.showRequestEditBtn = false;
    // Check to open model and configure inner content
    if (modelType === 'requestEdit') {
      this.showRequestEditBtn = true;
      this.showRequestTrashBtn = false;
      this.setModelConfiguration(
        ConstantsService.Model.RequestEdit.header,
        ConstantsService.Model.RequestEdit.title,
        ConstantsService.Model.RequestEdit.description
      );
    } else if (modelType === 'trash') {
      this.showRequestEditBtn = true;
      this.showRequestTrashBtn = true;
      this.setModelConfiguration(
        ConstantsService.Model.TournamentTrash.header,
        ConstantsService.Model.TournamentTrash.title,
        ConstantsService.Model.TournamentTrash.description
      );
    } else {
      this.setModelConfiguration(
        ConstantsService.Model.TournamentAbort.header,
        ConstantsService.Model.TournamentAbort.title,
        ConstantsService.Model.TournamentAbort.description
      );
    }
    this.modalService.dismissAll();
    this.modalService.open(content, {
      size: 'lg',
      centered: true,
      scrollable: true,
      windowClass: 'custom-modal-content',
    });
  }

  /**
   * Open model to view the Tournament
   * @param model : model
   * @param Id : Set Selected Id
   * @param Type : Check type of model to hide/show the button
   */
  viewTournament(model, Id, Type) {
    this.tournamentId = Id;
    this.viewModelBtnDisable = Type === 'view' ? true : false;
    this.modalService.open(model, {
      size: 'lg',
      centered: true,
      scrollable: true,
      windowClass: 'custom-modal-content',
    });
    this.getTournamentById(Id);
  }
  // get tournament details based on Id
  getTournamentById(Id) {
    this.tournamentService.getTournament(Id).subscribe(
      (res: any) => {
        this.tournamentDetails = res.data;
      },
      (err) => {}
    );
  }

  // Abort Tournament based on Selected Id
  abort() {
    const formValues = {
      tournamentStatus: ConstantsService.Status.Abort,
    };
    this.tournamentService
      .updateTournament(formValues, this.tournamentId)
      .subscribe(
        (res) => {
          if (res) {
            this.setModelConfiguration(
              ConstantsService.Model.TournamentAbortSuccess.header,
              ConstantsService.Model.TournamentAbortSuccess.title,
              ConstantsService.Model.TournamentAbortSuccess.description
            );
            // Remove Object from list based on ID
            this.isModified.emit({
              type: 'abort',
              value: true,
              id: this.tournamentId,
            });
            const tournamentObject = this.tournamentList;
            this.tournamentList = tournamentObject.filter(
              (resp) => resp._id !== this.tournamentId
            );
            this.hideBtn = false;
          } else {
            this.toastService.showError(
              this.translateService.instant(
                ConstantsService.APIError.TournamentUpdateError
              )
            );
          }
        },
        (err: any) => {
          const afterBlockData: InfoPopupComponentData = {
            title: this.translateService.instant(
              'API.TOURNAMENT.PUT.SUCCESS_HEADER'
            ),
            text:
              this.translateService.instant(err.error.messageCode) ||
              err.error.message,
            type: InfoPopupComponentType.info,
          };
          this.dialog.open(InfoPopupComponent, { data: afterBlockData });
        }
      );
  }

  // Trash the tournament
  trash() {
    const formValues = {
      tournamentStatus: ConstantsService.Status.Delete,
      changeRequest: this.changeRequestModel,
    };
    this.updateTournamentById(formValues);
  }

  // Approve/Publish tournament
  approveTournament() {
    const formValues = {
      tournamentStatus: ConstantsService.Status.Publish,
    };
    this.updateTournamentById(formValues);
  }

  // Change request
  requestEdit() {
    const formValues = {
      tournamentStatus: ConstantsService.Status.RequestEdit,
      changeRequest: this.changeRequestModel,
    };
    this.updateTournamentById(formValues);
  }

  // Update tournament by Id
  updateTournamentById(formValues) {
    if (this.tournamentId) {
      this.tournamentService
        .updateTournament(formValues, this.tournamentId)
        .subscribe(
          (res: any) => {
            this.modalService.dismissAll();
            this.SuccessModel(
              this.translateService.instant(
                'API.TOURNAMENT.PUT.SUCCESS_HEADER'
              ),
              this.translateService.instant(res.messageCode) || res.message
            );
            const tournamentObject = this.tournamentList;
            this.tournamentList = tournamentObject.filter(
              (resp) => resp._id !== this.tournamentId
            );
          },
          (error) => {
            this.toastService.showError(
              this.translateService.instant(
                ConstantsService.APIError.TournamentUpdateError
              )
            );
          }
        );
    }
  }

  /**
   * Confiure model
   * @param header : header value
   * @param title : Title value
   * @param description : Description value
   */
  setModelConfiguration(header, title, description) {
    this.modelTitle = this.translateService.instant(title);
    this.modelDesc = this.translateService.instant(description);
    this.modelHeader = this.translateService.instant(header);
  }

  // Display the model pop up for success message
  SuccessModel(title, text) {
    const data: InfoPopupComponentData = {
      title: this.translateService.instant(title),
      text: this.translateService.instant(text),
      type: InfoPopupComponentType.info,
    };
    this.dialog.open(InfoPopupComponent, { data });
  }
}
