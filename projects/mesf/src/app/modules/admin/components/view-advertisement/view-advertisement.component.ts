import {
  Component,
  OnInit,
  Input,
  ViewChild,
  TemplateRef,
  Output,
  EventEmitter,
} from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';

import { IAdvertisementManagement } from '../../../../shared/models/advertisement';
import { MatDialog } from '@angular/material/dialog';
import {
  ViewAdvertisementModelComponent,
  ViewAdvertisementPopupComponentData,
} from '../../popups/view-advertisement-model/view-advertisement-model.component';
import {
  InfoPopupComponentData,
  InfoPopupComponentType,
  InfoPopupComponent,
} from '../../../../shared/popups/info-popup/info-popup.component';

@Component({
  selector: 'app-view-advertisement',
  templateUrl: './view-advertisement.component.html',
  styleUrls: ['./view-advertisement.component.scss'],
})
export class ViewAdvertisementComponent implements OnInit {
  @Input() advertisementList: IAdvertisementManagement[] = [];
  @ViewChild('buttonsTemplate') buttonsTemplate: TemplateRef<any>;
  @Input() viewIconShow: boolean;
  @Output() action = new EventEmitter();
  @Input() deleteIconShow: boolean;
  @Input() checkIconShow: boolean;
  @ViewChild(DatatableComponent) table: DatatableComponent;
  columnsFirstTab = [
    { prop: 'campaignNumber', name: 'Campaign Number' },
    { prop: 'campaignName', name: 'Campaign Name' },
    { prop: 'advertisementType', name: 'Advertisement Type' },
    { prop: 'isSystemGenerated', name: 'Generated From' },
    { prop: 'actions', name: 'Actions' },
  ];

  constructor(public dialog: MatDialog) {}

  ngOnInit(): void {}

  onViewUserAccess(
    advertisement: IAdvertisementManagement,
    accessIcon?: boolean
  ): void {
    Object.assign(advertisement, { checkIconShow: accessIcon });
    const blockUserData: ViewAdvertisementPopupComponentData = {
      advertisement,
    };
    const dialogRef = this.dialog.open(ViewAdvertisementModelComponent, {
      data: blockUserData,
    });

    dialogRef.afterClosed().subscribe(() => {});
  }

  actionBannerAd(ad) {
    this.action.emit({ type: 'view', data: ad });
  }
  onRemoveUser(ad) {
    const blockUserData: InfoPopupComponentData = {
      title: 'REMOVE ADVERTISEMENT DETAILS',
      text: 'Are you sure you want to remove this details?',
      type: InfoPopupComponentType.confirm,
      btnText: 'Confirm',
    };
    const dialogRef = this.dialog.open(InfoPopupComponent, {
      data: blockUserData,
    });

    dialogRef.afterClosed().subscribe((confirmed) => {
      if (confirmed) {
        this.action.emit({ type: 'delete', data: ad });
      }
    });
  }
}
