import { Component, OnInit, ViewChild } from '@angular/core';
import {
  FormControl,
  FormGroup,
  Validators,
  FormBuilder,
} from '@angular/forms';
import {
  InfoPopupComponentData,
  InfoPopupComponentType,
  InfoPopupComponent,
} from '../../../../shared/popups/info-popup/info-popup.component';
import { MatDialog } from '@angular/material/dialog';
import { GlobalUtils } from '../../../../shared/service/global-utils/global-utils';
import {
  AdminService,
  ConstantsService,
  LanguageService,
} from '../../../../core/service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: [
    './banner.component.scss',
    '../site-configuration/site-configuration.component.scss',
  ],
})
export class BannerComponent implements OnInit {
  bannerUri: any = '';
  public ownerForm: FormGroup;
  advertisementList: any = [];
  showAdList = true;
  isEditable = {};
  isLoading = true;
  _id = 0;
  language = [];
  /**
   * ViewChild check of banner image object
   */
  @ViewChild('bannerUpload', { static: false }) set bannerUpload(element) {
    if (GlobalUtils.isBrowser() && element && this.bannerUri) {
      document.getElementById('bannerUpload').style.background =
        'url(' + this.bannerUri + ')';
    }
  }

  /**
   * constructor of the page
   * @param dialog
   * @param adminService
   */
  constructor(
    private dialog: MatDialog,
    private adminService: AdminService,
    private constantsService: ConstantsService,
    private translateService: TranslateService,
    private fb: FormBuilder,
    public languageService: LanguageService
  ) {}

  /**
   * Page Initialization
   */
  ngOnInit(): void {
    this.language = ConstantsService?.language;

    this.ownerForm = new FormGroup({
      title: this.fb.group(
        this.constantsService.languageFC([Validators.required])
      ),
      sub_title: this.fb.group(
        this.constantsService.languageFC([Validators.required])
      ),
      button_text: this.fb.group(
        this.constantsService.languageFC([Validators.required])
      ),
      order: new FormControl('', [Validators.required]),
      destination: new FormControl('', [
        Validators.required,
        Validators.pattern(
          '(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?'
        ),
      ]),
    });
    this.getBanners();
  }

  /**
   * method to change the status or order of banners
   * @param e
   * @param u_id
   */
  updateRow(e, type, u_id) {
    this.isEditable = {};
    if (type === 'status') {
      const blockUserData: InfoPopupComponentData = {
        title: this.translateService.instant(
          'API.BANNER.MODAL.STATUS_CONFIRMATION_MODAL.TITLE'
        ),
        text: this.translateService.instant(
          'API.BANNER.MODAL.STATUS_CONFIRMATION_MODAL.TEXT'
        ),
        type: InfoPopupComponentType.confirm,
        btnText: this.translateService.instant(
          'API.BANNER.MODAL.STATUS_CONFIRMATION_MODAL.BUTTONTEXT'
        ),
      };
      const dialogRef = this.dialog.open(InfoPopupComponent, {
        data: blockUserData,
      });

      dialogRef.afterClosed().subscribe((confirmed) => {
        if (confirmed) {
          this.isLoading = true;
          this.adminService.updateBanner(u_id, { status: 0 }).subscribe(
            (res: any) => {
              this.getBanners();
              const afterBlockData: InfoPopupComponentData = {
                title: this.translateService.instant(
                  'API.BANNER.PUT.SUCCESS_HEADER'
                ),
                text:
                  this.translateService.instant(res.messageCode) || res.message,
                type: InfoPopupComponentType.info,
              };
              this.dialog.open(InfoPopupComponent, { data: afterBlockData });
            },
            (err: any) => {
              this.isLoading = false;
              const afterBlockData: InfoPopupComponentData = {
                title: this.translateService.instant(
                  'API.BANNER.PUT.ERROR_HEADER'
                ),
                text:
                  this.translateService.instant(err.error.messageCode) ||
                  err.error.message,
                type: InfoPopupComponentType.info,
              };
              this.dialog.open(InfoPopupComponent, { data: afterBlockData });
            }
          );
        }
      });
    } else if (type === 'order' && e.target.value) {
      this.isLoading = true;
      this.adminService
        .updateBanner(u_id, { order: +e.target.value })
        .subscribe(
          (res: any) => {
            this.getBanners();
            const afterBlockData: InfoPopupComponentData = {
              title: this.translateService.instant(
                'API.BANNER.PUT.SUCCESS_HEADER'
              ),
              text:
                this.translateService.instant(res.messageCode) || res.message,
              type: InfoPopupComponentType.info,
            };
            this.dialog.open(InfoPopupComponent, { data: afterBlockData });
          },
          (err: any) => {
            this.isLoading = false;
            const afterBlockData: InfoPopupComponentData = {
              title: this.translateService.instant(
                'API.BANNER.PUT.ERROR_HEADER'
              ),
              text:
                this.translateService.instant(err.error.messageCode) ||
                err.error.message,
              type: InfoPopupComponentType.info,
            };
            this.dialog.open(InfoPopupComponent, { data: afterBlockData });
          }
        );
    }
  }

  /**
   * method to upload an image object in page
   */
  bannerUploader() {
    if (GlobalUtils.isBrowser()) {
      const banner: any = document.getElementById('bannerFileUpload');
      banner.click();
      banner.onchange = function () {
        const reader = new FileReader();
        reader.onload = function (event: any) {
          const img = new Image();
          img.onload = function () {
            this.bannerUri = event.target.result;
            document.getElementById('bannerUpload').style['background'] =
              'url(' + event.target.result + ')';
          }.bind(this);
          img.onerror = function () { };
          img.src = window.URL.createObjectURL(banner.files[0]);
        }.bind(this);
        reader.readAsDataURL(banner.files[0]);
      }.bind(this);
    }
  }

  /**
   * method to get list of all the banners with active status
   */
  getBanners() {
    this.isLoading = true;
    this.adminService.getBanner().subscribe((res: any) => {
      this.advertisementList = [...res.data];
      this.isLoading = false;
      this.showAdList = true;
    });
  }

  /**
   * method to check the form controls the validations
   * @param controlName
   * @param errorName
   */
  public hasError = (controlName: string, errorName: string) => {
    return this.ownerForm.controls[controlName].hasError(errorName);
  };

  /**
   * method to respond to the performing actions
   * @param type
   * @param data
   */
  advertisementAction(type, data) {
    if (type === 'view') {
      this.viewAd(data);
    }
  }

  /**
   * method to toggle the components on the page i.e. the table and the form
   */
  toggleBannerList() {
    this.resetBannerControls();
    this.showAdList = !this.showAdList;
  }

  /**
   * method to be called before saving the data to check the form status
   * and the destination url
   */
  upload() {
    if (this.ownerForm.invalid) {
      return;
    }
    if (this.ownerForm.value.destination) {
      const _url = this.ownerForm.value.destination;
      window
        .fetch(_url)
        .then((response) => {
          if (response.status == 200) {
            this.uploadBanner();
          }
        })
        .catch((err) => {
          this.ownerForm.controls['destination'].setErrors({ pattern: true });
          return;
        });
    }
  }

  /**
   * method to be called after form check to upload the data
   */
  uploadBanner() {
    if (this.bannerUri) {
      const formData = {
        ...this.ownerForm.value,
        bannerFileUrl: this.bannerUri,
      };
      this.isLoading = true;
      let subscriber;
      subscriber =
        this._id != 0
          ? this.adminService.updateBanner(this._id, formData)
          : this.adminService.addbanner(formData);
      subscriber.subscribe(
        (res: any) => {
          const afterBlockData: InfoPopupComponentData = {
            title: this.translateService.instant(
              'API.BANNER.POST.SUCCESS_HEADER'
            ),
            text: this.translateService.instant(res.messageCode) || res.message,
            type: InfoPopupComponentType.info,
          };
          this.dialog.open(InfoPopupComponent, { data: afterBlockData });
          this.isLoading = false;
          this.resetBannerControls();
          this.getBanners();
        },
        (err) => {
          const afterBlockData: InfoPopupComponentData = {
            title: this.translateService.instant(
              'API.BANNER.POST.ERROR_HEADER'
            ),
            text:
              this.translateService.instant(err.error.messageCode) ||
              err.error.message,
            type: InfoPopupComponentType.info,
          };
          this.dialog.open(InfoPopupComponent, { data: afterBlockData });
        }
      );
    }
  }

  /**
   * method to be called to view the row data of the table in the form
   * @param rowData
   */
  viewAd(rowData) {
    this.ownerForm.patchValue({
      ...rowData,
    });
    this._id = rowData._id;
    this.bannerUri = rowData.bannerFileUrl;
    this.ownerForm.setErrors(null);
    this.showAdList = false;
  }

  /**
   * method to be called before to reset the form controls
   */
  resetBannerControls() {
    this.bannerUri = '';
    this._id = 0;
    this.ownerForm.reset();
  }
}
