import { Component, Input, OnInit } from '@angular/core';

import { AdminService } from '../../../../core/service/admin.service';

import {
  InfoPopupComponent,
  InfoPopupComponentData,
  InfoPopupComponentType,
} from '../../../../shared/popups/info-popup/info-popup.component';

import { faSpinner } from '@fortawesome/free-solid-svg-icons';
import { MatDialog } from '@angular/material/dialog';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-prize-disbursal',
  templateUrl: './prize-disbursal.component.html',
  styleUrls: [
    './prize-disbursal.component.scss',
    '../esports-payment-details/esports-payment-details.component.scss',
  ],
})
export class PrizeDisbursalComponent implements OnInit {
  @Input() index;
  @Input() sportDetail;
  @Input() tournamentId;
  @Input() winner;

  componentLoader: boolean = true;

  status = '';

  paypalObject: any = {};

  faSpinner = faSpinner;

  constructor(
    private adminService: AdminService,
    private translateService: TranslateService,
    public dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.fetchStatus();
  }

  fetchStatus() {
    this.componentLoader = true;
    this.adminService
      .checkDisbursal({
        userId: this.sportDetail[this.index].winnerUser._id,
        tournamentId: this.tournamentId,
      })
      .subscribe(
        (res: any) => {
          this.componentLoader = false;
          if (res.data && res.data.provider == 'paypal') {
            this.status =
              res.data.status == 'SUCCESS' ||
              res.data.status == 'UNCLAIMED' ||
              res.data.status == 'PENDING'
                ? res.data.status
                : '';
          } else {
            this.status = '';
          }
        },
        (err) => {}
      );
  }

  disburseAmount(index, type) {
    if (type == 'paypal') {
      this.paypalObject.tournamentId = this.tournamentId;
      this.paypalObject.playerId = this.sportDetail[index].winnerUser._id;
      this.paypalObject.emailId = this.sportDetail[
        index
      ].winnerUser.accountDetail.paymentAccountId;
      this.update();
    }
  }

  update() {
    if (true) {
      this.componentLoader = true;
      this.adminService.paypalCall(this.paypalObject).subscribe(
        (res: any) => {
          this.componentLoader = false;
          this.fetchStatus();

          const afterBlockData: InfoPopupComponentData = {
            title: this.translateService.instant(
              'API.PRODUCT.POST.SUCCESS_HEADER'
            ),
            text: res.message,
            type: InfoPopupComponentType.info,
          };

          this.dialog.open(InfoPopupComponent, { data: afterBlockData });
          this.paypalObject = {};
        },
        (err: any) => {
          this.componentLoader = false;
          const afterBlockData: InfoPopupComponentData = {
            title: this.translateService.instant(
              'API.PRODUCT.POST.ERROR_HEADER'
            ),
            text: err.error.message,
            type: InfoPopupComponentType.info,
          };
          this.dialog.open(InfoPopupComponent, { data: afterBlockData });
        }
      );
    }
  }
}
