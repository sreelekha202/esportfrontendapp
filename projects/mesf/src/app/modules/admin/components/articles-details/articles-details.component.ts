import { Component, OnInit, Input } from '@angular/core';
import {
  faEye,
  faTrashAlt,
  faCheckSquare,
} from '@fortawesome/free-solid-svg-icons';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { IArticle } from '../../../../shared/models/articles';
import { ArticleApiService, ConstantsService } from '../../../../core/service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-dynasty-articles-details',
  templateUrl: './articles-details.component.html',
  styleUrls: ['./articles-details.component.scss'],
})
export class ArticlesDetailsComponent implements OnInit {
  faEye = faEye;
  faTrash = faTrashAlt;
  faCheckSquare = faCheckSquare;
  modelTitle: string;
  modelDesc: string;
  modelHeader: string;
  selectedId: string;
  hideBtn = false;
  articleModel: IArticle;
  viewModelBtnDisable = false;
  showRequestEditBtn = false;

  @Input() articlesList: IArticle[] = [];
  @Input() viewIconShow;
  @Input() deleteIconShow;
  @Input() checkIconShow;

  constructor(
    private modalService: NgbModal,
    private articleService: ArticleApiService,
    private translateService: TranslateService
  ) {}

  ngOnInit(): void {}

  /**
   * Open model pop on button click
   * @param content -content
   */
  open(content, Id, modelType) {
    this.selectedId = Id;
    this.hideBtn = true;
    this.showRequestEditBtn = false;
    // Check to open model and configure inner content
    if (modelType === 'requestEdit') {
      this.showRequestEditBtn = true;
      this.setModelConfiguration(
        ConstantsService.Model.RequestEdit.header,
        ConstantsService.Model.RequestEdit.title,
        ConstantsService.Model.RequestEdit.description
      );
    } else {
      this.setModelConfiguration(
        ConstantsService.Model.Delete.header,
        ConstantsService.Model.Delete.title,
        ConstantsService.Model.Delete.description
      );
    }
    this.modalService.open(content, {
      size: 'lg',
      centered: true,
      scrollable: true,
      windowClass: 'custom-modal-content',
    });
  }

  /**
   * Open model to view the articles
   * @param model : model
   * @param Id : Set Selected Id
   * @param Type : Check type of model to hide/show the button
   */
  viewArticle(model, Id, Type) {
    this.viewModelBtnDisable = Type === 'view' ? true : false;
    this.modalService.open(model, {
      size: 'lg',
      centered: true,
      scrollable: true,
      windowClass: 'custom-modal-content',
    });
    this.getArticleById(Id);
  }

  // Abort Articles based on Selected Id
  abort() {
    this.articleService.delArticle(this.selectedId).subscribe((res) => {
      if (res) {
        this.setModelConfiguration(
          ConstantsService.Model.Abort.header,
          ConstantsService.Model.Abort.title,
          ConstantsService.Model.Abort.description
        );
        // Remove Object from list based on Index
        const currIndex = this.articlesList.findIndex(
          (resp) => resp.id === this.selectedId
        );
        this.articlesList.splice(currIndex, 1);
        this.hideBtn = false;
      }
    });
  }

  /**
   * Confiure model
   * @param header : header value
   * @param title : Title value
   * @param description : Description value
   */
  setModelConfiguration(header, title, description) {
    this.modelTitle = this.translateService.instant(title);
    this.modelDesc = this.translateService.instant(description);
    this.modelHeader = this.translateService.instant(header);
  }

  // get articles details based on Id
  getArticleById(Id) {
    this.articleService.getArticleByID(Id).subscribe(
      (data: any) => {
        this.articleModel = data.data[0];
      },
      (err) => {}
    );
  }
}
