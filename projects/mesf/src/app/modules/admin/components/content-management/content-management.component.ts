import { Component, OnInit, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { IPagination } from '../../../../shared/models';

import {
  ArticleApiService,
  ConstantsService,
  LanguageService,
  UtilsService,
  OptionService,
  ToastService,
} from '../../../../core/service';
import { IArticle } from '../../../../shared/models/articles';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { MatTabChangeEvent } from '@angular/material/tabs';
import {
  InfoPopupComponent,
  InfoPopupComponentData,
  InfoPopupComponentType,
} from '../../../../shared/popups/info-popup/info-popup.component';
import { MatDialog } from '@angular/material/dialog';
import { TranslateService } from '@ngx-translate/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-content-management',
  templateUrl: './content-management.component.html',
  styleUrls: ['./content-management.component.scss'],
})
export class ContentManagementComponent implements OnInit {
  @ViewChild(DatatableComponent) table: DatatableComponent;
  tempArticleList = [];
  activeTabIndex;
  columnsFirstTab = [
    { name: 'Article Name' },
    { name: 'Game' },
    { name: 'Author Name' },
    { name: 'Publish Date' },
    { name: 'Likes' },
    { name: 'Views' },
    { name: '' },
  ];
  columnsSecondTab = [
    { name: 'Article Name' },
    { name: 'Game' },
    { name: 'Author Name' },
    { name: 'Publish Date' },
    { name: 'Likes' },
    { name: 'Views' },
    { name: '' },
  ];
  articleModel: any;
  modelTitle: string;
  modelDesc: string;
  modelHeader: string;
  selectedId: string;
  hideBtn = false;
  viewModelBtnDisable = false;
  showRequestEditBtn = false;
  articlesList: IArticle[] = [];
  changeRequestModel: string;
  isLoading = false;
  isFullLoading = false;
  page: IPagination;
  paginationData = {
    page: 1,
    limit: 50,
    sort: { createdDate: -1 },
  };
  tagsList = [];

  constructor(
    private articlesService: ArticleApiService,
    private modalService: NgbModal,
    private activeRoute: ActivatedRoute,
    private router: Router,
    private toastService: ToastService,
    private optionService: OptionService,
    public dialog: MatDialog,
    private translateService: TranslateService,
    public languageService: LanguageService,
    public utilsService: UtilsService
  ) {}

  ngOnInit(): void {
    this.activeRoute.queryParams.subscribe((params) => {
      if (params.activeTab) {
        this.activeTabIndex = +params.activeTab;
        const e = new MatTabChangeEvent();
        e.index = this.activeTabIndex;
        this.onTabChanged(e);
      } else {
        this.activeTabIndex = 0;
      }
    });
    this.getHighlightedArticles();
    this.fetchData();
  }

  fetchData = async () => {
    const option = await Promise.all([this.optionService.fetchAllTags()]);
    this.tagsList = option[0]?.data;
  };

  updateFeature(e, id) {
    this.isLoading = true;
    this.articlesService.updateArticle(id, { isFeature: e.checked }).subscribe(
      (res: any) => {
        this.isLoading = false;
        const query = JSON.stringify({
          articleStatus: ConstantsService.Status.Publish,
        });
        this.getArticlesDetailsByStatus(query);
        const afterBlockData: InfoPopupComponentData = {
          title: this.translateService.instant(
            'API.TOURNAMENT.PUT.SUCCESS_HEADER'
          ),
          text: res.message,
          type: InfoPopupComponentType.info,
        };
        this.dialog.open(InfoPopupComponent, { data: afterBlockData });
      },
      (err: any) => {
        this.isLoading = false;
        const afterBlockData: InfoPopupComponentData = {
          title: this.translateService.instant(
            'API.TOURNAMENT.PUT.ERROR_HEADER'
          ),
          text: err.error.message,
          type: InfoPopupComponentType.info,
        };
        this.dialog.open(InfoPopupComponent, { data: afterBlockData });
      }
    );
  }

  pageChanged(page): void {
    this.paginationData.page = page;
    const query = JSON.stringify({
      articleStatus: ConstantsService.Status.Publish,
    });
    this.getArticlesDetailsByStatus(query);
  }

  // Get Articles details
  getArticlesDetailsByStatus(query) {
    this.isLoading = true;
    //const option = JSON.stringify({ sort: { createdDate: -1 } });
    const pagination = JSON.stringify(this.paginationData);
    this.articlesService
      .getPaginatedArticles({ pagination: pagination, query: query })
      .subscribe(
        (res: any) => {
          if (res && res.data) {
            this.page = {
              totalItems: res?.data?.totalDocs,
              itemsPerPage: res?.data?.limit,
              maxSize: 5,
            };
            const response = res.data.docs.map((element) => {
              return {
                id: element._id,
                title: element.title.english,
                fullText: element?.fullText,
                game: element.gameDetails?.name,
                authorName: element.authorDetails?.fullName,
                createdDate: element.createdDate,
                likes: element.likes,
                isFeature: element?.isFeature,
                views: element.views,
              };
            });
            this.tempArticleList = [...response];
            this.articlesList = response || [];
          }
          this.isLoading = false;
        },
        (err) => {
          this.isLoading = false;
        }
      );
  }
  // get Trending Articles
  getHighlightedArticles() {
    this.isLoading = true;
    this.articlesService.getTrendingPosts().subscribe(
      (res: any) => {
        if (res && res.data) {
          const response = res.data.map((element) => {
            return {
              id: element._id,
              title: element.title.english,
              game: element.gameDetails?.name,
              fullText: element?.fullText,
              authorName: element.authorDetails?.fullName,
              createdDate: element.createdDate,
              likes: element.likes,
              views: element.views,
            };
          });
          this.tempArticleList = [...response];
          this.articlesList = response || [];
        }
        this.isLoading = false;
      },
      (err) => {
        this.isLoading = false;
      }
    );
  }

  /**
   * Get the article list based on Tab selection
   * @param tabChangeEvent : Event on Change of Tab
   */
  onTabChanged = (tabChangeEvent: MatTabChangeEvent): void => {
    this.activeTabIndex = tabChangeEvent.index;
    this.router.navigate(['.'], {
      relativeTo: this.activeRoute,
      queryParams: { activeTab: tabChangeEvent.index },
    });
    this.articlesList = [];
    if (tabChangeEvent.index === 0) {
      // Call an API to get the user details for Recently Updated Tab
      this.getHighlightedArticles();
    } else if (tabChangeEvent.index === 1) {
      const query = JSON.stringify({
        articleStatus: ConstantsService.Status.SubmitForApprove,
      });
      // Call an API to get the user details for Approval Pending
      this.getArticlesDetailsByStatus(query);
    } else if (tabChangeEvent.index === 2) {
      const query = JSON.stringify({
        articleStatus: ConstantsService.Status.Publish,
      });
      // Call an API to get the user details for Published Articles
      this.getArticlesDetailsByStatus(query);
    }
  };

  // Search filter
  updateFirstTabFilter(event) {
    const value = event.target.value.toLowerCase().trim();
    // get the amount of columns in the table
    const count = this.columnsFirstTab.length;
    const keys = Object.keys(this.tempArticleList[0]);
    // assign filtered matches to the active datatable
    this.articlesList = this.tempArticleList.filter((item) => {
      // iterate through each row's column data
      for (let i = 0; i < count; i++) {
        // check for a match
        if (
          (item[keys[i]] &&
            item[keys[i]].toString().toLowerCase().includes(value)) ||
          !value
        ) {
          // found match, return true to add to result set
          return true;
        }
      }
    });
    // Whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }

  /**
   * Open model to view the articles
   * @param model : model
   * @param Id : Set Selected Id
   * @param Type : Check type of model to hide/show the button
   */
  viewArticle(model, Id, Type) {
    this.selectedId = Id;
    this.viewModelBtnDisable = Type === 'view' ? true : false;
    this.modalService.open(model, {
      size: 'lg',
      centered: true,
      scrollable: true,
      windowClass: 'custom-modal-content',
    });
    this.getArticleById(Id);
  }

  // get articles details based on Id
  getArticleById(Id) {
    this.articlesService.getArticleByID(Id).subscribe(
      (data: any) => {
        this.articleModel = data.data[0];
      },
      (err) => {}
    );
  }

  /**
   * Open model pop on button click
   * @param content -content
   */
  open(content, Id, modelType) {
    this.selectedId = Id;
    this.hideBtn = true;
    this.showRequestEditBtn = false;
    // Check to open model and configure inner content
    if (modelType === 'requestEdit') {
      this.showRequestEditBtn = true;
      this.setModelConfiguration(
        ConstantsService.Model.RequestEdit.header,
        ConstantsService.Model.RequestEdit.title,
        ConstantsService.Model.RequestEdit.description
      );
    } else {
      this.setModelConfiguration(
        ConstantsService.Model.Delete.header,
        ConstantsService.Model.Delete.title,
        ConstantsService.Model.Delete.description
      );
    }
    this.modalService.open(content, {
      size: 'lg',
      centered: true,
      scrollable: true,
      windowClass: 'custom-modal-content',
    });
  }

  /**
   * Confiure model
   * @param header : header value
   * @param title : Title value
   * @param description : Description value
   */
  setModelConfiguration(header, title, description) {
    this.modelTitle = this.translateService.instant(title);
    this.modelDesc = this.translateService.instant(description);
    this.modelHeader = this.translateService.instant(header);
  }

  // Abort Articles based on Selected Id
  trash() {
    this.modalService.dismissAll();
    this.isFullLoading = true;
    const formValues = {
      articleStatus: ConstantsService.Status.Delete,
    };
    this.articlesService.updateArticle(this.selectedId, formValues).subscribe(
      (res) => {
        if (res) {
          this.SuccessModel(
            ConstantsService.Model.Article_Deleted.header,
            ConstantsService.Model.Article_Deleted.description
          );
          // Remove Object from list based on Index
          const articleObject = this.articlesList;
          this.articlesList = articleObject.filter(
            (resp) => resp.id !== this.selectedId
          );
          this.hideBtn = false;
        }
        this.isFullLoading = false;
      },
      (err) => {
        this.isFullLoading = false;
      }
    );
  }

  submitRequestEdit() {
    const formValues = {
      articleStatus: ConstantsService.Status.RequestEdit,
      changeRequest: this.changeRequestModel,
    };
    this.updateArticles(formValues);
  }

  approvePublish() {
    const formValues = {
      articleStatus: ConstantsService.Status.Publish,
    };
    this.updateArticles(formValues);
  }

  // Update Articles
  updateArticles(formValues) {
    if (this.selectedId) {
      this.modalService.dismissAll();
      this.isFullLoading = true;
      this.articlesService.updateArticle(this.selectedId, formValues).subscribe(
        (res) => {
          this.SuccessModel(
            ConstantsService.Success.Title,
            ConstantsService.Success.ArticlesUpdateSuccess
          );
          // Remove Object from list based on Index
          const articleObject = this.articlesList;
          this.articlesList = articleObject.filter(
            (resp) => resp.id !== this.selectedId
          );
          this.hideBtn = false;
          this.isFullLoading = false;
        },
        (error) => {
          this.toastService.showError(
            this.translateService.instant(
              ConstantsService.APIError.ArticlesUpdateError
            )
          );
          this.isFullLoading = false;
        }
      );
    }
  }
  // Display the model pop up for success message
  SuccessModel(title, text) {
    const data: InfoPopupComponentData = {
      title: this.translateService.instant(title),
      text: this.translateService.instant(text),
      type: InfoPopupComponentType.info,
    };
    this.dialog.open(InfoPopupComponent, { data });
  }
}
