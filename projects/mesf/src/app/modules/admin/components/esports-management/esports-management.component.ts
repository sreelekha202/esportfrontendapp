import { Component, OnInit } from '@angular/core';
import { MatTabChangeEvent } from '@angular/material/tabs';
import { AdminService } from '../../../../core/service/admin.service';
import {
  InfoPopupComponentData,
  InfoPopupComponentType,
  InfoPopupComponent,
} from '../../../../shared/popups/info-popup/info-popup.component';
import { ITournament } from '../../../../shared/models';
import { UserNotificationsService } from '../../../../core/service';
import { MatDialog } from '@angular/material/dialog';
import { AppHtmlRoutes } from '../../../../app-routing.model';
import { TranslateService } from '@ngx-translate/core';
import { IPagination } from '../../../../shared/models';
import {
  ConstantsService,
  TournamentService,
  UserService,
} from '../../../../core/service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-esports-management',
  templateUrl: './esports-management.component.html',
  styleUrls: ['./esports-management.component.scss'],
})
export class EsportsManagementComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;

  checkIconShow: boolean = false;
  deleteIconShow: boolean = false;
  editIconShow: boolean = false;
  isLoading: boolean = false;
  manageIconShow: boolean = false;
  viewIconShow: boolean = false;

  activeTabIndex;
  page: IPagination;
  userId;

  selectedTab: string = 'ongoing';
  statusText: string = ConstantsService.Status.Live;

  allTournamentList: any = [];
  finishedTournament: any = [];
  tournamentList = [];

  paginationData = {
    page: 1,
    limit: 50,
    sort: 'startDate',
  };

  constructor(
    private activeRoute: ActivatedRoute,
    private adminService: AdminService,
    private router: Router,
    private dialog: MatDialog,
    private tournamentService: TournamentService,
    private translateService: TranslateService
  ) {}

  ngOnInit(): void {
    this.activeRoute.queryParams.subscribe((params) => {
      if (params.activeTab) {
        this.activeTabIndex = +params.activeTab;
        this.switchData(this.activeTabIndex);
      } else {
        this.activeTabIndex = 0;
        this.getData('1', 'Latest');
        this.setConfiguration(true, true, false, false, true);
      }
    });
  }

  pageChanged(page): void {
    this.paginationData.page = page;
    switch (this.activeTabIndex) {
      case 0:
        this.getData('1', 'Latest'); //ongoing
        break;
      case 1:
        this.getData('0', 'Latest'); // upcoming
        break;
      case 2:
        this.getData('2', 'Latest'); //past
        break;
      case 4:
        this.getTournamentForPendingPayments();
        break;
      default:
        break;
    }
  }

  getTournamentForPendingPayments() {
    this.isLoading = true;

    this.adminService.getTournamentsforPayments().subscribe(
      (res: any) => {
        const response = res.data.map((element) => ({
          _id: element._id,
          game: element.gameDetail?.name,
          isFeature: element.isFeature,
          isPaid: element.isPaid,
          maxParticipants: element.maxParticipants,
          name: element.name,
          region: element.regionsAllowed,
          startDate: element.startDate,
          tournamentType: element.tournamentType,
        }));

        this.isLoading = false;
        this.finishedTournament = response;
      },
      (err: any) => {
        this.isLoading = false;
      }
    );
  }

  /**
   * method to update the modifications on tournaments
   * @param data
   */
  modifyTournamentHandler(data) {
    if (data.type === 'abort') {
      this.switchData(this.activeTabIndex);
    } else if (data.type === 'feature') {
      this.tournamentService
        .updateTournament({ isFeature: data.value }, data.id)
        .subscribe(
          (res: any) => {
            this.getData('0', 'Latest');

            const afterBlockData: InfoPopupComponentData = {
              title: this.translateService.instant(
                'API.TOURNAMENT.PUT.SUCCESS_HEADER'
              ),
              text: res.message,
              type: InfoPopupComponentType.info,
            };
            this.dialog.open(InfoPopupComponent, { data: afterBlockData });
          },
          (err: any) => {
            const afterBlockData: InfoPopupComponentData = {
              title: this.translateService.instant(
                'API.TOURNAMENT.PUT.ERROR_HEADER'
              ),
              text:
                this.translateService.instant(err.error.messageCode) ||
                err.error.message,
              type: InfoPopupComponentType.info,
            };
            this.dialog.open(InfoPopupComponent, { data: afterBlockData });
          }
        );
    } else if (data.type === 'update') {
      this.switchData(data.value);
    } else if (data.type === 'updatePaymentTable') {
      this.getTournamentForPendingPayments();
    }
  }

  /**
   * Get the tournament list based on Tab selection
   * @param tabChangeEvent : Event on Change of Tab
   */
  onTabChanged = (tabChangeEvent: MatTabChangeEvent): void => {
    this.activeTabIndex = tabChangeEvent.index;
    this.router.navigate(['.'], {
      relativeTo: this.activeRoute,
      queryParams: { activeTab: tabChangeEvent.index },
    });
    this.switchData(tabChangeEvent.index);
  };

  switchData(index) {
    switch (index) {
      case 0:
        this.resetPage();
        this.setConfiguration(true, true, false, false, true);
        this.getData('1', 'Latest'); //ongoing
        break;
      case 1:
        this.resetPage();
        this.setConfiguration(true, true, false, true, true);
        this.getData('0', 'Latest'); // upcoming
        break;
      case 2:
        this.resetPage();
        this.setConfiguration(true, true, false, false, true);
        this.getData('2', 'Latest'); //past
        break;
      case 3:
        this.setConfiguration(true, true, true, true, false);
        this.getTournamentDetailsForApprovalPending();
        break;
      case 4:
        this.getTournamentForPendingPayments();
        break;
      default:
        break;
    }
  }

  resetPage() {
    this.paginationData = {
      page: 1,
      limit: 50,
      sort: 'startDate',
    };
    this.tournamentList = [];
  }

  getData(status, sortOrder) {
    let params = {
      limit: this.paginationData.limit,
      page: this.paginationData.page,
      sort: sortOrder,
      status: status,
    };

    this.isLoading = true;
    this.tournamentService.getPaginatedTournaments(params).subscribe(
      (res: any) => {
        this.isLoading = false;
        this.mapTournamentFilterList(res.data.docs);
        this.page = {
          totalItems: res?.data?.totalDocs,
          itemsPerPage: res?.data?.limit,
          maxSize: 5,
        };
      },
      (err) => {
        this.isLoading = false;
      }
    );
  }

  getTournamentDetailsForApprovalPending() {
    this.isLoading = true;
    this.tournamentList = [];

    const query = JSON.stringify({
      tournamentStatus: 'submitted_for_approval',
    });

    this.tournamentService.getTournaments({ query: query }).subscribe(
      (res) => {
        if (res && res.data) {
          let response = res.data || [];
          this.mapTournamentFilterList(response);
        }
        this.isLoading = false;
      },
      (err) => {
        this.isLoading = false;
      }
    );
  }

  /**
   * Map the tournament details to display in the table
   * @param tournamentList : response from method
   */
  mapTournamentFilterList(tournamentList) {
    const response = tournamentList.map((element) => ({
      _id: element._id,
      game: element.gameDetail?.name,
      isFeature: element.isFeature,
      isPaid: element.isPaid,
      maxParticipants: element.maxParticipants,
      name: element.name,
      region: element.regionsAllowed,
      slug: element.slug,
      startDate: element.startDate,
      tournamentType: element.tournamentType,
    }));

    this.tournamentList = response;
  }

  /**
   * Configure icon based on boolean value
   * @param viewIcon : show/hide icon
   * @param deleteIcon:show/hide icon
   * @param checkIcon:show/hide icon
   */
  setConfiguration(viewIcon, deleteIcon, checkIcon, editIcon, manageIcon) {
    this.viewIconShow = viewIcon;
    this.deleteIconShow = deleteIcon;
    this.checkIconShow = checkIcon;
    this.editIconShow = editIcon;
    this.manageIconShow = manageIcon;
  }
}
