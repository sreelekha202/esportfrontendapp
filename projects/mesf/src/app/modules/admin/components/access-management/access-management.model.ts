export interface AccessManagementUser {
  userId: number;
  name: string;
  email: string;
  profilePicture: string;
  accessLevel: UserAccessType[];
  phone: string;
  buttons: string;
}

export enum UserAccessType {
  em = 'em',
  un = 'un',
  cm = 'cm',
  um = 'um',
  sc = 'sc',
  acm = 'acm',
}

export const AccessType = {
  em: 'ADMIN.ACCESS_MANAGEMENT.LABEL.ESPORTS_MANAGEMENT',
  un: 'ADMIN.ACCESS_MANAGEMENT.LABEL.USER_NOTIFICATION',
  cm: 'ADMIN.ACCESS_MANAGEMENT.LABEL.CONTENT_MANAGEMENT',
  um: 'ADMIN.ACCESS_MANAGEMENT.LABEL.USER_MANAGEMENT',
  sc: 'ADMIN.ACCESS_MANAGEMENT.LABEL.SITE_CONFIGURATION',
  acm: 'ADMIN.ACCESS_MANAGEMENT.LABEL.ACCESS_MANAGEMENT',
};
