import { MatDialog } from '@angular/material/dialog';
import { Component, OnInit } from '@angular/core';
import { IPagination } from '../../../../shared/models';
import { MatTabChangeEvent } from '@angular/material/tabs';
import {
  InfoPopupComponent,
  InfoPopupComponentData,
  InfoPopupComponentType,
} from '../../../../shared/popups/info-popup/info-popup.component';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import {
  ConstantsService,
  UserService,
  TournamentService,
} from '../../../../core/service';
import { IReward } from '../../../../shared/models/reward';
import { AppHtmlRoutes } from '../../../../app-routing.model';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-view-user-details',
  templateUrl: './view-user-details.component.html',
  styleUrls: ['./view-user-details.component.scss'],
})
export class ViewUserDetailsComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;
  // rowsFirstTab = [];
  userId: string;
  userObject: any;
  rewardList: IReward[] = [];
  createdTournamentList: any[] = [];
  joinedTournamentList: any[] = [];
  isLoading = false;
  // Hide OR Show the button based on Role
  isUserActive = false;
  isUserVerified = false;
  isUserAuthor = false;
  isUserInfluencer = false;
  page: IPagination;
  columnsFirstTab = [
    { name: 'Id' },
    { name: 'Full Name' },
    { name: 'Email' },
    { name: 'Phone Number' },
    { name: 'Sign Up From' },
    { name: 'Member Since' },
  ];
  rewardTransColumnTab = [
    { name: 'Date' },
    { name: 'Balance Reward' },
    { name: 'Reward Type' },
    { name: 'Description' },
    { name: 'Status' },
    { name: 'Expiry Date' },
  ];

  tournamentColumnTab = [
    { name: 'Tournament Name' },
    { name: 'Registration' },
    { name: 'Game' },
    { name: 'Region' },
    { name: 'Type' },
    { name: 'Start Date' },
    { name: '' },
  ];

  constructor(
    public dialog: MatDialog,
    private location: Location,
    private activatedRoute: ActivatedRoute,
    private userService: UserService,
    private tournamentService: TournamentService,
    private translateService: TranslateService
  ) {
    // this.rowsFirstTab = [...UserFirstTab];
  }

  ngOnInit(): void {
    this.getUserIdFromActiveRoute();
  }

  // Get the user details with all other tabs
  getUserIdFromActiveRoute() {
    this.activatedRoute.params.subscribe((params) => {
      this.userId = params['id'];
      if (this.userId) {
        this.getUserDetailsById();
        this.getRewardTransaction();
      }
    });
  }

  getJoinedTournamentQuery() {
    return {
      pagination: JSON.stringify(this.pagination),
      query: JSON.stringify({
        userId: this.userId,
      }),
    };
  }
  pagination = {
    page: 1,
    limit: 50,
    sort: 'startDate',
    projection: [
      '_id',
      'name',
      'description',
      'startDate',
      'regionsAllowed',
      'tournamentType',
    ],
  };

  pageChanged(page): void {
    this.pagination.page = page;
    this.getJoinedTournamentDetails();
  }

  // Get Joined tournament details
  async getJoinedTournamentDetails() {
    this.isLoading = true;
    const payload = await this.getJoinedTournamentQuery();
    const tournament = await this.tournamentService
      .getJoinedTournamentsByUser(payload)
      .toPromise();
    this.page = {
      totalItems: tournament?.data?.totalDocs,
      itemsPerPage: tournament?.data?.limit,
      maxSize: 5,
    };
    this.joinedTournamentList =
      tournament?.data?.docs.map(
        (element) => {
          return {
            id: element.tournament._id,
            slug: element.slug,
            tournamentName: element.tournament.name,
            registration: element.tournament.isPaid,
            game: element?.game?.name,
            region: element?.tournament?.regionsAllowed,
            type: element?.tournament?.tournamentType,
            startDate: element.tournament.startDate,
          };
        },
        (err) => {
          this.isLoading = false;
        }
      ) || [];
    this.isLoading = false;
  }

  // Get Created tournament details
  getCreatedTournamentDetails() {
    this.isLoading = true;
    const query = JSON.stringify({
      tournamentStatus: 'publish',
      isFinished: false,
      createdBy: this.userId,
    });
    this.tournamentService.getTournaments({ query: query }).subscribe(
      (data) => {
        this.isLoading = false;
        const tournamentDetails = data['data'].slice();
        const response = tournamentDetails.map((element) => {
          return {
            id: element._id,
            slug: element.slug,
            tournamentName: element.name,
            registration: element.isPaid,
            game: element.gameDetail?.name,
            region: element.regionsAllowed,
            type: element.tournamentType,
            startDate: element.startDate,
          };
        });
        this.createdTournamentList = response || [];
      },
      (err) => {
        this.isLoading = false;
      }
    );
  }

  getRewardTransaction() {
    this.isLoading = true;
    const query = JSON.stringify({ user: this.userId });
    this.userService.getRewardTransactionByUserId({ query: query }).subscribe(
      (res) => {
        this.isLoading = false;
        const response = res.data.map((element) => {
          return {
            date: element.createdOn,
            rewardId: element._id,
            slug: element.slug,
            rewardType: element.type,
            description: element.description,
            status: element.status,
            balanceReward: element.reward_value,
            expiryDate: element.expiredOn,
          };
        });
        this.rewardList = response || [];
      },
      (err) => {
        this.isLoading = false;
      }
    );
  }

  getUserDetailsById() {
    this.isLoading = true;
    this.userService.getRecentlyUpdatedUserList().subscribe(
      (res) => {
        this.isLoading = false;
        this.userObject = res.data.find((ele) => ele._id === this.userId);
        if (this.userObject && this.userObject.Status) {
          this.isUserActive = this.userObject.Status === 1 ? true : false;
          this.isUserVerified = this.userObject.isVerified === 1 ? true : false;
          this.isUserInfluencer =
            this.userObject.isInfluencer === 1 ? true : false;
          this.isUserAuthor = this.userObject.isAuthor === 1 ? true : false;
        }
        this.getUserPrefenrece();
      },
      (err) => {
        this.isLoading = false;
      }
    );
  }

  getUserPrefenrece() {
    this.isLoading = true;
    // Get User Preferences
    this.userService.getUserPrefernces(this.userId).subscribe(
      (res) => {
        if (res && res.data) {
          Object.assign(this.userObject, res.data);
        }
        this.isLoading = false;
      },
      (err) => {
        this.isLoading = false;
      }
    );
  }

  goBack() {
    this.location.back();
  }

  onBlockUser() {
    const blockUserData: InfoPopupComponentData = {
      title: this.translateService.instant(
        ConstantsService.Model.Block_Confirmation.header
      ),
      text: this.translateService.instant(
        ConstantsService.Model.Block_Confirmation.title
      ),
      type: InfoPopupComponentType.confirm,
      btnText: this.translateService.instant(
        ConstantsService.Model.Block_Confirmation.button
      ),
    };
    const dialogRef = this.dialog.open(InfoPopupComponent, {
      data: blockUserData,
    });
    dialogRef.afterClosed().subscribe((confirmed) => {
      if (confirmed) {
        this.blockUser();
      }
    });
  }

  blockUser() {
    this.isLoading = true;
    const formValue = {
      Status: 2,
    };
    this.userService.updateUser(formValue, this.userId).subscribe(
      (res) => {
        this.isLoading = false;
        this.isUserActive = !this.isUserActive;
        const userName = this.userObject?.fullName;
        const afterBlockData: InfoPopupComponentData = {
          title: this.translateService.instant(
            ConstantsService.Model.Block.title
          ),
          text: this.translateService.instant(
            ConstantsService.Model.Block.description
          ),
          type: InfoPopupComponentType.info,
        };
        this.dialog.open(InfoPopupComponent, { data: afterBlockData });
      },
      (err) => {
        this.isLoading = false;
      }
    );
  }

  onUnBlockUser() {
    this.isLoading = true;
    const formValue = {
      Status: 1,
    };
    this.userService.updateUser(formValue, this.userId).subscribe(
      (res) => {
        this.isLoading = false;
        this.isUserActive = !this.isUserActive;
        const afterBlockData: InfoPopupComponentData = {
          title: this.translateService.instant(
            ConstantsService.Model.Unblock.title
          ),
          text: this.translateService.instant(
            ConstantsService.Model.Unblock.description
          ),
          type: InfoPopupComponentType.info,
        };
        this.dialog.open(InfoPopupComponent, { data: afterBlockData });
      },
      (err) => {
        this.isLoading = false;
      }
    );
  }

  /**
   * Method to mark as Influencer/remove influencer
   * @param boolVal : Mark user account as Influencer/UnInfluencer
   */
  onInfluencer(boolVal) {
    this.isLoading = true;
    const formValue = {
      isInfluencer: boolVal ? 0 : 1,
    };
    this.userService.updateUser(formValue, this.userId).subscribe(
      (res) => {
        this.isLoading = false;
        this.isUserInfluencer = !this.isUserInfluencer;
        if (boolVal) {
          this.SuccessModel(
            ConstantsService.Model.UserInfluencer.title,
            ConstantsService.Model.UserInfluencer.description
          );
        } else {
          this.SuccessModel(
            ConstantsService.Model.UserUnInfluencer.title,
            ConstantsService.Model.UserUnInfluencer.description
          );
        }
      },
      (err) => {
        this.isLoading = false;
      }
    );
  }

  onTabChanged = (tabChangeEvent: MatTabChangeEvent): void => {
    switch (tabChangeEvent.index) {
      case 0: {
        this.getRewardTransaction();
        break;
      }
      case 1: {
        this.getJoinedTournamentDetails();
        break;
      }
      case 2: {
        this.getCreatedTournamentDetails();
        break;
      }
    }
  };

  /**
   * Method to verified/Unverified the account
   * @param boolVal : Mark user account ad verfied or unverified
   */
  onVerify(boolVal) {
    this.isLoading = true;
    const formValue = {
      isVerified: boolVal ? 0 : 1,
    };
    this.userService.updateUser(formValue, this.userId).subscribe(
      (res) => {
        this.isLoading = false;
        this.isUserVerified = !this.isUserVerified;
        if (boolVal) {
          this.SuccessModel(
            ConstantsService.Model.UserVerified.title,
            ConstantsService.Model.UserVerified.description
          );
        } else {
          this.SuccessModel(
            ConstantsService.Model.UserUnVerified.title,
            ConstantsService.Model.UserUnVerified.description
          );
        }
      },
      (err) => {
        this.isLoading = false;
      }
    );
  }

  /**
   * Method to mark as Author/UnAuthor
   * @param boolVal : Mark user account ad Author or Unauthor
   */
  onAuthor(boolVal) {
    this.isLoading = true;
    const formValue = {
      isAuthor: boolVal ? 0 : 1,
    };
    this.userService.updateUser(formValue, this.userId).subscribe(
      (res) => {
        this.isLoading = false;
        this.isUserAuthor = !this.isUserAuthor;
        if (boolVal) {
          this.SuccessModel(
            ConstantsService.Model.UserAuthor.title,
            ConstantsService.Model.UserAuthor.description
          );
        } else {
          this.SuccessModel(
            ConstantsService.Model.RevokeAuthor.title,
            ConstantsService.Model.RevokeAuthor.description
          );
        }
      },
      (err) => {
        this.isLoading = false;
      }
    );
  }

  /**
   * Model pop up
   * @param title : Display as header
   * @param text : Display as body text
   */
  SuccessModel(title, text) {
    const data: InfoPopupComponentData = {
      title: this.translateService.instant(title),
      text: this.translateService.instant(text),
      type: InfoPopupComponentType.info,
    };
    this.dialog.open(InfoPopupComponent, { data });
  }
}
