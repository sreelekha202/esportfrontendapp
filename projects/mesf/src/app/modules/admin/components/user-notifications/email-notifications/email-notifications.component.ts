import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GlobalUtils } from '../../../../../shared/service/global-utils/global-utils';
import { UserNotificationsService } from '../../../../../core/service';
import {
  InfoPopupComponentData,
  InfoPopupComponentType,
  InfoPopupComponent,
} from '../../../../../shared/popups/info-popup/info-popup.component';
import { TranslateService } from '@ngx-translate/core';
import { MatDialog } from '@angular/material/dialog';
@Component({
  selector: 'app-email-notifications',
  templateUrl: './email-notifications.component.html',
  styleUrls: [
    './email-notifications.component.scss',
    '../user-notifications.component.scss',
  ],
})
export class EmailNotificationsComponent implements OnInit, OnDestroy {
  emailNotificationForm = this.fb.group({
    userSegment: ['', Validators.required],
    emailTitle: ['', Validators.required],
    headerText: ['', Validators.required],
    message: ['', Validators.required],
  });
  isLoading = false;
  @ViewChild('bannerUpload', { static: false }) set bannerUpload(element) {
    if (GlobalUtils.isBrowser() && element && this.headerImageUri) {
      document.getElementById('bannerUpload').style.background =
        'url(' + this.headerImageUri + ')';
    }
  }
  headerImageUri = '';
  userSegementValues = {
    ALL_USERS: this.translateService.instant(
      'ADMIN.USER_NOTIFICATION.LABEL.ALL_USERS'
    ),
    TOURNAMENT_PLAYERS: this.translateService.instant(
      'ADMIN.USER_NOTIFICATION.LABEL.TOURNAMENT_PLAYERS'
    ),
    TOURNAMENT_ORGANIZERS: this.translateService.instant(
      'ADMIN.USER_NOTIFICATION.LABEL.TOURNAMENT_ORGANIZER'
    ),
  };
  emailSent = false;

  constructor(
    private fb: FormBuilder,
    private userNotificationsService: UserNotificationsService,
    private translateService: TranslateService,
    private dialog: MatDialog
  ) {}

  ngOnInit(): void {}

  ngOnDestroy(): void {
    this.resetControls();
  }

  sendEmails() {
    this.isLoading = true;
    const inputData = {
      title: this.emailNotificationForm.value.emailTitle,
      message: this.emailNotificationForm.value.message,
      userSegment: this.emailNotificationForm.value.userSegment,
      headerText: this.emailNotificationForm.value.headerText,
      headerImage: this.headerImageUri,
    };
    this.userNotificationsService.sendEmails(inputData).subscribe(
      (response) => {
        this.resetControls();
        this.isLoading = false;
        const afterBlockData: InfoPopupComponentData = {
          title: 'OK',
          text: response?.message,
          type: InfoPopupComponentType.info,
        };
        this.dialog.open(InfoPopupComponent, { data: afterBlockData });
      },
      (err: any) => {
        this.isLoading = false;
        const afterBlockData: InfoPopupComponentData = {
          title: 'Fail',
          text: err?.error?.message || err.message,
          type: InfoPopupComponentType.info,
        };
        this.dialog.open(InfoPopupComponent, { data: afterBlockData });
      }
    );
  }

  get userSegment() {
    return this.emailNotificationForm.get('userSegment');
  }

  markFormGroupTouched(formGroup: FormGroup) {
    (<any>Object).values(formGroup.controls).forEach((control) => {
      control.markAsTouched();

      if (control.controls) {
        this.markFormGroupTouched(control);
      }
    });
  }

  selectType(e) {
    this.emailNotificationForm.controls['userSegment'].setValue(e);
  }
  updateEmail() {
    if (!this.emailNotificationForm.valid) {
      this.markFormGroupTouched(this.emailNotificationForm);
      return;
    }
    this.sendEmails();
  }
  toggleEditPane() {}
  resetControls() {
    this.emailNotificationForm.reset();
    this.headerImageUri = '';
    this.isLoading = true;
    document.getElementById('bannerUpload').style['background'] = '';
  }
  uploadImage() {
    if (GlobalUtils.isBrowser()) {
      const banner: any = document.getElementById('bannerUploader');
      banner.click();
      banner.onchange = function () {
        const reader = new FileReader();
        reader.onload = function (event: any) {
          const img = new Image();
          img.onload = function () {
            this.headerImageUri = event.target.result;
            document.getElementById('bannerUpload').style['background'] =
              'url(' + event.target.result + ')';
          }.bind(this);
          img.onerror = function () { };
          img.src = window.URL.createObjectURL(banner.files[0]);
        }.bind(this);
        reader.readAsDataURL(banner.files[0]);
      }.bind(this);
    }
  }
  get emailTitle() {
    return this.emailNotificationForm.get('emailTitle');
  }

  public hasError = (controlName: string, errorName: string) => {
    return this.emailNotificationForm.controls[controlName].hasError(errorName);
  };

  get headerText() {
    return this.emailNotificationForm.get('headerText');
  }

  get message() {
    return this.emailNotificationForm.get('message');
  }
}
