import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-sent-notifications',
  templateUrl: './sent-notifications.component.html',
  styleUrls: [
    './sent-notifications.component.scss',
    '../user-notifications.component.scss',
  ],
})
export class SentNotificationsComponent implements OnInit {
  @Input() sentList = [];
  columnsFirstTab = [
    { name: 'Date' },
    { name: 'Type' },
    { name: 'Title' },
    { name: 'User Segment' },
    { name: 'Status' },
  ];
  constructor() {}

  ngOnInit(): void {}
}
