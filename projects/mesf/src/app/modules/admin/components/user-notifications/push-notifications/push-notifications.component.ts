import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GlobalUtils } from '../../../../../shared/service/global-utils/global-utils';
import { UserNotificationsService } from '../../../../../core/service';
import {
  InfoPopupComponentData,
  InfoPopupComponentType,
  InfoPopupComponent,
} from '../../../../../shared/popups/info-popup/info-popup.component';
import { TranslateService } from '@ngx-translate/core';
import { MatDialog } from '@angular/material/dialog';
@Component({
  selector: 'app-push-notifications',
  templateUrl: './push-notifications.component.html',
  styleUrls: [
    './push-notifications.component.scss',
    '../user-notifications.component.scss',
  ],
})
export class PushNotificationsComponent implements OnInit, OnDestroy {
  pushNotificationForm = this.fb.group({
    userSegment: ['', Validators.required],
    title: ['', Validators.required],
    message: ['', Validators.required],
  });
  @ViewChild('bannerUpload', { static: false }) set bannerUpload(element) {
    if (GlobalUtils.isBrowser() && element && this.headerImageUri) {
      document.getElementById('bannerUpload').style.background =
        'url(' + this.headerImageUri + ')';
    }
  }
  isLoading = false;
  headerImageUri = '';
  userSegementValues = {
    ALL_USERS: this.translateService.instant(
      'ADMIN.USER_NOTIFICATION.LABEL.ALL_USERS'
    ),
    TOURNAMENT_PLAYERS: this.translateService.instant(
      'ADMIN.USER_NOTIFICATION.LABEL.TOURNAMENT_PLAYERS'
    ),
    TOURNAMENT_ORGANIZERS: this.translateService.instant(
      'ADMIN.USER_NOTIFICATION.LABEL.TOURNAMENT_ORGANIZER'
    ),
  };
  messageSent = false;

  constructor(
    private fb: FormBuilder,
    private userNotificationsService: UserNotificationsService,
    private translateService: TranslateService,
    private dialog: MatDialog
  ) {}

  ngOnInit(): void {}

  ngOnDestroy(): void {
    this.resetControls();
  }
  sendPushNotifications() {
    this.isLoading = true;
    const inputData = {
      message: this.pushNotificationForm.value.message,
      title: this.pushNotificationForm.value.title,
      userSegment: this.pushNotificationForm.value.userSegment,
    };
    this.userNotificationsService.sendPushNotifications(inputData).subscribe(
      (response: any) => {
        this.resetControls();
        this.isLoading = false;
        const afterBlockData: InfoPopupComponentData = {
          title: 'OK',
          text: response?.message,
          type: InfoPopupComponentType.info,
        };
        this.dialog.open(InfoPopupComponent, { data: afterBlockData });
      },
      (err: any) => {
        this.isLoading = false;
        const afterBlockData: InfoPopupComponentData = {
          title: 'Fail',
          text: err?.error?.message || err?.message,
          type: InfoPopupComponentType.info,
        };
        this.dialog.open(InfoPopupComponent, { data: afterBlockData });
      }
    );
  }

  markFormGroupTouched(formGroup: FormGroup) {
    (<any>Object).values(formGroup.controls).forEach((control) => {
      control.markAsTouched();

      if (control.controls) {
        this.markFormGroupTouched(control);
      }
    });
  }

  selectType(e) {
    this.pushNotificationForm.controls['userSegment'].setValue(e);
  }
  updatePush() {
    if (!this.pushNotificationForm.valid) {
      this.markFormGroupTouched(this.pushNotificationForm);
      return;
    }
    this.sendPushNotifications();
  }
  toggleEditPane() {}
  resetControls() {
    this.pushNotificationForm.reset();
    this.headerImageUri = '';
    document.getElementById('bannerUpload').style['background'] = '';
  }
  uploadImage() {
    if (GlobalUtils.isBrowser()) {
      const banner: any = document.getElementById('bannerUploader');
      banner.click();
      banner.onchange = function () {
        const reader = new FileReader();
        reader.onload = function (event: any) {
          const img = new Image();
          img.onload = function () {
            this.headerImageUri = event.target.result;
            document.getElementById('bannerUpload').style['background'] =
              'url(' + event.target.result + ')';
          }.bind(this);
          img.onerror = function () { };
          img.src = window.URL.createObjectURL(banner.files[0]);
        }.bind(this);
        reader.readAsDataURL(banner.files[0]);
      }.bind(this);
    }
  }
  public hasError = (controlName: string, errorName: string) => {
    return this.pushNotificationForm.controls[controlName].hasError(errorName);
  };

  get userSegment() {
    return this.pushNotificationForm.get('userSegment');
  }

  get message() {
    return this.pushNotificationForm.get('message');
  }

  get title() {
    return this.pushNotificationForm.get('title');
  }
}
