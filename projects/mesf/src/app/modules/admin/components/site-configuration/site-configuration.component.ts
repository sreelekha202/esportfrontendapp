import { faTrash } from '@fortawesome/free-solid-svg-icons';
import { MatTabChangeEvent } from '@angular/material/tabs';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { GlobalUtils } from '../../../../shared/service/global-utils/global-utils';

enum SiteConfigurationTabs {
  siteCustomize,
  rewards,
  Alerts,
  banner,
}

@Component({
  selector: 'app-site-configuration',
  templateUrl: './site-configuration.component.html',
  styleUrls: ['./site-configuration.component.scss'],
})
export class SiteConfigurationComponent implements OnInit {
  siteConfigurationTabs = SiteConfigurationTabs;
  faTrash = faTrash;
  backgroundColour = 'var(--orange-color-main)';
  activeButtonColor = '#0C0E0F';
  nonActiveButtonColor = 'var(--orange-color-main)';
  viewIconShow = true;
  deleteIconShow = true;
  checkIconShow = false;
  activeTabIndex = 0;
  constructor() {}

  ngOnInit(): void {}

  onTabChange(matTab: MatTabChangeEvent): void {
    this.activeTabIndex = matTab.index;
  }
}
