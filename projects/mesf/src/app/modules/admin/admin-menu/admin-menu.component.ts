import { faBell } from '@fortawesome/free-solid-svg-icons';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { filter, map, take } from 'rxjs/operators';
import { UserService } from '../../../core/service';
import { AppHtmlRoutes } from '../../../app-routing.model';
import { IUser } from '../../../shared/models/user';

@Component({
  selector: 'app-admin-menu',
  templateUrl: './admin-menu.component.html',
  styleUrls: ['./admin-menu.component.scss'],
})
export class AdminMenuComponent implements OnInit {
  faBell = faBell;
  currenUser: IUser;
  url;

  constructor(private userService: UserService, private router: Router) {}
  ngOnInit(): void {
    this.url = this.router.url;
    this.userService.currentUser
      .pipe(
        filter((data) => !!data),
        take(1)
      )
      .subscribe((data) => {
        if (data) {
          this.currenUser = data;
          if (this.currenUser.accessLevel.length > 0) {
            let route = '';
            switch (this.url) {
              case '/admin/site-configuration': {
                route = '/admin/site-configuration';
                break;
              }
              case '/admin/access-management': {
                route = '/admin/access-management';
                break;
              }
              case '/admin/esports-management': {
                route = '/admin/esports-management';
                break;
              }
              case '/admin/content-management': {
                route = '/admin/content-management';
                break;
              }
              case '/admin/user-notifications': {
                route = '/admin/user-notifications';
                break;
              }
              case '/admin/user-management': {
                route = '/admin/user-management';
                break;
              }
              default: {
                route = undefined;
                break;
              }
            }
            if (route) {
              this.router.navigate([route]);
            }
          }
        }
      });
  }

  isAllowed(_moduleType) {
    if (this.currenUser && this.currenUser.accessLevel) {
      return this.currenUser.accessLevel.includes(_moduleType) ? true : false;
    }
    return false;
  }

  onLogOut(): void {
    this.userService.logout();
    this.router.navigate([AppHtmlRoutes.login]);
  }
}
