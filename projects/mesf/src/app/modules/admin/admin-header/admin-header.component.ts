import { AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import { faBell } from '@fortawesome/free-solid-svg-icons';
import { MatSidenav } from '@angular/material/sidenav';
import { Router } from '@angular/router';

import { SidenavService } from '../../../shared/service/sidenav/sidenav.service';
import {
  AppHtmlAdminRoutes,
  AppHtmlRoutesLoginType,
  AppHtmlRoutes,
} from '../../../app-routing.model';
import {
  UserService,
  LanguageService,
  ConstantsService,
} from '../../../core/service';
import { IUser } from '../../../shared/models/user';
import { TranslateService } from '@ngx-translate/core';
import { GlobalUtils } from '../../../shared/service/global-utils/global-utils';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-admin-header',
  templateUrl: './admin-header.component.html',
  styleUrls: ['./admin-header.component.scss'],
})
export class AdminHeaderComponent implements OnInit, AfterViewInit, OnDestroy {
  AppHtmlAdminRoutes = AppHtmlAdminRoutes;
  AppHtmlRoutes = AppHtmlRoutes;
  faBell = faBell;
  isMenuOpened = false;
  currentUser: IUser;
  AppLanguage = [];
  activeLang = ConstantsService?.defaultLangCode;
  AppHtmlRoutesLoginType = AppHtmlRoutesLoginType;

  userSubscription: Subscription;
  constructor(
    private sidenavService: SidenavService,
    private userService: UserService,
    private router: Router,
    public translate: TranslateService,
    private languageService: LanguageService
  ) {}

  ngOnInit(): void {
    this.activeLang = this.translate.currentLang;
    this.AppLanguage = ConstantsService?.language;
    this.languageService.setLanguage(this.translate.currentLang);
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.currentUser = data;
      }
    });
  }

  ngOnDestroy() {
    this.userSubscription?.unsubscribe();
  }

  onLanguageChange(lang): void {
    if (GlobalUtils.isBrowser()) {
      localStorage.setItem('currentLanguage', lang);
      this.activeLang = lang;
      this.translate.use(lang);
      this.languageService.setLanguage(lang);
    }
  }

  async ngAfterViewInit(): Promise<void> {
    const sidenav: MatSidenav = await this.sidenavService.getSidenav();

    if (sidenav) {
      sidenav.openedChange.subscribe((isOpened) => {
        this.isMenuOpened = isOpened;
      });
    }
  }

  onLogOut(): void {
    this.userService.logout();
    this.router.navigate([
      AppHtmlRoutes.userPageType,
      AppHtmlRoutesLoginType.phoneLogin,
    ]);
  }

  onMenuToggle(): void {
    this.sidenavService.toggle();
  }
}
