import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { AdvertisementManagementComponent } from './components/advertisement-management/advertisement-management.component';
import { SiteConfigurationComponent } from './components/site-configuration/site-configuration.component';
import { EsportsManagementComponent } from './components/esports-management/esports-management.component';
import { ContentManagementComponent } from './components/content-management/content-management.component';
import { UserNotificationsComponent } from './components/user-notifications/user-notifications.component';
import { AccessManagementComponent } from './components/access-management/access-management.component';
import { ViewUserDetailsComponent } from './components/view-user-details/view-user-details.component';
import { ViewUserAccessComponent } from './components/view-user-access/view-user-access.component';
import { UserManagementComponent } from './components/user-management/user-management.component';
import { AdminComponent } from './admin.component';
import { AdminControlGuard } from '../../shared/guard/admin-control.guard';
import { EsportsPaymentComponent } from './components/esports-payment/esports-payment.component';
import { EsportsPaymentDetailsComponent } from './components/esports-payment-details/esports-payment-details.component';
const routes: Routes = [
  {
    path: '',
    component: AdminComponent,
    canActivate: [AdminControlGuard],
    children: [
      {
        path: '',
        children: [],
      },
      {
        path: 'esports-management',
        canActivate: [AdminControlGuard],
        children: [
          {
            path: '',
            component: EsportsManagementComponent,
          },
          {
            path: 'esportsPayment',
            component: EsportsPaymentComponent,
          },
          {
            path: 'esportsPaymentDetails',
            component: EsportsPaymentDetailsComponent,
          },
        ],
      },

      {
        path: 'access-management',
        canActivate: [AdminControlGuard],
        children: [
          {
            path: '',
            component: AccessManagementComponent,
          },
          {
            path: 'view/:id',
            component: ViewUserAccessComponent,
          },
        ],
      },
      {
        path: 'content-management',
        canActivate: [AdminControlGuard],
        component: ContentManagementComponent,
      },
      {
        path: 'user-management',
        canActivate: [AdminControlGuard],
        children: [
          {
            path: '',
            component: UserManagementComponent,
          },
          {
            path: 'view/:id',
            component: ViewUserDetailsComponent,
          },
        ],
      },
      {
        path: 'user-notifications',
        component: UserNotificationsComponent,
      },
      {
        path: 'site-configuration',
        canActivate: [AdminControlGuard],
        component: SiteConfigurationComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminRoutingModule {}
