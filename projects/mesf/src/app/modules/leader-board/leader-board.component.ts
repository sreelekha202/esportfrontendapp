import { Component, OnInit } from '@angular/core';
import {
  LeaderboardService,
  GameService,
  UserService,
} from '../../core/service';
import { SwiperConfigInterface } from 'ngx-swiper-wrapper';
import { IPagination } from '../../shared/models';

@Component({
  selector: 'app-leader-board',
  templateUrl: './leader-board.component.html',
  styleUrls: ['./leader-board.component.scss'],
})
export class LeaderBoardComponent implements OnInit {
  public config: SwiperConfigInterface = {
    navigation: true,
    pagination: false,
    autoHeight: true,
  };
  gameList = [];
  stateList = [];
  selectedCountryName = '';
  leaderBoardColumns = [
    { name: 'RANK' },
    { name: 'Player Name' },
    { name: 'REGION' },
    { name: 'TROPHIES' },
    { name: 'POINTS' },
  ];
  selectedGame: any = { all: true };
  selectedRegions = '';
  selectedGames = 'all';
  selectedGameName = '';

  paginationData = {
    page: 1,
    limit: 100,
    sort: 'startDate',
  };
  gameId = 'all';
  showLoader = true;
  page: IPagination;
  leaderboardData: any = [];

  constructor(
    private leaderboardService: LeaderboardService,
    private gameService: GameService,
    private userService: UserService
  ) {}

  ngOnInit(): void {
    this.getGames();
    this.getStates([132]);

    // this.filterByGame('all');
    this.filterLeaderboard();
  }

  getGames() {
    const query = JSON.stringify({ isTournamentAllowed: true });
    this.gameService.getAllGames(query).subscribe(
      (res) => (this.gameList = res.data),
      (err) => {}
    );
  }

  getStates(countryIds) {
    this.stateList = [];
    this.userService.getStates().subscribe((data) => {
      for (const value of data.states) {
        if (countryIds.includes(parseInt(value.country_id))) {
          this.stateList.push(value.name);
        }
      }
    });
  }

  changeTab(value, type) {
    switch (type) {
      case 'regions': {
        if (value != '') {
          const region = this.stateList.find((o) => o === value);
          this.selectedRegions = region;
          this.filterLeaderboard();
        } else {
          this.selectedRegions = '';
          this.filterLeaderboard();
        }
        break;
      }
      case 'games': {
        const game = this.gameList.find((o) => o._id === value);
        this.selectedGameName = value == 'all' ? '' : game.name;
        this.selectedGames = value;
        this.filterLeaderboard();
        break;
      }
    }
  }

  filterLeaderboard() {
    this.paginationData.page = 1;
    this.selectedGame = {};
    this.selectedGame[this.selectedGames] = true;
    this.showLoader = true;
    const params = {
      page: this.paginationData.page,
      limit: this.paginationData.limit,
      gameId: this.selectedGames,
      state: this.selectedRegions,
      country: null,
    };
    // if (this.selectedRegions == 'Kuwait' || this.selectedRegions == 'Bahrain') {
    //   params.country = this.selectedRegions;
    // } else {
    //   params.state = this.selectedRegions;
    // }
    this.leaderboardService.getGameLeaderboard(params).subscribe(
      (res: any) => {
        this.showLoader = false;
        this.leaderboardData = [];
        // this.page = {
        //   totalItems: res?.data?.totalDocs,
        //   itemsPerPage: res?.data?.limit,
        //   maxSize: 5,
        // };
        if (res.data.docs) {
          let count = 0;
          let userData: any = {};
          for (const data of res.data.docs) {
            if (data.user.length > 0) {
              userData.id = data._id.user;
              userData.rank = count + 1;
              count++;
              userData.img =
                data.user[0].profilePicture == ''
                  ? './assets/images/Profile/article01.png'
                  : data.user[0].profilePicture;
              userData.name = data.user[0].fullName;
              userData.region = data.user[0].state ? data.user[0].state : '';
              userData.trophies = {};
              userData.trophies.first = data.firstPosition;
              userData.trophies.second = data.secondPosition;
              userData.trophies.third = data.thirdPosition;
              userData.points = data.points;
              userData.amount = data.amountPrice;
              userData.gamesCount = data.gamesCount;
              this.leaderboardData.push({ ...userData });
              userData = {};
            }
          }
        }
      },
      (err) => {
        this.showLoader = false;
      }
    );
  }

  filterByGame(gameId) {
    this.paginationData.page = 1;
    // this.slectedGame[gameId] = true;
    this.selectedGame = {};
    this.selectedGame[gameId] = true;
    if (gameId === 'all') {
      this.gameId = 'all';
    } else {
      this.gameId = gameId;
    }
    this.showLoader = true;
    const params = {
      page: this.paginationData.page,
      limit: this.paginationData.limit,
      gameId: this.gameId,
    };
    if (this.gameId) {
      params['game'] = this.gameId;
    }
    this.leaderboardService.getGameLeaderboard(params).subscribe(
      (res: any) => {
        this.showLoader = false;
        this.leaderboardData = [];
        // this.page = {
        //   totalItems: res?.data?.totalDocs,
        //   itemsPerPage: res?.data?.limit,
        //   maxSize: 5,
        // };
        if (res.data.docs) {
          let count = 0;
          let userData: any = {};
          for (const data of res.data.docs) {
            if (data.user.length > 0) {
              userData.id = data._id.user;
              userData.rank = count + 1;
              count++;
              userData.img =
                data.user[0].profilePicture == ''
                  ? './assets/images/Profile/article01.png'
                  : data.user[0].profilePicture;
              userData.name = data.user[0].fullName;
              userData.region = data.user[0].state ? data.user[0].state : '';
              userData.trophies = {};
              userData.trophies.first = data.firstPosition;
              userData.trophies.second = data.secondPosition;
              userData.trophies.third = data.thirdPosition;
              userData.points = data.points;
              userData.amount = data.amountPrice;
              userData.gamesCount = data.gamesCount;
              this.leaderboardData.push({ ...userData });
              userData = {};
            }
          }
        }
      },
      (err) => {
        this.showLoader = false;
      }
    );
  }
}
