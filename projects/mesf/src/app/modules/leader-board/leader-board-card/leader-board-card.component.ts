import { Component, OnInit, Input } from '@angular/core';

// interface LeaderBoardCard {
//   name: string;
//   games: number;
//   image: string;
//   myr: string | number;
//   place: number;
// }

@Component({
  selector: 'app-leader-board-card',
  templateUrl: './leader-board-card.component.html',
  styleUrls: ['./leader-board-card.component.scss'],
})
export class LeaderBoardCardComponent implements OnInit {
  @Input() params;

  constructor() {}

  ngOnInit(): void {}

  addEndingToPlace(place: number) {
    switch (place) {
      case 1:
        return `1st`;
      case 2:
        return `2nd`;
      case 3:
        return `3rd`;
      default:
        return place;
    }
  }

  addImageToPlace(place: number) {
    switch (place) {
      case 1:
        return `assets/icons/gold.svg`;
      case 2:
        return `assets/icons/silver.svg`;
      case 3:
        return `assets/icons/bronze.svg`;
    }
  }
}
