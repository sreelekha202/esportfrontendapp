import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TransactionService } from '../../core/service';

enum TopOffersFilter {
  TOPUP,
  VOUCHER,
}

@Component({
  selector: 'app-top-up',
  templateUrl: './top-up.component.html',
  styleUrls: ['./top-up.component.scss'],
})
export class TopUpComponent implements OnInit {
  TopOffersFilter = TopOffersFilter;
  topOffersFilter = TopOffersFilter.TOPUP;
  TxnId = '';
  OrderId = '';
  isTxnFailed = false;
  failedTransactionDetail = {
    items: [],
    orderId: '',
    txnId: '',
  };
  topOffers = [];

  constructor(
    private activatedRoute: ActivatedRoute,
    private transactionService: TransactionService
  ) {}

  ngOnInit(): void {
    this.TxnId = this.activatedRoute.snapshot.queryParams['TxnId'];
    this.OrderId = this.activatedRoute.snapshot.queryParams['OrderId'];
    this._getFailedTransaction();
    this._getProducts();
  }

  /**
   * Fetch product list of selected type
   * @param e
   */
  handleProductTypeChange(e) {
    this._getProducts();
  }

  /**
   * fetch transaction detail based on TxnId and OrderId
   */
  _getFailedTransaction() {
    if (
      !this.TxnId ||
      !this.OrderId ||
      this.TxnId.length == 0 ||
      this.OrderId.length == 0
    ) {
      return;
    }
    let tQuery = {
      txnId: this.TxnId,
      orderId: this.OrderId,
    };
    this.transactionService.getTransactionDetail(tQuery).subscribe(
      (data) => {
        if (data && data.data) {
          this.isTxnFailed = true;
          this.failedTransactionDetail = data.data;
        }
      },
      (err) => {}
    );
  }

  /**
   * fetch product list
   */
  _getProducts() {
    let productType = TopOffersFilter[this.topOffersFilter] || 'TOPUP';
    this.transactionService.getProduct(productType).subscribe(
      (data) => {
        if (data && data.data) {
          this.topOffers = data.data || [];
        }
      },
      (err) => {}
    );
  }
}

/**
 * {
      img: './assets/images/Home/top1.png',
      title: 'Free Fire',
      productId: 'free-fire',
    },
    {
      img: './assets/images/Home/top2.png',
      title: 'PUBG',
      productId: 'NA',
    },
    {
      img: './assets/images/Home/top3.png',
      title: 'Call of Duty',
      productId: 'NA',
    },
    {
      img: './assets/images/Home/top4.png',
      title: 'Mobile Legends',
      productId: 'mobile-legends',
    },
    {
      img: './assets/images/Home/top5.png',
      title: 'Lord fo Estera',
      productId: 'NA',
    },
    {
      img: './assets/images/Home/top6.png',
      title: 'Shellfire',
      productId: 'NA',
    },
    {
      img: './assets/images/Home/top7.png',
      title: 'Speed Drifters',
      productId: 'NA',
    },
    {
      img: './assets/images/Home/top8.png',
      title: 'Speed Drifters',
      productId: 'NA',
    },
 */
