import { Component, OnInit } from '@angular/core';

import {
  ArticleApiService,
  UtilsService,
  VideoLibraryService,
  UserService,
  ToastService,
  LanguageService,
  OptionService,
} from '../../../core/service';
import { JumbotronComponentData } from '../../../core/jumbotron/jumbotron.component';
import { AppHtmlRoutes } from '../../../app-routing.model';
import { TranslateService } from '@ngx-translate/core';
import { SwiperConfigInterface } from 'ngx-swiper-wrapper';
import { IUser } from '../../../shared/models';

@Component({
  selector: 'app-article-main',
  templateUrl: './article-main.component.html',
  styleUrls: ['./article-main.component.scss'],
})
export class ArticleMainComponent implements OnInit {
  user: IUser;
  AppHtmlRoutes = AppHtmlRoutes;
  jumbotron: JumbotronComponentData = {
    _id: '',
    slug: '',
    ribbon: '',
    dateTitle: '',
    title: '',
    isBtnHidden: true,
    btn: 'read post',
    user: {
      img: '',
      name: '',
    },
  };

  videoLibrary: Array<{}> = [];
  activeVideo = null;
  trendingAuthors = [];
  hottestPost: any = {};
  gameList = [];
  trendingPosts = [];
  recentPosts = [];
  showLoader = true;
  trendingNews = [];
  swiperConfig: SwiperConfigInterface = {
    spaceBetween: 30,
    navigation: true,
    pagination: false,
  };
  latestNews = [];
  categoryList;
  categoryId;

  constructor(
    private videoLibraryService: VideoLibraryService,
    private toastService: ToastService,
    public utilsService: UtilsService,
    private articleService: ArticleApiService,
    public languageService: LanguageService,
    public translateService: TranslateService,
    private userService: UserService,
    private optionService: OptionService
  ) {}

  ngOnInit(): void {
    this.getCurrentUserDetails();
    this.fetchVideoLibrary();
    this.loadArticleMainPageData();
    this.fetchOptions();
    this.getRecentPost();
    this.getGames();
  }

  fetchVideoLibrary = async () => {
    try {
      const pagination = {
        page: 1,
        limit: 5,
        sort: '-updatedOn',
        projection: [
          '_id',
          'title',
          'description',
          'youtubeUrl',
          'thumbnailUrl',
        ],
      };
      const query = `?pagination=${encodeURIComponent(
        JSON.stringify(pagination)
      )}`;

      const response = await this.videoLibraryService.fetchVideoLibrary(query);
      this.videoLibrary = await Promise.all(
        response?.data?.docs.map(async (item) => {
          return {
            ...item,
            youtubeUrl: await this.utilsService.getEmbbedUrl(item.youtubeUrl),
          };
        })
      );
      this.activeVideo = this.videoLibrary.length ? this.videoLibrary[0] : null;
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  loadArticleMainPageData() {
    this.getTrendingAuthors();
  }

  getTrendingAuthors() {
    this.articleService.getTrendingAuthors().subscribe(
      (res) => {
        this.trendingAuthors = res['data'];
        this.getHottestPost();
      },
      (err) => {
        this.showLoader = false;
      }
    );
  }

  fetchOptions = async () => {
    const option = await Promise.all([this.optionService.fetchAllCategories()]);
    this.categoryList = option[0]?.data;
    for (const iterator of this.categoryList) {
      if (iterator.name == 'News') {
        this.categoryId = iterator._id;
      }
    }
    this.getArticleNews(this.categoryId);
  };

  getArticleNews(categoryId) {
    const query = JSON.stringify({
      articleStatus: 'publish',
      category: categoryId,
    });
    const pagination = JSON.stringify({ limit: 7, sort: { createdDate: -1 } });
    let prefernce = JSON.stringify({
      prefernce: this.user?.preference?.game
        ? this.user?.preference?.game.map((item) => {
            return { gameDetails: item?._id };
          })
        : '',
    });
    this.articleService
      .getLatestArticle({ query, pagination, preference: prefernce })
      .subscribe(
        (res: any) => {
          this.latestNews = res['data']['docs'].slice(0, 2);
          this.trendingNews = res['data']['docs'];
        },
        (err) => {}
      );
  }

  getHottestPost = async () => {
    try {
      const hottestPost = await this.articleService
        .getHottestPost()
        .toPromise();
      this.hottestPost = hottestPost?.['data'][0] || null;
    } catch (error) {
      this.showLoader = false;
    }
  };

  getGames() {
    let filter = '{}';
    let option = JSON.stringify({ limit: 8 });
    let query = `query=${filter}&option=${option}`;
    this.articleService.getGameList(query).subscribe(
      (res) => {
        // this.getTrendingPosts();
        this.gameList = res['data'];
      },
      (err) => {
        this.showLoader = false;
      }
    );
  }

  getTrendingPosts() {
    this.articleService.getTrendingPosts().subscribe(
      (res) => {
        this.showLoader = false;
        this.trendingPosts = res['data'];
      },
      (err) => {
        this.showLoader = false;
      }
    );
  }
  getRecentPost() {
    let pagination = JSON.stringify({ limit: 8, sort: { createdDate: -1 } });
    let query = JSON.stringify({
      articleStatus: 'publish',
    });
    let perfernce = JSON.stringify({
      prefernce: this.user?.preference?.game
        ? this.user?.preference?.game.map((item) => {
            return { gameDetails: item?._id };
          })
        : '',
    });
    this.articleService
      .getLatestArticle({
        pagination: pagination,
        query: query,
        preference: perfernce,
      })
      .subscribe(
        (res) => {
          this.recentPosts = res['data']['docs'];
          this.showLoader = false;
        },
        (err) => {
          this.showLoader = false;
        }
      );
  }
  getCurrentUserDetails() {
    this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.user = data;
      }
    });
  }
}
