import { TranslateService } from '@ngx-translate/core';
import { Component, OnInit } from '@angular/core';

import { ArticleApiService, UserService } from '../../../core/service';
import { IPagination } from '../../../shared/models';
import { IUser } from '../../../shared/models';

@Component({
  selector: 'app-article-more',
  templateUrl: './article-more.component.html',
  styleUrls: ['./article-more.component.scss'],
})
export class ArticleMoreComponent implements OnInit {
  user: IUser;
  paginationData = {
    page: 1,
    limit: 20,
    sort: { createdDate: -1 },
  };

  articles: any = [];
  dataLoaded = false;
  showLoader = true;
  page: IPagination;

  constructor(
    private articleApiService: ArticleApiService,
    public translate: TranslateService,
    private userService: UserService
  ) {}

  ngOnInit() {
    this.getCurrentUserDetails();
    this.fetchArticles();
  }

  pageChanged(page): void {
    this.paginationData.page = page;
    this.fetchArticles();
  }

  getCurrentUserDetails() {
    this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.user = data;
      }
    });
  }

  fetchArticles = async () => {
    this.showLoader = true;
    try {
      const pagination = JSON.stringify(this.paginationData);
      const query = JSON.stringify({ articleStatus: 'publish' });
      let perfernce = JSON.stringify({
        prefernce: this.user?.preference?.game
          ? this.user?.preference?.game.map((item) => {
              return { gameDetails: item?._id };
            })
          : '',
      });
      this.articleApiService
        .getLatestArticle({ pagination, query, preference: perfernce })
        .subscribe(
          (res: any) => {
            this.showLoader = false;
            this.articles = res['data']['docs'];
            this.dataLoaded = true;
            this.page = {
              totalItems: res?.data?.totalDocs,
              itemsPerPage: res?.data?.limit,
              maxSize: 5,
            };
          },
          (err) => {}
        );
    } catch (error) {
      this.showLoader = false;
    }
  };
}
