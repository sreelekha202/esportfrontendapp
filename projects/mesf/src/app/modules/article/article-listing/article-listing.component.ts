import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {
  AuthServices,
  ArticleApiService,
  UserService,
  LanguageService,
} from '../../../core/service';

@Component({
  selector: 'app-article-listing',
  templateUrl: './article-listing.component.html',
  styleUrls: ['./article-listing.component.scss'],
})
export class ArticleListingComponent implements OnInit {
  articles: any = [];
  bannerList: any = [];
  dataLoaded = false;
  constructor(
    private articleApiService: ArticleApiService,
    private userService: UserService,
    public languageService: LanguageService
  ) {
    this.userService.currentUser.subscribe((data) => {});
  }
  ngOnInit() {
    this.getAllArticles();
  }
  getAllArticles() {
    this.articleApiService.getallArticle().subscribe(
      (data: any) => {
        this.articles = data.data;
        this.bannerList = data.data.map((obj) => {
          return { image: obj.image, _id: obj._id };
        });
        this.dataLoaded = true;
      },
      (err) => {}
    );
  }

  setFallbackImage(index) {
    this.bannerList[index]['image'] = 'assets/images/articles/fallback.png';
  }
}
