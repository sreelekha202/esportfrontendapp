import { Component, OnInit } from '@angular/core';
import {
  ArticleApiService,
  UserService,
  OptionService,
} from '../../../core/service';
import { IPagination } from '../../../shared/models';
import { IUser } from '../../../shared/models';

@Component({
  selector: 'app-article-news',
  templateUrl: './article-news.component.html',
  styleUrls: ['./article-news.component.scss'],
})
export class ArticleNewsComponent implements OnInit {
  user: IUser;
  articles: any = [];
  paginationData = {
    page: 1,
    limit: 20,
    sort: '-createdDate',
  };

  page: IPagination;
  categoryList = [];
  categoryId;
  constructor(
    private articleService: ArticleApiService,
    private userService: UserService,
    private optionService: OptionService
  ) {}

  ngOnInit(): void {
    this.getCurrentUserDetails();
    this.fetchOptions();
  }

  getCurrentUserDetails() {
    this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.user = data;
      }
    });
  }

  pageChanged(page): void {
    this.paginationData.page = page;
    this.getArticleNews(this.categoryId);
  }

  fetchOptions = async () => {
    const option = await Promise.all([this.optionService.fetchAllCategories()]);
    this.categoryList = option[0]?.data;

    for (const iterator of this.categoryList) {
      if (iterator.name == 'News') {
        this.categoryId = iterator._id;
      }
    }
    this.getArticleNews(this.categoryId);
  };

  getArticleNews(categoryId) {
    const query = JSON.stringify({
      articleStatus: 'publish',
      category: categoryId,
    });

    const pagination = JSON.stringify(this.paginationData);
    const prefernce = JSON.stringify({
      prefernce: this.user?.preference?.game
        ? this.user?.preference?.game.map((item) => {
            return { gameDetails: item };
          })
        : '',
    });

    this.articleService
      .getPaginatedArticles({ query, pagination, preference: prefernce })
      .subscribe(
        (res: any) => {
          this.articles = res['data']['docs'];
          this.page = {
            totalItems: res?.data?.totalDocs,
            itemsPerPage: res?.data?.limit,
            maxSize: 5,
          };
        },
        (err) => {}
      );
  }
}
