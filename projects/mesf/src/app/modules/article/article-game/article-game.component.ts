import { TranslateService } from '@ngx-translate/core';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { ArticleApiService, UserService } from '../../../core/service';
import { IPagination } from '../../../shared/models';
import { LanguageService } from '../../../core/service';
import { IUser } from '../../../shared/models';

@Component({
  selector: 'app-article-game',
  templateUrl: './article-game.component.html',
  styleUrls: ['./article-game.component.scss'],
})
export class ArticleGameComponent implements OnInit {
  user: IUser;
  gameId;
  articles = [];
  jumbotron = {
    _id: '',
    slug: '',
    ribbon: '',
    dateTitle: '',
    title: '',
    isBtnHidden: true,
    btn: '',
    user: {
      img: '',
      name: '',
    },
  };
  currentLang;
  hottestPost: any = {};
  paginationData = {
    page: 1,
    limit: 20,
    sort: { createdDate: -1 },
  };
  showLoader = true;
  page: IPagination;

  constructor(
    private location: Location,
    private activatedRoute: ActivatedRoute,
    private articleService: ArticleApiService,
    public translate: TranslateService,
    public language: LanguageService,
    private userService: UserService
  ) {}

  ngOnInit(): void {
    this.getCurrentUserDetails();
    this.gameId = this.activatedRoute.snapshot.params['id'];
    this.fetchArticles();
  }

  goBack(): void {
    this.location.back();
  }
  getCurrentUserDetails() {
    this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.user = data;
      }
    });
  }

  getHottestPost() {
    this.articleService.getHottestPost().subscribe(
      (res) => {
        if (res && res['data'] && res['data'].length > 0) {
          this.hottestPost = res['data'][0];
          let createdDate = new Date(this.hottestPost['createdDate']);
          let date = createdDate.getDate();
          let month = createdDate.toLocaleString('default', { month: 'short' });
          this.jumbotron = {
            _id: this.hottestPost._id,
            slug: this.hottestPost.slug,
            ribbon: 'Hottest post',
            dateTitle: `${this.hottestPost.game} | ${date} ${month}`,
            title: this.hottestPost.title.english,
            isBtnHidden: true,
            btn: 'read post',
            user: {
              img: this.hottestPost['authorDetails']['profilePicture']
                ? this.hottestPost['authorDetails']['profilePicture']
                : 'assets/images/Profile/avatar.png',
              name: this.hottestPost['authorDetails']['fullName'],
            },
          };
        }
      },
      (err) => {
        this.showLoader = false;
      }
    );
  }

  pageChanged(page): void {
    this.paginationData.page = page;
    this.fetchArticles();
  }

  fetchArticles = async () => {
    this.showLoader = true;
    try {
      let pagination = JSON.stringify(this.paginationData);
      let query = JSON.stringify({
        gameDetails: this.gameId,
        articleStatus: 'publish',
      });
      let perfernce = JSON.stringify({
        prefernce: this.user?.preference?.gamegenre
          ? this.user?.preference?.gamegenre.map((item) => {
              return { gameDetails: item?._id };
            })
          : '',
      });
      this.articleService
        .getLatestArticle({
          pagination: pagination,
          query: query,
          preference: perfernce,
        })
        .subscribe(
          (res: any) => {
            this.showLoader = false;
            this.articles = res['data']['docs'];
            this.page = {
              totalItems: res?.data?.totalDocs,
              itemsPerPage: res?.data?.limit,
              maxSize: 5,
            };
            this.getHottestPost();
          },
          (err) => {
            this.showLoader = false;
          }
        );
    } catch (error) {
      this.showLoader = false;
    }
  };
}
