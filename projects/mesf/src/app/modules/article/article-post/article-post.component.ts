import { MatDialog } from '@angular/material/dialog';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import {
  ArticleApiService,
  LanguageService,
  UserPreferenceService,
  UserService,
  HomeService,
  CommentService,
  ToastService,
  ConstantsService,
} from '../../../core/service';
import {
  InfoPopupComponent,
  InfoPopupComponentData,
  InfoPopupComponentType,
} from '../../../shared/popups/info-popup/info-popup.component';
import { toggleHeight } from '../../../animations';
import { GlobalUtils } from '../../../shared/service/global-utils/global-utils';
import { IArticle } from '../../../shared/models';
@Component({
  selector: 'app-article-post',
  templateUrl: './article-post.component.html',
  styleUrls: ['./article-post.component.scss'],
  animations: [toggleHeight],
})
export class ArticlePostComponent implements OnInit {
  article: IArticle;
  userId;
  active = 1;
  isBookmarked = false;
  relatedArticles = [];
  showLoader = false;
  articleDetails;

  constructor(
    private articleApiService: ArticleApiService,
    private activatedRoute: ActivatedRoute,
    private userService: UserService,
    private homeService: HomeService,
    public languageService: LanguageService,
    private userPreferenceService: UserPreferenceService,
    public matDialog: MatDialog,
    private commentService: CommentService,
    public toastService: ToastService
  ) {}

  ngOnInit(): void {
    this.activatedRoute.params.subscribe((params) => {
      const slug = this.activatedRoute?.snapshot?.params?.id;
      if (slug) {
        this.getArticle(slug);
      }
    });
    this.getCurrentUserDetails();
  }

  getArticle = async (slug) => {
    try {
      this.showLoader = true;
      const article: any = await this.articleApiService.getArticleBySlug(slug);
      this.article = article.data;
      this.showLoader = false;
      this.updateView();
      this.getRelatedArticles();
      this.showLoader = false;
    } catch (error) {
      this.showLoader = false;
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  getRelatedArticles = async () => {
    try {
      const query = JSON.stringify({
        $or: [
          { tags: { $in: this.article?.tags } },
          { gameDetails: this.article?.gameDetails?._id },
        ],
        articleStatus: ConstantsService.Status.Publish,
      });

      const option = JSON.stringify({ limit: 4, sort: { views: -1 } });

      const relatedArticle: any = await this.articleApiService
        .getArticles_PublicAPI({ query, option })
        .toPromise();

      this.relatedArticles = relatedArticle.data;
    } catch (error) {
      this.showLoader = false;
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  upsertBookmark = async () => {
    try {
      if (!this.userId) {
        this.toastService.showError('Session Expired Please Login');
        return;
      }

      if (this.article?.bookmarkProccesing) return;

      // Bookmark field
      const removeBookmark = `articleId=${this.article?._id}`;
      const addBookmark = { articleId: this.article._id };
      this.article.bookmarkProccesing = true;

      const bookmark: any = this.article?.isBookmarked
        ? await this.userPreferenceService
            .removeBookmark(removeBookmark)
            .toPromise()
        : await this.userPreferenceService.addBookmark(addBookmark).toPromise();

      this.article = {
        ...this.article,
        ...(bookmark?.data?.nModified == 1 && {
          isBookmarked: !this.article?.isBookmarked,
        }),
      };
      this.toastService.showSuccess(bookmark?.message);
      this.article.bookmarkProccesing = false;
    } catch (error) {
      this.article.bookmarkProccesing = false;
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  getCurrentUserDetails() {
    this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.userId = data['_id'];
      }
    });
  }

  updateView = async () => {
    const payload = {
      _id: this.article._id,
      modalName: 'article',
    };
    await this.homeService.updateView(payload);
  };

  openSocialShareComponent() {
    const data: InfoPopupComponentData = {
      title: `Share Article via`,
      text: ``,
      type: InfoPopupComponentType.socialSharing,
      cancelBtnText: 'Close',
    };
    this.matDialog.open(InfoPopupComponent, {
      data,
    });
  }

  onScrollTop() {
    if (GlobalUtils.isBrowser()) {
      window.scroll(0, 0);
    }
  }

  likeOrUnLikeArticle = async (
    objectId: string,
    objectType: string,
    type: number
  ) => {
    try {
      if (!this.userId) {
        this.toastService.showInfo('Please login to like this comment');
        return;
      }

      if (this.article.likeProccesing) return;

      this.article.likeProccesing = true;
      const payload = {
        objectId,
        objectType,
        type: type == -1 ? 1 : Number(!type),
      };

      const like = await this.commentService.upsertLike(payload);
      this.article = { ...this.article, ...like.data };
      this.article.likeProccesing = false;
    } catch (error) {
      this.article.likeProccesing = false;
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };
}
