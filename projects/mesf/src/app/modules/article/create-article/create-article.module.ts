import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { ShareModule } from 'ngx-sharebuttons';
import { PaginationModule, PaginationConfig } from 'ngx-bootstrap/pagination';
import { PipeModule } from '../../../shared/pipe/pipe.module';

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from '../../../shared/modules/shared.module';
import { CreateArticleComponent } from './create-article.component';
import { CoreModule } from '../../../core/core.module';
import { AuthorAccessGuard } from '../../../shared/guard/isAuthorInfluencer.guard';
import { FormComponentModule } from '../../../shared/components/form-component/form-component.module';

const routes: Routes = [
  {
    path: '',
    canActivate: [AuthorAccessGuard],
    component: CreateArticleComponent,
  },
  // {
  //   path: 'create',
  //   canActivate: [AuthorAccessGuard],
  //   component: CreateArticleComponent,
  // }
];

@NgModule({
  declarations: [CreateArticleComponent],
  imports: [
    SharedModule,
    CKEditorModule,
    CoreModule,
    ShareModule,
    PaginationModule.forRoot(),
    PipeModule,
    FormComponentModule,
    RouterModule.forChild(routes),
  ],
  providers: [{ provide: PaginationConfig }],
})
export class CreateArticleModule {}
