import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import {
  ArticleApiService,
  GameService,
  UserService,
  OptionService,
  ConstantsService,
  LanguageService,
  FormService,
  ToastService,
  S3UploadService,
} from '../../../core/service';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';

import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DOCUMENT } from '@angular/common';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl,
} from '@angular/forms';
import { UploadAdapter } from './ckEditorAdapter';
import { HttpClient } from '@angular/common/http';
import { faTrash } from '@fortawesome/free-solid-svg-icons';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { environment } from '../../../../environments/environment';
import { IUser } from '../../../shared/models';
import { CKEditor5 } from '@ckeditor/ckeditor5-angular/ckeditor';
import '@ckeditor/ckeditor5-build-classic/build/translations/ms';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-create-article',
  templateUrl: './create-article.component.html',
  styleUrls: ['./create-article.component.scss'],
})
export class CreateArticleComponent implements OnInit, OnDestroy {
  public Editor = ClassicEditor;
  articleForm: FormGroup;

  gameList = [];
  tagsList = [];
  categoryList = [];
  platformList = [];
  genreList = [];

  public ckConfig: CKEditor5.Config = {
    uiColor: '#000000',
    mediaEmbed: {
      previewsInData: true,
      extraProviders: [
        {
          name: 'Twitch',
          url: /(?:https:\/\/)?((www\.)?twitch\.tv)\/(\S+)/,
        },
      ],
    },
  };

  faTrash = faTrash;
  showLoader = false;
  apiLoaded: Array<boolean> = [];
  isLoaded = false;
  user: IUser;
  language = [];
  currLanguage = '';
  isEditorLoaded: boolean = true;
  slug;

  // banner config
  dimension = { width: 1088, height: 560 };
  bannerSize = 1000 * 1000 * 2;
  userSubscription: Subscription;

  constructor(
    private articleApiService: ArticleApiService,
    private modalService: NgbModal,
    private toastService: ToastService,
    private userService: UserService,
    private fb: FormBuilder,
    public s3UploadService: S3UploadService,
    public http: HttpClient,
    public gameService: GameService,
    public router: Router,
    public activatedRoute: ActivatedRoute,
    @Inject(DOCUMENT) private document: Document,
    private optionService: OptionService,
    private languageService: LanguageService,
    private constantsService: ConstantsService,
    private formService: FormService
  ) {}

  ngOnInit(): void {
    this.language = ConstantsService?.language;
    this.currLanguage = this.language[0]?.key;
    this.languageService.language.subscribe((lang) => {
      if (lang) {
        this.ckConfig.language = lang;
      }
      // refresh editor
      this.isEditorLoaded = false;
      setTimeout(() => (this.isEditorLoaded = true));
    });
    this.getCurrentUserDetails();
    this.createForm();
    this.fetchOptions();

    this.slug = this.activatedRoute?.snapshot?.params?.id;
  }

  ngOnDestroy() {
    if (this.userSubscription) {
      this.userSubscription?.unsubscribe();
    }
  }

  fetchOptions = async () => {
    try {
      this.apiLoaded.push(false);
      const option: any = await Promise.all([
        this.gameService.getGames().toPromise(),
        this.optionService.fetchAllCategories(),
        this.optionService.fetchAllGenres(),
        this.optionService.fetchAllTags(),
      ]);
      this.gameList = option[0]?.data;
      this.categoryList = option[1]?.data;
      this.genreList = option[2]?.data;
      this.tagsList = option[3]?.data;
      if (this.slug) {
        this.getArticle(this.slug);
      }
      this.apiLoaded.push(true);
    } catch (error) {
      this.apiLoaded.push(true);
      this.toastService.showError(error?.error?.message || error.message);
    } finally {
      this.allApiDataLoaded();
    }
  };

  open(content) {
    if (!this.user) {
      this.toastService.showError('Session Expired, Please Login');
      return;
    }

    Object.keys(this.articleForm.controls).forEach((field) => {
      const control = this.articleForm.get(field);
      control.markAsTouched({ onlySelf: true });
    });

    if (this.articleForm.invalid) {
      this.toastService.showError(
        'Article Either in one or both language must be completed'
      );
      return;
    }

    if (!this.articleForm.value?.s3RefKey) {
      this.toastService.showError('Please Upload Banner');
      return;
    }
    if (!this.articleForm.valid) {
      this.toastService.showError('Invalid Form');
      return;
    }

    this.modalService.open(content, {
      size: 'lg',
      centered: true,
      scrollable: true,
      windowClass: 'custom-modal-content',
    });
  }

  createForm() {
    this.articleForm = this.fb.group({
      game: ['', Validators.compose([Validators.required])],
      genre: [[], Validators.compose([])],
      tags: [[], Validators.compose([])],
      highlight: [0, Validators.compose([])],
      minRead: [2, Validators.compose([])],
      location: ['', Validators.compose([])],
      isSponsor: [false, Validators.compose([])],
      isInfluencer: [false, Validators.compose([])],
      isInfluencerHighlight: [false, Validators.compose([])],
      title: this.fb.group(this.constantsService.languageFC(), {
        validator: this.formService.groupFieldValidator(),
      }),
      content: this.fb.group(this.constantsService.languageFC(), {
        validator: this.formService.groupFieldValidator(),
      }),
      category: ['', Validators.compose([Validators.required])],
      platform: [[], Validators.compose([Validators.required])],
      image: ['', Validators.compose([Validators.required])],
      shortDescription: this.fb.group(this.constantsService.languageFC(), {
        validator: this.formService.groupFieldValidator(),
      }),
      gameDetails: ['', Validators.compose([])],
      s3RefKey: [''],
    });
  }

  saveArticle = async (status, modal?) => {
    try {
      if (!this.user) {
        this.toastService.showError('Session Expired, Please Login');
        return;
      }

      if (this.articleForm.invalid) {
        this.toastService.showError(
          'Article Either in one or both language must be completed'
        );
        return;
      }

      if (!this.articleForm.value?.s3RefKey) {
        this.toastService.showError('Please Upload Banner');
        return;
      }

      if (!this.articleForm.valid) {
        Object.keys(this.articleForm.controls).forEach((field) => {
          const control = this.articleForm.get(field);
          control.markAsTouched({ onlySelf: true });
        });
        this.toastService.showError('Invalid Form');
        return;
      }

      const formValues = this.articleForm.value;
      formValues['articleStatus'] = status;
      this.showLoader = true;

      const article = formValues?.id
        ? await this.articleApiService
            .updateArticle(formValues.id, formValues)
            .toPromise()
        : await this.articleApiService.saveArticle(formValues);

      this.showLoader = false;
      this.toastService.showSuccess(article?.message);

      if (modal) {
        modal.close();
      }
      this.router.navigate(['/profile/my-content']);
    } catch (error) {
      this.showLoader = false;
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  removeTag(tag) {
    let index = this.articleForm.value.tags.indexOf(tag);
    this.articleForm.value.tags.splice(index, 1);
  }

  removePlatform(platform) {
    let index = this.articleForm.value.platform.indexOf(platform);
    this.articleForm.value.platform.splice(index, 1);
    if (this.articleForm.value.platform.length < 1) {
      let control = this.articleForm.get('platform');
      control.setErrors({ required: true });
    }
  }

  getDate() {
    return Date();
  }

  getCurrentUserDetails() {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.user = data;
      }
    });
  }

  removeGenre(genre) {
    let index = this.articleForm.value.genre.indexOf(genre);
    this.articleForm.value.genre.splice(index, 1);
  }

  setDropdownValue(formControl, value, type?) {
    if (type == 'array') {
      let arrayField = this.articleForm.get(formControl).value;
      arrayField.push(value);
      this.articleForm.controls[formControl].setValue(arrayField);
    } else {
      this.articleForm.controls[formControl].setValue(value);
    }
  }

  eventChange = async (event) => {
    try {
      this.showLoader = true;
      const upload: any = await this.s3UploadService.validateAndUploadImage(
        event,
        this.dimension,
        this.bannerSize,
        'articleBanner'
      );

      // For s3RefKey
      const filePath = upload.data[0]['Key'].split('.')[0];
      const lastIndexSlash = filePath.lastIndexOf('/');
      const s3RefKey = filePath.substring(lastIndexSlash + 1);

      this.articleForm.get('image').setValue(upload.data[0]['Location']);
      this.articleForm.get('s3RefKey').setValue(s3RefKey);

      this.toastService.showSuccess(upload?.message);
      this.showLoader = false;
    } catch (error) {
      this.showLoader = false;
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  onReady(eventData) {
    const s3Service = this.s3UploadService;
    const toastService = this.toastService;
    eventData.plugins.get('FileRepository').createUploadAdapter = (event) => {
      return new UploadAdapter(event, s3Service, toastService);
    };
  }

  setMinRead(val) {
    this.articleForm.controls.minRead.setValue(val);
  }

  removeBanner() {
    this.articleForm.controls.image.setValue('');
  }

  getGames() {
    this.gameService.getGames().subscribe(
      (res) => {
        this.gameList = res['data'];
      },
      (err) => {}
    );
  }

  getArticle(id) {
    this.articleApiService.getArticleByID(id).subscribe(
      (res) => {
        let data = res['data'][0];
        this.articleForm.addControl('id', new FormControl(id));
        const category = data.category;
        const tag = data.tags.map((item) => item._id);
        const platforms = data.platform.map((item) => item._id);
        const genres = data.genre.map((item) => item._id);
        const gameDetails = data.gameDetails._id;
        this.setPlatformList(gameDetails, this.gameList);
        this.articleForm.patchValue({
          ...data,
          category,
          tag,
          platforms,
          genres,
          gameDetails,
        });
      },
      (err) => {}
    );
  }
  /**
   * Check All API's Are Finished.
   * If All APi's Finished Then Remove Full Page Loader
   */
  allApiDataLoaded() {
    const startedApiCount = this.apiLoaded.filter((el) => !el).length;
    const finishedApiCount = this.apiLoaded.filter((el) => el).length;
    this.isLoaded = startedApiCount === finishedApiCount;
  }

  /**
   * Set Values In Form
   * @param formControl formControl
   * @param value value
   * @param isAllowMultiple isAllowMultiple
   * @param platform platform
   */
  addItem(formControl, value, isAllowMultiple = true, platform = null) {
    if (isAllowMultiple) {
      const array = this.articleForm.get(formControl).value;
      const index = array.indexOf(value);
      if (index < 0) {
        array.push(value);
        this.articleForm.controls[formControl].setValue(array);
      }
    } else {
      this.articleForm.get(formControl).setValue(value);
      if (platform) {
        this.platformList = platform;
        this.articleForm.get('platform').setValue([]);
        this.articleForm.get('platform').updateValueAndValidity();
      }
    }
    this.articleForm.get(formControl).updateValueAndValidity();
  }

  /**
   * Remove Value from the Form
   * @param formControl formControl
   * @param value value
   */
  removeItem(formControl, value) {
    const index = this.articleForm.value[formControl].indexOf(value);
    this.articleForm.value[formControl].splice(index, 1);
    this.articleForm.get(formControl).updateValueAndValidity();
  }

  setGameName(name) {
    this.articleForm.get('game').setValue(name);
  }

  setPlatformList(gameId, gameList) {
    if (gameList.length && gameId) {
      for (let iterator of gameList) {
        if (iterator._id == gameId) {
          this.platformList = iterator.platform;
        }
      }
    }
  }

  onChangePlatform(id, gameList) {
    this.articleForm.get('platform').setValue([]);
    this.setPlatformList(id, gameList);
  }

  /**
   * Set Marked As touch For Form Fields
   * @param formControl formControl
   */
  setFomControlTouched(formControl) {
    this.articleForm.controls[formControl].markAsTouched({
      onlySelf: true,
    });
  }

  changeLocalLanguage(lang) {
    this.currLanguage = lang?.key;
    this.ckConfig.locale = lang?.code;
    this.isEditorLoaded = false;
    setTimeout(() => (this.isEditorLoaded = true));
  }
}
