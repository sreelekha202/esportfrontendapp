import { environment } from '../../../../environments/environment';

export class UploadAdapter {
  public loader: any;
  public s3Service;
  public toastService;

  constructor(loader, s3Service: any, toastService: any) {
    this.loader = loader;
    this.s3Service = s3Service;
    this.toastService = toastService;
  }

  upload() {
    return new Promise(async (resolve, reject) => {
      this.loader.file.then(async (f) => {
        try {
          const promises = [this.s3Service.toBase64(f)];
          const Base64String = await Promise.all(promises);
          const imageData = {
            path: environment.articleS3BucketName,
            files: Base64String,
          };

          const upload = await this.s3Service.fileUpload(imageData).toPromise();
          resolve({ default: upload.data[0]['Location'] });
        } catch (error) {
          this.toastService.showError(error?.error?.message || error?.message);
          resolve(error?.error?.message || error?.message);
        }
      });
    });
  }
}
