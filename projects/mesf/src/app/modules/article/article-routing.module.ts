import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { ArticleMoreComponent } from './article-more/article-more.component';
import { ArticleAuthorComponent } from './article-author/article-author.component';
import { ArticlePostComponent } from './article-post/article-post.component';
import { ArticleMainComponent } from './article-main/article-main.component';
import { ArticleGameComponent } from './article-game/article-game.component';
import { ArticleNewsComponent } from './article-news/article-news.component';

export const contentRoutes: Routes = [
  { path: '', component: ArticleMainComponent },
  { path: 'news', component: ArticleNewsComponent },
  { path: 'author/:id', component: ArticleAuthorComponent },
  { path: 'author/:id/:articleId', component: ArticlePostComponent },
  { path: 'more', component: ArticleMoreComponent },
  { path: ':id', component: ArticlePostComponent },
  { path: 'game/:id', component: ArticleGameComponent },
];

@NgModule({
  imports: [RouterModule.forChild(contentRoutes)],
  exports: [RouterModule],
})
export class ArticleRoutingModule {}
