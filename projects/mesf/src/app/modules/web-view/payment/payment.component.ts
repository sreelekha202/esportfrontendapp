import { Component, OnInit } from '@angular/core';
import {
  TransactionService,
  BracketService,
  ToastService,
} from '../../../core/service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'payment-web-view',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss'],
})
export class PaymentComponent implements OnInit {
  params;
  selectPaymentType = '';
  transaction;
  isProccesing = true;
  paymentMethods = [];
  prizePools = [];
  totalAmounts = [];
  tournament = {
    _id: '',
  };

  constructor(
    private transactionService: TransactionService,
    private toastService: ToastService,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private bracketService: BracketService
  ) {}

  ngOnInit(): void {
    this.activeRoute.queryParams.subscribe((params) => {
      this.params = params;
      this.tournament._id = this.params?.tournamentId;
      this.bracketService.setWebViewMeta(params);
      this.fetchTransaction();
    });
  }

  fetchTransaction = async () => {
    try {
      const payload = {
        tournamentId: this.params?.tournamentId,
        type: 'organizer',
      };

      const { data } = await this.transactionService.createOrUpdateTransaction(
        payload
      );
      this.totalAmounts.push({
        prizeLabel: 'PAYMENT.PRIZE_AMOUNT',
        amount: data.totalAmount - data.platformFee,
      });
      this.totalAmounts.push({
        prizeLabel: 'PAYMENT.TOTAL_AMOUNT',
        amount: data.totalAmount,
      });

      this.prizePools = data?.prizeList?.map((item) => {
        let img;
        if (item.name == '1st') {
          img = '../assets/images/payment/Gold.svg';
        } else if (item.name == '2nd') {
          img = '../assets/images/payment/Silver.svg';
        } else if (item.name == '3rd') {
          img = '../assets/images/payment/Bronze.svg';
        } else {
          img = '../assets/images/payment/Bronze.svg';
        }
        return {
          ...item,
          img,
        };
      });
      this.transaction = data;

      if (this.transaction.provider == 'paypal') {
        this.paymentMethods = [
          {
            img: '../assets/images/payment/paypal.png',
            name: 'Paypal',
            value: 'paypal',
          },
        ];
      }

      this.isProccesing = false;
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
      this.isProccesing = false;
    }
  };

  getPaymentResponse = async (order) => {
    try {
      this.isProccesing = true;
      const payload = {
        tournamentId: this.params?.tournamentId,
        transactionId: this.transaction?._id,
        txnId: order?.id,
        provider: 'paypal',
      };
      const response = await this.transactionService.createOrUpdateTransaction(
        payload
      );
      this.toastService.showSuccess(response?.message);
      this.router.navigate(['/web-view/payment-success']);
      this.isProccesing = false;
    } catch (error) {
      this.fetchTransaction();
      this.toastService.showError(error?.error?.message || error?.message);
      this.isProccesing = false;
    }
  };
}
