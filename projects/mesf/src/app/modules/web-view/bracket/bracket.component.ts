import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {
  BracketService,
  TournamentService,
  ToastService,
} from '../../../core/service';
export interface ParamsI {
  _id?: string;
  allowSeeding?: boolean;
  admin?: boolean;
  token?: string;
  participantId: string;
}

@Component({
  selector: 'bracket-web-view',
  templateUrl: './bracket.component.html',
  styleUrls: ['./bracket.component.scss'],
})
export class BracketComponent implements OnInit {
  tournament: any;
  structure;
  apiLoaded: Array<boolean> = [];
  allApprovedParticipants = [];
  isLoaded = false;
  params;

  constructor(
    private activeRoute: ActivatedRoute,
    private bracketService: BracketService,
    private tournamentService: TournamentService,
    private toastService: ToastService
  ) {}

  ngOnInit(): void {
    this.activeRoute.queryParams.subscribe((params) => {
      this.params = params;
      this.bracketService.setWebViewMeta(params);
      this.tournament = { _id: params?._id };
      if (params?._id) this.fetchTournament(params?._id, params?.allowSeeding);
    });
  }

  fetchTournament = async (id: string, allowSeeding: string) => {
    try {
      this.apiLoaded.push(false);
      const tournament = await this.tournamentService
        .getTournament(id)
        .toPromise();
      this.apiLoaded.push(true);

      this.tournament = tournament.data;

      if (
        this.tournament?.isSeeded &&
        ['single', 'double'].includes(this.tournament?.bracketType)
      ) {
        this.fetchAllMatches();
      } else if (!this.tournament?.isSeeded) {
        const payload =
          allowSeeding == 'true'
            ? {
                tournamentId: this.tournament._id,
              }
            : {
                bracketType: this.tournament?.bracketType,
                maximumParticipants: this.tournament?.maxParticipants,
                noOfSet: this.tournament?.noOfSet,
                ...(['round_robin', 'battle_royale'].includes(
                  this.tournament?.bracketType
                ) && {
                  noOfTeamInGroup: this.tournament?.noOfTeamInGroup,
                  noOfWinningTeamInGroup: this.tournament
                    ?.noOfWinningTeamInGroup,
                  noOfRoundPerGroup: this.tournament?.noOfRoundPerGroup,
                }),
              };
        const response = await this.bracketService.generateBracket(payload);
        this.structure = response.data;
      }
    } catch (error) {
      this.apiLoaded.push(true);
      this.toastService.showError(error.message);
    } finally {
      this.checkAllApiDataLoaded();
    }
  };

  /** Get all saved matches of the tournamnet */
  fetchAllMatches = async () => {
    try {
      this.apiLoaded.push(false);
      const queryParam = `?query=${encodeURIComponent(
        JSON.stringify({ tournamentId: this.tournament?._id })
      )}&select=bye,currentMatch,matchStatus,teamA,teamB,disQualifiedTeam,placeHolderA,placeHolderB,teamAWinSet,teamBWinSet,isThirdPlaceMatch,matchNo,battleRoyalTeams.noOfKill,battleRoyalTeams.placement,battleRoyalTeams.score,battleRoyalTeams._id,battleRoyalScoringOption,externalMatchId`;

      const response = await this.bracketService.fetchAllMatches(queryParam);
      this.structure = this.bracketService.assembleStructure(response.data);
      this.apiLoaded.push(true);
    } catch (error) {
      this.apiLoaded.push(true);
      this.toastService.showError(error.message);
    } finally {
      this.checkAllApiDataLoaded();
    }
  };

  checkAllApiDataLoaded() {
    const startedApiCount = this.apiLoaded.filter((el) => !el).length;
    const finishedApiCount = this.apiLoaded.filter((el) => el).length;
    this.isLoaded = startedApiCount === finishedApiCount;
  }

  getChildComponentResponse = (data) => {
    if (data) {
      this.fetchTournament(this.params?._id, this.params?.allowSeeding);
    }
  };
}
