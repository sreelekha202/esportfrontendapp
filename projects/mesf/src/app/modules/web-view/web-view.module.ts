import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/modules/shared.module';
import { PaymentModule } from '../payment/payment.module';

import { PaymentComponent } from './payment/payment.component';
import { BracketComponent } from './bracket/bracket.component';
import { PaymentSuccessComponent } from './payment-success/payment-success.component';
import { PaymentFailureComponent } from './payment-failure/payment-failure.component';

const routes: Routes = [
  {
    path: '',
    children: [
      { path: 'bracket', component: BracketComponent },
      { path: 'payment', component: PaymentComponent },
      { path: 'payment-success', component: PaymentSuccessComponent },
      { path: 'payment-failure', component: PaymentFailureComponent },
      { path: 'privacy-policy', component: PrivacyPolicyComponent },
    ],
  },
];

@NgModule({
  declarations: [
    PaymentComponent,
    BracketComponent,
    PaymentSuccessComponent,
    PaymentFailureComponent,
    PrivacyPolicyComponent,
  ],
  imports: [
    SharedModule,
    PaymentModule,
    //  CustomStreamingModule,
    RouterModule.forChild(routes),
  ],
})
export class WebViewModule {}
