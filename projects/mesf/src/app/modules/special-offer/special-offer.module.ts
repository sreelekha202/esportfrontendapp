import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SpecialOfferRoutingModule } from './special-offer-routing.module';
import { SpecialOfferComponent } from './special-offer.component';
import { PaymentPopupComponent } from './payment-popup/payment-popup.component';
import { PipeModule } from '../../shared/pipe/pipe.module';
import { SharedModule } from '../../shared/modules/shared.module';

@NgModule({
  declarations: [SpecialOfferComponent, PaymentPopupComponent],
  imports: [CommonModule, SpecialOfferRoutingModule, PipeModule, SharedModule],
})
export class SpecialOfferModule {}
