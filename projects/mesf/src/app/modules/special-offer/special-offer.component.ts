import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { CodaShopService, ToastService } from '../../core/service';
import { ActivatedRoute } from '@angular/router';
import { PaymentPopupComponent } from './payment-popup/payment-popup.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-special-offer',
  templateUrl: './special-offer.component.html',
  styleUrls: ['./special-offer.component.scss'],
})
export class SpecialOfferComponent implements OnInit {
  productList = [];
  productInfo = {
    name: '',
    productName: '',
    shortDesc: '',
    longDesc: '',
  };
  productName = '';
  userAccount = '';
  userValidated = false;
  validatingUserDetail = false;
  initiatingTransaction = false;
  //userNickName = "";
  userValidateResult = {
    message: {
      username: '',
      userAccount: '',
    },
    orderId: '',
    sku: {
      description: '',
    },
  };
  selectedProduct = {
    description: '',
    price: {
      currency: '',
      amount: '',
    },
    sku: '',
  };

  constructor(
    private location: Location,
    private codaShopService: CodaShopService,
    private activatedRoute: ActivatedRoute,
    public dialog: MatDialog,
    public toastService: ToastService
  ) {}

  ngOnInit(): void {
    this.productName = this.activatedRoute.snapshot.params['product'];
    this._getProductDetail();
    this._getProductInfo();
  }

  goBack() {
    this.location.back();
  }

  _getProductDetail() {
    if (!this.productName || this.productName.length == 0) {
      return;
    }
    this.codaShopService.getProductDetail(this.productName).subscribe(
      (product) => {
        this.productList = product?.data?.result?.skuList || [];
      },
      (err) => {}
    );
  }

  /**
   * Fetch product information
   */
  _getProductInfo() {
    if (!this.productName || this.productName.length == 0) {
      return;
    }
    this.codaShopService.getProductInfo(this.productName).subscribe(
      (product) => {
        this.productInfo = product?.data[0] || [];
      },
      (err) => {}
    );
  }

  productSelectHandler(product) {
    this.selectedProduct = product;
  }

  /**
   * triggered from UI, validate use detail against user entered UserId
   * and selected product. this function will fetch user detail for reconfirmation.
   * return if no product is selected.
   */
  validatePaymentDetail() {
    if (!this.userAccount || this.userAccount.length == 0) {
      this.toastService.showError('Please enter user detail');
      return;
    }
    if (
      !this.selectedProduct ||
      !this.selectedProduct.sku ||
      this.selectedProduct.sku.length == 0
    ) {
      this.toastService.showError(
        'No Product selected. Please select a product fro the list to proceed'
      );
      return;
    }
    this.validatingUserDetail = true;
    let dataToValidate = {
      userAccount: this.userAccount,
      selectedProduct: this.selectedProduct,
    };
    this.codaShopService.validateUserDetail(dataToValidate).subscribe(
      (response: any) => {
        this.validatingUserDetail = false;
        this.userValidated = response?.data?.result ? true : false;
        this.userValidateResult = response?.data?.result || {};
        //this._getCodaTransactionCode();
        /*const dialogRef = this.dialog.open(PaymentPopupComponent, {
          data: this.userValidateResult,
        });
        dialogRef.afterClosed().subscribe(() => { });*/
      },
      (err) => {
        this.validatingUserDetail = false;
      }
    );
  }

  _getCodaTransactionCode() {
    this.initiatingTransaction = true;
    this.codaShopService
      .getCodaTransactionCode(this.userValidateResult)
      .subscribe(
        (response: any) => {
          this.initiatingTransaction = false;
          const dialogRef = this.dialog.open(PaymentPopupComponent, {
            data: response?.data?.initResult?.txnId,
          });
          dialogRef.afterClosed().subscribe(() => {});
        },
        (err) => {
          this.initiatingTransaction = false;
        }
      );
  }

  /**
   * Initiate Transaction
   * this function will get txnId and open iframe for boost login
   */
  initiateTransaction() {
    this._getCodaTransactionCode();
  }

  /**
   * processTopup
   */
  processTopup() {
    this.codaShopService.processTopup(this.userValidateResult).subscribe(
      (response) => {},
      (err) => {}
    );
    //const blockUserData: ViewUserAccessPopupComponentData = {  };
  }
}
