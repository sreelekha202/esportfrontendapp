import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';

import { JumbotronComponentData } from '../../core/jumbotron/jumbotron.component';
import {
  HomeService,
  TournamentService,
  UserService,
  UtilsService,
  VideoLibraryService,
  ToastService,
  LanguageService,
} from '../../core/service';
import { environment } from '../../../environments/environment';
import {
  AppHtmlRoutes,
  AppHtmlRoutesLoginType,
} from '../../app-routing.model';
import { IUser } from '../../shared/models';
import { GlobalUtils } from '../../shared/service/global-utils/global-utils';

enum TopOffersFilter {
  vouchers,
  other,
}

enum OngoingTournamentFilter {
  upcoming,
  ongoing,
  completed,
}

enum TournamentFilter {
  ongoingTournaments,
  allTournaments,
  upcomingTournaments,
}

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  currentUser: IUser;
  AppHtmlRoutes = AppHtmlRoutes;
  AppHtmlRoutesLoginType = AppHtmlRoutesLoginType;

  OngoingTournamentFilter = OngoingTournamentFilter;

  TopOffersFilter = TopOffersFilter;
  topOffersFilter = TopOffersFilter.vouchers;

  TournamentFilter = TournamentFilter;
  tournamentFilter = TournamentFilter.upcomingTournaments;

  ongoingTournaments = [];
  upcomingTournaments = [];

  postData;
  jumbotron: JumbotronComponentData = {
    ribbon: 'Hottest post',
    _id: '',
    slug: '',
    dateTitle: '',
    title: '',
    btn: 'read post',
    user: {
      img: '',
      name: '',
    },
  };
  topOffers = [
    {
      img: './assets/images/Home/top1.png',
      title: 'Free Fire',
    },
    {
      img: './assets/images/Home/top2.png',
      title: 'PUBG',
    },
    {
      img: './assets/images/Home/top3.png',
      title: 'Call of Duty',
    },
    {
      img: './assets/images/Home/top4.png',
      title: 'Mobile Legends',
    },
    {
      img: './assets/images/Home/top5.png',
      title: 'Lord fo Estera',
    },
    {
      img: './assets/images/Home/top6.png',
      title: 'Shellfire',
    },
    {
      img: './assets/images/Home/top7.png',
      title: 'Speed Drifters',
    },
    {
      img: './assets/images/Home/top8.png',
      title: 'Speed Drifters',
    },
  ];
  trendingPosts = [
    // {
    //   id: Math.round(Math.random() * 100),
    //   img: './assets/images/Home/trending1.png',
    //   title: 'drift masters',
    //   text:
    //     'The Ups and Downs of Being An Assassin in Mobile Legends that You May Relate',
    //   date: '6 june',
    // },
    // {
    //   id: Math.round(Math.random() * 100),
    //   img: './assets/images/Home/trending2.png',
    //   title: 'second life',
    //   text:
    //     'The Ups and Downs of Being An Assassin in Mobile Legends that You May Relate',
    //   date: '6 june',
    // },
    // {
    //   id: Math.round(Math.random() * 100),
    //   img: './assets/images/Home/trending3.png',
    //   title: 'second life',
    //   text:
    //     'The Ups and Downs of Being An Assassin in Mobile Legends that You May Relate',
    //   date: 'date',
    // },
    // {
    //   id: Math.round(Math.random() * 100),
    //   img: './assets/images/Home/trending4.png',
    //   title: 'jdm monsters',
    //   text:
    //     'The Ups and Downs of Being An Assassin in Mobile Legends that You May Relate',
    //   date: '6 june',
    // },
  ];
  recentVideo = null;
  paginationData = {
    page: 1,
    limit: 3,
    sort: 'startDate',
  };

  constructor(
    private userService: UserService,
    private homeService: HomeService,
    public translate: TranslateService,
    public datePipe: DatePipe,
    private videoLibraryService: VideoLibraryService,
    private toastService: ToastService,
    public utilsService: UtilsService,
    private tournamentService: TournamentService,
    public languageService: LanguageService
  ) {}

  ngOnInit(): void {
    if (
      GlobalUtils.isBrowser() &&
      localStorage.getItem(environment.currentToken)
    ) {
      this.getUserData();
    }
    this.getTournament();
    this.getHottestPost();
    this.getTrendingPost();
    this.fetchRecentVideo();
  }

  getUserData() {
    this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.currentUser = data;
      } else {
        // const newVal = this.userService.getAuthenticatedUser();
      }
    });
  }

  getTournament() {
    let pagination = JSON.stringify(this.paginationData);
    // let query: any = {
    //   $and: [{ tournamentStatus: 'publish' }],
    //   $or: [{ startDate: { $lt: new Date() } }, { isSeeded: true }],
    // };
    // query = JSON.stringify(query);

    this.tournamentService
      .getPaginatedTournaments({
        status: '0',
        limit: 3,
        sort: 'participantJoined',
      })
      .subscribe(
        (res) => {
          this.upcomingTournaments = res['data']['docs'];
        },
        (err) => {}
      );
  }

  getHottestPost() {
    this.homeService.hottest_post().subscribe((data) => {
      this.postData = data;
      this.jumbotron.user.name = data.data[0]?.authorDetails?.fullName?.replace(
        /\s+/g,
        '<br/>'
      );
      this.jumbotron.dateTitle =
        data.data[0]?.game +
        ' | ' +
        this.datePipe.transform(data.data[0]?.createdDate, 'd MMM');
      this.jumbotron.user.img = data.data[0]?.authorDetails?.profilePicture;
      this.jumbotron.title = data.data[0]?.title;
      this.jumbotron._id = data.data[0]?._id;
      this.jumbotron.slug = data.data[0]?.slug;
    });
  }

  getTrendingPost() {
    this.homeService.trending_posts().subscribe((data) => {
      this.trendingPosts = data.data;
    });
  }

  fetchRecentVideo = async () => {
    try {
      const pagination = {
        page: 1,
        limit: 1,
        pagination: true,
        sort: '-updatedOn',
        projection: ['_id', 'title', 'description', 'youtubeUrl'],
      };
      const query = `?pagination=${encodeURIComponent(
        JSON.stringify(pagination)
      )}`;
      const response = await this.videoLibraryService.fetchVideoLibrary(query);
      this.recentVideo = !response?.data?.docs?.length
        ? null
        : {
            ...response?.data?.docs[0],
            youtubeUrl: await this.utilsService.getEmbbedUrl(
              response?.data?.docs[0].youtubeUrl
            ),
          };
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };
}
