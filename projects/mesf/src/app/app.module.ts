import {
  BrowserModule,
  BrowserTransferStateModule,
} from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  SocialAuthServiceConfig,
  SocialLoginModule,
  GoogleLoginProvider,
  FacebookLoginProvider,
} from 'angularx-social-login';
import { AngularFireMessagingModule } from '@angular/fire/messaging';
//import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';
import { NgxIntlTelInputModule } from 'ngx-intl-tel-input';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';
import { AngularFireModule } from '@angular/fire';
import { DatePipe } from '@angular/common';
import { ChartsModule } from 'ng2-charts';

import { PrivacyPolicyComponent } from './modules/privacy-policy/privacy-policy.component';
import { TermsAndConditionsComponent } from './modules/terms-and-conditions/terms-and-conditions.component';
import { JwtInterceptor } from './core/helpers/interceptors/token-interceptors.service';
import { SpecialOfferModule } from './modules/special-offer/special-offer.module';
import { NotfoundComponent } from './modules/not-found/not-found.component';
import { provideConfig } from './core/service/social-media-auth.service';
import { AboutUsComponent } from './modules/about-us/about-us.component';
import { PaginationService } from './core/service/pagination.service';
import { S3UploadService } from './shared/service/s3Upload.service';
import { TopUpComponent } from './modules/top-up/top-up.component';
import { HeaderComponent } from './core/header/header.component';
import { FooterComponent } from './core/footer/footer.component';
import { appInitializer } from './core/helpers/app.initializer';
import { SharedModule } from './shared/modules/shared.module';
import { HomeComponent } from './modules/home/home.component';
import { PipeModule } from './shared/pipe/pipe.module';
import { Home2Component } from './modules/home2/home2.component';
import { LeaderBoardComponent } from './modules/leader-board/leader-board.component';
import { ToastService } from './core/service/toast.service';
import { environment } from '../environments/environment';
import { AppRoutingModule } from './app-routing.module';
import { UserService } from './core/service';
import { CoreModule } from './core/core.module';
import { I18nModule } from './i18n/i18n.module';
import { AppComponent } from './app.component';
import { InlineSVGModule } from 'ng-inline-svg';
import { FormsModule } from '@angular/forms';
import { GamesComponent } from './modules/games/games.component';
import { MatchmakerDialogComponent } from './modules/games/matchmaker-dialog/matchmaker-dialog.component';
//const config: SocketIoConfig = { url: environment.socketEndPoint, options: {} };

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    Home2Component,
    AboutUsComponent,
    TopUpComponent,
    PrivacyPolicyComponent,
    TermsAndConditionsComponent,
    NotfoundComponent,
    LeaderBoardComponent,
    GamesComponent,
    MatchmakerDialogComponent,
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    BrowserAnimationsModule,
    BrowserTransferStateModule,
    AppRoutingModule,
    CoreModule,
    SharedModule,
    I18nModule,
    SocialLoginModule,
    NgxIntlTelInputModule,
    //SocketIoModule.forRoot(config),
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireMessagingModule,
    PipeModule,
    SpecialOfferModule,
    InlineSVGModule.forRoot(),
    ChartsModule,
    FormsModule,
  ],
  exports: [HeaderComponent, FooterComponent],
  providers: [
    CookieService,
    DatePipe,
    ToastService,
    S3UploadService,
    PaginationService,
    {
      provide: APP_INITIALIZER,
      useFactory: appInitializer,
      multi: true,
      deps: [UserService],
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptor,
      multi: true,
    },
    {
      provide: 'SocialAuthServiceConfig',
      useValue: {
        autoLogin: false,
        providers: [
          {
            id: GoogleLoginProvider.PROVIDER_ID,
            provider: new GoogleLoginProvider(environment.googleAPPID),
          },
          {
            id: FacebookLoginProvider.PROVIDER_ID,
            provider: new FacebookLoginProvider(environment.facebookAPPID),
          },
        ],
      } as SocialAuthServiceConfig,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
