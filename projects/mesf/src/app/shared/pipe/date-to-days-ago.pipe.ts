import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'dateToDayAgo',
})
export class DateAgoPipe implements PipeTransform {
  transform(value: any, args?: any): String {
    if (this.isValidDate(value)) {
      const seconds = Math.floor((+new Date() - +new Date(value)) / 1000);
      if (seconds < 29) return 'Just now';
      const intervals = {
        year: 31536000,
        month: 2592000,
        week: 604800,
        day: 86400,
        hour: 3600,
        minute: 60,
        second: 1,
      };
      let counter;
      for (const key in intervals) {
        counter = Math.floor(seconds / intervals[key]);
        if (counter > 0)
          if (counter === 1) {
            return `${counter} ${key} ago`;
          } else {
            return `${counter} ${key}'s ago`;
          }
      }
    } else {
      return 'Unknown';
    }
  }

  isValidDate(value) {
    if (toString.call(value) === '[object Date]') {
      return true;
    }

    let dateFormat = /[+-]?\d{4}(-[01]\d(-[0-3]\d(T[0-2]\d:[0-5]\d:?([0-5]\d(\.\d+)?)?[+-][0-2]\d:[0-5]\dZ?)?)?)?/;
    return dateFormat.test(value);
  }
}
