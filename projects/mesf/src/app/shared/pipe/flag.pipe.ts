import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'flag',
})
export class FlagPipe implements PipeTransform {
  transform(value) {
    switch (value) {
      case 'Kuwait':
        return 'assets/images/flags/kuwait.png';
      case 'Bahrain':
        return 'assets/images/flags/bahrain.png';
      case 'Saudi Arabia':
        return 'assets/images/flags/saudi.png';
      default:
        return 'assets/images/GamesCards/drift-masters.png';
    }
  }
}
