import { Pipe, PipeTransform } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Pipe({
  name: 'errorPipe',
})
export class ErrorPipe implements PipeTransform {
  errorString = {
    required: 'ERROR.REQUIRED',
    pastStartTime: 'ERROR.PAST_START_TIME',
    pastStartDate: 'ERROR.PAST_START_DATE',
    pastEndDate: 'ERROR.PAST_END_DATE',
    pastEndTime: 'ERROR.PAST_END_TIME',
    max: 'ERROR.MAX',
    min: 'ERROR.MIN',
    maxlength: 'ERROR.MAX_LENGTH_',
    playStoreUrl: 'ERROR.PLAYSTORE_URL',
    appStoreUrl: 'ERROR.APPSTORE_URL',
    website: 'ERROR.WEBSITE_URL',
    youtubeUrl: 'ERROR.YOUTUBE_URL',
    facebookVideoUrl: 'ERROR.FACEBOOK_VIDEO_URL',
    twitchVideoUrl: 'ERROR.TWITCH_VIDEO_URL',
    pattern: 'ERROR.PATTERN', // UNWANTED PATTERN
    none: null,
    email: 'ERROR.EMAIL',
    wait: 'ERROR.WAIT',
    isValidPhoneNumber: 'ERROR.PHONE_NUMBER',
    isNameAvailable: 'ERROR.IS_ALREADY_EXIST',
    multiStageConfigError: 'ERROR.PERFECT_DIVISION',
    invalidUrl: 'ERROR.YOUTUBE_URL_INVALID',
    checkInExpired: 'ERROR.CHECKIN_EXPIRED',
  };

  constructor(private translateService: TranslateService) {}

  transform(value, type?): string {
    if (value) {
      const error = Object.keys(value);
      const key = error.length ? error[0] : 'none';
      if (key === 'maxlength') {
        return `${this.translateService.instant('ERROR.MAX')} ${
          value?.maxlength?.requiredLength || value?.max?.max
        }`;
      } else if (key === 'minlength') {
        return `${this.translateService.instant('ERROR.MIN')} ${
          value?.minlength?.requiredLength || value?.min?.min
        }`;
      } else if (key === 'min') {
        return `${this.translateService.instant('ERROR.MIN')} ${
          value?.min?.requiredLength || value?.min?.min
        }`;
      } else if (key === 'max') {
        return `${this.translateService.instant('ERROR.MAX')} ${
          value?.max?.requiredLength || value?.max?.max
        }`;
      } else if (key === 'pattern') {
        return this.errorString[type] || this.errorString.pattern;
      } else if (key === 'validatePhoneNumber') {
        const isValidPhoneNumber = value?.validatePhoneNumber?.valid;
        return isValidPhoneNumber ? null : this.errorString?.isValidPhoneNumber;
      } else if (key === 'NRIC') {
        return this.translateService.instant('ERROR.NRIC');
      } else {
        return this.errorString[key] || this.errorString.required;
      }
    } else {
      return '';
    }
  }
}
