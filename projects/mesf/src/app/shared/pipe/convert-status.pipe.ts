import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'convertStatus',
})
export class ConvertStatusPipe implements PipeTransform {
  transform(value: string): string {
    switch (parseInt(value)) {
      case 0:
        return 'REDEEMED';
        break;
      case 1:
        return 'NOT REDEEMED';
        break;
      case 2:
        return 'PARTIAL REDEEMED';
        break;
      case 3:
        return 'EXPIRED';
        break;
      default:
        break;
    }
  }
}
