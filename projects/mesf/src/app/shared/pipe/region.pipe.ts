import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'region',
})
export class RegionPipe implements PipeTransform {
  transform(value, currLanguage, dropDownList, type) {
    switch (type) {
      case 'state':
        const state = dropDownList.find((item) => item.name == value);
        return state?.[currLanguage] || '';
      case 'country':
        const country = dropDownList.find((item) => item.name == value);
        return country?.[currLanguage] || '';
      default:
        return null;
    }
  }
}
