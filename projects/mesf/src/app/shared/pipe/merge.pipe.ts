import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'merge',
})
export class MergePipe implements PipeTransform {
  transform(value, type) {
    switch (type) {
      case 'arrayOfString':
        return !Array.isArray(value)
          ? 'N/A'
          : value.length
          ? value.join(',')
          : 'ESPORTS.SECTIONBV2.ALL';
      case 'arrayOfObject':
        return !Array.isArray(value)
          ? 'N/A'
          : value.length
          ? value
              .map(
                (item) =>
                  `<p>
                  ${item?.stage?.toUpperCase() || ''}
                  ${item.venue},
                  ${item.region},
                  ${item.country}
                  </p>`
              )
              .join('')
          : 'N/A';
      default:
        return null;
    }
  }
}
