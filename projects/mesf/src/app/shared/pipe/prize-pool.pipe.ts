import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'prizePool',
})
export class PrizePoolPipe implements PipeTransform {
  transform(value) {
    switch (value) {
      case 0:
        return 'TOURNAMENT.CREATE.PRIZE_1';
      case 1:
        return 'TOURNAMENT.CREATE.PRIZE_2';
      case 2:
        return 'TOURNAMENT.CREATE.PRIZE_3';
      case 3:
        return 'TOURNAMENT.CREATE.PRIZE_4';
      case 4:
        return 'TOURNAMENT.CREATE.PRIZE_5';
      case 5:
        return 'TOURNAMENT.CREATE.PRIZE_6';
      default:
        return 'TOURNAMENT.N_A';
    }
  }
}
