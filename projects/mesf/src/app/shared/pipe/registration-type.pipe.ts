import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'registrationType',
})
export class RegistrationTypePipe implements PipeTransform {
  transform(value: boolean): string {
    if (value === true) {
      return 'Paid';
    }
    return 'Free';
  }
}
