import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SafePipe } from './safe.pipe';
import { NumberSortPipe } from './number-sort.pipe';
import { BasicFilterPipe } from './basic-filter.pipe';
import { ErrorPipe } from './error.pipe';
import { NameByIdPipe } from './nameById.pipe';
import { CustomTranslatePipe } from './custom-translate.pipe';
import { MergePipe } from './merge.pipe';
import { BracketPipe } from './bracket.pipe';
import { DateAgoPipe } from './date-to-days-ago.pipe';
import { PrizePoolPipe } from './prize-pool.pipe';
import { ShortNumberPipe } from './short-number.pipe';
import { CustomStatusPipe } from './custom-status.pipe';
import { CapitalizeFirstPipe } from './capitalizefirst.pipe';
import { FlagPipe } from './flag.pipe';
import { RegionPipe } from './region.pipe';
import { TournamentTypePipe } from './tournament-type.pipe';
import { MinuteSecondsPipe } from './minuteSeconds.pipe';
import { ArticleStatusPipe } from './article-status.pipe';

@NgModule({
  declarations: [
    SafePipe,
    NumberSortPipe,
    BasicFilterPipe,
    ErrorPipe,
    NameByIdPipe,
    CustomTranslatePipe,
    MergePipe,
    BracketPipe,
    DateAgoPipe,
    PrizePoolPipe,
    ShortNumberPipe,
    CustomStatusPipe,
    CapitalizeFirstPipe,
    FlagPipe,
    RegionPipe,
    TournamentTypePipe,
    MinuteSecondsPipe,
    ArticleStatusPipe,
  ],
  imports: [CommonModule],
  providers: [RegionPipe],
  exports: [
    SafePipe,
    NumberSortPipe,
    BasicFilterPipe,
    ErrorPipe,
    NameByIdPipe,
    CustomTranslatePipe,
    MergePipe,
    BracketPipe,
    DateAgoPipe,
    PrizePoolPipe,
    ShortNumberPipe,
    CustomStatusPipe,
    CapitalizeFirstPipe,
    FlagPipe,
    RegionPipe,
    TournamentTypePipe,
    MinuteSecondsPipe,
    ArticleStatusPipe,
  ],
})
export class PipeModule {}
