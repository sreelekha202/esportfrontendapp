import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'bracket',
})
export class BracketPipe implements PipeTransform {
  transform(value) {
    switch (value) {
      case 'single':
        return 'OPTIONS.BRACKET.SINGLE';
      case 'double':
        return 'OPTIONS.BRACKET.DOUBLE';
      case 'round_robin':
        return 'OPTIONS.BRACKET.ROUND_ROBIN';
      case 'battle_royale':
        return 'OPTIONS.BRACKET.BATTLE_ROYALE';
      default:
        return 'N/A';
    }
  }
}
