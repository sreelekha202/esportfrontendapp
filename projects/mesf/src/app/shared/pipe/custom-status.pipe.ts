import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'customStatus',
})
export class CustomStatusPipe implements PipeTransform {
  transform(value) {
    switch (value) {
      case 1:
        return 'initiated';
        break;
      case 2:
        return 'passed';
        break;
      case 3:
        return 'cancel';
        break;
      case 4:
        return 'failed';
        break;
      default:
        return 'initiated';
        break;
    }
    return null;
  }
}
