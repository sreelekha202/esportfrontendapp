import { Pipe, PipeTransform } from '@angular/core';
import { LanguageService, ConstantsService } from '../../core/service';

@Pipe({
  name: 'customTranslate',
})
export class CustomTranslatePipe implements PipeTransform {
  currLanguage;

  constructor(public language: LanguageService) {
    this.language.language.subscribe((langCode) => {
      if (langCode) {
        this.currLanguage = ConstantsService?.language.find(
          (el) => el.code == langCode
        )?.key;
      }
    });
  }

  transform(value, language?) {
    if (value) {
      return !value
        ? 'N/A'
        : !(typeof value === 'object')
        ? value
        : value[this.currLanguage] || this.fetchDefaultValues(value);
    } else {
      return 'N/A';
    }
  }

  fetchDefaultValues(value) {
    for (let item of ConstantsService?.language) {
      if (value[item.key]) {
        return value[item.key];
      }
    }
    return 'N/A';
  }
}
