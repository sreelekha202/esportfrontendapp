export interface IAdvertisementManagement {
  campaignNumber: string;
  campaignName: string;
  advertisementType: string;
  budgetAmount: string;
  targetDevice: any[];
  gender: string;
  gamingPreference: any[];
  payPreference: string;
  bidAmount: string;
  suggestedBidAmount: string;
  budgetLimitType: string;
  checkIconShow: boolean;
}
