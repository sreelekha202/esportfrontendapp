import { TeamPlayerGenerasi } from './team-player-generasi';
export class TeamGenerasi {
  constructor() {
    this.teamMembers = new Array();
    this.substituteMembers = new Array();
  }
  logo: String;
  teamName: String;
  participantState: String;
  schoolName: String;
  name: String;
  email: String;
  managerStatus: String;
  phoneNumber: String;
  participantType: String;
  tournamentId: String;
  inGamerUserId: String;
  teamMembers: TeamPlayerGenerasi[];
  substituteMembers: TeamPlayerGenerasi[];
}
