interface TMatch {
  tournamentID?: String;
  participants?: Array<String>;
  tournamentName?: String;
  LastMessage?: String;
  LastMessageDateAndTime?: String;
  tournamentLogo?: String;
  matchId?: String;
}

export { TMatch };
