export interface IReward {
  _id: string;
  user: string;
  type: string;
  reward_type: string;
  description: string;
  reward_value: string;
  reward_balance: string;
  platform: string;
  createdBy: string;
  updatedBy: string;
  expiredOn: Date;
  createdOn: Date;
  updatedOn: Date;
  contentType: string;
}
