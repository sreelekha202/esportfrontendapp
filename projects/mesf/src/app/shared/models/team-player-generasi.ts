export class TeamPlayerGenerasi {
  name: String;
  inGamerUserId: String;
  account: String;
  idNumber: String;
  gender: String;
  motherName: String;
  motherContactNumber: String;
  motherEmail: String;
  fatherName: String;
  fatherContactNumber: String;
  fatherEmail: String;
  parental: String;
}
