export class IUser {
  id: Number;
  _id: String; // the unique key from backend is _id
  fullName: String;
  userName: String;
  phoneNumber: String;
  phoneisVerified: Boolean;
  email: String;
  emailisVerified: Boolean;
  identifiers: [
    {
      idenType: String;
      uuid: String;
    }
  ];
  accessLevel: String[];
  isPassword: Boolean;
  password: String;
  postalcode: String;
  emailSecond: String;
  coverPicture: String;
  profilePicture: String;
  aboutMe: String;
  country: String;
  state: String;
  progress: Number;
  havePin: Boolean;
  token: String;
  gender: String;
  gamesPlayed: String;
  martialStatus: String;
  parentalStatus: String;
  dob: Date;
  shortBio: String;
  profession: String;
  postalCode: Number;
  status: Number;
  createdOn: Date;
  updatedOn: Date;
  preference: any;
  accountDetail: {
    reward: Number;
    paymentType: string;
    paymentAccountId: string;
    isPaymentAccountVerified: string;
  };
  accountType: String;
  firstLogin: Number;
  isInfluencer: Number;
  isVerified: Number;
  organizerRating: Number;
  isPrivacyPolicyAccepted: Boolean;
}
