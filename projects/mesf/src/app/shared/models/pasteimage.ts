interface PasteImage {
  imagedata?: string;
  inProgress?: boolean;
  progress?: number;
  matchid?: string;
  attachmenturl?: string;
  thumburl?: string;
  ftype?: string;
}

export { PasteImage };
