import { IParticipant } from './participant';
import { ITournament } from './tournament';

interface IMatch {
  _id?: string;
  disQualifiedTeam?: object;
  teamA?: IParticipant;
  teamB?: IParticipant;
  bye?: boolean;
  matchStatus?: string;
  matchNo?: number;
  loserTeam?: IParticipant;
  winnerTeam?: IParticipant;
  placeHolderA?: string;
  placeHolderB?: string;
  sets?: IMatchSet[];
  totalSet?: number;
  teamAWinSet?: number;
  teamBWinSet?: number;
  isThirdPlaceMatch?: boolean;
  isNextMatchStart?: boolean;
  status?: number;
  tournamentId?: ITournament;
  createdOn?: Date;
  updatedOn?: Date;
  refereeId?: string;
  currentMatch?: object;
  loserNextMatch?: object;
  winnerNextMatch?: object;
}

interface IMatchSet {
  id?: number;
  teamAScore?: number;
  teamBScore?: number;
  winner?: null;
  status?: string;
  teamAScreenShot?: string;
  teamBScreenShot?: string;
}

export { IMatch, IMatchSet };
