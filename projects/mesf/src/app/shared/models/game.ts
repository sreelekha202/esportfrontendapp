interface IGame {
  _id?: string;
  name?: string;
  logo?: string;
  image?: string;
  activeTournament?: number;
}

export { IGame };
