import { IUser } from './user';
export interface IMessage {
  _id: string;
  toUser: string;
  sentBy: string;
  senderDetails: IUser;
  subject: string;
  message: string;
  seen: boolean;
  status: number;
  createdBy: string;
  createdAt: string;
  updatedAt: string;
  isChecked: boolean;
}
