export interface IBracket {
  id?: string;
  name?: string;
  bracketType?: string;
  maximumParticipants?: number;
  noOfRoundPerGroup?: number;
  isKnowParticipantName?: boolean;
  maximumRoundsPerday?: number;
  isBattleRoyal?: boolean;
  createdBy?: string;
  updatedBy?: string;
}
