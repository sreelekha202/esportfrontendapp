import { IGame } from './game';
import { IUser } from './user';

interface IArticle {
  _id?: string;
  game?: string;
  gameDetails?: IGame;
  genre?: string[];
  tags?: any[];
  highlight?: number;
  minRead?: number;
  location?: string;
  isSponsor?: boolean;
  isInfluencer?: boolean;
  isInfluencerHighlight?: boolean;
  status?: string;
  title?: string;
  slug?: string;
  content?: string;
  category?: string;
  platform?: string;
  author?: string;
  image?: string;
  shortDescription?: string;
  publishDate?: Date;
  likes?: number;
  id?: string;
  authorDetails?: IUser;
  createdDate?: Date;
  totalLikes?: number;
  type?: string;
  isBookmarked?: boolean;

  // For loader
  bookmarkProccesing?: boolean;
  likeProccesing?: boolean;
}

export { IArticle };
