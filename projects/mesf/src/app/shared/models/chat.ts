export interface IChat {
  matchid?: string;
  tournamentid?: string;
  fullName?: string;
  userid?: string;
  msg?: string;
  messagetype?: string;
  updatedAt?: number;
  attachmenturl?: string;
  thumbnailurl?: string;
  filesattached?: Array<any>;
  typeofchat?: string;
  matchName?: string;
  matchImage?: string;
  matchLocation?: string;
  chatusername?: string;
}
