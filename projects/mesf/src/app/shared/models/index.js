import { IMatch, IMatchSet } from "./match";
import { IVideoLibrary } from "./video-library";
import { IBracket } from "./bracket";
import { IAdvertisementManagement } from "./advertisement";
import { IArticle } from "./articles";
import { IChat } from "./chat";
import { IGame } from "./game";
import { IMessage } from "./message";
import { IReward } from "./reward";
import { ITournament } from "./tournament";
import { IUser } from "./user";
import { IParticipant } from "./participant";
import { IPagination } from "./pagination";

export {
  IMatch,
  IMatchSet,
  IVideoLibrary,
  IBracket,
  IAdvertisementManagement,
  IArticle,
  IChat,
  IGame,
  IMessage,
  IReward,
  ITournament,
  IUser,
  IParticipant,
  IPagination,
};
