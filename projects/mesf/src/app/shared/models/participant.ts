export interface IParticipant {
  _id?: string;
  teamMembers?: [];
  seed?: number;
  createdOn?: Date;
  updatedOn?: Date;
  checkedIn: boolean;
  status?: number;
  participantStatus?: string;
  matchWin?: number;
  matchLoss?: number;
  matchTie?: number;
  logo?: string;
  teamName?: string;
  name?: string;
  phonenumber?: string;
  email: string;
  tournamentUsername?: string;
  inGamerUserId?: string;
  participantType?: string;
  tournamentId?: string;
  userId?: string;
}
