import { TestBed } from '@angular/core/testing';

import { AdminControlGuard } from './admin-control.guard';

describe('AdminControlGuard', () => {
  let guard: AdminControlGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(AdminControlGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
