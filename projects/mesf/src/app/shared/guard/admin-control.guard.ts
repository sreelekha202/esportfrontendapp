import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { Observable } from 'rxjs';
import { UserService, AuthServices, ToastService } from '../../core/service';
import { environment } from '../../../environments/environment';
import { Router } from '@angular/router';
import { UserAccessType } from '../../modules/admin/components/access-management/access-management.model';
import 'rxjs/Rx';
import { GlobalUtils } from '../service/global-utils/global-utils';

@Injectable({
  providedIn: 'root',
})
export class AdminControlGuard implements CanActivate {
  constructor(
    private userService: UserService,
    private router: Router,
    private toastService: ToastService
  ) {}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    let url: string = state.url;
    if (GlobalUtils.isBrowser()) {
      return this.checkAdmin(url);
    } else {
      this.toastService.showInfo('Please login to access that page.');
      this.router.navigate(['/user/phone-login']);
      return false;
    }
  }

  checkAdmin(url: string): any {
    return new Promise((resolve, reject) => {
      this.userService.getProfile().subscribe(
        (response: any) => {
          if (
            response.data &&
            response.data.accountType &&
            response.data.accountType === 'admin'
          ) {
            let _moduleType;
            switch (url && url.split('?')[0]) {
              case '/admin': {
                response.data.accessLevel.push('admin');
                _moduleType = 'admin';
                break;
              }
              case '/admin/site-configuration': {
                _moduleType = UserAccessType.sc;
                break;
              }
              case '/admin/access-management': {
                _moduleType = UserAccessType.acm;
                break;
              }
              case '/admin/esports-management': {
                _moduleType = UserAccessType.em;
                break;
              }
              case '/admin/content-management': {
                _moduleType = UserAccessType.cm;
                break;
              }
              case '/admin/user-notifications': {
                _moduleType = UserAccessType.un;
                break;
              }
              case '/admin/user-management': {
                _moduleType = UserAccessType.um;
                break;
              }
              default: {
                _moduleType = null;
                break;
              }
            }
            if (
              _moduleType &&
              response.data.accessLevel.includes(_moduleType)
            ) {
              resolve(true);
            } else {
              this.router.navigate(['/404']);
              resolve(false);
            }
          } else {
            this.toastService.showInfo('Please login to access that page.');
            this.router.navigate(['/user/phone-login']);
            resolve(false);
          }
        },
        (fail: any) => {
          this.toastService.showInfo('Please login to access that page.');
          this.router.navigate(['/user/phone-login']);
          resolve(false);
        }
      );
    });
  }
}
