import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-counter',
  templateUrl: './counter.component.html',
  styleUrls: ['./counter.component.scss'],
})
export class CounterComponent implements OnInit {
  @Input() title: string;
  @Input() customFormControlName: string;
  @Input() customFormGroup: FormGroup;
  @Input() alertMessage: string;
  @Input() readOnly: boolean;
  @Input() hideTitle: boolean;
  @Output() valueEmit = new EventEmitter<number>();

  constructor() {}

  ngOnInit(): void {}

  increment() {
    const control = this.customFormGroup.get(this.customFormControlName);
    control.markAsTouched({ onlySelf: true });
    let val = this.customFormGroup.get(this.customFormControlName).value;
    val++;
    this.customFormGroup.controls[this.customFormControlName].setValue(val);
    this.counter(val);
  }

  decrement() {
    const control = this.customFormGroup.get(this.customFormControlName);
    control.markAsTouched({ onlySelf: true });
    let val = this.customFormGroup.get(this.customFormControlName).value;
    if (val > 1) {
      val--;
    }
    this.customFormGroup.controls[this.customFormControlName].setValue(val);
    this.counter(val);
  }

  counter(value) {
    this.valueEmit.emit(value || 0);
  }
}
