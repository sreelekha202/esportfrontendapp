import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-text-area',
  templateUrl: './text-area.component.html',
  styleUrls: ['./text-area.component.scss'],
})
export class TextAreaComponent implements OnInit {
  @Input() title: string;
  @Input() customFormControlName: string;
  @Input() customFormGroup: FormGroup;
  @Input() required: string;
  @Input() type: string;
  @Input() patternType: string;
  @Input() readOnly: boolean;
  @Input() row: number = 3;
  @Input() enableSearch: boolean;
  @Input() searchMessage: string;
  @Input() enableFormGroupValidation: boolean;

  @Output() valueEmit = new EventEmitter<any>();

  constructor() {}

  ngOnInit(): void {}

  setValue(val) {
    this.customFormGroup.controls[this.customFormControlName].setValue(val);
    this.valueEmit.emit(val);
  }

  valuesLog(data) {
  }
}
