import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { LanguageService } from '../../../../core/service';

@Component({
  selector: 'app-drop-down',
  templateUrl: './drop-down.component.html',
  styleUrls: ['./drop-down.component.scss'],
})
export class DropDownComponent implements OnInit {
  @Input() title: string;
  @Input() type: string;
  @Input() customFormControlName: string;
  @Input() customFormGroup: FormGroup;
  @Input() dropdownList;
  @Input() filterType;
  @Output() valueEmit = new EventEmitter<number>();

  currentLang: string = 'en';

  constructor(private languageService: LanguageService) {}

  ngOnInit(): void {
    this.languageService.language.subscribe(
      (lang) => (this.currentLang = lang)
    );
  }

  setValue(val) {
    this.customFormGroup.controls[this.customFormControlName].setValue(val);
    this.valueEmit.emit(val);
  }

  setValueInArray(val) {
    const array = this.customFormGroup.value[this.customFormControlName];
    const index = array.indexOf(val);
    if (index < 0) {
      array.push(val);
      this.customFormGroup.controls[this.customFormControlName].setValue(array);
    }
    this.valueEmit.emit(val);
  }
}
