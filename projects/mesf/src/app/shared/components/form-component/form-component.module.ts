import {
  NgModule,
  NO_ERRORS_SCHEMA,
  CUSTOM_ELEMENTS_SCHEMA,
} from '@angular/core';
import { CommonModule } from '@angular/common';

import { CounterComponent } from './counter/counter.component';
import { DropDownComponent } from './drop-down/drop-down.component';
import { ToggleSwitchComponent } from './toggle-switch/toggle-switch.component';
import { InputComponent } from './input/input.component';
import { UploadComponent } from './upload/upload.component';
import { PhoneNoComponent } from './phone-no/phone-no.component';
import { PhoneNoCustomComponent } from './phone-no-custom/phone-no-custom.component';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxIntlTelInputModule } from 'ngx-intl-tel-input';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PipeModule } from '../../pipe/pipe.module';
import { DirectivesModule } from '../../directives/directive.module';
import { TextAreaComponent } from './text-area/text-area.component';
import { LoadingModule } from '../../../core/loading/loading.module';
import { I18nModule } from '../../../i18n/i18n.module';
@NgModule({
  declarations: [
    CounterComponent,
    DropDownComponent,
    ToggleSwitchComponent,
    InputComponent,
    UploadComponent,
    PhoneNoComponent,
    PhoneNoCustomComponent,
    TextAreaComponent,
  ],
  imports: [
    CommonModule,
    FontAwesomeModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    PipeModule,
    NgxIntlTelInputModule,
    DirectivesModule,
    I18nModule,
    LoadingModule,
  ],
  exports: [
    CounterComponent,
    DropDownComponent,
    ToggleSwitchComponent,
    InputComponent,
    UploadComponent,
    PhoneNoComponent,
    PhoneNoCustomComponent,
    TextAreaComponent,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
})
export class FormComponentModule {}
