import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import {
  SearchCountryField,
  CountryISO,
} from 'ngx-intl-tel-input';

@Component({
  selector: 'app-phone-no',
  templateUrl: './phone-no.component.html',
  styleUrls: ['./phone-no.component.scss'],
})
export class PhoneNoComponent implements OnInit {
  @Input() title: string;
  @Input() customFormControlName: string;
  @Input() customFormGroup: FormGroup;
  @Input() readOnly: boolean;
  @Input() separateDialCode: boolean = false;

  @Output() valueEmit = new EventEmitter<any>();

  SearchCountryField = SearchCountryField;
  CountryISO = CountryISO;

  constructor() {}

  ngOnInit(): void {}
}
