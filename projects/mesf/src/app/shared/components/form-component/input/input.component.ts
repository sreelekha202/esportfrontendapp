import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss'],
})
export class InputComponent implements OnInit {
  @Input() title: string;
  @Input() customFormControlName: string;
  @Input() customFormGroup: FormGroup;
  @Input() required: string;
  @Input() type: string;
  @Input() patternType: string;
  @Input() readOnly: boolean;
  @Input() class: any;
  @Input() enableSearch: boolean;
  @Input() searchMessage: string;
  @Input() enableFormGroupValidation: boolean;

  @Output() valueEmit = new EventEmitter<any>();

  constructor() {}

  ngOnInit(): void {}

  setValue(val) {
    this.customFormGroup.controls[this.customFormControlName].setValue(val);
    this.valueEmit.emit(val);
  }
}
