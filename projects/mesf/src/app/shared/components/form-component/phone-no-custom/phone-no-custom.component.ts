import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import {
  SearchCountryField,
  CountryISO,
} from 'ngx-intl-tel-input';

@Component({
  selector: 'app-phone-no-custom',
  templateUrl: './phone-no-custom.component.html',
  styleUrls: ['./phone-no-custom.component.scss'],
})
export class PhoneNoCustomComponent implements OnInit {
  @Input() title: string;
  @Input() public customFormControlName: string;
  @Input() customFormGroup: FormGroup;
  @Input() readOnly: boolean;
  @Input() separateDialCode: boolean = false;

  @Output() valueEmit = new EventEmitter<any>();

  SearchCountryField = SearchCountryField;
  CountryISO = CountryISO;

  constructor() {}

  ngOnInit(): void {}
}
