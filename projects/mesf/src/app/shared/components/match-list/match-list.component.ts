import {
  Component,
  OnInit,
  Input,
  OnChanges,
  SimpleChanges,
} from '@angular/core';
import {
  BracketService,
  LanguageService,
  ToastService,
} from '../../../core/service';
import { TranslateService } from '@ngx-translate/core';
import { MatDialog } from '@angular/material/dialog';
import { OpenTeamDetailsComponent } from './open-team-details/open-team-details.component';
@Component({
  selector: 'app-match-list',
  templateUrl: './match-list.component.html',
  styleUrls: [
    './match-list.component.scss',
    '../../../modules/tournament/tournament-management/tournament-management.component.scss',
  ],
})
export class MatchListComponent implements OnInit, OnChanges {
  @Input() tournamentId;
  @Input() bracketType;
  @Input() numberOfWinning;
  @Input() participantType;

  round: Array<any> = [];
  stage: Array<any> = [];
  tHeader: Array<any> = [];
  isLoaded = false;
  standingRound: Array<any> = [];

  constructor(
    private bracketService: BracketService,
    private translateService: TranslateService,
    private languageService: LanguageService,
    private toastService: ToastService,
    private matDialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.languageService.language.subscribe((lang) =>
      this.translateService.use(lang || 'en')
    );
  }

  fetchDistinctRound = async () => {
    try {
      const queryParam = `?query=${encodeURIComponent(
        JSON.stringify({ tournamentId: this.tournamentId })
      )}`;
      const field = this.getFieldType(this.bracketType);
      const response = await this.bracketService.fetchDistinctValue(
        queryParam,
        field
      );
      this.round = response.data.map((r) => {
        return {
          id: r,
          isCollapsed: false,
          isLoaded: false,
        };
      });
      this.isLoaded = true;
    } catch (error) {
      this.isLoaded = true;
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  fetchDistinctStage = async () => {
    try {
      const queryParam = `?query=${encodeURIComponent(
        JSON.stringify({ tournamentId: this.tournamentId })
      )}`;
      const field = this.getFieldType(this.bracketType);
      const response = await this.bracketService.fetchDistinctValue(
        queryParam,
        field
      );
      this.stage = response.data.map((r) => {
        return {
          id: r,
          isCollapsed: false,
          isLoaded: false,
        };
      });
      this.isLoaded = true;
    } catch (error) {
      this.isLoaded = true;
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  fetchDistictStageGroup = async (stage, i) => {
    try {
      if (stage.isCollapsed && !stage.isLoaded) {
        const queryParam = `?query=${encodeURIComponent(
          JSON.stringify({
            tournamentId: this.tournamentId,
            'currentMatch.stage': stage.id,
          })
        )}`;
        const response = await this.bracketService.fetchDistinctValue(
          queryParam,
          'currentMatch.group'
        );
        this.stage[i].isLoaded = true;
        this.stage[i].group = response.data.map((g) => {
          return {
            id: g,
            isCollapsed: false,
            isLoaded: false,
          };
        });
      }
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  fetchDistictStageGroupRound = async (stage, group, i, j) => {
    try {
      if (group.isCollapsed && !group.isLoaded) {
        if (this.bracketType == 'battle_royale') {
          const queryParam = `?stage=${stage.id}&group=${group.id}&tournamentId=${this.tournamentId}`;
          const response = await this.bracketService.fetchStanding(queryParam);
          this.stage[i].group[j].standings = response.data;
          this.standingRound = response.data.length
            ? response.data[0].round
            : [];
        } else {
          const queryParam = `?query=${encodeURIComponent(
            JSON.stringify({
              tournamentId: this.tournamentId,
              'currentMatch.stage': stage.id,
              'currentMatch.group': group.id,
            })
          )}`;
          const response = await this.bracketService.fetchDistinctValue(
            queryParam,
            'currentMatch.round'
          );
          this.stage[i].group[j].round = response.data.map((r) => {
            return {
              id: r,
              isCollapsed: false,
              isLoaded: false,
            };
          });
        }
        this.stage[i].group[j].isLoaded = true;
      }
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  fetchDistictStageGroupRoundMatch = async (stage, group, round, i, j, k) => {
    try {
      if (round.isCollapsed && !round.isLoaded) {
        const queryParam = `?query=${encodeURIComponent(
          JSON.stringify({
            tournamentId: this.tournamentId,
            'currentMatch.stage': stage.id,
            'currentMatch.group': group.id,
            'currentMatch.round': round.id,
            matchStatus: { $ne: 'inactive' },
            bye: false,
          })
        )}&select=sets,totalSet,matchNo,winnerTeam,loserTeam,disQualifiedTeam,battleRoyalTeams`;
        const response = await this.bracketService.fetchAllMatches(queryParam);
        this.stage[i].group[j].round[k].match = response.data;
        this.stage[i].group[j].round[k].isLoaded = true;
        const totalSet = response.data.reduce((accumulator, currentValue) => {
          const max =
            currentValue.sets.length > currentValue.totalSet
              ? currentValue.sets.length
              : currentValue.totalSet;
          accumulator = max > accumulator ? max : accumulator;
          return accumulator;
        }, 0);
        response.data.length
          ? this.createSetHeader(totalSet)
          : this.createSetHeader(0);
      }
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  fetchMatches = async (round, i) => {
    try {
      if (round.isCollapsed && !round.isLoaded) {
        const queryParam = `?query=${encodeURIComponent(
          JSON.stringify({
            tournamentId: this.tournamentId,
            'currentMatch.round': round.id,
            matchStatus: { $ne: 'inactive' },
            bye: false,
          })
        )}&select=sets,totalSet,matchNo,winnerTeam,loserTeam,disQualifiedTeam,battleRoyalTeams`;
        const response = await this.bracketService.fetchAllMatches(queryParam);
        if (this.bracketType == 'battle_royale') {
        } else {
          const totalSet = response.data.reduce((accumulator, currentValue) => {
            const max =
              currentValue.sets.length > currentValue.totalSet
                ? currentValue.sets.length
                : currentValue.totalSet;
            accumulator = max > accumulator ? max : accumulator;
            return accumulator;
          }, 0);
          response.data.length
            ? this.createSetHeader(totalSet)
            : this.createSetHeader(0);
        }
        this.round[i].isLoaded = true;
        this.round[i].match = response.data;
      }
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  createSetHeader(noOfSet) {
    if (this.tHeader.length < noOfSet) {
      this.tHeader = [];
      for (let i = 0; i < noOfSet; i++) {
        this.tHeader.push({ name: `Set ${i + 1}` });
      }
    }
  }

  remainingtHeader(th, mh) {
    const rHeader = [];
    for (let i = 0; i < th.length - mh.length; i++) {
      rHeader.push({});
    }
    return rHeader;
  }

  getFieldType = (type) => {
    if (['single', 'double'].includes(type)) {
      return 'currentMatch.round';
    } else if (['round_robin', 'battle_royale'].includes(type)) {
      return 'currentMatch.stage';
    } else {
      return 'currentMatch.round';
    }
  };

  ngOnChanges(simpleChanges: SimpleChanges) {
    if (
      simpleChanges.hasOwnProperty('tournamentId') &&
      simpleChanges.tournamentId.currentValue &&
      simpleChanges.hasOwnProperty('bracketType') &&
      simpleChanges.bracketType.currentValue
    ) {
      if (['single', 'double'].includes(this.bracketType)) {
        this.fetchDistinctRound();
      } else if (['round_robin', 'battle_royale'].includes(this.bracketType)) {
        this.fetchDistinctStage();
      }
    }
  }

  openTeamDetails = async (player) => {
    const confirmed = await this.matDialog
      .open(OpenTeamDetailsComponent, { data: player })
      .afterClosed()
      .toPromise();
  };
}
