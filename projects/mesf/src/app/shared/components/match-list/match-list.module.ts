import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatchListComponent } from './match-list.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {
  FontAwesomeModule,
  FaIconLibrary,
} from '@fortawesome/angular-fontawesome';
import { faSortDown } from '@fortawesome/free-solid-svg-icons';
import { LoadingModule } from '../../../core/loading/loading.module';
import { I18nModule } from '../../../i18n/i18n.module';
import { OpenTeamDetailsComponent } from './open-team-details/open-team-details.component';

@NgModule({
  declarations: [MatchListComponent, OpenTeamDetailsComponent],
  imports: [
    CommonModule,
    NgbModule,
    FontAwesomeModule,
    I18nModule,
    LoadingModule,
  ],
  exports: [MatchListComponent],
})
export class MatchListModule {
  constructor(private library: FaIconLibrary) {
    library.addIcons(faSortDown);
  }
}
