import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import {
  CommentService,
  FormService,
  ToastService,
} from '../../../../core/service';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.scss'],
})
export class MessageComponent implements OnInit {
  @Input() placeholder: string;
  @Input() DOM_ID: string;
  @Input() objectId: string;
  @Input() objectType: string;
  @Input() enableInput: boolean;
  @Input() text: string;
  @Input() _id: string;

  @Output() valueEmitOnSubmit = new EventEmitter<any>();

  messageForm: FormGroup;
  isProcessing: boolean = false;

  constructor(
    private fb: FormBuilder,
    private formService: FormService,
    private commentService: CommentService,
    private toastService: ToastService
  ) {}

  ngOnInit(): void {
    this.messageForm = this.fb.group({
      text: [
        '',
        Validators.compose([Validators.required, this.formService.emptyCheck]),
      ],
    });
  }

  ngOnChanges() {
    if (this.text) {
      this.messageForm?.get('text')?.setValue(this.text);
    }
  }

  onSubmit = async () => {
    try {
      const payload = {
        comment: this.messageForm.value.text,
        objectId: this.objectId,
        objectType: this.objectType,
        ...(this._id && { _id: this._id }),
      };

      this.isProcessing = true;
      const message = await this.commentService.upsertComment(payload);
      this.isProcessing = false;
      this.messageForm.reset();
      this.valueEmitOnSubmit.emit({ ...message?.data, isCommentedUser: true });
    } catch (error) {
      this.isProcessing = false;
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };
}
