import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  SimpleChanges,
  OnChanges,
} from '@angular/core';
import {
  BracketService,
  UtilsService,
  ToastService,
} from '../../../../core/service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { IMatch, IMatchSet } from '../../../../shared/models';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-score-card',
  templateUrl: './score-card.component.html',
  styleUrls: ['./score-card.component.scss'],
})
export class ScoreCardComponent implements OnInit, OnChanges {
  @Input() match: IMatch;
  @Input() isAdmin: boolean;
  @Input() participantId;
  @Input() isScreenShotRequired: boolean = false;
  @Input() enableWebview: boolean = false;
  @Input() scoreReporting: number = 1;

  @Output() exit = new EventEmitter<any>();

  matchDetails: IMatch;
  screenShot: string;
  participantSet: IMatchSet;
  disQualifyStatus: Array<string> = [
    'No show',
    'Walkout',
    'Rule violation',
    'Improper behavior',
  ];
  teamList: Array<any> = [];
  disQualifiedTeam: any = {
    team: null,
    reason: '',
  };
  isLoaded = false;

  constructor(
    private bracketService: BracketService,
    private toastService: ToastService,
    private modalService: NgbModal,
    private translateService: TranslateService,
    private utilsService: UtilsService
  ) {}

  ngOnInit(): void {}

  setMatchDetails(md) {
    this.matchDetails = md;
  }

  /**
   * fetch Match Details
   */
  fetchMatchDetails = async () => {
    try {
      this.isLoaded = true;
      const queryParam = `?query=${encodeURIComponent(
        JSON.stringify({
          tournamentId: this.match?.tournamentId,
          _id: this.match?._id,
        })
      )}`;
      const match = await this.bracketService.fetchAllMatches(queryParam);
      this.matchDetails = match?.data?.length ? match.data[0] : null;
      if (this.matchDetails) {
        this.teamList = [
          { ...this.matchDetails.teamA },
          { ...this.matchDetails.teamB },
        ];
        this.disQualifiedTeam = this.matchDetails?.disQualifiedTeam;
      }
      this.isLoaded = false;
    } catch (error) {
      this.isLoaded = false;
      this.showPlatformBasedAlert(
        'showError',
        error?.error?.message || error?.message
      );
    }
  };

  /**
   * Exit from Score screen by passing refresh(true) to fetch latest bracket Scores
   */
  exitFromPage() {
    this.exit.emit({ refresh: true, isOpenScoreCard: false });
  }

  /**
   * Update score by both admin and reletad participant
   * But after finishing the set only admin can update the set score
   * After next match start even admin can't update the score of previous match
   * @param s set
   * @param value value
   * @param key key
   * @param delta delta
   * @param teamId teamId
   */
  updateScore(s, value, key, delta, teamId) {
    if (this.isAdmin && this.matchDetails.isNextMatchStart) {
      return;
    } else if (!this.isAdmin) {
      if (this.participantId !== teamId) {
        return;
      }
      if (
        this.participantId === teamId &&
        this.matchDetails.matchStatus === 'completed'
      ) {
        return;
      }

      if (this.scoreReporting <= 1) {
        return;
      }

      if (s?.status === 'completed') {
        return;
      }
    }

    s[key] = s[key] <= 0 && delta === '-1' ? value : value + delta;
    s.modify = true;
    this.participantSet = { ...this.participantSet, [key]: s[key], id: s?.id };
  }

  /**
   * Admin submit the update score
   * @param set set
   */
  submitScore = async (set) => {
    try {
      this.isLoaded = true;
      const queryParam = `?query=${encodeURIComponent(
        JSON.stringify({
          _id: this.matchDetails._id,
          'sets.id': set.id,
          tournamentId: this.matchDetails?.tournamentId?._id,
        })
      )}`;

      const payload = {
        $set: {
          'sets.$.teamAScore': set.teamAScore,
          'sets.$.teamBScore': set.teamBScore,
          ...(set.teamAScore == set.teamBScore && { winner: null }),
        },
      };
      const match = await this.bracketService.updateMatch(queryParam, payload);
      if (!match.success) {
        this.showPlatformBasedAlert('showInfo', match.message);
      }
      this.isLoaded = false;
      this.fetchMatchDetails();
    } catch (error) {
      this.isLoaded = false;
      this.showPlatformBasedAlert(
        'showError',
        error?.error?.message || error?.message
      );
    }
  };

  /**
   * Get player name by _id
   * @param id teamId
   */
  getPlayerName(id) {
    if (this.matchDetails?.teamA?._id === id) {
      return this.matchDetails?.teamA?.teamName;
    } else if (this.matchDetails?.teamB?._id === id) {
      return this.matchDetails?.teamB?.teamName;
    } else {
      return null;
    }
  }

  /**
   * Open popUp for participants to upload screenshot/view
   * Open popup for Admin to view uploaded screenshot
   * @param content model template
   * @param set set
   * @param player player
   */
  open = async (content, set, player) => {
    try {
      this.participantSet = set;
      this.screenShot = player;
      // this.toastService.showInfo('This feature is under development');
      const result = await this.modalService.open(content, {
        ariaLabelledBy: 'modal-basic-title',
        windowClass: 'modal-upload-screenshot',
        centered: true,
      }).result;
    } catch (error) {
      // this.toastService.showInfo('Again, This feature is under development');
    }
  };

  ngOnChanges(changes: SimpleChanges) {
    if (changes.hasOwnProperty('match') && changes.match.currentValue) {
      this.fetchMatchDetails();
    }
  }

  // convert image to base64
  participantScreenshot(event) {
    const files = event.target.files;
    if (files && files[0]) {
      const reader = new FileReader();
      reader.onload = this._handleReaderLoaded.bind(this);
      reader.readAsBinaryString(files[0]);
    }
  }

  _handleReaderLoaded(readerEvt) {
    const binaryString = readerEvt.target.result;
    this.participantSet[this.screenShot] =
      'data:image/png;base64,' + btoa(binaryString);
  }

  /**
   * Participant update thier score
   */
  submitParticipantScore = async () => {
    try {
      this.isLoaded = true;
      const queryParam = `?query=${encodeURIComponent(
        JSON.stringify({
          _id: this.matchDetails?._id,
          'sets.id': this.participantSet.id,
          tournamentId: this.matchDetails?.tournamentId?._id,
        })
      )}`;
      const payload = {
        $set: this.convertObject(this.participantSet),
      };
      const response = await this.bracketService.updateParticipantScore(
        queryParam,
        payload
      );
      this.showPlatformBasedAlert('showSuccess', response?.message);
      this.isLoaded = false;
      this.fetchMatchDetails();
    } catch (error) {
      this.isLoaded = false;
      this.showPlatformBasedAlert(
        'showError',
        error?.error?.message || error?.message
      );
    }
  };

  /**
   * Modify object based on request payload
   * @param obj obj
   */
  convertObject(obj) {
    const modifiedObj = {};
    for (const key in obj) {
      modifiedObj[`sets.$.${key}`] = obj[key];
    }
    return modifiedObj;
  }

  /**
   * Admin can disqualify any team from the match
   */
  disqualify = async () => {
    try {
      const invalid = Object.values(this.disQualifiedTeam).some(
        (item) => !item
      );
      if (invalid) {
        this.showPlatformBasedAlert(
          'showInfo',
          this.translateService.instant('ELIMINATION.DISQUALIFY_REQUIRED')
        );
        return;
      }
      this.isLoaded = true;
      const loserTeam = this.disQualifiedTeam.team;
      const winnerTeam = this.teamList.find((team) => team._id !== loserTeam)
        ?._id;
      const payload = {
        disQualifiedTeam: this.disQualifiedTeam,
        winnerTeam,
        loserTeam,
        matchStatus: 'completed',
      };
      const queryParam = `?query=${encodeURIComponent(
        JSON.stringify({
          _id: this.matchDetails?._id,
          tournamentId: this.matchDetails?.tournamentId?._id,
        })
      )}`;
      const match = await this.bracketService.updateMatch(queryParam, payload);

      this.showPlatformBasedAlert(
        match.success ? 'showSuccess' : 'showInfo',
        match.message
      );
      this.isLoaded = false;
      this.fetchMatchDetails();
    } catch (error) {
      this.isLoaded = false;
      this.showPlatformBasedAlert(
        'showError',
        error?.error?.message || error?.message
      );
    }
  };

  showPlatformBasedAlert(type, message) {
    this.enableWebview
      ? this.utilsService.showNativeAlert(message)
      : this.toastService[type](message);
  }
}
