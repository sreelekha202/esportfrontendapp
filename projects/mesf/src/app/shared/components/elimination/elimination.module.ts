import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SingleEliminationComponent } from './single-elimination/single-elimination.component';
import { DoubleEliminationComponent } from './double-elimination/double-elimination.component';
import { RoundRobinComponent } from './round-robin/round-robin.component';
import { MatchComponent } from './match/match.component';
import { ScoreCardComponent } from './score-card/score-card.component';
import { EliminationComponent } from './elimination.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AwardsComponent } from './awards/awards.component';
import { ProgressbarModule } from 'ngx-bootstrap/progressbar';
import {
  FontAwesomeModule,
  FaIconLibrary,
} from '@fortawesome/angular-fontawesome';
import {
  faTrophy,
  faPencilAlt,
  faSortUp,
  faSortDown,
  faCommentAlt,
  faCaretSquareUp,
  faCheckCircle,
} from '@fortawesome/free-solid-svg-icons';

import { CarouselModule } from 'ngx-bootstrap/carousel';
import { TabsModule } from 'ngx-bootstrap/tabs';

import { FormsModule } from '@angular/forms';
import { LoadingModule } from '../../../core/loading/loading.module';
import { PipeModule } from '../../../shared/pipe/pipe.module';
import { BattleRoyaleMultiComponent } from './battle-royale-multi/battle-royale-multi.component';
import { RoundRobinMultiComponent } from './round-robin-multi/round-robin-multi.component';
import { ChatwindowComponent } from './chatwindow/chatwindow.component';
import { ChatComponent } from './chatwindow/chat/chat.component';
import { CarouselComponent } from './carousel/carousel.component';

import { MatTooltipModule } from '@angular/material/tooltip';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatIconModule } from '@angular/material/icon';

import { NgxFlagIconCssModule } from 'ngx-flag-icon-css';
import { TeamComponent } from './match/team/team.component';
import { I18nModule } from '../../../i18n/i18n.module';

import { TableHeaderCellComponent } from './battle-royale-multi/table-header-cell/table-header-cell.component';
import { TableSliderCellComponent } from './battle-royale-multi/table-slider-cell/table-slider-cell.component';
import { TableCounterCellComponent } from './battle-royale-multi/table-counter-cell/table-counter-cell.component';
import { ScoreConfirmPopupComponent } from './battle-royale-multi/score-confirm-popup/score-confirm-popup.component';
@NgModule({
  declarations: [
    SingleEliminationComponent,
    DoubleEliminationComponent,
    RoundRobinComponent,
    MatchComponent,
    ScoreCardComponent,
    EliminationComponent,
    AwardsComponent,
    BattleRoyaleMultiComponent,
    RoundRobinMultiComponent,
    ChatwindowComponent,
    ChatComponent,
    CarouselComponent,
    TeamComponent,
    TableHeaderCellComponent,
    TableSliderCellComponent,
    TableCounterCellComponent,
    ScoreConfirmPopupComponent,
  ],
  imports: [
    CommonModule,
    NgbModule,
    FontAwesomeModule,
    FormsModule,
    ProgressbarModule.forRoot(),
    LoadingModule,
    I18nModule,
    PipeModule,
    CarouselModule.forRoot(),
    TabsModule.forRoot(),
    MatProgressSpinnerModule,
    MatIconModule,
    MatTooltipModule,
    NgxFlagIconCssModule,
  ],
  exports: [EliminationComponent, ChatComponent, ChatwindowComponent],
})
export class EliminationModule {
  constructor(private library: FaIconLibrary) {
    library.addIcons(
      faTrophy,
      faPencilAlt,
      faSortUp,
      faSortDown,
      faCommentAlt,
      faCaretSquareUp,
      faCheckCircle
    );
  }
}
