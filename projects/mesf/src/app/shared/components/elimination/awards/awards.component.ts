import {
  Component,
  OnInit,
  Input,
  OnChanges,
  SimpleChanges,
} from '@angular/core';
import {
  BracketService,
  LanguageService,
  TournamentService,
  ToastService,
} from '../../../../core/service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-awards',
  templateUrl: './awards.component.html',
  styleUrls: ['./awards.component.scss'],
})
export class AwardsComponent implements OnInit, OnChanges {
  @Input() tournamentId;
  @Input() type: string;
  championList: Array<any> = [];
  constructor(
    private bracketService: BracketService,
    private tournamentService: TournamentService,
    private toastService: ToastService,
    private translateService: TranslateService,
    private languageService: LanguageService
  ) {}

  ngOnInit(): void {
    this.languageService.language.subscribe((lang) =>
      this.translateService.use(lang || 'en')
    );
  }

  /**
   * Fetch winners of the tournament
   */
  fetchChampions = async () => {
    try {
      const encodeUrl = this.getQueryBasedOnBracketType(
        this.type,
        this.tournamentId
      );
      if (this.type === 'single' || this.type === 'double') {
        const response = await this.bracketService.fetchAllMatches(encodeUrl);
        for (const m of response.data) {
          const isWinnerExist = this.championList.find(
            (item) => item._id === m.winnerTeam._id
          );
          if (!isWinnerExist) {
            this.championList.push(m.winnerTeam);
          }
          const isLoserExist = this.championList.find(
            (item) => m.loserTeam && item._id === m.loserTeam._id
          );
          if (!isLoserExist) {
            this.championList.push(m.loserTeam);
          }
        }
      } else if (this.type === 'round_robin') {
        const response = await this.tournamentService
          .getParticipants(encodeUrl)
          .toPromise();
        this.championList = response.data;
      } else if (this.type === 'battle_royale') {
        const query = `?tournamentId=${this.tournamentId}&limit=3`;
        const response = await this.bracketService.battleRoyalePlayerStanding(
          query
        );
        this.championList = response.data.map((el) => el.participant[0]);
      }
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  /**
   * Get query based on Bracket type to find winners
   * @param type single, double and round_robin
   * @param tournamentId tournamentId
   */
  getQueryBasedOnBracketType(type, tournamentId) {
    let query;
    switch (true) {
      case type === 'single':
        query = `?query=${encodeURIComponent(
          JSON.stringify({
            tournamentId,
          })
        )}&select=winnerTeam,loserTeam,isThirdPlaceMatch&option=${encodeURIComponent(
          JSON.stringify({ sort: { matchNo: -1 }, limit: 2 })
        )}`;
        break;
      case type === 'double':
        query = `?query=${encodeURIComponent(
          JSON.stringify({
            tournamentId,
          })
        )}&select=winnerTeam,loserTeam&option=${encodeURIComponent(
          JSON.stringify({ sort: { matchNo: -1 }, limit: 3 })
        )}`;
        break;
      case type === 'round_robin':
        query = `?query=${encodeURIComponent(
          JSON.stringify({
            tournamentId,
          })
        )}&select=_id,teamName,logo&option=${encodeURIComponent(
          JSON.stringify({ sort: { matchWin: -1 }, limit: 3 })
        )}`;
        break;
      default:
        break;
    }
    return query;
  }

  fetchWinnerList = async () => {
    try {
      const response = await this.bracketService.fetchWinnerList(
        this.tournamentId
      );
      this.championList = response.data;
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  ngOnChanges(changes: SimpleChanges): void {
    if (
      changes.hasOwnProperty('tournamentId') &&
      changes.tournamentId.currentValue &&
      changes.hasOwnProperty('type') &&
      changes.type.currentValue
    ) {
      this.fetchWinnerList();
    }
  }
}
