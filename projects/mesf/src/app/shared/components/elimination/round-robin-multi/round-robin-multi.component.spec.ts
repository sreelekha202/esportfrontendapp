import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoundRobinMultiComponent } from './round-robin-multi.component';

describe('RoundRobinMultiComponent', () => {
  let component: RoundRobinMultiComponent;
  let fixture: ComponentFixture<RoundRobinMultiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RoundRobinMultiComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoundRobinMultiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
