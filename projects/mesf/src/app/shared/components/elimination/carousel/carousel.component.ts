import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.scss'],
})
export class CarouselComponent implements OnInit {
  @Input() list: Array<number> = [];
  @Input() title: string;
  @Output() emitValue = new EventEmitter<boolean>();

  constructor() {}

  ngOnInit(): void {}

  slideChange(event) {
    this.emitValue.emit(event || 0);
  }
}
