import {
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';

@Component({
  selector: 'app-team',
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.scss'],
})
export class TeamComponent implements OnInit, OnChanges {
  @Input() disqualified: boolean;
  @Input() logo: string;
  @Input() country: string;
  @Input() teamName: string;
  @Input() placeholder: string;
  @Input() bye: boolean = false;
  @Input() setScore: number;
  @Input() countryList: Array<any> = [];
  @Input() isShowCountryFlag: boolean = false;

  countryCode = null;

  constructor() {}

  ngOnInit(): void {}

  ngOnChanges(simpleChanges: SimpleChanges) {
    if (this.country && this.countryList) {
      this.setCountryCode(this.country, this.countryList);
    }
  }

  setCountryCode(country, countryList) {
    const selectedCountry = countryList.find((c) => c?.name == country);
    this.countryCode = selectedCountry
      ? selectedCountry?.sortname.toLowerCase()
      : null;
  }
}
