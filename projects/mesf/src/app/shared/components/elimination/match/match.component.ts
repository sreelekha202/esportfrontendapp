import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { LanguageService } from '../../../../core/service';

@Component({
  selector: 'app-match',
  templateUrl: './match.component.html',
  styleUrls: ['./match.component.scss'],
})
export class MatchComponent implements OnInit {
  @Input() match: any;
  @Input() isSeeded: boolean;
  @Input() participantId;
  @Input() isAdmin: boolean;
  @Input() countryList: Array<any> = [];
  @Input() isShowCountryFlag: boolean = false;
  @Input() participantType: string;

  @Output() openScoreCard = new EventEmitter<any>();

  isActionEnable = false;

  constructor(
    private translateService: TranslateService,
    private languageService: LanguageService
  ) {}

  ngOnInit(): void {
    this.languageService.language.subscribe((lang) =>
      this.translateService.use(lang || 'en')
    );
  }

  /**
   * Open Score card
   */
  scoreCard() {
    this.openScoreCard.emit({ isOpenScoreCard: true, match: this.match });
  }

  /**
   * Allow Admin and related participant to view score screen
   * @param isActionEnable check
   */
  onHover(isActionEnable: boolean) {
    this.isActionEnable =
      this.isSeeded &&
      isActionEnable &&
      this.match.matchStatus !== 'inactive' &&
      !this.match.bye &&
      (this.isAdmin ||
        (this.participantId &&
          [this.match?.teamA?._id, this.match?.teamB?._id].includes(
            this.participantId
          )));
  }
}
