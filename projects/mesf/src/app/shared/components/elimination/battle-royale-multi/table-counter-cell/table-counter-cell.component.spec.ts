import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TableCounterCellComponent } from './table-counter-cell.component';

describe('TableCounterCellComponent', () => {
  let component: TableCounterCellComponent;
  let fixture: ComponentFixture<TableCounterCellComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TableCounterCellComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableCounterCellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
