import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-table-slider-cell',
  templateUrl: './table-slider-cell.component.html',
  styleUrls: ['./table-slider-cell.component.scss'],
})
export class TableSliderCellComponent implements OnInit {
  @Input() notPlaying: boolean = false;
  @Input() readOnly: boolean = false;

  @Output() emitValue = new EventEmitter<boolean>(false);

  constructor() {}

  ngOnInit(): void {}

  eventChange(event) {
    this.emitValue.emit(event?.target?.checked);
  }
}
