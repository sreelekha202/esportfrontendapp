import {
  Component,
  OnInit,
  Input,
  SimpleChanges,
  Output,
  EventEmitter,
  OnChanges,
} from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { LanguageService } from '../../../../core/service/language.service';

@Component({
  selector: 'app-round-robin',
  templateUrl: './round-robin.component.html',
  styleUrls: ['./round-robin.component.scss'],
})
export class RoundRobinComponent implements OnInit, OnChanges {
  @Input() structure: any;
  @Input() participants: Array<{}>;
  @Input() isSeeded: boolean;
  @Input() participantId: any;
  @Input() isAdmin: boolean;

  @Output() openScoreCard = new EventEmitter<any>();

  constructor(
    private translateService: TranslateService,
    private languageService: LanguageService
  ) {}

  ngOnInit(): void {
    this.languageService.language.subscribe((lang) =>
      this.translateService.use(lang || 'en')
    );
  }

  /**
   * Open Score card screen
   * @param match match
   */
  scoreCard(match) {
    this.openScoreCard.emit({ isOpenScoreCard: true, match });
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.hasOwnProperty('structure') && changes.structure.currentValue) {
      this.structure = this.structure.round || this.structure;
    }
  }
}
