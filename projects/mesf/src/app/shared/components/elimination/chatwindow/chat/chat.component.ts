import {
  Component,
  OnInit,
  Input,
  ViewEncapsulation,
  ViewChild,
  ElementRef,
  OnDestroy,
} from '@angular/core';

import {
  trigger,
  state,
  style,
  animate,
  transition,
} from '@angular/animations';

import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {
  ChatService,
  LanguageService,
  UserService,
} from '../../../../../core/service';
import { TranslateService } from '@ngx-translate/core';
import { IChat } from '../../../../models';
import { forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: [
    trigger('openClose', [
      state(
        'open',
        style({
          height: '525px',
          opacity: 1,
        })
      ),
      state(
        'closed',
        style({
          height: '0px',
          opacity: 0.5,
        })
      ),
      transition('open => closed', [animate('1s')]),
      transition('closed => open', [animate('0.5s')]),
    ]),
  ],
})
export class ChatComponent implements OnInit, OnDestroy {
  @Input() matchdetails: any;
  @Input() typeofchat: any;
  @Input() windowposition: any;
  @Input() showChatComponent: boolean = false;

  @ViewChild('messageinput') messageinput: ElementRef;
  @ViewChild('lastelement') lastelement: ElementRef;
  @ViewChild('messageinputwrapper') inputWrapper: ElementRef;
  @ViewChild('fileInput') fileInput: ElementRef;

  isImageInClipBoard: boolean = false;
  imageElementToRemove: any = '';
  userObject: any;
  chats: Array<IChat>;
  testnumber: any = 1;
  currentTournamentOwner: string;
  typingmessage: string = '';
  showProgress: boolean = false;
  progressValue: number = 0;
  filesToUpload = [];
  dummyFiles = [];
  s3files = [];
  tid: string = '';
  subtitle: string = '';
  chattitle: string = '';
  matchtitle: string = '';
  chatidtodelete = '';

  constructor(
    private chatService: ChatService,
    private userService: UserService,
    private modalService: NgbModal,
    private translateService: TranslateService,
    private languageService: LanguageService
  ) {}

  ngOnInit(): void {
    this.languageService.language.subscribe((lang) =>
      this.translateService.use(lang || 'en')
    );

    this.userService?.currentUser?.subscribe((data) => {
      if (data) {
        this.userObject = data;
      }
    });

    this.chatService?.getTypingInfo().subscribe((typingObject) => {
      this.typingmessage = `${
        typingObject.fullName
      } ${this.translateService.instant('ELIMINATION.CHAT.IS_TYPING')}`;
    });

    this.chatService?.getStopTypingInfo().subscribe((typingObject) => {
      this.typingmessage = '';
    });

    this.chatService?.getMessages().subscribe((message: IChat) => {
      this.chats.push(message);
      this.moveToLastElement();
    });

    this.chatService.chatWindowPos.subscribe((winpos) => {
      this.windowposition = winpos;
    });

    this.chatService.currentMatch.subscribe((currentMatch) => {
      this.matchdetails = currentMatch;
    });

    this.chatService.typeOfChatSub.subscribe((typeOfChatSub) => {
      this.typeofchat = typeOfChatSub;
    });

    this.chatService.chatStatus.subscribe((status) => {
      this.showChatComponent = status;
      if (this.showChatComponent == true) {
        this.updateChats();
        this.moveToLastElement();
      }
    });
  }

  ngOnChanges() {
    this.updateChats();
    this.moveToLastElement();
  }

  ngAfterViewChecked() {
    this.moveToLastElement();
  }

  ngOnDestroy() {
    if (this.matchdetails) {
      this.chatService?.disconnectMatch(this.matchdetails._id, this.typeofchat);
    }
  }

  closeChat() {
    this.chatService.setChatStatus(false);
  }

  showProgressBar(value, hideProgress = false) {
    if (hideProgress) {
      this.showProgress = false;
      this.progressValue = 0;
    } else {
      this.showProgress = true;
      this.progressValue = value;
    }
  }

  deleteChat(content, chatid) {
    this.chatidtodelete = chatid;
    this.modalService.open(content, {
      centered: true,
      windowClass: 'custom-modal-content',
    });
  }

  onConfirmDelete() {
    this.chatService?.deleteChat(this.chatidtodelete).subscribe((resp: any) => {
      if (resp.status == 'success') {
        const chatobj = document.querySelector(
          '.chatmessage-' + this.chatidtodelete
        );
        if (typeof chatobj != 'undefined' && chatobj != null) {
          chatobj.innerHTML = 'Message Deleted!';
          this.chatidtodelete = '';
          this.modalService.dismissAll();
        }
      }
    });
  }

  showDelete($event, chatid) {
    const deleteobj = $event.target.querySelector('.delete-' + chatid);
    if (typeof deleteobj != 'undefined' && deleteobj != null) {
      deleteobj.style.display = 'block';
    }
  }

  hideDelete($event, chatid) {
    const deleteobj = $event.target.querySelector('.delete-' + chatid);
    if (typeof deleteobj != 'undefined' && deleteobj != null) {
      deleteobj.style.display = 'none';
    }
  }

  updateChats() {
    if (this.matchdetails) {
      if (this.typeofchat == 'tournament') {
        if (
          typeof this.matchdetails.tournamentId === 'string' ||
          this.matchdetails.tournamentId instanceof String
        ) {
          this.tid = this.matchdetails.tournamentId;
        } else {
          this.tid = this.matchdetails.tournamentId._id;
        }
        this.chats = [];
        this.chatService
          ?.getOwnerForTournament(this.tid)
          .subscribe((ownerName: any) => {
            if (ownerName?.organiserId == this.matchdetails?.teamAId) {
              this.currentTournamentOwner = this.matchdetails?.teamAGameId;
            } else if (ownerName?.organiserId == this.matchdetails?.teamBId) {
              this.currentTournamentOwner = this.matchdetails?.teamBGameId;
            } else {
              this.currentTournamentOwner = ownerName?.organiserName;
            }

            this.chattitle = this.matchdetails.name;
            this.matchtitle =
              this.translateService.instant('CHAT.MATCH_NUMBER') +
              this.matchdetails.matchno;
            // this.subtitle =
            //   ownerName?.organiserName +
            //   ' - ' +
            //   this.translateService.instant('ELIMINATION.CHAT.ORGANIZER') +
            //   ', ' +
            //   this.matchdetails.teamA.inGamerUserId +
            //   ', ' +
            //   this.matchdetails.teamB.inGamerUserId;
            this.subtitle = this.matchdetails.participants;
          });
        this.chatService?.connectMatch(this.matchdetails._id, 'tournament');
        this.chatService?.getAllChatMessages(this.matchdetails._id).subscribe(
          (data: Array<IChat>) => {
            this.chats = data;
          },
          (err) => {}
        );
      } else if (this.typeofchat == 'game') {
        this.tid = this.matchdetails.matchid;
        this.chats = [];
        //this.currentTournamentOwner = this.matchdetails;

        let betweenusers = this.tid.split('-');
        this.chatService.searchUsers(betweenusers[0]).subscribe(
          (res: any) => {
            if (res.data.length > 0) {
              this.chattitle = this.translateService.instant('CHAT.GAME_CHAT');
              this.matchtitle = this.matchdetails.matchName;
              this.subtitle =
                this.translateService.instant('CHAT.RECEIVER') +
                res.data[0].fullName;
            }
          },
          (err) => {}
        );

        this.chatService?.connectMatch(this.tid, 'user');
        this.chatService?.getAllUserChatMessages(this.tid).subscribe(
          (data: Array<IChat>) => {
            this.chats = data;
          },
          (err) => {}
        );
      } else {
        this.tid = this.matchdetails;
        this.chats = [];
        this.currentTournamentOwner = this.matchdetails;

        let betweenusers = this.matchdetails.split('-');
        this.chatService.searchUsers(betweenusers[0]).subscribe(
          (res: any) => {
            if (res.data.length > 0) {
              this.chattitle = this.translateService.instant(
                'CHAT.SINGLE_CHAT'
              );
              this.matchtitle = '';
              this.subtitle =
                this.translateService.instant('CHAT.RECEIVER') +
                res.data[0].fullName;
            }
          },
          (err) => {}
        );

        this.chatService?.connectMatch(this.matchdetails, 'user');
        this.chatService?.getAllUserChatMessages(this.matchdetails).subscribe(
          (data: Array<IChat>) => {
            this.chats = data;
          },
          (err) => {}
        );
      }
    }
  }

  moveToLastElement() {
    if (this.lastelement?.nativeElement != null) {
      this.lastelement?.nativeElement.scrollIntoView({
        behavior: 'smooth',
        block: 'start',
      });
    }
  }

  onTyping(event) {
    this.typingmessage = '';
    let typingObject;
    if (this.typeofchat == 'tournament') {
      let gameusername = '';
      if (this.userObject['_id'] == this.matchdetails?.teamAId) {
        gameusername = this.matchdetails?.teamAGameId;
      } else if (this.userObject['_id'] == this.matchdetails?.teamBId) {
        gameusername = this.matchdetails?.teamBGameId;
      } else {
        gameusername = this.userObject['fullName'];
      }
      typingObject = {
        matchid: this.matchdetails._id,
        fullName: gameusername,
        userid: this.userObject['_id'],
      };
    } else {
      typingObject = {
        matchid: this.matchdetails,
        fullName: this.userObject['fullName'],
        userid: this.userObject['_id'],
      };
    }

    this.chatService?.sendTyping(typingObject, this.typeofchat);
  }

  hideTyping(event) {
    let typingObject;
    if (this.typeofchat == 'tournament') {
      let gameusername = '';
      if (this.userObject['_id'] == this.matchdetails?.teamAId) {
        gameusername = this.matchdetails?.teamAGameId;
      } else if (this.userObject['_id'] == this.matchdetails?.teamBId) {
        gameusername = this.matchdetails?.teamBGameId;
      } else {
        gameusername = this.userObject['fullName'];
      }
      typingObject = {
        matchid: this.matchdetails._id,
        fullName: gameusername,
        userid: this.userObject['_id'],
      };
    } else {
      typingObject = {
        matchid: this.matchdetails,
        fullName: this.userObject['fullName'],
        userid: this.userObject['_id'],
      };
    }
    this.chatService?.stopTyping(typingObject, this.typeofchat);
  }

  sendMsg(messagetyped, type) {
    let messageObject: IChat;
    let typingObject;

    if (this.typeofchat == 'tournament') {
      let gameusername = '';
      if (this.userObject['_id'] == this.matchdetails?.teamAId) {
        gameusername = this.matchdetails?.teamAGameId;
      } else if (this.userObject['_id'] == this.matchdetails?.teamBId) {
        gameusername = this.matchdetails?.teamBGameId;
      } else {
        gameusername = this.userObject['fullName'];
      }
      messageObject = {
        matchid: this.matchdetails?._id,
        tournamentid: this.matchdetails?.tournamentId,
        fullName: this.userObject['fullName'],
        userid: this.userObject['_id'],
        msg: messagetyped,
        messagetype: type,
        updatedAt: Date.now(),
        typeofchat: this.typeofchat,
        chatusername: gameusername,
      };
    } else if (this.typeofchat == 'game') {
      messageObject = {
        matchid: this.matchdetails.matchid,
        fullName: this.userObject['fullName'],
        userid: this.userObject['_id'],
        msg: messagetyped,
        messagetype: type,
        updatedAt: Date.now(),
        typeofchat: this.typeofchat,
        matchName: this.matchdetails.matchName,
        matchImage: this.matchdetails.image,
        matchLocation: this.matchdetails.location,
        chatusername: this.userObject['fullName'],
      };
    } else {
      messageObject = {
        matchid: this.matchdetails,
        fullName: this.userObject['fullName'],
        userid: this.userObject['_id'],
        msg: messagetyped,
        messagetype: type,
        updatedAt: Date.now(),
        typeofchat: this.typeofchat,
        chatusername: this.userObject['fullName'],
      };
    }
    if (type == 'textwithfiles') {
      messageObject.filesattached = this.s3files;
      this.s3files = [];
      this.filesToUpload = [];
      this.dummyFiles = [];
    }
    if (this.typeofchat == 'tournament') {
      this.chatService?.sendMessage(messageObject, this.typeofchat);
      typingObject = {
        matchid: this.matchdetails?._id,
        fullName: this.userObject['fullName'],
        userid: this.userObject['_id'],
      };
    } else if (this.typeofchat == 'game') {
      this.chatService?.sendMessage(messageObject, this.typeofchat);
      typingObject = {
        matchid: this.matchdetails.matchid,
        fullName: this.userObject['fullName'],
        userid: this.userObject['_id'],
      };
    } else {
      this.chatService?.sendMessage(messageObject, this.typeofchat);
      typingObject = {
        matchid: this.matchdetails,
        fullName: this.userObject['fullName'],
        userid: this.userObject['_id'],
      };
    }
    this.chatService?.stopTyping(typingObject, this.typeofchat);
    this.messageinput.nativeElement.value = '';
    this.typingmessage = '';
    this.moveToLastElement();
  }

  onClickedOutside(e: Event) {
    this.chatService.setChatStatus(false);
  }

  uploadFiles(messagetyped) {
    this.fileInput.nativeElement.value = '';
    let observables = [];
    for (let i = 0; i < this.filesToUpload.length; i++) {
      const formData = new FormData();
      formData.append('filetoupload', this.filesToUpload[i].data);
      this.filesToUpload[i].inProgress = true;
      observables.push(this.chatService.upload(formData));
    }
    if (observables.length > 0) {
      forkJoin(observables).subscribe((results: any[]) => {
        if (results.length > 0) {
          for (let k = 0; k < results.length; k++) {
            if (results[k]?.status == 'success') {
              const s3url = results[k]?.filapath;
              const fname = s3url.split('/').pop();
              this.dummyFiles.splice(this.dummyFiles.indexOf(fname), 1);
              this.s3files.push(s3url);
            } else {
              this.s3files.push('error : ' + results[k]?.message);
            }
          }
        }
        this.sendMsg(messagetyped, 'textwithfiles');
      });
    }
  }

  onFileAttach(event) {
    event.preventDefault();
    let inputElement: HTMLElement = this.fileInput.nativeElement as HTMLElement;
    inputElement.click();
  }

  handleFileInput(files: FileList) {
    for (let index = 0; index < files.length; index++) {
      const file = files[index];
      this.filesToUpload.push({ data: file, inProgress: false, progress: 0 });
      this.dummyFiles.push(file.name);
    }
  }

  async onEnterKey(event) {
    event.preventDefault();
    let messagetyped = this.messageinput?.nativeElement.value;

    if (this.filesToUpload.length > 0) {
      this.uploadFiles(messagetyped);
    } else {
      if (messagetyped != '') {
        this.sendMsg(messagetyped, 'text');
      }
    }
  }
}
