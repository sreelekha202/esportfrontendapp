import {
  Component,
  OnInit,
  Input,
  Injectable,
  Output,
  EventEmitter,
} from '@angular/core';

import { ChatService } from './../../../../core/service/chat.service';

@Component({
  selector: 'app-chatwindow',
  templateUrl: './chatwindow.component.html',
  styleUrls: ['./chatwindow.component.scss'],
})
@Injectable({
  providedIn: 'root',
})
export class ChatwindowComponent implements OnInit {
  @Input() matchdetails: any;

  showChat: boolean = false;
  windowposition: String = 'chat_window chat_window_right';

  constructor(private chatService: ChatService) {}

  ngOnInit(): void {}

  toggleChat() {
    if (this.matchdetails) {
      this.chatService
        .getMatchInformation(this.matchdetails._id)
        .subscribe((result: any) => {
          const mtchdetails = {
            _id: result.matchdetails._id,
            picture: result.matchdetails.tournamentId.gameDetail.logo,
            name: result.matchdetails.tournamentId.name,
            matchno: result.matchdetails.matchNo,
            participants:
              result.matchdetails.teamA.inGamerUserId +
              ',' +
              result.matchdetails.teamB.inGamerUserId,
            tournamentId: result.matchdetails.tournamentId._id,
            teamAId: result.matchdetails.teamA.userId,
            teamBId: result.matchdetails.teamB.userId,
            teamAGameId: result.matchdetails.teamA.inGamerUserId,
            teamBGameId: result.matchdetails.teamB.inGamerUserId,
            type: 'tournament',
          };
          this.chatService.setWindowPos(this.windowposition);
          this.chatService.setCurrentMatch(mtchdetails);
          this.chatService.setTypeOfChat('tournament');
          this.chatService.setChatStatus(true);
          // let firstChat = this.chatService.getChatStatus();
          // if (firstChat == true) {
          //   this.chatService.setChatStatus(false);
          //   this.chatService?.disconnectMatch(mtchdetails._id, 'tournament');
          // } else {
          //   this.chatService.setCurrentMatch(mtchdetails);
          //   this.chatService.setTypeOfChat('tournament');
          //   this.chatService.setChatStatus(true);
          // }
        });
    }
  }
}
