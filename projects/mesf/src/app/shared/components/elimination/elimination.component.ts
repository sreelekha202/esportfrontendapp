import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  OnChanges,
  SimpleChanges,
} from '@angular/core';
import { IMatch } from '../../../shared/models';
import { TranslateService } from '@ngx-translate/core';
import {
  LanguageService,
  UserService,
  ToastService,
} from '../../../core/service';
@Component({
  selector: 'app-elimination',
  templateUrl: './elimination.component.html',
  styleUrls: ['./elimination.component.scss'],
})
export class EliminationComponent implements OnInit, OnChanges {
  @Input() structure: any;
  @Input() type: string;
  @Input() isSeeded: boolean;
  @Input() participants: Array<{}>;
  @Input() isFinished: boolean;
  @Input() tournamentId: string;
  @Input() isAdmin: boolean;
  @Input() participantId: string;
  @Input() isLoaded: boolean;
  @Input() stageType: string;
  @Input() stageBracketType: string;
  @Input() isScreenShotRequired: boolean = false;
  @Input() isShowCountryFlag: boolean = false;
  @Input() scoreReporting: number = 1;
  @Input() participantType: string;

  @Input() enableWebview: boolean;
  @Input() token: string;
  @Output() isRefresh = new EventEmitter<boolean>(false);

  isUpdateMatchScore = false;
  match: IMatch;
  countryList = [];

  constructor(
    private toastService: ToastService,
    private translateService: TranslateService,
    private languageService: LanguageService,
    private userService: UserService
  ) {}

  ngOnInit(): void {
    this.languageService.language.subscribe((lang) =>
      this.translateService.use(lang || 'en')
    );
    this.getAllCountries();
  }

  updatedScoreCard(data) {
    this.isUpdateMatchScore = data.isOpenScoreCard;
    this.match = data.match;
  }

  exitFromScoreCard(data) {
    if (typeof data === 'object') {
      this.isUpdateMatchScore = data.isOpenScoreCard;
      if (data.refresh) {
        this.isRefresh.emit(data.refresh);
      }
    } else {
      this.isRefresh.emit(data);
    }
  }

  getAllCountries = async () => {
    try {
      const data = await this.userService.getAllCountries().toPromise();
      this.countryList = data?.countries || [];
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  ngOnChanges(simpleChanges: SimpleChanges) {
  }
}
