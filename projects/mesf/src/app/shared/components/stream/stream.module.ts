import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StreamComponent } from './stream.component';
import {
  FontAwesomeModule,
  FaIconLibrary,
} from '@fortawesome/angular-fontawesome';
import {
  faTrash,
  faEdit,
  faCopy,
  faPlay,
  faStop,
  faExpand,
} from '@fortawesome/free-solid-svg-icons';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PipeModule } from '../../../shared/pipe/pipe.module';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { I18nModule } from '../../../i18n/i18n.module';
@NgModule({
  declarations: [StreamComponent],
  imports: [
    CommonModule,
    FontAwesomeModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    PipeModule,
    I18nModule,
    MatProgressSpinnerModule,
  ],
  exports: [StreamComponent],
})
export class StreamModule {
  constructor(private library: FaIconLibrary) {
    library.addIcons(faTrash, faEdit, faCopy, faPlay, faStop, faExpand);
  }
}
