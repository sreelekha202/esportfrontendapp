import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable()
export class S3UploadService {
  constructor(private http: HttpClient) {}

  fileUpload(formData) {
    return this.http.post(`${environment.apiEndPoint}file-upload`, formData);
  }
}
