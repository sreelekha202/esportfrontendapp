import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { GlobalUtils } from '../global-utils/global-utils';

@Injectable({
  providedIn: 'root',
})
export class ScreenService {
  isLgScreen = new BehaviorSubject(ScreenService.isLgScreen());

  static isLgScreen(): boolean {
    const md = 1199;
    return Boolean(window.innerWidth < md);
  }

  constructor() {}

  onAppResizeListener(): void {
    if (GlobalUtils.isBrowser()) {
      window.addEventListener('resize', () => {
        this.isLgScreen.next(ScreenService.isLgScreen());
      });
    }
  }
}
