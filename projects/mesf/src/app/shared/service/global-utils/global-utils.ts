import { Meta, MetaDefinition, Title } from '@angular/platform-browser';
import { Injectable } from '@angular/core';

declare const window: any;

@Injectable({
  providedIn: 'root',
})
export class GlobalUtils {
  static isBrowser() {
    return Boolean(typeof window !== 'undefined');
  }

  constructor(private meta: Meta, private title: Title) {}

  setMetaTags(tags: MetaDefinition[]): void {
    if (tags) {
      tags.forEach((tag) => {
        this.meta.updateTag(tag);
      });
    }
  }
}
