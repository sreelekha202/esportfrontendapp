import { APP_INITIALIZER, NgModule } from '@angular/core';

import { SharedModule } from './shared.module';
import { CookieService } from 'ngx-cookie-service';
import { DatePipe } from '@angular/common';
import {
  S3UploadService,
  ToastService,
  PaginationService,
} from '../../core/service';
import { appInitializer } from '../../core/helpers/app.initializer';
import { UserService } from '../../core/service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { JwtInterceptor } from '../../core/helpers/interceptors/token-interceptors.service';
import {
  SocialAuthServiceConfig,
  SocialLoginModule,
  GoogleLoginProvider,
  FacebookLoginProvider,
} from 'angularx-social-login';
import { provideConfig } from '../../core/service/social-media-auth.service';
import {
  BrowserModule,
  BrowserTransferStateModule,
} from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from '../../app-routing.module';
import { CoreModule } from '../../core/core.module';
import { I18nModule } from '../../i18n/i18n.module';
import { NgxIntlTelInputModule } from 'ngx-intl-tel-input';
import { SocketIoConfig, SocketIoModule } from 'ngx-socket-io';
import { AngularFireModule } from '@angular/fire';
import { environment } from '../../../environments/environment';
import { AngularFireMessagingModule } from '@angular/fire/messaging';
import { PipeModule } from '../pipe/pipe.module';
import { SpecialOfferModule } from '../../modules/special-offer/special-offer.module';
import { InlineSVGModule } from 'ng-inline-svg';
import { ChartsModule } from 'ng2-charts';
import { TranslateTestingModule } from 'ngx-translate-testing';

const config: SocketIoConfig = { url: environment.socketEndPoint, options: {} };

const components = [];

const sheets = [];

const popups = [];

const directives = [];

const modules = [
  BrowserModule.withServerTransition({ appId: 'serverApp' }),
  BrowserAnimationsModule,
  BrowserTransferStateModule,
  AppRoutingModule,
  CoreModule,
  SharedModule,
  I18nModule,
  SocialLoginModule,
  NgxIntlTelInputModule,
  SocketIoModule.forRoot(config),
  AngularFireModule.initializeApp(environment.firebase),
  AngularFireMessagingModule,
  PipeModule,
  SpecialOfferModule,
  InlineSVGModule.forRoot(),
  ChartsModule,
  TranslateTestingModule.withTranslations(
    'en',
    require('../../../assets/i18n/en.json')
  ),
];

const providers = [
  CookieService,
  DatePipe,
  ToastService,
  S3UploadService,
  PaginationService,
  {
    provide: APP_INITIALIZER,
    useFactory: appInitializer,
    multi: true,
    deps: [UserService],
  },
  {
    provide: HTTP_INTERCEPTORS,
    useClass: JwtInterceptor,
    multi: true,
  },
  {
    provide: 'SocialAuthServiceConfig',
    useValue: {
      autoLogin: false,
      providers: [
        {
          id: GoogleLoginProvider.PROVIDER_ID,
          provider: new GoogleLoginProvider(environment.googleAPPID),
        },
        {
          id: FacebookLoginProvider.PROVIDER_ID,
          provider: new FacebookLoginProvider(environment.facebookAPPID),
        },
      ],
    } as SocialAuthServiceConfig,
  },
];

@NgModule({
  imports: [...modules],
  declarations: [...components, ...directives, ...sheets, ...popups],
  exports: [...modules, ...components, ...directives, ...sheets, ...popups],
  providers: [...providers],
})
export class SharedTestingModule {}
