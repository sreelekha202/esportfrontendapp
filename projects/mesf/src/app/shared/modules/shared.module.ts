import {
  SWIPER_CONFIG,
  SwiperConfigInterface,
  SwiperModule,
} from 'ngx-swiper-wrapper';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ShareButtonsModule } from 'ngx-sharebuttons/buttons';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { ShareButtonModule } from 'ngx-sharebuttons/button';
import { NgxIntlTelInputModule } from 'ngx-intl-tel-input';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ShareIconsModule } from 'ngx-sharebuttons/icons';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { InlineSVGModule } from 'ng-inline-svg';
import { NgOtpInputModule } from 'ng-otp-input';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { MomentModule } from 'ngx-moment';
import { NgModule } from '@angular/core';

import { FullscreenLoadingComponent } from '../../core/fullscreen-loading/fullscreen-loading.component';
import { CustomPaginationModule } from '../../core/custom-pagination/custom-pagination.module';
import { TournamentItemComponent } from '../../core/tournament-item/tournament-item.component';
import { DiscussionModule } from '../../shared/components/discussion/discussion.module';
import { MatchListModule } from '../../shared/components/match-list/match-list.module';
import { SocialShareComponent } from '../popups/social-share/social-share.component';
import { EliminationModule } from '../components/elimination/elimination.module';
import { InfoPopupComponent } from '../popups/info-popup/info-popup.component';
import { RatingModule } from '../../shared/components/rating/rating.module';
import { StreamModule } from '../../shared/components/stream/stream.module';
import { SidenavService } from '../service/sidenav/sidenav.service';
import { LoadingModule } from '../../core/loading/loading.module';
import { PipeModule } from '../../shared/pipe/pipe.module';
import { MaterialModule } from './material.module';
import { I18nModule } from '../../i18n/i18n.module';
import { DirectivesModule } from '../../shared/directives/directive.module';
import { TranslateService } from '@ngx-translate/core';
import { LanguageService } from '../../core/service';
import { PopupPolicyComponent } from '../../modules/log-reg/popup-policy/popup-policy.component';

const DEFAULT_SWIPER_CONFIG: SwiperConfigInterface = {
  direction: 'horizontal',
  slidesPerView: 'auto',
};

const components = [
  TournamentItemComponent,
  FullscreenLoadingComponent,
  PopupPolicyComponent,
];

const sheets = [];

const popups = [InfoPopupComponent, SocialShareComponent];

const directives = [];

const modules = [
  CommonModule,
  RouterModule,
  FormsModule,
  ReactiveFormsModule,
  MaterialModule,
  EliminationModule,
  NgbModule,
  FontAwesomeModule,
  NgOtpInputModule,
  NgxIntlTelInputModule,
  NgxDatatableModule,
  BsDropdownModule,
  SwiperModule,
  InlineSVGModule.forRoot(),
  ShareButtonModule,
  ShareButtonsModule,
  ShareIconsModule,
  CollapseModule.forRoot(),
  MomentModule,
  RatingModule,
  LoadingModule,
  PipeModule,
  DirectivesModule,
  MatchListModule,
  StreamModule,
  DiscussionModule,
  CustomPaginationModule,
  I18nModule,
];

const providers = [
  SidenavService,
  {
    provide: SWIPER_CONFIG,
    useValue: DEFAULT_SWIPER_CONFIG,
  },
];

@NgModule({
  imports: [...modules],
  declarations: [...components, ...directives, ...sheets, ...popups],
  exports: [...modules, ...components, ...directives, ...sheets, ...popups],
  providers: [...providers],
})
export class SharedModule {
  constructor(
    private languageService: LanguageService,
    private translateService: TranslateService
  ) {
    this.languageService.language.subscribe((lang) => {
      this.translateService.use(lang);
    });
  }
}
