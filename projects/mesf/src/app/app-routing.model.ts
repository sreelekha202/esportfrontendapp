import { MetaDefinition } from '@angular/platform-browser';

export interface AppRoutesData {
  isRootPage?: boolean;
  title?: string;
  tags?: MetaDefinition[];
}

// Use this for routerLing and ts version of it like .navigate()
export enum AppHtmlRoutes {
  home = '/home',
  topUp = '/top-up',
  privacyPolicy = '/privacy-policy',
  termsandconditions = '/terms-and-conditions',
  register = '/register',
  registerEmail = '/register-email',
  login = '/login',
  userPageType = '/user',
  loginEmail = '/login-email',
  leaderBoard = '/leaderboard',
  aboutUs = '/about-us',
  search = '/search',
  searchTournament = '/search/tournament',
  searchArticle = '/search/article',
  searchVideo = '/search/video',
  searchShop = '/search/shop',
  specialOffer = '/special-offer',
  shop = '/shop',
  shopVoucher = '/shop/voucher',
  shopOrder = '/shop/order',
  videos = '/videos',
  tournament = '/tournament',
  tournamentCreate = '/tournament/create',
  tournamentEdit = '/tournament/edit',
  tournamentInfo = '/tournament/info',
  tournamentView = '/tournament',
  bracket = '/bracket',
  bracketCreate = '/bracket/create',
  article = '/article',
  admin = '/admin',
  dec = '/dec',
  notFound = '/404',
  games = '/games',
}
export enum AppHtmlRoutesLoginType {
  phoneLogin = 'phone-login',
  emailLogin = 'email-login',
  registration = 'registration',
  forgotPass = 'forgotPass',
  createPass = 'createPass',
}

export enum AppHtmlProfileRoutes {
  basicInfo = '/profile/basic-info',
  myContent = '/profile/my-content',
  inbox = '/profile/inbox',
  myBookmarks = '/profile/my-bookmarks',
  myBracket = '/profile/my-bracket',
  myTournamentCreated = '/profile/my-tournament/created',
  myTournamentJoined = '/profile/my-tournament/joined',
  myTransactions = '/profile/my-transactions',
  setting = '/profile/profile-setting',
}

export enum AppHtmlAdminRoutes {
  dashboard = '/admin/dashboard',
  esportsManagement = '/admin/esports-management',
  contentManagement = '/admin/content-management',
  userManagement = '/admin/user-management',
  userManagementView = '/admin/user-management/view',
}
