import 'zone.js/dist/zone-node';

import { ngExpressEngine } from '@nguniversal/express-engine';
import express from 'express';
import { join } from 'path';

import { AppServerModule } from './src/main.server';
import { APP_BASE_HREF } from '@angular/common';
import { existsSync } from 'fs';
const domino = require('domino');
const fs_1 = require('fs');
const compression = require('compression');
const helmet = require('helmet');
import fileUpload from 'express-fileupload';

(global as any).WebSocket = require('ws');
(global as any).XMLHttpRequest = require('xhr2');

import { environment } from './src/environments/environment';
import * as axios from 'axios';

function createTransformOptions() {
  const value = () => ({
    enumerable: true,
    configurable: true,
  });
  return { value };
}
// function getMockMutationObserver() {
//   return {};
// }

function applyDomino(global, templatePath) {
  const template = fs_1.readFileSync(templatePath).toString();
  const win = domino.createWindow(template);
  global['window'] = win;
  global['document'] = win.document;
  global['localStorage'] = win.localStorage || {
    getItem: function () {},
    setItem: function () {},
  };
  global['navigator'] = win.navigator;
  Object.defineProperty(
    win.document.body.style,
    'transform',
    createTransformOptions()
  );
  global['CSS'] = null;
  global['Prism'] = null;
  // global['MutationObserver'] = getMockMutationObserver();
}

const BROWSER_DIR = join(process.cwd(), 'dist/stc/browser');

applyDomino(global, join(BROWSER_DIR, 'index.html'));
// The Express app is exported so that it can be used by serverless Functions.
export function app(): express.Express {
  const server = express();
  const distFolder = join(process.cwd(), 'dist/stc/browser');
  const indexHtml = existsSync(join(distFolder, 'index.original.html'))
    ? 'index.original.html'
    : 'index';

  server.use(compression());
  // Our Universal express-engine (found @ https://github.com/angular/universal/tree/master/modules/express-engine)
  server.engine(
    'html',
    ngExpressEngine({
      bootstrap: AppServerModule,
    })
  );

  server.set('view engine', 'html');
  server.set('views', distFolder);

  server.use(
    helmet.contentSecurityPolicy({
      directives: {
        upgradeInsecureRequests: [],
        blockAllMixedContent: [],
        defaultSrc: ["'none'"],
        'connect-src': [
          "'self'",
          environment.apiEndPoint,
          'https://stcplay.gg/',
          'wss://stc-chat.dynasty-dev.com',
          'wss://stagechat.stcplay.gg',
          'wss://chat.stcplay.gg',
          'wss://widget-mediator.zopim.com',
          'https:',
        ],
        scriptSrc: [
          "'self'",
          // "'unsafe-inline'",
          'googletagmanager.com',
          'https://stcplay.gg/',
          'http://stc-chat.dynasty-dev.com',
          'wss://stc-chat.dynasty-dev.com',
          'wss://stagechat.stcplay.gg',
          'wss://chat.stcplay.gg',
          'firebaseio.com',
          'gstatic.com',
          'codapayments.com',
          'dynasty-dev.com',
          'stcplay.gg',
          'dynasty-staging.com',
          'static.zdassets.com',
          'https://www.google.com',
          'https://www.gstatic.com/',
          'https://www.paypal.com',
          'https://stcpay.com',
          'https://payfort.com',
          'https://*.stcpay.com',
          'https://*.paypal.com',
          'https://*.payfort.com',
          'https://www.google.com',
          'https://*.google.com',
          'https://*.facebook.net',
          'https://www.paypalobjects.com',
          'https://*.paypalobjects.com',
          'https://player.twitch.tv/',
          'https://login.dotomi.com/',
          'https://cdn.adjust.com/adjust-latest.min.js',
        ],
        scriptSrcElem: [
          "'self'",
          // "'unsafe-inline'",
          'https://stcplay.gg/',
          'https://www.googletagmanager.com',
          'http://www.googletagmanager.com',
          'https://static.zdassets.com',
          'https://www.google.com',
          'https://www.gstatic.com/',
          'https://www.paypal.com',
          'https://stcpay.com',
          'https://payfort.com',
          'https://*.stcpay.com',
          'https://*.paypal.com',
          'https://*.payfort.com',
          'https://www.google.com',
          'https://*.google.com',
          'https://*.facebook.net',
          'https://www.paypalobjects.com',
          'https://*.paypalobjects.com',
          'https://player.twitch.tv/',
          'https://login.dotomi.com/',
          'https://cdn.adjust.com/adjust-latest.min.js',
          'https://sc-static.net/scevent.min.js',
          'https://static.ads-twitter.com/uwt.js',
          'http://static.ads-twitter.com/uwt.js',
          'https://analytics.twitter.com/',
          'https://tr.snapchat.com/',
          'https://sc-static.net/js-sha256-v1.min.js',
          'https://googleads.g.doubleclick.net/',
          'http://googleads.g.doubleclick.net/',
          'https://www.googleadservices.com/',
          'http://www.googleadservices.com/',
          'https://bid.g.doubleclick.net/',
          'https://analytics.tiktok.com',
          'https://c.amazon-adsystem.com',
          'https://www.google-analytics.com',
          'https://p.teads.tv/',
          'https://js.adsrvr.org/',
          'https://p.teads.tv/teads-fellow.js',
          'https://js.adsrvr.org/up_loader.1.1.0.js',
          "'sha256-3TTmaQ8NxQoTiG0L35r8YmEKPyDAsxP5qh9bm4dBMvg='",
          "'sha256-rB2R3bg4VzWIJsprRl6Q80FXcywGNjDwUUs6enCX/9M='",
          "'sha256-mUgs6IcV6lu+p2tJ0VivS6kiyK7XS4pmvjXTM53AIWQ='",
          "'sha256-kJDEdzV+Y1jEWhlZolwKRKf5FcoVcCEaAIFy6Z8M+88='",
          "'sha256-Tlh4hTQv4uVBjAlItgBcMGeQUg+rhwogjxRefKzWLPk='",
          "'sha256-lj6z70BIHsHjd1ztZiaCDm6ydvPlsxfRszCRhapBj+U='",
          "'sha256-tTO0cyvJiw9c9aj7jifJUH3OlnQiE2cXvjpRIf9thp0='",
          "'sha256-vASHeoOmTnMxrcVwQhzQJomtBhB91tXxnOOMCSyr7BE='",
          "'sha256-XbvCfUswKzQYEsLIRVdOxxHPM7nCFVc18PFyc5ObgF8='",
          "'sha256-n4G0kB/bRuHJrI5NqBNY3qo9W7Qo/YRtlbtNS8mE7m4='",
          "'sha256-d8xVpEfOlXT388lPL445U0wcaE4cweRSVh5BQpm9scE='",
          "'sha256-7nnKyr+RUZ9a44Hg3lYwjgkUx5VyFQwv2ZUhVw6N7J4='",
          "'sha256-3dO3w7Y+Pvr8nNuM9JioBpuBGrxgMBNZhPSqYDD4qn4='",
          "'sha256-oByu3isyoualNW0NX6KCfG52kLL6eFagsUSOlk5jiUM='",
          "'sha256-SpoUIYB2lWP4p3eLCLc2S5ZZu8d+qvoX/qc11Xrpejk='",
          "'sha256-G2pHW39n1Tc1kNnaJG0FmhQb+4gJtoUjVnENpQmAqUg='",
          "'sha256-gPYT+eUJR7YwfR661dQ8sCWjfui9YNztkQt+clIDbf8='",
          "'sha256-ZdPRSViQTdqK9WFixtp5xmv9eepN1oxMeBrYBGlMq70='",
          "'sha256-r77nfYsRJ8fz0J6byuS8W5dxtZAkDIMbpS2pWGNaPIY='",
          "'sha256-drv7FHJh/sfV4aFDDIayq3+XwxiKshcd8Ipc75xFiE4='",
          "'sha256-av1KhcA3C9qy5r22rr3c3A6NJ+sJpehbO0ctu0eVWH4='",
          "'sha256-doB1Msxp1aPmV60JuvyRIHxMQHVPnM+NCZrSU3OtRKw='",
          "'sha256-o/cCfMBH7mFClYy9Zt0jCUqA+fCmIFbbAI7gLUa0pCU='",
          "'sha256-XUPGsjf3fIYckIuYEZnEp/zT6iRfAGmgpbvXdA6d0x0='",
          "'sha256-v6gwmtuHJReQ2WwSxIk4ogs5+69K0F8qMqra2N00xug='",
          "'sha256-AMaTXncOv/eAyukXZPocKW55zzxH+VYX7De+D9H5tRw='",
          "'sha256-zgEmLShLTo5XxZXtOwZdd9PuHqixsrmzq/3B4tHOQ4A='",
        ],
        objectSrc: ["'none'"],
        styleSrc: ["'self'", 'https:', "'unsafe-inline'"],
        mediaSrc: ["'self'", 'static.zdassets.com'],
        fontSrc: [
          "'self'",
          'fonts.googleapis.com',
          'fonts.gstatic.com',
          'data:',
          'https://stcplay.gg/',
          'https://sbcheckout.payfort.com',
          'https://www.paypal.com',
          'https://stcpay.com',
          'https://payfort.com',
          'https://*.stcpay.com',
          'https://*.paypal.com',
          'https://*.payfort.com',
          'https://www.google.com',
          'https://*.google.com',
          'https://*.facebook.net',
          'https://www.paypalobjects.com',
          'https://*.paypalobjects.com',
          'https://cdnjs.cloudflare.com',
          'https://netdna.bootstrapcdn.com',
        ],
        'frame-src': [
          "'self'",
          // "'unsafe-inline'",
          'https://stcplay.gg/',
          'https://www.youtube.com',
          'https://www.sandbox.paypal.com',
          'https://www.paypal.com',
          'https://www.google.com',
          'https://www.paypal.com',
          'https://stcpay.com',
          'https://payfort.com',
          'https://*.stcpay.com',
          'https://*.paypal.com',
          'https://*.payfort.com',
          'https://www.google.com',
          'https://*.google.com',
          'https://*.facebook.net',
          'https://www.paypalobjects.com',
          'https://*.paypalobjects.com',
          'https://player.twitch.tv/',
          'https://tr.snapchat.com/',
          'http://11015140.fls.doubleclick.net',
          'http://*.amazon-adsystem.com/',
          'https://bid.g.doubleclick.net',
          'https://9689267.fls.doubleclick.net/',
          'http://9689267.fls.doubleclick.net/',
          'https://insight.adsrvr.org/',
          'http://11069257.fls.doubleclick.net/',
          'https://www.facebook.com/'
        ],
        'img-src': [
          "'self'",
          // "'unsafe-inline'",
          'https://stcplay.gg/',
          'https://stc-img.s3.me-south-1.amazonaws.com',
          'https://*.youtube.com',
          'https://www.google.co.in',
          'https://*',
          'data:',
          'blob:',
          'https://stcplay.gg/',
          'https://sbcheckout.payfort.com',
          'https://www.paypal.com',
          'https://stcpay.com',
          'https://payfort.com',
          'https://*.stcpay.com',
          'https://*.paypal.com',
          'https://*.payfort.com',
          'https://www.google.com',
          'https://*.google.com',
          'https://*.facebook.net',
          'https://www.paypalobjects.com',
          'https://*.paypalobjects.com',
          'http://t.co/i/',
          'https://*.fls.doubleclick.net',
          'http://ad.doubleclick.net/',
        ],
        'frame-ancestors': ["'none'"],
        'base-uri': ["'self'"],
        'form-action': [
          "'self'",
          // "'unsafe-inline'",
          environment.apiEndPoint,
          'https://stcplay.gg/',
          'https://sbcheckout.payfort.com',
          'https://www.paypal.com',
          'https://stcpay.com',
          'https://payfort.com',
          'https://*.stcpay.com',
          'https://*.paypal.com',
          'https://*.payfort.com',
          'https://www.google.com',
          'https://*.google.com',
          'https://*.facebook.net',
          'https://www.paypalobjects.com',
          'https://*.paypalobjects.com',
          'https://tr.snapchat.com/',
          'https://www.facebook.com/',
        ],
        'manifest-src': ["'self'"],
      },
      reportOnly: false,
    })
  );
  server.use(helmet.dnsPrefetchControl());
  server.use(helmet.expectCt());
  server.use(helmet.frameguard());
  server.use(helmet.hidePoweredBy());
  server.use(helmet.hsts());
  server.use(helmet.ieNoOpen());
  server.use(helmet.noSniff());
  server.use(helmet.permittedCrossDomainPolicies());
  server.use(helmet.referrerPolicy());
  server.use((req, res, next) => {
    res.setHeader('X-XSS-Protection', '1; mode=block');
    next();
  });
  // server.use(helmet.xssFilter());

  // Example Express Rest API endpoints
  // server.get('/api/**', (req, res) => { });
  // Serve static files from /browser
  server.get(
    '*.*',
    express.static(distFolder, {
      maxAge: '1y',
    })
  );

  // All regular routes use the Universal engine
  server.use(fileUpload());

  server.post('/v1/upload', async (req, res) => {
    try {
      const file: any = req?.files?.file;
      const base64 = Buffer.from(file?.data).toString('base64');
      const base64String = `data:${file.mimetype};base64,${base64}`;
      const payload = {
        path: environment.articleS3BucketName,
        file: base64String,
      };
      const config = {
        headers: { Authorization: req.headers.authorization },
      };
      const request = axios.default;
      const response = await request.post(
        `${environment.apiEndPoint}file-upload`,
        payload,
        config
      );
      res.send({ imageUrl: response?.data?.data?.Location || base64String });
    } catch (error) {
      res.status(400).send({ message: error?.message });
    }
  });

  // All regular routes use the Universal engine
  server.get('*', (req, res) => {
    res.render(indexHtml, {
      req,
      providers: [{ provide: APP_BASE_HREF, useValue: req.baseUrl }],
    });
  });

  return server;
}

function run(): void {
  const port = process.env.PORT || 8080;

  // Start up the Node server
  const server = app();
  server.listen(port, () => {
    console.log(`Node Express server listening on http://localhost:${port}`);
  });
}

// Webpack will replace 'require' with '__webpack_require__'
// '__non_webpack_require__' is a proxy to Node 'require'
// The below code is to ensure that the server is run only when not requiring the bundle.
declare const __non_webpack_require__: NodeRequire;
const mainModule = __non_webpack_require__.main;
const moduleFilename = (mainModule && mainModule.filename) || '';
if (moduleFilename === __filename || moduleFilename.includes('iisnode')) {
  run();
}

export * from './src/main.server';
