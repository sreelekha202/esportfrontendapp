// handle the current active week
const activeWeek = 4;
document.querySelectorAll('.progress-circle').forEach((step, index) => {
  if (index < activeWeek) {
    step.classList.add('active');
  }
})

function getRecaptchaToken(){
  return grecaptcha.execute('6Lcs0-cUAAAAAIYIBZ8LbZg_uGGkYXnTZ1J3m5Kf', {
    action: 'homepage',
  })
  
}

// on form submit
document.querySelector('form').addEventListener('submit', function (e) {
  e.preventDefault();
  const formData = new FormData(e.target);
  showLoading(true);
  
  getRecaptchaToken()
    .then((token) => {
      // send request to API

      // showConfirmMessage();
      // renderErrMsg('error message');
      // inValidField('fullname');
    })
})

// modal for T&C
document.querySelectorAll('.open-modal').forEach(btn => {
  btn.addEventListener('click', e => {
    e.preventDefault();
    const modalContentID = e.target.dataset.modal;
    const modalContent = document.querySelector(`#${modalContentID}`);
    
    if (modalContent) modalContent.classList.add('show')
  })
})

function closeModal(){
  document.querySelectorAll('.modal').forEach(m => {
    m.classList.remove('show')
  })
}

document.querySelectorAll('.modal .close-modal').forEach(btn => {
  btn.addEventListener('click', closeModal)
})
