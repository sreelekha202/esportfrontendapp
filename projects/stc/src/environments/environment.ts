// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json` .

export const environment = {
  production: false,
  buildConfig: 'dev',
  apiEndPoint: 'https://ujjs9bth90.execute-api.me-south-1.amazonaws.com/dev/',
  apiEndPointV2: 'https://ujjs9bth90.execute-api.me-south-1.amazonaws.com/dev/v2/',
  currentToken: 'DUID',
  facebookAPPID: '1578462858982905',
  googleAPPID:
    '183623623972-6aerbro3aj790tj37f23kfcohu2k3jb7.apps.googleusercontent.com',
  socketEndPoint: 'http://stc-chat.dynasty-dev.com',
  cookieDomain: 'localhost',
  eCommerceAPIEndPoint:
    'https://tuub60cp6h.execute-api.ap-southeast-1.amazonaws.com/dev/',
  firebase: {
    apiKey: 'AIzaSyDHcxNG7GGrNLw1DXpocp4VQ6C5CdKG7Vo',
    authDomain: 'dynasty-prod.firebaseapp.com',
    projectId: 'dynasty-prod',
    storageBucket: 'dynasty-prod.appspot.com',
    messagingSenderId: '8205006144',
    appId: '1:8205006144:web:dd5ab3a9cdf982afa676a5',
    measurementId: 'G-4BGZT4M2GZ',
  },
  articleS3BucketName: 'article',
  teamS3BucketName: 'team',
  tournamentS3BucketName: 'tournament',
  iOSBundelName: 'stcmobileapp',
  iOSAppstoreURL: 'https://apps.apple.com/us/app/stc-play/id1558331986',
  playStoreURL: 'https://play.google.com/store/apps/details?id=com.stc.xplay',
  payfort: {
    access_code: '2sXOSK8ogBoMmRr8sUWB',
    merchant_identifier: '4527f1a6',
    url: 'https://sbcheckout.payfort.com/FortAPI/paymentPage',
  },
  paypalScopes:
    'profile email address https://uri.paypal.com/services/paypalattributes',
  paypalAccountMode: 'sandbox',
  paypal_client_id:
    'AV50RJrnpN5ae-vS3RWVHuRb41vpW1HtGjTlPiqy9BFGLM0es9_9B5nATS0k2VO0XUIyNI0PPC2xWAAw',
  RECAPTCHA_V2_SITE_KEY: '6LejgGQaAAAAAKu6EMSQW2514m8gmt-NHdyE4nxO',
  RECAPTCHA_SCRIPT_URL: 'https://www.google.com/recaptcha/api.js',
  defaultLangCode: 'ar',
  rtl: ['ar'],
  language: [
    { code: 'en', key: 'english', value: 'English' },
    { code: 'ar', key: 'arabic', value: 'عربى' },
  ],
  playNowTimer: 3,
  playNowTop20: 30,
  playNowTop50: 30,
  playNowAny: 15,
  enableFirebase: true,
  gTagId: '',
  gTagUrl: 'https://www.googletagmanager.com/gtag/js?id=',
  gtmId: '',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
