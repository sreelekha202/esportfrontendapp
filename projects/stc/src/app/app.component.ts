import {
  AfterViewInit,
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  Inject,
  PLATFORM_ID,
  OnDestroy,
} from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { Direction, Directionality } from '@angular/cdk/bidi';
import { MatSidenav } from '@angular/material/sidenav';
import { filter, map, take } from 'rxjs/operators';
import { Meta, Title } from '@angular/platform-browser';
import { DOCUMENT } from '@angular/common';
import { environment } from '../environments/environment';
import { AppRoutesData } from './app-routing.model';
import {
  IUser,
  EsportsUserService,
  EsportsLanguageService,
  EsportsConstantsService,
  GlobalUtils,
  EsportsToastService,
  EsportsPaginationService,
  EsportsChatService,
  SuperProperties,
  EsportsGtmService,
} from 'esports';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { isPlatformBrowser } from '@angular/common';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';

declare var gtag;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild('chatSidenav') public chatSidenav: MatSidenav;
  @ViewChild('menuSidenav') public menuSidenav: MatSidenav;
  @ViewChild('appMainConatiner', { read: ElementRef })
  public scroll: ElementRef;
  direction: Direction = 'ltr';
  isRootPage = false;
  isAdminPage = false;
  currenUser: IUser;
  isUserLoggedIn = false;
  currentUserId = '';
  currentUserName = '';
  matchlist: any;
  userlist: any;
  initialUserList = [];
  showChat = false;
  matchdetails: any;
  typeofchat: any;
  chatWindows = [];
  URL: string;
  currentUser;
  clicked = false;
  closeResult: boolean;
  public innerWidth: any;
  isSpin = false;
  isSpin2 = false;
  spamcomment: any;
  showSearchListTitle = false;
  windowposition = 'chat_window chat_window_right_drawer';
  @ViewChild('searchinput') searchinput: ElementRef;
  useridtoblock: any;
  isBrowser: boolean;
  userSubscription: Subscription;

  constructor(
    public router: Router,
    private activatedRoute: ActivatedRoute,
    private userService: EsportsUserService,
    private globalUtils: GlobalUtils,
    private paginationService: EsportsPaginationService,
    private titleService: Title,
    private meta: Meta,
    private modalService: NgbModal,
    private languageService: EsportsLanguageService,
    private ConstantsService: EsportsConstantsService,
    public eSportsToastService: EsportsToastService,
    public translate: TranslateService,
    private directionality: Directionality,
    private chatService: EsportsChatService,
    private gtmService: EsportsGtmService,
    @Inject(DOCUMENT) private document: Document,
    @Inject(PLATFORM_ID) private platformId
  ) {
    // should be here for SSR
    this.isBrowser = isPlatformBrowser(platformId);

    if (this.isBrowser && GlobalUtils.isBrowser()) {
      this.checkAdmin();
      this.globalRouterEvents();

      window.addEventListener('storage', (event) => {
        if (event.newValue && event.key === environment.currentToken) {
          window.location.reload();
        }
        if (event.oldValue && event.key === environment.currentToken) {
          window.location.reload();
        }
      });

      const naveEndEvents = router.events.pipe(
        filter((event) => event instanceof NavigationEnd)
      );
      naveEndEvents.subscribe((event: NavigationEnd) => {
        gtag('config', 'G-ZBDK4LLBTQ', {
          page_path: event.urlAfterRedirects,
        });
      });
    }
  }

  ngOnInit(): void {
    if (this.isBrowser) {
      this.globalSubscriptions();
      this.paginationService.pageChanged.subscribe(
        (res) => {
          if (res) {
            this.scroll.nativeElement.scrollTop = 0;
          }
        },
        (err) => {}
      );
      this.getCurrentUserDetails();
    }
  }

  ngAfterViewInit(): void {
    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
        if (this.scroll?.nativeElement?.scrollTop) {
          this.scroll.nativeElement.scrollTop = 0;
        }
        return;
      }
      if (this.scroll?.nativeElement?.scrollTop) {
        this.scroll.nativeElement.scrollTop = 0;
      }
    });
  }

  onActivate(): void {
    // window.scroll(0, 0);
  }

  onMenuClick(): void {
    this.menuSidenav.toggle();
  }

  // Get the account type
  checkAdmin(): void {
    this.userService.currentUser
      .pipe(
        filter((data) => !!data),
        take(1)
      )
      .subscribe((data) => {
        if (data) {
          if (data.accountType === EsportsConstantsService.AccountType.Admin) {
            this.isAdminPage = true;
          }
        } else {
          this.isAdminPage = false;
        }
      });
  }

  private globalRouterEvents(): void {
    this.router.events
      .pipe(
        filter((event) => event instanceof NavigationEnd),
        map(() => this.activatedRoute.root.firstChild.snapshot.data)
      )
      .subscribe((data: AppRoutesData) => {
        this.isRootPage = data && data.isRootPage;

        const title = data && data.title;
        const tags = data && data.tags;
        if (title) {
          this.titleService.setTitle(title);
        }
        if (tags) {
          tags.forEach((tag) => {
            this.meta.updateTag(tag);
          });
        }
      });
  }

  onMenuOpenDash(): void {
    this.menuSidenav.toggle();
  }

  private globalSubscriptions(): void {
    this.languageService.language.subscribe((lang: string) => {
      if (lang) {
        setTimeout(() => this.setDirection(lang));
      }
    });
  }

  private setDirection(lang: string): void {
    const htmlTag = this.document.getElementsByTagName(
      'html'
    )[0] as HTMLHtmlElement;
    const currLang = this.ConstantsService?.language.find(
      (el) => el.code == lang
    );
    this.direction = this.ConstantsService?.rtl.includes(currLang?.code)
      ? 'rtl'
      : 'ltr';
    htmlTag.dir = this.ConstantsService?.rtl.includes(currLang?.code)
      ? 'rtl'
      : 'ltr';
    htmlTag.lang = lang;
  }

  getCurrentUserDetails() {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.isUserLoggedIn = true;
        this.chatService.initialiseSocket();
        this.currentUser = data;
        this.URL =
          this.document.location.origin +
          '/user/registration?referral=' +
          data.accountDetail.referralId;
      }
    });
  }
  ngOnDestroy(): void {
    this.userSubscription?.unsubscribe();
  }
  copyToClipboard(event: any) {
    const dummyElement = this.document.createElement('textarea');
    var element = document.getElementById('myDIV');
    element.classList.toggle('disableBtn');
    this.document.body.appendChild(dummyElement);
    dummyElement.value = this.URL;
    event.target.disabled = true;
    dummyElement.select();
    this.document.execCommand('copy');
    this.document.body.removeChild(dummyElement);
  }
  open(content) {
    if (this.currentUser.accountDetail.referralId) {
      this.clicked = false;
      this.modalService
        .open(content, { ariaLabelledBy: 'modal-basic-title', centered: true })
        .result.then(
          (result) => {
            this.copyToClipboard(this.URL);
          },
          (reason) => {
            this.closeResult = false;
          }
        );
    } else {
      this.eSportsToastService.showError(
        this.translate.instant('PROFILE.BASIC_INFO.REFERRAL_ERROR_MESSAGE')
      );
    }
  }
  mainMenuViewed() {
    this.pushGTMTags('View_Main_Menu');
    if (this.router.url.includes('home')) {
      this.pushGTMTags('View_Home_Tab');
    }
  }

  pushGTMTags(eventName: string) {
    let superProperties: SuperProperties = {};
    if (this.currentUser) {
      superProperties = this.gtmService.assignLoggedInUsedData(
        this.currentUser
      );
    }
    this.gtmService.gtmEventWithSuperProp({
      eventName: eventName,
      superProps: superProperties,
    });
  }

}
