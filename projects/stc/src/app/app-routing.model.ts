import { MetaDefinition } from '@angular/platform-browser';

export interface AppRoutesData {
  isRootPage?: boolean;
  tags?: MetaDefinition[];
  title?: string;
}

// Use this for routerLing and ts version of it like .navigate()
export enum AppHtmlRoutes {
  aboutUs = '/about-us',
  admin = '/admin',
  antiSpamPolicy = '/anti-spam-policy',
  article = '/article',
  articleEdit = '/article/edit',
  bracket = '/bracket',
  bracketCreate = '/bracket/create',
  codeOfConduct = '/code-of-conduct',
  comingSoon = '/comingsoon',
  createTeam = '/create-team',
  dec = '/dec',
  games = '/games',
  help = '/help',
  home = '/home',
  leaderboard = '/leaderboard',
  login = '/login',
  loginEmail = '/login-email',
  manageTeam = '/manage-team/',
  myMatches = '/my-matches',
  notFound = '/404',
  privacyPolicy = '/privacy-policy',
  register = '/register',
  registerEmail = '/register-email',
  search = '/search',
  searchArticle = '/search/article',
  searchShop = '/search/shop',
  searchTournament = '/search/tournament',
  searchVideo = '/search/video',
  shop = '/shop',
  shopOrder = '/shop/order',
  shopVoucher = '/shop/voucher',
  specialOffer = '/special-offer',
  termsOfUse = '/terms-of-use',
  tournament = '/tournament',
  tournamentCreate = '/tournament/create',
  tournamentEdit = '/tournament/edit/',
  tournamentInfo = '/tournament/info',
  tournamentManage = '/tournament/manage/',
  tournamentView = '/tournament',
  userPageType = '/user',
  videos = '/videos',
  videosView = '/videos/view',
  oneM = '1m',
}

export enum AppHtmlRoutesLoginType {
  createPass = 'createPass',
  emailLogin = 'email-login',
  forgotPass = 'forgotPass',
  phoneLogin = 'phone-login',
  registration = 'registration',
  registrationAsGameCenter = 'registration-game-center',
  registrationAsOrganizer = 'registration-organizer',
  registrationAsSponser = 'registration-sponser',
  selectProfile = 'selectProfile',
  infoUpdate = 'infoUpdate',
}

export enum AppHtmlProfileRoutes {
  AppReferralComponent = '/profile/app-referral',
  basicInfo = '/profile/basic-info',
  inbox = '/profile/inbox',
  myBookmarks = '/profile/my-bookmarks',
  myBracket = '/profile/my-bracket',
  myContent = '/profile/my-content',
  myStats = '/profile/my-statistics',
  myTournamentCreated = '/profile/my-tournament/created',
  myTournamentJoined = '/profile/my-tournament/joined',
  myTransactions = '/profile/my-transactions',
  setting = '/profile/profile-setting',
  teams = '/profile/teams',
  teamsView = '/profile/teams/',
}

export enum AppHtmlAdminRoutes {
  accessManagement = '/admin/access-management',
  contentManagement = '/admin/content-management',
  dashboard = '/admin/dashboard',
  editTeam = '/edit-team',
  esportsManagement = '/admin/esports-management',
  siteConfiguration = '/admin/site-configuration',
  spamManagement = '/admin/spam-management',
  teamManagement = '/admin/team-management',
  teamManagementEdit = '/admin/team-management/edit',
  userManagement = '/admin/user-management',
  userManagementView = '/admin/user-management/view',
  userNotifications = '/admin/user-notifications',
}

export enum AppHtmlTeamRegRoutes {
  newTeam = '/team-registration',
  teammates = '/team-registration/teammates',
  registration = '/team-registration/registration',
}

export enum AppHtmlGwbegRoutes {
  about = '/gwb2021/about',
  donate = '/gwb2021/donate',
  experience = '/gwb2021/experience',
  game = '/gwb2021/game',
  home = '/home',
  gwbHome='/gwb2021',
  learn = '/gwb2021/learn',
  watch = '/gwb2021/watch',
}
