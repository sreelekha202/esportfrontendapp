import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StoreModelComponent } from './store-model.component';

describe('StoreModelComponent', () => {
  let component: StoreModelComponent;
  let fixture: ComponentFixture<StoreModelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [StoreModelComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StoreModelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
