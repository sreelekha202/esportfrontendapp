import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';

@AutoUnsubscribe()
@Component({
  selector: 'app-store-model',
  templateUrl: './store-model.component.html',
  styleUrls: ['./store-model.component.scss'],
})
export class StoreModelComponent implements OnInit, OnDestroy {
  constructor(public dialogRef: MatDialogRef<StoreModelComponent>) {}

  ngOnInit(): void {}
  ngOnDestroy() {}
  onNoClick(): void {
    this.dialogRef.close();
  }
}
