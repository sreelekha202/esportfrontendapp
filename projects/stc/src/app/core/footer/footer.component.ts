import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  OnDestroy,
} from '@angular/core';
import { AppHtmlRoutes } from '../../app-routing.model';
import { environment } from '../../../environments/environment';
import { MatDialog } from '@angular/material/dialog';
import { StoreModelComponent } from './store-model/store-model.component';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';

@AutoUnsubscribe()
@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FooterComponent implements OnInit, OnDestroy {
  AppHtmlRoutes = AppHtmlRoutes;

  socials = [
    {
      link: 'https://www.twitch.tv/stcplay',
      icon: 'assets/icons/twitch-blue.svg',
    },
    {
      link: 'https://twitter.com/stcplay',
      icon: 'assets/icons/twitter-blue.svg',
    },
    {
      link: 'https://www.facebook.com/stcplay.hub',
      icon: 'assets/icons/facebook-blue.svg',
    },
    {
      link: 'https://www.instagram.com/stcplay',
      icon: 'assets/icons/instagram-blue.svg',
    },
    {
      link: 'https://www.snapchat.com/add/stcplay',
      icon: 'assets/icons/snapchat-blue.svg',
    },
    {
      link: 'https://www.youtube.com/channel/UCu9ZQKKPwtCP1ROZVmULfVQ',
      icon: 'assets/icons/youtube-blue.svg',
    },
  ];

  stores = [
    {
      link: 'https://play.google.com/store/apps/details?id=com.stc.xplay',
      image: 'assets/images/play-store/google-store.png',
      alt: 'google store',
    },
    {
      link: 'https://apps.apple.com/us/app/stc-play/id1558331986',
      image: 'assets/images/play-store/apple-store.png',
      alt: 'apple store',
    },
  ];

  navigation = [
    { link: AppHtmlRoutes.aboutUs, text: 'FOOTER.ABOUT' },
    { link: AppHtmlRoutes.termsOfUse, text: 'FOOTER.TERMS_OF_USE' },
    { link: AppHtmlRoutes.privacyPolicy, text: 'FOOTER.PRIVACY' },
    { link: AppHtmlRoutes.codeOfConduct, text: 'FOOTER.CODE_OF_CONDUCT' },
    { link: AppHtmlRoutes.antiSpamPolicy, text: 'FOOTER.ANTI_SPAM_POLICY' },
  ];

  constructor(public dialog: MatDialog) {}

  buildConfig = environment.buildConfig || 'N/A';

  ngOnInit(): void {}

  openDialog() {
    this.dialog.open(StoreModelComponent, {});
  }

  ngOnDestroy() {
    this.AppHtmlRoutes = null;
    this.socials = null;
    this.stores = null;
    this.navigation = null;
  }
}
