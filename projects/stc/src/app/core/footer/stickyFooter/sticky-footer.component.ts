import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  PLATFORM_ID,
  Inject,
  OnDestroy,
} from '@angular/core';
import {
  AppHtmlRoutes,
  AppHtmlProfileRoutes,
  AppHtmlRoutesLoginType,
} from '../../../../app/app-routing.model';
import { TranslateService } from '@ngx-translate/core';
import { isPlatformBrowser } from '@angular/common';
import { EsportsGtmService, EsportsUserService, SuperProperties } from 'esports';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';

@AutoUnsubscribe()
@Component({
  selector: 'app-sticky-footer',
  templateUrl: './sticky-footer.component.html',
  styleUrls: ['./sticky-footer.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class stickyFooterComponent implements OnInit, OnDestroy {
  AppHtmlProfileRoutes = AppHtmlProfileRoutes;
  AppHtmlRoutes = AppHtmlRoutes;
  AppHtmlRoutesLoginType = AppHtmlRoutesLoginType;
  isBrowser: boolean;
  constructor(
    public translate: TranslateService,
    private gtmService: EsportsGtmService,
    private userService: EsportsUserService,
    @Inject(PLATFORM_ID) private platformId
  ) {
    this.isBrowser = isPlatformBrowser(platformId);
  }
  gameCenterDataforRegister = {
    footerLinkUrl: [
      AppHtmlRoutes.userPageType,
      AppHtmlRoutesLoginType.selectProfile,
    ],
  };

  ngOnInit(): void {}

  ngOnDestroy() {}

  loginClick() {
    this.pushGTMTags('Login_Bottom_Banner');
  }

  registerClick() {
    this.pushGTMTags('Register_Bottom_Banner');
  }

  pushGTMTags(eventName: string) {
    let superProperties: SuperProperties = {};
    if (this.userService.currentUserSubject.value) {
      superProperties = this.gtmService.assignLoggedInUsedData(
        this.userService.currentUserSubject.value
      );
    }
    this.gtmService.gtmEventWithSuperProp({
      eventName: eventName,
      superProps: superProperties,
    });
  }
}
