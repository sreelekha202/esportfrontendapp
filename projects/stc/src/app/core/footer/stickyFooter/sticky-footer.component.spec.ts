import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { stickyFooterComponent } from './sticky-footer.component';

describe('stickyFooterComponent', () => {
  let component: stickyFooterComponent;
  let fixture: ComponentFixture<stickyFooterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [stickyFooterComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(stickyFooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
