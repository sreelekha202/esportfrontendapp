import { Component, OnInit, Input, Inject, OnDestroy } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Subscription } from 'rxjs';

import {
  EsportsUserService,
  EsportsGtmService,
  SuperProperties,
} from 'esports';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';

@AutoUnsubscribe()
@Component({
  selector: 'app-deepLink-header',
  templateUrl: './deepLink-header.component.html',
  styleUrls: ['./deepLink-header.component.scss'],
})
export class deepLinkHeaderComponent implements OnInit, OnDestroy {
  stores = [];
  userSubscription: Subscription;
  constructor(
    private userService: EsportsUserService,
    private gtmService: EsportsGtmService,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<deepLinkHeaderComponent>
  ) {}

  ngOnInit(): void {
    this.dialogRef.updatePosition({ top: `0px` });
    this.userSubscription = this.userService.currentUser.subscribe(
      (data) => {
        if (data) {
          this.onClose();
        }
      },
      (error) => {}
    );
    this.stores = [
      {
        link: 'https://play.google.com/store/apps/details?id=com.stc.xplay',
        image: 'assets/images/play-store/google-store.png',
        alt: 'google store',
        show: this.data.isAndroid,
      },
      {
        link: 'https://apps.apple.com/us/app/stc-play/id1558331986',
        image: 'assets/images/play-store/apple-store.png',
        alt: 'apple store',
        show: this.data.isIphone,
      },
    ];
  }
  onClose(): void {
    this.dialogRef.close(false);
  }

  ngOnDestroy() {
    this.userSubscription?.unsubscribe();
  }

  deepLinkClick() {
    this.pushGTMTags('App_Banner_Install_Click');
  }

  pushGTMTags(eventName: string) {
    let superProperties: SuperProperties = {};
    if (this.userService.currentUserSubject.value) {
      superProperties = this.gtmService.assignLoggedInUsedData(
        this.userService.currentUserSubject.value
      );
    }
    this.gtmService.gtmEventWithSuperProp({
      eventName: eventName,
      superProps: superProperties,
    });
  }
}
