import { AfterViewInit, Component, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';
import { AppHtmlRoutes } from '../../app-routing.model';
import { TranslateService } from "@ngx-translate/core";
import { EsportsToastService, EsportsGtmService } from "esports"
import { HomeService } from '../../core/service';
@AutoUnsubscribe()
@Component({
  selector: 'app-search-input',
  templateUrl: './search-input.component.html',
  styleUrls: ['./search-input.component.scss'],
})
export class SearchInputComponent implements AfterViewInit, OnDestroy {
  AppHtmlRoutes = AppHtmlRoutes;

  text = '';
  paginationData = {
    page: 1,
    limit: 8,
    sort: '-startDate',
  };

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private gtmService: EsportsGtmService,
    public translate: TranslateService,
    public eSportsToastService: EsportsToastService,
    private homeService: HomeService
  ) {}

  ngAfterViewInit(): void {}

  ngOnDestroy(): void {}


  onSearch(): void {
    if (!this.text) {
      this.eSportsToastService.showInfo(
        this.translate.instant('SEARCH.INPUT_TEXT_REQUIRED')
      );
    }
    if (this.text) {
      setTimeout(() => {
        this.router.navigate([AppHtmlRoutes.search], {
          relativeTo: this.activatedRoute,
          queryParams: { text: this.text },
          queryParamsHandling: 'merge',
        });
      }, 1000);
    }
    if (this.text) {
      const data = {
        name: this.text,
      };
      this.gtmService.gtmEvent('searched_match', data);
    }
  }
  onSearchInput(): void {
    if (this.text) {
      setTimeout(() => {
        this.router.navigate([AppHtmlRoutes.search], {
          relativeTo: this.activatedRoute,
          queryParams: { text: this.text },
          queryParamsHandling: 'merge',
        });
      }, 1000);
    }
    if (this.text) {
      const data = {
        name: this.text,
      };
      this.gtmService.gtmEvent('searched_match', data);
    }
  }
  clearInput() {
    this.text = '';
    this.homeService.updateSearchParams(this.text, this.paginationData);
    this.router.navigate([AppHtmlRoutes.search], {
      relativeTo: this.activatedRoute,
      queryParams: { text: this.text },
      queryParamsHandling: 'merge', // remove to replace all query params by provided
    });
  }
}
