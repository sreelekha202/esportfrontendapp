import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class TempService {
  webView: any;

  constructor() { }

  set setWebView(data: any) {
    this.webView = data;
  }

  get getWebView() {
      return this.webView;
  }

}
