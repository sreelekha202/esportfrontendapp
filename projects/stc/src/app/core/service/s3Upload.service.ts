import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

import { environment } from '../../../environments/environment';
import { EsportsToastService } from "esports"
@Injectable({
  providedIn: 'root',
})
export class S3UploadService {
  constructor(
    private http: HttpClient,
    private eSportsToastService: EsportsToastService,
    private translateService: TranslateService
  ) {}

  fileUpload(formData) {
    return this.http.post(`${environment.apiEndPoint}file-upload`, formData);
  }

  // size in bytes
  validateAndUploadImage = async (event, dimension, size, type) => {
    try {
      const file = event?.target?.files;

      if (!file.length) {
        throw new Error(this.translateService.instant('ERROR.IMAGE_NF'));
      }

      if (file[0]?.size > size) {
        throw new Error(
          `${this.translateService.instant(
            'ERROR.IMAGE_SIZE'
          )} ${this.formatBytes(size)}`
        );
      }

      if (!/\/(png|jpe?g)$/i.test(file[0]?.type)) {
        throw new Error(this.translateService.instant('ERROR.IMAGE_FORMAT'));
      }
      return await this.uploadImage(file[0], dimension, type);
    } catch (error) {
      throw error;
    }
  };

  formatBytes = (bytes) => {
    if (bytes === 0) return '0 Bytes';
    const k = 1024;
    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
    const i = Math.floor(Math.log(bytes) / Math.log(k));
    return parseFloat((bytes / Math.pow(k, i)).toFixed(2)) + ' ' + sizes[i];
  };

  uploadImage = async (file, dimension, type) => {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      const self = this;
      reader.onload = async (e) => {
        try {
          const image: any = new Image();
          image.src = e.target.result;
          image.onload = async (imgEvent) => {
            try {
              if (
                imgEvent.target.height != dimension.height &&
                imgEvent.target.width != dimension.width
              ) {
                self.eSportsToastService.showInfo(
                  self.translateService.instant('ERROR.IMAGE_RESOLUTION')
                );
              }

              const payload = {
                path: environment.tournamentS3BucketName,
                file: await this.toBase64(file),
                type,
              };

              const imageUpload = await this.http
                .post(`${environment.apiEndPoint}file-upload`, payload)
                .toPromise();
              resolve(imageUpload);
            } catch (error) {
              reject(error);
            }
          };
        } catch (error) {
          reject(error);
        }
      };
    });
  };

  toBase64(file) {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = (error) => reject(error);
    });
  }
}
