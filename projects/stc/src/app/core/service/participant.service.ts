import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { SubscribableOrPromise } from 'rxjs';
import { IUpdateParticipantByStatus } from '../../shared/models/participant'
@Injectable({
  providedIn: 'root',
})
export class ParticipantService {
  BaseURL = environment.apiEndPoint;
  constructor(private http: HttpClient) {}

  public getParticipantsWithQuery(params) {
    const encodedUrl = encodeURIComponent(params.filter);
    let url = environment.apiEndPoint + `participant?query=${encodedUrl}`;
    if (params.projection) {
      const select = encodeURIComponent(params.select);
      url = `&select=${select}`;
    }
    if (params.option) {
      const opt = encodeURIComponent(params.option);
      url += `&option=${opt}`;
    }
    return this.http.get(`${url}`);
  }

  updateParticipantByStatus(payload: IUpdateParticipantByStatus): SubscribableOrPromise<any> {
    return this.http.post(`${environment.apiEndPoint}participant/v2/update-status`, payload).toPromise();
  }
  
  fetchParticipantByStatus(type: number, tournamentId: string, page: any): SubscribableOrPromise<any> {
    const requestUrl = `${environment.apiEndPoint}participant/v2?type=${type}&tournamentId=${tournamentId}&page=${page.num}&limit=${page.limit}`;
    return this.http.get(requestUrl).toPromise();
  }
}
