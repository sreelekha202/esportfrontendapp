import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable, SubscribableOrPromise } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class HomeService {
  public searchedTournamentSubject: BehaviorSubject<[]>;
  public searchedTournament: Observable<[]>;
  public searchedArticleSubject: BehaviorSubject<[]>;
  public searchedArticle: Observable<[]>;
  public searchedVideoSubject: BehaviorSubject<[]>;
  public searchedVideo: Observable<[]>;

  private tournamentStatusFilter = 3;
  private headerSearchText = '';
  private category = '';
  private sort = '';
  private page = '';
  private limit = '';

  constructor(private http: HttpClient) {
    this.searchedTournamentSubject = new BehaviorSubject([]);
    this.searchedTournament = this.searchedTournamentSubject.asObservable();
    this.searchedArticleSubject = new BehaviorSubject([]);
    this.searchedArticle = this.searchedArticleSubject.asObservable();
    this.searchedVideoSubject = new BehaviorSubject([]);
    this.searchedVideo = this.searchedVideoSubject.asObservable();
  }

  ongoing_tournaments(): Observable<any> {
    return this.http.get(environment.apiEndPoint + `home/ongoing_tournaments`);
  }
  hottest_post(): Observable<any> {
    return this.http.get(environment.apiEndPoint + `home/hottest_post`);
  }
  trending_posts(): Observable<any> {
    return this.http.get(environment.apiEndPoint + `home/trending_posts`);
  }

  /**
   * Tournament search private helper function
   */
  private _searchTournament(): Observable<any> {
    const encodedText = encodeURIComponent(this.headerSearchText);
    return this.http.get(
      environment.apiEndPoint +
        `home/tournament?text=${encodedText}&status=${this.tournamentStatusFilter}&category=${this.category}&sort=${this.sort}&page=${this.page}&limit=${this.limit}&gwb=true`
    );
  }

  /**
   * Article search private helper function
   */
  private _searchArticle(): Observable<any> {
    const encodedText = encodeURIComponent(this.headerSearchText);
    return this.http.get(
      environment.apiEndPoint +
        `home/article?text=${encodedText}&page=${this.page}&limit=${this.limit}`
    );
  }

  /**
   * method to get an announcement object
   */
  getAnnouncements() {
    return this.http.get(environment.apiEndPoint + `general/announcement`);
  }

  /**
   * banner list private helper function
   */
  _getBanner(): Observable<any> {
    return this.http.get(environment.apiEndPoint + `banner`);
  }

  /**
   * top players List
   */
  _topPlayers(): Observable<any> {
    return this.http.get(environment.apiEndPoint + `home/top-player`);
  }

  /**
   * leader board List
   */
  _leaderBoard(): Observable<any> {
    return this.http.get(environment.apiEndPoint + `home/fetchleaderboard`);
  }

  /**
   * Article search private helper function
   */
  private _searchVideo(): Observable<any> {
    const encodedText = encodeURIComponent(this.headerSearchText);
    return this.http.get(
      environment.apiEndPoint +
        `home/video?text=${encodedText}&page=${this.page}&limit=${this.limit}`
    );
  }

  /**
   * Tournament search service
   * can be called from anywhere
   */
  searchTournament(param, pagination) {
    this.headerSearchText = param || '';
    this.sort = pagination.sort || '-startDate';
    this.page = pagination.page || 1;
    this.limit = pagination.limit || 8;
    this._searchTournament().subscribe((res) => {
      this.searchedTournamentSubject.next(res.data || []);
      return this.searchedTournamentSubject.value;
    });
  }

  /**
   * Article search service
   * can be called from anywhere
   */
  searchArticle(param, pagination) {
    this.headerSearchText = param || '';
    this.sort = pagination.sort || '-startDate';
    this.page = pagination.page || 1;
    this.limit = pagination.limit || 8;
    this._searchArticle().subscribe((res) => {
      this.searchedArticleSubject.next(res.data);
      return this.searchedArticleSubject.value;
    });
  }

  /**
   * Video search service
   * can be called from anywhere
   */
  searchVideo(param, pagination) {
    this.headerSearchText = param || '';
    this.sort = pagination.sort || '-startDate';
    this.page = pagination.page || 1;
    this.limit = pagination.limit || 8;
    this._searchVideo().subscribe((res) => {
      this.searchedVideoSubject.next(res.data);
      return this.searchedVideoSubject.value;
    });
  }
  /**
   * Play Now Ladder Keys
   */

  _playNowKeys(): Observable<any> {
    return this.http.get(environment.apiEndPoint + `home/play-now-keys`);
  }

  /**
   * Update tournament status, this is global function.
   * Header search will take filter value from here only.
   * to add filter value call this function.
   * @param status TournamentStatus
   */
  updateTournamentStatusFilter(status) {
    this.tournamentStatusFilter = status || 0;
    this._searchTournament().subscribe((res) => {
      this.searchedTournamentSubject.next(res.data);
      return this.searchedTournamentSubject.value;
    });
  }

  updateSearchParams(param, pagination) {
    this.headerSearchText = param || '';
    this.sort = pagination.sort || '-startDate';
    this.page = pagination.page || 1;
    this.limit = pagination.limit || 8;

    this.searchTournament(param, pagination);
    this.searchArticle(param, pagination);
    this.searchVideo(param, pagination);
  }

  updateView(body): SubscribableOrPromise<any> {
    return this.http
      .patch(`${environment.apiEndPoint}home/updateView`, body)
      .toPromise();
  }

  fetchScore(): SubscribableOrPromise<any> {
    return this.http
      .get(`${environment.apiEndPoint}home/fetch_score`)
      .toPromise();
  }

  /**
   * show participants option
   */
  isShowParticipants(): SubscribableOrPromise<any> {
    return this.http
      .get(environment.apiEndPoint + `home/getShowParticipantsStatus`)
      .toPromise();
  }
}
