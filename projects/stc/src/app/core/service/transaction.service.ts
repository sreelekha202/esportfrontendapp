import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, SubscribableOrPromise } from 'rxjs';
import { environment } from '../../../environments/environment';
import { EsportsConstantsService } from 'esports'

@Injectable({
  providedIn: 'root',
})
export class TransactionService {
  constructor(
    private http: HttpClient,
    private constantService: EsportsConstantsService
  ) {}

  getTransactionDetail(tDetail): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    return this.http.post(
      environment.apiEndPoint + 'transaction/getDetail',
      tDetail,
      {
        headers: httpHeaders,
      }
    );
  }

  /**
   * Return product list promise
   */
  getProduct(type): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    const url = `${environment.apiEndPoint}transaction/getProduct?type=${type}`;
    return this.http.get(url);
  }

  createOrUpdateTransaction(payload): SubscribableOrPromise<any> {
    return this.http
      .patch(`${environment.apiEndPoint}transaction/pay`, payload)
      .toPromise();
  }

  verifyStcTransaction(payload): SubscribableOrPromise<any> {
    return this.http
      .post(`${environment.apiEndPoint}transaction/stcVerifyPay`, payload)
      .toPromise();
  }

  paymentDetail(params): SubscribableOrPromise<any> {
    const encodedPagination = encodeURIComponent(params.pagination);
    const url = `${environment.apiEndPoint}transaction/paymenthitstory?pagination=${encodedPagination}`;
    return this.http.get(url).toPromise();
  }

  payfortSign(payload): SubscribableOrPromise<any> {
    const url = `${environment.apiEndPoint}transaction/payfortSign`;
    return this.http.post(url, payload).toPromise();
  }

  createOrUpdateAccTransaction(payload): SubscribableOrPromise<any> {
    return this.http
      .patch(`${environment.apiEndPoint}transaction/account`, payload)
      .toPromise();
  }

  verifyAccTransaction(payload): SubscribableOrPromise<any> {
    return this.http
      .patch(`${environment.apiEndPoint}transaction/accountVerify`, payload)
      .toPromise();
  }
}
