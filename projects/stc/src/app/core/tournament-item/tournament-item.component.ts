import { Component, Input, OnDestroy, OnInit } from '@angular/core';

import { AppHtmlRoutes } from '../../app-routing.model';
import { EsportsLanguageService } from 'esports';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';

export interface TournamentItemComponentData {
  _id: string;
  banner: string;
  endDate: string;
  isFinished: boolean;
  isSeeded: boolean;
  maxParticipants: string;
  name: string;
  participantJoined: any;
  participants: string;
  regionsAllowed: string;
  registrationIsOpen: string;
  slug: string;
  startDate: string;
  startTime: string;
}

@AutoUnsubscribe()
@Component({
  selector: 'app-tournament-item',
  templateUrl: './tournament-item.component.html',
  styleUrls: ['./tournament-item.component.scss'],
})
export class TournamentItemComponent implements OnInit, OnDestroy {
  @Input() data: TournamentItemComponentData;

  AppHtmlRoutes = AppHtmlRoutes;

  currentLang;

  constructor(public language: EsportsLanguageService) {}

  ngOnInit(): void {
    this.language.language.subscribe((lang) => {
      if (lang) {
        this.currentLang = lang;
      }
    });
  }

  ngOnDestroy() {}

  getRegistrationStatus(
    startDate,
    startTime,
    isSeeded,
    isFinished,
    maxParticipants,
    participantsJoined
  ) {
    if (!startDate || !startTime) {
      return;
    }

    const timeObj = this.convert12HrsTo24HrsFormat(startTime);
    const sDate = new Date(startDate);

    sDate.setHours(timeObj['hour']);
    sDate.setMinutes(timeObj['minute']);

    if (isSeeded == true || isFinished == true) {
      return 'Closed';
    } else if (participantsJoined >= maxParticipants) {
      return 'Closed';
    } else if (sDate.getTime() < Date.now()) {
      return 'Closed';
    } else {
      return 'Open';
    }
  }

  convert12HrsTo24HrsFormat(time) {
    if (!time) {
      return;
    }
    let hours = Number(time.match(/^(\d+)/)[1]);
    let minutes = Number(time.match(/:(\d+)/)[1]);
    let AP = time.match(/\s(.*)$/);
    if (!AP) {
      AP = time.slice(-2);
    } else {
      AP = AP[1];
    }
    if (AP == 'PM' && hours < 12) {
      hours = hours + 12;
    }
    if (AP == 'AM' && hours == 12) {
      hours = hours - 12;
    }
    let Hours24 = hours.toString();
    let Minutes24 = minutes.toString();
    if (hours < 10) {
      Hours24 = '0' + Hours24;
    }
    if (minutes < 10) {
      Minutes24 = '0' + Minutes24;
    }

    return { hour: parseInt(Hours24), minute: parseInt(Minutes24) };
  }
}
