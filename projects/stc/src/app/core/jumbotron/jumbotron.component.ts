import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { EsportsLanguageService } from 'esports';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';
import { AppHtmlRoutes } from '../../app-routing.model';

export interface JumbotronComponentData {
  _id: string;
  btn: string;
  dateTitle: string;
  ribbon: string;
  slug: string;
  title?: any;
  user: {
    img: string;
    name: string;
  };

  isBtnHidden?: boolean;
}
@AutoUnsubscribe()
@Component({
  selector: 'app-jumbotron',
  templateUrl: './jumbotron.component.html',
  styleUrls: ['./jumbotron.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class JumbotronComponent implements OnInit, OnDestroy {
  @Input() backgroundUrl: any;
  @Input() data: JumbotronComponentData;

  AppHtmlRoutes = AppHtmlRoutes;

  constructor(public languageService: EsportsLanguageService) {}

  ngOnChanges() {}

  ngOnInit(): void {}
  ngOnDestroy() {}
}
