import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';

@AutoUnsubscribe()
@Component({
  selector: 'app-drop-down-select',
  templateUrl: './drop-down-select.component.html',
  styleUrls: ['./drop-down-select.component.scss'],
})
export class DropDownSelectComponent implements OnInit, OnDestroy {
  @Input() customFormControlName: string;
  @Input() customFormGroup: FormGroup;
  @Input() dropdownList;
  @Input() title: string;
  @Input() type: string;
  @Output() valueEmit = new EventEmitter<number>();

  constructor() {}

  ngOnInit(): void {}
  ngOnDestroy() {}

  setValue(val) {
    this.customFormGroup.get(this.customFormControlName).setValue(val);
    this.valueEmit.emit(val);
  }
}
