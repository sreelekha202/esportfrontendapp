import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit,
  EventEmitter,
  Output,
  OnDestroy,
} from '@angular/core';
import { EsportsLanguageService } from 'esports';
import { TranslateService } from '@ngx-translate/core';

import { AppHtmlRoutes } from '../../app-routing.model';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';

export interface ArticleItemComponentData {
  _id: string;
  author?: string;
  createdDate: string;
  id: number;
  image: string;
  shortDescription: any;
  slug: string;
  title: any;
}

@AutoUnsubscribe()
@Component({
  selector: 'app-article-item',
  templateUrl: './article-item.component.html',
  styleUrls: ['./article-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ArticleItemComponent implements OnInit, OnDestroy {
  @Input() data: ArticleItemComponentData;
  @Output() onArticleClick = new EventEmitter<ArticleItemComponentData>();

  AppHtmlRoutes = AppHtmlRoutes;

  constructor(
    public translate: TranslateService,
    public languageService: EsportsLanguageService
  ) {}

  ngOnInit(): void {}
  ngOnDestroy() {}
  onclick() {
    this.onArticleClick.emit(this.data);
  }
}
