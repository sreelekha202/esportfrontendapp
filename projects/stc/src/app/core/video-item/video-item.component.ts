import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit,
  EventEmitter,
  Output,
  OnDestroy,
} from '@angular/core';
import { EsportsLanguageService } from 'esports';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';
import { AppHtmlRoutes } from '../../app-routing.model';

@AutoUnsubscribe()
@Component({
  selector: 'app-video-item',
  templateUrl: './video-item.component.html',
  styleUrls: ['./video-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class VideoItemComponent implements OnInit, OnDestroy {
  @Input() data;
  @Output() onVideoClick = new EventEmitter();

  AppHtmlRoutes = AppHtmlRoutes;

  constructor(public languageService: EsportsLanguageService) {}

  ngOnInit(): void {}
  ngOnDestroy() {}
  onclick() {
    this.onVideoClick.emit(this.data);
  }
}
