import {
  Component,
  OnInit,
  Inject,
  HostListener,
  OnDestroy,
  Output,
  EventEmitter,
  AfterViewInit,
} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import {
  EsportsChatSidenavService,
  EsportsLanguageService,
  EsportsConstantsService,
  EsportsUserService,
  EsportsChatService,
  EsportsToastService,
  GlobalUtils,
  EsportsGtmService,
  SuperProperties,
  EventProperties,
} from 'esports';

import {
  AppHtmlRoutes,
  AppHtmlProfileRoutes,
  AppHtmlGwbegRoutes,
  AppHtmlRoutesLoginType
} from '../../app-routing.model';
import { DOCUMENT } from '@angular/common';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';

import { environment } from '../../../environments/environment';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';

const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;

@AutoUnsubscribe()
@Component({
  selector: 'app-aside',
  templateUrl: './aside.component.html',
  styleUrls: ['./aside.component.scss'],
})
export class AsideComponent implements OnInit, OnDestroy, AfterViewInit {
  AppHtmlProfileRoutes = AppHtmlProfileRoutes;
  AppHtmlRoutesLoginType = AppHtmlRoutesLoginType;
  AppHtmlRoutes = AppHtmlRoutes;
  AppHtmlGwbegRoutes = AppHtmlGwbegRoutes;
  activeLang;
  isAdmin = false;

  currentUser;
  clicked = false;
  AppLanguage = [];
  userStatistic = [];
  URL: string;
  closeResult: boolean;
  pageNavigation = [];
  gameCenterDataforRegister = {
    footerLinkUrl: [
      AppHtmlRoutes.userPageType,
      AppHtmlRoutesLoginType.selectProfile,
    ],
  };
  profileNavigation = [
    // {
    //   name: 'header.HELP',
    //   url: AppHtmlRoutes.help,
    //   icon: 'assets/icons/navigations/help.svg',
    // },
  ];
  public innerWidth: any;
  userSubscription: Subscription;
  AllCountries = [];
  @Output() viewed = new EventEmitter<Boolean>();

  constructor(
    @Inject(DOCUMENT) private document: Document,
    private chatSidenavService: EsportsChatSidenavService,
    private languageService: EsportsLanguageService,
    private ConstantsService: EsportsConstantsService,
    private modalService: NgbModal,
    private router: Router,
    private userService: EsportsUserService,
    private chatService: EsportsChatService,
    public translate: TranslateService,
    private gtmService: EsportsGtmService,
    private activatedRoute: ActivatedRoute,
    public eSportsToastService: EsportsToastService
  ) {}
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    if (GlobalUtils.isBrowser()) {
      this.innerWidth = window.innerWidth;
    }
  }

  ngAfterViewInit() {
    this.viewed.emit(true);
  }

  ngOnInit(): void {
    this.languageService.language.subscribe((lang: string) => {
      this.activeLang = lang;
      this.pageNavigation = [
        {
          name: 'header.HOME',
          url: AppHtmlRoutes.home,
          icon: 'assets/icons/navigations/home.svg',
          test_id: 'tournament-button-home',
          gtmEvent: 'View_Home_Tab',
        },
        // {
        //   name: 'header.GWB',
        //   url: AppHtmlGwbegRoutes.gwbHome,
        //   icon: 'assets/icons/navigations/gwb.svg',
        //   test_id: 'tournament-button-gwb',
        //   gtmEvent: 'View GWB Home Page',
        //   queryParam: { locale: this.activeLang },
        // },
        // {
        //   name: 'header.TURKI',
        //   url: AppHtmlGwbegRoutes.gwbHome,
        //   icon: 'assets/icons/navigations/turki.svg',
        //   test_id: 'tournament-button-turki',
        //   gtmEvent: 'View GWB Home Page',
        //   queryParam: { locale: this.activeLang },
        //   id: '3wb',
        // },
        {
          name: 'header.DISCOVER',
          url: AppHtmlRoutes.article,
          icon: 'assets/icons/navigations/discover.svg',
          test_id: 'tournament-button-discover',
          gtmEvent: 'View_Discover_Tab',
        },
        {
          name: 'header.GAMES',
          url: AppHtmlRoutes.games,
          icon: 'assets/icons/navigations/games.svg',
          test_id: 'tournament-button-games',
          gtmEvent: 'View_Matchmaking_Tab',
        },
        {
          name: 'header.TOURNAMENTS',
          url: AppHtmlRoutes.tournament,
          icon: 'assets/icons/navigations/tournaments.svg',
          test_id: 'tournament-button-tournaments',
          gtmEvent: 'View_Tournaments_Tab',
        },
        {
          name: 'header.LEADERBOARD',
          url: AppHtmlRoutes.leaderboard,
          icon: 'assets/icons/navigations/leaderboard.svg',
          test_id: 'tournament-button-leaderboard',
          gtmEvent: 'View_Leaderboard_Tab',
        },
      ];
    });
    if (GlobalUtils.isBrowser()) {
      this.getAllCountries();
      this.innerWidth = window.innerWidth;
      this.activeLang = this.translate.currentLang;
      this.AppLanguage = this.ConstantsService?.language;
      this.getUserData();
    }
  }

  ngOnDestroy() {
    this.userSubscription?.unsubscribe();
  }

  onConfirmLogout(content) {
    this.modalService.open(content, {
      centered: true,
      windowClass: 'custom-modal-content',
    });
  }
  copyToClipboard(event: any) {
    const dummyElement = this.document.createElement('textarea');
    var element = document.getElementById('myDIV');
    element.classList.toggle('disableBtn');
    this.document.body.appendChild(dummyElement);
    dummyElement.value = this.URL;
    event.target.disabled = true;
    dummyElement.select();
    this.document.execCommand('copy');
    this.document.body.removeChild(dummyElement);
  }
  open(content) {
    if (this.currentUser.accountDetail.referralId) {
      this.clicked = false;
      this.modalService
        .open(content, { ariaLabelledBy: 'modal-basic-title', centered: true })
        .result.then(
          (result) => {
            this.copyToClipboard(this.URL);
          },
          (reason) => {
            this.closeResult = false;
          }
        );
    } else {
      this.eSportsToastService.showError(
        this.translate.instant('PROFILE.BASIC_INFO.REFERRAL_ERROR_MESSAGE')
      );
    }
  }
  onLogOut(): void {
    this.pushGTMTags('Logout');
    if (GlobalUtils.isBrowser()) {
      this.chatService.setChatStatus(false);
      this.chatService?.disconnectUser();
      this.userService.logout(API, TOKEN);
      this.chatSidenavService.close();
      this.modalService.dismissAll();
      this.router.navigate([AppHtmlRoutes.home]);
    }
  }

  onLanguageChange(lang): void {
    if (GlobalUtils.isBrowser()) {
      this.pushGTM('turki', 'Change_Language');
      localStorage.setItem('currentLanguage', lang);
      this.activeLang = lang;
      this.translate.use(lang);
      this.languageService.setLanguage(lang);
      window.location.reload();
    }
  }

  getUserData() {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (!data) {
        return;
      }
      this.currentUser = data;
      this.URL =
        this.document.location.origin +
        '/user/registration?referral=' +
        data.accountDetail.referralId;
      this.isAdmin =
        data?.accountType === EsportsConstantsService.AccountType.Admin &&
        data?.accessLevel.length > 0;
      this.userStatistic = [
        {
          count: this.currentUser.accountDetail?.reward || 0,
          title: 'PROFILE.BASIC_INFO.FORM.REWARD.NAME',
        },
        {
          count: this.currentUser.preference?.followingCount || 0,
          title: 'header.following',
        },
        {
          count: this.currentUser.preference?.followersCount || 0,
          title: 'header.followers',
        },
      ];
    });
  }
  getAllCountries() {
    this.AllCountries = [];
    this.userService.getAllCountries().subscribe((data) => {
      this.AllCountries = data.countries;
    });
  }

  gotoAdmin() {
    const deafultUrl = this.userService.fetchAdminDefaultAccess();
    if (deafultUrl) {
      this.router.navigate([deafultUrl]);
    }
  }

  pushGTMTags(eventName: string) {
    let superProperties: SuperProperties = {};
    if (this.currentUser) {
      superProperties = this.gtmService.assignLoggedInUsedData(
        this.currentUser
      );
    }

    let eventProperties: EventProperties = {};

    if (eventName == 'View_Profile') {
      eventProperties['fromPage'] = this.router.url;
    }

    this.gtmService.gtmEventWithSuperProp({
      eventName: eventName,
      superProps: superProperties,
      eventProps: eventProperties,
    });
  }
  pushGTM(id, eventName: string) {
    let superProperties: SuperProperties = {};
    if (this.currentUser) {
      superProperties = this.gtmService.assignLoggedInUsedData(
        this.currentUser
      );
    }

    let eventProperties: EventProperties = {};

    if (eventName == 'View_Profile') {
      eventProperties['fromPage'] = this.router.url;
    }

    this.gtmService.gtmEventWithSuperProp({
      eventName: eventName,
      superProps: superProperties,
      eventProps: eventProperties,
    });
    setTimeout(() => {
      let el = document.getElementById(id);
      el.scrollIntoView({ behavior: 'smooth', block: 'center' });
    }, 1500);
  }
}
