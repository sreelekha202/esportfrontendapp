import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Input,
  Output,
  EventEmitter,
  OnDestroy,
} from '@angular/core';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';

import {
  AppHtmlProfileRoutes,
  AppHtmlRoutes,
} from '../../../app-routing.model';

@AutoUnsubscribe()
@Component({
  selector: 'app-navigate',
  templateUrl: './navigate.component.html',
  styleUrls: ['../aside.component.scss', './navigate.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NavigateComponent implements OnInit, OnDestroy {
  @Input() pageNavigation: any;
  @Input() currentUser: any;
  @Output() callback = new EventEmitter();

  AppHtmlProfileRoutes = AppHtmlProfileRoutes;
  AppHtmlRoutes = AppHtmlRoutes;

  constructor() {}

  ngOnInit(): void {}

  pushGTMTags(type) {
    this.callback.emit({ type: 'tag', data: type });
  }
  gotoAdmin() {
    this.callback.emit({ type: 'admin' });
  }

  pushGTM(id, event) {
    this.callback.emit({ type: 'tag_id', data: { id, event } });
  }

  ngOnDestroy() {
    this.callback.unsubscribe();
  }
}
