import { Injectable } from '@angular/core';
import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';


import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

import { environment } from '../../../../environments/environment';
import {
  EsportsConstantsService,
  GlobalUtils,
  EsportsUserService,
} from 'esports';
import { TempService } from "../../service";

const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
  constructor(
    private eSportConstantsService: EsportsConstantsService,
    private tempService: TempService,
    private userService: EsportsUserService
  ) {}

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    // add auth header with jwt if user is logged in and request is to api url

    const index = request.url.lastIndexOf('/');
    const url = request.url.substring(index + 1);
    const temp = this.tempService?.getWebView;
    if (GlobalUtils.isBrowser()) {
      if (url == 'file-upload' || url == 'upload') {
        if (localStorage.getItem(environment.currentToken)) {
          request = request.clone({
            setHeaders: {
              Authorization: `Bearer ${localStorage.getItem(
                environment.currentToken
              )}`,
            },
          });
        }
      } else {
        if (localStorage.getItem(environment.currentToken)) {
          request = request.clone({
            setHeaders: {
              'Content-Type': 'application/json',
              Authorization: `Bearer ${localStorage.getItem(
                environment.currentToken
              )}`,
            },
          });
        } else if (temp?.token) {
          request = request.clone({
            setHeaders: {
              'Content-Type': 'application/json',
              Authorization: `Bearer ${temp?.token}`,
            },
          });
        }
      }
      // Add current language
      const locale =
        localStorage.getItem('currentLanguage') ||
        this.eSportConstantsService.defaultLangCode;
      request = request.clone({
        setHeaders: { locale },
      });
    }
    // return next.handle(request);
    return next.handle(request).pipe(
      tap(
        (event) => {},
        (error) => {
          if (error instanceof HttpErrorResponse) {
            if (error.status === 0) {
              throw new Error(
                'Please try again.'
              );
            } else if (
              error?.status === 401 &&
              [error?.statusText, error?.error].includes('Unauthorized')
            ) {
              this.userService.refreshToken(API, TOKEN).subscribe(
                (data) => {
                  if (GlobalUtils.isBrowser()) {
                    window.location.reload();
                  }
                },
                (error) => {
                  throw new Error(
                    'Your token has been expired. Please login again.'
                  );
                }
              );

            }
          }
        }
      )
    );
  }
}
