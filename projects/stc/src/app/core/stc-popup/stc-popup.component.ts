import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { AppHtmlProfileRoutes } from '../../app-routing.model';
import { GlobalUtils } from 'esports';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';

@AutoUnsubscribe()
@Component({
  selector: 'app-stc-popup',
  templateUrl: './stc-popup.component.html',
  styleUrls: ['./stc-popup.component.scss'],
})
export class StcPopupComponent implements OnInit, OnDestroy {
  AppHtmlProfileRoutes = AppHtmlProfileRoutes;
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<any>
  ) {}

  ngOnInit(): void {}
  ngOnDestroy() {}

  goToUrl() {
    if (GlobalUtils.isBrowser()) {
      window.open('https://stcpay.com.sa/', '_blank');
    }
  }

  onClose(): void {
    this.dialogRef.close(false);
  }
}
