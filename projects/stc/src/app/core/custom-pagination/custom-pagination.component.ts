import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';

import { IPagination, EsportsPaginationService } from 'esports';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';
import { PageChangedEvent } from 'ngx-bootstrap/pagination';

@AutoUnsubscribe()
@Component({
  selector: 'app-pagination',
  templateUrl: './custom-pagination.component.html',
  styleUrls: ['./custom-pagination.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CustomPaginationComponent implements OnInit, OnChanges, OnDestroy {
  @Input() activePage = 1;
  @Input() page: IPagination;
  @Output() currentPage = new EventEmitter<Number>();

  constructor(private paginationService: EsportsPaginationService) {}

  ngOnInit(): void {}
  ngOnDestroy() {}

  pageChanged(event: PageChangedEvent) {
    this.currentPage.emit(event.page);
    this.paginationService.setPageChanged(true);
  }

  ngOnChanges(simpleChanges: SimpleChanges) {}
}
