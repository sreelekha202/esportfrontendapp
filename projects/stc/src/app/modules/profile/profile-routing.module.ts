import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { AppReferralComponent } from './app-referral/app-referral.component';
import { BasicInfoComponent } from './basic-info/basic-info.component';
import { InboxComponent } from './inbox/inbox.component';
import { InboxMessageComponent } from './inbox/inbox-message/inbox-message.component';
import { LeaderBoardPlayerComponent } from '../leader-board/leader-board-player/leader-board-player.component';
import { MyBookmarksComponent } from './my-bookmarks/my-bookmarks.component';
import { MyBracketComponent } from './my-bracket/my-bracket.component';
import { MyContentComponent } from './my-content/my-content.component';
import { MyTeamsComponent } from './my-teams/my-teams.component';
import { MyTeamsViewComponent } from './my-teams-view/my-teams-view.component';
import { MyTournamentComponent } from './my-tournament/my-tournament.component';
import { MyTransactionsComponent } from './my-transactions/my-transactions.component';
import { ProfileComponent } from './profile.component';
import { ProfileSettingComponent } from './profile-setting/profile-setting.component';

const routes: Routes = [
  {
    path: '',
    component: ProfileComponent,
    children: [
      {
        path: '',
        redirectTo: 'basic-info',
        pathMatch: 'full',
        data: {
          tags: [
            {
              name: 'title',
              content:
                'Complete your profile. Stay connected with the gaming community on stcplay.gg',
            },
          ],
          title: 'Complete your profile',
        },
      },
      {
        path: 'my-statistics',
        component: LeaderBoardPlayerComponent,
        data: {
          tags: [
            {
              name: 'title',
              content:
                'View your statistics based on the games and tournaments you have participated in.',
            },
          ],
          title: 'My Statistics',
        },
      },
      {
        path: 'basic-info',
        component: BasicInfoComponent,
        data: {
          tags: [
            {
              name: 'title',
              content:
                'Complete your profile. Stay connected with the gaming community on stcplay.gg',
            },
          ],
          title: 'Complete your profile',
        },
      },
      {
        path: 'teams',
        children: [
          {
            path: '',
            component: MyTeamsComponent,
          },
          {
            path: ':id',
            component: MyTeamsViewComponent,
          },
        ],
      },
      { path: 'inbox', component: InboxComponent },
      {
        path: 'my-content',
        component: MyContentComponent,
      },
      {
        path: 'my-bookmarks',
        component: MyBookmarksComponent,
        data: {
          tags: [
            {
              name: 'title',
              content:
                'View your saved articles and videos. Share your favourite content with your gaming community!',
            },
          ],
          title: 'My Saved Bookmarks - stc play',
        },
      },
      {
        path: 'my-bracket',
        component: MyBracketComponent,
        data: {
          tags: [
            {
              name: 'title',
              content:
                'Organize your own tournaments today from the games that you love! Create brackets for the tournaments created on stcplay.gg.',
            },
          ],
          title: 'Organize your own tournaments today on stc play',
        },
      },
      { path: 'my-tournament/created', component: MyTournamentComponent },
      { path: 'my-tournament/joined', component: MyTournamentComponent },
      {
        path: 'my-transactions',
        component: MyTransactionsComponent,
        data: {
          tags: [
            {
              name: 'title',
              content:
                'View past transaction and earned rewards from your activities on stcplay.gg. Organize and join tournaments, purchase game vouchers and get rewarded ',
            },
          ],
          title: 'View Past Transactions & Rewards',
        },
      },
      {
        path: 'app-referral',
        component: AppReferralComponent,
        data: {
          tags: [
            {
              name: 'title',
              content: 'Get the list of invitees ',
            },
          ],
          title: 'Get the list of invitees',
        },
      },
      {
        path: 'profile-setting',
        component: ProfileSettingComponent,
        data: {
          tags: [
            {
              name: 'title',
              content:
                'Change and update your account settings to your preference.',
            },
          ],
          title: 'My Account Settings - stc play',
        },
      },
      {
        path: 'inbox/:id',
        component: InboxMessageComponent,
        data: {
          tags: [
            {
              name: 'title',
              content:
                'Check your inbox for new announcements and updates from stcplay.gg administrators and tournament organizers.',
            },
          ],
          title: 'My Inbox -  stc play',
        },
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProfileRoutingModule {}
