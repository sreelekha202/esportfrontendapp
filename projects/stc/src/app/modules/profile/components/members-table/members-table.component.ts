import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-members-table',
  templateUrl: './members-table.component.html',
  styleUrls: ['./members-table.component.scss'],
})
export class MembersTableComponent implements OnInit {
  @Input() data = [];

  constructor() {}

  ngOnInit(): void {}
}
