import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import {
  EsportsUserService,
  EsportsToastService,
  EsportsGtmService,
  SuperProperties,
  IUser,
} from 'esports';
import { Subscription } from 'rxjs';
import { environment } from '../../../../../environments/environment';

import {
  AppHtmlRoutes,
  AppHtmlProfileRoutes,
  AppHtmlAdminRoutes,
} from '../../../../app-routing.model';

const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;

interface TeamCardInvite {
  _id: number | string;
  teamName: string;
  logo: string;
  teamId: {
    teamName: string;
    logo: string;
  };
}
enum InviteControl {
  view = 'view',
  active = 'active',
  rejected = 'rejected',
}

@Component({
  selector: 'app-team-card-invite',
  templateUrl: './team-card-invite.component.html',
  styleUrls: ['./team-card-invite.component.scss'],
})
export class TeamCardInviteComponent implements OnInit, OnDestroy {
  @Input() data: TeamCardInvite;

  AppHtmlRoutes = AppHtmlRoutes;
  AppHtmlProfileRoutes = AppHtmlProfileRoutes;
  AppHtmlAdminRoutes = AppHtmlAdminRoutes;
  user: IUser;
  userSubscription: Subscription;

  constructor(
    private userService: EsportsUserService,
    private router: Router,
    private gtmService: EsportsGtmService,
    public eSportsToastService: EsportsToastService
  ) {}

  ngOnInit(): void {
    this.getCurrentUserDetails();
  }

  async updateInvitees(teamId, status) {

    this.controlClicked(status);

    await this.userService.update_invitees(API, { teamId, status }).subscribe(
      (data) => {
        this.eSportsToastService.showSuccess(data?.message);
        this.router
          .navigateByUrl('/profile/basic-info', { skipLocationChange: true })
          .then(() => {
            this.router.navigate(['/profile/teams']);
          });
      },
      (error) => {}
    );
  }

  ngOnDestroy() {
    this.userSubscription?.unsubscribe();
  }

  view(data) {
    this.controlClicked(InviteControl.view)
    this.router.navigate([AppHtmlProfileRoutes.teamsView, data._id]);
  }

  getCurrentUserDetails() {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.user = data;
      }
    });
  }

  controlClicked(event: InviteControl) {
    switch (event) {
      case InviteControl.view:
        this.pushGTMTags('View_Team_Invitation');
        break;
      case InviteControl.active:
        this.pushGTMTags('Accept_Team_Invitation');
        break;
      case InviteControl.rejected:
        this.pushGTMTags('Reject_Team_Invitation');
        break;
      default:
        break;
    }
  }

  pushGTMTags(eventName: string) {
    let superProperties: SuperProperties = {};
    if (this.user) {
      superProperties = this.gtmService.assignLoggedInUsedData(this.user);
    }
    this.gtmService.gtmEventWithSuperProp({
      eventName: eventName,
      superProps: superProperties,
    });
  }
}
