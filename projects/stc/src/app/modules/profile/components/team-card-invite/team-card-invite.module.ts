import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TeamCardInviteComponent } from './team-card-invite.component';
import { I18nModule } from 'esports';
import { environment } from '../../../../../environments/environment';

@NgModule({
  declarations: [TeamCardInviteComponent],
  imports: [CommonModule, I18nModule.forRoot(environment)],
  exports: [TeamCardInviteComponent],
})
export class TeamCardInviteModule {}
