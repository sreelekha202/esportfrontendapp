import { Router } from '@angular/router';
import {
  EsportsUserService,
  EsportsToastService,
  IUser,
  EsportsGtmService,
  SuperProperties,
} from 'esports';
import { Component, OnInit, Input, Inject, PLATFORM_ID, OnDestroy } from '@angular/core';
import {
  AppHtmlRoutes,
  AppHtmlProfileRoutes,
  AppHtmlAdminRoutes,
} from '../../../../app-routing.model';

import { environment } from '../../../../../environments/environment';
import {
  InfoPopupComponent,
  InfoPopupComponentData,
  InfoPopupComponentType,
} from '../../../../shared/popups/info-popup/info-popup.component';
import { TranslateService } from '@ngx-translate/core';
import { MatDialog } from '@angular/material/dialog';
import { isPlatformBrowser } from '@angular/common';
import { Subscription } from 'rxjs';
const API = environment.apiEndPoint;

interface TeamCardControl {
  _id: number | string;
  teamName: string;
  logo: string;
  slug: string;
  detail: any;
}

enum TeamControl {
  view = 'view',
  manage = 'manage',
  edit = 'edit',
  delete = 'delete',
}
@Component({
  selector: 'app-team-card-control',
  templateUrl: './team-card-control.component.html',
  styleUrls: ['./team-card-control.component.scss'],
})
export class TeamCardControlComponent implements OnInit, OnDestroy {
  @Input() data: TeamCardControl;

  AppHtmlRoutes = AppHtmlRoutes;
  AppHtmlProfileRoutes = AppHtmlProfileRoutes;
  AppHtmlAdminRoutes = AppHtmlAdminRoutes;
  user: IUser;
  isBrowser: boolean;
  teamControl = TeamControl;
  userSubscription: Subscription;

  constructor(
    private userService: EsportsUserService,
    public eSportsToastService: EsportsToastService,
    private router: Router,
    public translate: TranslateService,
    public dialog: MatDialog,
    private gtmService: EsportsGtmService,
    @Inject(PLATFORM_ID) private platformId
  ) {
    this.isBrowser = isPlatformBrowser(platformId);
  }

  ngOnInit(): void {
    if (this.isBrowser) {
      this.getCurrentUserDetails();
    }
  }

  ngOnDestroy() {
    this.userSubscription?.unsubscribe();
  }

  deleteTeam(id) {
    const teamObj = {
      id: this.data._id,
      admin: true,
      query: {
        condition: { _id: this.data._id, status: 'active' },
        update: { status: 'inactive' },
      },
    };
    this.userService.update_team_admin(API, teamObj).subscribe(
      (res) => {
        this.eSportsToastService.showSuccess(res.message);
        this.router
          .navigateByUrl('/home', { skipLocationChange: true })
          .then(() => {
            this.router.navigate(['/profile/teams']);
          });
      },
      (err) => {
        this.eSportsToastService.showError(err.error.message);
      }
    );
  }

  getCurrentUserDetails() {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.user = data;
      }
    });
  }

  leaveTeam() {
    this.userService
      .update_member(API, {
        teamId: this.data._id,
        userId: this.user._id,
        name: this.user.fullName,
        status: 'deleted',
      })
      .subscribe(
        (data) => {
          this.eSportsToastService.showSuccess(data?.message);
          window.location.reload();
        },
        (error) => {}
      );
  }

  checkMemberTournamentStatus() {
    this.userService
      .getTournamentByTeamId(API, this.data._id)
      .subscribe((res: any) => {
        if (res?.data?.docs.length) {
          this.eSportsToastService.showError(
            this.translate.instant('PROFILE.TEAMS.ERRORS.ERROR11')
          );
        } else {
          this.onRemoveUser(this.data._id);
        }
      });
  }

  onRemoveUser(_id) {
    const confirmMessage = `${this.translate.instant(
      'API.AM.MODAL.REMOVE_TEAM_MODAL.TEXT'
    )} ${this.data.teamName}?`;
    const blockUserData: InfoPopupComponentData = {
      title: this.translate.instant('API.AM.MODAL.REMOVE_TEAM_MODAL.TITLE'),
      text: confirmMessage,
      type: InfoPopupComponentType.confirm,
      btnText: this.translate.instant(
        'API.AM.MODAL.REMOVE_TEAM_MODAL.BUTTONTEXT'
      ),
    };
    const dialogRef = this.dialog.open(InfoPopupComponent, {
      data: blockUserData,
    });

    dialogRef.afterClosed().subscribe((confirmed) => {
      if (confirmed) {
        this.userService
          .update_member(API, {
            teamId: this.data._id,
            userId: this.user._id,
            name: this.user.fullName,
            status: 'deleted',
          })
          .subscribe(
            (data) => {
              this.eSportsToastService.showSuccess(data?.message);
              window.location.reload();
            },
            (error) => {}
          );
      }
    });
  }

  controlClicked(event: TeamControl) {
    switch (event) {
      case TeamControl.view:
        this.pushGTMTags('View_Team');
        break;
      case TeamControl.edit:
        this.pushGTMTags('View_Edit_Team');
        break;
      case TeamControl.manage:
        this.pushGTMTags('View_Manage_Team');
        break;
      case TeamControl.delete:
        this.pushGTMTags('Delete Team');
        break;
      default:
        break;
    }
  }

  pushGTMTags(eventName: string) {
    let superProperties: SuperProperties = {};
    if (this.user) {
      superProperties = this.gtmService.assignLoggedInUsedData(this.user);
    }
    this.gtmService.gtmEventWithSuperProp({
      eventName: eventName,
      superProps: superProperties,
    });
  }
}
