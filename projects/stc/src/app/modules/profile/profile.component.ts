import { faBars, faTimes } from '@fortawesome/free-solid-svg-icons';
import {
  Component,
  OnDestroy,
  OnInit,
  Inject,
  PLATFORM_ID,
} from '@angular/core';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { environment } from '../../../environments/environment';
import { AppHtmlProfileRoutes } from '../../app-routing.model';
import {
  IUser,
  EsportsUserService,
  EsportsLanguageService,
  EsportsConstantsService,
  GlobalUtils,
  EsportsGtmService,
  SuperProperties,
  EventProperties,
} from 'esports';
import { TranslateService } from '@ngx-translate/core';
import { isPlatformBrowser } from '@angular/common';
@AutoUnsubscribe()
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
})
export class ProfileComponent implements OnInit, OnDestroy {
  faBars = faBars;
  faTimes = faTimes;
  mobMenuOpened = false;
  isBrowser: boolean;
  navLinks = [
    {
      title: 'PROFILE.BASIC_INFO.TITLE',
      url: AppHtmlProfileRoutes.basicInfo,
      gtmEvent: 'View_Profile',
    },
    {
      title: 'PROFILE.MY_STATISTICS.MY_STATISTICS',
      url: AppHtmlProfileRoutes.myStats,
      gtmEvent: 'View_User_Statistics',
    },
    {
      title: 'PROFILE.TEAMS.TITLE',
      url: AppHtmlProfileRoutes.teams,
      gtmEvent: 'View_User_Teams',
    },
    {
      title: 'PROFILE.PROFILE_SETTING.TITLE',
      url: AppHtmlProfileRoutes.setting,
      // gtmEvent: 'View_User_Preferences',
    },
    // {
    //   title: 'PROFILE.BRACKETS.TITLE',
    //   url: AppHtmlProfileRoutes.myBracket,
    // },
    {
      title: 'PROFILE.TOURNAMENTS.TITLE',
      url: '',
      dropdown: [
        {
          title: 'PROFILE.TOURNAMENTS.SUB_HEADER1',
          url: AppHtmlProfileRoutes.myTournamentJoined,
          gtmEvent: 'View_User_Joined_Tournaments',
        },
        {
          title: 'PROFILE.TOURNAMENTS.SUB_HEADER2',
          url: AppHtmlProfileRoutes.myTournamentCreated,
          gtmEvent: 'View_User_Created_Tournaments',
        },
      ],
    },
    {
      title: 'PROFILE.TRANSACTIONS.TITLE',
      url: AppHtmlProfileRoutes.myTransactions,
      gtmEvent: 'View_User_Transactions',
    },
    {
      title: 'PROFILE.BOOKMARKS.TITLE',
      url: AppHtmlProfileRoutes.myBookmarks,
      gtmEvent: 'View_User_Bookmarks',
    },
    {
      title: 'PROFILE.INBOX.TITLE',
      url: AppHtmlProfileRoutes.inbox,
      gtmEvent: 'View_User_Inbox',
    },
    {
      title: 'PROFILE.REFERRAL.TITLE',
      url: AppHtmlProfileRoutes.AppReferralComponent,
      gtmEvent: 'View_User_Referrals',
    },
  ];
  isTournamentPage = false;

  private sub1: Subscription;
  userSubscription: Subscription;
  currentUser: IUser;

  constructor(
    public router: Router,
    public location: Location,
    private userService: EsportsUserService,
    private languageService: EsportsLanguageService,
    private ConstantsService: EsportsConstantsService,
    private gtmService: EsportsGtmService,
    private translateService: TranslateService,
    @Inject(PLATFORM_ID) private platformId
  ) {
    this.isBrowser = isPlatformBrowser(platformId);
    if (
      GlobalUtils.isBrowser() &&
      !localStorage.getItem(environment.currentToken)
    ) {
      this.router.navigate(['/login']);
    }
    this.sub1 = this.router.events.subscribe(() => {
      this.checkTournamentPage();
    });
  }

  ngOnInit(): void {
    if (this.isBrowser) {
      this.languageService.language.subscribe((lang) =>
        this.translateService.use(
          lang || this.ConstantsService?.defaultLangCode
        )
      );
      this.getCurrentUserDetails();
    }
  }

  getCurrentUserDetails() {
    this.userSubscription = this.userService.currentUser.subscribe(
      (data: any) => {
        if (data) {
          this.currentUser = data;
          if (data.isAuthor == 0 || data.isInfluencer == 0) {
            const isContentExist = this.navLinks.find(
              (nav) => nav.title === 'PROFILE.CONTENTS.TITLE'
            );
            if (!isContentExist) {
              this.navLinks.splice(2, 0, {
                title: 'PROFILE.CONTENTS.TITLE',
                url: AppHtmlProfileRoutes.myContent,
                gtmEvent: null,
              });
            }
          }
        }
      }
    );
  }

  ngOnDestroy() {
    this.userSubscription?.unsubscribe();
  }

  private checkTournamentPage(): void {
    this.isTournamentPage = !!this.location
      .path()
      .match('/profile/my-tournament/');
  }

  pushGTMTags(eventName: string) {
    if (!eventName) {
      return;
    }
    let superProperties: SuperProperties = {};
    if (this.currentUser) {
      superProperties = this.gtmService.assignLoggedInUsedData(
        this.currentUser
      );
    }
    let eventProperties: EventProperties = {};
    if (eventName == 'View_Profile') {
      eventProperties['fromPage'] = this.router.url;
    }
    this.gtmService.gtmEventWithSuperProp({
      eventName: eventName,
      superProps: superProperties,
      eventProps: eventProperties,
    });
  }
}
