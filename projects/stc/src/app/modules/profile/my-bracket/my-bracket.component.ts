import { MatDialog } from '@angular/material/dialog';
import { Component, OnInit } from '@angular/core';

import {
  InfoPopupComponent,
  InfoPopupComponentData,
  InfoPopupComponentType,
} from '../../../shared/popups/info-popup/info-popup.component';
import { BracketService } from '../../../core/service';
import { TranslateService } from '@ngx-translate/core';
import { EsportsToastService, IPagination } from 'esports';
@Component({
  selector: 'app-my-bracket',
  templateUrl: './my-bracket.component.html',
  styleUrls: ['./my-bracket.component.scss'],
})
export class MyBracketComponent implements OnInit {
  bracketData;
  paginationData = {
    page: 1,
    limit: 4,
    sort: '-updatedOn',
    projection: [
      '_id',
      'name',
      'bracketType',
      'updatedOn',
      'maximumParticipants',
    ],
  };
  page: IPagination;

  constructor(
    private bracketService: BracketService,
    private eSportsToastService: EsportsToastService,
    private matDialog: MatDialog,
    private translateService: TranslateService
  ) {}

  ngOnInit(): void {
    this.fetchAllBracket();
  }

  fetchAllBracket = async () => {
    try {
      const query = `?pagination=${encodeURIComponent(
        JSON.stringify(this.paginationData)
      )}`;
      this.bracketData = await this.bracketService.fetchAllBracket(query);
      this.page = {
        totalItems: this.bracketData?.data?.totalDocs,
        itemsPerPage: this.bracketData?.data?.limit,
        maxSize: 5,
      };
    } catch (error) {
      this.eSportsToastService.showError(error?.error?.message || error?.message);
    }
  };

  pageChanged(page): void {
    this.paginationData.page = page;
    this.fetchAllBracket();
  }

  onRemoveBracket(bracket) {
    const blockUserData: InfoPopupComponentData = {
      title: this.translateService.instant('PROFILE.BRACKETS.POP_UP.TITLE'),
      text: `${this.translateService.instant('PROFILE.BRACKETS.POP_UP.BODY')}`,
      type: InfoPopupComponentType.confirm,
      btnText: `${this.translateService.instant('BUTTON.CONFIRM')}`,
    };
    const dialogRef = this.matDialog.open(InfoPopupComponent, {
      data: blockUserData,
    });

    dialogRef.afterClosed().subscribe(async (confirmed) => {
      if (confirmed) {
        try {
          const response = await this.bracketService.deleteBracket(
            bracket?._id
          );
          this.eSportsToastService.showSuccess(
            this.translateService.instant(response?.message)
          );
          this.fetchAllBracket();
        } catch (error) {
          this.eSportsToastService.showError(error?.error?.message || error?.message);
        }
      }
    });
  }
}
