import { Component, OnInit, Inject, PLATFORM_ID } from '@angular/core';
import { faTrash } from '@fortawesome/free-solid-svg-icons';
import { isPlatformBrowser } from '@angular/common';
import { MessageService } from '../../../core/service';
import { IMessage } from '../../../shared/models';
import {
  EsportsGtmService,
  EsportsUserService,
  SuperProperties,
} from 'esports';

@Component({
  selector: 'app-inbox',
  templateUrl: './inbox.component.html',
  styleUrls: ['./inbox.component.scss'],
})
export class InboxComponent implements OnInit {
  faTrash = faTrash;
  inboxMessages: Array<IMessage> = [];
  isBrowser: boolean;
  constructor(
    private messageService: MessageService,
    private userService: EsportsUserService,
    private gtmService: EsportsGtmService,
    @Inject(PLATFORM_ID) private platformId
  ) {
    this.isBrowser = isPlatformBrowser(platformId);
  }

  ngOnInit(): void {
    if (this.isBrowser) {
      this.getInboxMessages();
    }
  }

  onSelectAll(): void {
    if (this.inboxMessages.every((item) => item.isChecked)) {
      this.inboxMessages.forEach((item) => (item.isChecked = false));
    } else {
      this.inboxMessages.forEach((item) => (item.isChecked = true));
    }
  }

  onDeleteChecked(): void {
    const selectedMessages = this.inboxMessages.filter(
      (item) => item.isChecked
    );
    const selectedMessagesId = selectedMessages.map((item) => item._id);
    this.messageService
      .deleteMultipleMessage(selectedMessagesId)
      .subscribe((response) => {
        this.pushGTMTags('Delete_Inbox_Messages');
        this.getInboxMessages();
      });
    this.inboxMessages = this.inboxMessages.filter((item) => !item.isChecked);
  }

  onDeleteOne(index: number, messageId: string): void {
    this.inboxMessages.splice(index, 1);
    this.messageService
      .deleteMultipleMessage([messageId])
      .subscribe((response) => {
        this.pushGTMTags('Delete_Inbox_Messages');
        this.getInboxMessages();
      });
  }

  getInboxMessages(): void {
    this.messageService.getInboxMessages().subscribe(
      (response) => {
        this.inboxMessages = response.data;
      },
      (error) => {
        console.error(error);
      }
    );
  }

  viewMessageGTM() {
    this.pushGTMTags('View_Inbox_Messages');
  }

  pushGTMTags(eventName: string) {
    let superProperties: SuperProperties = {};
    if (this.userService.currentUserSubject.value) {
      superProperties = this.gtmService.assignLoggedInUsedData(
        this.userService.currentUserSubject.value
      );
    }
    this.gtmService.gtmEventWithSuperProp({
      eventName: eventName,
      superProps: superProperties,
    });
  }
}
