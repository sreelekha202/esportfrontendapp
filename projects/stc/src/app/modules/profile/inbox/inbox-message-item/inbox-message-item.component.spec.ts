import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InboxMessageItemComponent } from './inbox-message-item.component';

describe('InboxMessageComponent', () => {
  let component: InboxMessageItemComponent;
  let fixture: ComponentFixture<InboxMessageItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [InboxMessageItemComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InboxMessageItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
