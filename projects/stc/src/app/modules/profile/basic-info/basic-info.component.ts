import {
  Component,
  OnInit,
  ViewChild,
  Inject,
  TemplateRef,
  OnDestroy,
  AfterViewInit,
} from '@angular/core';
import { NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { DOCUMENT, LowerCasePipe } from '@angular/common';
import { environment } from '../../../../environments/environment';
import {
  IUser,
  EsportsUserService,
  EsportsLanguageService,
  EsportsConstantsService,
  GlobalUtils,
  EsportsToastService,
  EsportsGtmService,
  SuperProperties,
} from 'esports';
import { AuthServices, GameService } from '../../../core/service';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';
import { NgForm, Validators } from '@angular/forms';

const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;

declare var $: any;

const userType = new Map([
  [0, 'PROFILE.BASIC_INFO.TITLE'],
  [1, 'PROFILE.BASIC_INFO.GAME_CENTER'],
  [2, 'PROFILE.BASIC_INFO.SPONSER'],
  [3, 'PROFILE.BASIC_INFO.ORGANIZER'],
]);

@Component({
  selector: 'app-basic-info',
  templateUrl: './basic-info.component.html',
  styleUrls: ['./basic-info.component.scss'],
})
export class BasicInfoComponent implements OnInit, OnDestroy, AfterViewInit {
  dropdown: any;
  txtValue: any;
  currentUser: IUser;
  user: IUser;
  isDisabled = false;
  hideDropdown: any;
  userTypeDetail;
  countries = [];
  states = [];
  real_todayDate = new Date();
  startDate = new Date(1990, 0, 1);
  todayDate: Date = new Date(
    new Date().setFullYear(new Date().getFullYear() - 10)
  );
  currentLang = this.ConstantsService?.defaultLangCode;
  title = '';
  @ViewChild('content')
  private content: TemplateRef<any>;
  public base64textString;

  userSubscription: Subscription;

  gameList = [];
  selectedGame = [];

  isExist = false;
  timeoutId = null;

  f: NgForm; // f is nothing but the template reference of the Template Driven Form
  @ViewChild('f') currentForm: NgForm;
  userAccType: any;

  constructor(
    @Inject(DOCUMENT) private document: Document,
    private userService: EsportsUserService,
    private router: Router,
    public eSportsToastService: EsportsToastService,
    private modalService: NgbModal,
    config: NgbModalConfig,
    public language: EsportsLanguageService,
    public ConstantsService: EsportsConstantsService,
    public translate: TranslateService,
    public gameService: GameService,
    private authServices: AuthServices,
    private gtmService: EsportsGtmService,
  ) {
    config.backdrop = 'static';
    config.keyboard = false;
    config.centered = true;
    config.backdropClass = 'dark-modal';
  }

  ngOnInit(): void {
    this.fetchCountries();
    this.getPaymentAccountStatus();
    // this.userService.getAllCountries().subscribe((data) => {
    //   this.countries = data.countries;
    //   this.updateBootstrapSelect();
    // });
    if (
      GlobalUtils.isBrowser() &&
      localStorage.getItem(environment.currentToken)
    ) {
      this.getUserData();
    } else {
      this.router.navigate(['/']);
    }
    this.language.language.subscribe((lang) => {
      this.currentLang = lang;
      this.updateBootstrapSelect();
    });
    // this.getGames();
  }

  filterFunction() {
    var input, filter, a, i;
    input = document.getElementById('myInput');
    filter = input.value.toUpperCase();
    this.dropdown = document.getElementById('myDropdown');
    a = this.dropdown.getElementsByTagName('a');
    for (i = 0; i < a.length; i++) {
      this.txtValue = a[i].textContent || a[i].innerText;
      if (this.txtValue.toUpperCase().indexOf(filter) > -1) {
        a[i].style.display = '';
      } else {
        a[i].style.display = 'none';
      }
    }
  }

  getValue(value, i) {
    this.gameList.splice(i, 1);
    this.selectedGame.push(value);
    let game = {
      _id: value._id,
      name: value.name,
      userGameId: '',
    };
    this.user?.preference.gameDetails.push(game);
    this.hideDropdown = false;
  }
  removeGame(i, game) {
    this.gameList.splice(i, 0, game);
    this.selectedGame.splice(i, 1);
    this.user.preference.gameDetails.splice(i, 1);
  }

  fetchCountries = async () => {
    try {
      const data = await this.userService.getAllCountries().toPromise();
      this.countries = data.countries.filter((item) =>
        [191, 117, 17].includes(item.id)
      );
      this.updateBootstrapSelect();
    } catch (error) {
      this.eSportsToastService.showError(error?.message);
    }
  };

  ngOnDestroy() {
    if (this.user?.firstLogin == 1) {
      const data = {
        firstLogin: 0,
      };
      const res = this.userService.updateProfile(API, data).subscribe(
        () => {
          //this.userService.refreshCurrentUser();
        },
        (error) => {
          return false;
        }
      );
    }
    this.userSubscription?.unsubscribe();
  }

  isFullName() {
    this.currentForm.controls['fullName'].setValidators([
      Validators.required,
      Validators.minLength(3),
      Validators.maxLength(100),
      Validators.pattern(
        /^[a-zA-Z0-9\u0600-\u06FF,!@#$&()\\-`.+,/\"][\sa-zA-Z0-9\u0600-\u06FF,!@#$*%&()\\-`.+,/\"]*$/
      ),
    ]);
    this.currentForm.controls['fullName'].updateValueAndValidity();
  }

  ngAfterViewInit() {
    if (this.user && this.user.firstLogin === 1) {
      //this.openModal();
    }
  }

  getUserData() {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        if (data.preference && data.preference.gameDetails) {
          this.selectedGame = [...data.preference.gameDetails];
        }
        this.getGames(this.selectedGame);
        this.userTypeDetail = Object.assign({}, data.userTypeDetail);

        this.title = userType.get(this.userTypeDetail.userAccTypeNo || 0);
        this.userAccType = this.userTypeDetail?.userAccTypeNo;
        this.currentUser = data;
        this.user = Object.assign({}, this.currentUser);
        this.updateBootstrapSelect();
        this.onChangeCountry(this.user.country);
      } else {
        // const newVal = this.userService.getAuthenticatedUser();
      }
    });
  }

  openModal() {
    this.modalService.open(this.content, {
      size: 'md',
      centered: true,
      // scrollable: true,
      windowClass: 'preference-modal-content',
    });
  }

  onToggleEdit() {
    this.isDisabled = !this.isDisabled;
    this.updateBootstrapSelect();
  }

  cancel() {
    this.user = Object.assign({}, this.currentUser);
    this.base64textString = '';
    this.isDisabled = true;
    this.onChangeCountry(this.user.country);
    this.updateBootstrapSelect();
    this.removeGameWithEmptyUserGameId();
  }

  //convert image to binary
  handleFileSelect(evt) {
    let files = evt.target.files;
    let file = files[0];

    if (files && file) {
      let reader = new FileReader();

      reader.onload = this._handleReaderLoaded.bind(this);

      reader.readAsBinaryString(file);
    }
  }
  _handleReaderLoaded(readerEvt) {
    let binaryString = readerEvt.target.result;
    this.base64textString = 'data:image/png;base64,' + btoa(binaryString);
  }

  isUniqueName = async (name) => {
    try {
      const isAvailable = async () => {
        try {
          const response = await this.authServices
            .searchUsername(name, this.user._id)
            .toPromise();
          this.isExist = response.data?.isExist;
          if (this.isExist) {
            this.currentForm.form.controls['username'].setErrors({
              incorrect: true,
            });
          } else {
            this.currentForm.controls['username'].setErrors(null);
            this.currentForm.controls['username'].setValidators([
              Validators.required,
              Validators.minLength(3),
              Validators.pattern(/^[a-zA-Z0-9_]+$/),
            ]);
            this.currentForm.controls['username'].updateValueAndValidity();
          }
        } catch (error) {
          throw error;
        } finally {
          clearTimeout(this.timeoutId);
        }
      };
      if (!this.timeoutId) {
        this.timeoutId = setTimeout(isAvailable, 500);
      } else {
        clearTimeout(this.timeoutId);
        this.timeoutId = setTimeout(isAvailable, 500);
      }
    } catch (error) {
      this.eSportsToastService.showError(
        error?.error?.message || error?.message
      );
    }
  };

  saveProfile() {
    this.pushGTMTags('Update_Profile_Completed');

    try {
      if (!this.userAccType) {
        this.currentForm.controls['username'].setValidators([
          Validators.required,
          Validators.minLength(3),
          Validators.pattern(/^[a-zA-Z0-9_]+$/),
        ]);
        this.currentForm.controls['fullName'].setValidators([
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(100),
          Validators.pattern(
            /^[a-zA-Z0-9\u0600-\u06FF,!@#$&()\\-`.+,/\"][\sa-zA-Z0-9\u0600-\u06FF,!@#$*%&()\\-`.+,/\"]*$/
          ),
        ]);

        this.currentForm.controls['fullName'].updateValueAndValidity();
        this.currentForm.controls['username'].updateValueAndValidity();
        if (this.currentForm.invalid) {
          let error = [];

          if (this.currentForm.controls['username']?.errors?.required) {
            error.push(
              this.translate.instant('PROFILE.BASIC_INFO.FORM.NICKNAME.ERROR')
            );
          }
          if (this.currentForm.controls['dob']?.errors?.required) {
            error.push(
              this.translate.instant('PROFILE.BASIC_INFO.FORM.DOB.ERROR')
            );
          }
          if (this.currentForm.controls['fullName']?.errors?.required) {
            error.push(
              this.translate.instant('PROFILE.BASIC_INFO.FORM.FULL_NAME.ERROR')
            );
          }
          if (
            this.currentForm.controls['username']?.errors?.minlength ||
            this.currentForm.controls['fullName']?.errors?.minlength
          ) {
            error.push(this.translate.instant('LOG_REG.LOGIN.VALIDATION3'));
          }
          if (this.currentForm.controls['username']?.errors?.pattern) {
            error.push(this.translate.instant('LOG_REG.LOGIN.VALIDATION5'));
          }
          if (this.currentForm.controls['fullName']?.errors?.maxlength) {
            error.push(this.translate.instant('LOG_REG.LOGIN.VALIDATION4'));
          }
          if (this.currentForm.controls['fullName']?.errors?.pattern) {
            error.push(this.translate.instant('LOG_REG.LOGIN.VALIDATION6'));
          }
          this.eSportsToastService.showError(error.join('<br/>'));
          return;
        }
      }
      this.removeGameWithEmptyUserGameId();
      this.user.preference.gameDetails.forEach(elem => {
      elem.userGameId = elem.userGameId.trim();
      });
      const data = {
        fullName: this.user.fullName,
        username: this.user.username,
        profilePicture: this.base64textString,
        martialStatus: this.user.martialStatus,
        parentalStatus: this.user.parentalStatus,
        dob: this.user.dob,
        country: this.user.country,
        state: this.user.state,
        shortBio: this.user.shortBio,
        gender: this.user.gender,
        profession: this.user.profession,
        postalCode: this.user.postalCode,
        updatedBy: 'user',
        userTypeDetail: this.userTypeDetail.hasOwnProperty('_id')
          ? this.userTypeDetail
          : null,
        gameDetails: this.user.preference.gameDetails,
      };
      this.userService.updateProfile(API, data).subscribe(
        (profileRes) => {
          this.userService.refreshCurrentUser(API, TOKEN);
          this.eSportsToastService.showSuccess(profileRes?.message);
          this.router
            .navigateByUrl('/home', { skipLocationChange: true })
            .then(() => {
              this.router.navigate(['/profile/basic-info']);
            });
        },
        (error) => {
          this.eSportsToastService.showError(error);
          return false;
        }
      );
    } catch (err) {
      this.eSportsToastService.showError(err);
    }
  }

  private updateBootstrapSelect(): void {
    $('.selectpicker').selectpicker({
      noneSelectedText: this.translate.instant('NOTHING_SELECTED_TITLE'),
    });

    setTimeout(() => {
      // update bootstrap select
      $('.selectpicker').selectpicker('refresh');
      // $('.selectpicker').selectpicker({
      //   noneSelectedText : "PROFILE.BASIC_INFO.VALUE"
      //   // by this default 'Nothing selected' -->will change to Please Select
      // });
    });
  }
  getGames(existingGame) {
    const encodeUrl = `?query=${encodeURIComponent(
      JSON.stringify({ isTournamentAllowed: true })
    )}`;
    this.gameService.getAllGames(encodeUrl).subscribe(
      (res) => {
        if (res && res.data) {
          this.gameList = res.data.filter(function (obj) {
            return !this.has(obj._id);
          }, new Set(existingGame.map((obj) => obj._id)));
        }
      },
      (err) => {}
    );
  }

  sortByKey(array, key) {
    return array.sort(function (a, b) {
      var x = a[key];
      var y = b[key];
      return x < y ? -1 : x > y ? 1 : 0;
    });
  }
  onChangeCountry(countryValue) {
    if (countryValue !== this.currentUser.country) {
      this.user.state = '';
    }
    this.states.length = 0;
    let countriesList = [];
    const that = this;
    this.userService.getAllCountries().subscribe((data) => {
      countriesList = data.countries;

      const index = countriesList.findIndex((x) => x.name === countryValue);
      that.userService.getStates().subscribe((data) => {
        data.states.forEach(function (value) {
          if (value.country_id == countriesList[index]?.id) {
            that.states.push(value);
          }
        });
        if (this.translate.currentLang == 'ar') {
          that.states = this.sortByKey(that.states, 'ar');
        }
        that.updateBootstrapSelect();
      });
    });
  }
  copyToClipboard(text) {
    const dummyElement = this.document.createElement('textarea');
    this.document.body.appendChild(dummyElement);
    dummyElement.value = text;
    dummyElement.select();
    this.document.execCommand('copy');
    this.document.body.removeChild(dummyElement);
    this.eSportsToastService.showInfo('Copied');
  }

  getPaymentAccountStatus = async () => {
    try {
      const auth = await this.authServices.getPaymentAccountVerfiedStatus();
    } catch (error) {
      this.eSportsToastService.showError(
        error?.error?.message || error?.message
      );
    }
  };

  removeGameWithEmptyUserGameId() {
    this.user.preference.gameDetails = this.user.preference.gameDetails.filter(
      (game) => {
        const index = this.user.preference.gameDetails.indexOf(game._id);

        if (game.userGameId == '') {
          this.selectedGame.splice(index, 1);
        }

        return game.userGameId != '';
      }
    );
  }

  pushGTMTags(eventName: string) {
    if (!eventName) {
      return;
    }
    let superProperties: SuperProperties = {};
    if (this.currentUser) {
      superProperties = this.gtmService.assignLoggedInUsedData(
        this.currentUser
      );
    }

    this.gtmService.gtmEventWithSuperProp({
      eventName: eventName,
      superProps: superProperties,
    });
  }

  clickProfilePicture() {
    this.pushGTMTags('Profile_Image_Clicked');
  }

}
