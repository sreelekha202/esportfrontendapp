import { Component, OnDestroy, OnInit, EventEmitter } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TournamentService } from '../../../core/service';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';
import { MatTabChangeEvent } from '@angular/material/tabs';
import { debounceTime } from 'rxjs/operators';
import {
  EsportsUserService,
  EsportsToastService,
  IPagination,
  SuperProperties,
  EsportsGtmService,
  EventProperties,
  IUser
} from 'esports';
import { AppHtmlRoutes } from '../../../app-routing.model';

@Component({
  selector: 'app-my-tournament',
  templateUrl: './my-tournament.component.html',
  styleUrls: ['./my-tournament.component.scss'],
})
export class MyTournamentComponent implements OnInit, OnDestroy {
  AppHtmlRoutes = AppHtmlRoutes;
  tournamentList = [];
  type;
  tab = 0; //upcoming
  activeTabIndex = 0;
  userId;
  pagination = {
    page: 1,
    limit: 5,
  };
  cPagination;
  page: IPagination;
  isDraft = false;
  isHidden = false;
  user: IUser;
  userSubscription: Subscription;
  tournamentSubscription: Subscription;
  customTabEvent = new EventEmitter<MatTabChangeEvent>();

  constructor(
    private tournamentService: TournamentService,
    private router: Router,
    private userService: EsportsUserService,
    private eSportsToastService: EsportsToastService,
    private activatedRoute: ActivatedRoute,
    private gtmService: EsportsGtmService,
    private translateService: TranslateService
  ) {}

  ngOnInit(): void {
    this.type = this.activatedRoute.snapshot.routeConfig.path
      .split('/')
      .reverse()[0];
    this.getCurrentUserDetails();
    this.customTabEvent
      .pipe(debounceTime(30))
      .subscribe((tabChangeEvent: MatTabChangeEvent) => {
        this.attachTabChangeListener(tabChangeEvent);
      });

    this.activeTabIndex =
      +this.activatedRoute.snapshot.queryParams?.activeTab || 0;
    this.pushCreatedTournamentTags(this.activeTabIndex);
    this.cPagination = Object.assign({}, this.pagination);
    this.switchData(this.activeTabIndex);
  }

  ngOnDestroy() {
    this.userSubscription?.unsubscribe();
    this.tournamentSubscription?.unsubscribe();
  }

  showNotification() {
    if (this.activatedRoute.snapshot.queryParams.transaction) {
      if (this.activatedRoute.snapshot.queryParams.transaction == 'success') {
        this.eSportsToastService.showSuccess(
          this.translateService.instant(
            'PROFILE.TOURNAMENTS.PAYMENT.PAYMENT_SUCCESS'
          )
        );
      }
      if (this.activatedRoute.snapshot.queryParams.transaction == 'failed') {
        this.eSportsToastService.showError(
          this.translateService.instant(
            'PROFILE.TOURNAMENTS.PAYMENT.PAYMENT_FAILED'
          )
        );
      }
    }
  }
  getCurrentUserDetails() {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.userId = data._id;
        this.user = data;
        // this.cPagination = Object.assign({}, this.pagination);
        // this.fetchTournaments(this.type, this.tab);
        this.showNotification();
      }
    });
  }

  /**
   * Get tournament list
   */

  fetchTournaments = async (type, tab) => {
    try {
      this.type = type;
      this.tab = tab;
      this.isDraft = tab == 4;
      this.tournamentList = [];

      if (this.tournamentSubscription) {
        this.tournamentSubscription.unsubscribe();
      }

      let tournament;
      if (type === 'joined') {
        const payload = await this.getJoinedTournamentQuery(tab);
        this.isHidden = payload.isEdit;
        payload.isEdit ? delete payload.isEdit : payload;
        tournament = await this.tournamentService
          .getParticipantTournament(payload)
          .toPromise();
      } else if (type === 'created') {
        const payload = await this.getCreatedTournamentQuery(tab);
        this.isHidden = payload.isEdit;
        payload.isEdit ? delete payload.isEdit : payload;
        tournament = await this.tournamentService.fetchMyTournament(payload);
      }
      this.page = {
        totalItems: tournament?.data?.totalDocs,
        itemsPerPage: tournament?.data?.limit,
        maxSize: 5,
      };
      this.tournamentList = tournament?.data?.docs || [];
    } catch (error) {
      this.eSportsToastService.showError(
        error?.error?.message || error?.message
      );
    }
  };

  onTabChanged = (tabChangeEvent: MatTabChangeEvent): void => {
    this.customTabEvent.emit(tabChangeEvent);
  };

  attachTabChangeListener(tabChangeEvent: MatTabChangeEvent) {
    this.activeTabIndex = tabChangeEvent.index;
    this.router.navigate(['.'], {
      relativeTo: this.activatedRoute,
      queryParams: { activeTab: tabChangeEvent.index },
    });

    this.pushCreatedTournamentTags(tabChangeEvent.index);
    this.switchData(tabChangeEvent.index);
  }

  pushCreatedTournamentTags(index) {
    switch (index) {
      case 0:
        this.pushGTMTags('View_Upcoming_Tournament'); //upcoming
        break;
      case 1:
        this.pushGTMTags('View_Ongoing_Tournament'); //ongoing
        break;
      case 2:
        this.pushGTMTags('View_Past_Tournament'); //past
        break;
      case 3:
        this.pushGTMTags('View_Unpublished_Tournament'); //unpublished
        break;
      default:
        break;
    }
  }

  switchData(index: number) {
    switch (index) {
      case 0:
        this.fetchTournaments(this.type, 0); //upcoming
        break;
      case 1:
        this.fetchTournaments(this.type, 1); //ongoing
        break;
      case 2:
        this.fetchTournaments(this.type, 2); //past
        break;
      case 3:
        this.fetchTournaments(this.type, 5); //unpublished
        break;
      default:
        break;
    }
  }

  async getTournamentData(tabName) {
    this.resetPage();
    const payload = this.getCreatedTournamentQuery(tabName);
    const res = await this.tournamentService
      .getPaginatedTournaments(payload)
      .toPromise();
    this.tournamentList = res['data'].docs || [];
  }

  resetPage() {
    this.tournamentList = [];
  }

  pageChanged(page): void {
    this.cPagination.page = page;
    this.fetchTournaments(this.type, this.tab);
  }

  getCreatedTournamentQuery(tab) {
    switch (true) {
      case tab === 0: //upcoming
        return {
          page: this.cPagination.page,
          status: '0',
          limit: 5,
        };
      case tab === 1: //ongoing
        return {
          page: this.cPagination.page,
          status: '1',
          limit: 5,
          isEdit: true,
        };
      case tab === 2: //past
        return {
          page: this.cPagination.page,
          status: '2',
          limit: 5,
          isEdit: true,
        };
      case tab === 4: //draft
        return {
          page: this.cPagination.page,
          status: '4',
          limit: 5,
        };
      case tab === 5: //unpublished
        return {
          page: this.cPagination.page,
          status: '5',
          limit: 5,
        };
    }
  }

  getJoinedTournamentQuery(tab) {
    switch (true) {
      case tab === 0: //upcoming
        return {
          pagination: JSON.stringify(this.cPagination),
          type: 0,
        };
      case tab === 1: //ongoing
        return {
          pagination: JSON.stringify(this.cPagination),
          type: 1,
          isEdit: true,
        };
      case tab === 2: //past
        return {
          pagination: JSON.stringify(this.cPagination),
          type: 2,
          isEdit: true,
        };
    }
  }

  editTournamentGTM(){
    this.pushGTMTags('View_Edit_Tournament');
  }

  editParticipantInfoGTM() {
    this.pushGTMTags('View Edit Tournament Participant Info');
  }

  manageTournamentGTM() {
    this.pushGTMTags('Manage Tournaments Started');
  }

  pushGTMTags(eventName: string) {
    let superProperties: SuperProperties = {};
    if (this.user) {
      superProperties = this.gtmService.assignLoggedInUsedData(this.user);
    }
    let eventProperties: EventProperties = {};
    if (
      [
        'View_Upcoming_Tournament',
        'View_Ongoing_Tournament',
        'View_Past_Tournament',
        'View_Unpublished_Tournament',
      ].includes(eventName)
    ) {
      eventProperties.fromPage = this.router.url;
    }

    this.gtmService.gtmEventWithSuperProp({
      eventName: eventName,
      superProps: superProperties,
      eventProps: eventProperties
    });
  }
}
