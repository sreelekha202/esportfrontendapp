import {
  Component,
  OnDestroy,
  OnInit,
  Inject,
  PLATFORM_ID,
} from '@angular/core';
import { UserPreferenceService, UtilsService } from '../../../core/service';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';

import {
  EsportsUserService,
  EsportsLanguageService,
  EsportsToastService,
  EsportsGtmService,
  SuperProperties,
} from 'esports';
import { isPlatformBrowser } from '@angular/common';
import { MatTabChangeEvent } from '@angular/material/tabs';
enum MyBookmarksComponentTab {
  article = 'PROFILE.BOOKMARKS.TAB1',
  video = 'PROFILE.BOOKMARKS.TAB2',
}

@Component({
  selector: 'app-my-bookmarks',
  templateUrl: './my-bookmarks.component.html',
  styleUrls: ['./my-bookmarks.component.scss'],
})
export class MyBookmarksComponent implements OnInit, OnDestroy {
  MyBookmarksComponentTab = MyBookmarksComponentTab;
  userId;
  articleBookmarkList: Array<{}> = [];
  videoBookmarkList: Array<{}> = [];

  userSubscription: Subscription;
  isBrowser: boolean;
  constructor(
    private userPreferenceService: UserPreferenceService,
    private userService: EsportsUserService,
    private eSportsToastService: EsportsToastService,
    public translate: TranslateService,
    public language: EsportsLanguageService,
    public utilsService: UtilsService,
    public languageService: EsportsLanguageService,
    private gtmService: EsportsGtmService,
    @Inject(PLATFORM_ID) private platformId
  ) {
    this.isBrowser = isPlatformBrowser(platformId);
  }

  ngOnInit(): void {
    if (this.isBrowser) {
      this.getCurrentUserDetails();
      this.pushGTMTags('View_Article_Bookmarks');
    }
  }

  ngOnDestroy() {
    this.userSubscription?.unsubscribe();
  }

  getBookmarks = async () => {
    try {
      const filter = JSON.stringify({ userId: this.userId });
      const projection = 'articleBookmarks,videoLibrary';
      const userPreference = await this.userPreferenceService
        .getPreferences(filter, projection)
        .toPromise();
      this.articleBookmarkList =
        userPreference?.data[0]?.articleBookmarks || [];
      this.videoBookmarkList = userPreference?.data[0]?.videoLibrary || [];
    } catch (error) {
      this.eSportsToastService.showError(
        error?.error?.message || error?.message
      );
    }
  };

  getCurrentUserDetails() {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.userId = data._id;
        this.getBookmarks();
      }
    });
  }

  deleteBookmark = async (id, idType) => {
    try {
      if (!this.userId) {
        this.eSportsToastService.showError('Session Expired Please Login');
        return;
      }
      const queryParam = `${idType}=${id}`;
      const userPreference = await this.userPreferenceService
        .removeBookmark(queryParam)
        .toPromise();
      this.eSportsToastService.showSuccess(userPreference?.message);
      this.getBookmarks();
      this.pushDeleteBookmarkGTM(idType);

    } catch (error) {
      this.eSportsToastService.showError(
        error?.error?.message || error?.message
      );
    }
  };

  beforeTabChange(event) {
    const navtab = event.nextId;
  }

  pushDeleteBookmarkGTM(idType) {

    if (idType == 'articleId') {
      this.pushGTMTags('Delete_Article_Bookmark');
    } else {
      this.pushGTMTags('Delete_Video_Bookmark');
    }

  }

  tabChanged(matTabChanged: MatTabChangeEvent) {
    switch (matTabChanged.index) {
      case 0:
        this.pushGTMTags('View_Article_Bookmarks');
        break;
      case 1:
        this.pushGTMTags('View_Video_Bookmarks');
        break;
      default:
        break;
    }
  }

  pushGTMTags(eventName: string) {
    let superProperties: SuperProperties = {};
    if (this.userService.currentUserSubject.value) {
      superProperties = this.gtmService.assignLoggedInUsedData(
        this.userService.currentUserSubject.value
      );
    }
    this.gtmService.gtmEventWithSuperProp({
      eventName: eventName,
      superProps: superProperties,
    });
  }
}
