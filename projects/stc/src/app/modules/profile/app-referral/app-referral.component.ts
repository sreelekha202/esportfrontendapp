import {
  Component,
  OnInit,
  Inject,
  HostListener,
  OnDestroy,
} from '@angular/core';
import { TransactionService } from '../../../core/service';
import { DatePipe } from '@angular/common';
import { TranslateService } from '@ngx-translate/core';
import { DOCUMENT } from '@angular/common';
import {
  IUser,
  EsportsUserService,
  GlobalUtils,
  EsportsToastService,
  IPagination,
  EsportsGtmService,
  SuperProperties
} from 'esports';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';

import { environment } from '../../../../environments/environment';

const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;
@Component({
  selector: 'app-app-referral',
  templateUrl: './app-referral.component.html',
  styleUrls: ['./app-referral.component.scss'],
})
export class AppReferralComponent implements OnInit, OnDestroy {
  transactionDetail;
  rewardDetails;
  showLoader = false;
  closeResult: boolean;
  user: IUser;
  page: IPagination;
  clicked = false;
  URL: string;
  paginationData = {
    page: 1,
    limit: 50,
    sort: { createdOn: -1 },
  };
  rows = [];

  referral = {
    rows: [],
    columns: [
      { name: 'DATE' },
      { name: 'Invitation' },
      { name: 'Reward points' },
    ],
  };
  totalCount = 0;
  public innerWidth: any;
  userSubscription: Subscription;
  constructor(
    @Inject(DOCUMENT) private document: Document,
    private userService: EsportsUserService,
    public datePipe: DatePipe,
    private transactionServices: TransactionService,
    private translateService: TranslateService,
    private modalService: NgbModal,
    private gtmService: EsportsGtmService,
    public eSportsToastService: EsportsToastService
  ) {}
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    if (GlobalUtils.isBrowser()) {
    this.innerWidth = window.innerWidth;
    }
  }
  ngOnInit(): void {
    if (GlobalUtils.isBrowser()) {
    this.innerWidth = window.innerWidth;
    this.getUserData();
    }
  }

  getUserData() {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.user = Object.assign({}, data);
        this.URL =
          this.document.location.origin +
          '/user/registration?referral=' +
          data.accountDetail.referralId;
        if (data.accountDetail.referralId) {
          this.getUserinviteesList();
        }
      } else {
        // const newVal = this.userService.getAuthenticatedUser();
      }
    });
  }

  getUserinviteesList = () => {
    const pagination = JSON.stringify(this.paginationData);
    this.userService
      .getUserinviteesList(API, {
        pagination,
      })
      .subscribe(
        (res) => {
          this.showLoader = false;
          this.referral.rows = res.data;
          this.referral.rows.forEach((obj, index) => {
            obj.DATE = this.datePipe.transform(obj.DATE, 'MMM dd, yyyy');
          });
          this.totalCount = res.totals.count;
          this.page = {
            totalItems: res.totals.count,
            itemsPerPage: 50,
            maxSize: 50,
          };
        },
        (err) => {}
      );
  };
  open(content) {
    this.pushGTMTags('Invite_Friends_Clicked');
    if (this.user.accountDetail.referralId) {
      this.clicked = false;
      this.modalService
        .open(content, { ariaLabelledBy: 'modal-basic-title', centered: true })
        .result.then(
          (result) => {
            this.copyToClipboard(this.URL);
          },
          (reason) => {
            this.closeResult = false;
          }
        );
    } else {
      this.eSportsToastService.showError(
        this.translateService.instant(
          'PROFILE.BASIC_INFO.REFERRAL_ERROR_MESSAGE'
        )
      );
    }
  }
  setPage1(event) {
    this.paginationData.page = event;
    this.showLoader = false;
    this.rows.length = 0;
    this.getUserinviteesList();
  }
  copyToClipboard(event: any) {
    this.pushGTMTags('Copy_Referral_Code_Clicked');
    const dummyElement = this.document.createElement('textarea');
    var element = document.getElementById('myDIV');
    element.classList.toggle('disableBtn');
    this.document.body.appendChild(dummyElement);
    dummyElement.value = this.URL;
    event.target.disabled = true;
    dummyElement.select();
    this.document.execCommand('copy');
    this.document.body.removeChild(dummyElement);
  }

  ngOnDestroy() {
    this.modalService.dismissAll();
    this.userSubscription?.unsubscribe();
  }

  pushGTMTags(eventName: string) {
    if (!eventName) {
      return;
    }
    let superProperties: SuperProperties = {};
    if (this.user) {
      superProperties = this.gtmService.assignLoggedInUsedData(
        this.user
      );
    }

    this.gtmService.gtmEventWithSuperProp({
      eventName: eventName,
      superProps: superProperties,
    });
  }

}
