import {
  FontAwesomeModule,
  FaIconLibrary,
} from '@fortawesome/angular-fontawesome';
import { faTrash, faEdit, faEye } from '@fortawesome/free-solid-svg-icons';
import { NgCircleProgressModule } from 'ng-circle-progress';
import { ModalModule } from 'ngx-bootstrap/modal';
import { NgModule } from '@angular/core';

import { CoreModule } from '../../core/core.module';
import { FormComponentModule } from '../../shared/components/form-component/form-component.module';
import { LogRegModule } from '../log-reg/log-reg.module';
import { PaymentModule } from '../payment/payment.module';
import { ProfileRoutingModule } from './profile-routing.module';
import { SharedModule } from '../../shared/modules/shared.module';

import { AppReferralComponent } from './app-referral/app-referral.component';
import { BasicInfoComponent } from './basic-info/basic-info.component';
import { ChangePasswordComponent } from './profile-setting/change-password/change-password.component';
import { InboxComponent } from './inbox/inbox.component';
import { InboxMessageComponent } from './inbox/inbox-message/inbox-message.component';
import { InboxMessageItemComponent } from './inbox/inbox-message-item/inbox-message-item.component';
import { MembersTableComponent } from './components/members-table/members-table.component';
import { MyBookmarksComponent } from './my-bookmarks/my-bookmarks.component';
import { MyBracketComponent } from './my-bracket/my-bracket.component';
import { MyContentComponent } from './my-content/my-content.component';
import { MyTeamsComponent } from './my-teams/my-teams.component';
import { MyTeamsViewComponent } from './my-teams-view/my-teams-view.component';
import { MyTournamentComponent } from './my-tournament/my-tournament.component';
import { MyTransactionsComponent } from './my-transactions/my-transactions.component';
import { ProfileComponent } from './profile.component';
import { ProfileSettingComponent } from './profile-setting/profile-setting.component';

import { TournamentMatchesComponent } from './components/tournament-matches/tournament-matches.component';
import { RouterBackModule } from '../../shared/directives/router-back.module';
import { TeamCardControlComponent } from './components/team-card-control/team-card-control.component';
import { TeamCardInfoComponent } from './components/team-card-info/team-card-info.component';
import { TeamCardInviteModule } from './components/team-card-invite/team-card-invite.module';

@NgModule({
  declarations: [
    AppReferralComponent,
    BasicInfoComponent,
    ChangePasswordComponent,
    InboxComponent,
    InboxMessageComponent,
    InboxMessageItemComponent,
    MembersTableComponent,
    MyBookmarksComponent,
    MyBracketComponent,
    MyContentComponent,
    MyTeamsComponent,
    MyTeamsViewComponent,
    MyTournamentComponent,
    MyTransactionsComponent,
    ProfileComponent,
    ProfileSettingComponent,
    TournamentMatchesComponent,
    TeamCardControlComponent,
    TeamCardInfoComponent,
  ],
  imports: [
    SharedModule,
    CoreModule,
    FormComponentModule,
    ProfileRoutingModule,
    ModalModule.forRoot(),
    // Specify ng-circle-progress as an import
    NgCircleProgressModule.forRoot({
      // set defaults here
    }),
    LogRegModule,
    FontAwesomeModule,
    PaymentModule,
    RouterBackModule,
    TeamCardInviteModule,
  ],
})
export class ProfileModule {
  constructor(private library: FaIconLibrary) {
    library.addIcons(faTrash, faEdit, faEye);
  }
}
