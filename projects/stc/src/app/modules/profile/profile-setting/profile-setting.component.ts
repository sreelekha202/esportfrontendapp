import {
  Component,
  OnInit,
  TemplateRef,
  ViewChild,
  OnDestroy,
  ElementRef,
} from '@angular/core';
import {
  faFacebookF,
  faTwitter,
  faGoogle,
} from '@fortawesome/free-brands-svg-icons';
import {
  faDesktop,
  faMobileAlt,
  faCheck,
  faGamepad,
  faEdit,
} from '@fortawesome/free-solid-svg-icons';
import {
  CountryISO,
  NgxIntlTelInputComponent,
  SearchCountryField,
} from 'ngx-intl-tel-input';
import { NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { SwiperConfigInterface } from 'ngx-swiper-wrapper';
import { ActivatedRoute, Router } from '@angular/router';
import {
  PhoneNoFormComponentDataItem,
  PhoneNoFormComponentOutputData,
} from '../../log-reg/phone-no/phone-no.component';
import {
  UserPreferenceService,
  TransactionService,
  AuthServices,
} from '../../../core/service';
import {
  InfoPopupComponent,
  InfoPopupComponentData,
  InfoPopupComponentType,
} from '../../../shared/popups/info-popup/info-popup.component';
import { VerificationFormComponentData } from '../../log-reg/verification-form/verification-form.component';
import { TranslateService } from '@ngx-translate/core';
import { MatDialog } from '@angular/material/dialog';
import {
  IUser,
  EsportsUserService,
  GlobalUtils,
  EsportsToastService,
  EsportsChatService,
  EsportsGtmService,
  SuperProperties,
} from 'esports';
import { FormBuilder, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
enum ProfileSettingComponentTab {
  content = 'PROFILE.PROFILE_SETTING.TAB2',
  account = 'PROFILE.PROFILE_SETTING.TAB1',
  payment = 'PROFILE.PROFILE_SETTING.TAB3',
  chat = 'PROFILE.PROFILE_SETTING.TAB4',
}

import { environment } from '../../../../environments/environment';

const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;

declare var $: any;
@Component({
  selector: 'app-profile-setting',
  templateUrl: './profile-setting.component.html',
  styleUrls: ['./profile-setting.component.scss'],
  providers: [NgbModalConfig, NgbModal],
})
export class ProfileSettingComponent implements OnInit, OnDestroy {
  loading = false;
  @ViewChild('connectWithPaypal') connectWithPaypal: ElementRef;
  isDisabled = true;
  currentUser: IUser;
  user: IUser;
  ProfileSettingComponentTab = ProfileSettingComponentTab;
  faFacebookF = faFacebookF;
  faTwitter = faTwitter;
  faGoogle = faGoogle;
  faDesktop = faDesktop;
  faGamepad = faGamepad;
  faMobileAlt = faMobileAlt;
  faCheck = faCheck;
  faEdit = faEdit;
  stepTwo = false;
  selectedPrefrence;
  @ViewChild('EmailContent')
  private EmailContent: TemplateRef<any>;
  swiperConfig: SwiperConfigInterface = {
    width: 150,
    spaceBetween: 30,
    navigation: true,
  };
  isDesktopPlatform = true;
  isChangePassword = false;
  social = {
    facebook: false,
    google: false,
  };

  emailOutputData: PhoneNoFormComponentOutputData;

  emailData: PhoneNoFormComponentDataItem = {
    title: 'PROFILE.PROFILE_SETTING.ACCOUNT.MODAL.TITLE1',
    subtitle: 'LOG_REG.LOGIN.SUBTITLE1',
    submitBtn: 'BUTTON.NEXT',
    cancelBtn: 'BUTTON.CANCEL',
    InputName: 'email',
    InputType: 'email',
    InputPlaceholder: 'LOG_REG.firstInputPlaceholder1',
    InputIcon: 'email',
    error: '',
  };

  emailVerification: VerificationFormComponentData = {
    title: 'LOG_REG.LOGIN.TITLE2',
    subtitle: 'PROFILE.PROFILE_SETTING.ACCOUNT.MODAL.SUBTITLE1',
    footerTitle: 'LOG_REG.FORGOT.footerTitle',
    footerLinkTitle: 'LOG_REG.FORGOT.footerLinkTitle',
    timerMs: 0,
    submitBtn: 'BUTTON.NEXT',
    error: '',
  };

  game = [];
  genre = [];
  prefer = [];
  platform = [];
  error = '';
  input = '';
  rowsFirstTab = [];

  columnsFirstTab = [
    { name: 'Id' },
    { name: 'profilePic' },
    { name: 'fullName' },
  ];
  list = [
    {
      url: './assets/images/Home/top4.png',
      title: 'LEAGUE OF LEGENDS',
      isChecked: false,
    },
    {
      url: './assets/images/Home/top4.png',
      title: 'LEAGUE OF LEGENDS',
      isChecked: false,
    },
    {
      url: './assets/images/Home/top4.png',
      title: 'LEAGUE OF LEGENDS',
      isChecked: false,
    },
    {
      url: './assets/images/Home/top4.png',
      title: 'LEAGUE OF LEGENDS',
      isChecked: false,
    },
    {
      url: './assets/images/Home/top4.png',
      title: 'LEAGUE OF LEGENDS',
      isChecked: false,
    },
    {
      url: './assets/images/Home/top4.png',
      title: 'LEAGUE OF LEGENDS',
      isChecked: false,
    },
    {
      url: './assets/images/Home/top4.png',
      title: 'LEAGUE OF LEGENDS',
      isChecked: false,
    },
    {
      url: './assets/images/Home/top4.png',
      title: 'LEAGUE OF LEGENDS',
      isChecked: false,
    },
    {
      url: './assets/images/Home/top4.png',
      title: 'LEAGUE OF LEGENDS',
      isChecked: false,
    },
    {
      url: './assets/images/Home/top4.png',
      title: 'LEAGUE OF LEGENDS',
      isChecked: false,
    },
    {
      url: './assets/images/Home/top4.png',
      title: 'LEAGUE OF LEGENDS',
      isChecked: false,
    },
    {
      url: './assets/images/Home/top4.png',
      title: 'LEAGUE OF LEGENDS',
      isChecked: false,
    },
    {
      url: './assets/images/Home/top4.png',
      title: 'LEAGUE OF LEGENDS',
      isChecked: false,
    },
    {
      url: './assets/images/Home/top4.png',
      title: 'LEAGUE OF LEGENDS',
      isChecked: false,
    },
    {
      url: './assets/images/Home/top4.png',
      title: 'LEAGUE OF LEGENDS',
      isChecked: false,
    },
    {
      url: './assets/images/Home/top4.png',
      title: 'LEAGUE OF LEGENDS',
      isChecked: false,
    },
    {
      url: './assets/images/Home/top4.png',
      title: 'LEAGUE OF LEGENDS',
      isChecked: false,
    },
  ];

  SearchCountryField = SearchCountryField;
  CountryISO = CountryISO;
  preferredCountries: CountryISO[] = [
    CountryISO.SaudiArabia,
    CountryISO.Bahrain,
    CountryISO.Kuwait,
  ];
  selectedCountryISO = CountryISO.SaudiArabia;
  mySelectedCountryISO: CountryISO;
  ngxPlaceholder = '5XXXXXXXX';

  paymentInfo = {
    stcpay: 'stc pay',
    paypal: 'paypal',
  };

  selected = {
    type: '',
    paymentAccountId: '',
  };

  transaction;
  notSelect;

  form = this.formBuilder.group({
    paymentType: ['', Validators.required],
    paymentAccountId: ['', Validators.required],
  });

  userSubscription: Subscription;

  constructor(
    private userService: EsportsUserService,
    private formBuilder: FormBuilder,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private appAuthService: AuthServices,
    public eSportsToastService: EsportsToastService,
    private translateService: TranslateService,
    public translate: TranslateService,
    private matDialog: MatDialog,
    private modalService: NgbModal,
    config: NgbModalConfig,
    public preference: UserPreferenceService,
    private transactionService: TransactionService,
    private gtmService: EsportsGtmService,
    private chatService: EsportsChatService
  ) {
    config.backdrop = 'static';
    config.keyboard = false;
    config.centered = true;
    config.backdropClass = 'dark-modal';
  }

  popUpTitleAndText = (data) => {
    if (data?._id) {
      return {
        title: this.translateService.instant('TOURNAMENT.UPDATE'),
        text: this.translateService.instant('TOURNAMENT.UPDATE_TXT'),
      };
    } else if (data?.userid) {
      return {
        title: this.translateService.instant('CHAT.UNBLOCK_USER'),
        text: this.translateService.instant('CHAT.UNBLOCK_DECISION'),
      };
    } else {
      return {
        title: this.translateService.instant('TOURNAMENT.SAVE_TOURNAMENT'),
        text: this.translateService.instant('TOURNAMENT.STATUS_2'),
      };
    }
  };

  getPaypalObject = async (code) => {
    let response: any = await this.appAuthService.fetchPaypalUserInfo(code);
    if (response && response.data) {
      response.data = JSON.parse(response.data);
      if (response.data.emails && response.data.emails[0].confirmed) {
        const data: InfoPopupComponentData = {
          title: this.translateService.instant('ADMIN.SUCCESS.OK'),
          text: `${response.data.emails[0].value}`,
          type: InfoPopupComponentType.confirm,
          btnText: this.translateService.instant('TOURNAMENT.CONFIRM'),
        };

        const confirmed = await this.matDialog
          .open(InfoPopupComponent, { data })
          .afterClosed()
          .toPromise();

        if (confirmed) {
          const res = this.userService
            .updatePaymentInfo(API, {
              paymentType: 'paypal',
              paymentAccountId: response.data.emails[0].value,
            })
            .subscribe(
              (data) => {
                this.userService.refreshCurrentUser(API, TOKEN);
                this.selectedPrefrence = 'tab3';
                this.router
                  .navigateByUrl('/home', { skipLocationChange: true })
                  .then(() => {
                    this.router.navigate(['/profile/profile-setting']);
                  });
                this.eSportsToastService.showSuccess(data.message);
                this.loading = false;
              },
              (error) => {
                this.loading = false;
                return false;
              }
            );
        }
      } else {
        const afterBlockData: InfoPopupComponentData = {
          title: this.translateService.instant('ADMIN.ERROR.FAIL'),
          text: `${response.data.emails[0].value}`,
          type: InfoPopupComponentType.info,
        };
        this.matDialog.open(InfoPopupComponent, { data: afterBlockData });
      }
    }
  };

  ngOnInit(): void {
    if (
      GlobalUtils.isBrowser() &&
      localStorage.getItem(environment.currentToken)
    ) {
      if (
        this.activeRoute.snapshot.queryParams['activeTab'] == 0 ||
        this.activeRoute.snapshot.queryParams['activeTab']
      ) {
        this.fetchNews({
          index: this.activeRoute.snapshot.queryParams['activeTab'],
        });
        this.callPayPalButton();
      } else {
        if (!this.selectedPrefrence) {
          this.pushGTMTags('Preferred_Content_Tab_Click');
        }
      }

      this.activeRoute.queryParams.subscribe((params) => {
        this.getUserData();
        this.getAllPreferedData();
        // if (params['activeTab']) {
        //   this.fetchNews({ index: params['activeTab'] });
        //   this.callPayPalButton();
        // }
        if (params['code']) {
          this.getPaypalObject(params['code']);
        }
      });
      this.getUserData();
      this.getAllPreferedData();
    } else {
      this.router.navigate(['/']);
    }
    this.notSelect =
      this.translateService.currentLang === 'en'
        ? 'Nothing selected'
        : 'لا شيء محدد';
  }

  private loadExternalScript(scriptUrl: string) {
    return new Promise(function (resolve, reject) {
      const scriptElement = document.createElement('script');
      scriptElement.src = scriptUrl;
      scriptElement.onload = resolve;
      document.body.appendChild(scriptElement);
    });
  }

  ngAfterViewInit() {
    this.updateBootstrapSelect();
    $('.selectpicker').selectpicker({
      noneSelectedText: this.translate.instant('NOTHING_SELECTED_TITLE'),
    });
  }

  callPayPalButton() {
    if (GlobalUtils.isBrowser()) {
      this.loadExternalScript(
        'https://www.paypalobjects.com/js/external/connect/api.js'
      ).then(
        (res) => {
          window['paypal'].use(['login'], (login) => {
            login.render({
              appid: environment.paypal_client_id,
              scopes: environment.paypalScopes,
              authend: environment.paypalAccountMode,
              containerid: 'cwppButton',
              responseType: 'code',
              locale: 'en-us',
              buttonType: 'CWP',
              buttonShape: 'pill',
              buttonSize: 'lg',
              fullPage: 'true',
              returnurl: `${window.location.origin}/profile/profile-setting?activeTab=tab3`,
            });
          });
        },
        (err) => {}
      );
    }
  }

  ngOnDestroy() {
    this.modalService.dismissAll();
    this.userSubscription?.unsubscribe();
  }

  getUserData() {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.currentUser = data;
        //this.user = Object.assign({}, this.currentUser);
        this.user = { ...this.currentUser };
        this.selected.type = this.user.accountDetail.paymentType;
        this.selected.paymentAccountId =
          this.user.accountDetail.paymentAccountId;
        let that = this;
        this.user.identifiers.forEach(function (value) {
          if (value.idenType === 'GOOGLE') {
            that.social.google = true;
          }
          if (value.idenType === 'FACEBOOK') {
            that.social.facebook = true;
          }
        });
        this.getAllBlockedUser();
      } else {
        // const newVal = this.userService.getAuthenticatedUser();
      }
    });
  }

  getAllPreferedData() {
    let that = this;
    this.preference.getAllSettingPageData().subscribe(
      (data) => {
        this.game = data.data.game;
        this.genre = data.data.genre;
        this.prefer = data.data.prefer;
        this.platform = data.data.platform;
        this.preference.getLoginPrefrence().subscribe(
          (data) => {
            if (data.data?.game) {
              data.data.game.forEach(function (value) {
                const index = that.game.findIndex((x) => x._id === value);
                if (index > -1) {
                  that.game[index].isChecked = true;
                }
              });
            }
            if (data.data?.gamegenre) {
              data.data.gamegenre.forEach(function (value) {
                let index = that.genre.findIndex((x) => x._id === value);

                that.genre[index].isChecked = true;
              });
            }
            if (data.data?.prefercontent) {
              data.data.prefercontent.forEach(function (value) {
                let index = that.prefer.findIndex((x) => x._id === value);

                that.prefer[index].isChecked = true;
              });
            }
            if (data.data?.platform) {
              data.data.platform.forEach(function (value) {
                let index = that.platform.findIndex((x) => x._id === value);

                that.platform[index].isChecked = true;
              });
            }
          },
          (error) => {
            this.error = error.error.message;
          }
        );
      },
      (error) => {
        this.error = error.error.message;
      }
    );
  }

  onToggleItem(item): void {
    item.isChecked = !item.isChecked;
  }

  changePlatform(platform) {
    let index = this.platform.findIndex((x) => x === platform);
    if (index >= 0) {
      this.platform.splice(index, 1);
    } else {
      this.platform.push(platform);
    }
  }

  savePrefrences() {
    this.pushGTMTags('Preferred_Content_Update_Complete');
    let game = [];
    let genre = [];
    let prefer = [];
    let platform = [];

    this.game.forEach(function (value) {
      if (value.isChecked == true) {
        game.push(value._id);
      }
    });
    this.genre.forEach(function (value) {
      if (value.isChecked == true) {
        genre.push(value._id);
      }
    });
    this.prefer.forEach(function (value) {
      if (value.isChecked == true) {
        prefer.push(value._id);
      }
    });
    this.platform.forEach(function (value) {
      if (value.isChecked == true) {
        platform.push(value._id);
      }
    });
    let updateData = {
      game: game,
      genre: genre,
      prefer: prefer,
      platform: platform,
    };
    this.preference.addPrefrence(updateData).subscribe(
      (data) => {
        this.eSportsToastService.showSuccess(data.message);
        this.router
          .navigateByUrl('/home', { skipLocationChange: true })
          .then(() => {
            this.router.navigate(['/profile/profile-setting']);
          });
      },
      (error) => {
        this.error = error.error.message;
      }
    );
  }

  // public socialConnect(socialProvider: string) {
  //   let socialPlatformProvider;
  //   if (socialProvider === 'facebook') {
  //     socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
  //   } else if (socialProvider === 'google') {
  //     socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
  //   }
  //   this.authService.signIn(socialPlatformProvider).then((socialusers) => {
  //     if (socialusers) {
  //       // this.socialusers = socialusers;
  //       this.Savesresponse(socialusers.authToken, socialusers.provider);
  //     }
  //   });
  // }

  Savesresponse(authToken, provider) {
    this.userService.add_social(API, authToken, provider).subscribe(
      (data) => {
        this.userService.refreshCurrentUser(API, TOKEN);
        this.eSportsToastService.showSuccess(data.message);
        this.router
          .navigateByUrl('/home', { skipLocationChange: true })
          .then(() => {
            this.router.navigate(['/profile/profile-setting']);
          });
      },
      (error) => {
        this.eSportsToastService.showError(error.error.message);
      }
    );
  }

  add_Email() {
    this.stepTwo = false;
    this.emailData.title = 'PROFILE.PROFILE_SETTING.ACCOUNT.MODAL.TITLE1';
    this.emailData.InputName = 'email';
    this.emailData.InputType = 'email';
    this.emailData.InputPlaceholder = 'LOG_REG.LOGIN.firstInputPlaceholder1';
    this.emailData.InputIcon = 'email';
    this.emailData.error = '';
    this.emailVerification.title = 'LOG_REG.LOGIN.TITLE2';
    this.emailVerification.subtitle =
      'PROFILE.PROFILE_SETTING.ACCOUNT.MODAL.SUBTITLE1';
    this.modalService.open(this.EmailContent, {
      size: 'md',
      centered: true,
      // scrollable: true,
      windowClass: 'email-modal-content',
    });
  }

  add_Phone() {
    this.stepTwo = false;
    this.emailData.title = 'PROFILE.PROFILE_SETTING.ACCOUNT.MODAL.TITLE2';
    this.emailData.InputName = 'phone';
    this.emailData.InputType = 'phone';
    this.emailData.InputPlaceholder = 'LOG_REG.LOGIN.firstInputPlaceholder';
    this.emailData.InputIcon = 'phone';
    this.emailData.error = '';
    this.emailVerification.title = 'LOG_REG.LOGIN.TITLE1';
    this.emailVerification.subtitle =
      'PROFILE.PROFILE_SETTING.ACCOUNT.MODAL.SUBTITLE2';
    this.modalService.open(this.EmailContent, {
      size: 'md',
      centered: true,
      // scrollable: true,
      windowClass: 'email-modal-content',
    });
  }

  addEmailNext(data) {
    this.loading = true;
    if (data) {
      if (data.type === 'email') {
        this.emailOutputData = data;

        const queryData = {
          email: data.email,
        };
        this.emailData.error = '';
        this.userService.addEmail(API, queryData).subscribe(
          (response) => {
            this.emailData.error = '';
            this.emailVerification.timerMs = 240;
            this.emailOutputData.mailPhone = queryData.email;
            this.emailOutputData.type = 'add_email';
            this.stepTwo = true;
            this.loading = false;
          },
          (error) => {
            this.emailData.error = error.error.message;
            this.stepTwo = false;
            this.loading = false;
          }
        );
      }
      if (data.type === 'phone') {
        this.emailOutputData = data;
        const queryData = {
          phoneNumber: data.phone?.e164Number,
        };
        this.emailData.error = '';
        this.userService.addEmail(API, queryData).subscribe(
          (response) => {
            this.emailData.error = '';
            this.emailVerification.timerMs = 240;
            this.emailOutputData.mailPhone = data.phone?.e164Number;
            this.emailOutputData.type = 'add_phone';
            this.stepTwo = true;
            this.loading = false;
          },
          (error) => {
            this.emailData.error = error.error.message;
            this.stepTwo = false;
            this.loading = false;
          }
        );
      }
    }
  }

  submitEmail(data) {
    this.pushGTMTags('Account_Update_Complete');
    this.loading = true;
    let queryData;
    if (data.otp) {
      if (data.formValue.type === 'add_email') {
        queryData = {
          type: 'email',
          otp: data.otp,
        };
      }
      if (data.formValue.type === 'add_phone') {
        queryData = {
          type: 'phone',
          otp: data.otp,
        };
      }
      this.emailVerification.error = '';
      this.userService.verifyEmail(API, queryData).subscribe(
        (response) => {
          this.loading = false;
          this.emailVerification.error = '';
          this.modalService.dismissAll();
          this.userService.refreshCurrentUser(API, TOKEN);
          this.eSportsToastService.showSuccess(response.message);

          this.router
            .navigateByUrl('/home', { skipLocationChange: true })
            .then(() => {
              this.router.navigate(['/profile/profile-setting']);
            });
        },
        (error) => {
          this.emailVerification.error = error.error.message;
          this.loading = false;
        }
      );
    }
  }

  onChangePassword(): void {
    this.isChangePassword = true;
  }

  onChangePasswordSubmit(): void {
    this.isChangePassword = false;
  }

  onToggleEdit() {
    this.notSelect =
      this.translateService.currentLang === 'en'
        ? 'Nothing selected'
        : 'لا شيء محدد';

    this.selected.type = this.user.accountDetail.paymentType
      ? this.user.accountDetail.paymentType
      : this.translateService.instant('NOTHING_SELECTED_TITLE');
    this.isDisabled = !this.isDisabled;
    this.form.controls['paymentAccountId'].enable();
    this.updateBootstrapSelect();
  }

  cancel() {
    if (this.activeRoute.snapshot.queryParams?.activeTab == '2') {
      this.pushGTMTags('Payment_Info_Update_Cancellation');
    }

    this.router
      .navigateByUrl('/profile', { skipLocationChange: true })
      .then(() => {
        this.router.navigate(['/profile/profile-setting'], {
          queryParams: { activeTab: this.selectedPrefrence },
        });
      });
    // this.selected.type = this.user.accountDetail.paymentType
    //   ? this.user.accountDetail.paymentType
    //   : '';
    // this.selected.paymentAccountId = this.user.accountDetail.paymentAccountId
    //   ? this.user.accountDetail.paymentAccountId
    //   : '';
    // this.isDisabled = true;
    // this.form.controls['paymentAccountId'].disable();
    // this.updateBootstrapSelect();
  }

  private updateBootstrapSelect(): void {
    $('.selectpicker').selectpicker({
      noneSelectedText: this.translateService.instant('NOTHING_SELECTED_TITLE'),
    });
    setTimeout(() => {
      // update bootstrap select
      $('.selectpicker').selectpicker('refresh');
    });
  }

  onChangePaymentType(type) {
    if (type === 'stcplay') {
      this.form.get('paymentAccountId').setValidators([Validators.required]);
      this.form.get('paymentAccountId').setValue('');
      this.form.get('paymentAccountId').updateValueAndValidity();
    } else if (type === 'paypal') {
      this.callPayPalButton();
      this.form
        .get('paymentAccountId')
        .setValidators([Validators.required, Validators.email]);
      this.form.get('paymentAccountId').setValue('');
    }
    this.selected.paymentAccountId = '';
    this.form.get('paymentAccountId').updateValueAndValidity();
  }

  savePaymentAccount() {
    this.pushGTMTags('Account_Update_Complete');
    this.loading = true;
    try {
      if (this.form.invalid && !this.form.value?.id) {
        this.form.markAllAsTouched();
        this.loading = false;
        return;
      }

      const { value } = this.form;
      const paymentAccountId =
        value.paymentType === 'stcpay'
          ? value.paymentAccountId.nationalNumber === '0539396141'
            ? value.paymentAccountId.nationalNumber
            : value.paymentAccountId.e164Number
          : value.paymentAccountId;
      const res = this.userService
        .updatePaymentInfo(API, {
          paymentType: value.paymentType,
          paymentAccountId: paymentAccountId,
        })
        .subscribe(
          (data) => {
            this.userService.refreshCurrentUser(API, TOKEN);
            this.selectedPrefrence = 'tab3';
            this.router
              .navigateByUrl('/home', { skipLocationChange: true })
              .then(() => {
                this.router.navigate(['/profile/profile-setting']);
              });
            this.eSportsToastService.showSuccess(data.message);
            this.loading = false;
          },
          (error) => {
            this.loading = false;
            return false;
          }
        );
    } catch (error) {}
  }

  fetchNews(tabChangeEvent: any) {
    if (tabChangeEvent.index == 0) {
      this.pushGTMTags('Preferred_Content_Tab_Click');
    }
    if (tabChangeEvent.index == 1) {
      this.pushGTMTags('Account_Tab_Click');
    }
    if (tabChangeEvent.index === 2) {
      this.isDisabled = true;
      this.pushGTMTags('Payment_Info_Tab_Click');
    }
    if (tabChangeEvent.index === 3) {
      this.getAllBlockedUser();
    }
    this.selectedPrefrence = tabChangeEvent.index;
    this.router.navigate(['.'], {
      relativeTo: this.activeRoute,
      queryParams: { activeTab: tabChangeEvent.index },
    });
    this.form.controls['paymentAccountId'].disable();
    this.updateBootstrapSelect();
    this.callPayPalButton();
  }

  getStcPaymentResponse = async (order) => {
    try {
      this.loading = true;
      const payload = {
        accountId: this.user?.accountDetail?._id,
        type: 'stcpay',
        mobileNo: order?.e164Number,
      };
      const response =
        await this.transactionService.createOrUpdateAccTransaction(payload);
      this.transaction = response;
      this.loading = false;
      this.eSportsToastService.showSuccess(response?.message);
    } catch (error) {
      this.loading = false;
      this.eSportsToastService.showError(
        error?.error?.data?.Text || error?.error?.message || error?.message
      );
    }
  };

  getStcOtpVerify = async (params) => {
    try {
      this.loading = true;
      const payload = {
        OtpReference:
          this.transaction['data']['DirectPaymentAuthorizeV4ResponseMessage'][
            'OtpReference'
          ],
        STCPayPmtReference:
          this.transaction['data']['DirectPaymentAuthorizeV4ResponseMessage'][
            'STCPayPmtReference'
          ],
        otp: params,
        id: this.transaction.data.id,
        accountId: this.user?.accountDetail?._id,
      };
      const response = await this.transactionService.verifyAccTransaction(
        payload
      );
      this.userService.refreshCurrentUser(API, TOKEN);
      this.selectedPrefrence = 'tab3';
      this.router
        .navigateByUrl('/home', { skipLocationChange: true })
        .then(() => {
          this.router.navigate(['/profile/profile-setting']);
        });
      this.loading = false;
      this.eSportsToastService.showSuccess(response?.message, true, 9000);
    } catch (error) {
      this.loading = false;
      this.eSportsToastService.showError(
        error?.error?.message || error?.message
      );
    }
  };
  ngOnChanges() {
    this.updateBootstrapSelect();
  }
  getAllBlockedUser = async () => {
    this.loading = true;
    await this.chatService.getBlockedUsers(this.currentUser._id).subscribe(
      (res: any) => {
        const blockedUsers = res?.blockedUserList;
        this.rowsFirstTab = res.blockedUserList;
        this.loading = false;
      },
      (error) => {
        this.loading = false;
        this.eSportsToastService.showError(
          error?.error?.message || error?.message
        );
      }
    );
  };
  unBlockUser = async (id) => {
    // this.loading = true;
    const payload = {
      userid: this.currentUser._id,
      blockedUserId: id,
    };
    const data: InfoPopupComponentData = {
      ...this.popUpTitleAndText(payload),
      type: InfoPopupComponentType.confirm,
      btnText: this.translateService.instant('TOURNAMENT.CONFIRM'),
    };

    const confirmed = await this.matDialog
      .open(InfoPopupComponent, { data })
      .afterClosed()
      .toPromise();
    if (confirmed) {
      this.chatService.unblockUser(payload).subscribe(
        (res: any) => {
          if (res && res?.unBlockedUser) {
            // this.loading = false;
            this.getAllBlockedUser();
          }
        },
        (error) => {
          // this.loading = false;
          this.eSportsToastService.showError(
            error?.error?.message || error?.message
          );
        }
      );
    }
  };

  pushGTMTags(eventName: string) {
    let superProperties: SuperProperties = {};
    if (this.currentUser) {
      superProperties = this.gtmService.assignLoggedInUsedData(
        this.currentUser
      );
    }

    this.gtmService.gtmEventWithSuperProp({
      eventName: eventName,
      superProps: superProperties,
    });
  }
}
