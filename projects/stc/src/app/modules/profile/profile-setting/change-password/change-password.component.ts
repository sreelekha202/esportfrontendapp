import { FormControl, FormGroup, Validators } from '@angular/forms';
//import { CustomValidators } from 'ngx-custom-validators';
import {
  Component,
  EventEmitter,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';

import { MYCustomValidators } from '../../../log-reg/password-form/custom-validators';
import { EsportsUserService, EsportsToastService } from 'esports';
import { IUser, EsportsGtmService, SuperProperties } from 'esports';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { environment } from '../../../../../environments/environment';

const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss'],
})
export class ChangePasswordComponent implements OnInit, OnDestroy {
  @Output() changePassword = new EventEmitter();
  currentUser: IUser;
  user: IUser;

  oldPassword = new FormControl('', [
    Validators.required,
    // check whether the entered password has a number
    // MYCustomValidators.patternValidator(/\d/, { hasNumber: true }),
    // check whether the entered password has upper case letter
    // MYCustomValidators.patternValidator(/[A-Z]/, { hasCapitalCase: true }),
    // check whether the entered password has a lower case letter
    // MYCustomValidators.patternValidator(/[a-z]/, { hasSmallCase: true }),
    // check whether the entered password has a special character
    // MYCustomValidators.patternValidator(
    //   /[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/,
    //   { hasSpecialCharacters: true }
    // ),
    // Validators.minLength(8),
  ]);
  error = '';
  password = new FormControl('', [
    Validators.required,
    // check whether the entered password has a number
    // MYCustomValidators.patternValidator(/\d/, { hasNumber: true }),
    // check whether the entered password has upper case letter
    // MYCustomValidators.patternValidator(/[A-Z]/, { hasCapitalCase: true }),
    // check whether the entered password has a lower case letter
    MYCustomValidators.patternValidator(/[a-zA-Z]/, { hasCharacter: true }),
    // check whether the entered password has a special character
    // MYCustomValidators.patternValidator(
    //   /[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/,
    //   { hasSpecialCharacters: true }
    // ),
    // Validators.minLength(8),
  ]);
  certainPassword = new FormControl(
    ''
    //CustomValidators.equalTo(this.password)
  );

  form = new FormGroup(
    {
      oldPassword: this.oldPassword,
      password: this.password,
      certainPassword: this.certainPassword,
    },

    { validators: MYCustomValidators.passwordMatchValidator }
  );

  userSubscription: Subscription;

  constructor(
    private userService: EsportsUserService,
    public eSportsToastService: EsportsToastService,
    private gtmService: EsportsGtmService,
    private router: Router
  ) {}

  ngOnInit() {
    this.getUserData();
  }

  ngOnDestroy() {
    this.userSubscription?.unsubscribe();
  }

  getUserData() {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.currentUser = data;
        this.user = Object.assign({}, this.currentUser);
        if (!data.isPassword) {
          this.form.removeControl('oldPassword');
          this.form.updateValueAndValidity();
        }
      } else {
        // const newVal = this.userService.getAuthenticatedUser();
      }
    });
  }

  cancel() {
    this.pushGTMTags('Change_Password_Cancellation');
    this.router
      .navigateByUrl('/home', { skipLocationChange: true })
      .then(() => {
        this.router.navigate(['/profile/profile-setting']);
      });
  }

  onVisibilityToggle(el: HTMLInputElement): void {
    el.type = el.type === 'text' ? 'password' : 'text';
  }

  onSubmitEvent(): void {
    this.error = '';
    const updatePassword = {
      old_password: this.form.value.oldPassword,
      new_password: this.form.value.password,
    };
    this.userService.changePassword(API, updatePassword).subscribe(
      (data) => {
        this.pushGTMTags('Change_Password_Complete');
        this.error = '';
        this.userService.refreshCurrentUser(API, TOKEN);
        this.eSportsToastService.showSuccess(data.message);
        this.changePassword.emit();
      },
      (error) => {
        this.error = error.error.message;
      }
    );
    // this.changePassword.emit();
  }

  pushGTMTags(eventName: string) {
    let superProperties: SuperProperties = {};
    if (this.currentUser) {
      superProperties = this.gtmService.assignLoggedInUsedData(
        this.currentUser
      );
    }

    this.gtmService.gtmEventWithSuperProp({
      eventName: eventName,
      superProps: superProperties,
    });
  }
}
