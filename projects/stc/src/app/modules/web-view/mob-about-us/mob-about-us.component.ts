import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

import { EsportsLanguageService } from 'esports'

@Component({
  selector: 'app-mob-about-us',
  templateUrl: './mob-about-us.component.html',
  styleUrls: ['./mob-about-us.component.scss'],
})
export class MobAboutUsComponent implements OnInit {
  description = [
    { text: 'ABOUTUS.DESCRIPTION1' },
    { text: 'ABOUTUS.DESCRIPTION_NEW' },
    { text: 'ABOUTUS.DESCRIPTION2' },
  ];

  advantages = [
    {
      icon: '/assets/images/AboutUs/about_us_vision.png',
      title: 'ABOUTUS.TITLE_2',
      text: 'ABOUTUS.DESCRIPTION3',
    },
    {
      icon: '/assets/images/AboutUs/about_us_mission.png',
      title: 'ABOUTUS.TITLE_3',
      text: 'ABOUTUS.DESCRIPTION4',
    },
  ];

  constructor(
    public language: EsportsLanguageService,
    public translate: TranslateService,
    private activeRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.activeRoute.queryParams.subscribe((params) => {
      this.language.setLanguage(params?.locale || 'ar');
    });
  }
}
