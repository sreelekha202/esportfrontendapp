import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MobAboutUsComponent } from './mob-about-us.component';

describe('MobAboutUsComponent', () => {
  let component: MobAboutUsComponent;
  let fixture: ComponentFixture<MobAboutUsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MobAboutUsComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MobAboutUsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
