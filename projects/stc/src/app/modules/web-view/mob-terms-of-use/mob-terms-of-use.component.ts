
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EsportsLanguageService } from 'esports'

@Component({
  selector: 'app-mob-terms-of-use',
  templateUrl: './mob-terms-of-use.component.html',
  styleUrls: ['./mob-terms-of-use.component.scss'],
})
export class MobTermsOfUseComponent implements OnInit {
  constructor(
    public language: EsportsLanguageService,
    private activeRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.activeRoute.queryParams.subscribe((params) => {
      this.language.setLanguage(params?.locale || 'ar');
    });
  }
}
