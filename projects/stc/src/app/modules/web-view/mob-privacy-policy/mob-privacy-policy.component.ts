
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EsportsLanguageService } from 'esports';

@Component({
  selector: 'app-mob-privacy-policy',
  templateUrl: './mob-privacy-policy.component.html',
  styleUrls: ['./mob-privacy-policy.component.scss'],
})
export class MobPrivacyPolicyComponent implements OnInit {
  constructor(
    public language: EsportsLanguageService,
    private activeRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.activeRoute.queryParams.subscribe((params) => {
      this.language.setLanguage(params?.locale || 'ar');
    });
  }
}
