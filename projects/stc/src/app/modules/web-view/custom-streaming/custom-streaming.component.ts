import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-custom-streaming',
  templateUrl: './custom-streaming.component.html',
  styleUrls: ['./custom-streaming.component.scss'],
})
export class CustomStreamingComponent implements OnInit {
  params;
  constructor(private activeRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activeRoute.queryParams.subscribe((params) => {
      this.params = params;
    });
  }
}
