import { Component, OnInit } from '@angular/core';
import { TransactionService } from '../../../core/service';
import { ActivatedRoute, Router } from '@angular/router';
import { EsportsConstantsService, EsportsToastService } from 'esports'
import { TempService } from '../../../core/service/temp.service';
@Component({
  selector: 'payment-web-view',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss'],
})
export class PaymentComponent implements OnInit {
  params;
  selectPaymentType = '';
  transaction;
  isProccesing = true;
  paymentMethods = [];
  prizePools = [];
  totalAmounts = [];
  tournament = {
    _id: '',
  };

  constructor(
    private transactionService: TransactionService,
    private eSportsToastService: EsportsToastService,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private constantService: EsportsConstantsService,
    private tempService: TempService
  ) {}

  ngOnInit(): void {
    this.activeRoute.queryParams.subscribe((params) => {
      this.params = params;
      this.tournament._id = this.params?.tournamentId;
      this.tempService.setWebView = this.params;
      this.fetchTransaction();
    });
  }

  fetchTransaction = async () => {
    try {
      const payload = {
        tournamentId: this.params?.tournamentId,
        type: 'organizer',
      };

      const { data } = await this.transactionService.createOrUpdateTransaction(
        payload
      );
      this.totalAmounts.push({
        prizeLabel: 'PRIZE_AMOUNT',
        amount: data.totalAmount - data.platformFee,
      });
      this.totalAmounts.push({
        prizeLabel: 'TOTAL_AMOUNT',
        amount: data.totalAmount,
      });

      this.prizePools = data?.prizeList?.map((item) => {
        let img;
        if (item.name == '1st') {
          img = '../assets/images/payment/Gold.svg';
        } else if (item.name == '2nd') {
          img = '../assets/images/payment/Silver.svg';
        } else if (item.name == '3rd') {
          img = '../assets/images/payment/Bronze.svg';
        } else {
          img = '../assets/images/payment/Bronze.svg';
        }
        return {
          ...item,
          img,
        };
      });
      this.transaction = data;

      if (this.transaction.provider == 'paypal') {
        this.paymentMethods = [
          {
            img: '../assets/images/payment/paypal.png',
            name: 'Paypal',
            value: 'paypal',
          },
        ];
      } else if (this.transaction.provider == 'stcpay') {
        this.paymentMethods = [
          {
            img: '../assets/images/payment/stc_play.png',
            name: 'stc pay',
            value: 'stcpay',
          },
        ];
      }
      if (this.params?.tournamentId) {
        this.paymentMethods.push({
          img: '../assets/images/payment/payfort.png',
          name: 'Payfort',
          value: 'payfort',
        });
      }

      this.isProccesing = false;
    } catch (error) {
      this.eSportsToastService.showError(
        error?.error?.message || error?.message
      );
      this.isProccesing = false;
      this.router.navigate(['/web-view/payment-failure']);
    }
  };

  getPaymentResponse = async (order) => {
    try {
      this.isProccesing = true;
      const payload = {
        tournamentId: this.params?.tournamentId,
        transactionId: this.transaction?._id,
        txnId: order?.id,
        provider: 'paypal',
      };
      const response = await this.transactionService.createOrUpdateTransaction(
        payload
      );
      this.eSportsToastService.showSuccess(response?.message);
      this.router.navigate(['/web-view/payment-success']);
      this.isProccesing = false;
    } catch (error) {
      this.eSportsToastService.showError(
        error?.error?.message || error?.message
      );
      this.isProccesing = false;
      this.router.navigate(['/web-view/payment-failure']);
    }
  };

  getStcPaymentResponse = async (order) => {
    try {
      const payload = {
        tournamentId: this.params?.tournamentId,
        type: 'stcpay',
        mobileNo: order.number.replace(/\s+/g, ''),
      };

      const response = await this.transactionService.createOrUpdateTransaction(
        payload
      );
      this.transaction = response;
      this.eSportsToastService.showSuccess(response?.message);
    } catch (error) {
      this.eSportsToastService.showError(
        error?.error?.message || error?.message
      );
    }
  };

  getStcOtpVerify = async (params) => {
    try {
      const payload = {
        OtpReference: this.transaction['data'][
          'DirectPaymentAuthorizeV4ResponseMessage'
        ]['OtpReference'],
        STCPayPmtReference: this.transaction['data'][
          'DirectPaymentAuthorizeV4ResponseMessage'
        ]['STCPayPmtReference'],
        otp: params,
        id: this.transaction.data.id,
        tournamentId: this.params?.tournamentId,
      };
      const response = await this.transactionService.verifyStcTransaction(
        payload
      );
      this.eSportsToastService.showSuccess(response?.message);
      this.router.navigate(['/web-view/payment-success']);
    } catch (error) {
      this.eSportsToastService.showError(
        error?.error?.message || error?.message
      );
      this.router.navigate(['/web-view/payment-failure']);
    }
  };
}
