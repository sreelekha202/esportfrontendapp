import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { Location } from '@angular/common';

import { AppHtmlTeamRegRoutes } from '../../../../app-routing.model';
import { TournamentService } from '../../../../core/service';
import {
  IUser,
  EsportsUserService,
  EsportsToastService,
  EsportsGtmService,
  SuperProperties,
} from 'esports';
import { IParticipant } from '../../../../shared/models';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import {
  InfoPopupComponent,
  InfoPopupComponentData,
  InfoPopupComponentType,
} from '../../../../shared/popups/info-popup/info-popup.component';
@Component({
  selector: 'app-select-teammates',
  templateUrl: './select-teammates.component.html',
  styleUrls: ['./select-teammates.component.scss'],
})
export class SelectTeammatesComponent implements OnInit, OnDestroy {
  AppHtmlTeamRegRoutes = AppHtmlTeamRegRoutes;

  user: IUser;
  @Input() tournament;
  @Input() team;
  @Input() members: any = [];
  isDisable: boolean = true;
  isProcessing: boolean = false;

  returnData: any = {};
  teamParticipant: any = {};
  selected = [];
  userSubscription: Subscription;

  constructor(
    private location: Location,
    private userService: EsportsUserService,
    private gtmService: EsportsGtmService,
    private tournamentService: TournamentService,
    private translateService: TranslateService,
    private router: Router,
    private matDialog: MatDialog,
    private eSportsToastService: EsportsToastService
  ) {}

  ngOnInit(): void {}
  ngAfterContentInit(): void {
    this.getCurrentUserDetails();
  }

  ngOnDestroy() {
    this.userSubscription?.unsubscribe();
  }

  getCurrentUserDetails() {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.user = data;
        this.membersFunction();
      }
    });
  }

  selectPlayer(playerIndex: number, player): void {
    if (player.userId.Status == 3) {
        const afterBlockData: InfoPopupComponentData = {
        title: this.translateService.instant('TOURNAMENT.ERROR'),
        text: this.translateService.instant('TOURNAMENT.UNABLE_TO_SELECT'),
        type: InfoPopupComponentType.info,
      };
      this.matDialog.open(InfoPopupComponent, { data: afterBlockData });
    } else {
      if (player.userId._id !== this.user._id) {
        let flag = true;

        this.tournamentService
          .fetchParticipantRegistrationStatusMyTeams(
            this.tournament._id,
            player.userId._id
          )
          .subscribe(
            (data) => {
              this.eSportsToastService.showError(data?.message);
            },
            (err) => {
              let totalMemberSize = 0;
              let totalMemberSizeFalse = 0;

              if (this.tournament?.allowSubstituteMember) {
                if (this.tournament?.substituteMemberSize > 0) {
                  totalMemberSize +=
                    this.tournament?.teamSize +
                    this.tournament?.substituteMemberSize;
                } else {
                  totalMemberSize = this.tournament?.teamSize;
                }
              } else {
                totalMemberSize = this.tournament?.teamSize;
                totalMemberSizeFalse =
                  this.tournament?.teamSize +
                  this.tournament?.substituteMemberSize;
              }

              const indexSelected = this.selected.findIndex(
                (x) => x.teamParticipantId === player._id
              );

              if (indexSelected > -1) {
              } else {
                if (
                  this.tournament?.allowSubstituteMember
                    ? this.selected.length >= totalMemberSize - 1
                    : this.selected.length >= totalMemberSizeFalse - 1
                ) {
                  this.eSportsToastService.showError(
                    this.translateService.instant('PROFILE.TEAMS.ERRORS.ERROR8')
                  );
                  flag = false;
                } else {
                }
              }

              if (flag) {
                if (player.userId._id === this.user._id) {
                } else {
                  this.members = this.members.map((player, idx) =>
                    playerIndex === idx
                      ? { ...player, isSelected: !player.isSelected }
                      : player
                  );
                  const index = this.selected.findIndex(
                    (x) => x.teamParticipantId === player._id
                  );
                  if (index > -1) {
                    this.selected.splice(index, 1);
                  } else {
                    const userGames =
                      player?.userId?.preference?.gameDetails || [];
                    const game = userGames.find((game) => {
                      return game._id === this.tournament?.gameDetail?._id;
                    });

                    this.selected.push({
                      userId: player.userId._id,
                      name: player.userId?.fullName,
                      profilePicture: player.userId?.profilePicture,
                      phoneNumber: player.userId.phoneNumber,
                      email: player.userId.email,
                      teamParticipantId: player._id,
                      inGamerUserId: game?.userGameId || '',
                    });
                  }
                }

                if (this.selected.length >= totalMemberSize - 1) {
                  this.isDisable = false;
                } else {
                  this.isDisable = true;
                }
              }
            }
          );
      }
    }
  }

  membersFunction() {
    if (this.members) {
      const index = this.members?.findIndex(
        (x) => x.userId._id === this.user._id
      );
      this.members=this.members.filter(member=> member.userId.Status !=3)
      const userGames = this.user?.preference?.gameDetails || [];
      const game = userGames.find((game) => {
        return game._id === this.tournament?.gameDetail?._id;
      });

      this.teamParticipant = {
        teamId: this.team?._id,
        logo: this.team?.logo,
        teamName: this.team?.teamName,
        name: this.members[index].userId?.fullName,
        profilePicture: this.members[index].userId?.profilePicture,
        phoneNumber: this.members[index].userId.phoneNumber,
        email: this.members[index].userId.email,
        inGamerUserId: game?.userGameId || '',
      };
      this.members[index].isSelected = true;
      //this.members.splice(index, 1);
    }
  }

  goBack() {
    this.pushGTMTags('Cancel_Match_Teammates');
    this.location.back();
  }

  submitMember = async () => {
    try {
      if (this.tournament?.substituteMemberSize > 0) {
        const teamMembers = this.selected.slice(
          0,
          this.tournament?.teamSize - 1
        );
        const substituteMembers = this.selected.slice(
          this.tournament?.teamSize - 1,
          this.tournament?.teamSize - 1 + this.selected.length
        );
        this.teamParticipant['teamMembers'] = teamMembers;
        this.teamParticipant['substituteMembers'] = substituteMembers;
      } else {
        this.teamParticipant['teamMembers'] = this.selected;
        this.teamParticipant['substituteMembers'] = [];
      }
      this.teamParticipant.tournamentId = this.tournament?._id;
      this.teamParticipant.participantType = this.tournament?.participantType;
      this.isProcessing = true;
      const { isProcessing, message } =
        await this.tournamentService.saveParticipantDetails(
          this.teamParticipant,
          this.tournament
        );
      this.pushGTMTags('Confirm_Match_Teammates');

      if (isProcessing) {
        this.tournamentService.setSelectedTeam(this.teamParticipant);
      } else {
        this.eSportsToastService.showSuccess(message);
        this.router.navigate(['tournament', this.tournament?.slug]);
        this.tournamentService.tournamentActiveTab = 3;
      }
      this.isProcessing = false;
    } catch (error) {
      this.isProcessing = false;
      this.eSportsToastService.showSuccess(
        error?.error?.message || error?.message
      );
    }
  };

  pushGTMTags(eventName: string) {
    let superProperties: SuperProperties = {};
    if (this.user) {
      superProperties = this.gtmService.assignLoggedInUsedData(
        this.user
      );
    }
    this.gtmService.gtmEventWithSuperProp({
      eventName: eventName,
      superProps: superProperties,
    });
  }

}
