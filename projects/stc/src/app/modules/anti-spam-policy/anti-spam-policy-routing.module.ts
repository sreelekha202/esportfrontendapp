import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { AntiSpamPolicyComponent } from './anti-spam-policy.component';

const routes: Routes = [{ path: '', component: AntiSpamPolicyComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AntiSpamPolicyRoutingModule {}
