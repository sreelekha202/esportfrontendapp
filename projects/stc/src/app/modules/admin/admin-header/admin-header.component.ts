import {
  AfterViewInit,
  Component,
  OnDestroy,
  OnInit,
  PLATFORM_ID,
  Inject,
} from '@angular/core';
import { Router } from '@angular/router';

import { SidenavService } from '../../../shared/service/sidenav/sidenav.service';
import {
  EsportsUserService,
  EsportsLanguageService,
  EsportsConstantsService,
} from 'esports';
import { isPlatformBrowser } from '@angular/common';
import {
  AppHtmlAdminRoutes,
  AppHtmlRoutes,
  AppHtmlRoutesLoginType,
} from '../../../app-routing.model';

import { IUser, GlobalUtils } from 'esports';

import { faBell } from '@fortawesome/free-solid-svg-icons';
import { MatSidenav } from '@angular/material/sidenav';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';

import { environment } from '../../../../environments/environment';

const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;

@Component({
  selector: 'app-admin-header',
  templateUrl: './admin-header.component.html',
  styleUrls: ['./admin-header.component.scss'],
})
export class AdminHeaderComponent implements OnInit, AfterViewInit, OnDestroy {
  activeLang = this.ConstantsService?.defaultLangCode;
  AppHtmlAdminRoutes = AppHtmlAdminRoutes;
  AppHtmlRoutes = AppHtmlRoutes;
  AppHtmlRoutesLoginType = AppHtmlRoutesLoginType;
  isMenuOpened = false;
  currentUser: IUser;
  AppLanguage = [];
  faBell = faBell;
  isBrowser: boolean;
  userSubscription: Subscription;

  constructor(
    private languageService: EsportsLanguageService,
    private ConstantsService: EsportsConstantsService,
    private router: Router,
    private sidenavService: SidenavService,
    private userService: EsportsUserService,
    public translate: TranslateService,
    @Inject(PLATFORM_ID) private platformId
  ) {
    this.isBrowser = isPlatformBrowser(platformId);
  }

  ngOnInit(): void {
    if (this.isBrowser) {
      this.AppLanguage = this.ConstantsService?.language;
      this.activeLang = this.translate.currentLang;
      this.languageService.setLanguage(this.activeLang);
      this.userSubscription = this.userService.currentUser.subscribe((data) => {
        if (data) {
          this.currentUser = data;
        }
      });
    }
  }

  ngOnDestroy() {
    this.userSubscription?.unsubscribe();
  }

  onLanguageChange(lang): void {
    if (GlobalUtils.isBrowser()) {
      localStorage.setItem('currentLanguage', lang);
      this.activeLang = lang;
      this.translate.use(lang);
      this.languageService.setLanguage(lang);
    }
  }

  async ngAfterViewInit(): Promise<void> {
    const sidenav: MatSidenav = await this.sidenavService.getSidenav();

    if (sidenav) {
      sidenav.openedChange.subscribe((isOpened) => {
        this.isMenuOpened = isOpened;
      });
    }
  }

  onLogOut(): void {
    this.userService.logout(API, TOKEN);
    this.router.navigate([
      AppHtmlRoutes.userPageType,
      AppHtmlRoutesLoginType.phoneLogin,
    ]);
  }

  onMenuToggle(): void {
    this.sidenavService.toggle();
  }
}
