import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';

import { AdminRoutingModule } from './admin-routing.module';
import { CoreModule } from '../../core/core.module';
import { FormComponentModule } from '../../shared/components/form-component/form-component.module';
import { SharedModule } from '../../shared/modules/shared.module';

import { AccessManagementComponent } from './components/access-management/access-management.component';
import { AdminComponent } from './admin.component';
import { AdminHeaderComponent } from './admin-header/admin-header.component';
import { AdminMenuComponent } from './admin-menu/admin-menu.component';
import { AdvertisementManagementComponent } from './components/advertisement-management/advertisement-management.component';
import { AnnouncementConfigurationComponent } from './components/announcement-configuration/announcement-configuration.component';
import { ArticlesDetailsComponent } from './components/articles-details/articles-details.component';
import { AutomatedDialogComponent } from './components/esports-payment-details/automated-dialog/automated-dialog.component';
import { BannerComponent } from './components/banner/banner.component';
import { ContentManagementComponent } from './components/content-management/content-management.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { EmailNotificationsComponent } from './components/user-notifications/email-notifications/email-notifications.component';
import { EsportsManagementComponent } from './components/esports-management/esports-management.component';
import { EsportsPaymentComponent } from './components/esports-payment/esports-payment.component';
import { EsportsPaymentDetailsComponent } from './components/esports-payment-details/esports-payment-details.component';
import { GameComponent } from './components/game/game.component';
import { InboxNotificationsComponent } from './components/user-notifications/inbox-notifications/inbox-notifications.component';
import { LanguageConfigurationComponent } from './components/language-configuration/language-configuration/language-configuration.component';
import { LeaderboardConfigurationComponent } from './components/leaderboard-configuration/leaderboard-configuration.component';
import { PaymentDisbursementDialogComponent } from './components/esports-payment-details/payment-disbursement-dialog/payment-disbursement-dialog.component';
import { PaymentSuccessfullDialogComponent } from './components/esports-payment-details/payment-successfull-dialog/payment-successfull-dialog.component';
import { PushNotificationsComponent } from './components/user-notifications/push-notifications/push-notifications.component';
import { RegionConfigurationComponent } from './components/region-configuration/region-configuration.component';
import { RewardConfigurationComponent } from './components/reward-configuration/reward-configuration.component';
import { SendCouponDialogComponent } from './components/esports-payment-details/send-coupon-dialog/send-coupon-dialog.component';
import { SentNotificationsComponent } from './components/user-notifications/sent-notifications/sent-notifications.component';
import { ShopConfigurationComponent } from './components/shop-configuration/shop-configuration.component';
import { SiteConfigurationComponent } from './components/site-configuration/site-configuration.component';
import { SpamManagementComponent } from './components/spam-management/spam-management.component';
import { TeamManagementComponent } from './components/team-management/team-management.component';
import { TeamManagementEditComponent } from './components/team-management-edit/team-management-edit.component';
import { TournamentDetailsComponent } from './components/tournament-details/tournament-details.component';
import { UserDetailsComponent } from './components/user-details/user-details.component';
import { UserManagementComponent } from './components/user-management/user-management.component';
import { UserNotificationsComponent } from './components/user-notifications/user-notifications.component';
import { ViewAdvertisementComponent } from './components/view-advertisement/view-advertisement.component';
import { ViewAdvertisementModelComponent } from './popups/view-advertisement-model/view-advertisement-model.component';
import { ViewUserAccessComponent } from './components/view-user-access/view-user-access.component';
import { ViewUserAccessPopupComponent } from './popups/view-user-access/view-user-access-popup.component';
import { ViewUserDetailsComponent } from './components/view-user-details/view-user-details.component';
import { NgCircleProgressModule } from 'ng-circle-progress';
import { RouterBackModule } from '../../shared/directives/router-back.module';
import { TeamCardInviteModule } from '../profile/components/team-card-invite/team-card-invite.module';
import { PrizeDisbursalModule } from './components/prize-disbursal/prize-disbursal.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { EsportsModule, WYSIWYGEditorModule } from 'esports';
import { environment } from '../../../environments/environment';
import { PrizemoneyRefundComponent } from './components/prizemoney-refund/prizemoney-refund.component';
import { RefundDetailsComponent } from './components/prizemoney-refund/refund-details/refund-details.component';
import { LazyLoadImageModule } from 'ng-lazyload-image';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { JwtInterceptor } from '../../core/helpers/interceptors/token-interceptors.service';
import { HighlightedComponent } from './components/content-management/highlighted/highlighted.component';
import { PendingComponent } from './components/content-management/pending/pending.component';
import { PublishedComponent } from './components/content-management/published/published.component';

@NgModule({
  declarations: [
    AccessManagementComponent,
    AdminComponent,
    AdminHeaderComponent,
    AdminMenuComponent,
    AdvertisementManagementComponent,
    AnnouncementConfigurationComponent,
    ArticlesDetailsComponent,
    AutomatedDialogComponent,
    BannerComponent,
    ContentManagementComponent,
    DashboardComponent,
    EmailNotificationsComponent,
    EsportsManagementComponent,
    EsportsPaymentComponent,
    EsportsPaymentDetailsComponent,
    GameComponent,
    InboxNotificationsComponent,
    LanguageConfigurationComponent,
    LeaderboardConfigurationComponent,
    PaymentDisbursementDialogComponent,
    PaymentSuccessfullDialogComponent,
    PushNotificationsComponent,
    RegionConfigurationComponent,
    RewardConfigurationComponent,
    SendCouponDialogComponent,
    SentNotificationsComponent,
    ShopConfigurationComponent,
    SiteConfigurationComponent,
    SpamManagementComponent,
    TeamManagementComponent,
    TeamManagementEditComponent,
    TournamentDetailsComponent,
    UserDetailsComponent,
    UserManagementComponent,
    UserNotificationsComponent,
    ViewAdvertisementComponent,
    ViewAdvertisementModelComponent,
    ViewUserAccessComponent,
    ViewUserAccessPopupComponent,
    ViewUserDetailsComponent,
    PrizemoneyRefundComponent,
    RefundDetailsComponent,
    HighlightedComponent,
    PendingComponent,
    PublishedComponent,
  ],
  imports: [
    AdminRoutingModule,
    CoreModule,
    FormComponentModule,
    FormsModule,
    NgCircleProgressModule.forRoot({}),
    SharedModule,
    RouterBackModule,
    TeamCardInviteModule,
    PrizeDisbursalModule,
    NgSelectModule,
    LazyLoadImageModule,
    WYSIWYGEditorModule.forRoot(environment),
    EsportsModule.forRoot(environment)
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptor,
      multi: true,
    }
  ]
})
export class AdminModule { }
