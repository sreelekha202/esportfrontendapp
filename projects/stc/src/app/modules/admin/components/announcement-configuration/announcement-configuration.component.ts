import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { EsportsAdminService, GlobalUtils } from 'esports';
import { environment } from '../../../../../environments/environment';

const API = environment.apiEndPoint;

import {
  InfoPopupComponent,
  InfoPopupComponentData,
  InfoPopupComponentType,
} from '../../../../shared/popups/info-popup/info-popup.component';

import { MatDialog } from '@angular/material/dialog';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-announcement-configuration',
  templateUrl: './announcement-configuration.component.html',
  styleUrls: [
    './announcement-configuration.component.scss',
    '../site-configuration/site-configuration.component.scss',
  ],
})
export class AnnouncementConfigurationComponent implements OnInit {
  imageError = false;
  isActive = false;
  isLoading = true;
  showAdList = true;

  public ownerForm: FormGroup;

  announcementList: any = [];
  announcementUri: any = '';
  isEditable = {};

  _id = 0;

  /**
   * ViewChild check of announcement image object
   */
  @ViewChild('announcementUpload', { static: false }) set announcementUpload(
    element
  ) {
    if (GlobalUtils.isBrowser() && element && this.announcementUri) {
      document.getElementById('announcementUpload').style.background =
        'url(' + this.announcementUri + ')';
    }
  }

  /**
   * constructor of the page
   * @param dialog
   * @param adminService
   */
  constructor(
    private adminService: EsportsAdminService,
    private dialog: MatDialog,
    private translateService: TranslateService
  ) {}

  /**
   * Page Initialization
   */
  ngOnInit(): void {
    this.ownerForm = new FormGroup({
      name: new FormControl('', [Validators.required]),
      en_header: new FormControl('', [Validators.required]),
      ar_header: new FormControl('', [Validators.required]),
      en_description: new FormControl('', [Validators.required]),
      ar_description: new FormControl('', [Validators.required]),
      platform: new FormControl('', [Validators.required]),
      destination: new FormControl('', [
        Validators.pattern(
          '(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?'
        ),
      ]),
    });
    this.getAnnouncements();
  }

  /**
   * method to change the status or order of announcements
   * @param e
   * @param u_id
   */
  updateRow(e, type, u_id) {
    this.isEditable = {};
    if (type === 'status') {
      this.isLoading = true;

      this.adminService
        .updateAnnouncement(API, { status: e.checked ? 1 : 0 }, u_id)
        .subscribe(
          (res: any) => {
            this.getAnnouncements();
            const afterBlockData: InfoPopupComponentData = {
              title: this.translateService.instant(
                'ANNOUNCEMENT.SUCCESS_HEADER'
              ),
              text: res.message,
              type: InfoPopupComponentType.info,
            };
            this.dialog.open(InfoPopupComponent, { data: afterBlockData });
          },
          (err: any) => {
            this.isLoading = false;
            this.getAnnouncements();

            const afterBlockData: InfoPopupComponentData = {
              title: this.translateService.instant('ANNOUNCEMENT.ERROR_HEADER'),
              text: err?.error?.message || err.error.message,
              type: InfoPopupComponentType.info,
            };
            this.dialog.open(InfoPopupComponent, { data: afterBlockData });
          }
        );
    } else if (type === 'delete') {
      this.isLoading = true;

      this.adminService.deleteAnnouncement(API, u_id).subscribe(
        (res: any) => {
          this.getAnnouncements();
          const afterBlockData: InfoPopupComponentData = {
            title: this.translateService.instant('ANNOUNCEMENT.SUCCESS_HEADER'),
            text:res.message,
            type: InfoPopupComponentType.info,
          };
          this.dialog.open(InfoPopupComponent, { data: afterBlockData });
        },
        (err: any) => {
          this.isLoading = false;
          this.getAnnouncements();
          const afterBlockData: InfoPopupComponentData = {
            title: this.translateService.instant('ANNOUNCEMENT.ERROR_HEADER'),
            text:
              this.translateService.instant(
                `ANNOUNCEMENT.${err.error.messageCode}`
              ) || err.error.message,
            type: InfoPopupComponentType.info,
          };
          this.dialog.open(InfoPopupComponent, { data: afterBlockData });
        }
      );
    }
  }

  /**
   * method to upload an image object in page
   */
  announcementUploader() {
    if (GlobalUtils.isBrowser()) {
    const announcement: any = document.getElementById('announcementFileUpload');
    announcement.click();

    announcement.onchange = function () {
      const reader = new FileReader();

      reader.onload = function (event: any) {
        const img = new Image();

        img.onload = function () {
          this.announcementUri = event.target.result;
          this.imageError = false;
          document.getElementById('announcementUpload').style.background =
            'url(' + event.target.result + ')';
        }.bind(this);

        img.onerror = function () {
          alert('not a valid file: ' + announcement.files[0].type);
        };

        img.src = window.URL.createObjectURL(announcement.files[0]);
      }.bind(this);
      reader.readAsDataURL(announcement.files[0]);
    }.bind(this);
  }
  }

  /**
   * method to get list of all the announcements with active status
   */
  getAnnouncements() {
    this.isLoading = true;
    this.adminService.getAnnouncement(API).subscribe((res: any) => {
      this.announcementList = [...res.data];
      this.isLoading = false;
      this.showAdList = true;
    });
  }

  /**
   * method to check the form controls the validations
   * @param controlName
   * @param errorName
   */
  public hasError = (controlName: string, errorName: string) => {
    return this.ownerForm.controls[controlName].hasError(errorName);
  };

  /**
   * method to respond to the performing actions
   * @param type
   * @param data
   */
  rowAction(type, data) {
    if (type === 'view') {
      this.viewAd(data);
    }
  }

  /**
   * method to toggle the components on the page i.e. the table and the form
   */
  toggleAnnouncementList() {
    this.resetAnnouncementControls();
    this.showAdList = !this.showAdList;
  }

  /**
   * method to be called before saving the data to check the form status
   * and the destination url
   */
  upload() {
    if (GlobalUtils.isBrowser()) {
      if (this.ownerForm.invalid) { 
        return;
      } else {
        this.uploadAnnouncement();
      }
      // else if (!this.announcementUri) {
      //   this.imageError = true;
      //   return;
      // }

      // if (this.ownerForm.value.destination) {
      //   const _url = this.ownerForm.value.destination;
      //   window
      //     .fetch(_url)
      //     .then((response) => {
      //       if (response.status == 200) {
      //         this.uploadAnnouncement();
      //       }
      //     })
      //     .catch((err) => {
      //       this.ownerForm.controls.destination.setErrors({ pattern: true });
      //       return;
      //     });
      // }
    }
  }

  /**
   * method to be called after form check to upload the data
   */
  uploadAnnouncement() {
    // if (this.announcementUri) {
      let tempDesc = {
        "en": this.ownerForm.value.en_description,
        "ar": this.ownerForm.value.ar_description
      }
      let tempHeader = {
        "en": this.ownerForm.value.en_header,
        "ar": this.ownerForm.value.ar_header
      }
      const formData = {
        announcementFileUrl: (this.announcementUri) ? this.announcementUri : '',
        description: tempDesc,
        destination: this.ownerForm.value.destination,
        header: tempHeader,
        name: this.ownerForm.value.name,
        announcementPlatform: this.ownerForm.value.platform,
      };

      this.isLoading = true;
      let subscriber;

      subscriber =
        this._id != 0
          ? this.adminService.updateAnnouncement(API, formData, this._id)
          : this.adminService.addannouncement(API, formData);

      subscriber.subscribe(
        (res: any) => {
          const afterBlockData: InfoPopupComponentData = {
            title: this.translateService.instant(`ANNOUNCEMENT.SUCCESS_HEADER`),
            text: res.message,
            type: InfoPopupComponentType.info,
          };

          this.dialog.open(InfoPopupComponent, { data: afterBlockData });
          this.isLoading = false;
          this.resetAnnouncementControls();
          this.getAnnouncements();
        },
        (err) => {
          const afterBlockData: InfoPopupComponentData = {
            title: this.translateService.instant('ANNOUNCEMENT.ERROR_HEADER'),
            text: err.error.message,
            type: InfoPopupComponentType.info,
          };
          this.dialog.open(InfoPopupComponent, { data: afterBlockData });
        }
      );
   // }
  }

  /**
   * method to be called to view the row data of the table in the form
   * @param rowData
   */
  viewAd(rowData) {
    let tempPlatform;
    if(rowData.announcementPlatform.length == 2 ) {
      tempPlatform = 'mobile';
    } else if(rowData.announcementPlatform.length == 1 &&  rowData.announcementPlatform[0] == 'ios') {
      tempPlatform = 'ios';
    } else if(rowData.announcementPlatform.length == 1 &&  rowData.announcementPlatform[0] == 'web') {
      tempPlatform = 'web';
    } else if(rowData.announcementPlatform.length == 1 &&  rowData.announcementPlatform[0] == 'android') {
      tempPlatform = 'android';
    } else {
        tempPlatform = 'all';
    }
    this.ownerForm.setValue({
      en_description: rowData.description.en,
      ar_description: rowData.description.ar,
      destination: rowData.destination,
      ar_header: rowData.header.ar,
      en_header: rowData.header.en,
      name: rowData.name,
      platform: tempPlatform,
    });

    this._id = rowData._id;
    this.announcementUri = rowData.announcementFileUrl;
    this.isActive = rowData.status;
    this.ownerForm.setErrors(null);
    this.showAdList = false;
  }

  /**
   * method to be called before to reset the form controls
   */
  resetAnnouncementControls() {
    this.announcementUri = '';
    this._id = 0;
    this.isActive = false;
    this.imageError = false;
    this.ownerForm.reset();
  }
}