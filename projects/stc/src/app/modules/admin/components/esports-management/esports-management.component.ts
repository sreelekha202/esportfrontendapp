import { Component, OnInit, EventEmitter } from '@angular/core';
import { MatTabChangeEvent } from '@angular/material/tabs';
import {
  InfoPopupComponentData,
  InfoPopupComponentType,
  InfoPopupComponent,
} from '../../../../shared/popups/info-popup/info-popup.component';
import { MatDialog } from '@angular/material/dialog';
import { AppHtmlRoutes } from '../../../../app-routing.model';
import { TranslateService } from '@ngx-translate/core';
import { ActivatedRoute, Router } from '@angular/router';
import {
  EsportsTournamentService,
  EsportsAdminService,
  EsportsConstantsService,
  EsportsToastService,
  IPagination,
} from 'esports';
import { environment } from '../../../../../environments/environment';
import { debounceTime } from 'rxjs/operators';

const API = environment.apiEndPoint;

@Component({
  selector: 'app-esports-management',
  templateUrl: './esports-management.component.html',
  styleUrls: ['./esports-management.component.scss'],
})
export class EsportsManagementComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;

  checkIconShow: boolean = false;
  deleteIconShow: boolean = false;
  editIconShow: boolean = false;
  isLoading: boolean = false;
  manageIconShow: boolean = false;
  viewIconShow: boolean = false;
  IAMAFFTournament: boolean = false;

  activeTabIndex;
  page: IPagination;
  userId;

  selectedTab: string = 'ongoing';
  statusText: string = EsportsConstantsService.Status.Live;

  allTournamentList: any = [];
  finishedTournament: any = [];
  tournamentList = [];
  text: string;
  paginationData = {
    page: 1,
    limit: 50,
    sort: 'startDate',
  };
  timeoutId = null;
  customTabEvent = new EventEmitter<MatTabChangeEvent>();

  constructor(
    private activeRoute: ActivatedRoute,
    private dialog: MatDialog,
    private router: Router,
    private tournamentService: EsportsTournamentService,
    private translateService: TranslateService,
    private eSportsToastService: EsportsToastService,
    private esportsAdminService: EsportsAdminService
  ) {}

  ngOnInit(): void {
    this.activeRoute.queryParams
      .subscribe((params) => {
        if (params.activeTab) {
          this.activeTabIndex = +params.activeTab;
          this.switchData(this.activeTabIndex);
        } else {
          this.activeTabIndex = 0;
          this.getData('1', 'Latest');
          this.setConfiguration(true, true, false, false, true);
        }
      })
      .unsubscribe();
    this.fetchOptions();
    this.customTabEvent
      .pipe(debounceTime(30))
      .subscribe((tabChangeEvent: MatTabChangeEvent) => {
        this.activeTabIndex = tabChangeEvent.index;
        this.router.navigate(['.'], {
          relativeTo: this.activeRoute,
          queryParams: { activeTab: tabChangeEvent.index },
        });
        this.switchData(tabChangeEvent.index);
      });
  }

  pageChanged(page): void {
    this.paginationData.page = page;
    switch (this.activeTabIndex) {
      case 0:
        this.getData('1', 'Latest'); //ongoing
        break;
      case 1:
        this.getData('0', 'Latest'); // upcoming
        break;
      case 2:
        this.getData('2', 'Latest'); //past
        break;
      case 3:
        this.getData('5', 'Latest'); // Approval pending
        break;
      case 4:
        this.getData('10', 'Oldest'); // payament  pending
        // this.getTournamentForPendingPayments();
        break;
      default:
        break;
    }
  }

  /**
   * method to update the modifications on tournaments
   * @param data
   */
  modifyTournamentHandler = async (data: any) => {
    try {
      if (data?.type === 'abort') {
        this.switchData(this.activeTabIndex);
      } else if (data.type === 'feature') {
        const response = await this.tournamentService.updateTournament(
          { isFeature: data?.value },
          data?.id
        );
        const afterBlockData: InfoPopupComponentData = {
          title: this.translateService.instant(
            'API.TOURNAMENT.PUT.SUCCESS_HEADER'
          ),
          text: response?.message,
          type: InfoPopupComponentType.info,
        };
        this.dialog.open(InfoPopupComponent, { data: afterBlockData });
        this.getData('0', 'Latest');
      } else if (data?.type === 'update') {
        this.switchData(data?.value);
        if (data.value == 3) {
          this.getData('5', 'Latest');
        } else {
          this.switchData(data?.value);
        }
      } else if (data?.type === 'updatePaymentTable') {
        //this.getTournamentForPendingPayments();
        this.getData('10', 'Oldest');
      } else if (data?.type === 'GWB') {
        const response = await this.tournamentService
          .updateGWBTournament(API, { level: data?.value }, data?.id)
          .toPromise();
        if (data?.tabIndex == 0) {
          this.getData(1, 'Latest');
        } else if (data?.tabIndex == 1) {
          this.getData(0, 'Latest');
        } else {
          this.getData(data?.tabIndex, 'Latest');
        }
        const afterBlockData: InfoPopupComponentData = {
          title: this.translateService.instant(
            'API.TOURNAMENT.PUT.SUCCESS_HEADER'
          ),
          text: response?.message,
          type: InfoPopupComponentType.info,
        };
        this.dialog.open(InfoPopupComponent, { data: afterBlockData });
      } else if (data?.text) {
        this.text = data?.text;
        this.paginationData.page = data?.page || 1;
        this.paginationData.limit = 50;
        this.getData(data?.status, 'Latest');
      }
    } catch (error) {
      const errorMessage = error?.error?.message || error?.message;
      const afterBlockData: InfoPopupComponentData = {
        title: this.translateService.instant('API.TOURNAMENT.PUT.ERROR_HEADER'),
        text: errorMessage,
        type: InfoPopupComponentType.info,
      };
      this.dialog.open(InfoPopupComponent, { data: afterBlockData });
    }
  };

  /**
   * Get the tournament list based on Tab selection
   * @param tabChangeEvent : Event on Change of Tab
   */
  onTabChanged = (tabChangeEvent: MatTabChangeEvent): void => {
    this.customTabEvent.emit(tabChangeEvent);
  };

  switchData(index) {
    switch (index) {
      case 0:
        this.resetPage();
        this.setConfiguration(true, true, false, false, true);
        this.getData('1', 'Latest'); //ongoing
        break;
      case 1:
        this.resetPage();
        this.setConfiguration(true, true, false, true, true);
        this.getData('0', 'Latest'); // upcoming
        break;
      case 2:
        this.resetPage();
        this.setConfiguration(true, true, false, false, true);
        this.getData('2', 'Latest'); //past
        break;
      case 3:
        this.resetPage();
        this.setConfiguration(true, true, true, true, false);
        this.getData('5', 'Latest');
        break;
      case 4:
        this.resetPage1();
        this.getData('10', 'Oldest');
        // this.getTournamentForPendingPayments();
        break;
      default:
        break;
    }
  }
  resetPage1() {
    this.paginationData = {
      page: 1,
      limit: 50,
      sort: 'Oldest',
    };
    this.tournamentList = [];
  }

  resetPage() {
    this.paginationData = {
      page: 1,
      limit: 50,
      sort: 'startDate',
    };
    this.tournamentList = [];
  }

  getData = async (status, sortOrder) => {
    try {
      const params = {
        limit: this.paginationData.limit,
        page: this.paginationData.page,
        status: status,
        sort: sortOrder,
        ...(status == 5 && {
          tournamentStatus: 'submitted_for_approval',
        }),
        ...(this.text && {
          text: this.text,
        }),
      };
      let response;

      this.isLoading = true;

      if (status == 5 || status == 10) {
        this.tournamentList = [];
        response = await this.tournamentService
          .getPaginatedTournaments(API, params)
          .toPromise();
      } else {
        if (params.text) {
          response = await this.tournamentService.fetchTournamentByStatus(
            API,
            params
          );
        } else {
          this.tournamentList = [];
          response = await this.tournamentService.fetchTournamentByStatus(
            API,
            params
          );
        }
        this.text = '';
      }

      this.mapTournamentFilterList(response?.data?.docs);
      this.page = {
        totalItems: response?.data?.totalDocs,
        itemsPerPage: response?.data?.limit,
        maxSize: 5,
      };
      this.isLoading = false;
    } catch (error) {
      this.isLoading = false;
    }
  };

  /**
   * Map the tournament details to display in the table
   * @param tournamentList : response from method
   */
  mapTournamentFilterList(tournamentList) {
    const response = tournamentList.map((element) => ({
      _id: element._id,
      game: element.gameDetail?.name,
      isFeature: element.isFeature,
      isPaid: element.isPaid,
      maxParticipants: element.maxParticipants,
      name: element.name,
      region: element.regionsAllowed,
      slug: element.slug,
      startDate: element.startDate,
      tournamentType: element.tournamentType,
      level: element.level,
    }));

    this.tournamentList = response;
  }

  /**
   * Configure icon based on boolean value
   * @param viewIcon : show/hide icon
   * @param deleteIcon:show/hide icon
   * @param checkIcon:show/hide icon
   */
  setConfiguration(viewIcon, deleteIcon, checkIcon, editIcon, manageIcon) {
    this.viewIconShow = viewIcon;
    this.deleteIconShow = deleteIcon;
    this.checkIconShow = checkIcon;
    this.editIconShow = editIcon;
    this.manageIconShow = manageIcon;
  }

  fetchOptions = async () => {
    try {
      const { data } = await this.esportsAdminService.getOptions(API);

      this.IAMAFFTournament = data?.IAMAFFTournament;
    } catch (error) {
      this.eSportsToastService.showError(
        error?.error?.message || error?.message
      );
    }
  };

  getModifiedOptions = async (body) => {
    try {
      const optionUpdate = async () => {
        try {
          const { data, message } = await this.esportsAdminService.patchOptions(
            API,
            body
          );

          this.IAMAFFTournament = data?.IAMAFFTournament;

          this.eSportsToastService.showSuccess(message);
        } catch (error) {
          clearTimeout(this.timeoutId);
          this.eSportsToastService.showError(
            error?.error?.message || error?.message
          );
        }
      };

      if (this.timeoutId) clearTimeout(this.timeoutId);

      this.timeoutId = setTimeout(optionUpdate, 1000);
    } catch (error) {
      this.eSportsToastService.showError(
        error?.error?.message || error?.message
      );
    }
  };
}
