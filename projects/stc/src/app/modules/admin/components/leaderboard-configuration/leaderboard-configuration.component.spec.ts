import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeaderboardConfigurationComponent } from './leaderboard-configuration.component';

describe('LeaderboardConfigurationComponent', () => {
  let component: LeaderboardConfigurationComponent;
  let fixture: ComponentFixture<LeaderboardConfigurationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LeaderboardConfigurationComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeaderboardConfigurationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
