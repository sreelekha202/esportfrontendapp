import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentDisbursementDialogComponent } from './payment-disbursement-dialog.component';

describe('PaymentDisbursementDialogComponent', () => {
  let component: PaymentDisbursementDialogComponent;
  let fixture: ComponentFixture<PaymentDisbursementDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PaymentDisbursementDialogComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentDisbursementDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
