import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutomatedDialogComponent } from './automated-dialog.component';

describe('AutomatedDialogComponent', () => {
  let component: AutomatedDialogComponent;
  let fixture: ComponentFixture<AutomatedDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AutomatedDialogComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutomatedDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
