import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

import { AutomatedDialogComponent } from './automated-dialog/automated-dialog.component';
import { PaymentDisbursementDialogComponent } from './payment-disbursement-dialog/payment-disbursement-dialog.component';
import { PaymentSuccessfullDialogComponent } from './payment-successfull-dialog/payment-successfull-dialog.component';
import { SendCouponDialogComponent } from './send-coupon-dialog/send-coupon-dialog.component';
import {
  InfoPopupComponent,
  InfoPopupComponentData,
  InfoPopupComponentType,
} from '../../../../shared/popups/info-popup/info-popup.component';
import { EsportsAdminService } from 'esports';
import { MatDialog } from '@angular/material/dialog';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { environment } from '../../../../../environments/environment';

const API = environment.apiEndPoint;

@Component({
  selector: 'app-esports-payment-details',
  templateUrl: './esports-payment-details.component.html',
  styleUrls: ['./esports-payment-details.component.scss'],
})
export class EsportsPaymentDetailsComponent implements OnInit {
  @Input() tournamentId: any;
  @Output() isToggle = new EventEmitter();

  isLoading: boolean = true;
  isTransRequired: boolean = false;

  isPaid: any;
  transactionValue: any;

  paypalObject: any = {};
  sportDetail = [];
  tournamentDetails = [];

  organizer = {
    check: 'assets/images/Special-offer/right-mark.svg',
    email: 'Joan04@gmail.com',
    globalFees: '200 MYR',
    mobileNo: '915595652',
    organizerImage: 'assets/images/Profile/stc_avatar.png',
    organizerName: 'joan doe',
    paymentDate: '15 sep 20',
    paymentMethod: 'assets/images/payment/stc_play.png',
    prizeAmount: '1800 MYR',
    status: 'in Escrow',
    total: '2000',
  };

  winners = [
    {
      check: 'assets/images/Special-offer/right-mark.svg',
      name: 'winner Name',
      paymentMethod: 'assets/global-images/logo.svg',
      prize: '100 MYR',
      rank: '1',
      winnerImage: 'assets/images/Profile/stc_avatar.png',
    },
    {
      check: 'assets/images/Special-offer/right-mark.svg',
      name: 'winner Name',
      paymentMethod: 'assets/images/Special-offer/boost.png',
      prize: '100 MYR',
      rank: '1',
      winnerImage: 'assets/images/Profile/stc_avatar.png',
    },
  ];

  columnsFirstTab = [
    { name: 'Tournament Name' },
    { name: 'Registration' },
    { name: 'Game' },
    { name: 'Region' },
    { name: 'Type' },
    { name: 'Start Date' },
    { name: 'End Date' },
    { name: 'Participant(s)' },
    { name: '' },
  ];

  constructor(
    private adminService: EsportsAdminService,
    private modalService: NgbModal,
    private translateService: TranslateService,
    public dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.getWinnersList();
  }

  getWinnersList() {
    const query = {
      filter: {
        tournament: this.tournamentId,
        isAward: true,
        awardPosition: 3,
      },
      option: {
        sort: { awardPosition: 1 },
      },
    };

    this.isLoading = true;

    this.adminService.getTournamentWinners(API, query).subscribe(
      (res: any) => {
        this.isLoading = false;
        if (res.data.length > 0) {
          this.sportDetail = res.data;
          this.tournamentDetails = [res.data[0].tournament];
        }
      },
      (err: any) => {}
    );
  }

  cancel() {
    this.isToggle.emit({ toggle: false });
  }

  disburseAmount(index, type, model) {
    if (type == 'stcpay') {
      const formData = {
        tournamentId: this.tournamentId,
        mobileNumber: this.sportDetail[index].winnerUser.accountDetail
          .paymentAccountId,
        playerId: this.sportDetail[index].winnerUser._id,
      };

      this.isLoading = true;

      this.adminService.stcCall(API, formData).subscribe(
        (res: any) => {
          this.isLoading = false;
          const afterBlockData: InfoPopupComponentData = {
            title: this.translateService.instant(
              'API.PRODUCT.POST.SUCCESS_HEADER'
            ),
            text: this.translateService.instant(res.messageCode) || res.message,
            type: InfoPopupComponentType.info,
          };
          this.dialog.open(InfoPopupComponent, { data: afterBlockData });
          this.getWinnersList();
        },
        (err: any) => {
          this.isLoading = false;
          this.getWinnersList();
        }
      );
    } else if (type == 'paypal') {
      this.paypalObject.tournamentId = this.tournamentId;
      this.paypalObject.playerId = this.sportDetail[index].winnerUser._id;
      this.paypalObject.emailId = this.sportDetail[
        index
      ].winnerUser.accountDetail.paymentAccountId;
      this.update();
    }
  }

  update() {
    if (!this.transactionValue) {
      this.isTransRequired = false;
      this.modalService.dismissAll();
      this.isLoading = true;

      this.adminService.paypalCall(API, this.paypalObject).subscribe(
        (res: any) => {
          this.isLoading = false;
          const afterBlockData: InfoPopupComponentData = {
            title: this.translateService.instant(
              'API.PRODUCT.POST.SUCCESS_HEADER'
            ),
            text: this.translateService.instant(res.messageCode) || res.message,
            type: InfoPopupComponentType.info,
          };
          this.dialog.open(InfoPopupComponent, { data: afterBlockData });
          this.paypalObject = {};
          this.transactionValue = '';
          this.getWinnersList();
        },
        (err: any) => {
          this.isLoading = false;
        }
      );
    } else {
      this.isTransRequired = true;
    }
  }

  openDialog = async () => {
    try {
      const pdResponse = await this.dialog
        .open(PaymentDisbursementDialogComponent, {
          data: 5,
          panelClass: 'custom-dialog-container',
        })
        .afterClosed()
        .toPromise();
      if (pdResponse == 'automated') {
        const AResponse = await this.dialog
          .open(AutomatedDialogComponent, {
            data: 2000,
            panelClass: 'custom-dialog-container',
          })
          .afterClosed()
          .toPromise();
        if (AResponse == 'cancel') {
          const canResponse = await this.dialog
            .open(PaymentDisbursementDialogComponent, {
              data: 5,
              panelClass: 'custom-dialog-container',
            })
            .afterClosed()
            .toPromise();
        }
      } else if (pdResponse == 'coupon') {
        const CResponse = await this.dialog
          .open(SendCouponDialogComponent, {
            data: 5,
            panelClass: 'custom-dialog-container',
          })
          .afterClosed()
          .toPromise();
        if (CResponse == 'successfull') {
          const SResponse = await this.dialog
            .open(PaymentSuccessfullDialogComponent, {
              data: 5,
              panelClass: 'custom-dialog-container',
            })
            .afterClosed()
            .toPromise();
        } else if (CResponse == 'cancel') {
          const canResponse = await this.dialog
            .open(PaymentDisbursementDialogComponent, {
              data: 5,
              panelClass: 'custom-dialog-container',
            })
            .afterClosed()
            .toPromise();
        }
      }
    } catch (error) {}
  };
}
