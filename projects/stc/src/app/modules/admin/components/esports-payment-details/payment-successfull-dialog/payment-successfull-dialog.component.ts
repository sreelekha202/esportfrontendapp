import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-payment-successfull-dialog',
  templateUrl: './payment-successfull-dialog.component.html',
  styleUrls: [
    './payment-successfull-dialog.component.scss',
    '../esports-payment-details.component.scss',
  ],
})
export class PaymentSuccessfullDialogComponent implements OnInit {
  paymentSuccessfull = {
    successImage: 'assets/images/Register/successfull.svg',
  };

  constructor() {}

  ngOnInit(): void {}
}
