import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { EsportsAdminService, EsportsUserService, EsportsGameService } from 'esports';
import { environment } from '../../../../../environments/environment';
import * as _moment from 'moment';
import {MomentDateAdapter, MAT_MOMENT_DATE_ADAPTER_OPTIONS} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import { MatDatepicker } from '@angular/material/datepicker';
import { Moment } from 'moment';

const API = environment.apiEndPoint;
export const MY_FORMATS = {
  parse: {
    dateInput: 'MM-DD-YYYY',
  },
  display: {
    dateInput: 'MM-DD-YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY', 
  },
};


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  providers: [
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    },

    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
  ]
})
export class DashboardComponent implements OnInit {
  public ownerForm: FormGroup;
  @ViewChild('fromDatepicker') fromDatePicker: MatDatepicker<Moment>;
  @ViewChild('toDatepicker') toDatepicker: MatDatepicker<Moment>;
  currentDate: Date = new Date();

  countryList = [];
  gameList = [];
  stateList = [];
  dateData={
    toDate:'',
    fromDate:'' 
  }
  maxStartDateValue = this.setTodaysDate();
  minStartDateValue = this.setTodaysDate();

  dashboardData: any;
  tornamentDetails: any;
  onGoingTournamentPer = 0;

  filter = { country: '', region: '', gameId: '' };

  constructor(
    private adminService: EsportsAdminService,
    private fb: FormBuilder,
    private gameService: EsportsGameService,
    private userService: EsportsUserService
  ) {}

  ngOnInit(): void {
    this.ownerForm = this.fb.group({
      game: [''],
      country: [''],
      region: [''],
    });
    this.loadData();
    this.getGames();
    this.fetchCountries();
  }

  setTodaysDate() {
    return new Date();
  }

  loadData() {
    this.adminService.getCountsOnfilter(API, this.filter).subscribe((res: any) => {
      this.dashboardData = res.data;
      this.getAnalytics(null);
      if (
        this.dashboardData &&
        this.dashboardData.ongoingTournamentsCount &&
        this.dashboardData.upComingTournamentsCount
      ) {
        const total =
          this.dashboardData?.ongoingTournamentsCount +
          this.dashboardData?.upComingTournamentsCount;
        this.onGoingTournamentPer =
          (this.dashboardData?.ongoingTournamentsCount / total) * 100;
      } else {
        this.onGoingTournamentPer = 0;
      }
    });
  }

  getAnalytics(filter){
    this.adminService.analyticsData(API, filter).subscribe((res:any)=>{
      this.dashboardData.user= filter ? res.data.user.filtered : res.data.user.total;
    })
  }  

  fetchCountries = async () => {
    try {
      const data = await this.userService.getAllCountries().toPromise();
      this.countryList.push({
        ar: 'كل',
        en: 'ALL',
        id: 0,
        name: 'ALL',
        phoneCode: 973,
        sortname: 'ALL',
      });
      const countryList = data.countries.filter((item) =>
        [191, 117, 17].includes(item.id)
      );
      this.countryList = [].concat(this.countryList, countryList);
    } catch (error) {}
  };

  selectType(e) {
    if (e == 'ALL') {
      this.filter.gameId = '';
      this.loadData();
    } else {
      this.filter.gameId = e;
      this.loadData();
    }
  }

  clearDate(){
    this.dateData.fromDate = this.dateData.toDate = null; 
    this.fromDatePicker.select(undefined);
    this.toDatepicker.select(undefined);
    this.minStartDateValue = this.setTodaysDate();
    this.getAnalytics(null);
  }

  setFilter(value, type) {
    this.dateData[type]=value;
    if(type == 'fromDate'){
      this.dashboardData.user = null;
      this.toDatepicker.select(undefined);
      this.minStartDateValue = new Date(value);
    }
    if(this.dateData.fromDate && this.dateData.toDate){
      this.getAnalytics(this.dateData);
    }
  }
  getGames() {
    this.gameService.getAdminAllGames(API).subscribe(
      (res) => {
        if (res && res.data) {
          this.gameList = res.data;
        }
      },
      (err) => {}
    );
  }

  onCountryChange = async (countryname) => {
    this.stateList.length = 0;
    if (countryname == 'ALL') {
      this.filter.country = '';
      this.stateList = [];
      this.ownerForm.patchValue({
        region: '',
      });
      this.loadData();
    } else {
      this.filter.country = countryname;
      this.loadData();
      const country = this.countryList.find((item) => item.name == countryname);
      const data = await this.userService.getStates().toPromise();
      this.stateList = data.states.filter(
        (item) => item.country_id == country.id
      );
    }
  };

  onRegionChange = async (regionName) => {
    if (regionName == 'ALL') {
      this.filter.region = '';
      this.loadData();
    } else {
      this.filter.region = regionName;
      this.loadData();
    }
  };
}
