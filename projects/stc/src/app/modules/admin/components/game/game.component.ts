import { Component, OnInit, ViewChild } from '@angular/core';

import { DatatableComponent } from '@swimlane/ngx-datatable';
import { MatDialog } from '@angular/material/dialog';
import { TranslateService } from '@ngx-translate/core';
import { EsportsGameService, GlobalUtils, EsportsToastService } from 'esports';
import { environment } from '../../../../../environments/environment';


const API = environment.apiEndPoint;

import {
  InfoPopupComponent,
  InfoPopupComponentData,
  InfoPopupComponentType,
} from '../../../../shared/popups/info-popup/info-popup.component';


@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.scss'],
})
export class GameComponent implements OnInit {
  @ViewChild(DatatableComponent) table: DatatableComponent;

  isLoading: boolean = true;
  isTournamentAllowed: boolean = false;
  platformsError: boolean = false;
  showGameTable: boolean = true;
  isOrder: boolean;

  imageURI: any = '';
  logoURI: any = '';
  name: any;

  platformList = [];
  rowsFirstTab = [];
  selectedItems = [];
  tempTable = [];

  columnsFirstTab = [
    { name: 'name' },
    { name: 'bracketTypes' },
    { name: 'platforms' },
  ];

  dropdownSettings = {};
  isEditable = {};

  type = {
    single: false,
    double: false,
    round_robin: false,
    battle_royale: false,
    swiss_safeis: false,
    ladder: false,
  };

  newObj: any = {
    _id: '0123456789ab',
  };

  @ViewChild('logoUpload', { static: false }) set logoUpload(element) {
    if (GlobalUtils.isBrowser() && element && this.logoURI) {
      document.getElementById('logoUpload').style.background =
        'url(' + this.logoURI + ')';
    }
  }

  @ViewChild('imageUpload', { static: false }) set imageUpload(element) {
    if (GlobalUtils.isBrowser() && element && this.imageURI) {
      document.getElementById('imageUpload').style.background =
        'url(' + this.imageURI + ')';
    }
  }

  constructor(
    private gameService: EsportsGameService,
    private translateService: TranslateService,
    public dialog: MatDialog,
    public globalUtils: GlobalUtils,
    public eSportsToastService: EsportsToastService
  ) {}

  ngOnInit(): void {
    this.platformList = [];
    this.selectedItems = [];

    this.dropdownSettings = {
      classes: 'myclass multiselect-class',
      enableSearchFilter: true,
      searchPlaceholderText: this.translateService.instant('GAME.SEARCH'),
      selectAllText: this.translateService.instant('GAME.SELECT_ALL'),
      singleSelection: false,
      text: '',
      unSelectAllText: this.translateService.instant('GAME.UNSELECT_ALL'),
    };

    this.fetchPlatforms();
  }

  searchByValueFilter(event) {
    const value = event.target.value.toLowerCase().trim();
    const count = Object.keys(this.tempTable[0]).length;
    const keys = Object.keys(this.tempTable[0]);
    const filterData = this.tempTable.filter((item) => {
      return item && item['name'].toLowerCase().includes(value);
    });
    this.rowsFirstTab = [...filterData];
    this.table.offset = 0;
  }

  /**
   * method to be called to get games list
   */
  getGames() {
    this.isLoading = true;
    this.gameService.getAdminAllGames(API).subscribe(
      (res) => {
        if (res && res.data) {
          this.rowsFirstTab = res.data;
          this.tempTable = res.data;
          this.isLoading = false;
        }
      },
      (err) => {
        this.isLoading = false;
        const afterBlockData: InfoPopupComponentData = {
          title: this.translateService.instant('API.GAME.GET.ERROR_HEADER'),
          text: err?.error?.message || err?.message,
          type: InfoPopupComponentType.info,
        };
        this.dialog.open(InfoPopupComponent, { data: afterBlockData });
      }
    );
  }

  emitChangedValue(e, type) {
    if (type !== 'isTournamentAllowed') {
      this.type[type] = e.target.checked;
    } else {
      this.isTournamentAllowed = e.target.checked;
    }
  }

  showPlatformName(item) {
    const platformName: any = this.platformList.find((ele) => {
      return ele.id == item;
    });
    return platformName.itemName;
  }

  /**
   * method to change the status and order of games
   * @param e
   * @param u_id
   */
  getBannerOrder(Id) {
    if (this.rowsFirstTab.find((el) => el.order == Id)) {
      this.isOrder = true;
    } else {
      return;
    }
  }
  updateRow(e, type, row, rowIndex) {
    this.isEditable = {};
    if (type === 'status') {
      const blockUserData: InfoPopupComponentData = {
        title: this.translateService.instant(
          'API.GAME.MODAL.STATUS_CONFIRMATION_MODAL.TITLE'
        ),
        text: this.translateService.instant(
          'API.GAME.MODAL.STATUS_CONFIRMATION_MODAL.TEXT'
        ),
        type: InfoPopupComponentType.confirm,
        btnText: this.translateService.instant(
          'API.GAME.MODAL.STATUS_CONFIRMATION_MODAL.BUTTONTEXT'
        ),
      };
      const dialogRef = this.dialog.open(InfoPopupComponent, {
        data: blockUserData,
      });

      dialogRef.afterClosed().subscribe((confirmed) => {
        if (confirmed) {
          this.isLoading = true;
          this.gameService
            .saveGame(API, row['_id'], { status: e.checked ? 1 : 0 })
            .subscribe(
              (res: any) => {
                this.getGames();
                const afterBlockData: InfoPopupComponentData = {
                  title: this.translateService.instant(
                    'API.GAME.PUT.SUCCESS_HEADER'
                  ),
                  text: res?.message,
                  type: InfoPopupComponentType.info,
                };
                this.dialog.open(InfoPopupComponent, { data: afterBlockData });
              },
              (err: any) => {
                this.isLoading = false;
                const afterBlockData: InfoPopupComponentData = {
                  title: this.translateService.instant(
                    'API.GAME.PUT.ERROR_HEADER'
                  ),
                  text: err?.error?.message || err?.message,
                  type: InfoPopupComponentType.info,
                };
                this.dialog.open(InfoPopupComponent, { data: afterBlockData });
              }
            );
        }
      });
    } else if (
      type === 'order' &&
      e.target.value &&
      row['order'] != e.target.value
    ) {
      this.isLoading = true;
      this.getBannerOrder(e.target.value);
      if (this.isOrder) {
        this.isOrder = false;
        this.isLoading = false;
        this.isEditable[rowIndex] = true;
        this.eSportsToastService.showError(
          this.translateService.instant(
            'ADMIN.SITE_CONFIGURATION.SUB_PAGE.UPDATE_ORDER'
          )
        );
      }
      else {
        this.gameService
          .saveGame(API, row['_id'], { order: +e.target.value })
          .subscribe(
            (res: any) => {
              this.getGames();
              const afterBlockData: InfoPopupComponentData = {
                title: this.translateService.instant(
                  'API.GAME.PUT.SUCCESS_HEADER'
                ),
                text: res?.message,
                type: InfoPopupComponentType.info,
              };
              this.dialog.open(InfoPopupComponent, { data: afterBlockData });
            },
            (err) => {
              this.isLoading = false;
              const afterBlockData: InfoPopupComponentData = {
                title: this.translateService.instant(
                  'API.GAME.PUT.ERROR_HEADER'
                ),
                text: err?.error?.message || err?.message,
                type: InfoPopupComponentType.info,
              };
              this.dialog.open(InfoPopupComponent, { data: afterBlockData });
            }
          );
       }
      
    }
  }

  /**
   * method to be called to delete the game
   * @param _gameId
   */
  onRemoveGame(_gameId) {
    this.isLoading = true;
    this.gameService.disableGame(API, _gameId).subscribe(
      (res: any) => {
        this.getGames();
        const afterBlockData: InfoPopupComponentData = {
          title: this.translateService.instant('API.GAME.DELETE.ERROR_HEADER'),
          text: res?.message,
          type: InfoPopupComponentType.info,
        };
        this.dialog.open(InfoPopupComponent, { data: afterBlockData });
        this.isLoading = false;
      },
      (err: any) => {
        this.isLoading = false;
        const afterBlockData: InfoPopupComponentData = {
          title: this.translateService.instant('API.GAME.DELETE.ERROR_HEADER'),
          text: err?.error?.message || err?.message,
          type: InfoPopupComponentType.info,
        };
        this.dialog.open(InfoPopupComponent, { data: afterBlockData });
      }
    );
  }

  async fetchPlatforms() {
    const platformsData: any = await this.gameService
      .fetchPlatforms(API)
      .toPromise();
    if (platformsData.data) {
      this.platformList = platformsData.data.map((item) => {
        return {
          id: item._id,
          itemName: item.name,
        };
      });
    } else {
      this.platformList = [];
    }
    this.getGames();
  }

  /**
   * method to load the log image on the page
   */
  logoUploader() {
    if (GlobalUtils.isBrowser()) {
      const logo: any = document.getElementById('logofileUpload');
      logo.click();
      logo.onchange = function () {
        const reader = new FileReader();
        reader.onload = function (event: any) {
          const img = new Image();
          img.onload = function () {
            this.logoURI = event.target.result;
            document.getElementById('logoUpload').style['background'] =
              'url(' + event.target.result + ')';
          }.bind(this);
          img.onerror = function () {};
          img.src = window.URL.createObjectURL(logo.files[0]);
        }.bind(this);
        reader.readAsDataURL(logo.files[0]);
      }.bind(this);
    }
  }

  /**
   * method to load image on the page
   */
  imageUploader() {
    if (GlobalUtils.isBrowser()) {
      const image: any = document.getElementById('imagefileUpload');
      image.click();
      image.onchange = function () {
        const reader = new FileReader();
        reader.onload = function (event: any) {
          const img = new Image();
          img.onload = function () {
            this.imageURI = event.target.result;
            document.getElementById('imageUpload').style['background'] =
              'url(' + event.target.result + ')';
          }.bind(this);
          img.onerror = function () {};
          img.src = window.URL.createObjectURL(image.files[0]);
        }.bind(this);
        reader.readAsDataURL(image.files[0]);
      }.bind(this);
    }
  }

  onItemSelect(item: any) {
    if (this.selectedItems.length > 0) {
      this.platformsError = false;
    }
  }

  OnItemDeSelect(item: any) {
    if (this.selectedItems.length == 0) {
      this.platformsError = true;
    }
  }

  onSelectAll(items: any) {
    if (this.selectedItems.length > 0) {
      this.platformsError = false;
    }
  }

  onDeSelectAll(items: any) {
    if (this.selectedItems.length == 0) {
      this.platformsError = true;
    }
  }

  /**
   * method to call before editing the game
   * @param game
   */
  editGame(game) {
    this.newObj = { ...game };

    if (GlobalUtils.isBrowser()) {
      this.selectedItems = this.newObj.platform.map((item) => {
        return item?._id;
      });
    }
    this.name = this.newObj.name;
    if (this.newObj.bracketTypes) {
      this.type = { ...this.type, ...this.newObj?.bracketTypes };
    }
    this.logoURI = this.newObj.logo;
    this.imageURI = this.newObj.image;
    this.isTournamentAllowed = this.newObj.isTournamentAllowed
      ? this.newObj.isTournamentAllowed
      : false;
    this.showGameTable = false;
  }

  /**
   * method to reset the controls of the game
   */
  resetGamesControl() {
    this.type = {
      single: false,
      double: false,
      round_robin: false,
      battle_royale: false,
      swiss_safeis: false,
      ladder: false,
    };

    this.newObj._id = '0123456789ab';
    this.logoURI = '';
    this.imageURI = '';
    this.name = '';
    this.selectedItems = [];
  }

  /**
   * method to toggle the edit panel
   */
  toggleGamePanel() {
    this.resetGamesControl();
    this.rowsFirstTab = [];
    this.getGames();
    this.showGameTable = !this.showGameTable;
  }

  /**
   * method to call to save the game
   */
  saveGame() {
    let selectedPlatform = this.selectedItems;
    if (
      this.logoURI &&
      this.imageURI &&
      this.name &&
      this.selectedItems.length > 0
    ) {
      const gameObj = {
        bracketTypes: this.type,
        image: this.imageURI,
        isTournamentAllowed: this.isTournamentAllowed,
        logo: this.logoURI,
        name: this.name,
        platform: selectedPlatform,
        slug: this.name.replace(/\s/g, ''),
      };

      this.isLoading = true;

      this.gameService.saveGame(API, this.newObj._id, gameObj).subscribe(
        (res: any) => {
          this.getGames();
          const afterBlockData: InfoPopupComponentData = {
            title: this.translateService.instant(
              'API.GAME.POST.SUCCESS_HEADER'
            ),
            text: res?.message,
            type: InfoPopupComponentType.info,
          };
          this.dialog.open(InfoPopupComponent, { data: afterBlockData });
          this.resetGamesControl();
          this.showGameTable = true;
        },
        (err: any) => {
          this.isLoading = false;
          const afterBlockData: InfoPopupComponentData = {
            title: this.translateService.instant('API.GAME.POST.ERROR_HEADER'),
            text: err?.error?.message || err?.message,
            type: InfoPopupComponentType.info,
          };
          this.dialog.open(InfoPopupComponent, { data: afterBlockData });
        }
      );
    } else {
      if (this.selectedItems.length == 0) {
        this.platformsError = true;
      }
    }
  }
}
