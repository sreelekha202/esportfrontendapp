import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrizeDisbursalComponent } from './prize-disbursal.component';

describe('PrizeDisbursalComponent', () => {
  let component: PrizeDisbursalComponent;
  let fixture: ComponentFixture<PrizeDisbursalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PrizeDisbursalComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrizeDisbursalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
