import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PrizeDisbursalComponent } from './prize-disbursal.component';
import { I18nModule } from 'esports';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { environment } from '../../../../../environments/environment';

@NgModule({
  declarations: [PrizeDisbursalComponent],
  imports: [CommonModule, I18nModule.forRoot(environment), FontAwesomeModule],
  exports: [PrizeDisbursalComponent],
})
export class PrizeDisbursalModule {}
