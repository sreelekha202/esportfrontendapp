import { Component, OnInit, ViewChild } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';


import {
  EsportsAdminService,
  EsportsLanguageService,
  EsportsConstantsService,
  EsportsToastService,
  GlobalUtils,
} from 'esports';
import {
  InfoPopupComponentData,
  InfoPopupComponentType,
  InfoPopupComponent,
} from '../../../../shared/popups/info-popup/info-popup.component';

import { MatDialog } from '@angular/material/dialog';
import { TranslateService } from '@ngx-translate/core';
import { environment } from '../../../../../environments/environment';

const API = environment.apiEndPoint;

@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: [
    './banner.component.scss',
    '../site-configuration/site-configuration.component.scss',
  ],
})
export class BannerComponent implements OnInit {
  public ownerForm: FormGroup;

  isLoading = true;
  showAdList = true;

  advertisementList: any = [];
  isEditable = {};
  language = [];
  rows = [];
  isOrder: boolean;
  bannerUri: any = '';

  _id = 0;

  @ViewChild('bannerUpload', { static: false }) set bannerUpload(element) {
    if (GlobalUtils.isBrowser() && element && this.bannerUri) {
      document.getElementById('bannerUpload').style.background =
        'url(' + this.bannerUri + ')';
    }
  }

  constructor(
    private adminService: EsportsAdminService,
    private constantsService: EsportsConstantsService,
    private dialog: MatDialog,
    private fb: FormBuilder,
    private translateService: TranslateService,
    public languageService: EsportsLanguageService,
    public eSportsToastService: EsportsToastService,
    public translate: TranslateService
  ) {}

  ngOnInit(): void {
    this.language = this.constantsService?.language;

    this.ownerForm = this.fb.group({
      title: this.fb.group(
        this.constantsService.languageFC([Validators.required])
      ),

      sub_title: this.fb.group(
        this.constantsService.languageFC([Validators.required])
      ),

      button_text: this.fb.group(
        this.constantsService.languageFC([])
      ),

      order: new FormControl('', [Validators.required, Validators.min(1), Validators.pattern('^(0|[1-9][0-9]*)$')]),
      destination: new FormControl('', [
        Validators.pattern(
          /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/
        ),
      ]),
    });
    this.getBanners();
  }

  /**
   * method to change the status or order of banners
   * @param e
   * @param u_id
   */
  getBannerOrder(Id) {
    if (this.rows.find((el) => el.order == Id)) {
      this.isOrder = true;
    } else {
      return;
    }
  }
  updateRow(e, type, u_id, rowIndex) {
    this.isEditable = {};
    if (type === 'status') {
      const blockUserData: InfoPopupComponentData = {
        title: this.translateService.instant(
          'API.BANNER.MODAL.STATUS_CONFIRMATION_MODAL.TITLE'
        ),

        text: this.translateService.instant(
          'API.BANNER.MODAL.STATUS_CONFIRMATION_MODAL.TEXT'
        ),

        type: InfoPopupComponentType.confirm,

        btnText: this.translateService.instant(
          'API.BANNER.MODAL.STATUS_CONFIRMATION_MODAL.BUTTONTEXT'
        ),
      };

      const dialogRef = this.dialog.open(InfoPopupComponent, {
        data: blockUserData,
      });
      dialogRef.afterClosed().subscribe((confirmed) => {
        if (confirmed) {
          this.isLoading = true;

          this.adminService.updateBanner(API, u_id, { status: 0 }).subscribe(
            (res: any) => {
              this.getBanners();

              const afterBlockData: InfoPopupComponentData = {
                title: this.translateService.instant(
                  'API.BANNER.PUT.SUCCESS_HEADER'
                ),
                text: res.message,
                type: InfoPopupComponentType.info,
              };
              this.dialog.open(InfoPopupComponent, { data: afterBlockData });
            },
            (err: any) => {
              this.isLoading = false;

              const afterBlockData: InfoPopupComponentData = {
                title: this.translateService.instant(
                  'API.BANNER.PUT.ERROR_HEADER'
                ),
                text:
                  this.translateService.instant(err.error.messageCode) ||
                  err.error.message,
                type: InfoPopupComponentType.info,
              };
              this.dialog.open(InfoPopupComponent, { data: afterBlockData });
            }
          );
        }
      });
    } else if (type === 'order' && e.target.value) {
      this.isLoading = true;
      this.getBannerOrder(Math.abs(e.target.value));
      if (this.isOrder) {
        this.isOrder = false;
        this.isLoading = false;
        this.isEditable[rowIndex] = true;
        this.eSportsToastService.showError(
          this.translate.instant(
            'ADMIN.SITE_CONFIGURATION.SUB_PAGE.UPDATE_ORDER'
          )
        );
      } else {
        this.adminService
          .updateBanner(API, u_id, { order: +e.target.value })
          .subscribe(
            (res: any) => {
              this.getBanners();
              const afterBlockData: InfoPopupComponentData = {
                title: this.translateService.instant(
                  'API.BANNER.PUT.SUCCESS_HEADER'
                ),
                text: res.message,
                type: InfoPopupComponentType.info,
              };
              this.dialog.open(InfoPopupComponent, { data: afterBlockData });
            },
            (err: any) => {
              this.isLoading = false;

              const afterBlockData: InfoPopupComponentData = {
                title: this.translateService.instant(
                  'API.BANNER.PUT.ERROR_HEADER'
                ),
                text:
                  this.translateService.instant(err.error.messageCode) ||
                  err.error.message,
                type: InfoPopupComponentType.info,
              };
              this.dialog.open(InfoPopupComponent, { data: afterBlockData });
            }
          );
      }
    }
  }

  /**
   * method to upload an image object in page
   */
  bannerUploader() {
    if (GlobalUtils.isBrowser()) {
      const banner: any = document.getElementById('bannerFileUpload');
      banner.click();

      banner.onchange = function () {
        const reader = new FileReader();

        reader.onload = function (event: any) {
          const img = new Image();

          img.onload = function () {
            this.bannerUri = event.target.result;
            document.getElementById('bannerUpload').style.background =
              'url(' + event.target.result + ')';
          }.bind(this);

          img.onerror = function () {};

          img.src = window.URL.createObjectURL(banner.files[0]);
        }.bind(this);
        reader.readAsDataURL(banner.files[0]);
      }.bind(this);
    }
  }

  /**
   * method to get list of all the banners with active status
   */
  getBanners() {
    this.isLoading = true;
    this.adminService.getBanner(API).subscribe((res: any) => {
      this.advertisementList = [...res.data];
      this.rows = res?.data;
      this.isLoading = false;
      this.showAdList = true;
    });
  }

  /**
   * method to check the form controls the validations
   * @param controlName
   * @param errorName
   */
  public hasError = (controlName: string, errorName: string) => {
    return this.ownerForm.controls[controlName].hasError(errorName);
  };

  /**
   * method to respond to the performing actions
   * @param type
   * @param data
   */
  advertisementAction(type, data) {
    if (type === 'view') {
      this.viewAd(data);
    }
  }

  /**
   * method to toggle the components on the page i.e. the table and the form
   */
  toggleBannerList() {
    this.resetBannerControls();
    this.showAdList = !this.showAdList;
  }

  /**
   * method to be called before saving the data to check the form status
   * and the destination url
   */
  upload() {
    /*if (this.ownerForm.invalid) {
      return;
    }
    if (
      this.ownerForm.value.destination &&
      this.ownerForm.value.destination.startsWith('https://')
    ) {*/
      this.uploadBanner();
   /* } else {
      this.ownerForm.controls.destination.setErrors({ pattern: true });
      return;
    }*/

  }

  /**
   * method to be called after form check to upload the data
   */
  uploadBanner() {
    if (this.bannerUri) {
      const formData = {
        bannerFileUrl: this.bannerUri,
        ...this.ownerForm.value,
      };

      this.isLoading = true;

      let subscriber;
      subscriber =
        this._id != 0
          ? this.adminService.updateBanner(API, this._id, formData)
          : this.adminService.addbanner(API, formData);
      subscriber.subscribe(
        (res: any) => {
          const afterBlockData: InfoPopupComponentData = {
            title: this.translateService.instant(
              'API.BANNER.POST.SUCCESS_HEADER'
            ),
            text: res.message,
            type: InfoPopupComponentType.info,
          };
          this.dialog.open(InfoPopupComponent, { data: afterBlockData });
          this.isLoading = false;
          this.resetBannerControls();
          this.getBanners();
        },
        (err) => {
          const afterBlockData: InfoPopupComponentData = {
            title: this.translateService.instant(
              'API.BANNER.POST.ERROR_HEADER'
            ),
            text:
              this.translateService.instant(err.error.messageCode) ||
              err.error.message,
            type: InfoPopupComponentType.info,
          };
          this.dialog.open(InfoPopupComponent, { data: afterBlockData });
        }
      );
    }
  }

  /**
   * method to be called to view the row data of the table in the form
   * @param rowData
   */
  viewAd(rowData) {
    this.ownerForm.patchValue({
      ...rowData,
    });
    this._id = rowData._id;
    this.bannerUri = rowData.bannerFileUrl;
    this.ownerForm.setErrors(null);
    this.showAdList = false;
  }

  /**
   * method to be called before to reset the form controls
   */
  resetBannerControls() {
    this.bannerUri = '';
    this._id = 0;
    this.ownerForm.reset();
  }
}
