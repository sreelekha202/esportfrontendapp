import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';

import { EsportsAdminService, GlobalUtils } from 'esports';
import { environment } from '../../../../../environments/environment';

const API = environment.apiEndPoint;
import {
  InfoPopupComponentData,
  InfoPopupComponentType,
  InfoPopupComponent,
} from '../../../../shared/popups/info-popup/info-popup.component';

import { MatDialog } from '@angular/material/dialog';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-shop-configuration',
  templateUrl: './shop-configuration.component.html',
  styleUrls: ['./shop-configuration.component.scss'],
})
export class ShopConfigurationComponent implements OnInit {
  public ownerForm: FormGroup;

  bannerError: boolean = false;
  isEditViewOpen: boolean = false;
  isLoading: boolean = false;
  logoError: boolean = false;

  id;
  selectedStatus: any;
  selectedType: any;

  bannerUri = '';
  logoUri = '';

  productsList: [];

  statusType = {
    Active: 'Active',
    InProgress: 'In-Progress',
    Done: 'Done',
  };

  typeValues = {
    TOPUP: this.translateService.instant(
      'ADMIN.SITE_CONFIGURATION.SUB_PAGE.TOP_UP'
    ),
    VOUCHER: this.translateService.instant(
      'ADMIN.SITE_CONFIGURATION.SUB_PAGE.VOUCHER'
    ),
  };

  @ViewChild('logoUpload', { static: false }) set logoUpload(element) {
    if (GlobalUtils.isBrowser() && element && this.logoUri) {
      document.getElementById('logoUpload').style.background =
        'url(' + this.logoUri + ')';
    }
  }

  @ViewChild('bannerUpload', { static: false }) set bannerUpload(element) {
    if (GlobalUtils.isBrowser() && element && this.bannerUri) {
      document.getElementById('bannerUpload').style.background =
        'url(' + this.bannerUri + ')';
    }
  }

  constructor(
    private adminService: EsportsAdminService,
    private dialog: MatDialog,
    private fb: FormBuilder,
    private translateService: TranslateService
  ) {}

  ngOnInit(): void {
    this.ownerForm = this.fb.group({
      name: ['', Validators.compose([Validators.required])],
      p_name: ['', Validators.compose([Validators.required])],
      type: ['', Validators.compose([Validators.required])],
      s_descp: ['', Validators.compose([Validators.required])],
      l_descp: ['', Validators.compose([Validators.required])],
      status: [''],
    });
    this.loadProducts();
  }

  selectValue(e) {
    this.selectedStatus = e;
  }

  selectType(e) {
    this.selectedType = e;
  }

  public hasError = (controlName: string, errorName: string) => {
    return this.ownerForm.controls[controlName].hasError(errorName);
  };

  loadProducts() {
    this.isLoading = true;

    this.adminService.getAllProducts(API).subscribe(
      (res: any) => {
        this.productsList = res.data;
        this.isLoading = false;
      },
      (err: any) => {
        const afterBlockData: InfoPopupComponentData = {
          title: this.translateService.instant('API.PRODUCT.GET.ERROR_HEADER'),
          text:
            this.translateService.instant(err.error.messageCode) ||
            err.error.message,
          type: InfoPopupComponentType.info,
        };
        this.dialog.open(InfoPopupComponent, { data: afterBlockData });
      }
    );
  }

  markFormGroupTouched(formGroup: FormGroup) {
    (<any>Object).values(formGroup.controls).forEach((control) => {
      control.markAsTouched();

      if (control.controls) {
        this.markFormGroupTouched(control);
      }
    });
  }

  sync(_this) {
    const element: any = document.getElementById(_this);
    (<HTMLInputElement>(
      document.getElementById('p_name')
    )).value = element.value.replace(/\s/g, '');
    this.ownerForm.controls.p_name.setValue(
      (<HTMLInputElement>document.getElementById('p_name')).value
    );
  }

  /**
   * method to upload an image object in page
   */
  logoUploader() {
    if (GlobalUtils.isBrowser()) {
    const logo: any = document.getElementById('logoUploader');
    logo.click();

    logo.onchange = function () {
      const reader = new FileReader();
      reader.onload = function (event: any) {
        const img = new Image();
        img.onload = function () {
          this.logoUri = event.target.result;
          this.logoError = false;
          document.getElementById('logoUpload').style['background'] =
            'url(' + event.target.result + ')';
        }.bind(this);
        img.onerror = function () {};
        img.src = window.URL.createObjectURL(logo.files[0]);
      }.bind(this);
      reader.readAsDataURL(logo.files[0]);
    }.bind(this);
  }
  }

  bannerUploader() {
    if (GlobalUtils.isBrowser()) {
    const banner: any = document.getElementById('bannerUploader');
    banner.click();

    banner.onchange = function () {
      const reader = new FileReader();
      reader.onload = function (event: any) {
        const img = new Image();
        img.onload = function () {
          this.bannerUri = event.target.result;
          this.bannerError = false;
          document.getElementById('bannerUpload').style['background'] =
            'url(' + event.target.result + ')';
        }.bind(this);
        img.onerror = function () {};
        img.src = window.URL.createObjectURL(banner.files[0]);
      }.bind(this);
      reader.readAsDataURL(banner.files[0]);
    }.bind(this);
  }
  }

  updateProduct() {
    if (!this.ownerForm.valid) {
      this.markFormGroupTouched(this.ownerForm);
      return;
    } else if (!this.logoUri) {
      this.logoError = true;
      return;
    } else if (!this.bannerUri) {
      this.bannerError = true;
      return;
    }

    this.isLoading = true;

    const formData = {
      banner: this.bannerUri,
      logo: this.logoUri,
      longDesc: this.ownerForm.value.l_descp,
      name: this.ownerForm.value.name,
      productName: this.ownerForm.value.p_name,
      shortDesc: this.ownerForm.value.s_descp,
      type: this.selectedType,
    };

    const subsciber = this.id
      ? this.adminService.updateProduct(API, this.id, formData)
      : this.adminService.addProduct(API, formData);

    subsciber.subscribe(
      (res: any) => {
        this.isEditViewOpen = false;
        this.isLoading = false;
        const afterBlockData: InfoPopupComponentData = {
          title: this.translateService.instant(
            'API.PRODUCT.POST.SUCCESS_HEADER'
          ),
          text: this.translateService.instant(res.messageCode) || res.message,
          type: InfoPopupComponentType.info,
        };
        this.dialog.open(InfoPopupComponent, { data: afterBlockData });
        this.loadProducts();
      },
      (err: any) => {
        this.isLoading = false;
        const afterBlockData: InfoPopupComponentData = {
          title: this.translateService.instant('API.PRODUCT.PUT.ERROR_HEADER'),
          text:
            this.translateService.instant(err.error.messageCode) ||
            err.error.message,
          type: InfoPopupComponentType.info,
        };
        this.dialog.open(InfoPopupComponent, { data: afterBlockData });
      }
    );
  }

  toggleEditPane() {
    this.isEditViewOpen = !this.isEditViewOpen;
  }

  resetControls() {
    this.ownerForm.reset();
    this.logoUri = '';
    this.bannerUri = '';
    this.bannerError = false;
    this.logoError = false;
    this.id = null;
  }

  editPane(row) {
    this.isEditViewOpen = !this.isEditViewOpen;
    this.id = row._id;
    this.logoUri = row.logo;
    this.bannerUri = row.banner;

    this.ownerForm.setValue({
      l_descp: row.longDesc,
      name: row.name,
      p_name: row.productName,
      s_descp: row.shortDesc,
      status: 1,
      type: row.type,
    });

    this.ownerForm.setErrors(null);
  }
}
