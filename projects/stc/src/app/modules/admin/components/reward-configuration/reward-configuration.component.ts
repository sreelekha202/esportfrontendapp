import { Component, OnInit } from '@angular/core';
import { EsportsRewardService } from 'esports';

import {
  InfoPopupComponent,
  InfoPopupComponentData,
  InfoPopupComponentType,
} from '../../../../shared/popups/info-popup/info-popup.component';

import { MatDialog } from '@angular/material/dialog';
import { TranslateService } from '@ngx-translate/core';
import { environment } from '../../../../../environments/environment';

const API = environment.apiEndPoint;

@Component({
  selector: 'app-reward-configuration',
  templateUrl: './reward-configuration.component.html',
  styleUrls: ['./reward-configuration.component.scss'],
})
export class RewardConfigurationComponent implements OnInit {
  rewardsList: any;
  isLoading = true;
  defaultData;

  constructor(
    private dialog: MatDialog,
    private rewardService: EsportsRewardService,
    private translateService: TranslateService
  ) {}

  ngOnInit(): void {
    this.loadData();
  }

  loadData() {
    this.isLoading = true;

    this.rewardService.getRewardsData(API).subscribe(
      (res: any) => {
        this.defaultData = JSON.stringify(res.data);
        this.rewardsList = res.data;
        this.isLoading = false;
      },
      (err: any) => {
        const afterBlockData: InfoPopupComponentData = {
          title: this.translateService.instant('API.REWARD.GET.ERROR_HEADER'),
          text: err.error.message,
          type: InfoPopupComponentType.info,
        };
        this.dialog.open(InfoPopupComponent, { data: afterBlockData });
      }
    );
  }

  cancelChanges() {
    this.rewardsList = [...JSON.parse(this.defaultData)];
  }

  updateRewards() {
    const data = this.rewardsList.filter(function (item) {
      item.points = Math.abs(item.points);
      return item.reward_type;
    });
    this.isLoading = true;
    this.rewardService.updateRewards(API, data).subscribe(
      (res: any) => {
        this.loadData();

        const afterBlockData: InfoPopupComponentData = {
          title: this.translateService.instant(
            'API.PRODUCT.PUT.SUCCESS_HEADER'
          ),
          text: this.translateService.instant(res.message) || res.message,
          type: InfoPopupComponentType.info,
        };
        this.dialog.open(InfoPopupComponent, { data: afterBlockData });
      },
      (err: any) => {
        this.isLoading = false;

        const afterBlockData: InfoPopupComponentData = {
          title: this.translateService.instant('API.PRODUCT.PUT.ERROR_HEADER'),
          text: err.error.message,
          type: InfoPopupComponentType.info,
        };
        this.dialog.open(InfoPopupComponent, { data: afterBlockData });
      }
    );
  }

  /**
   * mehtod to add the new row in the table
   */
  addNewRow() {
    const _newNode = {
      newrecord: true,
      points: '',
      reward_type: '',
      status: '',
    };

    this.rewardsList.push(_newNode);
    this.rewardsList = [...this.rewardsList];
  }

  /**
   * method to update the cell values
   * @param event
   * @param cell
   * @param rowIndex
   */
  updateValue(event, cell, rowIndex) {
    if (cell === 'status') {
      this.rewardsList[rowIndex][cell] = event.checked ? 1 : 0;
    } else if (cell === 'points') {
      this.rewardsList[rowIndex][cell] = +event.target.value;
    } else {
      this.rewardsList[rowIndex][cell] = event.target.value;
    }
    this.rewardsList = [...this.rewardsList];
  }
}
