import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RewardConfigurationComponent } from './reward-configuration.component';

describe('RewardConfigurationComponent', () => {
  let component: RewardConfigurationComponent;
  let fixture: ComponentFixture<RewardConfigurationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RewardConfigurationComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RewardConfigurationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
