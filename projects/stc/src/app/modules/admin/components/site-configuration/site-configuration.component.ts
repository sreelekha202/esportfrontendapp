import { Component, OnInit } from '@angular/core';

import { faTrash } from '@fortawesome/free-solid-svg-icons';
import { MatTabChangeEvent } from '@angular/material/tabs';

enum SiteConfigurationTabs {
  siteCustomize,
  rewards,
  Alerts,
  banner,
}

@Component({
  selector: 'app-site-configuration',
  templateUrl: './site-configuration.component.html',
  styleUrls: ['./site-configuration.component.scss'],
})
export class SiteConfigurationComponent implements OnInit {
  siteConfigurationTabs = SiteConfigurationTabs;

  checkIconShow = false;
  deleteIconShow = true;
  viewIconShow = true;

  activeTabIndex = 0;

  activeButtonColor = '#0C0E0F';
  backgroundColour = '#FC4500';
  nonActiveButtonColor = '#FC4500';

  faTrash = faTrash;

  constructor() {}

  ngOnInit(): void {}

  onTabChange(matTab: MatTabChangeEvent): void {
    this.activeTabIndex = matTab.index;
  }
}
