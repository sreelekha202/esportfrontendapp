import {
  Component,
  EventEmitter,
  Inject,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  ViewChild,
  ElementRef,
} from '@angular/core';
import { DOCUMENT } from '@angular/common';

import { AppHtmlRoutes } from '../../../../app-routing.model';

import {
  InfoPopupComponent,
  InfoPopupComponentData,
  InfoPopupComponentType,
} from '../../../../shared/popups/info-popup/info-popup.component';

import { DatatableComponent } from '@swimlane/ngx-datatable';
import { MatDialog } from '@angular/material/dialog';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import {
  EsportsAdminService,
  EsportsGameService,
  EsportsTournamentService,
  EsportsConstantsService,
  EsportsToastService,
  IPagination,
} from 'esports';
import { environment } from '../../../../../environments/environment';
import { FormGroup, FormBuilder } from '@angular/forms';
import { HomeService } from 'projects/stc/src/app/core/service';
import { fromEvent } from 'rxjs';
import {
  debounceTime,
  distinctUntilChanged,
  filter,
  map,
} from 'rxjs/operators';

const API = environment.apiEndPoint;

@Component({
  selector: 'app-tournament-details',
  templateUrl: './tournament-details.component.html',
  styleUrls: ['./tournament-details.component.scss'],
})
export class TournamentDetailsComponent implements OnInit, OnChanges {
  platformList: any = [];
  @Input() activePage: any;
  @Input() checkIconShow: boolean;
  @Input() deleteIconShow: boolean;
  @Input() editIconShow: boolean;
  @Input() tournamentType: String;
  @Input() isFeatureEditable: boolean;
  @Input() isGWB: boolean;
  @Input() manageIconShow: boolean;
  @Input() page: IPagination;
  @Input() tournamentList = [];
  @Input() type: any;
  @Input() viewIconShow: boolean;
  @Input() activeTab: number;
  @Input() IAMAFFTournament: boolean = false;

  @Output() currentPage = new EventEmitter();
  @Output() isModified = new EventEmitter();
  @Output() modifiedOptions = new EventEmitter();

  statusForPaidPrize = '';
  status: number;
  text: string = 'hi';
  @ViewChild(DatatableComponent) table: DatatableComponent;
  @ViewChild('searchInput') inputName;
  @ViewChild('searchInput', { static: true }) searchInput: ElementRef;
  AppHtmlRoutes = AppHtmlRoutes;
  abortModelType = 'delete';
  hideBtn = false;
  showRequestEditBtn = false;
  showRequestTrashBtn = false;
  viewModelBtnDisable = false;
  tournamentToDelete: '';
  changeRequestModel: string;
  modelDesc: string;
  modelHeader: string;
  modelTitle: string;

  articleModel: any;
  tournamentDetails: any;
  tournamentId: any;
  isShowParticipants: false;
  rows: any = [];
  tempTournamentList = [];

  columnsFirstTab = [
    { name: 'Tournament Name' },
    { name: 'Registration' },
    { name: 'Game' },
    { name: 'Region' },
    { name: 'Type' },
    { name: 'Start Date' },
    { name: 'End Date' },
    { name: 'Participant(s)' },
    { name: '' },
  ];
  optionForm: FormGroup;

  constructor(
    @Inject(DOCUMENT) private document: Document,
    private adminService: EsportsAdminService,
    private modalService: NgbModal,
    private tournamentService: EsportsTournamentService,
    private translateService: TranslateService,
    public dialog: MatDialog,
    private gameService: EsportsGameService,
    public eSportsToastService: EsportsToastService,
    private homeService: HomeService,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.fetchData();
    //this.inputName.nativeElement.value = this.text;
    this.optionForm = this.fb.group({
      IAMAFFTournament: [false],
    });
    if (this.activeTab == 0) {
      this.getShowParticipantsStatus();
    }
    this.initializeSearchtracker();
    this.checkSearchStatus();
  }

  initializeSearchtracker() {
    if (this.activeTab == 0) {
      this.status = 1;
    } else if (this.activeTab == 1) {
      this.status = 0;
    } else if (this.activeTab == 3) {
      this.status = 5;
    } else { 
      this.status = this.activeTab;
    }
    fromEvent(this.searchInput.nativeElement, 'keyup')
      .pipe(
        map((event: any) => {
          return event.target.value;
        }),
        filter((res) => res.length > 1),
        debounceTime(1000),
        distinctUntilChanged()
      )
      .subscribe((text: string) => {
        this.isModified.emit({
          text: text,
          page: 1,
          status: this.status,
        });
      });
  }

  checkSearchStatus() {
    fromEvent(this.searchInput.nativeElement, 'keyup').subscribe(
      (res: any) => {
        if (res.target.value == '') {
          this.isModified.emit({ type: 'update', value: this.activeTab });
        }
      },
      (err) => {}
    );
  }

  clearSearch() {
    if (this.inputName.nativeElement.value) {
      this.inputName.nativeElement.value = '';
    }
    this.isModified.emit({ type: 'update', value: this.activeTab });
  }

  ngOnChanges(simpleChanges: SimpleChanges): void {
    this.rows = [];
    this.tempTournamentList = [];

    if (this.tournamentList?.length) {
      this.rows = [...this.tournamentList];
      this.tempTournamentList = [...this.tournamentList];
    }

    if (this.activeTab == 3 && this.optionForm) {
      this.optionForm.patchValue({
        IAMAFFTournament: this.IAMAFFTournament || false,
      });
    }
  }

  /**
   * method to emit the feature value change to parent
   * @param e
   * @param _id
   */
  updateValue(e, _id) {
    this.isModified.emit({ type: 'feature', value: e.checked, id: _id });
    this.rows = [];
  }
  updateValueGWB(e, _id) {
    if (e.checked == true) {
      this.isModified.emit({
        type: 'GWB',
        value: 2,
        id: _id,
        tabIndex: this.activeTab,
      });
      this.rows = [];
    } else if (e.checked == false) {
      this.isModified.emit({
        type: 'GWB',
        value: 0,
        id: _id,
        tabIndex: this.activeTab,
      });
      this.rows = [];
    }
  }
  fetchData = async () => {
    const option = await this.gameService.fetchPlatforms(API).toPromise();
    this.platformList = option.data;
  };
  getTournament(Id) {
    const tournament = this.rows.find((el) => el._id == Id);
    this.tournamentToDelete = tournament.name;
  }
  pageChanged($event) {
    if (this.inputName.nativeElement.value) {
      this.isModified.emit({
        text: this.inputName.nativeElement.value,
        page: $event,
        tabIndex: this.activeTab,
      });
    } else {
      this.currentPage.emit($event);
    }
  }

  /**
   * Open model pop on button click
   * @param content -content
   */
  open(content, Id, modelType) {
    this.getTournament(Id);
    this.tournamentId = Id;
    this.hideBtn = true;
    this.showRequestEditBtn = false;
    // Check to open model and configure inner content
    if (modelType === 'requestEdit') {
      this.showRequestEditBtn = true;
      this.showRequestTrashBtn = false;
      this.setModelConfiguration(
        EsportsConstantsService.Model.RequestEdit.header,
        EsportsConstantsService.Model.RequestEdit.title,
        EsportsConstantsService.Model.RequestEdit.description,
        modelType
      );
    } else if (modelType === 'trash') {
      this.showRequestEditBtn = true;
      this.showRequestTrashBtn = true;
      this.setModelConfiguration(
        EsportsConstantsService.Model.TournamentTrash.header,
        EsportsConstantsService.Model.TournamentTrash.title,
        EsportsConstantsService.Model.TournamentTrash.description,
        modelType
      );
    } else {
      this.setModelConfiguration(
        EsportsConstantsService.Model.TournamentAbort.header,
        EsportsConstantsService.Model.TournamentAbort.title1,
        EsportsConstantsService.Model.TournamentAbort.description,
        modelType
      );
    }
    this.modalService.dismissAll();
    this.modalService.open(content, {
      size: 'lg',
      centered: true,
      scrollable: true,
      windowClass: 'custom-modal-content',
    });
  }

  /**
   * Open model to view the Tournament
   * @param model : model
   * @param Id : Set Selected Id
   * @param Type : Check type of model to hide/show the button
   */
  viewTournament(model, Id, Type) {
    this.tournamentId = Id;
    this.viewModelBtnDisable = Type === 'view' ? true : false;
    this.modalService.open(model, {
      size: 'lg',
      centered: true,
      scrollable: true,
      windowClass: 'custom-modal-content',
    });
    this.getTournamentById(Id);
  }
  // get tournament details based on Id
  getTournamentById(Id) {
    this.tournamentService.getTournament(API, Id).subscribe(
      (res: any) => {
        this.tournamentDetails = res.data;
        if (this.tournamentDetails?.isCharged == true) {
          this.statusForPaidPrize = 'Paid';
        } else {
          this.statusForPaidPrize = 'Unpaid';
        }
      },
      (err) => {}
    );
  }

  // Abort Tournament based on Selected Id
  abort() {
    const formValues = {
      tournamentId: this.tournamentId,
    };

    this.tournamentService.abortTournament(API, formValues).subscribe(
      (res) => {
        if (res) {
          this.setModelConfiguration(
            EsportsConstantsService.Model.TournamentAbortSuccess.header,
            EsportsConstantsService.Model.TournamentAbortSuccess.title,
            EsportsConstantsService.Model.TournamentAbortSuccess.description,
            this.abortModelType
          );
          // Remove Object from list based on ID
          this.isModified.emit({
            type: 'abort',
            value: true,
            id: this.tournamentId,
          });
          const tournamentObject = this.tournamentList;
          this.tournamentList = tournamentObject.filter(
            (resp) => resp._id !== this.tournamentId
          );
          this.hideBtn = false;
        } else {
          this.eSportsToastService.showError(
            this.translateService.instant(
              EsportsConstantsService.APIError.TournamentUpdateError
            )
          );
        }
      },
      (err: any) => {
        const afterBlockData: InfoPopupComponentData = {
          title: this.translateService.instant(
            'API.TOURNAMENT.PUT.SUCCESS_HEADER'
          ),
          text:
            this.translateService.instant(err.error.messageCode) ||
            err.error.message,
          type: InfoPopupComponentType.info,
        };
        this.dialog.open(InfoPopupComponent, { data: afterBlockData });
      }
    );
  }

  // Trash the tournament
  trash() {
    const formValues = {
      tournamentStatus: EsportsConstantsService.Status.Delete,
      changeRequest: this.changeRequestModel,
    };
    this.updateTournamentById(formValues);
  }

  // Approve/Publish tournament
  approveTournament() {
    const formValues = {
      tournamentStatus: EsportsConstantsService.Status.Publish,
    };
    this.updateTournamentById(formValues);
  }

  // Change request
  requestEdit() {
    const formValues = {
      tournamentStatus: EsportsConstantsService.Status.RequestEdit,
      changeRequest: this.changeRequestModel,
    };
    this.updateTournamentById(formValues);
  }

  // Update tournament by Id
  updateTournamentById = async (formValues) => {
    try {
      if (!this.tournamentId) return;

      const response = await this.tournamentService.updateTournament(
        formValues,
        this.tournamentId
      );
      this.modalService.dismissAll();
      this.changeRequestModel = '';
      this.isModified.emit({ type: 'update', value: this.activeTab });
      this.SuccessModel(
        this.translateService.instant('API.TOURNAMENT.PUT.SUCCESS_HEADER'),
        response?.message
      );
      const tournamentObject = this.tournamentList.filter(
        (resp) => resp._id !== this.tournamentId
      );
      this.tournamentList = [...tournamentObject];
    } catch (error) {
      this.eSportsToastService.showError(
        this.translateService.instant(
          EsportsConstantsService.APIError.TournamentUpdateError
        )
      );
    }
  };

  /**
   * Confiure model
   * @param header : header value
   * @param title : Title value
   * @param description : Description value
   */
  setModelConfiguration(header, title, description, modelType) {
    if (modelType == 'delete') {
      this.modelTitle =
        this.translateService.instant(title) +
        '"' +
        `${this.tournamentToDelete}` +
        '"';
      this.modelDesc = this.translateService.instant(description);
      this.modelHeader = this.translateService.instant(header);
    } else {
      this.modelTitle = this.translateService.instant(title);
      this.modelDesc = this.translateService.instant(description);
      this.modelHeader = this.translateService.instant(header);
    }
  }

  // Display the model pop up for success message
  SuccessModel(title, text) {
    const data: InfoPopupComponentData = {
      title: this.translateService.instant(title),
      text: this.translateService.instant(text),
      type: InfoPopupComponentType.info,
    };
    this.dialog.open(InfoPopupComponent, { data });
  }

  export(row) {
    this.fetchParticipants(row._id, row);
  }

  getWinnersList = async () => {};

  exportToCSV(data, row, winnerList) {
    const csvData = [
      {
        'Number of players joined': data.length,
        'Match Results': '',
        'Winners list': winnerList
          .map((item) => {
            return item.winnerUser.fullName;
          })
          .toString(),
        Game: row.game,
        'Start Date': row.startDate,
        'End Date': row.endDate ? row.endDate : '',
      },
    ];
    this.JSONToCSVConvertor(csvData, '', true, {});
  }

  JSONToCSVConvertor(JSONData, ReportTitle, ShowLabel, tournamentDetail) {
    const arrData =
      typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;
    let row = '';
    let CSV = '';

    if (ShowLabel) {
      for (const i in tournamentDetail) {
        row += i + ',';
      }
      //row = row.slice(0, -1);
      CSV += row;
      row = '';
      for (const i in tournamentDetail) {
        row += '"' + tournamentDetail[i] + '",';
      }
      row.slice(0, row.length - 1);
      CSV += row + '\r\n';

      if (ReportTitle) {
        CSV += ReportTitle + '\r\n\n';
      }
      row = '';
      for (const index in arrData[0]) {
        row += index + ',';
      }
      row = row.slice(0, -1);
      CSV += row + '\r\n';
    }

    for (let i = 0; i < arrData.length; i++) {
      let row = '';
      for (const index in arrData[i]) {
        if (index === 'Mobile Number') {
          row += '"=""' + arrData[i][index] + '""",';
        } else {
          row += '"' + arrData[i][index] + '",';
        }
      }
      row.slice(0, row.length - 1);
      CSV += row + '\r\n';
    }

    if (CSV == '') {
      this.eSportsToastService.showError('Invalid data');
      return;
    }
    const universalBOM = '\uFEFF';
    let fileName = 'STC_PARTICIPANTS';
    fileName += ReportTitle.replace(/ /g, '_');
    const uri =
      'data:text/csv;charset=utf-8,' + encodeURIComponent(universalBOM + CSV);
    const link = this.document.createElement('a');
    link.href = uri;
    link.download = fileName + '.csv';
    this.document.body.appendChild(link);
    link.click();
    this.document.body.removeChild(link);
  }

  fetchParticipants = async (id, row) => {
    try {
      // this.isLoaded = false;
      const encodeUrl = `?query=${encodeURIComponent(
        JSON.stringify({ tournamentId: id })
      )}`;
      const participants = await this.tournamentService
        .getParticipants(API, encodeUrl)
        .toPromise();

      const query = {
        filter: {
          tournament: id,
          isAward: true,
          awardPosition: 3,
        },
        option: {
          sort: { awardPosition: 1 },
        },
      };
      const winnerList: any = await this.adminService
        .getTournamentWinners(API, query)
        .toPromise();
      this.exportToCSV(participants?.data, row, winnerList.data);
      // this.participarntDa = participants?.data;
      // this.participantsListPending = participants?.data.filter(
      //   (item) => !item?.participantStatus
      // );

      // this.participantsListApproved = participants?.data.filter(
      //   (item) => item?.participantStatus === 'approved'
      // );

      // this.participantsListRejected = participants?.data.filter(
      //   (item) => item?.participantStatus === 'rejected'
      // );
      // this.setParticipant();
      // this.isLoaded = true;
    } catch (error) {
      // this.isLoaded = true;
      // this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  IAMAFFTournamentHandler = async (check) => {
    this.modifiedOptions.emit(this.optionForm.value);
  };

  async getShowParticipantsStatus() {
    try {
      const data = await this.homeService.isShowParticipants();
      this.isShowParticipants = data?.showParticipant;
    } catch (error) {
      console.error(error?.error?.message && error?.message);
    }
  }

  async updateShowParticipantStatus(value) {
    try {
      const data = await this.adminService.updateShowParticipantsStatus(API, {
        isShowParticipant: value,
      });
      if (data?.success) {
        this.isShowParticipants = data?.isShowParticipants;
        this.eSportsToastService.showSuccess(data.message);
      }
    } catch (error) {
      console.error(error?.error?.message && error?.message);
    }
  }

  endTournament(tournamentId) {
    const data: InfoPopupComponentData = {
      title: this.translateService.instant('API.TOURNAMENT.PUT.CONFIRM'),
      text: this.translateService.instant(
        'API.TOURNAMENT.PUT.END_CONFIRM_TEXT'
      ),
      type: InfoPopupComponentType.confirm,
      btnText: this.translateService.instant('API.TOURNAMENT.PUT.CONFIRM'),
    };
    const dialogRef = this.dialog.open(InfoPopupComponent, { data });
    dialogRef.afterClosed().subscribe((confirmed) => {
      if (confirmed) {
        this.tournamentId = tournamentId;
        const formValues = {
          isFinished: true,
        };
        this.updateTournamentById(formValues);
      }
    });
  }
}
