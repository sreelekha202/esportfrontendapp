import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { EsportsLanguageService } from 'esports';

@Component({
  selector: 'app-highlighted',
  templateUrl: './highlighted.component.html',
  styleUrls: [ '../content-management.component.scss','./highlighted.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HighlightedComponent implements OnInit {

  constructor(public languageService: EsportsLanguageService) { }
  @Input() articlesList;
  @Input() page;
  @Output() callBack = new EventEmitter();

  ngOnInit(): void {
  }

  open(id, type) {
    this.callBack.emit({
      type: 'open',
      data: {
        id, type
      }
    });
  }

  viewArticle(id, type) {
    this.callBack.emit({
      type: 'view',
      data: {
        id, type
      }
    });
  }

  pageChanged(e) {
    this.callBack.emit({
      type: 'pageChange', 
      data: {
        event: e
      }
    })
  }

}
