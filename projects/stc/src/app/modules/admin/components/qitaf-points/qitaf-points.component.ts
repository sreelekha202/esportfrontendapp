import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EsportsAdminService, EsportsToastService } from 'esports';
import { TranslateService } from '@ngx-translate/core';
import { environment } from '../../../../../environments/environment';

const API = environment.apiEndPoint;
@Component({
  selector: 'app-qitaf-points',
  templateUrl: './qitaf-points.component.html',
  styleUrls: ['./qitaf-points.component.scss'],
})
export class QitafPointsComponent implements OnInit {
  form: FormGroup;
  isLoading = false;
  tempData: any = null;

  constructor(
    private fb: FormBuilder,
    private adminService: EsportsAdminService,
    private eSportsToastService: EsportsToastService,
    private translateService: TranslateService
  ) {
    this.form = this.fb.group({
      registrationPoints: [0, Validators.required],
      referralPoints: [0, Validators.required],
      RegistartionMaxCount: [0, Validators.required],
      RegistartionCounter: [0, Validators.required],
      referralMaxCount: [0, Validators.required],
      referralCounter: [0, Validators.required],
      IsRegistrationPointsActive: [true],
      IsReferralPointsActive: [true],
    });
  }

  ngOnInit(): void {
    this.getQitafConfigPoints();
  }

  async getQitafConfigPoints() {
    this.isLoading = true;
    try {
      const qitafPoints = await this.adminService.getQitafConfigPoints(API);
      const qitafPromoPoints = await this.adminService.getQitafConfigPromoPoints(
        API,
        'eidsar20'
      );
      if (
        qitafPoints &&
        qitafPoints.data &&
        qitafPoints.data.length &&
        qitafPromoPoints &&
        qitafPromoPoints.data &&
        qitafPromoPoints.data.promoData
      ) {
        this.tempData = qitafPoints.data[0];
        this.form.setValue({
          registrationPoints:
            qitafPromoPoints?.data?.promoData?.priceAmount || 0, // set fetched intial value temprary set as 0
          IsRegistrationPointsActive:
            qitafPromoPoints?.data?.promoData?.isActive || false,
          RegistartionMaxCount:
            qitafPromoPoints?.data?.promoData?.maxCount || 0,
          RegistartionCounter: qitafPromoPoints?.data?.promoData?.counter || 0,

          referralPoints: qitafPoints.data[0]?.referralPoints || 0,
          referralMaxCount: qitafPoints.data[0]?.maxCount || 0,
          referralCounter: qitafPoints.data[0]?.counter || 0,
          IsReferralPointsActive:
            qitafPoints.data[0]?.IsReferralPointsActive || false,
        });
      }
      this.isLoading = false;
    } catch (error) {
      this.isLoading = false;
      console.error(error);
    }
  }

  increment(type) {
    this.form
      .get(type.toString())
      .setValue(this.form.get(type.toString()).value + 1);
  }

  decrement(type) {
    if (this.form.get(type.toString()).value > 0) {
      this.form
        .get(type.toString())
        .setValue(this.form.get(type.toString()).value - 1);
    }
  }

  async saveRefrral() {
    if (
      this.form.value &&
      this.form.value.hasOwnProperty('referralPoints') &&
      this.form.value.referralPoints >= 0 &&
      this.form.value.referralMaxCount >= 0 &&
      this.form.value.referralCounter >= 0
    ) {
      this.isLoading = true;
      try {
        await this.adminService.updateQitafConfigPoints(API, {
          // registrationPoints: parseInt(this.form.value.registrationPoints),
          referralPoints: parseInt(this.form.value.referralPoints),
          maxCount: parseInt(this.form.value.referralMaxCount),
          counter: parseInt(this.form.value.referralCounter),
          IsReferralPointsActive: this.form?.value?.IsReferralPointsActive,
        });
        this.eSportsToastService.showSuccess(
          this.translateService.instant('ADMIN.QITAF.SUUCESSFULLY_UPDATED')
        );
        this.isLoading = false;
      } catch (error) {
        this.isLoading = false;
        console.error(error);
      }
    } else {
      this.eSportsToastService.showInfo(
        this.translateService.instant('ADMIN.QITAF.INVALID_PARAMETER')
      );
    }
  }

  async saveNewRegistration() {
    if (
      this.form.value &&
      this.form.value.hasOwnProperty('registrationPoints') &&
      this.form.value.registrationPoints >= 0 &&
      this.form.value.RegistartionMaxCount >=0 &&
      this.form.value.RegistartionCounter >=0
    ) {
      this.isLoading = true;
      try {
        await this.adminService.updateQitafConfigPromoPoints(API, {
          amount: parseInt(this.form.value.registrationPoints),
          isActive: this.form?.value?.IsRegistrationPointsActive,
          maxCount: parseInt(this.form.value.RegistartionMaxCount),
          counter: parseInt(this.form.value.RegistartionCounter),
        });
        this.eSportsToastService.showSuccess(
          this.translateService.instant('ADMIN.QITAF.SUUCESSFULLY_UPDATED')
        );
        this.isLoading = false;
      } catch (error) {
        this.isLoading = false;
        console.error(error);
      }
    } else {
      this.eSportsToastService.showInfo(
        this.translateService.instant('ADMIN.QITAF.INVALID_PARAMETER')
      );
    }
  }
}
