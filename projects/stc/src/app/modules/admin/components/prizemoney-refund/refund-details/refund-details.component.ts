import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import {
  EsportsUserService,
  EsportsTransactionService,
  EsportsToastService,
} from 'esports';
import { environment } from '../../../../../../environments/environment';
const API = environment.apiEndPoint;

@Component({
  selector: 'app-refund-details',
  templateUrl: './refund-details.component.html',
  styleUrls: ['./refund-details.component.scss'],
})
export class RefundDetailsComponent implements OnInit {
  @Input() tournamentDetails: any;
  @Output() isToggle = new EventEmitter();
  isLoading = false;
  disbursalStatus;
  organizerDetail;
  paymentMethodImage;
  accountDetail;
  paymentReceivedByImage;
  enableDisbursal: Boolean = false;

  columns = [
    { name: 'Tournament Name' },
    { name: 'Game' },
    { name: 'End Date' },
    { name: 'Participant(s)' },
    { name: 'Disbursal Status' },
    { name: 'Details' },
  ];
  rows: any = [];
  status = '';
  constructor(
    private userService: EsportsUserService,
    private transactionService: EsportsTransactionService,
    public eSportsToastService: EsportsToastService
  ) {}

  ngOnInit(): void {
    this.rows = [this.tournamentDetails];
    this.disbursalStatus = this.tournamentDetails?.lastDisbursalRecord;
    this.getDisbursalStatus();
    this.getOrganizerDetails();
    this.setReceivedMethod();
  }

  getTotalAmount() {
    const totalPrice = this.tournamentDetails?.prizeList || [];
    const prizeSum = totalPrice.reduce((acc, el) => acc + el.value, 0);
    return prizeSum;
  }

  cancel() {
    this.isToggle.emit({ toggle: false });
  }

  async getOrganizerDetails() {
    try {
      this.userService
        .getUserAccountDetails(API, this.tournamentDetails?.organizerDetail)
        .subscribe((res) => {
          this.organizerDetail = res['data'];
          this.accountDetail = this.organizerDetail?.accountDetail;
          this.setPaymentMethod();
        });
    } catch (error) {
      this.eSportsToastService.showError(
        error?.error?.message || error?.message
      );
    }
  }

  setPaymentMethod() {
    const isAccountVerified =
      this.organizerDetail?.accountDetail?.isPaymentAccountVerified;
    if (isAccountVerified) {
      const paymentType = this.organizerDetail?.accountDetail?.paymentType;
      switch (paymentType) {
        case 'paypal':
          this.paymentMethodImage = 'assets/images/payment/paypal.png';
          break;
        case 'stcpay':
          this.paymentMethodImage = 'assets/images/payment/stc_play.png';
          break;
        case 'payfort':
          this.paymentMethodImage = 'assets/images/payment/payfort.png';
          break;
        default:
          this.paymentMethodImage = '';
          break;
      }
    }
  }

  setReceivedMethod() {
    const paymentMethodUsed =
      this.tournamentDetails?.successTransaction?.provider;
    if (paymentMethodUsed) {
      switch (paymentMethodUsed) {
        case 'paypal':
          this.paymentReceivedByImage = 'assets/images/payment/paypal.png';
          if (this.accountDetail?.paymentAccountId) {
            this.enableDisbursal = true;
          }
          break;
        case 'stcpay':
          this.paymentReceivedByImage = 'assets/images/payment/stc_play.png';
          this.enableDisbursal = true;
          break;
        case 'payfort':
          this.paymentReceivedByImage = 'assets/images/payment/payfort.png';
          break;
        default:
          this.paymentReceivedByImage = '';
          break;
      }
    }
  }

  async getDisbursalStatus() {
    try {
      if (
        this.tournamentDetails?.lastDisbursalRecord?.status === 'NOT DISBURSED'
      ) {
        return;
      }
      const res = await this.transactionService.getPrizeMoneyRefundStatus(
        API,
        this.tournamentDetails._id
      );
      this.disbursalStatus = res.data;
      this.status = this.disbursalStatus?.status;
      if (this.disbursalStatus?.status?.toLowerCase() == 'ok') {
        this.status = 'SUCCESS';
      }
    } catch (error) {
      this.eSportsToastService.showError(
        error?.error?.message || error?.message
      );
    }
  }

  async initiateDisbursal() {
    try {
      if (!this.accountDetail?.paymentAccountId) {
        return;
      }

      this.isLoading = true;
      const payload = {
        tournamentId: this.tournamentDetails._id,
      };
      const res = await this.transactionService.refundPrizeMoney(API, payload);
      this.eSportsToastService.showSuccess(res.message);
      this.disbursalStatus = res.data;
      this.getDisbursalStatus();
      this.isLoading = false;
    } catch (error) {
      this.eSportsToastService.showError(
        error?.error?.message || error?.message
      );
      this.isLoading = false;
    }
  }
}
