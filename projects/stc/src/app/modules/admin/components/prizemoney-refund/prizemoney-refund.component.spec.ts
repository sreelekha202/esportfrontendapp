import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PrizemoneyRefundComponent } from './prizemoney-refund.component';

describe('PrizemoneyRefundComponent', () => {
  let component: PrizemoneyRefundComponent;
  let fixture: ComponentFixture<PrizemoneyRefundComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PrizemoneyRefundComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PrizemoneyRefundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
