import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {
  InfoPopupComponent,
  InfoPopupComponentData,
  InfoPopupComponentType,
} from '../../../../../shared/popups/info-popup/info-popup.component';

import { MatDialog } from '@angular/material/dialog';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
import { EsportsNotificationsService, GlobalUtils } from 'esports';
import { environment } from '../../../../../../environments/environment';

const API = environment.apiEndPoint;

@Component({
  selector: 'app-email-notifications',
  templateUrl: './email-notifications.component.html',
  styleUrls: ['./email-notifications.component.scss'],
})
export class EmailNotificationsComponent implements OnInit, OnDestroy {
  emailSent: boolean = false;
  isLoading: boolean = false;
  editorConfig = {};

  headerImageUri = '';

  userSegementValues = {
    ALL_USERS: this.translateService.instant(
      'ADMIN.USER_NOTIFICATION.LABEL.ALL_USERS'
    ),
    TOURNAMENT_PLAYERS: this.translateService.instant(
      'ADMIN.USER_NOTIFICATION.LABEL.TOURNAMENT_PLAYERS'
    ),
    TOURNAMENT_ORGANIZERS: this.translateService.instant(
      'ADMIN.USER_NOTIFICATION.LABEL.TOURNAMENT_ORGANIZER'
    ),
  };

  emailNotificationForm = this.fb.group({
    emailTitle: ['', Validators.required],
    headerText: ['', Validators.required],
    message: ['', Validators.required],
    userSegment: ['', Validators.required],
  });

  @ViewChild('bannerUpload', { static: false }) set bannerUpload(element) {
    if (GlobalUtils.isBrowser() && element && this.headerImageUri) {
      document.getElementById('bannerUpload').style.background =
        'url(' + this.headerImageUri + ')';
    }
  }

  constructor(
    private dialog: MatDialog,
    private fb: FormBuilder,
    private translateService: TranslateService,
    private userNotificationsService: EsportsNotificationsService
  ) {
    translateService.onLangChange.subscribe((event: LangChangeEvent) => {
      this.userSegementValues = {
        ALL_USERS: this.translateService.instant(
          'ADMIN.USER_NOTIFICATION.LABEL.ALL_USERS'
        ),
        TOURNAMENT_PLAYERS: this.translateService.instant(
          'ADMIN.USER_NOTIFICATION.LABEL.TOURNAMENT_PLAYERS'
        ),
        TOURNAMENT_ORGANIZERS: this.translateService.instant(
          'ADMIN.USER_NOTIFICATION.LABEL.TOURNAMENT_ORGANIZER'
        ),
      };
    });
  }

  ngOnInit(): void {
    this.editorConfig = {
      editable: true,
      spellcheck: true,
      height: '100px',
      width: 'auto',
      minWidth: '0',
      translate: 'yes',
      enableToolbar: true,
      showToolbar: true,
      defaultParagraphSeparator: '',
      defaultFontName: '',
      defaultFontSize: '',
      fonts: [
        { class: 'arial', name: 'Arial' },
        { class: 'times-new-roman', name: 'Times New Roman' },
        { class: 'calibri', name: 'Calibri' },
        { class: 'comic-sans-ms', name: 'Comic Sans MS' },
      ],
      customClasses: [
        {
          name: 'quote',
          class: 'quote',
        },
        {
          name: 'redText',
          class: 'redText',
        },
        {
          name: 'titleText',
          class: 'titleText',
          tag: 'h1',
        },
      ],
      sanitize: true,
      toolbarPosition: 'top',
      toolbarHiddenButtons: [['bold', 'italic'], ['fontSize']],
    };
  }

  ngOnDestroy(): void {
    this.resetControls();
  }

  sendEmails() {
    this.isLoading = true;

    const inputData = {
      headerImage: this.headerImageUri,
      headerText: this.emailNotificationForm.value.headerText,
      message: this.emailNotificationForm.value.message,
      title: this.emailNotificationForm.value.emailTitle,
      userSegment: this.emailNotificationForm.value.userSegment,
    };

    this.userNotificationsService.sendEmails(API, inputData).subscribe(
      (response: any) => {
        this.resetControls();
        this.isLoading = false;
        const afterBlockData: InfoPopupComponentData = {
          title: this.translateService.instant(
            'ADMIN.USER_NOTIFICATION.RESPONSE.SUCCESS'
          ),
          text: response?.message,
          type: InfoPopupComponentType.info,
        };
        this.dialog.open(InfoPopupComponent, { data: afterBlockData });
      },
      (err: any) => {
        this.isLoading = false;
        const afterBlockData: InfoPopupComponentData = {
          title: this.translateService.instant(
            'ADMIN.USER_NOTIFICATION.RESPONSE.ERROR'
          ),
          text: err?.error?.message || err?.message,
          type: InfoPopupComponentType.info,
        };
        this.dialog.open(InfoPopupComponent, { data: afterBlockData });
      }
    );
  }

  get userSegment() {
    return this.emailNotificationForm.get('userSegment');
  }

  markFormGroupTouched(formGroup: FormGroup) {
    (<any>Object).values(formGroup.controls).forEach((control) => {
      control.markAsTouched();

      if (control.controls) {
        this.markFormGroupTouched(control);
      }
    });
  }

  selectType(e) {
    this.emailNotificationForm.controls['userSegment'].setValue(e);
  }

  updateEmail() {
    if (!this.emailNotificationForm.valid) {
      this.markFormGroupTouched(this.emailNotificationForm);
      return;
    }

    this.sendEmails();
  }

  toggleEditPane() {}

  resetControls() {
    this.emailNotificationForm.reset();
    this.headerImageUri = '';
    this.isLoading = false;
    document.getElementById('bannerUpload').style['background'] = '';
  }

  uploadImage() {
    if (GlobalUtils.isBrowser()) {
    const banner: any = document.getElementById('bannerUploader');

    banner.click();

    banner.onchange = function () {
      const reader = new FileReader();
      reader.onload = function (event: any) {
        const img = new Image();
        img.onload = function () {
          this.headerImageUri = event.target.result;
          document.getElementById('bannerUpload').style['background'] =
            'url(' + event.target.result + ')';
        }.bind(this);
        img.onerror = function () {
          alert('not a valid file: ' + banner.files[0].type);
        };
        img.src = window.URL.createObjectURL(banner.files[0]);
      }.bind(this);
      reader.readAsDataURL(banner.files[0]);
    }.bind(this);
  }
  }

  get emailTitle() {
    return this.emailNotificationForm.get('emailTitle');
  }

  public hasError = (controlName: string, errorName: string) => {
    return this.emailNotificationForm.controls[controlName].hasError(errorName);
  };

  get headerText() {
    return this.emailNotificationForm.get('headerText');
  }

  get message() {
    return this.emailNotificationForm.get('message');
  }
}
