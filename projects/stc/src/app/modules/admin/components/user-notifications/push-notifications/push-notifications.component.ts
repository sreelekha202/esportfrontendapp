import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {
  InfoPopupComponent,
  InfoPopupComponentData,
  InfoPopupComponentType,
} from '../../../../../shared/popups/info-popup/info-popup.component';

import { MatDialog } from '@angular/material/dialog';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
import { EsportsNotificationsService, GlobalUtils } from 'esports';
import { environment } from '../../../../../../environments/environment';

const API = environment.apiEndPoint;

@Component({
  selector: 'app-push-notifications',
  templateUrl: './push-notifications.component.html',
  styleUrls: ['./push-notifications.component.scss'],
})
export class PushNotificationsComponent implements OnInit, OnDestroy {
  isLoading: boolean = false;
  messageSent: boolean = false;

  headerImageUri = '';

  pushNotificationForm = this.fb.group({
    message: ['', Validators.required],
    title: ['', Validators.required],
    userSegment: ['', Validators.required],
  });

  userSegementValues = {
    ALL_USERS: this.translateService.instant(
      'ADMIN.USER_NOTIFICATION.LABEL.ALL_USERS'
    ),
    TOURNAMENT_PLAYERS: this.translateService.instant(
      'ADMIN.USER_NOTIFICATION.LABEL.TOURNAMENT_PLAYERS'
    ),
    TOURNAMENT_ORGANIZERS: this.translateService.instant(
      'ADMIN.USER_NOTIFICATION.LABEL.TOURNAMENT_ORGANIZER'
    ),
  };

  @ViewChild('bannerUpload', { static: false }) set bannerUpload(element) {
    if (GlobalUtils.isBrowser() && element && this.headerImageUri) {
      document.getElementById('bannerUpload').style.background =
        'url(' + this.headerImageUri + ')';
    }
  }

  constructor(
    private dialog: MatDialog,
    private fb: FormBuilder,
    private translateService: TranslateService,
    private userNotificationsService: EsportsNotificationsService
  ) {
    translateService.onLangChange.subscribe((event: LangChangeEvent) => {
      this.userSegementValues = {
        ALL_USERS: this.translateService.instant(
          'ADMIN.USER_NOTIFICATION.LABEL.ALL_USERS'
        ),
        TOURNAMENT_PLAYERS: this.translateService.instant(
          'ADMIN.USER_NOTIFICATION.LABEL.TOURNAMENT_PLAYERS'
        ),
        TOURNAMENT_ORGANIZERS: this.translateService.instant(
          'ADMIN.USER_NOTIFICATION.LABEL.TOURNAMENT_ORGANIZER'
        ),
      };
    });
  }

  ngOnInit(): void {}

  ngOnDestroy(): void {
    this.resetControls();
  }

  sendPushNotifications() {
    this.isLoading = true;

    const inputData = {
      message: this.pushNotificationForm.value.message,
      title: this.pushNotificationForm.value.title,
      userSegment: this.pushNotificationForm.value.userSegment,
    };

    this.userNotificationsService.sendPushNotifications(API, inputData).subscribe(
      (response: any) => {
        this.resetControls();
        this.isLoading = false;

        const afterBlockData: InfoPopupComponentData = {
          title: this.translateService.instant(
            'ADMIN.USER_NOTIFICATION.RESPONSE.SUCCESS'
          ),
          text: response?.message,
          type: InfoPopupComponentType.info,
        };
        this.dialog.open(InfoPopupComponent, { data: afterBlockData });
      },
      (err: any) => {
        this.isLoading = false;

        const afterBlockData: InfoPopupComponentData = {
          title: this.translateService.instant(
            'ADMIN.USER_NOTIFICATION.RESPONSE.ERROR'
          ),
          text: err?.error?.message || err?.message,
          type: InfoPopupComponentType.info,
        };
        this.dialog.open(InfoPopupComponent, { data: afterBlockData });
      }
    );
  }

  markFormGroupTouched(formGroup: FormGroup) {
    (<any>Object).values(formGroup.controls).forEach((control) => {
      control.markAsTouched();

      if (control.controls) {
        this.markFormGroupTouched(control);
      }
    });
  }

  selectType(e) {
    this.pushNotificationForm.controls['userSegment'].setValue(e);
  }

  updatePush() {
    if (!this.pushNotificationForm.valid) {
      this.markFormGroupTouched(this.pushNotificationForm);
      return;
    }
    this.sendPushNotifications();
  }

  toggleEditPane() {}

  resetControls() {
    this.pushNotificationForm.reset();
    this.headerImageUri = '';
    this.isLoading = false;
    document.getElementById('bannerUpload').style['background'] = '';
  }

  uploadImage() {
    if (GlobalUtils.isBrowser()) {
    const banner: any = document.getElementById('bannerUploader');
    banner.click();

    banner.onchange = function () {
      const reader = new FileReader();

      reader.onload = function (event: any) {
        const img = new Image();
        img.onload = function () {
          this.headerImageUri = event.target.result;
          document.getElementById('bannerUpload').style['background'] =
            'url(' + event.target.result + ')';
        }.bind(this);
        img.onerror = function () {
          alert('not a valid file: ' + banner.files[0].type);
        };

        img.src = window.URL.createObjectURL(banner.files[0]);
      }.bind(this);
      reader.readAsDataURL(banner.files[0]);
    }.bind(this);
  }
  }

  public hasError = (controlName: string, errorName: string) => {
    return this.pushNotificationForm.controls[controlName].hasError(errorName);
  };

  get userSegment() {
    return this.pushNotificationForm.get('userSegment');
  }

  get message() {
    return this.pushNotificationForm.get('message');
  }

  get title() {
    return this.pushNotificationForm.get('title');
  }
}
