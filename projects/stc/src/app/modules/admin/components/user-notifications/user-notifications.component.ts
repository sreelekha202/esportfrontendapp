import { Component, OnInit } from '@angular/core';
import {
  InfoPopupComponent,
  InfoPopupComponentData,
  InfoPopupComponentType,
} from '../../../../shared/popups/info-popup/info-popup.component';

import { MatDialog } from '@angular/material/dialog';
import { MatTabChangeEvent } from '@angular/material/tabs';
import { TranslateService } from '@ngx-translate/core';
import { EsportsNotificationsService } from 'esports';
import { environment } from '../../../../../environments/environment';

const API = environment.apiEndPoint;

@Component({
  selector: 'app-user-notifications',
  templateUrl: './user-notifications.component.html',
  styleUrls: ['./user-notifications.component.scss'],
})
export class UserNotificationsComponent implements OnInit {
  isLoading = false;

  activeTabIndex = 0;

  sentList = [];
  status = ['In Queue', 'Sent'];
  type = ['Email', 'Push', 'Inbox'];

  UserSegment = [
    'All users',
    'Paying users',
    'Tournament players',
    'Tournament organizers',
    'Recently purchased',
    'No purchase in the last 30 days',
  ];

  constructor(
    private dialog: MatDialog,
    private translateService: TranslateService,
    private userNotificationsService: EsportsNotificationsService
  ) {}

  ngOnInit(): void {}

  onTabChange(matTab: MatTabChangeEvent): void {
    this.activeTabIndex = matTab.index;
    if (this.activeTabIndex == 3) {
      this.getSentList();
    }
  }

  getSentList() {
    this.isLoading = true;

    this.userNotificationsService.getSentItems(API).subscribe(
      (res: any) => {
        this.isLoading = false;
        this.sentList = res.data.map((item) => {
          return {
            Date: item.createdOn,
            Type: this.type[item.type],
            Title: item.title,
            UserSegment: this.UserSegment[item.userSegment],
            Status: this.status[item.status],
          };
        });
      },
      (err: any) => {
        this.isLoading = false;
        const afterBlockData: InfoPopupComponentData = {
          title: 'Fail',
          text:
            this.translateService.instant(err.error.messageCode) ||
            err.error.message,
          type: InfoPopupComponentType.info,
        };
        this.dialog.open(InfoPopupComponent, { data: afterBlockData });
      }
    );
  }
}
