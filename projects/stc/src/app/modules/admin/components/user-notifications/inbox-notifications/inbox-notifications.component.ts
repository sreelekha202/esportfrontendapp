import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {
  InfoPopupComponent,
  InfoPopupComponentData,
  InfoPopupComponentType,
} from '../../../../../shared/popups/info-popup/info-popup.component';

import { MatDialog } from '@angular/material/dialog';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
import { EsportsNotificationsService } from 'esports';
import { environment } from '../../../../../../environments/environment';

const API = environment.apiEndPoint;

@Component({
  selector: 'app-inbox-notifications',
  templateUrl: './inbox-notifications.component.html',
  styleUrls: [
    './inbox-notifications.component.scss',
    '../user-notifications.component.scss',
  ],
})
export class InboxNotificationsComponent implements OnInit, OnDestroy {
  isLoading = false;
  messageSent = false;

  inboxNotificationForm = this.fb.group({
    userSegment: ['', Validators.required],
    title: ['', Validators.required],
    message: ['', Validators.required],
  });

  userSegementValues = {
    ALL_USERS: this.translateService.instant(
      'ADMIN.USER_NOTIFICATION.LABEL.ALL_USERS'
    ),
    TOURNAMENT_PLAYERS: this.translateService.instant(
      'ADMIN.USER_NOTIFICATION.LABEL.TOURNAMENT_PLAYERS'
    ),
    TOURNAMENT_ORGANIZERS: this.translateService.instant(
      'ADMIN.USER_NOTIFICATION.LABEL.TOURNAMENT_ORGANIZER'
    ),
  };

  constructor(
    private dialog: MatDialog,
    private fb: FormBuilder,
    private translateService: TranslateService,
    private userNotificationsService: EsportsNotificationsService
  ) {
    translateService.onLangChange.subscribe((event: LangChangeEvent) => {
      this.userSegementValues = {
        ALL_USERS: this.translateService.instant(
          'ADMIN.USER_NOTIFICATION.LABEL.ALL_USERS'
        ),
        TOURNAMENT_PLAYERS: this.translateService.instant(
          'ADMIN.USER_NOTIFICATION.LABEL.TOURNAMENT_PLAYERS'
        ),
        TOURNAMENT_ORGANIZERS: this.translateService.instant(
          'ADMIN.USER_NOTIFICATION.LABEL.TOURNAMENT_ORGANIZER'
        ),
      };
    });
  }

  ngOnInit(): void {}

  ngOnDestroy(): void {
    this.resetControls();
  }

  selectType(e) {
    this.inboxNotificationForm.controls.userSegment.setValue(e);
  }

  updateInbox() {
    if (!this.inboxNotificationForm.valid) {
      this.markFormGroupTouched(this.inboxNotificationForm);
      return;
    }

    this.sendInboxMessages();
  }

  toggleEditPane() {}

  resetControls() {
    this.isLoading = false;
    this.inboxNotificationForm.reset();
  }

  public hasError = (controlName: string, errorName: string) => {
    return this.inboxNotificationForm.controls[controlName].hasError(errorName);
  };

  markFormGroupTouched(formGroup: FormGroup) {
    (Object as any).values(formGroup.controls).forEach((control) => {
      control.markAsTouched();

      if (control.controls) {
        this.markFormGroupTouched(control);
      }
    });
  }

  sendInboxMessages() {
    this.isLoading = true;

    const inputData = {
      message: this.inboxNotificationForm.value.message,
      title: this.inboxNotificationForm.value.title,
      userSegment: this.inboxNotificationForm.value.userSegment,
    };

    this.userNotificationsService.sendInboxMessages(API, inputData).subscribe(
      (response: any) => {
        this.resetControls();
        this.isLoading = false;
        const afterBlockData: InfoPopupComponentData = {
          title: this.translateService.instant(
            'ADMIN.USER_NOTIFICATION.RESPONSE.SUCCESS'
          ),
          text: response?.message,
          type: InfoPopupComponentType.info,
        };
        this.dialog.open(InfoPopupComponent, { data: afterBlockData });
      },
      (err: any) => {
        this.isLoading = false;
        const afterBlockData: InfoPopupComponentData = {
          title: this.translateService.instant(
            'ADMIN.USER_NOTIFICATION.RESPONSE.ERROR'
          ),
          text: err?.error?.message || err?.message,
          type: InfoPopupComponentType.info,
        };
        this.dialog.open(InfoPopupComponent, { data: afterBlockData });
      }
    );
  }

  get userSegment() {
    return this.inboxNotificationForm.get('userSegment');
  }

  get message() {
    return this.inboxNotificationForm.get('message');
  }
}
