import {
  Component,
  OnInit,
  OnDestroy,
  PLATFORM_ID,
  Inject,
} from '@angular/core';
import { Router } from '@angular/router';

import { faBell } from '@fortawesome/free-solid-svg-icons';
import { filter, take } from 'rxjs/operators';

import { AppHtmlAdminRoutes, AppHtmlRoutes } from '../../../app-routing.model';
import { IUser, EsportsUserService } from 'esports';
import { Subscription } from 'rxjs';
import { isPlatformBrowser } from '@angular/common';
import { environment } from '../../../../environments/environment';

const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;

@Component({
  selector: 'app-admin-menu',
  templateUrl: './admin-menu.component.html',
  styleUrls: ['./admin-menu.component.scss'],
})
export class AdminMenuComponent implements OnInit, OnDestroy {
  AppHtmlAdminRoutes = AppHtmlAdminRoutes;

  currenUser: IUser;
  faBell = faBell;
  userSubscription: Subscription;
  isBrowser: boolean;
  
  constructor(
    private userService: EsportsUserService,
    private router: Router,
    @Inject(PLATFORM_ID) private platformId
  ) {
    this.isBrowser = isPlatformBrowser(platformId);
  }

  ngOnInit(): void {
    if (this.isBrowser) {
      this.userSubscription = this.userService.currentUser.subscribe((data) => {
        if (data) {
          this.currenUser = data;
        }
      });
    }
  }

  ngOnDestroy() {
    this.userSubscription?.unsubscribe();
  }

  isAllowed(_moduleType) {
    if (this.currenUser && this.currenUser.accessLevel) {
      return this.currenUser.accessLevel.includes(_moduleType) ? true : false;
    }

    return false;
  }

  onLogOut(): void {
    this.userService.logout(API, TOKEN);
    this.router.navigate([AppHtmlRoutes.login]);
  }
}
