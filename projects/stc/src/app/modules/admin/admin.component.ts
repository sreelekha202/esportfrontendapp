import {
  AfterViewInit,
  Component,
  OnInit,
  ViewChild,
  OnDestroy,
  PLATFORM_ID,
  Inject,
} from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import { Router } from '@angular/router';
import { ScreenService } from '../../shared/service/screen/screen.service';
import { SidenavService } from '../../shared/service/sidenav/sidenav.service';
import { EsportsUserService, IUser, GlobalUtils } from 'esports';
import { Subscription } from 'rxjs';
import { isPlatformBrowser } from '@angular/common';
import { environment } from '../../../environments/environment';

const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss'],
})
export class AdminComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild('sidenav') public sidenav: MatSidenav;
  isLgScreen = false;
  currenUser: IUser;
  userSubscription: Subscription;
  isBrowser: boolean;
  constructor(
    private screenService: ScreenService,
    private userService: EsportsUserService,
    private router: Router,
    private sidenavService: SidenavService,
    @Inject(PLATFORM_ID) private platformId
  ) {
    this.isBrowser = isPlatformBrowser(platformId);
  }

  ngOnInit(): void {
    if (this.isBrowser) {
      if (this.router.url == '/admin') {
        const deafultUrl = this.userService.fetchAdminDefaultAccess();
        if (deafultUrl) {
          this.router.navigate([deafultUrl]);
        }
      }
    }
  }

  ngAfterViewInit(): void {
    this.sidenavService.setSidenav(this.sidenav);
    this.screenService.onAppResizeListener();
    this.initSubscriptions();
  }

  ngOnDestroy() {
    this.userSubscription?.unsubscribe();
  }

  private initSubscriptions(): void {
    if (GlobalUtils.isBrowser()) {
      setTimeout(() => {
        this.screenService.isLgScreen.subscribe((isLgScreen: boolean) => {
          this.isLgScreen = isLgScreen;

          if (isLgScreen) {
            this.sidenavService.close();
          } else {
            this.sidenavService.open();
          }
        });
      });
    }
  }
}
