import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  AfterViewInit,
  ChangeDetectorRef,
  Inject,
  PLATFORM_ID,
} from '@angular/core';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';

import {
  GameService,
  TournamentService,
  UtilsService,
  HomeService
} from '../../core/service';
import { AppHtmlRoutes } from '../../app-routing.model';
import { TranslateService } from '@ngx-translate/core';
// import { ArticleItemComponentData } from '../../core/article-item/article-item.component';
// import { SwiperConfigInterface } from 'ngx-swiper-wrapper';
import {
  EsportsLanguageService,
  EsportsConstantsService,
  IPagination,
  SuperProperties,
  EventProperties,
  EsportsGtmService,
  IUser,
  EsportsUserService,
  EsportsTimezone
} from 'esports';
import { Router } from '@angular/router';
import { isPlatformBrowser } from '@angular/common';

@Component({
  selector: 'app-tournament',
  templateUrl: './tournament.component.html',
  styleUrls: ['./tournament.component.scss'],
  providers: [NgbCarouselConfig],
})
export class TournamentComponent implements OnInit, AfterViewInit {
  @ViewChild('mainContainer', { read: ElementRef })
  public scroll: ElementRef<any>;
  AppHtmlRoutes = AppHtmlRoutes;
  faSearch = faSearch;
  isBrowser: boolean;
  showNavigationArrows = false;
  showNavigationIndicators = true;

  tournamentData;
  tournamentsData = [];
  tournamentList = { ongoing: [], upcoming: [], completed: [] };
  allTournamentList: any = [];
  allTournamentListClone: any = [];

  gameList = [];
  slectedGame: any = { all: true };
  paginationData = {
    page: 1,
    limit: 8,
    sort: 'Latest',
  };
  currentTab = 'ongoing';
  activeTab = 'ongoing';
  gameId;
  showLoader = true;
  page1: IPagination;
  page2: IPagination;
  timezone;
  ladderTournamentData;
  type: string = '';
  currentUser: IUser;
  isShowParticipants;

  constructor(
    config: NgbCarouselConfig,
    private tournamentService: TournamentService,
    private gameService: GameService,
    private translateService: TranslateService,
    private languageService: EsportsLanguageService,
    private ConstantsService: EsportsConstantsService,
    private utilsService: UtilsService,
    private cdr: ChangeDetectorRef,
    private gtmService: EsportsGtmService,
    private userService: EsportsUserService,
    private homeService: HomeService,
    private router: Router,
    private esportsTimezone: EsportsTimezone,
    @Inject(PLATFORM_ID) private platformId
  ) {
    this.isBrowser = isPlatformBrowser(platformId);
    config.showNavigationArrows = true;
    config.showNavigationIndicators = true;
  }

  ngOnInit(): void {
    if(this.isBrowser){
    this.timezone = this.esportsTimezone.getTimeZoneName();
    this.languageService.language.subscribe((lang) =>
      this.translateService.use(
        lang || this.ConstantsService?.defaultLangCode
      )
    );
    this.getPartOfTournament();
    this.getOnGoingTournamentsPaginated(false);
    this.getOnGoingTournamentsPaginated(true);
    this.getGames();

    this.userService.currentUser.subscribe(data => {
      if (data) {
        this.currentUser = data;
      }
    })
    this.getShowParticipantsStatus();}
  }

  isNumber(val): boolean { const str: string = val.toString(); return str.startsWith("+"); }

  ngAfterViewInit() {
    this.utilsService.scrollToTop(this.scroll);
  }

  getPartOfTournament() {
    this.tournamentService
      .getPaginatedTournaments({
        status: '6',
        limit: 8,
        sort: 'participantJoined',
      })
      .subscribe(
        (res: any) => (this.tournamentsData = res.data.docs),
        (err) => {}
      );
  }

  getGames() {
    const encodeUrl = `?query=${encodeURIComponent(
      JSON.stringify({ isTournamentAllowed: true })
    )}`;
    this.gameService.getAllGames(encodeUrl).subscribe(
      (res) => {
        this.gameList = res.data;
      },
      (err) => {}
    );
  }

  filterByGame(gameId) {
    this.paginationData.page = 1;
    this.slectedGame = {};
    this.slectedGame[gameId] = true;
    if (gameId === 'all') {
      this.gameId = null;
    } else {
      this.gameId = gameId;
    }

    switch (this.currentTab) {
      case 'ongoing':
        this.type == 'ladder' ? this.getOnGoingTournamentsPaginated(true) : this.getOnGoingTournamentsPaginated(false);
        break;
      case 'completed':
        this.type == 'ladder' ? this.getOnGoingTournamentsPaginated(true) : this.getOnGoingTournamentsPaginated(false);
        break;
      case 'upcoming':
        this.type == 'ladder' ? this.getOnGoingTournamentsPaginated(true) : this.getOnGoingTournamentsPaginated(false);
        break;
    }
  }

  getCompletedTournamentsPaginated(ladder?) {
    this.showLoader = true;
    const pagination = JSON.stringify(this.paginationData);
    let query: any = { tournamentStatus: 'publish', isFinished: true };
    if (this.gameId) {
      query.gameDetail = this.gameId;
    }
    query = JSON.stringify(query);
    const params = {
      status: '2',
      page: this.paginationData.page,
      limit: this.paginationData.limit,
      sort: 'Latest',
      ladder: false
    };
    if(ladder) params.ladder = true;
    if (this.gameId) {
      params['game'] = this.gameId;
    }
    this.tournamentService.getPaginatedTournaments(params).subscribe(
      (res: any) => {
        if(ladder){
          this.showLoader = false;
          this.ladderTournamentData = res?.data?.docs;
          this.page1 = {
            totalItems: res?.data?.totalDocs,
            itemsPerPage: res?.data?.limit,
            maxSize: 5,
          };
        } else {
          this.showLoader = false;
          this.tournamentData = res?.data?.docs;
          this.page2 = {
            totalItems: res?.data?.totalDocs,
            itemsPerPage: res?.data?.limit,
            maxSize: 5,
          };
        }
      },
      (err) => {
        this.showLoader = false;
        console.error(err);
      }
    );
  }

  getOnGoingTournamentsPaginated(ladder?) {
    this.showLoader = true;
    const pagination = JSON.stringify(this.paginationData);
    let query: any = {
      $and: [{ tournamentStatus: 'publish' }],
      $or: [{ startDate: { $lt: new Date() } }, { isSeeded: true }],
    };
    if (this.gameId) {
      query.gameDetail = this.gameId;
    }
    query = JSON.stringify(query);
    const params = {
      status: '1',
      page: this.paginationData.page,
      limit: this.paginationData.limit,
      sort: 'Latest',
      ladder: false
    };
    if(ladder) params.ladder = true;
    if (this.gameId) {
      params['game'] = this.gameId;
    }
    this.tournamentService.getPaginatedTournaments(params).subscribe(
      (res: any) => {
        if(ladder){
          this.showLoader = false;
          this.ladderTournamentData = res?.data?.docs;
          this.page1 = {
            totalItems: res?.data?.totalDocs,
            itemsPerPage: res?.data?.limit,
            maxSize: 5,
          };
        } else {
          this.showLoader = false;
          this.tournamentData = res?.data?.docs;
          this.page2 = {
            totalItems: res?.data?.totalDocs,
            itemsPerPage: res?.data?.limit,
            maxSize: 5,
          };
        }
      },
      (err) => {
        this.showLoader = false;
        console.error(err);
      }
    );
  }

  getUpcomingTournamentsPaginated(ladder?) {
    this.showLoader = true;
    const pagination = JSON.stringify(this.paginationData);
    let query: any = {
      $and: [
        { tournamentStatus: 'publish' },
        { startDate: { $gt: new Date() } },
        { isSeeded: false },
        { isFinished: false },
      ],
    };

    if (this.gameId) {
      query.gameDetail = this.gameId;
    }
    query = JSON.stringify(query);
    const params = {
      status: '0',
      page: this.paginationData.page,
      limit: this.paginationData.limit,
      sort: 'Latest',
      ladder: false
    };
    if(ladder) params.ladder = true;
    if (this.gameId) {
      params['game'] = this.gameId;
    }
    this.tournamentService.getPaginatedTournaments(params).subscribe(
      (res: any) => {
        if(ladder){
          this.showLoader = false;
          this.ladderTournamentData = res?.data?.docs;
          this.page1 = {
            totalItems: res?.data?.totalDocs,
            itemsPerPage: res?.data?.limit,
            maxSize: 5,
          };
        } else {
          this.showLoader = false;
          this.tournamentData = res?.data?.docs;
          this.page2 = {
            totalItems: res?.data?.totalDocs,
            itemsPerPage: res?.data?.limit,
            maxSize: 5,
          };
        }

      },
      (err) => {
        this.showLoader = false;
        console.error(err);
      }
    );
  }

  pageChanged(page, type?): void {
    this.paginationData.page = page;
    type == 'ladder' ? this.type = type : ''
    type == 'ladder' ? this.currentTab = this.activeTab : this.currentTab
    switch (this.currentTab) {
      case 'ongoing':
        type == 'ladder' ? this.getOnGoingTournamentsPaginated(true) : this.getOnGoingTournamentsPaginated(false);
        break;
      case 'completed':
        type == 'ladder' ? this.getCompletedTournamentsPaginated(true) : this.getCompletedTournamentsPaginated(false);
        break;
      case 'upcoming':
        type == 'ladder' ? this.getUpcomingTournamentsPaginated(true) : this.getUpcomingTournamentsPaginated(false);
        break;
    }
  }
  changeTab(value, type?) {
    this.paginationData.page = 1;
    switch (value) {
      case 'ongoing':
        type == 'ladder' ? this.activeTab = 'ongoing' : this.currentTab = 'ongoing';
        type == 'ladder' ? this.getOnGoingTournamentsPaginated(true) : this.getOnGoingTournamentsPaginated(false);
        this.pushGTMTags('View_Ongoing_Tournament');
        break;
      case 'completed':
        type == 'ladder' ? this.activeTab = 'completed' : this.currentTab = 'completed';
        type == 'ladder' ? this.getCompletedTournamentsPaginated(true) : this.getCompletedTournamentsPaginated(false);
        break;
      case 'upcoming':
        type == 'ladder' ? this.activeTab = 'upcoming' : this.currentTab = 'upcoming';
        type == 'ladder' ? this.getUpcomingTournamentsPaginated(true) : this.getUpcomingTournamentsPaginated(false);
        this.pushGTMTags('View_Upcoming_Tournament')
        break;
    }
  }

  scrollToTop() {
    this.scroll.nativeElement.scrollIntoView();
  }

  ngAfterViewChecked() {
    this.cdr.detectChanges();
  }

  addSTCTournamentGTMTag(tournamentName){
    this.pushGTMTags('View_Stc_Play_Tournament', tournamentName);
  }

  addCreateTournamentGTMTag(){
    this.pushGTMTags('Create_Tournament_Click');
  }

  pushGTMTags(eventName: string, tournamentName: string = null, tournamentDetail = null) {
    let superProperties: SuperProperties = {};
    if (this.currentUser) {
      superProperties = this.gtmService.assignLoggedInUsedData(
        this.currentUser
      );
    }

    let eventProperties: EventProperties = {};

    if (eventName == 'View_Stc_Play_Tournament') {
      eventProperties['fromPage'] = this.router.url;
      eventProperties['tournamentName'] = tournamentName;
    }
    if (['View_Ongoing_Tournament', 'View_Upcoming_Tournament', ''].includes(eventName)) {
      eventProperties['fromPage'] = this.router.url;
      if (tournamentDetail) {
        eventProperties['tournamentName'] = tournamentDetail.name;
        eventProperties['title'] = tournamentDetail.name;
      }
    }

    this.gtmService.gtmEventWithSuperProp({
      eventName: eventName,
      superProps: superProperties,
      eventProps: eventProperties,
    });
  }

  async getShowParticipantsStatus(){
    try {
     const data = await this.homeService.isShowParticipants();
     this.isShowParticipants = data?.showParticipant;
    } catch (error) {
      console.error(error?.error?.message && error?.message);
    }
  }

  viewTournamentGTM(tournamentDetails, TournamentBracketType) {
    if (TournamentBracketType == 'ladder') {
      if (this.activeTab == 'upcoming') {
        this.pushGTMTags('View_Upcoming_Tournament', null, tournamentDetails);
      } else if (this.activeTab == 'ongoing') {
        this.pushGTMTags('View_Ongoing_Tournament', null, tournamentDetails);
      }
    } else {
      if (this.currentTab == 'upcoming') {
        this.pushGTMTags('View_Upcoming_Tournament', null, tournamentDetails);
      } else if (this.currentTab == 'ongoing') {
        this.pushGTMTags('View_Ongoing_Tournament', null, tournamentDetails);
      }
    }
  }

}
