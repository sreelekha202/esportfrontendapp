import { Component, OnInit } from '@angular/core';
import { AppHtmlRoutes } from '../../../app-routing.model';
import { TranslateService } from '@ngx-translate/core';
import { EsportsLanguageService, EsportsConstantsService } from 'esports';

@Component({
  selector: 'app-tournament-info',
  templateUrl: './tournament-info.component.html',
  styleUrls: ['./tournament-info.component.scss'],
})
export class TournamentInfoComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;

  constructor(
    private translateService: TranslateService,
    private languageService: EsportsLanguageService,
    private ConstantsService: EsportsConstantsService
  ) {}

  ngOnInit(): void {
    this.languageService.language.subscribe((lang) =>
      this.translateService.use(
        lang || this.ConstantsService?.defaultLangCode
      )
    );
  }
}
