import { Component, Input, OnInit } from '@angular/core';
import { FormArray, FormGroup } from '@angular/forms';
import { EsportsUserService, EsportsToastService } from 'esports';
import { TranslateService } from '@ngx-translate/core';
@Component({
  selector: 'app-venue-address',
  templateUrl: './venue-address.component.html',
  styleUrls: ['./venue-address.component.scss'],
})
export class VenueAddressComponent implements OnInit {
  @Input() customFormArrayName: string;
  @Input() index: number;
  @Input() customFormGroup: FormGroup;
  @Input() type: string;

  countryList = [];
  stageMatchFormat = {
    quarterFinal: 'TOURNAMENT.QUARTER_FINAL',
    semiFinal: 'TOURNAMENT.SEMI_FINAL',
    final: 'TOURNAMENT.FINAL',
  };

  stateList = [];

  constructor(
    private userService: EsportsUserService,
    private eSportsToastService: EsportsToastService,
    private translateService: TranslateService
  ) {}

  ngOnInit(): void {
    this.fetchCountries();
  }

  fetchCountries = async () => {
    try {
      this.customFormGroup.value.venueAddress[this.index].region = '';
      const data = await this.userService.getAllCountries().toPromise();
      this.countryList = data.countries.filter((item) =>
        [191, 117, 17].includes(item.id)
      );
      if (this.customFormGroup.value.venueAddress[this.index].country) {
        this.onCountryChange(
          this.customFormGroup.value.venueAddress[this.index].country,
          true
        );
      }
    } catch (error) {
      this.eSportsToastService.showError(error?.message);
    }
  };
 sortByKey(array, key) {
    return array.sort(function (a, b) {
      var x = a[key];
      var y = b[key];
      return x < y ? -1 : x > y ? 1 : 0;
    });
  }
  onCountryChange = async (countryname, isEdit) => {
    this.stateList.length = 0;
    if (!isEdit) {
      ((this.customFormGroup.get('venueAddress') as FormArray)?.controls[
        this.index
      ] as FormGroup)
        ?.get('region')
        .reset('');
    }
    const country = this.countryList.find((item) => item.name == countryname);
    const data = await this.userService.getStates().toPromise();
    this.stateList = data.states.filter(
      (item) => item.country_id == country.id
    );
    if (this.translateService.currentLang == 'ar') {
      this.stateList = this.sortByKey(this.stateList, 'ar');
    }
  };
}
