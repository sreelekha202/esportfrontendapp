import {
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
  OnDestroy
} from '@angular/core';
import {
  TournamentService,
  TransactionService,
  AuthServices,
} from '../../../../../core/service';
import { Router } from '@angular/router';
import { IUser, EsportsUserService, EsportsToastService } from 'esports';
import { environment } from '../../../../../../environments/environment';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss'],
})
export class PaymentComponent implements OnInit, OnChanges, OnDestroy {
  @Input() tournament;
  selectPaymentType = '';
  transaction;
  isOtpValid = false;
  isProccesing = true;
  user: IUser;
  paymentMethods = [];
  prizePools = [];
  totalAmounts = [];
  isPaymentLoaded = false;
  isPayfort = true;
  payfortForm = {
    command: '',
    access_code: '',
    merchant_identifier: '',
    merchant_reference: '',
    amount: '',
    currency: '',
    language: '',
    customer_email: '',
    signature: '',
    order_description: '',
    return_url: '',
    phone_number: '',
    customer_name: '',
  };
  currenUser: IUser;
  payfort = 'payfort';
  userSubscription: Subscription;

  constructor(
    private transactionService: TransactionService,
    private tournamentService: TournamentService,
    private eSportsToastService: EsportsToastService,
    private router: Router,
    private userService: EsportsUserService,
    private authServices: AuthServices
  ) {}

  ngOnInit(): void {
    this.getPaymentAccountStatus();
  }

  ngOnDestroy() {
    this.userSubscription?.unsubscribe();
  }

  populatePayfortForm = async () => {
    try {
      this.userSubscription = this.userService.currentUser.subscribe((data) => {
        if (data) {
          this.currenUser = data;
        }
      });
      if (this.transaction.currencyCode == 'SAR') {
        this.isPayfort = true;
      } else {
        this.isPayfort = false;
      }
      this.payfortForm.command = 'PURCHASE';
      this.payfortForm.access_code = environment.payfort.access_code;
      this.payfortForm.merchant_identifier =
        environment.payfort.merchant_identifier;
      this.payfortForm.merchant_reference = this.transaction._id;
      this.payfortForm.amount = (this.transaction.totalAmount * 100).toString();
      this.payfortForm.amount = parseInt(this.payfortForm.amount).toString();
      this.payfortForm.currency = this.transaction.currencyCode;
      this.payfortForm.language = 'ar';
      this.payfortForm.customer_email = this.currenUser.email.toString();
      this.payfortForm.order_description = this.tournament._id;
      this.payfortForm.return_url =
        environment.apiEndPoint + 'transaction/payfortVerify';
      this.payfortForm.phone_number = this.currenUser.phoneNumber.toString();
      this.payfortForm.customer_name = this.currenUser.fullName.toString();
      const signatureData = await this.transactionService.payfortSign(
        this.payfortForm
      );
      this.payfortForm.signature = signatureData.data;
    } catch (error) {
      this.eSportsToastService.showError(error?.error?.message || error?.message);
      this.isProccesing = false;
    }
  };

  fetchtransaction = async () => {
    try {
      this.isPaymentLoaded = true;
      const payload = {
        tournamentId: this.tournament?._id,
        type: 'organizer',
      };

      const { data } = await this.transactionService.createOrUpdateTransaction(
        payload
      );

      this.totalAmounts.push({
        prizeLabel: 'PROFILE.TOURNAMENTS.PAYMENT.PRIZE_AMOUNT',
        amount: data.totalAmount - data.platformFee,
      });
      this.totalAmounts.push({
        prizeLabel: 'PROFILE.TOURNAMENTS.PAYMENT.TOTAL_AMOUNT',
        amount: data.totalAmount,
      });

      this.prizePools = data?.prizeList?.map((item) => {
        let img;

        if (item.name == '1st') {
          img = '../assets/images/payment/Gold.svg';
        } else if (item.name == '2nd') {
          img = '../assets/images/payment/Silver.svg';
        } else if (item.name == '3rd') {
          img = '../assets/images/payment/Bronze.svg';
        } else {
          img = '../assets/images/payment/Bronze.svg';
        }

        return {
          ...item,
          img,
        };
      });

      this.transaction = data;
      // this.populatePayfortForm();
      if (this.transaction.provider == 'paypal') {
        this.paymentMethods = [
          {
            img: '../assets/images/payment/paypal.png',
            name: 'Paypal',
            value: 'paypal',
          },
        ];
      } else if (this.transaction.provider == 'stcpay') {
        this.paymentMethods = [
          {
            img: '../assets/images/payment/stc_play.png',
            name: 'stc pay',
            value: 'stcpay',
          },
        ];
      } 
      // else if (this.transaction.provider == 'payfort'){
      //   this.paymentMethods = [
      //     {
      //     img: '../assets/images/payment/payfort.png',
      //     name: 'Payfort',
      //     value: 'payfort',
      //   }
      // ];
      // }
      this.isProccesing = false;
    } catch (error) {
      this.eSportsToastService.showError(error?.error?.message || error?.message);
      this.isProccesing = false;
    }
  };

  getPaymentResponse = async (order) => {
    try {
      this.isProccesing = true;
      const payload = {
        tournamentId: this.tournament?._id,
        transactionId: this.transaction?._id,
        txnId: order?.id,
        provider: 'paypal',
      };
      await this.tournamentService
        .updateTournament({ paymentType: 'paypal' }, this.tournament._id)
        .toPromise();
      const response = await this.transactionService.createOrUpdateTransaction(
        payload
      );
      this.eSportsToastService.showSuccess(response?.message);
      this.router.navigate(['/profile/my-tournament/created']);
      this.isProccesing = false;
    } catch (error) {
      this.eSportsToastService.showError(error?.error?.message || error?.message);
      this.isProccesing = false;
    }
  };

  getStcPaymentResponse = async (order) => {
    try {
      const payload = {
        tournamentId: this.tournament?._id,
        type: 'stcpay',
        mobileNo: order.e164Number,
      };
      await this.tournamentService
        .updateTournament({ paymentType: 'stcpay' }, this.tournament._id)
        .toPromise();
      const response = await this.transactionService.createOrUpdateTransaction(
        payload
      );
      this.transaction = response;
      this.eSportsToastService.showSuccess(response?.message);
    } catch (error) {
      this.eSportsToastService.showError(
        error?.error?.data?.Text || error?.error?.message || error?.message
      );
    }
  };

  getStcOtpVerify = async (params) => {
    try {
      const payload = {
        OtpReference: this.transaction['data'][
          'DirectPaymentAuthorizeV4ResponseMessage'
        ]['OtpReference'],
        STCPayPmtReference: this.transaction['data'][
          'DirectPaymentAuthorizeV4ResponseMessage'
        ]['STCPayPmtReference'],
        otp: params,
        id: this.transaction.data.id,
        tournamentId: this.tournament?._id,
      };
      const response = await this.transactionService.verifyStcTransaction(
        payload
      );
      this.eSportsToastService.showSuccess(response?.message);
      this.router.navigate(['/profile/my-tournament/created']);
    } catch (error) {
      this.eSportsToastService.showError(error?.error?.message || error?.message);
    }
  };

  ngOnChanges(simpleChanges: SimpleChanges) {
    if (simpleChanges.hasOwnProperty('tournament') && this.tournament) {
      if (!this.isPaymentLoaded) {
        this.fetchtransaction();
      }
    }
  }

  getPaymentAccountStatus = async () => {
    try {
      const auth = await this.authServices.getPaymentAccountVerfiedStatus();
    } catch (error) {
      this.eSportsToastService.showError(error?.error?.message || error?.message);
    }
  };
}
