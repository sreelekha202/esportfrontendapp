import {
  Component,
  OnInit,
  Inject,
  ViewChild,
  ElementRef,
  TemplateRef,
  OnDestroy,
} from '@angular/core';
import {
  faUpload,
  faBars,
  faThList,
  faTrash,
  faPlus,
  faPlusCircle,
  faCopy,
} from '@fortawesome/free-solid-svg-icons';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl,
  FormArray,
  AbstractControl,
  ValidatorFn,
} from '@angular/forms';
import {
  TournamentService,
  UtilsService,
  GameService,
} from '../../../core/service';
import {
  EsportsUserService,
  EsportsLanguageService,
  EsportsConstantsService,
  EsportsToastService,
  IUser,
  EsportsTimezone,
  EsportsGtmService,
  EventProperties,
  SuperProperties,
} from 'esports';
import { HttpClient } from '@angular/common/http';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import {
  debounceTime,
  distinctUntilChanged,
  map,
  switchMap,
  catchError,
} from 'rxjs/operators';
import { Observable } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { DOCUMENT } from '@angular/common';
import { SwiperConfigInterface } from 'ngx-swiper-wrapper';
import { S3UploadService } from '../../../shared/service/s3Upload.service';
import { faInfoCircle, faInfo } from '@fortawesome/free-solid-svg-icons';
import { SearchCountryField, CountryISO } from 'ngx-intl-tel-input';
import { environment } from '../../../../environments/environment';
import {
  InfoPopupComponent,
  InfoPopupComponentData,
  InfoPopupComponentType,
} from '../../../shared/popups/info-popup/info-popup.component';
import { MatDialog } from '@angular/material/dialog';
import { TranslateService } from '@ngx-translate/core';
import 'rxjs/add/operator/filter';
import { Subscription } from 'rxjs';

const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss'],
})
export class CreateComponent implements OnInit, OnDestroy {
  @ViewChild('mainContainer', { read: ElementRef })
  public scroll: ElementRef<any>;

  @ViewChild('longContent') private regionModal: TemplateRef<any>;
  dialog: NgbModalRef | null;

  public config: SwiperConfigInterface = {
    // a11y: true,
    direction: 'horizontal',
    slidesPerView: 'auto',
    keyboard: false,
    mousewheel: false,
    scrollbar: false,
    navigation: true,
    pagination: true,
    centeredSlides: true,
    slideToClickedSlide: true,
    breakpoints: {
      320: {
        slidesPerView: 3,
        spaceBetween: 15,
      },
      575: {
        slidesPerView: 4,
        spaceBetween: 15,
        autoHeight: false,
      },

      1199: {
        slidesPerView: 'auto',
        autoHeight: true,
      },
    },
  };
  editTournament: boolean = false;
  currentId: any;
  counterValue: number = 2;
  active = 1;
  condition_active = 'description';
  faUpload = faUpload;
  faBars = faBars;
  faThList = faThList;
  tournamentForm: FormGroup;
  gameArray = [];
  editorConfig = {};
  searchKeyword: any;
  minCheckInOpenDateValue = this.setMinDate();
  maxCheckInOpenDateValue: any;
  minCheckInClosingDateValue = this.setMinDate();
  maxCheckInClosingDateValue: any;
  minStartDateValue = this.setMinDate();
  isParticipantRegistered: boolean = false;
  regionSearchKeyword: any;
  regionsArray = [];
  sponsors: any = [];
  faTrash = faTrash;
  faPlus = faPlus;
  faCopy = faCopy;
  timezone;
  gameSearchText = '';

  userId;
  domain;
  currencyList = [
    { name: 'SAR', flag: 'assets/images/flags/AR.png' },
    { name: 'BHD', flag: 'assets/images/flags/bahrain.png' },
    { name: 'KWD', flag: 'assets/images/flags/kuwait.png' },
  ];
  venueAddress: any = [];
  matchFormat = {
    1: 'TOURNAMENT.BEST_OF_1',
    3: 'TOURNAMENT.BEST_OF_3',
    5: 'TOURNAMENT.BEST_OF_5',
    7: 'TOURNAMENT.BEST_OF_7',
  };
  isSpreateDialer: boolean = false;

  stageMatchFormat = {
    quarterFinal: 'TOURNAMENT.QUARTER_FINAL',
    semiFinal: 'TOURNAMENT.SEMI_FINAL',
    final: 'TOURNAMENT.FINAL',
  };

  contactOptionsList = [
    'Facebook',
    'WhatsApp',
    'Email',
    'Twitter',
    'Discord',
    'None',
  ];
  selected = {
    phoneNumber: '',
  };
  stageBracketType = ['single', 'double'];

  selectedFiles: FileList;
  bannerSRC;
  selectedLogos: FileList;
  srcLogos = [];
  urlAlreadyExist = false;
  selectedParticipants = [];
  faInfoCircle = faInfoCircle;
  faInfo = faInfo;
  faPlusCircle = faPlusCircle;

  requiredFields = {
    step1: [
      { name: 'CREATE_TOURNAMENT_VALIDATION.KEY1', value: 'name' },
      { name: 'CREATE_TOURNAMENT_VALIDATION.KEY2', value: 'gameDetail' },
      { name: 'CREATE_TOURNAMENT_VALIDATION.KEY3', value: 'participantType' },
      { name: 'CREATE_TOURNAMENT_VALIDATION.KEY4', value: 'startDate' },
      { name: 'CREATE_TOURNAMENT_VALIDATION.KEY5', value: 'startTime' },
      { name: 'CREATE_TOURNAMENT_VALIDATION.KEY6', value: 'bracketType' },
      { name: 'CREATE_TOURNAMENT_VALIDATION.KEY7', value: 'noOfSet' },
      { name: 'CREATE_TOURNAMENT_VALIDATION.KEY8', value: 'stageMatch' },
      { name: 'CREATE_TOURNAMENT_VALIDATION.KEY9', value: 'stageMatchNoOfSet' },
      { name: 'CREATE_TOURNAMENT_VALIDATION.KEY11', value: 'regStartDate' },
      { name: 'CREATE_TOURNAMENT_VALIDATION.KEY12', value: 'regEndDate' },
      { name: 'CREATE_TOURNAMENT_VALIDATION.KEY13', value: 'regStartTime' },
      { name: 'CREATE_TOURNAMENT_VALIDATION.KEY14', value: 'regEndTime' },
      { name: 'CREATE_TOURNAMENT_VALIDATION.KEY15', value: 'gameSelection' },
      { name: 'TOURNAMENT.NUMBER_OF_ROUND', value: 'noOfRoundPerGroup' },
      { name: 'TOURNAMENT.CHECK_IN_START_TIME', value: 'checkInStartDate' },
      { name: 'TOURNAMENT.REG_FEE_CURRENCY', value: 'regFeeCurrency' },
      { name: 'TOURNAMENT.REG_FEE', value: 'regFee' },
      { name: 'TOURNAMENT.TEAM_SIZE', value: 'teamSize' },
      { name: 'TOURNAMENT.PARTICIPANT_LIMIT', value: 'maxParticipants' },
      { name: 'TOURNAMENT.NUMBER_OF_PALCEMENT', value: 'noOfPlacement' },
      { name: 'TOURNAMENT.POINT_PER_KILLS', value: 'pointsKills' },
      { name: 'TOURNAMENT.INPUT_PLACEMENT_POINTS', value: 'placementPoints' },
      {
        name: 'TOURNAMENT.NO_OF_PARTICIPANT_PER_GROUP',
        value: 'noOfTeamInGroup',
      },
      {
        name: 'TOURNAMENT.NO_OF_WINNING_PARTICIPANT_PER_GROUP',
        value: 'noOfWinningTeamInGroup',
      },
      { name: 'TOURNAMENT.NO_OF_ROUNDS_PER_GROUP', value: 'noOfRoundPerGroup' },
      { name: 'TOURNAMENT.PLATFORM', value: 'platform' },
      { name: 'TOURNAMENT.END_DATE', value: 'endDate' },
      { name: 'TOURNAMENT.END_TIME', value: 'endTime' },
      { name: 'TOURNAMENT.TEAM_FORMAT', value: 'teamFormat' },
      { name: 'TOURNAMENT.STAGE_BRACKET_TYPE', value: 'stageBracketType' },
      {
        name: 'TOURNAMENT.SUBSTITUTE_MEMBER_SIZE',
        value: 'substituteMemberSize',
      },
      {
        name: 'TOURNAMENT.NUMBER_OF_ROUND',
        value: 'noOfRound',
      },
      {
        name: 'TOURNAMENT.NUMBER_OF_LOSS',
        value: 'noOfLoss',
      },
      {
        name: 'TOURNAMENT.SET_DURATION',
        value: 'setDuration',
      },
    ],
    step2: [
      { name: 'TOURNAMENT.TOURNAMENT_TYPE', value: 'tournamentType' },
      { name: 'TOURNAMENT.ADD_MONEY_PRIZE', value: 'prizeList' },
      { name: 'TOURNAMENT.SPONSORS', value: 'sponsors' },
      { name: 'TOURNAMENT.ADD_CURRENCY', value: 'prizeCurrency' },
      { name: 'TOURNAMENT.VENUE_ADDRESS', value: 'venueAddress' },
    ],
    step3: [
      { name: 'TOURNAMENT.TOURNAMENT_URL', value: 'url' },
      { name: 'TOURNAMENT.TWITCH_VIDEO_LINK', value: 'twitchVideoLink' },
      { name: 'TOURNAMENT.YOUTUBE_VIDEO_LINK', value: 'youtubeVideoLink' },
      { name: 'TOURNAMENT.FACEBOOK_VIDEO_LINK', value: 'facebookVideoLink' },
      { name: 'TOURNAMENT.CONTACT_DETAILS', value: 'whatsApp' },
      { name: 'TOURNAMENT.CONTACT_REQUIRED', value: 'contactDetails' },
    ],
    step4: [],
  };
  invalidFieldList = [];
  nextButtonClicked = {
    step1: false,
    step2: false,
  };
  currentUrlToCheck = '';
  checkInTimeOptions = [];

  selectedSponsorBanners: FileList;
  facebookRegex: any =
    /(?:(?:http|https):\/\/)?(?:www.|m.)?facebook.com\/(?!home.php)(?:(?:\w)*#!\/)?(?:pages\/)?(?:[?\w\-]*\/)?(?:profile.php\?id=(?=\d.*))?([\w\.-]+)/;
  emailRegex =
    /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  phoneRegex =
    /^\s*(?:\+?(\d{1,3}))?[-. (]*(\d{3})[-. )]*(\d{3})[-. ]*(\d{4})(?: *x(\d+))?\s*$/;
  youtubeRegex = /^(https?\:\/\/)?((www\.)?youtube\.com|youtu\.?be)\/.+$/;
  twitchRegex = /(?:https:\/\/)?((www\.)?twitch\.tv)\/(\S+)/;
  twitterRegex = /http(?:s)?:\/\/(?:www\.)?twitter\.com\/([a-zA-Z0-9_]+)/;
  playStoreUrlRegex = /^(https?\:\/\/)?((www\.)?play\.google\.com)\/.+$/;
  appStoreUrlRegex = /^(https?\:\/\/)?((www\.)?itunes\.apple\.com)\/.+$/;
  webUrlRegex =
    /(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})/;

  SearchCountryField = SearchCountryField;
  CountryISO = CountryISO;
  preferredCountries: CountryISO[] = [
    CountryISO.SaudiArabia,
    CountryISO.Bahrain,
    CountryISO.Kuwait,
  ];
  countryCode;
  isPaymentDone = false;
  selectedGameIndex = 2;
  timObjClone;
  isActive = true;
  tournamentPath = 'tournament/';
  phoneNumberObject: any;
  // isConfirmed = false;
  participantType = {
    individual: 'TEAM.ONE',
    team: 'TOURNAMENT.TEAM',
  };

  showLoader = true;
  userFullName;
  isProcessing = false;
  bracketList: Array<any> = [];
  platform = [];
  teamFormat = {
    2: 'TOURNAMENT.DUO',
    4: 'TOURNAMENT.SQUAD',
    n: 'TOURNAMENT.vs',
  };

  rrFormat = {
    1: 'OPTIONS.RR_1',
    2: 'OPTIONS.RR_2',
  };

  enableteamCounter = false;
  enableSubstituteTeamCounter = false;
  isCharged = false;
  isArabic: boolean;
  fallbackURL: string;
  activeTab: number;
  prizeLimit = 6;
  substituteMemberLimit: number = 1;
  userSubscription: Subscription;
  hasRequiredAccess: boolean = true;
  user: IUser;
  searchSubscription: Subscription;
  inputSubscription: Subscription;
  searchResults: any[];
  customLoader: Boolean = false;
  searchInput = new FormControl('');
  pagination = {
    page: 1,
    limit: 100,
    hasNextPage: true,
  };

  constructor(
    private http: HttpClient,
    public eSportsToastService: EsportsToastService,
    private gameService: GameService,
    private tournamentService: TournamentService,
    private fb: FormBuilder,
    private modalService: NgbModal,
    private userService: EsportsUserService,
    private route: Router,
    private s3Service: S3UploadService,
    private utilsService: UtilsService,
    private activeRoute: ActivatedRoute,
    private matDialog: MatDialog,
    private translateService: TranslateService,
    private languageService: EsportsLanguageService,
    private esportsTimezone: EsportsTimezone,
    private gtmService: EsportsGtmService,
    @Inject(DOCUMENT) private document: Document
  ) {}

  ngOnDestroy() {
    this.userSubscription?.unsubscribe();
    this.inputSubscription?.unsubscribe();
    this.searchSubscription?.unsubscribe();
  }

  ngOnInit(): void {
    this.timezone = this.esportsTimezone.getTimeZoneName();
    this.checkInTimeOptions = EsportsConstantsService?.checkInTimeOptions;
    this.languageService.language.subscribe((lang) => {
      this.isArabic = Boolean(lang === 'ar');
    });
    this._initiateTournament();
    this.getCurrentUserDetails();
    this.fetchRegions();
    this.domain =
      this.document.location.protocol +
      '//' +
      this.document.location.hostname +
      '/';
    // this.insertCalenderStyle('', 'transparent');
    const { id } = this.activeRoute.snapshot.params;
    this.editTournament = !!id;
    if (!id) {
      this.getGames();
    } else {
      this.fetchTournamentDetails(id);
    }

    this.activeRoute.queryParams.subscribe((params) => {
      if (params['fallback']) {
        this.fallbackURL = '/admin/esports-management';
        this.activeTab = params['activeTab'] || 0;
      }
    });

    this.editorConfig = {
      editable: true,
      spellcheck: true,
      height: '100px',
      width: 'auto',
      minWidth: '0',
      translate: 'yes',
      enableToolbar: true,
      showToolbar: true,
      defaultParagraphSeparator: '',
      defaultFontName: '',
      defaultFontSize: '',
      fonts: [
        { class: 'arial', name: 'Arial' },
        { class: 'times-new-roman', name: 'Times New Roman' },
        { class: 'calibri', name: 'Calibri' },
        { class: 'comic-sans-ms', name: 'Comic Sans MS' },
      ],
      customClasses: [
        {
          name: 'quote',
          class: 'quote',
        },
        {
          name: 'redText',
          class: 'redText',
        },
        {
          name: 'titleText',
          class: 'titleText',
          tag: 'h1',
        },
      ],
      sanitize: true,
      toolbarPosition: 'top',
      toolbarHiddenButtons: [['bold', 'italic'], ['fontSize']],
    };
    this.handleSearchInput();
  }
  /**
   * Initializing tournament form with its default value and validation
   */
  _initiateTournament() {
    this.tournamentForm = this.fb.group(
      {
        name: [
          '',
          Validators.compose([Validators.required, Validators.maxLength(100)]),
        ],
        url: ['', Validators.compose([Validators.required])],
        participantType: ['', Validators.compose([Validators.required])],
        startDate: [
          '',
          Validators.compose([
            Validators.required,
            this.ValidateStartDate.bind(this),
          ]),
        ],
        startTime: [
          '',
          Validators.compose([
            Validators.required,
            this.ValidateStartTime.bind(this),
          ]),
        ],
        endDate: [
          '',
          Validators.compose([
            Validators.required,
            this.ValidateEndDate.bind(this),
          ]),
        ],
        endTime: [
          '',
          Validators.compose([
            Validators.required,
            this.ValidateEndTime.bind(this),
          ]),
        ],
        isPaid: [false, Validators.compose([])],
        isRegStartDate: [false, Validators.compose([])],
        isRegEndDate: [false, Validators.compose([])],
        description: ['', Validators.compose([])],
        rules: ['', Validators.compose([])],
        criticalRules: ['', Validators.compose([])],
        isPrize: [false, Validators.compose([])],
        faqs: ['', Validators.compose([])],
        schedule: ['', Validators.compose([])],
        isIncludeSponsor: [false, Validators.compose([])],
        tournamentType: ['', Validators.compose([Validators.required])],
        isScreenshotRequired: [false, Validators.compose([])],
        isShowCountryFlag: [false, Validators.compose([])],
        isManualApproverParticipant: [false, Validators.compose([])],
        isSpecifyAllowedRegions: [false, Validators.compose([])],
        isParticipantsLimit: [true, Validators.compose([])],
        scoreReporting: [2, Validators.compose([])],
        invitationLink: ['', Validators.compose([])],
        youtubeVideoLink: [
          '',
          Validators.compose([Validators.pattern(this.youtubeRegex)]),
        ],
        participants: [[], Validators.compose([])],
        facebookVideoLink: [
          '',
          Validators.compose([Validators.pattern(this.facebookRegex)]),
        ],
        contactDetails: [''],
        twitchVideoLink: [
          '',
          Validators.compose([Validators.pattern(this.twitchRegex)]),
        ],
        visibility: [1, Validators.compose([])],
        checkInEndDate: ['', Validators.compose([])],
        regionsAllowed: [[], Validators.compose([])],
        sponsors: this.fb.array([]),
        banner: ['', Validators.compose([])],
        // country: ['', Validators.compose([Validators.required])],
        maxParticipants: [
          2,
          Validators.compose([
            Validators.required,
            Validators.min(2),
            Validators.max(1000),
            this.teamInGroupValidation('maxParticipants').bind(this),
          ]),
        ],
        bracketType: ['', Validators.compose([Validators.required])],
        noOfSet: ['', Validators.compose([Validators.required])],
        stageMatch: [''],
        stageMatchNoOfSet: [''],
        noOfRoundPerGroup: [1],
        isAllowMultipleRounds: [false],
        //commenting because none of the type is selected by default
        //venueAddress: this.fb.array([this.createVenueAddress('offline')]),
        venueAddress: this.fb.array([]),
        contactOn: ['', Validators.compose([])],
        gameDetail: ['', Validators.compose([Validators.required])],
        teamSize: [
          '',
          // Validators.compose([Validators.max(10), Validators.min(2)]),
        ],
        whatsApp: ['', Validators.compose([])],
        prizeList: this.fb.array([], this.customPrizeValidator()),
        noOfPlacement: [2],
        placementPoints: this.fb.array([]),
        substituteMemberSize: [0],
        isKillPointRequired: [false, Validators.required],
        platform: ['', Validators.required],
        teamFormat: [''],
        allowSubstituteMember: [false],
        allowAdvanceStage: [false],
        stageBracketType: [''],
        step: [1],
        timezone: ['', Validators.compose([])],
      },
      {
        validator: this.formValidation(),
      }
    );
  }

  /**
   *
   * @param formData updated form data
   * Function to save tournament.
   */
  submitTournament = async (status) => {
    try {
      if (
        this.tournamentForm.value.contactOn == 'WhatsApp' &&
        this.phoneNumberObject
      ) {
        this.tournamentForm.controls.whatsApp.setValue(this.phoneNumberObject);
      }
      if (this.tournamentForm.value.startTime) {
        this.timObjClone = Object.assign(
          {},
          this.tournamentForm.value.startTime
        );
      }

      if (!this.userId) {
        this.eSportsToastService.showError(
          this.translateService.instant('TOURNAMENT.PLEASE_LOGIN')
        );
        return;
      }

      if (this.urlAlreadyExist) {
        this.eSportsToastService.showError(
          this.translateService.instant('TOURNAMENT.URL_TAKEN')
        );
        return;
      }

      if (!this.tournamentForm.valid) {
        this.markFormGroupTouched(this.tournamentForm);
        this.eSportsToastService.showError(
          this.translateService.instant('TOURNAMENT.INVALID_FORM')
        );
        return;
      }

      const formValues = JSON.parse(JSON.stringify(this.tournamentForm?.value));
      if (formValues.isCheckInRequired) {
        formValues.checkInStartDate = this.getCheckInDt(
          formValues.startDate,
          formValues.startTime,
          formValues.checkInStartDate
        );
      }

      let { hour, minute } = formValues.startTime;
      formValues.startTime = this.utilsService.convertTimeIntoString(
        (hour < 10 ? '0' + hour : hour) +
          ':' +
          (minute < 10 ? '0' + minute : minute) +
          ''
      );

      if (formValues?.isRegStartDate) {
        hour = formValues.regStartTime?.hour;
        minute = formValues.regStartTime?.minute;
        formValues.regStartTime = this.utilsService.convertTimeIntoString(
          (hour < 10 ? '0' + hour : hour) +
            ':' +
            (minute < 10 ? '0' + minute : minute) +
            ''
        );
      }

      if (formValues?.isRegEndDate) {
        hour = formValues.regEndTime?.hour;
        minute = formValues.regEndTime?.minute;
        formValues.regEndTime = this.utilsService.convertTimeIntoString(
          (hour < 10 ? '0' + hour : hour) +
            ':' +
            (minute < 10 ? '0' + minute : minute) +
            ''
        );
      }
      // formValues.country = 'IN';
      const currentDate = new Date();
      const regEndDateChanged = this.tournamentForm.get('regEndDate');
      const regEndTimeChanged = this.tournamentForm.get('regEndTime');
      if (
        (regEndTimeChanged?.dirty || regEndDateChanged?.dirty) &&
        this.setStartDate('regEndDate', 'regEndTime') >
          JSON.stringify(currentDate)
      ) {
        formValues.isRegistrationClosed = false;
      }
      formValues.createdBy = this.userId;
      formValues.updatedBy = this.userId;
      formValues.tournamentStatus = status;
      formValues.organizerDetail = this.userId;
      formValues.timezone = this.timezone;
      formValues.startDate = this.setStartDate('startDate', 'startTime');
      formValues.endDate = this.setStartDate('endDate', 'endTime');
      if (formValues?.isRegStartDate) {
        formValues.regStartDate = this.setStartDate(
          'regStartDate',
          'regStartTime'
        );
      }

      if (formValues?.isRegEndDate) {
        formValues.regEndDate = this.setStartDate('regEndDate', 'regEndTime');
      }
      if (typeof formValues.contactDetails == 'object') {
        formValues.contactDetails = formValues.contactDetails.e164Number;
      }
      if (typeof formValues.contactDetails == 'number' && formValues?._id) {
        formValues.contactDetails = '+' + formValues.contactDetails.toString();
      }

      if (formValues.url) {
        formValues.url = this.domain + this.tournamentPath + formValues.url;
      }

      const data: InfoPopupComponentData = {
        ...this.popUpTitleAndText(formValues),
        type: InfoPopupComponentType.confirm,
        btnText: this.translateService.instant('TOURNAMENT.CONFIRM'),
      };

      const confirmed = await this.matDialog
        .open(InfoPopupComponent, { data, width: '400px' })
        .afterClosed()
        .toPromise();

      if (confirmed) {
        this.gtmEventBasicStepCreated();
        this.showLoader = true;
        delete formValues.teamFormat;
        delete formValues.endTime;
        const response = formValues?._id
          ? await this.tournamentService
              .updateTournament(formValues, formValues?._id)
              .toPromise()
          : await this.tournamentService.saveTournament(formValues).toPromise();

        if (this.editTournament) {
          this.pushGTMTags('Publish_Complete');
        }
        this.eSportsToastService.showSuccess(response?.message);
        if (response?.data?.enablePayment) {
          this.setTournamentValue(response.data);
        } else if (this.fallbackURL) {
          this.route.navigate([this.fallbackURL], {
            queryParams: { activeTab: this.activeTab },
          });
        } else {
          this.route.navigate(['/profile/my-tournament/created']);
        }

        this.showLoader = false;
      }
    } catch (error) {
      this.showLoader = false;
      this.eSportsToastService.showError(
        error?.error?.message || error?.message
      );
    }
  };

  popUpTitleAndText = (data) => {
    if (data?._id) {
      return {
        title: this.translateService.instant('TOURNAMENT.UPDATE'),
        text: this.translateService.instant('TOURNAMENT.UPDATE_TXT'),
      };
    } else {
      return {
        title: this.translateService.instant('TOURNAMENT.SAVE_TOURNAMENT'),
        text: this.translateService.instant('TOURNAMENT.STATUS_2'),
      };
    }
  };

  dateValidation(selectedDate) {
    if (selectedDate == 'start') {
      // this.insertCalenderStyle('.startDate', 'white');
      if (this.tournamentForm.value.startDate) {
        this.maxCheckInOpenDateValue = this.tournamentForm.value.startDate;
        this.maxCheckInClosingDateValue = this.tournamentForm.value.startDate;
      }
    }

    if (selectedDate == 'open') {
      // this.insertCalenderStyle('.checkInOpeningDate', 'white');
      if (this.tournamentForm.value.checkInStartDate) {
        this.minCheckInClosingDateValue =
          this.tournamentForm.value.checkInStartDate;
        this.minStartDateValue = this.tournamentForm.value.checkInEndDate
          ? this.tournamentForm.value.checkInEndDate
          : this.tournamentForm.value.checkInStartDate;
      }
    }
    if (selectedDate == 'close') {
      // this.insertCalenderStyle('.checkInClosingDate', 'white');
      if (this.tournamentForm.value.checkInEndDate) {
        this.maxCheckInOpenDateValue = this.tournamentForm.value.checkInEndDate;
        this.minStartDateValue = this.tournamentForm.value.checkInEndDate;
      }
    }
  }

  isControlHasError(
    controlName: string,
    validationType: string,
    arrayField?: string,
    index?: number
  ): boolean {
    const control = this.tournamentForm.controls[controlName];
    let result;
    if (!control) {
      return false;
    }

    if (arrayField) {
      let control =
        this.tournamentForm.controls[controlName]['controls'][index][
          'controls'
        ][arrayField];

      result =
        control.hasError(validationType) && (control.dirty || control.touched);
    } else {
      result =
        control.hasError(validationType) && (control.dirty || control.touched);
    }
    return result;
  }

  getErrorMessage(validationType) {
    switch (validationType) {
      case 'required':
        return this.translateService.instant('TOURNAMENT.REQUIRED_FIELD');
      case 'min':
        return this.translateService.instant('TOURNAMENT.VALUE_ZERO');
      case 'int':
        return this.translateService.instant('TOURNAMENT.DECIMALS_NOT');
    }
  }

  /**
   * Tournament name change handler
   */
  tournamentChangeHandler(name) {
    if (!name) {
      return;
    }
    let nameArray = name.split(' ');
    let url = nameArray.join('_');
    this.tournamentForm.controls.url.setValue(url);
  }

  /**
   * Return next tab reference
   */
  getNextTab() {
    return this.active <= 3 ? this.active + 1 : 1;
  }

  /**
   * Return previous tab reference
   */
  getPreviousTab() {
    return this.active >= 2 ? this.active - 1 : 4;
  }

  getGames = async () => {
    try {
      const encodeUrl = `?query=${encodeURIComponent(
        JSON.stringify({
          isTournamentAllowed: true,
          searchBy: this.gameSearchText,
        })
      )}`;
      const { data } = await this.gameService
        .getAllGames(encodeUrl)
        .toPromise();
      if (Array.isArray(data)) {
        if (this.isArabic) {
          this.gameArray = data.reverse();
        } else {
          this.gameArray = data;
        }
      }

      if (!this.tournamentForm?.value?._id && this.gameArray.length) {
        this.setGameOnSlide(Math.floor(this.gameArray.length / 2), true);
      }

      this.showLoader = false;
    } catch (error) {
      this.showLoader = false;
      this.eSportsToastService.showError(
        error?.error?.message || error?.message
      );
    }
  };
  searchByValueFilter(event) {
    if (
      (this.gameSearchText != '' && event.target.value.trim() == '') ||
      (event.target.value.trim() != '' &&
        this.gameSearchText != event.target.value.trim())
    ) {
      this.gameSearchText = event.target.value.trim();
      this.getGames();
    }
  }
  fetchRegions = async () => {
    try {
      const data = await this.userService.getStates().toPromise();

      this.regionsArray = data.states
        .filter((item) => item.country_id == 191)
        .concat(data.states.filter((item) => item.country_id == 117))
        .concat(data.states.filter((item) => item.country_id == 17));
    } catch (error) {
      this.eSportsToastService.showError(error?.message);
    }
  };

  setGameOnSlide(index, enableToast = true) {
    this.selectedGameIndex = index;
    this.tournamentForm.controls.gameDetail.setValue(
      this.gameArray[index]?._id
    );

    this.bracketList = EsportsConstantsService.bracketType.filter(
      (item) => this.gameArray[index].bracketTypes[item.value]
    );
    this.platform = this.gameArray[index]?.platform;

    if (!this.tournamentForm?.value?._id) {
      this.tournamentForm.get('bracketType').setValue('');
      this.tournamentForm.get('platform').setValue('');

      this.tournamentForm.get('bracketType').updateValueAndValidity();
      this.tournamentForm.get('platform').updateValueAndValidity();
    }

    if (enableToast) {
      this.eSportsToastService.showSuccess(
        `<h6>${this.gameArray[index]['name']} ${this.translateService.instant(
          'TOURNAMENT.SELECTED'
        )}</h6>`
      );
    }
  }

  setMinDate() {
    let date = new Date();
    let year: any = date.getFullYear();
    let month: any = date.getMonth() + 1;
    month = month < 10 ? '0' + month : month;
    let day: any = date.getDate();
    day = day < 10 ? '0' + day : day;
    let d = `${year}-${month}-${day}`;
    return d;
  }

  getStartTime() {
    if (
      this.tournamentForm.value.startTime &&
      this.tournamentForm.value.startDate
    ) {
      const startDate = new Date(this.tournamentForm.value.startDate);
      const { hour, minute } = this.tournamentForm.value.startTime;
      startDate.setHours(hour, minute);
      return startDate;
    } else {
      return '';
    }
  }

  selectGame(id, index) {
    this.tournamentForm.controls.gameDetail.setValue(id);
    this.eSportsToastService.showSuccess(
      `<h6>${this.gameArray[index]['name']} ${this.translateService.instant(
        'TOURNAMENT.SELECTED'
      )}</h6>`
    );
  }

  getGameData(key) {
    let check = typeof this.tournamentForm.value.gameDetail;
    if (check === 'string') {
      for (let game of this.gameArray) {
        if (game._id == this.tournamentForm.value.gameDetail) {
          return game[key];
        }
      }
    } else if (check === 'object') {
      for (let game of this.gameArray) {
        if (game._id == this.tournamentForm.value.gameDetail._id) {
          return game[key];
        }
      }
    } else {
      return '';
    }
  }

  regionHandler = async (event) => {
    try {
      if (event?.target?.checked) {
        const result = await this.modalService.open(this.regionModal, {
          scrollable: true,
          windowClass: 'custom-modal-content modal-regions-tournament',
          centered: true,
        }).result;
        this.updateRegionConfig();
      } else if (event?.target?.checked == false) {
        this.tournamentForm.value.regionsAllowed.length = 0;
      }
    } catch (error) {
      this.updateRegionConfig();
    }
  };

  updateRegionConfig = () => {
    if (this.tournamentForm.value.regionsAllowed.length < 1) {
      this.tournamentForm.controls.isSpecifyAllowedRegions.setValue(false);
    }
  };

  addRegion(event, id) {
    if (event.target.checked) {
      this.tournamentForm.value.regionsAllowed.push(id);
    } else {
      const index = this.tournamentForm.value.regionsAllowed.indexOf(id);
      if (index >= 0) {
        this.tournamentForm.value.regionsAllowed.splice(index, 1);
      }
    }
  }

  removeRegions(index) {
    this.tournamentForm.value.regionsAllowed.splice(index, 1);
    this.updateRegionConfig();
  }

  createSponsor(): FormGroup {
    // if (!this.tournamentForm?.value?.isIncludeSponsor) {
    //   return this.fb.group({});
    // }
    return this.fb.group({
      sponsorName: ['', Validators.compose([Validators.required])],
      website: ['', Validators.compose([Validators.pattern(this.webUrlRegex)])],
      playStoreUrl: [
        '',
        Validators.compose([Validators.pattern(this.playStoreUrlRegex)]),
      ],
      appStoreUrl: [
        '',
        Validators.compose([Validators.pattern(this.appStoreUrlRegex)]),
      ],
      sponsorLogo: ['', Validators.compose([Validators.required])],
      sponsorBanner: ['', Validators.compose([Validators.required])],
    });
  }

  addSponsor(): void {
    this.sponsors = this.tournamentForm.get('sponsors') as FormArray;
    this.sponsors.push(this.createSponsor());
  }

  sponsorClickHandler(event, sponsors?) {
    if (!event.target.checked) {
      let sponsors = <FormArray>this.tournamentForm.controls['sponsors'];
      for (let i = sponsors.length - 1; i >= 0; i--) {
        sponsors.removeAt(i);
      }
    } else if (event.target.checked) {
      if (!this.tournamentForm.value.sponsors.length && !sponsors) {
        this.addSponsor();
      }
      if (sponsors) {
        for (let i = 0; i < sponsors.length; i++) {
          this.addSponsor();
        }
      }
    }
  }

  get sponsorControls() {
    return this.tournamentForm.get('sponsors')['controls'];
  }

  removeSponsor(i: number) {
    this.sponsors.removeAt(i);
    if (this.tournamentForm.value.sponsors.length < 1) {
      this.tournamentForm.controls.isIncludeSponsor.setValue(false);
    }
  }

  getSponsors() {
    if (this.tournamentForm.value.sponsors.length) {
      let sponsor = this.tournamentForm.value.sponsors
        .reduce((acc, currentValue) => {
          return acc + currentValue['sponsorName'] + ', ';
        }, '')
        .replace(/,\s*$/, '');

      return sponsor ? sponsor : '';
    } else {
      return '';
    }
  }

  isParticipantAlreadyAdded(arr, _id) {
    const { length } = arr;
    const id = length + 1;
    const found = arr.some((el) => el?._id === _id);
    return found;
  }

  removeParticipant(index) {
    this.tournamentForm.value.participants.splice(index, 1);
  }

  getCurrentUserDetails() {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.userFullName = data['fullName'];
        this.user = data;
      }
      this.userId = data?._id;
    });
  }

  paidRegistrationHandler(event) {
    if (event.target.checked) {
      if (!this.tournamentForm.contains('regFeeCurrency')) {
        let validatorFn = this.fb.control('', [Validators.required]);
        this.tournamentForm.addControl('regFeeCurrency', validatorFn);
      }

      if (!this.tournamentForm.contains('regFee')) {
        let validatorFnRegFee = this.fb.control(
          '',
          Validators.compose([Validators.required, Validators.min(1)])
        );
        this.tournamentForm.addControl('regFee', validatorFnRegFee);
      }
    } else if (!event.target.checked) {
      if (this.tournamentForm.contains('regFeeCurrency')) {
        this.tournamentForm.removeControl('regFeeCurrency');
      }

      if (this.tournamentForm.contains('regFee')) {
        this.tournamentForm.removeControl('regFee');
      }
    }
  }

  regStartDateHandler(isStartDtRequired: boolean = false) {
    if (isStartDtRequired) {
      const regStartDtValidatorFn = this.fb.control(
        '',
        Validators.compose([
          Validators.required,
          this.ValidateRegStartDate.bind(this),
        ])
      );

      const regStartTimeValidatorFn = this.fb.control(
        '',
        Validators.compose([
          Validators.required,
          this.ValidateRegStartTime.bind(this),
        ])
      );
      this.tournamentForm.addControl('regStartDate', regStartDtValidatorFn);
      this.tournamentForm.addControl('regStartTime', regStartTimeValidatorFn);
    } else {
      this.tournamentForm.removeControl('regStartDate');
      this.tournamentForm.removeControl('regStartTime');
    }
  }

  regEndDateHandler(isEndDtRequired: boolean = false) {
    if (isEndDtRequired) {
      const regEndDateValidatorFn = this.fb.control(
        '',
        Validators.compose([
          Validators.required,
          this.ValidateRegEndDate.bind(this),
        ])
      );
      const regEndTimeValidatorFn = this.fb.control(
        '',
        Validators.compose([
          Validators.required,
          this.ValidateRegEndTime.bind(this),
        ])
      );

      this.tournamentForm.addControl('regEndDate', regEndDateValidatorFn);
      this.tournamentForm.addControl('regEndTime', regEndTimeValidatorFn);
    } else {
      this.tournamentForm.removeControl('regEndDate');
      this.tournamentForm.removeControl('regEndTime');
    }
  }

  prizeClickHandler(checked, prizeLen = 1) {
    if (checked) {
      this.tournamentForm.removeControl('prizeList');
      this.tournamentForm.addControl(
        'prizeList',
        this.fb.array([], this.customPrizeValidator())
      );
      for (let i = 0; i < prizeLen; i++) this.addPrize(i);
      this.tournamentForm.addControl(
        'prizeCurrency',
        new FormControl('', Validators.compose([Validators.required]))
      );
    } else {
      this.tournamentForm.removeControl('prizeList');
      this.tournamentForm.removeControl('prizeCurrency');
    }
  }

  customPrizeValidator(): ValidatorFn {
    return (formArray: FormArray): { [key: string]: any } | null => {
      let total = 0;
      for (let item of formArray.controls) {
        total = total + item.value.value;
      }
      const maxParticipants =
        formArray['_parent']?.controls?.maxParticipants?.value;
      const prizeLimit = maxParticipants > 6 ? 6 : maxParticipants;
      if (formArray.controls.length > prizeLimit) {
        return { prizeLimit: true };
      }
      const isPrizeValid = formArray.controls.length ? total > 0 : true;
      return isPrizeValid ? null : { prizeMoneyRequired: true };
    };
  }

  createVenueAddress(type): FormGroup {
    if (['offline', 'hybrid'].includes(type)) {
      return this.fb.group({
        country: ['', Validators.compose([Validators.required])],
        region: ['', Validators.compose([Validators.required])],
        venue: ['', Validators.compose([Validators.required])],
        ...(type == 'hybrid' && {
          stage: ['', Validators.compose([Validators.required])],
        }),
      });
    }
  }

  addVenueAddress(type): void {
    this.venueAddress = this.tournamentForm.get('venueAddress') as FormArray;
    this.venueAddress.push(this.createVenueAddress(type));
  }

  venueClickHandler(type, venueLength = 1) {
    if (this.tournamentForm.value.venueAddress.length) {
      this.clearFormArray('venueAddress');
    }

    if (type == 'offline' || type == 'hybrid') {
      for (let i = 0; i < venueLength; i++) this.addVenueAddress(type);
    }
  }

  get venueControls() {
    return this.tournamentForm.get('venueAddress')['controls'];
  }
  removeVenue(i: number) {
    this.venueAddress.removeAt(i);
  }

  setVenueAddressFieldValues(value, field, index) {
    let venueArray = this.tournamentForm.value.venueAddress;
    venueArray[index][field] = value;
    this.tournamentForm.controls.venueAddress.setValue(venueArray);
  }

  setContactOn(val) {
    this.tournamentForm.controls.contactDetails.setValue('');
    this.tournamentForm.controls.whatsApp.setValue('');
    this.tournamentForm.controls.contactOn.setValue(val);
    switch (val) {
      case 'WhatsApp':
        this.isSpreateDialer = true;
        this.tournamentForm.controls['contactDetails'].setValidators([
          Validators.required,
        ]);
        break;
      case 'Email':
        this.tournamentForm.controls['contactDetails'].setValidators([
          Validators.required,
          Validators.pattern(this.emailRegex),
        ]);
        break;
      case 'Facebook':
        this.tournamentForm.controls['contactDetails'].setValidators([
          Validators.required,
          Validators.pattern(this.facebookRegex),
        ]);
        break;
      case 'Twitter':
        this.tournamentForm.controls['contactDetails'].setValidators([
          Validators.required,
          Validators.pattern(this.twitterRegex),
        ]);
        break;
      case 'None':
        this.tournamentForm.controls.contactDetails.setValue('');
        this.tournamentForm.controls.contactOn.setValue('');
        this.tournamentForm.controls.contactDetails.clearValidators();
        break;
      default:
        this.tournamentForm.controls['contactDetails'].setValidators([
          Validators.required,
        ]);
        break;
    }

    this.tournamentForm.get('contactDetails').updateValueAndValidity();
  }

  setRegFeeCurrency(val) {
    this.tournamentForm.controls.regFeeCurrency.setValue(val);
  }

  setPrizeCurrency(val) {
    this.tournamentForm.controls.prizeCurrency.setValue(val);
  }

  participantTypeHandler(val) {
    if (val == 'team') {
      this.tournamentForm.controls['teamFormat'].setValidators([
        Validators.required,
      ]);
      this.tournamentForm.controls['teamSize'].setValidators([
        Validators.required,
        Validators.max(10),
        Validators.min(2),
      ]);
    } else {
      this.tournamentForm.controls['teamSize'].clearValidators();
      this.tournamentForm.controls['teamFormat'].clearValidators();
      this.tournamentForm.controls['teamSize'].setValue(0);
      this.tournamentForm.controls['teamFormat'].setValue('');
      this.tournamentForm.controls['substituteMemberSize'].clearValidators();
      this.tournamentForm.controls['substituteMemberSize'].setValue(0);
    }
    this.tournamentForm.controls['teamFormat'].updateValueAndValidity();
    this.tournamentForm.controls['teamSize'].updateValueAndValidity();
  }

  copyInputMessage() {
    let text =
      this.domain + this.tournamentPath + this.tournamentForm.value.url;
    this.copyToClipboard(text);
  }

  copyToClipboard(text) {
    var dummyElement = this.document.createElement('textarea');
    this.document.body.appendChild(dummyElement);
    dummyElement.value = text;
    dummyElement.select();
    this.document.execCommand('copy');
    this.document.body.removeChild(dummyElement);
    this.eSportsToastService.showSuccess(
      this.translateService.instant('TOURNAMENT.URL_COPY')
    );
  }

  toBase64(file) {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = (error) => reject(error);
    });
  }

  async upload(event) {
    this.showLoader = true;
    // let bannerFormData = new FormData();
    // for (let i = 0; i < this.selectedFiles.length; i++) {
    //   bannerFormData.set(
    //     `file${i}`,
    //     this.selectedFiles[i],
    //     this.selectedFiles[i]['name']
    //   );
    // }

    const files = event.target.files;
    let promises = [];
    for (let obj of files) {
      promises.push(this.toBase64(obj));
    }
    let Base64String = await Promise.all(promises);
    let imageData = {
      path: environment.tournamentS3BucketName,
      files: Base64String,
    };

    this.s3Service.fileUpload(imageData).subscribe(
      (res) => {
        this.showLoader = false;
        this.tournamentForm.controls.banner.setValue(
          res['data'][0]['Location']
        );
        this.bannerSRC = res['data'][0]['Location'];
        this.eSportsToastService.showSuccess(
          this.translateService.instant('TOURNAMENT.BANNER_UPLOADED')
        );
      },
      (err) => {
        this.showLoader = false;
        this.eSportsToastService.showError(
          this.translateService.instant('TOURNAMENT.ERROR_LOGO')
        );
      }
    );
    // let filePath = bannerDetails.Key.split('.')[0];
    // let lastIndexSlash = filePath.lastIndexOf('/');
    // this.s3RefKey = filePath.substring(lastIndexSlash + 1);
  }

  selectFile(event) {
    // let FILE = (event.target as HTMLInputElement).files;
    // this.selectedFiles = FILE;
    this.isValidImage(event, { width: 1920, height: 300 }, 'tBanner');
  }

  isValidImage(event: any, values, type): Boolean {
    if (event.target.files && event.target.files[0]) {
      if (event.target.files[0]['size'] / (1000 * 1000) > 2) {
        this.eSportsToastService.showError(
          this.translateService.instant('TOURNAMENT.ERROR_IMG_2')
        );
        return;
      }

      if (
        event.target.files[0].type != 'image/jpeg' &&
        event.target.files[0].type != 'image/jpg' &&
        event.target.files[0].type != 'image/png'
      ) {
        this.eSportsToastService.showError(
          this.translateService.instant('TOURNAMENT.ERROR_IMG_1')
        );
        return;
      }

      try {
        const file = event.target.files[0];

        const reader = new FileReader();
        reader.readAsDataURL(file);
        let th = this;
        reader.onload = (e) => {
          let image: any = new Image();
          image.src = e.target.result;
          image.onload = function () {
            let height = this.height;
            let width = this.width;
            if (height != values['height'] && width != values['width']) {
              th.eSportsToastService.showInfo(
                th.translateService.instant('TOURNAMENT.ERROR_IMG')
              );
            }
            switch (type) {
              case 'tBanner':
                th.upload(event);
                break;
              case 'sLogo':
                th.uploadLogo(event, values['index']);
                break;
              case 'sBanner':
                th.uploadSponsorBanner(event, values['index']);
                break;
            }
          };
        };
      } catch (err) {
        return false;
      }
    } else {
      return false;
    }
  }

  async uploadLogo(event, index) {
    this.showLoader = true;
    // let logoFormData = new FormData();

    // for (let i = 0; i < this.selectedLogos.length; i++) {
    //   logoFormData.set(
    //     `file${i}`,
    //     this.selectedLogos[i],
    //     this.selectedLogos[i]['name']
    //   );
    // }

    const files = event.target.files;
    let promises = [];
    for (let obj of files) {
      promises.push(this.toBase64(obj));
    }
    let Base64String = await Promise.all(promises);
    let imageData = {
      path: environment.tournamentS3BucketName,
      files: Base64String,
    };

    this.s3Service.fileUpload(imageData).subscribe(
      (res) => {
        this.showLoader = false;
        this.tournamentForm.controls.sponsors['controls'][index]['controls'][
          'sponsorLogo'
        ].setValue(res['data'][0]['Location']);
      },
      (err) => {
        this.showLoader = false;
        this.eSportsToastService.showError(
          this.translateService.instant('TOURNAMENT.ERROR_LOGO')
        );
      }
    );
  }

  selectLogo(event, index) {
    // let FILE = (event.target as HTMLInputElement).files;
    // this.selectedLogos = FILE;
    this.isValidImage(
      event,
      { width: 180, height: 180, index: index },
      'sLogo'
    );
  }

  checkIfUrlExistOnUrlChange = async () => {
    try {
      const url =
        this.domain + this.tournamentPath + this.tournamentForm.value.url;
      const query = JSON.stringify({
        url,
        ...(this.tournamentForm?.value?._id && {
          _id: { $ne: this.tournamentForm?.value?._id },
        }),
      });
      const { data } = await this.tournamentService
        .getTournaments({ query })
        .toPromise();

      if (data?.length > 0) {
        this.urlAlreadyExist = true;
      }
    } catch (error) {
      this.eSportsToastService.showError(
        error?.error?.message || error?.message
      );
    }
  };

  checkIfUrlExistOnNameChange() {
    let url = this.domain + this.tournamentPath + this.tournamentForm.value.url;
    this.currentUrlToCheck = this.tournamentForm.value.url;
    let query = JSON.stringify({ url: url });
    this.checkUrlCall(query);
  }

  recursiveCheckForAvailableURL() {
    let url = this.domain + this.tournamentPath + this.currentUrlToCheck;
    let query = JSON.stringify({ url: url });
    this.checkUrlCall(query);
  }

  checkUrlCall(query) {
    this.tournamentService.getTournaments({ query: query }).subscribe(
      (res) => {
        if (res['data'].length > 0) {
          let alreadyExistUrl = res['data'][0]['url'];
          let constantPartOfURL = this.domain + this.tournamentPath;
          alreadyExistUrl = alreadyExistUrl.replace(constantPartOfURL, '');
          let endNumbers = alreadyExistUrl.match(/\d+$/);
          if (endNumbers) {
            let numberPart = parseInt(endNumbers);
            numberPart++;
            let newUrl =
              alreadyExistUrl.slice(0, -endNumbers[0].length) + numberPart;
            this.currentUrlToCheck = newUrl;
            this.recursiveCheckForAvailableURL();
          } else {
            let newUrl = alreadyExistUrl + '1';
            this.currentUrlToCheck = newUrl;
            this.recursiveCheckForAvailableURL();
          }
        } else {
          this.tournamentForm.controls.url.setValue(this.currentUrlToCheck);
        }
      },
      (err) => {}
    );
  }

  searchUsers(searchKeyword) {
    this.userService.searchUsers(API, searchKeyword).subscribe(
      (res) => {
        return res['data'];
      },
      (err) => {}
    );
  }

  markAsTouched(control: any) {
    control.markAsTouched();
    if (control.controls) {
      if (Array.isArray(control.controls)) {
        for (let obj of control.controls) {
          for (let key in obj.controls) {
            obj.controls[key].markAsTouched();
          }
        }
      }
    }
  }

  isStepValid(step, content?) {
    const getErrorTitle = (step) => {
      switch (step) {
        case 'step1':
          return this.translateService.instant('STEP_TITLE.STEP1');
        case 'step2':
          return this.translateService.instant('STEP_TITLE.STEP2');
        case 'step3':
          return this.translateService.instant('STEP_TITLE.STEP3');
      }
    };

    if (
      this.tournamentForm.value.contactOn == 'WhatsApp' &&
      this.phoneNumberObject
    ) {
      this.tournamentForm.controls.whatsApp.setValue(this.phoneNumberObject);
    }
    this.nextButtonClicked[step] = true;
    this.invalidFieldList = [];
    let invalidFieldListValue = [];
    for (let obj of this.requiredFields[step]) {
      if (this.tournamentForm?.controls?.[obj['value']]?.invalid) {
        this.invalidFieldList.push(obj.name);
        invalidFieldListValue.push(obj.value);
      }
    }

    /* game selection is mandatory */
    if(this.gameArray && !this.gameArray.length){
      this.invalidFieldList.push('CREATE_TOURNAMENT_VALIDATION.KEY15');
      invalidFieldListValue.push('gameSelection');
    }
    else if(this.gameArray && this.gameArray.length){
      this.invalidFieldList.splice(this.invalidFieldList.indexOf('CREATE_TOURNAMENT_VALIDATION.KEY15'),1);
      invalidFieldListValue.splice(invalidFieldListValue.indexOf('gameSelection'),1);
    }

    if (this.invalidFieldList.length > 0) {
      this.setContactDetail();

      for (let key in this.tournamentForm.controls) {
        if (invalidFieldListValue.includes(key)) {
          this.markAsTouched(this.tournamentForm.controls[key]);
        }
      }

      let message = `<h2 [ngClass]="{'arabicTitle':isArabic}">${getErrorTitle(
        step
      )}</h2><div>${this.translateService.instant(
        'TOURNAMENT.ERROR_TITLE'
      )}</div><div class='invalidFields'><ul class="NumberList">`;
      for (let obj of this.invalidFieldList)
        message += `<li> ${this.translateService.instant(obj)} </li>`;
      message += '</ul></div>';

      this.eSportsToastService.showError(message);
      return false;
    } else {
      this.setGTMTagsOnNextClick(step);
      this.setContactDetail();
      if (this.urlAlreadyExist) {
        this.eSportsToastService.showError(
          this.translateService.instant('TOURNAMENT.URL_TAKEN')
        );
        return;
      } else {
        this.tournamentForm.get('step').setValue(this.active);
        this.gtmEventBasicStepCreate();
        return true;
      }
    }
  }

  /* Start Date Minus CheckIn Hour */
  getCheckInDt(startDate, startTime, checkInStartDate) {
    const startDt = new Date(startDate);
    startDt.setHours(startTime.hour || 0, startTime.minute || 0);
    const duration = this.checkInTimeOptions.find(
      (item) => item.name == checkInStartDate
    ).value;
    const checkInDt = new Date(startDt);
    checkInDt.setHours(
      checkInDt.getHours() - duration.hour,
      checkInDt.getMinutes() - duration.minute
    );
    return checkInDt.toISOString();
  }

  setCheckInStartTime(val) {
    this.tournamentForm.controls.checkInStartDate.setValue(val);
  }

  selectSponsorBanner(event, index) {
    // let FILE = (event.target as HTMLInputElement).files;
    // this.selectedSponsorBanners = FILE;
    this.isValidImage(
      event,
      { width: 1200, height: 200, index: index },
      'sBanner'
    );
  }

  async uploadSponsorBanner(event, index) {
    this.showLoader = true;
    const files = event.target.files;
    let promises = [];
    for (let obj of files) {
      promises.push(this.toBase64(obj));
    }
    let Base64String = await Promise.all(promises);
    let imageData = {
      path: environment.tournamentS3BucketName,
      files: Base64String,
    };

    this.s3Service.fileUpload(imageData).subscribe(
      (res) => {
        this.showLoader = false;
        this.tournamentForm.controls.sponsors['controls'][index]['controls'][
          'sponsorBanner'
        ].setValue(res['data'][0]['Location']);
      },
      (err) => {
        this.showLoader = false;
        this.eSportsToastService.showError(
          this.translateService.instant('TOURNAMENT.ERROR_BANNER')
        );
      }
    );
  }

  removeBanner() {
    this.bannerSRC = '';
    this.tournamentForm.controls.banner.setValue('');
  }

  markFormGroupTouched(formGroup: FormGroup) {
    (<any>Object).values(formGroup.controls).forEach((control) => {
      control.markAsTouched();

      if (control.controls) {
        this.markFormGroupTouched(control);
      }
    });
  }

  checkInDateHandler(event) {
    this.tournamentForm.controls.checkInStartDate.setValue('');
    if (event.target.checked) {
      this.tournamentForm.controls['checkInStartDate'].setValidators([
        Validators.required,
        this.ValidateCheckInDate.bind(this),
      ]);
      this.tournamentForm.controls['checkInStartDate'].updateValueAndValidity();
    } else {
      this.tournamentForm.controls['checkInStartDate'].setValidators([]);
      this.tournamentForm.controls['checkInStartDate'].updateValueAndValidity();
    }
  }

  // confirm() {
  //   this.isConfirmed = true;
  // }

  navChangeHandler(event) {
    this.setContactDetail();
    if (event.activeId && event.activeId == 1) {
      if (!this.isStepValid('step1')) {
        event.preventDefault();
      }
      if (event.nextId == 3) {
        if (!this.isStepValid('step2')) {
          event.preventDefault();
        }
      }
    }

    if (event.activeId && event.activeId == 2) {
      if (event.nextId == 3) {
        if (!this.isStepValid('step2')) {
          event.preventDefault();
        }
      }
    }
  }

  setContactDetail() {
    if (
      this.tournamentForm.value.contactOn == 'WhatsApp' &&
      this.phoneNumberObject
    ) {
      let dialCode = this.phoneNumberObject.dialCode;
      let phone = this.phoneNumberObject.e164Number.replace(dialCode, '');
      this.tournamentForm.controls.contactDetails.setValue(
        this.phoneNumberObject.e164Number
      );
      this.tournamentForm.controls.whatsApp.setValue(phone);
    }
  }

  setContactDetailOnBlur() {
    if (
      this.tournamentForm.value.whatsApp &&
      this.tournamentForm.value.whatsApp.e164Number &&
      this.tournamentForm.value.whatsApp.number
    ) {
      this.phoneNumberObject = Object.assign(
        {},
        this.tournamentForm.value.whatsApp
      );
      let dialCode = this.tournamentForm.value.whatsApp.dialCode;
      let phone = this.tournamentForm.value.whatsApp.e164Number.replace(
        dialCode,
        ''
      );
      this.tournamentForm.controls.contactDetails.setValue(
        this.tournamentForm.value.whatsApp.e164Number
      );
      this.tournamentForm.controls.whatsApp.setValue(phone);
    }
  }

  // Date Time Validator
  ValidateStartTime(control: AbstractControl): { [key: string]: any } | null {
    return !control.value
      ? null
      : this.ValidateBothStartDateAndTime(control['_parent'], 'pastStartTime');
  }

  ValidateStartDate(control: AbstractControl): { [key: string]: any } | null {
    return !control.value
      ? null
      : this.ValidateBothStartDateAndTime(control['_parent'], 'pastStartDate');
  }

  ValidateEndTime(control: AbstractControl): { [key: string]: any } | null {
    return !control.value
      ? null
      : this.validateBothEndDateAndTime(control['_parent'], 'pastEndTime');
  }

  ValidateEndDate(control: AbstractControl): { [key: string]: any } | null {
    return !control.value
      ? null
      : this.validateBothEndDateAndTime(control['_parent'], 'pastEndDate');
  }

  //Registration date time validator
  ValidateRegStartTime(
    control: AbstractControl
  ): { [key: string]: any } | null {
    return !control.value
      ? null
      : this.validateRegStartDateAndTime(
          control['_parent'],
          'pastRegStartTime'
        );
  }

  ValidateRegStartDate(
    control: AbstractControl
  ): { [key: string]: any } | null {
    return !control.value
      ? null
      : this.validateRegStartDateAndTime(
          control['_parent'],
          'pastRegStartDate'
        );
  }

  ValidateRegEndTime(control: AbstractControl): { [key: string]: any } | null {
    return !control.value
      ? null
      : this.validateRegEndDateAndTime(control['_parent'], 'pastRegEndTime');
  }

  ValidateRegEndDate(control: AbstractControl): { [key: string]: any } | null {
    return !control.value
      ? null
      : this.validateRegEndDateAndTime(control['_parent'], 'pastRegEndDate');
  }

  validateBothEndDateAndTime(
    formGroup: FormGroup,
    ekey: string
  ): { [key: string]: boolean } {
    const { startDate, startTime, endDate, endTime } = formGroup.controls;
    if (
      (ekey == 'pastEndDate' && !endDate?.value) ||
      (ekey == 'pastEndTime' && !endTime?.value)
    ) {
      return { required: true };
    } else if (
      startDate?.value &&
      startTime?.value &&
      endDate?.value &&
      endTime?.value
    ) {
      const startDt = new Date(startDate.value);
      startDt.setHours(startTime.value?.hour);
      startDt.setMinutes(startTime.value?.minute);

      const endDt = new Date(endDate.value);
      endDt.setHours(endTime?.value?.hour);
      endDt.setMinutes(endTime?.value?.minute);
      if (endDt > startDt) {
        this.tournamentForm
          .get(ekey == 'pastEndDate' ? 'endTime' : 'endDate')
          .setErrors(null);
        return null;
      } else if (
        ekey == 'pastEndDate' &&
        endDt.toDateString() === startDt.toDateString()
      ) {
        return null;
      } else {
        return {
          [ekey]: true,
        };
      }
    }
    return null;
  }

  ValidateBothStartDateAndTime(
    formGroup: FormGroup,
    ekey: string
  ): { [key: string]: boolean } {
    const { startDate, startTime } = formGroup.controls;
    const time = startTime.value || {};
    const { hour, minute } = time;

    if (startTime.value && startDate.value) {
      const date = new Date(startDate.value);
      date.setHours(hour);
      date.setMinutes(minute);
      const diff = date.getTime() - Date.now();
      if (diff < 300000) {
        return { [ekey]: true };
      }
      const isCheckInValid = this.validateCheckedIn(formGroup);
      const endDateValidate = this.validateBothEndDateAndTime(
        formGroup,
        'pastEndDate'
      );
      const endTimeValidate = this.validateBothEndDateAndTime(
        formGroup,
        'pastEndTime'
      );
      if (this.tournamentForm.value?.isRegStartDate) {
        const regStartDateValidate = this.validateRegStartDateAndTime(
          formGroup,
          'pastRegStartDate'
        );
        const regStartTimeValidate = this.validateRegStartDateAndTime(
          formGroup,
          'pastRegStartTime'
        );
        this.tournamentForm
          ?.get('regStartDate')
          ?.setErrors(regStartDateValidate);
        this.tournamentForm
          ?.get('regStartTime')
          ?.setErrors(regStartTimeValidate);
      }

      if (this.tournamentForm.value.isRegEndDate) {
        const regEndTimeValidate = this.validateRegEndDateAndTime(
          formGroup,
          'pastRegEndTime'
        );
        const regEndDateValidate = this.validateRegEndDateAndTime(
          formGroup,
          'pastRegEndDate'
        );
        this.tournamentForm?.get('regEndTime')?.setErrors(regEndTimeValidate);
        this.tournamentForm?.get('regEndDate')?.setErrors(regEndDateValidate);
      }

      this.tournamentForm.get('endTime').setErrors(endTimeValidate);
      this.tournamentForm.get('endDate').setErrors(endDateValidate);
      this.tournamentForm.get('checkInStartDate')?.setErrors(isCheckInValid);
      this.tournamentForm.get('startTime').setErrors(null);
      this.tournamentForm.get('startDate').setErrors(null);
    }

    return null;
  }

  validateRegStartDateAndTime(
    formGroup: FormGroup,
    ekey: string
  ): { [key: string]: boolean } {
    const { regStartDate, regStartTime, startDate, startTime } =
      formGroup.controls;
    const time = startTime.value || {};
    const { hour, minute } = time;

    if (
      (ekey == 'pastRegStartDate' && !regStartDate?.value) ||
      (ekey == 'pastRegStartTime' && !regStartTime?.value)
    ) {
      return { required: true };
    } else if (
      startDate?.value &&
      startTime?.value &&
      regStartDate?.value &&
      regStartTime?.value
    ) {
      const currentDate = new Date();
      const startDt = new Date(startDate.value);
      startDt.setHours(hour);
      startDt.setMinutes(minute);
      const regstartDt = new Date(regStartDate.value);
      regstartDt.setHours(regStartTime?.value?.hour);
      regstartDt.setMinutes(regStartTime?.value?.minute);
      if (startDt > regstartDt && regstartDt > currentDate) {
        this.tournamentForm
          .get(ekey == 'pastRegStartTime' ? 'regStartDate' : 'regStartTime')
          .setErrors(null);
        return null;
      } else {
        return {
          [ekey]: true,
        };
      }
    }
    return null;
  }

  validateRegEndDateAndTime(
    formGroup: FormGroup,
    ekey: string
  ): { [key: string]: boolean } {
    const {
      regStartDate,
      regStartTime,
      regEndDate,
      regEndTime,
      startDate,
      startTime,
      noOfRound,
      setDuration,
      bracketType,
      isCheckInRequired,
      checkInStartDate,
      endDate,
      endTime,
      isRegStartDate,
    } = formGroup.controls;

    if (
      (ekey == 'pastRegEndDate' && !regEndDate?.value) ||
      (ekey == 'pastRegEndTime' && !regEndTime?.value)
    ) {
      return { required: true };
    } else if (
      regEndDate?.value &&
      regEndTime?.value &&
      startDate?.value &&
      startTime?.value &&
      endDate?.value &&
      endTime?.value
    ) {
      const regEndDt = new Date(regEndDate?.value);
      regEndDt.setHours(regEndTime?.value?.hour);
      regEndDt.setMinutes(regEndTime?.value?.minute);

      let regstartDt = new Date();

      if (isRegStartDate?.value) {
        regstartDt = new Date(regStartDate?.value);
        regstartDt.setHours(regStartTime?.value?.hour);
        regstartDt.setMinutes(regStartTime?.value?.minute);
      }

      let tournamentStartDt, tournamentStartDate;
      tournamentStartDt = new Date(startDate.value);
      tournamentStartDt.setHours(startTime?.value?.hour);
      tournamentStartDt.setMinutes(startTime?.value?.minute);
      if (bracketType?.value == 'swiss_safeis') {
        const tournamentDuration =
          setDuration?.value.days * 24 * 60 +
          setDuration?.value.hours * 60 +
          setDuration?.value.mins;
        const timeToBeAdded = (noOfRound?.value - 1) * tournamentDuration;
        tournamentStartDate = new Date(
          tournamentStartDt.getTime() + timeToBeAdded * 60000
        );
      } else if (bracketType?.value == 'ladder') {
        tournamentStartDt = new Date(endDate.value);
        tournamentStartDt.setHours(endTime?.value?.hour);
        tournamentStartDt.setMinutes(endTime?.value?.minute);
        tournamentStartDate = tournamentStartDt;
      } else {
        tournamentStartDate = tournamentStartDt;
      }
      if (isCheckInRequired?.value && checkInStartDate?.value) {
        const checkInStart = new Date(startDate.value);
        checkInStart.setHours(startTime.value.hour);
        checkInStart.setMinutes(startTime.value.minute);
        const checkInTime = this.checkInTimeOptions.find(
          (el) => el.name === checkInStartDate.value
        );
        checkInStart.setHours(
          checkInStart.getHours() - checkInTime?.value?.hour
        );
        checkInStart.setMinutes(
          checkInStart.getMinutes() - checkInTime?.value?.minute
        );
        tournamentStartDate = checkInStart;
      }
      if (regEndDt > regstartDt && regEndDt <= tournamentStartDate) {
        this.tournamentForm
          .get(ekey == 'pastRegEndDate' ? 'regEndTime' : 'regEndDate')
          .setErrors(null);
        return null;
      } else if (ekey == 'pastRegEndDate' && regstartDt == regEndDt) {
        return null;
      } else {
        const invalidField = this.getErrorKey(bracketType?.value, ekey);
        return invalidField;
      }
    }
    return null;
  }

  validateCheckedIn(formGroup: FormGroup): { [key: string]: boolean } {
    const { startDate, startTime, isCheckInRequired, checkInStartDate } =
      formGroup.controls;
    if (
      startDate?.value &&
      startTime?.value &&
      isCheckInRequired?.value &&
      checkInStartDate?.value
    ) {
      const checkInStart = new Date(startDate.value);
      checkInStart.setHours(startTime.value.hour);
      checkInStart.setMinutes(startTime.value.minute);
      const checkInTime = this.checkInTimeOptions.find(
        (el) => el.name === checkInStartDate.value
      );
      checkInStart.setHours(checkInStart.getHours() - checkInTime?.value?.hour);
      checkInStart.setMinutes(
        checkInStart.getMinutes() - checkInTime?.value?.minute
      );
      if (checkInStart.getTime() < Date.now()) {
        return { checkInExpired: true };
      }
    }
    return null;
  }

  setStartDate(f1, f2) {
    const date = this.tournamentForm.value[f1];
    const time = this.tournamentForm.value[f2];
    const dateTime = new Date(date);
    dateTime.setHours(time.hour);
    dateTime.setMinutes(time.minute);
    return dateTime.toISOString();
  }

  ValidateCheckInDate(control: AbstractControl): { [key: string]: any } | null {
    return control.value ? this.validateCheckedIn(control['_parent']) : null;
  }

  scrollToTop() {
    this.scroll.nativeElement.scrollIntoView();
  }

  isRegionDisable(cityName) {
    let isDisable = false;
    for (let obj of this.tournamentForm.value.venueAddress) {
      if (obj['region'] == cityName) {
        isDisable = true;
        break;
      }
    }
    return isDisable;
  }

  enablePrize() {
    if (this.tournamentForm.value.isPrize != true) {
      this.tournamentForm.controls.isPrize.setValue(true);
      this.prizeClickHandler(true);
    }
  }

  addPrize(i): void {
    const prizeList = this.tournamentForm.get('prizeList') as FormArray;
    let name = '';
    if (prizeList.controls.length < this.prizeLimit) {
      switch (true) {
        case prizeList.controls.length == 0:
          name = '1st';
          break;
        case prizeList.controls.length == 1:
          name = '2nd';
          break;
        case prizeList.controls.length == 2:
          name = '3rd';
          break;
        default:
          name = `${prizeList.controls.length + 1}th`;
          break;
      }
      const createPrizeForm = (): FormGroup => {
        const validator = [Validators.required, Validators.pattern('^[0-9]*$')];
        if (i == 0) {
          validator.push(Validators.min(1));
        }
        return this.fb.group({
          name: [name],
          value: ['', Validators.compose(validator)],
        });
      };
      prizeList.push(createPrizeForm());
    } else {
      this.eSportsToastService.showError(
        this.translateService.instant(
          this.prizeLimit == 6
            ? 'TOURNAMENT.MAX_6_ALLOWED'
            : 'TOURNAMENT.MAX_20_ALLOWED'
        )
      );
    }
  }

  removePrize(index): void {
    const prizeList = this.tournamentForm.get('prizeList') as FormArray;
    prizeList.removeAt(index);
  }

  /**
   *
   * @param field
   * @param fieldType 'STRING', 'BOOLEAN', 'NUMBER'
   * @param defaultValue
   * @param min
   * @param max
   */
  addNewControlAndUpdateValidity = (
    field,
    fieldType,
    defaultValue,
    min?,
    max?,
    pattern?,
    customValidation?
  ) => {
    const formControl = this.tournamentForm.get(field);
    const validation = [];

    switch (fieldType) {
      case 'NUMBER':
        validation.push(Validators.required);
        if (typeof min == 'number') validation.push(Validators.min(min));
        if (typeof max == 'number') validation.push(Validators.max(max));
        if (pattern) validation.push(pattern);
        if (customValidation) validation.push(customValidation);
        break;
      case 'BOOLEAN':
        validation.push(Validators.required);
        break;
      case 'STRING':
        validation.push(Validators.required);
        break;
    }

    if (formControl) {
      formControl.setValue(defaultValue);
      formControl.setValidators(validation);
      formControl.updateValueAndValidity();
    } else {
      this.tournamentForm.addControl(
        field,
        new FormControl(defaultValue, Validators.compose(validation))
      );
    }
  };

  removeControlOrValidity = (field) => {
    const formControl = this.tournamentForm.get(field);
    if (formControl) {
      this.tournamentForm.removeControl(field);
    } else {
    }
  };

  bracketChangeHandler(
    type,
    noOfPlacement?,
    maxPlacement = 2,
    maxParticipants?,
    noOfRound?,
    noOfLoss?
  ) {
    const enableCheckInFunctionality = [
      'single',
      'double',
      'round_robin',
      'battle_royale',
    ].includes(type);
    this.participantCheckIn(enableCheckInFunctionality);

    let maximumParticipants = 16384;

    if (['round_robin', 'battle_royale'].includes(type)) {
      type == 'battle_royale'
        ? this.removeControlOrValidity('noOfSet')
        : this.addNewControlAndUpdateValidity('noOfSet', 'STRING', '');
      type == 'battle_royale'
        ? this.addNewControlAndUpdateValidity(
            'noOfPlacement',
            'NUMBER',
            2,
            2,
            this.tournamentForm?.value?.noOfTeamInGroup || maxPlacement || 2
          )
        : this.removeControlOrValidity('noOfPlacement');
      type == 'battle_royale'
        ? this.placementCounter(noOfPlacement || 2)
        : this.removeControlOrValidity('placementPoints');
      type == 'battle_royale'
        ? this.addNewControlAndUpdateValidity(
            'isKillPointRequired',
            'BOOLEAN',
            false
          )
        : this.removeControlOrValidity('isKillPointRequired');
      type == 'battle_royale'
        ? this.removeControlOrValidity('stageBracketType')
        : this.addNewControlAndUpdateValidity('stageBracketType', 'STRING', '');
      type == 'battle_royale'
        ? this.removeControlOrValidity('allowAdvanceStage')
        : this.addNewControlAndUpdateValidity(
            'allowAdvanceStage',
            'BOOLEAN',
            false
          );

      this.multiStageTournamentHandler(true, type, maxParticipants);
    } else {
      this.multiStageTournamentHandler(false);
      this.addNewControlAndUpdateValidity('noOfSet', 'STRING', '');
      this.removeControlOrValidity('noOfPlacement');
      this.removeControlOrValidity('placementPoints');
      this.removeControlOrValidity('isKillPointRequired');
      this.removeControlOrValidity('stageBracketType');
      this.addNewControlAndUpdateValidity(
        'allowAdvanceStage',
        'BOOLEAN',
        false
      );
    }

    switch (type) {
      case 'single':
        maximumParticipants = 16384;
        break;
      case 'double':
        maximumParticipants = 16384;
        break;
      case 'round_robin':
        maximumParticipants = 128;
        break;
      case 'battle_royale':
        maximumParticipants = 16384;
        break;
    }

    if (['swiss_safeis', 'ladder'].includes(type)) {
      if (type == 'swiss_safeis') {
        this.addNewControlAndUpdateValidity(
          'noOfRound',
          'NUMBER',
          noOfRound || 1,
          1
        );
        this.addNewControlAndUpdateValidity(
          'noOfLoss',
          'NUMBER',
          noOfLoss || 0,
          0,
          (noOfRound || 1) - 1
        );
        this.tournamentForm.addControl(
          'setDuration',
          this.fb.group(
            {
              days: [1, Validators.required],
              hours: [0, Validators.required],
              mins: [0, Validators.required],
            },
            { validator: this.durationValidator }
          )
        );
      }
      this.addNewControlAndUpdateValidity('enableTiebreaker', 'BOOLEAN', false);
      this.tournamentForm.get('isParticipantsLimit').setValue(false);
      this.removeControlOrValidity('maxParticipants');
      this.prizeLimit = 20;
    } else {
      this.removeControlOrValidity('noOfRound');
      this.removeControlOrValidity('noOfLoss');
      this.removeControlOrValidity('setDuration');
      this.removeControlOrValidity('enableTiebreaker');

      this.tournamentForm.get('isParticipantsLimit').setValue(true);
      this.addNewControlAndUpdateValidity(
        'maxParticipants',
        'NUMBER',
        2,
        2,
        maximumParticipants,
        false,
        this.teamInGroupValidation('maxParticipants').bind(this)
      );
      this.prizeLimit = 6;
    }

    if (type == 'single') {
      this.addNewControlAndUpdateValidity(
        'isAllowThirdPlaceMatch',
        'BOOLEAN',
        false
      );
    } else {
      this.removeControlOrValidity('isAllowThirdPlaceMatch');
    }
  }

  maxParticipantHandler(value, bracketType) {
    if (['battle_royale', 'round_robin'].includes(bracketType)) {
      const maxValidator =
        bracketType == 'round_robin'
          ? [Validators.max(value >= 24 ? 24 : value)]
          : [Validators.max(value)];
      this.tournamentForm
        .get('noOfTeamInGroup')
        .setValidators([
          Validators.required,
          Validators.min(2),
          ...maxValidator,
          this.teamInGroupValidation('noOfTeamInGroup').bind(this),
        ]);
      this.tournamentForm
        .get('noOfWinningTeamInGroup')
        .setValidators([
          Validators.required,
          Validators.min(1),
          this.teamInGroupValidation('noOfWinningTeamInGroup').bind(this),
        ]);
      this.tournamentForm.get('noOfTeamInGroup').updateValueAndValidity();
      this.tournamentForm
        .get('noOfWinningTeamInGroup')
        .updateValueAndValidity();
    }
  }

  noOfTeamsPerGroupHandler(value, bracketType) {
    if (bracketType === 'battle_royale') {
      this.tournamentForm
        .get('noOfPlacement')
        .setValidators([
          Validators.required,
          Validators.min(2),
          Validators.max(value || 2),
        ]);
      this.tournamentForm.get('noOfPlacement').updateValueAndValidity();
    }
  }

  placementCounter = async (value) => {
    this.tournamentForm.addControl('placementPoints', this.fb.array([]));
    const placementPointsList = this.tournamentForm.get(
      'placementPoints'
    ) as FormArray;

    const createPlacementForm = (position): FormGroup => {
      return this.fb.group({
        position: [position],
        value: [
          '',
          Validators.compose([
            Validators.required,
            Validators.pattern('^[0-9]*$'),
          ]),
        ],
      });
    };

    // remove placement node
    const deletePlacementForm = async (count, value) => {
      placementPointsList.removeAt(count);
      return count <= value ? true : await deletePlacementForm(--count, value);
    };

    // add placement node
    const addPlacementForm = async (count, value) => {
      placementPointsList.push(createPlacementForm(count));
      return count >= value ? true : await addPlacementForm(++count, value);
    };

    let count = placementPointsList.length;

    if (count > value) {
      await deletePlacementForm(count - 1, value);
    }

    if (count < value) {
      await addPlacementForm(count + 1, value);
    }
  };

  setTournamentValue(data) {
    this.selected.phoneNumber = data?.contactDetails;
    this.substituteMemberLimit = data?.substituteMemberSize || 1;
    // Get Date time for start date and end date of the tournament
    const getDateTime = (f1, f2, date) => {
      const d = new Date(date);
      const formatToTwoDigit = (number) =>
        number < 10 ? `0${number}` : number;

      return {
        [f1]: `${d.getFullYear()}-${formatToTwoDigit(
          d.getMonth() + 1
        )}-${formatToTwoDigit(d.getDate())}`,
        [f2]: {
          hour: d.getHours(),
          minute: d.getMinutes(),
          second: 0,
        },
      };
    };

    // get check-in time for tournament
    const setStartCheckedInDate = (startDate, checkInStartDate) => {
      const d1 = new Date(startDate).getTime();
      const d2 = new Date(checkInStartDate).getTime();
      const hour = Math.floor((d1 - d2) / (3600 * 1000));
      const minute = Math.floor((d1 - d2) / (60 * 1000)) - hour * 60;
      const value = this.checkInTimeOptions.find((el) => {
        return el.value.minute == minute && el.value.hour == hour;
      });

      return value.name;
    };

    const getTeamFormatValues = (data) => {
      const n = data.teamSize.toString();
      if (['2', '4'].includes(n)) {
        this.enableteamCounter = false;
        return {
          teamFormat: n,
          teamSize: n,
        };
      } else {
        this.enableteamCounter = true;
        return {
          teamFormat: 'n',
          teamSize: n,
        };
      }
    };

    // get and set gameIndex
    const gameIndex = this.gameArray.findIndex(
      (item) => item?._id === (data.gameDetail?._id || data?.gameDetail)
    );
    this.setGameOnSlide(
      gameIndex > -1 ? gameIndex : Math.floor(this.gameArray.length / 2),
      false
    );

    this.participantTypeHandler(data.participantType);
    this.allowSubstituteMemberHandler(data?.allowSubstituteMember);
    this.bracketChangeHandler(
      data?.bracketType,
      data?.noOfPlacement,
      data?.noOfTeamInGroup,
      data?.maxParticipants,
      data?.noOfRound,
      data?.noOfLoss
    );

    this.prizeClickHandler(data?.isPrize, data?.prizeList?.length || 0);

    if (data.isIncludeSponsor) {
      this.clearFormArray('sponsors');
      const sponserEvent = { target: { checked: data.isIncludeSponsor } };
      this.sponsorClickHandler(sponserEvent, data.sponsors);
    }

    this.venueClickHandler(data?.tournamentType, data?.venueAddress?.length);

    this.killingPointHandler(data.isKillPointRequired);

    if (data.contactOn == 'WhatsApp') {
      data.contactDetails = +data.contactDetails;
    }
    this.setContactOn(data.contactOn || 'None');
    if (data.countryCode) {
      this.countryCode = data.countryCode;
    }

    if (data.bracketType == 'battle_royale') {
      this.placementCounter(data.noOfPlacement);
    }

    if (data.regStartDate) {
      this.tournamentForm.controls.isRegStartDate.setValue(true);
    }

    data.url = data.url.split('/').reverse()[0];
    this.tournamentForm.addControl('_id', new FormControl(data?._id));

    this.regStartDateHandler(data.isRegStartDate);
    this.regEndDateHandler(data.isRegEndDate);

    const isSingleElimination = [
      data?.bracketType,
      data?.stageBracketType,
    ].includes('single');
    if (isSingleElimination) {
      this.addNewControlAndUpdateValidity(
        'isAllowThirdPlaceMatch',
        'BOOLEAN',
        false
      );
    }
    this.tournamentForm.patchValue({
      ...data,
      ...(data.startDate &&
        getDateTime('startDate', 'startTime', data.startDate)),
      ...(data.endDate && getDateTime('endDate', 'endTime', data.endDate)),
      ...(data.isRegStartDate &&
        getDateTime('regStartDate', 'regStartTime', data.regStartDate)),
      ...(data.isRegEndDate &&
        getDateTime('regEndDate', 'regEndTime', data.regEndDate)),
      ...(data.isCheckInRequired && {
        checkInStartDate: setStartCheckedInDate(
          data.startDate,
          data.checkInStartDate
        ),
      }),
      ...(data?.participantType == 'team' && getTeamFormatValues(data)),
      gameDetail: data.gameDetail?._id || data?.gameDetail,
    });

    if (data?.enablePayment) {
      this.active = 4;
      this.tournamentForm.get('step').setValue(5);
    }
    this.showLoader = false;
  }

  fetchTournamentDetails = async (slug) => {
    try {
      this.showLoader = true;
      const { data } = await this.tournamentService
        .getTournamentBySlug(slug)
        .toPromise();
      const game = await this.tournamentService.getGames().toPromise();
      const t_id = data?._id;
      this.fetchRegisteredParticipant(t_id);
      if (Array.isArray(game?.data)) {
        if (this.isArabic) {
          this.gameArray = game.data.reverse();
        } else {
          this.gameArray = game.data;
        }
      }
      const isEventOrganizer =
        this?.user?.isEO == 0 && this.user?._id === data?.organizerDetail?._id;
      const hasAdminAccess =
        this.user?.accountType == 'admin' &&
        this.user?.accessLevel?.includes('em');

      this.hasRequiredAccess = isEventOrganizer || hasAdminAccess;
      if (!this.hasRequiredAccess) {
        throw new Error(
          this.translateService.instant('VIEW_TOURNAMENT.ACCESS_DENIED')
        );
      }

      if (!data) {
        throw new Error(this.translateService.instant('VIEW_TOURNAMENT.ERROR'));
      } else {
        this.userFullName = data.organizerDetail.fullName;
        this.setTournamentValue(data);
        this.isCharged = data?.isCharged;
      }
    } catch (error) {
      this.showLoader = false;
      this.eSportsToastService.showError(
        error?.error?.message || error?.message
      );
    }
  };

  fetchRegisteredParticipant = async (tournamentId) => {
    try {
      const encodeUrl = `?query=${encodeURIComponent(
        JSON.stringify({ tournamentId })
      )}&select=''&option=${encodeURIComponent(
        JSON.stringify({ count: true })
      )}`;
      const response = await this.tournamentService
        .getParticipants(encodeUrl)
        .toPromise();
      this.isParticipantRegistered = !!response?.totals?.count;
    } catch (error) {
      this.eSportsToastService.showError(
        error?.error?.message || error?.message
      );
    }
  };

  clearFormArray = (field): void => {
    const array = this.tournamentForm.get(field) as FormArray;
    array.clear();
  };

  multiStageTournamentHandler = (
    isEnableFields: boolean,
    bracketType?,
    maxParticipants?
  ): void => {
    if (isEnableFields) {
      const noOfRound = bracketType == 'round_robin' ? 2 : 10;
      this.addNewControlAndUpdateValidity(
        'noOfTeamInGroup',
        'NUMBER',
        2,
        2,
        maxParticipants || this.tournamentForm.value?.maxParticipants || 2,
        false,
        this.teamInGroupValidation('noOfTeamInGroup').bind(this)
      );
      this.addNewControlAndUpdateValidity(
        'noOfWinningTeamInGroup',
        'NUMBER',
        1,
        1,
        false,
        false,
        this.teamInGroupValidation('noOfWinningTeamInGroup').bind(this)
      );
      this.addNewControlAndUpdateValidity(
        'noOfRoundPerGroup',
        'NUMBER',
        1,
        1,
        noOfRound
      );
      this.addNewControlAndUpdateValidity('noOfStage', 'NUMBER', 1);
    } else {
      this.removeControlOrValidity('noOfTeamInGroup');
      this.removeControlOrValidity('noOfWinningTeamInGroup');
      this.removeControlOrValidity('noOfRoundPerGroup');
      this.removeControlOrValidity('noOfStage');
    }
  };

  teamInGroupValidation = (field?: string): ValidatorFn => {
    return (control: AbstractControl): { [key: string]: any } | null => {
      if (control['_parent']) {
        const {
          noOfTeamInGroup,
          noOfWinningTeamInGroup,
          maxParticipants,
          noOfStage,
          bracketType
        } = control?.['_parent']?.controls;

        if (
          maxParticipants?.valid &&
          noOfTeamInGroup?.value &&
          !noOfTeamInGroup?.errors?.max &&
          noOfWinningTeamInGroup?.value &&
          maxParticipants?.value &&
          noOfStage
        ) {
          const isPowerOfTwo = (n) => {
            if (n == 0) return false;
            return (
              parseInt(Math.ceil(Math.log(n) / Math.log(2)).toString()) ==
              parseInt(Math.floor(Math.log(n) / Math.log(2)).toString())
            );
          };
          const fieldValidation = (
            maxParticipants,
            noOfTeamInGroup,
            noOfWinningTeam
          ) => {
            if (maxParticipants < noOfTeamInGroup) {
              return { max: { max: maxParticipants } };
            } else if (noOfTeamInGroup > noOfWinningTeam) {
              if (bracketType.value == "round_robin") {
                const totalWinners = (maxParticipants / noOfTeamInGroup) * noOfWinningTeam;
                const pow2 = isPowerOfTwo(totalWinners);
                return pow2 ? null : { multiStageConfigError: true };
              }

              const check = noOfTeamInGroup % noOfWinningTeam != 0;
              return check ? { multiStageConfigError: true } : null;
            } else {
              return { multiStageConfigError: true };
            }
          };
          const errorConfig = fieldValidation(
            maxParticipants?.value,
            noOfTeamInGroup?.value,
            noOfWinningTeamInGroup?.value
          );

          if (!errorConfig) {
            const totalStage = (
              totalPlayer,
              noOfPlayerPerGroup,
              noOfWinning,
              noOfStage,
              previousStagePlayer
            ) => {
              let nextStagePlayer;
              const createStage = () => {
                nextStagePlayer =
                  Math.ceil(totalPlayer / noOfPlayerPerGroup) * noOfWinning;
                if (nextStagePlayer < previousStagePlayer) {
                  // noOfPlayerPerGroup = noOfPlayerPerGroup + 1;
                  nextStagePlayer =
                    Math.ceil(totalPlayer / noOfPlayerPerGroup) * noOfWinning;
                } else {
                  createStage();
                }
              };

              if (totalPlayer !== noOfPlayerPerGroup) {
                createStage();
              }
              return totalPlayer == noOfPlayerPerGroup
                ? noOfStage
                : !(Math.ceil(nextStagePlayer / noOfPlayerPerGroup) > 1)
                ? noOfStage + 1
                : Math.ceil(nextStagePlayer / noOfPlayerPerGroup) == 2
                ? noOfStage + 2
                : totalStage(
                    nextStagePlayer,
                    noOfPlayerPerGroup,
                    noOfWinning,
                    noOfStage + 1,
                    previousStagePlayer
                  );
            };

            const noOfStage = totalStage(
              maxParticipants?.value,
              noOfTeamInGroup?.value,
              noOfWinningTeamInGroup?.value,
              1,
              maxParticipants?.value
            );
            this.tournamentForm.get('noOfStage').setValue(noOfStage);
          }

          if (field == 'noOfTeamInGroup') {
            return errorConfig;
          } else {
            this.tournamentForm.get('noOfTeamInGroup').setErrors(errorConfig);
            return null;
          }
        }
      } else {
        return null;
      }
    };
  };

  killingPointHandler = (isAllowKillingPoint) => {
    isAllowKillingPoint
      ? this.tournamentForm.addControl(
          'pointsKills',
          new FormControl(1, [Validators.required, Validators.min(1)])
        )
      : this.tournamentForm.removeControl('pointsKills');
    this.tournamentForm.updateValueAndValidity();
  };

  teamFormatHandler = (event, field) => {
    this.enableteamCounter = event == 'n';
    if (!this.enableteamCounter) {
      this.tournamentForm.get(field).setValue(event);
    } else {
      this.tournamentForm.get(field).setValue(2);
    }
  };

  allowAdvanceStageHandler = (allowAdvanceStage) => {
    if (allowAdvanceStage) {
      this.tournamentForm
        .get('stageMatch')
        .setValidators([Validators.required]);
      this.tournamentForm
        .get('stageMatchNoOfSet')
        .setValidators([Validators.required]);
    } else {
      this.tournamentForm.get('stageMatch').setValue('');
      this.tournamentForm.get('stageMatchNoOfSet').setValue('');
      this.tournamentForm.get('stageMatch').clearValidators();
      this.tournamentForm.get('stageMatchNoOfSet').clearValidators();
    }
    this.tournamentForm.get('stageMatch').updateValueAndValidity();
    this.tournamentForm.get('stageMatchNoOfSet').updateValueAndValidity();
  };

  allowSubstituteMemberHandler = (check) => {
    //const value = this.tournamentForm.value?.substituteMemberSize;

    if (check) {
      this.tournamentForm.controls['substituteMemberSize'].setValidators([
        Validators.required,
        Validators.max(10),
        Validators.min(this.substituteMemberLimit),
      ]);
      //this.tournamentForm.controls['substituteMemberSize'].setValue(value || 1);
    } else {
      const substituteMemberSize =
        this.tournamentForm.value.substituteMemberSize;
      this.tournamentForm.controls['substituteMemberSize'].clearValidators();
      this.tournamentForm.controls['substituteMemberSize'].setValue(
        substituteMemberSize || 0
      );
    }
    this.tournamentForm.get('substituteMemberSize').updateValueAndValidity();
  };

  durationValidator = (
    control: AbstractControl
  ): { [key: string]: boolean } => {
    const { days, hours, mins } = control.value;
    const regEndTimeValidate = this.validateRegEndDateAndTime(
      this.tournamentForm,
      'pastRegEndTime'
    );
    this.tournamentForm.get('regEndTime')?.setErrors(regEndTimeValidate);
    if (days || hours || mins) return null;

    return {
      required: true,
    };
  };

  roundHandler(noOfRound) {
    if (noOfRound) {
      const regEndTimeValidate = this.validateRegEndDateAndTime(
        this.tournamentForm,
        'pastRegEndTime'
      );
      this.tournamentForm.get('regEndTime').setErrors(regEndTimeValidate);
      this.addNewControlAndUpdateValidity(
        'noOfLoss',
        'NUMBER',
        0,
        0,
        noOfRound - 1
      );
    }
  }

  formValidation() {
    return (formGroup: FormGroup) => {
      const {
        startDate,
        startTime,
        endDate,
        endTime,
        setDuration,
        bracketType,
        noOfRound,
      } = formGroup.controls;
      if (
        startDate?.value &&
        startTime?.value &&
        endDate?.value &&
        endTime?.value &&
        setDuration?.value &&
        bracketType?.value &&
        noOfRound?.value
      ) {
        if (bracketType?.value == 'swiss_safeis') {
          const dt1 = new Date(startDate?.value);
          dt1.setHours(
            startTime?.value?.hour || 0,
            startTime?.value?.minute || 0
          );

          const dt2 = new Date(endDate?.value);
          dt2.setHours(endTime?.value?.hour || 0, endTime?.value?.minute || 0);

          const diff = dt2.getTime() - dt1.getTime();
          const convert_ms_to_min = 1000 * 60;
          const convert_days_to_min = 24 * 60;
          const total_tournament_minutes = diff / convert_ms_to_min;
          const swiss_total_minutes =
            noOfRound?.value *
            (setDuration?.value?.days * convert_days_to_min +
              setDuration?.value?.hours * 60 +
              setDuration?.value?.mins);
          const { days, hours, mins } = setDuration.value;
          if (total_tournament_minutes < swiss_total_minutes) {
            setDuration.setErrors({ invalidDuration: true });
          } else if (!(days || hours || mins)) {
            setDuration.setErrors({ required: true });
          } else {
            setDuration.setErrors(null);
          }

          setDuration.markAsTouched();
        }
      }
    };
  }

  getErrorKey = (
    brackeyType: string,
    key: string
  ): { [key: string]: boolean } => {
    const hasRegistrationEndDtKey =
      key == 'pastRegEndTime' || key == 'pastRegEndDate';
    if (brackeyType == 'swiss_safeis' && hasRegistrationEndDtKey) {
      const k =
        key == 'pastRegEndDate' ? 'swissPastRegEndDate' : 'swissPastRegEndTime';
      return {
        [k]: true,
      };
    } else if (brackeyType == 'ladder' && hasRegistrationEndDtKey) {
      const k =
        key == 'pastRegEndDate'
          ? 'ladderPastRegEndDate'
          : 'ladderPastRegEndTime';
      return {
        [k]: true,
      };
    } else {
      return { [key]: true };
    }
  };

  participantCheckIn(showCheckInFunctionality: boolean) {
    if (showCheckInFunctionality) {
      const isCheckInRequiredValidators = this.fb.control(
        false,
        Validators.compose([])
      );
      this.tournamentForm.addControl(
        'isCheckInRequired',
        isCheckInRequiredValidators
      );

      const checkInStartDateValidators = this.fb.control(
        '',
        Validators.compose([])
      );
      this.tournamentForm.addControl(
        'checkInStartDate',
        checkInStartDateValidators
      );
    } else {
      this.tournamentForm.removeControl('isCheckInRequired');
      this.tournamentForm.removeControl('checkInStartDate');
    }
  }

  setGTMTagsOnNextClick(step) {
    if (this.editTournament && step == 'step1') {
      this.pushGTMTags('Basic_Information_Complete');
    }
    if (this.editTournament && step == 'step2') {
      this.pushGTMTags('Conditions_Complete');
    }
  }

  pushGTMTags(eventName: string) {
    let superProperties: SuperProperties = {};
    if (this.user) {
      superProperties = this.gtmService.assignLoggedInUsedData(this.user);
    }
    let eventProperties: EventProperties = {};

    if (
      [
        'Basic_Information_Complete',
        'Conditions_Complete',
        'Publish_Complete',
      ].includes(eventName)
    ) {
      eventProperties.fromPage = this.route.url;
    }

    this.gtmService.gtmEventWithSuperProp({
      eventName: eventName,
      superProps: superProperties,
      eventProps: eventProperties,
    });
  }

  setCurrentDate() {
    const getDateTime = (f1, f2, date) => {
      const d = new Date(date);
      const formatToTwoDigit = (number) =>
        number < 10 ? `0${number}` : number;

      return {
        [f1]: `${d.getFullYear()}-${formatToTwoDigit(
          d.getMonth() + 1
        )}-${formatToTwoDigit(d.getDate())}`,
        [f2]: {
          hour: d.getHours(),
          minute: d.getMinutes(),
          second: 0,
        },
      };
    };

    this.tournamentForm.patchValue({
      ...getDateTime('regStartDate', 'regStartTime', new Date()),
    });
  }

  gtmEventBasicStepCreate() {
    if (this.editTournament) {
      return;
    }

    let superProperties: SuperProperties = {};
    if (this.user) {
      superProperties = this.gtmService.assignLoggedInUsedData(this.user);
    }

    let eventProperties: EventProperties = {};
    let eventName = '';
    switch (this.active) {
      case 1:
        eventName = `Basic_Information_Complete`;
        break;
      case 2:
        eventName = `Conditions_Complete `;
        break;
    }

    this.gtmService.gtmEventWithSuperProp({
      eventName: eventName,
      superProps: superProperties,
      eventProps: eventProperties,
    });
  }

  gtmEventBasicStepCreated() {
    if (this.editTournament) {
      return;
    }

    let superProperties: SuperProperties = {};
    if (this.user) {
      superProperties = this.gtmService.assignLoggedInUsedData(this.user);
    }

    let eventProperties: EventProperties = {};
    let eventName = `Publish_Complete `;
    const gamePlatoform = this.gameArray[this.selectedGameIndex]?.platform.map(
      (game) => {
        return game.name;
      }
    );
    eventProperties = {
      title: this.gameArray[this.selectedGameIndex].name,
      platform:
        gamePlatoform && gamePlatoform.length > 0
          ? gamePlatoform.join(',')
          : '',
    };

    this.gtmService.gtmEventWithSuperProp({
      eventName: eventName,
      superProps: superProperties,
      eventProps: eventProperties,
    });
  }

  handleSearchInput() {
    this.inputSubscription = this.searchInput.valueChanges
      .pipe(debounceTime(300))
      .subscribe((value) => {
        if (value.trim() != '' && value.trim().length > 2) {
          this.customLoader = true;
          this.resetPagination();
          this.searchSubscription?.unsubscribe();
          this.searchSubscription = this.userService
            .searchUserWithPagination(API, value, this.pagination)
            .subscribe(
              (results) => {
                this.searchResults = results['data']['docs'];
                this.pagination.hasNextPage = results['data']['hasNextPage'];
                this.customLoader = false;
              },
              (err) => {
                this.customLoader = false;
                this.eSportsToastService.showError(
                  this.translateService.instant('TOURNAMENT.SEARCH_FAILED')
                );
              }
            );
        } else {
          this.resetSearch();
        }
      });
  }

  resetSearch() {
    this.searchSubscription?.unsubscribe();
    this.customLoader = false;
    this.searchResults = [];
    this.resetPagination();
  }

  searchPlayers() {
    if (this.pagination.hasNextPage) {
      this.pagination.page++;
      this.customLoader = true;
      this.searchSubscription?.unsubscribe();
      this.searchSubscription = this.userService
        .searchUserWithPagination(API, this.searchInput.value, this.pagination)
        .subscribe(
          (results) => {
            this.searchResults = this.searchResults.concat(
              ...results['data']['docs']
            );
            this.pagination.hasNextPage = results['data']['hasNextPage'];
            this.customLoader = false;
          },
          (err) => {
            this.customLoader = false;
            this.eSportsToastService.showError(
              this.translateService.instant('TOURNAMENT.SEARCH_FAILED')
            );
          }
        );
    }
  }

  addPlayer(player) {
    if (player && typeof player == 'object') {
      if (
        !this.isParticipantAlreadyAdded(
          this.tournamentForm.value.participants,
          player?._id
        )
      ) {
        this.tournamentForm.value.participants.push(player);
      }
    }
    this.searchInput.setValue('');
    this.resetSearch();
  }

  resetPagination() {
    this.pagination.page = 1;
    this.pagination.hasNextPage = true;
  }
}
