import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowUserReportComponent } from './show-user-report.component';

describe('ShowUserReportComponent', () => {
  let component: ShowUserReportComponent;
  let fixture: ComponentFixture<ShowUserReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ShowUserReportComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowUserReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
