import { Component, Input, OnInit } from '@angular/core';
import { EsportsToastService, EsportsAdminService } from "esports";
import { environment } from "../../../../../environments/environment";
const API = environment.apiEndPoint;
@Component({
  selector: 'app-winners',
  templateUrl: './winners.component.html',
  styleUrls: ['./winners.component.scss'],
})
export class WinnersComponent implements OnInit {
  @Input() tournamentId: any;

  sportDetail = [];

  constructor(
    private adminService: EsportsAdminService,
    private eSportsToastService: EsportsToastService
  ) {}

  ngOnInit(): void {}

  ngOnChanges() {
    if (this.tournamentId) {
      this.getWinnersList();
    }
  }

  getWinnersList = async () => {
    try {
      const query = {
        filter: {
          tournament: this.tournamentId,
          isAward: true,
        awardPosition: 3,
        },
        option: {
          sort: { awardPosition: 1 },
        },
      };

      const { data }: any = await this.adminService
        .getTournamentWinners(API, query)
        .toPromise();
      if (data.length > 0) {
        this.sportDetail = data;
      }
    } catch (error) {
      this.eSportsToastService.showError(error?.error?.message || error?.message);
    }
  };
}
