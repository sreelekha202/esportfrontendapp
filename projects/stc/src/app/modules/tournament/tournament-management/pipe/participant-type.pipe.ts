import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'participantType',
})
export class ParticipantTypePipe implements PipeTransform {
  transform(value, size) {
    switch (value) {
      case 'individual':
        return size <= 1
          ? 'PARTICIPANT_EXPORT.SINGLE'
          : 'PARTICIPANT_EXPORT.ALL';
      case 'team':
        return size <= 1
          ? 'PARTICIPANT_EXPORT.TEAM'
          : 'PARTICIPANT_EXPORT.TEAMS';
      default:
        return 'N/A';
    }
  }
}
