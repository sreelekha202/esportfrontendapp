import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { EsportsNotificationsService, EsportsToastService } from 'esports';
import { finalize } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';
import { environment } from '../../../../../../environments/environment';
const API = environment.apiEndPoint;
@Component({
  selector: 'app-email',
  templateUrl: './email.component.html',
  styleUrls: ['./email.component.scss','../notification.component.scss'],
})
export class EmailComponent implements OnInit {
  @Input() tournamentId: any;
  participantTypeArray = [
    {
      text: this.translateService.instant(
        'ADMIN.USER_NOTIFICATION.LABEL.APPROVED'
      ),
      value: 'approved',
    },
    {
      text: this.translateService.instant(
        'ADMIN.USER_NOTIFICATION.LABEL.PENDING'
      ),
      value: 'pending',
    },
    {
      text: this.translateService.instant(
        'ADMIN.USER_NOTIFICATION.LABEL.REJECTED'
      ),
      value: 'rejected',
    },
  ];
  isLoading = false;
  selectedParticipantType;
  editorConfig = {};
  emailForm = this.fb.group({
    subject: ['', Validators.required],
    headerText: ['', Validators.required],
    content: ['', Validators.required],
    type: ['', Validators.required],
  });
  constructor(
    private fb: FormBuilder,
    private userNotificationsService: EsportsNotificationsService,
    private eSportsToastService: EsportsToastService,
    private translateService:TranslateService
  ) {}

  ngOnInit(): void {
    this.editorConfig = {
      editable: true,
      spellcheck: true,
      height: '100px',
      width: 'auto',
      minWidth: '0',
      translate: 'yes',
      enableToolbar: true,
      showToolbar: true,
      defaultParagraphSeparator: '',
      defaultFontName: '',
      defaultFontSize: '',
      fonts: [
        { class: 'arial', name: 'Arial' },
        { class: 'times-new-roman', name: 'Times New Roman' },
        { class: 'calibri', name: 'Calibri' },
        { class: 'comic-sans-ms', name: 'Comic Sans MS' },
      ],
      customClasses: [
        {
          name: 'quote',
          class: 'quote',
        },
        {
          name: 'redText',
          class: 'redText',
        },
        {
          name: 'titleText',
          class: 'titleText',
          tag: 'h1',
        },
      ],
      sanitize: true,
      toolbarPosition: 'top',
      toolbarHiddenButtons: [['bold', 'italic'], ['fontSize']],
    };
  }

  selectParticipantType(item) {
    this.selectedParticipantType = item;
    this.emailForm.controls['type'].setValue(item.value);
  }

  public hasError = (controlName: string, errorName: string) => {
    return this.emailForm.controls[controlName].hasError(errorName);
  };

  sendEmail(form) {
    if (form.invalid) {
      return;
    }
    this.isLoading = true;
    const inputData = {
      title: this.emailForm.value.subject,
      message: this.emailForm.value.content,
      userSegment: 'TOURNAMENT_PLAYERS_BY_ID',
      headerText: this.emailForm.value.headerText,
      tournamentId: this.tournamentId,
      participantType: this.emailForm.value.type,
    };
    this.userNotificationsService
      .sendEmails(API, inputData)
      .pipe(
        finalize(() => {
          this.isLoading = false;
        })
      )
      .subscribe(
        (res: any) => {
          if (res) {
            this.eSportsToastService.showSuccess(res?.message);
          }
        },
        (err: any) => {
          this.eSportsToastService.showError(
            err?.error?.message || err?.message
          );
        }
      );
  }
}
