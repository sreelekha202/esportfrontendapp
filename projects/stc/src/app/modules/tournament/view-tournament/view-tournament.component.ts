import { Title } from '@angular/platform-browser';
import { TranslateService } from '@ngx-translate/core';
import { MatDialog } from '@angular/material/dialog';
import {
  Component,
  OnInit,
  Inject,
  OnDestroy,
  ViewChild,
  EventEmitter,
  PLATFORM_ID,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import 'rxjs/add/operator/filter';

import {
  InfoPopupComponent,
  InfoPopupComponentData,
  InfoPopupComponentType,
} from '../../../shared/popups/info-popup/info-popup.component';
import {
  RatingService,
  TournamentService,
  UtilsService,
  DeeplinkService,
  HomeService,
} from '../../../core/service';
import { DOCUMENT, isPlatformBrowser } from '@angular/common';
import { Location } from '@angular/common';
import {
  IUser,
  EsportsUserService,
  EsportsLanguageService,
  EsportsConstantsService,
  GlobalUtils,
  EsportsToastService,
  EsportsChatService,
  SuperProperties,
  EsportsTimezone,
  EsportsGtmService,
} from 'esports';
import { Subscription } from 'rxjs';
import {
  LadderPopupComponent,
  LadderPopupComponentData,
} from '../../../shared/popups/ladder-popup/ladder-popup.component';
import { environment } from '../../../../environments/environment';
import { MatTabChangeEvent, MatTabGroup } from '@angular/material/tabs';

@Component({
  selector: 'app-view-tournament',
  templateUrl: './view-tournament.component.html',
  styleUrls: ['./view-tournament.component.scss'],
})
export class ViewTournamentComponent implements OnInit, OnDestroy {
  tournamentDetails;
  active = 1;
  isLoaded = false;
  hideMessage = true;
  user: IUser;
  enableComment = false;
  text: string;
  participantId: string | null;
  isOrganizer: boolean = false;
  isParticipant: boolean = false;
  userSubscription: Subscription;
  challengeListener: Subscription;
  challengeErrorListener: Subscription;
  tournamentStatus: string | null;
  timezone;
  activeTabIndex = -1;
  participantStatus: string | null;
  showPlayNowBtn: boolean = true;
  slug: string | null;
  startDateIntervalId = null;
  endDateIntervalId = null;
  playNowKeys: any;
  @ViewChild('tabs') tabGroup: MatTabGroup;
  customTabEvent = new EventEmitter<MatTabChangeEvent>();
  tournamentEventSubscription: Subscription;

  playNowDefaultKeys = {
    LADDER_PLAY_NOW_DURATION_MIN: 3,
    LADDER_PLAY_NOW_INTERVAL_DURATION_SEC: 30,
    LADDER_PLAY_NOW_ANY_INTERVAL_DURATION_SEC: 15,
    LADDER_PLAY_NOW_FIRST_INTERVAL_BRACKET: 20,
    LADDER_PLAY_NOW_RECURSIVE_INTERVAL_BRACKET: 50,
  };

  constructor(
    private tournamentService: TournamentService,
    private activatedRoute: ActivatedRoute,
    private eSportsToastService: EsportsToastService,
    private utilsService: UtilsService,
    private matDialog: MatDialog,
    private ratingService: RatingService,
    private translateService: TranslateService,
    private globalUtils: GlobalUtils,
    private languageService: EsportsLanguageService,
    private ConstantsService: EsportsConstantsService,
    private esportsChatService: EsportsChatService,
    private router: Router,
    private titleService: Title,
    @Inject(DOCUMENT) private document: Document,
    private deeplinkService: DeeplinkService,
    private userService: EsportsUserService,
    private gtmService: EsportsGtmService,
    private location: Location,
    private esportsTimezone: EsportsTimezone,
    private homeService: HomeService,
    @Inject(PLATFORM_ID) private platformId
  ) {}

  ngOnDestroy() {
    this.userSubscription?.unsubscribe();
    this.unsubscribeListeners();
    this.esportsChatService.disconnectTournamentEvents(this.slug);
    clearInterval(this.startDateIntervalId);
    clearInterval(this.endDateIntervalId);
  }

  async ngOnInit() {
    // should be here for SSR
    const isBrowser = isPlatformBrowser(this.platformId);
    if (isBrowser && GlobalUtils.isBrowser()) {
      this.timezone = this.esportsTimezone.getTimeZoneName();
      this.slug = this.activatedRoute.snapshot.params.id; // get slug from route

      this.esportsChatService.connectTournamentEvents(this.slug);
      this.fetchTournamentEvents();

      this.getCurrentUserDetails();
      await this.fetchTournamentDetails(this.slug);
      this.pushGTMTags('Tournament_Details_Views');
      this.getPlayNowKeys();
      this.customTabEvent.subscribe((tabChangeEvent: MatTabChangeEvent) => {
        this.switchData(tabChangeEvent.index);
      });

      this.activatedRoute.queryParams
        .subscribe((params) => {
          if (params.activeTabIndex) {
            this.activeTabIndex = +params.activeTabIndex;
            this.switchData(this.activeTabIndex);
          } else if (!params.activeTabIndex && this.activeTabIndex > -1) {
            this.location.back();
          } else {
            this.activeTabIndex = 0;
            this.switchData(this.activeTabIndex);
          }
        })
        .unsubscribe();

      if (this.slug) {
        this.deeplinkService.deeplink({
          objectType: 'tournament',
          objectId: this.slug,
        });
      }
    }
  }

  isNumber(val): boolean {
    const str: string = val.toString();
    return str.startsWith('+');
  }
  getPlayNowKeys = async () => {
    this.homeService._playNowKeys().subscribe(
      (res: any) => {
        if (res.data) {
          this.playNowKeys = res.data;
        }
      },

      (err: any) => {}
    );
  };

  onTabChanged = (tabChangeEvent: MatTabChangeEvent): void => {
    this.customTabEvent.emit(tabChangeEvent);
    if (this.activeTabIndex == 1 || this.activeTabIndex == 0) {
      this.fetchTournamentDetails(this.tournamentDetails?.slug);
    }
  };

  switchData(index: number) {
    switch (index) {
      case 0:
        this.pushGTMTags('Select_Tournament_Overview_Tab');
        this.router.navigate(['.'], {
          relativeTo: this.activatedRoute,
          queryParams: { activeTabIndex: index },
        });
        break;
      case 1:
        this.pushGTMTags('Select_Tournament_Info_Tab');
        this.router.navigate(['.'], {
          relativeTo: this.activatedRoute,
          queryParams: { activeTabIndex: index },
        });
        break;
      case 2:
        this.pushGTMTags('Select_Tournament_Participants_Tab');
        this.router.navigate(['.'], {
          relativeTo: this.activatedRoute,
          queryParams: { activeTabIndex: index },
        });
        break;
      case 3:
        this.router.navigate(['.'], {
          relativeTo: this.activatedRoute,
          queryParams: { activeTabIndex: index },
        });
        break;
      case 4:
        this.pushGTMTags('Select_Tournament_Matches_Tab');
        this.router.navigate(['.'], {
          relativeTo: this.activatedRoute,
          queryParams: { activeTabIndex: index },
        });
        break;
      case 5:
        this.pushGTMTags('Select_Tournament_Discussion_Tab');
        this.router.navigate(['.'], {
          relativeTo: this.activatedRoute,
          queryParams: { activeTabIndex: index },
        });
        break;
      case 6:
        this.router.navigate(['.'], {
          relativeTo: this.activatedRoute,
          queryParams: { activeTabIndex: index },
        });
        break;

      default:
        break;
    }
  }

  getCurrentUserDetails() {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.user = data;
      }
    });
  }

  fetchTournamentDetails = async (
    slug,
    isTournamentStart: boolean = false,
    isTournamentFinished: boolean = false
  ) => {
    try {
      this.isLoaded = false;
      const query = JSON.stringify({
        slug: slug,
        tournamentStatus: 'publish',
      });
      const tournament = await this.tournamentService
        .getTournaments({ query })
        .toPromise();
      this.tournamentDetails = tournament.data.length
        ? tournament.data[0]
        : null;

      if (!this.tournamentDetails) {
        this.hideMessage = false;
        throw new Error(this.translateService.instant('VIEW_TOURNAMENT.ERROR'));
      }

      this.showTournamentAlertAndStartTimer(
        isTournamentStart,
        isTournamentFinished
      );

      if (
        (this.tournamentDetails && this.tournamentDetails.banner) ||
        this.tournamentDetails.gameDetail.logo
      ) {
        const domain = `${this.document.location.protocol}//${this.document.location.hostname}`;
        if (this.user) {
          this.tournamentDetails.username = this.user.username
            ? this.user.username
            : this.user.fullName;
          this.tournamentDetails.loggedInStatus = 'LoggedIn';
        } else {
          this.tournamentDetails.username = '';
          this.tournamentDetails.loggedInStatus = 'Guest';
        }
        this.gtmService.gtmEvent('viewed_tournament', this.tournamentDetails);
        this.titleService.setTitle(this.tournamentDetails.name);
        this.globalUtils.setMetaTags([
          {
            property: 'twitter:image',
            content:
              this.tournamentDetails.banner ||
              this.tournamentDetails.gameDetail.logo,
          },
          {
            property: 'og:image',
            content:
              this.tournamentDetails.banner ||
              this.tournamentDetails.gameDetail.logo,
          },
          {
            property: 'og:image:secure_url',
            content:
              this.tournamentDetails.banner ||
              this.tournamentDetails.gameDetail.logo,
          },
          {
            property: 'og:image:url',
            content:
              this.tournamentDetails.banner ||
              this.tournamentDetails.gameDetail.logo,
          },
          {
            property: 'og:image:width',
            content: '1200',
          },
          {
            property: 'og:image:height',
            content: '630',
          },
          {
            name: 'description',
            content: this.tournamentDetails.description,
          },
          {
            property: 'og:description',
            content: this.tournamentDetails.description,
          },
          {
            property: 'twitter:description',
            content: this.tournamentDetails.description,
          },
          {
            name: 'title',
            content: this.tournamentDetails.name,
          },
          {
            name: 'title',
            content: this.tournamentDetails.name,
          },
          {
            property: 'og:title',
            content: this.tournamentDetails.name,
          },
          {
            property: 'twitter:title',
            content: this.tournamentDetails.name,
          },
          {
            property: 'og:url',
            content: domain + this.router.url,
          },
        ]);
      }

      const data = await this.fetchParticipantRegisterationStatus(
        this.tournamentDetails?._id
      );
      if (data?.type == 'already-registered' || data?.participantId) {
        this.isParticipant = true;
      }

      this.active = this.tournamentService?.tournamentActiveTab || 1;
      this.isLoaded = true;
    } catch (error) {
      this.isLoaded = true;
      this.eSportsToastService.showError(
        error?.error?.message || error?.message
      );
    }
  };

  buttonResponse(data) {
    if (data) {
      this.fetchTournamentDetails(this.tournamentDetails?.slug);
    }
  }

  enableRating(data) {
    if (data?.isAuthorized) {
      // add logic for rating
    }
  }

  rateUs = async () => {
    try {
      const query = `?query=${encodeURIComponent(
        JSON.stringify({
          posterId: this.tournamentDetails._id,
          type: 'Tournament',
        })
      )}`;
      const rating = await this.ratingService.getRating(query);
      const rate = rating.data || {
        raterId: this.user?._id,
        posterId: this.tournamentDetails._id,
        value: 3,
        type: 'Tournament',
      };
      const blockUserData: InfoPopupComponentData = {
        title: 'Rate US',
        text: `Your feedback is valuable for us.`,
        type: InfoPopupComponentType.rating,
        btnText: 'Confirm',
        rate: rate.value,
      };

      const dialogRef = this.matDialog.open(InfoPopupComponent, {
        data: blockUserData,
      });

      const confirmed = await dialogRef.afterClosed().toPromise();

      if (confirmed) {
        rate.value = confirmed;
        const addOrUpdateRating = rate._id
          ? await this.ratingService.updateRating(rate._id, rate)
          : await this.ratingService.addRating(rate);
        this.eSportsToastService.showSuccess(addOrUpdateRating?.message);
      }
    } catch (error) {
      this.eSportsToastService.showError(
        error?.error?.message || error?.message
      );
    }
  };

  shareTournament = async () => {
    try {
      this.pushGTMTags('Tournament_Share');
      const data: InfoPopupComponentData = {
        title: this.translateService.instant('ARTICLE_POST.SHARE_TOURNAMENT'),
        text: ``,
        type: InfoPopupComponentType.socialSharing,
        cancelBtnText: 'Close',
      };

      const dialogRef = this.matDialog.open(InfoPopupComponent, {
        data,
      });

      await dialogRef.afterClosed().toPromise();
    } catch (error) {
      this.eSportsToastService.showError(
        error?.error?.message || error?.message
      );
    }
  };

  fetchParticipantRegisterationStatus = async (id: string) => {
    try {
      const { data } =
        await this.tournamentService.fetchParticipantRegistrationStatus(id);
      this.enableComment =
        this.tournamentDetails?.organizerDetail?._id == this.user?._id ||
        data?.isAllowComment;
      this.participantId = data?.participantId;
      this.tournamentStatus = data?.type;
      this.participantStatus = data?.participantStatus;
      const isOngoingOrCompleted = [
        'tournament-finished',
        'tournament-started',
      ].includes(data?.type);
      if (this.participantId) {
        if (!this.enableComment && !isOngoingOrCompleted) {
          this.text = 'DISCUSSION.UNAPPROVED_PARTICIPANT';
        }
      } else {
        if (!isOngoingOrCompleted) {
          this.text = 'DISCUSSION.PARTICIPANT_NA';
        }
      }
      return data;
    } catch (error) {
      this.eSportsToastService.showError(
        error?.error?.message || error?.message
      );
    }
  };

  async ladderPopup() {
    //this.matDialog.open(LadderPopupComponent);
    try {
      let popupData: LadderPopupComponentData = {
        isShowLoading: true,
        isShowNotFound: false,
        isShowScheduler: false,
        payload: { endDate: this.tournamentDetails.endDate }
      };

      this.matDialog.open(LadderPopupComponent, { data: popupData });
      const match = await this.tournamentService
        .getOpponentParticipantForMatch({
          tournamentid: this.tournamentDetails._id,
          participantid: this.participantId,
        })
        .toPromise();
      this.matDialog.closeAll();
      if (match && match.success && match.success == true) {
        //this.router.navigate([`/${match.data.challenge._id}`])
        this.router.navigate(
          [`tournament/${this.activatedRoute.snapshot.params.id}/ladder-card`],
          { queryParams: { chid: match.data.challenge._id } }
        ); //?cid=${match.data.challenge._id}
      } else {
        popupData.isShowLoading = false;
        popupData.isShowNotFound = true;
        const notFoundPopupRef = this.matDialog.open(LadderPopupComponent, {
          data: popupData,
        });
        notFoundPopupRef.afterClosed().subscribe((action) => {
          if (action == 'scheduleMatch') {
            this.ladderPopup();
          } else if (action == 'searchAgain') {
            this.ladderPlayNowPopup();
          }
        });
      }
    } catch (error) {
      this.matDialog.closeAll();
      this.eSportsToastService.showError(
        error?.error?.message || error?.message
      );
    }
  }

  async ladderPlayNowPopup() {
    try {
      if (this.playNowKeys) {
        let leftTime = this.playNowKeys.LADDER_PLAY_NOW_DURATION_MIN * 60;
        let leftTimePlayWithTop20 =
          this.playNowKeys.LADDER_PLAY_NOW_INTERVAL_DURATION_SEC ||
          this.playNowDefaultKeys.LADDER_PLAY_NOW_INTERVAL_DURATION_SEC;
        let leftTimePlayWithTop50 =
          this.playNowKeys.LADDER_PLAY_NOW_INTERVAL_DURATION_SEC ||
          this.playNowDefaultKeys.LADDER_PLAY_NOW_INTERVAL_DURATION_SEC;
        let leftTimePlayWithAny =
          this.playNowKeys.LADDER_PLAY_NOW_ANY_INTERVAL_DURATION_SEC ||
          this.playNowDefaultKeys.LADDER_PLAY_NOW_ANY_INTERVAL_DURATION_SEC;
        let firstBracket =
          this.playNowKeys.LADDER_PLAY_NOW_FIRST_INTERVAL_BRACKET ||
          this.playNowDefaultKeys.LADDER_PLAY_NOW_FIRST_INTERVAL_BRACKET;
        let recursiveBracket =
          this.playNowKeys.LADDER_PLAY_NOW_RECURSIVE_INTERVAL_BRACKET ||
          this.playNowDefaultKeys.LADDER_PLAY_NOW_RECURSIVE_INTERVAL_BRACKET;
        let isLadderPopUpClosed = false;
        const tournamentEndsIn =
          (new Date(this.tournamentDetails.endDate).getTime() -
            new Date().getTime()) /
          1000;
        this.esportsChatService.connectSocket();
        this.esportsChatService.connectUser();
        // if tournament ends in less than a minute hide play now button
        if (tournamentEndsIn < leftTime) {
          if (tournamentEndsIn < 60) {
            this.showPlayNowBtn = false;
            leftTime = 0;
            this.eSportsToastService.showInfo(
              this.translateService.instant('LADDER.TOURNAMENT_ENDED')
            );
            return;
          } else {
            leftTime = tournamentEndsIn - 60;
          }
        }

        let popupData: LadderPopupComponentData = {
          isShowLoading: true,
          payload: { leftTime: leftTime,endDate: this.tournamentDetails.endDate },
        };

        // this.ladderPopup();

        const loadDialogRef = this.matDialog.open(LadderPopupComponent, {
          data: popupData,
          disableClose: true,
        });
        this.esportsChatService.findOpponent({
          tournamentId: this.tournamentDetails._id,
          bracket: firstBracket,
        });

        this.challengeErrorListener = this.esportsChatService
          .listenChallengeError()
          .subscribe((payload) => {
            loadDialogRef.close(true);
            if (payload.error) {
              if (payload.error.toString().indexOf('just finished') > -1) {
                this.eSportsToastService.showError(
                  this.translateService.instant('LADDER.MATCH_JUST_FINISHED')
                );
              } else if (payload.error.toString().indexOf('confliction') > -1) {
              this.eSportsToastService.showError(
                  this.translateService.instant('LADDER.MATCH_SCORE_CONFLICTED')
                );
              } else {
              this.eSportsToastService.showError(
                  this.translateService.instant(
                    'LADDER.MATCH_NOT_FOUND_PART1'
                  ) +
                    payload.error +
                    this.translateService.instant(
                      'LADDER.MATCH_NOT_FOUND_PART2'
                    )
                );
              }
            } else {
              this.eSportsToastService.showError(
                this.translateService.instant('LADDER.MATCH_FOUND_ERROR')
              );
            }
            this.unsubscribeListeners();
          });

        this.challengeListener = this.esportsChatService
          .listenChallengeFound()
          .subscribe((payload) => {
            loadDialogRef.close(true);
            this.eSportsToastService.showSuccess(
              this.translateService.instant('LADDER.MATCH_FOUND')
            );
            this.router.navigate(
              [
                `tournament/${this.activatedRoute.snapshot.params.id}/ladder-card`,
              ],
              { queryParams: { chid: payload.data.newChallenge._id } }
            );
          });

        const loopCount = leftTime / leftTimePlayWithTop50 - 2; // first interval for 20 and last for any
        for (let index = 1; index <= loopCount; index++) {
          setTimeout(() => {
            if (isLadderPopUpClosed === false) {
              this.esportsChatService.findOpponent({
                tournamentId: this.tournamentDetails._id,
                bracket: recursiveBracket * index,
              });
            }
          }, leftTimePlayWithTop50 * index * 1000);
        }

        setTimeout(() => {
          if (isLadderPopUpClosed === false) {
            this.esportsChatService.findOpponent({
              tournamentId: this.tournamentDetails._id,
              bracket: 0,
            });
          }
        }, (leftTime - leftTimePlayWithAny) * 1000);

        loadDialogRef.afterClosed().subscribe((loadCloseData) => {
          isLadderPopUpClosed = true;
          this.esportsChatService.removeFromQueue(null);
          this.unsubscribeListeners();
          if (loadCloseData == 'timeOut') {
            if (new Date(this.tournamentDetails.endDate) > new Date()) {
              const notFoundPopupRef = this.matDialog.open(
                LadderPopupComponent,
                {
                  data: { isShowNotFound: true },
                }
              );
              notFoundPopupRef.afterClosed().subscribe((action) => {
                if (action == 'scheduleMatch') {
                  this.ladderPopup();
                } else if (action == 'searchAgain') {
                  this.ladderPlayNowPopup();
                }
              });
            } else {
              this.eSportsToastService.showInfo(
                this.translateService.instant('LADDER.TOURNAMENT_ENDED')
              );
            }
          }
        });
      } else {
        this.eSportsToastService.showInfo(
          this.translateService.instant('LADDER.PLAY_NOW_KEY_ERROR'),
          false
        );
      }
    } catch (error) {
      this.matDialog.closeAll();
      this.eSportsToastService.showError(
        error?.error?.message || error?.message
      );
    }
  }

  unsubscribeListeners() {
    if (this.challengeListener) {
      this.challengeListener.unsubscribe();
    }
    if (this.challengeErrorListener) {
      this.challengeErrorListener.unsubscribe();
    }
  }

  fetchTournamentEvents = () => {
    this.tournamentEventSubscription = this.esportsChatService
      .getTournamentEvents(this.slug)
      .subscribe((data) => {
        if (
          data?.eventType == 'tournament-started' &&
          data?.userId != this.user?._id
        ) {
          this.eSportsToastService.showInfo(
            this.translateService.instant('ELIMINATION.TOURNAMENT_STARTED'),
            false
          );
          this.fetchTournamentDetails(this.slug);
        } else if (
          data?.eventType == 'tournament-finished' &&
          data?.userId != this.user?._id
        ) {
          this.eSportsToastService.showInfo(
            this.translateService.instant('ELIMINATION.TOURNAMENT_FINISHED'),
            false
          );
          this.fetchTournamentDetails(this.slug);
        }
      });
  };

  /**
   * Start date Timer
   */
  startDateTimer = () => {
    const startDate = new Date(this.tournamentDetails.startDate);
    const currentDate = new Date();
    const timeDiff = startDate.getTime() - currentDate.getTime();
    if (timeDiff > 0) {
      if (!this.startDateIntervalId) {
        this.startDateIntervalId = setInterval(
          () => this.startDateTimer(),
          1000
        );
      }
    } else if (
      ['swiss_safeis', 'ladder'].includes(this.tournamentDetails?.bracketType)
    ) {
      if (this.startDateIntervalId) {
        this.fetchTournamentDetails(this.tournamentDetails?.slug, true, false);
        clearInterval(this.startDateIntervalId);
        this.startDateIntervalId = null;
      }
    }
  };

  /**
   * End date timer
   */
  endDateTimer = () => {
    const endDate = new Date(this.tournamentDetails?.endDate);
    const currentDate = new Date();
    const timeDiff = endDate.getTime() - currentDate.getTime();

    /* if tournament ends in less than a minute hide play now button */
    if (timeDiff / 1000 < 60) {
      if (this.showPlayNowBtn) {
        this.matDialog.closeAll();
        this.unsubscribeListeners();
        this.esportsChatService.removeFromQueue(true);
        this.eSportsToastService.showInfo(
          this.translateService.instant('LADDER.TOURNAMENT_ENDED')
        );
      }
      this.showPlayNowBtn = false;
    }
    if (timeDiff > 0) {
      if (!this.endDateIntervalId) {
        this.endDateIntervalId = setInterval(() => this.endDateTimer(), 1000);
      }
    } else if (
      ['swiss_safeis', 'ladder'].includes(this.tournamentDetails?.bracketType)
    ) {
      if (this.endDateIntervalId) {
        this.fetchTournamentDetails(this.tournamentDetails?.slug, false, true);
        clearInterval(this.endDateIntervalId);
        this.endDateIntervalId = null;
      }
    }
  };

  showTournamentAlertAndStartTimer = (
    isTournamentStart: boolean,
    isTournamentFinished: boolean
  ) => {
    if (this.tournamentDetails?.isFinished) {
      if (isTournamentFinished) {
        this.eSportsToastService.showInfo(
          this.translateService.instant('ELIMINATION.TOURNAMENT_FINISHED'),
          false
        );
      }
    } else if (this.tournamentDetails?.isSeeded) {
      if (isTournamentStart) {
        this.eSportsToastService.showInfo(
          this.translateService.instant('ELIMINATION.TOURNAMENT_STARTED'),
          false
        );
      }
      this.endDateTimer();
    } else {
      this.startDateTimer();
    }
  };

  pushGTMTags(eventName: string) {
    let superProperties: SuperProperties = {};
    if (this.user) {
      superProperties = this.gtmService.assignLoggedInUsedData(this.user);
    }
    this.gtmService.gtmEventWithSuperProp({
      eventName: eventName,
      superProps: superProperties,
    });
  }
}
