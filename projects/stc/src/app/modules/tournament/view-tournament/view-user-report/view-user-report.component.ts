import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  OnDestroy,
  Inject,
  PLATFORM_ID,
} from '@angular/core';
import { UserReportsService, EsportsUserService } from 'esports';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { environment } from '../../../../../environments/environment';
import { isPlatformBrowser } from '@angular/common';
@Component({
  selector: 'app-view-user-report',
  templateUrl: './view-user-report.component.html',
  styleUrls: ['./view-user-report.component.scss'],
})
export class ViewUserReportComponent implements OnInit, OnDestroy {
  @Input() userReportDetail;
  @Output() backBtn = new EventEmitter();
  @Output() reloadData = new EventEmitter();
  replyText;
  base64textString;
  isLoaded = true;
  screenshot = '../assets/images/articles/banner.png';
  userSubscription: Subscription;
  isAdmin = false;
  isBrowser: boolean;

  constructor(
    private UserReportsService: UserReportsService,
    private modalService: NgbModal,
    private userService: EsportsUserService,
    @Inject(PLATFORM_ID) private platformId
  ) {
    this.isBrowser = isPlatformBrowser(platformId);
  }

  ngOnInit(): void {
    if (this.isBrowser) {
      this.getCurrentUserDetails();
    }
  }

  ngOnDestroy() {
    this.userSubscription?.unsubscribe();
  }

  getCurrentUserDetails() {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        if (data?.accountType === 'admin') {
          this.isAdmin = true;
        }
      }
    });
  }

  onReply(isClose) {
    if (
      this.userReportDetail.report._id &&
      (this.replyText || this.base64textString)
    ) {
      const data = {
        reportId: this.userReportDetail.report._id,
        reportText: this.replyText,
        reportImage: this.base64textString || '',
      };
      this.isLoaded = false;
      this.UserReportsService.replyOnUserReport(
        environment.apiEndPoint,
        data
      ).subscribe(
        (res) => {
          if (isClose) {
            this.UserReportsService.closeReport(
              environment.apiEndPoint,
              this.userReportDetail.report._id
            ).subscribe(
              (res) => {
                this.isLoaded = true;
              },
              (err) => {
                this.isLoaded = true;
              }
            );
            this.replyText = '';
            this.base64textString = '';
            this.backBtn.emit(null);
          } else {
            this.replyText = '';
            this.base64textString = '';
            this.backBtn.emit(null);
            this.isLoaded = true;
          }
          this.reloadData.emit(null);
        },
        (err) => {
          this.isLoaded = true;
        }
      );
    }
  }

  //convert image to binary
  handleFileSelect(evt) {
    const files = evt.target.files;
    const file = files[0];

    if (files && file) {
      const reader = new FileReader();
      reader.onload = this._handleReaderLoaded.bind(this);
      reader.readAsBinaryString(file);
    }
  }

  _handleReaderLoaded(readerEvt) {
    const binaryString = readerEvt.target.result;
    this.base64textString = 'data:image/png;base64,' + btoa(binaryString);
  }

  backBtnClicked() {
    this.backBtn.emit(null);
  }

  downloadUrl(url: string) {
    const a: any = document.createElement('a');
    a.href = url;
    a.download = 'commentImg';
    document.body.appendChild(a);
    a.style = 'display: none';
    a.click();
    a.remove();
  }

  openResult(content) {
    this.modalService.open(content, {
      centered: true,
      size: 'lg',
      scrollable: false,
      windowClass: 'custom-modal-content',
    });
  }

  exitFromScoreCard(data) {
    this.modalService.dismissAll();
  }
}
