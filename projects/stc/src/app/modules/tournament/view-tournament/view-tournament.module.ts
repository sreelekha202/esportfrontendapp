import { NgModule } from '@angular/core';
import { FaIconLibrary } from '@fortawesome/angular-fontawesome';
import { faTrash, faShareAlt } from '@fortawesome/free-solid-svg-icons';

import { SharedModule } from '../../../shared/modules/shared.module';
import { ViewTournamenttRoutingModule } from './view-tournament-routing.module';
import { EsportsModule, EsportsCustomPaginationModule } from 'esports';
import { environment } from '../../../../environments/environment';

import { ViewTournamentComponent } from './view-tournament.component';
import { JoinParticipantComponent } from './join-participant/join-participant.component';
import { OverviewComponent } from './overview/overview.component';
import { ParticipantsComponent } from './participants/participants.component';
import { BracketsComponent } from './brackets/brackets.component';
import { JoinButtonComponent } from './join-button/join-button.component';
import { InfoComponent } from './info/info.component';
import { FormComponentModule } from '../../../shared/components/form-component/form-component.module';
import { UserReportComponent } from './user-report/user-report.component';
import { ViewUserReportComponent } from './view-user-report/view-user-report.component';
import { CountdownModule } from 'ngx-countdown';
import { LadderMatchCardComponent } from './ladder-match-card/ladder-match-card.component';
import { MatchViewComponent } from './match-view/match-view.component';
import { CoreModule } from '../../../core/core.module';

@NgModule({
  declarations: [
    ViewTournamentComponent,
    JoinParticipantComponent,
    OverviewComponent,
    ParticipantsComponent,
    BracketsComponent,
    JoinButtonComponent,
    InfoComponent,
    UserReportComponent,
    ViewUserReportComponent,
    LadderMatchCardComponent,
    MatchViewComponent,
  ],
  imports: [
    ViewTournamenttRoutingModule,
    SharedModule,
    FormComponentModule,
    CountdownModule,
    EsportsModule.forRoot(environment),
    EsportsCustomPaginationModule,
    CoreModule,
  ],
  providers: [],
})
export class ViewTournamentModule {
  constructor(private library: FaIconLibrary) {
    library.addIcons(faTrash, faShareAlt);
  }
}
