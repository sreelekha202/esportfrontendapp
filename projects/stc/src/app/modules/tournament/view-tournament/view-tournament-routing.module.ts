import { LadderMatchCardComponent } from './ladder-match-card/ladder-match-card.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ViewTournamentComponent } from './view-tournament.component';
import { JoinParticipantComponent } from './join-participant/join-participant.component';
import { AuthGuard } from '../../../shared/guard/auth.guard';
import { MatchViewComponent } from './match-view/match-view.component';
const routes: Routes = [
  { path: '', component: ViewTournamentComponent },
  {
    path: 'join',
    canActivate: [AuthGuard],
    component: JoinParticipantComponent,
  },
  {
    path: 'join/:pId',
    canActivate: [AuthGuard],
    component: JoinParticipantComponent,
  },
  {
    path: 'ladder-card',
    component: LadderMatchCardComponent,
  },
  {
    path: 'match-view',
    component: MatchViewComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ViewTournamenttRoutingModule {}
