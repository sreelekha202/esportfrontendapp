import { ReportComponent } from './report/report.component';
import { TournamentRoutingModule } from './tournament-routing.module';
import { TournamentComponent } from './tournament.component';
import { NgModule } from '@angular/core';

import { TournamentInfoComponent } from './tournament-info/tournament-info.component';
import { SharedModule } from '../../shared/modules/shared.module';
import { CoreModule } from '../../core/core.module';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

@NgModule({
  declarations: [TournamentComponent, TournamentInfoComponent, ReportComponent],
  imports: [
    SharedModule,
    CoreModule,
    TournamentRoutingModule,
    MatProgressSpinnerModule,
  ],
  providers: [],
})
export class TournamentModule {}
