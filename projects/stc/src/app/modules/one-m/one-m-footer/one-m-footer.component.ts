import { Component, OnInit } from '@angular/core';
import { AppHtmlRoutes } from '../../../app-routing.model';
@Component({
  selector: 'app-one-m-footer',
  templateUrl: './one-m-footer.component.html',
  styleUrls: ['./one-m-footer.component.scss']
})
export class OneMFooterComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;
  
  navigation = [
    { link: AppHtmlRoutes.aboutUs, text: 'FOOTER.ABOUT' },
    { link: AppHtmlRoutes.termsOfUse, text: 'FOOTER.TERMS_OF_USE' },
    { link: AppHtmlRoutes.privacyPolicy, text: 'FOOTER.PRIVACY' },
    { link: AppHtmlRoutes.codeOfConduct, text: 'FOOTER.CODE_OF_CONDUCT' },
    { link: AppHtmlRoutes.antiSpamPolicy, text: 'FOOTER.ANTI_SPAM_POLICY' },
  ];
  constructor() { }

  ngOnInit(): void {
  }

}