import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OneMFooterComponent } from './one-m-footer.component';

describe('OneMFooterComponent', () => {
  let component: OneMFooterComponent;
  let fixture: ComponentFixture<OneMFooterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OneMFooterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OneMFooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
