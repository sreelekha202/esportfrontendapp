import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OneMLoginComponent } from './one-m-login/one-m-login.component';
import { OneMPagesComponent } from './one-m-pages/one-m-pages.component';
import { OneMComponent } from './one-m.component';

const routes: Routes = [
  {
    path: '',
    component: OneMComponent,
    children: [
      {
        path: '',
        component: OneMPagesComponent,
      },
    ],
  },
  {
    path: '1m-login',
    component: OneMComponent,
    children: [
      {
        path: '',
        component: OneMLoginComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OneMRoutingModule { }
