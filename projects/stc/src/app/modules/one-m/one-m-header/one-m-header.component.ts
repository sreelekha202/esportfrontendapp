import { Component, OnInit } from '@angular/core';
import {
  EsportsChatSidenavService,
  EsportsLanguageService,
  EsportsConstantsService,
  EsportsUserService,
  EsportsChatService,
  EsportsToastService,
  GlobalUtils,
  EsportsGtmService,
  SuperProperties,
  EventProperties,
} from 'esports';

import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-one-m-header',
  templateUrl: './one-m-header.component.html',
  styleUrls: ['./one-m-header.component.scss']
})
export class OneMHeaderComponent implements OnInit {

  activeLang;
  currentUser;

  constructor(
    public translate: TranslateService,
    private languageService: EsportsLanguageService,
    private gtmService: EsportsGtmService,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.activeLang = this.translate.currentLang;
  }

  onLanguageChange(lang): void {
    if (GlobalUtils.isBrowser()) {
      this.pushGTM('turki', 'Change_Language');
      localStorage.setItem('currentLanguage', lang);
      this.activeLang = lang;
      this.translate.use(lang);
      this.languageService.setLanguage(lang);
      window.location.reload();
    }
  }

  pushGTM(id, eventName: string) {
    let superProperties: SuperProperties = {};
    if (this.currentUser) {
      superProperties = this.gtmService.assignLoggedInUsedData(
        this.currentUser
      );
    }

    let eventProperties: EventProperties = {};

    if (eventName == 'View_Profile') {
      eventProperties['fromPage'] = this.router.url;
    }

    this.gtmService.gtmEventWithSuperProp({
      eventName: eventName,
      superProps: superProperties,
      eventProps: eventProperties,
    });
    setTimeout(() => {
      let el = document.getElementById(id);
      el.scrollIntoView({ behavior: 'smooth', block: 'center' });
    }, 1500);
  }

}
