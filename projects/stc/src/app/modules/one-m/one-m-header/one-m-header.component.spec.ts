import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OneMHeaderComponent } from './one-m-header.component';

describe('OneMHeaderComponent', () => {
  let component: OneMHeaderComponent;
  let fixture: ComponentFixture<OneMHeaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OneMHeaderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OneMHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
