import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-one-m-login',
  templateUrl: './one-m-login.component.html',
  styleUrls: ['./one-m-login.component.scss']
})
export class OneMLoginComponent implements OnInit {

  activeLang;
  schedule1: boolean = true;
  schedule2: boolean = false;
  schedule3: boolean = false;
  schedule4: boolean = false;
  schedule5: boolean = false;
  schedule6: boolean = false;
  schedule7: boolean = false;
  schedule8: boolean = false;
  schedule9: boolean = false;

  constructor(public translate: TranslateService,) { }

  ngOnInit(): void {
    this.activeLang = this.translate.currentLang;
  }

  clickSchedule(index) {
    if(index == 1) {
      this.schedule1 = true;
      this.schedule2 = false;
      this.schedule3 = false;
      this.schedule4 = false;
      this.schedule5 = false;
      this.schedule6 = false;
      this.schedule7 = false;
      this.schedule8 = false;
      this.schedule9 = false;
    } else if(index == 2) {
      this.schedule2 = true;
      this.schedule1 = false;
      this.schedule3 = false;
      this.schedule4 = false;
      this.schedule5 = false;
      this.schedule6 = false;
      this.schedule7 = false;
      this.schedule8 = false;
      this.schedule9 = false;
    }
    else if(index == 3) {
      this.schedule1 = false;
      this.schedule2 = false;
      this.schedule3 = true;
      this.schedule4 = false;
      this.schedule5 = false;
      this.schedule6 = false;
      this.schedule7 = false;
      this.schedule8 = false;
      this.schedule9 = false;
    }
    else if(index == 4) {
      this.schedule1 = false;
      this.schedule2 = false;
      this.schedule3 = false;
      this.schedule4 = true;
      this.schedule5 = false;
      this.schedule6 = false;
      this.schedule7 = false;
      this.schedule8 = false;
      this.schedule9 = false;
    }
    else if(index == 5) {
      this.schedule1 = false;
      this.schedule2 = false;
      this.schedule3 = false;
      this.schedule4 = false;
      this.schedule5 = true;
      this.schedule6 = false;
      this.schedule7 = false;
      this.schedule8 = false;
      this.schedule9 = false;
    }
    else if(index == 6) {
      this.schedule1 = false;
      this.schedule2 = false;
      this.schedule3 = false;
      this.schedule4 = false;
      this.schedule5 = false;
      this.schedule6 = true;
      this.schedule7 = false;
      this.schedule8 = false;
      this.schedule9 = false;
    }
    else if(index == 7) {
      this.schedule1 = false;
      this.schedule2 = false;
      this.schedule3 = false;
      this.schedule4 = false;
      this.schedule5 = false;
      this.schedule6 = false;
      this.schedule7 = true;
      this.schedule8 = false;
      this.schedule9 = false;
    }
    else if(index == 8) {
      this.schedule1 = false;
      this.schedule2 = false;
      this.schedule3 = false;
      this.schedule4 = false;
      this.schedule5 = false;
      this.schedule6 = false;
      this.schedule7 = false;
      this.schedule8 = true;
      this.schedule9 = false;
    }
    else if(index == 9) {
      this.schedule1 = false;
      this.schedule2 = false;
      this.schedule3 = false;
      this.schedule4 = false;
      this.schedule5 = false;
      this.schedule6 = false;
      this.schedule7 = false;
      this.schedule8 = false;
      this.schedule9 = true;
    }
  }

}
