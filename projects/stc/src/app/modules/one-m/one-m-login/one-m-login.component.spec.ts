import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OneMLoginComponent } from './one-m-login.component';

describe('OneMLoginComponent', () => {
  let component: OneMLoginComponent;
  let fixture: ComponentFixture<OneMLoginComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OneMLoginComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OneMLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
