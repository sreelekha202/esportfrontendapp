import { AuthServices, ScriptLoadingService } from '../../../core/service';
import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import {
  CountryISO,
  NgxIntlTelInputComponent,
  PhoneNumberFormat,
  SearchCountryField,
} from 'ngx-intl-tel-input';
import { EsportsLanguageService, EsportsToastService, EsportsUserService } from 'esports';
import { AbstractControl, FormBuilder, Validators } from '@angular/forms';

import { Country } from 'ngx-intl-tel-input/lib/model/country.model';
import { ReCaptcha2Component } from 'ngx-captcha';
import { TranslateService } from '@ngx-translate/core';
import { environment } from '../../../../environments/environment';
declare var $: any;

const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;

@Component({
  selector: 'app-one-m-pages',
  templateUrl: './one-m-pages.component.html',
  styleUrls: ['./one-m-pages.component.scss'],
})
export class OneMPagesComponent implements OnInit {
  @ViewChild('captchaElem2') captchaElem2: ReCaptcha2Component;
  step = 1;
  isExist: boolean = false;
  loadingStatus: boolean = false;
  confirmMessage: boolean = false;
  showBackBtn: boolean = false;
  type = 'password';
  confirmType = 'password';
  LANGUAGE = 'en';
  errormsg = '';
  message = '';
  token = '';
  schedule1: boolean = true;
  schedule2: boolean = false;
  schedule3: boolean = false;
  schedule4: boolean = false;
  schedule5: boolean = false;
  schedule6: boolean = false;
  schedule7: boolean = false;
  schedule8: boolean = false;
  schedule9: boolean = false;
  otp = '';

  RECAPTCHA_V2_SITE_KEY = environment.RECAPTCHA_V2_SITE_KEY;
  currentUser: any;
  form = this.formBuilder.group({
    fullName: [null, Validators.required],
    phone: [null, [Validators.required, Validators.pattern(/^(00|0|)(05|5|)(\d{9})$/)]],
    username: [
      null,
      [
        Validators.required,
        Validators.minLength(3),
        Validators.pattern(/^[a-zA-Z0-9_]+$/),
      ],
    ],
    email: [null, [Validators.required, Validators.email]],
    password: [null, Validators.required],
    password_confirm: [null, [Validators.required]],
    privacyPolicy: ['', Validators.required],
    captcha: ['', Validators.required],
  },
    {
      validator: this.passwordConfirming
    }
  );
  SearchCountryField = SearchCountryField;
  PhoneNumberFormat = PhoneNumberFormat;
  CountryISO = CountryISO;
  preferredCountries: CountryISO[] = [
    CountryISO.SaudiArabia,
    CountryISO.Bahrain,
    CountryISO.Kuwait,
    // CountryISO.India,
  ];
  selectedCountryISO = CountryISO.SaudiArabia;
  mySelectedCountryISO: CountryISO;
  activeLang;

  constructor(
    private translate: TranslateService,
    private userService: EsportsUserService,
    private languageService: EsportsLanguageService,
    private formBuilder: FormBuilder,
    private authServices: AuthServices,
    private eSportsToastService: EsportsToastService,
    private scriptLoadingService: ScriptLoadingService
  ) { }

  ngOnInit(): void {
    this.activeLang = this.translate.currentLang;
    this.getUserData();
    this.loadScript();

    $(document).ready(function () {
      $('.accordion-button').on('click', function () {
        if ($(this).hasClass('arrow_down')) {
          $(this).closest('.panel-default').find('.panel-collapse').slideUp();
          $(this).removeClass('arrow_down');
        }
        else {
          $('.accordion-button').removeClass('arrow_down');
          $(this).addClass('arrow_down');
          $('.panel-collapse').slideUp();
          $(this).closest('.panel-default').find('.panel-collapse').slideDown();
        }


      });
    });
  }

  loadScript() {
    let Url1 = 'https://www.googletagmanager.com/gtm.js?id=GTM-PNMHV9J';
    this.scriptLoadingService.registerScript(Url1, 'gtm', (res) => {});
    let Url = 'https://www.googletagmanager.com/ns.html?id=GTM-PNMHV9J';
    this.scriptLoadingService.registerScript(Url, 'gtm', (res) => {});
  }

  getUserData() {
    this.currentUser = localStorage.getItem('DUID');
  }

  onSubmitEvent() {
    if (this.form.valid) {
      let tempPhone;
      if(this.form.value.phone.indexOf(0) === 0) {
        tempPhone  = this.form.value.phone.slice(1)
      } else {
      tempPhone  = this.form.value.phone;
      }
      this.step = 2;
      this.loadingStatus = true;
      this.message = '';
      // Form Submit
      let register = {
        captcha: this.form.value.captcha,
        fullName: this.form.value.fullName,
        username: this.form.value.username,
        password: this.form.value.password,
        phoneNumber: '+966' + tempPhone,
        email: this.form.value.email,
        // referredBy: '',
        registrationSource: '1m',
        captchaElem: this.captchaElem2,
      };

      this.authServices.register(register).subscribe(
        (authData) => {
          this.token = authData.data.token;
          if (authData.code === 'ER1001' || authData.messageCode === 'R_E003') {
            this.step = 3;
            this.errormsg = authData.message;
            this.showBackBtn = true;
            this.loadingStatus = false;
          } else {
            if (authData.status === 'error') {
              this.step = 3;
              this.errormsg = authData.message;
              this.showBackBtn = true;
            } else {
              this.token = authData.data.token;
              this.loadingStatus = false;
              this.step = 2;
            //  this.showBackBtn = false;
             // this.message = 'thankMsg';
            }
          }
        },
        (error) => {
          this.step = 3;
          this.showBackBtn = true;
          this.errormsg = error.error.message;
          this.loadingStatus = false;
        }
      );
    }
    if (this.captchaElem2) {
      this.captchaElem2.resetCaptcha();
    }
  }

  passwordConfirming(c: AbstractControl): { invalid: boolean } {
    if (c.get('password').value !== c.get('password_confirm').value) {
      return { invalid: true };
    }
  }

  goToRegister() {
    this.step = 1;
    this.loadingStatus = false;
    this.captchaElem2.resetCaptcha();
  }

  confirmOTP(): void {
    if (this.otp.length == 6) {
      //this.loadingStatus = true;
      this.authServices.confirmOTP(this.otp, this.token, 'phone').subscribe(
        async (data) => {
          this.step = 3;
          this.showBackBtn = false;
          this.message = 'thankMsg';
        },
        error => {

        }
      );
    }
  }

  onCountryChange(e: Country) {
    this.mySelectedCountryISO = e.iso2 as CountryISO;
  }

  // isUniqueName = async (name) => {
  //   // if(name.length > 4) {
  //     this.loadingStatus = true;
  //     this.authServices.searchUsername(name).subscribe(
  //       (response) => {
  //         this.isExist = response.data?.isExist;
  //         if(this.isExist) {
  //           this.loadingStatus = false;
  //           this.confirmMessage = true;
  //           this.showBackBtn = true;
  //           this.step1 = false;
  //           this.loadingStatus = false;
  //           this.message= "existUsername";
  //         }
  //       }
  //     );
  //   //}
  // };

  onVisibilityToggle(type): void {
    this.type = this.type === 'text' ? 'password' : 'text';
  }

  onVisibilityToggleConfirm(confirmType): void {
    this.confirmType = this.confirmType === 'text' ? 'password' : 'text';
  }

  resolved(token: string) {
    this.form.get('captcha').setValue(token);
  }

  handleReset() {
    this.form.get('captcha').setValue('');
  }

  getCurrentLang() {
    this.languageService.language.subscribe((lang) => {
      this.LANGUAGE = lang || 'en';
    });
  }

  clickSchedule(index) {
    if(index == 1) {
      this.schedule1 = true;
      this.schedule2 = false;
      this.schedule3 = false;
      this.schedule4 = false;
      this.schedule5 = false;
      this.schedule6 = false;
      this.schedule7 = false;
      this.schedule8 = false;
      this.schedule9 = false;
    } else if(index == 2) {
      this.schedule2 = true;
      this.schedule1 = false;
      this.schedule3 = false;
      this.schedule4 = false;
      this.schedule5 = false;
      this.schedule6 = false;
      this.schedule7 = false;
      this.schedule8 = false;
      this.schedule9 = false;
    }
    else if(index == 3) {
      this.schedule1 = false;
      this.schedule2 = false;
      this.schedule3 = true;
      this.schedule4 = false;
      this.schedule5 = false;
      this.schedule6 = false;
      this.schedule7 = false;
      this.schedule8 = false;
      this.schedule9 = false;
    }
    else if(index == 4) {
      this.schedule1 = false;
      this.schedule2 = false;
      this.schedule3 = false;
      this.schedule4 = true;
      this.schedule5 = false;
      this.schedule6 = false;
      this.schedule7 = false;
      this.schedule8 = false;
      this.schedule9 = false;
    }
    else if(index == 5) {
      this.schedule1 = false;
      this.schedule2 = false;
      this.schedule3 = false;
      this.schedule4 = false;
      this.schedule5 = true;
      this.schedule6 = false;
      this.schedule7 = false;
      this.schedule8 = false;
      this.schedule9 = false;
    }
    else if(index == 6) {
      this.schedule1 = false;
      this.schedule2 = false;
      this.schedule3 = false;
      this.schedule4 = false;
      this.schedule5 = false;
      this.schedule6 = true;
      this.schedule7 = false;
      this.schedule8 = false;
      this.schedule9 = false;
    }
    else if(index == 7) {
      this.schedule1 = false;
      this.schedule2 = false;
      this.schedule3 = false;
      this.schedule4 = false;
      this.schedule5 = false;
      this.schedule6 = false;
      this.schedule7 = true;
      this.schedule8 = false;
      this.schedule9 = false;
    }
    else if(index == 8) {
      this.schedule1 = false;
      this.schedule2 = false;
      this.schedule3 = false;
      this.schedule4 = false;
      this.schedule5 = false;
      this.schedule6 = false;
      this.schedule7 = false;
      this.schedule8 = true;
      this.schedule9 = false;
    }
    else if(index == 9) {
      this.schedule1 = false;
      this.schedule2 = false;
      this.schedule3 = false;
      this.schedule4 = false;
      this.schedule5 = false;
      this.schedule6 = false;
      this.schedule7 = false;
      this.schedule8 = false;
      this.schedule9 = true;
    }
  }
}
