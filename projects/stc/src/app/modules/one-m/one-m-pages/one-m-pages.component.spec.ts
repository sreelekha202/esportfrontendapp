import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OneMPagesComponent } from './one-m-pages.component';

describe('OneMPagesComponent', () => {
  let component: OneMPagesComponent;
  let fixture: ComponentFixture<OneMPagesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OneMPagesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OneMPagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
