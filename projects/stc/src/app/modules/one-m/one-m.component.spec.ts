import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OneMComponent } from './one-m.component';

describe('OneMComponent', () => {
  let component: OneMComponent;
  let fixture: ComponentFixture<OneMComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OneMComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OneMComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
