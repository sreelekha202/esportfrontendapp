import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OneMRoutingModule } from './one-m-routing.module';
import { OneMComponent } from './one-m.component';
import { SharedModule } from '../../shared/modules/shared.module';
import { NgxCaptchaModule } from 'ngx-captcha';
import { OneMFooterComponent } from './one-m-footer/one-m-footer.component';
import { OneMHeaderComponent } from './one-m-header/one-m-header.component';
import { OneMPagesComponent } from './one-m-pages/one-m-pages.component';
import { OneMLoginComponent } from './one-m-login/one-m-login.component';


@NgModule({
  declarations: [
    OneMComponent,
    OneMFooterComponent,
    OneMHeaderComponent,
    OneMPagesComponent,
    OneMLoginComponent
  ],
  imports: [
    SharedModule,
    CommonModule,
    OneMRoutingModule,
    NgxCaptchaModule
  ]
})
export class OneMModule { }
