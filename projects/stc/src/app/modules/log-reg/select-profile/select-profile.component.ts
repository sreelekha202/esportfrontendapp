import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import {
  AppHtmlRoutes,
  AppHtmlRoutesLoginType,
} from '../../../app-routing.model';
import { AuthServices } from '../../../core/service';
import {
  EsportsLanguageService,
  GlobalUtils,
  EsportsGtmService,
  SuperProperties,
} from 'esports';
import { TranslateService } from '@ngx-translate/core';

export interface SelectProfileComponentDataItem {
  footerLabel: string;
  footerLinkText: string;
  footerLinkUrl: string[];
  submitBtn: string;
  subtitle: string;
  title: string;
}

const userLinks = new Map([
  [0, 'registration'],
  [1, 'registrationAsGameCenter'],
  [2, 'registration'],
  [3, 'registrationAsOrganizer'],
]);

const whiteimageArray = new Map([
  [0, 'assets/images/Register/active-gamer.svg'],
  [1, 'assets/images/Register/active-gamecenter.svg'],
  [2, 'assets/images/Register/Team_white.svg'],
  [3, 'assets/images/Register/icon-organizer.svg'],
]);

@Component({
  selector: 'app-select-profile',
  templateUrl: './select-profile.component.html',
  styleUrls: ['./select-profile.component.scss'],
})
export class SelectProfileComponent implements OnInit {
  @Output() submit = new EventEmitter();

  data: SelectProfileComponentDataItem = {
    title: 'LOG_REG.SELECT_PROFILE.TITLE',
    subtitle: 'LOG_REG.SELECT_PROFILE.SUBTITLE',
    submitBtn: 'LOG_REG.SELECT_PROFILE.SUBMIT_BTN',
    footerLabel: 'LOG_REG.REGISTER.footerText',
    footerLinkText: 'LOG_REG.REGISTER.footerLink',
    footerLinkUrl: [
      AppHtmlRoutes.userPageType,
      AppHtmlRoutesLoginType.phoneLogin,
    ],
  };

  AppHtmlRoutesLoginType = AppHtmlRoutesLoginType;

  selectedProfile = 0;

  profiles = [];
  isBrowser:Boolean = false;

  constructor(
    private router: Router,
    private authService: AuthServices,
    public languageService: EsportsLanguageService,
    private translateService: TranslateService,
    private gtmService: EsportsGtmService
  ) {}

  ngOnInit(): void {
    if (GlobalUtils.isBrowser()) {
      this.isBrowser = true;
      this.authService.getRegistrationType().subscribe((res) => {
        if (res && res.data.length) {
          this.profiles = res.data.map((item, index) => {
            return {
              name: item.name,
              image: item.imageUrl,
              url: AppHtmlRoutesLoginType[userLinks.get(item.statusId) || 0],
              whiteimage: whiteimageArray.get(index) || '',
              profileText: this.translateService.instant(`LOG_REG.REGISTER.PROFILE_TEXT.${item.name['english'].replace(/ /g, "_").toUpperCase()}`)
            };
          });
          localStorage.setItem(
            'registrationType',
            this.profiles[this.selectedProfile].name.english.toString()
          );
        }
      });
    }
  }

  onSelectProfile(index: number): void {
    if (GlobalUtils.isBrowser()) {
      this.selectedProfile = index;
      localStorage.setItem(
        'registrationType',
        this.profiles[this.selectedProfile].name.english.toString()
      );
    }
  }

  onSubmitEvent(): void {
    let data: SuperProperties = {};
    data['profiletype'] =
      this.profiles[this.selectedProfile].name.english.toString();

    this.gtmService.gtmEventWithSuperProp({
      eventName: 'Profile_Type',
      superProps: data,
    });

    this.router.navigate(['/user', this.profiles[this.selectedProfile].url]);
    // this.submit.emit({
    //   type: this.profiles[this.selectedProfile].type,
    // });
  }
}
