import {
  CountryISO,
  SearchCountryField,
} from 'ngx-intl-tel-input';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';

import { AppHtmlRoutesLoginType } from '../../../app-routing.model';

export interface PhoneNoFormComponentOutputData {
  [key: string]: string;
}

export interface PhoneNoFormComponentDataItem {
  title: string;
  subtitle: string;
  submitBtn: string;
  cancelBtn: string;
  InputName: string;
  InputType: string;
  InputPlaceholder: string;
  InputIcon: string;
  error?: string;
  value?: string;
}

@Component({
  selector: 'app-phone-no-form',
  templateUrl: './phone-no.component.html',
  styleUrls: ['./phone-no.component.scss'],
})
export class PhoneNoFormComponent implements OnInit {
  @Output() submit = new EventEmitter();
  @Input() data: PhoneNoFormComponentDataItem;
  @Input() set type(type: AppHtmlRoutesLoginType) {
    this.activeTypeValue = type;
  }

  AppHtmlRoutesLoginType = AppHtmlRoutesLoginType;
  activeTypeValue: AppHtmlRoutesLoginType;

  form = this.formBuilder.group({
    firstInput: ['', Validators.required],
  });

  SearchCountryField = SearchCountryField;
  CountryISO = CountryISO;
  preferredCountries: CountryISO[] = [
    CountryISO.SaudiArabia,
    CountryISO.Bahrain,
    CountryISO.Kuwait,
  ];

  constructor(private formBuilder: FormBuilder) {}

  ngOnInit() {
    // this.data.InputType = 'email';
    // if (this.data.InputType === 'email') {
    //   this.form
    //     .get('firstInput')
    //     .setValidators([Validators.required, Validators.email]);
    //   this.form.get('firstInput').updateValueAndValidity();
    // } else {
    //   this.form.get('firstInput').setValidators([Validators.required]);
    //   this.form.get('firstInput').updateValueAndValidity();
    // }
    this.changeInput('phone');
  }

  onSubmitEvent(): void {
    this.submit.emit({
      [this.data.InputName]: this.form.value.firstInput,
      type: this.data.InputName,
    });
  }

  changeInput(input): void {
    this.data.error = '';
    if (input === 'phone') {
      this.data.title = 'LOG_REG.LOGIN.TITLE2';
      this.data.InputName = 'email';
      this.data.InputType = 'email';
      this.data.InputPlaceholder = 'LOG_REG.LOGIN.firstInputPlaceholder1';
      this.data.InputIcon = 'email';
      this.form
        .get('firstInput')
        .setValidators([Validators.required, Validators.email]);
      this.form.get('firstInput').reset();
    } else {
      this.data.title = 'LOG_REG.LOGIN.TITLE1';
      this.data.InputName = 'phone';
      this.data.InputType = 'phone';
      this.data.InputPlaceholder = 'LOG_REG.LOGIN.firstInputPlaceholder';
      this.data.InputIcon = 'phone';
      this.form.get('firstInput').setValidators([Validators.required]);
      this.form.get('firstInput').reset();
    }
  }
}
