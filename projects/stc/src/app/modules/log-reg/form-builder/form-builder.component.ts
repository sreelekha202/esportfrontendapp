import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ViewChild,
} from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import {
  CountryISO,
  NgxIntlTelInputComponent,
  SearchCountryField,
} from 'ngx-intl-tel-input';
import { AuthServices } from '../../../core/service';
import {
  EsportsUserService,
  EsportsLanguageService,
  EsportsConstantsService,
  GlobalUtils,
  EsportsToastService
} from 'esports';
import { Country } from 'ngx-intl-tel-input/lib/model/country.model';
import { TranslateService } from '@ngx-translate/core';
import { environment } from '../../../../environments/environment';
import { ReCaptcha2Component } from 'ngx-captcha';

const userType = new Map([
  ['registration-game-center', 1],
  ['registration-sponser', 2],
  ['registration-organizer', 3],
]);
declare var $: any;

@Component({
  selector: 'app-form-builder',
  templateUrl: './form-builder.component.html',
  styleUrls: ['./form-builder.component.scss'],
})
export class FormBuilderComponent implements OnInit {
  @Output() dateChange: EventEmitter<MatDatepickerInputEvent<any>>;
  @Output() submit = new EventEmitter();
  @ViewChild('ngxTel') ngxTel: NgxIntlTelInputComponent;
  // @ViewChild('captchaElem') captchaElem: ReCaptcha2Component;
  @Input() data;
  @Input() fields;
  @Input() type;

  tempForm = {
    keepMeLoggedIn: [false],
    privacyPolicy: [false],
    dobPolicy: [false],
    // captcha: ['', Validators.required],
  };
  form = null;
  RECAPTCHA_V2_SITE_KEY = environment.RECAPTCHA_V2_SITE_KEY;
  isDisabled = true;
  base64textString = null;
  countries = [];
  states = [];
  newDate = new Date();
  todayDate: Date = new Date(
    new Date().setFullYear(new Date().getFullYear() - 10)
  );

  parentalDate: Date = new Date(
    new Date().setFullYear(new Date().getFullYear() - 18)
  );
  parental = false;
  isLoading = false;
  SearchCountryField = SearchCountryField;
  CountryISO = CountryISO;
  preferredCountries = [
    CountryISO.SaudiArabia,
    CountryISO.Bahrain,
    CountryISO.Kuwait,
  ];
  selectedCountryISO = CountryISO.SaudiArabia;
  mySelectedCountryISO: CountryISO;
  currentLang = this.ConstantsService?.defaultLangCode;
  constructor(
    private formBuilder: FormBuilder,
    private eSportsToastService: EsportsToastService,
    private translateService: TranslateService,
    private userService: EsportsUserService,
    public translate: TranslateService,
    public language: EsportsLanguageService,
    public ConstantsService: EsportsConstantsService,
    private authServices: AuthServices
  ) {}

  ngOnInit(): void {
    this.fetchCountries();
    
    for (let i = 0; i < this.fields.length; i++) {
      if (this.fields[i].formGroup) {
        for (let j = 0; j < this.fields[i].fields.length; j++) {
          const element = this.fields[i].fields[j];
          const validatorArray = [];
          if (element.name) {
            if (element.required) {
              validatorArray.push(Validators.required);
            }

            if (element.regex) {
              validatorArray.push(Validators.pattern(element.regex));
            }
            this.tempForm[element.name] = ['', validatorArray];
          } else if (element.formGroup) {
            for (
              let secoundInner = 0, secoundInnerLength = element.fields.length;
              secoundInner < secoundInnerLength;
              secoundInner++
            ) {
              const innerElement = element.fields[secoundInner];
              if (innerElement.required) {
                validatorArray.push(Validators.required);
              }

              if (innerElement.regex) {
                validatorArray.push(Validators.pattern(innerElement.regex));
              }
              this.tempForm[innerElement.name] = ['', validatorArray];
            }
          }
        }
      } else {
        const validatorArray = [];
        if (this.fields[i].required) {
          validatorArray.push(Validators.required);
        }

        if (this.fields[i].regex) {
          validatorArray.push(Validators.pattern(this.fields[i].regex));
        }
        this.tempForm[this.fields[i].name] = ['', validatorArray];
      }
    }
    this.form = this.formBuilder.group(this.tempForm);
    if (this.form.get('Email')) {
      this.form
        .get('Email')
        .setValidators([Validators.required, Validators.email]);
    }
    this.language.language.subscribe((lang) => {
      this.currentLang = lang;
      this.updateBootstrapSelect();
    });
  }

  onSubmitEvent(): void {
    // if (this.captchaElem) {
    //   this.captchaElem.resetCaptcha();
    // }
    this.submit.emit({
      ...this.form.value,
      logo: this.base64textString,
      email: this.form.value.Email,
      userType: userType.get(this.type) || null,
    });
  }

  fetchCountries = async () => {
    try {
      const data = await this.userService.getAllCountries().toPromise();
      this.countries = data.countries.filter((item) =>
        [191, 117, 17].includes(item.id)
      );
      this.updateBootstrapSelect();
    } catch (error) {
      this.eSportsToastService.showError(error?.message);
    }
  };
  sortByKey(array, key) {
    return array.sort(function (a, b) {
      var x = a[key];
      var y = b[key];
      return x < y ? -1 : x > y ? 1 : 0;
    });
  }

  async onChangeCountry(countryValue) {
    let countriesList = [];
    this.states = [];
    const that = this;
    const data = await this.userService.getAllCountries().toPromise();
    if (data) {
      countriesList = data.countries;
      const index = countriesList.findIndex((x) => x.name === countryValue);
      const stateData = await that.userService.getStates().toPromise();
      if (stateData) {
        stateData.states.forEach(function (value) {
          if (value.country_id == countriesList[index]?.id) {
            that.states.push(value);
          }
        });
        if (this.translateService.currentLang == 'ar') {
          that.states = this.sortByKey(that.states, 'ar');
        }
      }
    }
    that.updateBootstrapSelect();
  }

  onCountryCodeChange(e: Country) {
    this.mySelectedCountryISO = e.iso2 as CountryISO;
    this.onKeepLoggedIn();
  }

  private updateBootstrapSelect(): void {
    $('.refresh').selectpicker({
      noneSelectedText: this.translate.instant('NOTHING_SELECTED_TITLE'),
    });

    setTimeout(() => {
      $('.refresh').selectpicker('refresh');
    });
  }

  parentalCheckBox(date: any) {
    const selectedDate = new Date(date.value);
    if (selectedDate > this.parentalDate && selectedDate < this.todayDate) {
      this.parental = true;
      this.form.get('dobPolicy').setValidators(Validators.pattern('true'));
    } else {
      this.parental = false;
      this.form.get('dobPolicy').setValidators(Validators.pattern('false'));
    }
    this.form.get('dobPolicy').updateValueAndValidity();
  }

  onCountryChange(e: Country) {
    this.mySelectedCountryISO = e.iso2 as CountryISO;
    this.onKeepLoggedIn();
  }

  onKeepLoggedIn() {
    if (GlobalUtils.isBrowser()) {
      if (this.form.get('privacyPolicy').value) {
        const keepMeLoggedIn = {
          keepMeLoggedIn: true,
          mySelectedCountryISO:
            this.mySelectedCountryISO || this.preferredCountries[0],
        };
        localStorage.setItem('keepMeLoggedIn', JSON.stringify(keepMeLoggedIn));
      } else {
        localStorage.removeItem('keepMeLoggedIn');
      }
    }
  }

  //convert image to binary
  handleFileSelect(evt) {
    let files = evt.target.files;
    let file = files[0];

    if (files && file) {
      const idxDot = file.name.lastIndexOf('.') + 1;
      const extFile = file.name.substr(idxDot, file.name.length).toLowerCase();
      if (
        extFile == 'jpg' ||
        extFile == 'jpeg' ||
        extFile == 'png' ||
        extFile == 'gif'
      ) {
        const fileSize = file.size / 1024 / 1024;
        if (fileSize < 10) {
          let reader = new FileReader();
          reader.onload = this._handleReaderLoaded.bind(this);
          reader.readAsBinaryString(file);
        } else {
          this.eSportsToastService.showInfo(
            this.translateService.instant('LOG_REG.REGISTER.IMAGE_LIMIT_ERROR')
          );
        }
      } else {
        this.eSportsToastService.showError(
          this.translateService.instant('LOG_REG.REGISTER.INVALID_IMAGE_TYPE')
        );
      }
    }
  }
  _handleReaderLoaded(readerEvt) {
    let binaryString = readerEvt.target.result;
    this.base64textString = 'data:image/png;base64,' + btoa(binaryString);
  }

  resolved(token: string) {
    this.isLoading = true;
    this.authServices.validateCaptcha({ captcha: token }).subscribe(
      (data) => {
        this.form.get('captcha').setValue(token);
        this.isLoading = false;
      },
      (err) => {
        this.isLoading = false;
        if (err?.error?.message) {
          this.eSportsToastService.showError(err?.error?.message);
        }
      }
    );
  }

  handleReset() {
    this.form.get('captcha').setValue('');
  }
}
