import { Component, OnInit, ViewChildren } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthServices } from '../../../../core/service';
import {
  faLock,
  faPhone,
  faEye,
  faEyeSlash,
} from '@fortawesome/free-solid-svg-icons';
import { TranslateService } from '@ngx-translate/core';
import { environment } from '../../../../../environments/environment';
import { GlobalUtils, EsportsToastService } from 'esports';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  faLock = faLock;
  faPhone = faPhone;
  faEye = faEye;
  faEyeSlash = faEyeSlash;
  loginForm: FormGroup;
  confirmForm: FormGroup;
  newPasswordForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error = '';
  didFail = false;
  message = '';

  forgotOTP = false;
  login = true;
  newPassword = false;

  @ViewChildren('formRow') rows: any;

  timeLeft = 60;
  interval;

  fieldTextType = false;
  rfieldTextType = false;

  constructor(
    private formBuilder: FormBuilder,
    private cf: FormBuilder,
    private fb: FormBuilder,
    public eSportsToastService: EsportsToastService,
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthServices,
    public translate: TranslateService,
    public globalUtils: GlobalUtils
  ) {
    if (
      GlobalUtils.isBrowser() &&
      localStorage.getItem(environment.currentToken)
    ) {
      this.router.navigate(['/home']);
    }
  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      phoneno: [
        '',
        Validators.compose([
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(100),
        ]),
      ],
      password: [
        '',
        Validators.compose([
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(100),
        ]),
      ],
    });

    this.confirmForm = this.cf.group({
      code1: ['', Validators.compose([Validators.required])],
      code2: ['', Validators.compose([Validators.required])],
      code3: ['', Validators.compose([Validators.required])],
      code4: ['', Validators.compose([Validators.required])],
    });

    this.newPasswordForm = this.fb.group({
      password: [
        '',
        Validators.compose([
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(100),
        ]),
      ],
      rpassword: [
        '',
        Validators.compose([
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(100),
        ]),
      ],
    });

    // get return url from route parameters or default to '/'
    // this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    this.returnUrl = this.authService.redirectUrl || '/';
  }

  onSubmit(value) {
    if (GlobalUtils.isBrowser()) {
      const controls = this.loginForm.controls;
      // check form
      if (this.loginForm.invalid) {
        Object.keys(controls).forEach((controlName) =>
          controls[controlName].markAsTouched()
        );
        return;
      }
      this.loading = true;
      const login = {
        password: value.password,
        phoneNumber: '+' + value.phoneno,
        type: 'phone',
      };
      // this.authService
      //   .login(login)
      //   .pipe(first())
      //   .subscribe(
      //     (data) => {
      //       if (data.data.firstLogin == 1) {
      //         this.router.navigate(['/profile']);
      //       } else {
      //         this.router.navigate([this.returnUrl]);
      //       }
      //     },
      //     (error) => {
      //       this.didFail = true;
      //       this.message = error.error.message;
      //       this.loading = false;
      //     }
      //   );

      this.authService.login(login).subscribe(
        (user) => {
          if (user.code == 'ER1001') {
          } else {
            // store user details and jwt token in local storage to keep user logged in between page refreshes
            localStorage.setItem(environment.currentToken, user.data.token);
            if (user.data.firstLogin == 1) {
              this.router.navigate(['/profile']);
            } else {
              this.router.navigate([this.returnUrl]);
            }
          }
        },
        (error) => {
          this.didFail = true;
          this.message = error.error.message;
          this.loading = false;
        }
      );
    }
  }

  onPasswordSubmit(value) {
    const controls = this.newPasswordForm.controls;
    // check form
    if (this.newPasswordForm.invalid) {
      Object.keys(controls).forEach((controlName) =>
        controls[controlName].markAsTouched()
      );
      return;
    } else if (controls.password.value !== controls.rpassword.value) {
      this.didFail = true;
      this.message = "password doesn't match";
      return;
    } else {
      this.didFail = false;
      this.message = '';
      const obj = this.confirmForm.value;
      const code = obj.code1 + obj.code2 + obj.code3 + obj.code4;
      let data = {
        type: 'update',
        phoneOTP: code,
        phoneNumber: '+' + this.loginForm.controls.phoneno.value,
        password: value.password,
      };
      this.authService.forgotPassword(data).subscribe(
        (user) => {
          this.login = false;
          this.didFail = false;
          this.message = '';
          this.eSportsToastService.showSuccess(user.message);
          this.router
            .navigateByUrl('/register', { skipLocationChange: true })
            .then(() => {
              this.router.navigate(['/login']);
            });
        },
        (error) => {
          this.didFail = true;
          this.message = error.error.message;
          this.loading = false;
        }
      );
    }
  }

  forgot_password() {
    const controls = this.loginForm.controls;
    if (controls.phoneno.status == 'INVALID') {
      controls.phoneno.markAsTouched();
      return;
    }
    let data = {
      type: 'phone',
      phoneNumber: '+' + controls.phoneno.value,
    };
    this.authService.forgotPassword(data).subscribe(
      (user) => {
        this.login = false;
        this.didFail = false;
        this.message = '';
        this.forgotOTP = true;
        this.interval = setInterval(() => {
          if (this.timeLeft > 0) {
            this.timeLeft--;
          }
          // else {
          //   this.timeLeft = 60;
          // }
        }, 1000);
      },
      (error) => {
        this.didFail = true;
        this.message = error.error.message;
        this.loading = false;
      }
    );
  }

  keyUpEvent(event, index) {
    let pos = index;
    if (event.keyCode === 8 && event.which === 8) {
      pos = index - 1;
    } else {
      pos = index + 1;
    }
    if (pos > -1 && pos < 4) {
      this.rows._results[pos].nativeElement.focus();
    }
  }
  onConfirm(value) {
    const controls = this.confirmForm.controls;
    /** check form */
    if (this.confirmForm.invalid) {
      Object.keys(controls).forEach((controlName) =>
        controls[controlName].markAsTouched()
      );
      this.didFail = true;
      this.message = 'Insert valid data';
      return;
    }

    this.didFail = false;
    this.message = '';
    this.loading = true;

    const obj = this.confirmForm.value;
    const code = obj.code1 + obj.code2 + obj.code3 + obj.code4;

    let data = {
      phoneNumber: '+' + this.loginForm.value.phoneno,
      type: 'phone_otp',
      phoneOTP: code,
    };
    this.authService.forgotPassword(data).subscribe(
      (data) => {
        this.didFail = false;
        this.message = '';
        this.forgotOTP = false;
        this.newPassword = true;
        this.loading = false;
      },
      (error) => {
        this.loading = false;
        this.didFail = true;
        this.message = error.error.message;
      }
    );
  }
  resendOtp() {
    let data = {
      phoneNumber: '+' + this.loginForm.value.phoneno,
      type: 'forgot_phone',
    };
    this.authService.resendOTP(data).subscribe(
      (data) => {
        this.didFail = false;
        this.message = '';
        this.loading = false;
        this.timeLeft = 60;
      },
      (error) => {
        this.loading = false;
        this.didFail = true;
        this.message = error.error.message;
      }
    );
  }

  isControlHasError(controlName: string, validationType: string): boolean {
    const control = this.loginForm.controls[controlName];
    if (!control) {
      return false;
    }

    const result =
      control.hasError(validationType) && (control.dirty || control.touched);
    return result;
  }

  isPasswordControlHasError(
    controlName: string,
    validationType: string
  ): boolean {
    const control = this.newPasswordForm.controls[controlName];
    if (!control) {
      return false;
    }

    const result =
      control.hasError(validationType) && (control.dirty || control.touched);
    return result;
  }

  toggleFieldTextType() {
    this.fieldTextType = !this.fieldTextType;
  }
  rtoggleFieldTextType() {
    this.rfieldTextType = !this.rfieldTextType;
  }
}
