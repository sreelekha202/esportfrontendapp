import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import {
  CountryISO,
  NgxIntlTelInputComponent,
  SearchCountryField,
  PhoneNumberFormat,
} from 'ngx-intl-tel-input';
import { faFacebookF, faTwitter } from '@fortawesome/free-brands-svg-icons';
import { Country } from 'ngx-intl-tel-input/lib/model/country.model';
import { faEnvelope } from '@fortawesome/free-solid-svg-icons';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import {
  AppHtmlRoutes,
  AppHtmlRoutesLoginType,
} from '../../../app-routing.model';
import { AuthServices } from '../../../core/service';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { TranslateService } from '@ngx-translate/core';
import { take } from 'rxjs/operators';
import { environment } from '../../../../environments/environment';
import { DatePipe, LowerCasePipe } from '@angular/common';
import {
  EsportsLanguageService,
  GlobalUtils,
  EsportsToastService,
  EsportsGtmService,
  SuperProperties,
} from 'esports';

export interface UpdateFormComponentOutputData {
  [key: string]: string;
}

export interface UpdateInfoFormComponentDataItem {
  title: string;
  subtitle: string;
  submitBtn: string;
  firstInputName: string;
  firstInputType: string;
  firstInputPlaceholder: string;
  firstInputIcon: string;
  secondInputName: string;
  secondInputType: string;
  secondInputPlaceholder: string;
  secondInputIcon: string;
  thirdInputName: string;
  thirdInputType: string;
  thirdInputPlaceholder: string;
  thirdInputIcon: string;
  error?: string;
  data: any;
}

declare var $: any;

@Component({
  selector: 'app-update-form',
  templateUrl: './update-form.component.html',
  styleUrls: ['./update-form.component.scss']
})
export class UpdateFormComponent implements OnInit {

  @Output() dateChange: EventEmitter<MatDatepickerInputEvent<any>>;
  @ViewChild('ngxTel') ngxTel: NgxIntlTelInputComponent;
  @Output() submit = new EventEmitter();
  @Input() data: UpdateInfoFormComponentDataItem;
  LANGUAGE = 'en';
  showToast: boolean;
  timeoutId = null;
  @Input() set type(type: AppHtmlRoutesLoginType) {
    this.activeTypeValue = type;
    if (this.activeTypeValue === AppHtmlRoutesLoginType.infoUpdate) {
      this.form
        .get('firstInput')
        .setValidators([
          Validators.required,
          Validators.minLength(3)
        ]);
        this.form
        .get('secondInput')
        .setValidators([Validators.required, Validators.email]);

        this.form.get('thirdInput').setValidators([Validators.required]);
      // this.form.get('dobPolicy').setValidators(Validators.pattern('false'));
      // this.form.get('dobPolicy').updateValueAndValidity();
    }
  }

  AppHtmlRoutes = AppHtmlRoutes;
  AppHtmlRoutesLoginType = AppHtmlRoutesLoginType;
  activeTypeValue: AppHtmlRoutesLoginType;

  form = this.formBuilder.group({
    firstInput: ['', Validators.required],
    secondInput: ['', Validators.required],
    thirdInput: ['', Validators.required],
  });

  todayDate: Date = new Date(
    new Date().setFullYear(new Date().getFullYear() - 10)
  );
  parentalDate: Date = new Date(
    new Date().setFullYear(new Date().getFullYear() - 18)
  );
  parental = false;
  isExist = false;

  faFacebookF = faFacebookF;
  faTwitter = faTwitter;
  faEnvelope = faEnvelope;
  isLoading = false;
  SearchCountryField = SearchCountryField;
  PhoneNumberFormat = PhoneNumberFormat;
  CountryISO = CountryISO;
  preferredCountries: CountryISO[] = [
    CountryISO.SaudiArabia,
    CountryISO.Bahrain,
    CountryISO.Kuwait,
    // CountryISO.India,
  ];
  selectedCountryISO = CountryISO.SaudiArabia;
  mySelectedCountryISO: CountryISO;

  constructor(
    private formBuilder: FormBuilder,
    private translateService: TranslateService,
    private router: Router,
    public eSportsToastService: EsportsToastService,
    private authServices: AuthServices,
    private languageService: EsportsLanguageService,
    private gtmService: EsportsGtmService,
    private datePipe: DatePipe
  ) {
    //this.checkForKeepMeLoggedIn();
  }

  ngOnInit() {
    this.showToast = true;
    this.getCurrentLang();
  }

  isUniqueName = async (name) => {
    try {
      const isAvailable = async () => {
        try {
          const response = await this.authServices
            .searchUsername(name)
            .toPromise();
          this.isExist = response.data?.isExist;
          if (this.isExist) {
            this.form.controls['fifthInput'].setErrors({
              incorrect: true,
            });
          } else {
            this.form.controls['fifthInput'].setErrors(null);
            this.form.controls['fifthInput'].setValidators([
              Validators.required,
              Validators.minLength(3),
              Validators.pattern(/^[a-zA-Z0-9_]+$/),
            ]);

            this.form.controls['fifthInput'].updateValueAndValidity();
          }
        } catch (error) {
          throw error;
        } finally {
          clearTimeout(this.timeoutId);
        }
      };
      if (!this.timeoutId) {
        this.timeoutId = setTimeout(isAvailable, 500);
      } else {
        clearTimeout(this.timeoutId);
        this.timeoutId = setTimeout(isAvailable, 500);
      }
    } catch (error) {
      this.eSportsToastService.showError(
        error?.error?.message || error?.message
      );
    }
  };

  getCurrentLang() {
    this.languageService.language.subscribe((lang) => {
      this.LANGUAGE = lang || 'en';
    });
  }

  onCountryChange(e: Country) {
    this.mySelectedCountryISO = e.iso2 as CountryISO;
    this.onKeepLoggedIn();
  }

  onKeepLoggedIn() {
    if (GlobalUtils.isBrowser()) {
      if (this.form.get('privacyPolicy').value) {
        const keepMeLoggedIn = {
          keepMeLoggedIn: true,
          mySelectedCountryISO:
            this.mySelectedCountryISO || this.preferredCountries[0],
        };
        localStorage.setItem('keepMeLoggedIn', JSON.stringify(keepMeLoggedIn));
      } else {
        localStorage.removeItem('keepMeLoggedIn');
      }
    }
  }

  onSubmitEvent(): void {
    this.submit.emit({
      [this.data.firstInputName]: this.form.value.firstInput.e164Number,
      [this.data.secondInputName]: this.form.value.secondInput,
      [this.data.thirdInputName]: this.datePipe.transform(this.form.value.thirdInput, 'YYYY-MM-dd'),
      firstInputType: this.data.firstInputName,
      secondInputType: this.data.secondInputName,
      thirdInputType: this.data.thirdInputName,
      data: this.form.value,
    });
  }

  private checkForKeepMeLoggedIn(): void {
    if (GlobalUtils.isBrowser()) {
      const keepMeLoggedIn = localStorage.getItem('keepMeLoggedIn');
      if (keepMeLoggedIn) {
        const parsedKeepMeLoggedIn = JSON.parse(keepMeLoggedIn);
        if (parsedKeepMeLoggedIn) {
          this.form
            .get('privacyPolicy')
            .setValue(parsedKeepMeLoggedIn.keepMeLoggedIn);
          this.selectedCountryISO = parsedKeepMeLoggedIn.mySelectedCountryISO;
        }
      }
    }
  }

  toast() {
    if (this.showToast == true) {
      if (this.activeTypeValue === this.AppHtmlRoutesLoginType.registration) {
        this.showToast = false;
        //this.test = this.registrationStep;
        this.translateService
          .get('LOG_REG.ALERTS.TEN_YEARS')
          .pipe(take(1))
          .subscribe((text: string) => {
            // this.eSportsToastService.showError(text);
          });
      }
    }
  }
}
