import {
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import { SubmitFormComponentOutputData } from '../submit-form/submit-form.component';
import { AppHtmlRoutesLoginType } from '../../../app-routing.model';
import { AuthServices } from '../../../core/service';

export interface VerificationFormComponentData {
  title: string;
  subtitle: string;
  footerTitle: string;
  footerLinkTitle: string;
  timerMs: number;
  submitBtn: string;
  error?: string;
}

@Component({
  selector: 'app-verification-form',
  templateUrl: './verification-form.component.html',
  styleUrls: ['./verification-form.component.scss'],
})
export class VerificationFormComponent implements OnInit, OnDestroy {
  @Output() submit = new EventEmitter();
  @Input() formValue: SubmitFormComponentOutputData;
  @Input() data: VerificationFormComponentData;
  @Input() set type(type: AppHtmlRoutesLoginType) {
    this.activeTypeValue = type;
  }

  AppHtmlRoutesLoginType = AppHtmlRoutesLoginType;
  activeTypeValue: AppHtmlRoutesLoginType;

  code: string;
  codeEntered: boolean;
  interval;

  constructor(private authService: AuthServices) {}

  ngOnInit(): void {
    this.start();
    this.data.error = "";
  }

  ngOnDestroy(): void {
    if (this.interval) {
      clearInterval(this.interval);
    }
  }

  start(): void {
    this.interval = setInterval(() => {
      if (!(this.data.timerMs - 1)) {
        clearInterval(this.interval);
      }
      this.data.timerMs--;
    }, 1000);
  }

  onOtpChange(code: string): void {
    this.codeEntered = code.length === 6;

    if (this.codeEntered) {
      this.code = code;
    }
  }

  onSubmitEvent(): void {
    this.submit.emit({
      otp: this.code,
      formValue: this.formValue,
    });
  }

  resendOtp(type) {
    if (this.activeTypeValue === AppHtmlRoutesLoginType.emailLogin) {
      const queryData = {
        phoneNumber: this.formValue.mailPhone,
        type: 'phone',
      };
      this.data.error = '';
      this.authService.resendOTP(queryData).subscribe(
        (data) => {
          this.data.timerMs = 600;
          this.start();
        },
        (error) => {
          this.data.error = error?.error?.message;
        }
      );
    }
    if (this.activeTypeValue === AppHtmlRoutesLoginType.registration) {
      let data;
      if (type === 'email') {
        data = {
          email: this.formValue.mailPhone,
          type: 'email',
        };
      }
      else if (type === 'phone') {
        data = {
          phoneNumber: this.formValue.mailPhone,
          type: 'phone',
        };
      } else {
        data = {
          phoneNumber: this.formValue.mailPhone,
          type: 'phone',
        };
      }
      // let data = {
      //   email: this.firstStepForm.value.emailId,
      //   type: 'email',
      // };
      this.data.error = '';
      this.authService.resendOTP(data).subscribe(
        (authData) => {
          this.data.timerMs = 600;
          this.start();
        },
        (error) => {
          this.data.error = error?.error?.message;
        }
      );
    }
    if (this.activeTypeValue === AppHtmlRoutesLoginType.forgotPass) {
      let data;
      if (this.formValue.formValue['type'] === 'email') {
        data = {
          email: this.formValue.mailPhone,
          type: 'email',
        };
      }
      if (this.formValue.formValue['type'] === 'phone') {
        data = {
          phoneNumber: this.formValue.mailPhone,
          type: 'phone',
        };
      }
      this.data.error = '';
      this.authService.resendOTP(data).subscribe(
        (data) => {
          this.data.timerMs = 600;
          this.start();
        },
        (error) => {
          this.data.error = error?.error?.message;
        }
      );
    }
    if (this.activeTypeValue === AppHtmlRoutesLoginType.registrationAsGameCenter ||
      this.activeTypeValue === AppHtmlRoutesLoginType.registrationAsOrganizer ||
      this.activeTypeValue === AppHtmlRoutesLoginType.registrationAsSponser) {
      let data;
      data = {
        phoneNumber: this.formValue.mailPhone,
        type: 'phone',
      };
      this.data.error = '';
      this.authService.resendOTP(data).subscribe(
        (data) => {
          this.data.timerMs = 600;
          this.start();
        },
        (error) => {
          this.data.error = error?.error?.message;
        }
      );
    }
    if (this.formValue.type === 'add_email') {
      let data;

      data = {
        email: this.formValue.mailPhone,
        type: 'email',
      };

      this.data.error = '';
      this.authService.resendOTP(data).subscribe(
        (data) => {
          this.data.timerMs = 600;
          this.start();
        },
        (error) => {
          this.data.error = error?.error?.message;
        }
      );
    }
    if (this.formValue.type === 'add_phone') {
      let data;

      data = {
        phoneNumber: this.formValue.mailPhone,
        type: 'phone',
      };

      this.data.error = '';
      this.authService.resendOTP(data).subscribe(
        (data) => {
          this.data.timerMs = 600;
          this.start();
        },
        (error) => {
          this.data.error = error?.error?.message;
        }
      );
    }
  }
}
