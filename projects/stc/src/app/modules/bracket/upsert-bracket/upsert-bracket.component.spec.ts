import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpsertBracketComponent } from './upsert-bracket.component';

describe('UpsertBracketComponent', () => {
  let component: UpsertBracketComponent;
  let fixture: ComponentFixture<UpsertBracketComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UpsertBracketComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpsertBracketComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
