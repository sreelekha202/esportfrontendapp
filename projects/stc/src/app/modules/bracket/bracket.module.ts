import { SharedModule } from '../../shared/modules/shared.module';
import { BracketRoutingModule } from './bracket-routing.module';
import { NgModule } from '@angular/core';

import { UpsertBracketComponent } from './upsert-bracket/upsert-bracket.component';
import { BracketPreviewComponent } from './bracket-preview/bracket-preview.component';
import { BracketComponent } from './bracket.component';
import { FormComponentModule } from '../../shared/components/form-component/form-component.module';

@NgModule({
  declarations: [
    UpsertBracketComponent,
    BracketComponent,
    BracketPreviewComponent,
  ],
  imports: [BracketRoutingModule, SharedModule, FormComponentModule],
})
export class BracketModule {}
