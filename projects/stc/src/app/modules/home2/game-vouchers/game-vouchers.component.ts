import { Component, OnDestroy, OnInit } from '@angular/core';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';

@AutoUnsubscribe()
@Component({
  selector: 'app-game-vouchers',
  templateUrl: './game-vouchers.component.html',
  styleUrls: ['./game-vouchers.component.scss'],
})
export class GameVouchersComponent implements OnInit, OnDestroy {
  constructor() {}

  ngOnInit(): void {}
  ngOnDestroy() {}
}
