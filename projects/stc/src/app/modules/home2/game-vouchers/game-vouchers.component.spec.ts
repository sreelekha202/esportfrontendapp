import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GameVouchersComponent } from './game-vouchers.component';

describe('GameVouchersComponent', () => {
  let component: GameVouchersComponent;
  let fixture: ComponentFixture<GameVouchersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GameVouchersComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GameVouchersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
