import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ChangeDetectionStrategy,
  OnDestroy,
} from '@angular/core';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';

@AutoUnsubscribe()
@Component({
  selector: 'app-latest-article',
  templateUrl: './latest-article.component.html',
  styleUrls: ['./latest-article.component.scss', '../home2.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LatestArticleComponent implements OnInit, OnDestroy {
  constructor() {}
  @Input() latestArticle: any;
  @Output() callBack = new EventEmitter();
  ngOnInit(): void {}
  ngOnDestroy() {}
  addLatestArticleGtmTag(e) {
    this.callBack.emit({ type: 'add', data: e });
  }
  addViewMoreArticleGTM(e) {
    this.callBack.emit({ type: 'viewMore', data: e });
  }
}
