import {
  Component,
  OnDestroy,
  OnInit,
  Inject,
  PLATFORM_ID,
  Renderer2,
} from '@angular/core';
import { GameService } from 'projects/stc/src/app/core/service';
import {
  VideoLibraryService,
  UtilsService,
  HomeService,
  TournamentService,
  ArticleApiService,
  AuthServices,
} from '../../core/service';
import {
  InfoPopupComponent,
  InfoPopupComponentData,
  InfoPopupComponentType,
} from '../../shared/popups/info-popup/info-popup.component';
import { MatDialog } from '@angular/material/dialog';
import { AppHtmlRoutes } from '../../app-routing.model';
import { TranslateService } from '@ngx-translate/core';
import {
  IUser,
  EsportsUserService,
  EsportsLanguageService,
  EsportsToastService,
  GlobalUtils,
  SuperProperties,
  EventProperties,
  EsportsGtmService,
  EsportsTimezone,
} from 'esports';
import { Subscription } from 'rxjs';
import { ScriptLoadingService } from '../../core/service/script-loading.service';
import { isPlatformBrowser } from '@angular/common';
import { environment } from '../../../environments/environment';
import { FireProductService } from '../../../app/shared/service/fire-product.service';
import isURL from 'validator/es/lib/isURL';
import { Router } from '@angular/router';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';

declare const zE;
const API = environment.apiEndPoint;

@AutoUnsubscribe()
@Component({
  selector: 'app-home2',
  templateUrl: './home2.component.html',
  styleUrls: ['./home2.component.scss'],
})
export class Home2Component implements OnInit, OnDestroy {
  user: IUser;
  isBrowser: boolean;
  AppHtmlRoutes = AppHtmlRoutes;
  postData;
  trendingPosts;
  isAnnouncementAvailabale = false;
  announcementData: any;

  videoTitle: any = [];
  articleTitle: any = [];

  videoLibrary: Array<{}> = [];
  activeVideo = null;
  tournamentsfor3wb = [];
  tournamentsData = [];
  gwbTouranmentData = [];
  saudi_E_League = [];

  currentData = [];
  promoAmount;

  featureTournamentsData = [];

  ongoingTournamentsData = [];
  turkiTournaments = [];

  tournamentsSlides = [];
  latestArticle = [];

  data: any = [];
  devices = [];
  topPlayers = [];

  highlights = [];

  leaderBoardRows = [];
  leaderBoardColumns = [
    { name: 'RANK' },
    { name: 'Player Name' },
    // { name: 'GAME' },
    { name: 'REGION' },
    { name: 'TROPHIES' },
    { name: 'POINTS' },
  ];
  allTournamentsData = [
    { gameName: 'PUBG' },
    { gameName: 'CS:GO' },
    { gameName: 'Fortnite' },
    { gameName: 'Call Of Duty' },
  ];
  gameName = ['عيشها بلا حدود'];
  games = [
    {
      image: 'assets/images/Gwb/all-tournaments.png',
      name: 'GWB.COMPONENTS.TOURNAMENTS.ALL_TOURNAMENTS',
      isSelected: false,
    },
  ];
  timezone;
  isShowParticipants;
  userSubscription: Subscription;
  listener;
  currentLanguage: string = 'arabic';

  constructor(
    private gameService: GameService,
    private eSportsToastService: EsportsToastService,
    private videoLibraryService: VideoLibraryService,
    public utilsService: UtilsService,
    private authService: AuthServices,
    private homeService: HomeService,
    private tournamentService: TournamentService,
    public language: EsportsLanguageService,
    public matDialog: MatDialog,
    public translateService: TranslateService,
    public userService: EsportsUserService,
    private articleService: ArticleApiService,
    public languageService: EsportsLanguageService,
    private scriptLoadingService: ScriptLoadingService,
    private fireProductService: FireProductService,
    private esportsTimezone: EsportsTimezone,
    private gtmService: EsportsGtmService,
    private router: Router,
    private _renderer: Renderer2,
    @Inject(PLATFORM_ID) private platformId
  ) {
    this.isBrowser = isPlatformBrowser(platformId);
  }

  ngOnInit(): void {
    this.timezone = this.esportsTimezone.getTimeZoneName();
    this.getCurrentUserDetails();
    this.getGames();
    this.getTurkiTournaments({
      status: '12',
      limit: 8,
      page: 1,
    });
    if (GlobalUtils.isBrowser()) {
      this.addIframeListener();
    }

    this.languageService.language
      .subscribe((lang) => {
        this.currentLanguage = lang === 'en' ? 'english' : 'arabic';
      })
      .unsubscribe();
    // this.getProductData();
  }

  isNumber(val): boolean {
    const str: string = val.toString();
    return str.startsWith('+');
  }

  ngOnDestroy() {
    this.userSubscription?.unsubscribe();
    if (GlobalUtils.isBrowser()) {
      window.removeEventListener('blur', this.listener);
    }
  }

  getHottestPost() {
    this.homeService.hottest_post().subscribe((data) => {});
  }
  getRecentPost() {
    const pagination = JSON.stringify({ limit: 8, sort: { createdDate: -1 } });
    const query = JSON.stringify({
      articleStatus: 'publish',
    });
    const perfernce = JSON.stringify({
      prefernce: this.user?.preference?.game
        ? this.user?.preference?.game.map((item) => {
            return { gameDetails: item };
          })
        : '',
    });
    this.articleService
      .getLatestArticle({
        pagination,
        query,
        preference: perfernce,
      })
      .subscribe(
        (res: any) => {
          this.latestArticle = res.data.docs;
          this.articleTitle.push(this.latestArticle[0]);
        },
        (err) => {}
      );
  }

  getProductData() {
    this.fireProductService.getProducts().subscribe((res) => {
      if (res && res.data && res.data.length > 0) {
        this.devices = res.data;
        this.devices.forEach((device) => {
          if (
            !isURL(device.url, {
              protocols: ['http', 'https', 'ftp'],
              require_tld: true,
              require_protocol: false,
              require_host: true,
              require_port: false,
              require_valid_protocol: true,
              allow_underscores: false,
              allow_trailing_dot: false,
              allow_protocol_relative_urls: false,
              validate_length: true,
            })
          ) {
            device.url = '../404';
          }
        });
      } else {
        this.devices = [];
      }
    });
  }
  /**
   * method to call the announcement data
   */
  getAnnouncement() {
    this.homeService.getAnnouncements().subscribe(
      (res: any) => {
        if (res.data && res.data.length > 0) {
          const data = res.data.map((ele) => {
            return {
              uniqueId: ele._id,
              image: ele.announcementFileUrl,
              header: ele.header,
              description: ele.description,
              destination: ele.destination,
            };
          });
          data[0].description = this.replaceCommaLine(data[0].description);
          this.announcementData = data[0];
          if (this.announcementData.uniqueId) {
            if (GlobalUtils.isBrowser()) {
              const itemId = localStorage.getItem('announcement');
              if (itemId != this.announcementData.uniqueId) {
                this.isAnnouncementAvailabale = true;
              } else {
                this.isAnnouncementAvailabale = false;
              }
            }
          }
        }
      },
      (err: any) => {}
    );
  }

  replaceCommaLine(data) {
    //convert string to array and remove whitespace
    let dataToArray = data.split('\\n').map((item) => item.trim());
    //convert array to string replacing comma with new line
    return dataToArray.join('<br>');
  }

  getLeaderBoard() {
    this.homeService._leaderBoard().subscribe((data) => {
      this.leaderBoardRows = data.data.leaderboardData;
      if (this.leaderBoardRows) {
        this.leaderBoardRows.forEach((obj, index) => {
          obj.rank = index + 1;
        });
      }
    });
  }

  fetchVideoLibrary = async () => {
    try {
      const pagination = {
        page: 1,
        limit: 3,
        sort: '-updatedOn',
        projection: [
          '_id',
          'title',
          'description',
          'youtubeUrl',
          'thumbnailUrl',
          'createdBy',
        ],
      };
      const query = `?pagination=${encodeURIComponent(
        JSON.stringify(pagination)
      )}`;

      const response = await this.videoLibraryService.fetchVideoLibrary(query);
      this.videoLibrary = await Promise.all(
        response?.data?.docs.map(async (item) => {
          this.videoTitle.push(item);
          return {
            ...item,
            youtubeUrl: await this.utilsService.getEmbbedUrl(item.youtubeUrl),
          };
        })
      );
      this.activeVideo = this.videoLibrary.length ? this.videoLibrary[0] : null;
    } catch (error) {
      //console.error(error?.error?.message && error?.message);
    }
  };

  getBanner() {
    this.homeService._getBanner().subscribe(
      (res: any) => {
        this.tournamentsSlides = res.data;
      },
      (err: any) => {}
    );
  }

  getTopPlayers() {
    this.homeService._topPlayers().subscribe(
      (res: any) => {
        this.topPlayers = res.data.map((ele) => {
          return {
            fullName: ele?.fullName,
            photo: ele?.profilePicture
              ? ele?.profilePicture
              : './assets/images/Profile/stc_avatar.png',
          };
        });
      },
      (err: any) => {}
    );
  }
  getTournament() {
    // let query: any = {
    //   $and: [{ tournamentStatus: 'publish' }],
    //   $or: [{ startDate: { $lt: new Date() } }, { isSeeded: true }],
    // };
    // query = JSON.stringify(query);

    this.tournamentService
      .getPaginatedTournaments({
        status: '0',
        // need to status change
        limit: 6,
        sort: 'participantJoined',
        prefernce: '',
      })
      .subscribe(
        (res: any) => {
          // this.showLoader = false;
          this.tournamentsData = res.data.docs;
        },
        (err) => {
          // this.showLoader = false;
        }
      );
  }
  getGWBTournaments() {
    this.tournamentService
      .getPaginatedTournaments({
        status: '0',
        // need to status change
        limit: 6,
        sort: 'participantJoined',
        level2: true,
      })
      .subscribe(
        (res: any) => {
          this.gwbTouranmentData = res?.data?.docs;
        },
        (err) => {
          // this.showLoader = false;
        }
      );
  }
  getSaudi_ELeague() {
    this.tournamentService
      .getPaginatedTournaments({
        status: '7',
        // need to status change
        limit: 6,
        sort: 'participantJoined',
        prefernce: '',
      })
      .subscribe(
        (res: any) => {
          // this.showLoader = false;
          this.saudi_E_League = res.data.docs;
        },
        (err) => {
          // this.showLoader = false;
        }
      );
  }

  getCurrentTournament() {
    this.tournamentService
      .getPaginatedTournaments({
        status: '1',
        limit: 6,
        prefernce: this.user?.preference?.game
          ? this.user?.preference?.game.map((item) => {
              return { gameDetail: item };
            })
          : '',
      })
      .subscribe(
        (res: any) => {
          // this.showLoader = false;
          this.currentData = res.data.docs;
        },
        (err) => {
          // this.showLoader = false;
        }
      );
  }

  getOnGoingTournaments() {
    this.tournamentService
      .getPaginatedTournaments({
        status: '6',
        limit: 5,
        sort: 'participantJoined',
      })
      .subscribe(
        (res: any) => {
          this.featureTournamentsData = res.data.docs;
        },
        (err) => {
          // this.showLoader = false;
        }
      );
  }

  getRegistrationStatus(
    startDate,
    startTime,
    isSeeded,
    isFinished,
    maxParticipants,
    participantsJoined
  ) {
    if (!startDate || !startTime) {
      return;
    }
    const timeObj = this.convert12HrsTo24HrsFormat(startTime);
    const sDate = new Date(startDate);
    sDate.setHours(timeObj.hour);
    sDate.setMinutes(timeObj.minute);
    if (isSeeded == true || isFinished == true) {
      return 'Closed';
    } else if (participantsJoined >= maxParticipants) {
      return 'Closed';
    } else if (sDate.getTime() < Date.now()) {
      return 'Closed';
    } else {
      return 'Open';
    }
  }

  convert12HrsTo24HrsFormat(time) {
    if (!time) {
      return;
    }
    let hours = Number(time.match(/^(\d+)/)[1]);
    const minutes = Number(time.match(/:(\d+)/)[1]);
    let AP = time.match(/\s(.*)$/);
    if (!AP) {
      AP = time.slice(-2);
    } else {
      AP = AP[1];
    }
    if (AP == 'PM' && hours < 12) {
      hours = hours + 12;
    }
    if (AP == 'AM' && hours == 12) {
      hours = hours - 12;
    }
    let Hours24 = hours.toString();
    let Minutes24 = minutes.toString();
    if (hours < 10) {
      Hours24 = '0' + Hours24;
    }
    if (minutes < 10) {
      Minutes24 = '0' + Minutes24;
    }

    return { hour: parseInt(Hours24), minute: parseInt(Minutes24) };
  }

  openSocialShareComponent() {
    const data: InfoPopupComponentData = {
      title: this.translateService.instant('ARTICLE_POST.SHARE_TOURNAMENT'),
      text: ``,
      type: InfoPopupComponentType.socialSharing,
      cancelBtnText: 'Close',
    };
    this.matDialog.open(InfoPopupComponent, {
      data,
    });
  }

  promoNotify() {
    this.authService.get_promo('eidsar20').subscribe((res: any) => {
      const promoAmount = res.data.promoData.priceAmount;
      this.userService.promoNotify(API).subscribe(
        (res2: any) => {
          if (res2?.data?.toNotify) {
            // show notification of successful reward transaction
            this.eSportsToastService.showPromo(
              this.translateService
                .instant('EIDPROMO.REWARD_SUCCESS')
                .replace('20', promoAmount)
            );
          }
        },
        (err: any) => {}
      );
    });
  }

  getCurrentUserDetails() {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.user = data;
        this.getAnnouncement();
        this.fetchVideoLibrary();
        this.getTournament();
        this.getGWBTournaments();
        this.getCurrentTournament();
        this.getOnGoingTournaments();
        // this.getHottestPost();
        this.getSaudi_ELeague();
        this.getRecentPost();
        this.getBanner();
        this.getTopPlayers();
        this.getLeaderBoard();
        this.promoNotify();
        this.getShowParticipantsStatus();
      } else {
        this.getAnnouncement();
        this.fetchVideoLibrary();
        this.getTournament();
        this.getCurrentTournament();
        this.getOnGoingTournaments();
        // this.getHottestPost();
        this.getGWBTournaments();
        this.getSaudi_ELeague();
        this.getRecentPost();
        this.getBanner();
        this.getTopPlayers();
        this.getLeaderBoard();
        this.getShowParticipantsStatus();
      }
      // this.loadZendesk();
      this.languageService.language.subscribe((lang: string) => {
        this.loadZendesk(lang);
      });
    });
  }

  loadZendesk(lang) {
    this.translateService.use(lang);
    const isEnglish = lang === 'en';
    const language = isEnglish ? 'en' : 'ar';
    let zenDeskUrl =
      'https://static.zdassets.com/ekr/snippet.js?key=073f03b8-0c38-4a51-beaf-dab96dc8177c';
    this.scriptLoadingService.registerScript(zenDeskUrl, 'zE', (res) => {
      zE('webWidget', 'setLocale', language);
      zE('webWidget', 'updateSettings', {
        webWidget: {
          chat: {
            title: {
              '*': this.translateService.instant(
                'CHAT.ZENDESK_CHAT_WINDOW_TITLE'
              ),
            },
          },
          offset: {
            vertical: '53px',
            mobile: {
              horizontal: '2px',
              vertical: '40px',
            },
          },
        },
      });
    });
  }

  gmtEvntLogVideo(i, video) {
    this.activeVideo = video;
    let eventProperties: EventProperties = {};
    eventProperties = {
      title: this.videoTitle[i].title.english,
      fromPage: this.router.url,
    };

    let superData = this.gtmService.assignLoggedInUsedData(this.user);

    this.gtmService.gtmEventWithSuperProp({
      eventName: 'View_Video',
      superProps: superData,
      eventProps: eventProperties,
    });
  }

  gmtEvntLogArticle(i) {
    let eventProperties: EventProperties = {};
    eventProperties = {
      title: this.videoTitle[i].title.english,
      fromPage: this.router.url,
    };

    let superData = this.gtmService.assignLoggedInUsedData(this.user);
    this.gtmService.gtmEventWithSuperProp({
      eventName: 'View_Article',
      superProps: superData,
      eventProps: eventProperties,
    });
  }

  onIframeClick() {
    this.pushGTMTags('View_Video', this.activeVideo);
  }

  selectVideo(video) {
    this.activeVideo = video;
    this.pushGTMTags('View_Video', video);
  }

  addLatestArticleGtmTag(article) {
    this.pushGTMTags('View_Article', article);
  }

  onBannerClick(banner) {
    this.pushGTMTags('Banner_Click', banner);
  }

  /**
   * @param eventName
   * @param eventData article or video or banner or popup(announcement) details
   */
  pushGTMTags(eventName: string, eventData = null) {
    let superProperties: SuperProperties = {};
    if (this.user) {
      superProperties = this.gtmService.assignLoggedInUsedData(this.user);
    }

    const getEventProperties = (eventName, eventData) => {
      let eventProperties: EventProperties = {};
      switch (eventName) {
        case 'View_Article':
          eventProperties = {
            fromPage: this.router.url,
            articleTitle:
              eventData.fullText?.trim() || eventData.title?.english,
          };
          break;
        case 'View_Video':
          eventProperties = {
            fromPage: this.router.url,
            videoTitle: eventData.fullText?.trim() || eventData.title?.english,
          };
          break;
        case 'Banner_Click':
          eventProperties = {
            title: eventData?.title[this.currentLanguage],
            bannerFileUrl: eventData.bannerFileUrl,
            destination: eventData.destination,
          };
          break;
        case 'Pop_Up_Rendered':
          eventProperties = {
            popUpId: eventData.uniqueId,
            imageName: eventData.image,
            imageType: 'Foreground',
            redirectionURL: eventData.destination,
          };
          break;
        default:
          break;
      }
      return eventProperties;
    };

    this.gtmService.gtmEventWithSuperProp({
      eventName: eventName,
      superProps: superProperties,
      eventProps: getEventProperties(eventName, eventData),
    });
  }

  async getTurkiTournaments(payload) {
    try {
      this.tournamentService.getPaginatedTournaments(payload).subscribe(
        (res: any) => {
          this.turkiTournaments = res.data.docs;
        },
        (err) => {
          console.error(err);
        }
      );
    } catch (error) {
    }
  }

  async getShowParticipantsStatus() {
    try {
      const data = await this.homeService.isShowParticipants();
      this.isShowParticipants = data?.showParticipant;
    } catch (error) {
      console.error(error?.error?.message && error?.message);
    }
  }

  async getGames() {
    try {
      const encodeUrl = `?query=${encodeURIComponent(
        JSON.stringify({
          isTournamentAllowed: true,
          name: this.gameName,
        })
      )}`;
      this.data = await this.gameService.getAllGames(encodeUrl).toPromise();
      if (this.data?.data[0]?._id) {
        this.getTournaments({
          status: '11',
          limit: 40,
          page: 1,
          game: this.data.data[0]._id,
        });
      }
      else { 
        this.tournamentsfor3wb = [];
      }
    } catch (error) {
        this.eSportsToastService.showError(
        this.translateService.instant(
          '3WB_TOURNAMENTS.ERRORS.ERROR_WHILE_FETCHING_GAMES'
        )
      );
    }
  }
  async getTournaments(payload) {
    try {
      this.tournamentService
        .getPaginatedTournaments(payload)
        .subscribe((res: any) => {
          this.tournamentsfor3wb = res.data.docs;
        });
    } catch (error) {
      this.eSportsToastService.showError(
        this.translateService.instant(
          '3WB_TOURNAMENTS.ERRORS.ERROR_WHILE_FETCH_TOURNAMENTS'
        )
      );
    }
  }

  addIframeListener() {
    this.listener = window.addEventListener('blur', () => {
      if (
        document.activeElement.id == 'launcher' &&
        document.activeElement.tagName.toUpperCase() == 'IFRAME'
      ) {
        this.pushGTMTags('View_Help');
      }
    });
  }

  addViewMoreArticleGTM($event) {
    this.pushGTMTags('View_All_Latest_Articles');
  }
}
