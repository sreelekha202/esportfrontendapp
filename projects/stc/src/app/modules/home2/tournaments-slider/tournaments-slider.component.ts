import { Component, OnInit, Input, EventEmitter, Output,ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';
import { EsportsLanguageService, GlobalUtils } from 'esports';
import { TranslateService } from '@ngx-translate/core';
import { IUser } from 'projects/esports/src/public-api';
import {
  EsportsGtmService,
  EventProperties,
} from '../../../../../../esports/src/lib/services/esport.gtm.service';
import { Router } from '@angular/router';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';

export interface Slide {
  bannerFileUrl: string;
  title: any;
  sub_title: any;
  button_text: any;
  destination: string;
}

@AutoUnsubscribe()
@Component({
  selector: 'app-tournaments-slider',
  templateUrl: './tournaments-slider.component.html',
  styleUrls: ['./tournaments-slider.component.scss'],
  providers: [NgbCarouselConfig],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TournamentsSliderComponent implements OnInit, OnDestroy {
  showNavigationArrows = false;
  showNavigationIndicators = true;
  bannerList: any = [];
  @Input() slides: any[];
  @Input() user: IUser;
  @Output() onBannerClicked = new EventEmitter<any>();

  constructor(
    private config: NgbCarouselConfig,
    private translate: TranslateService,
    public language: EsportsLanguageService,
    private gtmService: EsportsGtmService,
    private router: Router
  ) {
    config.showNavigationArrows = true;
    config.showNavigationIndicators = true;
  }

  ngOnChanges() {
    this.bannerList = this.slides;
  }

  /**
   * destination url
   * @param obj
   */
  redirectLink(obj, i) {
    this.gtmBannerEvent(i);
    if (GlobalUtils.isBrowser()) {
      window.location.href = obj;
    }
  }

  ngOnInit(): void {}

  ngOnDestroy() {}

  gtmBannerEvent(i) {
    let eventProperties: EventProperties = {};
    eventProperties = {
      title: this.bannerList[i].title.english,
      fromPage: this.router.url,
    };

    let superData = {};
    if (this.user) {
      superData = this.gtmService.assignLoggedInUsedData(this.user);
    }

    this.gtmService.gtmEventWithSuperProp({
      eventName: 'View_Banner',
      superProps: superData,
      eventProps: eventProperties,
    });
  }
  onBannerClick(bannerData) {
    let clickedBanner = this.slides.find(
      (ele) => ele._id == bannerData.activeId
    );
    if (clickedBanner) {
      this.onBannerClicked.emit(clickedBanner);
    }
  }
}
