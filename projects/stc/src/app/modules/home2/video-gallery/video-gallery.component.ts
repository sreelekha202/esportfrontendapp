import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ChangeDetectionStrategy,
  OnDestroy,
} from '@angular/core';
import { EsportsLanguageService } from 'esports';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';

@AutoUnsubscribe()
@Component({
  selector: 'app-video-gallery',
  templateUrl: './video-gallery.component.html',
  styleUrls: ['./video-gallery.component.scss', '../home2.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class VideoGalleryComponent implements OnInit, OnDestroy {
  constructor(public languageService: EsportsLanguageService) {}
  @Input() activeVideo: any;
  @Input() videoLibrary: any;
  @Output() callBack = new EventEmitter();
  ngOnInit(): void {}
  gmtEvntLogVideo(i, video) {
    this.callBack.emit({ type: 'evtLog', data: { index: i, val: video } });
  }
  onIframeClick() {
    this.callBack.emit({ type: 'active' });
  }
  ngOnDestroy() {
    this.callBack.unsubscribe();
  }
}
