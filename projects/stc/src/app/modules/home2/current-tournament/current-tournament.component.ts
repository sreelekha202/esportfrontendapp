import {
  Component,
  OnInit,
  Input,
  ChangeDetectionStrategy,
  OnDestroy,
} from '@angular/core';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';
import { AppHtmlRoutes } from '../../../app-routing.model';

@AutoUnsubscribe()
@Component({
  selector: 'app-current-tournament',
  templateUrl: './current-tournament.component.html',
  styleUrls: ['./current-tournament.component.scss', '../home2.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CurrentTournamentComponent implements OnInit, OnDestroy {
  @Input() currentData: any;

  constructor() {}

  ngOnInit(): void {}
  ngOnDestroy() {}
}

