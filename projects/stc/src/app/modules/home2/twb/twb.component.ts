import {
  Component,
  OnInit,
  Input,
  ChangeDetectionStrategy,
  OnDestroy,
} from '@angular/core';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';
import { AppHtmlRoutes } from '../../../app-routing.model';
@Component({
  selector: 'app-twb',
  templateUrl: './twb.component.html',
  styleUrls: ['./twb.component.scss', '../home2.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
@AutoUnsubscribe()
export class TwbComponent implements OnInit, OnDestroy {
  AppHtmlRoutes = AppHtmlRoutes;
  @Input() tournamentsfor3wb: any;
  @Input() isShowParticipants: any;
  @Input() timezone: any;
  constructor() {}

  ngOnInit(): void {}

  ngOnDestroy() {}

  isNumber(val): boolean {
    const str: string = val.toString();
    return str.startsWith('+');
  }
}
