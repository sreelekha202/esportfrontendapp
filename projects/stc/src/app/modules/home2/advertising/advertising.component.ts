import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';

export interface Advertising {
  logoUrl: string;
  backgroundImageUrl: string;
  title: string;
}
@AutoUnsubscribe()
@Component({
  selector: 'app-advertising',
  templateUrl: './advertising.component.html',
  styleUrls: ['./advertising.component.scss'],
})
export class AdvertisingComponent implements OnInit, OnDestroy {
  @Input() params: Advertising;

  constructor() {}

  ngOnInit(): void {}
  ngOnDestroy() {}
}
