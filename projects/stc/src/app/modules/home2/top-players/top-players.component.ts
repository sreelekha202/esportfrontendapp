import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';

interface TopPlayer {
  fullName: string;
  team: string;
  photo: string;
}

@AutoUnsubscribe()
@Component({
  selector: 'app-top-players',
  templateUrl: './top-players.component.html',
  styleUrls: ['./top-players.component.scss'],
})
export class TopPlayersComponent implements OnInit, OnDestroy {
  @Input() players: any[];

  constructor() {}

  ngOnInit(): void {}
  ngOnDestroy() {}
}
