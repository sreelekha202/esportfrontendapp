import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SportsFederationComponent } from './sports-federation.component';

describe('SportsFederationComponent', () => {
  let component: SportsFederationComponent;
  let fixture: ComponentFixture<SportsFederationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SportsFederationComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SportsFederationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
