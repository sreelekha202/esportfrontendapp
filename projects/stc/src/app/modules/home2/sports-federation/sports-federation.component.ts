import { Component, OnInit, Input, OnDestroy } from '@angular/core';

import { GlobalUtils } from 'esports';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';
import { AppHtmlRoutes } from '../../../app-routing.model';

@AutoUnsubscribe()
@Component({
  selector: 'app-sports-federation',
  templateUrl: './sports-federation.component.html',
  styleUrls: ['./sports-federation.component.scss'],
})
export class SportsFederationComponent implements OnInit, OnDestroy {
  @Input() params;

  AppHtmlRoutes = AppHtmlRoutes;

  constructor() {}

  ngOnInit(): void {}

  ngOnDestroy() {}

  goTop(): void {
    if (GlobalUtils.isBrowser()) {
      document.querySelector('mat-sidenav-content').scroll({
        top: 0,
        left: 0,
        behavior: 'smooth',
      });
    }
  }
}
