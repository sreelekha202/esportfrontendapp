import {
  Component,
  OnInit,
  Input,
  ChangeDetectionStrategy,
  OnDestroy,
} from '@angular/core';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';
import { AppHtmlRoutes } from '../../../app-routing.model';
@AutoUnsubscribe()
@Component({
  selector: 'app-upcoming-tournament',
  templateUrl: './upcoming-tournament.component.html',
  styleUrls: [
    './upcoming-tournament.component.scss',
    '../home2.component.scss',
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UpcomingTournamentComponent implements OnInit, OnDestroy {
  AppHtmlRoutes = AppHtmlRoutes;
  @Input() tournamentsData: any;
  @Input() isShowParticipants: any;
  @Input() timezone: any;
  constructor() {}

  ngOnInit(): void {}
  ngOnDestroy() {}

  isNumber(val): boolean {
    const str: string = val.toString();
    return str.startsWith('+');
  }
}
