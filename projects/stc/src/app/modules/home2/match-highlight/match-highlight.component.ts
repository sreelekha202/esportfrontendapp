import { EsportsLanguageService } from 'esports';
import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { EsportsTimezone } from 'esports';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';

@AutoUnsubscribe()
@Component({
  selector: 'app-match-highlight',
  templateUrl: './match-highlight.component.html',
  styleUrls: ['./match-highlight.component.scss'],
})
export class MatchHighlightComponent implements OnInit, OnDestroy {
  @Input() params;
  timezone;

  constructor(
    public languageService: EsportsLanguageService,
    private esportsTimezone: EsportsTimezone
  ) {}

  ngOnInit(): void {
    this.timezone = this.esportsTimezone.getTimeZoneName();
  }

  ngOnDestroy() { }

  isNumber(val): boolean {
    const str: string = val.toString();
    return str.startsWith('+');
  }
}
