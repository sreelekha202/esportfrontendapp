import { TranslateService } from '@ngx-translate/core';
import { Component, OnInit, Input,ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { AppHtmlRoutes } from '../../../app-routing.model';
import {
  InfoPopupComponent,
  InfoPopupComponentData,
  InfoPopupComponentType,
} from '../../../shared/popups/info-popup/info-popup.component';
import { MatDialog } from '@angular/material/dialog';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';

@AutoUnsubscribe()
@Component({
  selector: 'app-tournaments-card',
  templateUrl: './tournaments-card.component.html',
  styleUrls: ['./tournaments-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TournamentsCardComponent implements OnInit, OnDestroy {
  @Input() params;

  AppHtmlRoutes = AppHtmlRoutes;
  constructor(
    public matDialog: MatDialog,
    public translateService: TranslateService
  ) {}

  ngOnInit(): void {}

  ngOnDestroy() {}

  openSocialShareComponent() {
    const data: InfoPopupComponentData = {
      title: this.translateService.instant('ARTICLE_POST.SHARE_TOURNAMENT'),
      text: ``,
      type: InfoPopupComponentType.socialSharing,
      cancelBtnText: 'Close',
    };
    this.matDialog.open(InfoPopupComponent, {
      data,
    });
  }
}
