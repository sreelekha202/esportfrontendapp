import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VouchersSliderComponent } from './vouchers-slider.component';

describe('VouchersSliderComponent', () => {
  let component: VouchersSliderComponent;
  let fixture: ComponentFixture<VouchersSliderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [VouchersSliderComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VouchersSliderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
