import { SwiperConfigInterface } from 'ngx-swiper-wrapper';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';

export interface VoucherSlide {
  isSelected: boolean;
  logo: string;
  img: string;
  title: string;
  text: string;
}

@AutoUnsubscribe()
@Component({
  selector: 'app-vouchers-slider',
  templateUrl: './vouchers-slider.component.html',
  styleUrls: ['./vouchers-slider.component.scss'],
})
export class VouchersSliderComponent implements OnInit, OnDestroy {
  swiperConfig: SwiperConfigInterface = {
    width: 187,
  };

  swiperIndex = 0;

  slider: Array<VoucherSlide> = [
    {
      isSelected: true,
      logo: '/assets/images/AboutUs/col3.png',
      img: '/assets/images/TopPlayers/top-player-01.png',
      title: 'FREE FIRE',
      text:
        'Free Fire is the fastest Battle Royale survival shooter game available. Experience 10 minutes fast paced matches on a stranded island against 49 other players to be the last man standing and earn your Booyah!',
    },
    {
      isSelected: false,
      logo: '/assets/images/AboutUs/col5.png',
      img: '/assets/images/TopPlayers/top-player-02.png',
      title: 'LEAGUE LEGENDS',
      text:
        'Welcome to Summoner’s Rift. Experience the number 1 PC MOBA game in the world. Summon Ryze, Orianna and more on in the Fields of Justice.',
    },
    {
      isSelected: false,
      logo: '/assets/images/AboutUs/col7.png',
      img: '/assets/images/TopPlayers/top-player-03.png',
      title: 'CALL OF DUTY',
      text:
        'Experience the thrill of Call of Duty on the go. Play as iconic characters in battle royale and multiplayer in the best free mobile FPS game. More modes unique to Call of Duty Mobile are available.',
    },
    {
      isSelected: false,
      logo: '/assets/images/AboutUs/col3.png',
      img: '/assets/images/TopPlayers/top-player-01.png',
      title: 'FIFA20',
      text:
        'The newest iteration of the best football simulation in the world is back. Refined graphics, improved simulation engine and even more game   modes. It’s your time to play the footbal clubs and players of your dreams.',
    },
    {
      isSelected: false,
      logo: '/assets/images/AboutUs/col5.png',
      img: '/assets/images/TopPlayers/top-player-02.png',
      title: 'Pubg',
      text:
        'The official Player Unknown’s Battlegrounds for your mobile. Experience the original mobile battle royale game and earn your Chicken Dinner.',
    },
    {
      isSelected: false,
      logo: '/assets/images/AboutUs/col3.png',
      img: '/assets/images/TopPlayers/top-player-01.png',
      title: 'BUY STEAM WALLET CODES',
      text:
        'At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas.',
    },
  ];

  activeSlide: VoucherSlide;

  constructor() {
    this.activeSlide = this.slider[this.swiperIndex];
  }

  ngOnInit(): void {}

  ngOnDestroy() {}

  onSlidePrev(): void {
    if (this.swiperIndex > 0) {
      this.activeSlide = this.slider[this.swiperIndex];
    }
  }

  onSlideNext(): void {
    if (this.slider.length > this.swiperIndex) {
      this.activeSlide = this.slider[this.swiperIndex];
    }
  }

  onSetMainSlide(selectedSlide: VoucherSlide, index: number): void {
    this.swiperIndex = index;

    this.slider.forEach((item) => (item.isSelected = false));

    selectedSlide.isSelected = true;
    this.activeSlide = selectedSlide;
  }
}
