import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GwbComponent } from './gwb.component';

describe('GwbComponent', () => {
  let component: GwbComponent;
  let fixture: ComponentFixture<GwbComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GwbComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GwbComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
