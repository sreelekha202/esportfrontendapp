import { Component,Input, OnInit, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';
import { AppHtmlRoutes } from '../../../app-routing.model';
@Component({
  selector: 'app-gwb',
  templateUrl: './gwb.component.html',
  styleUrls: ['./gwb.component.scss', '../home2.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
@AutoUnsubscribe()
export class GwbComponent implements OnInit, OnDestroy {
  AppHtmlRoutes = AppHtmlRoutes;
  @Input() gwbTouranmentData: any;
  @Input() isShowParticipants: any;
  @Input() timezone: any;
  constructor() {}

  ngOnInit(): void {}

  ngOnDestroy() {}

  isNumber(val): boolean {
    const str: string = val.toString();
    return str.startsWith('+');
  }
}

