import { Component, OnDestroy, OnInit } from '@angular/core';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';
import { HomeService } from '../../../core/service';
@AutoUnsubscribe()
@Component({
  selector: 'app-tournaments-tracker',
  templateUrl: './tournaments-tracker.component.html',
  styleUrls: ['./tournaments-tracker.component.scss'],
})
export class TournamentsTrackerComponent implements OnInit, OnDestroy {
  scoreList;
  cards = [
    {
      tournamentName: 'Dota 2 Championship',
      gamer1Name: 'Tony Matt',
      gamer2Name: 'Cormac',
      score1: 2,
      score2: 0,
      city: 'Malacca Championship',
    },
    {
      tournamentName: 'Dota 2 Championship',
      gamer1Name: 'WESTLEY',
      gamer2Name: 'CASHEL',
      score1: 2,
      score2: 1,
      city: 'Malacca Championship',
    },
    {
      tournamentName: 'Dota 2 Championship',
      gamer1Name: 'Tony Matt',
      gamer2Name: 'Cormac',
      score1: 3,
      score2: 1,
      city: 'Malacca Championship',
    },
    {
      tournamentName: 'Dota 2 Championship',
      gamer1Name: 'WESTLEY',
      gamer2Name: 'CASHEL',
      score1: 2,
      score2: 0,
      city: 'Malacca Championship',
    },
    {
      tournamentName: 'Dota 2 Championship',
      gamer1Name: 'Tony Matt',
      gamer2Name: 'Cormac',
      score1: 2,
      score2: 0,
      city: 'Malacca Championship',
    },
    {
      tournamentName: 'Dota 2 Championship',
      gamer1Name: 'WESTLEY',
      gamer2Name: 'CASHEL',
      score1: 2,
      score2: 0,
      city: 'Malacca Championship',
    },
    {
      tournamentName: 'Dota 2 Championship',
      gamer1Name: 'Tony Matt',
      gamer2Name: 'Cormac',
      score1: 2,
      score2: 0,
      city: 'Malacca Championship',
    },
    {
      tournamentName: 'Dota 2 Championship',
      gamer1Name: 'WESTLEY',
      gamer2Name: 'CASHEL',
      score1: 2,
      score2: 0,
      city: 'Malacca Championship',
    },
  ];

  constructor(private homeService: HomeService) {}

  ngOnInit(): void {
    this.getScoreDetail();
  }
  ngOnDestroy() {}

  getScoreDetail = async () => {
    try {
      const scoreData = await this.homeService.fetchScore();
      this.scoreList = scoreData.data;
    } catch (error) {}
  };
}
