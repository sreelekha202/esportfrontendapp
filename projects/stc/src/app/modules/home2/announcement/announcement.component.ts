import { Component, Input, OnInit, OnDestroy,ChangeDetectionStrategy } from '@angular/core';
import { GlobalUtils } from 'esports';
import {
  IUser,
  SuperProperties,
  EventProperties,
  EsportsGtmService,
  EsportsUserService,
} from 'esports';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';
import { Subscription } from 'rxjs';
import { sampleTime } from 'rxjs/operators';

@AutoUnsubscribe()
@Component({
  selector: 'app-announcement',
  templateUrl: './announcement.component.html',
  styleUrls: ['./announcement.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AnnouncementComponent implements OnInit, OnDestroy {
  constructor(
    private gtmService: EsportsGtmService,
    private userService: EsportsUserService
  ) {}
  @Input() data: any;
  @Input() showBox: any;
  userSubscription: Subscription;
  currentUser: IUser;

  ngOnInit(): void {
    this.userSubscription = this.userService.currentUser
      .pipe(sampleTime(500))
      .subscribe((user) => {
        if (user) {
          this.currentUser = user;
        }
        if (this.data && this.showBox) {
          this.pushGTMTags('Pop_Up_Rendered', this.data);
        }
      });
  }
  closeModal() {
    if (GlobalUtils.isBrowser()) {
      this.pushGTMTags('Pop_Up_Seen', this.data);
      localStorage.setItem('announcement', this.data.uniqueId);
      this.showBox = false;
    }
  }

  /**
   * destination url
   * @param obj
   */
  redirectLink(obj) {
    if (GlobalUtils.isBrowser()) {
      this.pushGTMTags('Pop_Up_Clicked', this.data);
      this.closeModal();
      window.location.href = obj;
    }
  }

  /**
   * @param eventName
   * @param eventData popup(announcement) details
   */
  pushGTMTags(eventName: string, eventData = null) {
    let superProperties: SuperProperties = {};
    if (this.currentUser) {
      superProperties = this.gtmService.assignLoggedInUsedData(
        this.currentUser
      );
    }

    let eventProperties: EventProperties = {
      popUpId: eventData?.uniqueId,
      imageName: eventData?.image,
      imageType: 'Foreground',
      redirectionURL: eventData?.destination,
    };

    this.gtmService.gtmEventWithSuperProp({
      eventName: eventName,
      superProps: superProperties,
      eventProps: eventProperties,
    });
  }

  ngOnDestroy() {
    this.userSubscription?.unsubscribe();
  }
}
