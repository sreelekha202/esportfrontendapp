import { Component,Input,  OnInit,ViewEncapsulation, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';

@Component({
  selector: 'app-devices',
  templateUrl: './devices.component.html',
  styleUrls: ['./devices.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
@AutoUnsubscribe()
export class DevicesComponent implements OnInit, OnDestroy {
  @Input() devices: any;
  constructor() {}

  ngOnInit(): void {}

  ngOnDestroy() {}
}
