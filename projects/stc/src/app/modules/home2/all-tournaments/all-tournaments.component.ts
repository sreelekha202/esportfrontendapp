import { TranslateService } from '@ngx-translate/core';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { TournamentService } from '../../../core/service';
import {
  InfoPopupComponent,
  InfoPopupComponentData,
  InfoPopupComponentType,
} from '../../../shared/popups/info-popup/info-popup.component';
import { MatDialog } from '@angular/material/dialog';
import { AppHtmlRoutes } from '../../../app-routing.model';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';

@AutoUnsubscribe()
@Component({
  selector: 'app-all-tournaments',
  templateUrl: './all-tournaments.component.html',
  styleUrls: ['./all-tournaments.component.scss'],
})
export class AllTournamentsComponent implements OnInit, OnDestroy {
  tournaments = [];
  AppHtmlRoutes = AppHtmlRoutes;
  constructor(
    private tournamentService: TournamentService,
    public matDialog: MatDialog,
    public translateService: TranslateService
  ) {}

  ngOnInit(): void {
    this.getOnGoingTournaments();
  }

  ngOnDestroy() {}

  getOnGoingTournaments() {
    this.tournamentService
      .getPaginatedTournaments({
        status: '1',
        limit: 3,
        sort: 'Latest',
      })
      .subscribe(
        (res: any) => {
          this.tournaments = res.data.docs;
        },
        (err) => {
          // this.showLoader = false;
          console.error(err);
        }
      );
  }

  getRegistrationStatus(
    startDate,
    startTime,
    isSeeded,
    isFinished,
    maxParticipants,
    participantsJoined
  ) {
    if (!startDate || !startTime) {
      return;
    }
    const timeObj = this.convert12HrsTo24HrsFormat(startTime);
    const sDate = new Date(startDate);
    sDate.setHours(timeObj['hour']);
    sDate.setMinutes(timeObj['minute']);
    if (isSeeded == true || isFinished == true) {
      return 'Closed';
    } else if (participantsJoined >= maxParticipants) {
      return 'Closed';
    } else if (sDate.getTime() < Date.now()) {
      return 'Closed';
    } else {
      return 'Open';
    }
  }

  convert12HrsTo24HrsFormat(time) {
    if (!time) {
      return;
    }
    let hours = Number(time.match(/^(\d+)/)[1]);
    let minutes = Number(time.match(/:(\d+)/)[1]);
    let AP = time.match(/\s(.*)$/);
    if (!AP) {
      AP = time.slice(-2);
    } else {
      AP = AP[1];
    }
    if (AP == 'PM' && hours < 12) {
      hours = hours + 12;
    }
    if (AP == 'AM' && hours == 12) {
      hours = hours - 12;
    }
    let Hours24 = hours.toString();
    let Minutes24 = minutes.toString();
    if (hours < 10) {
      Hours24 = '0' + Hours24;
    }
    if (minutes < 10) {
      Minutes24 = '0' + Minutes24;
    }

    return { hour: parseInt(Hours24), minute: parseInt(Minutes24) };
  }

  openSocialShareComponent() {
    const data: InfoPopupComponentData = {
      title: this.translateService.instant('ARTICLE_POST.SHARE_TOURNAMENT'),
      text: ``,
      type: InfoPopupComponentType.socialSharing,
      cancelBtnText: 'Close',
    };
    this.matDialog.open(InfoPopupComponent, {
      data,
    });
  }
}
