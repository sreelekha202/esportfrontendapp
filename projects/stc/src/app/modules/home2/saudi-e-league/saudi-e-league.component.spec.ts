import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SaudiELeagueComponent } from './saudi-e-league.component';

describe('SaudiELeagueComponent', () => {
  let component: SaudiELeagueComponent;
  let fixture: ComponentFixture<SaudiELeagueComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SaudiELeagueComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SaudiELeagueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
