import { Component,Input, OnInit, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';
import { AppHtmlRoutes } from '../../../app-routing.model';
@Component({
  selector: 'app-saudi-e-league',
  templateUrl: './saudi-e-league.component.html',
  styleUrls: ['./saudi-e-league.component.scss', '../home2.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
@AutoUnsubscribe()
export class SaudiELeagueComponent implements OnInit, OnDestroy {
  AppHtmlRoutes = AppHtmlRoutes;
  @Input() saudi_E_League: any;
  @Input() isShowParticipants: any;
  @Input() timezone: any;
  constructor() {}

  ngOnInit(): void {}

  ngOnDestroy() {}

  isNumber(val): boolean {
    const str: string = val.toString();
    return str.startsWith('+');
  }
}
