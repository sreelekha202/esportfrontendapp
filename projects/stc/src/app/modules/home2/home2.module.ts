import { NgModule } from '@angular/core';

import { SharedModule } from '../../shared/modules/shared.module';
import { CoreModule } from '../../core/core.module';

import { Home2RoutingModule } from './home2-routing.module';
import { Home2Component } from './home2.component';
import { TournamentsCardModule } from './tournaments-card/tournaments-card.module';
import { DevicesComponent } from './devices/devices.component';
import { SaudiELeagueComponent } from './saudi-e-league/saudi-e-league.component';
import { GwbComponent } from './gwb/gwb.component';
import { TwbComponent } from './twb/twb.component';
import { VideoGalleryComponent } from './video-gallery/video-gallery.component';
import { LatestArticleComponent } from './latest-article/latest-article.component';
import { UpcomingTournamentComponent } from './upcoming-tournament/upcoming-tournament.component';
import { CurrentTournamentComponent } from './current-tournament/current-tournament.component';

@NgModule({
  declarations: [Home2Component, DevicesComponent, SaudiELeagueComponent, GwbComponent, TwbComponent, VideoGalleryComponent, LatestArticleComponent, UpcomingTournamentComponent, CurrentTournamentComponent],
  imports: [
    SharedModule,
    CoreModule,
    Home2RoutingModule,
    TournamentsCardModule,
  ],
  exports: [],
})
export class Home2Module {}
