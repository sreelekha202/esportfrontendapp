import { EsportsLanguageService } from 'esports';
import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';

@AutoUnsubscribe()
@Component({
  selector: 'app-hottest-posts',
  templateUrl: './hottest-posts.component.html',
  styleUrls: ['./hottest-posts.component.scss'],
})
export class HottestPostsComponent implements OnInit, OnDestroy {
  @Input() params: any;

  constructor(public languageService: EsportsLanguageService) {}

  ngOnInit() {}
  ngOnDestroy() {}
}
