import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GamesCardsComponent } from './games-cards.component';

describe('GamesListComponent', () => {
  let component: GamesCardsComponent;
  let fixture: ComponentFixture<GamesCardsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GamesCardsComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GamesCardsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
