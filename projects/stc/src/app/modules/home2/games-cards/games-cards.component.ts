import { EsportsLanguageService } from 'esports';
import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';

@AutoUnsubscribe()
@Component({
  selector: 'app-games-cards',
  templateUrl: './games-cards.component.html',
  styleUrls: ['./games-cards.component.scss'],
})
export class GamesCardsComponent implements OnInit, OnDestroy {
  @Input() cards = [];

  constructor(public languageService: EsportsLanguageService) {}

  ngOnInit(): void {}
  ngOnDestroy() {}
}
