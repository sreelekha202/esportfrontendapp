import { NgModule } from '@angular/core';

import { SharedModule } from '../../shared/modules/shared.module';
import { CoreModule } from '../../core/core.module';

import { GamesRoutingModule } from './games-routing.module';
import { GamesComponent } from './games.component';
import { MatchmakerDialogComponent } from './matchmaker-dialog/matchmaker-dialog.component';

@NgModule({
  declarations: [GamesComponent, MatchmakerDialogComponent],
  imports: [SharedModule, CoreModule, GamesRoutingModule],
  exports: [],
})
export class GamesModule {}
