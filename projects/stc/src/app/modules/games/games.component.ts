import {
  Component,
  OnDestroy,
  OnInit,
  Inject,
  PLATFORM_ID,
} from '@angular/core';
import { GameService } from '../../core/service';
import {
  EsportsChatService,
  EsportsGtmService,
  EventProperties,
  SuperProperties,
} from 'esports';
import { MatDialog } from '@angular/material/dialog';
import { MatchmakerDialogComponent } from './matchmaker-dialog/matchmaker-dialog.component';
import { IUser, EsportsUserService } from 'esports';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { isPlatformBrowser } from '@angular/common';
@Component({
  selector: 'app-games',
  templateUrl: './games.component.html',
  styleUrls: ['./games.component.scss'],
})
export class GamesComponent implements OnInit, OnDestroy {
  gameList = [];
  windowposition = 'chat_window chat_window_right_drawer';
  currenUser: IUser;
  isUserLoggedIn = false;
  currentUserId = '';
  currentUserName = '';
  matchdetails: any;
  isBrowser: boolean;
  userSubscription: Subscription;
  gameSearchText = '';

  constructor(
    private gameService: GameService,
    public gamesMatchmaking: MatDialog,
    private chatService: EsportsChatService,
    private userService: EsportsUserService,
    private router: Router,
    private gtmService: EsportsGtmService,
    @Inject(PLATFORM_ID) private platformId
  ) {
    this.isBrowser = isPlatformBrowser(platformId);
  }

  ngOnInit(): void {
    if (this.isBrowser) {
      this.getGames();
      this.userSubscription = this.userService.currentUser.subscribe((data) => {
        if (data) {
          this.currenUser = data;
          this.isUserLoggedIn = !!this.currenUser;
          this.currentUserId = this.currenUser._id;
        } else {
        }
      });
    }
  }

  ngOnDestroy() {
    this.userSubscription?.unsubscribe();
  }

  getGames() {
    const encodeUrl = `?query=${encodeURIComponent(
      JSON.stringify({
        isTournamentAllowed: true,
        searchBy: this.gameSearchText,
      })
    )}`;
    this.gameService.getAllGames(encodeUrl).subscribe(
      (res) => {
        this.gameList = res.data;
      },
      (err) => {}
    );
  }
  searchByValueFilter(event) {
    if (
      (this.gameSearchText != '' && event.target.value.trim() == '') ||
      (event.target.value.trim() != '' &&
        this.gameSearchText != event.target.value.trim())
    ) {
      this.gameSearchText = event.target.value.trim();
      this.getGames();
    }
  }
  openDialog(game, gameIndex) {
    this.gtmEventGameCard(gameIndex);

   const dialogRef = this.gamesMatchmaking.open(MatchmakerDialogComponent, {
      data: game,
      panelClass: 'matchMaking',
      disableClose: true,
      hasBackdrop: false,
    });

    dialogRef.afterClosed().subscribe((result) => {
      // if (!(result == 'nomatchfound' || result == 'mm-not-done')) {
      //   if (this.currenUser) {
      //     this.router.navigateByUrl('/tournament/info');
      //   }
      // }
    });
  }

  pushGTMTags(eventName: string) {
    let superProperties: SuperProperties = {};
    if (this.currenUser) {
      superProperties = this.gtmService.assignLoggedInUsedData(this.currenUser);
    }
    this.gtmService.gtmEventWithSuperProp({
      eventName: eventName,
      superProps: superProperties,
    });
  }
  // GA Events- Select Matchmaking Game

  gtmEventGameCard(index){
    let superProperties: SuperProperties = {};
    if (this.currenUser) {
      superProperties = this.gtmService.assignLoggedInUsedData(
        this.currenUser
      );
    }

    let eventProperties: EventProperties = {};

    let eventName = 'Select_Matchmaking_Game'

    eventProperties['gameName'] = this.gameList[index].name;

    this.gtmService.gtmEventWithSuperProp({
      eventName: eventName,
      superProps: superProperties,
      eventProps: eventProperties,
    });

  }
}
