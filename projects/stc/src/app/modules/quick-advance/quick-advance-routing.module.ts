import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { QuickAdvanceComponent } from './quick-advance.component';
import { AuthGuard } from '../../shared/guard/auth.guard';
import { QuickTournamentComponent } from './quick-tournament/quick-tournament.component';
import { SelectTypeComponent } from './select-type/select-type.component';

const routes: Routes = [
  {
    path: '',
    component: QuickAdvanceComponent,
    children: [
      {
        path: '',
        component: SelectTypeComponent,
      },
      {
        path: 'quicktournament',
        component: QuickTournamentComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'advancetournament',
        loadChildren: () =>
          import('./advance-tournamnet/advance.module').then(
            (m) => m.AdvanceTournamentModule
          ),
        canActivate: [AuthGuard],
        data: {
          isRootPage: true,
        },
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class QuickAdvanceRoutingModule {}
