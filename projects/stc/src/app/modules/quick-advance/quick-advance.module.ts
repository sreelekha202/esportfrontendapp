import { QuickAdvanceRoutingModule } from './quick-advance-routing.module';
import { QuickAdvanceComponent } from './quick-advance.component';
import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/modules/shared.module';
import { CoreModule } from '../../core/core.module';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { QuickTournamentComponent } from './quick-tournament/quick-tournament.component';
import { EsportsQuickTournamentType1Module, EsportsHeaderModule } from 'esports';
import { environment } from '../../../environments/environment';
import { SelectTypeComponent } from './select-type/select-type.component';
import { HeaderComponent } from './components/header/header.component';

@NgModule({
  declarations: [
    QuickAdvanceComponent,
    QuickTournamentComponent,
    SelectTypeComponent,
    HeaderComponent,
  ],
  imports: [
    SharedModule,
    CoreModule,
    QuickAdvanceRoutingModule,
    MatProgressSpinnerModule,
    EsportsQuickTournamentType1Module.forRoot(environment),
    EsportsHeaderModule.forRoot({
      borderColor: '#772bcb',
      backgroundColor: '#fff',
      fontColor: '#1d1d1d',
    }),
  ],
  providers: [],
})
export class QuickAdvanceModule {}
