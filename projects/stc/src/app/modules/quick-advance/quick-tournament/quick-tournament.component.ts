import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GlobalUtils, QuickTournamnetComponentData } from 'esports';

@Component({
  selector: 'app-quick-tournament',
  templateUrl: './quick-tournament.component.html',
  styleUrls: ['./quick-tournament.component.scss'],
})
export class QuickTournamentComponent implements OnInit {
  quickData: QuickTournamnetComponentData = {
    step: 1,
  };

  constructor(private router: Router) {
    if (GlobalUtils.isBrowser()) {
      this.quickData.step = 1;
    }
  }

  ngOnInit(): void {}

  tournamentCreated(data) {
    if(data?.data?.step) {
      this.quickData.step = data.data.step
    } else {
      this.router.navigateByUrl('/create-tournament/CreateQuick/'+(data.tournamentdetails.slug));
    }
  }
  back(data) {
    if (GlobalUtils.isBrowser()) {
      if (data.back) {
        if (this.quickData.step === 1) {
          this.router.navigateByUrl('tournament');
        } else {
          this.quickData.step = this.quickData.step - 1;
        }
      }
    }
  }

  close(data) {
    this.router.navigateByUrl('tournament');
  }
}
