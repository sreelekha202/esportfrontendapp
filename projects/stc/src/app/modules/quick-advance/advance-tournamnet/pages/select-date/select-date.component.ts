import { Component, Input, OnChanges, OnInit } from '@angular/core';
import {
  ControlContainer,
  FormGroup,
  FormGroupDirective,
  FormControl,
  Validators
} from '@angular/forms';
import { EsportsToastService } from 'esports';
import { TranslateService } from '@ngx-translate/core';
@Component({
  selector: 'app-select-date',
  templateUrl: './select-date.component.html',
  styleUrls: ['./select-date.component.scss'],
  viewProviders: [
    { provide: ControlContainer, useExisting: FormGroupDirective },
  ],
})
export class SelectDateComponent implements OnInit {
  Step5: FormGroup | null;
  minDate: Date | null;
  date: any;
  constructor(
    private tournament: FormGroupDirective,
    private toastService: EsportsToastService,
    private translateService: TranslateService
  ) {}
  time:any;
  endTime:any;

  ngOnInit(): void {
    this.Step5 = this.tournament?.form;
    if(!this.Step5?.get('noOfParticipants'))
    this.Step5.addControl('noOfParticipants', new FormControl(0,[]));

    if(!this.Step5?.get('startTime'))
    this.Step5.addControl('startTime', new FormControl('', Validators.required));

    if(!this.Step5?.get('endDate'))
    this.Step5.addControl('endDate', new FormControl('', Validators.required));

    if(this.Step5?.value.startTime)
      this.time = this.Step5?.get('startTime').value;
    
    this.setMinimumDate();
  }

  setMinimumDate() {
    const date = new Date();
    date.setMinutes(date.getMinutes() + 5);
    date.setSeconds(0);
    this.minDate = date;
  }

  onEnter(event: any): void {
    if (this.time) {
      this.date = this.Step5?.get('startDate')?.value;
      const myArr = this.time.split(' ');
      let AMPM = myArr[1];
      let time = myArr[0].split(':');
      let hour: number = Number(time[0]);
      let minute = Number(time[1]);
      AMPM == 'PM' || AMPM == 'pm' || AMPM == 'Pm' || AMPM == 'pM'
        ? (hour = hour + 12)
        : '';
      const date = new Date(this.date);
      date.setHours(hour);
      date.setMinutes(minute);
      this.date = date;
      this.Step5?.get('startDate').setValue(this.date);
      if (this.Step5?.get('startDate')?.invalid) return;
      this.Step5.get('step').setValue(this.Step5?.get('step')?.value + 1);
    }
  }
  onTimeChange(t) {
    this.time = t;
  }
  onStepChange(step: number): void {
    if (
      this.Step5?.get('startDate')?.invalid &&
      this.Step5?.get('step')?.value < step ||
      typeof this.time === 'undefined') {
      this.toastService.showError(this.translateService.instant('CREATE_TOURNMENT.SELECT_DATE_TIME'));
      return;
    }

    if(this.checkCurrentTime()){
      this.toastService.showError(this.translateService.instant('CREATE_TOURNMENT.SELECT_FUTURE_TIME'));
      return;
    }
    
    if(this.Step5?.get('noOfParticipants')?.value < 2){
      this.toastService.showError(this.translateService.instant('CREATE_TOURNMENT.SELECT_MIN_PARTICIPANTS'));
      return;
    }

    this.Step5?.get('step')?.setValue(step + 1);
  }

  /* check for current time check for creating the tournament */
  checkCurrentTime(){
     var getSelectedTime:any = new Date(this.Step5.value.startDate).setDate(this.Step5.value.startDate.getDate()+1);
     getSelectedTime = new Date(getSelectedTime).toISOString().substring(0, 10)
     getSelectedTime = getSelectedTime+' '+this.time;
     var getSelectedDTime = new Date(getSelectedTime).getTime();
     let finalselectedTime = new Date().getTime();
    if(finalselectedTime > getSelectedDTime){
      return true;
    }
    return false;
  }
}
