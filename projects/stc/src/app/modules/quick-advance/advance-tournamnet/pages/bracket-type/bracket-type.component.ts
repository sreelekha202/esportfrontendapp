import { Component, OnInit } from '@angular/core';
import {
  AbstractControl,
  ControlContainer,
  FormControl,
  FormGroupDirective,
  ValidatorFn,
  Validators,
  FormBuilder,
  FormArray,
  FormGroup,
} from '@angular/forms';
import { EsportsToastService, EsportsTournamentService } from 'esports';

@Component({
  selector: 'app-bracket-type',
  templateUrl: './bracket-type.component.html',
  styleUrls: ['./bracket-type.component.scss'],
  viewProviders: [
    { provide: ControlContainer, useExisting: FormGroupDirective },
  ],
})
export class BracketTypeComponent implements OnInit {
  Step5;
  prizeLimit: number = 6;
  showPosition: 0;
  multiStage = ['battle_royale', 'round_robin'];
  rrFormat = {
    1: 'OPTIONS.RR_1',
    2: 'OPTIONS.RR_2',
  };
  lengthList = 0;
  dataFilter = '';
  desc = '';
  platforms = [
    {
      name: 'Single Elimination',
      img: '../../../../../assets/icons/matchmaking/format/Single-elimination.svg',
      desc: 'A Single-Elimination format is a tournament where the loser of each game is immediately eliminated from the competition. Each winner will play another in the next round, until the final match-up, the winner of which will be declared the champion.',
    },
    {
      name: 'Double Elimination',
      img: '../../../../../assets/icons/matchmaking/format/Double-elimination.svg',
      desc: "A Double-Elimination format is a tournament in which a participant ceases to be eligible to win the tournament's championship upon having lost two matches.",
    },
    {
      name: 'Round Robin',
      img: '../../../../../assets/icons/matchmaking/format/Round-robin.svg',
      desc: 'A Round Robin tournament is a competition in which each contestant meets all other contestants in turn. Each participant plays every other participant once, with the winner emerging from the succession of events.',
    },
    {
      name: 'Ladder',
      img: '../../../../../assets/icons/matchmaking/format/Ladder.svg',
      desc: 'In a Ladder tournament, there are no eliminations and players are ranked as if on the rungs of a ladder - the objective is to reach the highest position by the end date set by the organiser.',
    },
    {
      name: 'Swiss System',
      img: '../../../../../assets/icons/matchmaking/format/Swiss-system.svg',
      desc: 'Swiss System is used for larger tournaments. In the first round, opponents are drawn at random or via predetermined seeds. In the second round, competitors that won in the first will play each other, as do players that lost. Competitors will continue to play each other with the same score record for the number of rounds as set by the tournament organiser.',
    },
    {
      name: 'Battle Royale',
      img: '../../../../../assets/icons/matchmaking/format/Battle-royale.svg',
      desc: 'Battle Royale tournaments take place in multiplayer arenas where the last man standing is declared winner and runners up based on the order they are eliminated. Players are not assigned specific opponents or times to play, instead they seek other competitors and attempt to eliminate them to improve their own standing.',
    },
  ];
  constructor(
    public eSportsTournamentService: EsportsTournamentService,
    private tournament: FormGroupDirective,
    private fb: FormBuilder,
    private toastService: EsportsToastService
  ) {}

  ngOnInit(): void {
    for (let key in this.eSportsTournamentService.getBracketTypes) {
      if (this.eSportsTournamentService.getBracketTypes[key]) {
        this.lengthList++;
      }
    }
    this.Step5 = this.tournament.form;
    this.Step5.addControl(
      'bracketType',
      new FormControl('', Validators.required)
    );
    this.Step5.addControl('isParticipantsLimit', new FormControl(true));
    this.Step5.addControl(
      'maxParticipants',
      new FormControl(
        2,
        Validators.compose([
          Validators.required,
          Validators.min(2),
          Validators.max(1000),
          this.teamInGroupValidation('maxParticipants').bind(this),
        ])
      )
    );
  }
  checkShowTitle(idx) {
    let index = 0;
    let position = 0;
    for (let key in this.eSportsTournamentService.getBracketTypes) {
      index++;
      if (this.eSportsTournamentService.getBracketTypes[key] && idx > index) {
        position += 1;
      }
    }
    return position;
  }
  onSelectBracketType(v) {
    this.Step5.get('bracketType').setValue(v);
    this.bracketChangeHandler(v);
  }

  // custom field controller

  addNewControlAndUpdateValidity = (
    field,
    fieldType,
    defaultValue,
    min?,
    max?,
    pattern?,
    customValidation?
  ) => {
    const formControl = this.Step5.get(field);
    const validation = [];

    switch (fieldType) {
      case 'NUMBER':
        validation.push(Validators.required);
        if (typeof min == 'number') validation.push(Validators.min(min));
        if (typeof max == 'number') validation.push(Validators.max(max));
        if (pattern) validation.push(pattern);
        if (customValidation) validation.push(customValidation);
        break;
      case 'BOOLEAN':
        validation.push(Validators.required);
        break;
      case 'STRING':
        validation.push(Validators.required);
        break;
    }

    if (formControl) {
      formControl.setValue(defaultValue);
      formControl.setValidators(validation);
      formControl.updateValueAndValidity();
    } else {
      this.Step5.addControl(
        field,
        new FormControl(defaultValue, Validators.compose(validation))
      );
    }
  };

  removeControlOrValidity = (field) => {
    const formControl = this.Step5.get(field);
    if (formControl) {
      this.Step5.removeControl(field);
    } else {
    }
  };

  // custom validators

  teamInGroupValidation = (field?: string): ValidatorFn => {
    return (control: AbstractControl): { [key: string]: any } | null => {
      if (control['_parent']) {
        const {
          noOfTeamInGroup,
          noOfWinningTeamInGroup,
          maxParticipants,
          noOfStage,
        } = control?.['_parent']?.controls;

        if (
          maxParticipants?.valid &&
          noOfTeamInGroup?.value &&
          !noOfTeamInGroup?.errors?.max &&
          noOfWinningTeamInGroup?.value &&
          maxParticipants?.value &&
          noOfStage
        ) {
          const fieldValidation = (
            maxParticipants,
            noOfTeamInGroup,
            noOfWinningTeam
          ) => {
            if (maxParticipants < noOfTeamInGroup) {
              return { max: { max: maxParticipants } };
            } else if (noOfTeamInGroup > noOfWinningTeam) {
              const check = noOfTeamInGroup % noOfWinningTeam != 0;
              return check ? { multiStageConfigError: true } : null;
            } else {
              return { multiStageConfigError: true };
            }
          };
          const errorConfig = fieldValidation(
            maxParticipants?.value,
            noOfTeamInGroup?.value,
            noOfWinningTeamInGroup?.value
          );

          if (!errorConfig) {
            const totalStage = (
              totalPlayer,
              noOfPlayerPerGroup,
              noOfWinning,
              noOfStage,
              previousStagePlayer
            ) => {
              let nextStagePlayer;
              const createStage = () => {
                nextStagePlayer =
                  Math.ceil(totalPlayer / noOfPlayerPerGroup) * noOfWinning;
                if (nextStagePlayer < previousStagePlayer) {
                  // noOfPlayerPerGroup = noOfPlayerPerGroup + 1;
                  nextStagePlayer =
                    Math.ceil(totalPlayer / noOfPlayerPerGroup) * noOfWinning;
                } else {
                  createStage();
                }
              };

              if (totalPlayer !== noOfPlayerPerGroup) {
                createStage();
              }
              return totalPlayer == noOfPlayerPerGroup
                ? noOfStage
                : !(Math.ceil(nextStagePlayer / noOfPlayerPerGroup) > 1)
                ? noOfStage + 1
                : Math.ceil(nextStagePlayer / noOfPlayerPerGroup) == 2
                ? noOfStage + 2
                : totalStage(
                    nextStagePlayer,
                    noOfPlayerPerGroup,
                    noOfWinning,
                    noOfStage + 1,
                    previousStagePlayer
                  );
            };

            const noOfStage = totalStage(
              maxParticipants?.value,
              noOfTeamInGroup?.value,
              noOfWinningTeamInGroup?.value,
              1,
              maxParticipants?.value
            );
            this.Step5.get('noOfStage').setValue(noOfStage);
          }

          if (field == 'noOfTeamInGroup') {
            return errorConfig;
          } else {
            this.Step5.get('noOfTeamInGroup').setErrors(errorConfig);
            return null;
          }
        }
      } else {
        return null;
      }
    };
  };

  durationValidator = (
    control: AbstractControl
  ): { [key: string]: boolean } => {
    const { days, hours, mins } = control.value;

    if (days || hours || mins) return null;

    return {
      required: true,
    };
  };

  // handler

  maxParticipantHandler(value, bracketType) {
    if (['battle_royale', 'round_robin'].includes(bracketType)) {
      const maxValidator =
        bracketType == 'round_robin'
          ? [Validators.max(value >= 24 ? 24 : value)]
          : [Validators.max(value)];
      this.Step5.get('noOfTeamInGroup').setValidators([
        Validators.required,
        Validators.min(2),
        ...maxValidator,
        this.teamInGroupValidation('noOfTeamInGroup').bind(this),
      ]);
      this.Step5.get('noOfWinningTeamInGroup').setValidators([
        Validators.required,
        Validators.min(1),
        this.teamInGroupValidation('noOfWinningTeamInGroup').bind(this),
      ]);
      this.Step5.get('noOfTeamInGroup').updateValueAndValidity();
      this.Step5.get('noOfWinningTeamInGroup').updateValueAndValidity();
    }
  }

  placementCounter = async (value) => {
    this.Step5.addControl('placementPoints', this.fb.array([]));
    const placementPointsList = this.Step5.get('placementPoints') as FormArray;

    const createPlacementForm = (position): FormGroup => {
      return this.fb.group({
        position: [position],
        value: [
          '',
          Validators.compose([
            Validators.required,
            Validators.pattern('^[0-9]*$'),
          ]),
        ],
      });
    };

    // remove placement node
    const deletePlacementForm = async (count, value) => {
      placementPointsList.removeAt(count);
      return count <= value ? true : await deletePlacementForm(--count, value);
    };

    // add placement node
    const addPlacementForm = async (count, value) => {
      placementPointsList.push(createPlacementForm(count));
      return count >= value ? true : await addPlacementForm(++count, value);
    };

    let count = placementPointsList.length;

    if (count > value) {
      await deletePlacementForm(count - 1, value);
    }

    if (count < value) {
      await addPlacementForm(count + 1, value);
    }
  };

  multiStageTournamentHandler = (
    isEnableFields: boolean,
    bracketType?,
    maxParticipants?
  ): void => {
    if (isEnableFields) {
      const noOfRound = bracketType == 'round_robin' ? 2 : 10;
      this.addNewControlAndUpdateValidity(
        'noOfTeamInGroup',
        'NUMBER',
        2,
        2,
        maxParticipants || this.Step5.value?.maxParticipants || 2,
        false,
        this.teamInGroupValidation('noOfTeamInGroup').bind(this)
      );
      this.addNewControlAndUpdateValidity(
        'noOfWinningTeamInGroup',
        'NUMBER',
        1,
        1,
        false,
        false,
        this.teamInGroupValidation('noOfWinningTeamInGroup').bind(this)
      );
      this.addNewControlAndUpdateValidity(
        'noOfRoundPerGroup',
        'NUMBER',
        1,
        1,
        noOfRound
      );
      this.addNewControlAndUpdateValidity('noOfStage', 'NUMBER', 1);
    } else {
      this.removeControlOrValidity('noOfTeamInGroup');
      this.removeControlOrValidity('noOfWinningTeamInGroup');
      this.removeControlOrValidity('noOfRoundPerGroup');
      this.removeControlOrValidity('noOfStage');
    }
  };

  bracketChangeHandler(
    type,
    noOfPlacement?,
    maxPlacement = 2,
    maxParticipants?,
    noOfRound?,
    noOfLoss?
  ) {
    let maximumParticipants = 1024;

    if (['round_robin', 'battle_royale'].includes(type)) {
      type == 'battle_royale'
        ? this.removeControlOrValidity('noOfSet')
        : this.addNewControlAndUpdateValidity('noOfSet', 'STRING', '');
      type == 'battle_royale'
        ? this.addNewControlAndUpdateValidity(
            'noOfPlacement',
            'NUMBER',
            2,
            2,
            this.Step5?.value?.noOfTeamInGroup || maxPlacement || 2
          )
        : this.removeControlOrValidity('noOfPlacement');
      type == 'battle_royale'
        ? this.placementCounter(noOfPlacement || 2)
        : this.removeControlOrValidity('placementPoints');
      type == 'battle_royale'
        ? this.addNewControlAndUpdateValidity(
            'isKillPointRequired',
            'BOOLEAN',
            false
          )
        : this.removeControlOrValidity('isKillPointRequired');
      type == 'battle_royale'
        ? this.removeControlOrValidity('stageBracketType')
        : this.addNewControlAndUpdateValidity('stageBracketType', 'STRING', '');
      type == 'battle_royale'
        ? this.removeControlOrValidity('allowAdvanceStage')
        : this.addNewControlAndUpdateValidity(
            'allowAdvanceStage',
            'BOOLEAN',
            false
          );

      this.multiStageTournamentHandler(true, type, maxParticipants);
    } else {
      this.multiStageTournamentHandler(false);
      this.addNewControlAndUpdateValidity('noOfSet', 'STRING', '');
      this.removeControlOrValidity('noOfPlacement');
      this.removeControlOrValidity('placementPoints');
      this.removeControlOrValidity('isKillPointRequired');
      this.removeControlOrValidity('stageBracketType');
      this.addNewControlAndUpdateValidity(
        'allowAdvanceStage',
        'BOOLEAN',
        false
      );
    }

    switch (type) {
      case 'single':
        maximumParticipants = 1024;
        break;
      case 'double':
        maximumParticipants = 512;
        break;
      case 'round_robin':
        maximumParticipants = 40;
        break;
      case 'battle_royale':
        maximumParticipants = 1024;
        break;
    }

    if (type == 'swiss_safeis') {
      this.addNewControlAndUpdateValidity(
        'noOfRound',
        'NUMBER',
        noOfRound || 1,
        1
      );
      this.addNewControlAndUpdateValidity(
        'noOfLoss',
        'NUMBER',
        noOfLoss || 0,
        0,
        (noOfRound || 1) - 1
      );
      this.addNewControlAndUpdateValidity('enableTiebreaker', 'BOOLEAN', false);
      this.Step5.addControl(
        'setDuration',
        this.fb.group(
          {
            days: [1, Validators.required],
            hours: [0, Validators.required],
            mins: [0, Validators.required],
          },
          { validator: this.durationValidator }
        )
      );
      this.Step5.get('isParticipantsLimit').setValue(false);
      this.removeControlOrValidity('maxParticipants');
      this.prizeLimit = 20;
    } else {
      this.removeControlOrValidity('noOfRound');
      this.removeControlOrValidity('noOfLoss');
      this.removeControlOrValidity('setDuration');
      this.removeControlOrValidity('enableTiebreaker');

      this.Step5.get('isParticipantsLimit').setValue(true);
      this.addNewControlAndUpdateValidity(
        'maxParticipants',
        'NUMBER',
        2,
        2,
        maximumParticipants,
        false,
        this.teamInGroupValidation('maxParticipants').bind(this)
      );
      this.prizeLimit = 6;
    }
  }

  noOfTeamsPerGroupHandler(value, bracketType) {
    if (bracketType === 'battle_royale') {
      this.Step5.get('noOfPlacement').setValidators([
        Validators.required,
        Validators.min(2),
        Validators.max(value || 2),
      ]);
      this.Step5.get('noOfPlacement').updateValueAndValidity();
    }
  }

  killingPointHandler = (isAllowKillingPoint) => {
    isAllowKillingPoint
      ? this.Step5.addControl(
          'pointsKills',
          new FormControl(1, [Validators.required, Validators.min(1)])
        )
      : this.Step5.removeControl('pointsKills');
    this.Step5.updateValueAndValidity();
  };

  roundHandler(noOfRound) {
    if (noOfRound)
      this.addNewControlAndUpdateValidity(
        'noOfLoss',
        'NUMBER',
        0,
        0,
        noOfRound - 1
      );
  }

  clickEvent(value) {
    this.dataFilter = value.name;
    this.desc = value.desc;
  }
  onStepChange(step) {
    if (this.Step5.get('bracketType').invalid) {
      this.toastService.showError('Please choose tournament format.');
      return;
    }
    this.Step5.get('step').setValue(step + 1);
  }
}
