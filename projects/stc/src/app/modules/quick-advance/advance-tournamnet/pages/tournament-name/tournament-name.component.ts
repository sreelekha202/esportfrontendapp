import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import {
  ControlContainer,
  FormGroup,
  FormGroupDirective,
  FormControl,
} from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { environment } from '../../../../../../environments/environment';
import { EsportsToastService, S3UploadService } from 'esports';
@Component({
  selector: 'app-tournament-name',
  templateUrl: './tournament-name.component.html',
  styleUrls: ['./tournament-name.component.scss'],
  viewProviders: [
    { provide: ControlContainer, useExisting: FormGroupDirective },
  ],
})
export class TournamentNameComponent implements OnInit {
  Step1: FormGroup | null;
  toggle: boolean = false;

  fileToUpload: File | null = null;
  checkFileName: boolean = false;
  toggleInputFile: boolean = null;
  advTournamentNameTextarea: string;
  bannerSRC: string = '';

  visibility: number = 1;
  dataVisibility = 1;
  visiblitytype = [
    {
      id: 2,
      name: 'Private',
      type: 'private',
      icon: 'assets/icons/matchmaking/platforms/single-player-2.svg',
      title: 'Single',
      createdBy: 'admin',
      createdOn: '2020-08-19T12:56:15.185Z',
      status: '1',
      updatedBy: 'admin',
      updatedOn: '2020-08-19T12:56:15.185Z',
      _id: 'aaa',
      is_selected: false,
    },
    {
      id: 1,
      name: 'Public',
      type: 'public',
      icon: 'assets/icons/matchmaking/platforms/team-2.svg',
      title: 'team',
      createdBy: 'admin1',
      createdOn: '2020-08-19T12:56:15.185Z',
      status: '2',
      updatedBy: 'admin',
      updatedOn: '2020-08-19T12:56:15.185Z',
      _id: 'bbb',
      is_selected: false,
    },
  ];

  constructor(
    private tournament: FormGroupDirective,
    private toastService: EsportsToastService,
    public s3Service: S3UploadService,
    private translateService: TranslateService
  ) {}

  ngOnInit(): void {
    this.Step1 = this.tournament.form;

    if (!this.Step1?.get('bannerSRC'))
      this.Step1.addControl('bannerSRC', new FormControl('', []));

    if (!this.Step1?.get('tournamentAddOnDescription'))
      this.Step1.addControl(
        'tournamentAddOnDescription',
        new FormControl('', [])
      );
  }

  onEnter(event: any): void {
    if (this.Step1.get('name').valid) {
      this.Step1.get('step').setValue(2);
    }
  }

  onStepChange(step: number): void {
    if (this.Step1.get('name').invalid) {
      this.toastService.showError('Please enter tournament name.');
      return;
    }
    this.Step1.get('step').setValue(step + 1);
  }

  clickEvent() {
    this.toggle = !this.toggle;
  }

  handleFileInput(files: FileList) {
    this.isValidImage(files, { width: 1920, height: 300 });
  }

  isValidImage(files: any, values): Boolean {
    if (files && files[0]) {
      if (files[0]['size'] / (1000 * 1000) > 2) {
        this.toastService.showError(
          this.translateService.instant('TOURNAMENT.ERROR_IMG_2')
        );
        return;
      }

      if (
        files[0].type != 'image/jpeg' &&
        files[0].type != 'image/jpg' &&
        files[0].type != 'image/png'
      ) {
        this.toastService.showError(
          this.translateService.instant('TOURNAMENT.ERROR_IMG_1')
        );
        return;
      }

      try {
        const file = files[0];

        const reader = new FileReader();
        reader.readAsDataURL(file);
        let th = this;
        reader.onload = (e) => {
          let image: any = new Image();
          image.src = e.target.result;
          image.onload = function () {
            let height = this.height;
            let width = this.width;
            if (height != values['height'] && width != values['width']) {
              th.toastService.showInfo(
                th.translateService.instant('TOURNAMENT.ERROR_IMG')
              );
            }
            th.upload(files);
          };
        };
      } catch (err) {
        return false;
      }
    } else {
      return false;
    }
  }

  async upload(files) {
    let promises = [];
    for (let obj of files) {
      promises.push(this.toBase64(obj));
    }
    let Base64String = await Promise.all(promises);
    let imageData = {
      path: environment.tournamentS3BucketName,
      files: Base64String,
    };

    this.s3Service.fileUpload(environment.apiEndPoint, imageData).subscribe(
      (res) => {
        this.bannerSRC = res['data'][0]['Location'];
        this.Step1?.get('bannerSRC')?.setValue(this.bannerSRC);
        this.toggleInputFile = !this.toggleInputFile;
        this.checkFileName = true;
        this.fileToUpload = files.item(0);
        this.toastService.showSuccess(
          this.translateService.instant('TOURNAMENT.BANNER_UPLOADED')
        );
      },
      (err) => {
        this.toastService.showError(
          this.translateService.instant('TOURNAMENT.ERROR_LOGO')
        );
      }
    );
  }

  toBase64(file) {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = (error) => reject(error);
    });
  }

  onOverviewChangAdvance = (text) => {
    this.advTournamentNameTextarea = text;
    this.Step1?.get('tournamentAddOnDescription')?.setValue(
      this.advTournamentNameTextarea
    );
  };

  clickCheckedVisibility(checked) {
    this.dataVisibility = checked.id;
    this.visibility = checked.id;
    checked.ichecked = true;
  }
}
