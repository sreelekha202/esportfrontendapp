import { Component, OnInit } from '@angular/core';
import {
  ControlContainer,
  FormControl,
  FormGroupDirective,
  Validators,
} from '@angular/forms';
import { el } from 'date-fns/locale';
import { EsportsToastService, EsportsTournamentService } from 'esports';

@Component({
  selector: 'app-entrance-fee',
  templateUrl: './entrance-fee.component.html',
  styleUrls: ['./entrance-fee.component.scss'],
  viewProviders: [
    { provide: ControlContainer, useExisting: FormGroupDirective },
  ],
})
export class EntranceFeeComponent implements OnInit {
  weightageInput: String = '';
  Step6;
  payouts = [0];
  sponsors = [0];
  isPaidFlag = false;
  entranceFee: number = null;
  numberOfParticipants: number = null;
  selectCurrency: string = 'USD';
  selectCurrencyWagger: string = 'USD';
  calculatedWaggerPoolAmount: number;
  selectPay: string = '';
  selectPayC1: string = '';
  list = [
    { name: 'Yes', value: true },
    { name: 'No', value: false },
  ];
  typeOfChecked = {};
  waggerPool = {
    fee: this.entranceFee,
    payout: '',
    currency: 'USD',
    participants: this.numberOfParticipants,
    amounts: this.payouts,
    prize: [],
  };
  fromSponpors = {
    entranceFee: 150,
    payout: '',
    currency: 'USD',
    amounts: this.sponsors,
    prize: [],
  };
  dataCashPrizesActive = false;
  dataCashPrizesIDItem = null;
  dataCashPrizesPool = false;
  dataCashPrizesSponsors = false;
  dataCashPrizesNoCash = false;
  dataCashPrizes = [
    {
      id: 0,
      textTitle: 'From a wager pool',
      img: 'assets/icons/matchmaking/cash-prize/Icon-pool.svg',
    },
    {
      id: 1,
      textTitle: 'From organizer',
      img: 'assets/icons/matchmaking/cash-prize/Icon-sponsors.svg',
    },
    {
      id: 2,
      textTitle: 'No cash prizes',
      img: 'assets/icons/matchmaking/cash-prize/Icon-No-cash.svg',
    },
  ];
  currencies = ['USD', 'VND', 'DKK'];
  payoutsMethod = [
    'Select payout method 1',
    'Select payout method 2',
    'Select payout method 3',
  ];
  payoutsMethodC1 = [
    'Select payout method 1',
    'Select payout method 2',
    'Select payout method 3',
  ];

  constructor(
    private tournament: FormGroupDirective,
    private eSportsTournamentService: EsportsTournamentService,
    private toastService: EsportsToastService
  ) { }

  ngOnInit(): void {
    this.Step6 = this.tournament.form;
    this.Step6.addControl('isPaid', new FormControl(false));

    if (!this.Step6.value.waggerPoolDetails)
      this.Step6.addControl('waggerPoolDetails', new FormControl(''));

    if (!this.Step6.value.organizerDetails)
      this.Step6.addControl('organizerDetails', new FormControl(''));

    if (this.Step6.value.isPaid)
      this.clickChecked(this.Step6.value.isPaid)

    if (this.Step6.get('isPaid').value) this.isPaidFlag = true;
  }

  addPayout(): void {
    let idP = this.payouts[this.payouts.length - 1];
    this.payouts.push(idP + 1);
  }

  addSponsor(): void {
    let idP = this.sponsors[this.sponsors.length - 1];
    this.sponsors.push(idP + 1);
  }

  onToggleDelete(id): void {
    const indexPayout = this.payouts.findIndex((p) => p === id);
    if (indexPayout < 0) return;

    const newPayout = this.payouts;
    if (newPayout.length > 1) {
      newPayout.splice(indexPayout, 1);
    }
    this.payouts = newPayout;
  }

  onToggleDeleteSponsor(id): void {
    const indexSponsor = this.sponsors.findIndex((p) => p === id);
    if (indexSponsor < 0) return;

    const newSponsor = this.sponsors;
    if (newSponsor.length > 1) {
      newSponsor.splice(indexSponsor, 1);
    }
    this.sponsors = newSponsor;
  }

  onToggleEntranceFee(value): void {
    this.Step6.get('isPaid').setValue(value);
    this.paidRegistrationHandler(value);
  }

  paidRegistrationHandler(check) {
    if (check) {
      const regCurrencyValidators = Validators.compose([Validators.required]);
      const regFeeValidators = Validators.compose([
        Validators.required,
        Validators.min(1),
      ]);
      this.Step6.addControl(
        'regFeeCurrency',
        new FormControl('KWD', regCurrencyValidators)
      );
      this.Step6.addControl('regFee', new FormControl(1, regFeeValidators));
    } else {
      this.Step6.removeControl('regFeeCurrency');
      this.Step6.removeControl('regFee');
    }
  }

  clickChecked(item) {
    this.typeOfChecked = item;
    if (item.id == 0) {
      this.dataCashPrizesIDItem = item.id;
      this.dataCashPrizesActive = true;
      this.dataCashPrizesPool = true;
      this.dataCashPrizesSponsors = false;
      this.dataCashPrizesNoCash = false;
      this.Step6.get('isPaid').setValue(item);
    }
    if (item.id == 1) {
      this.dataCashPrizesIDItem = item.id;
      this.dataCashPrizesActive = true;
      this.dataCashPrizesPool = false;
      this.dataCashPrizesSponsors = true;
      this.dataCashPrizesNoCash = false;
      this.Step6.get('isPaid').setValue(item);
    }
    if (item.id == 2) {
      this.dataCashPrizesIDItem = item.id;
      this.dataCashPrizesActive = true;
      this.dataCashPrizesPool = false;
      this.dataCashPrizesSponsors = false;
      this.dataCashPrizesNoCash = true;
      this.Step6.get('isPaid').setValue(item);
    }
  }

  onStepChange(step) {
    // if (
    //   this.Step6.get('isPaid').value &&
    //   step > this.Step6.get('step').value &&
    //   !this.isPaidFlag
    // ) {
    //   this.eSportsTournamentService.totalPages.next(
    //     this.eSportsTournamentService.totalPages.value + 1
    //   );
    //   this.Step6.get('step').setValue(step + 1);
    // }
    if (!this.Step6.value.isPaid || !this.Step6.value.isPaid.textTitle) {
      this.toastService.showError('Please choose cash prize.');
      return;
    }
    this.Step6.get('organizerDetails').setValue(this.fromSponpors);
    this.Step6.get('waggerPoolDetails').setValue(this.waggerPool);
    /*this.eSportsTournamentService.totalPages.next(
      this.eSportsTournamentService.totalPages.value + 1
    );*/
    this.Step6.get('step').setValue(step + 1);
  }

  // Handlechange currency
  onToggleChangeCurrency(cur): void {
    this.selectCurrency = cur;
    this.fromSponpors = { ...this.fromSponpors, currency: cur };
  }

  // Handlechange currency for waggerpool
  onToggleChangeCurrencyWagger(cur): void {
    this.selectCurrencyWagger = cur;
    this.waggerPool = { ...this.waggerPool, currency: cur };
  }

  // Handlechange payloadMethod Component 2
  onToggleChangePayouts(pay): void {
    this.selectPay = pay;
    this.fromSponpors = { ...this.fromSponpors, payout: pay };
  }

  // Handlechange payloadMethod Component 1
  onToggleChangePayoutsC1(pay): void {
    this.selectPayC1 = pay;
    this.waggerPool = { ...this.waggerPool, payout: pay };
  }

  handleChangeComponent1(e) {
    this.calculatedWaggerPoolAmount = (((this.entranceFee && this.entranceFee != 0) && (this.numberOfParticipants && this.numberOfParticipants != 0)) ? (this.numberOfParticipants * this.entranceFee) : null)
    this.waggerPool = {
      ...this.waggerPool,
      [e.target.name]: e.target.value,
    };
  }
  // Handlechange waggerPool Component 1
  handleChangeWeightC1(e, ind) {
    const name = e.target.name;
    const value = e.target.value;
    let prizeVo: any = (this.waggerPool.prize.length ? this.waggerPool.prize : []);
    let prize: any = {};
    if (!this.waggerPool.prize[ind] || !this.waggerPool.prize[ind].from || !this.waggerPool.prize[ind].to || !this.waggerPool.prize[ind].weightage) {
      prize = {
        from: (name === 'fromNth' ? value : ((this.waggerPool.prize.length && this.waggerPool.prize[ind]) ? this.waggerPool.prize[ind].from : '')),
        to: (name === 'toNth' ? value : ((this.waggerPool.prize.length && this.waggerPool.prize[ind]) ? this.waggerPool.prize[ind].to : '')),
        weightage: (name === 'weightage' ? value : ((this.waggerPool.prize.length && this.waggerPool.prize[ind]) ? this.waggerPool.prize[ind].weightage : ''))
      };
    } else {
      prize.from = (name === 'fromNth' ? value : ((this.waggerPool.prize.length && this.waggerPool.prize[ind]) ? this.waggerPool.prize[ind].from : ''));
      prize.to = (name === 'toNth' ? value : ((this.waggerPool.prize.length && this.waggerPool.prize[ind]) ? this.waggerPool.prize[ind].to : ''));
      prize.weightage = (name === 'weightage' ? value : ((this.waggerPool.prize.length && this.waggerPool.prize[ind]) ? this.waggerPool.prize[ind].weightage : ''));
    }

    prizeVo[ind] = prize;

    this.waggerPool = {
      ...this.waggerPool,
      prize: prizeVo,
    };
  }

  // Handlechange sponsors Component 2
  handleChangeWeightC2(e, ind) {
    const name = e.target.name;
    const value = e.target.value;
    let prizeVo: any = (this.fromSponpors.prize.length ? this.fromSponpors.prize : []);
    let prize: any = {};
    if (!this.fromSponpors.prize[ind] || !this.fromSponpors.prize[ind].from || !this.fromSponpors.prize[ind].to || !this.fromSponpors.prize[ind].weightage) {
      prize = {
        from: (name === 'fromNth' ? value : ((this.fromSponpors.prize.length && this.fromSponpors.prize[ind]) ? this.fromSponpors.prize[ind].from : '')),
        to: (name === 'toNth' ? value : ((this.fromSponpors.prize.length && this.fromSponpors.prize[ind]) ? this.fromSponpors.prize[ind].to : '')),
        weightage: (name === 'weightage' ? value : ((this.fromSponpors.prize.length && this.fromSponpors.prize[ind]) ? this.fromSponpors.prize[ind].weightage : '')),
      };
    } else {
      prize.from = (name === 'fromNth' ? value : ((this.fromSponpors.prize.length && this.fromSponpors.prize[ind]) ? this.fromSponpors.prize[ind].from : ''));
      prize.to = (name === 'toNth' ? value : ((this.fromSponpors.prize.length && this.fromSponpors.prize[ind]) ? this.fromSponpors.prize[ind].to : ''));
      prize.weightage = (name === 'weightage' ? value : ((this.fromSponpors.prize.length && this.fromSponpors.prize[ind]) ? this.fromSponpors.prize[ind].weightage : ''));
    }

    prizeVo[ind] = prize;

    this.fromSponpors = {
      ...this.fromSponpors,
      prize: prize,
    };
  }

}
