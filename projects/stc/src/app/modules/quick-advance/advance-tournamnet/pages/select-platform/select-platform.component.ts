import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Subscription } from 'rxjs';
import {
  ControlContainer,
  FormGroup,
  FormGroupDirective,
  FormControl,
} from '@angular/forms';
import {
  EsportsTournamentService,
  EsportsToastService,
  GlobalUtils,
} from 'esports';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-select-platform',
  templateUrl: './select-platform.component.html',
  styleUrls: ['./select-platform.component.scss'],
  viewProviders: [
    { provide: ControlContainer, useExisting: FormGroupDirective },
  ],
})
export class SelectPlatformComponent implements OnInit {
  @Input() number;
  @Output() numberChange = new EventEmitter();
  Step3: FormGroup | null;
  selectedPlatformIndex = null;
  selectedPlatformIndex1 = null;
  tournamentDetails: Subscription;
  platforms = [];
  gametype = [];
  createTournament: any;
  bracketType: any;
  dataFilter = '';
  dataFilterTeamDropdownItem = '';
  valuePlatform2 = null;
  activeButton = '';
  dataTeamDropdownItem = [
    {
      id: 0,
      text: 'Duo',
      subText: 'Each participating team will comprise of 2 players',
    },
    {
      id: 1,
      text: 'Squad',
      subText: 'Each participating team will comprise of 4 players',
    },
    {
      id: 2,
      text: 'X vs X',
      subText: 'Set the number of players allowed per team',
    },
  ];
  constructor(
    public eSportsTournamentService: EsportsTournamentService,
    private tournament: FormGroupDirective,
    private toastService: EsportsToastService,
    private translate: TranslateService
  ) {}

  ngOnInit(): void {
    if (GlobalUtils.isBrowser()) {
      this.Step3 = this.tournament.form;

      if (!this.Step3?.get('tournamentType'))
        this.Step3.addControl('tournamentType', new FormControl('', []));

      if (!this.Step3?.get('teamTournamentType'))
        this.Step3.addControl('teamTournamentType', new FormControl('', []));

      if (
        this.Step3.value.tournamentType &&
        this.Step3.value.tournamentType != 'online'
      )
        this.setPlatform2(this.Step3.value.tournamentType == 'Team' ? 1 : 0);

      if (
        this.Step3.value.teamTournamentType &&
        this.Step3.value.tournamentType &&
        this.Step3.value.tournamentType != 'online'
      )
        this.clickChange(this.Step3.value.teamTournamentType);

      this.gametype = [
        {
          name: 'Single',
          type: 'single',
          icon: 'assets/icons/matchmaking/platforms/single-player-2.svg',
          title: 'Single',
          createdBy: 'admin',
          createdOn: '2020-08-19T12:56:15.185Z',
          status: '1',
          updatedBy: 'admin',
          updatedOn: '2020-08-19T12:56:15.185Z',
          _id: 'aaa',
          is_selected: false,
        },
        {
          name: 'Team',
          type: 'team',
          icon: 'assets/icons/matchmaking/platforms/team-2.svg',
          title: 'team',
          createdBy: 'admin1',
          createdOn: '2020-08-19T12:56:15.185Z',
          status: '2',
          updatedBy: 'admin',
          updatedOn: '2020-08-19T12:56:15.185Z',
          _id: 'bbb',
          is_selected: false,
        },
      ];
      this.tournamentDetails =
        this.eSportsTournamentService.createTournament.subscribe((data) => {
          if (data) {
            this.createTournament = data;

            if (
              this.createTournament.selectedGame &&
              this.createTournament.selectedGame.platform &&
              this.createTournament.selectedGame.platform.length > 0
            )
              this.createTournament.selectedGame.platform.map((obj) => {
                if (obj.name == 'pc') {
                  this.platforms.push({
                    ...obj,
                    type: 'pc',
                    icon: 'assets/icons/matchmaking/platforms/pc.svg',
                    title: 'MATCHMAKING.SELECT_PLATFORM.PLATFORMS.PC',
                  });
                } else if (obj.name == 'Mobile') {
                  this.platforms.push({
                    ...obj,
                    type: 'mobile',
                    icon: 'assets/icons/matchmaking/platforms/mobile.svg',
                    title: 'MATCHMAKING.SELECT_PLATFORM.PLATFORMS.MOBILE',
                  });
                } else if (obj.name == 'Other') {
                  this.platforms.push({
                    ...obj,
                    type: 'console',
                    icon: 'assets/icons/matchmaking/platforms/console.svg',
                    title: 'MATCHMAKING.SELECT_PLATFORM.PLATFORMS.CONSOLE',
                  });
                } else if (obj.name == 'Xbox') {
                  this.platforms.push({
                    ...obj,
                    type: 'xbox',
                    icon: 'assets/icons/matchmaking/platforms/xbox.svg',
                    title: 'MATCHMAKING.SELECT_PLATFORM.PLATFORMS.XBOX',
                  });
                } else if (obj.name == 'PS4') {
                  this.platforms.push({
                    ...obj,
                    type: 'ps4',
                    icon: 'assets/icons/matchmaking/platforms/ps4.svg',
                    title: 'MATCHMAKING.SELECT_PLATFORM.PLATFORMS.PS4',
                  });
                } else if (obj.name == 'PS5') {
                  this.platforms.push({
                    ...obj,
                    type: 'ps5',
                    icon: 'assets/icons/matchmaking/platforms/ps5.svg',
                    title: 'MATCHMAKING.SELECT_PLATFORM.PLATFORMS.PS5',
                  });
                } else {
                  this.platforms.push({
                    ...obj,
                    type: 'pc',
                    icon: 'assets/icons/matchmaking/platforms/pc.svg',
                    title: 'MATCHMAKING.SELECT_PLATFORM.PLATFORMS.PC',
                  });
                }
              });

            if (localStorage.hasOwnProperty('selectedPlatformIndex')) {
              this.setPlatform(
                Number(localStorage.getItem('selectedPlatformIndex'))
              );
            }
            if (localStorage.hasOwnProperty('selectedPlatformIndex1')) {
              this.setPlatform2(
                Number(localStorage.getItem('selectedPlatformIndex1'))
              );
            }
          }
        });
    }
  }

  onSelectPlatform(id: string) {
    this.Step3?.get('platform')?.setValue(id);
  }

  //number counter input box
  plusNumber(): void {
    this.number = (this.number === undefined ? 0 : this.number) + 1;
    this.numberChange.emit(this.number);
  }
  minusNumber(): void {
    if (this.number > 1) {
      this.number = this.number - 1;
      this.numberChange.emit(this.number);
    }
  }
  onChange() {
    this.numberChange.emit(this.number);
    this.Step3?.get('noOfParticipants')?.setValue(this.number);
  }

  onStepChange(step: number) {
    if (this.Step3?.get('platform')?.invalid) {
      this.toastService.showError(
        this.translate.instant('SELECT_PLATFORM.ERR_CHOOSE_PLATFORM')
      );
      return;
    }
    if (this.valuePlatform2 === null) {
      this.toastService.showError(
        this.translate.instant('SELECT_PLATFORM.ERR_CHOOSE_TOURNAMENT_TYPE')
      );
      return;
    }
    if (this.valuePlatform2 == 1) {
      this.Step3?.get('tournamentType')?.setValue('Team');
    } else {
      this.Step3?.get('tournamentType')?.setValue('Single');
    }
    this.Step3?.get('step')?.setValue(step + 1);
  }
  setPlatform(i) {
    this.selectedPlatformIndex = i;
  }
  setPlatform2(i) {
    this.selectedPlatformIndex1 = i;
    this.valuePlatform2 = i;
  }
  clickEvent(value) {
    this.dataFilter = value.text;
  }
  clickChange(value) {
    this.dataFilterTeamDropdownItem = value;
    this.activeButton = value;
    if (this.valuePlatform2 == 1) {
      this.Step3?.get('teamTournamentType')?.setValue(
        this.dataFilterTeamDropdownItem
      );
    }
  }
}
