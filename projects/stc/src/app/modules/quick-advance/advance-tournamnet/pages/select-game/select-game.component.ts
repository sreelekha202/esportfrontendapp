import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AppHtmlRoutes } from '../../../../../app-routing.model';
import { environment } from '../../../../../../environments/environment';

const API = environment.apiEndPoint;

import {
  ControlContainer,
  FormControl,
  FormGroup,
  FormGroupDirective,
} from '@angular/forms';
import {
  EsportsTournamentService,
  EsportsToastService,
} from 'esports';

@Component({
  selector: 'app-select-game',
  templateUrl: './select-game.component.html',
  styleUrls: ['./select-game.component.scss'],
  viewProviders: [
    { provide: ControlContainer, useExisting: FormGroupDirective },
  ],
})
export class SelectGameComponent implements OnInit {
  Step2: FormGroup | null;
  text: string = '';
  tournamentDetails: Subscription;
  createTournament: any;
  AppHtmlRoutes = AppHtmlRoutes;
  games = [];
  selectedGame = null;
  showLoader: boolean = true;
  constructor(
    public eSportsTournamentService: EsportsTournamentService,
    private tournament: FormGroupDirective,
    private toastService: EsportsToastService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.Step2 = this.tournament.form;
    this.tournamentDetails = this.eSportsTournamentService.createTournament.subscribe(
      (data) => {
        if (data) {
          this.createTournament = data;
        }
      }
    );
    this.getGames();
  }

  onTextChange = (data) => {
    this.text = data;
    // search on API
    //  this.getGames();
  };

  // search on Array
  filterGame = () => {
    return this.games.filter((game) =>
      game.name.includes(this.text.toLowerCase().trim())
    );
  };

  getGames() {
    this.showLoader = true;
    const param: any = {};
    this.text ? (param.search = this.text) : '';
    this.eSportsTournamentService.getAllGames(param).subscribe(
      (res) => {
        this.showLoader = false;
        if (res && res.data && res.data.length) {
          res.data.map((obj) => {
            this.games.push({ ...obj, name: String(obj.name).toLowerCase() });
          });
        }
      },
      (err) => {
        this.showLoader = false;
      }
    );
  }

  onSelectGame(game: any) {
    const param: any = {};
    this.text ? (param.search = this.text) : '';
    this.eSportsTournamentService.setPlatformList = game?.platform;
    if (game?.isQuickFormatAllow && this.Step2?.value?.type == 'quick') {
      this.Step2.addControl('noOfTeamInGroup', new FormControl(''))
      this.Step2.addControl('noOfWinningTeamInGroup', new FormControl(''))
      this.Step2.addControl('noOfRoundPerGroup', new FormControl(''))
      this.Step2.addControl('stageBracketType', new FormControl(''))
      this.Step2.patchValue({
        ...game.quickFormatConfig,
      });
    } else {
      this.eSportsTournamentService.setBracketTypes = game?.bracketTypes;
    }

    this.Step2.get('gameDetail').setValue(game?._id);
  }

  onStepChange(step: number): void {
    if (
      this.Step2?.get('gameDetail')?.invalid
      // && this.Step2?.get('step')?.value < step
    ) {
      this.toastService.showError('Please select the game.');
      return;
    }
    this.Step2?.get('step')?.setValue(step + 1);
  }
}
