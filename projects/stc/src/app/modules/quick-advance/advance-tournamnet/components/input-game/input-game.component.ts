import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-input-game',
  templateUrl: './input-game.component.html',
  styleUrls: ['./input-game.component.scss'],
})
export class InputGameComponent implements OnInit {
  @Input() placeholder: string = '';
  @Input() text: string = '';
  @Output() onTextChange = new EventEmitter<string>();

  constructor() {}
  ngOnInit(): void {}

  onSearchTextChange = () => this.onTextChange.emit(this.text);
}
