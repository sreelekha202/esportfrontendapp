import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
@Component({
  selector: 'app-tournament-date',
  templateUrl: './tournament-date.component.html',
  styleUrls: ['./tournament-date.component.scss'],
})
export class TournamentDateComponent implements OnInit {
  @Input() title: string = '';
  @Input() placeholder: string = '';
  @Input() value: string | number = '';
  @Input() isBasic: boolean = false;
  @Input() isDisabled: boolean = false;
  @Input() customFormControlName: string;
  @Input() customFormGroup: FormGroup;
  @Input() minDate: Date | null;
  @Input() types: number;
  @Input() page?: Boolean = true;

  @Output() submit = new EventEmitter();

  stepSecond: number = 0;
  constructor() {}
  ngOnInit(): void {}
  onSubmit() {
    this.submit.next(true);
  }
}
