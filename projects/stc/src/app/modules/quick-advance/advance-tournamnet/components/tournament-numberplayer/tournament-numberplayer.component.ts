import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup, FormGroupDirective } from '@angular/forms';

@Component({
  selector: 'app-tournament-numberplayer',
  templateUrl: './tournament-numberplayer.component.html',
  styleUrls: ['./tournament-numberplayer.component.scss'],
})
export class TournamentNumberplayerComponent implements OnInit {
  constructor(private tournament: FormGroupDirective) {}

  ngOnInit(): void {
    this.Step5 = this.tournament?.form;
    this.number = this.Step5.value.noOfParticipants
      ? this.Step5.value.noOfParticipants
      : 0;
  }

  number: number = 0;
  Step5: FormGroup | null;
  @Input() customFormGroup: FormGroup;
  @Input() label?: Boolean = true;

  plusNumber(): void {
    this.number = this.number + 1;
    this.Step5?.get('noOfParticipants').setValue(this.number);
  }
  minusNumber(): void {
    if (this.number > 0) {
      this.number = this.number - 1;
      this.Step5?.get('noOfParticipants').setValue(this.number);
    }
  }

  updateChange(val) {
    this.Step5?.get('noOfParticipants').setValue(val);
  }
}
