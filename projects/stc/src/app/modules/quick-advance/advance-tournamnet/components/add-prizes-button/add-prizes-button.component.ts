import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-add-prizes-button',
  templateUrl: './add-prizes-button.component.html',
  styleUrls: ['./add-prizes-button.component.scss'],
})
export class AddPrizesButtonComponent implements OnInit {
  @Input() text: string = 'Add more prizes';

  constructor() {}

  ngOnInit(): void {}
}
