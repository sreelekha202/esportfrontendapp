import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AppHtmlRoutes } from '../../../../../app-routing.model';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  @Input() isFirstPage: boolean = false;
  @Input() isAdvance: boolean = false;
  @Output() backClick = new EventEmitter();

  AppHtmlRoutes = AppHtmlRoutes;
  checkQuick8 = false;

  constructor(private router: Router, private activeRoute: ActivatedRoute) {}

  ngOnInit(): void {}

  navigateToInfoPage(params: string) {
    this.router.navigate([params], { relativeTo: this.activeRoute });
  }

  onCloseCLick() {
    this.router.navigate(['play']);
  }

  onBackClick() {
    this.backClick.emit();

    /* set selected tournament type here*/
    if (localStorage.getItem('selectedTournamentType')) {
      setTimeout(() => {
        localStorage.removeItem('selectedTournamentType');
      }, 500);
    }
  }
}
