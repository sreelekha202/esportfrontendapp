import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'platformPipe',
})
export class PlatformPipe implements PipeTransform {
  transform(value) {
    const platforms = [
      {
        image: 'assets/icons/matchmaking/platforms/pc.svg',
        name: 'pc',
      },
      {
        image: 'assets/icons/matchmaking/platforms/mobile.svg',
        name: 'mobile',
      },
      {
        image: 'assets/icons/matchmaking/platforms/console.svg',
        name: 'ps4',
      },
      {
        image: 'assets/icons/matchmaking/platforms/console.svg',
        name: 'ps5',
      },
      {
        image: 'assets/icons/matchmaking/platforms/console.svg',
        name: 'others',
      },
    ];
    const p = platforms.find(
      (p) => p.name.toLowerCase() === value.toLowerCase()
    );
    return p ? p.image : 'assets/icons/matchmaking/platforms/console.svg';
  }
}
