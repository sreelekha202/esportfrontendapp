import { Component, Input, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  @Input() isFirstPage: boolean = false;
  @Input() isAdvance: boolean = false;

  constructor(
    private router: Router,
    private activeRoute: ActivatedRoute
  ) { }

  ngOnInit(): void { }

  navigateToInfoPage(params: string) {
    this.router.navigate([params], { relativeTo: this.activeRoute });
  }
}
