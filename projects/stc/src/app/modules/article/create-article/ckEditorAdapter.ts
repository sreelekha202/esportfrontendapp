import { TranslateService } from '@ngx-translate/core';
import { environment } from '../../../../environments/environment';

export class UploadAdapter {
  public loader: any;
  public s3Service;
  public toastService;
  constructor(
    loader,
    s3Service: any,
    toastService: any,
    private translateService: TranslateService
  ) {
    this.loader = loader;
    this.s3Service = s3Service;
    this.toastService = toastService;
    this.translateService = translateService;
  }

  upload() {
    return new Promise(async (resolve, reject) => {
      this.loader.file.then(async (file) => {
        // let selectedFiles = file;
        // let ckFormData = new FormData();
        // ckFormData.append('file', selectedFiles);
        const files = [];
        files[0] = file;
        const promises = [];
        for (const obj of files) {
          promises.push(this.toBase64(obj));
        }
        const Base64String = await Promise.all(promises);
        const imageData = {
          path: environment.articleS3BucketName,
          files: Base64String,
        };

        this.s3Service.fileUpload(imageData).subscribe(
          (res) => {
            resolve({ default: res.data[0].Location });
          },
          (err) => {
            if (err.status == 401) {
              this.toastService.showError(
                this.translateService.instant('ARTICLE_POST.SESSION_EXPIRED')
              );
              reject('Please Login');
            }
            reject(err.error.message);
          }
        );
      });
    });
  }

  abort() {}

  toBase64(file) {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = (error) => reject(error);
    });
  }
}
