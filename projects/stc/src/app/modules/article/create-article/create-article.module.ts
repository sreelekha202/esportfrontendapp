import { ShareModule } from 'ngx-sharebuttons';
import { PaginationModule, PaginationConfig } from 'ngx-bootstrap/pagination';
import { PipeModule, WYSIWYGEditorModule } from 'esports';

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from '../../../shared/modules/shared.module';
import { CreateArticleComponent } from './create-article.component';
import { CoreModule } from '../../../core/core.module';
import { AuthorAccessGuard } from '../../../shared/guard/isAuthorInfluencer.guard';
import {
  FontAwesomeModule
} from '@fortawesome/angular-fontawesome';
import { environment } from '../../../../environments/environment';

const routes: Routes = [
  {
    path: '',
    canActivate: [AuthorAccessGuard],
    component: CreateArticleComponent,
  },
  // {
  //   path: 'create',
  //   canActivate: [AuthorAccessGuard],
  //   component: CreateArticleComponent,
  // }
];

@NgModule({
  declarations: [CreateArticleComponent],
  imports: [
    SharedModule,
    CoreModule,
    ShareModule,
    WYSIWYGEditorModule.forRoot(environment),
    FontAwesomeModule,
    PaginationModule.forRoot(),
    PipeModule,
    RouterModule.forChild(routes),
  ],
  providers: [{ provide: PaginationConfig }],
})
export class CreateArticleModule {}
