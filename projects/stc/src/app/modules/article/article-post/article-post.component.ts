import { Title } from '@angular/platform-browser';
import { TranslateService } from '@ngx-translate/core';
import { MatDialog } from '@angular/material/dialog';
import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import 'rxjs/add/operator/filter';
import {
  ArticleApiService,
  CommentService,
  UserPreferenceService,
  HomeService,
  DeeplinkService,
} from '../../../core/service';
import {
  InfoPopupComponent,
  InfoPopupComponentData,
  InfoPopupComponentType,
} from '../../../shared/popups/info-popup/info-popup.component';
import { toggleHeight } from '../../../animations';
import { DOCUMENT } from '@angular/common';
import { IArticle } from '../../../shared/models';
import {
  IUser,
  EsportsUserService,
  EsportsLanguageService,
  EsportsConstantsService,
  EsportsToastService,
  GlobalUtils,
  CustomTranslatePipe,
  EsportsGtmService,
  SuperProperties,
} from 'esports';
import { Subscription } from 'rxjs';
@Component({
  selector: 'app-article-post',
  templateUrl: './article-post.component.html',
  styleUrls: ['./article-post.component.scss'],
  animations: [toggleHeight],
  providers: [CustomTranslatePipe],
})
export class ArticlePostComponent implements OnInit, OnDestroy {
  article: IArticle;
  user: IUser;
  relatedArticles = [];
  showLoader = false;
  active: any = 1;
  currentLang;
  userSubscription: Subscription;
  loadPageWithData = false;
  constructor(
    private articleApiService: ArticleApiService,
    private activatedRoute: ActivatedRoute,
    private eSportsToastService: EsportsToastService,
    private userService: EsportsUserService,
    private homeService: HomeService,
    public language: EsportsLanguageService,
    public userPreferenceService: UserPreferenceService,
    public matDialog: MatDialog,
    private translateService: TranslateService,
    private router: Router,
    private globalUtils: GlobalUtils,
    private titleService: Title,
    private customTranslatePipe: CustomTranslatePipe,
    @Inject(DOCUMENT) private document: Document,
    private commentService: CommentService,
    public languageService: EsportsLanguageService,
    private deeplinkService: DeeplinkService,
    private gtmServices:EsportsGtmService,
  ) {}

  ngOnInit(): void {
    this.currentLang =
      this.translateService.currentLang == 'en' ? 'english' : 'arabic';
    this.userSubscription = this.activatedRoute.params.subscribe((params) => {
      if (params?.id) {
        this.getArticleBySlug(params?.id);
        if (GlobalUtils.isBrowser()) {
          this.deeplinkService.deeplink({
            objectType: 'article',
            objectId: params?.id,
          });
        }
      }
    });
    this.getCurrentUserDetails();
    this.gtmEvntLog("Post_Views");
  }

  ngOnDestroy() {
    this.userSubscription?.unsubscribe();
  }

  getArticleBySlug = async (slug) => {
    try {
      this.showLoader = true;
      const active = this.activatedRoute.snapshot.queryParamMap.get('active');
      this.active = active ? active : '1';

      const article: any = await this.articleApiService.getArticleBySlug(slug);
      if (article.data) {
        this.loadPageWithData = true;
        this.article = article.data;
      }

      if (!this.article) this.router.navigate(['/404']);
      this.article.content.english = this.article.content.english.replace(/<a /gi,"<a (click)=\"gtmEvntLog(\'Embedded_Link_Click\')\"");
      this.showLoader = false;
      this.setMetaTags();
      this.updateView();
      this.getRelatedArticles();
      this.showLoader = false;
    } catch (error) {
      this.showLoader = false;
      this.eSportsToastService.showError(
        error?.error?.message || error?.message
      );
    }
  };

  getCurrentUserDetails() {
    this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.user = data;
      }
    });
  }

  setMetaTags() {
    this.titleService.setTitle(
      this.customTranslatePipe.transform(this.article?.title)
    );
    if (this.article?.image) {
      this.globalUtils.setMetaTags([
        {
          property: 'twitter:image',
          content: this.article.image,
        },
        {
          property: 'og:image',
          content: this.article.image,
        },
        {
          property: 'og:image:secure_url',
          content: this.article.image,
        },
        {
          property: 'og:image:url',
          content: this.article.image,
        },
        {
          property: 'og:image:width',
          content: '1200',
        },
        {
          property: 'og:image:height',
          content: '630',
        },
        {
          name: 'description',
          content: this.customTranslatePipe.transform(
            this.article?.shortDescription
          ),
        },
        {
          name: 'title',
          content: this.customTranslatePipe.transform(this.article?.title),
        },
        {
          property: 'og:description',
          content: this.customTranslatePipe.transform(
            this.article?.shortDescription
          ),
        },
        {
          property: 'twitter:description',
          content: this.customTranslatePipe.transform(
            this.article?.shortDescription
          ),
        },
        {
          property: 'og:title',
          content: this.customTranslatePipe.transform(this.article?.title),
        },
        {
          property: 'twitter:title',
          content: this.customTranslatePipe.transform(this.article?.title),
        },
        {
          property: 'og:url',
          content:
            this.document.location.protocol +
            '//' +
            this.document.location.hostname +
            this.router.url,
        },
      ]);
    }
  }

  updateView = async () => {
    this.gtmEvntLog("Latest_Articles_Views");
    const payload = {
      _id: this.article._id,
      modalName: 'article',
    };
    await this.homeService.updateView(payload);
  };

  getRelatedArticles = async () => {
    try {
      const query = JSON.stringify({
        $or: [
          { tags: { $in: this.article?.tags } },
          { gameDetails: this.article?.gameDetails?._id },
        ],
        articleStatus: EsportsConstantsService.Status.Publish,
      });

      const option = JSON.stringify({ limit: 4, sort: { views: -1 } });

      const relatedArticle: any = await this.articleApiService
        .getArticles_PublicAPI({ query, option })
        .toPromise();

      this.relatedArticles = relatedArticle.data;
    } catch (error) {
      this.showLoader = false;
      this.eSportsToastService.showError(
        error?.error?.message || error?.message
      );
    }
  };

  upsertBookmark = async () => {
    try {
      if (!this.user?._id) {
        this.eSportsToastService.showInfo(
          this.translateService.instant('ARTICLE.BOOKMARK_ALERT')
        );
        return;
      }

      if (this.article?.bookmarkProccesing) return;

      // Bookmark field
      const removeBookmark = `articleId=${this.article?._id}`;
      const addBookmark = { articleId: this.article._id };
      this.article.bookmarkProccesing = true;

      const bookmark: any = this.article?.isBookmarked
        ? await this.userPreferenceService
            .removeBookmark(removeBookmark)
            .toPromise()
        : await this.userPreferenceService.addBookmark(addBookmark).toPromise();

      this.article = {
        ...this.article,
        ...(bookmark?.data?.nModified == 1 && {
          isBookmarked: !this.article?.isBookmarked,
        }),
      };
      this.gtmEvntLog("Post_Bookmark");
      this.eSportsToastService.showSuccess(bookmark?.message);
      this.article.bookmarkProccesing = false;
    } catch (error) {
      this.article.bookmarkProccesing = false;
      this.eSportsToastService.showError(
        error?.error?.message || error?.message
      );
    }
  };

  openSocialShareComponent() {
    this.gtmEvntLog("Post_Shares");
    const data: InfoPopupComponentData = {
      title: this.translateService.instant('ARTICLE_POST.SHARE'),
      text: ``,
      type: InfoPopupComponentType.socialSharing,
      cancelBtnText: 'Close',
    };
    this.matDialog.open(InfoPopupComponent, {
      data,
    });
  }

  likeOrUnLikeArticle = async (
    objectId: string,
    objectType: string,
    type: number
  ) => {
    try {
      if (!this.user?._id) {
        this.eSportsToastService.showInfo(
          this.translateService.instant('ARTICLE.LIKE_ALERT')
        );
        return;
      }

      if (this.article.likeProccesing) return;

      this.article.likeProccesing = true;
      const payload = {
        objectId,
        objectType,
        type: type == -1 ? 1 : Number(!type),
      };
      const like = await this.commentService.upsertLike(payload);
      this.gtmEvntLog("Like_Article");
      this.article = { ...this.article, ...like.data };
      this.article.likeProccesing = false;
    } catch (error) {
      this.article.likeProccesing = false;
      this.eSportsToastService.showError(
        error?.error?.message || error?.message
      );
    }
  };

  viewRelatedArticle($event) {
    this.gtmEvntLog('View_Related_Article');
  }

  // GA Event Like/ Unlike Article
  gtmEvntLog(eventName) {
    let superProperties: SuperProperties = {};
    if (this.user) {
      superProperties = this.gtmServices.assignLoggedInUsedData(
        this.user
      );
    }
    this.gtmServices.gtmEventWithSuperProp({
      eventName: eventName,
      superProps: superProperties
    });
  }
}
