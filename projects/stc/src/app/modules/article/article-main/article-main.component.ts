import { SwiperConfigInterface } from 'ngx-swiper-wrapper';
import { TranslateService } from '@ngx-translate/core';
import {
  Component,
  OnDestroy,
  OnInit,
  PLATFORM_ID,
  Inject,
} from '@angular/core';

import { JumbotronComponentData } from '../../../core/jumbotron/jumbotron.component';
import { AppHtmlRoutes } from '../../../app-routing.model';
import { Router } from '@angular/router';
import {
  IUser,
  EsportsUserService,
  EsportsLanguageService,
  EsportsToastService,
  EsportsArticleService,
  EsportsUtilsService,
  EsportsGameService,
  EsportsVideoLibraryService,
  EsportsOptionService,
  SuperProperties,
  EventProperties,
  EsportsGtmService,
} from 'esports';
import { Subscription } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { isPlatformBrowser } from '@angular/common';

const API = environment.apiEndPoint;

@Component({
  selector: 'app-article-main',
  templateUrl: './article-main.component.html',
  styleUrls: ['./article-main.component.scss'],
})
export class ArticleMainComponent implements OnInit, OnDestroy {
  user: IUser;
  AppHtmlRoutes = AppHtmlRoutes;
  jumbotron: JumbotronComponentData = {
    _id: '',
    slug: '',
    ribbon: '',
    dateTitle: '',
    title: '',
    isBtnHidden: true,
    btn: 'read post',
    user: {
      img: '',
      name: '',
    },
  };

  videoLibrary: Array<{}> = [];
  activeVideo = null;
  trendingAuthors = [];
  gameList = [];
  latestArticle = [];
  showLoader = true;
  trendingNews = [];
  swiperConfig: SwiperConfigInterface = {
    spaceBetween: 30,
    navigation: true,
  };
  latestNews = [];
  latestArticleData = [];
  trendingData = [];
  hottestData = [];
  categoryList = [];
  categoryId;
  isBrowser: boolean;
  userSubscription: Subscription;

  constructor(
    private videoLibraryService: EsportsVideoLibraryService,
    private eSportsToastService: EsportsToastService,
    public utilsService: EsportsUtilsService,
    private articleService: EsportsArticleService,
    public language: EsportsLanguageService,
    public translateService: TranslateService,
    private gameService: EsportsGameService,
    private userService: EsportsUserService,
    private optionService: EsportsOptionService,
    public languageService: EsportsLanguageService,
    private router: Router,
    private gtmService: EsportsGtmService,
    @Inject(PLATFORM_ID) private platformId
  ) {
    this.isBrowser = isPlatformBrowser(platformId);
  }

  ngOnInit(): void {
    if (this.isBrowser) {
      this.getCurrentUserDetails();
      this.getArticleList();
      // this.getHottestPost();
      this.fetchVideoLibrary();
      // this.loadArticleMainPageData();
      this.fetchOptions();
      // this.getTrendingPost();
      // this.getRecentPost();
      this.getGames();
    }
  }

  ngOnDestroy() {
    if (this.userSubscription) {
      this.userSubscription?.unsubscribe();
    }
  }

  fetchVideoLibrary = async () => {
    try {
      const pagination = {
        page: 1,
        limit: 5,
        sort: '-updatedOn',
        projection: [
          '_id',
          'slug',
          'title',
          'description',
          'youtubeUrl',
          'thumbnailUrl',
          'createdBy',
        ],
      };
      const query = `?pagination=${encodeURIComponent(
        JSON.stringify(pagination)
      )}`;

      const response = await this.videoLibraryService.fetchVideoLibrary(
        API,
        query
      );
      this.videoLibrary = await Promise.all(
        response?.data?.docs.map(async (item) => {
          return {
            ...item,
            youtubeUrl: await this.utilsService.getEmbbedUrl(item.youtubeUrl),
          };
        })
      );
      this.activeVideo = this.videoLibrary.length ? this.videoLibrary[0] : null;
    } catch (error) {
      this.eSportsToastService.showError(
        error?.error?.message || error?.message
      );
    }
  };

  fetchOptions = async () => {
    const option = await Promise.all([
      this.optionService.fetchAllCategories(API),
    ]);
    this.categoryList = option[0]?.data;

    for (const iterator of this.categoryList) {
      if (iterator.name == 'News') {
        this.categoryId = iterator._id;
      }
    }
    this.getArticleNews(this.categoryId);
  };

  getArticleNews(categoryId) {
    const query = JSON.stringify({
      articleStatus: 'publish',
      category: categoryId,
    });
    const pagination = JSON.stringify({ limit: 7, sort: { createdDate: -1 } });
    const prefernce = JSON.stringify({
      prefernce: this.user?.preference?.game
        ? this.user?.preference?.game.map((item) => {
            return { gameDetails: item };
          })
        : '',
    });
    this.articleService
      .getLatestArticle(API, { query, pagination, preference: prefernce })
      .subscribe(
        (res: any) => {
          this.latestNews = res.data.docs;
        },
        (err) => {}
      );
  }

  getGames = async () => {
    try {
      const { data } = await this.gameService.getGames(API).toPromise();
      this.gameList = data || [];
    } catch (error) {
      this.showLoader = false;
      this.eSportsToastService.showError(
        error?.error?.message || error?.message
      );
    }
  };

  getRecentPost() {
    const pagination = JSON.stringify({ limit: 8, sort: { createdDate: -1 } });
    const query = JSON.stringify({
      articleStatus: 'publish',
    });
    const perfernce = JSON.stringify({
      prefernce: this.user?.preference?.game
        ? this.user?.preference?.game.map((item) => {
            return { gameDetails: item };
          })
        : '',
    });
    this.articleService
      .getLatestArticle(API, {
        pagination,
        query,
        preference: perfernce,
      })
      .subscribe(
        (res: any) => {
          this.latestArticle = res.data.docs;
          this.showLoader = false;
        },
        (err) => {
          this.showLoader = false;
        }
      );
  }

  getArticleList() {
    this.articleService.getArticleList(API).subscribe(
      (res: any) => {
        this.latestArticleData = res.data || [];
        for (let index = 0; index < this.latestArticleData.length; index++) {
          const element = this.latestArticleData[index];
          if (index == 1) {
            this.hottestData.push(element);
          } else {
            this.trendingData.push(element);
          }
        }
        this.showLoader = false;
      },
      (err) => {
        this.showLoader = false;
      }
    );
  }
  getCurrentUserDetails() {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.user = data;
      }
      this.getRecentPost();
    });
  }
  gtmevent(item) {
    let name;
    if (item?.trend == 1) {
      name = 'Hottest Post';
    } else if (item?.trend == 2) {
      name = 'Trending Post';
    } else if (item?.trend == 3) {
      name = 'Editor Pick Post';
    } else if (item == 'clickAllArticle') {
      name = 'All Latest Article';
    } else {
      return;
    }

    let eventName = `View ${name}`;
    let event = {
      fromPage: 'Discover Tab',
    };

    let superProperties: SuperProperties = {};
    if (this.user) {
      superProperties = this.gtmService.assignLoggedInUsedData(this.user);
    }

    this.gtmService.gtmEventWithSuperProp({
      eventName: eventName,
      superProps: superProperties,
      eventProps: event,
    });
  }

  addLatestArticleGtmTag(article) {
    this.pushGTMTags('View_Article', article);
  }

  onIframeClick() {
    this.pushGTMTags('View_Video', this.activeVideo);
  }

  selectVideo(video) {
    this.activeVideo = video;
    this.pushGTMTags('View_Video', video);
  }

  /**
   * @param eventName
   * @param articleOrVideo article or video
   */
  pushGTMTags(eventName: string, articleOrVideo = null) {
    let superProperties: SuperProperties = {};
    if (this.user) {
      superProperties = this.gtmService.assignLoggedInUsedData(this.user);
    }

    let eventProperties: EventProperties = {};
    eventProperties['fromPage'] = this.router.url;
    if (eventName == 'View_Article' && articleOrVideo) {
      eventProperties['articleTitle'] =
        articleOrVideo.fullText?.trim() || articleOrVideo.title?.english;
    }
    if (eventName == 'View_Video') {
      eventProperties['videoTitle'] =
        articleOrVideo.fullText?.trim() || articleOrVideo.title?.english;
    }

    this.gtmService.gtmEventWithSuperProp({
      eventName: eventName,
      superProps: superProperties,
      eventProps: eventProperties,
    });
  }

  postByGameGTM() {
    this.pushGTMTags('View_All_Posts_By_Game');
  }

  viewAllVideoGTM() {
    this.pushGTMTags('View_All_Latest_Videos');
  }

  viewHottestPost(trend) {
    if (trend == 1) {
      this.pushGTMTags('View_Hottest_Post');
    }
  }

}
