import { TranslateService } from '@ngx-translate/core';
import { Component, OnDestroy, OnInit } from '@angular/core';

import { ArticleApiService } from '../../../core/service';
import { IUser, IPagination, EsportsUserService } from 'esports';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-article-more',
  templateUrl: './article-more.component.html',
  styleUrls: ['./article-more.component.scss'],
})
export class ArticleMoreComponent implements OnInit, OnDestroy {
  user: IUser;
  paginationData = {
    page: 1,
    limit: 21,
    sort: { createdDate: -1 },
  };

  articles: any = [];
  dataLoaded = false;
  showLoader = true;
  page: IPagination;

  userSubscription: Subscription;

  constructor(
    private articleApiService: ArticleApiService,
    public translate: TranslateService,
    private userService: EsportsUserService
  ) {}

  ngOnInit() {
    this.getCurrentUserDetails();
  }

  ngOnDestroy() {
    if (this.userSubscription) {
      this.userSubscription?.unsubscribe();
    }
  }

  pageChanged(page): void {
    this.paginationData.page = page;
    this.fetchArticles();
  }

  fetchArticles = async () => {
    this.showLoader = true;
    try {
      const pagination = JSON.stringify(this.paginationData);
      const query = JSON.stringify({ articleStatus: 'publish' });
      const prefernce = JSON.stringify({
        prefernce: this.user?.preference?.game
          ? this.user?.preference?.game.map((item) => {
              return { gameDetails: item };
            })
          : '',
      });
      this.articleApiService
        .getLatestArticle({ pagination, query, preference: prefernce })
        .subscribe(
          (res: any) => {
            this.showLoader = false;
            this.articles = res['data']['docs'];
            this.dataLoaded = true;
            this.page = {
              totalItems: res?.data?.totalDocs,
              itemsPerPage: res?.data?.limit,
              maxSize: 5,
            };
          },
          (err) => {}
        );
    } catch (error) {
      this.showLoader = false;
    }
  };

  getCurrentUserDetails() {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.user = data;
      }
      this.fetchArticles();
    });
  }
}
