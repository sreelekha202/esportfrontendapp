import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { ArticleMoreComponent } from './article-more/article-more.component';
import { ArticleAuthorComponent } from './article-author/article-author.component';
import { ArticlePostComponent } from './article-post/article-post.component';
import { ArticleMainComponent } from './article-main/article-main.component';
import { ArticleGameComponent } from './article-game/article-game.component';
import { ArticleNewsComponent } from './article-news/article-news.component';

export const contentRoutes: Routes = [
  {
    path: '',
    component: ArticleMainComponent,
    data: {
      tags: [
        {
          name: 'title',
          content:
            'Create an account with stcplay.gg  and never miss any game highlight or updates on our tournament!',
        },
      ],
      title: 'Game highlight',
    },
  },

  {
    path: 'news',
    component: ArticleNewsComponent,
    data: {
      tags: [
        {
          name: 'title',
          content:
            'Create an account with stcplay.gg  and never miss any game highlight or updates on our tournament!',
        },
      ],
      title: 'Game highlight',
    },
  },
  { path: 'author/:id', component: ArticleAuthorComponent },
  { path: 'author/:id/:articleId', component: ArticlePostComponent },
  { path: 'more', component: ArticleMoreComponent },
  // {
  //   path: 'create',
  //   canActivate: [AuthorAccessGuard],
  //   component: CreateArticleComponent,
  // },
  { path: ':id', component: ArticlePostComponent },
  {
    path: 'game/:id',
    // canActivate: [AuthGuard],
    component: ArticleGameComponent,
    data: {
      tags: [
        {
          name: 'title',
          content:
            'Create an account with stcplay.gg  and never miss any game highlight or updates on our tournament!',
        },
      ],
      title: 'Game highlight',
    },
  },
  // {
  //   path: 'edit/:id',
  //   canActivate: [AuthorAccessGuard],
  //   component: CreateArticleComponent,
  // },
];

@NgModule({
  imports: [RouterModule.forChild(contentRoutes)],
  exports: [RouterModule],
})
export class ArticleRoutingModule {}
