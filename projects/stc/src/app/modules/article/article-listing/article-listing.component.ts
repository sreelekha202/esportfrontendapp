import { Component, OnInit } from '@angular/core';

import { EsportsUserService, EsportsArticleService } from 'esports';
import { environment } from "../../../../environments/environment";
const API = environment.apiEndPoint;

@Component({
  selector: 'app-article-listing',
  templateUrl: './article-listing.component.html',
  styleUrls: ['./article-listing.component.scss'],
})
export class ArticleListingComponent implements OnInit {
  articles: any = [];
  bannerList: any = [];
  dataLoaded = false;
  constructor(
    private articleApiService: EsportsArticleService,
    private userService: EsportsUserService
  ) {
    this.userService.currentUser.subscribe((data) => { });
  }
  ngOnInit() {
    this.getAllArticles();
  }
  getAllArticles() {
    this.articleApiService.getallArticle(API).subscribe(
      (data: any) => {
        this.articles = data.data;
        this.bannerList = data.data.map((obj) => {
          return { image: obj.image, _id: obj._id };
        });
        this.dataLoaded = true;
      },
      (err) => { }
    );
  }

  setFallbackImage(index) {
    this.bannerList[index].image = 'assets/images/articles/fallback.png';
  }
}
