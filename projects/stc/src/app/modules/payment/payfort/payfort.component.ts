import {
  Component,
  ElementRef,
  Input,
  OnInit,
  ViewChild,
} from '@angular/core';
import { environment } from '../../../../environments/environment';
import {
  CountryISO,
  SearchCountryField,
} from 'ngx-intl-tel-input';
import { TransactionService } from '../../../core/service';
import { FormBuilder, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { EsportsLanguageService, EsportsToastService } from 'esports'

@Component({
  selector: 'app-payfort',
  templateUrl: './payfort.component.html',
  styleUrls: ['./payfort.component.scss'],
})
export class PayfortComponent implements OnInit {
  @Input() transaction;
  @Input() tournament;
  @Input() view;
  @ViewChild('myForm') myForm: ElementRef;

  SearchCountryField = SearchCountryField;
  CountryISO = CountryISO;
  preferredCountries: CountryISO[] = [
    CountryISO.SaudiArabia,
    CountryISO.Bahrain,
    CountryISO.Kuwait,
  ];
  selectedCountryISO = CountryISO.SaudiArabia;
  mySelectedCountryISO: CountryISO;

  form = this.formBuilder.group({
    phoneNumber: ['', Validators.required],
    otpInput: ['', Validators.required],
  });

  isPayfort = true;
  payfortForm = {
    command: '',
    access_code: '',
    merchant_identifier: '',
    merchant_reference: '',
    amount: '',
    currency: '',
    language: '',
    customer_email: '',
    signature: '',
    order_description: '',
    return_url: '',
    phone_number: '',
    customer_name: '',
    url: '',
  };
  currentLang = 'en';
  isProccesing = 'false';

  constructor(
    private formBuilder: FormBuilder,
    private languageService: EsportsLanguageService,
    private translateService: TranslateService,
    private transactionService: TransactionService,
    private eSportsToastService: EsportsToastService
  ) {}

  ngOnInit(): void {
    this.populatePayfortForm().then(() => {
      this.isProccesing = 'true';
      setTimeout(() => {
        this.myForm.nativeElement.submit();
      }, 500);
    });
    this.languageService.language.subscribe((lang) => {
      this.translateService.use(lang || 'en');
      this.currentLang = lang;
    });
  }

  populatePayfortForm = async () => {
    try {
      this.payfortForm.command = 'PURCHASE';
      this.payfortForm.access_code = environment.payfort.access_code;
      this.payfortForm.url = environment.payfort.url;
      this.payfortForm.merchant_identifier =
        environment.payfort.merchant_identifier;
      this.payfortForm.merchant_reference = this.transaction._id;
      if (this.transaction.currencyCode === 'BHD') {
        this.payfortForm.amount = (
          this.transaction.totalAmount * 1000
        ).toString();
      } else {
        this.payfortForm.amount = (
          this.transaction.totalAmount * 100
        ).toString();
      }
      this.payfortForm.amount = parseInt(this.payfortForm.amount).toString();
      this.payfortForm.currency = this.transaction.currencyCode;
      this.payfortForm.language = this.currentLang;
      this.payfortForm.order_description =
        this.tournament._id + ' ' + this.view;
      this.payfortForm.return_url =
        environment.apiEndPoint + 'transaction/payfortVerify';
      const signedData = await this.transactionService.payfortSign(
        this.payfortForm
      );
      this.payfortForm.signature = signedData.data.signature;
      this.payfortForm.phone_number = signedData.data.phone_number.toString();
      this.payfortForm.customer_name = signedData.data.customer_name.toString();
      this.payfortForm.customer_email = signedData.data.customer_email.toString();
    } catch (error) {
      this.eSportsToastService.showError(error?.error?.message || error?.message);
    }
  };
}
