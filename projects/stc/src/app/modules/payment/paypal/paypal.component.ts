import {
  AfterViewChecked,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { PaypalScriptService } from '../../../core/service/paypal-script.service';

declare var paypal;

@Component({
  selector: 'app-paypal',
  templateUrl: './paypal.component.html',
  styleUrls: ['./paypal.component.scss'],
})
export class PaypalComponent implements OnInit, AfterViewChecked {
  @ViewChild('paypal', { static: true }) paypalElement: ElementRef;
  @Input() amount;
  @Input() currencyCode;
  @Output() isTransactionInProgress = new EventEmitter(false);
  @Output() paypalResponse = new EventEmitter();

  constructor(private paypalScriptService: PaypalScriptService) {}

  ngOnInit(): void {
    this.paypalScriptService.registerScript((paypal: any) => {
      paypal
        .Buttons({
          createOrder: (data, actions) => {
            this.isTransactionInProgress.emit(true);
            return actions.order.create({
              purchase_units: [
                {
                  amount: {
                    value: this.amount,
                    currency_code: this.currencyCode,
                  },
                },
              ],
            });
          },
          onApprove: async (data, actions) => {
            const order = await actions.order.capture();
            this.paypalResponse.emit(order);
            this.isTransactionInProgress.emit(false);
          },
          onError: (error) => {
            this.isTransactionInProgress.emit(false);
          },
          onCancel: () => {
            this.isTransactionInProgress.emit(false);
          },
        })
        .render(this.paypalElement.nativeElement);
    });
  }

  ngAfterViewChecked(): void {}
}
