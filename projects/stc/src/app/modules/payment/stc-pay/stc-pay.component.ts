import {
  AfterViewChecked,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
} from '@angular/core';
import {
  CountryISO,
  NgxIntlTelInputComponent,
  SearchCountryField,
} from 'ngx-intl-tel-input';
import { FormBuilder, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { EsportsLanguageService } from 'esports'

@Component({
  selector: 'app-stc-pay',
  templateUrl: './stc-pay.component.html',
  styleUrls: ['./stc-pay.component.scss'],
})
export class StcPayComponent implements OnInit {
  @Input() isOTP;
  @Output() stcResponse = new EventEmitter();
  @Output() stcOtpVerify = new EventEmitter();

  SearchCountryField = SearchCountryField;
  CountryISO = CountryISO;
  preferredCountries: CountryISO[] = [
    CountryISO.SaudiArabia,
    CountryISO.Bahrain,
    CountryISO.Kuwait,
  ];
  selectedCountryISO = CountryISO.SaudiArabia;
  mySelectedCountryISO: CountryISO;

  form = this.formBuilder.group({
    phoneNumber: ['', Validators.required],
    otpInput: ['', Validators.required],
  });
  constructor(
    private formBuilder: FormBuilder,
    private languageService: EsportsLanguageService,
    private translateService: TranslateService
  ) {}

  ngOnInit(): void {
    this.languageService.language.subscribe((lang) =>
      this.translateService.use(lang || 'en')
    );
    if (this.isOTP) {
      this.form.get('phoneNumber').clearValidators();
      this.form.get('phoneNumber').updateValueAndValidity();
    } else {
      this.form.get('otpInput').clearValidators();
      this.form.get('otpInput').updateValueAndValidity();
    }
  }

  onSubmitEvent() {
    if (this.isOTP) {
      this.stcOtpVerify.emit(this.form.value.otpInput);
    } else {
      this.stcResponse.emit(this.form.value.phoneNumber);
    }
  }
}
