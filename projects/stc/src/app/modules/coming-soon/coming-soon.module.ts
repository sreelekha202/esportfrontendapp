import { NgModule } from '@angular/core';

import { SharedModule } from '../../shared/modules/shared.module';
import { CoreModule } from '../../core/core.module';

import { ComingSoonRoutingModule } from './coming-soon-routing.module';

@NgModule({
  declarations: [],
  imports: [SharedModule, CoreModule, ComingSoonRoutingModule],
  exports: [],
})
export class ComingSoonModule {}
