import {
  Component,
  OnInit,
  OnDestroy,
  ViewChild,
  EventEmitter,
  Inject,
  PLATFORM_ID,
} from '@angular/core';
import {
  UserReportsService,
  EsportsUserService,
  EsportsParticipantService,
  IPagination,
  EsportsToastService,
  EsportsGtmService,
  SuperProperties,
  IUser,
} from 'esports';
import { Subscription } from 'rxjs';
import { environment } from '../../../environments/environment';
import { MatTabChangeEvent, MatTabGroup } from '@angular/material/tabs';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { isPlatformBrowser } from '@angular/common';
@Component({
  selector: 'app-my-matches',
  templateUrl: './my-matches.component.html',
  styleUrls: ['./my-matches.component.scss'],
})
export class MyMatchesComponent implements OnInit, OnDestroy {
  mock_cards = [];
  isLoaded = false;
  isAdmin = false;
  userSubscription: Subscription;
  challenges = [];
  activeTabIndex = -1;
  isBrowser: boolean;
  page: IPagination = {
    activePage: 1,
    totalItems: 1,
    itemsPerPage: 1,
    maxSize: 10
  };
  isProcessing: boolean = false;
  matchData: any;
  currentUser: IUser

  @ViewChild('tabs') tabGroup: MatTabGroup;
  customTabEvent = new EventEmitter<MatTabChangeEvent>();

  constructor(
    private userReportsService: UserReportsService,
    private userService: EsportsUserService,
    private esportsParticipantService: EsportsParticipantService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private location: Location,
    private gtmService: EsportsGtmService,
    @Inject(PLATFORM_ID) private platformId,
    private eSportsToastService: EsportsToastService
  ) {
    this.isBrowser = isPlatformBrowser(platformId);
  }

  ngOnInit(): void {
    if (this.isBrowser) {
      this.getCurrentUserDetails();
      this.getMyChallenges();
      const activeTabIndex = this.activatedRoute.snapshot?.queryParams?.activeTabIndex;

      if (!activeTabIndex) {
        this.pushGTMTags('My_Matches_Current_Tab_Clicked');
      }

      if ([0,2].includes(activeTabIndex)) {
        this.activeTabIndex = activeTabIndex;
        this.fetchMyMatches();
      }

      if (!activeTabIndex && activeTabIndex != 0) {
        this.fetchMyMatches();
      }

      this.customTabEvent.subscribe((tabChangeEvent: MatTabChangeEvent) => {
        this.page.activePage = 1;
        this.switchData(tabChangeEvent.index);
        if (tabChangeEvent.index == 0) {
          this.pushGTMTags('My_Matches_Current_Tab_Clicked');
        }
        if (tabChangeEvent.index == 1) {
          this.pushGTMTags('My_Matches_Challenges_Tab_Clicked');
        }
      });

      this.activatedRoute.queryParams.subscribe((params) => {
        if (params.activeTabIndex) {
          this.activeTabIndex = +params.activeTabIndex;
          this.switchData(this.activeTabIndex);
          if ([0,2].includes(this.activeTabIndex)) {
            this.fetchMyMatches();
          }
        } else if (!params.activeTabIndex && this.activeTabIndex > -1) {
          this.location.back();
        } else {
          this.switchData(this.activeTabIndex);
          this.activeTabIndex = 0;
        }
      });
    }
  }

  onTabChanged = (tabChangeEvent: MatTabChangeEvent): void => {
    this.customTabEvent.emit(tabChangeEvent);
  };

  switchData(index: number) {
    switch (index) {
      case 0:
        this.router.navigate(['.'], {
          relativeTo: this.activatedRoute,
          queryParams: { activeTabIndex: index },
        });
        break;
      case 1:
        this.router.navigate(['.'], {
          relativeTo: this.activatedRoute,
          queryParams: { activeTabIndex: index },
        });
        break;
      case 2:
        this.router.navigate(['.'], {
          relativeTo: this.activatedRoute,
          queryParams: { activeTabIndex: index },
        });
        break;
      default:
        break;
    }
  }

  ngOnDestroy() {
    this.userSubscription?.unsubscribe();
  }

  getCurrentUserDetails() {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.currentUser = data;
        if (data?.accountType === 'admin') {
          this.isAdmin = true;
        }
      }
    });
  }

  getMyChallenges() {
    this.isLoaded = true;
    this.esportsParticipantService
      .getMyChallenges(environment.apiEndPoint)
      .subscribe(
        (res) => {
          let data = res['data'];
          this.challenges = data['docs'];
          this.isLoaded = false;
        },
        (err) => {
          this.isLoaded = false;
        }
      );
  }

  reloadChallenges(event) {
    this.getMyChallenges();
    // this.getMyMatches();
  }

  pageChanged(page: number): void {
    this.page.activePage = page;
    this.fetchMyMatches();
  }

  fetchMyMatches = async () => {
    try {
     if (this.isProcessing) return;
     this.isProcessing = true;
     const queryParam = `&page=${this.page.activePage}`;
     const matchStatus = this.activeTabIndex <= 0 ? "current": "completed";
     const { data } = await this.userReportsService.getUserMatchesByStatus(matchStatus, queryParam);
     this.page.itemsPerPage = data?.limit;
     this.page.totalItems = data?.totalDocs;
     this.matchData = data;
     this.isProcessing = false;
    } catch(error) {
      this.isProcessing = false;
      const errorMessage = error?.error?.message || error?.message;
      this.eSportsToastService.showError(errorMessage);
    }
  };

  pushGTMTags(eventName: string) {
    let superProperties: SuperProperties = {};
    if (this.currentUser) {
      superProperties = this.gtmService.assignLoggedInUsedData(
        this.currentUser
      );
    }
    this.gtmService.gtmEventWithSuperProp({
      eventName: eventName,
      superProps: superProperties,
    });
  }

}
