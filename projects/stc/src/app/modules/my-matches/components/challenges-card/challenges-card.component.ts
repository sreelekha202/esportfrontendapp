import {
  Component,
  OnInit,
  Input,
  Inject,
  PLATFORM_ID,
  Output,
  EventEmitter,
} from '@angular/core';
import {
  EsportsParticipantService,
  EsportsToastService,
  EsportsUserService,
} from 'esports';
import { environment } from '../../../../../environments/environment';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';
import {
  LadderPopupComponent,
  LadderPopupComponentData,
} from '../../../../shared/popups/ladder-popup/ladder-popup.component';
import { MatDialog } from '@angular/material/dialog';

import { isPlatformBrowser } from '@angular/common';
@Component({
  selector: 'app-challenges-card',
  templateUrl: './challenges-card.component.html',
  styleUrls: ['./challenges-card.component.scss'],
})
export class ChallengesCardComponent implements OnInit {
  @Output() reload = new EventEmitter<any>();
  @Input() data: any;
  isBrowser: boolean;
  constructor(
    private esportsParticipantService: EsportsParticipantService,
    private translateService: TranslateService,
    private eSportsToastService: EsportsToastService,
    public matDialog: MatDialog,
    private userService: EsportsUserService,
    @Inject(PLATFORM_ID) private platformId
  ) {
    this.isBrowser = isPlatformBrowser(platformId);
  }

  userSubscription: Subscription;
  accepted = '';
  opponentStatus = '';
  currentUser;
  isLoading = false;
  banner = '';

  ngOnInit(): void {
    if (this.isBrowser) {
      this.getCurrentUserDetails();
      this.banner = this.data?.activeTournament?.banner
        ? this.data?.activeTournament?.banner
        : this.data?.gameDetail?.image;
    }
  }

  getCurrentUserDetails() {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data && data._id == this.data.challengeFromUserId) {
        this.accepted = this.data.challengeFromStatus;
        this.opponentStatus = this.data.challengeToStatus;
      } else {
        this.accepted = this.data.challengeToStatus;
        this.opponentStatus = this.data.challengeFromStatus;
      }
    });
  }

  acceptMatch() {
    const payload = {
      tournamentId: this.data.tournamentId,
      challengeId: this.data._id,
      accept: true,
    };
    this.esportsParticipantService
      .acceptOrRejectChallenge(environment.apiEndPoint, payload)
      .subscribe(
        (res) => {
          const successMessage = this.translateService.instant(
            'LADDER.ACCEPT_SUCCESS'
          );
          this.accepted = 'accepted';
          this.eSportsToastService.showSuccess(successMessage);
        },
        (error) => {
          this.eSportsToastService.showError(
            error?.error?.message || error?.message
          );
        },
        () => {
          this.reloadData();
        }
      );
  }

  rejectMatch() {
    const payload = {
      tournamentId: this.data.tournamentId,
      challengeId: this.data._id,
      accept: false,
    };
    this.esportsParticipantService
      .acceptOrRejectChallenge(environment.apiEndPoint, payload)
      .subscribe(
        (res) => {
          const successMessage = this.translateService.instant(
            'LADDER.REJECT_SUCCESS'
          );
          this.eSportsToastService.showSuccess(successMessage);
          this.accepted = 'rejected';
        },
        (error) => {
          this.eSportsToastService.showError(
            error?.error?.message || error?.message
          );
        },
        () => {
          this.reloadData();
        }
      );
  }

  ngOnDestroy() {
    this.userSubscription?.unsubscribe();
  }

  suggestTime() {
    const data: LadderPopupComponentData = {
      isShowScheduler: true,
      isShowLoading: false,
      isShowNotFound: false,
      payload: { endDate: this.data?.activeTournament?.endDate },
    };
    const dialogRef = this.matDialog.open(LadderPopupComponent, { data });

    dialogRef.afterClosed().subscribe((res) => {
      if (res) {
        const { startDate, startTime } = res;
        const newTime = new Date(startDate.toString());
        newTime.setHours(startTime.hour);
        newTime.setMinutes(startTime.minute);
        const payload = {
          challengeId: this.data._id,
          newTime,
        };

        this.isLoading = true;
        this.esportsParticipantService
          .suggestNewTime(environment.apiEndPoint, payload)
          .subscribe(
            (response) => {
              this.eSportsToastService.showSuccess(
                this.translateService.instant('LADDER.CHALLENGE_RESCHEDULED')
              );
            },
            (err) => {
              this.eSportsToastService.showError(
                this.translateService.instant(
                  'LADDER.CHALLENGE_RESCHEDULED_ERROR'
                )
              );
            },
            () => {
              this.isLoading = false;
              this.reloadData();
            }
          );
      }
    });
  }

  reloadData() {
    this.reload.emit();
  }
}
