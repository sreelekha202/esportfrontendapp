import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GwbTournamentsComponent } from './gwb-tournaments.component';

describe('GwbTournamentsComponent', () => {
  let component: GwbTournamentsComponent;
  let fixture: ComponentFixture<GwbTournamentsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GwbTournamentsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GwbTournamentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
