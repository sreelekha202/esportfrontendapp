import { Component, OnInit, Inject, PLATFORM_ID } from '@angular/core';
import { GameService, HomeService } from 'projects/stc/src/app/core/service';
import { TournamentService } from '../../../../core/service/tournament.service';

import { EsportsLanguageService, IPagination } from 'esports';
import { AppHtmlRoutes } from '../../../../modules/../../app/app-routing.model';
import { SwiperConfigInterface } from 'ngx-swiper-wrapper';
import { EsportsConstantsService } from 'esports';
import { isPlatformBrowser } from '@angular/common';
@Component({
  selector: 'app-gwb-tournaments',
  templateUrl: './gwb-tournaments.component.html',
  styleUrls: ['./gwb-tournaments.component.scss'],
})
export class GwbTournamentsComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;
  games = [
    {
      image: 'assets/images/Gwb/all-tournaments.png',
      name: 'GWB.COMPONENTS.TOURNAMENTS.ALL_TOURNAMENTS',
      isSelected: false,
    },
  ];

  selectedGameIndex: number;

  public config: SwiperConfigInterface = {
    keyboard: false,
    mousewheel: true,
    scrollbar: false,
    navigation: true,
    pagination: false,
    autoHeight: false,

    breakpoints: {
      320: {
        slidesPerView: 3,
        spaceBetween: 15,
        autoHeight: false,
      },
      768: {
        slidesPerView: 5,
        spaceBetween: 15,
        autoHeight: false,
      },
      1199: {
        slidesPerView: 5,
        spaceBetween: 15,
        autoHeight: false,
      },
      1919: {
        slidesPerView: 5,
        spaceBetween: 15,
        autoHeight: false,
      },
    },
  };

  defaultGames = [
    'Apex Legends',
    'Brawlhalla',
    'Call of Duty: Black Ops Cold War',
    'DOTA 2',
    'FIFA 21',
    'Mobile Legends',
    'Rainbow Six Siege',
    'Super smash bros. Ultimate',
    'Chess',
    'Clash Royale',
    'CS:GO',
    'Fortnite',
    'Gran Turismo',
    'Hearthstone',
    'Mortal Kombat 11',
    'Overwatch',
    'eFootball PES 2021',
    'PUBG',
    'Rocket League',
    'Streetfighter V',
    'Tekken 7',
    'Valorant',
    'Dragon Ball FighterZ',
    'عيشها بلا حدود',
    'eFootBall PES 2021',
    'League of Legends',
  ];

  tournaments = [];
  turkiTournaments = [];
  page: IPagination;
  page1: IPagination;

  // MOCK PAGINATION SETTINGS
  paginationData = {
    page: 1,
    limit: 8,
    sort: {endDate:-1},
  };

  selectedGame = null;
  defaultGameSelected: boolean;
  isShowParticipants;
  isArabic: boolean;
  isBrowser: boolean;
  constructor(
    private gameService: GameService,
    private tournamentService: TournamentService,
    private languageService: EsportsLanguageService,
    private ConstantsService: EsportsConstantsService,
    private homeService: HomeService,
    @Inject(PLATFORM_ID) private platformId
  ) {
    this.isBrowser = isPlatformBrowser(platformId);
  }

  ngOnInit(): void {
    this.languageService.language.subscribe((lang) => {
      this.isArabic = Boolean(lang === 'ar');
      this.games = [];
      this.getGames();
      this.getTurkiTournaments({
        status: '12',
        limit: 8,
        page: 1,
        sort: {endDate:-1},
      });
    });
    this.getShowParticipantsStatus();
    // this.getGames();
  }

  async onSelectGame(index: number) {
    this.selectedGame = this.games[index];
    this.defaultGameSelected = false;
    if (index != 0) {
      this.getTournaments({
        status: '11',
        limit: 8,
        page: 1,
        game: this.selectedGame._id,
        sort: {endDate:-1},
      });
    } else {
      await this.getTournaments({
        status: '11',
        limit: 8,
        page: 1,
        sort: {endDate:-1},
      });
    }
    this.games.forEach((game, i) => {
      game.isSelected = i === index;
    });
  }

  pageChanged(id, page, type?): void {
    this.paginationData.page = page;
    if (type == 'turki') {
      this.getTurkiTournaments({
        status: '12',
        limit: 8,
        page: page,
        sort: {endDate:-1},
        game: this.defaultGameSelected ? '' : this.selectedGame._id,
      });
    }
    this.getTournaments({
      status: '11',
      limit: 8,
      page: page,
      sort: {endDate:-1},
      game: this.defaultGameSelected ? '' : this.selectedGame._id,
    });

    setTimeout(() => {
      let el = document.getElementById(id);
      el.scrollIntoView({ behavior: 'smooth', block: 'center' });
    }, 500);
  }

  async getGames() {
    try {
      const encodeUrl = `?query=${encodeURIComponent(
        JSON.stringify({
          isTournamentAllowed: true,
          name: this.defaultGames,
        })
      )}`;
      const { data } = await this.gameService
        .getAllGames(encodeUrl)
        .toPromise();
      this.games = [];
      if (!Array.isArray(data)) return;
      await this.getTournaments({
        status: '11',
        limit: 8,
        page: 1,
        sort: {endDate:-1},
      });
      if (this.isArabic) {
        this.games.push(...data.reverse());
      } else {
        this.games.push(...data);
      }
      this.selectedGame = this.games[0];
      this.defaultGameSelected = true;
      // MOCK SELECTED
      this.games = this.games.map((item) => ({
        ...item,
        isSelected: false,
      }));
      this.games.unshift({
        image: 'assets/images/Gwb/all-tournaments.png',
        name: 'GWB.COMPONENTS.TOURNAMENTS.ALL_TOURNAMENTS',
        isSelected: false,
      });
      this.games[0].isSelected = true;
    } catch (error) {
    }
  }

  async getTournaments(payload) {
    try {
      this.tournamentService.getPaginatedTournaments(payload).subscribe(
        (res: any) => {
          this.tournaments = res.data.docs;
          this.page = {
            totalItems: res?.data?.totalDocs,
            itemsPerPage: res?.data?.limit,
            maxSize: 5,
            activePage: 1,
          };
        },
        (err) => {
          // this.showLoader = false;
          console.error(err);
        }
      );
    } catch (error) {
    }
  }

  async getTurkiTournaments(payload) {
    try {
      this.tournamentService.getPaginatedTournaments(payload).subscribe(
        (res: any) => {
          this.turkiTournaments = res.data.docs;
          this.page1 = {
            totalItems: res?.data?.totalDocs,
            itemsPerPage: res?.data?.limit,
            maxSize: 5,
          };
        },
        (err) => {
          // this.showLoader = false;
          console.error(err);
        }
      );
    } catch (error) {
    }
  }

  setGameOnSlide(index, enableToast = true) {
    this.selectedGameIndex = index;
  }

  timeList = {
    AM: 'صباحا',
    PM: 'مساء',
    '0': '۰',
    '1': '١',
    '2': '٢',
    '3': '٣',
    '4': '٤',
    '5': '٥',
    '6': '٦',
    '7': '٧',
    '8': '٨',
    '9': '٩',
  };

  getTranslatedTime(time) {
    if (this.isBrowser) {
      const locale = this.ConstantsService.defaultLangCode;
      let timeString = '';
      if (!locale || locale === 'ar') {
        for (let i = 0; i < time.length; i++) {
          if (time.charAt(i) === ':') {
            timeString = timeString + ':';
          } else if (time.charAt(i) === ' ') {
            timeString = timeString + ' ';
          } else if (time.charAt(i) === 'A') {
            timeString = timeString + ' ' + this.timeList['AM'];
            break;
          } else if (time.charAt(i) === 'P') {
            timeString = timeString + ' ' + this.timeList['PM'];
            break;
          } else {
            timeString = timeString + this.timeList[time.charAt(i)];
          }
        }
        return timeString;
      } else {
        return time;
      }
    }
  }

  getTranslatedDate(date) {
    if (this.isBrowser) {
      const locale = this.ConstantsService.defaultLangCode;
      if (!locale || locale === 'ar') {
        let options = {
          weekday: 'long',
          year: 'numeric',
          month: 'long',
          day: 'numeric',
        };
        let defaultDate = new Date(date);
        return defaultDate.toLocaleDateString('ar-EG', options);
      } else {
        return date;
      }
    }
  }

  async getShowParticipantsStatus() {
    try {
      const data = await this.homeService.isShowParticipants();
      this.isShowParticipants = data?.showParticipant;
    } catch (error) {
      console.error(error?.error?.message && error?.message);
    }
  }
}
