import {
  Component,
  OnInit,
  PLATFORM_ID,
  Inject,
  Renderer2,
  OnDestroy,
} from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

import { faBars, faTimes } from '@fortawesome/free-solid-svg-icons';
import { AppHtmlGwbegRoutes } from 'projects/stc/src/app/app-routing.model';
import {
  AppHtmlRoutes,
  AppHtmlProfileRoutes,
} from '../../../../app-routing.model';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router, ActivatedRoute } from '@angular/router';

import {
  IUser,
  EsportsLanguageService,
  EsportsConstantsService,
  EsportsUserService,
  EsportsChatService,
  GlobalUtils,
  EsportsChatSidenavService,
} from 'esports';
import { environment } from '../../../../../environments/environment';
import { AuthServices } from '../../../../core/service';
import { DOCUMENT } from '@angular/common';
import { sampleTime } from 'rxjs/operators';

const API = environment.apiEndPoint;
const TOKEN = environment.currentToken;

@Component({
  selector: 'app-gwb-header',
  templateUrl: './gwb-header.component.html',
  styleUrls: ['./gwb-header.component.scss'],
})
export class GwbHeaderComponent implements OnInit, OnDestroy {
  AppHtmlGwbegRoutes = AppHtmlGwbegRoutes;
  activeLang = this.ConstantsService?.defaultLangCode;
  AppHtmlProfileRoutes = AppHtmlProfileRoutes;
  collapsed: boolean = true;
  mobMenuOpened: boolean = false;

  AppLanguage = [];
  currentUser: IUser;
  faBars = faBars;
  faTimes = faTimes;
  snapchatPixel;
  dotomiPixel;
  googleSiteTag;
  googleSiteTag2;
  googlePageViewEvent;
  floodlightTags;

  homeLinks = [
    {
      title: 'GWB.COMPONENTS.HEADER.LINKS.HOME',
      url: AppHtmlGwbegRoutes.home,
      liveLink: 'https://www.gamerswithoutborders.com',
      arLiveLink: 'https://www.gamerswithoutborders.com/index_ar.html',
      target: '_blank',
    },
  ];

  links = [
    {
      title: 'GWB.COMPONENTS.HEADER.LINKS.ABOUT',
      url: AppHtmlGwbegRoutes.about,
      id: 'gwb_about',
    },
    {
      title: 'GWB.COMPONENTS.HEADER.LINKS.WATCH',
      url: AppHtmlGwbegRoutes.watch,
      liveLink: 'https://watch.gamerswithoutborders.com',
      arLiveLink: 'https://watch.gamerswithoutborders.com/ar',
      target: '_blank',
    },
    {
      title: 'GWB.COMPONENTS.HEADER.LINKS.EXPERIENCE',
      url: AppHtmlGwbegRoutes.experience,
      liveLink: 'https://watch.gamerswithoutborders.com/experience',
      arLiveLink: 'https://watch.gamerswithoutborders.com/ar/experience',
      target: '_blank',
    },
    {
      title: 'GWB.COMPONENTS.HEADER.LINKS.GAME',
      url: AppHtmlGwbegRoutes.game,
      id: 'gwb_game',
    },
    {
      title: 'GWB.COMPONENTS.HEADER.LINKS.LEARN',
      url: AppHtmlGwbegRoutes.learn,
      liveLink: 'https://www.gamerswithoutborders.com/learn.html',
      arLiveLink: 'https://www.gamerswithoutborders.com/learn_ar.html',
      target: '_blank',
    },
    {
      title: 'GWB.COMPONENTS.HEADER.LINKS.DONATE',
      url: AppHtmlGwbegRoutes.donate,
      liveLink: 'https://www.gamerswithoutborders.com/donate.html',
      arLiveLink: 'https://www.gamerswithoutborders.com/donate_ar.html',
      target: '_blank',
    },
  ];
  twitterScript;

  constructor(
    @Inject(PLATFORM_ID) private platformId,
    private ConstantsService: EsportsConstantsService,
    private languageService: EsportsLanguageService,
    public translate: TranslateService,
    private userService: EsportsUserService,
    private modalService: NgbModal,
    private chatService: EsportsChatService,
    private chatSidenavService: EsportsChatSidenavService,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private _renderer2: Renderer2,
    @Inject(DOCUMENT) private _document: Document,
    private authService: AuthServices
  ) {}

  ngOnInit(): void {
    if (GlobalUtils.isBrowser()) {
      this.activeRoute.queryParams.subscribe((params) => {
        this.activeLang = params?.locale || 'ar';
      });
      this.AppLanguage = this.ConstantsService?.language;
      this.getUserData();
      this.addTwitterScript();
      this.addGoogleSiteTag();
      this.addGooglePageViewEvent();
      this.addFloodLightTag();
    }
  }

  ngOnDestroy() {
    if (this.snapchatPixel) {
      this.snapchatPixel.remove();
    }
    if (this.dotomiPixel) {
      this.dotomiPixel.remove();
    }
    if (this.twitterScript) {
      this.twitterScript.remove();
    }
    if (this.googleSiteTag) {
      this.googleSiteTag.remove();
    }
    if (this.googleSiteTag2) {
      this.googleSiteTag2.remove();
    }
    if (this.googlePageViewEvent) {
      this.googlePageViewEvent.remove();
    }
    if (this.floodlightTags) {
      this.floodlightTags.remove();
    }
  }

  getUserData() {
    this.userService.currentUser.pipe(sampleTime(400)).subscribe((data) => {
      if (data) {
        this.currentUser = data;
      }
      if (this.snapchatPixel) {
        this.snapchatPixel.remove();
      }
      this.addSnapchatScript();
    });
  }
  onConfirmLogout(content) {
    this.modalService.open(content, {
      centered: true,
      windowClass: 'custom-modal-content',
    });
  }

  goToUrl() {
    if (GlobalUtils.isBrowser()) {
      window.open('https://www.gamerswithoutborders.com/', '_blank');
    }
  }
  goToArUrl() {
    if (GlobalUtils.isBrowser()) {
      window.open(
        'https://www.gamerswithoutborders.com/index_ar.html',
        '_blank'
      );
    }
  }
  onLogOut(): void {
    if (GlobalUtils.isBrowser()) {
      this.chatService.setChatStatus(false);
      this.chatService?.disconnectUser();
      this.userService.logout(API, TOKEN);
      this.chatSidenavService.close();
      this.modalService.dismissAll();
      // this.router.navigate([AppHtmlRoutes.home]);
    }
  }
  onLanguageChange(lang): void {
    if (GlobalUtils.isBrowser()) {
      localStorage.setItem('currentLanguage', lang);
      this.activeLang = lang;
      this.translate.use(lang);
      this.languageService.setLanguage(lang);
      this.router.navigate(['.'], {
        relativeTo: this.activeRoute,
        queryParams: { locale: lang },
      });
      // window.location.reload();
      this.addTwitterScript();
    }
  }

  anchorGwb(id) {
    let el = document.getElementById(id);
    el.scrollIntoView({ behavior: 'smooth', block: 'end' });
  }

  onLogin() {
    this.authService.redirectUrl = this.router.url;
    this.router.navigate(['/user/phone-login'], {
      relativeTo: this.activeRoute,
    });
  }

  addTwitterScript() {
    if (this.twitterScript) {
      document.getElementById('twitter-snippet').remove();
    }
    const locale = this.activeLang;
    let scriptBody = `
        !function(e,t,n,s,u,a){e.twq||(s=e.twq=function(){s.exe?s.exe.apply(s,arguments):s.queue.push(arguments);
        },s.version='1.1',s.queue=[],u=t.createElement(n),u.async=!0,u.src='//static.ads-twitter.com/uwt.js',
        a=t.getElementsByTagName(n)[0],a.parentNode.insertBefore(u,a))}(window,document,'script');
        // Insert Twitter Pixel ID and Standard Event data below
        twq('init','o64n3');
        twq('track','PageView');
       `;

    if (locale === 'ar') {
      scriptBody = `
            !function(e,t,n,s,u,a){e.twq||(s=e.twq=function(){s.exe?s.exe.apply(s,arguments):s.queue.push(arguments);
            },s.version='1.1',s.queue=[],u=t.createElement(n),u.async=!0,u.src='//static.ads-twitter.com/uwt.js',
            a=t.getElementsByTagName(n)[0],a.parentNode.insertBefore(u,a))}(window,document,'script');
            // Insert Twitter Pixel ID and Standard Event data below
            twq('init','o64o6');
            twq('track','PageView');
            `;
    }

    this.twitterScript = this._renderer2.createElement('script');
    this.twitterScript.type = `text/javascript`;
    this.twitterScript.id = 'twitter-snippet';
    this.twitterScript.text = scriptBody;
    // this._renderer2.appendChild(this._document.body, this.twitterScript);
    let appRoot = this._document.getElementById('stc-ga-tags');
    this._renderer2.insertBefore(this._document.body, this.twitterScript, appRoot);
  }

  addSnapchatScript() {
    let userEmail = '';
    if (this.currentUser) {
      userEmail = this.currentUser?.email;
    }
    let scriptBody = `(function(e,t,n){if(e.snaptr)return;var a=e.snaptr=function()
      {a.handleRequest?a.handleRequest.apply(a,arguments):a.queue.push(arguments)};
      a.queue=[];var s='script';r=t.createElement(s);r.async=!0;
      r.src=n;var u=t.getElementsByTagName(s)[0];
      u.parentNode.insertBefore(r,u);})(window,document,
      'https://sc-static.net/scevent.min.js');
      snaptr('init', '5ceb2304-7103-4e9a-af62-c5372f2f1f95', {
      'user_email': '${userEmail}'
      });
      snaptr('track', 'PAGE_VIEW');`;

    this.snapchatPixel = this._renderer2.createElement('script');
    this.snapchatPixel.type = `text/javascript`;
    this.snapchatPixel.text = scriptBody;
    this.snapchatPixel.id = 'snapchat-snippet';
    this._renderer2.appendChild(this._document.head, this.snapchatPixel);
  }

  addDotomiScript() {
    this.dotomiPixel = this._renderer2.createElement('script');
    let cacheBuster = new Date().getTime().toString();
    this.dotomiPixel.type = `text/javascript`;
    this.dotomiPixel.src = `https://login.dotomi.com/profile/visit/js/1_0?dtm_cid=81587&dtm_cmagic=f77691&dtm_fid=3701&cachebuster=${cacheBuster}`;
    this.dotomiPixel.id = 'dotomi-snippet';
    this._renderer2.appendChild(this._document.body, this.dotomiPixel);
  }

  addGoogleSiteTag() {
    this.googleSiteTag = this._renderer2.createElement('script');
    this.googleSiteTag.type = `text/javascript`;
    this.googleSiteTag.src = `https://www.googletagmanager.com/gtag/js?id=AW-361232228`;
    this._renderer2.appendChild(this._document.body, this.googleSiteTag);

    this.googleSiteTag2 = this._renderer2.createElement('script');
    this.googleSiteTag2.type = `text/javascript`;
    let scriptBody = `window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
      gtag('config', 'AW-361232228');`;
    this.googleSiteTag2.text = scriptBody;
    this._renderer2.appendChild(this._document.body, this.googleSiteTag2);
  }

  addGooglePageViewEvent() {
    this.googlePageViewEvent = this._renderer2.createElement('script');
    this.googlePageViewEvent.type = `text/javascript`;
    let scriptBody = `gtag('event', 'conversion', {'send_to': 'AW-361232228/2aRuCMHM86cCEOTun6wB'});`;
    this.googlePageViewEvent.text = scriptBody;
    this._renderer2.appendChild(this._document.body, this.googlePageViewEvent);
  }

  addFloodLightTag() {
    try {
      this.floodlightTags = this._renderer2.createElement('div');
      this.floodlightTags.id = 'floodlight-snippet';
      this.floodlightTags.innerHTML =
        `<!--
        Start of Floodlight Tag: Please do not remove
        Activity name of this tag: Gamers_Community_Tournaments_STC website_DV360
        URL of the webpage where the tag is expected to be placed: https://stcplay.gg/gamerswithoutborders
        This tag must be placed between the <body> and </body> tags, as close as possible to the opening tag.
        Creation Date: 06/07/2021
        -->
        <script type="text/javascript">
        var axel = Math.random() + "";
        var a = axel * 10000000000000;
        document.write('<iframe src="https://11002123.fls.doubleclick.net/activityi;src=11002123;type=invmedia;cat=gamer000;dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;tfua=;npa=;gdpr=`+'${GDPR}'+`;gdpr_consent=`+'${GDPR_CONSENT_755}'+`;ord=' + a + '?" width="1" height="1" frameborder="0" style="display:none"></iframe>');
        </script>
        <noscript>
        <iframe src="https://11002123.fls.doubleclick.net/activityi;src=11002123;type=invmedia;cat=gamer000;dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;tfua=;npa=;gdpr=`+'${GDPR}'+`;gdpr_consent=`+'${GDPR_CONSENT_755}'+`;ord=1?" width="1" height="1" frameborder="0" style="display:none"></iframe>
        </noscript>
        <!-- End of Floodlight Tag: Please do not remove -->`;

      let appRoot = this._document.getElementById('stc-ga-tags');
      this._renderer2.insertBefore(this._document.body, this.floodlightTags, appRoot);
    } catch (error) {

    }
  }
}
