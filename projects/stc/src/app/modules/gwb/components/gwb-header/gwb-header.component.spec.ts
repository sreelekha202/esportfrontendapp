import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GwbHeaderComponent } from './gwb-header.component';

describe('GwbHeaderComponent', () => {
  let component: GwbHeaderComponent;
  let fixture: ComponentFixture<GwbHeaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GwbHeaderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GwbHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
