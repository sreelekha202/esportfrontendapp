import { NgModule } from '@angular/core';

import { SharedModule } from '../../shared/modules/shared.module';
import { CoreModule } from '../../core/core.module';

import { GwbAppComponent } from './gwb-app.component';
import { GwbRoutingModule } from './gwb-app-routing.module';

import { GwbHomeComponent } from './pages/home/gwb-home.component';
import { GwbHeaderComponent } from './components/gwb-header/gwb-header.component';
import { GwbTournamentsComponent } from './components/gwb-tournaments/gwb-tournaments.component';
import { GwbFooterComponent } from './gwb-footer/gwb-footer.component';

@NgModule({
  declarations: [
    GwbAppComponent,
    GwbHeaderComponent,
    GwbHomeComponent,
    GwbTournamentsComponent,
    GwbFooterComponent,
  ],
  imports: [SharedModule, CoreModule, GwbRoutingModule],
  exports: [],
})
export class GwbAppModule {}
