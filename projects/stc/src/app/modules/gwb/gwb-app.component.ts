import {
  Component,
  OnInit,
  Inject,
  PLATFORM_ID,
  Renderer2,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EsportsLanguageService } from 'esports';
import { DOCUMENT, isPlatformBrowser } from '@angular/common';
import { AuthServices } from '../../core/service';

@Component({
  selector: 'gwb-app',
  templateUrl: './gwb-app.component.html',
  styleUrls: ['./gwb-app.component.scss'],
})
export class GwbAppComponent implements OnInit {
  isBrowser: boolean;
  constructor(
    public language: EsportsLanguageService,
    private activeRoute: ActivatedRoute,
    public authService: AuthServices,
    public router: Router,
    @Inject(PLATFORM_ID) private platformId,
    private _renderer2: Renderer2,
    @Inject(DOCUMENT) private _document: Document
  ) {
    this.isBrowser = isPlatformBrowser(platformId);
  }

  ngOnInit(): void {
    this.activeRoute.queryParams.subscribe((params) => {
      this.language.setLanguage(params?.locale || 'ar');
    });
    let script = this._renderer2.createElement('script');
    let cacheBuster = new Date().getTime().toString();
    script.type = `text/javascript`;
    script.src = `https://login.dotomi.com/profile/visit/js/1_0?dtm_cid=81587&dtm_cmagic=f77691&dtm_fid=3699&cachebuster=${cacheBuster}`;
    this._renderer2.appendChild(this._document.body, script);
    // Set registration source to GWB2021 in local storage for registration tracking
    if (!this.authService.getRegistrationSrc()) {
      if (
        this.router.url.includes('gwb2021') ||
        this.router.url.includes('gamerswithoutborders')
      )
        this.authService.setRegistrationSrc('GWB2021');
    }
  }
}
