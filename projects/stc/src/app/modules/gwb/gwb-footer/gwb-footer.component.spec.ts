import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GwbFooterComponent } from './gwb-footer.component';

describe('GwbFooterComponent', () => {
  let component: GwbFooterComponent;
  let fixture: ComponentFixture<GwbFooterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GwbFooterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GwbFooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
