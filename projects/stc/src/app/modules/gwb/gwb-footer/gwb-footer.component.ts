import { Component, OnInit } from '@angular/core';
import { AppHtmlRoutes } from '../../../app-routing.model';
@Component({
  selector: 'app-gwb-footer',
  templateUrl: './gwb-footer.component.html',
  styleUrls: ['./gwb-footer.component.scss'],
})
export class GwbFooterComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;

  navigation = [
    { link: AppHtmlRoutes.aboutUs, text: 'FOOTER.ABOUT' },
    { link: AppHtmlRoutes.termsOfUse, text: 'FOOTER.TERMS_OF_USE' },
    { link: AppHtmlRoutes.privacyPolicy, text: 'FOOTER.PRIVACY' },
    { link: AppHtmlRoutes.codeOfConduct, text: 'FOOTER.CODE_OF_CONDUCT' },
    { link: AppHtmlRoutes.antiSpamPolicy, text: 'FOOTER.ANTI_SPAM_POLICY' },
  ];

  constructor() {}

  ngOnInit(): void {}
}
