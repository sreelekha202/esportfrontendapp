import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { GwbAppComponent } from './gwb-app.component';
import { GwbHomeComponent } from './pages/home/gwb-home.component';

const routes: Routes = [
  {
    path: '',
    component: GwbAppComponent,
    children: [
      {
        path: '',
        component: GwbHomeComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GwbRoutingModule {}
