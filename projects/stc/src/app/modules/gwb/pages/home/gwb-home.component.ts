import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

import { UtilsService } from '../../../../core/service';
import { FireProductService } from '../../../../shared/service/fire-product.service';

import { GlobalUtils, EsportsConstantsService } from 'esports';

@Component({
  selector: 'app-gwb-home',
  templateUrl: './gwb-home.component.html',
  styleUrls: ['./gwb-home.component.scss'],
})
export class GwbHomeComponent implements OnInit {
  activeLang = this.ConstantsService?.defaultLangCode;

  streams = [];

  about = [
    'GWB.HOME.ABOUT.ITEMS.1',
    'GWB.HOME.ABOUT.ITEMS.2',
    'GWB.HOME.ABOUT.ITEMS.3',
    'GWB.HOME.ABOUT.ITEMS.4',
    'GWB.HOME.ABOUT.ITEMS.5',
    'GWB.HOME.ABOUT.ITEMS.6',
  ];

  allGames = [
    'GWB.HOME.ALL_GAMES.ITEMS.1',
    'GWB.HOME.ALL_GAMES.ITEMS.2',
    'GWB.HOME.ALL_GAMES.ITEMS.3',
    'GWB.HOME.ALL_GAMES.ITEMS.4',
    'GWB.HOME.ALL_GAMES.ITEMS.5',
    'GWB.HOME.ALL_GAMES.ITEMS.6',
  ];

  donates = ['GWB.HOME.DONATE.ITEMS.1', 'GWB.HOME.DONATE.ITEMS.2'];

  descriptions = [
    'GWB.HOME.ABOUT.DESRIPTION.1',
    'GWB.HOME.ABOUT.DESRIPTION.2',
    'GWB.HOME.ABOUT.DESRIPTION.3',
  ];

  socials = {
    primary: [
      {
        icon: 'assets/icons/gwb/socials/instagram.svg',
        link: 'https://instagram.com/gwbps_ar',
        text: 'GWB.HOME.SOCIALS.ARABIC',
      },
      {
        icon: 'assets/icons/gwb/socials/instagram.svg',
        link: 'https://instagram.com/gwbps',
        text: 'GWB.HOME.SOCIALS.ENGLISH',
      },
      {
        icon: 'assets/icons/gwb/socials/twitter.svg',
        link: 'https://Twitter.com/GWBps_ar',
        text: 'GWB.HOME.SOCIALS.ARABIC',
      },
      {
        icon: 'assets/icons/gwb/socials/twitter.svg',
        link: 'https://Twitter.com/GWBps',
        text: 'GWB.HOME.SOCIALS.ENGLISH',
      },
    ],
    secondary: [
      {
        icon: 'assets/icons/gwb/socials/snapchat.svg',
        link: 'https://www.snapchat.com/add/GWBPS',
      },
      {
        icon: 'assets/icons/gwb/socials/facebook.svg',
        link: 'https://www.facebook.com/GWBPS/',
      },
      {
        icon: 'assets/icons/gwb/socials/youtube.svg',
        link: 'https://www.youtube.com/channel/UCp0l2CHB_W3G7kWvSzSlzEw',
      },
    ],
  };

  constructor(
    private ConstantsService: EsportsConstantsService,
    private fireProductService: FireProductService,
    public translate: TranslateService,
    public utilsService: UtilsService
  ) {}

  ngOnInit(): void {
    if (GlobalUtils.isBrowser()) {
      this.activeLang = this.translate.currentLang;
    }

    this.getStreams();
  }

  async getStreams() {
    this.fireProductService.getVideoStreams().subscribe(async (res) => {
      if (res && res.data && res.data.length > 0) {
        this.streams = await Promise.all(
          res?.data?.map(async (item) => {
            return {
              ...item,
              url: await this.utilsService.getEmbbedUrlDynamic(
                item.url,
                item.provider
              ),
            };
          })
        );
        if (this.streams.findIndex((x) => x.url === 'Invalid Url') != -1)
          this.streams.splice(
            this.streams.findIndex((x) => x.url === 'Invalid Url'),
            1
          );
        this.streams;
      } else {
        this.streams = [];
      }
    });
  }
}
