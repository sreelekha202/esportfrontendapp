import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
import { EsportsLanguageService } from 'esports'

@Component({
  selector: 'app-privacy-policy',
  templateUrl: './privacy-policy.component.html',
  styleUrls: ['./privacy-policy.component.scss'],
})
export class PrivacyPolicyComponent implements OnInit {
  arebicText = false;
  public currentLang: string;
  currLanguage = 'english';
  constructor(
    private location: Location,
    public translate: TranslateService,
    public language: EsportsLanguageService
  ) {}

  ngOnInit(): void {
    this.language.language.subscribe((lang) => {
      if (lang) {
        this.arebicText = lang === 'ms' ? true : false;
      }
    });
  }

  goBack() {
    this.location.back();
  }
}
