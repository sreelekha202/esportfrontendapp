import { Component, OnInit, OnDestroy } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';
import { VideoLibraryService } from '../../core/service';
import {
  EsportsLanguageService,
  EsportsConstantsService,
  EsportsToastService,
  IPagination,
  IUser,
  EsportsUserService,
  SuperProperties,
  EventProperties,
  EsportsGtmService,
} from 'esports';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

enum VideosFilter {
  allVideos,
  recent,
}

@Component({
  selector: 'app-videos',
  templateUrl: './videos.component.html',
  styleUrls: ['./videos.component.scss'],
})
export class VideosComponent implements OnInit, OnDestroy {
  VideosFilter = VideosFilter;
  videosFilter: VideosFilter = VideosFilter.allVideos;
  videos: Array<any> = [];
  pagination = {
    page: 1,
    limit: 20,
    sort: '-updatedOn',
    projection: [
      '_id',
      'slug',
      'title',
      'description',
      'youtubeUrl',
      'thumbnailUrl',
      'updatedOn',
    ],
  };
  videoDetails;
  isLoaded = false;
  page: IPagination;
  user: IUser;
  userSubscription: Subscription;

  constructor(
    private videoLibraryService: VideoLibraryService,
    private eSportsToastService: EsportsToastService,
    private translateService: TranslateService,
    private languageService: EsportsLanguageService,
    private router: Router,
    private gtmService: EsportsGtmService,
    private userService: EsportsUserService,
    private ConstantsService: EsportsConstantsService
  ) {}

  ngOnInit(): void {
    this.languageService.language.subscribe((lang) =>
      this.translateService.use(lang || this.ConstantsService?.defaultLangCode)
    );
    this.fetchAllVideoLibrary();
    this.getCurrentUserDetails();
  }

  ngOnDestroy() {
    if (this.userSubscription) {
      this.userSubscription?.unsubscribe();
    }
  }

  fetchAllVideoLibrary = async () => {
    try {
      this.isLoaded = false;
      const encodeUrl = `?pagination=${encodeURIComponent(
        JSON.stringify(this.pagination)
      )}`;
      this.videoDetails = await this.videoLibraryService.fetchVideoLibrary(
        encodeUrl
      );
      this.page = {
        totalItems: this.videoDetails?.data?.totalDocs,
        itemsPerPage: this.videoDetails?.data?.limit,
        maxSize: 5,
      };
      this.isLoaded = true;
    } catch (error) {
      this.isLoaded = true;
      this.eSportsToastService.showError(
        error?.error?.message || error?.message
      );
    }
  };

  pageChanged(page) {
    this.pagination.page = page;
    this.fetchAllVideoLibrary();
  }
  addViewVideoGtmTag(video) {
    this.pushGTMTags('View_Video', video);
  }

  getCurrentUserDetails() {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.user = data;
      }
    });
  }

  pushGTMTags(eventName: string, video = null) {
    let superProperties: SuperProperties = {};
    if (this.user) {
      superProperties = this.gtmService.assignLoggedInUsedData(this.user);
    }

    let eventProperties: EventProperties = {};

    if (eventName == 'View_Video' && video) {
      eventProperties['fromPage'] = this.router.url;
      eventProperties['videoTitle'] = video.title?.english;
    }

    this.gtmService.gtmEventWithSuperProp({
      eventName: eventName,
      superProps: superProperties,
      eventProps: eventProperties,
    });
  }
}
