import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeaderBoardPlayerComponent } from './leader-board-player.component';

describe('LeaderBoardPlayerComponent', () => {
  let component: LeaderBoardPlayerComponent;
  let fixture: ComponentFixture<LeaderBoardPlayerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LeaderBoardPlayerComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeaderBoardPlayerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
