import { SwiperConfigInterface } from 'ngx-swiper-wrapper';
import { Component, OnInit, Inject, PLATFORM_ID } from '@angular/core';
import { Router } from '@angular/router';
import { GameService } from '../../core/service';
import {
  EsportsLeaderboardService,
  EsportsUserService,
  EsportsLanguageService,
  EsportsConstantsService,
  IPagination,
  EsportsGtmService,
  SuperProperties,
} from 'esports';
import { AppHtmlRoutes } from '../../app-routing.model';
import { environment } from '../../../environments/environment';
import { TranslateService } from '@ngx-translate/core';
import * as mockData from './mock-leader-board-data';
import { isPlatformBrowser } from '@angular/common';
import { EventProperties } from 'projects/esports/src/public-api';

@Component({
  selector: 'app-leader-board',
  templateUrl: './leader-board.component.html',
  styleUrls: ['./leader-board.component.scss'],
})
export class LeaderBoardComponent implements OnInit {
  public config: SwiperConfigInterface = {
    navigation: true,
    pagination: false,
    autoHeight: true,
  };
  apiEndPoint = environment.apiEndPoint;
  gameList = [];
  stateList = [];
  boardCardsData = mockData.boardCardsData;
  leaderBoardRows = mockData.leaderBoardRows;
  leaderBoardColumns = mockData.leaderBoardColumns;
  isBrowser: boolean;
  selectedRegions = '';
  selectedGames = 'all';

  selectedGame: any = { all: true };
  page: IPagination = {
    itemsPerPage: 10,
    totalItems: 200,
    activePage: 0,
    maxSize: 5,
  };
  gameId = 'all';
  showLoader = true;
  leaderboardData: any = [];
  currentLang = this.ConstantsService?.defaultLangCode;

  constructor(
    private leaderboardService: EsportsLeaderboardService,
    private gameService: GameService,
    private userService: EsportsUserService,
    private router: Router,
    public language: EsportsLanguageService,
    public ConstantsService: EsportsConstantsService,
    private translateService: TranslateService,
    private gtmService: EsportsGtmService,
    @Inject(PLATFORM_ID) private platformId
  ) {
    this.isBrowser = isPlatformBrowser(platformId);
  }

  ngOnInit(): void {
    if (this.isBrowser) {
      this.getGames();
      // 17: Bahrain, 117: kuwait,

      // this.filterLeaderboard();
      this.language.language.subscribe((lang) => {
        this.currentLang = lang;
        this.getStates([191]);
      });
    }
  }

  getGames() {
    const encodeUrl = `?query=${encodeURIComponent(
      JSON.stringify({ isTournamentAllowed: true })
    )}`;
    this.gameService.getAllGames(encodeUrl).subscribe(
      (res) => (this.gameList = res.data),
      (err) => {}
    );
  }
  sortByKey(array, key) {
    return array.sort(function (a, b) {
      var x = a[key];
      var y = b[key];
      return x < y ? -1 : x > y ? 1 : 0;
    });
  }
  getStates(countryIds) {
    this.stateList = [];
    this.userService.getStates().subscribe((data) => {
      this.stateList = data.states.filter((el) =>
        countryIds.includes(parseInt(el.country_id))
      );
      if (this.translateService.currentLang == 'ar') {
        this.stateList = this.sortByKey(this.stateList, 'ar');
      }
      this.stateList.unshift({
        id: 117,
        sortname: 'KW',
        name: 'Kuwait',
        phoneCode: 965,
        en: 'Kuwait',
        ar: 'دولة الكويت',
      });
      this.stateList.unshift({
        id: 17,
        sortname: 'BH',
        name: 'Bahrain',
        phoneCode: 973,
        en: 'Bahrain',
        ar: 'مملكة البحرين',
      });
      this.filterLeaderboard();
    });
  }

  filterLeaderboard() {
    this.showLoader = true;
    const params = {
      page: +this.page.activePage + 1,
      limit: this.page.itemsPerPage,
      gameId: this.selectedGames,
      state: null,
      country: null,
    };
    if (this.selectedRegions == 'Kuwait' || this.selectedRegions == 'Bahrain') {
      params.country = this.selectedRegions;
    } else {
      params.state = this.selectedRegions;
    }
    this.leaderboardService
      .getGameLeaderboard(this.apiEndPoint, params)
      .subscribe(
        (res: any) => {
          this.showLoader = false;
          this.leaderboardData = [];
          this.page.totalItems = res?.data?.totalDocs;
          if (res.data.docs) {
            let count = (params.page - 1) * params.limit;
            let userData: any = {};
            for (const data of res.data.docs) {
              if (data.user.length > 0) {
                userData.id = data._id.user;
                userData.rank = count + 1;
                count++;
                userData.img =
                  data.user[0].profilePicture == ''
                    ? './assets/images/Profile/stc_avatar.png'
                    : data.user[0].profilePicture;
                userData.name = data.user[0].fullName;
                userData.region = data.user[0].state ? data.user[0].state : '';
                userData.trophies = {};
                userData.trophies.first = data.firstPosition;
                userData.trophies.second = data.secondPosition;
                userData.trophies.third = data.thirdPosition;
                userData.points = data.points;
                userData.amount = data.amountPrice;
                userData.gamesCount = data.gamesCount;
                this.leaderboardData.push({ ...userData });
                userData = {};
              }
            }
          }
        },
        (err) => {
          this.showLoader = false;
        }
      );
  }

  changeTab(value, type) {
    switch (type) {
      case 'regions': {
        this.selectedRegions = value;
        this.page.activePage = 0;
        this.filterLeaderboard();
        break;
      }
      case 'games': {
        this.selectedGames = value;
        this.page.activePage = 0;
        this.filterLeaderboard();
        break;
      }
    }
  }

  onRowClick(event) {
    if (event.type === 'click') {
      this.pushGTMTags('View_Profile_Long_Details_In_Leaderboard', event.row)
      this.router.navigate([AppHtmlRoutes.leaderboard, event.row.id]);
    }
  }
  pageChanged(page): void {
    this.page.activePage = page.offset;
    this.filterLeaderboard();
  }

  pushGTMTags(eventName: string, profileData = null) {
    let superProperties: SuperProperties = {};
    if (this.userService.currentUserSubject.value) {
      superProperties = this.gtmService.assignLoggedInUsedData(
        this.userService.currentUserSubject.value
      );
    }
    let eventProperties: EventProperties = {};
    if (eventName = 'View_Profile_Long_Details_In_Leaderboard') {
      eventProperties.profileName = profileData?.name;
      eventProperties.profileRanking = profileData?.rank;
    }

    this.gtmService.gtmEventWithSuperProp({
      eventName: eventName,
      superProps: superProperties,
      eventProps: eventProperties
    });
  }
}
