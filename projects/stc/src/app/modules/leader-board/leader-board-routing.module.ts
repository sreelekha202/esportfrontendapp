import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { LeaderBoardPlayerComponent } from './leader-board-player/leader-board-player.component';
import { LeaderBoardComponent } from './leader-board.component';

const routes: Routes = [
  { path: '', component: LeaderBoardComponent },
  { path: ':id', component: LeaderBoardPlayerComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LeaderBoardRoutingModule {}
