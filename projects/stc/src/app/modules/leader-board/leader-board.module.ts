import { ChartsModule } from 'ng2-charts';
import { NgModule } from '@angular/core';

import { LeaderBoardRoutingModule } from './leader-board-routing.module';
import { SharedModule } from '../../shared/modules/shared.module';
import { CoreModule } from '../../core/core.module';
import { LeaderBoardComponent } from './leader-board.component';
import { LeaderBoardPlayerComponent } from './leader-board-player/leader-board-player.component';
import { LeaderBoardCardComponent } from './leader-board-card/leader-board-card.component';

@NgModule({
  declarations: [
    LeaderBoardComponent,
    LeaderBoardPlayerComponent,
    LeaderBoardCardComponent,
  ],
  imports: [SharedModule, CoreModule, LeaderBoardRoutingModule, ChartsModule],
  exports: [],
})
export class LeaderBoardModule {}
