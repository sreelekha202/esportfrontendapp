import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';
import {
  debounceTime,
} from 'rxjs/operators';
import {
  IUser,
  EsportsUserService,
  GlobalUtils,
  EsportsToastService,
  EsportsGtmService,
  SuperProperties,
  EventProperties,
} from 'esports';
import { AppHtmlRoutes, AppHtmlProfileRoutes } from '../../app-routing.model';
import { Location } from '@angular/common';
import { environment } from '../../../environments/environment';

const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;
@Component({
  selector: 'app-create-team',
  templateUrl: './create-team.component.html',
  styleUrls: ['./create-team.component.scss'],
})
export class CreateTeamComponent implements OnInit, OnDestroy {
  AppHtmlRoutes = AppHtmlRoutes;
  AppHtmlProfileRoutes = AppHtmlProfileRoutes;
  form: FormGroup;
  currentUser: IUser;
  dropdown: any;
  txtValue: any;
  emailInvitation = [];

  get email(): any {
    return this.form.get('email');
  }
  hideDropdown: any;

  biographyLength = 250;

  mock_select = [
    {
      name: 'facebook',
      placeholder: 'facebook_url',
      value: '',
    },
    {
      name: 'twitch',
      placeholder: 'twitch_url',
      value: '',
    },
    {
      name: 'instagram',
      placeholder: 'instagram_url',
      value: '',
    },
    {
      name: 'youtube',
      placeholder: 'youtube_url',
      value: '',
    },
    {
      name: 'discord',
      placeholder: 'discord_url',
      value: '',
    },
    {
      name: 'twitter',
      placeholder: 'twitter_url',
      value: '',
    },
  ];
  isInEditMode: boolean = false;
  clicked: boolean = false;
  selectedSocial = [];
  teamData;
  teamId: string;
  userSubscription: Subscription;
  searchSubscription: Subscription;
  inputSubscription: Subscription;
  searchResults: any[];
  customLoader: Boolean = false;
  searchInput = new FormControl('');
  pagination = {
    page: 1,
    limit: 100,
    hasNextPage: true
  };
  fallbackURL: string;
  constructor(
    private fb: FormBuilder,
    private userService: EsportsUserService,
    public eSportsToastService: EsportsToastService,
    private translateService: TranslateService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private gtmService: EsportsGtmService,
    private location: Location
  ) {}

  ngOnInit(): void {
    if (
      GlobalUtils.isBrowser() &&
      localStorage.getItem(environment.currentToken)
    ) {
      this.handleSearchInput();
      this.getUserData();
    } else {
      this.router.navigate(['/']);
    }
    this.createTournamentForm();
    if (this.activatedRoute.snapshot.params['id']) {
      this.isInEditMode = true;
      this.teamId = this.activatedRoute.snapshot.params['id'];
      this.fallbackURL = '/admin/team-management';
      this.getTeamData(this.activatedRoute.snapshot.params['id']);
    }
  }
  getTeamData(id) {
    const query = `?query=${encodeURIComponent(JSON.stringify({ _id: id }))}`;
    this.userService.getTeamList(API, query).subscribe(
      (res) => {
        this.teamData = {
          teamLogo: res.data[0].logo,
          socialMedia: res.data[0]?.social,
          shortDescription: res.data[0].shortDescription,
          teamName: res.data[0].name,
          members: res.data[0].member,
        };
        var list = [];

        if (this.teamData?.socialMedia) {
          for (
            var i = 0;
            i < Object.keys(this.teamData?.socialMedia).length;
            i++
          ) {
            var item = {};
            item['name'] = Object.keys(this.teamData?.socialMedia)[i];
            item['placeholder'] =
              Object.keys(this.teamData?.socialMedia)[i] + ' url';
            item['value'] =
              this.teamData?.socialMedia[
                Object.keys(this.teamData?.socialMedia)[i]
              ];
            let validatorFn;
            if(item['name'] == 'instagram') {
              validatorFn = this.fb.control('', [Validators.pattern(/(?:(?:http|https):\/\/)?(?:www\.)?(?:instagram\.com|instagr\.am)\/([A-Za-z0-9-_\.]+)/im)]);
            }
            else if(item['name'] == 'facebook') {
              validatorFn = this.fb.control('', [Validators.pattern(/(?:http:\/\/)?(?:www.)?facebook.com\/(?:(?:\w)*#!\/)?(?:pages\/)?(?:[?\w\-]*\/)?(?:profile.php\?id=(\d.*))?([\w\-]*)?/im)]);
            }
            else if(item['name'] == 'twitter') {
              validatorFn = this.fb.control('', [Validators.pattern(/http(?:s)?:\/\/(?:www\.)?twitter\.com\/([a-zA-Z0-9_]+)/im)]);
            }
            else if(item['name'] == 'youtube') {
              validatorFn = this.fb.control('', [Validators.pattern(/http(?:s?):\/\/(?:www\.)?youtu(?:be\.com\/watch\?v=|\.be\/)([\w\-\_]*)(&(amp;)?‌​[\w\?‌​=]*)?/im)]);
            } else {
              validatorFn = this.fb.control('', []);
            } 
            this.form.addControl(
              Object.keys(this.teamData?.socialMedia)[i],
              validatorFn
            );
            this.form.controls[
              Object.keys(this.teamData?.socialMedia)[i]
            ].setValue(
              this.teamData?.socialMedia[
                Object.keys(this.teamData?.socialMedia)[i]
              ]
            );
            this.form.updateValueAndValidity();

            list.push(item);
          }
        }
        this.selectedSocial = list;
        const indexm = this.teamData.members.findIndex(
          (x) => JSON.stringify(x._id) === JSON.stringify(this.currentUser?._id)
        );
        if (indexm > -1) {
          if (
            this.teamData.members[indexm].role != 'player' ||
            this.currentUser.accountType == 'admin'
          ) {
            this.form.patchValue(this.teamData);
          } else {
            this.eSportsToastService.showError(
              this.translateService.instant('PROFILE.TEAMS.ERRORS.ERROR6')
            );
            this.router.navigate(['/profile/teams']);
          }
        } else if (this.currentUser.accountType == 'admin') {
          this.form.patchValue(this.teamData);
        } else {
          this.eSportsToastService.showError(
            this.translateService.instant('PROFILE.TEAMS.ERRORS.ERROR6')
          );
          this.router.navigate(['/profile/teams']);
        }
      },
      (err) => {}
    );
  }
  cancel() {
    if (this.isInEditMode) {
      this.pushGTMTags('Edit_Team_Cancellation');
    } else {
      this.pushGTMTags('Create_New_Team_Cancellation');
    }

    this.location.back();
  }
  ngOnDestroy() {
    this.userSubscription?.unsubscribe();
    this.inputSubscription?.unsubscribe();
    this.searchSubscription?.unsubscribe();
  }

  getUserData() {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.currentUser = data;
      }
    });
  }

  filterFunction() {
    var input, filter, a, i;
    input = document.getElementById('myInput');
    filter = input.value.toUpperCase();
    this.dropdown = document.getElementById('myDropdown');
    a = this.dropdown.getElementsByTagName('a');
    for (i = 0; i < a.length; i++) {
      this.txtValue = a[i].textContent || a[i].innerText;
      if (this.txtValue.toUpperCase().indexOf(filter) > -1) {
        a[i].style.display = '';
      } else {
        a[i].style.display = 'none';
      }
    }
  }

  getValue(value) {
    if (!this.isSocialAlreadyAdded(this.selectedSocial, value?.name)) {
      this.selectedSocial.push(value);
      let validatorFn;
      if(value?.name == 'instagram') {
        validatorFn = this.fb.control('', [Validators.pattern(/(?:(?:http|https):\/\/)?(?:www\.)?(?:instagram\.com|instagr\.am)\/([A-Za-z0-9-_\.]+)/im)]);
      }
      else if(value?.name == 'facebook') {
        validatorFn = this.fb.control('', [Validators.pattern(/(?:http:\/\/)?(?:www.)?facebook.com\/(?:(?:\w)*#!\/)?(?:pages\/)?(?:[?\w\-]*\/)?(?:profile.php\?id=(\d.*))?([\w\-]*)?/im)]);
      }
      else if(value?.name == 'twitter') {
        validatorFn = this.fb.control('', [Validators.pattern(/http(?:s)?:\/\/(?:www\.)?twitter\.com\/([a-zA-Z0-9_]+)/im)]);
      }
      else if(value?.name == 'youtube') {
        validatorFn = this.fb.control('', [Validators.pattern(/http(?:s?):\/\/(?:www\.)?youtu(?:be\.com\/watch\?v=|\.be\/)([\w\-\_]*)(&(amp;)?‌​[\w\?‌​=]*)?/im)]);
      } else{
        validatorFn = this.fb.control('', []);
      } 
      this.form.addControl(value.name, validatorFn);
      this.form.updateValueAndValidity();
    }
    this.hideDropdown = false;
  }
  removeSocial(i) {
    this.form.controls[this.selectedSocial[i].name].reset();
    this.selectedSocial.splice(i, 1);
  }

  createTournamentForm() {
    this.form = this.fb.group({
      teamLogo: [''],
      teamName: ['', Validators.required],
      shortDescription: [''],
      members: [[], Validators.compose([])],
      socialMedia: [''],
      email: [''],
    });
  }
  createTeam() {
    if (!this.form.value.email) {

      const controls = this.form.controls;
      this.clicked = true;
      if (this.form.invalid) {
        Object.keys(controls).forEach((controlName) =>
          controls[controlName].markAsTouched()
        );
        return;
      }

      let social = {
        facebook: '',
        twitch: '',
        youtube: '',
        discord: '',
        twitter: '',
        instagram: '',
      };
      let members = [];
      let that = this;
      this.mock_select.forEach(function (value) {
        if (
          that.form.value[value.name] != null &&
          that.form.value[value.name]
        ) {
          social[value.name] = that.form.value[value.name];
        } else {
          delete social[value.name];
        }
      });

      this.form.value.members.forEach(function (value) {
        members.push(value._id);
      });

      let payload = {
        logo: this.form.value.teamLogo,
        teamName: this.form.value.teamName,
        shortDescription: this.form.value.shortDescription,
        social: social,
        members: members,
        email: this.emailInvitation,
      };
      if (this.isInEditMode) {
        this.pushGTMTags('Edit_Team_Complete');
        this.updateTeam(payload);
      } else {
        this.pushGTMTags('Create_New_Team_Complete');
        this.userService.create_team(API, payload).subscribe(
          (data) => {
            this.clicked = false;
            this.eSportsToastService.showSuccess(data?.message);
            this.location.back();
          },
          (error) => {
            this.clicked = false;
            this.eSportsToastService.showError(error?.error?.message);
          }
        );
      }
    } else {
      this.eSportsToastService.showError(
          "'" +
          this.form.value.email +
          "'" +
          this.translateService.instant('PROFILE.TEAMS.ERRORS.ERROR5') +
          this.translateService.instant('PROFILE.TEAMS.ERRORS.ERROR13')
      );
    }
  }
  updateTeam(payload) {
    this.clicked = true;
    let teamData = {
      logo: payload.logo,
      teamName: payload.teamName,
      shortDescription: payload.shortDescription,
      social: payload.social,
    };
    const teamObj = {
      id: this.teamId,
      admin: true,
      member: this.form.value.members,
      userName: this.currentUser.fullName,
      email: payload.email,
      query: {
        condition: { _id: this.teamId },
        update: teamData,
      },
    };
    this.userService.team_update(API, teamObj).subscribe(
      (res) => {
        this.clicked = false;
        if (res.data) {
          this.eSportsToastService.showSuccess(res.message);
          this.location.back();
        } else {
          this.eSportsToastService.showError(res.message);
        }
      },
      (err) => {
        this.clicked = false;
        this.eSportsToastService.showError(err.error.message);
      }
    );
  }

  isMembersAlreadyAdded(arr, _id) {
    const { length } = arr;
    const id = length + 1;
    const found = arr.some((el) => el?._id === _id);
    return found;
  }

  isSocialAlreadyAdded(arr, name) {
    const { length } = arr;
    const id = length + 1;
    const found = arr.some((el) => el?.name === name);
    return found;
  }

  removeMember(index) {
    // if (this.isInEditMode) {
    //   if (this.form.value.members[index].role == 'owner') {
    //     this.eSportsToastService.showError(
    //       this.translateService.instant('PROFILE.TEAMS.ERRORS.ERROR2')
    //     );
    //   } else {
    //     this.form.value.members[index].status = 'deleted';
    //   }
    // } else {
    this.form.value.members.splice(index, 1);
    // }
  }

  validateEmail(email) {
    const regularExpression = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (!regularExpression.test(String(email).toLowerCase())) {
      return 0;
    }
    return 1;
  }
  addEmails() {
    if (this.validateEmail(this.form.value.email)) {
      if (this.emailInvitation.indexOf(this.form.value.email) == -1) {
        this.emailInvitation.push(this.form.value.email);
      } else {
        this.eSportsToastService.showError(
          this.translateService.instant('PROFILE.TEAMS.ERRORS.ERROR10')
        );
      }
    } else {
      this.eSportsToastService.showError(
        this.translateService.instant('PROFILE.TEAMS.ERRORS.ERROR5')
      );
    }
    this.email.reset();
  }
  removeEmail(index) {
    this.emailInvitation.splice(index, 1);
  }

  pushGTMTags(eventName: string) {
    if (!eventName) {
      return;
    }
    let superProperties: SuperProperties = {};
    if (this.currentUser) {
      superProperties = this.gtmService.assignLoggedInUsedData(
        this.currentUser
      );
    }
    let eventProperties: EventProperties = {};
    if (eventName === 'Create_New_Team_Complete') {
      eventProperties.inviteExternalUsers = this.emailInvitation.length;
    }
    this.gtmService.gtmEventWithSuperProp({
      eventName: eventName,
      superProps: superProperties,
      eventProps: eventProperties,
    });
  }

  handleSearchInput() {
    this.inputSubscription = this.searchInput.valueChanges.pipe(debounceTime(300)).subscribe(value=> {
      if (value.trim() != '' && value.trim().length > 2) {
        this.customLoader = true;
        this.resetPagination();
        this.searchSubscription?.unsubscribe();
        this.searchSubscription = this.userService.searchUserWithPagination(API, value, this.pagination).subscribe(results=> {
          this.searchResults = results['data']['docs'];
          this.pagination.hasNextPage = results['data']['hasNextPage'];
          this.customLoader = false;
        }, err => {
          this.customLoader = false;
          this.eSportsToastService.showError(
            this.translateService.instant('TOURNAMENT.SEARCH_FAILED')
          );
        })
      } else {
        this.resetSearch();
      }
    })
  }

  resetSearch(){
    this.searchSubscription?.unsubscribe();
    this.customLoader = false;
    this.searchResults = [];
    this.resetPagination();
  }

  searchPlayers() {
    if (this.pagination.hasNextPage) {
      this.pagination.page++;
      this.customLoader = true;
      this.searchSubscription?.unsubscribe();
      this.searchSubscription = this.userService.searchUserWithPagination(API, this.searchInput.value, this.pagination).subscribe(results=> {
        this.searchResults = this.searchResults.concat(...results['data']['docs']);
        this.pagination.hasNextPage = results['data']['hasNextPage'];
        this.customLoader = false;
      }, err => {
        this.customLoader = false;
        this.eSportsToastService.showError(
          this.translateService.instant('TOURNAMENT.SEARCH_FAILED')
        );
      })
    }
  }

  addPlayer(player) {
    if (player && typeof player == 'object') {
      if (
        !this.isMembersAlreadyAdded(
          this.form.value.members,
          player?._id
        )
      ) {
        if (player._id != this.currentUser._id) {
          this.form.value.members.push(player);
        } else {
          this.eSportsToastService.showError(
            this.translateService.instant('PROFILE.TEAMS.ERRORS.ERROR4')
          );
        }
      } else {
        this.eSportsToastService.showError(
          this.translateService.instant('PROFILE.TEAMS.ERRORS.ERROR3')
        );
      }
    }
    this.searchInput.setValue('');
    this.resetSearch();
  }

  resetPagination() {
    this.pagination.page = 1;
    this.pagination.hasNextPage = true;
  }

}
