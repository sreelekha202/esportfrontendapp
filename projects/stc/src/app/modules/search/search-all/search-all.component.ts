import { TranslateService } from '@ngx-translate/core';
import { Component, OnInit } from '@angular/core';
import { HomeService } from '../../../core/service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-search-all',
  templateUrl: './search-all.component.html',
  styleUrls: ['./search-all.component.scss'],
})
export class SearchAllComponent implements OnInit {
  constructor(
    public translate: TranslateService,
    private homeService: HomeService,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {}
}
