import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchTournamentsComponent } from './search-tournaments.component';

describe('HomeComponent', () => {
  let component: SearchTournamentsComponent;
  let fixture: ComponentFixture<SearchTournamentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SearchTournamentsComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchTournamentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
