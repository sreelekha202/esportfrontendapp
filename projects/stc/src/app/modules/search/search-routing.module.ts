import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { SearchTournamentsComponent } from './search-tournaments/search-tournaments.component';
import { SearchArticleComponent } from './search-article/search-article.component';
import { SearchVideoComponent } from './search-video/search-video.component';
import { SearchAllComponent } from './search-all/search-all.component';
import { SearchComponent } from './search.component';

const routes: Routes = [
  {
    path: '',
    component: SearchComponent,
    children: [
      {
        path: '',
        component: SearchAllComponent,
        data: {
          tags: [
            {
              name: 'title',
              content:
                'stc play is an online gaming hub that enables users to find suggested tournaments, articles and videos.',
            },
          ],
          title: 'Find suggested tournaments, articles and videos on stc play',
        },
      },
      { path: 'tournament', component: SearchTournamentsComponent },
      { path: 'article', component: SearchArticleComponent },
      { path: 'video', component: SearchVideoComponent },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SearchRoutingModule {}
