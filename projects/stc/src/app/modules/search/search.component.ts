import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { FormBuilder } from '@angular/forms';
import { filter, map, sampleTime } from 'rxjs/operators';
import { Subscription, zip } from 'rxjs';

import { AppHtmlRoutes } from '../../app-routing.model';
import { HomeService } from '../../core/service';
import { IPagination } from 'projects/esports/src/lib/models/paginations';
import {
  SuperProperties,
  EsportsGtmService,
  EventProperties,
  EsportsUserService,
} from 'esports';

export enum OngoingTournamentFilter {
  upcoming,
  ongoing,
  completed,
  all,
}

@Component({
  selector: 'app-home',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SearchComponent implements OnInit, OnDestroy {
  text: string;
  isAllPage: boolean;
  activeRoute: string;
  AppHtmlRoutes = AppHtmlRoutes;
  OngoingTournamentFilter = OngoingTournamentFilter;
  tournamentFilter: OngoingTournamentFilter = OngoingTournamentFilter.all;
  page: IPagination;
  paginationData = {
    page: 1,
    limit: 8,
    sort: '-startDate',
  };
  noOfSearches: number = 0;

  private sub: Subscription;
  private sub1: Subscription;
  private searchSubscription: Subscription;

  constructor(
    public translate: TranslateService,
    public router: Router,
    public activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private homeService: HomeService,
    private gtmService: EsportsGtmService,
    private userService: EsportsUserService,
    private changeDetectorRef: ChangeDetectorRef
  ) {
    this.initSubscription();
  }

  ngOnInit(): void {
    // TODO: find why we have this bug, there some issue deeper
    // fix select title on init page load (refresh rtc.)
    setTimeout(() => (this.activeRoute = this.router.url));
    this.searchSubscribe();
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
    this.sub1.unsubscribe();
    this.searchSubscription.unsubscribe();
  }

  onSearchCategory(path: AppHtmlRoutes): void {
    this.activeRoute = path;
    this.tournamentFilter = OngoingTournamentFilter.all;
    this.handleTournamentStatusChange();

    switch (path) {
      case AppHtmlRoutes.searchArticle:
        this.pushGTMTags('Select_Search_Article_Section');
        break;
      case AppHtmlRoutes.searchTournament:
        this.pushGTMTags('Select_Search_Tournament_Section');
        break;
      case AppHtmlRoutes.searchVideo:
        this.pushGTMTags('Select_Search_Video_Section');
        break;
      default:
        this.pushGTMTags('Select_Search_All_Section');
        break;
    }
  }

  /**
   * Function will update filter value to search service.
   */
  handleTournamentStatusChange() {
    this.homeService.updateTournamentStatusFilter(this.tournamentFilter);
  }

  private initSubscription(): void {
    this.sub = this.activatedRoute.queryParamMap.subscribe((e) => {
      if (e.get('text')) {
        this.text = e.get('text');
        this.homeService.updateSearchParams(this.text, this.paginationData);
      } else {
        this.text = '';
        this.homeService.updateSearchParams(this.text, this.paginationData);
      }
    });

    this.sub1 = this.router.events
      .pipe(filter((event) => event instanceof NavigationEnd))
      .subscribe((event: NavigationEnd) => {
        const shouldContainTwoOrMoreSlashes = new RegExp('(?:.*?/){2}');
        this.isAllPage = !Boolean(
          shouldContainTwoOrMoreSlashes.test(event.url)
        );
      });
  }

  searchSubscribe() {
    this.searchSubscription = zip(
      this.homeService.searchedTournament,
      this.homeService.searchedArticle,
      this.homeService.searchedVideo
    )
      .pipe(
        sampleTime(1000),
        map(([tournament, article, video]) => ({ tournament, article, video }))
      )
      .subscribe((result) => {
        this.noOfSearches++;

        let totalNoOfResults = +(
          +result.article['totalDocs'] +
          +result.tournament['totalDocs'] +
          +result.video['totalDocs']
        );

        let eventProperties: EventProperties = {
          fromPage: this.router.url,
          searchTerms: this.text,
          noOfArticles: result.article['totalDocs'],
          noOfResults: totalNoOfResults,
          noOfTournaments: result.tournament['totalDocs'],
          noOfVideos: result.video['totalDocs'],
          noOfSearches: this.noOfSearches,
          searchCharacterLength: this.text.length,
        };

        this.pushGTMTags('Search', eventProperties);
      });
  }

  pushGTMTags(eventName: string, _eventProperties: EventProperties = null) {
    let superProperties: SuperProperties = {};
    if (this.userService.currentUserSubject.value) {
      superProperties = this.gtmService.assignLoggedInUsedData(
        this.userService.currentUserSubject.value
      );
    }

    let eventProperties: EventProperties = {};
    if (eventName == 'Search') {
      eventProperties = _eventProperties;
    }
    this.gtmService.gtmEventWithSuperProp({
      eventName: eventName,
      superProps: superProperties,
      eventProps: eventProperties,
    });
  }
}
