import { NgModule } from '@angular/core';

import { SharedModule } from '../../shared/modules/shared.module';
import { CoreModule } from '../../core/core.module';

import { CodeOfConductRoutingModule } from './code-of-conduct-routing.module';
import { CodeOfConductComponent } from './code-of-conduct.component';

@NgModule({
  declarations: [CodeOfConductComponent],
  imports: [SharedModule, CoreModule, CodeOfConductRoutingModule],
  exports: [],
})
export class CodeOfConductModule {}
