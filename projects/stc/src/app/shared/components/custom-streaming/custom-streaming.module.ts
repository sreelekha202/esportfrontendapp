import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomStreamingComponent } from './custom-streaming.component';
import {
  FontAwesomeModule,
  FaIconLibrary,
} from '@fortawesome/angular-fontawesome';
import { faPlay, faStop, faExpand } from '@fortawesome/free-solid-svg-icons';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { I18nModule } from 'esports';
import { MatTooltipModule } from '@angular/material/tooltip';
import { environment } from '../../../../environments/environment';
@NgModule({
  declarations: [CustomStreamingComponent],
  imports: [
    CommonModule,
    FontAwesomeModule,
    NgbModule,
    MatProgressSpinnerModule,
    I18nModule.forRoot(environment),
    MatTooltipModule,
  ],
  exports: [CustomStreamingComponent],
})
export class CustomStreamingModule {
  constructor(private library: FaIconLibrary) {
    library.addIcons(faPlay, faStop, faExpand);
  }
}
