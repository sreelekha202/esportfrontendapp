import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-counter',
  templateUrl: './counter.component.html',
  styleUrls: ['./counter.component.scss'],
})
export class CounterComponent implements OnInit {
  @Input() value: number = 0;
  @Input() min: number = 0;
  @Input() max: number = 0;

  @Output() counterChange = new EventEmitter<number>();

  constructor() {}

  ngOnInit(): void {}

  increment() {
    this.updateValue('inc');
  }

  decrement() {
    this.updateValue('dec');
  }

  private updateValue(action: 'inc' | 'dec') {
    const delta = action === 'inc' ? 1 : -1;
    this.value += delta;
    this.counterChangeHandler();
  }

  counterChangeHandler() {
    this.value =
      this.value > this.max
        ? this.max || this.value
        : this.value < this.min
        ? this.min
        : this.value || 0;
    this.counterChange.emit(this.value);
  }
}
