import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ChangeDetectorRef,
  AfterContentChecked,
} from '@angular/core';
import { IMatch, ITournament } from '../../../models';
import { BracketService, UtilsService } from '../../../../core/service';
import { ScoreConfirmPopupComponent } from './score-confirm-popup/score-confirm-popup.component';
import { MatDialog } from '@angular/material/dialog';
import { EsportsToastService } from "esports";
@Component({
  selector: 'app-battle-royale-multi',
  templateUrl: './battle-royale-multi.component.html',
  styleUrls: ['./battle-royale-multi.component.scss'],
})
export class BattleRoyaleMultiComponent implements OnInit, AfterContentChecked {
  @Input() structure: any;
  @Input() isSeeded: boolean;
  @Input() isAdmin: boolean;
  @Input() tournament: ITournament;

  @Output() isRefresh = new EventEmitter<boolean>(false);

  stage: Array<any> = [];
  standings = [];

  currStage = 1;
  currGroup = 1;
  currRound = 0;

  stageLoading: boolean = false;
  groupLoading: boolean = false;
  isAllDataLoaded: boolean = false;
  isEdit = false;
  isProcessing = false;
  isProcessRequest = false;

  match: IMatch;
  refMatch: IMatch;

  rSorting = {
    name: 0,
    placement: 0,
    noOfKill: 0,
    score: 0,
  };
  sSorting: any = {};

  activeAttributeSorting = '';
  activeAttributeStandingSorting = 'totalScore';

  constructor(
    private bracketService: BracketService,
    private eSportsToastService: EsportsToastService,
    private utilsService: UtilsService,
    private ref: ChangeDetectorRef,
    private matDialog: MatDialog
  ) {}

  ngOnInit(): void {}

  ngOnChanges() {
    if (this.isSeeded && this.tournament?._id) {
      this.stage = [];
      this.fetchDistinctStage();
    }

    if (
      !this.isSeeded &&
      typeof this.isSeeded === 'boolean' &&
      this.structure
    ) {
      this.stage = this.bracketService.assembleMultiStageBattleStructure(
        this.structure
      );
    }
  }

  ngAfterContentChecked() {
    this.ref.detectChanges();
  }

  selectedStage(currStage) {
    this.isAllDataLoaded = false;
    this.isEdit = false;
    this.currStage = currStage + 1;
    this.fetchDistinctStageGroup(this.currStage);
  }

  selectedGroup(currGroup) {
    this.isAllDataLoaded = false;
    this.isEdit = false;
    this.currGroup = currGroup + 1;
    this.fetchGroupRoundDetails(this.currStage, this.currGroup);
  }

  fetchDistinctStage = async () => {
    try {
      const filterQuery = {
        tournamentId: this.tournament?._id,
      };

      const queryParam = `?query=${this.utilsService.getEncodedQuery(
        filterQuery
      )}`;

      const { data } = await this.bracketService.fetchDistinctValue(
        queryParam,
        'currentMatch.stage'
      );

      if (this.stage.length != data.length) {
        this.stageLoading = true;
        this.stage = data.map((s) => {
          return {
            id: s,
            isLoaded: false,
            group: [],
          };
        });
      }
      this.stageLoading = false;
    } catch (error) {
      this.stageLoading = false;
    }
  };

  fetchDistinctStageGroup = async (stageId) => {
    try {
      this.groupLoading = true;
      const isLoaded = this.stage[stageId - 1]?.isLoaded;
      if (!isLoaded) {
        this.currGroup = 1;
        const filterQuery = {
          tournamentId: this.tournament?._id,
          'currentMatch.stage': stageId,
        };
        const queryParam = `?query=${this.utilsService.getEncodedQuery(
          filterQuery
        )}`;

        const { data } = await this.bracketService.fetchDistinctValue(
          queryParam,
          'currentMatch.group'
        );

        this.stage[stageId - 1].group = data.map((g) => {
          return {
            id: g,
            isLoaded: false,
          };
        });
        this.stage[stageId - 1].isLoaded = true;
      } else {
        this.isAllDataLoaded = true;
      }
      this.groupLoading = false;
    } catch (error) {
      this.groupLoading = false;
    }
  };

  fetchGroupRoundDetails = async (stageId, groupId) => {
    try {
      const isLoaded = this.stage[stageId - 1]?.group[groupId - 1]?.isLoaded;
      this.isEdit = false;

      if (!isLoaded) {
        this.stage[stageId - 1].group[groupId - 1].isLoaded = true;
        const filterQuery = {
          tournamentId: this.tournament?._id,
          'currentMatch.stage': stageId,
          'currentMatch.group': groupId,
        };

        const optionQuery = {
          sort: { 'currentMatch.matchPosition': 1 },
        };

        const queryParam = `?query=${this.utilsService.getEncodedQuery(
          filterQuery
        )}
                            &option=${this.utilsService.getEncodedQuery(
                              optionQuery
                            )}`;

        const { data } = await this.bracketService.fetchAllMatches(queryParam);
        this.stage[stageId - 1].group[groupId - 1].round = data;
      }

      this.tabChange({ index: this.currRound });
      this.isAllDataLoaded = true;
    } catch (error) {
      this.stage[stageId - 1].group[groupId - 1].isLoaded = false;
      // this.eSportsToastService.showError(error?.error?.message || error?.message);
    }
  };

  tabChange(event) {
    this.currRound = event.index;
    this.isEdit = false;

    if (this.isSeeded && event.index == 0) {
      this.fetchStanding();
    }

    if (this.activeAttributeSorting)
      this.fieldSorting(this.activeAttributeSorting);
  }

  fetchStanding = async () => {
    try {
      this.standings = [];
      this.isProcessing = true;
      const queryParam = `?stage=${this.currStage}&group=${this.currGroup}&tournamentId=${this.tournament?._id}`;
      const response = await this.bracketService.fetchStanding(queryParam);
      this.standings = response.data;
      this.isProcessing = false;

      this.activeAttributeStandingSorting = 'totalScore';

      if (this.standings.length) {
        this.sSorting = {
          name: 0,
          totalScore: 1,
        };
        for (let i = 1; i <= this.standings[0]?.round?.length; i++) {
          this.sSorting[`round${i}`] = 0;
        }
      }

      this.standingSorting(this.activeAttributeStandingSorting);
    } catch (error) {
      this.isProcessing = false;
      // this.eSportsToastService.showError(error?.error?.message || error?.message);
    }
  };

  SubmitScore = async () => {
    try {
      const payload = {
        battleRoyalTeams: this.refMatch.battleRoyalTeams.map((p) => {
          return {
            placement: p?.placement,
            noOfKill: p?.noOfKill,
            notPlaying: p?.notPlaying,
            _id: p?._id,
          };
        }),
      };

      const isCompleted = payload.battleRoyalTeams.every((item) => {
        return item?.notPlaying || !!item?.placement;
      });

      if (!isCompleted) {
        const confirmed = await this.matDialog
          .open(ScoreConfirmPopupComponent)
          .afterClosed()
          .toPromise();

        if (!confirmed) return;
      }

      this.isProcessRequest = true;
      const filterQuery = {
        tournamentId: this.tournament._id,
        _id: this.refMatch?._id,
      };
      const queryParam = `?query=${this.utilsService.getEncodedQuery(
        filterQuery
      )}`;

      const response = await this.bracketService.updateMatch(
        queryParam,
        payload
      );

      this.isProcessRequest = false;
      this.eSportsToastService.showSuccess(response?.message);
      this.stage[this.currStage - 1].group[this.currGroup - 1].isLoaded = false;
      this.fetchGroupRoundDetails(this.currStage, this.currGroup);
      this.fetchDistinctStage();
    } catch (error) {
      this.isProcessRequest = false;
      // this.eSportsToastService.showError(error?.error?.message || error?.message);
    }
  };

  counterResponse(value, key, index) {
    this.refMatch.battleRoyalTeams[index] = {
      ...this.refMatch?.battleRoyalTeams[index],
      [key]: value,
    };
  }

  enableEdit(m) {
    this.isEdit = true;
    this.refMatch = JSON.parse(JSON.stringify(m));
  }

  fieldSorting(field, enableEventSorting = false) {
    if (enableEventSorting) {
      this.rSorting[field] = this.rSorting[field] == 1 ? -1 : 1;
      this.activeAttributeSorting = field;
    }
    let funcRef;

    if (field == 'name') {
      // For string sorting
      const attr =
        this.tournament?.participantType == 'individual' ? 'name' : 'teamName';
      funcRef =
        this.rSorting[field] == -1
          ? (a: any, b: any) => (a?.playerId[attr] > b?.playerId[attr] ? -1 : 1)
          : (a: any, b: any) =>
              a?.playerId[attr] > b?.playerId[attr] ? 1 : -1;
    } else {
      // For number sorting
      funcRef =
        this.rSorting[field] == -1
          ? (a: any, b: any) => a[field] - b[field]
          : (a: any, b: any) => b[field] - a[field];
    }

    this.stage[this.currStage - 1]?.group[this.currGroup - 1]?.round[
      this.currRound - 1
    ]?.battleRoyalTeams?.sort(funcRef);
  }

  standingSorting(field, enableEventSorting = false, index = 0) {
    if (enableEventSorting) {
      this.sSorting[field] = this.sSorting[field] == 1 ? -1 : 1;
      this.activeAttributeStandingSorting = field;
    }

    let funcRef;

    if (field == 'name') {
      // For string sorting
      funcRef =
        this.sSorting[field] == -1
          ? (a: any, b: any) => (a?.player?.name > b?.player?.name ? -1 : 1)
          : (a: any, b: any) => (a?.player?.name > b?.player?.name ? 1 : -1);
    } else if (field == 'totalScore') {
      // For total score sorting
      funcRef =
        this.sSorting[field] == -1
          ? (a: any, b: any) => a[field] - b[field]
          : (a: any, b: any) => b[field] - a[field];
    } else {
      // For round score sorting
      funcRef =
        this.sSorting[field] == -1
          ? (a: any, b: any) => a.round[index] - b.round[index]
          : (a: any, b: any) => b.round[index] - a.round[index];
    }

    this.standings.sort(funcRef);
  }

  receiveSliderValue(value, i) {
    this.refMatch.battleRoyalTeams[i] = {
      ...this.refMatch.battleRoyalTeams[i],
      ...(value && {
        noOfKill: 0,
        placement: 0,
      }),
      notPlaying: value,
    };
  }
}
