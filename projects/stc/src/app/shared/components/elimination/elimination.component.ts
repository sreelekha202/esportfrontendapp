import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  OnChanges,
  SimpleChanges,
} from '@angular/core';
import { IMatch, ITournament } from '../../../shared/models';
import { TranslateService } from '@ngx-translate/core';
import { EsportsToastService, EsportsUserService } from 'esports';
import { environment } from '../../../../environments/environment';

const API = environment.apiEndPoint;

@Component({
  selector: 'app-elimination',
  templateUrl: './elimination.component.html',
  styleUrls: ['./elimination.component.scss'],
})
export class EliminationComponent implements OnInit, OnChanges {
  @Input() structure: any;
  @Input() type: string;
  @Input() isSeeded: boolean;
  @Input() isAdmin: boolean;
  @Input() participantId: string;
  @Input() isLoaded: boolean;
  @Input() enableWebview: boolean;
  @Input() disableStanding: boolean;
  @Input() tournament: ITournament;
  @Input() hideMockStructure: boolean;

  @Output() isRefresh = new EventEmitter<boolean>(false);

  isUpdateMatchScore = false;
  match: IMatch;
  API = environment.apiEndPoint;
  countryList = [];

  constructor(
    private eSportsToastService: EsportsToastService,
    private eSportsUserService: EsportsUserService,
    private translateService: TranslateService
  ) {}

  ngOnInit(): void {
    this.getAllCountries();
  }

  getAllCountries = async () => {
    try {
      const data = await this.eSportsUserService.getAllCountries().toPromise();
      this.countryList = data?.countries || [];
    } catch (error) {
      this.eSportsToastService.showError(
        error?.error?.message || error?.message
      );
    }
  };

  updatedScoreCard(data) {
    this.isUpdateMatchScore = data.isOpenScoreCard;
    this.match = data.match;
  }

  exitFromScoreCard(data) {
    if (typeof data === 'object') {
      this.isUpdateMatchScore = data.isOpenScoreCard;
      if (data.refresh) {
        this.isRefresh.emit(data.refresh);
      }
    } else {
      this.isRefresh.emit(data);
    }
  }

  ngOnChanges(simpleChanges: SimpleChanges) {
    if (simpleChanges.hasOwnProperty('isSeeded') && !this.hideMockStructure) {
      if (typeof this.isSeeded === 'boolean' && !this.isSeeded) {
        this.eSportsToastService.showInfo(
          this.translateService.instant('ELIMINATION.MOCK_STRUCTURE')
        );
      }
    }
  }
}
