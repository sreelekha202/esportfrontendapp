import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'participantType',
})
export class ParticipantTypePipe implements PipeTransform {
  transform(value, type) {
    return type == 'individual'
      ? value?.name || value?.teamName
      : value?.teamName;
  }
}
