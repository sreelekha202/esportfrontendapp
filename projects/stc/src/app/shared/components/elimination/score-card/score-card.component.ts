import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  OnDestroy,
  // Inject,
  // PLATFORM_ID,
} from '@angular/core';
// import { isPlatformBrowser } from '@angular/common';

import { BracketService, UtilsService } from '../../../../core/service';
import { Router } from '@angular/router';
import { AppHtmlRoutes } from '../../../../app-routing.model';

import {
  IMatch,
  IMatchSet,
  ITournament,
  IUser,
} from '../../../../shared/models';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import {
  EsportsChatService,
  EsportsUserService,
  EsportsToastService,
} from 'esports';
import { Subscription } from 'rxjs';
import { EsportsGtmService, SuperProperties } from 'esports';

@Component({
  selector: 'app-score-card',
  templateUrl: './score-card.component.html',
  styleUrls: ['./score-card.component.scss'],
})
export class ScoreCardComponent implements OnInit, OnChanges, OnDestroy {
  @Input() enableWebview: boolean = false;
  @Input() match: IMatch;
  @Input() participantId;
  @Input() tournament: ITournament;
  @Input() isShowTitle: boolean = true;
  @Input() isShowChat: boolean = true;
  @Input() isShowReportBtn: boolean = true;
  @Input() isShowDisqualifySection: boolean = true;
  @Input() isEventOrganizerOrAdmin: boolean = false;
  @Input() isLadderTournament: boolean = false;

  // public isBrowser: boolean;
  // public matchdetails: any;
  // public showChat: boolean = false;
  // public typeofchat: any;
  @Output() exit = new EventEmitter<any>();

  isLoaded: boolean = false;

  screenShot: string;
  showResultLog = false;

  matchDetails: IMatch;
  participantSet: IMatchSet;
  user: IUser;

  showLog: boolean = false;
  tournamentOrganiserID: string = '';
  currentUserID: string = '';
  windowPositionAbuser: String = 'chat_window chat_window_abuser_drawer';
  chats: any;

  disQualifyStatus: Array<string> = [
    'No show',
    'Walkout',
    'Rule violation',
    'Improper behavior',
  ];

  teamList: Array<any> = [];

  disQualifiedTeam: any = {
    reason: '',
    team: null,
  };

  userSubscription: Subscription;
  matchEventSubscription: Subscription;
  isMatchParticipant: boolean = false;
  windowposition: String = 'chat_window chat_window_right';
  logRowData: any = [];
  logButton: any = {
    status: 0,
  };

  constructor(
    // @Inject(PLATFORM_ID) public platformId,
    private bracketService: BracketService,
    private modalService: NgbModal,
    private eSportsToastService: EsportsToastService,
    private translateService: TranslateService,
    private utilsService: UtilsService,
    public router: Router,
    public esportsChatService: EsportsChatService,
    private gtmService: EsportsGtmService,
    public esportsUserService: EsportsUserService
  ) {
    // this.isBrowser = isPlatformBrowser(platformId);
  }

  ngOnInit(): void {
    this.fetchCurrentUser();
    if (this.isEventOrganizerOrAdmin) {
      this.isShowChat = false;
      this.showLog = true;
    }
    this.esportsChatService
      ?.getOwnerForTournament(this.tournament._id)
      .subscribe((ownerName: any) => {
        this.tournamentOrganiserID = ownerName?.organiserId;
        this.currentUserID = this.esportsChatService.getCurrentUserId();
        if (this.isEventOrganizerOrAdmin) {
          this.isShowChat = false;
          this.showLog = true;
        } else {
          if (this.currentUserID == this.tournamentOrganiserID) {
            this.isShowChat = false;
            this.showLog = true;
          } else {
            this.isShowChat = true;
            this.showLog = false;
          }
        }
      });
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.hasOwnProperty('match') && changes.match.currentValue) {
      this.fetchMatchDetails();
      this.esportsChatService.connectMatchEvents(this.match?._id);
      this.fetchMatchEvents(this.match?._id);
    }
  }

  ngOnDestroy() {
    this.esportsChatService.disconnectMatchEvents(this.match?._id);
    this.userSubscription?.unsubscribe();
    this.matchEventSubscription?.unsubscribe();
  }

  fetchCurrentUser = () => {
    this.userSubscription = this.esportsUserService.currentUser.subscribe(
      (data) => {
        if (data) {
          this.user = data;
          this.pushGTMTags('Enter_Match_Result_Clicked');
        }
      }
    );
  };

  getMatchResultLogs = async () => {
    try {
      this.isLoaded = true;
      const matchLog = await this.bracketService.fetchMatchResults(
        this.match?._id
      );
      this.showResultLog = true;
      this.isLoaded = false;
      this.logButton.status = 1;
      if (matchLog.data.length > 0) {
        this.logRowData = matchLog.data;
      }
    } catch (error) {
      this.isLoaded = false;
      this.showAlertBasedOnPlatform(
        error?.error?.message || error?.message,
        'showError'
      );
    }
  };

  fetchMatchEvents = (matchId) => {
    this.matchEventSubscription = this.esportsChatService
      .getMatchEvents(matchId)
      .subscribe((data) => {
        if (data?.eventType == 'refresh' && data?.userId != this.user?._id) {
          this.eSportsToastService.showInfo(
            this.translateService.instant('ELIMINATION.SCREEN_REFRESH'),
            false
          );
        }
      });
  };

  setMatchDetails(md) {
    this.matchDetails = md;
  }

  fetchMatchDetails = async () => {
    try {
      this.isLoaded = true;
      const query = {
        tournamentId: this.tournament._id,
        _id: this.match?._id,
      };
      const queryParam = `?query=${this.utilsService.getEncodedQuery(query)}`;

      const match = await this.bracketService.fetchAllMatches(queryParam);
      this.matchDetails = match?.data?.length ? match.data[0] : null;
      if (this.matchDetails) {
        this.teamList = [
          { ...this.matchDetails.teamA },
          { ...this.matchDetails.teamB },
        ];
        this.disQualifiedTeam = this.matchDetails?.disQualifiedTeam;
        if (
          this.currentUserID == this.matchDetails?.teamA?.userId._id ||
          this.currentUserID == this.matchDetails?.teamB?.userId?._id
        ) {
          if (this.isEventOrganizerOrAdmin) {
            this.isShowChat = false;
            this.showLog = true;
          } else {
            if (this.currentUserID == this.tournamentOrganiserID) {
              this.isShowChat = false;
              this.showLog = true;
            } else {
              this.isShowChat = true;
              this.showLog = false;
            }
          }
        }
        this.esportsChatService
          ?.getAllChatMessages(this.matchDetails._id, 1, 30)
          .subscribe((chats: any) => {
            this.chats = JSON.stringify(chats?.docs).replace(/"/g, '\\"');
          });
      }
      this.isLoaded = false;
    } catch (error) {
      this.isLoaded = false;
      this.showAlertBasedOnPlatform(
        error?.error?.message || error?.message,
        'showError'
      );
    }
  };

  /**
   * Exit from Score screen by passing refresh(true) to fetch latest bracket Scores
   */
  exitFromPage() {
    this.exit.emit({ refresh: true, isOpenScoreCard: false });
  }

  /**
   * Admin submit the update score
   * @param set set
   */
  submitScore = async (set) => {
    try {
      const status = await this.isAllowScore(set.teamAScore, set.teamBScore);
      if (!status) return;

      this.isLoaded = true;

      const query = {
        _id: this.matchDetails._id,
        'sets.id': set.id,
      };
      const queryParam = `?query=${this.utilsService.getEncodedQuery(query)}`;

      const payload = {
        teamAScore: set.teamAScore,
        teamBScore: set.teamBScore,
      };
      const match = await this.bracketService.updateMatch(queryParam, payload);
      if (!match.success) {
        this.showAlertBasedOnPlatform(match.message, 'showInfo');
      }
      this.isLoaded = false;
      this.esportsChatService.emitMatchEvents(this.match?._id, this.user?._id);
      this.fetchMatchDetails();
    } catch (error) {
      this.isLoaded = false;
      this.showAlertBasedOnPlatform(
        error?.error?.message || error?.message,
        'showError'
      );
    }
  };

  disqualify = async () => {
    try {
      const invalid = Object.values(this.disQualifiedTeam).some(
        (item) => !item
      );
      if (invalid) {
        this.showAlertBasedOnPlatform(
          this.translateService.instant('ELIMINATION.DISQUALIFY_REQUIRED'),
          'showInfo'
        );
        return;
      }
      this.isLoaded = true;

      const loserTeam = this.disQualifiedTeam.team;
      const winnerTeam = this.teamList.find(
        (team) => team._id !== loserTeam
      )?._id;

      const payload = {
        disQualifiedTeam: this.disQualifiedTeam,
        winnerTeam,
        loserTeam,
        matchStatus: 'completed',
      };
      const query = {
        _id: this.matchDetails?._id,
        tournamentId: this.tournament?._id,
      };
      const queryParam = `?query=${this.utilsService.getEncodedQuery(query)}`;

      const match = await this.bracketService.updateMatch(queryParam, payload);

      this.showAlertBasedOnPlatform(
        match.message,
        match.success ? 'showSuccess' : 'showInfo'
      );
      this.isLoaded = false;
      this.sendEvents(match?.tournamentFinished);
      this.fetchMatchDetails();
    } catch (error) {
      this.isLoaded = false;
      this.showAlertBasedOnPlatform(
        'showError',
        error?.error?.message || error?.message
      );
    }
  };

  showAlertBasedOnPlatform = (message, type) => {
    this.enableWebview
      ? this.utilsService.showNativeAlert(message)
      : this.eSportsToastService[type](message);
  };

  genrateReport(matchId) {
    if (!this.enableWebview) {
      this.exit.emit({ refresh: true, isOpenScoreCard: false });
      this.router.navigate([
        `${AppHtmlRoutes.tournament}/report/${this.matchDetails?._id}/${this.matchDetails?.tournamentId?._id}`,
      ]);
    }
  }

  isAllowScore = async (setA, setB) => {
    const isInValid =
      setA == setB &&
      ['swiss_safeis', 'ladder'].includes(this.tournament?.bracketType);
    if (isInValid) {
      this.eSportsToastService.showInfo(
        this.translateService.instant('ELIMINATION.SCORE_ALERT')
      );
    }
    return !isInValid;
  };

  getUpdatedSet(updatedSet, index) {
    this.matchDetails.sets[index] = updatedSet;
  }

  saveSetScore = async (isSubmit: boolean, set: IMatchSet) => {
    try {
      if (isSubmit) {
        const status = await this.isAllowScore(set.teamAScore, set.teamBScore);
        if (!status) return;

        const query = {
          _id: this.matchDetails?._id,
          'sets.id': set.id,
        };
        const queryParam = `?query=${this.utilsService.getEncodedQuery(query)}`;

        const payload = {
          teamAScore: set.teamAScore,
          teamBScore: set.teamBScore,
          teamAScreenShot: set.teamAScreenShot,
          teamBScreenShot: set.teamBScreenShot,
        };
        this.isLoaded = true;

        const match = this.isEventOrganizerOrAdmin
          ? await this.bracketService.updateMatch(queryParam, payload)
          : await this.bracketService.updateParticipantScore(
              queryParam,
              payload
            );
        this.pushGTMTags('Enter_Match_Result_Confirmed');

        if (!this.isEventOrganizerOrAdmin)
          this.showAlertBasedOnPlatform(match?.message, 'showSuccess');

        if (!match.success) {
          this.eSportsToastService.showInfo(
            this.translateService.instant(match.message)
          );
          // this.showAlertBasedOnPlatform(match.message, 'showInfo');
        }

        this.isLoaded = false;
        this.sendEvents(match?.tournamentFinished);
        this.fetchMatchDetails();
      }
    } catch (error) {
      this.isLoaded = false;
      this.showAlertBasedOnPlatform(
        error?.error?.message || error?.message,
        'showError'
      );
    }
  };

  refreshScoreBoard(isRefresh: boolean) {
    if (isRefresh) {
      this.fetchMatchDetails();
    }
  }

  resetLogButton() {
    this.showResultLog = false;
    this.logButton.status = 0;
  }

  chatFunction(id) {
    this.exitFromPage();
    this.esportsChatService.getMatchInformation(id).subscribe((result: any) => {
      const mtchdetails = {
        matchid: result.matchdetails._id,

        teamAId: result.matchdetails.teamA.userId._id,

        teamBId: result.matchdetails.teamB.userId._id,

        tournamentid: result.matchdetails.tournamentId,

        typeofchat: 'tournament',

        users: [
          {
            _id: result.matchdetails.teamA.userId._id,
            fullName: result.matchdetails.teamA.userId.fullName,
            profilePicture: result.matchdetails.teamA.userId.profilePicture,
          },
          {
            _id: result.matchdetails.teamB.userId._id,
            fullName: result.matchdetails.teamB.userId.fullName,
            profilePicture: result.matchdetails.teamB.userId.profilePicture,
          },
        ],
      };

      this.esportsChatService.setWindowPos(this.windowposition);

      let firstChat = this.esportsChatService.getChatStatus();

      if (firstChat == true) {
        this.esportsChatService.setChatStatus(false);
        this.esportsChatService?.disconnectMatch(
          mtchdetails.matchid,
          'tournament'
        );

        this.esportsChatService.setTextButtonStatus(false);
      } else {
        this.esportsChatService.setCurrentMatch(mtchdetails);

        this.esportsChatService.setTypeOfChat('tournament');

        this.esportsChatService.setChatStatus(true);

        this.esportsChatService.setTextButtonStatus(true);
      }
    });
  }

  onChatClick(data) {
    if (data.click) {
      this.exitFromPage();
    }
  }

  sendEvents(isTournamentFinished: boolean) {
    this.esportsChatService.emitMatchEvents(this.match?._id, this.user?._id);
    this.exit.emit({ refresh: isTournamentFinished, isOpenScoreCard: true });
  }

  pushGTMTags(eventName: string) {
    let superProperties: SuperProperties = {};
    if (this.user) {
      superProperties = this.gtmService.assignLoggedInUsedData(this.user);
    }

    this.gtmService.gtmEventWithSuperProp({
      eventName: eventName,
      superProps: superProperties,
    });
  }
}
