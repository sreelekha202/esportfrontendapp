import {
  Component,
  Input,
  OnInit,
  OnChanges,
  OnDestroy,
  Inject,
  PLATFORM_ID,
} from '@angular/core';
import { CommentService } from '../../../core/service';
import { toggleOpacity, VisibilityState } from '../../../animations';
import {
  InfoPopupComponent,
  InfoPopupComponentData,
  InfoPopupComponentType,
} from '../../popups/info-popup/info-popup.component';
import { MatDialog } from '@angular/material/dialog';
import { TranslateService } from '@ngx-translate/core';
import { formatDistanceToNow } from 'date-fns';
import {
  EsportsUserService,
  EsportsToastService,
  SuperProperties,
  EsportsGtmService,
  IUser,
  EventProperties,
} from 'esports';
import { Subscription } from 'rxjs';
import { isPlatformBrowser } from '@angular/common';
@Component({
  selector: 'app-discussion',
  templateUrl: './discussion.component.html',
  styleUrls: ['./discussion.component.scss'],
  animations: [toggleOpacity],
})
export class DiscussionComponent implements OnInit, OnChanges, OnDestroy {
  @Input() type: string;
  @Input() objectId: string;
  @Input() userId: string;
  @Input() allowComment: boolean = false;
  @Input() text: string;
  @Input() creatorId;

  VisibilityState = VisibilityState;
  collapseOrExpand: VisibilityState = VisibilityState.Visible;
  isBrowser: boolean;
  comments: Array<any> = [];
  page: number = 1;
  isAdmin = false;
  isLoadingNextComments: boolean = false;
  hasNextComment: boolean = true;
  allowDelete: boolean = false;
  isAllowToPinTheComment: boolean = false;
  userSubscription: Subscription;
  currentUser: IUser;

  constructor(
    private userService: EsportsUserService,
    private commentService: CommentService,
    private eSportsToastService: EsportsToastService,
    private matDialog: MatDialog,
    private translateService: TranslateService,
    private gtmService: EsportsGtmService,
    @Inject(PLATFORM_ID) private platformId
  ) {
    this.isBrowser = isPlatformBrowser(platformId);
  }

  ngOnInit(): void {
    if (this.isBrowser) {
      this.getCurrentUserDetails();
    }
  }

  ngOnDestroy(): void {
    this.userSubscription?.unsubscribe();
  }

  getCurrentUserDetails() {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.currentUser = data;
        if (data.accountType == 'admin') {
          this.allowDelete = true;
          this.isAllowToPinTheComment = true;
        }
        if (data._id == this.creatorId) {
          this.allowDelete = true;
          this.isAllowToPinTheComment = true;
        }
      }
    });
  }
  ngOnChanges() {
    if (this.type && this.objectId) {
      this.comments = [];
      this.page = 1;
      this.getAllComment();
    }
  }

  getAllComment = async () => {
    try {
      if (this.isLoadingNextComments) return;
      this.isLoadingNextComments = true;
      const { data, hasNext } = await this.commentService.getAllComment(
        this.type,
        this.objectId,
        this.page
      );

      this.comments = [...this.comments, ...data];
      this.page += this.page;
      this.hasNextComment = hasNext;
      this.isLoadingNextComments = false;
    } catch (error) {
      this.isLoadingNextComments = false;
      this.eSportsToastService.showError(
        error?.error?.message || error?.message
      );
    }
  };

  getAllReply = async (commentId, index) => {
    try {
      if (this.comments[index].isLoadingNextReplies) return;

      this.comments[index].page = this.comments[index].page || 1;
      this.comments[index].isLoadingNextReplies =
        this.comments[index].isLoadingNextReplies || true;

      const { data, hasNext } = await this.commentService.getAllComment(
        'comment',
        commentId,
        this.comments[index].page
      );

      this.comments[index].replies = [
        ...(this.comments[index].replies || []),
        ...data,
      ];
      this.comments[index].page += this.comments[index].page;
      this.comments[index].hasNextReply = hasNext;

      this.comments[index].isLoadingNextReplies =
        !this.comments[index].isLoadingNextReplies;

      this.comments[index].showReplies = true;
      this.comments[index].totalReplies =
        this.comments[index]?.replies?.length < 50
          ? this.comments[index]?.replies?.length
          : this.comments[index].totalReplies;
    } catch (error) {
      this.comments[index].isLoadingNextReplies =
        !this.comments[index].isLoadingNextReplies;
      this.eSportsToastService.showError(
        error?.error?.message || error?.message
      );
    }
  };

  getCommentResponse = (payload) => {

    if (this.type == 'tournament') {
      this.pushGTMTags('Tournament_Discussion_Message');
    } else {
      this.pushGTMTags('Add_Post_Comment', payload);
    }

    this.comments.unshift(payload);
    this.comments.sort((a, b) => b.isPinned - a.isPinned);
  };
  getUpdatedCommentResponse = (payload, i) => {
    this.comments[i] = { ...this.comments[i], ...payload };
    this.comments[i].isEdit = false;
  };

  getCommentCancelResponse = (payload, i) => {
    this.comments[i].isEdit = false;
  };

  getReplyResponse = (payload, i) => {
    if (this.comments[i]?.showReplies) {
      if (!this.comments[i]?.replies?.length) {
        this.comments[i].replies = [payload];
      } else {
        this.comments[i].replies.unshift(payload);
      }
    }
    this.comments[i].totalReplies = (this.comments[i].totalReplies || 0) + 1;
    this.comments[i].openReply = false;
  };

  getReplyCancelResponse = (event, i) => {
    this.comments[i].openReply = false;
  };

  getUpdatedReplyResponse = (payload, i, j) => {
    this.comments[i].replies[j] = {
      ...this.comments[i].replies[j],
      ...payload,
    };
    this.comments[i].replies[j].isEdit = false;
  };

  likeOrUnLikeComment = async (
    objectId: string,
    objectType: string,
    type: number,
    i = 0,
    j = 0,
    isReply = false
  ) => {
    try {
      if (!this.userId) {
        this.eSportsToastService.showInfo('DISCUSSION.LOGIN_TO_LIKE');
        return;
      }

      // if (!this.allowComment) {
      //   this.eSportsToastService.showInfo(
      //     'Please Join this tournament to like this comment'
      //   );
      //   return;
      // }

      const payload = {
        objectId,
        objectType,
        type: type == -1 ? 1 : Number(!type),
      };

      const like = await this.commentService.upsertLike(payload);
      if (isReply) {
        this.comments[i].replies[j] = {
          ...this.comments[i].replies[j],
          ...like.data,
        };
      } else {
        this.comments[i] = { ...this.comments[i], ...like.data };
      }
      if (this.type == 'article') {
        this.pushGTMTags('Like_Users_Reply');
      }

    } catch (error) {
      this.eSportsToastService.showError(
        error?.error?.message || error?.message
      );
    }
  };

  deleteCommentOrReply = async (id, i, j = -1) => {
    try {
      const data: InfoPopupComponentData = {
        title: this.translateService.instant('DISCUSSION.DELETE_COMMENT_TITLE'),
        text: this.translateService.instant('DISCUSSION.DELETE_COMMENT_TEXT'),
        type: InfoPopupComponentType.confirm,
        btnText: this.translateService.instant('BUTTON.CONFIRM'),
      };

      const confirmed = await this.matDialog
        .open(InfoPopupComponent, { data })
        .afterClosed()
        .toPromise();

      if (confirmed) {
        const deleteComment = await this.commentService.deleteComment(id);
        if (deleteComment.data) {
          if (j > -1) {
            this.comments[i].replies.splice(j, 1);
            this.comments[i].totalReplies = this.comments[i].totalReplies - 1;
          } else {
            this.comments.splice(i, 1);
          }
          this.eSportsToastService.showSuccess(deleteComment?.message);
        }
      }
    } catch (error) {
      this.eSportsToastService.showError(
        error?.error?.message || error?.message
      );
    }
  };

  getDateFromNow(date) {
    return formatDistanceToNow(new Date(date), { addSuffix: true });
  }

  async pinComment(id) {
    await this.commentService.pinToComment(id);
    this.comments = this.comments
      .map((item) => {
        if (item._id === id) {
          return {
            ...item,
            isPinned: !item.isPinned || false,
          };
        } else {
          return item;
        }
      })
      .sort((a, b) => {
        let n = b.isPinned - a.isPinned;
        if (n !== 0) {
          return n;
        }
        return b.modifiedOn - a.modifiedOn;
      });
  }

  gtmevents() {
    let status;
    if (this.collapseOrExpand == 'visible') {
      status = 'Show';
    } else if (this.collapseOrExpand == 'hidden') {
      status = 'Hide';
    }
    let event = {
      details: null,
      platform: 'Web',
    };
    let eventName = `${status}_Comments`;

    let superProperties: SuperProperties = {};
    if (this.currentUser) {
      superProperties = this.gtmService.assignLoggedInUsedData(
        this.currentUser
      );
    }
    this.gtmService.gtmEventWithSuperProp({
      eventName: eventName,
      superProps: superProperties,
      eventProps: event,
    });
  }

  pushGTMTags(eventName: string, eventData = null) {
    let superProperties: SuperProperties = {};
    if (this.currentUser) {
      superProperties = this.gtmService.assignLoggedInUsedData(
        this.currentUser
      );
    }

    let eventProperties: EventProperties = {};
    if (eventName == 'Add_Post_Comment') {
      eventProperties.commentLength = eventData.comment.length;
    }

    this.gtmService.gtmEventWithSuperProp({
      eventName: eventName,
      superProps: superProperties,
      eventProps: eventProperties,
    });
  }
}
