import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DiscussionComponent } from './discussion.component';
import { MessageComponent } from './message/message.component';
import { NeedLoginComponent } from './need-login/need-login.component';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { I18nModule, PipeModule } from 'esports';
import { MaterialModule } from '../../modules/material.module';
import { FormatDistanceToNowPipeModule } from 'ngx-date-fns';
import { environment } from '../../../../environments/environment';

@NgModule({
  declarations: [DiscussionComponent, MessageComponent, NeedLoginComponent],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    I18nModule.forRoot(environment),
    MaterialModule,
    FormatDistanceToNowPipeModule,
    PipeModule,
  ],
  exports: [DiscussionComponent, MessageComponent, NeedLoginComponent],
})
export class DiscussionModule {}
