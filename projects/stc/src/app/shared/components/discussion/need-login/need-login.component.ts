import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {
  AppHtmlRoutes,
  AppHtmlRoutesLoginType,
} from '../../../../app-routing.model';
import { AuthServices } from '../../../../core/service';

@Component({
  selector: 'app-need-login',
  templateUrl: './need-login.component.html',
  styleUrls: ['./need-login.component.scss'],
})
export class NeedLoginComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;
  AppHtmlRoutesLoginType = AppHtmlRoutesLoginType;

  constructor(
    private router: Router,
    private authService: AuthServices,
    private activeRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {}

  redirect(data) {
    this.authService.redirectUrl = this.router.url;

    if (data === 'login') {
      this.router.navigate(
        [AppHtmlRoutes.userPageType, AppHtmlRoutesLoginType.phoneLogin],
        {
          relativeTo: this.activeRoute,
        }
      );
    } else {
      this.router.navigate(
        [AppHtmlRoutes.userPageType, AppHtmlRoutesLoginType.registration],
        {
          relativeTo: this.activeRoute,
        }
      );
    }
  }
}
