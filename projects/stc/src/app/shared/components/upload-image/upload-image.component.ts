import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { S3UploadService } from '../../../core/service';
import { EsportsToastService } from 'esports';
import { environment } from '../../../../environments/environment';
import { TranslateService } from '@ngx-translate/core';
@Component({
  selector: 'app-upload-image',
  templateUrl: './upload-image.component.html',
  styleUrls: ['./upload-image.component.scss'],
})
export class UploadImageComponent implements OnInit {
  @Input() customFormControlName: string;
  @Input() customFormGroup: FormGroup;

  @Input() title: string;
  @Input() subtitle: string;

  @Input() dimension;
  @Input() size;
  @Input() type;
  @Input() isTeam: boolean = false;

  @Input() id;
  @Input() required: boolean = false;

  isProcessing: boolean = false;

  constructor(
    private s3UploadService: S3UploadService,
    private eSportsToastService: EsportsToastService,
    private translateService: TranslateService
  ) {}

  ngOnInit(): void {}

  eventChange = async (event) => {
    try {
      this.isProcessing = true;

      var files = event.target.files;
      var file = files[0];

      if (file) {
        const upload: any = await this.s3UploadService.toBase64(file);
        // const upload: any = await this.s3UploadService.validateUploadImage(
        //   event,
        //   this.dimension,
        //   this.size,
        //   this.type
        // );

        // this.customFormGroup
        //   .get(this.customFormControlName)
        //   .setValue(upload?.data?.Location);
        this.customFormGroup.get(this.customFormControlName).setValue(upload);
        //this.eSportsToastService.showSuccess(upload?.message);
        this.isProcessing = false;
      } else {
        this.customFormGroup.get(this.customFormControlName).setValue('');
        //this.eSportsToastService.showSuccess(upload?.message);
        this.isProcessing = false;
      }
    } catch (error) {
      this.isProcessing = false;
      this.eSportsToastService.showError(
        error?.error?.message || error?.message
      );
    }
  };
  async selectFile(event) {
    this.isValidImage(event, { height: 560, width: 1088 });
  }
  isValidImage(event: any, values): Boolean {
    if (event.target.files && event.target.files[0]) {
      if (event.target.files[0]['size'] / (1000 * 1000) > 2) {
        this.eSportsToastService.showError(
          this.translateService.instant('ARTICLE_POST.IMG_LESS')
        );
        return;
      }

      if (
        event.target.files[0].type != 'image/jpeg' &&
        event.target.files[0].type != 'image/jpg' &&
        event.target.files[0].type != 'image/png'
      ) {
        this.eSportsToastService.showError(
          this.translateService.instant('ARTICLE_POST.UPLOAD_CORRECT')
        );
        return;
      }

      try {
        const file = event.target.files[0];

        const reader = new FileReader();
        reader.readAsDataURL(file);
        let th = this;
        reader.onload = (e) => {
          let image: any = new Image();
          image.src = e.target.result;
          image.onload = function () {
            let height = this.height;
            let width = this.width;
            if (height == values['height'] && width == values['width']) {
              th.eSportsToastService.showInfo(
                this.translateService.instant('ARTICLE_POST.PROPER')
              );
            }
            th.upload(event);
          };
        };
      } catch (err) {
        return false;
      }
    } else {
      return false;
    }
  }
  async upload(event) {
    const files = event.target.files;
    let promises = [];
    for (let obj of files) {
      promises.push(this.toBase64(obj));
    }
    let Base64String = await Promise.all(promises);
    let imageData = {
      path: environment.teamS3BucketName,
      files: Base64String,
    };
    this.s3UploadService.fileUpload(imageData).subscribe(
      (res) => {
        if (
          res &&
          res['data'] &&
          res['data'][0] &&
          res['data'][0]['Location']
        ) {
          this.customFormGroup
            .get(this.customFormControlName)
            .setValue(res['data'][0]['Location']);
          this.eSportsToastService.showSuccess(
            this.translateService.instant('ARTICLE_POST.BANNER_UPLOAD')
          );
        }
      },
      (err) => {
        if (
          err &&
          err.error &&
          err.error.statusCode &&
          err.error.statusCode == 401
        ) {
          this.eSportsToastService.showError(
            this.translateService.instant('ARTICLE_POST.SESSION_EXPIRED')
          );
        } else {
          this.eSportsToastService.showError(
            this.translateService.instant('ARTICLE_POST.ERROR_BANNER')
          );
        }
      }
    );
  }
  toBase64(file) {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = (error) => reject(error);
    });
  }
}
