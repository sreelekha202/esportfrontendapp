import { Component, OnInit, Input } from '@angular/core';
import { EsportsToastService, IPagination, ITournament } from 'esports';
import { BracketService, TournamentService } from '../../../../core/service';
import { ScorePopupComponent } from '../../../../modules/my-matches/components/score-popup/score-popup.component';
import { MatDialog } from '@angular/material/dialog';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'view-round',
  templateUrl: './view-round.component.html',
  styleUrls: ['./view-round.component.scss'],
})
export class ViewRoundComponent implements OnInit {
  @Input() stage: number = 0;
  @Input() group: number = 0;
  @Input() round: number = 0;

  @Input() tournament: ITournament;
  @Input() isScoreEdit: boolean = false;
  @Input() isAdmin: boolean = false;

  page: IPagination = {
    activePage: 1,
    totalItems: 1,
    itemsPerPage: 1,
    maxSize: 10,
  };
  isProcessing: boolean = false;
  isRoundProcessing: boolean = false;
  isCollapsed: boolean = false;
  enableParticipantScoring: boolean = false;
  matchData: any;
  setHeader: Array<any> = [];
  roundDetailsForm: FormGroup;

  constructor(
    private eSportsToastService: EsportsToastService,
    private tournamentService: TournamentService,
    private bracketService: BracketService,
    private matDialog: MatDialog,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.roundDetailsForm = this.fb.group({
      enableParticipantScoring: [false],
    });
  }

  ngOnChanges() {
    if (this.tournament && this.round) this.fetchRoundMatches();
  }

  fetchRoundMatches = async () => {
    try {
      if (
        this.isProcessing ||
        !this.tournament?._id ||
        !this.round ||
        !this.isCollapsed
      )
        return;
      this.isProcessing = true;
      let queryParam = `page=${this.page.activePage}`;

      if (this.stage) queryParam = queryParam + `&stage=${this.stage}`;

      if (this.group) queryParam = queryParam + `&group=${this.group}`;

      if (this.round) queryParam = queryParam + `&round=${this.round}`;

      const { data } = await this.bracketService.fetchMatchesByPage(
        this.tournament?._id,
        queryParam
      );
      const highestActiveSetInMatch = await this.fetchHighestActiveSetInMatch(
        data?.docs
      );
      this.createSetHeader(this.tournament?.noOfSet, highestActiveSetInMatch);
      this.matchData = data;
      this.page.itemsPerPage = data?.limit;
      this.page.totalItems = data?.totalDocs;
      this.isProcessing = false;
    } catch (error) {
      this.isProcessing = false;
      const errorMessage = error?.error?.message || error?.message;
      this.eSportsToastService.showError(errorMessage);
    }
  };

  fetchHighestActiveSetInMatch = async (
    matchList: Array<any>
  ): Promise<number> => {
    let highestActiveSetInMatch = 0;
    for (let match of matchList) {
      if (highestActiveSetInMatch < match?.sets?.length) {
        highestActiveSetInMatch = match?.sets?.length;
      }
    }
    return highestActiveSetInMatch;
  };

  createSetHeader(noOfSetInMatch: number, highestActiveSetInMatch: number) {
    const limit =
      noOfSetInMatch < highestActiveSetInMatch
        ? highestActiveSetInMatch
        : noOfSetInMatch;
    this.setHeader = [];
    for (let i = 0; i < limit; i++) {
      this.setHeader.push({ name: `Set ${i + 1}` });
    }
  }

  openScoreCard = async (m) => {
    try {
      const payload = {
        width: '700px',
        height: '700px',
        data: {
          match: {
            _id: m._id,
            tournamentId: this.tournament,
          },
          isEventOrganizerOrAdmin: this.isAdmin,
        },
      };

      await this.matDialog
        .open(ScorePopupComponent, payload)
        .afterClosed()
        .toPromise();
      this.fetchRoundMatches();
    } catch (error) {}
  };

  pageChanged(page: number) {
    this.page.activePage = page;
    this.fetchRoundMatches();
  }

  fetchRoundDetails = async () => {
    try {
      if (
        !this.isAdmin ||
        this.isRoundProcessing ||
        !this.tournament?._id ||
        !this.round ||
        !this.isCollapsed
      )
        return;
      this.isRoundProcessing = true;
      let queryParam = '';
      if (this.stage) queryParam = queryParam + `&stage=${this.stage}`;
      if (this.group) queryParam = queryParam + `&group=${this.group}`;
      if (this.round) queryParam = queryParam + `&round=${this.round}`;

      const { data } = await this.tournamentService.fetchRoundDetails(
        this.tournament?._id,
        queryParam
      );
      this.roundDetailsForm.patchValue({
        enableParticipantScoring: !!data?.enableParticipantScoring
      });
      this.isRoundProcessing = false;
    } catch (error) {
      this.isRoundProcessing = false;
      // const errorMessage = error?.error?.message || error?.message;
      // this.eSportsToastService.showError(errorMessage);
    }
  };

  participantScoringHandler = async (
    enableParticipantScoring: boolean = false
  ) => {
    try {
      const payload = {
        enableParticipantScoring,
      };
      let queryParam = '';
      if (this.stage) queryParam = queryParam + `&stage=${this.stage}`;
      if (this.group) queryParam = queryParam + `&group=${this.group}`;
      if (this.round) queryParam = queryParam + `&round=${this.round}`;

      const { data, message } = await this.tournamentService.updateRoundDetails(
        this.tournament?._id,
        queryParam,
        payload
      );
      this.roundDetailsForm.patchValue({
        enableParticipantScoring: !!data?.enableParticipantScoring
      });
      this.eSportsToastService.showSuccess(message);
    } catch (error) {
      const errorMessage = error?.error?.message || error?.message;
      this.eSportsToastService.showError(errorMessage);
    }
  };
}
