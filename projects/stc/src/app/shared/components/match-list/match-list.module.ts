import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatchListComponent } from './match-list.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {
  FontAwesomeModule,
  FaIconLibrary,
} from '@fortawesome/angular-fontawesome';
import { faSortDown } from '@fortawesome/free-solid-svg-icons';
import {
  I18nModule,
  EsportsLoaderModule,
  PipeModule,
  EsportsCustomPaginationModule,
} from 'esports';
import { EliminationModule } from '../elimination/elimination.module';
import { environment } from '../../../../environments/environment';
import { LadderMatchComponent } from './ladder-match/ladder-match.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { MatIconModule } from '@angular/material/icon';
import { MatTooltipModule } from '@angular/material/tooltip';
import { ViewRoundComponent } from "./view-round/view-round.component";
import { FormComponentModule } from "../form-component/form-component.module";
@NgModule({
  declarations: [MatchListComponent, LadderMatchComponent, ViewRoundComponent],
  imports: [
    CommonModule,
    NgbModule,
    FontAwesomeModule,
    EsportsLoaderModule.setColor('#501090'),
    I18nModule.forRoot(environment),
    EliminationModule,
    NgxDatatableModule,
    PipeModule,
    MatIconModule,
    MatTooltipModule,
    EsportsCustomPaginationModule,
    FormComponentModule
  ],
  exports: [MatchListComponent],
})
export class MatchListModule {
  constructor(private library: FaIconLibrary) {
    library.addIcons(faSortDown);
  }
}
