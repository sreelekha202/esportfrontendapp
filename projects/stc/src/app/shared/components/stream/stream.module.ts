import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StreamComponent } from './stream.component';
import {
  FontAwesomeModule,
  FaIconLibrary,
} from '@fortawesome/angular-fontawesome';
import {
  faTrash,
  faEdit,
  faCopy,
  faPlay,
  faStop,
  faExpand,
} from '@fortawesome/free-solid-svg-icons';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { CustomStreamingModule } from '../custom-streaming/custom-streaming.module';
import { I18nModule, PipeModule } from 'esports';
import { MatTooltipModule } from '@angular/material/tooltip';
import { environment } from '../../../../environments/environment';
import { IframeTrackerDirective } from './iframeTrack.directive'

@NgModule({
  declarations: [StreamComponent, IframeTrackerDirective],
  imports: [
    CommonModule,
    FontAwesomeModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    PipeModule,
    I18nModule.forRoot(environment),
    MatProgressSpinnerModule,
    CustomStreamingModule,
    MatTooltipModule,
  ],
  exports: [StreamComponent, IframeTrackerDirective],
})
export class StreamModule {
  constructor(private library: FaIconLibrary) {
    library.addIcons(faTrash, faEdit, faCopy, faPlay, faStop, faExpand);
  }
}
