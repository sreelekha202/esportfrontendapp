import {
  Component,
  OnInit,
  Input,
  SimpleChanges,
  Inject,
  OnChanges,
  OnDestroy,
  PLATFORM_ID,
} from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DOCUMENT } from '@angular/common';
import { TranslateService } from '@ngx-translate/core';
import { TournamentService, UtilsService } from '../../../core/service';
import {
  EsportsLanguageService,
  EsportsConstantsService,
  EsportsToastService,
  EsportsUserService,
  EsportsGtmService,
  SuperProperties,
  IUser
} from 'esports';
import { Subscription } from 'rxjs';
import { isPlatformBrowser } from '@angular/common';


@Component({
  selector: 'app-stream',
  templateUrl: './stream.component.html',
  styleUrls: ['./stream.component.scss'],
})
export class StreamComponent implements OnInit, OnChanges, OnDestroy {
  @Input() tournamentId;
  @Input() isAdmin: boolean;
  @Input() tournament;

  streamForm: FormGroup;
  editIndex = -1;
  streamList: Array<any> = [];
  isBrowser: boolean;
  isOpenForm = false;
  providerList: Array<object> = [];
  preProviderList: Array<object> = [];
  preTournamentStream: Array<object> = [];
  playVideo;
  playback;
  customStreamLoader = false;

  // TEST DATA
  activeVideo = null;
  videoLibrary: Array<{}> = [];

  currentUser: IUser;
  userSubscription: Subscription;

  constructor(
    private fb: FormBuilder,
    private tournamentService: TournamentService,
    private modalService: NgbModal,
    private eSportsToastService: EsportsToastService,
    private utilsService: UtilsService,
    @Inject(DOCUMENT) private document: Document,
    private languageService: EsportsLanguageService,
    private ConstantsService: EsportsConstantsService,
    private userService: EsportsUserService,
    private gtmService: EsportsGtmService,
    private translateService: TranslateService,
    @Inject(PLATFORM_ID) private platformId
  ) {
    this.isBrowser = isPlatformBrowser(platformId);
  }

  ngOnInit(): void {
    if (this.isBrowser) {
      this.languageService.language.subscribe((lang) =>
        this.translateService.use(
          lang || this.ConstantsService?.defaultLangCode
        )
      );

      this.streamForm = this.fb.group({
        type: ['', Validators.required],
        videoUrl: this.fb.group({
          providerName: [null, Validators.required],
          channelName: ['', Validators.required],
        }),
        title: ['', Validators.required],
        description: ['', Validators.required],
      });

      this.userSubscription = this.userService.currentUser.subscribe(data=>{
        if (data) {
          this.currentUser = data;
        }
      })
    }
    this.userSubscription = this.userService.currentUser.subscribe(data=>{
      if (data) {
        this.currentUser = data;
      }
    })
  }

  ngOnDestroy() {
    this.userSubscription.unsubscribe();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (
      changes.hasOwnProperty('tournamentId') &&
      changes.tournamentId.currentValue &&
      changes.hasOwnProperty('isAdmin') &&
      this.isAdmin
    ) {
      this.fetchTournamentDetails(true);
    }

    if (this.tournament) {
      this.fetchTournamentDetails(false);
    }
  }

  /** Fetch tournament Streams */
  fetchTournamentDetails = async (check) => {
    try {
      let tournamentDetails;
      let response;

      const tournament404 = async () => {
        throw new Error('Tournament is not Found');
      };

      if (check) {
        const query = JSON.stringify({ _id: this.tournamentId });
        const select = `&select=stream,youtubeVideoLink,facebookVideoLink,twitchVideoLink`;

        response = await this.tournamentService
          .getTournaments({ query }, select)
          .toPromise();
      }

      tournamentDetails = !check
        ? this.tournament
        : response?.data?.length
        ? response.data[0]
        : await tournament404();

      if (
        tournamentDetails?.youtubeVideoLink ||
        tournamentDetails?.twitchVideoLink ||
        tournamentDetails?.facebookVideoLink
      ) {
        this.preTournamentStream = [];
      }

      if (tournamentDetails?.youtubeVideoLink) {
        try {
          const channelName = await this.utilsService.getVideoId(
            tournamentDetails?.youtubeVideoLink
          );
          const embedUrl: any = this.utilsService.createEmbedUrl('YOUTUBE', {
            channelName,
          });
          this.preTournamentStream.push(embedUrl);
        } catch (error) {
          this.eSportsToastService.showInfo(
            this.translateService.instant(
              'MANAGE_TOURNAMENT.STREAM.ERROR.YOUTUBE_URL'
            )
          );
        }
      }

      if (tournamentDetails?.twitchVideoLink) {
        try {
          const channelName = tournamentDetails?.twitchVideoLink
            .split('/')
            .reverse()[0];
          const embedUrl: any = this.utilsService.createEmbedUrl('TWITCH', {
            channelName,
          });
          this.preTournamentStream.push(embedUrl);
        } catch (error) {
          this.eSportsToastService.showInfo(
            this.translateService.instant(
              'MANAGE_TOURNAMENT.STREAM.ERROR.TWITCH_URL'
            )
          );
        }
      }

      if (tournamentDetails?.facebookVideoLink) {
        try {
          const match = /videos\/(\d+)+|v=(\d+)|vb.\d+\/(\d+)/;
          const channelName =
            tournamentDetails?.facebookVideoLink.match(match)[1];
          const embedUrl: any = this.utilsService.createEmbedUrl('FACEBOOK', {
            channelName,
          });
          this.preTournamentStream.push(embedUrl);
        } catch (error) {
          this.eSportsToastService.showInfo(
            this.translateService.instant(
              'MANAGE_TOURNAMENT.STREAM.ERROR.FACEBOOK_URL'
            )
          );
        }
      }

      this.streamList = tournamentDetails?.stream || [];
      this.providerList = [
        { key: 'YOUTUBE', value: 'https://www.youtube.com/watch?v=' },
        { key: 'TWITCH', value: 'https://www.twitch.tv/' },
        { key: 'LIVESTREAM', value: 'rtmp://streaming.stcplay.gg/live/' },
        { key: 'FACEBOOK', value: 'https://www.facebook.com/' },
      ];
      const promiseArray = this.streamList.map((item) => {
        const i = this.providerList.findIndex(
          (item2: any) => item2.key.toUpperCase() === item.type.toUpperCase()
        );

        if (i >= 0) {
          this.providerList.splice(i, 1);
        }

        return {
          ...item,
          url: this.utilsService.createEmbedUrl(
            item.type.toUpperCase(),
            item.videoUrl
          ),
        };
      });

      this.streamList = await Promise.all(promiseArray);
      this.preProviderList = JSON.parse(JSON.stringify(this.providerList));
    } catch (error) {
      this.eSportsToastService.showError(
        error?.error?.message || error?.message
      );
    }
  };

  /** Edit And Reset Form-value Handler */
  formAction(check: boolean) {
    this.isOpenForm = check;
    this.editIndex = -1;
    this.streamForm.reset();
    this.providerList = this.preProviderList;
  }

  /** Save Stream */
  saveStream() {
    if (this.streamForm.invalid) {
      this.streamForm.markAllAsTouched();
      return;
    }
    const data = this.streamForm.value;
    const streami = this.streamList.findIndex(
      (item: any) => item.type === data.type
    );
    if (this.editIndex >= 0) {
      this.streamList[this.editIndex] = data;
    } else if (streami < 0) {
      this.streamList.push(data);
    } else {
      this.streamList[streami] = data;
    }
    this.updateStream();
  }

  /** Set Provider */
  changeProvider(e) {
    this.streamForm
      .get('videoUrl')
      .get('providerName')
      .setValue(e.target.value, {
        onlySelf: true,
      });
    const streamP: any = this.providerList.find(
      (item: any) => item.value === e.target.value
    );
    this.streamForm.get('type').setValue(streamP.key);
    this.streamForm.get('videoUrl').get('channelName').reset('');
    if (streamP?.key === 'LIVESTREAM') {
      this.fetchUniqueStreamKey();
    }
  }

  /** Update Tournament Stream */
  updateStream = async () => {
    try {
     const response= await this.tournamentService
        .updateTournament({ stream: this.streamList }, this.tournamentId)
        .toPromise();
        this.eSportsToastService.showSuccess(response.message)
      this.fetchTournamentDetails(true);
      this.formAction(false);
    } catch (error) {
      this.fetchTournamentDetails(true);
      this.eSportsToastService.showError(
        error?.error?.message || error?.statusText || error?.message
      );
    }
  };

  /** Remove Tournament Stream */
  removeStream = async (content, i) => {
    try {
      const response = await this.modalService.open(content, {
        windowClass: 'confirm-modal-content',
        centered: true,
      }).result;
      if (response) {
        this.streamList.splice(i, 1);
        this.updateStream();
      }
      } catch (error) {
      // this.eSportsToastService.showError('Unable to remove stream');
    }
  };

  /** Open Edit Form */
  editStream(i) {
    this.editIndex = i;
    this.isOpenForm = true;
    this.streamForm.patchValue({
      ...this.streamList[i],
    });
    this.providerList.push({
      key: this.streamList[i].type,
      value: this.streamList[i].videoUrl.providerName,
    });
  }

  /** Get Unique Stream Key */
  fetchUniqueStreamKey = async () => {
    try {
      const streamKey = await this.tournamentService.getStreamKey();
      this.streamForm
        .get('videoUrl')
        .get('channelName')
        .patchValue(streamKey.data, {
          onlySelf: true,
        });
      this.streamForm.patchValue({
        videoUrl: {
          channelName: streamKey.data,
        },
      });
    } catch (error) {
      this.eSportsToastService.showError(
        error?.error?.message || error?.message
      );
    }
  };

  /** Copy Text From Clipboard */
  copyToClipboard(text) {
    const dummyElement = this.document.createElement('textarea');
    this.document.body.appendChild(dummyElement);
    dummyElement.value = text;
    dummyElement.select();
    this.document.execCommand('copy');
    this.document.body.removeChild(dummyElement);
    this.eSportsToastService.showInfo('Copied');
  }

  pushGTMTags(eventName: string) {
    let superProperties: SuperProperties = {};
    if (this.currentUser) {
      superProperties = this.gtmService.assignLoggedInUsedData(
        this.currentUser
      );
    }
    this.gtmService.gtmEventWithSuperProp({
      eventName: eventName,
      superProps: superProperties,
    });
  }

  onIframeClick(){
    this.pushGTMTags('Watch_Tournament_Stream')
  }

}
