import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { EsportsLanguageService } from 'esports'

@Component({
  selector: 'app-drop-down',
  templateUrl: './drop-down.component.html',
  styleUrls: [
    './drop-down.component.scss',
    '../../../../modules/tournament/create/create.component.scss',
  ],
})
export class DropDownComponent implements OnInit {
  @Input() title: string;
  @Input() type: string;
  @Input() customFormControlName: string;
  @Input() customFormGroup: FormGroup;
  @Input() dropdownList;
  @Input() filterType;
  @Input() enableHiddenLabel: boolean;
  @Input() isMandatory: boolean = true;

  @Output() valueEmit = new EventEmitter<number>();

  currentLang = 'en';

  constructor(private languageService: EsportsLanguageService) {}

  ngOnInit(): void {
    this.languageService.language.subscribe(
      (lang) => (this.currentLang = lang)
    );
  }

  setValue(val) {
    this.customFormGroup.controls[this.customFormControlName].setValue(val);
    this.valueEmit.emit(val);
  }

  setValueAndDisplay(item) {
    this.customFormGroup.controls[this.customFormControlName].setValue(
      item.display
    );
    this.valueEmit.emit(item.value);
  }

  setValueInArray(val) {
    const array = this.customFormGroup.value[this.customFormControlName];
    const index = array.indexOf(val);
    if (index < 0) {
      array.push(val);
      this.customFormGroup.controls[this.customFormControlName].setValue(array);
    }
    this.valueEmit.emit(val);
  }
}
