import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CounterComponent } from './counter/counter.component';
import { DropDownComponent } from './drop-down/drop-down.component';
import { ToggleSwitchComponent } from './toggle-switch/toggle-switch.component';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { InputComponent } from './input/input.component';
import { PhoneNoComponent } from './phone-no/phone-no.component';
import { UploadComponent } from './upload/upload.component';
import { NgxIntlTelInputModule } from 'ngx-intl-tel-input';
import { DurationCounterComponent } from './duration-counter/duration-counter.component';
import { I18nModule, EsportsLoaderModule, PipeModule } from 'esports';
import { MatTooltipModule } from '@angular/material/tooltip';
import { environment } from '../../../../environments/environment';

const components = [
  CounterComponent,
  DropDownComponent,
  ToggleSwitchComponent,
  InputComponent,
  PhoneNoComponent,
  UploadComponent,
  DurationCounterComponent,
];

const modules = [
  CommonModule,
  FontAwesomeModule,
  FormsModule,
  ReactiveFormsModule,
  NgxIntlTelInputModule,
  NgbModule,
  PipeModule,
  EsportsLoaderModule.setColor('#501090'),
  I18nModule.forRoot(environment),
  MatTooltipModule,
];
@NgModule({
  declarations: [...components],
  imports: [...modules],
  exports: [...components],
})
export class FormComponentModule {}
