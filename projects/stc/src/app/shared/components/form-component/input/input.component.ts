import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss'],
})
export class InputComponent implements OnInit {
  @Input() customFormControlName: string;
  @Input() customFormGroup: FormGroup;
  @Input() enableSearch: boolean;
  @Input() patternType: string;
  @Input() readOnly: boolean;
  @Input() required: string;
  @Input() searchMessage: string;
  @Input() title: string;
  @Input() type: string;
  @Output() valueEmit = new EventEmitter<any>();

  constructor() {}

  ngOnInit(): void {}

  setValue(val) {
    this.customFormGroup.controls[this.customFormControlName].setValue(val);
    this.valueEmit.emit(val);
  }
}
