import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OnlyNumberDirective } from './only-number.directive';
import { NgbdSortableHeader } from './sort-table.directive';
import { IframeTrackerDirective } from './iframeTracker.directive';
// import { IframeTrackerDirective } from "esports"

@NgModule({
  declarations: [
    OnlyNumberDirective,
    NgbdSortableHeader,
    IframeTrackerDirective,
  ],
  imports: [CommonModule],
  exports: [OnlyNumberDirective, NgbdSortableHeader, IframeTrackerDirective],
})
export class DirectivesModule {}
