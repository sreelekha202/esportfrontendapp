import {
  Directive,
  Inject,
  PLATFORM_ID,
  TemplateRef,
  ViewContainerRef,
  OnInit,
} from '@angular/core';
import { isPlatformServer } from '@angular/common';

@Directive({
  selector: 'appShellRenderDirective',
})
export class appShellRenderDirective implements OnInit {
  constructor(
    @Inject(PLATFORM_ID) private platformId,
    private templateRef: TemplateRef<any>,
    private viewContainer: ViewContainerRef
  ) {}

  ngOnInit() {
    if (isPlatformServer(this.platformId)) {
      this.viewContainer.createEmbeddedView(this.templateRef);
    } else {
      this.viewContainer.clear();
    }
  }
}
