export interface IBracket {
  id?: string;
  name?: string;
  bracketType?: string;
  noOfRoundPerGroup?: number;
  noOfSet?: number;
  noOfStage?: number;
  noOfTeamInGroup?: number;
  noOfWinningTeamInGroup?: number;
  maximumParticipants?: number;
  createdBy?: string;
  updatedBy?: string;
}
