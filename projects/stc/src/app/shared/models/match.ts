import { IParticipant } from './participant';
import { ITournament } from './tournament';

interface IMatch {
  _id?: string;
  disQualifiedTeam?: object;
  teamA?: IParticipant;
  teamB?: IParticipant;
  bye?: boolean;
  matchStatus?: string;
  matchNo?: number;
  loserTeam?: IParticipant;
  winnerTeam?: IParticipant;
  placeHolderA?: string;
  placeHolderB?: string;
  sets?: IMatchSet[];
  totalSet?: number;
  teamAWinSet?: number;
  teamBWinSet?: number;
  isThirdPlaceMatch?: boolean;
  isNextMatchStart?: boolean;
  status?: number;
  tournamentId?: ITournament;
  createdOn?: Date;
  updatedOn?: Date;
  refereeId?: string;
  currentMatch?: object;
  loserNextMatch?: object;
  winnerNextMatch?: object;
  battleRoyalTeams?: BattleRoyaleTeam[];
  loserTeam2?: string;
  isScoreUpdatedByParticipant: boolean;
  scoreUpdatedByParticipantDt: Date;
  scoreUpdateByParticipant: string;
}

interface IMatchSet {
  id?: number;
  teamAScore?: number;
  teamBScore?: number;
  winner?: null;
  status?: string;
  teamAScreenShot?: string;
  teamBScreenShot?: string;
  enableResubmitBtn?: boolean;
}

interface BattleRoyaleTeam {
  _id?: string;
  id?: number;
  playerId?: string;
  gamerId?: string;
  noOfKill?: number;
  placement?: number;
  score?: number;
}

export { IMatch, IMatchSet };
