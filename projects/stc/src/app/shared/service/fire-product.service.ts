import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable, SubscribableOrPromise } from 'rxjs';
import { environment } from '../../../environments/environment';

import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class FireProductService {
  constructor(private http: HttpClient, private router: Router) {}

  getProducts(): Observable<any> {
    return this.http.get(environment.apiEndPoint + 'home/getFirestoreProducts');
  }
  getVideoStreams(): Observable<any> {
    return this.http.get(
      environment.apiEndPoint + 'home/getFirestoreVideoStreams'
    );
  }
}
