importScripts("https://www.gstatic.com/firebasejs/7.16.1/firebase-app.js");
importScripts(
  "https://www.gstatic.com/firebasejs/7.16.1/firebase-messaging.js"
);

firebase.initializeApp({
  apiKey: "AIzaSyB1mEgrCbtK1vvE4zR7VcOzXtcWs4ysvLw",
  authDomain: "dynasty-test.firebaseapp.com",
  databaseURL: "https://dynasty-test.firebaseio.com",
  projectId: "dynasty-test",
  storageBucket: "dynasty-test.appspot.com",
  messagingSenderId: "415594613035",
  appId: "1:415594613035:web:c0a1d274208530618add6e",
  measurementId: "G-YBTGYLZ7RN",
});

const messaging = firebase.messaging();

messaging.setBackgroundMessageHandler(function (payload) {
  const notificationTitle = payload.data.title;
  const notificationOptions = {
    body: payload.data.body,
  };
  return self.registration.showNotification(
    notificationTitle,
    notificationOptions
  );
});
