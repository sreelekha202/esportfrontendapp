import { AfterViewInit, Component, ElementRef, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { Direction } from '@angular/cdk/bidi';
import { Meta, Title } from '@angular/platform-browser';
import {
  SuperProperties,
  EsportsGtmService,
  EsportsChatService,
  EsportsPaginationService,
  EsportsConstantsService,
  EsportsScriptLoadingService,
  IUser,
  GlobalUtils,
} from 'esports';
import { filter, map, take } from 'rxjs/operators';
import { AppHtmlRoutes } from './app-routing.model';
import { AppRoutesData } from './app-routing.model';
import { environment } from '../environments/environment';
import { UserService } from './core/service';
import { UserPopupComponent } from './core/header/components/user-popup/user-popup.component';
import { MatDialog } from '@angular/material/dialog';

declare var gtag;
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit, AfterViewInit {
  AppHtmlRoutes = AppHtmlRoutes;
  isAdminPage: boolean = false;
  isRootPage: boolean = false;
  isUserLoggedIn: boolean = false;
  direction: Direction = 'ltr';
  currentUserId: String = '';
  currentUser: any;
  public scroll: ElementRef;
  isBrowser = GlobalUtils.isBrowser();
  constructor(
    private activatedRoute: ActivatedRoute,
    private meta: Meta,
    private paginationService: EsportsPaginationService,
    private titleService: Title,
    private userService: UserService,
    public router: Router,
    private chatService: EsportsChatService,
    private gtmService: EsportsGtmService,
    public matDialog: MatDialog,
    private esportsScriptLoadingService: EsportsScriptLoadingService,
  ) {
    if (GlobalUtils.isBrowser()) {
      this.checkAdmin();
      this.globalRouterEvents();
      window.addEventListener('storage', (event) => {
        if (event.newValue && event.key === environment.currentToken) {
          window.location.reload();
        }
        if (event.oldValue && event.key === environment.currentToken) {
          window.location.reload();
        }
      });
    }
  }

  ngOnInit(): void {
    if (GlobalUtils.isBrowser()) {
      this.paginationService.pageChanged.subscribe(
        (res) => {
          if (res) {
            this.scroll.nativeElement.scrollTop = 0;
          }
        },
        (err) => { }
      );

      this.userService.currentUser.subscribe((data) => {
        if (data) {
          this.currentUser = data;
          this.isUserLoggedIn = !!this.currentUser;
          this.currentUserId = this.currentUser._id;
          this.chatService.initialiseSocket();
          if (!this.currentUser.isAccountActivated) {
            this.matDialog.open(UserPopupComponent, {
              panelClass: 'user_popup_main',
              disableClose: true,
            });
          }
        } else {
        }
      });
    }
  }

  ngAfterViewInit(): void {
    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
        if (this.scroll?.nativeElement?.scrollTop) {
          this.scroll.nativeElement.scrollTop = 0;
        }
        return;
      }
      if (this.scroll?.nativeElement?.scrollTop) {
        this.scroll.nativeElement.scrollTop = 0;
      }
    });
  }

  onActivate(): void { }

  checkAdmin(): void {
    this.userService.currentUser
      .pipe(
        filter((data) => !!data),
        take(1)
      )
      .subscribe((data) => {
        if (data) {
          if (data.accountType === EsportsConstantsService.AccountType.Admin) {
            this.isAdminPage = true;
          }
        } else {
          this.isAdminPage = false;
        }
      });
  }

  private globalRouterEvents(): void {
    this.router.events
      .pipe(
        filter((event) => event instanceof NavigationEnd),
        map(() => this.activatedRoute.root.firstChild.snapshot.data)
      )
      .subscribe((data: AppRoutesData) => {
        this.isRootPage = data && data.isRootPage;
        const title = data && data.title;
        const tags = data && data.tags;
        if (title) {
          this.titleService.setTitle(title);
        }
        if (tags) {
          tags.forEach((tag) => {
            this.meta.updateTag(tag);
          });
        }
      });
  }

  mainMenuViewed() {
    this.pushGTMTags('View_Main_Menu');
    if (this.router.url.includes('home')) {
      this.pushGTMTags('View_Home_Tab');
    }
  }

  pushGTMTags(eventName: string) {
    let superProperties: SuperProperties = {};
    if (this.currentUser) {
      superProperties = this.gtmService.assignLoggedInUsedData(
        this.currentUser
      );
    }
    this.gtmService.gtmEventWithSuperProp({
      eventName: eventName,
      superProps: superProperties,
    });
  }
}
