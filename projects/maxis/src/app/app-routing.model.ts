import { MetaDefinition } from '@angular/platform-browser';

export interface AppRoutesData {
  isRootPage?: boolean;
  tags?: MetaDefinition[];
  title?: string;
}

export enum AppHtmlRoutes {
  home = '/home',
  play = '/play',
  shop = '/shop',
  videos = '/videos',
  userPageType = '/user',
  search = '/search',
  searchArticle = '/search/article',
  searchShop = '/search/shop',
  searchTournament = '/search/tournament',
  searchVideo = '/search/video',
  tournaments = '/tournaments',
  tournament = '/tournament',
  tournamentManage = '/tournament/manage',
  tournamentedit = '/tournament/edit/',
  tournamentView = '/tournament',
  manageTeam = '/manage-team/',
  article = '/updates',
  videosView = '/videos/view',
  login = '/login',
  faq = '/faq',
  post = '/content/more/all-post',
  news = '/updates/details/',
  newsdetail = '/details',
  viewteam = '/view-team',
  manageteam = '/manage-team/team-members',
  manageEdit = '/manage-team/edit-team',
  manageTournament = '/manage-tournament',
  underconstruction = '/underconstruction',
  contactUs = '/contact-us',
  termsConditions = '/terms-conditions',
  privacyPolicy = '/privacy-policy',
  termsOfUse = '/terms-of-use',
  transaction = 'account/transaction'
}

export enum AppHtmlProfileRoutes {
  accountSetting = '/profile/account-settings',
  bookmarks = '/profile/bookmarks',
  connections = '/404',
  content = '/profile/content',
  dashboard = '/profile/dashboard',
  inbox = '/profile/inbox',
  matches = '/profile/matches',
  myStats = '/profile/my-stats',
  myTeams = '/profile/my-teams',
  myTeamsView = '/profile/my-teams/',
  tournamentsCreated = '/profile/tournaments-created',
  tournamentsJoined = '/profile/tournaments-joined',
  transactions = '/profile/transactions',
  setting = '',
  newsCreate = '/updates/create',
  newsEdit = '/updates/edit/',
  createTeam = '/create-team'
}
