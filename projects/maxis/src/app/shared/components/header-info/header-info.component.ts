import { Component, OnInit, Input } from "@angular/core";
import { Router } from "@angular/router";

import { AppHtmlRoutes, AppHtmlProfileRoutes } from "../../../app-routing.model";
import { Location } from "@angular/common";
@Component({
  selector: "app-header-info",
  templateUrl: "./header-info.component.html",
  styleUrls: ["./header-info.component.scss"],
})
export class HeaderInfoComponent implements OnInit {
  @Input() title: string = "";

  AppHtmlRoutes = AppHtmlRoutes;
  AppHtmlProfileRoutes = AppHtmlProfileRoutes;

  constructor(private router: Router, private location: Location) {}

  ngOnInit(): void {}
  cancel() {
    this.location.back();
  }
}
