import { Component, OnInit } from '@angular/core';
import { toggleOpacity, VisibilityState } from '../../../animations';

@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.scss'],
  animations: [toggleOpacity],
})
export class CommentsComponent implements OnInit {
  VisibilityState = VisibilityState;
  collapseOrExpand: VisibilityState = VisibilityState.Visible;

  comments = [];

  constructor() {}

  ngOnInit(): void {
    
  }
}
