import { Component, Injectable, Input, OnInit } from "@angular/core";
import { EsportsChatService } from 'esports';

@Component({
  selector: "app-chatwindow",
  templateUrl: "./chatwindow.component.html",
  styleUrls: ["./chatwindow.component.scss"],
})
@Injectable({
  providedIn: "root",
})
export class ChatwindowComponent implements OnInit {
  @Input() matchdetails: any;
  showChat: boolean = false;
  windowposition: String = "chat_window chat_window_right";

  constructor(private chatService: EsportsChatService) {}

  ngOnInit(): void {}

  toggleChat() {
    if (this.matchdetails) {
      this.chatService
        .getMatchInformation(this.matchdetails._id)

        .subscribe((result: any) => {
          const mtchdetails = {
            _id: result[0]._id,
            picture: result[0].tournamentId.gameDetail.logo,
            name: result[0].tournamentId.name,
            matchno: result[0].matchNo,
            participants:
              result[0].teamA.inGamerUserId +
              "," +
              result[0].teamB.inGamerUserId,
            tournamentId: result[0].tournamentId._id,
            teamAId: result[0].teamA.userId,
            teamBId: result[0].teamB.userId,
            teamAGameId: result[0].teamA.inGamerUserId,
            teamBGameId: result[0].teamB.inGamerUserId,
            type: "tournament",
          };

          this.chatService.setWindowPos(this.windowposition);

          let firstChat = this.chatService.getChatStatus();

          if (firstChat == true) {
            this.chatService.setChatStatus(false);
            this.chatService?.disconnectMatch(mtchdetails._id, "tournament");
          } else {
            this.chatService.setCurrentMatch(mtchdetails);
            this.chatService.setTypeOfChat("tournament");
            this.chatService.setChatStatus(true);
          }
        });
    }
  }
}
