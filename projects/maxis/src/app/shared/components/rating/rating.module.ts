import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RatingComponent } from './rating.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [RatingComponent],
  imports: [CommonModule, NgbModule],
  exports: [RatingComponent],
})
export class RatingModule {}
