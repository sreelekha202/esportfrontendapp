import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
} from "@angular/core";

@Component({
  selector: "app-rating",
  templateUrl: "./rating.component.html",
  styleUrls: ["./rating.component.scss"],
})
export class RatingComponent implements OnInit, OnChanges {
  @Input() currentRate;
  @Input() fontSize;
  @Input() maxStar;
  @Input() readonly = false;
  @Output() rating = new EventEmitter<number>();

  constructor() {}

  ngOnInit(): void {}

  ngOnChanges() {}

  selectValue() {
    if (!this.readonly) {
      this.rating.emit(this.currentRate);
    }
  }
}
