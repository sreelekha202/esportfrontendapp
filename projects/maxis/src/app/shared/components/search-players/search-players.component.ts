import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { UserService } from '../../../core/service';

@Component({
  selector: 'app-search-players',
  templateUrl: './search-players.component.html',
  styleUrls: ['./search-players.component.scss'],
})
export class SearchPlayersComponent implements OnInit {
  @Input() heading: string = "";
  @Input() listTitle: string = "";
  @Output() list = new EventEmitter<any>();;

  players: any = [];
  text: string = '';
  constructor(private userService: UserService) { }

  ngOnInit(): void {
        
    this.text = "";
    this.getvalues('');
    setInterval(() => {
      this.senddata();
    }, 1000);
  }

  senddata() {
    let data1 = [];
    for (let data of this.players) {
      if (data.isSelected == true) {
        data1.push(data)
      }
    }
    this.list.emit(data1);
  }

  addPlayer(index: number): void {
    this.players.forEach((_, i) => {
      if (index === i) {
        this.players[i].isSelected = !this.players[i].isSelected;
      }
    });
  }
  filteredPlayer() {
    return this.players.filter((obj) => obj.nickname.includes(this.text.toLowerCase().trim()));
  }

  getvalues(e) {
    let pre_player = this.players
    this.players = []
    this.userService.searchUsers(e).subscribe(
      (res: any) => {
        this.players = res;
        for (let d of this.players) {
          d.isSelected = false;
        }
        for (let d2 of pre_player) {
          if (d2.isSelected == true) {
            let f = 1;
            for (let d3 of this.players) {
              if (d3.fullName == d2.fullName) {
                f = 2;
                d3.isSelected = true
              }
            }
            if (f == 1) {
              this.players.push(d2)
            }
          }
        }
      },
      (err) => { }
    );
  }

}
