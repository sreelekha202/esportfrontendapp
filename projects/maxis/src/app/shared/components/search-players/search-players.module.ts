import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { I18nModule } from 'esports';
import { LazyLoadImageModule } from 'ng-lazyload-image';
import { SearchPlayersComponent } from './search-players.component';
import { environment } from '../../../../environments/environment';

@NgModule({
  declarations: [SearchPlayersComponent],
  imports: [
    CommonModule,
    I18nModule.forRoot(environment),
    LazyLoadImageModule,
    FormsModule,
  ],
  exports: [SearchPlayersComponent],
})
export class SearchPlayersModule {}
