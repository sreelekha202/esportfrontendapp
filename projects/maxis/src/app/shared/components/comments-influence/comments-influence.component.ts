import { Component, OnInit } from '@angular/core';
import { IUser } from 'esports';
import { Subscription } from 'rxjs';
import { CommentService, UserService } from '../../../core/service';

@Component({
  selector: 'app-comments-influence',
  templateUrl: './comments-influence.component.html',
  styleUrls: ['./comments-influence.component.scss'],
})
export class CommentsInfluenceComponent implements OnInit {
  currentUser: IUser;
  userSubscription: Subscription;

  comments = [];
  tournamentDetails: any;

  constructor(private userService: UserService, private commentService: CommentService) { }

  ngOnInit(): void {

    this.tournamentDetails = localStorage.getItem("tournamentDetails")
    this.tournamentDetails = JSON.parse(this.tournamentDetails)
    this.comments = [];
  }
}
