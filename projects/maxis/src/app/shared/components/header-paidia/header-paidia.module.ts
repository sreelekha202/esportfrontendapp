import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderPaidiaComponent } from './header-paidia.component';
import { RouterModule } from '@angular/router';
import { MatIconModule } from '@angular/material/icon';
import { I18nModule } from 'esports';
import { environment } from '../../../../environments/environment';

@NgModule({
  declarations: [HeaderPaidiaComponent],
  imports: [CommonModule, RouterModule, MatIconModule,  I18nModule.forRoot(environment),],
  exports: [HeaderPaidiaComponent],
})
export class HeaderPaidiaModule {}
