import { Component, OnInit, Input } from '@angular/core';
import { AppHtmlRoutes } from '../../../app-routing.model';
@Component({
  selector: 'app-header-paidia',
  templateUrl: './header-paidia.component.html',
  styleUrls: ['./header-paidia.component.scss'],
})
export class HeaderPaidiaComponent implements OnInit {
  @Input() currentPageLink: string;
  @Input() title: string;
  AppHtmlRoutes = AppHtmlRoutes;
  constructor() {}

  ngOnInit(): void {}
}
