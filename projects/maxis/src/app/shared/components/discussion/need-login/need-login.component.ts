import { Component, OnInit } from '@angular/core';
import { GlobalUtils } from 'esports';
import { AppHtmlRoutes } from '../../../../app-routing.model';
import { environment } from '../../../../../environments/environment';

@Component({
  selector: 'app-need-login',
  templateUrl: './need-login.component.html',
  styleUrls: ['./need-login.component.scss'],
})
export class NeedLoginComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;

  constructor() {}

  ngOnInit(): void {}
  onLogin() {
    if (GlobalUtils.isBrowser()) {
      window.location.replace(`${environment.maxisLogin}`);
    }
  }
}
