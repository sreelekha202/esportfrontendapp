import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

interface Field {
  title: string;
  value: string;
}

@Component({
  selector: 'app-multiple-select',
  templateUrl: './multiple-select.component.html',
  styleUrls: ['./multiple-select.component.scss'],
})
export class MultipleSelectComponent implements OnInit {
  @Input() placehoder: string = 'Select Countries';
  @Input() menuButton: string = 'Add games';
  @Input() fields: any = [];
  @Output() valueEmit = new EventEmitter<any>();
  form: FormGroup;
  showLoader = false;
  isMenuOpened: boolean = false;

  constructor(public fb: FormBuilder) { }

  ngOnInit(): void {
    for (let d of this.fields) {
      d.checked = false;
    }
  }

  onGenerateForm() {
    const formGroups = {};
    for (let i = 0; i < this.fields.length; i++) {
      formGroups[this.fields[i].sortname.toLowerCase()] = false;
    }
    return formGroups;
  }

  onToggleMenu() {
    let senddata = []
    for (let d of this.fields) {
      if (d.checked == true) {
        senddata.push(d)
      }
    }
    this.valueEmit.emit(senddata)
    this.isMenuOpened = !this.isMenuOpened;
  }
}
