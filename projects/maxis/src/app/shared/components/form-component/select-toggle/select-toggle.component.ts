import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-select-toggle',
  templateUrl: './select-toggle.component.html',
  styleUrls: ['./select-toggle.component.scss'],
})
export class SelectToggleComponent implements OnInit {
  @Input() customFormGroup: FormGroup;
  @Input() customFormControlName: string;
  @Input() title: string;
  @Input() tooltip: string;
  @Input() leftValue: string;
  @Input() rightValue: string;
  @Input() value: number;
  @Output() valueEmit = new EventEmitter<string>();
 

  constructor() { }

  ngOnInit(): void {
  }

  handleChange(data) {
    this.valueEmit.emit(data.target.value)
  }
}
