import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { ChartsModule } from 'ng2-charts';
import { I18nModule } from 'esports';
import { InlineSVGModule } from 'ng-inline-svg';
import { LazyLoadImageModule } from 'ng-lazyload-image';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatIconModule } from '@angular/material/icon';

import { GameStatisticComponent } from './game-statistic.component';
import { environment } from '../../../../../environments/environment';

const modules = [
  ChartsModule,
  CommonModule,
  I18nModule.forRoot(environment),
  InlineSVGModule,
  LazyLoadImageModule,
  MatExpansionModule,
  MatIconModule,
];

@NgModule({
  declarations: [GameStatisticComponent],
  imports: modules,
  exports: [GameStatisticComponent],
})
export class GameStatisticModule {}
