import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-months-statistic',
  templateUrl: './months-statistic.component.html',
  styleUrls: ['./months-statistic.component.scss'],
})
export class MonthsStatisticComponent implements OnInit {
  selectedMonth: any = 'Select month';

  months = [
    'JANUARY',
    'FEBRUARY',
    'MARCH',
    'APRIL',
    'MAY',
    'JUNE',
    'JULY',
    'AUGUST',
    'SEPTEMBER',
    'OCTOBER',
    'NOVEMBER',
    'DECEMBER',
  ];
  threemonth: any;

  constructor() {
    this.threemonth = "THIS_MONTH";
    this.selectedMonth = this.months[new Date().getMonth()]
  }

  ngOnInit(): void {

  }
  selecteMonths(type) {
    this.threemonth = type;
  }

}
