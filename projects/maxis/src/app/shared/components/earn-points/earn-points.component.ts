import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-earn-points',
  templateUrl: './earn-points.component.html',
  styleUrls: ['./earn-points.component.scss'],
})
export class EarnPointsComponent implements OnInit {
  @Input() percents: number = 74;
  @Input() points: number = 0;
  @Input() title: string = '';

  constructor() {}

  ngOnInit(): void {}
}
