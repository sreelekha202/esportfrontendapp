import { Directive, HostListener } from '@angular/core';
import { Location } from '@angular/common';
import { GlobalUtils } from 'esports';
@Directive({
  selector: '[routerBack]',
})
export class RouterBackDirective {
  constructor(private location: Location) {}

  @HostListener('click')
  onClick() {
    if (GlobalUtils.isBrowser()) {
      this.location.back();
    }
  }
}
