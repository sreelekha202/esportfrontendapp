export interface IReward {
  _id: string;
  contentType: string;
  createdBy: string;
  createdOn: Date;
  description: string;
  expiredOn: Date;
  platform: string;
  reward_balance: string;
  reward_type: string;
  reward_value: string;
  type: string;
  updatedBy: string;
  updatedOn: Date;
  user: string;
}
