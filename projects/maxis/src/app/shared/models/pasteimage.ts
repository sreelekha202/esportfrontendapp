interface PasteImage {
  attachmenturl?: string;
  ftype?: string;
  imagedata?: string;
  inProgress?: boolean;
  matchid?: string;
  progress?: number;
  thumburl?: string;
}

export { PasteImage };
