export interface IBracket {
  bracketType?: string;
  createdBy?: string;
  id?: string;
  maximumParticipants?: number;
  name?: string;
  noOfRoundPerGroup?: number;
  noOfSet?: number;
  noOfStage?: number;
  noOfTeamInGroup?: number;
  noOfWinningTeamInGroup?: number;
  updatedBy?: string;
}
