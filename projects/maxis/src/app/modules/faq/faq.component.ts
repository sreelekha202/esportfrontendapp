import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.scss'],
})
export class FaqComponent implements OnInit {
  panelOpenState = false;
  selectedItem = 'Registration';
  list_item = [
    {
      title: 'Registration',
    },
    {
      title: 'Profile',
    },
    {
      title: 'Shop',
    },
    {
      title: 'Geng Coin',
    },
    {
      title: 'How to Redeem Vouchers',
    },
  ];
  public faqSelectedValue = this.list_item[0].title;
  constructor() {}

  ngOnInit(): void {}
  listClick(event, newValue) {
    this.selectedItem = newValue.title;
  }
  navigateTo(value) {
    this.selectedItem = value;
  }
}
