import { Component, OnInit } from '@angular/core';
import { AppHtmlRoutes } from '../../app-routing.model';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.scss']
})
export class ContactUsComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;
  constructor() { }

  ngOnInit(): void {
  }
}
