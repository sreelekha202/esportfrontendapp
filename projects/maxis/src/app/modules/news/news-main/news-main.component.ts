import { Component, OnInit } from '@angular/core';
import { ProfileService } from './../../../core/service/profile.service';
import { TranslateService } from '@ngx-translate/core';
import {
  EsportsArticleService,
  EsportsLanguageService,
  IUser,
  EsportsUtilsService,
  EventProperties,
  SuperProperties,
  EsportsGtmService,
} from 'esports';
import {
  InfoPopupComponent,
  InfoPopupComponentData,
  InfoPopupComponentType,
} from '../../../shared/popups/info-popup/info-popup.component';
import { MatDialog } from '@angular/material/dialog';
import { environment } from '../../../../environments/environment';
import { AppHtmlRoutes } from '../../../app-routing.model';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
const API = environment.apiEndPoint;

@Component({
  selector: 'app-news-main',
  templateUrl: './news-main.component.html',
  styleUrls: ['./news-main.component.scss'],
})
export class NewsMainComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;
  allTournaments: any = [];
  newsData: any = [];
  selectedItem: any = '';
  shop_slider = {
    slidesToShow: 1,
    slidesToScroll: 1,
    centerMode: true,
    dots: true,
    autoplay: true,
    autoplaySpeed: 3000,
    variableWidth: true,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true,
          dots: true,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  };
  showLoader: boolean = true;
  user: IUser;
  hottestArticle = [];
  latestArticle = [];
  currentUser: IUser;
  searchFilterForm = [
    {
      title: 'Select game',
      fields: [{ label: 'Select games', value: 'all' }],
    },
    {
      title: 'Article type',
      fields: [{ label: 'All', value: 'all' }],
    },
  ];

  browserFilterCategory = [
    {
      path: 'assets/icons/news/promos.svg',
      name: 'Shop promos',
    },
    {
      path: 'assets/icons/news/game-remote-g.svg',
      name: 'Games',
    },
    {
      path: 'assets/icons/news/star.svg',
      name: 'Top picks',
    },
  ];
  categoryList: any = [];
  categoryListt: any = [];
  trendingNews = [];
  categoryId: any;

  constructor(
    private articleService: EsportsArticleService,
    public matDialog: MatDialog,
    public language: EsportsLanguageService,
    public translateService: TranslateService,
    private profileService: ProfileService,
    public utilsService: EsportsUtilsService,
    public gtmService: EsportsGtmService,
    private router: Router,
    iconRegistry: MatIconRegistry,
    sanitizer: DomSanitizer
  ) {
    iconRegistry.addSvgIcon(
      'Games',
      sanitizer.bypassSecurityTrustResourceUrl(
        'assets/icons/news/icon-games.svg'
      )
    );
    iconRegistry.addSvgIcon(
      'esports',
      sanitizer.bypassSecurityTrustResourceUrl(
        'assets/icons/news/icon-esports.svg'
      )
    );
    iconRegistry.addSvgIcon(
      'Top picks',
      sanitizer.bypassSecurityTrustResourceUrl('assets/icons/news/star.svg')
    );
    iconRegistry.addSvgIcon(
      'Shop promos',
      sanitizer.bypassSecurityTrustResourceUrl(
        'assets/icons/news/icon-promos.svg'
      )
    );
  }

  ngOnInit(): void {
    let eventProperties: EventProperties = {};
    this.pushGTMTags('view_update_tab', eventProperties);
    this.getRecentPost();
    this.getAllCategory();
  }

  openBanner(slug, title) {
    let eventProperties: EventProperties = {};
    eventProperties['banner_title'] = title;
    eventProperties['bannerFileURL'] = slug;
    this.pushGTMTags('banner_click', eventProperties);
    this.router.navigate([AppHtmlRoutes.news, slug])
  }

  getArticleNews(categoryId) {
    const query = JSON.stringify({
      articleStatus: 'publish',
      category: categoryId,
    });
    const option = JSON.stringify({ limit: 7, sort: { createdDate: -1 } });
    this.articleService
      ?.getArticles_PublicAPI(API, { query, option })
      .subscribe(
        (res: any) => {
          this.trendingNews = res?.data;
        },
        (err) => {}
      );
  }
  getAllCategory() {
    this.profileService.getAllCategories().subscribe((res) => {
      if (res?.data && res?.data) {
        this.categoryListt = res?.data;
        this.categoryListt?.filter((cat: any) => {
          if (cat?.name == 'News') {
            this.categoryId = cat?._id;
          }
          this.categoryList?.push({ [cat._id]: cat });
        });

        this.getArticleNews(this.categoryId);
      }
    });
  }

  openSocialShareComponent() {
    const data: InfoPopupComponentData = {
      title: this.translateService.instant('ARTICLE_POST.SHARE'),
      text: ``,
      type: InfoPopupComponentType.socialSharing,
      cancelBtnText: 'Close',
    };

    this.matDialog.open(InfoPopupComponent, {
      data,
    });
  }

  browseByCategory(event, name) {
    this.categoryListt.map((obj) => {
      if (name.name == obj.name) this.getArticleNews(obj._id);
    });
    this.selectedItem = name.name;
    let eventProperties: EventProperties = {};
    eventProperties['tab_value'] = name.name;
    this.pushGTMTags('select_tab', eventProperties);
  }


  getRecentPost() {
    const pagination = JSON.stringify({
      limit: 100,
      sort: { createdDate: -1 },
    });
    const query = JSON.stringify({
      articleStatus: 'publish',
    });

    const perfernce = JSON.stringify({
      prefernce: this.user?.preference?.game
        ? this.user?.preference?.game.map((item) => {
            return { gameDetails: item };
          })
        : '',
    });

    this.articleService
      .getLatestArticle(API, {
        pagination: pagination,
        query: query,
        preference: perfernce,
      })
      .subscribe(
        (res: any) => {
          if (res && res.data && res.data.docs) {
            let latestArticle = res.data.docs;
            latestArticle.map((art: any) => {
              if (art.isFeature) {
                this.latestArticle.push({
                  ...art,
                  category_name: this.categoryList[art.category],
                });
              }
            });
          }
          this.showLoader = false;
        },
        (err) => {
          this.showLoader = false;
        }
      );
  }

  pushGTMTags(eventName: string, eventProperties: any) {
    let superProperties: SuperProperties = {};
    if (this.currentUser) {
      superProperties = this.gtmService.assignLoggedInUsedData(
        this.currentUser
      );
    }

    this.gtmService.gtmEventWithSuperProp({
      eventName: eventName,
      superProps: superProperties,
      eventProps: eventProperties,
    });
  }
}
