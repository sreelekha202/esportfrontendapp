import { Component, OnInit } from '@angular/core';
import { IPagination } from '../../../shared/models';
import { EsportsGameService, IUser } from 'esports';
import { environment } from './../../../../environments/environment';
import {
  InfoPopupComponent,
  InfoPopupComponentData,
  InfoPopupComponentType,
} from '../../../shared/popups/info-popup/info-popup.component';
import { ProfileService } from './../../../core/service/profile.service';
import { TranslateService } from '@ngx-translate/core';
import { MatDialog } from '@angular/material/dialog';
import { AppHtmlRoutes } from '../../../app-routing.model';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
const API = environment.apiEndPoint;
declare var $: any;
@Component({
  selector: 'app-news-all',
  templateUrl: './news-all.component.html',
  styleUrls: ['./news-all.component.scss'],
})
export class NewsAllComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;
  hasNextPage;
  gameArray = [];
  gameList = [];
  articles: any = [];
  user: IUser;
  page: IPagination;
  showLoader = false;
  dataLoaded = false;
  filterShow: boolean = false;
  selectedCategory: any = 'all';
  paginationData = {
    page: 1,
    limit: 8,
    sort: '-views',
  };
  sortOrderArray: any = [
    { label: 'Newest to Oldest', value: '-createdOn' },
    { label: 'Oldest to Newest', value: 'createdOn' },
  ];
  sortOrderType = this.sortOrderArray[0]['value'];

  categoryList: any = [];
  date: any;
  constructor(
    private esportsGameService: EsportsGameService,
    private profileService: ProfileService,
    public translateService: TranslateService,
    public matDialog: MatDialog,
    iconRegistry: MatIconRegistry,
    sanitizer: DomSanitizer
  ) {
    iconRegistry.addSvgIcon(
      'Games',
      sanitizer.bypassSecurityTrustResourceUrl(
        'assets/icons/news/icon-games.svg'
      )
    );
    iconRegistry.addSvgIcon(
      'esports',
      sanitizer.bypassSecurityTrustResourceUrl(
        'assets/icons/news/icon-esports.svg'
      )
    );
    iconRegistry.addSvgIcon(
      'Top picks',
      sanitizer.bypassSecurityTrustResourceUrl('assets/icons/news/star.svg')
    );
    iconRegistry.addSvgIcon(
      'Shop promos',
      sanitizer.bypassSecurityTrustResourceUrl(
        'assets/icons/news/icon-promos.svg'
      )
    );
  }

  ngOnInit(): void {
    this.getGames();
    this.getAllCategory();
    this.getRecentPost('');
  }

  getGames() {
    this.esportsGameService.getGames(API).subscribe((data: any) => {
      this.gameList = data.data;
    });
  }

  getAllCategory() {
    this.profileService.getAllCategories().subscribe((res) => {
      if (res && res?.data) {
        this.categoryList = [{ name: 'All Updates', _id: 'all' }, ...res?.data];
      }
    });
  }
  getRecentPost(id, clearPage = false) {
    if (id) {
      let isChecked = id?.target?.checked ? true : false;
      const gameid = { gameDetails: id?.target?.value };

      if (isChecked && id !== '') {
        this.gameArray.push(gameid);
      } else {
        for (let i = 0; i < this.gameArray.length; i++) {
          if (JSON.stringify(this.gameArray[i]) == JSON.stringify(gameid)) {
            this.gameArray.splice(i, 1);
            if (this.gameArray == []) {
              id = '';
            }
          }
        }
      }
    }
    let gameDetailsList = '';
    this.gameArray.map(({ gameDetails }, i) => {
      if (i == 0) {
        gameDetailsList = gameDetails;
      } else {
        gameDetailsList = gameDetailsList + '||' + gameDetails;
      }
    });
    console.log(this.gameArray);

    if (clearPage) {
      this.paginationData.page = 1;
      this.paginationData.limit = 8;
    }

    this.showLoader = true;
    let param: any = {
      articleStatus: 'publish',
      page: this.paginationData.page,
      limit: this.paginationData.limit,
      sort: this.sortOrderType,
      gameDetails: gameDetailsList,
    };
    this.selectedCategory != 'all'
      ? (param.category = this.selectedCategory)
      : '';
    param.category ? param.category : delete param.category;
    param.gameDetails ? param.gameDetails : delete param.gameDetails;
    this.profileService.getLatestArticle(API, param).subscribe(
      (res: any) => {
        if (res && res.data && res.data.docs) {
          if (this.hasNextPage && !clearPage) {
            this.articles = [...this.articles, ...res['data']['docs']];
          } else {
            this.articles = res['data']['docs'];
          }
        }
        this.showLoader = false;
        this.page = {
          totalItems: res?.data?.totalDocs,
          itemsPerPage: res?.data?.limit,
          maxSize: 5,
        };
        this.hasNextPage = res.data.hasNextPage ? true : false;
      },
      (err) => {
        this.showLoader = false;
      }
    );
  }

  openSocialShareComponent() {
    const data: InfoPopupComponentData = {
      title: this.translateService.instant('ARTICLE_POST.SHARE'),
      text: ``,
      type: InfoPopupComponentType.socialSharing,
      cancelBtnText: 'Close',
    };

    this.matDialog.open(InfoPopupComponent, {
      data,
    });
  }

  LoadMoreData() {
    this.paginationData.page++;
    this.getRecentPost('');
  }
  filterShowM() {
    this.filterShow = true;
  }
  filterCloseM() {
    this.filterShow = false;
  }
  resetFilter() {
    $('input[type=checkbox]').each((index, checkbox) => {
      checkbox.checked = false;
    });
    this.gameArray = [];
    this.sortOrderType = this.sortOrderArray[0]['value'];
    this.selectedCategory = 'all';
    this.getRecentPost('', true);
  }
}
