import { ActivatedRoute, Router } from '@angular/router';
import { EsportsToastService, EsportsArticleService } from 'esports';
import { ProfileService } from '../../../core/service/profile.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { environment } from '../../../../environments/environment';
import { MatDialog } from '@angular/material/dialog';
import { SubmitForApprovalComponent } from './components/submit-for-approval/submit-for-approval.component';
import { UserService } from '../../../core/service';
const API = environment.apiEndPoint;

@Component({
  selector: 'app-edit-content',
  templateUrl: './edit-content.component.html',
  styleUrls: ['./edit-content.component.scss'],
})
export class EditContentComponent implements OnInit {
  imgurl = '';
  form: FormGroup;
  addForm: FormGroup;
  stateList: any = [];
  categoryList: any = [];
  gameList: any = [];
  tagList: any = [];
  platformList: any = [];
  genreList: any = [];
  articleStatus = ['draft', 'submitted_for_approval'];
  minReadList: any = [
    { _id: 2, name: '2 Minutes' },
    { _id: 3, name: '3 Minutes' },
    { _id: 4, name: '4 Minutes' },
    { _id: 5, name: '5 Minutes' },
    { _id: 6, name: '6 Minutes' },
    { _id: 7, name: '7 Minutes' },
  ];
  showLoader: boolean;
  currentUser: any;
  loader: any;
  editorConfig = {};
  articleId: any;
  editObject: any;
  articleedit: any = [
    {
      id: 1,
      name: 'Edit article',
      url: ''
    },
    {
      id: 2,
      name: 'View article',
      url: ''
    }
  ]
  isSelectedPage = 1;
  constructor(
    private fb: FormBuilder,
    private articleService: EsportsArticleService,
    public activatedRoute: ActivatedRoute,
    private profileService: ProfileService,
    private userService: UserService,
    private toastService: EsportsToastService,
    private router: Router,
    public dialog: MatDialog
  ) {
    this.addForm = this.fb.group({
      title: [null, Validators.compose([Validators.required, Validators.maxLength(70)])],
      category: [null, Validators.compose([Validators.required])],
      gameDetails: [null, Validators.compose([Validators.required])],
      minRead: [null],
      tags: [null],
      platform: [null, Validators.compose([Validators.required])],
      genre: [null],
      shortDescription: [
        null,
        Validators.compose([Validators.required, Validators.maxLength(150)]),
      ],
      content: [''],
      image: [null, Validators.compose([Validators.required])],
      author: [null],
      game: [null],
      articleStatus: ['']
    });
    this.articleId = this.activatedRoute.snapshot.params['id'];
  }

  ngOnInit(): void {
    this.editorConfig = {
      editable: true,
      spellcheck: true,
      height: '180px',
      width: 'auto',
      minWidth: '0',
      translate: 'yes',
      enableToolbar: true,
      showToolbar: true,
      defaultParagraphSeparator: '',
      defaultFontName: '',
      defaultFontSize: '',
      fonts: [
        { class: 'arial', name: 'Arial' },
        { class: 'times-new-roman', name: 'Times New Roman' },
        { class: 'calibri', name: 'Calibri' },
        { class: 'comic-sans-ms', name: 'Comic Sans MS' },
      ],

      customClasses: [
        {
          name: 'quote',
          class: 'quote',
        },
        {
          name: 'redText',
          class: 'redText',
        },
        {
          name: 'titleText',
          class: 'titleText',
          tag: 'h1',
        },
      ],
      sanitize: true,
      toolbarPosition: 'top',
      toolbarHiddenButtons: [['bold', 'italic'], ['fontSize']],
    };
    this.userService.currentUser.subscribe((userdata) => {
      if (userdata) {
        this.currentUser = userdata;
        this.addForm.patchValue({ author: this.currentUser._id });
      }
    });
    this.getAllCategories();
    this.getAllGenres();
    this.getAllTags();
    this.getAllGame();

    this.getArticle(this.articleId);
  }
  getArticle(id) {
    this.articleService.getArticleByID(API, id).subscribe(
      (res) => {
        this.editObject = res['data'][0];
        this.addForm.patchValue({
          title: this.editObject.title ? this.editObject.title['english'] : '',
          category: this.editObject.category,
          gameDetails: this.editObject.gameDetails._id,
          minRead: this.editObject.minRead,
          tags: this.editObject.tags,
          platform: [],
          genre: this.editObject.genre,
          shortDescription: this.editObject.shortDescription
            ? this.editObject.shortDescription['english']
            : '',
          content: this.editObject.content
            ? this.editObject.content['english']
            : '',
          image: this.editObject.image,
          author: this.editObject.author,
          game: this.editObject.game,
          articleStatus: this.editObject.articleStatus
        });
      },
      (err) => { }
    );
  }
  getAllCategories() {
    this.profileService.getAllCategories().subscribe((res) => {
      if (res) this.categoryList = res.data;
    });
  }
  getAllTags() {
    this.profileService.getAllTags().subscribe((res) => {
      if (res) this.tagList = res.data;
    });
  }
  getAllGenres() {
    this.profileService.getAllGenres().subscribe((res) => {
      if (res) this.genreList = res.data;
    });
  }
  getAllGame() {
    this.profileService.getAllGame().subscribe((res) => {
      if (res) { this.gameList = res.data; this.onChangeGame(true) }
    });
  }
  onChangeGame(isEditMode = false) {
    this.gameList.map((res) => {
      if (this.addForm.value.gameDetails == res._id) {
        this.platformList = res.platform;
        isEditMode ? this.addForm.patchValue({ platform: this.editObject.platform }) : '';
      }
    });
  }
  onSubmit(type = null) {
    if (this.addForm.value.gameDetails) {
      this.gameList.map((g) => {
        if (g._id == this.addForm.value.gameDetails) {
          this.addForm.patchValue({ game: g.name });
        }
      });
    }
    if (type == this.articleStatus[0]) {
      this.addForm.patchValue({ articleStatus: this.articleStatus[0] });
    }
    if (type == this.articleStatus[1]) {
      this.addForm.patchValue({ articleStatus: this.articleStatus[1] });
    }
    if (this.addForm.valid) {
      this.showLoader = true;
      let temp = {
        shortDescription: { english: this.addForm.value.shortDescription },
        title: { english: this.addForm.value.title },
        content: { english: this.addForm.value.content },
      };
      this.profileService
        .updateContent({ ...this.addForm.value, ...temp }, this.articleId)
        .subscribe(
          (res) => {
            this.showLoader = false;
            this.toastService.showSuccess(res.message);
            if (type == this.articleStatus[0]) {
              this.addForm.patchValue({ articleStatus: this.articleStatus[0] });
            }
            if (type == this.articleStatus[1]) {
              this.dialog.open(SubmitForApprovalComponent, {
                panelClass: 'custom_dialog',
                disableClose: true,
              });
              this.addForm.patchValue({ articleStatus: this.articleStatus[1] });
            }
            this.router.navigateByUrl('/profile/dashboard');
          },
          (err) => {
            this.toastService.showError(err.error.message);
            this.showLoader = false;
          }
        );
    }
  }
  onPageChange() {
    this.isSelectedPage == 2 ? this.router.navigateByUrl('/updates/details/' + this.editObject?.slug) : '';
  }
}
