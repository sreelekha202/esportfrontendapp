import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterBackModule } from '../../../shared/directives/router-back.module';
import { SharedModule } from '../../../shared/modules/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { HeaderPaidiaModule } from '../../../shared/components/header-paidia/header-paidia.module';
import { MaterialModule } from '../../../shared/modules/material.module';
import { MatExpansionModule } from '@angular/material/expansion';
import { CoreModule } from '../../../core/core.module';
import { TeamIconComponent } from './components/team-icon/team-icon.component';
import { EditContentComponent } from './edit-content.component';
import { SaveAsDraftComponent } from './components/save-as-draft/save-as-draft.component';
import { SubmitForApprovalComponent } from './components/submit-for-approval/submit-for-approval.component';
import { WYSIWYGEditorModule } from 'esports';
import { environment } from '../../../../environments/environment';

export const routes: Routes = [
  {
    path: '',
    component: EditContentComponent,
  },
];

@NgModule({
  declarations: [
    EditContentComponent,
    SaveAsDraftComponent,
    SubmitForApprovalComponent,
    TeamIconComponent,
  ],
  imports: [
    CommonModule,
    CoreModule,
    RouterBackModule,
    MaterialModule,
    MatExpansionModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    HeaderPaidiaModule,
    WYSIWYGEditorModule.forRoot(environment),
  ],
})
export class EditContentModule {}
