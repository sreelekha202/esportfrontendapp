import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NewsMainComponent } from './news-main/news-main.component';
import { RouterModule, Routes } from '@angular/router';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { SharedModule } from '../../shared/modules/shared.module';
import { CoreModule } from '../../core/core.module';
import { MaterialModule } from '../../shared/modules/material.module';
import { MatExpansionModule } from '@angular/material/expansion';
import { RouterBackModule } from '../../shared/directives/router-back.module';
import { S3UploadService } from '../../core/service/s3Upload.service';
import { NewsDetailsComponent } from './news-details/news-details.component';
import { NewsAllComponent } from './news-all/news-all.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

export const routes: Routes = [
  {
    path: '',
    component: NewsMainComponent
  },
  {
    path: 'details/:id',
    data: { isRootPage: true },
    component: NewsDetailsComponent
  },
  {
    path: 'all',
    component: NewsAllComponent
  },
]

@NgModule({
  declarations: [
    NewsMainComponent,
    NewsDetailsComponent,
    NewsAllComponent,
  ],
  imports: [
    CommonModule,
    CoreModule,
    MaterialModule,
    MatExpansionModule,
    RouterBackModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    SlickCarouselModule
  ],
  providers: [
    S3UploadService
  ]
})
export class NewsModule { }
