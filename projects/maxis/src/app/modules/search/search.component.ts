import {
  ChangeDetectionStrategy,
  Component,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { filter } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { AppHtmlRoutes } from '../../app-routing.model';
import { HomeService } from '../../core/service';
import {
  IPagination,
  EsportsGtmService,
  EventProperties,
  SuperProperties,
  IUser,
} from 'esports';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';

export enum OngoingTournamentFilter {
  upcoming,
  ongoing,
  completed,
  all,
}
@AutoUnsubscribe()
@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SearchComponent implements OnInit, OnDestroy {
  text: string = '';
  isAllPage: boolean;
  activeRoute: string;
  AppHtmlRoutes = AppHtmlRoutes;
  OngoingTournamentFilter = OngoingTournamentFilter;
  tournamentFilter: OngoingTournamentFilter = OngoingTournamentFilter.all;
  page: IPagination;
  paginationData = {
    page: 1,
    limit: 8,
    sort: '-startDate',
  };
  currentUser: IUser;
  private sub: Subscription;
  private sub1: Subscription;

  constructor(
    public translate: TranslateService,
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public homeService: HomeService,
    private gtmService: EsportsGtmService,
  ) {
    this.initSubscription();
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
    this.sub1.unsubscribe();
  }

  onSearchCategory(path: AppHtmlRoutes): void {
    this.activeRoute = path;
    this.tournamentFilter = OngoingTournamentFilter.all;
    this.handleTournamentStatusChange();
    switch (path) {
      case AppHtmlRoutes.search:
        this.pushGTMTags('select_tab', 'All');
        break;
      case AppHtmlRoutes.searchShop:
        this.pushGTMTags('select_tab', 'Shop Items');
        break;
      case AppHtmlRoutes.searchArticle:
        this.pushGTMTags('select_tab', 'Articles');
        break;
      default:
        this.pushGTMTags('select_tab', 'All');
        break;
    }
  }

  /**
   * Function will update filter value to search service.
   */
  handleTournamentStatusChange() {
    this.homeService.updateTournamentStatusFilter(this.tournamentFilter);
  }

  private initSubscription(): void {
    this.sub = this.activatedRoute.queryParamMap.subscribe((e) => {
      if (e.get('text')) {
        this.text = e.get('text');
        this.homeService.updateSearchParams(this.text, this.paginationData);
        this.homeService.searchArticle();
        this.homeService.searchShop();
      } else {
        this.homeService.updateSearchParams('', this.paginationData);
        this.homeService.searchArticle();
        this.homeService.searchShop();
      }
    });

    this.sub1 = this.router.events
      .pipe(filter((event) => event instanceof NavigationEnd))
      .subscribe((event: NavigationEnd) => {
        const shouldContainTwoOrMoreSlashes = new RegExp('(?:.*?/){2}');
        this.isAllPage = !Boolean(
          shouldContainTwoOrMoreSlashes.test(event.url)
        );
      });
  }

  pushGTMTags(eventName: string, props: any) {
    let eventProperties: EventProperties = {};
    let superProperties: SuperProperties = {};
    if (this.currentUser) {
      superProperties = this.gtmService.assignLoggedInUsedData(
        this.currentUser
      );
    }
    eventProperties['tabValue'] = props;
    this.gtmService.gtmEventWithSuperProp({
      eventName: eventName,
      superProps: superProperties,
      eventProps: eventProperties,
    });
  }

  onTextChange(text) {
    this.text = text;
  }
}
