import { NgModule } from '@angular/core';
import { CoreModule } from '../../core/core.module';
import { SearchRoutingModule } from './search-routing.module';
import { SearchAllComponent } from './search-all/search-all.component';
import { SearchArticleComponent } from './search-article/search-article.component';
import { SearchComponent } from './search.component';
import { SearchTournamentsComponent } from './search-tournaments/search-tournaments.component';
import { SearchVideoComponent } from './search-video/search-video.component';
import { SharedModule } from '../../shared/modules/shared.module';
import { SearchShopComponent } from './search-shop/search-shop.component';
import { ShopCardComponent } from './search-shop/components/shop-card/shop-card.component';

@NgModule({
  declarations: [
    SearchAllComponent,
    SearchArticleComponent,
    SearchComponent,
    SearchTournamentsComponent,
    SearchShopComponent,
    SearchVideoComponent,
    ShopCardComponent
  ],
  imports: [CoreModule, SharedModule, SearchRoutingModule],
})
export class SearchModule { }
