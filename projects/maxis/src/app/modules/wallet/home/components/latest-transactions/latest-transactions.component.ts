import { Component, Input, OnInit } from '@angular/core';
@Component({
  selector: 'app-latest-transactions',
  templateUrl: './latest-transactions.component.html',
  styleUrls: ['./latest-transactions.component.scss'],
})
export class latestTransactionsComponent implements OnInit {
  sortflag = false;
  @Input() latesttransactions;
  constructor() {}
  ngOnInit(): void {}

  sort() {
    if (this.sortflag == false) {
      this.latesttransactions.sort(function (a, b) {
        var dateA = new Date(a.createdAt).getTime(),
          dateB = new Date(b.createdAt).getTime();
        return dateA - dateB; //sort by date ascending
      });
      this.sortflag = true;
    } else {
      this.latesttransactions.sort(function (a, b) {
        var dateA = new Date(a.createdAt).getTime(),
          dateB = new Date(b.createdAt).getTime();
        return dateB - dateA; //sort by date ascending
      });
      this.sortflag = false;
    }
  }
}
