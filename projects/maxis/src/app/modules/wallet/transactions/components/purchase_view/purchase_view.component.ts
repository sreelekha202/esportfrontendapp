import { Component, Input, OnInit } from '@angular/core';
import { EsportsToastService, GlobalUtils, IUser } from 'esports';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';
import { Subscription } from 'rxjs';
import { UserService } from '../../../../../core/service';
import { printCSS } from '../printCSS';

@AutoUnsubscribe()
@Component({
  selector: 'app-purchase-view',
  templateUrl: './purchase_view.component.html',
  styleUrls: ['./purchase_view.component.scss'],
})
export class ViewPurchaseComponent implements OnInit {
  sortflag = false;
  isPrint: boolean = false;
  @Input() showdata;
  currenUser: IUser;
  userSubscription: Subscription;
  constructor(
    private toast: EsportsToastService,
    public userService: UserService
  ) {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.currenUser = data;
      }
    });
  }
  ngOnInit(): void {}

  copyInputMessage(inputElement) {
    if (GlobalUtils.isBrowser()) {
      inputElement.select();
      document.execCommand('copy');
      inputElement.setSelectionRange(0, 0);
      this.toast.showSuccess('Copied!');
    }
  }
  myPrint() {
    if (GlobalUtils.isBrowser()) {
      this.isPrint = true;
      setTimeout(() => {
        var divContents = document.getElementById('myElementId').innerHTML;
        var printWindow = window.open('', '', 'height=800,width=800');
        printWindow.document.write('<html><head><title>Print</title>');
        printWindow.document.write('</head><body >');
        printWindow.document.write(`${printCSS}`);
        printWindow.document.write(divContents);
        printWindow.document.write('</body></html>');
        printWindow.document.close();
        printWindow.print();
      }, 200);
      this.isPrint = false;
    }
  }
  ngOnDestroy(): void {}
}
