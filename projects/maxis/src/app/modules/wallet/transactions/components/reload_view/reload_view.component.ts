import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { GlobalUtils, IUser } from 'esports';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';
import { Subscription } from 'rxjs';
import { UserService } from '../../../../../core/service/user.service';
import { printCSS } from '../printCSS';

@AutoUnsubscribe()
@Component({
  selector: 'app-reload-view',
  templateUrl: './reload_view.component.html',
  styleUrls: ['./reload_view.component.scss'],
})
export class ViewReloadComponent implements OnInit, OnDestroy {
  sortflag = false;
  isPrint: boolean = false;
  @Input() showdata: any;
  tableflag: boolean;
  currenUser: IUser;
  userSubscription: Subscription;
  constructor(public userService: UserService) {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.currenUser = data;
      }
    });
  }
  ngOnInit(): void {}

  myPrint() {
    if (GlobalUtils.isBrowser()) {
      this.isPrint = true;
      setTimeout(() => {
        var divContents = document.getElementById('myElementId').innerHTML;
        var printWindow = window.open('', '', 'height=800,width=800');
        printWindow.document.write('<html><head><title>Print</title>');
        printWindow.document.write('</head><body >');
        printWindow.document.write(`${printCSS}`);
        printWindow.document.write(divContents);
        printWindow.document.write('</body></html>');
        printWindow.document.close();
        printWindow.print();
      }, 200);
      this.isPrint = false;
    }
  }
  ngOnDestroy(): void {}
}
