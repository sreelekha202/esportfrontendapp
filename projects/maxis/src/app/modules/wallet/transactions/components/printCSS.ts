export const printCSS = `
<style>
    .print_view .heading {
      height: 50px;
      width: 100%;
      display: flex;
      align-items: center;
      justify-content: center;
      background-color: #171a17;
      border: 1px solid;
      padding: 10px 0;
    }
    .print_view .main_content{
      padding:0 30px;
    }
    .print_view .thanks_msg {
      margin-top: 20px;
      padding-bottom: 20px;
      border-bottom: 1px solid rgba(0, 0, 0, 0.75);
      text-align: center;
    }
    .print_view .thanks_msg h3 {
      font-size: 25px;
      letter-spacing: 0;
      color: #fff;
      color:#000;
      font-family: sans-serif;
      font-weight: 900;
    }
    .print_view .thanks_msg p {
      font-size: 15px;
      text-align: center;
      letter-spacing: 0.1px;
      color: #000;
      margin-top: 15px;
      font-family: sans-serif;
      line-height: 25px;
    }
    .print_view .msg_content .geng_coins {
      margin: 35px 0;
      display: flex;
      align-items: center;
    }
    .print_view .msg_content .geng_coins img{
      height: 95px;
      margin-right: 20px;
    }

    .print_view .msg_content .geng_coins p {
      font-size: 25px;
      letter-spacing: 0.25px;
      color: #000;
      font-family: sans-serif;
      font-weight: 700;
    }
    .print_view .msg_content .purchase_details,
    .print_view .msg_content .item_description {
      margin-top: 20px;
      border-bottom: 1px solid rgba(0, 0, 0, 0.75);
      padding-bottom: 20px;
    }
    .print_view .msg_content .item_description table tr td:last-child{
      text-align:right;
    }
    table{
      width:100%;
    }
    table tr td{
      padding-bottom:10px;
    }
    table tr td:first-child{
      min-width: 40px;
      max-width: 40px;
      font-family: sans-serif;
    }
    table tr td:last-child{
      font-family: sans-serif;
      font-weight: 700;
    }
    .item_description table tr td:last-child{
      font-weight:700;
    }
    .item_description table tr:last-child td{
      font-weight:900;
    }
    .print_view .msg_content .purchase_details h6,
    .print_view .msg_content .item_description h6 {
      font-family: sans-serif;
      font-weight: 700;
      font-size: 20px;
      letter-spacing: 0.15px;
      color: #000;
      margin: 0 0 20px 0;
    }
    .print_view .msg_content .purchase_details table tr td,
    .print_view .msg_content .item_description table tr td{
      font-size: 15px;
      letter-spacing: 0.18px;
      color: #000;
    }
    .conversation_rate p{
      font-family:sans-serif;
    }
    .game_details h4{
      margin-bottom: 8px;
          font-family: sans-serif;
          font-size: 25px;
          margin-top: -10px;
          font-weight: 900;
    }
    .game_details p{
      margin-top: 0;
      font-size: 15px !important;
      font-family: sans-serif;
      margin-bottom: 12px;
    }
    .game_details span{
      display: inline-block;
      background-color: #8DE4FF;
      font-family: sans-serif;
      font-size: 10px;
      font-weight: 600;
      color: #000;
      padding: 5px 8px;
    }
    .special_thanks{
      position: fixed;
      bottom: 0;
    }
    .special_thanks p{
      margin: 0;
      margin-bottom: 5px;
      font-size: 15px;
      font-family: sans-serif;
      font-weight: 600;
    }
    .text_center{
      text-align:center;
      font-family: sans-serif;
    }
    .voucher_code{
      font-family: sans-serif;
      border: 2px solid rgba(0, 0, 0, 0.75);
      box-sizing: border-box;
    }
</style>
`;
