import { AuthServices } from './../../../core/service/auth.service';
import { Component, Inject, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { UserService } from '../../../core/service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { SettingsLightboxComponent } from '../settings-lightbox.component';
import { environment } from '../../../../environments/environment';
import { EsportsGtmService, EsportsToastService, EventProperties, SuperProperties } from 'esports';
import { DOCUMENT } from '@angular/common';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';
const API = environment.apiEndPoint;
const TOKEN = environment.currentToken;
@AutoUnsubscribe()
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
})
export class ProfileComponent implements OnInit {
  url: any = '/assets/images/demo_user.svg';
  editprofile = false;
  box = false;
  normal = true;
  showLoader: boolean;
  currentUser: any;
  userSubscription: Subscription;
  addForm: FormGroup;
  editImageUrl: any = null;
  selectedFile: any;
  imagesList = [];
  isdisabled = true;
  isSetData = false;
  pronouns = [
    { title: 'She/her', value: 'she' },
    { title: 'He/His', value: 'he' },
    { title: 'They/them', value: 'they' },
    { title: 'Custom', value: 'Custom' },
  ];

  stateList: any = [];
  countryList: any = [];
  cityList: any = [];
  base64textString: any;
  isExist: any;
  selectedValue: any;
  malaysia: number = 132;
  stateCode: number;
  cityCode: number;
  tempImg: any;
  isPhoneNumber: boolean;
  constructor(
    @Inject(DOCUMENT) document,
    private maxisUserService: UserService,
    private authServices: AuthServices,
    private fb: FormBuilder,
    private toastService: EsportsToastService,
    private gtmService: EsportsGtmService,
    public dialogRef: MatDialogRef<SettingsLightboxComponent>
  ) {
    this.addForm = this.fb.group({
      username: [
        null,
        Validators.compose([
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(25),
          Validators.pattern(/^[a-zA-Z0-9_]+$/),
        ]),
      ],
      email: [null, Validators.compose([])],
      country: [this.malaysia, Validators.compose([Validators.required])],
      state: [null, Validators.compose([Validators.required])],
      city: [null],
      shortBio: [null, Validators.compose([Validators.maxLength(200)])],
      profilePicture: [''],
      phoneNumber: [null],
    });
  }

  ngOnInit(): void {
    this.getAvatars();
    this.userSubscription = this.maxisUserService.currentUser.subscribe(
      (data) => {
        if (data) {
          this.currentUser = data;
          this.setUserData();
          if (data.profilePicture) this.url = data.profilePicture;
        }
      }
    );
    this.maxisUserService.getAllCountries().subscribe((res) => {
      this.countryList = res.countries;
      this.addForm.patchValue({
        country: Number(this.malaysia),
      });
    });
  }

  ngAfterViewInit() {
    if (!this.addForm.valid) document.getElementById('hjhgjg').click();
  }
  getAvatars() {
    this.maxisUserService.getAvatars().subscribe((res) => {
      this.imagesList = [];
      let tempList = res.data[0].avatarUrls;
      tempList.map((img) => {
        this.imagesList.push({ ...img, isSelected: false });
      });
    });
  }

  setUserData() {
    let countryCode: number;
    this.isSetData = true;
    if (this.currentUser.country == 'Malaysia') countryCode = this.malaysia;
    else countryCode = this.malaysia;
    this.getCountryWiseState(countryCode);
    this.editImageUrl = this.currentUser.profilePicture;
    this.addForm.patchValue({
      profilePicture: this.currentUser.profilePicture,
      username: this.currentUser.username,
      country: countryCode,
      shortBio: this.currentUser.shortBio,
      email: this.currentUser.email,
    });
    if (!this.validateEmail(this.currentUser.phoneNumber)) {
      this.isPhoneNumber = true;
      this.addForm.patchValue({
        phoneNumber: this.currentUser.phoneNumber,
      });
    }
  }

  getCountryWiseState(country_id: any) {
    this.stateList = [];
    let allState: any = [];
    this.maxisUserService.getStates().subscribe((res) => {
      allState = res.states;
      allState.filter((obj) => {
        if (obj['country_id'] == country_id) {
          this.stateList.push(obj);
        }
      });
      if (this.isSetData) {
        for (const sl of this.stateList) {
          if (this.currentUser.state == sl.name) {
            this.stateCode = sl.id;
            this.addForm.patchValue({
              state: this.stateCode,
            });
            this.isSetData = true;
            this.getStateWiseCities(this.stateCode);
            break;
          }
        }
      }
    });
  }

  getStateWiseCities(state_id: any) {
    this.cityList = [];
    let allCity: any = [];
    this.maxisUserService.getCity().subscribe((res) => {
      allCity = res.cities;
      allCity.filter((obj) => {
        if (obj['state_id'] == state_id) {
          this.cityList.push(obj);
        }
      });
      if (this.isSetData) {
        for (const cl of this.cityList) {
          if (this.currentUser.city == cl.name) {
            this.cityCode = cl.id;
            this.addForm.patchValue({
              city: this.cityCode,
            });
            this.isSetData = false;
            break;
          }
        }
      }
    });
  }

  onStateChange = (data: any) => {
    this.getStateWiseCities(data.value);
  };

  onSubmit() {
    if (this.addForm.valid) {
      this.showLoader = true;
      let data = this.addForm.value;
      if (data.country == this.malaysia) {
        data.country = 'Malaysia';
      }

      this.cityList.forEach((cl) => {
        if (data.city == cl.id) {
          data.city = cl.name;
        }
      });

      this.stateList.forEach((sl) => {
        if (data.state == sl.id) {
          data.state = sl.name;
        }
      });
      delete data.email;
      delete data.phoneNumber;
      let eventProperties: EventProperties = {};
      eventProperties['country'] = data.country;
      eventProperties['state'] = data.state;
      this.pushGTMTags('save_profile_updates', eventProperties);
      this.maxisUserService.updateUserPopup(data).subscribe(
        (res) => {
          this.setUserData();
          this.box = true;
          this.normal = true;
          this.toastService.showSuccess(res.message);
          this.onClose();
          this.maxisUserService.refreshCurrentUser(API, TOKEN);
          setTimeout(() => {
            this.showLoader = false;
          }, 1000);
        },
        (err) => {
          this.showLoader = false;
          this.toastService.showError(err.message);
        }
      );
    }
  }
  onClose(): void {
    this.dialogRef.close();
  }

  fileChangeEvent(fileInput: any) {
    const reg = /(.*?)\.(jpg|jpeg|png|gif|giff|JPG|JPEG|PNG)$/;
    if (!fileInput.target.files[0].name.match(reg)) {
      this.toastService.showError('ACCOUNT_SETTINGS.PROFILE.FORM.FILE_SELECT_IMAGE');
      this.removeFile();
      return false;
    } else {
      this.removeFile();
      this.selectedFile = fileInput.target.files[0];
      var reader = new FileReader();
      reader.onload = this.handleReaderLoaded.bind(this);
      reader.readAsBinaryString(this.selectedFile);
    }
  }

  handleReaderLoaded(readerEvt) {
    var binaryString = readerEvt.target.result;
    this.base64textString = 'data:image/png;base64,' + btoa(binaryString);
    this.tempImg = this.base64textString;
    this.imagesList.map((obj, i) => {
      if (obj.hasOwnProperty('temp') && obj.temp) delete this.imagesList[i];
    });
    this.imagesList.splice(0, 0, {
      avatarImg: this.tempImg,
      isSelected: false,
      temp: true,
    });
    this.selectImg(this.tempImg, 0);
  }

  removeFile() {
    this.selectedFile = null;
  }
  onresetForm() {
    this.addForm.reset();
    this.setUserData();
    this.onClose();
  }

  onSelectFile(event) {
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]); // read file as data url
      reader.onload = (event) => {
        this.url = event.target.result;
      };
    }
  }
  isUniqueName = async (name) => {
    if (this.currentUser.username != name) {
      this.authServices.searchUsername(name).subscribe((res) => {
        this.isExist = res.data?.isExist;
        if (this.isExist) {
          this.addForm.controls['username'].setErrors({ incorrect: true });
        } else {
          this.addForm.controls['username'].setErrors(null);
          this.addForm.controls['username'].setValidators([
            Validators.required,
            Validators.minLength(3),
            Validators.maxLength(25),
            Validators.pattern(/^[a-zA-Z0-9_]+$/),
          ]);
          this.addForm.controls['username'].updateValueAndValidity();
        }
      });
    }
  };
  onFileInput(data) {}

  openprofile() {
    this.pushGTMTags('editProfilePhoto', '');
    this.normal = false;
    this.editprofile = true;
  }

  pushGTMTags(eventName: string, eventProperties : any) {
    let superProperties: SuperProperties = {};
    if(eventName == 'save_profile_update') {if (this.currentUser) {
      superProperties = this.gtmService.assignLoggedInUsedData(
        this.currentUser
      );
    }
      this.gtmService.gtmEventWithSuperProp({
        eventName: eventName,
        superProps: superProperties,
        eventProps: eventProperties,
      });
    } else {
      this.gtmService.gtmEventWithSuperProp({
        eventName: eventName,
        superProps: superProperties
      });
    }

  }

  close() {
    this.normal = true;
    this.editprofile = false;
    setTimeout(() => {
      if (!this.addForm.valid) document.getElementById('hjhgjg').click();
    }, 200);
  }
  selectImg(img, index) {
    let tempList = this.imagesList;
    this.imagesList = [];
    tempList.map((img, i) => {
      if (i == index) this.imagesList.push({ ...img, isSelected: false });
      else this.imagesList.push({ ...img, isSelected: true });
    });
    // this.Setimage(img);
    this.tempImg = img;
  }

  setImage() {
    this.url = this.tempImg;
    this.addForm.get('profilePicture').setValue(this.url);
    this.close();
  }
  ngOnDestroy(): void {
    if (this.userSubscription) this.userSubscription.unsubscribe();
  }

  OpenSettingsLightbox() {
    this.box = false;
    this.normal = true;
  }
  validateEmail(email) {
    var re = /\S+@\S+\.\S+/;
    return re.test(email);
  }
}
