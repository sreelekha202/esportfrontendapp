import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-settings-lightbox',
  templateUrl: './settings-lightbox.component.html',
  styleUrls: ['./settings-lightbox.component.scss'],
})
export class SettingsLightboxComponent implements OnInit {
  constructor(
    @Inject(MAT_DIALOG_DATA) public data,
    public dialogRef: MatDialogRef<SettingsLightboxComponent>
  ) {
    this.dialogRef.updateSize('100%', '100%');
  }

  ngOnInit(): void {}
  onClose(): void {
    this.dialogRef.close();
  }
}
