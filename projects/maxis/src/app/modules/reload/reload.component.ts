import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';
import { Subscription } from 'rxjs';
import { AppHtmlProfileRoutes } from '../../app-routing.model';
import { UserService } from '../../core/service';
import { WalletService } from '../../core/service/wallet.service';
import { environment } from '../../../environments/environment';
import { EsportsGtmService, EventProperties, IUser, SuperProperties } from 'esports';
import { Router } from '@angular/router';
declare var $: any;
@AutoUnsubscribe()
@Component({
  selector: 'app-reload',
  templateUrl: './reload.component.html',
  styleUrls: ['./reload.component.scss'],
})
export class ReloadComponent implements OnInit {
  maxisOnlinePayment = environment.maxisOnlinePayment;
  AppHtmlProfileRoutes = AppHtmlProfileRoutes;
  firtstimeflag: boolean = false;
  closeResult = '';
  firstflag1 = false;
  secondflag1 = false;
  thridflag1 = false;
  isSelected: boolean = false;
  isPaymentmethod: boolean = false;
  otp1 = '';
  otp2 = '';
  otp3 = '';
  otp4 = '';
  otp5 = '';
  otp6 = '';
  otp7 = '';
  otp8 = '';
  checkbox = false;
  amount = '';
  expdate = '';
  coins = '';
  datafortopupmoney: any = [];
  datafortopupmethod: any = [];
  uidtoken = '';
  amountfordisplay: any;
  coinsfordisplay: any;
  emailfordiaplay: any;
  latesttransactions: any = [];
  profiledata: any;
  type: any = '';
  sortflag = false;
  error_text = '';
  showwalletthings = true;
  error_text1: any = '';
  dcbSSTDetails: any;
  onlineBankingSSTDetails: any;
  showLoader: boolean;
  isShow: boolean = false;
  transactionAmount: number;
  paymentMethodID: number;
  userSubscription: Subscription;
  walletSubscription: Subscription;
  maxLimit: number = 0;
  currentUser: IUser;

  constructor(
    public dialogRef: MatDialogRef<ReloadComponent>,
    private router: Router,
    private walletService: WalletService,
    private maxisUserService: UserService,
    private modalService: NgbModal,
    private gtmService: EsportsGtmService
  ) {
    this.dialogRef.updateSize('100%', '100%');
  }

  ngOnInit(): void {
    let eventProperties: EventProperties = {};
    eventProperties['tabValue'] = 'Reload';
    this.pushGTMTags('select_tab', eventProperties);
    this.showLoader = true;
    this.userSubscription = this.maxisUserService.currentUser.subscribe(
      (data) => {
        if (data) {
          this.profiledata = data;
        }
      }
    );
    this.walletSubscription = this.walletService.recordsLoaded.subscribe(
      (data: any) => {
        if (data) {
          this.firtstimeflag = true;
          this.amount = data.data.currentBalanceCoins;
          this.expdate = data.data.expiryDate;
          this.coins = data.data.expiryCoins;
          this.maxLimit = data.data.maxLimit;
          if (this.amount == '0') {
            this.firtstimeflag = false;
          }
        }
      }
    );
    this.walletService.getWallettopupamount().subscribe(
      (data: any) => {
        this.datafortopupmoney = data.data;
        for (let data of this.datafortopupmoney) {
          data.selectflag = false;
        }
      },
      (error) => {}
    );
    this.walletService.getWallettopupmethods().subscribe(
      (data: any) => {
        this.datafortopupmethod = data.data;
        let flag = true;
        for (let data of this.datafortopupmethod) {
          data.paymentMethod == 'DCB' ? (flag = false) : '';
          data.selectflag = false;
        }
        flag ? this.showHotLink() : '';

        this.walletService.updateSubscriberProfile().subscribe(
          (res) => {
            if (res.success) {
              this.isShow = true;
              this.showLoader = false;
            }
          },
          (error) => {
            this.isShow = false;
            this.onClose();
            this.showLoader = false;
          }
        );
      },
      (error) => {}
    );
  }

  onClose(): void {
    this.dialogRef.close();
  }

  closeModal(): void {
    this.dialogRef.close(true);
  }
  onCloseAndRedirect(): void {
    this.dialogRef.close();
    this.onClose();
  }

  selectedvaluefun(data) {
    for (let d of this.datafortopupmoney) {
      if (d.dpwDisplayValue == data.dpwDisplayValue) {
        d.selectflag = true;
        this.isSelected = true;
      } else {
        d.selectflag = false;
      }
    }
  }

  selectedvaluefun1(data) {
    for (let d of this.datafortopupmethod) {
      if (d.paymentMethod == data.paymentMethod) {
        d.selectflag = true;
        this.isPaymentmethod = true;
      } else {
        d.selectflag = false;
      }
    }
  }

  senddatatobackend() {
    let eventProperties: EventProperties = {};
    this.pushGTMTags('start_reload', eventProperties);
    if (this.isSelected == true && this.isPaymentmethod == true) {
      let paymentMethod = '';
      let transactionAmount = 0;
      let paymentmethodid = 0;
      if (
        this.profiledata.phoneNumber != '' &&
        this.profiledata.phoneNumber != undefined
      ) {
        this.emailfordiaplay = this.profiledata.phoneNumber;
      } else {
        this.emailfordiaplay = this.profiledata.email;
      }
      for (let d of this.datafortopupmoney) {
        if (d.selectflag == true) {
          transactionAmount = Number(d.topUpValue);
          this.amountfordisplay = d.topUpValue;
          this.coinsfordisplay = d.dpwDisplayValue;
        }
      }
      for (let d of this.datafortopupmethod) {
        if (d.selectflag == true) {
          if (d.paymentMethod == 'Credit/Debit card') {
            paymentmethodid = 5;
          } else if (d.paymentMethod == 'Online banking') {
            paymentmethodid = 10;
          } else if (d.paymentMethod == 'DCB') {
            paymentmethodid = 0;
          }
          paymentMethod = d.paymentMethod;
        }
      }
      if (paymentmethodid != 0) {
        this.type = '';
        if (paymentmethodid == 0) {
          document.getElementById('myCheck').click();
        } else {
          this.paymentMethodID = paymentmethodid;
          this.transactionAmount = transactionAmount;
          document.getElementById('myCheck5').click();
          this.onlineBanking(true);
        }
      } else {
        this.type = 'DCB';
        let datatosend = {
          customerType: 'MSISDN',
          customerValue: this.profiledata.phoneNumber,
          billPrice: transactionAmount,
          paymentMethod: 'DCB',
          description: this.coinsfordisplay + 'GC @ Geng Gamer',
        };

        this.callGTM('continue_reload', transactionAmount, paymentMethod);
        this.showLoader = true;
        this.walletService.redirecttopaymentforDBC(datatosend, true).subscribe(
          (res: any) => {
            this.showLoader = false;
            this.dcbSSTDetails = res.data;
            document.getElementById('myCheck').click();
            this.callGTM('complete_reload', transactionAmount, paymentMethod);
          },
          (error) => {
            this.error_text = error.error.message;
            this.error_text1 = error.error.errorName;
            document.getElementById('myCheck2').click();
            this.showLoader = false;
          }
        );
      }
    } else {
      return false;
    }
  }

  callGTM(event, amount, payment_method) {
    let eventProperties: EventProperties = {};
    if (event == 'continue_reload' || event == 'complete_reload') {
      eventProperties['reloadMethod'] = payment_method;
      eventProperties['reloadAmount'] = amount;
    }

    this.pushGTMTags(event, eventProperties);
  }

  senddatatobackend1() {
    let paymentMethod = '';
    let transactionAmount = 0;
    let paymentmethodid = 0;
    this.error_text = '';
    this.error_text1 = '';
    if (
      this.profiledata.phoneNumber != '' &&
      this.profiledata.phoneNumber != undefined
    ) {
      this.emailfordiaplay = this.profiledata.phoneNumber;
    } else {
      this.emailfordiaplay = this.profiledata.email;
    }
    for (let d of this.datafortopupmoney) {
      if (d.selectflag == true) {
        transactionAmount = Number(d.topUpValue);
        this.amountfordisplay = d.topUpValue;
        this.coinsfordisplay = d.dpwDisplayValue;
      }
    }
    for (let d of this.datafortopupmethod) {
      if (d.selectflag == true) {
        if (d.paymentMethod == 'Credit/Debit card') {
          paymentmethodid = 5;
        } else if (d.paymentMethod == 'Online banking') {
          paymentmethodid = 10;
        } else if (d.paymentMethod == 'DCB') {
          paymentmethodid = 0;
        }
        paymentMethod = d.paymentMethod;
      }
    }
    if (paymentmethodid != 0) {
      this.type = '';
      this.paymentMethodID = paymentmethodid;
      this.transactionAmount = transactionAmount;
      document.getElementById('myCheck5').click();
      this.onlineBanking(true);
    } else {
      this.type = 'DCB';
      document.getElementById('myCheck').click();
    }
  }
  onlineBanking(sst = false) {
    let datatosend = {
      screenToDisplay: this.paymentMethodID,
      amount: this.transactionAmount,
    };
    this.showLoader = true;
    this.walletService.redirectToPayment(datatosend, sst).subscribe(
      (data: any) => {
        if (data) {
          this.showLoader = false;
          if (!sst) {
            this.showLoader = true;
            this.uidtoken = data.responseData;
            setTimeout(() => {
              document.getElementById('myCheck3').click();
            }, 1000);
          } else {
            this.onlineBankingSSTDetails = data.data;
          }
        }
      },
      (error) => {
        this.showLoader = false;
        this.error_text = error.error.message;
        this.error_text1 = error.error.errorName;
        document.getElementById('myCheck2').click();
      }
    );
  }

  submitfordcb() {
    this.showwalletthings = false;
    this.error_text = '';
    this.error_text1 = '';
    let transactionAmount = 0;
    for (let d of this.datafortopupmoney) {
      if (d.selectflag == true) {
        transactionAmount = Number(d.topUpValue);
        this.amountfordisplay = d.topUpValue;
        this.coinsfordisplay = d.dpwDisplayValue;
      }
    }
    let datatosend = {
      customerType: 'MSISDN',
      customerValue: this.profiledata.phoneNumber,
      billPrice: transactionAmount,
      paymentMethod: 'DCB',
      description: this.coinsfordisplay + 'GC @ Geng Gamer',
    };
    this.showLoader = true;
    this.walletService.redirecttopaymentforDBC(datatosend).subscribe(
      (data: any) => {
        this.showLoader = false;
        this.walletService.loadWalletDetails();
        this.showwalletthings = true;
        if (data.success == true) {
          this.openmodel();
        }
      },
      (error) => {
        this.walletService.loadWalletDetails();
        this.showwalletthings = true;
        this.error_text = error.error.message;
        this.error_text1 = error.error.errorName;
        document.getElementById('myCheck2').click();
        this.showLoader = false;
      }
    );
  }

  openmodel() {
    this.onClose();
    document.getElementById('myCheck1').click();
    this.walletService.getWalletdetails().subscribe(
      (data1: any) => {
        this.amount = data1.data.currentBalanceCoins;
        this.maxLimit = data1.data.maxLimit;
        this.expdate = data1.data.expiryDate;
        this.coins = data1.data.expiryCoins;
        if (this.amount == '0') {
          this.firtstimeflag = false;
        }
      },
      (error) => {
        if (
          error.error.message ==
          'Error in getting epmUser Wallet. No wallet associated with the user.'
        ) {
          this.firtstimeflag = true;
          this.amount = '0';
        }
      }
    );

    this.walletService.getWallettopupamount().subscribe(
      (data: any) => {
        this.datafortopupmoney = data.data;
        for (let data of this.datafortopupmoney) {
          data.selectflag = false;
        }
      },
      (error) => {}
    );
    this.walletService.getWallettopupmethods().subscribe(
      (data: any) => {
        this.datafortopupmethod = data.data;
        for (let data of this.datafortopupmethod) {
          data.selectflag = false;
        }
      },
      (error) => {}
    );
  }

  open(content) {
    this.onClose();
    this.modalService
      .open(content, {
        ariaLabelledBy: 'modal-basic-title',
        size: 'lg',
        windowClass: 'reload_modal',
        backdrop: true,
      })
      .result.then(
        (result) => {
          this.closeResult = `Closed with: ${result}`;
        },
        (reason) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }

  open1(content) {
    this.onClose();
    this.modalService
      .open(content, {
        ariaLabelledBy: 'modal-basic-title',
        size: 'lg',
        windowClass: 'reload_modal',
        backdrop: true,
      })
      .result.then(
        (result) => {
          this.closeResult = `Closed with: ${result}`;
        },
        (reason) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  finalsubmit() {
    let datatosend = {
      walletPin: this.otp1 + this.otp2 + this.otp3 + this.otp4,
      walletBalance: 0,
      subscriberSegment: 'OLO',
    };
    this.walletService.submitnewuserwallet(datatosend).subscribe(
      (data: any) => {},
      (error) => {}
    );
  }

  open3(content) {
    this.onClose();
    this.modalService
      .open(content, {
        ariaLabelledBy: 'modal-basic-title',
        windowClass: 'reload_modal',
        backdrop: true,
      })
      .result.then(
        (result) => {
          this.closeResult = `Closed with: ${result}`;
        },
        (reason) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }
  validateEmail(email) {
    let re = /\S+@\S+\.\S+/;
    return re.test(email);
  }
  showHotLink() {
    $('#Hotlink').modal('show');
  }
  closeHotLink() {
    $('#Hotlink').modal('hide');
    this.onClose();
  }
  ngOnDestroy(): void {
    if (this.userSubscription) this.userSubscription.unsubscribe();
    if (this.walletSubscription) this.walletSubscription?.unsubscribe();
  }

  pushGTMTags(eventName: string, eventProperties: any) {
    let superProperties: SuperProperties = {};
    if (this.currentUser) {
      superProperties = this.gtmService.assignLoggedInUsedData(
        this.currentUser
      );
    }
    this.gtmService.gtmEventWithSuperProp({
      eventName: eventName,
      superProps: superProperties,
      eventProps: eventProperties,
    });
  }
}
