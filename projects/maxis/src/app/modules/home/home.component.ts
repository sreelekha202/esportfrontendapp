import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import { AuthServices, UserService } from '../../core/service';
import { AppHtmlRoutes } from '../../app-routing.model';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';
import { UserPopupComponent } from '../../core/header/components/user-popup/user-popup.component';
import { MatDialog } from '@angular/material/dialog';
import { SwiperComponent, SwiperConfigInterface } from 'ngx-swiper-wrapper';
import { environment } from '../../../environments/environment';
import {
  EsportsLanguageService,
  EsportsToastService,
  EsportsUtilsService,
  IUser,
  GlobalUtils,
} from 'esports';
const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;
declare var $: any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;
  showLoader: boolean = true;
  currentUser: IUser;
  userSubscription: Subscription;
  selectedGameIndex = 0;
  isShowVideo = false;
  newsArtical = [];
  newsVideos = [];
  @ViewChild(SwiperComponent, { static: false }) compRef?: SwiperComponent;

  public config: SwiperConfigInterface = {
    breakpoints: {
      320: {
        slidesPerView: 1.4,
        spaceBetween: 10,
        autoHeight: false,
      },
      480: {
        slidesPerView: 1.4,
        spaceBetween: 10,
        autoHeight: true,
      },
      640: {
        slidesPerView: 2.3,
        spaceBetween: 10,
        autoHeight: true,
      },
      800: {
        slidesPerView: 4.4,
        spaceBetween: 10,
        autoHeight: true,
      },
      1280: {
        slidesPerView: 4.4,
        spaceBetween: 10,
        autoHeight: true,
      },
    },
  };
  currLanguage: string = 'english';
  tournaments = [];
  isAllPage: boolean = true;
  activeRoute;
  isAllPageSet(status) {
    this.isAllPage = status;
  }
  constructor(
    public language: EsportsLanguageService,
    public toastService: EsportsToastService,
    public translateService: TranslateService,
    public utilsService: EsportsUtilsService,
    private userService: UserService,
    private activatedRoute: ActivatedRoute,
    private authService: AuthServices,
    public matDialog: MatDialog
  ) {}

  ngOnInit(): void {
    if (GlobalUtils.isBrowser()) {
      $(document).on('scroll', function () {
        const topOffset = $('.home_wrap').offset().top;
        const scrollTop = window.scrollY;
        const rightValue = Math.max(40 - 0.2 * (topOffset - scrollTop), -30);

        if (rightValue < 40) {
          $('.home_animation_h1_text1').css('left', rightValue + 'vw');
          $('.home_animation_h1_text2').css('right', rightValue + 'vw');
        }
      });
      this.language.language.subscribe((lang) => {
        if (lang) {
          this.currLanguage = lang === 'ms' ? 'malay' : 'english';
        }
      });

      this.activatedRoute.queryParams.subscribe((params) => {
        this.showLoader = false;
        if (params.code != null) {
          let auth_code = params['code'];
          this.showLoader = true;
          this.authService.maxis_auth(auth_code).subscribe(
            (authData) => {
              this.showLoader = false;
              localStorage.setItem(
                environment.currentToken,
                authData.data.token
              );
              localStorage.setItem('refreshToken', authData.data.refreshToken);
              this.userService.getAuthenticatedUser(API, TOKEN);
              if (authData.data.firstLogin == 1) {
                this.matDialog.open(UserPopupComponent, { disableClose: true });
              }
            },
            (err) => {
              this.toastService.showError(err?.error?.message);
              localStorage.removeItem(TOKEN);
              localStorage.removeItem('refreshToken');
              window.location.replace(`${environment.maxisLogout}`);
              this.showLoader = false;
            }
          );
        } else if (params.token != null) {
          let mstoken = params['token'];
          this.showLoader = true;
          this.authService.maxis_authMsToken(mstoken).subscribe(
            (authData) => {
              this.showLoader = false;
              localStorage.setItem(
                environment.currentToken,
                authData.data.token
              );
              localStorage.setItem('refreshToken', authData.data.refreshToken);
              this.userService.getAuthenticatedUser(API, TOKEN);
              if (authData.data.firstLogin == 1) {
                this.matDialog.open(UserPopupComponent, { disableClose: true });
              }
            },
            (err) => {
              this.toastService.showError(err?.error?.message);
              localStorage.removeItem(TOKEN);
              localStorage.removeItem('refreshToken');
              window.location.replace(`${environment.maxisLogout}`);
              this.showLoader = false;
            }
          );
        }
      });
    }

    this.newsArtical = [
      {
        url: 'assets/images/image-article.png',
        title: 'Latest Update: Top 15 Games in 2021',
        date: 'Aug 2, 2021',
        createdBy: 'jagong_gong',
        type: 'GAMES',
        icon: 'icon_games',
      },
      {
        url: 'assets/images/content-1_1.png',
        title: 'Latest News 2021: The Best Esports Games Right Now',
        date: 'Aug 2, 2021',
        createdBy: 'kun0ichi_88',
        type: 'ESPORTS',
        icon: 'icon_esports',
      },
      {
        url: 'assets/images/content-1_3.png',
        title: 'Fortnite tips and tricks you should know about',
        date: 'Aug 2, 2021',
        createdBy: 'kun0ichi_88',
        type: 'TOP PICKS',
        icon: 'icon_toppicks',
      },
      {
        url: 'assets/images/content-1_4.png',
        title: 'Women in esports are big — And underserved',
        date: 'Aug 2, 2021',
        createdBy: 'jagong_gong',
        type: 'ESPORTS',
        icon: 'icon_esports',
      },
    ];

    this.newsVideos = [
      {
        url: 'assets/images/image.png',
        title:
          'Geguri – the First Female Pro Signed to an Overwatch League Signed to an Overwatch League',
        time: '3 day ago',
        Views: '145K views',
        createdBy: 'IGN',
        icon: 'video_icon1',
      },
      {
        url: 'assets/images/imageVideo.png',
        title:
          'Geguri – the First Female Pro Signed to an Overwatch League Signed to an Overwatch League Signed to an Overwatch League',
        time: '4 weeks ago',
        Views: '480K views',
        createdBy: 'Valorant Vision',
        icon: 'video_icon2',
      },
      {
        url: 'assets/images/image-video3.png',
        title:
          'CARMILLA | Updated Guide 2021 | Best Build | Top Global Item Mist...',
        time: '1 day ago',
        Views: '27K views',
        createdBy: 'Stylosa',
        icon: 'video_icon3',
      },
      {
        url: 'assets/images/image-video4.png',
        title: 'BEST PHOENIX IN VALORANT?! | BEST OF SUBROZA',
        time: '2 hours ago',
        Views: '38K views',
        createdBy: 'Strongwill Game Theory',
        icon: 'video_icon4',
      },
    ];

    // MOCK SUGGESTED TOURNAMENTS
    this.tournaments = [
      {
        image:
          'https://maxis-stg-image.s3.ap-southeast-1.amazonaws.com/stage/home/FortniteFirststrike.svg',
        title: 'Fortnite First strike tournament 2021',
        date: 'Aug 04, 2021',
        user: 'By jagong_gong',
        time: '9:00 am',
        user_image: 'user_jagong',
      },
      {
        image:
          'https://maxis-stg-image.s3.ap-southeast-1.amazonaws.com/stage/home/OverwatchAll-Stars2021Championships!!!.svg',
        title: 'Overwatch All-Stars 2021 Championships!!!',
        date: 'Aug 05, 2021',
        user: 'By sitikus96',
        time: '9:00 am',
        user_image: 'user_sitikus',
      },
      {
        image:
          'https://maxis-stg-image.s3.ap-southeast-1.amazonaws.com/stage/home/Valorantsummer2021championships.svg',
        title: 'Valorant summer 2021 championships',
        date: 'Aug 05, 2021',
        user: 'By tasyadudz',
        time: '11:00 am',
        user_image: 'user_tasyadudz',
      },
      {
        image:
          'https://maxis-stg-image.s3.ap-southeast-1.amazonaws.com/stage/home/image60.svg',
        title: 'Overwatch All-Stars 2021 Championships!!!',
        date: 'Aug 08, 2021',
        user: 'By sitikus96',
        time: '01:00 am',
        user_image: 'user_sitikus',
      },
      {
        image:
          'https://maxis-stg-image.s3.ap-southeast-1.amazonaws.com/stage/home/image59.svg',
        title: 'Fortnite First strike tournament 2021',
        date: 'Aug 08, 2021',
        user: 'By jagong_gong',
        time: '10:00 am',
        user_image: 'user_jagong',
      },
    ];
  }

  prevSlide() {
    this.compRef.directiveRef.prevSlide();
  }
  nextSlide() {
    this.compRef.directiveRef.nextSlide();
  }

  showArticles() {
    this.isShowVideo = false;
  }

  showVideos() {
    this.isShowVideo = true;
  }
}
