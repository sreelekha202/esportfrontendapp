import { Component, OnInit, ViewChild } from '@angular/core';
import { SwiperComponent, SwiperConfigInterface } from 'ngx-swiper-wrapper';
import { Router } from '@angular/router';
import { EsportsGtmService, EsportsToastService, EventProperties, IUser, SuperProperties } from 'esports';
import { Subscription } from 'rxjs/Subscription';
import { ShopService } from '../../../core/service/shop.service';
import { UserService } from '../../../core/service';

@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.scss'],
})
export class ShopComponent implements OnInit {
  shops = [];
  @ViewChild(SwiperComponent, { static: false }) compRef?: SwiperComponent;

  selectedGameIndex = 0;
  userSubscription: Subscription;
  currentUser: IUser;
  showLoader: boolean = true;
  public config: SwiperConfigInterface = {
    breakpoints: {
      320: {
        slidesPerView: 2.3,
        spaceBetween: 16,
        autoHeight: false,
      },
      480: {
        slidesPerView: 2.3,
        spaceBetween: 16,
        autoHeight: true,
      },
      640: {
        slidesPerView: 3,
        spaceBetween: 16,
        autoHeight: true,
      },
      800: {
        slidesPerView: 5,
        spaceBetween: 16,
        autoHeight: true,
      },
      1280: {
        slidesPerView: 6.3,
        spaceBetween: 16,
        autoHeight: true,
      },
    },
  };

  constructor(
    private shopService: ShopService,
    private router: Router,
    private toast: EsportsToastService,
    private userService: UserService,
    public gtmService: EsportsGtmService
  ) {}

  ngOnInit(): void {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.currentUser = data;
      }
    });
    this.topUpList();
  }

  prevSlide() {
    this.compRef.directiveRef.prevSlide();
  }
  nextSlide() {
    this.compRef.directiveRef.nextSlide();
  }

  topUpList() {
    this.showLoader = true;
    this.shopService.topUpList().subscribe(
      (res: any) => {
        this.shops = res.data;
        this.showLoader = false;
      },
      (err) => {
        this.showLoader = false;
      }
    );
  }
  visitShop() {
    this.pushGTMTags('visit_shop');
    this.router.navigateByUrl('/shop');
  }

  redirectToupOrVoucher(shop: any) {
    if (this.currentUser) {
      if (shop.category == 'VOUCHER') {
        this.router.navigateByUrl('/shop/voucher/' + shop._id);
      }
      if (shop.category == 'TOPUP') {
        this.router.navigateByUrl('/shop/topup/' + shop._id);
      }
    } else {
      this.toast.showError('Authentication is required. Please login.');
    }
  }

  pushGTMTags(eventName: string) {
    let eventProperties: EventProperties = {};
    let superProperties: SuperProperties = {};
    if (this.currentUser) {
      superProperties = this.gtmService.assignLoggedInUsedData(
        this.currentUser
      );
    }
    this.gtmService.gtmEventWithSuperProp({
      eventName: eventName,
      superProps: superProperties,
      eventProps: eventProperties,
    });
  }
}
