import { NgModule } from '@angular/core';
import { HomeComponent } from './home.component';
import { HomeRoutingModule } from './home-routing.module';
import { PreviewSliderComponent } from './preview-slider/preview-slider.component';
import { ShopComponent } from './shop/shop.component';
import { ShopCardComponent } from './shop-card/shop-card.component';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { SharedModule } from '../../shared/modules/shared.module';
import { CoreModule } from './../../core/core.module';
const modules = [SharedModule, CoreModule, HomeRoutingModule];

@NgModule({
  declarations: [
    HomeComponent,
    PreviewSliderComponent,
    ShopComponent,
    ShopCardComponent,
  ],
  imports: [modules, SlickCarouselModule],
})
export class HomeModule {}
