import { Component, Input, OnInit } from '@angular/core';
import {
  SuperProperties,
  EventProperties,
  EsportsGtmService,
  IUser,
} from 'esports';
import { Subscription } from 'rxjs';
import { sampleTime } from 'rxjs/operators';
import { UserService } from '../../../core/service';

@Component({
  selector: 'app-announcement',
  templateUrl: './announcement.component.html',
  styleUrls: ['./announcement.component.scss'],
})
export class AnnouncementComponent implements OnInit {
  constructor(
    private gtmService: EsportsGtmService,
    private userService: UserService
  ) {}
  @Input() data: any;
  @Input() showBox: any;
  userSubscription: Subscription;
  currentUser: IUser;
  ngOnInit(): void {
    this.userSubscription = this.userService.currentUser
      .pipe(sampleTime(500))
      .subscribe((user) => {
        if (user) {
          this.currentUser = user;
        }
        if (this.data && this.showBox) {
          this.pushGTMTags('pop_up_rendered', this.data);
        }
      });
  }
  closeModal() {
    this.pushGTMTags('pop_up_seen', this.data);
    localStorage.setItem('announcement', this.data.uniqueId);
    this.showBox = false;
  }

  /**
   * destination url
   * @param obj
   */
  redirectLink(obj) {
    this.pushGTMTags('pop_up_clicked', this.data);
    this.closeModal();
    window.location.href = obj;
  }

  /**
   * @param eventName
   * @param eventData popup(announcement) details
   */

  pushGTMTags(eventName: string, eventData = null) {
    let superProperties: SuperProperties = {};
    if (this.currentUser) {
      superProperties = this.gtmService.assignLoggedInUsedData(
        this.currentUser
      );
    }
    let eventProperties: EventProperties = {
      popUpId: eventData?.uniqueId,
      imageName: eventData?.image,
      imageType: 'Foreground',
      redirectionURL: eventData?.destination,
    };
    this.gtmService.gtmEventWithSuperProp({
      eventName: eventName,
      superProps: superProperties,
      eventProps: eventProperties,
    });
  }
}
