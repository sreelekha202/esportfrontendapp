import { Component, OnInit } from '@angular/core';
import { GlobalUtils } from 'esports';
import { ActivatedRoute, Router } from '@angular/router';
import { AppHtmlRoutes } from '../../app-routing.model';
@Component({
  selector: 'app-notfound',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.scss'],
})
export class NotfoundComponent implements OnInit {
  constructor(private router: Router,private activatedRoute: ActivatedRoute,) { }
  AppHtmlRoutes = AppHtmlRoutes;
  text: string = '';
  ngOnInit(): void { }
  onSearch(): void {
    if (GlobalUtils.isBrowser()) {
      let a = document.getElementById('dsgfdjkf').click();
      this.router.navigate([AppHtmlRoutes.search], {
        relativeTo: this.activatedRoute,
        queryParams: { text: this.text },
        queryParamsHandling: 'merge', // remove to replace all query params by provided
      });
    }
  }
}
