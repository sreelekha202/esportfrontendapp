import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EsportsGtmService, EventProperties, IUser, SuperProperties } from 'esports';
import { AppHtmlRoutes } from '../../../app-routing.model';

@Component({
  selector: 'app-purchase-successful',
  templateUrl: './purchase-successful.component.html',
  styleUrls: ['./purchase-successful.component.scss'],
})
export class PurchaseSuccessfulComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes
  @Input() shopItem: any;
  @Input() type: string = 'normal';
  currentUser: IUser;
  
  constructor(private router: Router, public gtmService: EsportsGtmService) { }

  ngOnInit(): void {
    window.scroll(0, 0);
  }

  navigatefunc() {
    let eventProperties: EventProperties = {};
    eventProperties['purchase_type'] = 'Top-Up';
    this.pushGTMTags('view_transaction_history', eventProperties);
    let flag = '1';
    sessionStorage.setItem('flagforpruchase', flag);
    this.router.navigateByUrl('account/transaction');
  }

  navigatefuncForNewPurchase() {
    let eventProperties: EventProperties = {};
    eventProperties['purchase_type'] = 'Top-Up';
    this.pushGTMTags('make_another_purchase', eventProperties);
    this.router.navigate([AppHtmlRoutes.shop]);
  }

  pushGTMTags(eventName: string, eventProperties: any) {
    let superProperties: SuperProperties = {};

    if (this.currentUser) {
      superProperties = this.gtmService.assignLoggedInUsedData(
        this.currentUser
      );
    }

    this.gtmService.gtmEventWithSuperProp({
      eventName: eventName,
      superProps: superProperties,
      eventProps: eventProperties,
    });
  }
}
