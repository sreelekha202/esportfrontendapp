import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EsportsGtmService, EsportsToastService, EventProperties, GlobalUtils, IUser, SuperProperties } from 'esports';
import { AppHtmlRoutes } from '../../../app-routing.model';

@Component({
  selector: 'app-voucher-purchase-successful',
  templateUrl: './voucher-purchase-successful.component.html',
  styleUrls: ['./voucher-purchase-successful.component.scss'],
})
export class VoucherPurchaseSuccessfulComponent implements OnInit {
  @Input() shopItem;
  AppHtmlRoutes = AppHtmlRoutes;
  constructor(private toast: EsportsToastService, private router: Router, public gtmService: EsportsGtmService) { }
  currentUser: IUser;
  ngOnInit(): void {
    window.scroll(0, 0);
  }

  copyInputMessage(inputElement) {
    if (GlobalUtils.isBrowser()) {
      inputElement.select();
      document.execCommand('copy');
      inputElement.setSelectionRange(0, 0);
      this.toast.showSuccess('Copied!');
    }
  }

  navigatefunc() {
    let eventProperties: EventProperties = {};
    eventProperties['purchase_type'] = 'Voucher';
    this.pushGTMTags('view_transaction_history', eventProperties);
    let flag = '1';
    sessionStorage.setItem('flagforpruchase', flag);
    this.router.navigateByUrl('account/transaction');
  }

  navigatefuncForNewPurchase() {
    let eventProperties: EventProperties = {};
    eventProperties['purchase_type'] = 'Voucher';
    this.pushGTMTags('make_another_purchase', eventProperties);
    this.router.navigate([AppHtmlRoutes.shop]);
  }

  pushGTMTags(eventName: string, eventProperties: any) {
    let superProperties: SuperProperties = {};

    if (this.currentUser) {
      superProperties = this.gtmService.assignLoggedInUsedData(
        this.currentUser
      );
    }

    this.gtmService.gtmEventWithSuperProp({
      eventName: eventName,
      superProps: superProperties,
      eventProps: eventProperties,
    });
  }
}
