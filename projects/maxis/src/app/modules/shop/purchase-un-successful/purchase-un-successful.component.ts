import { Component, Input, OnInit } from '@angular/core';
import { AppHtmlRoutes } from '../../../app-routing.model';

@Component({
  selector: 'app-purchase-un-successful',
  templateUrl: './purchase-un-successful.component.html',
  styleUrls: ['./purchase-un-successful.component.scss'],
})
export class PurchaseUnSuccessfulComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;
  @Input() error: any;
  constructor() {}

  ngOnInit(): void {
    window.scroll(0, 0);
  }
}
