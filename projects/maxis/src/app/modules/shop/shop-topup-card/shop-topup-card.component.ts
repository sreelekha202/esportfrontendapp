import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-shop-topup-card',
  templateUrl: './shop-topup-card.component.html',
  styleUrls: ['./shop-topup-card.component.scss']
})
export class ShopTopupCardComponent implements OnInit {

  constructor() { }

  @Input() shop: any = {};

  ngOnInit(): void { }
}
