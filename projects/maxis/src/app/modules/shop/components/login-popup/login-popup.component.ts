import { Component, OnInit } from '@angular/core';
import { GlobalUtils } from 'esports';
import { AppHtmlRoutes } from '../../../../app-routing.model';
import { environment } from '../../../../../environments/environment';

@Component({
  selector: 'app-login-popup',
  templateUrl: './login-popup.component.html',
  styleUrls: ['./login-popup.component.scss'],
})
export class LoginPopupComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;
  constructor() {}

  ngOnInit(): void {}
  onLogin() {
    if (GlobalUtils.isBrowser()) {
      window.location.replace(`${environment.maxisLogin}`);
    }
  }
}
