import { ProfileService } from './../../../core/service/profile.service';
import { Component, OnInit } from '@angular/core';
import { AppHtmlProfileRoutes } from '../../../app-routing.model';

@Component({
  selector: 'app-my-teams',
  templateUrl: './my-teams.component.html',
  styleUrls: ['./my-teams.component.scss'],
})
export class MyTeamsComponent implements OnInit {
  AppHtmlProfileRoutes = AppHtmlProfileRoutes
  active = 1;
  nextId: number = 1;
  myTeams = [];
  teamRequests = [];
  showLoader: boolean = true;
  paginationData = {
    page: 1,
    limit: 10,
    sort: { _id: -1 },
    text: '',
  };
  constructor(private profileService: ProfileService) { }

  ngOnInit(): void {

    this.getMyTeams();
    this.getMyInvite();
  }
  navChanges = (e) => {
    this.nextId = e.nextId;
    switch (this.nextId) {
      case 1:
        this.getMyTeams();
        break;
      case 2:
        this.getMyInvite();
        break;
    }
  }
  getMyInvite() {
    this.showLoader = true;
    const pagination = JSON.stringify(this.paginationData);
    this.profileService.getMyInvite({
      pagination: pagination,
    }).subscribe((res) => {
      this.teamRequests = []
      for (let d of res.data.docs) {
        let d1 = {
          image: d.logo,
          name: d.teamName,
          teamsLength: 5,
          createtdAt: 1,
          _id: d._id
        }
        this.teamRequests.push(d1)
      }
      this.showLoader = false;
    }, (err) => { this.showLoader = false; })
  }
  getMyTeams() {
    const pagination = JSON.stringify(this.paginationData);
    this.profileService.getMyTeam({
      pagination: pagination,
    }).subscribe((res) => {
      this.myTeams = []
      for (let d of res.data.docs) {
        let d1 = {
          image:
            d.logo,
          name: d.teamName,
          id: d._id,
          teamsLength: 5,
          createtdAt: 1,
        }
        this.myTeams.push(d1)

      }
      this.showLoader = false;
    }, (err) => { this.showLoader = false; })
   
  }

  acceptfunction(data) {
    const datatosend = {
      "teamId": data._id,
      "status": "active"
    }
    this.profileService.accept_reject(datatosend).subscribe((data: any) => {
    })
  }
}
