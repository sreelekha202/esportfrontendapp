import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { EsportsGtmService, SuperProperties } from 'esports';
import { AppHtmlRoutes } from '../../../../app-routing.model';
import { UserService } from 'projects/maxis/src/app/core/service';

@Component({
  selector: 'app-match-card',
  templateUrl: './match-card.component.html',
  styleUrls: ['./match-card.component.scss'],
})
export class MatchCardComponent implements OnInit {
  @Input() data: any;
  @Input() isEditable: boolean = false;
  @Input() index: number;
  AppHtmlRoutes = AppHtmlRoutes;
  constructor(
    private router: Router,
    private gtmService: EsportsGtmService,
    private userService: UserService
  ) {}

  ngOnInit(): void {
    this.data = this.data;
  }

  genrateReport() {
    this.pushGTMTags('view_report_match_issue');
    if (this.data?.tournamentId && this.data?.matchId) {
      this.router.navigate([
        `${AppHtmlRoutes.tournament}/report/${this.data?.matchId}/${this.data?.tournamentId}`,
      ]);
    }
  }
  reportScore(data) {
    localStorage.setItem('t_report', JSON.stringify(data));
    this.router.navigate([`/report-score`]);
  }

  exitFromScoreCard(data) {}

  viewMatch() {
    this.pushGTMTags('view_match_page');
    if (this.data?.tournamentSlug) {
      this.router.navigate([
        `${AppHtmlRoutes.tournament}/${this.data?.tournamentSlug}`,
      ]);
    }
  }

  /**
   * @param eventName
   */

  pushGTMTags(eventName: string) {
    let superProperties: SuperProperties = {};
    if (this.userService.currentUserSubject.value) {
      superProperties = this.gtmService.assignLoggedInUsedData(
        this.userService.currentUserSubject.value
      );
    }
    this.gtmService.gtmEventWithSuperProp({
      eventName: eventName,
      superProps: superProperties,
    });
  }
}
