import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-transactions-table',
  templateUrl: './transactions-table.component.html',
  styleUrls: ['./transactions-table.component.scss'],
})
export class TransactionsTableComponent implements OnInit {
  @Input() data = [];
  @Input() pagination: any;
  @Output() PageOnChanges = new EventEmitter<string>();
  transactionsPerPage = [5, 10, 20];
  slicesTransactions = [];

  constructor() {}
  ngOnInit(): void {
    this.slicesTransactions = this.data.slice(0, this.transactionsPerPage[0]);
  }
  onPageChange(event): void {
    const startIndex = event.pageIndex * event.pageSize;
    let endIndex = startIndex + event.pageSize;
    if (endIndex > this.data.length) {
      endIndex = this.data.length;
    }
    this.slicesTransactions = this.data.slice(startIndex, endIndex);
    this.PageOnChanges.emit(event);
  }
}
