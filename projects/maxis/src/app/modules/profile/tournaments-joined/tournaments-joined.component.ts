import { Component, OnInit } from '@angular/core';
import { ProfileService } from '../../../core/service/profile.service';
import { HomeService } from '../../../core/service';
@Component({
  selector: 'app-tournaments-joined',
  templateUrl: './tournaments-joined.component.html',
  styleUrls: ['./tournaments-joined.component.scss'],
})
export class TournamentsJoinedComponent implements OnInit {
  tournaments = [];
  active = 1;
  nextId = 1;
  ongoingList = [];
  upcomingList = [];
  pastList = [];
  showLoader: boolean = false;

  tournamentsPerPage = [5, 10, 25];

  slicesTournaments = [];

  constructor(
    private profileService: ProfileService,
    private homeService: HomeService
  ) {}

  ngOnInit(): void {
    this.getOnGoing();
    this.slicesTournaments = this.tournaments.slice(
      0,
      this.tournamentsPerPage[0]
    );
  }

  getOnGoing() {
    this.showLoader = true;
    this.homeService.getongoing().subscribe(
      (res: any) => {
        this.ongoingList = res.data.docs;
        this.showLoader = false;
      },
      (err) => {
        this.showLoader = false;
      }
    );
  }

  getUpcoming() {
    this.homeService.getUpcoming().subscribe(
      (res: any) => {
        this.upcomingList = res.data.docs;
        this.showLoader = false;
      },
      (err) => {
        this.showLoader = false;
      }
    );
  }
  getPastList() {
    this.homeService.getPastList().subscribe(
      (res: any) => {
        this.pastList = res.data.docs;
        this.showLoader = false;
      },
      (err) => {
        this.showLoader = false;
      }
    );
  }

  navChanges = (e) => {
    this.nextId = e.nextId;
    switch (this.nextId) {
      case 1:
        this.getOnGoing();
        break;
      case 2:
        this.getUpcoming();
        break;
      case 3:
        this.getPastList();
        break;
      default:
        break;
    }
  };

  getTournaments() {
    this.showLoader = true;
    const params: any = {};
    params.status = this.nextId - 1;
    params.pagination = true;
    this.profileService.getMyJoinedTournament(params).subscribe(
      (res) => {
        this.tournaments = res.data.docs;
        this.showLoader = false;
      },
      (err) => {
        this.showLoader = false;
      }
    );
  }

  onPageChange(event): void {
    const startIndex = event.pageIndex * event.pageSize;
    let endIndex = startIndex + event.pageSize;

    if (endIndex > this.tournaments.length) {
      endIndex = this.tournaments.length;
    }
    this.slicesTournaments = this.tournaments.slice(startIndex, endIndex);
  }
}
