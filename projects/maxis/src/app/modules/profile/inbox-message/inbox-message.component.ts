import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { MessageService } from '../../../core/service';

@Component({
  selector: 'app-inbox-message',
  templateUrl: './inbox-message.component.html',
  styleUrls: ['./inbox-message.component.scss'],
})
export class InboxMessageComponent implements OnInit {
  showLoader: boolean = true;

  message = null;

  constructor(
    private location: Location,
    private messageService: MessageService,
    private route: ActivatedRoute,

  ) {}

  ngOnInit(): void {
    this.getMessage(this.route.snapshot.params.id);
  }

  getMessage(messageId: string): void {
    this.messageService.getMessage(messageId).subscribe(
      (response) => {
        this.message = response.data[0];
        this.showLoader = false;
      },
      (error) => {
        this.showLoader = false;
      }
    );
  }

  goBack() {
    this.location.back();
  }
}
