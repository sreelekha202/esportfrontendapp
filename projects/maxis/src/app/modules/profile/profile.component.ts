import { Component, OnInit, OnDestroy } from '@angular/core';
import { Location } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { AppHtmlProfileRoutes, AppHtmlRoutes } from '../../app-routing.model';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';
import { TranslateService } from '@ngx-translate/core';
import { MessageService, UserService } from '../../core/service';
import { EsportsLanguageService, GlobalUtils } from 'esports';
import { environment } from '../../../environments/environment';
import { MatDialog } from '@angular/material/dialog';
import { SettingsLightboxComponent } from '../settings-lightbox/settings-lightbox.component';
import { Subscription } from 'rxjs';

AutoUnsubscribe();
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
})
export class ProfileComponent implements OnInit, OnDestroy {
  AppHtmlProfileRoutes = AppHtmlProfileRoutes;
  msgCount: any;
  userSubscription: Subscription;
  currentUser: any;
  AppHtmlRoutes = AppHtmlRoutes
  constructor(
    private languageService: EsportsLanguageService,
    private userService: UserService,
    private translateService: TranslateService,
    private messageService: MessageService,
    public location: Location,
    public router: Router,
    public matDialog: MatDialog,
  ) {
    if (
      GlobalUtils.isBrowser() &&
      !localStorage.getItem(environment.currentToken)
    ) {
      this.location.back();
      this.router.navigate(['/login']);
    }
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) { this.currentUser = data; }
    });
  }

  ngOnInit(): void {
    this.getMessages()
    this.languageService.language.subscribe((lang) =>
      this.translateService.use(lang || 'en')
    );
    this.messageService.messageCount.subscribe((count) => { this.msgCount = count });
  }

  openSettingsLightbox(selectedTab = 0) {
    const data = { selectedTab, };
    this.matDialog.open(SettingsLightboxComponent, {
      data,
      disableClose: true,
      position: {
        top: '34px',
      },
      panelClass:'account_settings'
    });
  }

  getMessages(): void {
    this.messageService.getInboxMessages().subscribe(
      (response) => {
        this.messageService.messageCountSubject.next(response.data.length)
      }, (error) => { }
    );
  }

  ngOnDestroy(): void { }
}
