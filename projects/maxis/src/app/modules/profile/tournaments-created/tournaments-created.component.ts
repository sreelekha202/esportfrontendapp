import { ProfileService } from './../../../core/service/profile.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tournaments-created',
  templateUrl: './tournaments-created.component.html',
  styleUrls: ['./tournaments-created.component.scss'],
})
export class TournamentsCreatedComponent implements OnInit {
  tournaments = [];
  showLoader: boolean = true;
  active = 2;
  nextId = 1;
  constructor(private profileService: ProfileService) { }
  ngOnInit(): void { this.getTournaments(); }
  navChanges = (e) => { this.nextId = e.nextId; this.getTournaments() }
  getTournaments() {
    this.showLoader = true
    const params: any = {};
    params.status = this.nextId - 1;
    this.profileService.getMyCreatedTournament(params).subscribe((res) => {
      this.tournaments = res.data.docs;
      this.showLoader = false;
    }, (err) => { this.showLoader = false; })
  }
}
