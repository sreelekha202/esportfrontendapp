import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
} from '@angular/core';
import { IPagination } from '../../shared/models';
import { PageChangedEvent } from 'ngx-bootstrap/pagination';
import { EsportsPaginationService, GlobalUtils } from 'esports';

@Component({
  selector: 'app-pagination',
  templateUrl: './custom-pagination.component.html',
  styleUrls: ['./custom-pagination.component.scss'],
})
export class CustomPaginationComponent implements OnInit {
  @Input() activePage: Number = 1;
  @Input() page: IPagination;
  @Output() currentPage = new EventEmitter<Number>();

  constructor(private paginationService: EsportsPaginationService) {}

  ngOnInit(): void {}

  pageChanged(event: PageChangedEvent) {
    this.currentPage.emit(event.page);
    this.paginationService.setPageChanged(true);
  }
  onScrollTop() {
    if (GlobalUtils.isBrowser()) {
      window.scroll(0, 0);
    }
  }
}
