import { MatDialog } from '@angular/material/dialog';
import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
} from '@angular/common/http';

import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { map } from 'rxjs/operators';
import { CookieService } from 'ngx-cookie-service';
import { environment } from '../../../environments/environment';
import { Router } from '@angular/router';
import { PayPopupComponent } from '../pay-popup/pay-popup.component';
import { GlobalUtils } from 'esports';

const API = environment.apiEndPoint + 'auth/';
const token = environment.currentToken;

@Injectable({
  providedIn: 'root',
})
export class AuthServices {
  // store the URL so we can redirect after logging in
  redirectUrl: string;

  constructor(
    private cookieService: CookieService,
    private globalUtils: GlobalUtils,
    private http: HttpClient,
    private router: Router,
    private dialog: MatDialog
  ) { }

  register(data): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    return this.http.post(environment.apiEndPoint + 'auth/register', data, {
      headers: httpHeaders,
    });
  }

  confirmUser(utoken: string, code: string, type: string): Observable<any> {
    const data = {
      token: utoken,
      otp: code,
      type: type,
    };

    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    return this.http.post(API + 'stc_verify', data, { headers: httpHeaders });
  }

  login(data) {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');

    return this.http.post<any>(API + 'login', data, {
      headers: httpHeaders,
    });
  }

  update(data): Observable<any> {
    return this.http.put(API + 'login_update', data);
  }

  confirm(uid: string, code: string, utype: string): Observable<any> {
    const data = {
      otp: code,
      type: utype,
      id: uid,
    };

    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    return this.http.post(API + 'login_verify', data, {
      headers: httpHeaders,
    });
  }

  social(authtoken, sprovider) {
    if (GlobalUtils.isBrowser()) {
      const httpHeaders = new HttpHeaders();
      httpHeaders.set('Content-Type', 'application/json');

      const data = {
        type: 'formation',
        provider: sprovider,
        token: authtoken,
      };

      return this.http
        .post<any>(API + 'social_login', data, {
          headers: httpHeaders,
        })
        .pipe(
          map((user) => {
            if (user.code == 'ER1001') {
              return user;
            } else {
              this.setCookie(user.data.token, 'accessToken');
              // store user details and jwt token in local storage to keep user logged in between page refreshes
              localStorage.setItem(token, user.data.token);
              return user;
            }
          })
        );
    }
  }

  social_login(data): Observable<any> {
    return this.http.post(API + 'social_login', data);
  }

  resendOTP(data): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    return this.http.post(API + 'resend_otp', data);
  }

  forgotPassword(data): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    return this.http.post(API + 'forgot_password', data);
  }

  isAuthenticated() { }

  initAuth() { }

  verify(username: string) { }

  devicelist() { }

  // Error
  handleError(error: HttpErrorResponse) {
    let msg = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      msg = error.error.message;
    } else {
      // server-side error
      msg = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    return throwError(msg);
  }

  setCookie(value, name) {
    var date = new Date();
    const MILLISECONDS_IN_A_DAY = 864000;

    if (GlobalUtils.isBrowser()) {
      // Get Unix milliseconds at current time plus 365 days
      date.setTime(+date + 365 * MILLISECONDS_IN_A_DAY); //24 \* 60 \* 60 \* 100
      this.cookieService.set(name, value, date, '/', environment.cookieDomain);
    }
  }

  searchUsername(text, Id?): Observable<any> {
    const urlString = Id
      ? `auth/search_username?text=${encodeURIComponent(text)}&Id=${Id}`
      : `auth/search_username?text=${encodeURIComponent(text)}`;
    return this.http.get(environment.apiEndPoint + urlString);
  }

  getPaymentAccountVerfiedStatus = async () => {
    try {
      const accountPaymentStatus: any = await this.http
        .get(`${API}getPaymentAccountStatus`)
        .toPromise();

      if (
        accountPaymentStatus?.data &&
        accountPaymentStatus?.data.hasOwnProperty('stcAccountVerified') &&
        !accountPaymentStatus?.data?.stcAccountVerified
      ) {
        const result = await this.dialog
          .open(PayPopupComponent, {
            panelClass: 'payment-dialog',
            autoFocus: false,
            restoreFocus: false,
          })
          .afterClosed()
          .toPromise();
        return result;
      } else if (!accountPaymentStatus?.data) {
        this.router.navigateByUrl('/user/phone-login');
      } else {
        return true;
      }
    } catch (error) {
      throw error;
    }
  };

  maxis_auth(auth_code): Observable<any> {
    return this.http.get(
      environment.apiEndPoint + 'auth/visit-epm-portal?code=' + auth_code
    );
  }
  maxis_authMsToken(mstoken): Observable<any> {
    return this.http.get(
      environment.apiEndPoint + 'auth/visit-with-mstoken?mstoken=' + mstoken
    );
  }
  // Registration FormData
  public formDataSubject = new BehaviorSubject(null);
  public formData = this.formDataSubject.asObservable();
}
