import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class ShopService {
  constructor(private http: HttpClient) {}

  topUpList() {
    let url = `${environment.apiEndPoint}home/eshop/getProducts?type=topup`;
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    return this.http.get(url, {
      headers: httpHeaders,
    });
  }

  productDetails(productId) {
    let url = `${environment.apiEndPoint}user/eshop/product-details/${productId}`;
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');

    return this.http.get(url, {
      headers: httpHeaders,
    });
  }

  anonymousDetails(productId):Observable<any>{
    return this.http.get(`${environment.apiEndPoint}user/eshop/anonymous-details/${productId}`);
  }

  confirmPurchase(data, param): Observable<any> {
    let url = `${environment.apiEndPoint}user/eshop/purchase-by-category?`;
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    return this.http.post(url, data, {
      headers: httpHeaders,
      params: param,
    });
  }

  getProductValidateUser(data) {
    let url = `${environment.apiEndPoint}user/eshop/product-validate-user`;
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    return this.http.post(url, data, {
      headers: httpHeaders,
    });
  }

  getProductsByCategory(params: any = null): Observable<any> {
    return this.http.get(
      `${environment.apiEndPoint}user/eshop/get-products-by-category`,
      { params: params }
    );
  }
  getProductCategoryGroups(): Observable<any> {
    return this.http.get(
      `${environment.apiEndPoint}user/eshop/get-product-category-groups`
    );
  }
}
