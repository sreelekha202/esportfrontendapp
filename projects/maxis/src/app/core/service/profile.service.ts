import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';

@Injectable({ providedIn: 'root' })
export class ProfileService {
  constructor(private httpClient: HttpClient) {}

  getMatches(params): Observable<any> {
    return this.httpClient.get(
      `${environment.apiEndPoint}/match/v2/my_matches`,
      { params: params }
    );
  }
  getMyStatistics(id, params): Observable<any> {
    return this.httpClient.get(
      `${environment.apiEndPoint}home/fetchleaderboard/${id}`,
      { params: params }
    );
  }
  getMyJoinedTournament(params): Observable<any> {
    return this.httpClient.get(
      `${environment.apiEndPoint}participant/tournament`,
      { params: params }
    );
  }
  getMyCreatedTournament(params): Observable<any> {
    return this.httpClient.get(
      `${environment.apiEndPoint}tournament/my_tournaments`,
      { params: params }
    );
  }
  getBookmarks(params): Observable<any> {
    return this.httpClient.get(`${environment.apiEndPoint}userpreference`, {
      params: params,
    });
  }
  getPurchaseHistory(params): Observable<any> {
    return this.httpClient.get(
      `${environment.apiEndPoint}transaction/paymenthitstory`,
      { params: params }
    );
  }
  getRewardseHistory(params): Observable<any> {
    return this.httpClient.get(
      `${environment.apiEndPoint}user/reward_transaction`,
      { params: params }
    );
  }
  getMyTeams(params): Observable<any> {
    return this.httpClient.get(`${environment.apiEndPoint}user/my_teams`, {
      params: params,
    });
  }
  getMyInvites(params): Observable<any> {
    return this.httpClient.get(`${environment.apiEndPoint}user/my_invites`, {
      params: params,
    });
  }

  getMyTeam(params): Observable<any> {
    const encodedPagination = encodeURIComponent(params['pagination']);
    const url = `${environment.apiEndPoint}user/my_teams?pagination=${encodedPagination}`;
    return this.httpClient.get(url);
  }
  deleteTeam(teamId: number): Observable<void> {
    return this.httpClient.delete<void>(`url/${teamId}`);
  }

  getMyInvite(params): Observable<any> {
    const encodedPagination = encodeURIComponent(params['pagination']);
    const url = `${environment.apiEndPoint}user/my_invites?pagination=${encodedPagination}`;
    return this.httpClient.get(url);
  }

  getplayers(id): Observable<any> {
    return this.httpClient.get(`${environment.apiEndPoint}home/team/${id}`);
  }

  accept_reject(data) {
    return this.httpClient.post(
      `${environment.apiEndPoint}user/update_invites`,
      data
    );
  }

  jointournament(data) {
    return this.httpClient.post(`${environment.apiEndPoint}participant`, data);
  }

  ckeckuserid(data, id) {
    const url = `${environment.apiEndPoint}participant/search?field=inGamerUserId&text=${data}&tournamentId=${id}`;
    return this.httpClient.get(url);
  }

  // Content
  getContent(params): Observable<any> {
    return this.httpClient.get(`${environment.apiEndPoint}article`, {
      params: params,
    });
  }
  getAllCategories(): Observable<any> {
    return this.httpClient.get(
      `${environment.apiEndPoint}option/all-categories`
    );
  }
  getAllTags(): Observable<any> {
    return this.httpClient.get(`${environment.apiEndPoint}option/all-tags`);
  }
  getAllGame(): Observable<any> {
    return this.httpClient.get(`${environment.apiEndPoint}game`);
  }
  getAllGenres(): Observable<any> {
    return this.httpClient.get(`${environment.apiEndPoint}option/all-genres`);
  }
  saveContent(data): Observable<any> {
    return this.httpClient.post(`${environment.apiEndPoint}article`, data);
  }
  updateContent(data, id): Observable<any> {
    return this.httpClient.put(`${environment.apiEndPoint}article/${id}`, data);
  }
  getGameDetails(id): Observable<any> {
    return this.httpClient.get(`${environment.apiEndPoint}tournament/${id}`);
  }
  // manage Tournamnet
  getUserReport(t_id): Observable<any> {
    return this.httpClient.get(
      `${environment.apiEndPoint}tournament/reports_by_tournament/${t_id}`
    );
  }
  getParticipant(params): Observable<any> {
    return this.httpClient.get(`${environment.apiEndPoint}participant/v2`, {
      params: params,
    });
  }

  public getLatestArticle(api, params) {
    /**
     * @param
     * articleStatus: 'publish',
     * page: 1,
     * limit: 8,
     * sort: 'createdOn', or sort: '-createdOn'
     * gameDetails: first_game_id || second_game_id,
     */
    return this.httpClient.get(`${api}home/articles`, { params: params });
  }
}
