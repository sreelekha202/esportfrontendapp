import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";

import { SubscribableOrPromise } from "rxjs";

import { environment } from "../../../environments/environment";
import { IBracket } from "../../shared/models";

@Injectable({
  providedIn: "root",
})
export class BracketService {
  bracketData;
  mobileWebViewMetaData;

  constructor(private http: HttpClient) {}

  fetchAllBracket(query): SubscribableOrPromise<any> {
    return this.http
      .get(`${environment.apiEndPoint}bracket${query}`)
      .toPromise();
  }

  getBracketByID(id: string): SubscribableOrPromise<any> {
    return this.http.get(`${environment.apiEndPoint}bracket/${id}`).toPromise();
  }

  saveBracket(bracket: IBracket): SubscribableOrPromise<any> {
    return this.http
      .post(`${environment.apiEndPoint}bracket`, bracket)
      .toPromise();
  }

  updateBracket(id: string, bracket: IBracket): SubscribableOrPromise<any> {
    return this.http
      .patch(`${environment.apiEndPoint}bracket/${id}`, bracket)
      .toPromise();
  }

  deleteBracket(id: string): SubscribableOrPromise<any> {
    return this.http
      .delete(`${environment.apiEndPoint}bracket/${id}`)
      .toPromise();
  }

  generateBracket(payload): SubscribableOrPromise<any> {
    return this.http
      .post(`${environment.apiEndPoint}match`, payload)
      .toPromise();
  }

  generateBracket1(payload){
    return this.http.post(`${environment.apiEndPoint}match`, payload);
  }

  fetchAllMatches(query): SubscribableOrPromise<any> {
    return this.http.get(`${environment.apiEndPoint}match${query}`).toPromise();
  }

  saveGeneratedBracket(payload): SubscribableOrPromise<any> {
    return this.http
      .post(`${environment.apiEndPoint}match/save`, payload)
      .toPromise();
  }

  fetchDistinctValue(query, field): SubscribableOrPromise<any> {
    return this.http
      .get(
        `${environment.apiEndPoint}match/distict-round${query}&field=${field}`
      )
      .toPromise();
  }

  updateMatch(queryParam, payload): SubscribableOrPromise<any> {
    return this.http
      .patch(`${environment.apiEndPoint}match/update${queryParam}`, payload)
      .toPromise();
  }

  updateParticipantScore(queryParam, payload): SubscribableOrPromise<any> {
    return this.http
      .patch(
        `${environment.apiEndPoint}match/pariticipant-score${queryParam}`,
        payload
      )
      .toPromise();
  }

  battleRoyalePlayerStanding(queryParam): SubscribableOrPromise<any> {
    return this.http
      .get(
        `${environment.apiEndPoint}match/battle-royale-standing${queryParam}`
      )
      .toPromise();
  }

  fetchStanding(queryParam): SubscribableOrPromise<any> {
    return this.http
      .get(`${environment.apiEndPoint}match/player-standing${queryParam}`)
      .toPromise();
  }

  fetchWinnerList(tId): SubscribableOrPromise<any> {
    return this.http
      .get(`${environment.apiEndPoint}match/champions?tId=${tId}`)
      .toPromise();
  }

  // For Single, Double and Round Robin
  assembleStructure(list) {
    const obj = {};
    const insertAt = (array, index, ele) => {
      array[index] = ele;
      return array;
    };

    for (const l of list) {
      obj[l.currentMatch.round] = obj[l.currentMatch.round] || [];
      if (l.currentMatch.hasOwnProperty("subround")) {
        obj[l.currentMatch.round] = obj[l.currentMatch.round] || {};
        obj[l.currentMatch.round].subround =
          obj[l.currentMatch.round].subround || {};
        obj[l.currentMatch.round].subround[l.currentMatch.subround] =
          obj[l.currentMatch.round].subround[l.currentMatch.subround] || [];
        obj[l.currentMatch.round].subround[l.currentMatch.subround] = insertAt(
          obj[l.currentMatch.round].subround[l.currentMatch.subround],
          l.currentMatch.matchPosition,
          l
        );
      } else {
        obj[l.currentMatch.round] = insertAt(
          obj[l.currentMatch.round],
          l.currentMatch.matchPosition,
          l
        );
      }
    }
    return obj;
  }

  // For Multi Stage Battle Royale
  assembleMultiStageBattleStructure(list) {
    if (list) {
      const obj = [];

      for (const l of list) {
        obj[l.currentMatch.stage - 1] = obj[l.currentMatch.stage - 1] || {
          id: l.currentMatch.stage,
          isLoaded: true,
          group: [],
        };

        obj[l.currentMatch.stage - 1].group[l.currentMatch.group - 1] = obj[
          l.currentMatch.stage - 1
        ]?.group[l.currentMatch.group - 1] || {
          id: l.currentMatch.group,
          isLoaded: true,
          round: [],
        };

        obj[l.currentMatch.stage - 1].group[l.currentMatch.group - 1].round[
          l.currentMatch.matchPosition
        ] =
          obj[l.currentMatch.stage - 1]?.group[l.currentMatch.group - 1]?.round[
            l.currentMatch.matchPosition
          ] || l;
      }
      return obj;
    } else {
      return [];
    }
  }

  // For Multi Stage Round Robin
  assembleMultiRoundRobinStructure(list) {
    if (list) {
      const obj = [];
      for (const l of list) {
        obj[l.currentMatch.stage - 1] = obj[l.currentMatch.stage - 1] || {
          id: l.currentMatch.stage,
          isLoaded: true,
          group: [],
        };

        obj[l.currentMatch.stage - 1].group[l.currentMatch.group - 1] = obj[
          l.currentMatch.stage - 1
        ]?.group[l.currentMatch.group - 1] || {
          id: l.currentMatch.group,
          isLoaded: true,
          round: [],
        };

        obj[l.currentMatch.stage - 1].group[l.currentMatch.group - 1].round[
          l.currentMatch.round - 1
        ] = obj[l.currentMatch.stage - 1]?.group[l.currentMatch.group - 1]
          ?.round[l.currentMatch.round - 1] || {
          id: l.currentMatch.round,
          match: [],
        };

        obj[l.currentMatch.stage - 1].group[l.currentMatch.group - 1].round[
          l.currentMatch.round - 1
        ].match[l.currentMatch.matchPosition] = l;
      }
      return obj;
    } else {
      return [];
    }
  }

  // For Multi Stage Round Robin
  assembleMultiStageRoundRobinStructure(list) {
    const obj = {};
    const insertAt = (array, index, ele) => {
      array[index] = ele;
      return array;
    };
    for (const l of list) {
      obj[l.currentMatch.stage] = obj[l.currentMatch.stage] || { group: {} };

      obj[l.currentMatch.stage].group[l.currentMatch.group] = obj[
        l.currentMatch.stage
      ]?.group[l.currentMatch.group] || { round: {} };

      obj[l.currentMatch.stage].group[l.currentMatch.group].round[
        l.currentMatch.round
      ] =
        obj[l.currentMatch.stage].group[l.currentMatch.group].round[
          l.currentMatch.round
        ] || [];
      if (l.currentMatch.hasOwnProperty("subround")) {
        obj[l.currentMatch.stage].group[l.currentMatch.group].round[
          l.currentMatch.round
        ] =
          obj[l.currentMatch.stage].group[l.currentMatch.group].round[
            l.currentMatch.round
          ] || {};
        obj[l.currentMatch.stage].group[l.currentMatch.group].round[
          l.currentMatch.round
        ].subround =
          obj[l.currentMatch.stage].group[l.currentMatch.group].round[
            l.currentMatch.round
          ].subround || {};
        obj[l.currentMatch.stage].group[l.currentMatch.group].round[
          l.currentMatch.round
        ].subround[l.currentMatch.subround] =
          obj[l.currentMatch.stage].group[l.currentMatch.group].round[
            l.currentMatch.round
          ].subround[l.currentMatch.subround] || [];
        obj[l.currentMatch.stage].group[l.currentMatch.group].round[
          l.currentMatch.round
        ].subround[l.currentMatch.subround] = insertAt(
          obj[l.currentMatch.stage].group[l.currentMatch.group].round[
            l.currentMatch.round
          ].subround[l.currentMatch.subround],
          l.currentMatch.matchPosition,
          l
        );
      } else {
        obj[l.currentMatch.stage].group[l.currentMatch.group].round[
          l.currentMatch.round
        ] = insertAt(
          obj[l?.currentMatch?.stage].group[l?.currentMatch?.group].round[
            l?.currentMatch?.round
          ],
          l?.currentMatch?.matchPosition,
          l
        );
      }
    }
    return obj;
  }

  setBracketData(data: any) {
    this.bracketData = data;
  }

  getBracketData() {
    return this.bracketData;
  }

  setWebViewMeta(data) {
    this.mobileWebViewMetaData = data;
  }

  getWebViewMeta() {
    return this.mobileWebViewMetaData;
  }
}
