import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Component, Inject, OnInit } from '@angular/core';
import { AppHtmlProfileRoutes } from '../../app-routing.model';

@Component({
  selector: 'app-pay-popup',
  templateUrl: './pay-popup.component.html',
  styleUrls: ['./pay-popup.component.scss'],
})
export class PayPopupComponent implements OnInit {
  AppHtmlProfileRoutes = AppHtmlProfileRoutes;
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<any>
  ) { }

  ngOnInit(): void { }

  onClose(): void {
    this.dialogRef.close(false);
  }
}
