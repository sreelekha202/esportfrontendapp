import { TranslateService } from '@ngx-translate/core';
import { ActivatedRoute, Router } from '@angular/router';
import {
  AfterViewInit,
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  Output,
} from '@angular/core';
import { AppHtmlRoutes } from '../../app-routing.model';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';
import { EsportsToastService } from 'esports';

@AutoUnsubscribe()
@Component({
  selector: 'app-search-input',
  templateUrl: './search-input.component.html',
  styleUrls: ['./search-input.component.scss'],
})
export class SearchInputComponent implements AfterViewInit, OnDestroy {
  AppHtmlRoutes = AppHtmlRoutes;

  @Input() text: string = '';
  @Output() onTextChange = new EventEmitter();

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    public toastService: EsportsToastService,
    public translate: TranslateService
  ) {}

  ngAfterViewInit(): void {}

  ngOnDestroy(): void {}

  onSearch(): void {
    this.emitText();
    this.router.navigate([AppHtmlRoutes.search], {
      relativeTo: this.activatedRoute,
      queryParams: { text: this.text },
      queryParamsHandling: 'merge', // remove to replace all query params by provided
    });
  }

  onSearchReset() {
    this.text = '';
    this.onSearch();
  }
  emitText = () => this.onTextChange.emit(this.text);
}
