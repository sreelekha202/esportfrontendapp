import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { EsportsLanguageService } from 'esports';
import { AppHtmlRoutes } from '../../app-routing.model';

@Component({
  selector: 'app-article-item',
  templateUrl: './article-item.component.html',
  styleUrls: ['./article-item.component.scss'],
})
export class ArticleItemComponent implements OnInit {
  @Output() onTop = new EventEmitter();
  @Input() data;

  AppHtmlRoutes = AppHtmlRoutes;

  constructor(public languageService: EsportsLanguageService) {}

  ngOnInit(): void {}
}
