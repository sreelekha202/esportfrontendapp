import { environment } from "../../../environments/environment";
import { GlobalUtils } from "esports";
import { UserService } from "../service";

const token = environment.currentToken;
const api = environment.apiEndPoint;

export function appInitializer(userService: UserService) {
  return () =>
    new Promise((resolve) => {
      // attempt to refresh token on app start up to auto authenticate
      if (
        GlobalUtils.isBrowser() &&
        localStorage.getItem(environment.currentToken)
      ) {
        const jwtToken = JSON.parse(
          atob(localStorage.getItem(token).split(".")[1])
        );

        const expires: Date = new Date(jwtToken.exp * 1000);

        if (expires > new Date()) {
          userService.startRefreshTokenTimer(api, token);
          userService.getAllCountries().subscribe().add(resolve);
        } else {
          userService
            .refreshToken(api, token)
            .subscribe(
              (data) => { },
              (error) => {
                userService.logout(api, token);
              }
            )
            .add(resolve);
        }
      } else {
        userService.getAllCountries().subscribe().add(resolve);
      }
    });
}
