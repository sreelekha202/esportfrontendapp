import {
  Component,
  ElementRef,
  EventEmitter,
  Inject,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { UserService } from '../../core/service';
import { fromEvent, Subscription } from 'rxjs';
import { environment } from '../../../environments/environment';
import { AppHtmlProfileRoutes, AppHtmlRoutes } from '../../app-routing.model';
import { TranslateService } from '@ngx-translate/core';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';
import {
  faBars,
  faTimes,
  faSearch,
  faBell,
} from '@fortawesome/free-solid-svg-icons';
import { MatDialog } from '@angular/material/dialog';
import { SettingsLightboxComponent } from '../../modules/settings-lightbox/settings-lightbox.component';
declare var $: any;
const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;
const token = environment.currentToken;
import { WalletService } from '../../core/service/wallet.service';
import {
  EsportsChatService,
  EsportsChatSidenavService,
  EsportsConstantsService,
  EsportsLanguageService,
  EsportsToastService,
  GlobalUtils,
  EsportsGtmService,
  EventProperties,
  SuperProperties,
} from 'esports';
import { ReloadComponent } from '../../modules/reload/reload.component';
import { ActivatedRoute, Router } from '@angular/router';

@AutoUnsubscribe()
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit, OnDestroy {
  activeLang = this.ConstantsService?.defaultLangCode;
  AppHtmlProfileRoutes = AppHtmlProfileRoutes;
  AppHtmlRoutes = AppHtmlRoutes;
  collapsed: boolean = true;
  isAdmin: boolean = false;
  box: boolean = false;
  isUserLoggedIn: boolean = false;
  mobMenuOpened: boolean = false;
  showLoader: boolean = false;
  currentUser: any;
  matchcount: number;
  currentUserId: any;
  AppLanguage = [];
  faBell = faBell;
  faSearch = faSearch;
  faBars = faBars;
  faTimes = faTimes;
  datafortopupmoney: any = [];
  datafortopupmethod: any = [];
  amount = '';
  expdate = '';
  coins = '';
  latesttransactions: any = [];
  firtstimeflag: boolean = false;
  text: string = '';
  addNavClass: boolean = false;
  hamburgerActive: boolean = false;
  userLinks = [
    {
      title: 'My Account',
      icon: 'Vector',
      url: AppHtmlProfileRoutes.dashboard,
    },
    // {
    //   title: 'My Tournaments',
    //   icon: 'Vector_2',
    //   url: AppHtmlRoutes.tournament,
    // },
    // {
    //   title: 'My Teams',
    //   icon: 'Vector_3',
    //   url: AppHtmlProfileRoutes.myTeams,
    // },
    // {
    //   title: 'My Stats',
    //   icon: 'Vector_4',
    //   url: AppHtmlProfileRoutes.myStats,
    // }
  ];

  userSubscription: Subscription;
  private subscriptions: StringMap<Subscription> = {};
  sub1: Subscription;
  isAuthor: boolean = false;
  @Output() viewed = new EventEmitter<Boolean>();
  constructor(
    @Inject(DOCUMENT) private document: Document,
    private chatService: EsportsChatService,
    private chatSidenavService: EsportsChatSidenavService,
    private elementRef: ElementRef,
    private languageService: EsportsLanguageService,
    // private userService: UserService,
    private userService: UserService,
    public toastService: EsportsToastService,
    public translate: TranslateService,
    private walletService: WalletService,
    public matDialog: MatDialog,
    private router: Router,
    public ConstantsService: EsportsConstantsService,
    private activatedRoute: ActivatedRoute,
    private gtmService: EsportsGtmService
  ) {
    this.userService.congratulationPopup.subscribe((res) => {
      this.box = res;
    });
  }

  ngOnInit(): void {
    if (GlobalUtils.isBrowser()) {
      this.activeLang = this.translate.currentLang;
      this.AppLanguage = this.ConstantsService?.language;
      this.matchcount = 0;
      this.showLoader = true;
      this.userSubscription = this.userService.currentUser.subscribe(
        (data) => {
          if (data) {
            this.currentUser = data;
            this.isUserLoggedIn = !!this.currentUser;
            this.showLoader = false;
            this.currentUserId = this.currentUser._id;
            // this.isAdmin =
            //   data?.accountType === EsportsConstantsService.AccountType.Admin;
            if (this.currentUser.hasOwnProperty('isAuthor')) {
              this.currentUser.isAuthor == 0 ? (this.isAuthor = true) : '';
            }
            // const accessToken = localStorage.getItem(environment.currentToken);
            this.chatService?.login.subscribe((data) => {
              // if (data.accessToken != 'undefined' && data.accessToken != null) {
              this.chatService?.connectUser();
              this.chatService.unreadCount.subscribe((unreadCount) => {
                this.matchcount = unreadCount;
              });
              this.chatService?.getUnReadCount().subscribe((res: any) => {});
              this.walletService.loadWalletDetails();
              this.subscriptions.onLoaded =
                this.walletService.subscribeToLoadedRecords((data: any) => {
                  this.firtstimeflag = true;
                  this.amount = data.data.currentBalanceCoins;
                  this.expdate = data.data.expiryDate;
                  this.coins = data.data.expiryCoins;
                  if (this.amount == '0') {
                    this.firtstimeflag = false;
                  }
                });
              this.walletService.getWallettopupmethods().subscribe(
                (data: any) => {
                  this.datafortopupmethod = data.data;
                  for (let data of this.datafortopupmethod) {
                    data.selectflag = false;
                  }
                },
                (error) => {}
              );
              this.walletService.getWallettopupamount().subscribe(
                (data: any) => {
                  this.datafortopupmoney = data.data;
                  for (let data of this.datafortopupmoney) {
                    data.selectflag = false;
                  }
                },
                (error) => {}
              );
            });
          } else {
            this.userService.getAuthenticatedUser(API, TOKEN);
          }
        },
        (error) => {
          this.showLoader = false;
          this.toastService.showError(error.message);
        }
      );
    } else {
    }
    this.onHeaderScroll();
  }
  ngAfterViewInit(): void {
    this.viewed.emit(true);
    if (GlobalUtils.isBrowser()) {
      $('.hamburgercls').on('click', function () {
        this.classList.toggle('active');
        document.querySelector('.mobile-menu').classList.toggle('active');
        $('#nav-icon').toggleClass('open');
      });

      $('.closenav').on('click', function () {
        $('.mobile-menu').removeClass('active');
        $('.hamburgercls').removeClass('active');
        $('#nav-icon').removeClass('open');
      });

      $('.linkroute').on('click', function () {
        $('.mobile-menu').removeClass('active');
        $('.hamburgercls').removeClass('active');
        $('#nav-icon').removeClass('open');
      });
    }
  }
  adminLink() {
    let eventProperties: EventProperties = {};
    eventProperties['tabValue'] = "Transactions";
    this.pushGTMTags('select_tab', eventProperties);
    if (GlobalUtils.isBrowser()) {
      $('.mobile-menu').removeClass('active');
      $('.hamburgercls').removeClass('active');
      $('#nav-icon').removeClass('open');
    }
  }
  ngOnDestroy() {
    if (this.userSubscription) this.userSubscription?.unsubscribe();
    if (this.currentUserId) this.chatService?.disconnectUser();
  }
  closenav() {
    this.addNavClass = true;
  }
  onChatSidenavToggle(): void {
    this.chatSidenavService.toggle();
    if (this.currentUserId) {
      this.chatService?.getAllMatches(this.currentUserId, 1, 30);
    }
    if (this.chatSidenavService.checkOpen()) {
      if (this.chatService.getChatStatus()) {
        this.chatService.setChatStatus(false);
      }
    } else {
      if (this.chatService.getChatStatus()) {
        this.chatService.setChatStatus(false);
      }
    }
  }
  onLogin = () => window.location.replace(`${environment.maxisLogin}`);

  onLogOut(): void {
    if (GlobalUtils.isBrowser()) {
      let eventProperties: EventProperties = {};
      this.pushGTMTags('logout', eventProperties);
      localStorage.removeItem(token);
      localStorage.removeItem('refreshToken');
      window.location.href = environment.maxisLogout;
      // The difference between href and replace, is that replace() removes the URL of
      // the current document from the document history,
      // meaning that it is not possible to use the "back"
      // button to navigate back to the original document.
      // window.location.replace(`${environment.maxisLogout}`);
    }
  }

  onLanguageChange(lang): void {
    if (GlobalUtils.isBrowser()) {
      localStorage.setItem('currentLanguage', lang);
      this.activeLang = lang;
      this.translate.use(lang);
      this.languageService.setLanguage(lang);
    }
  }

  private onHeaderScroll(): void {
    if (GlobalUtils.isBrowser()) {
      const scrollingContent = document.querySelector('mat-sidenav-content');

      if (scrollingContent) {
        const header = this.elementRef.nativeElement;

        this.sub1 = fromEvent(scrollingContent, 'scroll').subscribe(
          (e: Event) => {
            if ((e.target as Element).scrollTop > header.clientHeight) {
              header.classList.add('is-scrolling');
            } else {
              header.classList.remove('is-scrolling');
            }
          }
        );
      }
    }
  }

  onScrollBody() {
    const hiddenOrVisible = this.mobMenuOpened ? 'hidden' : 'visible';
    this.document.body.style.overflow = hiddenOrVisible;
  }

  openSettingsLightbox($event) {
    let selectedTab = 0;
    const data = { selectedTab };
    this.matDialog.open(SettingsLightboxComponent, {
      panelClass: 'account_settings',
      // hasBackdrop: false,
      disableClose: true,
      data,
      position: {
        top: '34px',
      },
    });
    this.box ? this.close() : '';
  }

  close() {
    this.userService.congratulationPopupSubject.next(false);
    this.box = false;
  }
  openReloadPopup() {
    let eventProperties: EventProperties = {};
    this.pushGTMTags('select_my_balance', eventProperties);
    this.matDialog.open(ReloadComponent, {
      disableClose: true,
      panelClass: 'reload_popup',
      hasBackdrop: true,
    });
    this.close();
  }
  onSearch(): void {
    let eventProperties: EventProperties = {};
    eventProperties['searchTerms'] = this.text;
    this.pushGTMTags('search', eventProperties);
    if (GlobalUtils.isBrowser()) {
      let a = document.getElementById('dsgfdjkf').click();
      this.router.navigate([AppHtmlRoutes.search], {
        relativeTo: this.activatedRoute,
        queryParams: { text: this.text },
        queryParamsHandling: 'merge', // remove to replace all query params by provided
      });
    }
  }

  pushGTMTags(eventName: string, eventProperties: any) {
    let superProperties: SuperProperties = {};
    if (this.currentUser) {
      superProperties = this.gtmService.assignLoggedInUsedData(
        this.currentUser
      );
    }

    this.gtmService.gtmEventWithSuperProp({
      eventName: eventName,
      superProps: superProperties,
      eventProps: eventProperties,
    });
  }
}
