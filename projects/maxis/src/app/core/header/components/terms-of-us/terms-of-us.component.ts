import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-terms-of-us',
  templateUrl: './terms-of-us.component.html',
  styleUrls: ['./terms-of-us.component.scss'],
})
export class TermsOfUsComponent implements OnInit {
  constructor(
    private location: Location,
    public matDialog: MatDialog,
    public dialogRef: MatDialogRef<TermsOfUsComponent>
  ) {}
  demo = '';
  ngOnInit(): void {}

  goBack() {
    this.location.back();
  }
  onClose() {
    this.dialogRef.close();
  }
}
