import { UserService } from './../../../service/user.service';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialogRef } from '@angular/material/dialog';
import { MatDialog } from '@angular/material/dialog';
import { EsportsGtmService, EsportsToastService, EventProperties, SuperProperties } from 'esports';
import { environment } from '../../../../../environments/environment';
import { AppHtmlRoutes } from '../../../../app-routing.model';
import { TermsOfUsComponent } from '../terms-of-us/terms-of-us.component';
import { AuthServices } from '../../../../core/service';
const TOKEN = environment.currentToken;
const API = environment.apiEndPoint;

@Component({
  selector: 'app-user-popup',
  templateUrl: './user-popup.component.html',
  styleUrls: ['./user-popup.component.scss'],
})
export class UserPopupComponent implements OnInit {
  AppHtmlRoutes = AppHtmlRoutes;
  addForm: FormGroup;
  isExist = false;
  timeoutId = null;
  currentUser: any;

  constructor(
    public dialogRef: MatDialogRef<UserPopupComponent>,
    private fb: FormBuilder,
    private router: Router,
    private userService: UserService,
    private toastService: EsportsToastService,
    private authServices: AuthServices,
    public matDialog: MatDialog,
    public gtmService: EsportsGtmService,
  ) {
    this.addForm = this.fb.group({
      isAccountActivated: [true],
      privacyPolicy: [
        false,
        Validators.compose([Validators.required, this.privacyPolicyValidator]),
      ],
      username: [
        '',
        Validators.compose([
          Validators.required,
          Validators.minLength(3),
          Validators.pattern(/^[a-zA-Z0-9_]+$/),
        ]),
      ],
    });
  }
  @ViewChild('secondDialog', { static: true }) secondDialog: TemplateRef<any>;
  demo = '';
  ngOnInit(): void {
    this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.currentUser = data;
      }
    });
  }

  privacyPolicyValidator(control: FormControl) {
    let temp: boolean = control.value;
    if (!temp) {
      return {
        privacyPolicy: {
          privacyPolicy: temp,
        },
      };
    }
    return null;
  }

  onClose(): void {
    this.dialogRef.close();
  }

  onCloseAndRedirect(): void {
    this.onClose();
    this.router.navigateByUrl('/home');
  }
  termsofservice() {
    this.matDialog.open(TermsOfUsComponent, {
      disableClose: true,
      panelClass: "terms_conditions"
    });
  }

  onSubmit(formData: any) {
    let eventProperties: EventProperties = {};
    this.pushGTMTags('create_account', eventProperties);
    if (this.addForm.valid && this.addForm.value.privacyPolicy) {
      this.userService.updateUserPopup(this.addForm.value).subscribe(
        (res) => {
          this.onClose();
          let eventProperties: EventProperties = {};
          this.pushGTMTags('account_created', eventProperties);
          this.userService.congratulationPopupSubject.next(true);
          this.userService.refreshCurrentUser(API, TOKEN);
        },
        (err) => {
          this.toastService.showError(err.error.message);
        }
      );
    }
  }
  isUniqueName = async (name) => {
    this.demo = name;
    try {
      const isAvailable = async () => {
        try {
          if (this.currentUser.username != name) {
            const response = await this.authServices
              .searchUsername(name)
              .toPromise();
            this.isExist = response.data?.isExist;
            if (this.isExist) {
              this.addForm.controls['username'].setErrors({
                incorrect: true,
              });
            } else {
              this.addForm.controls['username'].setErrors(null);
              this.addForm.controls['username'].setValidators([
                Validators.required,
                Validators.minLength(3),
                Validators.maxLength(25),
                Validators.pattern(/^[a-zA-Z0-9_]+$/),
              ]);
              this.addForm.controls['username'].updateValueAndValidity();
            }
          }
        } catch (error) {
          throw error;
        } finally {
          clearTimeout(this.timeoutId);
        }
      };
      if (!this.timeoutId) {
        this.timeoutId = setTimeout(isAvailable, 500);
      } else {
        clearTimeout(this.timeoutId);
        this.timeoutId = setTimeout(isAvailable, 500);
      }
    } catch (error) {
      this.toastService.showError(error?.error?.message || error?.message);
    }
  };

  pushGTMTags(eventName: string, eventProperties: any) {
    let superProperties: SuperProperties = {};
    if (this.currentUser) {
      superProperties = this.gtmService.assignLoggedInUsedData(
        this.currentUser
      );
    }
    this.gtmService.gtmEventWithSuperProp({
      eventName: eventName,
      superProps: superProperties,
      eventProps: eventProperties,
    });
  }
}
