import { Component, OnInit, ViewChild } from '@angular/core';
import { UserService } from '../../core/service';
import { AppHtmlRoutes } from '../../app-routing.model';
import { environment } from '../../../environments/environment';
import { MatDialog } from '@angular/material/dialog';
import { SettingsLightboxComponent } from '../../modules/settings-lightbox/settings-lightbox.component';
import { ReloadComponent } from '../../modules/reload/reload.component';
import { Subscription } from 'rxjs';
import { EsportsToastService } from 'esports';
import { TranslateService } from '@ngx-translate/core';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';
import { Router } from '@angular/router';
import { MatAccordion } from '@angular/material/expansion';

@AutoUnsubscribe()
@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
})
export class FooterComponent implements OnInit {
  @ViewChild(MatAccordion) accordion: MatAccordion;
  AppHtmlRoutes = AppHtmlRoutes;
  userSubscription: Subscription;
  buildConfig = environment.buildConfig || 'N/A';
  box: boolean = false;
  panelOpenState = false;
  socials = [
    {
      link: 'https://twitter.com/paidia.gg',
      icon: 'assets/icons/socials/twitter.svg',
    },
    {
      link: 'https://www.facebook.com/paidia.gg',
      icon: 'assets/icons/socials/facebook.svg',
    },
    {
      link: 'https://www.instagram.com/paidia.gg/',
      icon: 'assets/icons/socials/instagram.svg',
    },
    {
      link: 'https://www.linkedin.com/company/paidia.gg',
      icon: 'assets/icons/socials/linkedin.svg',
    },
  ];
  currentUser: any;

  constructor(
    public matDialog: MatDialog,
    private userService: UserService,
    private toastService: EsportsToastService,
    private translateService: TranslateService,
    public router: Router
  ) {
    this.userSubscription = this.userService.currentUser.subscribe((data) => {
      if (data) {
        this.currentUser = data;
      }
    });
  }

  ngOnInit(): void {}

  openSettingsLightbox(selectedTab = 0) {
    const data = { selectedTab };
    if (this.currentUser && this.currentUser._id) {
      this.matDialog.open(SettingsLightboxComponent, {
        panelClass: 'account_settings',
        disableClose: true,
        data,
        position: {
          top: '34px',
        },
      });
    } else {
      this.toastService.showInfo(
        this.translateService.instant('BASIC_INFO_POPUP.PLEASE_LOGIN')
      );
    }
  }
  openRelod() {
    if (this.currentUser && this.currentUser._id) {
      this.matDialog.open(ReloadComponent, {
        disableClose: true,
        panelClass: 'reload_popup',
      });
    } else {
      this.toastService.showInfo(
        this.translateService.instant('BASIC_INFO_POPUP.PLEASE_LOGIN')
      );
    }
  }
  close() {
    this.userService.congratulationPopupSubject.next(false);
    this.box = false;
  }
  ngOnDestroy() {
    if (this.userSubscription) this.userSubscription?.unsubscribe();
  }
  ngAfterViewInit(): void {
    this.router.events.subscribe((evt) => {
      this.accordion.closeAll();
    });
  }
}
