import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NotfoundComponent } from './modules/not-found/not-found.component';
import { AuthGuard } from './shared/guard/auth.guard';

const routes: Routes = [
  {
    path: 'home',
    data: {
      title: 'Home',
      tags: [
        {
          name: 'title',
          content: 'Home | Geng Gamer',
        },
        {
          name: 'description',
          content: 'Buy in-game currency for your favourite mobile games and more with Geng Gamer. Reload, top up, and enjoy exclusive offers right here today.',
        },
      ]
    },
    loadChildren: () =>
      import('./modules/shop/shop.module').then((m) => m.ShopModule),
  },
  {
    path: 'shop',
    data: {
      title: 'Shop',
      tags: [
        {
          name: 'title',
          content: 'Shop | Geng Gamer',
        },
        {
          name: 'description',
          content: 'Buy in-game currency for your favourite mobile games and more with Geng Gamer. Reload, top up, and enjoy exclusive offers right here today.',
        },
      ]
    },
    loadChildren: () =>
      import('./modules/shop/shop.module').then((m) => m.ShopModule),
  },
  {
    path: 'updates',
    data: {
      title: 'Updates',
      tags: [
        {
          name: 'title',
          content: 'Updates | Geng Gamer',
        }
      ]
    },
    loadChildren: () =>
      import('./modules/news/news.module').then((m) => m.NewsModule),
  },
  {
    path: 'updates/create',
    data: {
      isRootPage: true,
      title: 'Updates',
      tags: [
        {
          name: 'title',
          content: 'Updates | Geng Gamer',
        }
      ]
    },
    canActivate: [AuthGuard],
    loadChildren: () =>
      import('./modules/news/add-content/add-content.module').then(
        (m) => m.AddContentModule
      ),
  },
  {
    path: 'updates/edit/:id',
    data: {
      isRootPage: true,
      title: 'Updates',
      tags: [
        {
          name: 'title',
          content: 'Updates | Geng Gamer',
        }
      ]
    },
    canActivate: [AuthGuard],
    loadChildren: () =>
      import('./modules/news/edit-content/edit-content.module').then(
        (m) => m.EditContentModule
      ),
  },
  {
    path: 'search',
    loadChildren: () =>
      import('./modules/search/search.module').then((m) => m.SearchModule),
    data: {
      title: 'Search',
      tags: [
        {
          name: 'title',
          content: 'Search | Geng Gamer',
        },
        {
          name: 'description',
          content: 'Search | Geng Gamer',
        },
      ],
    },
  },
  {
    path: 'privacy-policy',
    loadChildren: () =>
      import('./modules/privacy-policy/privacy-policy.module').then(
        (m) => m.PrivacyPolicyModule
      ),
    data: {
      title: 'Privacy Policy',
      tags: [
        {
          name: 'title',
          content: 'Privacy Policy | Geng Gamer',
        },
        {
          name: 'description',
          content: 'Read more about the Privacy Policy of Geng Gamer here.',
        },
      ],
    },
  },
  {
    path: 'terms-of-use',
    loadChildren: () =>
      import('./modules/terms-of-use/terms-of-use.module').then(
        (m) => m.TermsOfUseModule
      ),
    data: {
      title: 'Terms of Use',
      tags: [
        {
          name: 'title',
          content: 'Terms of Use | Geng Gamer',
        },
        {
          name: 'description',
          content: 'Read more about the Terms of Use of Geng Gamer here.',
        },
      ],
    },
  },
  {
    path: 'terms-conditions',
    loadChildren: () =>
      import('./modules/terms-conditions/terms-conditions.module').then(
        (m) => m.TermsConditionsModule
      ),
    data: {
      title: 'Terms & Conditions',
      tags: [
        {
          name: 'title',
          content: 'Terms & Conditions | Geng Gamer',
        },
        {
          name: 'description',
          content: 'Read more about the Terms & Conditions of Geng Gamer here.',
        },
      ],
    },
  },
  {
    path: 'faq',
    loadChildren: () =>
      import('./modules/faq/faq.module').then((m) => m.FaqModule),
    data: {
      title: 'FAQs',
      tags: [
        {
          name: 'title',
          content: 'Frequently Asked Questions | Geng Gamer',
        },
        {
          name: 'description',
          content:
            'Find out the answers to frequently asked questions about reload, top up, and more in Geng Gamer.',
        },
      ],
    },
  },
  {
    path: 'contact-us',
    loadChildren: () =>
      import('./modules/contact-us/contact-us.module').then(
        (m) => m.ContactUsModule
      ),
    data: {
      title: 'Contact Us',
      tags: [
        {
          name: 'title',
          content: 'Contact Us | Geng Gamer',
        },
        {
          name: 'description',
          content:
            'Got a question? Drop us your enquiry or feedback in this Contact Us page.',
        },
      ],
    },
  },
  {
    path: 'profile',
    canActivate: [AuthGuard],
    loadChildren: () =>
      import('./modules/profile/profile.module').then((m) => m.ProfileModule),
    data: {
      title: 'Profile',
      tags: [
        {
          name: 'title',
          content: 'Profile | Geng Gamer',
        }
      ]
    }
  },
  {
    path: 'account',
    canActivate: [AuthGuard],
    loadChildren: () =>
      import('./modules/wallet/wallet.module').then((m) => m.WalletModule),
    data: {
      title: 'Transactions',
      tags: [
        {
          name: 'title',
          content: 'My Transactions | Geng Gamer',
        }
      ]
    }
  },
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: '404', component: NotfoundComponent },
  { path: '**', redirectTo: '/home', pathMatch: 'full' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      scrollPositionRestoration: 'enabled',
      initialNavigation: 'enabled',
    }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule { }
