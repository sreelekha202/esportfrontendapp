import idbReady from 'safari-14-idb-fix';
import { enableProdMode } from "@angular/core";
import { platformBrowserDynamic } from "@angular/platform-browser-dynamic";
import { AppModule } from "./app/app.module";
import { environment } from "./environments/environment";

if (environment.production) {
  enableProdMode();
}
// Workaround for Safari/iOS bug;
const startupPromise = new Promise<void>(resolve => {
    resolve();
    idbReady().then(() => {
      const isIphone = /iPhone/.test(navigator.userAgent);

      if (!isIphone || !window.indexedDB) {
        resolve();
      }

      window.indexedDB.open('dummy', 1);

      setTimeout(() => {
        window.indexedDB.deleteDatabase('dummy');
      }, 10000);

      resolve();
    });
  });

platformBrowserDynamic()
  .bootstrapModule(AppModule)
  .catch((err) => console.error(err));
