export const environment = {
  production: true,
  buildConfig: 'prod',
  apiEndPoint: 'https://api.genggamer.gg/',
  currentToken: 'DUID',
  socketEndPoint: '' ,
  socketEndPointWSS: '' ,
  cookieDomain: '.genggamer.gg',
  articleS3BucketName: 'article',
  tournamentS3BucketName: 'tournament',
  enableFirebase: false,
  maxisLogin:
  'https://id2.maxis.com.my/oauth2/authorize?client_id=MAXISGAME&scope=openid%20maxis_profile%20email%20entitlement%20name%20uuid%20msisdn%20brand&response_type=code&redirect_uri=https://www.genggamer.gg/home&nonce=maxisgame&brand=MAXIS',
  maxisLogout:
  'https://id2.maxis.com.my/logout?returnUrl=https://www.genggamer.gg/home',
  defaultLangCode: 'en',
  rtl: ['en'],
  language: [
    { code: 'en', key: 'english', value: 'English' },
  ],
  pageSizeOptions: [5, 10, 15, 20],
  maxisOnlinePayment: 'https://genggamer.gg/paymentByDIDToken.do',
  gTagId: '',
  gTagUrl: 'https://www.googletagmanager.com/gtag/js?id=',
  gtmId: '',
};
