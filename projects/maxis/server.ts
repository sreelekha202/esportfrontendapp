import 'zone.js/dist/zone-node';

import { ngExpressEngine } from '@nguniversal/express-engine';
import express from 'express';
import { join } from 'path';

import { AppServerModule } from './src/main.server';
import { APP_BASE_HREF } from '@angular/common';
import { existsSync } from 'fs';
const domino = require('domino');
const fs_1 = require('fs');
const compression = require('compression');
const helmet = require('helmet');

(global as any).WebSocket = require('ws');
(global as any).XMLHttpRequest = require('xhr2');

import { environment } from './src/environments/environment';

function createTransformOptions() {
  const value = () => ({
    enumerable: true,
    configurable: true,
  });
  return { value };
}
function getMockMutationObserver() {
  return {};
}

function applyDomino(global, templatePath) {
  const template = fs_1.readFileSync(templatePath).toString();
  const win = domino.createWindow(template);
  global['window'] = win;
  global['document'] = win.document;
  global['localStorage'] = win.localStorage || {
    getItem: function () {},
    setItem: function () {},
  };
  global['navigator'] = win.navigator;
  Object.defineProperty(
    win.document.body.style,
    'transform',
    createTransformOptions()
  );
  global['CSS'] = null;
  global['Prism'] = null;
  global['MutationObserver'] = getMockMutationObserver();
}

//exports.applyDomino = applyDomino;
const BROWSER_DIR = join(process.cwd(), 'dist/maxis/browser');

applyDomino(global, join(BROWSER_DIR, 'index.html'));
// The Express app is exported so that it can be used by serverless Functions.
export function app(): express.Express {
  const server = express();
  const distFolder = join(process.cwd(), 'dist/maxis/browser');
  const indexHtml = existsSync(join(distFolder, 'index.original.html'))
    ? 'index.original.html'
    : 'index';

  server.use(compression());

  // Our Universal express-engine (found @ https://github.com/angular/universal/tree/master/modules/express-engine)
  server.engine(
    'html',
    ngExpressEngine({
      bootstrap: AppServerModule,
    })
  );

  server.set('view engine', 'html');
  server.set('views', distFolder);

  server.use(
    helmet.contentSecurityPolicy({
      directives: {
        upgradeInsecureRequests: [],
        blockAllMixedContent: [],
        defaultSrc: ["'none'"],
        'connect-src': [
          "'self'",
          environment.apiEndPoint,
          environment.socketEndPointWSS,
          environment.socketEndPoint,
          'https://vimeo.com',
        ],
        scriptSrc: [
          "'self'",
          'googletagmanager.com',
          environment.apiEndPoint,
          environment.socketEndPointWSS,
          'dynasty-staging.com',
          'dynasty-dev.com',
        ],
        scriptSrcElem: [
          "'self'",
          'https://www.googletagmanager.com',
          "'sha256-xgqphM9ePVp3yPBxalkUw/t0M76+eTbOpyYwz4nnSsg='",
          "'sha256-ACotEtBlkqjCUAsddlA/3p2h7Q0iHuDXxk577uNsXwA='",
          "'sha256-2daR3BDHUgNt2bWp/u+3CNDJtsIDrpz+22+QPnNNS5c='",
        ],
        objectSrc: ["'none'"],
        styleSrc: [
          "'self'",
          "'unsafe-inline'"
        ],
        mediaSrc: ["'self'", 'static.zdassets.com'],
        fontSrc: [
          "'self'",
          'fonts.googleapis.com',
          'fonts.gstatic.com',
          'netdna.bootstrapcdn.com',
          'data:',
        ],
        'frame-src': [
          "'self'",
        ],
        'img-src': [
          "'self'",
          'data:',
          'blob:',
          'https://maxis-dev-image.s3.ap-southeast-1.amazonaws.com/dev/',
          'https://maxis-stg-image.s3.ap-southeast-1.amazonaws.com/stage/',
          'https://maxis-prod-images.s3.ap-southeast-1.amazonaws.com/prod/',
          'https://img.youtube.com/',
          'https://img.icons8.com/',
          'https://i.vimeocdn.com',
        ],
        'frame-ancestors': ["'none'"],
        'base-uri': ["'self'"],
        'form-action': [
          "'self'",
          environment.apiEndPoint,
          'https://pay-staging.maxis.com.my/maxis-web-adapter-sit/paymentByDIDToken.do',
        ],
        'manifest-src': ["'self'"],
      },
      reportOnly: false,
    })
  );
  server.use(helmet.dnsPrefetchControl());
  server.use(helmet.expectCt());
  server.use(helmet.frameguard());
  server.use(helmet.hidePoweredBy());
  server.use(helmet.hsts());
  server.use(helmet.ieNoOpen());
  server.use(helmet.noSniff());
  server.use(helmet.permittedCrossDomainPolicies());
  server.use(helmet.referrerPolicy());
  server.use((req, res, next) => {
    res.setHeader('X-XSS-Protection', '1; mode=block');
    next();
  });
  // Example Express Rest API endpoints
  // server.get('/api/**', (req, res) => { });
  // Serve static files from /browser
  server.get(
    '*.*',
    express.static(distFolder, {
      maxAge: '1y',
    })
  );

  // All regular routes use the Universal engine
  server.get('*', (req, res) => {
    res.render(indexHtml, {
      req,
      providers: [{ provide: APP_BASE_HREF, useValue: req.baseUrl }],
    });
  });

  server.post('*', (req, res) => {
    res.render(indexHtml, {
      req,
      providers: [{ provide: APP_BASE_HREF, useValue: req.baseUrl }],
    });
  });

  return server;
}

function run(): void {
  const port = process.env.PORT || 8080;

  // Start up the Node server
  const server = app();
  server.listen(port, () => {
    console.log(`Node Express server listening on http://localhost:${port}`);
  });
}

// Webpack will replace 'require' with '__webpack_require__'
// '__non_webpack_require__' is a proxy to Node 'require'
// The below code is to ensure that the server is run only when not requiring the bundle.
declare const __non_webpack_require__: NodeRequire;
const mainModule = __non_webpack_require__.main;
const moduleFilename = (mainModule && mainModule.filename) || '';
if (moduleFilename === __filename || moduleFilename.includes('iisnode')) {
  run();
}

export * from './src/main.server';
