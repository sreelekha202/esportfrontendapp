# What is this repository for?

This repository is for all project centralize front end
# EsportWeb

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.2.0.

# Create Workspace

ng new esportWeb --create-application=false

# To generate library

ng generate library esports

# To generate application

ng generate application application-name (eg:ng generate application stc)

# To build library

ng build esports

For incremental builds use :- ng build esports --watch
# To run application 
npm install
npm run postinstall:application-name (eg:npm run postinstall:stc)
ng serve application-name (eg:ng serve stc)

# To run analyze 
npm install
npm run application-name:build:dev:ssr  (eg:npm run stc:build:dev:ssr)
npm run analyze:application-name (eg:npm run analyze:stc)


# system requirenment
Angular CLI: 11.2.17
Node: 14.17.6
npm: 6.14.15

components:
    chat
    counter
    drop down select
    chat log
    chat sidebar
    toggle switch

modules:
    custom pagination
    lader standings
    loader
    season
    snack bar
    quick tournament
    info popup
    material module
    shared module

pipes:
    All pipes

Services
    All services