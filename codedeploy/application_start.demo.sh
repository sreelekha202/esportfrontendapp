#!/bin/bash

export PATH=$PATH:/home/admin/.nvm/versions/node/v14.9.0/bin
cd /home/admin/dynastyesportsweb && /home/admin/.nvm/versions/node/v14.9.0/bin/pm2 start dist/demo/server/main.js -i max
