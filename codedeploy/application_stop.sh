#!/bin/bash
set -e

EXIT_CODE=0

/home/ec2-user/.nvm/versions/node/v14.9.0/bin/pm2 stop --silent main || EXIT_CODE=$?

echo $EXIT_CODE
